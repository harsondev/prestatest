{**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 *}
{block name='header_banner'}
  {capture name='displayBanner'}{hook h='displayBanner'}{/capture}
  {if $smarty.capture.displayBanner}
  <div class="header-banner">
    {$smarty.capture.displayBanner nofilter}
  </div>
  {/if}
{/block}

{block name='header_nav'}
  <nav class="header-nav hidden-md-up">
    <div class="container">
      <div class="row">
        <div class="text-sm-center mobile">
          <div class="lb-mobile">
            <div class="menu-toggle" id="menu-icon">
              <span></span>
              <span></span>
              <span></span>
            </div>
            <div class="search-toggle"><i class="psticon-search"></i></div>
          </div>
          
          <div class="top-logo" id="_mobile_logo"></div>
          <div class="" id="_mobile_user_info"></div>
          <div class="" id="_mobile_cart"></div>
          <div class="clearfix"></div>
        </div>
      </div>
    </div>
  </nav>
{/block}

{block name='header_top'}
  <div class="header-top">
    <div class="container">
       <div class="row">
        <div class="hidden-sm-down" id="_desktop_logo">
          {if $shop.logo_details}
            {renderLogo}
          {/if}
        </div>
        {hook h='displayTop'}
        
      </div>
      <div id="mobile_top_menu_wrapper" class="row hidden-md-up" style="display:none;">
        <div class="js-top-menu mobile" id="_mobile_top_menu"></div>
        <div class="js-top-menu-bottom">
          <div id="_mobile_currency_selector"></div>
          <div id="_mobile_language_selector"></div>
          <div id="_mobile_contact_link"></div>
        </div>
      </div>
    </div>
  </div>
  {hook h='displayNavFullWidth'}
{/block}
