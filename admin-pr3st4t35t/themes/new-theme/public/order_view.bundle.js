/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./js/fos_js_routes.json":
/*!*******************************!*\
  !*** ./js/fos_js_routes.json ***!
  \*******************************/
/***/ ((module) => {

"use strict";
module.exports = JSON.parse('{"base_url":"","routes":{"admin_common_notifications":{"tokens":[["text","/common/notifications"]],"defaults":[],"requirements":[],"hosttokens":[],"methods":["POST"],"schemes":[]},"admin_product_form":{"tokens":[["variable","/","\\\\d+","id"],["text","/sell/catalog/products"]],"defaults":[],"requirements":{"id":"\\\\d+"},"hosttokens":[],"methods":["GET","POST"],"schemes":[]},"admin_feature_get_feature_values":{"tokens":[["variable","/","\\\\d+","idFeature"],["text","/sell/catalog/products/features"]],"defaults":{"idFeature":0},"requirements":{"idFeature":"\\\\d+"},"hosttokens":[],"methods":["GET"],"schemes":[]},"admin_products_combinations":{"tokens":[["text","/combinations"],["variable","/","[^/]++","productId"],["text","/sell/catalog/products-v2"]],"defaults":[],"requirements":[],"hosttokens":[],"methods":["GET"],"schemes":[]},"admin_products_combinations_ids":{"tokens":[["text","/combinations/ids"],["variable","/","[^/]++","productId"],["text","/sell/catalog/products-v2"]],"defaults":[],"requirements":[],"hosttokens":[],"methods":["GET"],"schemes":[]},"admin_products_combinations_update_combination_from_listing":{"tokens":[["text","/update-combination-from-listing"],["variable","/","\\\\d+","combinationId"],["text","/sell/catalog/products-v2/combinations"]],"defaults":[],"requirements":{"combinationId":"\\\\d+"},"hosttokens":[],"methods":["PATCH"],"schemes":[]},"admin_products_combinations_edit_combination":{"tokens":[["text","/edit"],["variable","/","\\\\d+","combinationId"],["text","/sell/catalog/products-v2/combinations"]],"defaults":[],"requirements":{"combinationId":"\\\\d+"},"hosttokens":[],"methods":["GET","POST"],"schemes":[]},"admin_products_combinations_remove_combination":{"tokens":[["text","/remove"],["variable","/","\\\\d+","combinationId"],["text","/sell/catalog/products-v2/combinations"]],"defaults":[],"requirements":{"combinationId":"\\\\d+"},"hosttokens":[],"methods":["DELETE"],"schemes":[]},"admin_products_attribute_groups":{"tokens":[["text","/attribute-groups"],["variable","/","[^/]++","productId"],["text","/sell/catalog/products-v2"]],"defaults":[],"requirements":[],"hosttokens":[],"methods":["GET"],"schemes":[]},"admin_all_attribute_groups":{"tokens":[["text","/sell/catalog/products-v2/all-attribute-groups"]],"defaults":[],"requirements":[],"hosttokens":[],"methods":["GET"],"schemes":[]},"admin_products_combinations_generate":{"tokens":[["variable","/","[^/]++","productId"],["text","/sell/catalog/products-v2/generate-combinations"]],"defaults":[],"requirements":[],"hosttokens":[],"methods":["POST"],"schemes":[]},"admin_products_v2_get_images":{"tokens":[["text","/images"],["variable","/","\\\\d+","productId"],["text","/sell/catalog/products-v2"]],"defaults":[],"requirements":{"productId":"\\\\d+"},"hosttokens":[],"methods":["GET"],"schemes":[]},"admin_products_v2_add_image":{"tokens":[["text","/sell/catalog/products-v2/images/add"]],"defaults":[],"requirements":[],"hosttokens":[],"methods":["POST"],"schemes":[]},"admin_products_v2_update_image":{"tokens":[["text","/update"],["variable","/","\\\\d+","productImageId"],["text","/sell/catalog/products-v2/images"]],"defaults":[],"requirements":{"productImageId":"\\\\d+"},"hosttokens":[],"methods":["PATCH"],"schemes":[]},"admin_products_v2_delete_image":{"tokens":[["text","/delete"],["variable","/","\\\\d+","productImageId"],["text","/sell/catalog/products-v2/images"]],"defaults":[],"requirements":{"productImageId":"\\\\d+"},"hosttokens":[],"methods":["POST"],"schemes":[]},"admin_categories_get_categories_tree":{"tokens":[["text","/sell/catalog/categories/tree"]],"defaults":[],"requirements":[],"hosttokens":[],"methods":["GET"],"schemes":[]},"admin_cart_rules_search":{"tokens":[["text","/sell/catalog/cart-rules/search"]],"defaults":[],"requirements":[],"hosttokens":[],"methods":["GET"],"schemes":[]},"admin_customers_view":{"tokens":[["text","/view"],["variable","/","\\\\d+","customerId"],["text","/sell/customers"]],"defaults":[],"requirements":{"customerId":"\\\\d+"},"hosttokens":[],"methods":["GET","POST"],"schemes":[]},"admin_customers_search":{"tokens":[["text","/sell/customers/search"]],"defaults":[],"requirements":[],"hosttokens":[],"methods":["GET"],"schemes":[]},"admin_customers_carts":{"tokens":[["text","/carts"],["variable","/","\\\\d+","customerId"],["text","/sell/customers"]],"defaults":[],"requirements":{"customerId":"\\\\d+"},"hosttokens":[],"methods":["GET"],"schemes":[]},"admin_customers_orders":{"tokens":[["text","/orders"],["variable","/","\\\\d+","customerId"],["text","/sell/customers"]],"defaults":[],"requirements":{"customerId":"\\\\d+"},"hosttokens":[],"methods":["GET"],"schemes":[]},"admin_addresses_create":{"tokens":[["text","/sell/addresses/new"]],"defaults":[],"requirements":[],"hosttokens":[],"methods":["GET","POST"],"schemes":[]},"admin_addresses_edit":{"tokens":[["text","/edit"],["variable","/","\\\\d+","addressId"],["text","/sell/addresses"]],"defaults":[],"requirements":{"addressId":"\\\\d+"},"hosttokens":[],"methods":["GET","POST"],"schemes":[]},"admin_order_addresses_edit":{"tokens":[["text","/edit"],["variable","/","delivery|invoice","addressType"],["variable","/","\\\\d+","orderId"],["text","/sell/addresses/order"]],"defaults":[],"requirements":{"orderId":"\\\\d+","addressType":"delivery|invoice"},"hosttokens":[],"methods":["GET","POST"],"schemes":[]},"admin_cart_addresses_edit":{"tokens":[["text","/edit"],["variable","/","delivery|invoice","addressType"],["variable","/","\\\\d+","cartId"],["text","/sell/addresses/cart"]],"defaults":[],"requirements":{"cartId":"\\\\d+","addressType":"delivery|invoice"},"hosttokens":[],"methods":["GET","POST"],"schemes":[]},"admin_carts_view":{"tokens":[["text","/view"],["variable","/","\\\\d+","cartId"],["text","/sell/orders/carts"]],"defaults":[],"requirements":{"cartId":"\\\\d+"},"hosttokens":[],"methods":["GET"],"schemes":[]},"admin_carts_info":{"tokens":[["text","/info"],["variable","/","\\\\d+","cartId"],["text","/sell/orders/carts"]],"defaults":[],"requirements":{"cartId":"\\\\d+"},"hosttokens":[],"methods":["GET"],"schemes":[]},"admin_carts_create":{"tokens":[["text","/sell/orders/carts/new"]],"defaults":[],"requirements":[],"hosttokens":[],"methods":["POST"],"schemes":[]},"admin_carts_edit_addresses":{"tokens":[["text","/addresses"],["variable","/","\\\\d+","cartId"],["text","/sell/orders/carts"]],"defaults":[],"requirements":{"cartId":"\\\\d+"},"hosttokens":[],"methods":["POST"],"schemes":[]},"admin_carts_edit_carrier":{"tokens":[["text","/carrier"],["variable","/","\\\\d+","cartId"],["text","/sell/orders/carts"]],"defaults":[],"requirements":{"cartId":"\\\\d+"},"hosttokens":[],"methods":["POST"],"schemes":[]},"admin_carts_edit_currency":{"tokens":[["text","/currency"],["variable","/","\\\\d+","cartId"],["text","/sell/orders/carts"]],"defaults":[],"requirements":{"cartId":"\\\\d+"},"hosttokens":[],"methods":["POST"],"schemes":[]},"admin_carts_edit_language":{"tokens":[["text","/language"],["variable","/","\\\\d+","cartId"],["text","/sell/orders/carts"]],"defaults":[],"requirements":{"cartId":"\\\\d+"},"hosttokens":[],"methods":["POST"],"schemes":[]},"admin_carts_set_delivery_settings":{"tokens":[["text","/rules/delivery-settings"],["variable","/","\\\\d+","cartId"],["text","/sell/orders/carts"]],"defaults":[],"requirements":{"cartId":"\\\\d+"},"hosttokens":[],"methods":["POST"],"schemes":[]},"admin_carts_add_cart_rule":{"tokens":[["text","/cart-rules"],["variable","/","[^/]++","cartId"],["text","/sell/orders/carts"]],"defaults":[],"requirements":[],"hosttokens":[],"methods":["POST"],"schemes":[]},"admin_carts_delete_cart_rule":{"tokens":[["text","/delete"],["variable","/","[^/]++","cartRuleId"],["text","/cart-rules"],["variable","/","[^/]++","cartId"],["text","/sell/orders/carts"]],"defaults":[],"requirements":[],"hosttokens":[],"methods":["POST"],"schemes":[]},"admin_carts_add_product":{"tokens":[["text","/products"],["variable","/","\\\\d+","cartId"],["text","/sell/orders/carts"]],"defaults":[],"requirements":{"cartId":"\\\\d+"},"hosttokens":[],"methods":["POST"],"schemes":[]},"admin_carts_edit_product_price":{"tokens":[["text","/price"],["variable","/","\\\\d+","productId"],["text","/products"],["variable","/","\\\\d+","cartId"],["text","/sell/orders/carts"]],"defaults":[],"requirements":{"cartId":"\\\\d+","productId":"\\\\d+"},"hosttokens":[],"methods":["POST"],"schemes":[]},"admin_carts_edit_product_quantity":{"tokens":[["text","/quantity"],["variable","/","\\\\d+","productId"],["text","/products"],["variable","/","\\\\d+","cartId"],["text","/sell/orders/carts"]],"defaults":[],"requirements":{"cartId":"\\\\d+","productId":"\\\\d+"},"hosttokens":[],"methods":["POST"],"schemes":[]},"admin_carts_delete_product":{"tokens":[["text","/delete-product"],["variable","/","\\\\d+","cartId"],["text","/sell/orders/carts"]],"defaults":[],"requirements":{"cartId":"\\\\d+"},"hosttokens":[],"methods":["POST"],"schemes":[]},"admin_orders_place":{"tokens":[["text","/sell/orders/place"]],"defaults":[],"requirements":[],"hosttokens":[],"methods":["POST"],"schemes":[]},"admin_orders_view":{"tokens":[["text","/view"],["variable","/","\\\\d+","orderId"],["text","/sell/orders"]],"defaults":[],"requirements":{"orderId":"\\\\d+"},"hosttokens":[],"methods":["GET","POST"],"schemes":[]},"admin_orders_duplicate_cart":{"tokens":[["text","/duplicate-cart"],["variable","/","\\\\d+","orderId"],["text","/sell/orders"]],"defaults":[],"requirements":{"orderId":"\\\\d+"},"hosttokens":[],"methods":["POST"],"schemes":[]},"admin_orders_update_product":{"tokens":[["variable","/","\\\\d+","orderDetailId"],["text","/products"],["variable","/","\\\\d+","orderId"],["text","/sell/orders"]],"defaults":[],"requirements":{"orderId":"\\\\d+","orderDetailId":"\\\\d+"},"hosttokens":[],"methods":["POST"],"schemes":[]},"admin_orders_partial_refund":{"tokens":[["text","/partial-refund"],["variable","/","\\\\d+","orderId"],["text","/sell/orders"]],"defaults":[],"requirements":{"orderId":"\\\\d+"},"hosttokens":[],"methods":["POST"],"schemes":[]},"admin_orders_standard_refund":{"tokens":[["text","/standard-refund"],["variable","/","\\\\d+","orderId"],["text","/sell/orders"]],"defaults":[],"requirements":{"orderId":"\\\\d+"},"hosttokens":[],"methods":["POST"],"schemes":[]},"admin_orders_return_product":{"tokens":[["text","/return-product"],["variable","/","\\\\d+","orderId"],["text","/sell/orders"]],"defaults":[],"requirements":{"orderId":"\\\\d+"},"hosttokens":[],"methods":["POST"],"schemes":[]},"admin_orders_send_process_order_email":{"tokens":[["text","/sell/orders/process-order-email"]],"defaults":[],"requirements":[],"hosttokens":[],"methods":["POST"],"schemes":[]},"admin_orders_add_product":{"tokens":[["text","/products"],["variable","/","\\\\d+","orderId"],["text","/sell/orders"]],"defaults":[],"requirements":{"orderId":"\\\\d+"},"hosttokens":[],"methods":["POST"],"schemes":[]},"admin_orders_delete_product":{"tokens":[["text","/delete"],["variable","/","\\\\d+","orderDetailId"],["text","/products"],["variable","/","\\\\d+","orderId"],["text","/sell/orders"]],"defaults":[],"requirements":{"orderId":"\\\\d+","orderDetailId":"\\\\d+"},"hosttokens":[],"methods":["POST"],"schemes":[]},"admin_orders_get_discounts":{"tokens":[["text","/discounts"],["variable","/","\\\\d+","orderId"],["text","/sell/orders"]],"defaults":[],"requirements":{"orderId":"\\\\d+"},"hosttokens":[],"methods":["GET"],"schemes":[]},"admin_orders_get_prices":{"tokens":[["text","/prices"],["variable","/","\\\\d+","orderId"],["text","/sell/orders"]],"defaults":[],"requirements":{"orderId":"\\\\d+"},"hosttokens":[],"methods":["GET"],"schemes":[]},"admin_orders_get_payments":{"tokens":[["text","/payments"],["variable","/","\\\\d+","orderId"],["text","/sell/orders"]],"defaults":[],"requirements":{"orderId":"\\\\d+"},"hosttokens":[],"methods":["GET"],"schemes":[]},"admin_orders_get_products":{"tokens":[["text","/products"],["variable","/","\\\\d+","orderId"],["text","/sell/orders"]],"defaults":[],"requirements":{"orderId":"\\\\d+"},"hosttokens":[],"methods":["GET"],"schemes":[]},"admin_orders_get_invoices":{"tokens":[["text","/invoices"],["variable","/","\\\\d+","orderId"],["text","/sell/orders"]],"defaults":[],"requirements":{"orderId":"\\\\d+"},"hosttokens":[],"methods":["GET"],"schemes":[]},"admin_orders_get_documents":{"tokens":[["text","/documents"],["variable","/","\\\\d+","orderId"],["text","/sell/orders"]],"defaults":[],"requirements":{"orderId":"\\\\d+"},"hosttokens":[],"methods":["GET"],"schemes":[]},"admin_orders_get_shipping":{"tokens":[["text","/shipping"],["variable","/","\\\\d+","orderId"],["text","/sell/orders"]],"defaults":[],"requirements":{"orderId":"\\\\d+"},"hosttokens":[],"methods":["GET"],"schemes":[]},"admin_orders_cancellation":{"tokens":[["text","/cancellation"],["variable","/","\\\\d+","orderId"],["text","/sell/orders"]],"defaults":[],"requirements":{"orderId":"\\\\d+"},"hosttokens":[],"methods":["POST"],"schemes":[]},"admin_orders_configure_product_pagination":{"tokens":[["text","/sell/orders/configure-product-pagination"]],"defaults":[],"requirements":[],"hosttokens":[],"methods":["POST"],"schemes":[]},"admin_orders_product_prices":{"tokens":[["text","/products/prices"],["variable","/","\\\\d+","orderId"],["text","/sell/orders"]],"defaults":[],"requirements":{"orderId":"\\\\d+"},"hosttokens":[],"methods":["GET"],"schemes":[]},"admin_orders_products_search":{"tokens":[["text","/sell/orders/products/search"]],"defaults":[],"requirements":[],"hosttokens":[],"methods":["GET"],"schemes":[]},"admin_shops_search":{"tokens":[["variable","/","[^/]++","searchTerm"],["text","/configure/advanced/shops/search"]],"defaults":[],"requirements":[],"hosttokens":[],"methods":["GET"],"schemes":[]}},"prefix":"","host":"localhost","port":"","scheme":"http","locale":[]}');

/***/ }),

/***/ "./js/app/cldr/exception/localization.js":
/*!***********************************************!*\
  !*** ./js/app/cldr/exception/localization.js ***!
  \***********************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

"use strict";


Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 */
var LocalizationException = function LocalizationException(message) {
  (0, _classCallCheck3.default)(this, LocalizationException);

  this.message = message;
  this.name = 'LocalizationException';
};

exports.default = LocalizationException;

/***/ }),

/***/ "./js/app/cldr/index.js":
/*!******************************!*\
  !*** ./js/app/cldr/index.js ***!
  \******************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

"use strict";


Object.defineProperty(exports, "__esModule", ({
  value: true
}));
exports.NumberSymbol = exports.NumberFormatter = exports.NumberSpecification = exports.PriceSpecification = undefined;

var _numberFormatter = __webpack_require__(/*! @app/cldr/number-formatter */ "./js/app/cldr/number-formatter.js");

var _numberFormatter2 = _interopRequireDefault(_numberFormatter);

var _numberSymbol = __webpack_require__(/*! @app/cldr/number-symbol */ "./js/app/cldr/number-symbol.js");

var _numberSymbol2 = _interopRequireDefault(_numberSymbol);

var _price = __webpack_require__(/*! @app/cldr/specifications/price */ "./js/app/cldr/specifications/price.js");

var _price2 = _interopRequireDefault(_price);

var _number = __webpack_require__(/*! @app/cldr/specifications/number */ "./js/app/cldr/specifications/number.js");

var _number2 = _interopRequireDefault(_number);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 */
exports.PriceSpecification = _price2.default;
exports.NumberSpecification = _number2.default;
exports.NumberFormatter = _numberFormatter2.default;
exports.NumberSymbol = _numberSymbol2.default;

/***/ }),

/***/ "./js/app/cldr/number-formatter.js":
/*!*****************************************!*\
  !*** ./js/app/cldr/number-formatter.js ***!
  \*****************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

"use strict";


Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var _toConsumableArray2 = __webpack_require__(/*! babel-runtime/helpers/toConsumableArray */ "./node_modules/babel-runtime/helpers/toConsumableArray.js");

var _toConsumableArray3 = _interopRequireDefault(_toConsumableArray2);

var _keys = __webpack_require__(/*! babel-runtime/core-js/object/keys */ "./node_modules/babel-runtime/core-js/object/keys.js");

var _keys2 = _interopRequireDefault(_keys);

var _slicedToArray2 = __webpack_require__(/*! babel-runtime/helpers/slicedToArray */ "./node_modules/babel-runtime/helpers/slicedToArray.js");

var _slicedToArray3 = _interopRequireDefault(_slicedToArray2);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _numberSymbol = __webpack_require__(/*! @app/cldr/number-symbol */ "./js/app/cldr/number-symbol.js");

var _numberSymbol2 = _interopRequireDefault(_numberSymbol);

var _price = __webpack_require__(/*! @app/cldr/specifications/price */ "./js/app/cldr/specifications/price.js");

var _price2 = _interopRequireDefault(_price);

var _number = __webpack_require__(/*! @app/cldr/specifications/number */ "./js/app/cldr/specifications/number.js");

var _number2 = _interopRequireDefault(_number);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var escapeRE = __webpack_require__(/*! lodash.escaperegexp */ "./node_modules/lodash.escaperegexp/index.js"); /**
                                                * Copyright since 2007 PrestaShop SA and Contributors
                                                * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
                                                *
                                                * NOTICE OF LICENSE
                                                *
                                                * This source file is subject to the Open Software License (OSL 3.0)
                                                * that is bundled with this package in the file LICENSE.md.
                                                * It is also available through the world-wide-web at this URL:
                                                * https://opensource.org/licenses/OSL-3.0
                                                * If you did not receive a copy of the license and are unable to
                                                * obtain it through the world-wide-web, please send an email
                                                * to license@prestashop.com so we can send you a copy immediately.
                                                *
                                                * DISCLAIMER
                                                *
                                                * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
                                                * versions in the future. If you wish to customize PrestaShop for your
                                                * needs please refer to https://devdocs.prestashop.com/ for more information.
                                                *
                                                * @author    PrestaShop SA and Contributors <contact@prestashop.com>
                                                * @copyright Since 2007 PrestaShop SA and Contributors
                                                * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
                                                */
/**
 * These placeholders are used in CLDR number formatting templates.
 * They are meant to be replaced by the correct localized symbols in the number formatting process.
 */


var CURRENCY_SYMBOL_PLACEHOLDER = '¤';
var DECIMAL_SEPARATOR_PLACEHOLDER = '.';
var GROUP_SEPARATOR_PLACEHOLDER = ',';
var MINUS_SIGN_PLACEHOLDER = '-';
var PERCENT_SYMBOL_PLACEHOLDER = '%';
var PLUS_SIGN_PLACEHOLDER = '+';

var NumberFormatter = function () {
  /**
   * @param NumberSpecification specification Number specification to be used
   *   (can be a number spec, a price spec, a percentage spec)
   */
  function NumberFormatter(specification) {
    (0, _classCallCheck3.default)(this, NumberFormatter);

    this.numberSpecification = specification;
  }

  /**
   * Formats the passed number according to specifications.
   *
   * @param int|float|string number The number to format
   * @param NumberSpecification specification Number specification to be used
   *   (can be a number spec, a price spec, a percentage spec)
   *
   * @return string The formatted number
   *                You should use this this value for display, without modifying it
   */


  (0, _createClass3.default)(NumberFormatter, [{
    key: 'format',
    value: function format(number, specification) {
      if (specification !== undefined) {
        this.numberSpecification = specification;
      }

      /*
       * We need to work on the absolute value first.
       * Then the CLDR pattern will add the sign if relevant (at the end).
       */
      var num = Math.abs(number).toFixed(this.numberSpecification.getMaxFractionDigits());

      var _extractMajorMinorDig = this.extractMajorMinorDigits(num),
          _extractMajorMinorDig2 = (0, _slicedToArray3.default)(_extractMajorMinorDig, 2),
          majorDigits = _extractMajorMinorDig2[0],
          minorDigits = _extractMajorMinorDig2[1];

      majorDigits = this.splitMajorGroups(majorDigits);
      minorDigits = this.adjustMinorDigitsZeroes(minorDigits);

      // Assemble the final number
      var formattedNumber = majorDigits;

      if (minorDigits) {
        formattedNumber += DECIMAL_SEPARATOR_PLACEHOLDER + minorDigits;
      }

      // Get the good CLDR formatting pattern. Sign is important here !
      var pattern = this.getCldrPattern(number < 0);
      formattedNumber = this.addPlaceholders(formattedNumber, pattern);
      formattedNumber = this.replaceSymbols(formattedNumber);

      formattedNumber = this.performSpecificReplacements(formattedNumber);

      return formattedNumber;
    }

    /**
     * Get number's major and minor digits.
     *
     * Major digits are the "integer" part (before decimal separator),
     * minor digits are the fractional part
     * Result will be an array of exactly 2 items: [majorDigits, minorDigits]
     *
     * Usage example:
     *  list(majorDigits, minorDigits) = this.getMajorMinorDigits(decimalNumber);
     *
     * @param DecimalNumber number
     *
     * @return string[]
     */

  }, {
    key: 'extractMajorMinorDigits',
    value: function extractMajorMinorDigits(number) {
      // Get the number's major and minor digits.
      var result = number.toString().split('.');
      var majorDigits = result[0];
      var minorDigits = result[1] === undefined ? '' : result[1];

      return [majorDigits, minorDigits];
    }

    /**
     * Splits major digits into groups.
     *
     * e.g.: Given the major digits "1234567", and major group size
     *  configured to 3 digits, the result would be "1 234 567"
     *
     * @param string majorDigits The major digits to be grouped
     *
     * @return string The grouped major digits
     */

  }, {
    key: 'splitMajorGroups',
    value: function splitMajorGroups(digit) {
      if (!this.numberSpecification.isGroupingUsed()) {
        return digit;
      }

      // Reverse the major digits, since they are grouped from the right.
      var majorDigits = digit.split('').reverse();

      // Group the major digits.
      var groups = [];
      groups.push(majorDigits.splice(0, this.numberSpecification.getPrimaryGroupSize()));
      while (majorDigits.length) {
        groups.push(majorDigits.splice(0, this.numberSpecification.getSecondaryGroupSize()));
      }

      // Reverse back the digits and the groups
      groups = groups.reverse();
      var newGroups = [];
      groups.forEach(function (group) {
        newGroups.push(group.reverse().join(''));
      });

      // Reconstruct the major digits.
      return newGroups.join(GROUP_SEPARATOR_PLACEHOLDER);
    }

    /**
     * Adds or remove trailing zeroes, depending on specified min and max fraction digits numbers.
     *
     * @param string minorDigits Digits to be adjusted with (trimmed or padded) zeroes
     *
     * @return string The adjusted minor digits
     */

  }, {
    key: 'adjustMinorDigitsZeroes',
    value: function adjustMinorDigitsZeroes(minorDigits) {
      var digit = minorDigits;

      if (digit.length > this.numberSpecification.getMaxFractionDigits()) {
        // Strip any trailing zeroes.
        digit = digit.replace(/0+$/, '');
      }

      if (digit.length < this.numberSpecification.getMinFractionDigits()) {
        // Re-add needed zeroes
        digit = digit.padEnd(this.numberSpecification.getMinFractionDigits(), '0');
      }

      return digit;
    }

    /**
     * Get the CLDR formatting pattern.
     *
     * @see http://cldr.unicode.org/translation/number-patterns
     *
     * @param bool isNegative If true, the negative pattern
     * will be returned instead of the positive one
     *
     * @return string The CLDR formatting pattern
     */

  }, {
    key: 'getCldrPattern',
    value: function getCldrPattern(isNegative) {
      if (isNegative) {
        return this.numberSpecification.getNegativePattern();
      }

      return this.numberSpecification.getPositivePattern();
    }

    /**
     * Replace placeholder number symbols with relevant numbering system's symbols.
     *
     * @param string number
     *                       The number to process
     *
     * @return string
     *                The number with replaced symbols
     */

  }, {
    key: 'replaceSymbols',
    value: function replaceSymbols(number) {
      var symbols = this.numberSpecification.getSymbol();

      var map = {};
      map[DECIMAL_SEPARATOR_PLACEHOLDER] = symbols.getDecimal();
      map[GROUP_SEPARATOR_PLACEHOLDER] = symbols.getGroup();
      map[MINUS_SIGN_PLACEHOLDER] = symbols.getMinusSign();
      map[PERCENT_SYMBOL_PLACEHOLDER] = symbols.getPercentSign();
      map[PLUS_SIGN_PLACEHOLDER] = symbols.getPlusSign();

      return this.strtr(number, map);
    }

    /**
     * strtr() for JavaScript
     * Translate characters or replace substrings
     *
     * @param str
     *  String to parse
     * @param pairs
     *  Hash of ('from' => 'to', ...).
     *
     * @return string
     */

  }, {
    key: 'strtr',
    value: function strtr(str, pairs) {
      var substrs = (0, _keys2.default)(pairs).map(escapeRE);

      return str.split(RegExp('(' + substrs.join('|') + ')')).map(function (part) {
        return pairs[part] || part;
      }).join('');
    }

    /**
     * Add missing placeholders to the number using the passed CLDR pattern.
     *
     * Missing placeholders can be the percent sign, currency symbol, etc.
     *
     * e.g. with a currency CLDR pattern:
     *  - Passed number (partially formatted): 1,234.567
     *  - Returned number: 1,234.567 ¤
     *  ("¤" symbol is the currency symbol placeholder)
     *
     * @see http://cldr.unicode.org/translation/number-patterns
     *
     * @param formattedNumber
     *  Number to process
     * @param pattern
     *  CLDR formatting pattern to use
     *
     * @return string
     */

  }, {
    key: 'addPlaceholders',
    value: function addPlaceholders(formattedNumber, pattern) {
      /*
       * Regex groups explanation:
       * #          : literal "#" character. Once.
       * (,#+)*     : any other "#" characters group, separated by ",". Zero to infinity times.
       * 0          : literal "0" character. Once.
       * (\.[0#]+)* : any combination of "0" and "#" characters groups, separated by '.'.
       *              Zero to infinity times.
       */
      return pattern.replace(/#?(,#+)*0(\.[0#]+)*/, formattedNumber);
    }

    /**
     * Perform some more specific replacements.
     *
     * Specific replacements are needed when number specification is extended.
     * For instance, prices have an extended number specification in order to
     * add currency symbol to the formatted number.
     *
     * @param string formattedNumber
     *
     * @return mixed
     */

  }, {
    key: 'performSpecificReplacements',
    value: function performSpecificReplacements(formattedNumber) {
      if (this.numberSpecification instanceof _price2.default) {
        return formattedNumber.split(CURRENCY_SYMBOL_PLACEHOLDER).join(this.numberSpecification.getCurrencySymbol());
      }

      return formattedNumber;
    }
  }], [{
    key: 'build',
    value: function build(specifications) {
      var symbol = void 0;

      if (undefined !== specifications.numberSymbols) {
        symbol = new (Function.prototype.bind.apply(_numberSymbol2.default, [null].concat((0, _toConsumableArray3.default)(specifications.numberSymbols))))();
      } else {
        symbol = new (Function.prototype.bind.apply(_numberSymbol2.default, [null].concat((0, _toConsumableArray3.default)(specifications.symbol))))();
      }

      var specification = void 0;

      if (specifications.currencySymbol) {
        specification = new _price2.default(specifications.positivePattern, specifications.negativePattern, symbol, parseInt(specifications.maxFractionDigits, 10), parseInt(specifications.minFractionDigits, 10), specifications.groupingUsed, specifications.primaryGroupSize, specifications.secondaryGroupSize, specifications.currencySymbol, specifications.currencyCode);
      } else {
        specification = new _number2.default(specifications.positivePattern, specifications.negativePattern, symbol, parseInt(specifications.maxFractionDigits, 10), parseInt(specifications.minFractionDigits, 10), specifications.groupingUsed, specifications.primaryGroupSize, specifications.secondaryGroupSize);
      }

      return new NumberFormatter(specification);
    }
  }]);
  return NumberFormatter;
}();

exports.default = NumberFormatter;

/***/ }),

/***/ "./js/app/cldr/number-symbol.js":
/*!**************************************!*\
  !*** ./js/app/cldr/number-symbol.js ***!
  \**************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

"use strict";


Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _localization = __webpack_require__(/*! @app/cldr/exception/localization */ "./js/app/cldr/exception/localization.js");

var _localization2 = _interopRequireDefault(_localization);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var NumberSymbol = function () {
  /**
   * NumberSymbolList constructor.
   *
   * @param string decimal Decimal separator character
   * @param string group Digits group separator character
   * @param string list List elements separator character
   * @param string percentSign Percent sign character
   * @param string minusSign Minus sign character
   * @param string plusSign Plus sign character
   * @param string exponential Exponential character
   * @param string superscriptingExponent Superscripting exponent character
   * @param string perMille Permille sign character
   * @param string infinity The infinity sign. Corresponds to the IEEE infinity bit pattern.
   * @param string nan The NaN (Not A Number) sign. Corresponds to the IEEE NaN bit pattern.
   *
   * @throws LocalizationException
   */
  function NumberSymbol(decimal, group, list, percentSign, minusSign, plusSign, exponential, superscriptingExponent, perMille, infinity, nan) {
    (0, _classCallCheck3.default)(this, NumberSymbol);

    this.decimal = decimal;
    this.group = group;
    this.list = list;
    this.percentSign = percentSign;
    this.minusSign = minusSign;
    this.plusSign = plusSign;
    this.exponential = exponential;
    this.superscriptingExponent = superscriptingExponent;
    this.perMille = perMille;
    this.infinity = infinity;
    this.nan = nan;

    this.validateData();
  }

  /**
   * Get the decimal separator.
   *
   * @return string
   */


  (0, _createClass3.default)(NumberSymbol, [{
    key: 'getDecimal',
    value: function getDecimal() {
      return this.decimal;
    }

    /**
     * Get the digit groups separator.
     *
     * @return string
     */

  }, {
    key: 'getGroup',
    value: function getGroup() {
      return this.group;
    }

    /**
     * Get the list elements separator.
     *
     * @return string
     */

  }, {
    key: 'getList',
    value: function getList() {
      return this.list;
    }

    /**
     * Get the percent sign.
     *
     * @return string
     */

  }, {
    key: 'getPercentSign',
    value: function getPercentSign() {
      return this.percentSign;
    }

    /**
     * Get the minus sign.
     *
     * @return string
     */

  }, {
    key: 'getMinusSign',
    value: function getMinusSign() {
      return this.minusSign;
    }

    /**
     * Get the plus sign.
     *
     * @return string
     */

  }, {
    key: 'getPlusSign',
    value: function getPlusSign() {
      return this.plusSign;
    }

    /**
     * Get the exponential character.
     *
     * @return string
     */

  }, {
    key: 'getExponential',
    value: function getExponential() {
      return this.exponential;
    }

    /**
     * Get the exponent character.
     *
     * @return string
     */

  }, {
    key: 'getSuperscriptingExponent',
    value: function getSuperscriptingExponent() {
      return this.superscriptingExponent;
    }

    /**
     * Gert the per mille symbol (often "‰").
     *
     * @see https://en.wikipedia.org/wiki/Per_mille
     *
     * @return string
     */

  }, {
    key: 'getPerMille',
    value: function getPerMille() {
      return this.perMille;
    }

    /**
     * Get the infinity symbol (often "∞").
     *
     * @see https://en.wikipedia.org/wiki/Infinity_symbol
     *
     * @return string
     */

  }, {
    key: 'getInfinity',
    value: function getInfinity() {
      return this.infinity;
    }

    /**
     * Get the NaN (not a number) sign.
     *
     * @return string
     */

  }, {
    key: 'getNan',
    value: function getNan() {
      return this.nan;
    }

    /**
     * Symbols list validation.
     *
     * @throws LocalizationException
     */

  }, {
    key: 'validateData',
    value: function validateData() {
      if (!this.decimal || typeof this.decimal !== 'string') {
        throw new _localization2.default('Invalid decimal');
      }

      if (!this.group || typeof this.group !== 'string') {
        throw new _localization2.default('Invalid group');
      }

      if (!this.list || typeof this.list !== 'string') {
        throw new _localization2.default('Invalid symbol list');
      }

      if (!this.percentSign || typeof this.percentSign !== 'string') {
        throw new _localization2.default('Invalid percentSign');
      }

      if (!this.minusSign || typeof this.minusSign !== 'string') {
        throw new _localization2.default('Invalid minusSign');
      }

      if (!this.plusSign || typeof this.plusSign !== 'string') {
        throw new _localization2.default('Invalid plusSign');
      }

      if (!this.exponential || typeof this.exponential !== 'string') {
        throw new _localization2.default('Invalid exponential');
      }

      if (!this.superscriptingExponent || typeof this.superscriptingExponent !== 'string') {
        throw new _localization2.default('Invalid superscriptingExponent');
      }

      if (!this.perMille || typeof this.perMille !== 'string') {
        throw new _localization2.default('Invalid perMille');
      }

      if (!this.infinity || typeof this.infinity !== 'string') {
        throw new _localization2.default('Invalid infinity');
      }

      if (!this.nan || typeof this.nan !== 'string') {
        throw new _localization2.default('Invalid nan');
      }
    }
  }]);
  return NumberSymbol;
}(); /**
      * Copyright since 2007 PrestaShop SA and Contributors
      * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
      *
      * NOTICE OF LICENSE
      *
      * This source file is subject to the Open Software License (OSL 3.0)
      * that is bundled with this package in the file LICENSE.md.
      * It is also available through the world-wide-web at this URL:
      * https://opensource.org/licenses/OSL-3.0
      * If you did not receive a copy of the license and are unable to
      * obtain it through the world-wide-web, please send an email
      * to license@prestashop.com so we can send you a copy immediately.
      *
      * DISCLAIMER
      *
      * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
      * versions in the future. If you wish to customize PrestaShop for your
      * needs please refer to https://devdocs.prestashop.com/ for more information.
      *
      * @author    PrestaShop SA and Contributors <contact@prestashop.com>
      * @copyright Since 2007 PrestaShop SA and Contributors
      * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
      */


exports.default = NumberSymbol;

/***/ }),

/***/ "./js/app/cldr/specifications/number.js":
/*!**********************************************!*\
  !*** ./js/app/cldr/specifications/number.js ***!
  \**********************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

"use strict";


Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _localization = __webpack_require__(/*! @app/cldr/exception/localization */ "./js/app/cldr/exception/localization.js");

var _localization2 = _interopRequireDefault(_localization);

var _numberSymbol = __webpack_require__(/*! @app/cldr/number-symbol */ "./js/app/cldr/number-symbol.js");

var _numberSymbol2 = _interopRequireDefault(_numberSymbol);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 */
var NumberSpecification = function () {
  /**
   * Number specification constructor.
   *
   * @param string positivePattern CLDR formatting pattern for positive amounts
   * @param string negativePattern CLDR formatting pattern for negative amounts
   * @param NumberSymbol symbol Number symbol
   * @param int maxFractionDigits Maximum number of digits after decimal separator
   * @param int minFractionDigits Minimum number of digits after decimal separator
   * @param bool groupingUsed Is digits grouping used ?
   * @param int primaryGroupSize Size of primary digits group in the number
   * @param int secondaryGroupSize Size of secondary digits group in the number
   *
   * @throws LocalizationException
   */
  function NumberSpecification(positivePattern, negativePattern, symbol, maxFractionDigits, minFractionDigits, groupingUsed, primaryGroupSize, secondaryGroupSize) {
    (0, _classCallCheck3.default)(this, NumberSpecification);

    this.positivePattern = positivePattern;
    this.negativePattern = negativePattern;
    this.symbol = symbol;

    this.maxFractionDigits = maxFractionDigits;
    // eslint-disable-next-line
    this.minFractionDigits = maxFractionDigits < minFractionDigits ? maxFractionDigits : minFractionDigits;

    this.groupingUsed = groupingUsed;
    this.primaryGroupSize = primaryGroupSize;
    this.secondaryGroupSize = secondaryGroupSize;

    if (!this.positivePattern || typeof this.positivePattern !== 'string') {
      throw new _localization2.default('Invalid positivePattern');
    }

    if (!this.negativePattern || typeof this.negativePattern !== 'string') {
      throw new _localization2.default('Invalid negativePattern');
    }

    if (!this.symbol || !(this.symbol instanceof _numberSymbol2.default)) {
      throw new _localization2.default('Invalid symbol');
    }

    if (typeof this.maxFractionDigits !== 'number') {
      throw new _localization2.default('Invalid maxFractionDigits');
    }

    if (typeof this.minFractionDigits !== 'number') {
      throw new _localization2.default('Invalid minFractionDigits');
    }

    if (typeof this.groupingUsed !== 'boolean') {
      throw new _localization2.default('Invalid groupingUsed');
    }

    if (typeof this.primaryGroupSize !== 'number') {
      throw new _localization2.default('Invalid primaryGroupSize');
    }

    if (typeof this.secondaryGroupSize !== 'number') {
      throw new _localization2.default('Invalid secondaryGroupSize');
    }
  }

  /**
   * Get symbol.
   *
   * @return NumberSymbol
   */


  (0, _createClass3.default)(NumberSpecification, [{
    key: 'getSymbol',
    value: function getSymbol() {
      return this.symbol;
    }

    /**
     * Get the formatting rules for this number (when positive).
     *
     * This pattern uses the Unicode CLDR number pattern syntax
     *
     * @return string
     */

  }, {
    key: 'getPositivePattern',
    value: function getPositivePattern() {
      return this.positivePattern;
    }

    /**
     * Get the formatting rules for this number (when negative).
     *
     * This pattern uses the Unicode CLDR number pattern syntax
     *
     * @return string
     */

  }, {
    key: 'getNegativePattern',
    value: function getNegativePattern() {
      return this.negativePattern;
    }

    /**
     * Get the maximum number of digits after decimal separator (rounding if needed).
     *
     * @return int
     */

  }, {
    key: 'getMaxFractionDigits',
    value: function getMaxFractionDigits() {
      return this.maxFractionDigits;
    }

    /**
     * Get the minimum number of digits after decimal separator (fill with "0" if needed).
     *
     * @return int
     */

  }, {
    key: 'getMinFractionDigits',
    value: function getMinFractionDigits() {
      return this.minFractionDigits;
    }

    /**
     * Get the "grouping" flag. This flag defines if digits
     * grouping should be used when formatting this number.
     *
     * @return bool
     */

  }, {
    key: 'isGroupingUsed',
    value: function isGroupingUsed() {
      return this.groupingUsed;
    }

    /**
     * Get the size of primary digits group in the number.
     *
     * @return int
     */

  }, {
    key: 'getPrimaryGroupSize',
    value: function getPrimaryGroupSize() {
      return this.primaryGroupSize;
    }

    /**
     * Get the size of secondary digits groups in the number.
     *
     * @return int
     */

  }, {
    key: 'getSecondaryGroupSize',
    value: function getSecondaryGroupSize() {
      return this.secondaryGroupSize;
    }
  }]);
  return NumberSpecification;
}();

exports.default = NumberSpecification;

/***/ }),

/***/ "./js/app/cldr/specifications/price.js":
/*!*********************************************!*\
  !*** ./js/app/cldr/specifications/price.js ***!
  \*********************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

"use strict";


Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _localization = __webpack_require__(/*! @app/cldr/exception/localization */ "./js/app/cldr/exception/localization.js");

var _localization2 = _interopRequireDefault(_localization);

var _number = __webpack_require__(/*! @app/cldr/specifications/number */ "./js/app/cldr/specifications/number.js");

var _number2 = _interopRequireDefault(_number);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Currency display option: symbol notation.
 */
/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 */
var CURRENCY_DISPLAY_SYMBOL = 'symbol';

var PriceSpecification = function (_NumberSpecification) {
  (0, _inherits3.default)(PriceSpecification, _NumberSpecification);

  /**
   * Price specification constructor.
   *
   * @param string positivePattern CLDR formatting pattern for positive amounts
   * @param string negativePattern CLDR formatting pattern for negative amounts
   * @param NumberSymbol symbol Number symbol
   * @param int maxFractionDigits Maximum number of digits after decimal separator
   * @param int minFractionDigits Minimum number of digits after decimal separator
   * @param bool groupingUsed Is digits grouping used ?
   * @param int primaryGroupSize Size of primary digits group in the number
   * @param int secondaryGroupSize Size of secondary digits group in the number
   * @param string currencySymbol Currency symbol of this price (eg. : €)
   * @param currencyCode Currency code of this price (e.g.: EUR)
   *
   * @throws LocalizationException
   */
  function PriceSpecification(positivePattern, negativePattern, symbol, maxFractionDigits, minFractionDigits, groupingUsed, primaryGroupSize, secondaryGroupSize, currencySymbol, currencyCode) {
    (0, _classCallCheck3.default)(this, PriceSpecification);

    var _this = (0, _possibleConstructorReturn3.default)(this, (PriceSpecification.__proto__ || (0, _getPrototypeOf2.default)(PriceSpecification)).call(this, positivePattern, negativePattern, symbol, maxFractionDigits, minFractionDigits, groupingUsed, primaryGroupSize, secondaryGroupSize));

    _this.currencySymbol = currencySymbol;
    _this.currencyCode = currencyCode;

    if (!_this.currencySymbol || typeof _this.currencySymbol !== 'string') {
      throw new _localization2.default('Invalid currencySymbol');
    }

    if (!_this.currencyCode || typeof _this.currencyCode !== 'string') {
      throw new _localization2.default('Invalid currencyCode');
    }
    return _this;
  }

  /**
   * Get type of display for currency symbol.
   *
   * @return string
   */


  (0, _createClass3.default)(PriceSpecification, [{
    key: 'getCurrencySymbol',


    /**
     * Get the currency symbol
     * e.g.: €.
     *
     * @return string
     */
    value: function getCurrencySymbol() {
      return this.currencySymbol;
    }

    /**
     * Get the currency ISO code
     * e.g.: EUR.
     *
     * @return string
     */

  }, {
    key: 'getCurrencyCode',
    value: function getCurrencyCode() {
      return this.currencyCode;
    }
  }], [{
    key: 'getCurrencyDisplay',
    value: function getCurrencyDisplay() {
      return CURRENCY_DISPLAY_SYMBOL;
    }
  }]);
  return PriceSpecification;
}(_number2.default);

exports.default = PriceSpecification;

/***/ }),

/***/ "./js/components/event-emitter.js":
/*!****************************************!*\
  !*** ./js/components/event-emitter.js ***!
  \****************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

"use strict";


Object.defineProperty(exports, "__esModule", ({
  value: true
}));
exports.EventEmitter = undefined;

var _events = __webpack_require__(/*! events */ "./node_modules/events/events.js");

var _events2 = _interopRequireDefault(_events);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * We instanciate one EventEmitter (restricted via a const) so that every components
 * register/dispatch on the same one and can communicate with each other.
 */
var EventEmitter = exports.EventEmitter = new _events2.default(); /**
                                                                   * Copyright since 2007 PrestaShop SA and Contributors
                                                                   * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
                                                                   *
                                                                   * NOTICE OF LICENSE
                                                                   *
                                                                   * This source file is subject to the Open Software License (OSL 3.0)
                                                                   * that is bundled with this package in the file LICENSE.md.
                                                                   * It is also available through the world-wide-web at this URL:
                                                                   * https://opensource.org/licenses/OSL-3.0
                                                                   * If you did not receive a copy of the license and are unable to
                                                                   * obtain it through the world-wide-web, please send an email
                                                                   * to license@prestashop.com so we can send you a copy immediately.
                                                                   *
                                                                   * DISCLAIMER
                                                                   *
                                                                   * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
                                                                   * versions in the future. If you wish to customize PrestaShop for your
                                                                   * needs please refer to https://devdocs.prestashop.com/ for more information.
                                                                   *
                                                                   * @author    PrestaShop SA and Contributors <contact@prestashop.com>
                                                                   * @copyright Since 2007 PrestaShop SA and Contributors
                                                                   * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
                                                                   */

exports.default = EventEmitter;

/***/ }),

/***/ "./js/components/form/text-with-length-counter.js":
/*!********************************************************!*\
  !*** ./js/components/form/text-with-length-counter.js ***!
  \********************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

"use strict";


Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 */

var _window = window,
    $ = _window.$;

/**
 * TextWithLengthCounter handles input with length counter UI.
 *
 * Usage:
 *
 * There must be an element that wraps both input & counter display with ".js-text-with-length-counter" class.
 * Counter display must have ".js-countable-text-display" class and input must have ".js-countable-text-input" class.
 * Text input must have "data-max-length" attribute.
 *
 * <div class="js-text-with-length-counter">
 *  <span class="js-countable-text"></span>
 *  <input class="js-countable-input" data-max-length="255">
 * </div>
 *
 * In Javascript you must enable this component:
 *
 * new TextWithLengthCounter();
 */

var TextWithLengthCounter = function TextWithLengthCounter() {
  var _this = this;

  (0, _classCallCheck3.default)(this, TextWithLengthCounter);

  this.wrapperSelector = '.js-text-with-length-counter';
  this.textSelector = '.js-countable-text';
  this.inputSelector = '.js-countable-input';

  $(document).on('input', this.wrapperSelector + ' ' + this.inputSelector, function (e) {
    var $input = $(e.currentTarget);
    var remainingLength = $input.data('max-length') - $input.val().length;

    $input.closest(_this.wrapperSelector).find(_this.textSelector).text(remainingLength);
  });
};

exports.default = TextWithLengthCounter;

/***/ }),

/***/ "./js/components/modal.js":
/*!********************************!*\
  !*** ./js/components/modal.js ***!
  \********************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

"use strict";


Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var _toConsumableArray2 = __webpack_require__(/*! babel-runtime/helpers/toConsumableArray */ "./node_modules/babel-runtime/helpers/toConsumableArray.js");

var _toConsumableArray3 = _interopRequireDefault(_toConsumableArray2);

exports.default = ConfirmModal;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 */

var _window = window,
    $ = _window.$;

/**
 * ConfirmModal component
 *
 * @param {String} id
 * @param {String} confirmTitle
 * @param {String} confirmMessage
 * @param {String} closeButtonLabel
 * @param {String} confirmButtonLabel
 * @param {String} confirmButtonClass
 * @param {Array} customButtons
 * @param {Boolean} closable
 * @param {Function} confirmCallback
 * @param {Function} cancelCallback
 *
 */

function ConfirmModal(params, confirmCallback, cancelCallback) {
  var _this = this;

  // Construct the modal
  var id = params.id,
      closable = params.closable;

  this.modal = Modal(params);

  // jQuery modal object
  this.$modal = $(this.modal.container);

  this.show = function () {
    _this.$modal.modal();
  };

  this.modal.confirmButton.addEventListener('click', confirmCallback);

  this.$modal.modal({
    backdrop: closable ? true : 'static',
    keyboard: closable !== undefined ? closable : true,
    closable: closable !== undefined ? closable : true,
    show: false
  });

  this.$modal.on('hidden.bs.modal', function () {
    document.querySelector('#' + id).remove();
    if (cancelCallback) {
      cancelCallback();
    }
  });

  document.body.appendChild(this.modal.container);
}

/**
 * Modal component to improve lisibility by constructing the modal outside the main function
 *
 * @param {Object} params
 *
 */
function Modal(_ref) {
  var _modal$footer;

  var _ref$id = _ref.id,
      id = _ref$id === undefined ? 'confirm-modal' : _ref$id,
      confirmTitle = _ref.confirmTitle,
      _ref$confirmMessage = _ref.confirmMessage,
      confirmMessage = _ref$confirmMessage === undefined ? '' : _ref$confirmMessage,
      _ref$closeButtonLabel = _ref.closeButtonLabel,
      closeButtonLabel = _ref$closeButtonLabel === undefined ? 'Close' : _ref$closeButtonLabel,
      _ref$confirmButtonLab = _ref.confirmButtonLabel,
      confirmButtonLabel = _ref$confirmButtonLab === undefined ? 'Accept' : _ref$confirmButtonLab,
      _ref$confirmButtonCla = _ref.confirmButtonClass,
      confirmButtonClass = _ref$confirmButtonCla === undefined ? 'btn-primary' : _ref$confirmButtonCla,
      _ref$customButtons = _ref.customButtons,
      customButtons = _ref$customButtons === undefined ? [] : _ref$customButtons;

  var modal = {};

  // Main modal element
  modal.container = document.createElement('div');
  modal.container.classList.add('modal', 'fade');
  modal.container.id = id;

  // Modal dialog element
  modal.dialog = document.createElement('div');
  modal.dialog.classList.add('modal-dialog');

  // Modal content element
  modal.content = document.createElement('div');
  modal.content.classList.add('modal-content');

  // Modal header element
  modal.header = document.createElement('div');
  modal.header.classList.add('modal-header');

  // Modal title element
  if (confirmTitle) {
    modal.title = document.createElement('h4');
    modal.title.classList.add('modal-title');
    modal.title.innerHTML = confirmTitle;
  }

  // Modal close button icon
  modal.closeIcon = document.createElement('button');
  modal.closeIcon.classList.add('close');
  modal.closeIcon.setAttribute('type', 'button');
  modal.closeIcon.dataset.dismiss = 'modal';
  modal.closeIcon.innerHTML = '×';

  // Modal body element
  modal.body = document.createElement('div');
  modal.body.classList.add('modal-body', 'text-left', 'font-weight-normal');

  // Modal message element
  modal.message = document.createElement('p');
  modal.message.classList.add('confirm-message');
  modal.message.innerHTML = confirmMessage;

  // Modal footer element
  modal.footer = document.createElement('div');
  modal.footer.classList.add('modal-footer');

  // Modal close button element
  modal.closeButton = document.createElement('button');
  modal.closeButton.setAttribute('type', 'button');
  modal.closeButton.classList.add('btn', 'btn-outline-secondary', 'btn-lg');
  modal.closeButton.dataset.dismiss = 'modal';
  modal.closeButton.innerHTML = closeButtonLabel;

  // Modal confirm button element
  modal.confirmButton = document.createElement('button');
  modal.confirmButton.setAttribute('type', 'button');
  modal.confirmButton.classList.add('btn', confirmButtonClass, 'btn-lg', 'btn-confirm-submit');
  modal.confirmButton.dataset.dismiss = 'modal';
  modal.confirmButton.innerHTML = confirmButtonLabel;

  // Constructing the modal
  if (confirmTitle) {
    modal.header.append(modal.title, modal.closeIcon);
  } else {
    modal.header.appendChild(modal.closeIcon);
  }

  modal.body.appendChild(modal.message);
  (_modal$footer = modal.footer).append.apply(_modal$footer, [modal.closeButton].concat((0, _toConsumableArray3.default)(customButtons), [modal.confirmButton]));
  modal.content.append(modal.header, modal.body, modal.footer);
  modal.dialog.appendChild(modal.content);
  modal.container.appendChild(modal.dialog);

  return modal;
}

/***/ }),

/***/ "./js/components/router.js":
/*!*********************************!*\
  !*** ./js/components/router.js ***!
  \*********************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

"use strict";


Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var _assign = __webpack_require__(/*! babel-runtime/core-js/object/assign */ "./node_modules/babel-runtime/core-js/object/assign.js");

var _assign2 = _interopRequireDefault(_assign);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _fosRouting = __webpack_require__(/*! fos-routing */ "./node_modules/fos-routing/dist/routing.js");

var _fosRouting2 = _interopRequireDefault(_fosRouting);

var _fos_js_routes = __webpack_require__(/*! @js/fos_js_routes.json */ "./js/fos_js_routes.json");

var _fos_js_routes2 = _interopRequireDefault(_fos_js_routes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 */

var _window = window,
    $ = _window.$;

/**
 * Wraps FOSJsRoutingbundle with exposed routes.
 * To expose route add option `expose: true` in .yml routing config
 *
 * e.g.
 *
 * `my_route
 *    path: /my-path
 *    options:
 *      expose: true
 * `
 * And run `bin/console fos:js-routing:dump --format=json --target=admin-dev/themes/new-theme/js/fos_js_routes.json`
 */

var Router = function () {
  function Router() {
    (0, _classCallCheck3.default)(this, Router);

    if (window.prestashop && window.prestashop.customRoutes) {
      (0, _assign2.default)(_fos_js_routes2.default.routes, window.prestashop.customRoutes);
    }

    _fosRouting2.default.setData(_fos_js_routes2.default);
    _fosRouting2.default.setBaseUrl($(document).find('body').data('base-url'));

    return this;
  }

  /**
   * Decorated "generate" method, with predefined security token in params
   *
   * @param route
   * @param params
   *
   * @returns {String}
   */


  (0, _createClass3.default)(Router, [{
    key: 'generate',
    value: function generate(route) {
      var params = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

      var tokenizedParams = (0, _assign2.default)(params, {
        _token: $(document).find('body').data('token')
      });

      return _fosRouting2.default.generate(route, tokenizedParams);
    }
  }]);
  return Router;
}();

exports.default = Router;

/***/ }),

/***/ "./js/pages/order/OrderViewPageMap.js":
/*!********************************************!*\
  !*** ./js/pages/order/OrderViewPageMap.js ***!
  \********************************************/
/***/ ((__unused_webpack_module, exports) => {

"use strict";


Object.defineProperty(exports, "__esModule", ({
  value: true
}));
/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 */

/* eslint-disable max-len */

exports.default = {
  mainDiv: '#order-view-page',
  orderPaymentDetailsBtn: '.js-payment-details-btn',
  orderPaymentFormAmountInput: '#order_payment_amount',
  orderPaymentInvoiceSelect: '#order_payment_id_invoice',
  viewOrderPaymentsBlock: '#view_order_payments_block',
  viewOrderPaymentsAlert: '.js-view-order-payments-alert',
  privateNoteToggleBtn: '.js-private-note-toggle-btn',
  privateNoteBlock: '.js-private-note-block',
  privateNoteInput: '#private_note_note',
  privateNoteSubmitBtn: '.js-private-note-btn',
  addCartRuleModal: '#addOrderDiscountModal',
  addCartRuleInvoiceIdSelect: '#add_order_cart_rule_invoice_id',
  addCartRuleNameInput: '#add_order_cart_rule_name',
  addCartRuleTypeSelect: '#add_order_cart_rule_type',
  addCartRuleValueInput: '#add_order_cart_rule_value',
  addCartRuleValueUnit: '#add_order_cart_rule_value_unit',
  addCartRuleSubmit: '#add_order_cart_rule_submit',
  cartRuleHelpText: '.js-cart-rule-value-help',
  updateOrderStatusActionBtn: '#update_order_status_action_btn',
  updateOrderStatusActionInput: '#update_order_status_action_input',
  updateOrderStatusActionInputWrapper: '#update_order_status_action_input_wrapper',
  updateOrderStatusActionForm: '#update_order_status_action_form',
  showOrderShippingUpdateModalBtn: '.js-update-shipping-btn',
  updateOrderShippingTrackingNumberInput: '#update_order_shipping_tracking_number',
  updateOrderShippingCurrentOrderCarrierIdInput: '#update_order_shipping_current_order_carrier_id',
  updateCustomerAddressModal: '#updateCustomerAddressModal',
  openOrderAddressUpdateModalBtn: '.js-update-customer-address-modal-btn',
  updateOrderAddressTypeInput: '#change_order_address_address_type',
  deliveryAddressEditBtn: '#js-delivery-address-edit-btn',
  invoiceAddressEditBtn: '#js-invoice-address-edit-btn',
  orderMessageNameSelect: '#order_message_order_message',
  orderMessagesContainer: '.js-order-messages-container',
  orderMessage: '#order_message_message',
  orderMessageChangeWarning: '.js-message-change-warning',
  orderDocumentsTabCount: '#orderDocumentsTab .count',
  orderDocumentsTabBody: '#orderDocumentsTabContent .card-body',
  orderShippingTabCount: '#orderShippingTab .count',
  orderShippingTabBody: '#orderShippingTabContent .card-body',
  allMessagesModal: '#view_all_messages_modal',
  allMessagesList: '#all-messages-list',
  openAllMessagesBtn: '.js-open-all-messages-btn',
  // Products table elements
  productOriginalPosition: '#orderProductsOriginalPosition',
  productModificationPosition: '#orderProductsModificationPosition',
  productsPanel: '#orderProductsPanel',
  productsCount: '#orderProductsPanelCount',
  productDeleteBtn: '.js-order-product-delete-btn',
  productsTable: '#orderProductsTable',
  productsPagination: '.order-product-pagination',
  productsNavPagination: '#orderProductsNavPagination',
  productsTablePagination: '#orderProductsTablePagination',
  productsTablePaginationNext: '#orderProductsTablePaginationNext',
  productsTablePaginationPrev: '#orderProductsTablePaginationPrev',
  productsTablePaginationLink: '.page-item:not(.d-none):not(#orderProductsTablePaginationNext):not(#orderProductsTablePaginationPrev) .page-link',
  productsTablePaginationActive: '#orderProductsTablePagination .page-item.active span',
  productsTablePaginationTemplate: '#orderProductsTablePagination .page-item.d-none',
  productsTablePaginationNumberSelector: '#orderProductsTablePaginationNumberSelector',
  productsTableRow: function productsTableRow(productId) {
    return '#orderProduct_' + productId;
  },
  productsTableRowEdited: function productsTableRowEdited(productId) {
    return '#editOrderProduct_' + productId;
  },
  productsTableRows: 'tr.cellProduct',
  productsCellLocation: 'tr .cellProductLocation',
  productsCellRefunded: 'tr .cellProductRefunded',
  productsCellLocationDisplayed: 'tr:not(.d-none) .cellProductLocation',
  productsCellRefundedDisplayed: 'tr:not(.d-none) .cellProductRefunded',
  productsTableCustomizationRows: '#orderProductsTable .order-product-customization',
  productEditButtons: '.js-order-product-edit-btn',
  productEditBtn: function productEditBtn(productId) {
    return '#orderProduct_' + productId + ' .js-order-product-edit-btn';
  },
  productAddBtn: '#addProductBtn',
  productActionBtn: '.js-product-action-btn',
  productAddActionBtn: '#add_product_row_add',
  productCancelAddBtn: '#add_product_row_cancel',
  productAddRow: '#addProductTableRow',
  productSearchInput: '#add_product_row_search',
  productSearchInputAutocomplete: '#addProductTableRow .dropdown',
  productSearchInputAutocompleteMenu: '#addProductTableRow .dropdown .dropdown-menu',
  productAddIdInput: '#add_product_row_product_id',
  productAddTaxRateInput: '#add_product_row_tax_rate',
  productAddCombinationsBlock: '#addProductCombinations',
  productAddCombinationsSelect: '#addProductCombinationId',
  productAddPriceTaxExclInput: '#add_product_row_price_tax_excluded',
  productAddPriceTaxInclInput: '#add_product_row_price_tax_included',
  productAddQuantityInput: '#add_product_row_quantity',
  productAddAvailableText: '#addProductAvailable',
  productAddLocationText: '#addProductLocation',
  productAddTotalPriceText: '#addProductTotalPrice',
  productAddInvoiceSelect: '#add_product_row_invoice',
  productAddFreeShippingSelect: '#add_product_row_free_shipping',
  productAddNewInvoiceInfo: '#addProductNewInvoiceInfo',
  productEditSaveBtn: '.productEditSaveBtn',
  productEditCancelBtn: '.productEditCancelBtn',
  productEditRowTemplate: '#editProductTableRowTemplate',
  productEditRow: '.editProductRow',
  productEditImage: '.cellProductImg',
  productEditName: '.cellProductName',
  productEditUnitPrice: '.cellProductUnitPrice',
  productEditQuantity: '.cellProductQuantity',
  productEditAvailableQuantity: '.cellProductAvailableQuantity',
  productEditTotalPrice: '.cellProductTotalPrice',
  productEditPriceTaxExclInput: '.editProductPriceTaxExcl',
  productEditPriceTaxInclInput: '.editProductPriceTaxIncl',
  productEditInvoiceSelect: '.editProductInvoice',
  productEditQuantityInput: '.editProductQuantity',
  productEditLocationText: '.editProductLocation',
  productEditAvailableText: '.editProductAvailable',
  productEditTotalPriceText: '.editProductTotalPrice',
  // Product Discount List
  productDiscountList: {
    list: '.table.discountList'
  },
  // Product Pack Modal
  productPackModal: {
    modal: '#product-pack-modal',
    table: '#product-pack-modal-table tbody',
    rows: '#product-pack-modal-table tbody tr:not(#template-pack-table-row)',
    template: '#template-pack-table-row',
    product: {
      img: '.cell-product-img img',
      link: '.cell-product-name a',
      name: '.cell-product-name .product-name',
      ref: '.cell-product-name .product-reference',
      supplierRef: '.cell-product-name .product-supplier-reference',
      quantity: '.cell-product-quantity',
      availableQuantity: '.cell-product-available-quantity'
    }
  },
  // Order price elements
  orderProductsTotal: '#orderProductsTotal',
  orderDiscountsTotalContainer: '#order-discounts-total-container',
  orderDiscountsTotal: '#orderDiscountsTotal',
  orderWrappingTotal: '#orderWrappingTotal',
  orderShippingTotalContainer: '#order-shipping-total-container',
  orderShippingTotal: '#orderShippingTotal',
  orderTaxesTotal: '#orderTaxesTotal',
  orderTotal: '#orderTotal',
  orderHookTabsContainer: '#order_hook_tabs',
  // Product cancel/refund elements
  cancelProduct: {
    form: 'form[name="cancel_product"]',
    buttons: {
      abort: 'button.cancel-product-element-abort',
      save: '#cancel_product_save',
      partialRefund: 'button.partial-refund-display',
      standardRefund: 'button.standard-refund-display',
      returnProduct: 'button.return-product-display',
      cancelProducts: 'button.cancel-product-display'
    },
    inputs: {
      quantity: '.cancel-product-quantity input',
      amount: '.cancel-product-amount input',
      selector: '.cancel-product-selector input'
    },
    table: {
      cell: '.cancel-product-cell',
      header: 'th.cancel-product-element p',
      actions: 'td.cellProductActions, th.product_actions'
    },
    checkboxes: {
      restock: '#cancel_product_restock',
      creditSlip: '#cancel_product_credit_slip',
      voucher: '#cancel_product_voucher'
    },
    radios: {
      voucherRefundType: {
        productPrices: 'input[voucher-refund-type="0"]',
        productPricesVoucherExcluded: 'input[voucher-refund-type="1"]',
        negativeErrorMessage: '.voucher-refund-type-negative-error'
      }
    },
    toggle: {
      partialRefund: '.cancel-product-element:not(.hidden):not(.shipping-refund), .cancel-product-amount',
      standardRefund: '.cancel-product-element:not(.hidden):not(.shipping-refund-amount):not(.restock-products), .cancel-product-selector',
      returnProduct: '.cancel-product-element:not(.hidden):not(.shipping-refund-amount), .cancel-product-selector',
      cancelProducts: '.cancel-product-element:not(.hidden):not(.shipping-refund-amount):not(.shipping-refund):not(.restock-products):not(.refund-credit-slip):not(.refund-voucher):not(.voucher-refund-type), .cancel-product-selector'
    }
  },
  printOrderViewPageButton: '.js-print-order-view-page',
  orderNoteToggleBtn: '.js-order-notes-toggle-btn',
  orderNoteBlock: '.js-order-notes-block',
  orderNoteInput: '#internal_note_note',
  orderNoteSubmitBtn: '.js-order-notes-btn',
  refreshProductsListLoadingSpinner: '#orderProductsPanel .spinner-order-products-container#orderProductsLoading'
};

/***/ }),

/***/ "./js/pages/order/invoice-note-manager.js":
/*!************************************************!*\
  !*** ./js/pages/order/invoice-note-manager.js ***!
  \************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

"use strict";


Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _OrderViewPageMap = __webpack_require__(/*! ./OrderViewPageMap */ "./js/pages/order/OrderViewPageMap.js");

var _OrderViewPageMap2 = _interopRequireDefault(_OrderViewPageMap);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _window = window,
    $ = _window.$;

/**
 * Manages adding/editing note for invoice documents.
 */
/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 */

var InvoiceNoteManager = function () {
  function InvoiceNoteManager() {
    (0, _classCallCheck3.default)(this, InvoiceNoteManager);

    this.setupListeners();
  }

  (0, _createClass3.default)(InvoiceNoteManager, [{
    key: 'setupListeners',
    value: function setupListeners() {
      this.initShowNoteFormEventHandler();
      this.initCloseNoteFormEventHandler();
      this.initEnterPaymentEventHandler();
    }
  }, {
    key: 'initShowNoteFormEventHandler',
    value: function initShowNoteFormEventHandler() {
      $('.js-open-invoice-note-btn').on('click', function (event) {
        event.preventDefault();
        var $btn = $(event.currentTarget);
        var $noteRow = $btn.closest('tr').next();

        $noteRow.removeClass('d-none');
      });
    }
  }, {
    key: 'initCloseNoteFormEventHandler',
    value: function initCloseNoteFormEventHandler() {
      $('.js-cancel-invoice-note-btn').on('click', function (event) {
        $(event.currentTarget).closest('tr').addClass('d-none');
      });
    }
  }, {
    key: 'initEnterPaymentEventHandler',
    value: function initEnterPaymentEventHandler() {
      $('.js-enter-payment-btn').on('click', function (event) {
        var $btn = $(event.currentTarget);
        var paymentAmount = $btn.data('payment-amount');

        $(_OrderViewPageMap2.default.viewOrderPaymentsBlock).get(0).scrollIntoView({ behavior: 'smooth' });
        $(_OrderViewPageMap2.default.orderPaymentFormAmountInput).val(paymentAmount);
      });
    }
  }]);
  return InvoiceNoteManager;
}();

exports.default = InvoiceNoteManager;

/***/ }),

/***/ "./js/pages/order/message/order-view-page-messages-handler.js":
/*!********************************************************************!*\
  !*** ./js/pages/order/message/order-view-page-messages-handler.js ***!
  \********************************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

"use strict";


Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _OrderViewPageMap = __webpack_require__(/*! ../OrderViewPageMap */ "./js/pages/order/OrderViewPageMap.js");

var _OrderViewPageMap2 = _interopRequireDefault(_OrderViewPageMap);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _window = window,
    $ = _window.$;

/**
 * All actions for order view page messages are registered in this class.
 */
/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 */

var OrderViewPageMessagesHandler = function () {
  function OrderViewPageMessagesHandler() {
    var _this = this;

    (0, _classCallCheck3.default)(this, OrderViewPageMessagesHandler);

    this.$orderMessageChangeWarning = $(_OrderViewPageMap2.default.orderMessageChangeWarning);
    this.$messagesContainer = $(_OrderViewPageMap2.default.orderMessagesContainer);

    return {
      listenForPredefinedMessageSelection: function listenForPredefinedMessageSelection() {
        return _this.handlePredefinedMessageSelection();
      },
      listenForFullMessagesOpen: function listenForFullMessagesOpen() {
        return _this.onFullMessagesOpen();
      }
    };
  }

  /**
   * Handles predefined order message selection.
   *
   * @private
   */


  (0, _createClass3.default)(OrderViewPageMessagesHandler, [{
    key: 'handlePredefinedMessageSelection',
    value: function handlePredefinedMessageSelection() {
      var _this2 = this;

      $(document).on('change', _OrderViewPageMap2.default.orderMessageNameSelect, function (e) {
        var $currentItem = $(e.currentTarget);
        var valueId = $currentItem.val();

        if (!valueId) {
          return;
        }

        var message = _this2.$messagesContainer.find('div[data-id=' + valueId + ']').text().trim();
        var $orderMessage = $(_OrderViewPageMap2.default.orderMessage);
        var isSameMessage = $orderMessage.val().trim() === message;

        if (isSameMessage) {
          return;
        }

        if ($orderMessage.val() && !window.confirm(_this2.$orderMessageChangeWarning.text())) {
          return;
        }

        $orderMessage.val(message);
        $orderMessage.trigger('input');
      });
    }

    /**
     * Listens for event when all messages modal is being opened
     *
     * @private
     */

  }, {
    key: 'onFullMessagesOpen',
    value: function onFullMessagesOpen() {
      var _this3 = this;

      $(document).on('click', _OrderViewPageMap2.default.openAllMessagesBtn, function () {
        return _this3.scrollToMsgListBottom();
      });
    }

    /**
     * Scrolls down to the bottom of all messages list
     *
     * @private
     */

  }, {
    key: 'scrollToMsgListBottom',
    value: function scrollToMsgListBottom() {
      var $msgModal = $(_OrderViewPageMap2.default.allMessagesModal);
      var msgList = document.querySelector(_OrderViewPageMap2.default.allMessagesList);

      var classCheckInterval = window.setInterval(function () {
        if ($msgModal.hasClass('show')) {
          msgList.scrollTop = msgList.scrollHeight;
          clearInterval(classCheckInterval);
        }
      }, 10);
    }
  }]);
  return OrderViewPageMessagesHandler;
}();

exports.default = OrderViewPageMessagesHandler;

/***/ }),

/***/ "./js/pages/order/order-shipping-manager.js":
/*!**************************************************!*\
  !*** ./js/pages/order/order-shipping-manager.js ***!
  \**************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

"use strict";


Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _OrderViewPageMap = __webpack_require__(/*! ./OrderViewPageMap */ "./js/pages/order/OrderViewPageMap.js");

var _OrderViewPageMap2 = _interopRequireDefault(_OrderViewPageMap);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _window = window,
    $ = _window.$; /**
                    * Copyright since 2007 PrestaShop SA and Contributors
                    * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
                    *
                    * NOTICE OF LICENSE
                    *
                    * This source file is subject to the Open Software License (OSL 3.0)
                    * that is bundled with this package in the file LICENSE.md.
                    * It is also available through the world-wide-web at this URL:
                    * https://opensource.org/licenses/OSL-3.0
                    * If you did not receive a copy of the license and are unable to
                    * obtain it through the world-wide-web, please send an email
                    * to license@prestashop.com so we can send you a copy immediately.
                    *
                    * DISCLAIMER
                    *
                    * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
                    * versions in the future. If you wish to customize PrestaShop for your
                    * needs please refer to https://devdocs.prestashop.com/ for more information.
                    *
                    * @author    PrestaShop SA and Contributors <contact@prestashop.com>
                    * @copyright Since 2007 PrestaShop SA and Contributors
                    * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
                    */

var OrderShippingManager = function () {
  function OrderShippingManager() {
    (0, _classCallCheck3.default)(this, OrderShippingManager);

    this.initOrderShippingUpdateEventHandler();
  }

  (0, _createClass3.default)(OrderShippingManager, [{
    key: 'initOrderShippingUpdateEventHandler',
    value: function initOrderShippingUpdateEventHandler() {
      $(_OrderViewPageMap2.default.mainDiv).on('click', _OrderViewPageMap2.default.showOrderShippingUpdateModalBtn, function (event) {
        var $btn = $(event.currentTarget);

        $(_OrderViewPageMap2.default.updateOrderShippingTrackingNumberInput).val($btn.data('order-tracking-number'));
        $(_OrderViewPageMap2.default.updateOrderShippingCurrentOrderCarrierIdInput).val($btn.data('order-carrier-id'));
      });
    }
  }]);
  return OrderShippingManager;
}();

exports.default = OrderShippingManager;

/***/ }),

/***/ "./js/pages/order/view/order-discounts-refresher.js":
/*!**********************************************************!*\
  !*** ./js/pages/order/view/order-discounts-refresher.js ***!
  \**********************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

"use strict";


Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _router = __webpack_require__(/*! @components/router */ "./js/components/router.js");

var _router2 = _interopRequireDefault(_router);

var _OrderViewPageMap = __webpack_require__(/*! @pages/order/OrderViewPageMap */ "./js/pages/order/OrderViewPageMap.js");

var _OrderViewPageMap2 = _interopRequireDefault(_OrderViewPageMap);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 */

var _window = window,
    $ = _window.$;

var OrderDiscountsRefresher = function () {
  function OrderDiscountsRefresher() {
    (0, _classCallCheck3.default)(this, OrderDiscountsRefresher);

    this.router = new _router2.default();
  }

  (0, _createClass3.default)(OrderDiscountsRefresher, [{
    key: 'refresh',
    value: function refresh(orderId) {
      $.ajax(this.router.generate('admin_orders_get_discounts', { orderId: orderId })).then(function (response) {
        $(_OrderViewPageMap2.default.productDiscountList.list).replaceWith(response);
      });
    }
  }]);
  return OrderDiscountsRefresher;
}();

exports.default = OrderDiscountsRefresher;

/***/ }),

/***/ "./js/pages/order/view/order-documents-refresher.js":
/*!**********************************************************!*\
  !*** ./js/pages/order/view/order-documents-refresher.js ***!
  \**********************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

"use strict";


Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _router = __webpack_require__(/*! @components/router */ "./js/components/router.js");

var _router2 = _interopRequireDefault(_router);

var _OrderViewPageMap = __webpack_require__(/*! @pages/order/OrderViewPageMap */ "./js/pages/order/OrderViewPageMap.js");

var _OrderViewPageMap2 = _interopRequireDefault(_OrderViewPageMap);

var _invoiceNoteManager = __webpack_require__(/*! ../invoice-note-manager */ "./js/pages/order/invoice-note-manager.js");

var _invoiceNoteManager2 = _interopRequireDefault(_invoiceNoteManager);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _window = window,
    $ = _window.$; /**
                    * Copyright since 2007 PrestaShop SA and Contributors
                    * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
                    *
                    * NOTICE OF LICENSE
                    *
                    * This source file is subject to the Open Software License (OSL 3.0)
                    * that is bundled with this package in the file LICENSE.md.
                    * It is also available through the world-wide-web at this URL:
                    * https://opensource.org/licenses/OSL-3.0
                    * If you did not receive a copy of the license and are unable to
                    * obtain it through the world-wide-web, please send an email
                    * to license@prestashop.com so we can send you a copy immediately.
                    *
                    * DISCLAIMER
                    *
                    * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
                    * versions in the future. If you wish to customize PrestaShop for your
                    * needs please refer to https://devdocs.prestashop.com/ for more information.
                    *
                    * @author    PrestaShop SA and Contributors <contact@prestashop.com>
                    * @copyright Since 2007 PrestaShop SA and Contributors
                    * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
                    */

var OrderDocumentsRefresher = function () {
  function OrderDocumentsRefresher() {
    (0, _classCallCheck3.default)(this, OrderDocumentsRefresher);

    this.router = new _router2.default();
    this.invoiceNoteManager = new _invoiceNoteManager2.default();
  }

  (0, _createClass3.default)(OrderDocumentsRefresher, [{
    key: 'refresh',
    value: function refresh(orderId) {
      var _this = this;

      $.getJSON(this.router.generate('admin_orders_get_documents', { orderId: orderId })).then(function (response) {
        $(_OrderViewPageMap2.default.orderDocumentsTabCount).text(response.total);
        $(_OrderViewPageMap2.default.orderDocumentsTabBody).html(response.html);
        _this.invoiceNoteManager.setupListeners();
      });
    }
  }]);
  return OrderDocumentsRefresher;
}();

exports.default = OrderDocumentsRefresher;

/***/ }),

/***/ "./js/pages/order/view/order-invoices-refresher.js":
/*!*********************************************************!*\
  !*** ./js/pages/order/view/order-invoices-refresher.js ***!
  \*********************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

"use strict";


Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var _keys = __webpack_require__(/*! babel-runtime/core-js/object/keys */ "./node_modules/babel-runtime/core-js/object/keys.js");

var _keys2 = _interopRequireDefault(_keys);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _router = __webpack_require__(/*! @components/router */ "./js/components/router.js");

var _router2 = _interopRequireDefault(_router);

var _OrderViewPageMap = __webpack_require__(/*! @pages/order/OrderViewPageMap */ "./js/pages/order/OrderViewPageMap.js");

var _OrderViewPageMap2 = _interopRequireDefault(_OrderViewPageMap);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 */

var _window = window,
    $ = _window.$;

var OrderInvoicesRefresher = function () {
  function OrderInvoicesRefresher() {
    (0, _classCallCheck3.default)(this, OrderInvoicesRefresher);

    this.router = new _router2.default();
  }

  (0, _createClass3.default)(OrderInvoicesRefresher, [{
    key: 'refresh',
    value: function refresh(orderId) {
      $.getJSON(this.router.generate('admin_orders_get_invoices', { orderId: orderId })).then(function (response) {
        if (!response || !response.invoices || (0, _keys2.default)(response.invoices).length <= 0) {
          return;
        }

        var $paymentInvoiceSelect = $(_OrderViewPageMap2.default.orderPaymentInvoiceSelect);
        var $addProductInvoiceSelect = $(_OrderViewPageMap2.default.productAddInvoiceSelect);
        var $existingInvoicesGroup = $addProductInvoiceSelect.find('optgroup:first');
        var $productEditInvoiceSelect = $(_OrderViewPageMap2.default.productEditInvoiceSelect);
        var $addDiscountInvoiceSelect = $(_OrderViewPageMap2.default.addCartRuleInvoiceIdSelect);
        $existingInvoicesGroup.empty();
        $paymentInvoiceSelect.empty();
        $productEditInvoiceSelect.empty();
        $addDiscountInvoiceSelect.empty();

        (0, _keys2.default)(response.invoices).forEach(function (invoiceName) {
          var invoiceId = response.invoices[invoiceName];
          var invoiceNameWithoutPrice = invoiceName.split(' - ')[0];

          $existingInvoicesGroup.append('<option value="' + invoiceId + '">' + invoiceNameWithoutPrice + '</option>');
          $paymentInvoiceSelect.append('<option value="' + invoiceId + '">' + invoiceNameWithoutPrice + '</option>');
          $productEditInvoiceSelect.append('<option value="' + invoiceId + '">' + invoiceNameWithoutPrice + '</option>');
          $addDiscountInvoiceSelect.append('<option value="' + invoiceId + '">' + invoiceName + '</option>');
        });

        document.querySelector(_OrderViewPageMap2.default.productAddInvoiceSelect).selectedIndex = 0;
      });
    }
  }]);
  return OrderInvoicesRefresher;
}();

exports.default = OrderInvoicesRefresher;

/***/ }),

/***/ "./js/pages/order/view/order-payments-refresher.js":
/*!*********************************************************!*\
  !*** ./js/pages/order/view/order-payments-refresher.js ***!
  \*********************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

"use strict";


Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _router = __webpack_require__(/*! @components/router */ "./js/components/router.js");

var _router2 = _interopRequireDefault(_router);

var _OrderViewPageMap = __webpack_require__(/*! @pages/order/OrderViewPageMap */ "./js/pages/order/OrderViewPageMap.js");

var _OrderViewPageMap2 = _interopRequireDefault(_OrderViewPageMap);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 */

var _window = window,
    $ = _window.$;

var OrderPaymentsRefresher = function () {
  function OrderPaymentsRefresher() {
    (0, _classCallCheck3.default)(this, OrderPaymentsRefresher);

    this.router = new _router2.default();
  }

  (0, _createClass3.default)(OrderPaymentsRefresher, [{
    key: 'refresh',
    value: function refresh(orderId) {
      $.ajax(this.router.generate('admin_orders_get_payments', { orderId: orderId })).then(function (response) {
        $(_OrderViewPageMap2.default.viewOrderPaymentsAlert).remove();
        $(_OrderViewPageMap2.default.viewOrderPaymentsBlock + ' .card-body').prepend(response);
      }, function (response) {
        if (response.responseJSON && response.responseJSON.message) {
          $.growl.error({ message: response.responseJSON.message });
        }
      });
    }
  }]);
  return OrderPaymentsRefresher;
}();

exports.default = OrderPaymentsRefresher;

/***/ }),

/***/ "./js/pages/order/view/order-prices-refresher.js":
/*!*******************************************************!*\
  !*** ./js/pages/order/view/order-prices-refresher.js ***!
  \*******************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

"use strict";


Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _router = __webpack_require__(/*! @components/router */ "./js/components/router.js");

var _router2 = _interopRequireDefault(_router);

var _OrderViewPageMap = __webpack_require__(/*! @pages/order/OrderViewPageMap */ "./js/pages/order/OrderViewPageMap.js");

var _OrderViewPageMap2 = _interopRequireDefault(_OrderViewPageMap);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 */

var _window = window,
    $ = _window.$;

var OrderPricesRefresher = function () {
  function OrderPricesRefresher() {
    (0, _classCallCheck3.default)(this, OrderPricesRefresher);

    this.router = new _router2.default();
  }

  (0, _createClass3.default)(OrderPricesRefresher, [{
    key: 'refresh',
    value: function refresh(orderId) {
      $.getJSON(this.router.generate('admin_orders_get_prices', { orderId: orderId })).then(function (response) {
        $(_OrderViewPageMap2.default.orderTotal).text(response.orderTotalFormatted);
        $(_OrderViewPageMap2.default.orderDiscountsTotal).text('-' + response.discountsAmountFormatted);
        $(_OrderViewPageMap2.default.orderDiscountsTotalContainer).toggleClass('d-none', !response.discountsAmountDisplayed);
        $(_OrderViewPageMap2.default.orderProductsTotal).text(response.productsTotalFormatted);
        $(_OrderViewPageMap2.default.orderShippingTotal).text(response.shippingTotalFormatted);
        $(_OrderViewPageMap2.default.orderShippingTotalContainer).toggleClass('d-none', !response.shippingTotalDisplayed);
        $(_OrderViewPageMap2.default.orderTaxesTotal).text(response.taxesTotalFormatted);
      });
    }
  }, {
    key: 'refreshProductPrices',
    value: function refreshProductPrices(orderId) {
      $.getJSON(this.router.generate('admin_orders_product_prices', { orderId: orderId })).then(function (productPricesList) {
        productPricesList.forEach(function (productPrices) {
          var orderProductTrId = _OrderViewPageMap2.default.productsTableRow(productPrices.orderDetailId);
          var $quantity = $(productPrices.quantity);

          if (productPrices.quantity > 1) {
            $quantity = $quantity.wrap('<span class="badge badge-secondary rounded-circle"></span>');
          }

          $(orderProductTrId + ' ' + _OrderViewPageMap2.default.productEditUnitPrice).text(productPrices.unitPrice);
          $(orderProductTrId + ' ' + _OrderViewPageMap2.default.productEditQuantity).html($quantity.html());
          $(orderProductTrId + ' ' + _OrderViewPageMap2.default.productEditAvailableQuantity).text(productPrices.availableQuantity);
          $(orderProductTrId + ' ' + _OrderViewPageMap2.default.productEditTotalPrice).text(productPrices.totalPrice);

          // update order row price values
          var productEditButton = $(_OrderViewPageMap2.default.productEditBtn(productPrices.orderDetailId));

          productEditButton.data('product-price-tax-incl', productPrices.unitPriceTaxInclRaw);
          productEditButton.data('product-price-tax-excl', productPrices.unitPriceTaxExclRaw);
          productEditButton.data('product-quantity', productPrices.quantity);
        });
      });
    }

    /**
     * This method will check if the same product is already present in the order
     * and if so and if the price of the 2 products doesn't match will return either
     * 'invoice' if the 2 products are in 2 different invoices or 'product' if the 2 products
     * are in the same invoice (or no invoice yet). Only products that have different customizations
     * can be twice in a same invoice.
     * Will return null if no matching products are found.
     */

  }, {
    key: 'checkOtherProductPricesMatch',
    value: function checkOtherProductPricesMatch(givenPrice, productId, combinationId, invoiceId, orderDetailId) {
      var productRows = document.querySelectorAll('tr.cellProduct');
      // We convert the expected values into int/float to avoid a type mismatch that would be wrongly interpreted
      var expectedProductId = Number(productId);
      var expectedCombinationId = Number(combinationId);
      var expectedGivenPrice = Number(givenPrice);
      var unmatchingInvoicePriceExists = false;
      var unmatchingProductPriceExists = false;

      productRows.forEach(function (productRow) {
        var productRowId = $(productRow).attr('id');

        // No need to check edited row (especially if it's the only one for this product)
        if (orderDetailId && productRowId === 'orderProduct_' + orderDetailId) {
          return;
        }

        var productEditBtn = $('#' + productRowId + ' ' + _OrderViewPageMap2.default.productEditButtons);
        var currentOrderInvoiceId = Number(productEditBtn.data('order-invoice-id'));

        var currentProductId = Number(productEditBtn.data('product-id'));
        var currentCombinationId = Number(productEditBtn.data('combination-id'));

        if (currentProductId !== expectedProductId || currentCombinationId !== expectedCombinationId) {
          return;
        }

        if (expectedGivenPrice !== Number(productEditBtn.data('product-price-tax-incl'))) {
          if (invoiceId === '' || invoiceId && currentOrderInvoiceId && invoiceId === currentOrderInvoiceId) {
            unmatchingProductPriceExists = true;
          } else {
            unmatchingInvoicePriceExists = true;
          }
        }
      });

      if (unmatchingInvoicePriceExists) {
        return 'invoice';
      }
      if (unmatchingProductPriceExists) {
        return 'product';
      }

      return null;
    }
  }]);
  return OrderPricesRefresher;
}();

exports.default = OrderPricesRefresher;

/***/ }),

/***/ "./js/pages/order/view/order-prices.js":
/*!*********************************************!*\
  !*** ./js/pages/order/view/order-prices.js ***!
  \*********************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

"use strict";


Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var _isNan = __webpack_require__(/*! babel-runtime/core-js/number/is-nan */ "./node_modules/babel-runtime/core-js/number/is-nan.js");

var _isNan2 = _interopRequireDefault(_isNan);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 */

var OrderPrices = function () {
  function OrderPrices() {
    (0, _classCallCheck3.default)(this, OrderPrices);
  }

  (0, _createClass3.default)(OrderPrices, [{
    key: "calculateTaxExcluded",
    value: function calculateTaxExcluded(taxIncluded, taxRatePerCent, currencyPrecision) {
      var priceTaxIncl = parseFloat(taxIncluded);

      if (priceTaxIncl < 0 || (0, _isNan2.default)(priceTaxIncl)) {
        priceTaxIncl = 0;
      }
      var taxRate = taxRatePerCent / 100 + 1;

      return window.ps_round(priceTaxIncl / taxRate, currencyPrecision);
    }
  }, {
    key: "calculateTaxIncluded",
    value: function calculateTaxIncluded(taxExcluded, taxRatePerCent, currencyPrecision) {
      var priceTaxExcl = parseFloat(taxExcluded);

      if (priceTaxExcl < 0 || (0, _isNan2.default)(priceTaxExcl)) {
        priceTaxExcl = 0;
      }
      var taxRate = taxRatePerCent / 100 + 1;

      return window.ps_round(priceTaxExcl * taxRate, currencyPrecision);
    }
  }, {
    key: "calculateTotalPrice",
    value: function calculateTotalPrice(quantity, unitPrice, currencyPrecision) {
      return window.ps_round(unitPrice * quantity, currencyPrecision);
    }
  }]);
  return OrderPrices;
}();

exports.default = OrderPrices;

/***/ }),

/***/ "./js/pages/order/view/order-product-add-autocomplete.js":
/*!***************************************************************!*\
  !*** ./js/pages/order/view/order-product-add-autocomplete.js ***!
  \***************************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

"use strict";


Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var _values = __webpack_require__(/*! babel-runtime/core-js/object/values */ "./node_modules/babel-runtime/core-js/object/values.js");

var _values2 = _interopRequireDefault(_values);

var _keys = __webpack_require__(/*! babel-runtime/core-js/object/keys */ "./node_modules/babel-runtime/core-js/object/keys.js");

var _keys2 = _interopRequireDefault(_keys);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _router = __webpack_require__(/*! @components/router */ "./js/components/router.js");

var _router2 = _interopRequireDefault(_router);

var _OrderViewPageMap = __webpack_require__(/*! @pages/order/OrderViewPageMap */ "./js/pages/order/OrderViewPageMap.js");

var _OrderViewPageMap2 = _interopRequireDefault(_OrderViewPageMap);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 */
var _window = window,
    $ = _window.$;

var OrderProductAutocomplete = function () {
  function OrderProductAutocomplete(input) {
    (0, _classCallCheck3.default)(this, OrderProductAutocomplete);

    this.activeSearchRequest = null;
    this.router = new _router2.default();
    this.input = input;
    this.results = [];
    this.dropdownMenu = $(_OrderViewPageMap2.default.productSearchInputAutocompleteMenu);
    /**
     * Permit to link to each value of dropdown a callback after item is clicked
     */
    this.onItemClickedCallback = function () {};
  }

  (0, _createClass3.default)(OrderProductAutocomplete, [{
    key: 'listenForSearch',
    value: function listenForSearch() {
      var _this = this;

      this.input.on('click', function (event) {
        event.stopImmediatePropagation();
        _this.updateResults(_this.results);
      });

      this.input.on('keyup', function (event) {
        return _this.delaySearch(event.currentTarget);
      });
      $(document).on('click', function () {
        return _this.dropdownMenu.hide();
      });
    }
  }, {
    key: 'delaySearch',
    value: function delaySearch(input) {
      var _this2 = this;

      clearTimeout(this.searchTimeoutId);

      // Search only if the search phrase length is greater than 2 characters
      if (input.value.length < 2) {
        return;
      }

      this.searchTimeoutId = setTimeout(function () {
        _this2.search(input.value, $(input).data('currency'), $(input).data('order'));
      }, 300);
    }
  }, {
    key: 'search',
    value: function search(_search, currency, orderId) {
      var _this3 = this;

      var params = { search_phrase: _search };

      if (currency) {
        params.currency_id = currency;
      }

      if (orderId) {
        params.order_id = orderId;
      }

      if (this.activeSearchRequest !== null) {
        this.activeSearchRequest.abort();
      }

      this.activeSearchRequest = $.get(this.router.generate('admin_orders_products_search', params));
      this.activeSearchRequest.then(function (response) {
        return _this3.updateResults(response);
      }).always(function () {
        _this3.activeSearchRequest = null;
      });
    }
  }, {
    key: 'updateResults',
    value: function updateResults(results) {
      var _this4 = this;

      this.dropdownMenu.empty();

      if (!results || !results.products || (0, _keys2.default)(results.products).length <= 0) {
        this.dropdownMenu.hide();
        return;
      }

      this.results = results.products;

      (0, _values2.default)(this.results).forEach(function (val) {
        var link = $('<a class="dropdown-item" data-id="' + val.productId + '" href="#">' + val.name + '</a>');

        link.on('click', function (event) {
          event.preventDefault();
          _this4.onItemClicked($(event.target).data('id'));
        });

        _this4.dropdownMenu.append(link);
      });

      this.dropdownMenu.show();
    }
  }, {
    key: 'onItemClicked',
    value: function onItemClicked(id) {
      var selectedProduct = this.results.filter(function (product) {
        return product.productId === id;
      });

      if (selectedProduct.length !== 0) {
        this.input.val(selectedProduct[0].name);
        this.onItemClickedCallback(selectedProduct[0]);
      }
    }
  }]);
  return OrderProductAutocomplete;
}();

exports.default = OrderProductAutocomplete;

/***/ }),

/***/ "./js/pages/order/view/order-product-add.js":
/*!**************************************************!*\
  !*** ./js/pages/order/view/order-product-add.js ***!
  \**************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

"use strict";


Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var _keys = __webpack_require__(/*! babel-runtime/core-js/object/keys */ "./node_modules/babel-runtime/core-js/object/keys.js");

var _keys2 = _interopRequireDefault(_keys);

var _values = __webpack_require__(/*! babel-runtime/core-js/object/values */ "./node_modules/babel-runtime/core-js/object/values.js");

var _values2 = _interopRequireDefault(_values);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _router = __webpack_require__(/*! @components/router */ "./js/components/router.js");

var _router2 = _interopRequireDefault(_router);

var _OrderViewPageMap = __webpack_require__(/*! @pages/order/OrderViewPageMap */ "./js/pages/order/OrderViewPageMap.js");

var _OrderViewPageMap2 = _interopRequireDefault(_OrderViewPageMap);

var _eventEmitter = __webpack_require__(/*! @components/event-emitter */ "./js/components/event-emitter.js");

var _orderViewEventMap = __webpack_require__(/*! @pages/order/view/order-view-event-map */ "./js/pages/order/view/order-view-event-map.js");

var _orderViewEventMap2 = _interopRequireDefault(_orderViewEventMap);

var _orderPrices = __webpack_require__(/*! @pages/order/view/order-prices */ "./js/pages/order/view/order-prices.js");

var _orderPrices2 = _interopRequireDefault(_orderPrices);

var _orderProductRenderer = __webpack_require__(/*! @pages/order/view/order-product-renderer */ "./js/pages/order/view/order-product-renderer.js");

var _orderProductRenderer2 = _interopRequireDefault(_orderProductRenderer);

var _modal = __webpack_require__(/*! @components/modal */ "./js/components/modal.js");

var _modal2 = _interopRequireDefault(_modal);

var _orderPricesRefresher = __webpack_require__(/*! @pages/order/view/order-prices-refresher */ "./js/pages/order/view/order-prices-refresher.js");

var _orderPricesRefresher2 = _interopRequireDefault(_orderPricesRefresher);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 */

var _window = window,
    $ = _window.$;

var OrderProductAdd = function () {
  function OrderProductAdd() {
    (0, _classCallCheck3.default)(this, OrderProductAdd);

    this.router = new _router2.default();
    this.productAddActionBtn = $(_OrderViewPageMap2.default.productAddActionBtn);
    this.productIdInput = $(_OrderViewPageMap2.default.productAddIdInput);
    this.combinationsBlock = $(_OrderViewPageMap2.default.productAddCombinationsBlock);
    this.combinationsSelect = $(_OrderViewPageMap2.default.productAddCombinationsSelect);
    this.priceTaxIncludedInput = $(_OrderViewPageMap2.default.productAddPriceTaxInclInput);
    this.priceTaxExcludedInput = $(_OrderViewPageMap2.default.productAddPriceTaxExclInput);
    this.taxRateInput = $(_OrderViewPageMap2.default.productAddTaxRateInput);
    this.quantityInput = $(_OrderViewPageMap2.default.productAddQuantityInput);
    this.availableText = $(_OrderViewPageMap2.default.productAddAvailableText);
    this.locationText = $(_OrderViewPageMap2.default.productAddLocationText);
    this.totalPriceText = $(_OrderViewPageMap2.default.productAddTotalPriceText);
    this.invoiceSelect = $(_OrderViewPageMap2.default.productAddInvoiceSelect);
    this.freeShippingSelect = $(_OrderViewPageMap2.default.productAddFreeShippingSelect);
    this.productAddMenuBtn = $(_OrderViewPageMap2.default.productAddBtn);
    this.available = null;
    this.setupListener();
    this.product = {};
    this.currencyPrecision = $(_OrderViewPageMap2.default.productsTable).data('currencyPrecision');
    this.priceTaxCalculator = new _orderPrices2.default();
    this.orderProductRenderer = new _orderProductRenderer2.default();
    this.orderPricesRefresher = new _orderPricesRefresher2.default();
    this.isOrderTaxIncluded = $(_OrderViewPageMap2.default.productAddRow).data('isOrderTaxIncluded');
    this.taxExcluded = null;
    this.taxIncluded = null;
  }

  (0, _createClass3.default)(OrderProductAdd, [{
    key: 'setupListener',
    value: function setupListener() {
      var _this = this;

      this.combinationsSelect.on('change', function (event) {
        var taxExcluded = window.ps_round($(event.currentTarget).find(':selected').data('priceTaxExcluded'), _this.currencyPrecision);
        _this.priceTaxExcludedInput.val(taxExcluded);
        _this.taxExcluded = parseFloat(taxExcluded);

        var taxIncluded = window.ps_round($(event.currentTarget).find(':selected').data('priceTaxIncluded'), _this.currencyPrecision);
        _this.priceTaxIncludedInput.val(taxIncluded);
        _this.taxIncluded = parseFloat(taxIncluded);

        _this.locationText.html($(event.currentTarget).find(':selected').data('location'));

        _this.available = $(event.currentTarget).find(':selected').data('stock');

        _this.quantityInput.trigger('change');
        _this.orderProductRenderer.toggleColumn(_OrderViewPageMap2.default.productsCellLocation);
      });

      this.quantityInput.on('change keyup', function (event) {
        if (_this.available !== null) {
          var newQuantity = Number(event.target.value);
          var remainingAvailable = _this.available - newQuantity;
          var availableOutOfStock = _this.availableText.data('availableOutOfStock');
          _this.availableText.text(remainingAvailable);
          _this.availableText.toggleClass('text-danger font-weight-bold', remainingAvailable < 0);
          var disableAddActionBtn = newQuantity <= 0 || remainingAvailable < 0 && !availableOutOfStock;
          _this.productAddActionBtn.prop('disabled', disableAddActionBtn);
          _this.invoiceSelect.prop('disabled', !availableOutOfStock && remainingAvailable < 0);

          _this.taxIncluded = parseFloat(_this.priceTaxIncludedInput.val());
          _this.totalPriceText.html(_this.priceTaxCalculator.calculateTotalPrice(newQuantity, _this.isOrderTaxIncluded ? _this.taxIncluded : _this.taxExcluded, _this.currencyPrecision));
        }
      });

      this.productIdInput.on('change', function () {
        _this.productAddActionBtn.removeAttr('disabled');
        _this.invoiceSelect.removeAttr('disabled');
      });

      this.priceTaxIncludedInput.on('change keyup', function (event) {
        _this.taxIncluded = parseFloat(event.target.value);
        _this.taxExcluded = _this.priceTaxCalculator.calculateTaxExcluded(_this.taxIncluded, _this.taxRateInput.val(), _this.currencyPrecision);
        var quantity = parseInt(_this.quantityInput.val(), 10);

        _this.priceTaxExcludedInput.val(_this.taxExcluded);
        _this.totalPriceText.html(_this.priceTaxCalculator.calculateTotalPrice(quantity, _this.isOrderTaxIncluded ? _this.taxIncluded : _this.taxExcluded, _this.currencyPrecision));
      });

      this.priceTaxExcludedInput.on('change keyup', function (event) {
        _this.taxExcluded = parseFloat(event.target.value);
        _this.taxIncluded = _this.priceTaxCalculator.calculateTaxIncluded(_this.taxExcluded, _this.taxRateInput.val(), _this.currencyPrecision);
        var quantity = parseInt(_this.quantityInput.val(), 10);

        _this.priceTaxIncludedInput.val(_this.taxIncluded);
        _this.totalPriceText.html(_this.priceTaxCalculator.calculateTotalPrice(quantity, _this.isOrderTaxIncluded ? _this.taxIncluded : _this.taxExcluded, _this.currencyPrecision));
      });

      this.productAddActionBtn.on('click', function (event) {
        return _this.confirmNewInvoice(event);
      });
      this.invoiceSelect.on('change', function () {
        return _this.orderProductRenderer.toggleProductAddNewInvoiceInfo();
      });
    }
  }, {
    key: 'setProduct',
    value: function setProduct(product) {
      this.productIdInput.val(product.productId).trigger('change');

      var taxExcluded = window.ps_round(product.priceTaxExcl, this.currencyPrecision);
      this.priceTaxExcludedInput.val(taxExcluded);
      this.taxExcluded = parseFloat(taxExcluded);

      var taxIncluded = window.ps_round(product.priceTaxIncl, this.currencyPrecision);
      this.priceTaxIncludedInput.val(taxIncluded);
      this.taxIncluded = parseFloat(taxIncluded);

      this.taxRateInput.val(product.taxRate);
      this.locationText.html(product.location);
      this.available = product.stock;
      this.availableText.data('availableOutOfStock', product.availableOutOfStock);
      this.quantityInput.val(1);
      this.quantityInput.trigger('change');
      this.setCombinations(product.combinations);
      this.orderProductRenderer.toggleColumn(_OrderViewPageMap2.default.productsCellLocation);
    }
  }, {
    key: 'setCombinations',
    value: function setCombinations(combinations) {
      var _this2 = this;

      this.combinationsSelect.empty();

      (0, _values2.default)(combinations).forEach(function (val) {
        _this2.combinationsSelect.append(
        /* eslint-disable-next-line max-len */
        '<option value="' + val.attributeCombinationId + '" data-price-tax-excluded="' + val.priceTaxExcluded + '" data-price-tax-included="' + val.priceTaxIncluded + '" data-stock="' + val.stock + '" data-location="' + val.location + '">' + val.attribute + '</option>');
      });

      this.combinationsBlock.toggleClass('d-none', (0, _keys2.default)(combinations).length === 0);

      if ((0, _keys2.default)(combinations).length > 0) {
        this.combinationsSelect.trigger('change');
      }
    }
  }, {
    key: 'addProduct',
    value: function addProduct(orderId) {
      var _this3 = this;

      this.productAddActionBtn.prop('disabled', true);
      this.invoiceSelect.prop('disabled', true);
      this.combinationsSelect.prop('disabled', true);

      var params = {
        product_id: this.productIdInput.val(),
        combination_id: $(':selected', this.combinationsSelect).val(),
        price_tax_incl: this.priceTaxIncludedInput.val(),
        price_tax_excl: this.priceTaxExcludedInput.val(),
        quantity: this.quantityInput.val(),
        invoice_id: this.invoiceSelect.val(),
        free_shipping: this.freeShippingSelect.prop('checked')
      };

      $.ajax({
        url: this.router.generate('admin_orders_add_product', { orderId: orderId }),
        method: 'POST',
        data: params
      }).then(function (response) {
        _eventEmitter.EventEmitter.emit(_orderViewEventMap2.default.productAddedToOrder, {
          orderId: orderId,
          orderProductId: params.product_id,
          newRow: response
        });
      }, function (response) {
        _this3.productAddActionBtn.prop('disabled', false);
        _this3.invoiceSelect.prop('disabled', false);
        _this3.combinationsSelect.prop('disabled', false);

        if (response.responseJSON && response.responseJSON.message) {
          $.growl.error({ message: response.responseJSON.message });
        }
      });
    }
  }, {
    key: 'confirmNewInvoice',
    value: function confirmNewInvoice(event) {
      var _this4 = this;

      var invoiceId = parseInt(this.invoiceSelect.val(), 10);
      var orderId = $(event.currentTarget).data('orderId');

      // Explicit 0 value is used when we the user selected New Invoice
      if (invoiceId === 0) {
        var modal = new _modal2.default({
          id: 'modal-confirm-new-invoice',
          confirmTitle: this.invoiceSelect.data('modal-title'),
          confirmMessage: this.invoiceSelect.data('modal-body'),
          confirmButtonLabel: this.invoiceSelect.data('modal-apply'),
          closeButtonLabel: this.invoiceSelect.data('modal-cancel')
        }, function () {
          _this4.confirmNewPrice(orderId, invoiceId);
        });
        modal.show();
      } else {
        // Last case is Nan, the selector is not even present, we simply add product and let the BO handle it
        this.addProduct(orderId);
      }
    }
  }, {
    key: 'confirmNewPrice',
    value: function confirmNewPrice(orderId, invoiceId) {
      var _this5 = this;

      var combinationValue = $(':selected', this.combinationsSelect).val();
      var combinationId = typeof combinationValue === 'undefined' ? 0 : combinationValue;
      var productPriceMatch = this.orderPricesRefresher.checkOtherProductPricesMatch(this.priceTaxIncludedInput.val(), this.productIdInput.val(), combinationId, invoiceId);

      if (productPriceMatch === 'invoice') {
        var modalEditPrice = new _modal2.default({
          id: 'modal-confirm-new-price',
          confirmTitle: this.invoiceSelect.data('modal-edit-price-title'),
          confirmMessage: this.invoiceSelect.data('modal-edit-price-body'),
          confirmButtonLabel: this.invoiceSelect.data('modal-edit-price-apply'),
          closeButtonLabel: this.invoiceSelect.data('modal-edit-price-cancel')
        }, function () {
          _this5.addProduct(orderId);
        });
        modalEditPrice.show();
      } else {
        this.addProduct(orderId);
      }
    }
  }]);
  return OrderProductAdd;
}();

exports.default = OrderProductAdd;

/***/ }),

/***/ "./js/pages/order/view/order-product-cancel.js":
/*!*****************************************************!*\
  !*** ./js/pages/order/view/order-product-cancel.js ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

"use strict";


Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var _isNan = __webpack_require__(/*! babel-runtime/core-js/number/is-nan */ "./node_modules/babel-runtime/core-js/number/is-nan.js");

var _isNan2 = _interopRequireDefault(_isNan);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _router = __webpack_require__(/*! @components/router */ "./js/components/router.js");

var _router2 = _interopRequireDefault(_router);

var _OrderViewPageMap = __webpack_require__(/*! @pages/order/OrderViewPageMap */ "./js/pages/order/OrderViewPageMap.js");

var _OrderViewPageMap2 = _interopRequireDefault(_OrderViewPageMap);

var _cldr = __webpack_require__(/*! @app/cldr */ "./js/app/cldr/index.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _window = window,
    $ = _window.$;

/**
 * manages all product cancel actions, that includes all refund operations
 */
/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 */

var OrderProductCancel = function () {
  function OrderProductCancel() {
    (0, _classCallCheck3.default)(this, OrderProductCancel);

    this.router = new _router2.default();
    this.cancelProductForm = $(_OrderViewPageMap2.default.cancelProduct.form);
    this.orderId = this.cancelProductForm.data('orderId');
    this.orderDelivered = parseInt(this.cancelProductForm.data('isDelivered'), 10) === 1;
    this.isTaxIncluded = parseInt(this.cancelProductForm.data('isTaxIncluded'), 10) === 1;
    this.discountsAmount = parseFloat(this.cancelProductForm.data('discountsAmount'));
    this.currencyFormatter = _cldr.NumberFormatter.build(this.cancelProductForm.data('priceSpecification'));
    this.useAmountInputs = true;
    this.listenForInputs();
  }

  (0, _createClass3.default)(OrderProductCancel, [{
    key: 'showPartialRefund',
    value: function showPartialRefund() {
      // Always start by hiding elements then show the others, since some elements are common
      this.hideCancelElements();
      $(_OrderViewPageMap2.default.cancelProduct.toggle.partialRefund).show();
      this.useAmountInputs = true;
      this.initForm($(_OrderViewPageMap2.default.cancelProduct.buttons.save).data('partialRefundLabel'), this.router.generate('admin_orders_partial_refund', {
        orderId: this.orderId
      }), 'partial-refund');
    }
  }, {
    key: 'showStandardRefund',
    value: function showStandardRefund() {
      // Always start by hiding elements then show the others, since some elements are common
      this.hideCancelElements();
      $(_OrderViewPageMap2.default.cancelProduct.toggle.standardRefund).show();
      this.useAmountInputs = false;
      this.initForm($(_OrderViewPageMap2.default.cancelProduct.buttons.save).data('standardRefundLabel'), this.router.generate('admin_orders_standard_refund', {
        orderId: this.orderId
      }), 'standard-refund');
    }
  }, {
    key: 'showReturnProduct',
    value: function showReturnProduct() {
      // Always start by hiding elements then show the others, since some elements are common
      this.hideCancelElements();
      $(_OrderViewPageMap2.default.cancelProduct.toggle.returnProduct).show();
      this.useAmountInputs = false;
      this.initForm($(_OrderViewPageMap2.default.cancelProduct.buttons.save).data('returnProductLabel'), this.router.generate('admin_orders_return_product', {
        orderId: this.orderId
      }), 'return-product');
    }
  }, {
    key: 'hideRefund',
    value: function hideRefund() {
      this.hideCancelElements();
      $(_OrderViewPageMap2.default.cancelProduct.table.actions).show();
    }
  }, {
    key: 'hideCancelElements',
    value: function hideCancelElements() {
      $(_OrderViewPageMap2.default.cancelProduct.toggle.standardRefund).hide();
      $(_OrderViewPageMap2.default.cancelProduct.toggle.partialRefund).hide();
      $(_OrderViewPageMap2.default.cancelProduct.toggle.returnProduct).hide();
      $(_OrderViewPageMap2.default.cancelProduct.table.actions).hide();
    }
  }, {
    key: 'initForm',
    value: function initForm(actionName, formAction, formClass) {
      this.updateVoucherRefund();

      this.cancelProductForm.prop('action', formAction);
      this.cancelProductForm.removeClass('standard-refund partial-refund return-product cancel-product').addClass(formClass);
      $(_OrderViewPageMap2.default.cancelProduct.buttons.save).html(actionName);
      $(_OrderViewPageMap2.default.cancelProduct.table.header).html(actionName);
      $(_OrderViewPageMap2.default.cancelProduct.checkboxes.restock).prop('checked', this.orderDelivered);
      $(_OrderViewPageMap2.default.cancelProduct.checkboxes.creditSlip).prop('checked', true);
      $(_OrderViewPageMap2.default.cancelProduct.checkboxes.voucher).prop('checked', false);
    }
  }, {
    key: 'listenForInputs',
    value: function listenForInputs() {
      var _this = this;

      $(document).on('change', _OrderViewPageMap2.default.cancelProduct.inputs.quantity, function (event) {
        var $productQuantityInput = $(event.target);
        var $parentCell = $productQuantityInput.parents(_OrderViewPageMap2.default.cancelProduct.table.cell);
        var $productAmount = $parentCell.find(_OrderViewPageMap2.default.cancelProduct.inputs.amount);
        var productQuantity = parseInt($productQuantityInput.val(), 10);

        if (productQuantity <= 0) {
          $productAmount.val(0);
          _this.updateVoucherRefund();

          return;
        }
        var priceFieldName = _this.isTaxIncluded ? 'productPriceTaxIncl' : 'productPriceTaxExcl';
        var productUnitPrice = parseFloat($productQuantityInput.data(priceFieldName));
        var amountRefundable = parseFloat($productQuantityInput.data('amountRefundable'));
        var guessedAmount = productUnitPrice * productQuantity < amountRefundable ? productUnitPrice * productQuantity : amountRefundable;
        var amountValue = parseFloat($productAmount.val());

        if (_this.useAmountInputs) {
          _this.updateAmountInput($productQuantityInput);
        }

        if ($productAmount.val() === '' || amountValue === 0 || amountValue > guessedAmount) {
          $productAmount.val(guessedAmount);
          _this.updateVoucherRefund();
        }
      });

      $(document).on('change', _OrderViewPageMap2.default.cancelProduct.inputs.amount, function () {
        _this.updateVoucherRefund();
      });

      $(document).on('change', _OrderViewPageMap2.default.cancelProduct.inputs.selector, function (event) {
        var $productCheckbox = $(event.target);
        var $parentCell = $productCheckbox.parents(_OrderViewPageMap2.default.cancelProduct.table.cell);
        var productQuantityInput = $parentCell.find(_OrderViewPageMap2.default.cancelProduct.inputs.quantity);
        var refundableQuantity = parseInt(productQuantityInput.data('quantityRefundable'), 10);
        var productQuantity = parseInt(productQuantityInput.val(), 10);

        if (!$productCheckbox.is(':checked')) {
          productQuantityInput.val(0);
        } else if ((0, _isNan2.default)(productQuantity) || productQuantity === 0) {
          productQuantityInput.val(refundableQuantity);
        }
        _this.updateVoucherRefund();
      });
    }
  }, {
    key: 'updateAmountInput',
    value: function updateAmountInput($productQuantityInput) {
      var $parentCell = $productQuantityInput.parents(_OrderViewPageMap2.default.cancelProduct.table.cell);
      var $productAmount = $parentCell.find(_OrderViewPageMap2.default.cancelProduct.inputs.amount);
      var productQuantity = parseInt($productQuantityInput.val(), 10);

      if (productQuantity <= 0) {
        $productAmount.val(0);

        return;
      }

      var priceFieldName = this.isTaxIncluded ? 'productPriceTaxIncl' : 'productPriceTaxExcl';
      var productUnitPrice = parseFloat($productQuantityInput.data(priceFieldName));
      var amountRefundable = parseFloat($productQuantityInput.data('amountRefundable'));
      var guessedAmount = productUnitPrice * productQuantity < amountRefundable ? productUnitPrice * productQuantity : amountRefundable;
      var amountValue = parseFloat($productAmount.val());

      if ($productAmount.val() === '' || amountValue === 0 || amountValue > guessedAmount) {
        $productAmount.val(guessedAmount);
      }
    }
  }, {
    key: 'getRefundAmount',
    value: function getRefundAmount() {
      var _this2 = this;

      var totalAmount = 0;

      if (this.useAmountInputs) {
        $(_OrderViewPageMap2.default.cancelProduct.inputs.amount).each(function (index, amount) {
          var floatValue = parseFloat(amount.value);
          totalAmount += !(0, _isNan2.default)(floatValue) ? floatValue : 0;
        });
      } else {
        $(_OrderViewPageMap2.default.cancelProduct.inputs.quantity).each(function (index, quantity) {
          var $quantityInput = $(quantity);
          var priceFieldName = _this2.isTaxIncluded ? 'productPriceTaxIncl' : 'productPriceTaxExcl';
          var productUnitPrice = parseFloat($quantityInput.data(priceFieldName));
          var productQuantity = parseInt($quantityInput.val(), 10);
          totalAmount += productQuantity * productUnitPrice;
        });
      }

      return totalAmount;
    }
  }, {
    key: 'updateVoucherRefund',
    value: function updateVoucherRefund() {
      var refundAmount = this.getRefundAmount();

      this.updateVoucherRefundTypeLabel($(_OrderViewPageMap2.default.cancelProduct.radios.voucherRefundType.productPrices), refundAmount);
      var refundVoucherExcluded = refundAmount - this.discountsAmount;
      this.updateVoucherRefundTypeLabel($(_OrderViewPageMap2.default.cancelProduct.radios.voucherRefundType.productPricesVoucherExcluded), refundVoucherExcluded);

      // Disable voucher excluded option when the voucher amount is too high
      if (refundVoucherExcluded < 0) {
        $(_OrderViewPageMap2.default.cancelProduct.radios.voucherRefundType.productPricesVoucherExcluded).prop('checked', false).prop('disabled', true);
        $(_OrderViewPageMap2.default.cancelProduct.radios.voucherRefundType.productPrices).prop('checked', true);
        $(_OrderViewPageMap2.default.cancelProduct.radios.voucherRefundType.negativeErrorMessage).show();
      } else {
        $(_OrderViewPageMap2.default.cancelProduct.radios.voucherRefundType.productPricesVoucherExcluded).prop('disabled', false);
        $(_OrderViewPageMap2.default.cancelProduct.radios.voucherRefundType.negativeErrorMessage).hide();
      }
    }
  }, {
    key: 'updateVoucherRefundTypeLabel',
    value: function updateVoucherRefundTypeLabel($input, refundAmount) {
      var defaultLabel = $input.data('defaultLabel');
      var $label = $input.parents('label');
      var formattedAmount = this.currencyFormatter.format(refundAmount);

      // Change the ending text part only to avoid removing the input (the EOL is on purpose for better display)
      $label.get(0).lastChild.nodeValue = '\n    ' + defaultLabel + ' ' + formattedAmount;
    }
  }, {
    key: 'showCancelProductForm',
    value: function showCancelProductForm() {
      var cancelProductRoute = this.router.generate('admin_orders_cancellation', { orderId: this.orderId });
      this.initForm($(_OrderViewPageMap2.default.cancelProduct.buttons.save).data('cancelLabel'), cancelProductRoute, 'cancel-product');
      this.hideCancelElements();
      $(_OrderViewPageMap2.default.cancelProduct.toggle.cancelProducts).show();
    }
  }]);
  return OrderProductCancel;
}();

exports.default = OrderProductCancel;

/***/ }),

/***/ "./js/pages/order/view/order-product-edit.js":
/*!***************************************************!*\
  !*** ./js/pages/order/view/order-product-edit.js ***!
  \***************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

"use strict";


Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _router = __webpack_require__(/*! @components/router */ "./js/components/router.js");

var _router2 = _interopRequireDefault(_router);

var _OrderViewPageMap = __webpack_require__(/*! @pages/order/OrderViewPageMap */ "./js/pages/order/OrderViewPageMap.js");

var _OrderViewPageMap2 = _interopRequireDefault(_OrderViewPageMap);

var _eventEmitter = __webpack_require__(/*! @components/event-emitter */ "./js/components/event-emitter.js");

var _orderViewEventMap = __webpack_require__(/*! @pages/order/view/order-view-event-map */ "./js/pages/order/view/order-view-event-map.js");

var _orderViewEventMap2 = _interopRequireDefault(_orderViewEventMap);

var _orderPrices = __webpack_require__(/*! @pages/order/view/order-prices */ "./js/pages/order/view/order-prices.js");

var _orderPrices2 = _interopRequireDefault(_orderPrices);

var _modal = __webpack_require__(/*! @components/modal */ "./js/components/modal.js");

var _modal2 = _interopRequireDefault(_modal);

var _orderPricesRefresher = __webpack_require__(/*! @pages/order/view/order-prices-refresher */ "./js/pages/order/view/order-prices-refresher.js");

var _orderPricesRefresher2 = _interopRequireDefault(_orderPricesRefresher);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _window = window,
    $ = _window.$; /**
                    * Copyright since 2007 PrestaShop SA and Contributors
                    * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
                    *
                    * NOTICE OF LICENSE
                    *
                    * This source file is subject to the Open Software License (OSL 3.0)
                    * that is bundled with this package in the file LICENSE.md.
                    * It is also available through the world-wide-web at this URL:
                    * https://opensource.org/licenses/OSL-3.0
                    * If you did not receive a copy of the license and are unable to
                    * obtain it through the world-wide-web, please send an email
                    * to license@prestashop.com so we can send you a copy immediately.
                    *
                    * DISCLAIMER
                    *
                    * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
                    * versions in the future. If you wish to customize PrestaShop for your
                    * needs please refer to https://devdocs.prestashop.com/ for more information.
                    *
                    * @author    PrestaShop SA and Contributors <contact@prestashop.com>
                    * @copyright Since 2007 PrestaShop SA and Contributors
                    * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
                    */

var OrderProductEdit = function () {
  function OrderProductEdit(orderDetailId) {
    (0, _classCallCheck3.default)(this, OrderProductEdit);

    this.router = new _router2.default();
    this.orderDetailId = orderDetailId;
    this.productRow = $('#orderProduct_' + this.orderDetailId);
    this.product = {};
    this.currencyPrecision = $(_OrderViewPageMap2.default.productsTable).data('currencyPrecision');
    this.priceTaxCalculator = new _orderPrices2.default();
    this.productEditSaveBtn = $(_OrderViewPageMap2.default.productEditSaveBtn);
    this.quantityInput = $(_OrderViewPageMap2.default.productEditQuantityInput);
    this.orderPricesRefresher = new _orderPricesRefresher2.default();
  }

  (0, _createClass3.default)(OrderProductEdit, [{
    key: 'setupListener',
    value: function setupListener() {
      var _this = this;

      this.quantityInput.on('change keyup', function (event) {
        var newQuantity = Number(event.target.value);
        var availableQuantity = parseInt($(event.currentTarget).data('availableQuantity'), 10);
        var previousQuantity = parseInt(_this.quantityInput.data('previousQuantity'), 10);
        var remainingAvailable = availableQuantity - (newQuantity - previousQuantity);
        var availableOutOfStock = _this.availableText.data('availableOutOfStock');
        _this.quantity = newQuantity;
        _this.availableText.text(remainingAvailable);
        _this.availableText.toggleClass('text-danger font-weight-bold', remainingAvailable < 0);
        _this.updateTotal();
        var disableEditActionBtn = newQuantity <= 0 || remainingAvailable < 0 && !availableOutOfStock;
        _this.productEditSaveBtn.prop('disabled', disableEditActionBtn);
      });

      this.productEditInvoiceSelect.on('change', function () {
        _this.productEditSaveBtn.prop('disabled', false);
      });

      this.priceTaxIncludedInput.on('change keyup', function (event) {
        _this.taxIncluded = parseFloat(event.target.value);
        _this.taxExcluded = _this.priceTaxCalculator.calculateTaxExcluded(_this.taxIncluded, _this.taxRate, _this.currencyPrecision);
        _this.priceTaxExcludedInput.val(_this.taxExcluded);
        _this.updateTotal();
      });

      this.priceTaxExcludedInput.on('change keyup', function (event) {
        _this.taxExcluded = parseFloat(event.target.value);
        _this.taxIncluded = _this.priceTaxCalculator.calculateTaxIncluded(_this.taxExcluded, _this.taxRate, _this.currencyPrecision);
        _this.priceTaxIncludedInput.val(_this.taxIncluded);
        _this.updateTotal();
      });

      this.productEditSaveBtn.on('click', function (event) {
        var $btn = $(event.currentTarget);
        var confirmed = window.confirm($btn.data('updateMessage'));

        if (!confirmed) {
          return;
        }

        $btn.prop('disabled', true);
        _this.handleEditProductWithConfirmationModal(event);
      });

      this.productEditCancelBtn.on('click', function () {
        _eventEmitter.EventEmitter.emit(_orderViewEventMap2.default.productEditionCanceled, {
          orderDetailId: _this.orderDetailId
        });
      });
    }
  }, {
    key: 'updateTotal',
    value: function updateTotal() {
      var updatedTotal = this.priceTaxCalculator.calculateTotalPrice(this.quantity, this.isOrderTaxIncluded ? this.taxIncluded : this.taxExcluded, this.currencyPrecision);
      this.priceTotalText.html(updatedTotal);
      this.productEditSaveBtn.prop('disabled', updatedTotal === this.initialTotal);
    }
  }, {
    key: 'displayProduct',
    value: function displayProduct(product) {
      this.productRowEdit = $(_OrderViewPageMap2.default.productEditRowTemplate).clone(true);
      this.productRowEdit.attr('id', 'editOrderProduct_' + this.orderDetailId);
      this.productRowEdit.find('*[id]').each(function removeAllIds() {
        $(this).removeAttr('id');
      });

      // Find controls
      this.productEditSaveBtn = this.productRowEdit.find(_OrderViewPageMap2.default.productEditSaveBtn);
      this.productEditCancelBtn = this.productRowEdit.find(_OrderViewPageMap2.default.productEditCancelBtn);
      this.productEditInvoiceSelect = this.productRowEdit.find(_OrderViewPageMap2.default.productEditInvoiceSelect);
      this.productEditImage = this.productRowEdit.find(_OrderViewPageMap2.default.productEditImage);
      this.productEditName = this.productRowEdit.find(_OrderViewPageMap2.default.productEditName);
      this.priceTaxIncludedInput = this.productRowEdit.find(_OrderViewPageMap2.default.productEditPriceTaxInclInput);
      this.priceTaxExcludedInput = this.productRowEdit.find(_OrderViewPageMap2.default.productEditPriceTaxExclInput);
      this.quantityInput = this.productRowEdit.find(_OrderViewPageMap2.default.productEditQuantityInput);
      this.locationText = this.productRowEdit.find(_OrderViewPageMap2.default.productEditLocationText);
      this.availableText = this.productRowEdit.find(_OrderViewPageMap2.default.productEditAvailableText);
      this.priceTotalText = this.productRowEdit.find(_OrderViewPageMap2.default.productEditTotalPriceText);

      // Init input values
      this.priceTaxExcludedInput.val(window.ps_round(product.price_tax_excl, this.currencyPrecision));
      this.priceTaxIncludedInput.val(window.ps_round(product.price_tax_incl, this.currencyPrecision));
      this.quantityInput.val(product.quantity).data('availableQuantity', product.availableQuantity).data('previousQuantity', product.quantity);
      this.availableText.data('availableOutOfStock', product.availableOutOfStock);

      // set this product's orderInvoiceId as selected
      if (product.orderInvoiceId) {
        this.productEditInvoiceSelect.val(product.orderInvoiceId);
      }

      // Init editor data
      this.taxRate = product.tax_rate;
      this.initialTotal = this.priceTaxCalculator.calculateTotalPrice(product.quantity, product.isOrderTaxIncluded ? product.price_tax_incl : product.price_tax_excl, this.currencyPrecision);
      this.isOrderTaxIncluded = product.isOrderTaxIncluded;
      this.quantity = product.quantity;
      this.taxIncluded = product.price_tax_incl;
      this.taxExcluded = product.price_tax_excl;

      // Copy product content in cells
      this.productEditImage.html(this.productRow.find(_OrderViewPageMap2.default.productEditImage).html());
      this.productEditName.html(this.productRow.find(_OrderViewPageMap2.default.productEditName).html());
      this.locationText.html(product.location);
      this.availableText.html(product.availableQuantity);
      this.priceTotalText.html(this.initialTotal);
      this.productRow.addClass('d-none').after(this.productRowEdit.removeClass('d-none'));

      this.setupListener();
    }
  }, {
    key: 'handleEditProductWithConfirmationModal',
    value: function handleEditProductWithConfirmationModal(event) {
      var _this2 = this;

      var productEditBtn = $('#orderProduct_' + this.orderDetailId + ' ' + _OrderViewPageMap2.default.productEditButtons);
      var productId = productEditBtn.data('product-id');
      var combinationId = productEditBtn.data('combination-id');
      var orderInvoiceId = productEditBtn.data('order-invoice-id');
      var productPriceMatch = this.orderPricesRefresher.checkOtherProductPricesMatch(this.priceTaxIncludedInput.val(), productId, combinationId, orderInvoiceId, this.orderDetailId);

      if (productPriceMatch === null) {
        this.editProduct($(event.currentTarget).data('orderId'), this.orderDetailId);

        return;
      }

      var dataSelector = productPriceMatch === 'product' ? this.priceTaxExcludedInput : this.productEditInvoiceSelect;

      var modalEditPrice = new _modal2.default({
        id: 'modal-confirm-new-price',
        confirmTitle: dataSelector.data('modal-edit-price-title'),
        confirmMessage: dataSelector.data('modal-edit-price-body'),
        confirmButtonLabel: dataSelector.data('modal-edit-price-apply'),
        closeButtonLabel: dataSelector.data('modal-edit-price-cancel')
      }, function () {
        _this2.editProduct($(event.currentTarget).data('orderId'), _this2.orderDetailId);
      });

      modalEditPrice.show();
    }
  }, {
    key: 'editProduct',
    value: function editProduct(orderId, orderDetailId) {
      var params = {
        price_tax_incl: this.priceTaxIncludedInput.val(),
        price_tax_excl: this.priceTaxExcludedInput.val(),
        quantity: this.quantityInput.val(),
        invoice: this.productEditInvoiceSelect.val()
      };

      $.ajax({
        url: this.router.generate('admin_orders_update_product', {
          orderId: orderId,
          orderDetailId: orderDetailId
        }),
        method: 'POST',
        data: params
      }).then(function () {
        _eventEmitter.EventEmitter.emit(_orderViewEventMap2.default.productUpdated, {
          orderId: orderId,
          orderDetailId: orderDetailId
        });
      }, function (response) {
        if (response.responseJSON && response.responseJSON.message) {
          $.growl.error({ message: response.responseJSON.message });
        }
      });
    }
  }]);
  return OrderProductEdit;
}();

exports.default = OrderProductEdit;

/***/ }),

/***/ "./js/pages/order/view/order-product-manager.js":
/*!******************************************************!*\
  !*** ./js/pages/order/view/order-product-manager.js ***!
  \******************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

"use strict";


Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _router = __webpack_require__(/*! @components/router */ "./js/components/router.js");

var _router2 = _interopRequireDefault(_router);

var _eventEmitter = __webpack_require__(/*! @components/event-emitter */ "./js/components/event-emitter.js");

var _orderViewEventMap = __webpack_require__(/*! @pages/order/view/order-view-event-map */ "./js/pages/order/view/order-view-event-map.js");

var _orderViewEventMap2 = _interopRequireDefault(_orderViewEventMap);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _window = window,
    $ = _window.$; /**
                    * Copyright since 2007 PrestaShop SA and Contributors
                    * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
                    *
                    * NOTICE OF LICENSE
                    *
                    * This source file is subject to the Open Software License (OSL 3.0)
                    * that is bundled with this package in the file LICENSE.md.
                    * It is also available through the world-wide-web at this URL:
                    * https://opensource.org/licenses/OSL-3.0
                    * If you did not receive a copy of the license and are unable to
                    * obtain it through the world-wide-web, please send an email
                    * to license@prestashop.com so we can send you a copy immediately.
                    *
                    * DISCLAIMER
                    *
                    * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
                    * versions in the future. If you wish to customize PrestaShop for your
                    * needs please refer to https://devdocs.prestashop.com/ for more information.
                    *
                    * @author    PrestaShop SA and Contributors <contact@prestashop.com>
                    * @copyright Since 2007 PrestaShop SA and Contributors
                    * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
                    */

var OrderProductManager = function () {
  function OrderProductManager() {
    (0, _classCallCheck3.default)(this, OrderProductManager);

    this.router = new _router2.default();
  }

  (0, _createClass3.default)(OrderProductManager, [{
    key: 'handleDeleteProductEvent',
    value: function handleDeleteProductEvent(event) {
      event.preventDefault();

      var $btn = $(event.currentTarget);
      var confirmed = window.confirm($btn.data('deleteMessage'));

      if (!confirmed) {
        return;
      }

      $btn.pstooltip('dispose');
      $btn.prop('disabled', true);
      this.deleteProduct($btn.data('orderId'), $btn.data('orderDetailId'));
    }
  }, {
    key: 'deleteProduct',
    value: function deleteProduct(orderId, orderDetailId) {
      $.ajax(this.router.generate('admin_orders_delete_product', { orderId: orderId, orderDetailId: orderDetailId }), {
        method: 'POST'
      }).then(function () {
        _eventEmitter.EventEmitter.emit(_orderViewEventMap2.default.productDeletedFromOrder, {
          oldOrderDetailId: orderDetailId,
          orderId: orderId
        });
      }, function (response) {
        if (response.responseJSON && response.responseJSON.message) {
          $.growl.error({ message: response.responseJSON.message });
        }
      });
    }
  }]);
  return OrderProductManager;
}();

exports.default = OrderProductManager;

/***/ }),

/***/ "./js/pages/order/view/order-product-renderer.js":
/*!*******************************************************!*\
  !*** ./js/pages/order/view/order-product-renderer.js ***!
  \*******************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

"use strict";


Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _OrderViewPageMap = __webpack_require__(/*! @pages/order/OrderViewPageMap */ "./js/pages/order/OrderViewPageMap.js");

var _OrderViewPageMap2 = _interopRequireDefault(_OrderViewPageMap);

var _orderProductEdit = __webpack_require__(/*! @pages/order/view/order-product-edit */ "./js/pages/order/view/order-product-edit.js");

var _orderProductEdit2 = _interopRequireDefault(_orderProductEdit);

var _router = __webpack_require__(/*! @components/router */ "./js/components/router.js");

var _router2 = _interopRequireDefault(_router);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _window = window,
    $ = _window.$; /**
                    * Copyright since 2007 PrestaShop SA and Contributors
                    * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
                    *
                    * NOTICE OF LICENSE
                    *
                    * This source file is subject to the Open Software License (OSL 3.0)
                    * that is bundled with this package in the file LICENSE.md.
                    * It is also available through the world-wide-web at this URL:
                    * https://opensource.org/licenses/OSL-3.0
                    * If you did not receive a copy of the license and are unable to
                    * obtain it through the world-wide-web, please send an email
                    * to license@prestashop.com so we can send you a copy immediately.
                    *
                    * DISCLAIMER
                    *
                    * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
                    * versions in the future. If you wish to customize PrestaShop for your
                    * needs please refer to https://devdocs.prestashop.com/ for more information.
                    *
                    * @author    PrestaShop SA and Contributors <contact@prestashop.com>
                    * @copyright Since 2007 PrestaShop SA and Contributors
                    * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
                    */

var OrderProductRenderer = function () {
  function OrderProductRenderer() {
    (0, _classCallCheck3.default)(this, OrderProductRenderer);

    this.router = new _router2.default();
  }

  (0, _createClass3.default)(OrderProductRenderer, [{
    key: 'addOrUpdateProductToList',
    value: function addOrUpdateProductToList($productRow, newRow) {
      if ($productRow.length > 0) {
        $productRow.html($(newRow).html());
      } else {
        $(_OrderViewPageMap2.default.productAddRow).before($(newRow).hide().fadeIn());
      }
    }
  }, {
    key: 'updateNumProducts',
    value: function updateNumProducts(numProducts) {
      $(_OrderViewPageMap2.default.productsCount).html(numProducts);
    }
  }, {
    key: 'editProductFromList',
    value: function editProductFromList(orderDetailId, quantity, priceTaxIncl, priceTaxExcl, taxRate, location, availableQuantity, availableOutOfStock, orderInvoiceId, isOrderTaxIncluded) {
      var $orderEdit = new _orderProductEdit2.default(orderDetailId);
      $orderEdit.displayProduct({
        price_tax_excl: priceTaxExcl,
        price_tax_incl: priceTaxIncl,
        tax_rate: taxRate,
        quantity: quantity,
        location: location,
        availableQuantity: availableQuantity,
        availableOutOfStock: availableOutOfStock,
        orderInvoiceId: orderInvoiceId,
        isOrderTaxIncluded: isOrderTaxIncluded
      });
      $(_OrderViewPageMap2.default.productAddActionBtn).addClass('d-none');
      $(_OrderViewPageMap2.default.productAddRow).addClass('d-none');
    }
  }, {
    key: 'moveProductsPanelToModificationPosition',
    value: function moveProductsPanelToModificationPosition() {
      var scrollTarget = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'body';

      $(_OrderViewPageMap2.default.productActionBtn).addClass('d-none');
      $(_OrderViewPageMap2.default.productAddActionBtn + ', ' + _OrderViewPageMap2.default.productAddRow).removeClass('d-none');
      this.moveProductPanelToTop(scrollTarget);
    }
  }, {
    key: 'moveProductsPanelToRefundPosition',
    value: function moveProductsPanelToRefundPosition() {
      this.resetAllEditRows();
      $(
      /* eslint-disable-next-line max-len */
      _OrderViewPageMap2.default.productAddActionBtn + ', ' + _OrderViewPageMap2.default.productAddRow + ', ' + _OrderViewPageMap2.default.productActionBtn).addClass('d-none');
      this.moveProductPanelToTop();
    }
  }, {
    key: 'moveProductPanelToTop',
    value: function moveProductPanelToTop() {
      var scrollTarget = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'body';

      var $modificationPosition = $(_OrderViewPageMap2.default.productModificationPosition);

      if ($modificationPosition.find(_OrderViewPageMap2.default.productsPanel).length > 0) {
        return;
      }
      $(_OrderViewPageMap2.default.productsPanel).detach().appendTo($modificationPosition);
      $modificationPosition.closest('.row').removeClass('d-none');

      // Show column location & refunded
      this.toggleColumn(_OrderViewPageMap2.default.productsCellLocation);
      this.toggleColumn(_OrderViewPageMap2.default.productsCellRefunded);

      // Show all rows, hide pagination controls
      var $rows = $(_OrderViewPageMap2.default.productsTable).find('tr[id^="orderProduct_"]');
      $rows.removeClass('d-none');
      $(_OrderViewPageMap2.default.productsPagination).addClass('d-none');

      var scrollValue = $(scrollTarget).offset().top - $('.header-toolbar').height() - 100;
      $('html,body').animate({ scrollTop: scrollValue }, 'slow');
    }
  }, {
    key: 'moveProductPanelToOriginalPosition',
    value: function moveProductPanelToOriginalPosition() {
      $(_OrderViewPageMap2.default.productAddNewInvoiceInfo).addClass('d-none');
      $(_OrderViewPageMap2.default.productModificationPosition).closest('.row').addClass('d-none');

      $(_OrderViewPageMap2.default.productsPanel).detach().appendTo(_OrderViewPageMap2.default.productOriginalPosition);

      $(_OrderViewPageMap2.default.productsPagination).removeClass('d-none');
      $(_OrderViewPageMap2.default.productActionBtn).removeClass('d-none');
      $(_OrderViewPageMap2.default.productAddActionBtn + ', ' + _OrderViewPageMap2.default.productAddRow).addClass('d-none');

      // Restore pagination
      this.paginate(1);
    }
  }, {
    key: 'resetAddRow',
    value: function resetAddRow() {
      $(_OrderViewPageMap2.default.productAddIdInput).val('');
      $(_OrderViewPageMap2.default.productSearchInput).val('');
      $(_OrderViewPageMap2.default.productAddCombinationsBlock).addClass('d-none');
      $(_OrderViewPageMap2.default.productAddCombinationsSelect).val('');
      $(_OrderViewPageMap2.default.productAddCombinationsSelect).prop('disabled', false);
      $(_OrderViewPageMap2.default.productAddPriceTaxExclInput).val('');
      $(_OrderViewPageMap2.default.productAddPriceTaxInclInput).val('');
      $(_OrderViewPageMap2.default.productAddQuantityInput).val('');
      $(_OrderViewPageMap2.default.productAddAvailableText).html('');
      $(_OrderViewPageMap2.default.productAddLocationText).html('');
      $(_OrderViewPageMap2.default.productAddNewInvoiceInfo).addClass('d-none');
      $(_OrderViewPageMap2.default.productAddActionBtn).prop('disabled', true);
    }
  }, {
    key: 'resetAllEditRows',
    value: function resetAllEditRows() {
      var _this = this;

      $(_OrderViewPageMap2.default.productEditButtons).each(function (key, editButton) {
        _this.resetEditRow($(editButton).data('orderDetailId'));
      });
    }
  }, {
    key: 'resetEditRow',
    value: function resetEditRow(orderProductId) {
      var $productRow = $(_OrderViewPageMap2.default.productsTableRow(orderProductId));
      var $productEditRow = $(_OrderViewPageMap2.default.productsTableRowEdited(orderProductId));
      $productEditRow.remove();
      $productRow.removeClass('d-none');
    }
  }, {
    key: 'paginate',
    value: function paginate(originalNumPage) {
      var $rows = $(_OrderViewPageMap2.default.productsTable).find('tr[id^="orderProduct_"]');
      var $customizationRows = $(_OrderViewPageMap2.default.productsTableCustomizationRows);
      var $tablePagination = $(_OrderViewPageMap2.default.productsTablePagination);
      var numRowsPerPage = parseInt($tablePagination.data('numPerPage'), 10);
      var maxPage = Math.ceil($rows.length / numRowsPerPage);
      var numPage = Math.max(1, Math.min(originalNumPage, maxPage));
      this.paginateUpdateControls(numPage);

      // Hide all rows...
      $rows.addClass('d-none');
      $customizationRows.addClass('d-none');
      // ... and display good ones

      var startRow = (numPage - 1) * numRowsPerPage + 1;
      var endRow = numPage * numRowsPerPage;

      for (var i = startRow - 1; i < Math.min(endRow, $rows.length); i += 1) {
        $($rows[i]).removeClass('d-none');
      }

      $customizationRows.each(function () {
        if (!$(this).prev().hasClass('d-none')) {
          $(this).removeClass('d-none');
        }
      });

      // Remove all edition rows (careful not to remove the template)
      $(_OrderViewPageMap2.default.productEditRow).not(_OrderViewPageMap2.default.productEditRowTemplate).remove();

      // Toggle Column Location & Refunded
      this.toggleColumn(_OrderViewPageMap2.default.productsCellLocationDisplayed);
      this.toggleColumn(_OrderViewPageMap2.default.productsCellRefundedDisplayed);
    }
  }, {
    key: 'paginateUpdateControls',
    value: function paginateUpdateControls(numPage) {
      // Why 3 ? Next & Prev & Template
      var totalPage = $(_OrderViewPageMap2.default.productsTablePagination).find('li.page-item').length - 3;
      $(_OrderViewPageMap2.default.productsTablePagination).find('.active').removeClass('active');
      $(_OrderViewPageMap2.default.productsTablePagination).find('li:has(> [data-page="' + numPage + '"])').addClass('active');
      $(_OrderViewPageMap2.default.productsTablePaginationPrev).removeClass('disabled');
      if (numPage === 1) {
        $(_OrderViewPageMap2.default.productsTablePaginationPrev).addClass('disabled');
      }
      $(_OrderViewPageMap2.default.productsTablePaginationNext).removeClass('disabled');
      if (numPage === totalPage) {
        $(_OrderViewPageMap2.default.productsTablePaginationNext).addClass('disabled');
      }
      this.togglePaginationControls();
    }
  }, {
    key: 'updateNumPerPage',
    value: function updateNumPerPage(numPerPage) {
      $(_OrderViewPageMap2.default.productsTablePagination).data('numPerPage', numPerPage);
      this.updatePaginationControls();
    }
  }, {
    key: 'togglePaginationControls',
    value: function togglePaginationControls() {
      // Why 3 ? Next & Prev & Template
      var totalPage = $(_OrderViewPageMap2.default.productsTablePagination).find('li.page-item').length - 3;
      $(_OrderViewPageMap2.default.productsNavPagination).toggleClass('d-none', totalPage <= 1);
    }
  }, {
    key: 'toggleProductAddNewInvoiceInfo',
    value: function toggleProductAddNewInvoiceInfo() {
      $(_OrderViewPageMap2.default.productAddNewInvoiceInfo).toggleClass('d-none', parseInt($(_OrderViewPageMap2.default.productAddInvoiceSelect).val(), 10) !== 0);
    }
  }, {
    key: 'toggleColumn',
    value: function toggleColumn(target) {
      var forceDisplay = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;

      var isColumnDisplayed = false;

      if (forceDisplay === null) {
        $(target).filter('td').each(function () {
          if ($(this).html() !== '') {
            isColumnDisplayed = true;
            return false;
          }
          return true;
        });
      } else {
        isColumnDisplayed = forceDisplay;
      }
      $(target).toggleClass('d-none', !isColumnDisplayed);
    }
  }, {
    key: 'updatePaginationControls',
    value: function updatePaginationControls() {
      var $tablePagination = $(_OrderViewPageMap2.default.productsTablePagination);
      var numPerPage = $tablePagination.data('numPerPage');
      var $rows = $(_OrderViewPageMap2.default.productsTable).find('tr[id^="orderProduct_"]');
      var numPages = Math.ceil($rows.length / numPerPage);

      // Update table data fields
      $tablePagination.data('numPages', numPages);

      // Clean all page links, reinsert the removed template
      var $linkPaginationTemplate = $(_OrderViewPageMap2.default.productsTablePaginationTemplate);
      $(_OrderViewPageMap2.default.productsTablePagination).find('li:has(> [data-page])').remove();
      $(_OrderViewPageMap2.default.productsTablePaginationNext).before($linkPaginationTemplate);

      // Add appropriate pages
      for (var i = 1; i <= numPages; i += 1) {
        var $linkPagination = $linkPaginationTemplate.clone();
        $linkPagination.find('span').attr('data-page', i);
        $linkPagination.find('span').html(i);
        $linkPaginationTemplate.before($linkPagination.removeClass('d-none'));
      }

      this.togglePaginationControls();
    }
  }]);
  return OrderProductRenderer;
}();

exports.default = OrderProductRenderer;

/***/ }),

/***/ "./js/pages/order/view/order-shipping-refresher.js":
/*!*********************************************************!*\
  !*** ./js/pages/order/view/order-shipping-refresher.js ***!
  \*********************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

"use strict";


Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _router = __webpack_require__(/*! @components/router */ "./js/components/router.js");

var _router2 = _interopRequireDefault(_router);

var _OrderViewPageMap = __webpack_require__(/*! @pages/order/OrderViewPageMap */ "./js/pages/order/OrderViewPageMap.js");

var _OrderViewPageMap2 = _interopRequireDefault(_OrderViewPageMap);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 */

var _window = window,
    $ = _window.$;

var OrderShippingRefresher = function () {
  function OrderShippingRefresher() {
    (0, _classCallCheck3.default)(this, OrderShippingRefresher);

    this.router = new _router2.default();
  }

  (0, _createClass3.default)(OrderShippingRefresher, [{
    key: 'refresh',
    value: function refresh(orderId) {
      $.getJSON(this.router.generate('admin_orders_get_shipping', { orderId: orderId })).then(function (response) {
        $(_OrderViewPageMap2.default.orderShippingTabCount).text(response.total);
        $(_OrderViewPageMap2.default.orderShippingTabBody).html(response.html);
      });
    }
  }]);
  return OrderShippingRefresher;
}();

exports.default = OrderShippingRefresher;

/***/ }),

/***/ "./js/pages/order/view/order-view-event-map.js":
/*!*****************************************************!*\
  !*** ./js/pages/order/view/order-view-event-map.js ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, exports) => {

"use strict";


Object.defineProperty(exports, "__esModule", ({
  value: true
}));
/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 */

exports.default = {
  productDeletedFromOrder: 'productDeletedFromOrder',
  productAddedToOrder: 'productAddedToOrder',
  productUpdated: 'productUpdated',
  productEditionCanceled: 'productEditionCanceled',
  productListPaginated: 'productListPaginated',
  productListNumberPerPage: 'productListNumberPerPage'
};

/***/ }),

/***/ "./js/pages/order/view/order-view-page.js":
/*!************************************************!*\
  !*** ./js/pages/order/view/order-view-page.js ***!
  \************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

"use strict";


Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _orderProductManager = __webpack_require__(/*! @pages/order/view/order-product-manager */ "./js/pages/order/view/order-product-manager.js");

var _orderProductManager2 = _interopRequireDefault(_orderProductManager);

var _OrderViewPageMap = __webpack_require__(/*! @pages/order/OrderViewPageMap */ "./js/pages/order/OrderViewPageMap.js");

var _OrderViewPageMap2 = _interopRequireDefault(_OrderViewPageMap);

var _orderViewEventMap = __webpack_require__(/*! @pages/order/view/order-view-event-map */ "./js/pages/order/view/order-view-event-map.js");

var _orderViewEventMap2 = _interopRequireDefault(_orderViewEventMap);

var _eventEmitter = __webpack_require__(/*! @components/event-emitter */ "./js/components/event-emitter.js");

var _orderDiscountsRefresher = __webpack_require__(/*! @pages/order/view/order-discounts-refresher */ "./js/pages/order/view/order-discounts-refresher.js");

var _orderDiscountsRefresher2 = _interopRequireDefault(_orderDiscountsRefresher);

var _orderProductRenderer = __webpack_require__(/*! @pages/order/view/order-product-renderer */ "./js/pages/order/view/order-product-renderer.js");

var _orderProductRenderer2 = _interopRequireDefault(_orderProductRenderer);

var _orderPricesRefresher = __webpack_require__(/*! @pages/order/view/order-prices-refresher */ "./js/pages/order/view/order-prices-refresher.js");

var _orderPricesRefresher2 = _interopRequireDefault(_orderPricesRefresher);

var _orderPaymentsRefresher = __webpack_require__(/*! @pages/order/view/order-payments-refresher */ "./js/pages/order/view/order-payments-refresher.js");

var _orderPaymentsRefresher2 = _interopRequireDefault(_orderPaymentsRefresher);

var _orderShippingRefresher = __webpack_require__(/*! @pages/order/view/order-shipping-refresher */ "./js/pages/order/view/order-shipping-refresher.js");

var _orderShippingRefresher2 = _interopRequireDefault(_orderShippingRefresher);

var _router = __webpack_require__(/*! @components/router */ "./js/components/router.js");

var _router2 = _interopRequireDefault(_router);

var _orderInvoicesRefresher = __webpack_require__(/*! ./order-invoices-refresher */ "./js/pages/order/view/order-invoices-refresher.js");

var _orderInvoicesRefresher2 = _interopRequireDefault(_orderInvoicesRefresher);

var _orderProductCancel = __webpack_require__(/*! ./order-product-cancel */ "./js/pages/order/view/order-product-cancel.js");

var _orderProductCancel2 = _interopRequireDefault(_orderProductCancel);

var _orderDocumentsRefresher = __webpack_require__(/*! ./order-documents-refresher */ "./js/pages/order/view/order-documents-refresher.js");

var _orderDocumentsRefresher2 = _interopRequireDefault(_orderDocumentsRefresher);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _window = window,
    $ = _window.$; /**
                    * Copyright since 2007 PrestaShop SA and Contributors
                    * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
                    *
                    * NOTICE OF LICENSE
                    *
                    * This source file is subject to the Open Software License (OSL 3.0)
                    * that is bundled with this package in the file LICENSE.md.
                    * It is also available through the world-wide-web at this URL:
                    * https://opensource.org/licenses/OSL-3.0
                    * If you did not receive a copy of the license and are unable to
                    * obtain it through the world-wide-web, please send an email
                    * to license@prestashop.com so we can send you a copy immediately.
                    *
                    * DISCLAIMER
                    *
                    * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
                    * versions in the future. If you wish to customize PrestaShop for your
                    * needs please refer to https://devdocs.prestashop.com/ for more information.
                    *
                    * @author    PrestaShop SA and Contributors <contact@prestashop.com>
                    * @copyright Since 2007 PrestaShop SA and Contributors
                    * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
                    */

var OrderViewPage = function () {
  function OrderViewPage() {
    (0, _classCallCheck3.default)(this, OrderViewPage);

    this.orderDiscountsRefresher = new _orderDiscountsRefresher2.default();
    this.orderProductManager = new _orderProductManager2.default();
    this.orderProductRenderer = new _orderProductRenderer2.default();
    this.orderPricesRefresher = new _orderPricesRefresher2.default();
    this.orderPaymentsRefresher = new _orderPaymentsRefresher2.default();
    this.orderShippingRefresher = new _orderShippingRefresher2.default();
    this.orderDocumentsRefresher = new _orderDocumentsRefresher2.default();
    this.orderInvoicesRefresher = new _orderInvoicesRefresher2.default();
    this.orderProductCancel = new _orderProductCancel2.default();
    this.router = new _router2.default();
    this.listenToEvents();
  }

  (0, _createClass3.default)(OrderViewPage, [{
    key: 'listenToEvents',
    value: function listenToEvents() {
      var _this = this;

      $(_OrderViewPageMap2.default.invoiceAddressEditBtn).fancybox({
        type: 'iframe',
        width: '90%',
        height: '90%'
      });
      $(_OrderViewPageMap2.default.deliveryAddressEditBtn).fancybox({
        type: 'iframe',
        width: '90%',
        height: '90%'
      });

      _eventEmitter.EventEmitter.on(_orderViewEventMap2.default.productDeletedFromOrder, function (event) {
        _this.orderPricesRefresher.refresh(event.orderId);
        _this.orderPaymentsRefresher.refresh(event.orderId);
        _this.refreshProductsList(event.orderId);
        _this.orderDiscountsRefresher.refresh(event.orderId);
        _this.orderDocumentsRefresher.refresh(event.orderId);
        _this.orderShippingRefresher.refresh(event.orderId);
      });

      _eventEmitter.EventEmitter.on(_orderViewEventMap2.default.productEditionCanceled, function (event) {
        _this.orderProductRenderer.resetEditRow(event.orderDetailId);
        var editRowsLeft = $(_OrderViewPageMap2.default.productEditRow).not(_OrderViewPageMap2.default.productEditRowTemplate).length;

        if (editRowsLeft > 0) {
          return;
        }
        _this.orderProductRenderer.moveProductPanelToOriginalPosition();
      });

      _eventEmitter.EventEmitter.on(_orderViewEventMap2.default.productUpdated, function (event) {
        _this.orderProductRenderer.resetEditRow(event.orderDetailId);
        _this.orderPricesRefresher.refresh(event.orderId);
        _this.orderPricesRefresher.refreshProductPrices(event.orderId);
        _this.refreshProductsList(event.orderId);
        _this.orderPaymentsRefresher.refresh(event.orderId);
        _this.orderDiscountsRefresher.refresh(event.orderId);
        _this.orderInvoicesRefresher.refresh(event.orderId);
        _this.orderDocumentsRefresher.refresh(event.orderId);
        _this.orderShippingRefresher.refresh(event.orderId);
        _this.listenForProductDelete();
        _this.listenForProductEdit();
        _this.resetToolTips();

        var editRowsLeft = $(_OrderViewPageMap2.default.productEditRow).not(_OrderViewPageMap2.default.productEditRowTemplate).length;

        if (editRowsLeft > 0) {
          return;
        }
        _this.orderProductRenderer.moveProductPanelToOriginalPosition();
      });

      _eventEmitter.EventEmitter.on(_orderViewEventMap2.default.productAddedToOrder, function (event) {
        _this.orderProductRenderer.resetAddRow();
        _this.orderPricesRefresher.refreshProductPrices(event.orderId);
        _this.orderPricesRefresher.refresh(event.orderId);
        _this.refreshProductsList(event.orderId);
        _this.orderPaymentsRefresher.refresh(event.orderId);
        _this.orderDiscountsRefresher.refresh(event.orderId);
        _this.orderInvoicesRefresher.refresh(event.orderId);
        _this.orderDocumentsRefresher.refresh(event.orderId);
        _this.orderShippingRefresher.refresh(event.orderId);
        _this.orderProductRenderer.moveProductPanelToOriginalPosition();
      });
    }
  }, {
    key: 'listenForProductDelete',
    value: function listenForProductDelete() {
      var _this2 = this;

      $(_OrderViewPageMap2.default.productDeleteBtn).off('click').on('click', function (event) {
        return _this2.orderProductManager.handleDeleteProductEvent(event);
      });
    }
  }, {
    key: 'resetToolTips',
    value: function resetToolTips() {
      $(_OrderViewPageMap2.default.productEditButtons).pstooltip();
      $(_OrderViewPageMap2.default.productDeleteBtn).pstooltip();
    }
  }, {
    key: 'listenForProductEdit',
    value: function listenForProductEdit() {
      var _this3 = this;

      $(_OrderViewPageMap2.default.productEditButtons).off('click').on('click', function (event) {
        var $btn = $(event.currentTarget);
        _this3.orderProductRenderer.moveProductsPanelToModificationPosition();
        _this3.orderProductRenderer.editProductFromList($btn.data('orderDetailId'), $btn.data('productQuantity'), $btn.data('productPriceTaxIncl'), $btn.data('productPriceTaxExcl'), $btn.data('taxRate'), $btn.data('location'), $btn.data('availableQuantity'), $btn.data('availableOutOfStock'), $btn.data('orderInvoiceId'), $btn.data('isOrderTaxIncluded'));
      });
    }
  }, {
    key: 'listenForProductPack',
    value: function listenForProductPack() {
      var _this4 = this;

      $(_OrderViewPageMap2.default.productPackModal.modal).on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var packItems = button.data('packItems');
        $(_OrderViewPageMap2.default.productPackModal.rows).remove();
        packItems.forEach(function (item) {
          var $item = $(_OrderViewPageMap2.default.productPackModal.template).clone();
          $item.attr('id', 'productpack_' + item.id).removeClass('d-none');
          $item.find(_OrderViewPageMap2.default.productPackModal.product.img).attr('src', item.imagePath);
          $item.find(_OrderViewPageMap2.default.productPackModal.product.name).html(item.name);
          $item.find(_OrderViewPageMap2.default.productPackModal.product.link).attr('href', _this4.router.generate('admin_product_form', { id: item.id }));
          if (item.reference !== '') {
            $item.find(_OrderViewPageMap2.default.productPackModal.product.ref).append(item.reference);
          } else {
            $item.find(_OrderViewPageMap2.default.productPackModal.product.ref).remove();
          }
          if (item.supplierReference !== '') {
            $item.find(_OrderViewPageMap2.default.productPackModal.product.supplierRef).append(item.supplierReference);
          } else {
            $item.find(_OrderViewPageMap2.default.productPackModal.product.supplierRef).remove();
          }
          if (item.quantity > 1) {
            $item.find(_OrderViewPageMap2.default.productPackModal.product.quantity + ' span').html(item.quantity);
          } else {
            $item.find(_OrderViewPageMap2.default.productPackModal.product.quantity).html(item.quantity);
          }
          $item.find(_OrderViewPageMap2.default.productPackModal.product.availableQuantity).html(item.availableQuantity);
          $(_OrderViewPageMap2.default.productPackModal.template).before($item);
        });
      });
    }
  }, {
    key: 'listenForProductAdd',
    value: function listenForProductAdd() {
      var _this5 = this;

      $(_OrderViewPageMap2.default.productAddBtn).on('click', function () {
        _this5.orderProductRenderer.toggleProductAddNewInvoiceInfo();
        _this5.orderProductRenderer.moveProductsPanelToModificationPosition(_OrderViewPageMap2.default.productSearchInput);
      });
      $(_OrderViewPageMap2.default.productCancelAddBtn).on('click', function () {
        return _this5.orderProductRenderer.moveProductPanelToOriginalPosition();
      });
    }
  }, {
    key: 'listenForProductPagination',
    value: function listenForProductPagination() {
      var _this6 = this;

      $(_OrderViewPageMap2.default.productsTablePagination).on('click', _OrderViewPageMap2.default.productsTablePaginationLink, function (event) {
        event.preventDefault();
        var $btn = $(event.currentTarget);
        _eventEmitter.EventEmitter.emit(_orderViewEventMap2.default.productListPaginated, {
          numPage: $btn.data('page')
        });
      });
      $(_OrderViewPageMap2.default.productsTablePaginationNext).on('click', function (event) {
        event.preventDefault();
        var $btn = $(event.currentTarget);

        if ($btn.hasClass('disabled')) {
          return;
        }
        var activePage = _this6.getActivePage();
        _eventEmitter.EventEmitter.emit(_orderViewEventMap2.default.productListPaginated, {
          numPage: parseInt($(activePage).html(), 10) + 1
        });
      });
      $(_OrderViewPageMap2.default.productsTablePaginationPrev).on('click', function (event) {
        event.preventDefault();
        var $btn = $(event.currentTarget);

        if ($btn.hasClass('disabled')) {
          return;
        }
        var activePage = _this6.getActivePage();
        _eventEmitter.EventEmitter.emit(_orderViewEventMap2.default.productListPaginated, {
          numPage: parseInt($(activePage).html(), 10) - 1
        });
      });
      $(_OrderViewPageMap2.default.productsTablePaginationNumberSelector).on('change', function (event) {
        event.preventDefault();
        var $select = $(event.currentTarget);
        var numPerPage = parseInt($select.val(), 10);
        _eventEmitter.EventEmitter.emit(_orderViewEventMap2.default.productListNumberPerPage, {
          numPerPage: numPerPage
        });
      });

      _eventEmitter.EventEmitter.on(_orderViewEventMap2.default.productListPaginated, function (event) {
        _this6.orderProductRenderer.paginate(event.numPage);
        _this6.listenForProductDelete();
        _this6.listenForProductEdit();
        _this6.resetToolTips();
      });

      _eventEmitter.EventEmitter.on(_orderViewEventMap2.default.productListNumberPerPage, function (event) {
        // Update pagination num per page (page links are regenerated)
        _this6.orderProductRenderer.updateNumPerPage(event.numPerPage);

        // Paginate to page 1
        _eventEmitter.EventEmitter.emit(_orderViewEventMap2.default.productListPaginated, {
          numPage: 1
        });

        // Save new config
        $.ajax({
          url: _this6.router.generate('admin_orders_configure_product_pagination'),
          method: 'POST',
          data: { numPerPage: event.numPerPage }
        });
      });
    }
  }, {
    key: 'listenForRefund',
    value: function listenForRefund() {
      var _this7 = this;

      $(_OrderViewPageMap2.default.cancelProduct.buttons.partialRefund).on('click', function () {
        _this7.orderProductRenderer.moveProductsPanelToRefundPosition();
        _this7.orderProductCancel.showPartialRefund();
      });

      $(_OrderViewPageMap2.default.cancelProduct.buttons.standardRefund).on('click', function () {
        _this7.orderProductRenderer.moveProductsPanelToRefundPosition();
        _this7.orderProductCancel.showStandardRefund();
      });

      $(_OrderViewPageMap2.default.cancelProduct.buttons.returnProduct).on('click', function () {
        _this7.orderProductRenderer.moveProductsPanelToRefundPosition();
        _this7.orderProductCancel.showReturnProduct();
      });

      $(_OrderViewPageMap2.default.cancelProduct.buttons.abort).on('click', function () {
        _this7.orderProductRenderer.moveProductPanelToOriginalPosition();
        _this7.orderProductCancel.hideRefund();
      });
    }
  }, {
    key: 'listenForCancelProduct',
    value: function listenForCancelProduct() {
      var _this8 = this;

      $(_OrderViewPageMap2.default.cancelProduct.buttons.cancelProducts).on('click', function () {
        _this8.orderProductRenderer.moveProductsPanelToRefundPosition();
        _this8.orderProductCancel.showCancelProductForm();
      });
    }
  }, {
    key: 'getActivePage',
    value: function getActivePage() {
      return $(_OrderViewPageMap2.default.productsTablePagination).find('.active span').get(0);
    }
  }, {
    key: 'refreshProductsList',
    value: function refreshProductsList(orderId) {
      var _this9 = this;

      $(_OrderViewPageMap2.default.refreshProductsListLoadingSpinner).show();

      var $tablePagination = $(_OrderViewPageMap2.default.productsTablePagination);
      var numRowsPerPage = $tablePagination.data('numPerPage');
      var initialNumProducts = $(_OrderViewPageMap2.default.productsTableRows).length;
      var currentPage = parseInt($(_OrderViewPageMap2.default.productsTablePaginationActive).html(), 10);

      $.ajax(this.router.generate('admin_orders_get_products', { orderId: orderId })).done(function (response) {
        // Delete previous product lines
        $(_OrderViewPageMap2.default.productsTable).find(_OrderViewPageMap2.default.productsTableRows).remove();
        $(_OrderViewPageMap2.default.productsTableCustomizationRows).remove();

        $(_OrderViewPageMap2.default.productsTable + ' tbody').prepend(response);

        $(_OrderViewPageMap2.default.refreshProductsListLoadingSpinner).hide();

        var newNumProducts = $(_OrderViewPageMap2.default.productsTableRows).length;
        var newPagesNum = Math.ceil(newNumProducts / numRowsPerPage);

        _this9.orderProductRenderer.updateNumProducts(newNumProducts);
        _this9.orderProductRenderer.updatePaginationControls();

        var numPage = 1;
        var message = '';

        // Display alert
        if (initialNumProducts > newNumProducts) {
          // product deleted
          message = initialNumProducts - newNumProducts === 1 ? window.translate_javascripts['The product was successfully removed.'] : window.translate_javascripts['[1] products were successfully removed.'].replace('[1]', initialNumProducts - newNumProducts);

          // Set target page to the page of the deleted item
          numPage = newPagesNum === 1 ? 1 : currentPage;
        } else if (initialNumProducts < newNumProducts) {
          // product added
          message = newNumProducts - initialNumProducts === 1 ? window.translate_javascripts['The product was successfully added.'] : window.translate_javascripts['[1] products were successfully added.'].replace('[1]', newNumProducts - initialNumProducts);

          // Move to first page to see the added product
          numPage = 1;
        }

        if (message !== '') {
          $.growl.notice({
            title: '',
            message: message
          });
        }

        // Move to page of the modified item
        _eventEmitter.EventEmitter.emit(_orderViewEventMap2.default.productListPaginated, {
          numPage: numPage
        });

        // Bind hover on product rows buttons
        _this9.resetToolTips();
      }).fail(function () {
        $.growl.error({
          title: '',
          message: 'Failed to reload the products list. Please reload the page'
        });
      });
    }
  }]);
  return OrderViewPage;
}();

exports.default = OrderViewPage;

/***/ }),

/***/ "./node_modules/babel-runtime/core-js/array/from.js":
/*!**********************************************************!*\
  !*** ./node_modules/babel-runtime/core-js/array/from.js ***!
  \**********************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

module.exports = { "default": __webpack_require__(/*! core-js/library/fn/array/from */ "./node_modules/core-js/library/fn/array/from.js"), __esModule: true };

/***/ }),

/***/ "./node_modules/babel-runtime/core-js/get-iterator.js":
/*!************************************************************!*\
  !*** ./node_modules/babel-runtime/core-js/get-iterator.js ***!
  \************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

module.exports = { "default": __webpack_require__(/*! core-js/library/fn/get-iterator */ "./node_modules/core-js/library/fn/get-iterator.js"), __esModule: true };

/***/ }),

/***/ "./node_modules/babel-runtime/core-js/is-iterable.js":
/*!***********************************************************!*\
  !*** ./node_modules/babel-runtime/core-js/is-iterable.js ***!
  \***********************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

module.exports = { "default": __webpack_require__(/*! core-js/library/fn/is-iterable */ "./node_modules/core-js/library/fn/is-iterable.js"), __esModule: true };

/***/ }),

/***/ "./node_modules/babel-runtime/core-js/number/is-nan.js":
/*!*************************************************************!*\
  !*** ./node_modules/babel-runtime/core-js/number/is-nan.js ***!
  \*************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

module.exports = { "default": __webpack_require__(/*! core-js/library/fn/number/is-nan */ "./node_modules/core-js/library/fn/number/is-nan.js"), __esModule: true };

/***/ }),

/***/ "./node_modules/babel-runtime/core-js/object/assign.js":
/*!*************************************************************!*\
  !*** ./node_modules/babel-runtime/core-js/object/assign.js ***!
  \*************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

module.exports = { "default": __webpack_require__(/*! core-js/library/fn/object/assign */ "./node_modules/core-js/library/fn/object/assign.js"), __esModule: true };

/***/ }),

/***/ "./node_modules/babel-runtime/core-js/object/create.js":
/*!*************************************************************!*\
  !*** ./node_modules/babel-runtime/core-js/object/create.js ***!
  \*************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

module.exports = { "default": __webpack_require__(/*! core-js/library/fn/object/create */ "./node_modules/core-js/library/fn/object/create.js"), __esModule: true };

/***/ }),

/***/ "./node_modules/babel-runtime/core-js/object/define-property.js":
/*!**********************************************************************!*\
  !*** ./node_modules/babel-runtime/core-js/object/define-property.js ***!
  \**********************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

module.exports = { "default": __webpack_require__(/*! core-js/library/fn/object/define-property */ "./node_modules/core-js/library/fn/object/define-property.js"), __esModule: true };

/***/ }),

/***/ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js":
/*!***********************************************************************!*\
  !*** ./node_modules/babel-runtime/core-js/object/get-prototype-of.js ***!
  \***********************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

module.exports = { "default": __webpack_require__(/*! core-js/library/fn/object/get-prototype-of */ "./node_modules/core-js/library/fn/object/get-prototype-of.js"), __esModule: true };

/***/ }),

/***/ "./node_modules/babel-runtime/core-js/object/keys.js":
/*!***********************************************************!*\
  !*** ./node_modules/babel-runtime/core-js/object/keys.js ***!
  \***********************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

module.exports = { "default": __webpack_require__(/*! core-js/library/fn/object/keys */ "./node_modules/core-js/library/fn/object/keys.js"), __esModule: true };

/***/ }),

/***/ "./node_modules/babel-runtime/core-js/object/set-prototype-of.js":
/*!***********************************************************************!*\
  !*** ./node_modules/babel-runtime/core-js/object/set-prototype-of.js ***!
  \***********************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

module.exports = { "default": __webpack_require__(/*! core-js/library/fn/object/set-prototype-of */ "./node_modules/core-js/library/fn/object/set-prototype-of.js"), __esModule: true };

/***/ }),

/***/ "./node_modules/babel-runtime/core-js/object/values.js":
/*!*************************************************************!*\
  !*** ./node_modules/babel-runtime/core-js/object/values.js ***!
  \*************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

module.exports = { "default": __webpack_require__(/*! core-js/library/fn/object/values */ "./node_modules/core-js/library/fn/object/values.js"), __esModule: true };

/***/ }),

/***/ "./node_modules/babel-runtime/core-js/symbol.js":
/*!******************************************************!*\
  !*** ./node_modules/babel-runtime/core-js/symbol.js ***!
  \******************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

module.exports = { "default": __webpack_require__(/*! core-js/library/fn/symbol */ "./node_modules/core-js/library/fn/symbol/index.js"), __esModule: true };

/***/ }),

/***/ "./node_modules/babel-runtime/core-js/symbol/iterator.js":
/*!***************************************************************!*\
  !*** ./node_modules/babel-runtime/core-js/symbol/iterator.js ***!
  \***************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

module.exports = { "default": __webpack_require__(/*! core-js/library/fn/symbol/iterator */ "./node_modules/core-js/library/fn/symbol/iterator.js"), __esModule: true };

/***/ }),

/***/ "./node_modules/babel-runtime/helpers/classCallCheck.js":
/*!**************************************************************!*\
  !*** ./node_modules/babel-runtime/helpers/classCallCheck.js ***!
  \**************************************************************/
/***/ ((__unused_webpack_module, exports) => {

"use strict";


exports.__esModule = true;

exports.default = function (instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
};

/***/ }),

/***/ "./node_modules/babel-runtime/helpers/createClass.js":
/*!***********************************************************!*\
  !*** ./node_modules/babel-runtime/helpers/createClass.js ***!
  \***********************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

"use strict";


exports.__esModule = true;

var _defineProperty = __webpack_require__(/*! ../core-js/object/define-property */ "./node_modules/babel-runtime/core-js/object/define-property.js");

var _defineProperty2 = _interopRequireDefault(_defineProperty);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      (0, _defineProperty2.default)(target, descriptor.key, descriptor);
    }
  }

  return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);
    if (staticProps) defineProperties(Constructor, staticProps);
    return Constructor;
  };
}();

/***/ }),

/***/ "./node_modules/babel-runtime/helpers/inherits.js":
/*!********************************************************!*\
  !*** ./node_modules/babel-runtime/helpers/inherits.js ***!
  \********************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

"use strict";


exports.__esModule = true;

var _setPrototypeOf = __webpack_require__(/*! ../core-js/object/set-prototype-of */ "./node_modules/babel-runtime/core-js/object/set-prototype-of.js");

var _setPrototypeOf2 = _interopRequireDefault(_setPrototypeOf);

var _create = __webpack_require__(/*! ../core-js/object/create */ "./node_modules/babel-runtime/core-js/object/create.js");

var _create2 = _interopRequireDefault(_create);

var _typeof2 = __webpack_require__(/*! ../helpers/typeof */ "./node_modules/babel-runtime/helpers/typeof.js");

var _typeof3 = _interopRequireDefault(_typeof2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function, not " + (typeof superClass === "undefined" ? "undefined" : (0, _typeof3.default)(superClass)));
  }

  subClass.prototype = (0, _create2.default)(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      enumerable: false,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf2.default ? (0, _setPrototypeOf2.default)(subClass, superClass) : subClass.__proto__ = superClass;
};

/***/ }),

/***/ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js":
/*!*************************************************************************!*\
  !*** ./node_modules/babel-runtime/helpers/possibleConstructorReturn.js ***!
  \*************************************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

"use strict";


exports.__esModule = true;

var _typeof2 = __webpack_require__(/*! ../helpers/typeof */ "./node_modules/babel-runtime/helpers/typeof.js");

var _typeof3 = _interopRequireDefault(_typeof2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (self, call) {
  if (!self) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return call && ((typeof call === "undefined" ? "undefined" : (0, _typeof3.default)(call)) === "object" || typeof call === "function") ? call : self;
};

/***/ }),

/***/ "./node_modules/babel-runtime/helpers/slicedToArray.js":
/*!*************************************************************!*\
  !*** ./node_modules/babel-runtime/helpers/slicedToArray.js ***!
  \*************************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

"use strict";


exports.__esModule = true;

var _isIterable2 = __webpack_require__(/*! ../core-js/is-iterable */ "./node_modules/babel-runtime/core-js/is-iterable.js");

var _isIterable3 = _interopRequireDefault(_isIterable2);

var _getIterator2 = __webpack_require__(/*! ../core-js/get-iterator */ "./node_modules/babel-runtime/core-js/get-iterator.js");

var _getIterator3 = _interopRequireDefault(_getIterator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function () {
  function sliceIterator(arr, i) {
    var _arr = [];
    var _n = true;
    var _d = false;
    var _e = undefined;

    try {
      for (var _i = (0, _getIterator3.default)(arr), _s; !(_n = (_s = _i.next()).done); _n = true) {
        _arr.push(_s.value);

        if (i && _arr.length === i) break;
      }
    } catch (err) {
      _d = true;
      _e = err;
    } finally {
      try {
        if (!_n && _i["return"]) _i["return"]();
      } finally {
        if (_d) throw _e;
      }
    }

    return _arr;
  }

  return function (arr, i) {
    if (Array.isArray(arr)) {
      return arr;
    } else if ((0, _isIterable3.default)(Object(arr))) {
      return sliceIterator(arr, i);
    } else {
      throw new TypeError("Invalid attempt to destructure non-iterable instance");
    }
  };
}();

/***/ }),

/***/ "./node_modules/babel-runtime/helpers/toConsumableArray.js":
/*!*****************************************************************!*\
  !*** ./node_modules/babel-runtime/helpers/toConsumableArray.js ***!
  \*****************************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

"use strict";


exports.__esModule = true;

var _from = __webpack_require__(/*! ../core-js/array/from */ "./node_modules/babel-runtime/core-js/array/from.js");

var _from2 = _interopRequireDefault(_from);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (arr) {
  if (Array.isArray(arr)) {
    for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) {
      arr2[i] = arr[i];
    }

    return arr2;
  } else {
    return (0, _from2.default)(arr);
  }
};

/***/ }),

/***/ "./node_modules/babel-runtime/helpers/typeof.js":
/*!******************************************************!*\
  !*** ./node_modules/babel-runtime/helpers/typeof.js ***!
  \******************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

"use strict";


exports.__esModule = true;

var _iterator = __webpack_require__(/*! ../core-js/symbol/iterator */ "./node_modules/babel-runtime/core-js/symbol/iterator.js");

var _iterator2 = _interopRequireDefault(_iterator);

var _symbol = __webpack_require__(/*! ../core-js/symbol */ "./node_modules/babel-runtime/core-js/symbol.js");

var _symbol2 = _interopRequireDefault(_symbol);

var _typeof = typeof _symbol2.default === "function" && typeof _iterator2.default === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof _symbol2.default === "function" && obj.constructor === _symbol2.default && obj !== _symbol2.default.prototype ? "symbol" : typeof obj; };

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = typeof _symbol2.default === "function" && _typeof(_iterator2.default) === "symbol" ? function (obj) {
  return typeof obj === "undefined" ? "undefined" : _typeof(obj);
} : function (obj) {
  return obj && typeof _symbol2.default === "function" && obj.constructor === _symbol2.default && obj !== _symbol2.default.prototype ? "symbol" : typeof obj === "undefined" ? "undefined" : _typeof(obj);
};

/***/ }),

/***/ "./node_modules/core-js/library/fn/array/from.js":
/*!*******************************************************!*\
  !*** ./node_modules/core-js/library/fn/array/from.js ***!
  \*******************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

__webpack_require__(/*! ../../modules/es6.string.iterator */ "./node_modules/core-js/library/modules/es6.string.iterator.js");
__webpack_require__(/*! ../../modules/es6.array.from */ "./node_modules/core-js/library/modules/es6.array.from.js");
module.exports = __webpack_require__(/*! ../../modules/_core */ "./node_modules/core-js/library/modules/_core.js").Array.from;


/***/ }),

/***/ "./node_modules/core-js/library/fn/get-iterator.js":
/*!*********************************************************!*\
  !*** ./node_modules/core-js/library/fn/get-iterator.js ***!
  \*********************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

__webpack_require__(/*! ../modules/web.dom.iterable */ "./node_modules/core-js/library/modules/web.dom.iterable.js");
__webpack_require__(/*! ../modules/es6.string.iterator */ "./node_modules/core-js/library/modules/es6.string.iterator.js");
module.exports = __webpack_require__(/*! ../modules/core.get-iterator */ "./node_modules/core-js/library/modules/core.get-iterator.js");


/***/ }),

/***/ "./node_modules/core-js/library/fn/is-iterable.js":
/*!********************************************************!*\
  !*** ./node_modules/core-js/library/fn/is-iterable.js ***!
  \********************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

__webpack_require__(/*! ../modules/web.dom.iterable */ "./node_modules/core-js/library/modules/web.dom.iterable.js");
__webpack_require__(/*! ../modules/es6.string.iterator */ "./node_modules/core-js/library/modules/es6.string.iterator.js");
module.exports = __webpack_require__(/*! ../modules/core.is-iterable */ "./node_modules/core-js/library/modules/core.is-iterable.js");


/***/ }),

/***/ "./node_modules/core-js/library/fn/number/is-nan.js":
/*!**********************************************************!*\
  !*** ./node_modules/core-js/library/fn/number/is-nan.js ***!
  \**********************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

__webpack_require__(/*! ../../modules/es6.number.is-nan */ "./node_modules/core-js/library/modules/es6.number.is-nan.js");
module.exports = __webpack_require__(/*! ../../modules/_core */ "./node_modules/core-js/library/modules/_core.js").Number.isNaN;


/***/ }),

/***/ "./node_modules/core-js/library/fn/object/assign.js":
/*!**********************************************************!*\
  !*** ./node_modules/core-js/library/fn/object/assign.js ***!
  \**********************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

__webpack_require__(/*! ../../modules/es6.object.assign */ "./node_modules/core-js/library/modules/es6.object.assign.js");
module.exports = __webpack_require__(/*! ../../modules/_core */ "./node_modules/core-js/library/modules/_core.js").Object.assign;


/***/ }),

/***/ "./node_modules/core-js/library/fn/object/create.js":
/*!**********************************************************!*\
  !*** ./node_modules/core-js/library/fn/object/create.js ***!
  \**********************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

__webpack_require__(/*! ../../modules/es6.object.create */ "./node_modules/core-js/library/modules/es6.object.create.js");
var $Object = __webpack_require__(/*! ../../modules/_core */ "./node_modules/core-js/library/modules/_core.js").Object;
module.exports = function create(P, D) {
  return $Object.create(P, D);
};


/***/ }),

/***/ "./node_modules/core-js/library/fn/object/define-property.js":
/*!*******************************************************************!*\
  !*** ./node_modules/core-js/library/fn/object/define-property.js ***!
  \*******************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

__webpack_require__(/*! ../../modules/es6.object.define-property */ "./node_modules/core-js/library/modules/es6.object.define-property.js");
var $Object = __webpack_require__(/*! ../../modules/_core */ "./node_modules/core-js/library/modules/_core.js").Object;
module.exports = function defineProperty(it, key, desc) {
  return $Object.defineProperty(it, key, desc);
};


/***/ }),

/***/ "./node_modules/core-js/library/fn/object/get-prototype-of.js":
/*!********************************************************************!*\
  !*** ./node_modules/core-js/library/fn/object/get-prototype-of.js ***!
  \********************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

__webpack_require__(/*! ../../modules/es6.object.get-prototype-of */ "./node_modules/core-js/library/modules/es6.object.get-prototype-of.js");
module.exports = __webpack_require__(/*! ../../modules/_core */ "./node_modules/core-js/library/modules/_core.js").Object.getPrototypeOf;


/***/ }),

/***/ "./node_modules/core-js/library/fn/object/keys.js":
/*!********************************************************!*\
  !*** ./node_modules/core-js/library/fn/object/keys.js ***!
  \********************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

__webpack_require__(/*! ../../modules/es6.object.keys */ "./node_modules/core-js/library/modules/es6.object.keys.js");
module.exports = __webpack_require__(/*! ../../modules/_core */ "./node_modules/core-js/library/modules/_core.js").Object.keys;


/***/ }),

/***/ "./node_modules/core-js/library/fn/object/set-prototype-of.js":
/*!********************************************************************!*\
  !*** ./node_modules/core-js/library/fn/object/set-prototype-of.js ***!
  \********************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

__webpack_require__(/*! ../../modules/es6.object.set-prototype-of */ "./node_modules/core-js/library/modules/es6.object.set-prototype-of.js");
module.exports = __webpack_require__(/*! ../../modules/_core */ "./node_modules/core-js/library/modules/_core.js").Object.setPrototypeOf;


/***/ }),

/***/ "./node_modules/core-js/library/fn/object/values.js":
/*!**********************************************************!*\
  !*** ./node_modules/core-js/library/fn/object/values.js ***!
  \**********************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

__webpack_require__(/*! ../../modules/es7.object.values */ "./node_modules/core-js/library/modules/es7.object.values.js");
module.exports = __webpack_require__(/*! ../../modules/_core */ "./node_modules/core-js/library/modules/_core.js").Object.values;


/***/ }),

/***/ "./node_modules/core-js/library/fn/symbol/index.js":
/*!*********************************************************!*\
  !*** ./node_modules/core-js/library/fn/symbol/index.js ***!
  \*********************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

__webpack_require__(/*! ../../modules/es6.symbol */ "./node_modules/core-js/library/modules/es6.symbol.js");
__webpack_require__(/*! ../../modules/es6.object.to-string */ "./node_modules/core-js/library/modules/es6.object.to-string.js");
__webpack_require__(/*! ../../modules/es7.symbol.async-iterator */ "./node_modules/core-js/library/modules/es7.symbol.async-iterator.js");
__webpack_require__(/*! ../../modules/es7.symbol.observable */ "./node_modules/core-js/library/modules/es7.symbol.observable.js");
module.exports = __webpack_require__(/*! ../../modules/_core */ "./node_modules/core-js/library/modules/_core.js").Symbol;


/***/ }),

/***/ "./node_modules/core-js/library/fn/symbol/iterator.js":
/*!************************************************************!*\
  !*** ./node_modules/core-js/library/fn/symbol/iterator.js ***!
  \************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

__webpack_require__(/*! ../../modules/es6.string.iterator */ "./node_modules/core-js/library/modules/es6.string.iterator.js");
__webpack_require__(/*! ../../modules/web.dom.iterable */ "./node_modules/core-js/library/modules/web.dom.iterable.js");
module.exports = __webpack_require__(/*! ../../modules/_wks-ext */ "./node_modules/core-js/library/modules/_wks-ext.js").f('iterator');


/***/ }),

/***/ "./node_modules/core-js/library/modules/_a-function.js":
/*!*************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_a-function.js ***!
  \*************************************************************/
/***/ ((module) => {

module.exports = function (it) {
  if (typeof it != 'function') throw TypeError(it + ' is not a function!');
  return it;
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_add-to-unscopables.js":
/*!*********************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_add-to-unscopables.js ***!
  \*********************************************************************/
/***/ ((module) => {

module.exports = function () { /* empty */ };


/***/ }),

/***/ "./node_modules/core-js/library/modules/_an-object.js":
/*!************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_an-object.js ***!
  \************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var isObject = __webpack_require__(/*! ./_is-object */ "./node_modules/core-js/library/modules/_is-object.js");
module.exports = function (it) {
  if (!isObject(it)) throw TypeError(it + ' is not an object!');
  return it;
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_array-includes.js":
/*!*****************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_array-includes.js ***!
  \*****************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// false -> Array#indexOf
// true  -> Array#includes
var toIObject = __webpack_require__(/*! ./_to-iobject */ "./node_modules/core-js/library/modules/_to-iobject.js");
var toLength = __webpack_require__(/*! ./_to-length */ "./node_modules/core-js/library/modules/_to-length.js");
var toAbsoluteIndex = __webpack_require__(/*! ./_to-absolute-index */ "./node_modules/core-js/library/modules/_to-absolute-index.js");
module.exports = function (IS_INCLUDES) {
  return function ($this, el, fromIndex) {
    var O = toIObject($this);
    var length = toLength(O.length);
    var index = toAbsoluteIndex(fromIndex, length);
    var value;
    // Array#includes uses SameValueZero equality algorithm
    // eslint-disable-next-line no-self-compare
    if (IS_INCLUDES && el != el) while (length > index) {
      value = O[index++];
      // eslint-disable-next-line no-self-compare
      if (value != value) return true;
    // Array#indexOf ignores holes, Array#includes - not
    } else for (;length > index; index++) if (IS_INCLUDES || index in O) {
      if (O[index] === el) return IS_INCLUDES || index || 0;
    } return !IS_INCLUDES && -1;
  };
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_classof.js":
/*!**********************************************************!*\
  !*** ./node_modules/core-js/library/modules/_classof.js ***!
  \**********************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// getting tag from 19.1.3.6 Object.prototype.toString()
var cof = __webpack_require__(/*! ./_cof */ "./node_modules/core-js/library/modules/_cof.js");
var TAG = __webpack_require__(/*! ./_wks */ "./node_modules/core-js/library/modules/_wks.js")('toStringTag');
// ES3 wrong here
var ARG = cof(function () { return arguments; }()) == 'Arguments';

// fallback for IE11 Script Access Denied error
var tryGet = function (it, key) {
  try {
    return it[key];
  } catch (e) { /* empty */ }
};

module.exports = function (it) {
  var O, T, B;
  return it === undefined ? 'Undefined' : it === null ? 'Null'
    // @@toStringTag case
    : typeof (T = tryGet(O = Object(it), TAG)) == 'string' ? T
    // builtinTag case
    : ARG ? cof(O)
    // ES3 arguments fallback
    : (B = cof(O)) == 'Object' && typeof O.callee == 'function' ? 'Arguments' : B;
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_cof.js":
/*!******************************************************!*\
  !*** ./node_modules/core-js/library/modules/_cof.js ***!
  \******************************************************/
/***/ ((module) => {

var toString = {}.toString;

module.exports = function (it) {
  return toString.call(it).slice(8, -1);
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_core.js":
/*!*******************************************************!*\
  !*** ./node_modules/core-js/library/modules/_core.js ***!
  \*******************************************************/
/***/ ((module) => {

var core = module.exports = { version: '2.6.11' };
if (typeof __e == 'number') __e = core; // eslint-disable-line no-undef


/***/ }),

/***/ "./node_modules/core-js/library/modules/_create-property.js":
/*!******************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_create-property.js ***!
  \******************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";

var $defineProperty = __webpack_require__(/*! ./_object-dp */ "./node_modules/core-js/library/modules/_object-dp.js");
var createDesc = __webpack_require__(/*! ./_property-desc */ "./node_modules/core-js/library/modules/_property-desc.js");

module.exports = function (object, index, value) {
  if (index in object) $defineProperty.f(object, index, createDesc(0, value));
  else object[index] = value;
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_ctx.js":
/*!******************************************************!*\
  !*** ./node_modules/core-js/library/modules/_ctx.js ***!
  \******************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// optional / simple context binding
var aFunction = __webpack_require__(/*! ./_a-function */ "./node_modules/core-js/library/modules/_a-function.js");
module.exports = function (fn, that, length) {
  aFunction(fn);
  if (that === undefined) return fn;
  switch (length) {
    case 1: return function (a) {
      return fn.call(that, a);
    };
    case 2: return function (a, b) {
      return fn.call(that, a, b);
    };
    case 3: return function (a, b, c) {
      return fn.call(that, a, b, c);
    };
  }
  return function (/* ...args */) {
    return fn.apply(that, arguments);
  };
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_defined.js":
/*!**********************************************************!*\
  !*** ./node_modules/core-js/library/modules/_defined.js ***!
  \**********************************************************/
/***/ ((module) => {

// 7.2.1 RequireObjectCoercible(argument)
module.exports = function (it) {
  if (it == undefined) throw TypeError("Can't call method on  " + it);
  return it;
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_descriptors.js":
/*!**************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_descriptors.js ***!
  \**************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// Thank's IE8 for his funny defineProperty
module.exports = !__webpack_require__(/*! ./_fails */ "./node_modules/core-js/library/modules/_fails.js")(function () {
  return Object.defineProperty({}, 'a', { get: function () { return 7; } }).a != 7;
});


/***/ }),

/***/ "./node_modules/core-js/library/modules/_dom-create.js":
/*!*************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_dom-create.js ***!
  \*************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var isObject = __webpack_require__(/*! ./_is-object */ "./node_modules/core-js/library/modules/_is-object.js");
var document = __webpack_require__(/*! ./_global */ "./node_modules/core-js/library/modules/_global.js").document;
// typeof document.createElement is 'object' in old IE
var is = isObject(document) && isObject(document.createElement);
module.exports = function (it) {
  return is ? document.createElement(it) : {};
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_enum-bug-keys.js":
/*!****************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_enum-bug-keys.js ***!
  \****************************************************************/
/***/ ((module) => {

// IE 8- don't enum bug keys
module.exports = (
  'constructor,hasOwnProperty,isPrototypeOf,propertyIsEnumerable,toLocaleString,toString,valueOf'
).split(',');


/***/ }),

/***/ "./node_modules/core-js/library/modules/_enum-keys.js":
/*!************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_enum-keys.js ***!
  \************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// all enumerable object keys, includes symbols
var getKeys = __webpack_require__(/*! ./_object-keys */ "./node_modules/core-js/library/modules/_object-keys.js");
var gOPS = __webpack_require__(/*! ./_object-gops */ "./node_modules/core-js/library/modules/_object-gops.js");
var pIE = __webpack_require__(/*! ./_object-pie */ "./node_modules/core-js/library/modules/_object-pie.js");
module.exports = function (it) {
  var result = getKeys(it);
  var getSymbols = gOPS.f;
  if (getSymbols) {
    var symbols = getSymbols(it);
    var isEnum = pIE.f;
    var i = 0;
    var key;
    while (symbols.length > i) if (isEnum.call(it, key = symbols[i++])) result.push(key);
  } return result;
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_export.js":
/*!*********************************************************!*\
  !*** ./node_modules/core-js/library/modules/_export.js ***!
  \*********************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var global = __webpack_require__(/*! ./_global */ "./node_modules/core-js/library/modules/_global.js");
var core = __webpack_require__(/*! ./_core */ "./node_modules/core-js/library/modules/_core.js");
var ctx = __webpack_require__(/*! ./_ctx */ "./node_modules/core-js/library/modules/_ctx.js");
var hide = __webpack_require__(/*! ./_hide */ "./node_modules/core-js/library/modules/_hide.js");
var has = __webpack_require__(/*! ./_has */ "./node_modules/core-js/library/modules/_has.js");
var PROTOTYPE = 'prototype';

var $export = function (type, name, source) {
  var IS_FORCED = type & $export.F;
  var IS_GLOBAL = type & $export.G;
  var IS_STATIC = type & $export.S;
  var IS_PROTO = type & $export.P;
  var IS_BIND = type & $export.B;
  var IS_WRAP = type & $export.W;
  var exports = IS_GLOBAL ? core : core[name] || (core[name] = {});
  var expProto = exports[PROTOTYPE];
  var target = IS_GLOBAL ? global : IS_STATIC ? global[name] : (global[name] || {})[PROTOTYPE];
  var key, own, out;
  if (IS_GLOBAL) source = name;
  for (key in source) {
    // contains in native
    own = !IS_FORCED && target && target[key] !== undefined;
    if (own && has(exports, key)) continue;
    // export native or passed
    out = own ? target[key] : source[key];
    // prevent global pollution for namespaces
    exports[key] = IS_GLOBAL && typeof target[key] != 'function' ? source[key]
    // bind timers to global for call from export context
    : IS_BIND && own ? ctx(out, global)
    // wrap global constructors for prevent change them in library
    : IS_WRAP && target[key] == out ? (function (C) {
      var F = function (a, b, c) {
        if (this instanceof C) {
          switch (arguments.length) {
            case 0: return new C();
            case 1: return new C(a);
            case 2: return new C(a, b);
          } return new C(a, b, c);
        } return C.apply(this, arguments);
      };
      F[PROTOTYPE] = C[PROTOTYPE];
      return F;
    // make static versions for prototype methods
    })(out) : IS_PROTO && typeof out == 'function' ? ctx(Function.call, out) : out;
    // export proto methods to core.%CONSTRUCTOR%.methods.%NAME%
    if (IS_PROTO) {
      (exports.virtual || (exports.virtual = {}))[key] = out;
      // export proto methods to core.%CONSTRUCTOR%.prototype.%NAME%
      if (type & $export.R && expProto && !expProto[key]) hide(expProto, key, out);
    }
  }
};
// type bitmap
$export.F = 1;   // forced
$export.G = 2;   // global
$export.S = 4;   // static
$export.P = 8;   // proto
$export.B = 16;  // bind
$export.W = 32;  // wrap
$export.U = 64;  // safe
$export.R = 128; // real proto method for `library`
module.exports = $export;


/***/ }),

/***/ "./node_modules/core-js/library/modules/_fails.js":
/*!********************************************************!*\
  !*** ./node_modules/core-js/library/modules/_fails.js ***!
  \********************************************************/
/***/ ((module) => {

module.exports = function (exec) {
  try {
    return !!exec();
  } catch (e) {
    return true;
  }
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_global.js":
/*!*********************************************************!*\
  !*** ./node_modules/core-js/library/modules/_global.js ***!
  \*********************************************************/
/***/ ((module) => {

// https://github.com/zloirock/core-js/issues/86#issuecomment-115759028
var global = module.exports = typeof window != 'undefined' && window.Math == Math
  ? window : typeof self != 'undefined' && self.Math == Math ? self
  // eslint-disable-next-line no-new-func
  : Function('return this')();
if (typeof __g == 'number') __g = global; // eslint-disable-line no-undef


/***/ }),

/***/ "./node_modules/core-js/library/modules/_has.js":
/*!******************************************************!*\
  !*** ./node_modules/core-js/library/modules/_has.js ***!
  \******************************************************/
/***/ ((module) => {

var hasOwnProperty = {}.hasOwnProperty;
module.exports = function (it, key) {
  return hasOwnProperty.call(it, key);
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_hide.js":
/*!*******************************************************!*\
  !*** ./node_modules/core-js/library/modules/_hide.js ***!
  \*******************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var dP = __webpack_require__(/*! ./_object-dp */ "./node_modules/core-js/library/modules/_object-dp.js");
var createDesc = __webpack_require__(/*! ./_property-desc */ "./node_modules/core-js/library/modules/_property-desc.js");
module.exports = __webpack_require__(/*! ./_descriptors */ "./node_modules/core-js/library/modules/_descriptors.js") ? function (object, key, value) {
  return dP.f(object, key, createDesc(1, value));
} : function (object, key, value) {
  object[key] = value;
  return object;
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_html.js":
/*!*******************************************************!*\
  !*** ./node_modules/core-js/library/modules/_html.js ***!
  \*******************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var document = __webpack_require__(/*! ./_global */ "./node_modules/core-js/library/modules/_global.js").document;
module.exports = document && document.documentElement;


/***/ }),

/***/ "./node_modules/core-js/library/modules/_ie8-dom-define.js":
/*!*****************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_ie8-dom-define.js ***!
  \*****************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

module.exports = !__webpack_require__(/*! ./_descriptors */ "./node_modules/core-js/library/modules/_descriptors.js") && !__webpack_require__(/*! ./_fails */ "./node_modules/core-js/library/modules/_fails.js")(function () {
  return Object.defineProperty(__webpack_require__(/*! ./_dom-create */ "./node_modules/core-js/library/modules/_dom-create.js")('div'), 'a', { get: function () { return 7; } }).a != 7;
});


/***/ }),

/***/ "./node_modules/core-js/library/modules/_iobject.js":
/*!**********************************************************!*\
  !*** ./node_modules/core-js/library/modules/_iobject.js ***!
  \**********************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// fallback for non-array-like ES3 and non-enumerable old V8 strings
var cof = __webpack_require__(/*! ./_cof */ "./node_modules/core-js/library/modules/_cof.js");
// eslint-disable-next-line no-prototype-builtins
module.exports = Object('z').propertyIsEnumerable(0) ? Object : function (it) {
  return cof(it) == 'String' ? it.split('') : Object(it);
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_is-array-iter.js":
/*!****************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_is-array-iter.js ***!
  \****************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// check on default Array iterator
var Iterators = __webpack_require__(/*! ./_iterators */ "./node_modules/core-js/library/modules/_iterators.js");
var ITERATOR = __webpack_require__(/*! ./_wks */ "./node_modules/core-js/library/modules/_wks.js")('iterator');
var ArrayProto = Array.prototype;

module.exports = function (it) {
  return it !== undefined && (Iterators.Array === it || ArrayProto[ITERATOR] === it);
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_is-array.js":
/*!***********************************************************!*\
  !*** ./node_modules/core-js/library/modules/_is-array.js ***!
  \***********************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// 7.2.2 IsArray(argument)
var cof = __webpack_require__(/*! ./_cof */ "./node_modules/core-js/library/modules/_cof.js");
module.exports = Array.isArray || function isArray(arg) {
  return cof(arg) == 'Array';
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_is-object.js":
/*!************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_is-object.js ***!
  \************************************************************/
/***/ ((module) => {

module.exports = function (it) {
  return typeof it === 'object' ? it !== null : typeof it === 'function';
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_iter-call.js":
/*!************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_iter-call.js ***!
  \************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// call something on iterator step with safe closing on error
var anObject = __webpack_require__(/*! ./_an-object */ "./node_modules/core-js/library/modules/_an-object.js");
module.exports = function (iterator, fn, value, entries) {
  try {
    return entries ? fn(anObject(value)[0], value[1]) : fn(value);
  // 7.4.6 IteratorClose(iterator, completion)
  } catch (e) {
    var ret = iterator['return'];
    if (ret !== undefined) anObject(ret.call(iterator));
    throw e;
  }
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_iter-create.js":
/*!**************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_iter-create.js ***!
  \**************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";

var create = __webpack_require__(/*! ./_object-create */ "./node_modules/core-js/library/modules/_object-create.js");
var descriptor = __webpack_require__(/*! ./_property-desc */ "./node_modules/core-js/library/modules/_property-desc.js");
var setToStringTag = __webpack_require__(/*! ./_set-to-string-tag */ "./node_modules/core-js/library/modules/_set-to-string-tag.js");
var IteratorPrototype = {};

// 25.1.2.1.1 %IteratorPrototype%[@@iterator]()
__webpack_require__(/*! ./_hide */ "./node_modules/core-js/library/modules/_hide.js")(IteratorPrototype, __webpack_require__(/*! ./_wks */ "./node_modules/core-js/library/modules/_wks.js")('iterator'), function () { return this; });

module.exports = function (Constructor, NAME, next) {
  Constructor.prototype = create(IteratorPrototype, { next: descriptor(1, next) });
  setToStringTag(Constructor, NAME + ' Iterator');
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_iter-define.js":
/*!**************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_iter-define.js ***!
  \**************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";

var LIBRARY = __webpack_require__(/*! ./_library */ "./node_modules/core-js/library/modules/_library.js");
var $export = __webpack_require__(/*! ./_export */ "./node_modules/core-js/library/modules/_export.js");
var redefine = __webpack_require__(/*! ./_redefine */ "./node_modules/core-js/library/modules/_redefine.js");
var hide = __webpack_require__(/*! ./_hide */ "./node_modules/core-js/library/modules/_hide.js");
var Iterators = __webpack_require__(/*! ./_iterators */ "./node_modules/core-js/library/modules/_iterators.js");
var $iterCreate = __webpack_require__(/*! ./_iter-create */ "./node_modules/core-js/library/modules/_iter-create.js");
var setToStringTag = __webpack_require__(/*! ./_set-to-string-tag */ "./node_modules/core-js/library/modules/_set-to-string-tag.js");
var getPrototypeOf = __webpack_require__(/*! ./_object-gpo */ "./node_modules/core-js/library/modules/_object-gpo.js");
var ITERATOR = __webpack_require__(/*! ./_wks */ "./node_modules/core-js/library/modules/_wks.js")('iterator');
var BUGGY = !([].keys && 'next' in [].keys()); // Safari has buggy iterators w/o `next`
var FF_ITERATOR = '@@iterator';
var KEYS = 'keys';
var VALUES = 'values';

var returnThis = function () { return this; };

module.exports = function (Base, NAME, Constructor, next, DEFAULT, IS_SET, FORCED) {
  $iterCreate(Constructor, NAME, next);
  var getMethod = function (kind) {
    if (!BUGGY && kind in proto) return proto[kind];
    switch (kind) {
      case KEYS: return function keys() { return new Constructor(this, kind); };
      case VALUES: return function values() { return new Constructor(this, kind); };
    } return function entries() { return new Constructor(this, kind); };
  };
  var TAG = NAME + ' Iterator';
  var DEF_VALUES = DEFAULT == VALUES;
  var VALUES_BUG = false;
  var proto = Base.prototype;
  var $native = proto[ITERATOR] || proto[FF_ITERATOR] || DEFAULT && proto[DEFAULT];
  var $default = $native || getMethod(DEFAULT);
  var $entries = DEFAULT ? !DEF_VALUES ? $default : getMethod('entries') : undefined;
  var $anyNative = NAME == 'Array' ? proto.entries || $native : $native;
  var methods, key, IteratorPrototype;
  // Fix native
  if ($anyNative) {
    IteratorPrototype = getPrototypeOf($anyNative.call(new Base()));
    if (IteratorPrototype !== Object.prototype && IteratorPrototype.next) {
      // Set @@toStringTag to native iterators
      setToStringTag(IteratorPrototype, TAG, true);
      // fix for some old engines
      if (!LIBRARY && typeof IteratorPrototype[ITERATOR] != 'function') hide(IteratorPrototype, ITERATOR, returnThis);
    }
  }
  // fix Array#{values, @@iterator}.name in V8 / FF
  if (DEF_VALUES && $native && $native.name !== VALUES) {
    VALUES_BUG = true;
    $default = function values() { return $native.call(this); };
  }
  // Define iterator
  if ((!LIBRARY || FORCED) && (BUGGY || VALUES_BUG || !proto[ITERATOR])) {
    hide(proto, ITERATOR, $default);
  }
  // Plug for library
  Iterators[NAME] = $default;
  Iterators[TAG] = returnThis;
  if (DEFAULT) {
    methods = {
      values: DEF_VALUES ? $default : getMethod(VALUES),
      keys: IS_SET ? $default : getMethod(KEYS),
      entries: $entries
    };
    if (FORCED) for (key in methods) {
      if (!(key in proto)) redefine(proto, key, methods[key]);
    } else $export($export.P + $export.F * (BUGGY || VALUES_BUG), NAME, methods);
  }
  return methods;
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_iter-detect.js":
/*!**************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_iter-detect.js ***!
  \**************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var ITERATOR = __webpack_require__(/*! ./_wks */ "./node_modules/core-js/library/modules/_wks.js")('iterator');
var SAFE_CLOSING = false;

try {
  var riter = [7][ITERATOR]();
  riter['return'] = function () { SAFE_CLOSING = true; };
  // eslint-disable-next-line no-throw-literal
  Array.from(riter, function () { throw 2; });
} catch (e) { /* empty */ }

module.exports = function (exec, skipClosing) {
  if (!skipClosing && !SAFE_CLOSING) return false;
  var safe = false;
  try {
    var arr = [7];
    var iter = arr[ITERATOR]();
    iter.next = function () { return { done: safe = true }; };
    arr[ITERATOR] = function () { return iter; };
    exec(arr);
  } catch (e) { /* empty */ }
  return safe;
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_iter-step.js":
/*!************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_iter-step.js ***!
  \************************************************************/
/***/ ((module) => {

module.exports = function (done, value) {
  return { value: value, done: !!done };
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_iterators.js":
/*!************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_iterators.js ***!
  \************************************************************/
/***/ ((module) => {

module.exports = {};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_library.js":
/*!**********************************************************!*\
  !*** ./node_modules/core-js/library/modules/_library.js ***!
  \**********************************************************/
/***/ ((module) => {

module.exports = true;


/***/ }),

/***/ "./node_modules/core-js/library/modules/_meta.js":
/*!*******************************************************!*\
  !*** ./node_modules/core-js/library/modules/_meta.js ***!
  \*******************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var META = __webpack_require__(/*! ./_uid */ "./node_modules/core-js/library/modules/_uid.js")('meta');
var isObject = __webpack_require__(/*! ./_is-object */ "./node_modules/core-js/library/modules/_is-object.js");
var has = __webpack_require__(/*! ./_has */ "./node_modules/core-js/library/modules/_has.js");
var setDesc = __webpack_require__(/*! ./_object-dp */ "./node_modules/core-js/library/modules/_object-dp.js").f;
var id = 0;
var isExtensible = Object.isExtensible || function () {
  return true;
};
var FREEZE = !__webpack_require__(/*! ./_fails */ "./node_modules/core-js/library/modules/_fails.js")(function () {
  return isExtensible(Object.preventExtensions({}));
});
var setMeta = function (it) {
  setDesc(it, META, { value: {
    i: 'O' + ++id, // object ID
    w: {}          // weak collections IDs
  } });
};
var fastKey = function (it, create) {
  // return primitive with prefix
  if (!isObject(it)) return typeof it == 'symbol' ? it : (typeof it == 'string' ? 'S' : 'P') + it;
  if (!has(it, META)) {
    // can't set metadata to uncaught frozen object
    if (!isExtensible(it)) return 'F';
    // not necessary to add metadata
    if (!create) return 'E';
    // add missing metadata
    setMeta(it);
  // return object ID
  } return it[META].i;
};
var getWeak = function (it, create) {
  if (!has(it, META)) {
    // can't set metadata to uncaught frozen object
    if (!isExtensible(it)) return true;
    // not necessary to add metadata
    if (!create) return false;
    // add missing metadata
    setMeta(it);
  // return hash weak collections IDs
  } return it[META].w;
};
// add metadata on freeze-family methods calling
var onFreeze = function (it) {
  if (FREEZE && meta.NEED && isExtensible(it) && !has(it, META)) setMeta(it);
  return it;
};
var meta = module.exports = {
  KEY: META,
  NEED: false,
  fastKey: fastKey,
  getWeak: getWeak,
  onFreeze: onFreeze
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_object-assign.js":
/*!****************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_object-assign.js ***!
  \****************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";

// 19.1.2.1 Object.assign(target, source, ...)
var DESCRIPTORS = __webpack_require__(/*! ./_descriptors */ "./node_modules/core-js/library/modules/_descriptors.js");
var getKeys = __webpack_require__(/*! ./_object-keys */ "./node_modules/core-js/library/modules/_object-keys.js");
var gOPS = __webpack_require__(/*! ./_object-gops */ "./node_modules/core-js/library/modules/_object-gops.js");
var pIE = __webpack_require__(/*! ./_object-pie */ "./node_modules/core-js/library/modules/_object-pie.js");
var toObject = __webpack_require__(/*! ./_to-object */ "./node_modules/core-js/library/modules/_to-object.js");
var IObject = __webpack_require__(/*! ./_iobject */ "./node_modules/core-js/library/modules/_iobject.js");
var $assign = Object.assign;

// should work with symbols and should have deterministic property order (V8 bug)
module.exports = !$assign || __webpack_require__(/*! ./_fails */ "./node_modules/core-js/library/modules/_fails.js")(function () {
  var A = {};
  var B = {};
  // eslint-disable-next-line no-undef
  var S = Symbol();
  var K = 'abcdefghijklmnopqrst';
  A[S] = 7;
  K.split('').forEach(function (k) { B[k] = k; });
  return $assign({}, A)[S] != 7 || Object.keys($assign({}, B)).join('') != K;
}) ? function assign(target, source) { // eslint-disable-line no-unused-vars
  var T = toObject(target);
  var aLen = arguments.length;
  var index = 1;
  var getSymbols = gOPS.f;
  var isEnum = pIE.f;
  while (aLen > index) {
    var S = IObject(arguments[index++]);
    var keys = getSymbols ? getKeys(S).concat(getSymbols(S)) : getKeys(S);
    var length = keys.length;
    var j = 0;
    var key;
    while (length > j) {
      key = keys[j++];
      if (!DESCRIPTORS || isEnum.call(S, key)) T[key] = S[key];
    }
  } return T;
} : $assign;


/***/ }),

/***/ "./node_modules/core-js/library/modules/_object-create.js":
/*!****************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_object-create.js ***!
  \****************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// 19.1.2.2 / 15.2.3.5 Object.create(O [, Properties])
var anObject = __webpack_require__(/*! ./_an-object */ "./node_modules/core-js/library/modules/_an-object.js");
var dPs = __webpack_require__(/*! ./_object-dps */ "./node_modules/core-js/library/modules/_object-dps.js");
var enumBugKeys = __webpack_require__(/*! ./_enum-bug-keys */ "./node_modules/core-js/library/modules/_enum-bug-keys.js");
var IE_PROTO = __webpack_require__(/*! ./_shared-key */ "./node_modules/core-js/library/modules/_shared-key.js")('IE_PROTO');
var Empty = function () { /* empty */ };
var PROTOTYPE = 'prototype';

// Create object with fake `null` prototype: use iframe Object with cleared prototype
var createDict = function () {
  // Thrash, waste and sodomy: IE GC bug
  var iframe = __webpack_require__(/*! ./_dom-create */ "./node_modules/core-js/library/modules/_dom-create.js")('iframe');
  var i = enumBugKeys.length;
  var lt = '<';
  var gt = '>';
  var iframeDocument;
  iframe.style.display = 'none';
  __webpack_require__(/*! ./_html */ "./node_modules/core-js/library/modules/_html.js").appendChild(iframe);
  iframe.src = 'javascript:'; // eslint-disable-line no-script-url
  // createDict = iframe.contentWindow.Object;
  // html.removeChild(iframe);
  iframeDocument = iframe.contentWindow.document;
  iframeDocument.open();
  iframeDocument.write(lt + 'script' + gt + 'document.F=Object' + lt + '/script' + gt);
  iframeDocument.close();
  createDict = iframeDocument.F;
  while (i--) delete createDict[PROTOTYPE][enumBugKeys[i]];
  return createDict();
};

module.exports = Object.create || function create(O, Properties) {
  var result;
  if (O !== null) {
    Empty[PROTOTYPE] = anObject(O);
    result = new Empty();
    Empty[PROTOTYPE] = null;
    // add "__proto__" for Object.getPrototypeOf polyfill
    result[IE_PROTO] = O;
  } else result = createDict();
  return Properties === undefined ? result : dPs(result, Properties);
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_object-dp.js":
/*!************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_object-dp.js ***!
  \************************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

var anObject = __webpack_require__(/*! ./_an-object */ "./node_modules/core-js/library/modules/_an-object.js");
var IE8_DOM_DEFINE = __webpack_require__(/*! ./_ie8-dom-define */ "./node_modules/core-js/library/modules/_ie8-dom-define.js");
var toPrimitive = __webpack_require__(/*! ./_to-primitive */ "./node_modules/core-js/library/modules/_to-primitive.js");
var dP = Object.defineProperty;

exports.f = __webpack_require__(/*! ./_descriptors */ "./node_modules/core-js/library/modules/_descriptors.js") ? Object.defineProperty : function defineProperty(O, P, Attributes) {
  anObject(O);
  P = toPrimitive(P, true);
  anObject(Attributes);
  if (IE8_DOM_DEFINE) try {
    return dP(O, P, Attributes);
  } catch (e) { /* empty */ }
  if ('get' in Attributes || 'set' in Attributes) throw TypeError('Accessors not supported!');
  if ('value' in Attributes) O[P] = Attributes.value;
  return O;
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_object-dps.js":
/*!*************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_object-dps.js ***!
  \*************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var dP = __webpack_require__(/*! ./_object-dp */ "./node_modules/core-js/library/modules/_object-dp.js");
var anObject = __webpack_require__(/*! ./_an-object */ "./node_modules/core-js/library/modules/_an-object.js");
var getKeys = __webpack_require__(/*! ./_object-keys */ "./node_modules/core-js/library/modules/_object-keys.js");

module.exports = __webpack_require__(/*! ./_descriptors */ "./node_modules/core-js/library/modules/_descriptors.js") ? Object.defineProperties : function defineProperties(O, Properties) {
  anObject(O);
  var keys = getKeys(Properties);
  var length = keys.length;
  var i = 0;
  var P;
  while (length > i) dP.f(O, P = keys[i++], Properties[P]);
  return O;
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_object-gopd.js":
/*!**************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_object-gopd.js ***!
  \**************************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

var pIE = __webpack_require__(/*! ./_object-pie */ "./node_modules/core-js/library/modules/_object-pie.js");
var createDesc = __webpack_require__(/*! ./_property-desc */ "./node_modules/core-js/library/modules/_property-desc.js");
var toIObject = __webpack_require__(/*! ./_to-iobject */ "./node_modules/core-js/library/modules/_to-iobject.js");
var toPrimitive = __webpack_require__(/*! ./_to-primitive */ "./node_modules/core-js/library/modules/_to-primitive.js");
var has = __webpack_require__(/*! ./_has */ "./node_modules/core-js/library/modules/_has.js");
var IE8_DOM_DEFINE = __webpack_require__(/*! ./_ie8-dom-define */ "./node_modules/core-js/library/modules/_ie8-dom-define.js");
var gOPD = Object.getOwnPropertyDescriptor;

exports.f = __webpack_require__(/*! ./_descriptors */ "./node_modules/core-js/library/modules/_descriptors.js") ? gOPD : function getOwnPropertyDescriptor(O, P) {
  O = toIObject(O);
  P = toPrimitive(P, true);
  if (IE8_DOM_DEFINE) try {
    return gOPD(O, P);
  } catch (e) { /* empty */ }
  if (has(O, P)) return createDesc(!pIE.f.call(O, P), O[P]);
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_object-gopn-ext.js":
/*!******************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_object-gopn-ext.js ***!
  \******************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// fallback for IE11 buggy Object.getOwnPropertyNames with iframe and window
var toIObject = __webpack_require__(/*! ./_to-iobject */ "./node_modules/core-js/library/modules/_to-iobject.js");
var gOPN = __webpack_require__(/*! ./_object-gopn */ "./node_modules/core-js/library/modules/_object-gopn.js").f;
var toString = {}.toString;

var windowNames = typeof window == 'object' && window && Object.getOwnPropertyNames
  ? Object.getOwnPropertyNames(window) : [];

var getWindowNames = function (it) {
  try {
    return gOPN(it);
  } catch (e) {
    return windowNames.slice();
  }
};

module.exports.f = function getOwnPropertyNames(it) {
  return windowNames && toString.call(it) == '[object Window]' ? getWindowNames(it) : gOPN(toIObject(it));
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_object-gopn.js":
/*!**************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_object-gopn.js ***!
  \**************************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

// 19.1.2.7 / 15.2.3.4 Object.getOwnPropertyNames(O)
var $keys = __webpack_require__(/*! ./_object-keys-internal */ "./node_modules/core-js/library/modules/_object-keys-internal.js");
var hiddenKeys = __webpack_require__(/*! ./_enum-bug-keys */ "./node_modules/core-js/library/modules/_enum-bug-keys.js").concat('length', 'prototype');

exports.f = Object.getOwnPropertyNames || function getOwnPropertyNames(O) {
  return $keys(O, hiddenKeys);
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_object-gops.js":
/*!**************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_object-gops.js ***!
  \**************************************************************/
/***/ ((__unused_webpack_module, exports) => {

exports.f = Object.getOwnPropertySymbols;


/***/ }),

/***/ "./node_modules/core-js/library/modules/_object-gpo.js":
/*!*************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_object-gpo.js ***!
  \*************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// 19.1.2.9 / 15.2.3.2 Object.getPrototypeOf(O)
var has = __webpack_require__(/*! ./_has */ "./node_modules/core-js/library/modules/_has.js");
var toObject = __webpack_require__(/*! ./_to-object */ "./node_modules/core-js/library/modules/_to-object.js");
var IE_PROTO = __webpack_require__(/*! ./_shared-key */ "./node_modules/core-js/library/modules/_shared-key.js")('IE_PROTO');
var ObjectProto = Object.prototype;

module.exports = Object.getPrototypeOf || function (O) {
  O = toObject(O);
  if (has(O, IE_PROTO)) return O[IE_PROTO];
  if (typeof O.constructor == 'function' && O instanceof O.constructor) {
    return O.constructor.prototype;
  } return O instanceof Object ? ObjectProto : null;
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_object-keys-internal.js":
/*!***********************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_object-keys-internal.js ***!
  \***********************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var has = __webpack_require__(/*! ./_has */ "./node_modules/core-js/library/modules/_has.js");
var toIObject = __webpack_require__(/*! ./_to-iobject */ "./node_modules/core-js/library/modules/_to-iobject.js");
var arrayIndexOf = __webpack_require__(/*! ./_array-includes */ "./node_modules/core-js/library/modules/_array-includes.js")(false);
var IE_PROTO = __webpack_require__(/*! ./_shared-key */ "./node_modules/core-js/library/modules/_shared-key.js")('IE_PROTO');

module.exports = function (object, names) {
  var O = toIObject(object);
  var i = 0;
  var result = [];
  var key;
  for (key in O) if (key != IE_PROTO) has(O, key) && result.push(key);
  // Don't enum bug & hidden keys
  while (names.length > i) if (has(O, key = names[i++])) {
    ~arrayIndexOf(result, key) || result.push(key);
  }
  return result;
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_object-keys.js":
/*!**************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_object-keys.js ***!
  \**************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// 19.1.2.14 / 15.2.3.14 Object.keys(O)
var $keys = __webpack_require__(/*! ./_object-keys-internal */ "./node_modules/core-js/library/modules/_object-keys-internal.js");
var enumBugKeys = __webpack_require__(/*! ./_enum-bug-keys */ "./node_modules/core-js/library/modules/_enum-bug-keys.js");

module.exports = Object.keys || function keys(O) {
  return $keys(O, enumBugKeys);
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_object-pie.js":
/*!*************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_object-pie.js ***!
  \*************************************************************/
/***/ ((__unused_webpack_module, exports) => {

exports.f = {}.propertyIsEnumerable;


/***/ }),

/***/ "./node_modules/core-js/library/modules/_object-sap.js":
/*!*************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_object-sap.js ***!
  \*************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// most Object methods by ES6 should accept primitives
var $export = __webpack_require__(/*! ./_export */ "./node_modules/core-js/library/modules/_export.js");
var core = __webpack_require__(/*! ./_core */ "./node_modules/core-js/library/modules/_core.js");
var fails = __webpack_require__(/*! ./_fails */ "./node_modules/core-js/library/modules/_fails.js");
module.exports = function (KEY, exec) {
  var fn = (core.Object || {})[KEY] || Object[KEY];
  var exp = {};
  exp[KEY] = exec(fn);
  $export($export.S + $export.F * fails(function () { fn(1); }), 'Object', exp);
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_object-to-array.js":
/*!******************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_object-to-array.js ***!
  \******************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var DESCRIPTORS = __webpack_require__(/*! ./_descriptors */ "./node_modules/core-js/library/modules/_descriptors.js");
var getKeys = __webpack_require__(/*! ./_object-keys */ "./node_modules/core-js/library/modules/_object-keys.js");
var toIObject = __webpack_require__(/*! ./_to-iobject */ "./node_modules/core-js/library/modules/_to-iobject.js");
var isEnum = __webpack_require__(/*! ./_object-pie */ "./node_modules/core-js/library/modules/_object-pie.js").f;
module.exports = function (isEntries) {
  return function (it) {
    var O = toIObject(it);
    var keys = getKeys(O);
    var length = keys.length;
    var i = 0;
    var result = [];
    var key;
    while (length > i) {
      key = keys[i++];
      if (!DESCRIPTORS || isEnum.call(O, key)) {
        result.push(isEntries ? [key, O[key]] : O[key]);
      }
    }
    return result;
  };
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_property-desc.js":
/*!****************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_property-desc.js ***!
  \****************************************************************/
/***/ ((module) => {

module.exports = function (bitmap, value) {
  return {
    enumerable: !(bitmap & 1),
    configurable: !(bitmap & 2),
    writable: !(bitmap & 4),
    value: value
  };
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_redefine.js":
/*!***********************************************************!*\
  !*** ./node_modules/core-js/library/modules/_redefine.js ***!
  \***********************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

module.exports = __webpack_require__(/*! ./_hide */ "./node_modules/core-js/library/modules/_hide.js");


/***/ }),

/***/ "./node_modules/core-js/library/modules/_set-proto.js":
/*!************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_set-proto.js ***!
  \************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// Works with __proto__ only. Old v8 can't work with null proto objects.
/* eslint-disable no-proto */
var isObject = __webpack_require__(/*! ./_is-object */ "./node_modules/core-js/library/modules/_is-object.js");
var anObject = __webpack_require__(/*! ./_an-object */ "./node_modules/core-js/library/modules/_an-object.js");
var check = function (O, proto) {
  anObject(O);
  if (!isObject(proto) && proto !== null) throw TypeError(proto + ": can't set as prototype!");
};
module.exports = {
  set: Object.setPrototypeOf || ('__proto__' in {} ? // eslint-disable-line
    function (test, buggy, set) {
      try {
        set = __webpack_require__(/*! ./_ctx */ "./node_modules/core-js/library/modules/_ctx.js")(Function.call, __webpack_require__(/*! ./_object-gopd */ "./node_modules/core-js/library/modules/_object-gopd.js").f(Object.prototype, '__proto__').set, 2);
        set(test, []);
        buggy = !(test instanceof Array);
      } catch (e) { buggy = true; }
      return function setPrototypeOf(O, proto) {
        check(O, proto);
        if (buggy) O.__proto__ = proto;
        else set(O, proto);
        return O;
      };
    }({}, false) : undefined),
  check: check
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_set-to-string-tag.js":
/*!********************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_set-to-string-tag.js ***!
  \********************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var def = __webpack_require__(/*! ./_object-dp */ "./node_modules/core-js/library/modules/_object-dp.js").f;
var has = __webpack_require__(/*! ./_has */ "./node_modules/core-js/library/modules/_has.js");
var TAG = __webpack_require__(/*! ./_wks */ "./node_modules/core-js/library/modules/_wks.js")('toStringTag');

module.exports = function (it, tag, stat) {
  if (it && !has(it = stat ? it : it.prototype, TAG)) def(it, TAG, { configurable: true, value: tag });
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_shared-key.js":
/*!*************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_shared-key.js ***!
  \*************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var shared = __webpack_require__(/*! ./_shared */ "./node_modules/core-js/library/modules/_shared.js")('keys');
var uid = __webpack_require__(/*! ./_uid */ "./node_modules/core-js/library/modules/_uid.js");
module.exports = function (key) {
  return shared[key] || (shared[key] = uid(key));
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_shared.js":
/*!*********************************************************!*\
  !*** ./node_modules/core-js/library/modules/_shared.js ***!
  \*********************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var core = __webpack_require__(/*! ./_core */ "./node_modules/core-js/library/modules/_core.js");
var global = __webpack_require__(/*! ./_global */ "./node_modules/core-js/library/modules/_global.js");
var SHARED = '__core-js_shared__';
var store = global[SHARED] || (global[SHARED] = {});

(module.exports = function (key, value) {
  return store[key] || (store[key] = value !== undefined ? value : {});
})('versions', []).push({
  version: core.version,
  mode: __webpack_require__(/*! ./_library */ "./node_modules/core-js/library/modules/_library.js") ? 'pure' : 'global',
  copyright: '© 2019 Denis Pushkarev (zloirock.ru)'
});


/***/ }),

/***/ "./node_modules/core-js/library/modules/_string-at.js":
/*!************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_string-at.js ***!
  \************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var toInteger = __webpack_require__(/*! ./_to-integer */ "./node_modules/core-js/library/modules/_to-integer.js");
var defined = __webpack_require__(/*! ./_defined */ "./node_modules/core-js/library/modules/_defined.js");
// true  -> String#at
// false -> String#codePointAt
module.exports = function (TO_STRING) {
  return function (that, pos) {
    var s = String(defined(that));
    var i = toInteger(pos);
    var l = s.length;
    var a, b;
    if (i < 0 || i >= l) return TO_STRING ? '' : undefined;
    a = s.charCodeAt(i);
    return a < 0xd800 || a > 0xdbff || i + 1 === l || (b = s.charCodeAt(i + 1)) < 0xdc00 || b > 0xdfff
      ? TO_STRING ? s.charAt(i) : a
      : TO_STRING ? s.slice(i, i + 2) : (a - 0xd800 << 10) + (b - 0xdc00) + 0x10000;
  };
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_to-absolute-index.js":
/*!********************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_to-absolute-index.js ***!
  \********************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var toInteger = __webpack_require__(/*! ./_to-integer */ "./node_modules/core-js/library/modules/_to-integer.js");
var max = Math.max;
var min = Math.min;
module.exports = function (index, length) {
  index = toInteger(index);
  return index < 0 ? max(index + length, 0) : min(index, length);
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_to-integer.js":
/*!*************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_to-integer.js ***!
  \*************************************************************/
/***/ ((module) => {

// 7.1.4 ToInteger
var ceil = Math.ceil;
var floor = Math.floor;
module.exports = function (it) {
  return isNaN(it = +it) ? 0 : (it > 0 ? floor : ceil)(it);
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_to-iobject.js":
/*!*************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_to-iobject.js ***!
  \*************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// to indexed object, toObject with fallback for non-array-like ES3 strings
var IObject = __webpack_require__(/*! ./_iobject */ "./node_modules/core-js/library/modules/_iobject.js");
var defined = __webpack_require__(/*! ./_defined */ "./node_modules/core-js/library/modules/_defined.js");
module.exports = function (it) {
  return IObject(defined(it));
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_to-length.js":
/*!************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_to-length.js ***!
  \************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// 7.1.15 ToLength
var toInteger = __webpack_require__(/*! ./_to-integer */ "./node_modules/core-js/library/modules/_to-integer.js");
var min = Math.min;
module.exports = function (it) {
  return it > 0 ? min(toInteger(it), 0x1fffffffffffff) : 0; // pow(2, 53) - 1 == 9007199254740991
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_to-object.js":
/*!************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_to-object.js ***!
  \************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// 7.1.13 ToObject(argument)
var defined = __webpack_require__(/*! ./_defined */ "./node_modules/core-js/library/modules/_defined.js");
module.exports = function (it) {
  return Object(defined(it));
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_to-primitive.js":
/*!***************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_to-primitive.js ***!
  \***************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// 7.1.1 ToPrimitive(input [, PreferredType])
var isObject = __webpack_require__(/*! ./_is-object */ "./node_modules/core-js/library/modules/_is-object.js");
// instead of the ES6 spec version, we didn't implement @@toPrimitive case
// and the second argument - flag - preferred type is a string
module.exports = function (it, S) {
  if (!isObject(it)) return it;
  var fn, val;
  if (S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;
  if (typeof (fn = it.valueOf) == 'function' && !isObject(val = fn.call(it))) return val;
  if (!S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;
  throw TypeError("Can't convert object to primitive value");
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_uid.js":
/*!******************************************************!*\
  !*** ./node_modules/core-js/library/modules/_uid.js ***!
  \******************************************************/
/***/ ((module) => {

var id = 0;
var px = Math.random();
module.exports = function (key) {
  return 'Symbol('.concat(key === undefined ? '' : key, ')_', (++id + px).toString(36));
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_wks-define.js":
/*!*************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_wks-define.js ***!
  \*************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var global = __webpack_require__(/*! ./_global */ "./node_modules/core-js/library/modules/_global.js");
var core = __webpack_require__(/*! ./_core */ "./node_modules/core-js/library/modules/_core.js");
var LIBRARY = __webpack_require__(/*! ./_library */ "./node_modules/core-js/library/modules/_library.js");
var wksExt = __webpack_require__(/*! ./_wks-ext */ "./node_modules/core-js/library/modules/_wks-ext.js");
var defineProperty = __webpack_require__(/*! ./_object-dp */ "./node_modules/core-js/library/modules/_object-dp.js").f;
module.exports = function (name) {
  var $Symbol = core.Symbol || (core.Symbol = LIBRARY ? {} : global.Symbol || {});
  if (name.charAt(0) != '_' && !(name in $Symbol)) defineProperty($Symbol, name, { value: wksExt.f(name) });
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_wks-ext.js":
/*!**********************************************************!*\
  !*** ./node_modules/core-js/library/modules/_wks-ext.js ***!
  \**********************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

exports.f = __webpack_require__(/*! ./_wks */ "./node_modules/core-js/library/modules/_wks.js");


/***/ }),

/***/ "./node_modules/core-js/library/modules/_wks.js":
/*!******************************************************!*\
  !*** ./node_modules/core-js/library/modules/_wks.js ***!
  \******************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var store = __webpack_require__(/*! ./_shared */ "./node_modules/core-js/library/modules/_shared.js")('wks');
var uid = __webpack_require__(/*! ./_uid */ "./node_modules/core-js/library/modules/_uid.js");
var Symbol = __webpack_require__(/*! ./_global */ "./node_modules/core-js/library/modules/_global.js").Symbol;
var USE_SYMBOL = typeof Symbol == 'function';

var $exports = module.exports = function (name) {
  return store[name] || (store[name] =
    USE_SYMBOL && Symbol[name] || (USE_SYMBOL ? Symbol : uid)('Symbol.' + name));
};

$exports.store = store;


/***/ }),

/***/ "./node_modules/core-js/library/modules/core.get-iterator-method.js":
/*!**************************************************************************!*\
  !*** ./node_modules/core-js/library/modules/core.get-iterator-method.js ***!
  \**************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var classof = __webpack_require__(/*! ./_classof */ "./node_modules/core-js/library/modules/_classof.js");
var ITERATOR = __webpack_require__(/*! ./_wks */ "./node_modules/core-js/library/modules/_wks.js")('iterator');
var Iterators = __webpack_require__(/*! ./_iterators */ "./node_modules/core-js/library/modules/_iterators.js");
module.exports = __webpack_require__(/*! ./_core */ "./node_modules/core-js/library/modules/_core.js").getIteratorMethod = function (it) {
  if (it != undefined) return it[ITERATOR]
    || it['@@iterator']
    || Iterators[classof(it)];
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/core.get-iterator.js":
/*!*******************************************************************!*\
  !*** ./node_modules/core-js/library/modules/core.get-iterator.js ***!
  \*******************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var anObject = __webpack_require__(/*! ./_an-object */ "./node_modules/core-js/library/modules/_an-object.js");
var get = __webpack_require__(/*! ./core.get-iterator-method */ "./node_modules/core-js/library/modules/core.get-iterator-method.js");
module.exports = __webpack_require__(/*! ./_core */ "./node_modules/core-js/library/modules/_core.js").getIterator = function (it) {
  var iterFn = get(it);
  if (typeof iterFn != 'function') throw TypeError(it + ' is not iterable!');
  return anObject(iterFn.call(it));
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/core.is-iterable.js":
/*!******************************************************************!*\
  !*** ./node_modules/core-js/library/modules/core.is-iterable.js ***!
  \******************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var classof = __webpack_require__(/*! ./_classof */ "./node_modules/core-js/library/modules/_classof.js");
var ITERATOR = __webpack_require__(/*! ./_wks */ "./node_modules/core-js/library/modules/_wks.js")('iterator');
var Iterators = __webpack_require__(/*! ./_iterators */ "./node_modules/core-js/library/modules/_iterators.js");
module.exports = __webpack_require__(/*! ./_core */ "./node_modules/core-js/library/modules/_core.js").isIterable = function (it) {
  var O = Object(it);
  return O[ITERATOR] !== undefined
    || '@@iterator' in O
    // eslint-disable-next-line no-prototype-builtins
    || Iterators.hasOwnProperty(classof(O));
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/es6.array.from.js":
/*!****************************************************************!*\
  !*** ./node_modules/core-js/library/modules/es6.array.from.js ***!
  \****************************************************************/
/***/ ((__unused_webpack_module, __unused_webpack_exports, __webpack_require__) => {

"use strict";

var ctx = __webpack_require__(/*! ./_ctx */ "./node_modules/core-js/library/modules/_ctx.js");
var $export = __webpack_require__(/*! ./_export */ "./node_modules/core-js/library/modules/_export.js");
var toObject = __webpack_require__(/*! ./_to-object */ "./node_modules/core-js/library/modules/_to-object.js");
var call = __webpack_require__(/*! ./_iter-call */ "./node_modules/core-js/library/modules/_iter-call.js");
var isArrayIter = __webpack_require__(/*! ./_is-array-iter */ "./node_modules/core-js/library/modules/_is-array-iter.js");
var toLength = __webpack_require__(/*! ./_to-length */ "./node_modules/core-js/library/modules/_to-length.js");
var createProperty = __webpack_require__(/*! ./_create-property */ "./node_modules/core-js/library/modules/_create-property.js");
var getIterFn = __webpack_require__(/*! ./core.get-iterator-method */ "./node_modules/core-js/library/modules/core.get-iterator-method.js");

$export($export.S + $export.F * !__webpack_require__(/*! ./_iter-detect */ "./node_modules/core-js/library/modules/_iter-detect.js")(function (iter) { Array.from(iter); }), 'Array', {
  // 22.1.2.1 Array.from(arrayLike, mapfn = undefined, thisArg = undefined)
  from: function from(arrayLike /* , mapfn = undefined, thisArg = undefined */) {
    var O = toObject(arrayLike);
    var C = typeof this == 'function' ? this : Array;
    var aLen = arguments.length;
    var mapfn = aLen > 1 ? arguments[1] : undefined;
    var mapping = mapfn !== undefined;
    var index = 0;
    var iterFn = getIterFn(O);
    var length, result, step, iterator;
    if (mapping) mapfn = ctx(mapfn, aLen > 2 ? arguments[2] : undefined, 2);
    // if object isn't iterable or it's array with default iterator - use simple case
    if (iterFn != undefined && !(C == Array && isArrayIter(iterFn))) {
      for (iterator = iterFn.call(O), result = new C(); !(step = iterator.next()).done; index++) {
        createProperty(result, index, mapping ? call(iterator, mapfn, [step.value, index], true) : step.value);
      }
    } else {
      length = toLength(O.length);
      for (result = new C(length); length > index; index++) {
        createProperty(result, index, mapping ? mapfn(O[index], index) : O[index]);
      }
    }
    result.length = index;
    return result;
  }
});


/***/ }),

/***/ "./node_modules/core-js/library/modules/es6.array.iterator.js":
/*!********************************************************************!*\
  !*** ./node_modules/core-js/library/modules/es6.array.iterator.js ***!
  \********************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";

var addToUnscopables = __webpack_require__(/*! ./_add-to-unscopables */ "./node_modules/core-js/library/modules/_add-to-unscopables.js");
var step = __webpack_require__(/*! ./_iter-step */ "./node_modules/core-js/library/modules/_iter-step.js");
var Iterators = __webpack_require__(/*! ./_iterators */ "./node_modules/core-js/library/modules/_iterators.js");
var toIObject = __webpack_require__(/*! ./_to-iobject */ "./node_modules/core-js/library/modules/_to-iobject.js");

// 22.1.3.4 Array.prototype.entries()
// 22.1.3.13 Array.prototype.keys()
// 22.1.3.29 Array.prototype.values()
// 22.1.3.30 Array.prototype[@@iterator]()
module.exports = __webpack_require__(/*! ./_iter-define */ "./node_modules/core-js/library/modules/_iter-define.js")(Array, 'Array', function (iterated, kind) {
  this._t = toIObject(iterated); // target
  this._i = 0;                   // next index
  this._k = kind;                // kind
// 22.1.5.2.1 %ArrayIteratorPrototype%.next()
}, function () {
  var O = this._t;
  var kind = this._k;
  var index = this._i++;
  if (!O || index >= O.length) {
    this._t = undefined;
    return step(1);
  }
  if (kind == 'keys') return step(0, index);
  if (kind == 'values') return step(0, O[index]);
  return step(0, [index, O[index]]);
}, 'values');

// argumentsList[@@iterator] is %ArrayProto_values% (9.4.4.6, 9.4.4.7)
Iterators.Arguments = Iterators.Array;

addToUnscopables('keys');
addToUnscopables('values');
addToUnscopables('entries');


/***/ }),

/***/ "./node_modules/core-js/library/modules/es6.number.is-nan.js":
/*!*******************************************************************!*\
  !*** ./node_modules/core-js/library/modules/es6.number.is-nan.js ***!
  \*******************************************************************/
/***/ ((__unused_webpack_module, __unused_webpack_exports, __webpack_require__) => {

// 20.1.2.4 Number.isNaN(number)
var $export = __webpack_require__(/*! ./_export */ "./node_modules/core-js/library/modules/_export.js");

$export($export.S, 'Number', {
  isNaN: function isNaN(number) {
    // eslint-disable-next-line no-self-compare
    return number != number;
  }
});


/***/ }),

/***/ "./node_modules/core-js/library/modules/es6.object.assign.js":
/*!*******************************************************************!*\
  !*** ./node_modules/core-js/library/modules/es6.object.assign.js ***!
  \*******************************************************************/
/***/ ((__unused_webpack_module, __unused_webpack_exports, __webpack_require__) => {

// 19.1.3.1 Object.assign(target, source)
var $export = __webpack_require__(/*! ./_export */ "./node_modules/core-js/library/modules/_export.js");

$export($export.S + $export.F, 'Object', { assign: __webpack_require__(/*! ./_object-assign */ "./node_modules/core-js/library/modules/_object-assign.js") });


/***/ }),

/***/ "./node_modules/core-js/library/modules/es6.object.create.js":
/*!*******************************************************************!*\
  !*** ./node_modules/core-js/library/modules/es6.object.create.js ***!
  \*******************************************************************/
/***/ ((__unused_webpack_module, __unused_webpack_exports, __webpack_require__) => {

var $export = __webpack_require__(/*! ./_export */ "./node_modules/core-js/library/modules/_export.js");
// 19.1.2.2 / 15.2.3.5 Object.create(O [, Properties])
$export($export.S, 'Object', { create: __webpack_require__(/*! ./_object-create */ "./node_modules/core-js/library/modules/_object-create.js") });


/***/ }),

/***/ "./node_modules/core-js/library/modules/es6.object.define-property.js":
/*!****************************************************************************!*\
  !*** ./node_modules/core-js/library/modules/es6.object.define-property.js ***!
  \****************************************************************************/
/***/ ((__unused_webpack_module, __unused_webpack_exports, __webpack_require__) => {

var $export = __webpack_require__(/*! ./_export */ "./node_modules/core-js/library/modules/_export.js");
// 19.1.2.4 / 15.2.3.6 Object.defineProperty(O, P, Attributes)
$export($export.S + $export.F * !__webpack_require__(/*! ./_descriptors */ "./node_modules/core-js/library/modules/_descriptors.js"), 'Object', { defineProperty: __webpack_require__(/*! ./_object-dp */ "./node_modules/core-js/library/modules/_object-dp.js").f });


/***/ }),

/***/ "./node_modules/core-js/library/modules/es6.object.get-prototype-of.js":
/*!*****************************************************************************!*\
  !*** ./node_modules/core-js/library/modules/es6.object.get-prototype-of.js ***!
  \*****************************************************************************/
/***/ ((__unused_webpack_module, __unused_webpack_exports, __webpack_require__) => {

// 19.1.2.9 Object.getPrototypeOf(O)
var toObject = __webpack_require__(/*! ./_to-object */ "./node_modules/core-js/library/modules/_to-object.js");
var $getPrototypeOf = __webpack_require__(/*! ./_object-gpo */ "./node_modules/core-js/library/modules/_object-gpo.js");

__webpack_require__(/*! ./_object-sap */ "./node_modules/core-js/library/modules/_object-sap.js")('getPrototypeOf', function () {
  return function getPrototypeOf(it) {
    return $getPrototypeOf(toObject(it));
  };
});


/***/ }),

/***/ "./node_modules/core-js/library/modules/es6.object.keys.js":
/*!*****************************************************************!*\
  !*** ./node_modules/core-js/library/modules/es6.object.keys.js ***!
  \*****************************************************************/
/***/ ((__unused_webpack_module, __unused_webpack_exports, __webpack_require__) => {

// 19.1.2.14 Object.keys(O)
var toObject = __webpack_require__(/*! ./_to-object */ "./node_modules/core-js/library/modules/_to-object.js");
var $keys = __webpack_require__(/*! ./_object-keys */ "./node_modules/core-js/library/modules/_object-keys.js");

__webpack_require__(/*! ./_object-sap */ "./node_modules/core-js/library/modules/_object-sap.js")('keys', function () {
  return function keys(it) {
    return $keys(toObject(it));
  };
});


/***/ }),

/***/ "./node_modules/core-js/library/modules/es6.object.set-prototype-of.js":
/*!*****************************************************************************!*\
  !*** ./node_modules/core-js/library/modules/es6.object.set-prototype-of.js ***!
  \*****************************************************************************/
/***/ ((__unused_webpack_module, __unused_webpack_exports, __webpack_require__) => {

// 19.1.3.19 Object.setPrototypeOf(O, proto)
var $export = __webpack_require__(/*! ./_export */ "./node_modules/core-js/library/modules/_export.js");
$export($export.S, 'Object', { setPrototypeOf: __webpack_require__(/*! ./_set-proto */ "./node_modules/core-js/library/modules/_set-proto.js").set });


/***/ }),

/***/ "./node_modules/core-js/library/modules/es6.object.to-string.js":
/*!**********************************************************************!*\
  !*** ./node_modules/core-js/library/modules/es6.object.to-string.js ***!
  \**********************************************************************/
/***/ (() => {



/***/ }),

/***/ "./node_modules/core-js/library/modules/es6.string.iterator.js":
/*!*********************************************************************!*\
  !*** ./node_modules/core-js/library/modules/es6.string.iterator.js ***!
  \*********************************************************************/
/***/ ((__unused_webpack_module, __unused_webpack_exports, __webpack_require__) => {

"use strict";

var $at = __webpack_require__(/*! ./_string-at */ "./node_modules/core-js/library/modules/_string-at.js")(true);

// 21.1.3.27 String.prototype[@@iterator]()
__webpack_require__(/*! ./_iter-define */ "./node_modules/core-js/library/modules/_iter-define.js")(String, 'String', function (iterated) {
  this._t = String(iterated); // target
  this._i = 0;                // next index
// 21.1.5.2.1 %StringIteratorPrototype%.next()
}, function () {
  var O = this._t;
  var index = this._i;
  var point;
  if (index >= O.length) return { value: undefined, done: true };
  point = $at(O, index);
  this._i += point.length;
  return { value: point, done: false };
});


/***/ }),

/***/ "./node_modules/core-js/library/modules/es6.symbol.js":
/*!************************************************************!*\
  !*** ./node_modules/core-js/library/modules/es6.symbol.js ***!
  \************************************************************/
/***/ ((__unused_webpack_module, __unused_webpack_exports, __webpack_require__) => {

"use strict";

// ECMAScript 6 symbols shim
var global = __webpack_require__(/*! ./_global */ "./node_modules/core-js/library/modules/_global.js");
var has = __webpack_require__(/*! ./_has */ "./node_modules/core-js/library/modules/_has.js");
var DESCRIPTORS = __webpack_require__(/*! ./_descriptors */ "./node_modules/core-js/library/modules/_descriptors.js");
var $export = __webpack_require__(/*! ./_export */ "./node_modules/core-js/library/modules/_export.js");
var redefine = __webpack_require__(/*! ./_redefine */ "./node_modules/core-js/library/modules/_redefine.js");
var META = __webpack_require__(/*! ./_meta */ "./node_modules/core-js/library/modules/_meta.js").KEY;
var $fails = __webpack_require__(/*! ./_fails */ "./node_modules/core-js/library/modules/_fails.js");
var shared = __webpack_require__(/*! ./_shared */ "./node_modules/core-js/library/modules/_shared.js");
var setToStringTag = __webpack_require__(/*! ./_set-to-string-tag */ "./node_modules/core-js/library/modules/_set-to-string-tag.js");
var uid = __webpack_require__(/*! ./_uid */ "./node_modules/core-js/library/modules/_uid.js");
var wks = __webpack_require__(/*! ./_wks */ "./node_modules/core-js/library/modules/_wks.js");
var wksExt = __webpack_require__(/*! ./_wks-ext */ "./node_modules/core-js/library/modules/_wks-ext.js");
var wksDefine = __webpack_require__(/*! ./_wks-define */ "./node_modules/core-js/library/modules/_wks-define.js");
var enumKeys = __webpack_require__(/*! ./_enum-keys */ "./node_modules/core-js/library/modules/_enum-keys.js");
var isArray = __webpack_require__(/*! ./_is-array */ "./node_modules/core-js/library/modules/_is-array.js");
var anObject = __webpack_require__(/*! ./_an-object */ "./node_modules/core-js/library/modules/_an-object.js");
var isObject = __webpack_require__(/*! ./_is-object */ "./node_modules/core-js/library/modules/_is-object.js");
var toObject = __webpack_require__(/*! ./_to-object */ "./node_modules/core-js/library/modules/_to-object.js");
var toIObject = __webpack_require__(/*! ./_to-iobject */ "./node_modules/core-js/library/modules/_to-iobject.js");
var toPrimitive = __webpack_require__(/*! ./_to-primitive */ "./node_modules/core-js/library/modules/_to-primitive.js");
var createDesc = __webpack_require__(/*! ./_property-desc */ "./node_modules/core-js/library/modules/_property-desc.js");
var _create = __webpack_require__(/*! ./_object-create */ "./node_modules/core-js/library/modules/_object-create.js");
var gOPNExt = __webpack_require__(/*! ./_object-gopn-ext */ "./node_modules/core-js/library/modules/_object-gopn-ext.js");
var $GOPD = __webpack_require__(/*! ./_object-gopd */ "./node_modules/core-js/library/modules/_object-gopd.js");
var $GOPS = __webpack_require__(/*! ./_object-gops */ "./node_modules/core-js/library/modules/_object-gops.js");
var $DP = __webpack_require__(/*! ./_object-dp */ "./node_modules/core-js/library/modules/_object-dp.js");
var $keys = __webpack_require__(/*! ./_object-keys */ "./node_modules/core-js/library/modules/_object-keys.js");
var gOPD = $GOPD.f;
var dP = $DP.f;
var gOPN = gOPNExt.f;
var $Symbol = global.Symbol;
var $JSON = global.JSON;
var _stringify = $JSON && $JSON.stringify;
var PROTOTYPE = 'prototype';
var HIDDEN = wks('_hidden');
var TO_PRIMITIVE = wks('toPrimitive');
var isEnum = {}.propertyIsEnumerable;
var SymbolRegistry = shared('symbol-registry');
var AllSymbols = shared('symbols');
var OPSymbols = shared('op-symbols');
var ObjectProto = Object[PROTOTYPE];
var USE_NATIVE = typeof $Symbol == 'function' && !!$GOPS.f;
var QObject = global.QObject;
// Don't use setters in Qt Script, https://github.com/zloirock/core-js/issues/173
var setter = !QObject || !QObject[PROTOTYPE] || !QObject[PROTOTYPE].findChild;

// fallback for old Android, https://code.google.com/p/v8/issues/detail?id=687
var setSymbolDesc = DESCRIPTORS && $fails(function () {
  return _create(dP({}, 'a', {
    get: function () { return dP(this, 'a', { value: 7 }).a; }
  })).a != 7;
}) ? function (it, key, D) {
  var protoDesc = gOPD(ObjectProto, key);
  if (protoDesc) delete ObjectProto[key];
  dP(it, key, D);
  if (protoDesc && it !== ObjectProto) dP(ObjectProto, key, protoDesc);
} : dP;

var wrap = function (tag) {
  var sym = AllSymbols[tag] = _create($Symbol[PROTOTYPE]);
  sym._k = tag;
  return sym;
};

var isSymbol = USE_NATIVE && typeof $Symbol.iterator == 'symbol' ? function (it) {
  return typeof it == 'symbol';
} : function (it) {
  return it instanceof $Symbol;
};

var $defineProperty = function defineProperty(it, key, D) {
  if (it === ObjectProto) $defineProperty(OPSymbols, key, D);
  anObject(it);
  key = toPrimitive(key, true);
  anObject(D);
  if (has(AllSymbols, key)) {
    if (!D.enumerable) {
      if (!has(it, HIDDEN)) dP(it, HIDDEN, createDesc(1, {}));
      it[HIDDEN][key] = true;
    } else {
      if (has(it, HIDDEN) && it[HIDDEN][key]) it[HIDDEN][key] = false;
      D = _create(D, { enumerable: createDesc(0, false) });
    } return setSymbolDesc(it, key, D);
  } return dP(it, key, D);
};
var $defineProperties = function defineProperties(it, P) {
  anObject(it);
  var keys = enumKeys(P = toIObject(P));
  var i = 0;
  var l = keys.length;
  var key;
  while (l > i) $defineProperty(it, key = keys[i++], P[key]);
  return it;
};
var $create = function create(it, P) {
  return P === undefined ? _create(it) : $defineProperties(_create(it), P);
};
var $propertyIsEnumerable = function propertyIsEnumerable(key) {
  var E = isEnum.call(this, key = toPrimitive(key, true));
  if (this === ObjectProto && has(AllSymbols, key) && !has(OPSymbols, key)) return false;
  return E || !has(this, key) || !has(AllSymbols, key) || has(this, HIDDEN) && this[HIDDEN][key] ? E : true;
};
var $getOwnPropertyDescriptor = function getOwnPropertyDescriptor(it, key) {
  it = toIObject(it);
  key = toPrimitive(key, true);
  if (it === ObjectProto && has(AllSymbols, key) && !has(OPSymbols, key)) return;
  var D = gOPD(it, key);
  if (D && has(AllSymbols, key) && !(has(it, HIDDEN) && it[HIDDEN][key])) D.enumerable = true;
  return D;
};
var $getOwnPropertyNames = function getOwnPropertyNames(it) {
  var names = gOPN(toIObject(it));
  var result = [];
  var i = 0;
  var key;
  while (names.length > i) {
    if (!has(AllSymbols, key = names[i++]) && key != HIDDEN && key != META) result.push(key);
  } return result;
};
var $getOwnPropertySymbols = function getOwnPropertySymbols(it) {
  var IS_OP = it === ObjectProto;
  var names = gOPN(IS_OP ? OPSymbols : toIObject(it));
  var result = [];
  var i = 0;
  var key;
  while (names.length > i) {
    if (has(AllSymbols, key = names[i++]) && (IS_OP ? has(ObjectProto, key) : true)) result.push(AllSymbols[key]);
  } return result;
};

// 19.4.1.1 Symbol([description])
if (!USE_NATIVE) {
  $Symbol = function Symbol() {
    if (this instanceof $Symbol) throw TypeError('Symbol is not a constructor!');
    var tag = uid(arguments.length > 0 ? arguments[0] : undefined);
    var $set = function (value) {
      if (this === ObjectProto) $set.call(OPSymbols, value);
      if (has(this, HIDDEN) && has(this[HIDDEN], tag)) this[HIDDEN][tag] = false;
      setSymbolDesc(this, tag, createDesc(1, value));
    };
    if (DESCRIPTORS && setter) setSymbolDesc(ObjectProto, tag, { configurable: true, set: $set });
    return wrap(tag);
  };
  redefine($Symbol[PROTOTYPE], 'toString', function toString() {
    return this._k;
  });

  $GOPD.f = $getOwnPropertyDescriptor;
  $DP.f = $defineProperty;
  __webpack_require__(/*! ./_object-gopn */ "./node_modules/core-js/library/modules/_object-gopn.js").f = gOPNExt.f = $getOwnPropertyNames;
  __webpack_require__(/*! ./_object-pie */ "./node_modules/core-js/library/modules/_object-pie.js").f = $propertyIsEnumerable;
  $GOPS.f = $getOwnPropertySymbols;

  if (DESCRIPTORS && !__webpack_require__(/*! ./_library */ "./node_modules/core-js/library/modules/_library.js")) {
    redefine(ObjectProto, 'propertyIsEnumerable', $propertyIsEnumerable, true);
  }

  wksExt.f = function (name) {
    return wrap(wks(name));
  };
}

$export($export.G + $export.W + $export.F * !USE_NATIVE, { Symbol: $Symbol });

for (var es6Symbols = (
  // 19.4.2.2, 19.4.2.3, 19.4.2.4, 19.4.2.6, 19.4.2.8, 19.4.2.9, 19.4.2.10, 19.4.2.11, 19.4.2.12, 19.4.2.13, 19.4.2.14
  'hasInstance,isConcatSpreadable,iterator,match,replace,search,species,split,toPrimitive,toStringTag,unscopables'
).split(','), j = 0; es6Symbols.length > j;)wks(es6Symbols[j++]);

for (var wellKnownSymbols = $keys(wks.store), k = 0; wellKnownSymbols.length > k;) wksDefine(wellKnownSymbols[k++]);

$export($export.S + $export.F * !USE_NATIVE, 'Symbol', {
  // 19.4.2.1 Symbol.for(key)
  'for': function (key) {
    return has(SymbolRegistry, key += '')
      ? SymbolRegistry[key]
      : SymbolRegistry[key] = $Symbol(key);
  },
  // 19.4.2.5 Symbol.keyFor(sym)
  keyFor: function keyFor(sym) {
    if (!isSymbol(sym)) throw TypeError(sym + ' is not a symbol!');
    for (var key in SymbolRegistry) if (SymbolRegistry[key] === sym) return key;
  },
  useSetter: function () { setter = true; },
  useSimple: function () { setter = false; }
});

$export($export.S + $export.F * !USE_NATIVE, 'Object', {
  // 19.1.2.2 Object.create(O [, Properties])
  create: $create,
  // 19.1.2.4 Object.defineProperty(O, P, Attributes)
  defineProperty: $defineProperty,
  // 19.1.2.3 Object.defineProperties(O, Properties)
  defineProperties: $defineProperties,
  // 19.1.2.6 Object.getOwnPropertyDescriptor(O, P)
  getOwnPropertyDescriptor: $getOwnPropertyDescriptor,
  // 19.1.2.7 Object.getOwnPropertyNames(O)
  getOwnPropertyNames: $getOwnPropertyNames,
  // 19.1.2.8 Object.getOwnPropertySymbols(O)
  getOwnPropertySymbols: $getOwnPropertySymbols
});

// Chrome 38 and 39 `Object.getOwnPropertySymbols` fails on primitives
// https://bugs.chromium.org/p/v8/issues/detail?id=3443
var FAILS_ON_PRIMITIVES = $fails(function () { $GOPS.f(1); });

$export($export.S + $export.F * FAILS_ON_PRIMITIVES, 'Object', {
  getOwnPropertySymbols: function getOwnPropertySymbols(it) {
    return $GOPS.f(toObject(it));
  }
});

// 24.3.2 JSON.stringify(value [, replacer [, space]])
$JSON && $export($export.S + $export.F * (!USE_NATIVE || $fails(function () {
  var S = $Symbol();
  // MS Edge converts symbol values to JSON as {}
  // WebKit converts symbol values to JSON as null
  // V8 throws on boxed symbols
  return _stringify([S]) != '[null]' || _stringify({ a: S }) != '{}' || _stringify(Object(S)) != '{}';
})), 'JSON', {
  stringify: function stringify(it) {
    var args = [it];
    var i = 1;
    var replacer, $replacer;
    while (arguments.length > i) args.push(arguments[i++]);
    $replacer = replacer = args[1];
    if (!isObject(replacer) && it === undefined || isSymbol(it)) return; // IE8 returns string on undefined
    if (!isArray(replacer)) replacer = function (key, value) {
      if (typeof $replacer == 'function') value = $replacer.call(this, key, value);
      if (!isSymbol(value)) return value;
    };
    args[1] = replacer;
    return _stringify.apply($JSON, args);
  }
});

// 19.4.3.4 Symbol.prototype[@@toPrimitive](hint)
$Symbol[PROTOTYPE][TO_PRIMITIVE] || __webpack_require__(/*! ./_hide */ "./node_modules/core-js/library/modules/_hide.js")($Symbol[PROTOTYPE], TO_PRIMITIVE, $Symbol[PROTOTYPE].valueOf);
// 19.4.3.5 Symbol.prototype[@@toStringTag]
setToStringTag($Symbol, 'Symbol');
// 20.2.1.9 Math[@@toStringTag]
setToStringTag(Math, 'Math', true);
// 24.3.3 JSON[@@toStringTag]
setToStringTag(global.JSON, 'JSON', true);


/***/ }),

/***/ "./node_modules/core-js/library/modules/es7.object.values.js":
/*!*******************************************************************!*\
  !*** ./node_modules/core-js/library/modules/es7.object.values.js ***!
  \*******************************************************************/
/***/ ((__unused_webpack_module, __unused_webpack_exports, __webpack_require__) => {

// https://github.com/tc39/proposal-object-values-entries
var $export = __webpack_require__(/*! ./_export */ "./node_modules/core-js/library/modules/_export.js");
var $values = __webpack_require__(/*! ./_object-to-array */ "./node_modules/core-js/library/modules/_object-to-array.js")(false);

$export($export.S, 'Object', {
  values: function values(it) {
    return $values(it);
  }
});


/***/ }),

/***/ "./node_modules/core-js/library/modules/es7.symbol.async-iterator.js":
/*!***************************************************************************!*\
  !*** ./node_modules/core-js/library/modules/es7.symbol.async-iterator.js ***!
  \***************************************************************************/
/***/ ((__unused_webpack_module, __unused_webpack_exports, __webpack_require__) => {

__webpack_require__(/*! ./_wks-define */ "./node_modules/core-js/library/modules/_wks-define.js")('asyncIterator');


/***/ }),

/***/ "./node_modules/core-js/library/modules/es7.symbol.observable.js":
/*!***********************************************************************!*\
  !*** ./node_modules/core-js/library/modules/es7.symbol.observable.js ***!
  \***********************************************************************/
/***/ ((__unused_webpack_module, __unused_webpack_exports, __webpack_require__) => {

__webpack_require__(/*! ./_wks-define */ "./node_modules/core-js/library/modules/_wks-define.js")('observable');


/***/ }),

/***/ "./node_modules/core-js/library/modules/web.dom.iterable.js":
/*!******************************************************************!*\
  !*** ./node_modules/core-js/library/modules/web.dom.iterable.js ***!
  \******************************************************************/
/***/ ((__unused_webpack_module, __unused_webpack_exports, __webpack_require__) => {

__webpack_require__(/*! ./es6.array.iterator */ "./node_modules/core-js/library/modules/es6.array.iterator.js");
var global = __webpack_require__(/*! ./_global */ "./node_modules/core-js/library/modules/_global.js");
var hide = __webpack_require__(/*! ./_hide */ "./node_modules/core-js/library/modules/_hide.js");
var Iterators = __webpack_require__(/*! ./_iterators */ "./node_modules/core-js/library/modules/_iterators.js");
var TO_STRING_TAG = __webpack_require__(/*! ./_wks */ "./node_modules/core-js/library/modules/_wks.js")('toStringTag');

var DOMIterables = ('CSSRuleList,CSSStyleDeclaration,CSSValueList,ClientRectList,DOMRectList,DOMStringList,' +
  'DOMTokenList,DataTransferItemList,FileList,HTMLAllCollection,HTMLCollection,HTMLFormElement,HTMLSelectElement,' +
  'MediaList,MimeTypeArray,NamedNodeMap,NodeList,PaintRequestList,Plugin,PluginArray,SVGLengthList,SVGNumberList,' +
  'SVGPathSegList,SVGPointList,SVGStringList,SVGTransformList,SourceBufferList,StyleSheetList,TextTrackCueList,' +
  'TextTrackList,TouchList').split(',');

for (var i = 0; i < DOMIterables.length; i++) {
  var NAME = DOMIterables[i];
  var Collection = global[NAME];
  var proto = Collection && Collection.prototype;
  if (proto && !proto[TO_STRING_TAG]) hide(proto, TO_STRING_TAG, NAME);
  Iterators[NAME] = Iterators.Array;
}


/***/ }),

/***/ "./node_modules/events/events.js":
/*!***************************************!*\
  !*** ./node_modules/events/events.js ***!
  \***************************************/
/***/ ((module) => {

"use strict";
// Copyright Joyent, Inc. and other Node contributors.
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.



var R = typeof Reflect === 'object' ? Reflect : null
var ReflectApply = R && typeof R.apply === 'function'
  ? R.apply
  : function ReflectApply(target, receiver, args) {
    return Function.prototype.apply.call(target, receiver, args);
  }

var ReflectOwnKeys
if (R && typeof R.ownKeys === 'function') {
  ReflectOwnKeys = R.ownKeys
} else if (Object.getOwnPropertySymbols) {
  ReflectOwnKeys = function ReflectOwnKeys(target) {
    return Object.getOwnPropertyNames(target)
      .concat(Object.getOwnPropertySymbols(target));
  };
} else {
  ReflectOwnKeys = function ReflectOwnKeys(target) {
    return Object.getOwnPropertyNames(target);
  };
}

function ProcessEmitWarning(warning) {
  if (console && console.warn) console.warn(warning);
}

var NumberIsNaN = Number.isNaN || function NumberIsNaN(value) {
  return value !== value;
}

function EventEmitter() {
  EventEmitter.init.call(this);
}
module.exports = EventEmitter;

// Backwards-compat with node 0.10.x
EventEmitter.EventEmitter = EventEmitter;

EventEmitter.prototype._events = undefined;
EventEmitter.prototype._eventsCount = 0;
EventEmitter.prototype._maxListeners = undefined;

// By default EventEmitters will print a warning if more than 10 listeners are
// added to it. This is a useful default which helps finding memory leaks.
var defaultMaxListeners = 10;

function checkListener(listener) {
  if (typeof listener !== 'function') {
    throw new TypeError('The "listener" argument must be of type Function. Received type ' + typeof listener);
  }
}

Object.defineProperty(EventEmitter, 'defaultMaxListeners', {
  enumerable: true,
  get: function() {
    return defaultMaxListeners;
  },
  set: function(arg) {
    if (typeof arg !== 'number' || arg < 0 || NumberIsNaN(arg)) {
      throw new RangeError('The value of "defaultMaxListeners" is out of range. It must be a non-negative number. Received ' + arg + '.');
    }
    defaultMaxListeners = arg;
  }
});

EventEmitter.init = function() {

  if (this._events === undefined ||
      this._events === Object.getPrototypeOf(this)._events) {
    this._events = Object.create(null);
    this._eventsCount = 0;
  }

  this._maxListeners = this._maxListeners || undefined;
};

// Obviously not all Emitters should be limited to 10. This function allows
// that to be increased. Set to zero for unlimited.
EventEmitter.prototype.setMaxListeners = function setMaxListeners(n) {
  if (typeof n !== 'number' || n < 0 || NumberIsNaN(n)) {
    throw new RangeError('The value of "n" is out of range. It must be a non-negative number. Received ' + n + '.');
  }
  this._maxListeners = n;
  return this;
};

function _getMaxListeners(that) {
  if (that._maxListeners === undefined)
    return EventEmitter.defaultMaxListeners;
  return that._maxListeners;
}

EventEmitter.prototype.getMaxListeners = function getMaxListeners() {
  return _getMaxListeners(this);
};

EventEmitter.prototype.emit = function emit(type) {
  var args = [];
  for (var i = 1; i < arguments.length; i++) args.push(arguments[i]);
  var doError = (type === 'error');

  var events = this._events;
  if (events !== undefined)
    doError = (doError && events.error === undefined);
  else if (!doError)
    return false;

  // If there is no 'error' event listener then throw.
  if (doError) {
    var er;
    if (args.length > 0)
      er = args[0];
    if (er instanceof Error) {
      // Note: The comments on the `throw` lines are intentional, they show
      // up in Node's output if this results in an unhandled exception.
      throw er; // Unhandled 'error' event
    }
    // At least give some kind of context to the user
    var err = new Error('Unhandled error.' + (er ? ' (' + er.message + ')' : ''));
    err.context = er;
    throw err; // Unhandled 'error' event
  }

  var handler = events[type];

  if (handler === undefined)
    return false;

  if (typeof handler === 'function') {
    ReflectApply(handler, this, args);
  } else {
    var len = handler.length;
    var listeners = arrayClone(handler, len);
    for (var i = 0; i < len; ++i)
      ReflectApply(listeners[i], this, args);
  }

  return true;
};

function _addListener(target, type, listener, prepend) {
  var m;
  var events;
  var existing;

  checkListener(listener);

  events = target._events;
  if (events === undefined) {
    events = target._events = Object.create(null);
    target._eventsCount = 0;
  } else {
    // To avoid recursion in the case that type === "newListener"! Before
    // adding it to the listeners, first emit "newListener".
    if (events.newListener !== undefined) {
      target.emit('newListener', type,
                  listener.listener ? listener.listener : listener);

      // Re-assign `events` because a newListener handler could have caused the
      // this._events to be assigned to a new object
      events = target._events;
    }
    existing = events[type];
  }

  if (existing === undefined) {
    // Optimize the case of one listener. Don't need the extra array object.
    existing = events[type] = listener;
    ++target._eventsCount;
  } else {
    if (typeof existing === 'function') {
      // Adding the second element, need to change to array.
      existing = events[type] =
        prepend ? [listener, existing] : [existing, listener];
      // If we've already got an array, just append.
    } else if (prepend) {
      existing.unshift(listener);
    } else {
      existing.push(listener);
    }

    // Check for listener leak
    m = _getMaxListeners(target);
    if (m > 0 && existing.length > m && !existing.warned) {
      existing.warned = true;
      // No error code for this since it is a Warning
      // eslint-disable-next-line no-restricted-syntax
      var w = new Error('Possible EventEmitter memory leak detected. ' +
                          existing.length + ' ' + String(type) + ' listeners ' +
                          'added. Use emitter.setMaxListeners() to ' +
                          'increase limit');
      w.name = 'MaxListenersExceededWarning';
      w.emitter = target;
      w.type = type;
      w.count = existing.length;
      ProcessEmitWarning(w);
    }
  }

  return target;
}

EventEmitter.prototype.addListener = function addListener(type, listener) {
  return _addListener(this, type, listener, false);
};

EventEmitter.prototype.on = EventEmitter.prototype.addListener;

EventEmitter.prototype.prependListener =
    function prependListener(type, listener) {
      return _addListener(this, type, listener, true);
    };

function onceWrapper() {
  if (!this.fired) {
    this.target.removeListener(this.type, this.wrapFn);
    this.fired = true;
    if (arguments.length === 0)
      return this.listener.call(this.target);
    return this.listener.apply(this.target, arguments);
  }
}

function _onceWrap(target, type, listener) {
  var state = { fired: false, wrapFn: undefined, target: target, type: type, listener: listener };
  var wrapped = onceWrapper.bind(state);
  wrapped.listener = listener;
  state.wrapFn = wrapped;
  return wrapped;
}

EventEmitter.prototype.once = function once(type, listener) {
  checkListener(listener);
  this.on(type, _onceWrap(this, type, listener));
  return this;
};

EventEmitter.prototype.prependOnceListener =
    function prependOnceListener(type, listener) {
      checkListener(listener);
      this.prependListener(type, _onceWrap(this, type, listener));
      return this;
    };

// Emits a 'removeListener' event if and only if the listener was removed.
EventEmitter.prototype.removeListener =
    function removeListener(type, listener) {
      var list, events, position, i, originalListener;

      checkListener(listener);

      events = this._events;
      if (events === undefined)
        return this;

      list = events[type];
      if (list === undefined)
        return this;

      if (list === listener || list.listener === listener) {
        if (--this._eventsCount === 0)
          this._events = Object.create(null);
        else {
          delete events[type];
          if (events.removeListener)
            this.emit('removeListener', type, list.listener || listener);
        }
      } else if (typeof list !== 'function') {
        position = -1;

        for (i = list.length - 1; i >= 0; i--) {
          if (list[i] === listener || list[i].listener === listener) {
            originalListener = list[i].listener;
            position = i;
            break;
          }
        }

        if (position < 0)
          return this;

        if (position === 0)
          list.shift();
        else {
          spliceOne(list, position);
        }

        if (list.length === 1)
          events[type] = list[0];

        if (events.removeListener !== undefined)
          this.emit('removeListener', type, originalListener || listener);
      }

      return this;
    };

EventEmitter.prototype.off = EventEmitter.prototype.removeListener;

EventEmitter.prototype.removeAllListeners =
    function removeAllListeners(type) {
      var listeners, events, i;

      events = this._events;
      if (events === undefined)
        return this;

      // not listening for removeListener, no need to emit
      if (events.removeListener === undefined) {
        if (arguments.length === 0) {
          this._events = Object.create(null);
          this._eventsCount = 0;
        } else if (events[type] !== undefined) {
          if (--this._eventsCount === 0)
            this._events = Object.create(null);
          else
            delete events[type];
        }
        return this;
      }

      // emit removeListener for all listeners on all events
      if (arguments.length === 0) {
        var keys = Object.keys(events);
        var key;
        for (i = 0; i < keys.length; ++i) {
          key = keys[i];
          if (key === 'removeListener') continue;
          this.removeAllListeners(key);
        }
        this.removeAllListeners('removeListener');
        this._events = Object.create(null);
        this._eventsCount = 0;
        return this;
      }

      listeners = events[type];

      if (typeof listeners === 'function') {
        this.removeListener(type, listeners);
      } else if (listeners !== undefined) {
        // LIFO order
        for (i = listeners.length - 1; i >= 0; i--) {
          this.removeListener(type, listeners[i]);
        }
      }

      return this;
    };

function _listeners(target, type, unwrap) {
  var events = target._events;

  if (events === undefined)
    return [];

  var evlistener = events[type];
  if (evlistener === undefined)
    return [];

  if (typeof evlistener === 'function')
    return unwrap ? [evlistener.listener || evlistener] : [evlistener];

  return unwrap ?
    unwrapListeners(evlistener) : arrayClone(evlistener, evlistener.length);
}

EventEmitter.prototype.listeners = function listeners(type) {
  return _listeners(this, type, true);
};

EventEmitter.prototype.rawListeners = function rawListeners(type) {
  return _listeners(this, type, false);
};

EventEmitter.listenerCount = function(emitter, type) {
  if (typeof emitter.listenerCount === 'function') {
    return emitter.listenerCount(type);
  } else {
    return listenerCount.call(emitter, type);
  }
};

EventEmitter.prototype.listenerCount = listenerCount;
function listenerCount(type) {
  var events = this._events;

  if (events !== undefined) {
    var evlistener = events[type];

    if (typeof evlistener === 'function') {
      return 1;
    } else if (evlistener !== undefined) {
      return evlistener.length;
    }
  }

  return 0;
}

EventEmitter.prototype.eventNames = function eventNames() {
  return this._eventsCount > 0 ? ReflectOwnKeys(this._events) : [];
};

function arrayClone(arr, n) {
  var copy = new Array(n);
  for (var i = 0; i < n; ++i)
    copy[i] = arr[i];
  return copy;
}

function spliceOne(list, index) {
  for (; index + 1 < list.length; index++)
    list[index] = list[index + 1];
  list.pop();
}

function unwrapListeners(arr) {
  var ret = new Array(arr.length);
  for (var i = 0; i < ret.length; ++i) {
    ret[i] = arr[i].listener || arr[i];
  }
  return ret;
}


/***/ }),

/***/ "./node_modules/fos-routing/dist/routing.js":
/*!**************************************************!*\
  !*** ./node_modules/fos-routing/dist/routing.js ***!
  \**************************************************/
/***/ ((module) => {

"use strict";
var _extends=Object.assign||function(a){for(var b,c=1;c<arguments.length;c++)for(var d in b=arguments[c],b)Object.prototype.hasOwnProperty.call(b,d)&&(a[d]=b[d]);return a},_typeof='function'==typeof Symbol&&'symbol'==typeof Symbol.iterator?function(a){return typeof a}:function(a){return a&&'function'==typeof Symbol&&a.constructor===Symbol&&a!==Symbol.prototype?'symbol':typeof a};function _classCallCheck(a,b){if(!(a instanceof b))throw new TypeError('Cannot call a class as a function')}var Routing=function a(){var b=this;_classCallCheck(this,a),this.setRoutes=function(a){b.routesRouting=a||[]},this.getRoutes=function(){return b.routesRouting},this.setBaseUrl=function(a){b.contextRouting.base_url=a},this.getBaseUrl=function(){return b.contextRouting.base_url},this.setPrefix=function(a){b.contextRouting.prefix=a},this.setScheme=function(a){b.contextRouting.scheme=a},this.getScheme=function(){return b.contextRouting.scheme},this.setHost=function(a){b.contextRouting.host=a},this.getHost=function(){return b.contextRouting.host},this.buildQueryParams=function(a,c,d){var e=new RegExp(/\[]$/);c instanceof Array?c.forEach(function(c,f){e.test(a)?d(a,c):b.buildQueryParams(a+'['+('object'===('undefined'==typeof c?'undefined':_typeof(c))?f:'')+']',c,d)}):'object'===('undefined'==typeof c?'undefined':_typeof(c))?Object.keys(c).forEach(function(e){return b.buildQueryParams(a+'['+e+']',c[e],d)}):d(a,c)},this.getRoute=function(a){var c=b.contextRouting.prefix+a;if(!!b.routesRouting[c])return b.routesRouting[c];else if(!b.routesRouting[a])throw new Error('The route "'+a+'" does not exist.');return b.routesRouting[a]},this.generate=function(a,c,d){var e=b.getRoute(a),f=c||{},g=_extends({},f),h='_scheme',i='',j=!0,k='';if((e.tokens||[]).forEach(function(b){if('text'===b[0])return i=b[1]+i,void(j=!1);if('variable'===b[0]){var c=(e.defaults||{})[b[3]];if(!1==j||!c||(f||{})[b[3]]&&f[b[3]]!==e.defaults[b[3]]){var d;if((f||{})[b[3]])d=f[b[3]],delete g[b[3]];else if(c)d=e.defaults[b[3]];else{if(j)return;throw new Error('The route "'+a+'" requires the parameter "'+b[3]+'".')}var h=!0===d||!1===d||''===d;if(!h||!j){var k=encodeURIComponent(d).replace(/%2F/g,'/');'null'===k&&null===d&&(k=''),i=b[1]+k+i}j=!1}else c&&delete g[b[3]];return}throw new Error('The token type "'+b[0]+'" is not supported.')}),''==i&&(i='/'),(e.hosttokens||[]).forEach(function(a){var b;return'text'===a[0]?void(k=a[1]+k):void('variable'===a[0]&&((f||{})[a[3]]?(b=f[a[3]],delete g[a[3]]):e.defaults[a[3]]&&(b=e.defaults[a[3]]),k=a[1]+b+k))}),i=b.contextRouting.base_url+i,e.requirements[h]&&b.getScheme()!==e.requirements[h]?i=e.requirements[h]+'://'+(k||b.getHost())+i:k&&b.getHost()!==k?i=b.getScheme()+'://'+k+i:!0===d&&(i=b.getScheme()+'://'+b.getHost()+i),0<Object.keys(g).length){var l=[],m=function(a,b){var c=b;c='function'==typeof c?c():c,c=null===c?'':c,l.push(encodeURIComponent(a)+'='+encodeURIComponent(c))};Object.keys(g).forEach(function(a){return b.buildQueryParams(a,g[a],m)}),i=i+'?'+l.join('&').replace(/%20/g,'+')}return i},this.setData=function(a){b.setBaseUrl(a.base_url),b.setRoutes(a.routes),'prefix'in a&&b.setPrefix(a.prefix),b.setHost(a.host),b.setScheme(a.scheme)},this.contextRouting={base_url:'',prefix:'',host:'',scheme:''}};module.exports=new Routing;

/***/ }),

/***/ "./node_modules/lodash.escaperegexp/index.js":
/*!***************************************************!*\
  !*** ./node_modules/lodash.escaperegexp/index.js ***!
  \***************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

/**
 * lodash (Custom Build) <https://lodash.com/>
 * Build: `lodash modularize exports="npm" -o ./`
 * Copyright jQuery Foundation and other contributors <https://jquery.org/>
 * Released under MIT license <https://lodash.com/license>
 * Based on Underscore.js 1.8.3 <http://underscorejs.org/LICENSE>
 * Copyright Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
 */

/** Used as references for various `Number` constants. */
var INFINITY = 1 / 0;

/** `Object#toString` result references. */
var symbolTag = '[object Symbol]';

/**
 * Used to match `RegExp`
 * [syntax characters](http://ecma-international.org/ecma-262/6.0/#sec-patterns).
 */
var reRegExpChar = /[\\^$.*+?()[\]{}|]/g,
    reHasRegExpChar = RegExp(reRegExpChar.source);

/** Detect free variable `global` from Node.js. */
var freeGlobal = typeof __webpack_require__.g == 'object' && __webpack_require__.g && __webpack_require__.g.Object === Object && __webpack_require__.g;

/** Detect free variable `self`. */
var freeSelf = typeof self == 'object' && self && self.Object === Object && self;

/** Used as a reference to the global object. */
var root = freeGlobal || freeSelf || Function('return this')();

/** Used for built-in method references. */
var objectProto = Object.prototype;

/**
 * Used to resolve the
 * [`toStringTag`](http://ecma-international.org/ecma-262/6.0/#sec-object.prototype.tostring)
 * of values.
 */
var objectToString = objectProto.toString;

/** Built-in value references. */
var Symbol = root.Symbol;

/** Used to convert symbols to primitives and strings. */
var symbolProto = Symbol ? Symbol.prototype : undefined,
    symbolToString = symbolProto ? symbolProto.toString : undefined;

/**
 * The base implementation of `_.toString` which doesn't convert nullish
 * values to empty strings.
 *
 * @private
 * @param {*} value The value to process.
 * @returns {string} Returns the string.
 */
function baseToString(value) {
  // Exit early for strings to avoid a performance hit in some environments.
  if (typeof value == 'string') {
    return value;
  }
  if (isSymbol(value)) {
    return symbolToString ? symbolToString.call(value) : '';
  }
  var result = (value + '');
  return (result == '0' && (1 / value) == -INFINITY) ? '-0' : result;
}

/**
 * Checks if `value` is object-like. A value is object-like if it's not `null`
 * and has a `typeof` result of "object".
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is object-like, else `false`.
 * @example
 *
 * _.isObjectLike({});
 * // => true
 *
 * _.isObjectLike([1, 2, 3]);
 * // => true
 *
 * _.isObjectLike(_.noop);
 * // => false
 *
 * _.isObjectLike(null);
 * // => false
 */
function isObjectLike(value) {
  return !!value && typeof value == 'object';
}

/**
 * Checks if `value` is classified as a `Symbol` primitive or object.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a symbol, else `false`.
 * @example
 *
 * _.isSymbol(Symbol.iterator);
 * // => true
 *
 * _.isSymbol('abc');
 * // => false
 */
function isSymbol(value) {
  return typeof value == 'symbol' ||
    (isObjectLike(value) && objectToString.call(value) == symbolTag);
}

/**
 * Converts `value` to a string. An empty string is returned for `null`
 * and `undefined` values. The sign of `-0` is preserved.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to process.
 * @returns {string} Returns the string.
 * @example
 *
 * _.toString(null);
 * // => ''
 *
 * _.toString(-0);
 * // => '-0'
 *
 * _.toString([1, 2, 3]);
 * // => '1,2,3'
 */
function toString(value) {
  return value == null ? '' : baseToString(value);
}

/**
 * Escapes the `RegExp` special characters "^", "$", "\", ".", "*", "+",
 * "?", "(", ")", "[", "]", "{", "}", and "|" in `string`.
 *
 * @static
 * @memberOf _
 * @since 3.0.0
 * @category String
 * @param {string} [string=''] The string to escape.
 * @returns {string} Returns the escaped string.
 * @example
 *
 * _.escapeRegExp('[lodash](https://lodash.com/)');
 * // => '\[lodash\]\(https://lodash\.com/\)'
 */
function escapeRegExp(string) {
  string = toString(string);
  return (string && reHasRegExpChar.test(string))
    ? string.replace(reRegExpChar, '\\$&')
    : string;
}

module.exports = escapeRegExp;


/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/global */
/******/ 	(() => {
/******/ 		__webpack_require__.g = (function() {
/******/ 			if (typeof globalThis === 'object') return globalThis;
/******/ 			try {
/******/ 				return this || new Function('return this')();
/******/ 			} catch (e) {
/******/ 				if (typeof window === 'object') return window;
/******/ 			}
/******/ 		})();
/******/ 	})();
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be in strict mode.
(() => {
"use strict";
/*!********************************!*\
  !*** ./js/pages/order/view.js ***!
  \********************************/


var _OrderViewPageMap = __webpack_require__(/*! @pages/order/OrderViewPageMap */ "./js/pages/order/OrderViewPageMap.js");

var _OrderViewPageMap2 = _interopRequireDefault(_OrderViewPageMap);

var _orderShippingManager = __webpack_require__(/*! @pages/order/order-shipping-manager */ "./js/pages/order/order-shipping-manager.js");

var _orderShippingManager2 = _interopRequireDefault(_orderShippingManager);

var _invoiceNoteManager = __webpack_require__(/*! @pages/order/invoice-note-manager */ "./js/pages/order/invoice-note-manager.js");

var _invoiceNoteManager2 = _interopRequireDefault(_invoiceNoteManager);

var _orderViewPage = __webpack_require__(/*! @pages/order/view/order-view-page */ "./js/pages/order/view/order-view-page.js");

var _orderViewPage2 = _interopRequireDefault(_orderViewPage);

var _orderProductAddAutocomplete = __webpack_require__(/*! @pages/order/view/order-product-add-autocomplete */ "./js/pages/order/view/order-product-add-autocomplete.js");

var _orderProductAddAutocomplete2 = _interopRequireDefault(_orderProductAddAutocomplete);

var _orderProductAdd = __webpack_require__(/*! @pages/order/view/order-product-add */ "./js/pages/order/view/order-product-add.js");

var _orderProductAdd2 = _interopRequireDefault(_orderProductAdd);

var _textWithLengthCounter = __webpack_require__(/*! @components/form/text-with-length-counter */ "./js/components/form/text-with-length-counter.js");

var _textWithLengthCounter2 = _interopRequireDefault(_textWithLengthCounter);

var _orderViewPageMessagesHandler = __webpack_require__(/*! ./message/order-view-page-messages-handler */ "./js/pages/order/message/order-view-page-messages-handler.js");

var _orderViewPageMessagesHandler2 = _interopRequireDefault(_orderViewPageMessagesHandler);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 */

var _window = window,
    $ = _window.$;


$(function () {
  var DISCOUNT_TYPE_AMOUNT = 'amount';
  var DISCOUNT_TYPE_PERCENT = 'percent';
  var DISCOUNT_TYPE_FREE_SHIPPING = 'free_shipping';

  new _orderShippingManager2.default();
  new _textWithLengthCounter2.default();
  var orderViewPage = new _orderViewPage2.default();
  var orderAddAutocomplete = new _orderProductAddAutocomplete2.default($(_OrderViewPageMap2.default.productSearchInput));
  var orderAdd = new _orderProductAdd2.default();

  orderViewPage.listenForProductPack();
  orderViewPage.listenForProductDelete();
  orderViewPage.listenForProductEdit();
  orderViewPage.listenForProductAdd();
  orderViewPage.listenForProductPagination();
  orderViewPage.listenForRefund();
  orderViewPage.listenForCancelProduct();

  orderAddAutocomplete.listenForSearch();
  orderAddAutocomplete.onItemClickedCallback = function (product) {
    return orderAdd.setProduct(product);
  };

  handlePaymentDetailsToggle();
  handlePrivateNoteChange();
  handleOrderNoteChange();
  handleUpdateOrderStatusButton();

  new _invoiceNoteManager2.default();
  var orderViewPageMessageHandler = new _orderViewPageMessagesHandler2.default();
  orderViewPageMessageHandler.listenForPredefinedMessageSelection();
  orderViewPageMessageHandler.listenForFullMessagesOpen();
  $(_OrderViewPageMap2.default.privateNoteToggleBtn).on('click', function (event) {
    event.preventDefault();
    togglePrivateNoteBlock();
  });

  $(_OrderViewPageMap2.default.orderNoteToggleBtn).on('click', function (event) {
    event.preventDefault();
    toggleOrderNoteBlock();
  });

  $(_OrderViewPageMap2.default.printOrderViewPageButton).on('click', function () {
    var tempTitle = document.title;
    document.title = $(_OrderViewPageMap2.default.mainDiv).data('orderTitle');
    window.print();
    document.title = tempTitle;
  });

  initAddCartRuleFormHandler();
  initChangeAddressFormHandler();
  initHookTabs();

  function initHookTabs() {
    $(_OrderViewPageMap2.default.orderHookTabsContainer).find('.nav-tabs li:first-child a').tab('show');
  }

  function handlePaymentDetailsToggle() {
    $(_OrderViewPageMap2.default.orderPaymentDetailsBtn).on('click', function (event) {
      var $paymentDetailRow = $(event.currentTarget).closest('tr').next(':first');

      $paymentDetailRow.toggleClass('d-none');
    });
  }

  function togglePrivateNoteBlock() {
    var $block = $(_OrderViewPageMap2.default.privateNoteBlock);
    var $btn = $(_OrderViewPageMap2.default.privateNoteToggleBtn);
    var isPrivateNoteOpened = $btn.hasClass('is-opened');

    if (isPrivateNoteOpened) {
      $btn.removeClass('is-opened');
      $block.addClass('d-none');
    } else {
      $btn.addClass('is-opened');
      $block.removeClass('d-none');
    }

    var $icon = $btn.find('.material-icons');
    $icon.text(isPrivateNoteOpened ? 'add' : 'remove');
  }

  function handlePrivateNoteChange() {
    var $submitBtn = $(_OrderViewPageMap2.default.privateNoteSubmitBtn);

    $(_OrderViewPageMap2.default.privateNoteInput).on('input', function () {
      $submitBtn.prop('disabled', false);
    });
  }

  function toggleOrderNoteBlock() {
    var $block = $(_OrderViewPageMap2.default.orderNoteBlock);
    var $btn = $(_OrderViewPageMap2.default.orderNoteToggleBtn);
    var isNoteOpened = $btn.hasClass('is-opened');

    $btn.toggleClass('is-opened', !isNoteOpened);
    $block.toggleClass('d-none', isNoteOpened);

    var $icon = $btn.find('.material-icons');
    $icon.text(isNoteOpened ? 'add' : 'remove');
  }

  function handleOrderNoteChange() {
    var $submitBtn = $(_OrderViewPageMap2.default.orderNoteSubmitBtn);

    $(_OrderViewPageMap2.default.orderNoteInput).on('input', function () {
      $submitBtn.prop('disabled', false);
    });
  }

  function initAddCartRuleFormHandler() {
    var $modal = $(_OrderViewPageMap2.default.addCartRuleModal);
    var $form = $modal.find('form');
    var $invoiceSelect = $modal.find(_OrderViewPageMap2.default.addCartRuleInvoiceIdSelect);
    var $valueHelp = $modal.find(_OrderViewPageMap2.default.cartRuleHelpText);
    var $valueInput = $form.find(_OrderViewPageMap2.default.addCartRuleValueInput);
    var $valueFormGroup = $valueInput.closest('.form-group');

    $modal.on('shown.bs.modal', function () {
      $(_OrderViewPageMap2.default.addCartRuleSubmit).attr('disabled', true);
    });

    $form.find(_OrderViewPageMap2.default.addCartRuleNameInput).on('keyup', function (event) {
      var cartRuleName = $(event.currentTarget).val();
      $(_OrderViewPageMap2.default.addCartRuleSubmit).attr('disabled', cartRuleName.trim().length === 0);
    });

    $form.find(_OrderViewPageMap2.default.addCartRuleApplyOnAllInvoicesCheckbox).on('change', function (event) {
      var isChecked = $(event.currentTarget).is(':checked');
      $invoiceSelect.attr('disabled', isChecked);
    });

    $form.find(_OrderViewPageMap2.default.addCartRuleTypeSelect).on('change', function (event) {
      var selectedCartRuleType = $(event.currentTarget).val();
      var $valueUnit = $form.find(_OrderViewPageMap2.default.addCartRuleValueUnit);

      if (selectedCartRuleType === DISCOUNT_TYPE_AMOUNT) {
        $valueHelp.removeClass('d-none');
        $valueUnit.html($valueUnit.data('currencySymbol'));
      } else {
        $valueHelp.addClass('d-none');
      }

      if (selectedCartRuleType === DISCOUNT_TYPE_PERCENT) {
        $valueUnit.html('%');
      }

      if (selectedCartRuleType === DISCOUNT_TYPE_FREE_SHIPPING) {
        $valueFormGroup.addClass('d-none');
        $valueInput.attr('disabled', true);
      } else {
        $valueFormGroup.removeClass('d-none');
        $valueInput.attr('disabled', false);
      }
    });
  }

  function handleUpdateOrderStatusButton() {
    var $btn = $(_OrderViewPageMap2.default.updateOrderStatusActionBtn);
    var $wrapper = $(_OrderViewPageMap2.default.updateOrderStatusActionInputWrapper);

    $(_OrderViewPageMap2.default.updateOrderStatusActionInput).on('change', function (event) {
      var $element = $(event.currentTarget);
      var $option = $('option:selected', $element);
      var selectedOrderStatusId = $element.val();

      $wrapper.css('background-color', $option.data('background-color'));
      $wrapper.toggleClass('is-bright', $option.data('is-bright') !== undefined);

      $btn.prop('disabled', parseInt(selectedOrderStatusId, 10) === $btn.data('orderStatusId'));
    });
  }

  function initChangeAddressFormHandler() {
    var $modal = $(_OrderViewPageMap2.default.updateCustomerAddressModal);

    $(_OrderViewPageMap2.default.openOrderAddressUpdateModalBtn).on('click', function (event) {
      $modal.find(_OrderViewPageMap2.default.updateOrderAddressTypeInput).val($(event.currentTarget).data('addressType'));
    });
  }
});
})();

window.order_view = __webpack_exports__;
/******/ })()
;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9bbmFtZV0vLi9qcy9hcHAvY2xkci9leGNlcHRpb24vbG9jYWxpemF0aW9uLmpzIiwid2VicGFjazovL1tuYW1lXS8uL2pzL2FwcC9jbGRyL2luZGV4LmpzIiwid2VicGFjazovL1tuYW1lXS8uL2pzL2FwcC9jbGRyL251bWJlci1mb3JtYXR0ZXIuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vanMvYXBwL2NsZHIvbnVtYmVyLXN5bWJvbC5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9qcy9hcHAvY2xkci9zcGVjaWZpY2F0aW9ucy9udW1iZXIuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vanMvYXBwL2NsZHIvc3BlY2lmaWNhdGlvbnMvcHJpY2UuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vanMvY29tcG9uZW50cy9ldmVudC1lbWl0dGVyLmpzIiwid2VicGFjazovL1tuYW1lXS8uL2pzL2NvbXBvbmVudHMvZm9ybS90ZXh0LXdpdGgtbGVuZ3RoLWNvdW50ZXIuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vanMvY29tcG9uZW50cy9tb2RhbC5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9qcy9jb21wb25lbnRzL3JvdXRlci5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9qcy9wYWdlcy9vcmRlci9PcmRlclZpZXdQYWdlTWFwLmpzIiwid2VicGFjazovL1tuYW1lXS8uL2pzL3BhZ2VzL29yZGVyL2ludm9pY2Utbm90ZS1tYW5hZ2VyLmpzIiwid2VicGFjazovL1tuYW1lXS8uL2pzL3BhZ2VzL29yZGVyL21lc3NhZ2Uvb3JkZXItdmlldy1wYWdlLW1lc3NhZ2VzLWhhbmRsZXIuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vanMvcGFnZXMvb3JkZXIvb3JkZXItc2hpcHBpbmctbWFuYWdlci5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9qcy9wYWdlcy9vcmRlci92aWV3L29yZGVyLWRpc2NvdW50cy1yZWZyZXNoZXIuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vanMvcGFnZXMvb3JkZXIvdmlldy9vcmRlci1kb2N1bWVudHMtcmVmcmVzaGVyLmpzIiwid2VicGFjazovL1tuYW1lXS8uL2pzL3BhZ2VzL29yZGVyL3ZpZXcvb3JkZXItaW52b2ljZXMtcmVmcmVzaGVyLmpzIiwid2VicGFjazovL1tuYW1lXS8uL2pzL3BhZ2VzL29yZGVyL3ZpZXcvb3JkZXItcGF5bWVudHMtcmVmcmVzaGVyLmpzIiwid2VicGFjazovL1tuYW1lXS8uL2pzL3BhZ2VzL29yZGVyL3ZpZXcvb3JkZXItcHJpY2VzLXJlZnJlc2hlci5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9qcy9wYWdlcy9vcmRlci92aWV3L29yZGVyLXByaWNlcy5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9qcy9wYWdlcy9vcmRlci92aWV3L29yZGVyLXByb2R1Y3QtYWRkLWF1dG9jb21wbGV0ZS5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9qcy9wYWdlcy9vcmRlci92aWV3L29yZGVyLXByb2R1Y3QtYWRkLmpzIiwid2VicGFjazovL1tuYW1lXS8uL2pzL3BhZ2VzL29yZGVyL3ZpZXcvb3JkZXItcHJvZHVjdC1jYW5jZWwuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vanMvcGFnZXMvb3JkZXIvdmlldy9vcmRlci1wcm9kdWN0LWVkaXQuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vanMvcGFnZXMvb3JkZXIvdmlldy9vcmRlci1wcm9kdWN0LW1hbmFnZXIuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vanMvcGFnZXMvb3JkZXIvdmlldy9vcmRlci1wcm9kdWN0LXJlbmRlcmVyLmpzIiwid2VicGFjazovL1tuYW1lXS8uL2pzL3BhZ2VzL29yZGVyL3ZpZXcvb3JkZXItc2hpcHBpbmctcmVmcmVzaGVyLmpzIiwid2VicGFjazovL1tuYW1lXS8uL2pzL3BhZ2VzL29yZGVyL3ZpZXcvb3JkZXItdmlldy1ldmVudC1tYXAuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vanMvcGFnZXMvb3JkZXIvdmlldy9vcmRlci12aWV3LXBhZ2UuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2JhYmVsLXJ1bnRpbWUvY29yZS1qcy9hcnJheS9mcm9tLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9iYWJlbC1ydW50aW1lL2NvcmUtanMvZ2V0LWl0ZXJhdG9yLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9iYWJlbC1ydW50aW1lL2NvcmUtanMvaXMtaXRlcmFibGUuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2JhYmVsLXJ1bnRpbWUvY29yZS1qcy9udW1iZXIvaXMtbmFuLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9iYWJlbC1ydW50aW1lL2NvcmUtanMvb2JqZWN0L2Fzc2lnbi5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvYmFiZWwtcnVudGltZS9jb3JlLWpzL29iamVjdC9jcmVhdGUuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2JhYmVsLXJ1bnRpbWUvY29yZS1qcy9vYmplY3QvZGVmaW5lLXByb3BlcnR5LmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9iYWJlbC1ydW50aW1lL2NvcmUtanMvb2JqZWN0L2dldC1wcm90b3R5cGUtb2YuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2JhYmVsLXJ1bnRpbWUvY29yZS1qcy9vYmplY3Qva2V5cy5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvYmFiZWwtcnVudGltZS9jb3JlLWpzL29iamVjdC9zZXQtcHJvdG90eXBlLW9mLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9iYWJlbC1ydW50aW1lL2NvcmUtanMvb2JqZWN0L3ZhbHVlcy5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvYmFiZWwtcnVudGltZS9jb3JlLWpzL3N5bWJvbC5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvYmFiZWwtcnVudGltZS9jb3JlLWpzL3N5bWJvbC9pdGVyYXRvci5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvYmFiZWwtcnVudGltZS9oZWxwZXJzL2NsYXNzQ2FsbENoZWNrLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9iYWJlbC1ydW50aW1lL2hlbHBlcnMvY3JlYXRlQ2xhc3MuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2JhYmVsLXJ1bnRpbWUvaGVscGVycy9pbmhlcml0cy5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvYmFiZWwtcnVudGltZS9oZWxwZXJzL3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4uanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2JhYmVsLXJ1bnRpbWUvaGVscGVycy9zbGljZWRUb0FycmF5LmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9iYWJlbC1ydW50aW1lL2hlbHBlcnMvdG9Db25zdW1hYmxlQXJyYXkuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2JhYmVsLXJ1bnRpbWUvaGVscGVycy90eXBlb2YuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9mbi9hcnJheS9mcm9tLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvZm4vZ2V0LWl0ZXJhdG9yLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvZm4vaXMtaXRlcmFibGUuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9mbi9udW1iZXIvaXMtbmFuLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvZm4vb2JqZWN0L2Fzc2lnbi5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L2ZuL29iamVjdC9jcmVhdGUuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9mbi9vYmplY3QvZGVmaW5lLXByb3BlcnR5LmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvZm4vb2JqZWN0L2dldC1wcm90b3R5cGUtb2YuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9mbi9vYmplY3Qva2V5cy5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L2ZuL29iamVjdC9zZXQtcHJvdG90eXBlLW9mLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvZm4vb2JqZWN0L3ZhbHVlcy5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L2ZuL3N5bWJvbC9pbmRleC5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L2ZuL3N5bWJvbC9pdGVyYXRvci5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX2EtZnVuY3Rpb24uanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19hZGQtdG8tdW5zY29wYWJsZXMuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19hbi1vYmplY3QuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19hcnJheS1pbmNsdWRlcy5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX2NsYXNzb2YuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19jb2YuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19jb3JlLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fY3JlYXRlLXByb3BlcnR5LmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fY3R4LmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fZGVmaW5lZC5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX2Rlc2NyaXB0b3JzLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fZG9tLWNyZWF0ZS5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX2VudW0tYnVnLWtleXMuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19lbnVtLWtleXMuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19leHBvcnQuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19mYWlscy5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX2dsb2JhbC5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX2hhcy5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX2hpZGUuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19odG1sLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9faWU4LWRvbS1kZWZpbmUuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19pb2JqZWN0LmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9faXMtYXJyYXktaXRlci5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX2lzLWFycmF5LmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9faXMtb2JqZWN0LmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9faXRlci1jYWxsLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9faXRlci1jcmVhdGUuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19pdGVyLWRlZmluZS5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX2l0ZXItZGV0ZWN0LmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9faXRlci1zdGVwLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9faXRlcmF0b3JzLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fbGlicmFyeS5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX21ldGEuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19vYmplY3QtYXNzaWduLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fb2JqZWN0LWNyZWF0ZS5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX29iamVjdC1kcC5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX29iamVjdC1kcHMuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19vYmplY3QtZ29wZC5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX29iamVjdC1nb3BuLWV4dC5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX29iamVjdC1nb3BuLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fb2JqZWN0LWdvcHMuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19vYmplY3QtZ3BvLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fb2JqZWN0LWtleXMtaW50ZXJuYWwuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19vYmplY3Qta2V5cy5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX29iamVjdC1waWUuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19vYmplY3Qtc2FwLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fb2JqZWN0LXRvLWFycmF5LmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fcHJvcGVydHktZGVzYy5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX3JlZGVmaW5lLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fc2V0LXByb3RvLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fc2V0LXRvLXN0cmluZy10YWcuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19zaGFyZWQta2V5LmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fc2hhcmVkLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fc3RyaW5nLWF0LmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fdG8tYWJzb2x1dGUtaW5kZXguanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL190by1pbnRlZ2VyLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fdG8taW9iamVjdC5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX3RvLWxlbmd0aC5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX3RvLW9iamVjdC5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX3RvLXByaW1pdGl2ZS5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX3VpZC5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX3drcy1kZWZpbmUuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL193a3MtZXh0LmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fd2tzLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9jb3JlLmdldC1pdGVyYXRvci1tZXRob2QuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL2NvcmUuZ2V0LWl0ZXJhdG9yLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9jb3JlLmlzLWl0ZXJhYmxlLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9lczYuYXJyYXkuZnJvbS5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvZXM2LmFycmF5Lml0ZXJhdG9yLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9lczYubnVtYmVyLmlzLW5hbi5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvZXM2Lm9iamVjdC5hc3NpZ24uanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL2VzNi5vYmplY3QuY3JlYXRlLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9lczYub2JqZWN0LmRlZmluZS1wcm9wZXJ0eS5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvZXM2Lm9iamVjdC5nZXQtcHJvdG90eXBlLW9mLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9lczYub2JqZWN0LmtleXMuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL2VzNi5vYmplY3Quc2V0LXByb3RvdHlwZS1vZi5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvZXM2LnN0cmluZy5pdGVyYXRvci5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvZXM2LnN5bWJvbC5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvZXM3Lm9iamVjdC52YWx1ZXMuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL2VzNy5zeW1ib2wuYXN5bmMtaXRlcmF0b3IuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL2VzNy5zeW1ib2wub2JzZXJ2YWJsZS5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvd2ViLmRvbS5pdGVyYWJsZS5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvZXZlbnRzL2V2ZW50cy5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvZm9zLXJvdXRpbmcvZGlzdC9yb3V0aW5nLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9sb2Rhc2guZXNjYXBlcmVnZXhwL2luZGV4LmpzIiwid2VicGFjazovL1tuYW1lXS93ZWJwYWNrL2Jvb3RzdHJhcCIsIndlYnBhY2s6Ly9bbmFtZV0vd2VicGFjay9ydW50aW1lL2dsb2JhbCIsIndlYnBhY2s6Ly9bbmFtZV0vLi9qcy9wYWdlcy9vcmRlci92aWV3LmpzIl0sIm5hbWVzIjpbIkxvY2FsaXphdGlvbkV4Y2VwdGlvbiIsIm1lc3NhZ2UiLCJuYW1lIiwiUHJpY2VTcGVjaWZpY2F0aW9uIiwiTnVtYmVyU3BlY2lmaWNhdGlvbiIsIk51bWJlckZvcm1hdHRlciIsIk51bWJlclN5bWJvbCIsImVzY2FwZVJFIiwicmVxdWlyZSIsIkNVUlJFTkNZX1NZTUJPTF9QTEFDRUhPTERFUiIsIkRFQ0lNQUxfU0VQQVJBVE9SX1BMQUNFSE9MREVSIiwiR1JPVVBfU0VQQVJBVE9SX1BMQUNFSE9MREVSIiwiTUlOVVNfU0lHTl9QTEFDRUhPTERFUiIsIlBFUkNFTlRfU1lNQk9MX1BMQUNFSE9MREVSIiwiUExVU19TSUdOX1BMQUNFSE9MREVSIiwic3BlY2lmaWNhdGlvbiIsIm51bWJlclNwZWNpZmljYXRpb24iLCJudW1iZXIiLCJ1bmRlZmluZWQiLCJudW0iLCJNYXRoIiwiYWJzIiwidG9GaXhlZCIsImdldE1heEZyYWN0aW9uRGlnaXRzIiwiZXh0cmFjdE1ham9yTWlub3JEaWdpdHMiLCJtYWpvckRpZ2l0cyIsIm1pbm9yRGlnaXRzIiwic3BsaXRNYWpvckdyb3VwcyIsImFkanVzdE1pbm9yRGlnaXRzWmVyb2VzIiwiZm9ybWF0dGVkTnVtYmVyIiwicGF0dGVybiIsImdldENsZHJQYXR0ZXJuIiwiYWRkUGxhY2Vob2xkZXJzIiwicmVwbGFjZVN5bWJvbHMiLCJwZXJmb3JtU3BlY2lmaWNSZXBsYWNlbWVudHMiLCJyZXN1bHQiLCJ0b1N0cmluZyIsInNwbGl0IiwiZGlnaXQiLCJpc0dyb3VwaW5nVXNlZCIsInJldmVyc2UiLCJncm91cHMiLCJwdXNoIiwic3BsaWNlIiwiZ2V0UHJpbWFyeUdyb3VwU2l6ZSIsImxlbmd0aCIsImdldFNlY29uZGFyeUdyb3VwU2l6ZSIsIm5ld0dyb3VwcyIsImZvckVhY2giLCJncm91cCIsImpvaW4iLCJyZXBsYWNlIiwiZ2V0TWluRnJhY3Rpb25EaWdpdHMiLCJwYWRFbmQiLCJpc05lZ2F0aXZlIiwiZ2V0TmVnYXRpdmVQYXR0ZXJuIiwiZ2V0UG9zaXRpdmVQYXR0ZXJuIiwic3ltYm9scyIsImdldFN5bWJvbCIsIm1hcCIsImdldERlY2ltYWwiLCJnZXRHcm91cCIsImdldE1pbnVzU2lnbiIsImdldFBlcmNlbnRTaWduIiwiZ2V0UGx1c1NpZ24iLCJzdHJ0ciIsInN0ciIsInBhaXJzIiwic3Vic3RycyIsIlJlZ0V4cCIsInBhcnQiLCJnZXRDdXJyZW5jeVN5bWJvbCIsInNwZWNpZmljYXRpb25zIiwic3ltYm9sIiwibnVtYmVyU3ltYm9scyIsImN1cnJlbmN5U3ltYm9sIiwicG9zaXRpdmVQYXR0ZXJuIiwibmVnYXRpdmVQYXR0ZXJuIiwicGFyc2VJbnQiLCJtYXhGcmFjdGlvbkRpZ2l0cyIsIm1pbkZyYWN0aW9uRGlnaXRzIiwiZ3JvdXBpbmdVc2VkIiwicHJpbWFyeUdyb3VwU2l6ZSIsInNlY29uZGFyeUdyb3VwU2l6ZSIsImN1cnJlbmN5Q29kZSIsImRlY2ltYWwiLCJsaXN0IiwicGVyY2VudFNpZ24iLCJtaW51c1NpZ24iLCJwbHVzU2lnbiIsImV4cG9uZW50aWFsIiwic3VwZXJzY3JpcHRpbmdFeHBvbmVudCIsInBlck1pbGxlIiwiaW5maW5pdHkiLCJuYW4iLCJ2YWxpZGF0ZURhdGEiLCJDVVJSRU5DWV9ESVNQTEFZX1NZTUJPTCIsIkV2ZW50RW1pdHRlciIsIkV2ZW50RW1pdHRlckNsYXNzIiwid2luZG93IiwiJCIsIlRleHRXaXRoTGVuZ3RoQ291bnRlciIsIndyYXBwZXJTZWxlY3RvciIsInRleHRTZWxlY3RvciIsImlucHV0U2VsZWN0b3IiLCJkb2N1bWVudCIsIm9uIiwiZSIsIiRpbnB1dCIsImN1cnJlbnRUYXJnZXQiLCJyZW1haW5pbmdMZW5ndGgiLCJkYXRhIiwidmFsIiwiY2xvc2VzdCIsImZpbmQiLCJ0ZXh0IiwiQ29uZmlybU1vZGFsIiwicGFyYW1zIiwiY29uZmlybUNhbGxiYWNrIiwiY2FuY2VsQ2FsbGJhY2siLCJpZCIsImNsb3NhYmxlIiwibW9kYWwiLCJNb2RhbCIsIiRtb2RhbCIsImNvbnRhaW5lciIsInNob3ciLCJjb25maXJtQnV0dG9uIiwiYWRkRXZlbnRMaXN0ZW5lciIsImJhY2tkcm9wIiwia2V5Ym9hcmQiLCJxdWVyeVNlbGVjdG9yIiwicmVtb3ZlIiwiYm9keSIsImFwcGVuZENoaWxkIiwiY29uZmlybVRpdGxlIiwiY29uZmlybU1lc3NhZ2UiLCJjbG9zZUJ1dHRvbkxhYmVsIiwiY29uZmlybUJ1dHRvbkxhYmVsIiwiY29uZmlybUJ1dHRvbkNsYXNzIiwiY3VzdG9tQnV0dG9ucyIsImNyZWF0ZUVsZW1lbnQiLCJjbGFzc0xpc3QiLCJhZGQiLCJkaWFsb2ciLCJjb250ZW50IiwiaGVhZGVyIiwidGl0bGUiLCJpbm5lckhUTUwiLCJjbG9zZUljb24iLCJzZXRBdHRyaWJ1dGUiLCJkYXRhc2V0IiwiZGlzbWlzcyIsImZvb3RlciIsImNsb3NlQnV0dG9uIiwiYXBwZW5kIiwiUm91dGVyIiwicHJlc3Rhc2hvcCIsImN1c3RvbVJvdXRlcyIsInJvdXRlcyIsIlJvdXRpbmciLCJzZXREYXRhIiwic2V0QmFzZVVybCIsInJvdXRlIiwidG9rZW5pemVkUGFyYW1zIiwiX3Rva2VuIiwiZ2VuZXJhdGUiLCJtYWluRGl2Iiwib3JkZXJQYXltZW50RGV0YWlsc0J0biIsIm9yZGVyUGF5bWVudEZvcm1BbW91bnRJbnB1dCIsIm9yZGVyUGF5bWVudEludm9pY2VTZWxlY3QiLCJ2aWV3T3JkZXJQYXltZW50c0Jsb2NrIiwidmlld09yZGVyUGF5bWVudHNBbGVydCIsInByaXZhdGVOb3RlVG9nZ2xlQnRuIiwicHJpdmF0ZU5vdGVCbG9jayIsInByaXZhdGVOb3RlSW5wdXQiLCJwcml2YXRlTm90ZVN1Ym1pdEJ0biIsImFkZENhcnRSdWxlTW9kYWwiLCJhZGRDYXJ0UnVsZUludm9pY2VJZFNlbGVjdCIsImFkZENhcnRSdWxlTmFtZUlucHV0IiwiYWRkQ2FydFJ1bGVUeXBlU2VsZWN0IiwiYWRkQ2FydFJ1bGVWYWx1ZUlucHV0IiwiYWRkQ2FydFJ1bGVWYWx1ZVVuaXQiLCJhZGRDYXJ0UnVsZVN1Ym1pdCIsImNhcnRSdWxlSGVscFRleHQiLCJ1cGRhdGVPcmRlclN0YXR1c0FjdGlvbkJ0biIsInVwZGF0ZU9yZGVyU3RhdHVzQWN0aW9uSW5wdXQiLCJ1cGRhdGVPcmRlclN0YXR1c0FjdGlvbklucHV0V3JhcHBlciIsInVwZGF0ZU9yZGVyU3RhdHVzQWN0aW9uRm9ybSIsInNob3dPcmRlclNoaXBwaW5nVXBkYXRlTW9kYWxCdG4iLCJ1cGRhdGVPcmRlclNoaXBwaW5nVHJhY2tpbmdOdW1iZXJJbnB1dCIsInVwZGF0ZU9yZGVyU2hpcHBpbmdDdXJyZW50T3JkZXJDYXJyaWVySWRJbnB1dCIsInVwZGF0ZUN1c3RvbWVyQWRkcmVzc01vZGFsIiwib3Blbk9yZGVyQWRkcmVzc1VwZGF0ZU1vZGFsQnRuIiwidXBkYXRlT3JkZXJBZGRyZXNzVHlwZUlucHV0IiwiZGVsaXZlcnlBZGRyZXNzRWRpdEJ0biIsImludm9pY2VBZGRyZXNzRWRpdEJ0biIsIm9yZGVyTWVzc2FnZU5hbWVTZWxlY3QiLCJvcmRlck1lc3NhZ2VzQ29udGFpbmVyIiwib3JkZXJNZXNzYWdlIiwib3JkZXJNZXNzYWdlQ2hhbmdlV2FybmluZyIsIm9yZGVyRG9jdW1lbnRzVGFiQ291bnQiLCJvcmRlckRvY3VtZW50c1RhYkJvZHkiLCJvcmRlclNoaXBwaW5nVGFiQ291bnQiLCJvcmRlclNoaXBwaW5nVGFiQm9keSIsImFsbE1lc3NhZ2VzTW9kYWwiLCJhbGxNZXNzYWdlc0xpc3QiLCJvcGVuQWxsTWVzc2FnZXNCdG4iLCJwcm9kdWN0T3JpZ2luYWxQb3NpdGlvbiIsInByb2R1Y3RNb2RpZmljYXRpb25Qb3NpdGlvbiIsInByb2R1Y3RzUGFuZWwiLCJwcm9kdWN0c0NvdW50IiwicHJvZHVjdERlbGV0ZUJ0biIsInByb2R1Y3RzVGFibGUiLCJwcm9kdWN0c1BhZ2luYXRpb24iLCJwcm9kdWN0c05hdlBhZ2luYXRpb24iLCJwcm9kdWN0c1RhYmxlUGFnaW5hdGlvbiIsInByb2R1Y3RzVGFibGVQYWdpbmF0aW9uTmV4dCIsInByb2R1Y3RzVGFibGVQYWdpbmF0aW9uUHJldiIsInByb2R1Y3RzVGFibGVQYWdpbmF0aW9uTGluayIsInByb2R1Y3RzVGFibGVQYWdpbmF0aW9uQWN0aXZlIiwicHJvZHVjdHNUYWJsZVBhZ2luYXRpb25UZW1wbGF0ZSIsInByb2R1Y3RzVGFibGVQYWdpbmF0aW9uTnVtYmVyU2VsZWN0b3IiLCJwcm9kdWN0c1RhYmxlUm93IiwicHJvZHVjdElkIiwicHJvZHVjdHNUYWJsZVJvd0VkaXRlZCIsInByb2R1Y3RzVGFibGVSb3dzIiwicHJvZHVjdHNDZWxsTG9jYXRpb24iLCJwcm9kdWN0c0NlbGxSZWZ1bmRlZCIsInByb2R1Y3RzQ2VsbExvY2F0aW9uRGlzcGxheWVkIiwicHJvZHVjdHNDZWxsUmVmdW5kZWREaXNwbGF5ZWQiLCJwcm9kdWN0c1RhYmxlQ3VzdG9taXphdGlvblJvd3MiLCJwcm9kdWN0RWRpdEJ1dHRvbnMiLCJwcm9kdWN0RWRpdEJ0biIsInByb2R1Y3RBZGRCdG4iLCJwcm9kdWN0QWN0aW9uQnRuIiwicHJvZHVjdEFkZEFjdGlvbkJ0biIsInByb2R1Y3RDYW5jZWxBZGRCdG4iLCJwcm9kdWN0QWRkUm93IiwicHJvZHVjdFNlYXJjaElucHV0IiwicHJvZHVjdFNlYXJjaElucHV0QXV0b2NvbXBsZXRlIiwicHJvZHVjdFNlYXJjaElucHV0QXV0b2NvbXBsZXRlTWVudSIsInByb2R1Y3RBZGRJZElucHV0IiwicHJvZHVjdEFkZFRheFJhdGVJbnB1dCIsInByb2R1Y3RBZGRDb21iaW5hdGlvbnNCbG9jayIsInByb2R1Y3RBZGRDb21iaW5hdGlvbnNTZWxlY3QiLCJwcm9kdWN0QWRkUHJpY2VUYXhFeGNsSW5wdXQiLCJwcm9kdWN0QWRkUHJpY2VUYXhJbmNsSW5wdXQiLCJwcm9kdWN0QWRkUXVhbnRpdHlJbnB1dCIsInByb2R1Y3RBZGRBdmFpbGFibGVUZXh0IiwicHJvZHVjdEFkZExvY2F0aW9uVGV4dCIsInByb2R1Y3RBZGRUb3RhbFByaWNlVGV4dCIsInByb2R1Y3RBZGRJbnZvaWNlU2VsZWN0IiwicHJvZHVjdEFkZEZyZWVTaGlwcGluZ1NlbGVjdCIsInByb2R1Y3RBZGROZXdJbnZvaWNlSW5mbyIsInByb2R1Y3RFZGl0U2F2ZUJ0biIsInByb2R1Y3RFZGl0Q2FuY2VsQnRuIiwicHJvZHVjdEVkaXRSb3dUZW1wbGF0ZSIsInByb2R1Y3RFZGl0Um93IiwicHJvZHVjdEVkaXRJbWFnZSIsInByb2R1Y3RFZGl0TmFtZSIsInByb2R1Y3RFZGl0VW5pdFByaWNlIiwicHJvZHVjdEVkaXRRdWFudGl0eSIsInByb2R1Y3RFZGl0QXZhaWxhYmxlUXVhbnRpdHkiLCJwcm9kdWN0RWRpdFRvdGFsUHJpY2UiLCJwcm9kdWN0RWRpdFByaWNlVGF4RXhjbElucHV0IiwicHJvZHVjdEVkaXRQcmljZVRheEluY2xJbnB1dCIsInByb2R1Y3RFZGl0SW52b2ljZVNlbGVjdCIsInByb2R1Y3RFZGl0UXVhbnRpdHlJbnB1dCIsInByb2R1Y3RFZGl0TG9jYXRpb25UZXh0IiwicHJvZHVjdEVkaXRBdmFpbGFibGVUZXh0IiwicHJvZHVjdEVkaXRUb3RhbFByaWNlVGV4dCIsInByb2R1Y3REaXNjb3VudExpc3QiLCJwcm9kdWN0UGFja01vZGFsIiwidGFibGUiLCJyb3dzIiwidGVtcGxhdGUiLCJwcm9kdWN0IiwiaW1nIiwibGluayIsInJlZiIsInN1cHBsaWVyUmVmIiwicXVhbnRpdHkiLCJhdmFpbGFibGVRdWFudGl0eSIsIm9yZGVyUHJvZHVjdHNUb3RhbCIsIm9yZGVyRGlzY291bnRzVG90YWxDb250YWluZXIiLCJvcmRlckRpc2NvdW50c1RvdGFsIiwib3JkZXJXcmFwcGluZ1RvdGFsIiwib3JkZXJTaGlwcGluZ1RvdGFsQ29udGFpbmVyIiwib3JkZXJTaGlwcGluZ1RvdGFsIiwib3JkZXJUYXhlc1RvdGFsIiwib3JkZXJUb3RhbCIsIm9yZGVySG9va1RhYnNDb250YWluZXIiLCJjYW5jZWxQcm9kdWN0IiwiZm9ybSIsImJ1dHRvbnMiLCJhYm9ydCIsInNhdmUiLCJwYXJ0aWFsUmVmdW5kIiwic3RhbmRhcmRSZWZ1bmQiLCJyZXR1cm5Qcm9kdWN0IiwiY2FuY2VsUHJvZHVjdHMiLCJpbnB1dHMiLCJhbW91bnQiLCJzZWxlY3RvciIsImNlbGwiLCJhY3Rpb25zIiwiY2hlY2tib3hlcyIsInJlc3RvY2siLCJjcmVkaXRTbGlwIiwidm91Y2hlciIsInJhZGlvcyIsInZvdWNoZXJSZWZ1bmRUeXBlIiwicHJvZHVjdFByaWNlcyIsInByb2R1Y3RQcmljZXNWb3VjaGVyRXhjbHVkZWQiLCJuZWdhdGl2ZUVycm9yTWVzc2FnZSIsInRvZ2dsZSIsInByaW50T3JkZXJWaWV3UGFnZUJ1dHRvbiIsIm9yZGVyTm90ZVRvZ2dsZUJ0biIsIm9yZGVyTm90ZUJsb2NrIiwib3JkZXJOb3RlSW5wdXQiLCJvcmRlck5vdGVTdWJtaXRCdG4iLCJyZWZyZXNoUHJvZHVjdHNMaXN0TG9hZGluZ1NwaW5uZXIiLCJJbnZvaWNlTm90ZU1hbmFnZXIiLCJzZXR1cExpc3RlbmVycyIsImluaXRTaG93Tm90ZUZvcm1FdmVudEhhbmRsZXIiLCJpbml0Q2xvc2VOb3RlRm9ybUV2ZW50SGFuZGxlciIsImluaXRFbnRlclBheW1lbnRFdmVudEhhbmRsZXIiLCJldmVudCIsInByZXZlbnREZWZhdWx0IiwiJGJ0biIsIiRub3RlUm93IiwibmV4dCIsInJlbW92ZUNsYXNzIiwiYWRkQ2xhc3MiLCJwYXltZW50QW1vdW50IiwiT3JkZXJWaWV3UGFnZU1hcCIsImdldCIsInNjcm9sbEludG9WaWV3IiwiYmVoYXZpb3IiLCJPcmRlclZpZXdQYWdlTWVzc2FnZXNIYW5kbGVyIiwiJG9yZGVyTWVzc2FnZUNoYW5nZVdhcm5pbmciLCIkbWVzc2FnZXNDb250YWluZXIiLCJsaXN0ZW5Gb3JQcmVkZWZpbmVkTWVzc2FnZVNlbGVjdGlvbiIsImhhbmRsZVByZWRlZmluZWRNZXNzYWdlU2VsZWN0aW9uIiwibGlzdGVuRm9yRnVsbE1lc3NhZ2VzT3BlbiIsIm9uRnVsbE1lc3NhZ2VzT3BlbiIsIiRjdXJyZW50SXRlbSIsInZhbHVlSWQiLCJ0cmltIiwiJG9yZGVyTWVzc2FnZSIsImlzU2FtZU1lc3NhZ2UiLCJjb25maXJtIiwidHJpZ2dlciIsInNjcm9sbFRvTXNnTGlzdEJvdHRvbSIsIiRtc2dNb2RhbCIsIm1zZ0xpc3QiLCJjbGFzc0NoZWNrSW50ZXJ2YWwiLCJzZXRJbnRlcnZhbCIsImhhc0NsYXNzIiwic2Nyb2xsVG9wIiwic2Nyb2xsSGVpZ2h0IiwiY2xlYXJJbnRlcnZhbCIsIk9yZGVyU2hpcHBpbmdNYW5hZ2VyIiwiaW5pdE9yZGVyU2hpcHBpbmdVcGRhdGVFdmVudEhhbmRsZXIiLCJPcmRlckRpc2NvdW50c1JlZnJlc2hlciIsInJvdXRlciIsIm9yZGVySWQiLCJhamF4IiwidGhlbiIsInJlc3BvbnNlIiwicmVwbGFjZVdpdGgiLCJPcmRlckRvY3VtZW50c1JlZnJlc2hlciIsImludm9pY2VOb3RlTWFuYWdlciIsImdldEpTT04iLCJ0b3RhbCIsImh0bWwiLCJPcmRlckludm9pY2VzUmVmcmVzaGVyIiwiaW52b2ljZXMiLCIkcGF5bWVudEludm9pY2VTZWxlY3QiLCIkYWRkUHJvZHVjdEludm9pY2VTZWxlY3QiLCIkZXhpc3RpbmdJbnZvaWNlc0dyb3VwIiwiJHByb2R1Y3RFZGl0SW52b2ljZVNlbGVjdCIsIiRhZGREaXNjb3VudEludm9pY2VTZWxlY3QiLCJlbXB0eSIsImludm9pY2VOYW1lIiwiaW52b2ljZUlkIiwiaW52b2ljZU5hbWVXaXRob3V0UHJpY2UiLCJzZWxlY3RlZEluZGV4IiwiT3JkZXJQYXltZW50c1JlZnJlc2hlciIsInByZXBlbmQiLCJyZXNwb25zZUpTT04iLCJncm93bCIsImVycm9yIiwiT3JkZXJQcmljZXNSZWZyZXNoZXIiLCJvcmRlclRvdGFsRm9ybWF0dGVkIiwiZGlzY291bnRzQW1vdW50Rm9ybWF0dGVkIiwidG9nZ2xlQ2xhc3MiLCJkaXNjb3VudHNBbW91bnREaXNwbGF5ZWQiLCJwcm9kdWN0c1RvdGFsRm9ybWF0dGVkIiwic2hpcHBpbmdUb3RhbEZvcm1hdHRlZCIsInNoaXBwaW5nVG90YWxEaXNwbGF5ZWQiLCJ0YXhlc1RvdGFsRm9ybWF0dGVkIiwicHJvZHVjdFByaWNlc0xpc3QiLCJvcmRlclByb2R1Y3RUcklkIiwib3JkZXJEZXRhaWxJZCIsIiRxdWFudGl0eSIsIndyYXAiLCJ1bml0UHJpY2UiLCJ0b3RhbFByaWNlIiwicHJvZHVjdEVkaXRCdXR0b24iLCJ1bml0UHJpY2VUYXhJbmNsUmF3IiwidW5pdFByaWNlVGF4RXhjbFJhdyIsImdpdmVuUHJpY2UiLCJjb21iaW5hdGlvbklkIiwicHJvZHVjdFJvd3MiLCJxdWVyeVNlbGVjdG9yQWxsIiwiZXhwZWN0ZWRQcm9kdWN0SWQiLCJOdW1iZXIiLCJleHBlY3RlZENvbWJpbmF0aW9uSWQiLCJleHBlY3RlZEdpdmVuUHJpY2UiLCJ1bm1hdGNoaW5nSW52b2ljZVByaWNlRXhpc3RzIiwidW5tYXRjaGluZ1Byb2R1Y3RQcmljZUV4aXN0cyIsInByb2R1Y3RSb3ciLCJwcm9kdWN0Um93SWQiLCJhdHRyIiwiY3VycmVudE9yZGVySW52b2ljZUlkIiwiY3VycmVudFByb2R1Y3RJZCIsImN1cnJlbnRDb21iaW5hdGlvbklkIiwiT3JkZXJQcmljZXMiLCJ0YXhJbmNsdWRlZCIsInRheFJhdGVQZXJDZW50IiwiY3VycmVuY3lQcmVjaXNpb24iLCJwcmljZVRheEluY2wiLCJwYXJzZUZsb2F0IiwidGF4UmF0ZSIsInBzX3JvdW5kIiwidGF4RXhjbHVkZWQiLCJwcmljZVRheEV4Y2wiLCJPcmRlclByb2R1Y3RBdXRvY29tcGxldGUiLCJpbnB1dCIsImFjdGl2ZVNlYXJjaFJlcXVlc3QiLCJyZXN1bHRzIiwiZHJvcGRvd25NZW51Iiwib25JdGVtQ2xpY2tlZENhbGxiYWNrIiwic3RvcEltbWVkaWF0ZVByb3BhZ2F0aW9uIiwidXBkYXRlUmVzdWx0cyIsImRlbGF5U2VhcmNoIiwiaGlkZSIsImNsZWFyVGltZW91dCIsInNlYXJjaFRpbWVvdXRJZCIsInZhbHVlIiwic2V0VGltZW91dCIsInNlYXJjaCIsImN1cnJlbmN5Iiwic2VhcmNoX3BocmFzZSIsImN1cnJlbmN5X2lkIiwib3JkZXJfaWQiLCJhbHdheXMiLCJwcm9kdWN0cyIsIm9uSXRlbUNsaWNrZWQiLCJ0YXJnZXQiLCJzZWxlY3RlZFByb2R1Y3QiLCJmaWx0ZXIiLCJPcmRlclByb2R1Y3RBZGQiLCJwcm9kdWN0SWRJbnB1dCIsImNvbWJpbmF0aW9uc0Jsb2NrIiwiY29tYmluYXRpb25zU2VsZWN0IiwicHJpY2VUYXhJbmNsdWRlZElucHV0IiwicHJpY2VUYXhFeGNsdWRlZElucHV0IiwidGF4UmF0ZUlucHV0IiwicXVhbnRpdHlJbnB1dCIsImF2YWlsYWJsZVRleHQiLCJsb2NhdGlvblRleHQiLCJ0b3RhbFByaWNlVGV4dCIsImludm9pY2VTZWxlY3QiLCJmcmVlU2hpcHBpbmdTZWxlY3QiLCJwcm9kdWN0QWRkTWVudUJ0biIsImF2YWlsYWJsZSIsInNldHVwTGlzdGVuZXIiLCJwcmljZVRheENhbGN1bGF0b3IiLCJvcmRlclByb2R1Y3RSZW5kZXJlciIsIk9yZGVyUHJvZHVjdFJlbmRlcmVyIiwib3JkZXJQcmljZXNSZWZyZXNoZXIiLCJpc09yZGVyVGF4SW5jbHVkZWQiLCJ0b2dnbGVDb2x1bW4iLCJuZXdRdWFudGl0eSIsInJlbWFpbmluZ0F2YWlsYWJsZSIsImF2YWlsYWJsZU91dE9mU3RvY2siLCJkaXNhYmxlQWRkQWN0aW9uQnRuIiwicHJvcCIsImNhbGN1bGF0ZVRvdGFsUHJpY2UiLCJyZW1vdmVBdHRyIiwiY2FsY3VsYXRlVGF4RXhjbHVkZWQiLCJjYWxjdWxhdGVUYXhJbmNsdWRlZCIsImNvbmZpcm1OZXdJbnZvaWNlIiwidG9nZ2xlUHJvZHVjdEFkZE5ld0ludm9pY2VJbmZvIiwibG9jYXRpb24iLCJzdG9jayIsInNldENvbWJpbmF0aW9ucyIsImNvbWJpbmF0aW9ucyIsImF0dHJpYnV0ZUNvbWJpbmF0aW9uSWQiLCJwcmljZVRheEV4Y2x1ZGVkIiwicHJpY2VUYXhJbmNsdWRlZCIsImF0dHJpYnV0ZSIsInByb2R1Y3RfaWQiLCJjb21iaW5hdGlvbl9pZCIsInByaWNlX3RheF9pbmNsIiwicHJpY2VfdGF4X2V4Y2wiLCJpbnZvaWNlX2lkIiwiZnJlZV9zaGlwcGluZyIsInVybCIsIm1ldGhvZCIsImVtaXQiLCJPcmRlclZpZXdFdmVudE1hcCIsInByb2R1Y3RBZGRlZFRvT3JkZXIiLCJvcmRlclByb2R1Y3RJZCIsIm5ld1JvdyIsImNvbmZpcm1OZXdQcmljZSIsImFkZFByb2R1Y3QiLCJjb21iaW5hdGlvblZhbHVlIiwicHJvZHVjdFByaWNlTWF0Y2giLCJjaGVja090aGVyUHJvZHVjdFByaWNlc01hdGNoIiwibW9kYWxFZGl0UHJpY2UiLCJPcmRlclByb2R1Y3RDYW5jZWwiLCJjYW5jZWxQcm9kdWN0Rm9ybSIsIm9yZGVyRGVsaXZlcmVkIiwiaXNUYXhJbmNsdWRlZCIsImRpc2NvdW50c0Ftb3VudCIsImN1cnJlbmN5Rm9ybWF0dGVyIiwiYnVpbGQiLCJ1c2VBbW91bnRJbnB1dHMiLCJsaXN0ZW5Gb3JJbnB1dHMiLCJoaWRlQ2FuY2VsRWxlbWVudHMiLCJpbml0Rm9ybSIsImFjdGlvbk5hbWUiLCJmb3JtQWN0aW9uIiwiZm9ybUNsYXNzIiwidXBkYXRlVm91Y2hlclJlZnVuZCIsIiRwcm9kdWN0UXVhbnRpdHlJbnB1dCIsIiRwYXJlbnRDZWxsIiwicGFyZW50cyIsIiRwcm9kdWN0QW1vdW50IiwicHJvZHVjdFF1YW50aXR5IiwicHJpY2VGaWVsZE5hbWUiLCJwcm9kdWN0VW5pdFByaWNlIiwiYW1vdW50UmVmdW5kYWJsZSIsImd1ZXNzZWRBbW91bnQiLCJhbW91bnRWYWx1ZSIsInVwZGF0ZUFtb3VudElucHV0IiwiJHByb2R1Y3RDaGVja2JveCIsInByb2R1Y3RRdWFudGl0eUlucHV0IiwicmVmdW5kYWJsZVF1YW50aXR5IiwiaXMiLCJ0b3RhbEFtb3VudCIsImVhY2giLCJpbmRleCIsImZsb2F0VmFsdWUiLCIkcXVhbnRpdHlJbnB1dCIsInJlZnVuZEFtb3VudCIsImdldFJlZnVuZEFtb3VudCIsInVwZGF0ZVZvdWNoZXJSZWZ1bmRUeXBlTGFiZWwiLCJyZWZ1bmRWb3VjaGVyRXhjbHVkZWQiLCJkZWZhdWx0TGFiZWwiLCIkbGFiZWwiLCJmb3JtYXR0ZWRBbW91bnQiLCJmb3JtYXQiLCJsYXN0Q2hpbGQiLCJub2RlVmFsdWUiLCJjYW5jZWxQcm9kdWN0Um91dGUiLCJPcmRlclByb2R1Y3RFZGl0IiwicHJldmlvdXNRdWFudGl0eSIsInVwZGF0ZVRvdGFsIiwiZGlzYWJsZUVkaXRBY3Rpb25CdG4iLCJjb25maXJtZWQiLCJoYW5kbGVFZGl0UHJvZHVjdFdpdGhDb25maXJtYXRpb25Nb2RhbCIsInByb2R1Y3RFZGl0aW9uQ2FuY2VsZWQiLCJ1cGRhdGVkVG90YWwiLCJwcmljZVRvdGFsVGV4dCIsImluaXRpYWxUb3RhbCIsInByb2R1Y3RSb3dFZGl0IiwiY2xvbmUiLCJyZW1vdmVBbGxJZHMiLCJvcmRlckludm9pY2VJZCIsInRheF9yYXRlIiwiYWZ0ZXIiLCJlZGl0UHJvZHVjdCIsImRhdGFTZWxlY3RvciIsImludm9pY2UiLCJwcm9kdWN0VXBkYXRlZCIsIk9yZGVyUHJvZHVjdE1hbmFnZXIiLCJwc3Rvb2x0aXAiLCJkZWxldGVQcm9kdWN0IiwicHJvZHVjdERlbGV0ZWRGcm9tT3JkZXIiLCJvbGRPcmRlckRldGFpbElkIiwiJHByb2R1Y3RSb3ciLCJiZWZvcmUiLCJmYWRlSW4iLCJudW1Qcm9kdWN0cyIsIiRvcmRlckVkaXQiLCJkaXNwbGF5UHJvZHVjdCIsInNjcm9sbFRhcmdldCIsIm1vdmVQcm9kdWN0UGFuZWxUb1RvcCIsInJlc2V0QWxsRWRpdFJvd3MiLCIkbW9kaWZpY2F0aW9uUG9zaXRpb24iLCJkZXRhY2giLCJhcHBlbmRUbyIsIiRyb3dzIiwic2Nyb2xsVmFsdWUiLCJvZmZzZXQiLCJ0b3AiLCJoZWlnaHQiLCJhbmltYXRlIiwicGFnaW5hdGUiLCJrZXkiLCJlZGl0QnV0dG9uIiwicmVzZXRFZGl0Um93IiwiJHByb2R1Y3RFZGl0Um93Iiwib3JpZ2luYWxOdW1QYWdlIiwiJGN1c3RvbWl6YXRpb25Sb3dzIiwiJHRhYmxlUGFnaW5hdGlvbiIsIm51bVJvd3NQZXJQYWdlIiwibWF4UGFnZSIsImNlaWwiLCJudW1QYWdlIiwibWF4IiwibWluIiwicGFnaW5hdGVVcGRhdGVDb250cm9scyIsInN0YXJ0Um93IiwiZW5kUm93IiwiaSIsInByZXYiLCJub3QiLCJ0b3RhbFBhZ2UiLCJ0b2dnbGVQYWdpbmF0aW9uQ29udHJvbHMiLCJudW1QZXJQYWdlIiwidXBkYXRlUGFnaW5hdGlvbkNvbnRyb2xzIiwiZm9yY2VEaXNwbGF5IiwiaXNDb2x1bW5EaXNwbGF5ZWQiLCJudW1QYWdlcyIsIiRsaW5rUGFnaW5hdGlvblRlbXBsYXRlIiwiJGxpbmtQYWdpbmF0aW9uIiwiT3JkZXJTaGlwcGluZ1JlZnJlc2hlciIsInByb2R1Y3RMaXN0UGFnaW5hdGVkIiwicHJvZHVjdExpc3ROdW1iZXJQZXJQYWdlIiwiT3JkZXJWaWV3UGFnZSIsIm9yZGVyRGlzY291bnRzUmVmcmVzaGVyIiwib3JkZXJQcm9kdWN0TWFuYWdlciIsIm9yZGVyUGF5bWVudHNSZWZyZXNoZXIiLCJvcmRlclNoaXBwaW5nUmVmcmVzaGVyIiwib3JkZXJEb2N1bWVudHNSZWZyZXNoZXIiLCJvcmRlckludm9pY2VzUmVmcmVzaGVyIiwib3JkZXJQcm9kdWN0Q2FuY2VsIiwibGlzdGVuVG9FdmVudHMiLCJmYW5jeWJveCIsInR5cGUiLCJ3aWR0aCIsInJlZnJlc2giLCJyZWZyZXNoUHJvZHVjdHNMaXN0IiwiZWRpdFJvd3NMZWZ0IiwibW92ZVByb2R1Y3RQYW5lbFRvT3JpZ2luYWxQb3NpdGlvbiIsInJlZnJlc2hQcm9kdWN0UHJpY2VzIiwibGlzdGVuRm9yUHJvZHVjdERlbGV0ZSIsImxpc3RlbkZvclByb2R1Y3RFZGl0IiwicmVzZXRUb29sVGlwcyIsInJlc2V0QWRkUm93Iiwib2ZmIiwiaGFuZGxlRGVsZXRlUHJvZHVjdEV2ZW50IiwibW92ZVByb2R1Y3RzUGFuZWxUb01vZGlmaWNhdGlvblBvc2l0aW9uIiwiZWRpdFByb2R1Y3RGcm9tTGlzdCIsImJ1dHRvbiIsInJlbGF0ZWRUYXJnZXQiLCJwYWNrSXRlbXMiLCJpdGVtIiwiJGl0ZW0iLCJpbWFnZVBhdGgiLCJyZWZlcmVuY2UiLCJzdXBwbGllclJlZmVyZW5jZSIsImFjdGl2ZVBhZ2UiLCJnZXRBY3RpdmVQYWdlIiwiJHNlbGVjdCIsInVwZGF0ZU51bVBlclBhZ2UiLCJtb3ZlUHJvZHVjdHNQYW5lbFRvUmVmdW5kUG9zaXRpb24iLCJzaG93UGFydGlhbFJlZnVuZCIsInNob3dTdGFuZGFyZFJlZnVuZCIsInNob3dSZXR1cm5Qcm9kdWN0IiwiaGlkZVJlZnVuZCIsInNob3dDYW5jZWxQcm9kdWN0Rm9ybSIsImluaXRpYWxOdW1Qcm9kdWN0cyIsImN1cnJlbnRQYWdlIiwiZG9uZSIsIm5ld051bVByb2R1Y3RzIiwibmV3UGFnZXNOdW0iLCJ1cGRhdGVOdW1Qcm9kdWN0cyIsInRyYW5zbGF0ZV9qYXZhc2NyaXB0cyIsIm5vdGljZSIsImZhaWwiLCJESVNDT1VOVF9UWVBFX0FNT1VOVCIsIkRJU0NPVU5UX1RZUEVfUEVSQ0VOVCIsIkRJU0NPVU5UX1RZUEVfRlJFRV9TSElQUElORyIsIm9yZGVyVmlld1BhZ2UiLCJvcmRlckFkZEF1dG9jb21wbGV0ZSIsIm9yZGVyQWRkIiwibGlzdGVuRm9yUHJvZHVjdFBhY2siLCJsaXN0ZW5Gb3JQcm9kdWN0QWRkIiwibGlzdGVuRm9yUHJvZHVjdFBhZ2luYXRpb24iLCJsaXN0ZW5Gb3JSZWZ1bmQiLCJsaXN0ZW5Gb3JDYW5jZWxQcm9kdWN0IiwibGlzdGVuRm9yU2VhcmNoIiwic2V0UHJvZHVjdCIsImhhbmRsZVBheW1lbnREZXRhaWxzVG9nZ2xlIiwiaGFuZGxlUHJpdmF0ZU5vdGVDaGFuZ2UiLCJoYW5kbGVPcmRlck5vdGVDaGFuZ2UiLCJoYW5kbGVVcGRhdGVPcmRlclN0YXR1c0J1dHRvbiIsIm9yZGVyVmlld1BhZ2VNZXNzYWdlSGFuZGxlciIsInRvZ2dsZVByaXZhdGVOb3RlQmxvY2siLCJ0b2dnbGVPcmRlck5vdGVCbG9jayIsInRlbXBUaXRsZSIsInByaW50IiwiaW5pdEFkZENhcnRSdWxlRm9ybUhhbmRsZXIiLCJpbml0Q2hhbmdlQWRkcmVzc0Zvcm1IYW5kbGVyIiwiaW5pdEhvb2tUYWJzIiwidGFiIiwiJHBheW1lbnREZXRhaWxSb3ciLCIkYmxvY2siLCJpc1ByaXZhdGVOb3RlT3BlbmVkIiwiJGljb24iLCIkc3VibWl0QnRuIiwiaXNOb3RlT3BlbmVkIiwiJGZvcm0iLCIkaW52b2ljZVNlbGVjdCIsIiR2YWx1ZUhlbHAiLCIkdmFsdWVJbnB1dCIsIiR2YWx1ZUZvcm1Hcm91cCIsImNhcnRSdWxlTmFtZSIsImFkZENhcnRSdWxlQXBwbHlPbkFsbEludm9pY2VzQ2hlY2tib3giLCJpc0NoZWNrZWQiLCJzZWxlY3RlZENhcnRSdWxlVHlwZSIsIiR2YWx1ZVVuaXQiLCIkd3JhcHBlciIsIiRlbGVtZW50IiwiJG9wdGlvbiIsInNlbGVjdGVkT3JkZXJTdGF0dXNJZCIsImNzcyJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztJQXdCTUEscUIsR0FDSiwrQkFBWUMsT0FBWixFQUFxQjtBQUFBOztBQUNuQixPQUFLQSxPQUFMLEdBQWVBLE9BQWY7QUFDQSxPQUFLQyxJQUFMLEdBQVksdUJBQVo7QUFDRCxDOztrQkFHWUYscUI7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ1BmOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7Ozs7QUEzQkE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs2QkE4QkVHLGU7OEJBQ0FDLGdCOzBCQUNBQyx5Qjt1QkFDQUMsc0I7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNMRjs7OztBQUNBOzs7O0FBQ0E7Ozs7OztBQUVBLElBQU1DLFdBQVdDLG1CQUFPQSxDQUFDLHdFQUFSLENBQWpCLEMsQ0FoQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQXdCQTs7Ozs7O0FBVUEsSUFBTUMsOEJBQThCLEdBQXBDO0FBQ0EsSUFBTUMsZ0NBQWdDLEdBQXRDO0FBQ0EsSUFBTUMsOEJBQThCLEdBQXBDO0FBQ0EsSUFBTUMseUJBQXlCLEdBQS9CO0FBQ0EsSUFBTUMsNkJBQTZCLEdBQW5DO0FBQ0EsSUFBTUMsd0JBQXdCLEdBQTlCOztJQUVNVCxlO0FBQ0o7Ozs7QUFJQSwyQkFBWVUsYUFBWixFQUEyQjtBQUFBOztBQUN6QixTQUFLQyxtQkFBTCxHQUEyQkQsYUFBM0I7QUFDRDs7QUFFRDs7Ozs7Ozs7Ozs7Ozs7MkJBVU9FLE0sRUFBUUYsYSxFQUFlO0FBQzVCLFVBQUlBLGtCQUFrQkcsU0FBdEIsRUFBaUM7QUFDL0IsYUFBS0YsbUJBQUwsR0FBMkJELGFBQTNCO0FBQ0Q7O0FBRUQ7Ozs7QUFJQSxVQUFNSSxNQUFNQyxLQUFLQyxHQUFMLENBQVNKLE1BQVQsRUFBaUJLLE9BQWpCLENBQXlCLEtBQUtOLG1CQUFMLENBQXlCTyxvQkFBekIsRUFBekIsQ0FBWjs7QUFUNEIsa0NBV0ssS0FBS0MsdUJBQUwsQ0FBNkJMLEdBQTdCLENBWEw7QUFBQTtBQUFBLFVBV3ZCTSxXQVh1QjtBQUFBLFVBV1ZDLFdBWFU7O0FBWTVCRCxvQkFBYyxLQUFLRSxnQkFBTCxDQUFzQkYsV0FBdEIsQ0FBZDtBQUNBQyxvQkFBYyxLQUFLRSx1QkFBTCxDQUE2QkYsV0FBN0IsQ0FBZDs7QUFFQTtBQUNBLFVBQUlHLGtCQUFrQkosV0FBdEI7O0FBRUEsVUFBSUMsV0FBSixFQUFpQjtBQUNmRywyQkFBbUJuQixnQ0FBZ0NnQixXQUFuRDtBQUNEOztBQUVEO0FBQ0EsVUFBTUksVUFBVSxLQUFLQyxjQUFMLENBQW9CZCxTQUFTLENBQTdCLENBQWhCO0FBQ0FZLHdCQUFrQixLQUFLRyxlQUFMLENBQXFCSCxlQUFyQixFQUFzQ0MsT0FBdEMsQ0FBbEI7QUFDQUQsd0JBQWtCLEtBQUtJLGNBQUwsQ0FBb0JKLGVBQXBCLENBQWxCOztBQUVBQSx3QkFBa0IsS0FBS0ssMkJBQUwsQ0FBaUNMLGVBQWpDLENBQWxCOztBQUVBLGFBQU9BLGVBQVA7QUFDRDs7QUFFRDs7Ozs7Ozs7Ozs7Ozs7Ozs7NENBY3dCWixNLEVBQVE7QUFDOUI7QUFDQSxVQUFNa0IsU0FBU2xCLE9BQU9tQixRQUFQLEdBQWtCQyxLQUFsQixDQUF3QixHQUF4QixDQUFmO0FBQ0EsVUFBTVosY0FBY1UsT0FBTyxDQUFQLENBQXBCO0FBQ0EsVUFBTVQsY0FBZVMsT0FBTyxDQUFQLE1BQWNqQixTQUFmLEdBQTRCLEVBQTVCLEdBQWlDaUIsT0FBTyxDQUFQLENBQXJEOztBQUVBLGFBQU8sQ0FBQ1YsV0FBRCxFQUFjQyxXQUFkLENBQVA7QUFDRDs7QUFFRDs7Ozs7Ozs7Ozs7OztxQ0FVaUJZLEssRUFBTztBQUN0QixVQUFJLENBQUMsS0FBS3RCLG1CQUFMLENBQXlCdUIsY0FBekIsRUFBTCxFQUFnRDtBQUM5QyxlQUFPRCxLQUFQO0FBQ0Q7O0FBRUQ7QUFDQSxVQUFNYixjQUFjYSxNQUFNRCxLQUFOLENBQVksRUFBWixFQUFnQkcsT0FBaEIsRUFBcEI7O0FBRUE7QUFDQSxVQUFJQyxTQUFTLEVBQWI7QUFDQUEsYUFBT0MsSUFBUCxDQUFZakIsWUFBWWtCLE1BQVosQ0FBbUIsQ0FBbkIsRUFBc0IsS0FBSzNCLG1CQUFMLENBQXlCNEIsbUJBQXpCLEVBQXRCLENBQVo7QUFDQSxhQUFPbkIsWUFBWW9CLE1BQW5CLEVBQTJCO0FBQ3pCSixlQUFPQyxJQUFQLENBQVlqQixZQUFZa0IsTUFBWixDQUFtQixDQUFuQixFQUFzQixLQUFLM0IsbUJBQUwsQ0FBeUI4QixxQkFBekIsRUFBdEIsQ0FBWjtBQUNEOztBQUVEO0FBQ0FMLGVBQVNBLE9BQU9ELE9BQVAsRUFBVDtBQUNBLFVBQU1PLFlBQVksRUFBbEI7QUFDQU4sYUFBT08sT0FBUCxDQUFlLFVBQUNDLEtBQUQsRUFBVztBQUN4QkYsa0JBQVVMLElBQVYsQ0FBZU8sTUFBTVQsT0FBTixHQUFnQlUsSUFBaEIsQ0FBcUIsRUFBckIsQ0FBZjtBQUNELE9BRkQ7O0FBSUE7QUFDQSxhQUFPSCxVQUFVRyxJQUFWLENBQWV2QywyQkFBZixDQUFQO0FBQ0Q7O0FBRUQ7Ozs7Ozs7Ozs7NENBT3dCZSxXLEVBQWE7QUFDbkMsVUFBSVksUUFBUVosV0FBWjs7QUFFQSxVQUFJWSxNQUFNTyxNQUFOLEdBQWUsS0FBSzdCLG1CQUFMLENBQXlCTyxvQkFBekIsRUFBbkIsRUFBb0U7QUFDbEU7QUFDQWUsZ0JBQVFBLE1BQU1hLE9BQU4sQ0FBYyxLQUFkLEVBQXFCLEVBQXJCLENBQVI7QUFDRDs7QUFFRCxVQUFJYixNQUFNTyxNQUFOLEdBQWUsS0FBSzdCLG1CQUFMLENBQXlCb0Msb0JBQXpCLEVBQW5CLEVBQW9FO0FBQ2xFO0FBQ0FkLGdCQUFRQSxNQUFNZSxNQUFOLENBQ04sS0FBS3JDLG1CQUFMLENBQXlCb0Msb0JBQXpCLEVBRE0sRUFFTixHQUZNLENBQVI7QUFJRDs7QUFFRCxhQUFPZCxLQUFQO0FBQ0Q7O0FBRUQ7Ozs7Ozs7Ozs7Ozs7bUNBVWVnQixVLEVBQVk7QUFDekIsVUFBSUEsVUFBSixFQUFnQjtBQUNkLGVBQU8sS0FBS3RDLG1CQUFMLENBQXlCdUMsa0JBQXpCLEVBQVA7QUFDRDs7QUFFRCxhQUFPLEtBQUt2QyxtQkFBTCxDQUF5QndDLGtCQUF6QixFQUFQO0FBQ0Q7O0FBRUQ7Ozs7Ozs7Ozs7OzttQ0FTZXZDLE0sRUFBUTtBQUNyQixVQUFNd0MsVUFBVSxLQUFLekMsbUJBQUwsQ0FBeUIwQyxTQUF6QixFQUFoQjs7QUFFQSxVQUFNQyxNQUFNLEVBQVo7QUFDQUEsVUFBSWpELDZCQUFKLElBQXFDK0MsUUFBUUcsVUFBUixFQUFyQztBQUNBRCxVQUFJaEQsMkJBQUosSUFBbUM4QyxRQUFRSSxRQUFSLEVBQW5DO0FBQ0FGLFVBQUkvQyxzQkFBSixJQUE4QjZDLFFBQVFLLFlBQVIsRUFBOUI7QUFDQUgsVUFBSTlDLDBCQUFKLElBQWtDNEMsUUFBUU0sY0FBUixFQUFsQztBQUNBSixVQUFJN0MscUJBQUosSUFBNkIyQyxRQUFRTyxXQUFSLEVBQTdCOztBQUVBLGFBQU8sS0FBS0MsS0FBTCxDQUFXaEQsTUFBWCxFQUFtQjBDLEdBQW5CLENBQVA7QUFDRDs7QUFFRDs7Ozs7Ozs7Ozs7Ozs7MEJBV01PLEcsRUFBS0MsSyxFQUFPO0FBQ2hCLFVBQU1DLFVBQVUsb0JBQVlELEtBQVosRUFBbUJSLEdBQW5CLENBQXVCcEQsUUFBdkIsQ0FBaEI7O0FBRUEsYUFBTzJELElBQUk3QixLQUFKLENBQVVnQyxhQUFXRCxRQUFRbEIsSUFBUixDQUFhLEdBQWIsQ0FBWCxPQUFWLEVBQ0pTLEdBREksQ0FDQSxVQUFDVyxJQUFEO0FBQUEsZUFBVUgsTUFBTUcsSUFBTixLQUFlQSxJQUF6QjtBQUFBLE9BREEsRUFFSnBCLElBRkksQ0FFQyxFQUZELENBQVA7QUFHRDs7QUFFRDs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztvQ0FtQmdCckIsZSxFQUFpQkMsTyxFQUFTO0FBQ3hDOzs7Ozs7OztBQVFBLGFBQU9BLFFBQVFxQixPQUFSLENBQWdCLHFCQUFoQixFQUF1Q3RCLGVBQXZDLENBQVA7QUFDRDs7QUFFRDs7Ozs7Ozs7Ozs7Ozs7Z0RBVzRCQSxlLEVBQWlCO0FBQzNDLFVBQUksS0FBS2IsbUJBQUwsWUFBb0NiLGVBQXhDLEVBQTREO0FBQzFELGVBQU8wQixnQkFDSlEsS0FESSxDQUNFNUIsMkJBREYsRUFFSnlDLElBRkksQ0FFQyxLQUFLbEMsbUJBQUwsQ0FBeUJ1RCxpQkFBekIsRUFGRCxDQUFQO0FBR0Q7O0FBRUQsYUFBTzFDLGVBQVA7QUFDRDs7OzBCQUVZMkMsYyxFQUFnQjtBQUMzQixVQUFJQyxlQUFKOztBQUVBLFVBQUl2RCxjQUFjc0QsZUFBZUUsYUFBakMsRUFBZ0Q7QUFDOUNELG9EQUFhbkUsc0JBQWIsaURBQTZCa0UsZUFBZUUsYUFBNUM7QUFDRCxPQUZELE1BRU87QUFDTEQsb0RBQWFuRSxzQkFBYixpREFBNkJrRSxlQUFlQyxNQUE1QztBQUNEOztBQUVELFVBQUkxRCxzQkFBSjs7QUFFQSxVQUFJeUQsZUFBZUcsY0FBbkIsRUFBbUM7QUFDakM1RCx3QkFBZ0IsSUFBSVosZUFBSixDQUNkcUUsZUFBZUksZUFERCxFQUVkSixlQUFlSyxlQUZELEVBR2RKLE1BSGMsRUFJZEssU0FBU04sZUFBZU8saUJBQXhCLEVBQTJDLEVBQTNDLENBSmMsRUFLZEQsU0FBU04sZUFBZVEsaUJBQXhCLEVBQTJDLEVBQTNDLENBTGMsRUFNZFIsZUFBZVMsWUFORCxFQU9kVCxlQUFlVSxnQkFQRCxFQVFkVixlQUFlVyxrQkFSRCxFQVNkWCxlQUFlRyxjQVRELEVBVWRILGVBQWVZLFlBVkQsQ0FBaEI7QUFZRCxPQWJELE1BYU87QUFDTHJFLHdCQUFnQixJQUFJWCxnQkFBSixDQUNkb0UsZUFBZUksZUFERCxFQUVkSixlQUFlSyxlQUZELEVBR2RKLE1BSGMsRUFJZEssU0FBU04sZUFBZU8saUJBQXhCLEVBQTJDLEVBQTNDLENBSmMsRUFLZEQsU0FBU04sZUFBZVEsaUJBQXhCLEVBQTJDLEVBQTNDLENBTGMsRUFNZFIsZUFBZVMsWUFORCxFQU9kVCxlQUFlVSxnQkFQRCxFQVFkVixlQUFlVyxrQkFSRCxDQUFoQjtBQVVEOztBQUVELGFBQU8sSUFBSTlFLGVBQUosQ0FBb0JVLGFBQXBCLENBQVA7QUFDRDs7Ozs7a0JBR1lWLGU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNqVGY7Ozs7OztJQUVNQyxZO0FBQ0o7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLHdCQUNFK0UsT0FERixFQUVFcEMsS0FGRixFQUdFcUMsSUFIRixFQUlFQyxXQUpGLEVBS0VDLFNBTEYsRUFNRUMsUUFORixFQU9FQyxXQVBGLEVBUUVDLHNCQVJGLEVBU0VDLFFBVEYsRUFVRUMsUUFWRixFQVdFQyxHQVhGLEVBWUU7QUFBQTs7QUFDQSxTQUFLVCxPQUFMLEdBQWVBLE9BQWY7QUFDQSxTQUFLcEMsS0FBTCxHQUFhQSxLQUFiO0FBQ0EsU0FBS3FDLElBQUwsR0FBWUEsSUFBWjtBQUNBLFNBQUtDLFdBQUwsR0FBbUJBLFdBQW5CO0FBQ0EsU0FBS0MsU0FBTCxHQUFpQkEsU0FBakI7QUFDQSxTQUFLQyxRQUFMLEdBQWdCQSxRQUFoQjtBQUNBLFNBQUtDLFdBQUwsR0FBbUJBLFdBQW5CO0FBQ0EsU0FBS0Msc0JBQUwsR0FBOEJBLHNCQUE5QjtBQUNBLFNBQUtDLFFBQUwsR0FBZ0JBLFFBQWhCO0FBQ0EsU0FBS0MsUUFBTCxHQUFnQkEsUUFBaEI7QUFDQSxTQUFLQyxHQUFMLEdBQVdBLEdBQVg7O0FBRUEsU0FBS0MsWUFBTDtBQUNEOztBQUVEOzs7Ozs7Ozs7aUNBS2E7QUFDWCxhQUFPLEtBQUtWLE9BQVo7QUFDRDs7QUFFRDs7Ozs7Ozs7K0JBS1c7QUFDVCxhQUFPLEtBQUtwQyxLQUFaO0FBQ0Q7O0FBRUQ7Ozs7Ozs7OzhCQUtVO0FBQ1IsYUFBTyxLQUFLcUMsSUFBWjtBQUNEOztBQUVEOzs7Ozs7OztxQ0FLaUI7QUFDZixhQUFPLEtBQUtDLFdBQVo7QUFDRDs7QUFFRDs7Ozs7Ozs7bUNBS2U7QUFDYixhQUFPLEtBQUtDLFNBQVo7QUFDRDs7QUFFRDs7Ozs7Ozs7a0NBS2M7QUFDWixhQUFPLEtBQUtDLFFBQVo7QUFDRDs7QUFFRDs7Ozs7Ozs7cUNBS2lCO0FBQ2YsYUFBTyxLQUFLQyxXQUFaO0FBQ0Q7O0FBRUQ7Ozs7Ozs7O2dEQUs0QjtBQUMxQixhQUFPLEtBQUtDLHNCQUFaO0FBQ0Q7O0FBRUQ7Ozs7Ozs7Ozs7a0NBT2M7QUFDWixhQUFPLEtBQUtDLFFBQVo7QUFDRDs7QUFFRDs7Ozs7Ozs7OztrQ0FPYztBQUNaLGFBQU8sS0FBS0MsUUFBWjtBQUNEOztBQUVEOzs7Ozs7Ozs2QkFLUztBQUNQLGFBQU8sS0FBS0MsR0FBWjtBQUNEOztBQUVEOzs7Ozs7OzttQ0FLZTtBQUNiLFVBQUksQ0FBQyxLQUFLVCxPQUFOLElBQWlCLE9BQU8sS0FBS0EsT0FBWixLQUF3QixRQUE3QyxFQUF1RDtBQUNyRCxjQUFNLElBQUlyRixzQkFBSixDQUEwQixpQkFBMUIsQ0FBTjtBQUNEOztBQUVELFVBQUksQ0FBQyxLQUFLaUQsS0FBTixJQUFlLE9BQU8sS0FBS0EsS0FBWixLQUFzQixRQUF6QyxFQUFtRDtBQUNqRCxjQUFNLElBQUlqRCxzQkFBSixDQUEwQixlQUExQixDQUFOO0FBQ0Q7O0FBRUQsVUFBSSxDQUFDLEtBQUtzRixJQUFOLElBQWMsT0FBTyxLQUFLQSxJQUFaLEtBQXFCLFFBQXZDLEVBQWlEO0FBQy9DLGNBQU0sSUFBSXRGLHNCQUFKLENBQTBCLHFCQUExQixDQUFOO0FBQ0Q7O0FBRUQsVUFBSSxDQUFDLEtBQUt1RixXQUFOLElBQXFCLE9BQU8sS0FBS0EsV0FBWixLQUE0QixRQUFyRCxFQUErRDtBQUM3RCxjQUFNLElBQUl2RixzQkFBSixDQUEwQixxQkFBMUIsQ0FBTjtBQUNEOztBQUVELFVBQUksQ0FBQyxLQUFLd0YsU0FBTixJQUFtQixPQUFPLEtBQUtBLFNBQVosS0FBMEIsUUFBakQsRUFBMkQ7QUFDekQsY0FBTSxJQUFJeEYsc0JBQUosQ0FBMEIsbUJBQTFCLENBQU47QUFDRDs7QUFFRCxVQUFJLENBQUMsS0FBS3lGLFFBQU4sSUFBa0IsT0FBTyxLQUFLQSxRQUFaLEtBQXlCLFFBQS9DLEVBQXlEO0FBQ3ZELGNBQU0sSUFBSXpGLHNCQUFKLENBQTBCLGtCQUExQixDQUFOO0FBQ0Q7O0FBRUQsVUFBSSxDQUFDLEtBQUswRixXQUFOLElBQXFCLE9BQU8sS0FBS0EsV0FBWixLQUE0QixRQUFyRCxFQUErRDtBQUM3RCxjQUFNLElBQUkxRixzQkFBSixDQUEwQixxQkFBMUIsQ0FBTjtBQUNEOztBQUVELFVBQUksQ0FBQyxLQUFLMkYsc0JBQU4sSUFBZ0MsT0FBTyxLQUFLQSxzQkFBWixLQUF1QyxRQUEzRSxFQUFxRjtBQUNuRixjQUFNLElBQUkzRixzQkFBSixDQUEwQixnQ0FBMUIsQ0FBTjtBQUNEOztBQUVELFVBQUksQ0FBQyxLQUFLNEYsUUFBTixJQUFrQixPQUFPLEtBQUtBLFFBQVosS0FBeUIsUUFBL0MsRUFBeUQ7QUFDdkQsY0FBTSxJQUFJNUYsc0JBQUosQ0FBMEIsa0JBQTFCLENBQU47QUFDRDs7QUFFRCxVQUFJLENBQUMsS0FBSzZGLFFBQU4sSUFBa0IsT0FBTyxLQUFLQSxRQUFaLEtBQXlCLFFBQS9DLEVBQXlEO0FBQ3ZELGNBQU0sSUFBSTdGLHNCQUFKLENBQTBCLGtCQUExQixDQUFOO0FBQ0Q7O0FBRUQsVUFBSSxDQUFDLEtBQUs4RixHQUFOLElBQWEsT0FBTyxLQUFLQSxHQUFaLEtBQW9CLFFBQXJDLEVBQStDO0FBQzdDLGNBQU0sSUFBSTlGLHNCQUFKLENBQTBCLGFBQTFCLENBQU47QUFDRDtBQUNGOzs7S0FoT0g7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O2tCQW1PZU0sWTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzNNZjs7OztBQUNBOzs7Ozs7QUF6QkE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztJQTJCTUYsbUI7QUFDSjs7Ozs7Ozs7Ozs7Ozs7QUFjQSwrQkFDRXdFLGVBREYsRUFFRUMsZUFGRixFQUdFSixNQUhGLEVBSUVNLGlCQUpGLEVBS0VDLGlCQUxGLEVBTUVDLFlBTkYsRUFPRUMsZ0JBUEYsRUFRRUMsa0JBUkYsRUFTRTtBQUFBOztBQUNBLFNBQUtQLGVBQUwsR0FBdUJBLGVBQXZCO0FBQ0EsU0FBS0MsZUFBTCxHQUF1QkEsZUFBdkI7QUFDQSxTQUFLSixNQUFMLEdBQWNBLE1BQWQ7O0FBRUEsU0FBS00saUJBQUwsR0FBeUJBLGlCQUF6QjtBQUNBO0FBQ0EsU0FBS0MsaUJBQUwsR0FBeUJELG9CQUFvQkMsaUJBQXBCLEdBQXdDRCxpQkFBeEMsR0FBNERDLGlCQUFyRjs7QUFFQSxTQUFLQyxZQUFMLEdBQW9CQSxZQUFwQjtBQUNBLFNBQUtDLGdCQUFMLEdBQXdCQSxnQkFBeEI7QUFDQSxTQUFLQyxrQkFBTCxHQUEwQkEsa0JBQTFCOztBQUVBLFFBQUksQ0FBQyxLQUFLUCxlQUFOLElBQXlCLE9BQU8sS0FBS0EsZUFBWixLQUFnQyxRQUE3RCxFQUF1RTtBQUNyRSxZQUFNLElBQUk1RSxzQkFBSixDQUEwQix5QkFBMUIsQ0FBTjtBQUNEOztBQUVELFFBQUksQ0FBQyxLQUFLNkUsZUFBTixJQUF5QixPQUFPLEtBQUtBLGVBQVosS0FBZ0MsUUFBN0QsRUFBdUU7QUFDckUsWUFBTSxJQUFJN0Usc0JBQUosQ0FBMEIseUJBQTFCLENBQU47QUFDRDs7QUFFRCxRQUFJLENBQUMsS0FBS3lFLE1BQU4sSUFBZ0IsRUFBRSxLQUFLQSxNQUFMLFlBQXVCbkUsc0JBQXpCLENBQXBCLEVBQTREO0FBQzFELFlBQU0sSUFBSU4sc0JBQUosQ0FBMEIsZ0JBQTFCLENBQU47QUFDRDs7QUFFRCxRQUFJLE9BQU8sS0FBSytFLGlCQUFaLEtBQWtDLFFBQXRDLEVBQWdEO0FBQzlDLFlBQU0sSUFBSS9FLHNCQUFKLENBQTBCLDJCQUExQixDQUFOO0FBQ0Q7O0FBRUQsUUFBSSxPQUFPLEtBQUtnRixpQkFBWixLQUFrQyxRQUF0QyxFQUFnRDtBQUM5QyxZQUFNLElBQUloRixzQkFBSixDQUEwQiwyQkFBMUIsQ0FBTjtBQUNEOztBQUVELFFBQUksT0FBTyxLQUFLaUYsWUFBWixLQUE2QixTQUFqQyxFQUE0QztBQUMxQyxZQUFNLElBQUlqRixzQkFBSixDQUEwQixzQkFBMUIsQ0FBTjtBQUNEOztBQUVELFFBQUksT0FBTyxLQUFLa0YsZ0JBQVosS0FBaUMsUUFBckMsRUFBK0M7QUFDN0MsWUFBTSxJQUFJbEYsc0JBQUosQ0FBMEIsMEJBQTFCLENBQU47QUFDRDs7QUFFRCxRQUFJLE9BQU8sS0FBS21GLGtCQUFaLEtBQW1DLFFBQXZDLEVBQWlEO0FBQy9DLFlBQU0sSUFBSW5GLHNCQUFKLENBQTBCLDRCQUExQixDQUFOO0FBQ0Q7QUFDRjs7QUFFRDs7Ozs7Ozs7O2dDQUtZO0FBQ1YsYUFBTyxLQUFLeUUsTUFBWjtBQUNEOztBQUVEOzs7Ozs7Ozs7O3lDQU9xQjtBQUNuQixhQUFPLEtBQUtHLGVBQVo7QUFDRDs7QUFFRDs7Ozs7Ozs7Ozt5Q0FPcUI7QUFDbkIsYUFBTyxLQUFLQyxlQUFaO0FBQ0Q7O0FBRUQ7Ozs7Ozs7OzJDQUt1QjtBQUNyQixhQUFPLEtBQUtFLGlCQUFaO0FBQ0Q7O0FBRUQ7Ozs7Ozs7OzJDQUt1QjtBQUNyQixhQUFPLEtBQUtDLGlCQUFaO0FBQ0Q7O0FBRUQ7Ozs7Ozs7OztxQ0FNaUI7QUFDZixhQUFPLEtBQUtDLFlBQVo7QUFDRDs7QUFFRDs7Ozs7Ozs7MENBS3NCO0FBQ3BCLGFBQU8sS0FBS0MsZ0JBQVo7QUFDRDs7QUFFRDs7Ozs7Ozs7NENBS3dCO0FBQ3RCLGFBQU8sS0FBS0Msa0JBQVo7QUFDRDs7Ozs7a0JBR1kvRSxtQjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3ZKZjs7OztBQUNBOzs7Ozs7QUFFQTs7O0FBM0JBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUE4QkEsSUFBTTRGLDBCQUEwQixRQUFoQzs7SUFFTTdGLGtCOzs7QUFDSjs7Ozs7Ozs7Ozs7Ozs7OztBQWdCQSw4QkFDRXlFLGVBREYsRUFFRUMsZUFGRixFQUdFSixNQUhGLEVBSUVNLGlCQUpGLEVBS0VDLGlCQUxGLEVBTUVDLFlBTkYsRUFPRUMsZ0JBUEYsRUFRRUMsa0JBUkYsRUFTRVIsY0FURixFQVVFUyxZQVZGLEVBV0U7QUFBQTs7QUFBQSw4SkFFRVIsZUFGRixFQUdFQyxlQUhGLEVBSUVKLE1BSkYsRUFLRU0saUJBTEYsRUFNRUMsaUJBTkYsRUFPRUMsWUFQRixFQVFFQyxnQkFSRixFQVNFQyxrQkFURjs7QUFXQSxVQUFLUixjQUFMLEdBQXNCQSxjQUF0QjtBQUNBLFVBQUtTLFlBQUwsR0FBb0JBLFlBQXBCOztBQUVBLFFBQUksQ0FBQyxNQUFLVCxjQUFOLElBQXdCLE9BQU8sTUFBS0EsY0FBWixLQUErQixRQUEzRCxFQUFxRTtBQUNuRSxZQUFNLElBQUkzRSxzQkFBSixDQUEwQix3QkFBMUIsQ0FBTjtBQUNEOztBQUVELFFBQUksQ0FBQyxNQUFLb0YsWUFBTixJQUFzQixPQUFPLE1BQUtBLFlBQVosS0FBNkIsUUFBdkQsRUFBaUU7QUFDL0QsWUFBTSxJQUFJcEYsc0JBQUosQ0FBMEIsc0JBQTFCLENBQU47QUFDRDtBQXBCRDtBQXFCRDs7QUFFRDs7Ozs7Ozs7Ozs7QUFTQTs7Ozs7O3dDQU1vQjtBQUNsQixhQUFPLEtBQUsyRSxjQUFaO0FBQ0Q7O0FBRUQ7Ozs7Ozs7OztzQ0FNa0I7QUFDaEIsYUFBTyxLQUFLUyxZQUFaO0FBQ0Q7Ozt5Q0F0QjJCO0FBQzFCLGFBQU9ZLHVCQUFQO0FBQ0Q7OztFQTFEOEI1RixnQjs7a0JBaUZsQkQsa0I7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3hGZjs7Ozs7O0FBRUE7Ozs7QUFJTyxJQUFNOEYsZUFBZUEsdUJBQUEsSUFBSUMsZ0JBQUosRUFBckIsQyxDQS9CUDs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztrQkFpQ2VELFk7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDakNmOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O2NBeUJZRSxNO0lBQUxDLEMsV0FBQUEsQzs7QUFFUDs7Ozs7Ozs7Ozs7Ozs7Ozs7OztJQWtCcUJDLHFCLEdBQ25CLGlDQUFjO0FBQUE7O0FBQUE7O0FBQ1osT0FBS0MsZUFBTCxHQUF1Qiw4QkFBdkI7QUFDQSxPQUFLQyxZQUFMLEdBQW9CLG9CQUFwQjtBQUNBLE9BQUtDLGFBQUwsR0FBcUIscUJBQXJCOztBQUVBSixJQUFFSyxRQUFGLEVBQVlDLEVBQVosQ0FBZSxPQUFmLEVBQTJCLEtBQUtKLGVBQWhDLFNBQW1ELEtBQUtFLGFBQXhELEVBQXlFLFVBQUNHLENBQUQsRUFBTztBQUM5RSxRQUFNQyxTQUFTUixFQUFFTyxFQUFFRSxhQUFKLENBQWY7QUFDQSxRQUFNQyxrQkFBa0JGLE9BQU9HLElBQVAsQ0FBWSxZQUFaLElBQTRCSCxPQUFPSSxHQUFQLEdBQWFuRSxNQUFqRTs7QUFFQStELFdBQU9LLE9BQVAsQ0FBZSxNQUFLWCxlQUFwQixFQUFxQ1ksSUFBckMsQ0FBMEMsTUFBS1gsWUFBL0MsRUFBNkRZLElBQTdELENBQWtFTCxlQUFsRTtBQUNELEdBTEQ7QUFNRCxDOztrQkFaa0JULHFCOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7a0JDSEdlLFk7Ozs7QUExQ3hCOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O2NBeUJZakIsTTtJQUFMQyxDLFdBQUFBLEM7O0FBRVA7Ozs7Ozs7Ozs7Ozs7Ozs7QUFlZSxTQUFTZ0IsWUFBVCxDQUFzQkMsTUFBdEIsRUFBOEJDLGVBQTlCLEVBQStDQyxjQUEvQyxFQUErRDtBQUFBOztBQUM1RTtBQUQ0RSxNQUVyRUMsRUFGcUUsR0FFckRILE1BRnFELENBRXJFRyxFQUZxRTtBQUFBLE1BRWpFQyxRQUZpRSxHQUVyREosTUFGcUQsQ0FFakVJLFFBRmlFOztBQUc1RSxPQUFLQyxLQUFMLEdBQWFDLE1BQU1OLE1BQU4sQ0FBYjs7QUFFQTtBQUNBLE9BQUtPLE1BQUwsR0FBY3hCLEVBQUUsS0FBS3NCLEtBQUwsQ0FBV0csU0FBYixDQUFkOztBQUVBLE9BQUtDLElBQUwsR0FBWSxZQUFNO0FBQ2hCLFVBQUtGLE1BQUwsQ0FBWUYsS0FBWjtBQUNELEdBRkQ7O0FBSUEsT0FBS0EsS0FBTCxDQUFXSyxhQUFYLENBQXlCQyxnQkFBekIsQ0FBMEMsT0FBMUMsRUFBbURWLGVBQW5EOztBQUVBLE9BQUtNLE1BQUwsQ0FBWUYsS0FBWixDQUFrQjtBQUNoQk8sY0FBVVIsV0FBVyxJQUFYLEdBQWtCLFFBRFo7QUFFaEJTLGNBQVVULGFBQWF2RyxTQUFiLEdBQXlCdUcsUUFBekIsR0FBb0MsSUFGOUI7QUFHaEJBLGNBQVVBLGFBQWF2RyxTQUFiLEdBQXlCdUcsUUFBekIsR0FBb0MsSUFIOUI7QUFJaEJLLFVBQU07QUFKVSxHQUFsQjs7QUFPQSxPQUFLRixNQUFMLENBQVlsQixFQUFaLENBQWUsaUJBQWYsRUFBa0MsWUFBTTtBQUN0Q0QsYUFBUzBCLGFBQVQsT0FBMkJYLEVBQTNCLEVBQWlDWSxNQUFqQztBQUNBLFFBQUliLGNBQUosRUFBb0I7QUFDbEJBO0FBQ0Q7QUFDRixHQUxEOztBQU9BZCxXQUFTNEIsSUFBVCxDQUFjQyxXQUFkLENBQTBCLEtBQUtaLEtBQUwsQ0FBV0csU0FBckM7QUFDRDs7QUFFRDs7Ozs7O0FBTUEsU0FBU0YsS0FBVCxPQVFHO0FBQUE7O0FBQUEscUJBUERILEVBT0M7QUFBQSxNQVBEQSxFQU9DLDJCQVBJLGVBT0o7QUFBQSxNQU5EZSxZQU1DLFFBTkRBLFlBTUM7QUFBQSxpQ0FMREMsY0FLQztBQUFBLE1BTERBLGNBS0MsdUNBTGdCLEVBS2hCO0FBQUEsbUNBSkRDLGdCQUlDO0FBQUEsTUFKREEsZ0JBSUMseUNBSmtCLE9BSWxCO0FBQUEsbUNBSERDLGtCQUdDO0FBQUEsTUFIREEsa0JBR0MseUNBSG9CLFFBR3BCO0FBQUEsbUNBRkRDLGtCQUVDO0FBQUEsTUFGREEsa0JBRUMseUNBRm9CLGFBRXBCO0FBQUEsZ0NBRERDLGFBQ0M7QUFBQSxNQUREQSxhQUNDLHNDQURlLEVBQ2Y7O0FBQ0QsTUFBTWxCLFFBQVEsRUFBZDs7QUFFQTtBQUNBQSxRQUFNRyxTQUFOLEdBQWtCcEIsU0FBU29DLGFBQVQsQ0FBdUIsS0FBdkIsQ0FBbEI7QUFDQW5CLFFBQU1HLFNBQU4sQ0FBZ0JpQixTQUFoQixDQUEwQkMsR0FBMUIsQ0FBOEIsT0FBOUIsRUFBdUMsTUFBdkM7QUFDQXJCLFFBQU1HLFNBQU4sQ0FBZ0JMLEVBQWhCLEdBQXFCQSxFQUFyQjs7QUFFQTtBQUNBRSxRQUFNc0IsTUFBTixHQUFldkMsU0FBU29DLGFBQVQsQ0FBdUIsS0FBdkIsQ0FBZjtBQUNBbkIsUUFBTXNCLE1BQU4sQ0FBYUYsU0FBYixDQUF1QkMsR0FBdkIsQ0FBMkIsY0FBM0I7O0FBRUE7QUFDQXJCLFFBQU11QixPQUFOLEdBQWdCeEMsU0FBU29DLGFBQVQsQ0FBdUIsS0FBdkIsQ0FBaEI7QUFDQW5CLFFBQU11QixPQUFOLENBQWNILFNBQWQsQ0FBd0JDLEdBQXhCLENBQTRCLGVBQTVCOztBQUVBO0FBQ0FyQixRQUFNd0IsTUFBTixHQUFlekMsU0FBU29DLGFBQVQsQ0FBdUIsS0FBdkIsQ0FBZjtBQUNBbkIsUUFBTXdCLE1BQU4sQ0FBYUosU0FBYixDQUF1QkMsR0FBdkIsQ0FBMkIsY0FBM0I7O0FBRUE7QUFDQSxNQUFJUixZQUFKLEVBQWtCO0FBQ2hCYixVQUFNeUIsS0FBTixHQUFjMUMsU0FBU29DLGFBQVQsQ0FBdUIsSUFBdkIsQ0FBZDtBQUNBbkIsVUFBTXlCLEtBQU4sQ0FBWUwsU0FBWixDQUFzQkMsR0FBdEIsQ0FBMEIsYUFBMUI7QUFDQXJCLFVBQU15QixLQUFOLENBQVlDLFNBQVosR0FBd0JiLFlBQXhCO0FBQ0Q7O0FBRUQ7QUFDQWIsUUFBTTJCLFNBQU4sR0FBa0I1QyxTQUFTb0MsYUFBVCxDQUF1QixRQUF2QixDQUFsQjtBQUNBbkIsUUFBTTJCLFNBQU4sQ0FBZ0JQLFNBQWhCLENBQTBCQyxHQUExQixDQUE4QixPQUE5QjtBQUNBckIsUUFBTTJCLFNBQU4sQ0FBZ0JDLFlBQWhCLENBQTZCLE1BQTdCLEVBQXFDLFFBQXJDO0FBQ0E1QixRQUFNMkIsU0FBTixDQUFnQkUsT0FBaEIsQ0FBd0JDLE9BQXhCLEdBQWtDLE9BQWxDO0FBQ0E5QixRQUFNMkIsU0FBTixDQUFnQkQsU0FBaEIsR0FBNEIsR0FBNUI7O0FBRUE7QUFDQTFCLFFBQU1XLElBQU4sR0FBYTVCLFNBQVNvQyxhQUFULENBQXVCLEtBQXZCLENBQWI7QUFDQW5CLFFBQU1XLElBQU4sQ0FBV1MsU0FBWCxDQUFxQkMsR0FBckIsQ0FBeUIsWUFBekIsRUFBdUMsV0FBdkMsRUFBb0Qsb0JBQXBEOztBQUVBO0FBQ0FyQixRQUFNekgsT0FBTixHQUFnQndHLFNBQVNvQyxhQUFULENBQXVCLEdBQXZCLENBQWhCO0FBQ0FuQixRQUFNekgsT0FBTixDQUFjNkksU0FBZCxDQUF3QkMsR0FBeEIsQ0FBNEIsaUJBQTVCO0FBQ0FyQixRQUFNekgsT0FBTixDQUFjbUosU0FBZCxHQUEwQlosY0FBMUI7O0FBRUE7QUFDQWQsUUFBTStCLE1BQU4sR0FBZWhELFNBQVNvQyxhQUFULENBQXVCLEtBQXZCLENBQWY7QUFDQW5CLFFBQU0rQixNQUFOLENBQWFYLFNBQWIsQ0FBdUJDLEdBQXZCLENBQTJCLGNBQTNCOztBQUVBO0FBQ0FyQixRQUFNZ0MsV0FBTixHQUFvQmpELFNBQVNvQyxhQUFULENBQXVCLFFBQXZCLENBQXBCO0FBQ0FuQixRQUFNZ0MsV0FBTixDQUFrQkosWUFBbEIsQ0FBK0IsTUFBL0IsRUFBdUMsUUFBdkM7QUFDQTVCLFFBQU1nQyxXQUFOLENBQWtCWixTQUFsQixDQUE0QkMsR0FBNUIsQ0FBZ0MsS0FBaEMsRUFBdUMsdUJBQXZDLEVBQWdFLFFBQWhFO0FBQ0FyQixRQUFNZ0MsV0FBTixDQUFrQkgsT0FBbEIsQ0FBMEJDLE9BQTFCLEdBQW9DLE9BQXBDO0FBQ0E5QixRQUFNZ0MsV0FBTixDQUFrQk4sU0FBbEIsR0FBOEJYLGdCQUE5Qjs7QUFFQTtBQUNBZixRQUFNSyxhQUFOLEdBQXNCdEIsU0FBU29DLGFBQVQsQ0FBdUIsUUFBdkIsQ0FBdEI7QUFDQW5CLFFBQU1LLGFBQU4sQ0FBb0J1QixZQUFwQixDQUFpQyxNQUFqQyxFQUF5QyxRQUF6QztBQUNBNUIsUUFBTUssYUFBTixDQUFvQmUsU0FBcEIsQ0FBOEJDLEdBQTlCLENBQWtDLEtBQWxDLEVBQXlDSixrQkFBekMsRUFBNkQsUUFBN0QsRUFBdUUsb0JBQXZFO0FBQ0FqQixRQUFNSyxhQUFOLENBQW9Cd0IsT0FBcEIsQ0FBNEJDLE9BQTVCLEdBQXNDLE9BQXRDO0FBQ0E5QixRQUFNSyxhQUFOLENBQW9CcUIsU0FBcEIsR0FBZ0NWLGtCQUFoQzs7QUFFQTtBQUNBLE1BQUlILFlBQUosRUFBa0I7QUFDaEJiLFVBQU13QixNQUFOLENBQWFTLE1BQWIsQ0FBb0JqQyxNQUFNeUIsS0FBMUIsRUFBaUN6QixNQUFNMkIsU0FBdkM7QUFDRCxHQUZELE1BRU87QUFDTDNCLFVBQU13QixNQUFOLENBQWFaLFdBQWIsQ0FBeUJaLE1BQU0yQixTQUEvQjtBQUNEOztBQUVEM0IsUUFBTVcsSUFBTixDQUFXQyxXQUFYLENBQXVCWixNQUFNekgsT0FBN0I7QUFDQSx5QkFBTXdKLE1BQU4sRUFBYUUsTUFBYix1QkFBb0JqQyxNQUFNZ0MsV0FBMUIsMENBQTBDZCxhQUExQyxJQUF5RGxCLE1BQU1LLGFBQS9EO0FBQ0FMLFFBQU11QixPQUFOLENBQWNVLE1BQWQsQ0FBcUJqQyxNQUFNd0IsTUFBM0IsRUFBbUN4QixNQUFNVyxJQUF6QyxFQUErQ1gsTUFBTStCLE1BQXJEO0FBQ0EvQixRQUFNc0IsTUFBTixDQUFhVixXQUFiLENBQXlCWixNQUFNdUIsT0FBL0I7QUFDQXZCLFFBQU1HLFNBQU4sQ0FBZ0JTLFdBQWhCLENBQTRCWixNQUFNc0IsTUFBbEM7O0FBRUEsU0FBT3RCLEtBQVA7QUFDRCxDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3pJRDs7OztBQUNBOzs7Ozs7QUExQkE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Y0E0Qll2QixNO0lBQUxDLEMsV0FBQUEsQzs7QUFFUDs7Ozs7Ozs7Ozs7Ozs7SUFhcUJ3RCxNO0FBQ25CLG9CQUFjO0FBQUE7O0FBQ1osUUFBSXpELE9BQU8wRCxVQUFQLElBQXFCMUQsT0FBTzBELFVBQVAsQ0FBa0JDLFlBQTNDLEVBQXlEO0FBQ3ZELDRCQUFjQyx3QkFBT0EsTUFBckIsRUFBNkI1RCxPQUFPMEQsVUFBUCxDQUFrQkMsWUFBL0M7QUFDRDs7QUFFREUseUJBQVFDLE9BQVIsQ0FBZ0JGLHVCQUFoQjtBQUNBQyx5QkFBUUUsVUFBUixDQUNFOUQsRUFBRUssUUFBRixFQUNHUyxJQURILENBQ1EsTUFEUixFQUVHSCxJQUZILENBRVEsVUFGUixDQURGOztBQU1BLFdBQU8sSUFBUDtBQUNEOztBQUVEOzs7Ozs7Ozs7Ozs7NkJBUVNvRCxLLEVBQW9CO0FBQUEsVUFBYjlDLE1BQWEsdUVBQUosRUFBSTs7QUFDM0IsVUFBTStDLGtCQUFrQixzQkFBYy9DLE1BQWQsRUFBc0I7QUFDNUNnRCxnQkFBUWpFLEVBQUVLLFFBQUYsRUFDTFMsSUFESyxDQUNBLE1BREEsRUFFTEgsSUFGSyxDQUVBLE9BRkE7QUFEb0MsT0FBdEIsQ0FBeEI7O0FBTUEsYUFBT2lELHFCQUFRTSxRQUFSLENBQWlCSCxLQUFqQixFQUF3QkMsZUFBeEIsQ0FBUDtBQUNEOzs7OztrQkFoQ2tCUixNOzs7Ozs7Ozs7Ozs7Ozs7O0FDM0NyQjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQXlCQTs7a0JBRWU7QUFDYlcsV0FBUyxrQkFESTtBQUViQywwQkFBd0IseUJBRlg7QUFHYkMsK0JBQTZCLHVCQUhoQjtBQUliQyw2QkFBMkIsMkJBSmQ7QUFLYkMsMEJBQXdCLDRCQUxYO0FBTWJDLDBCQUF3QiwrQkFOWDtBQU9iQyx3QkFBc0IsNkJBUFQ7QUFRYkMsb0JBQWtCLHdCQVJMO0FBU2JDLG9CQUFrQixvQkFUTDtBQVViQyx3QkFBc0Isc0JBVlQ7QUFXYkMsb0JBQWtCLHdCQVhMO0FBWWJDLDhCQUE0QixpQ0FaZjtBQWFiQyx3QkFBc0IsMkJBYlQ7QUFjYkMseUJBQXVCLDJCQWRWO0FBZWJDLHlCQUF1Qiw0QkFmVjtBQWdCYkMsd0JBQXNCLGlDQWhCVDtBQWlCYkMscUJBQW1CLDZCQWpCTjtBQWtCYkMsb0JBQWtCLDBCQWxCTDtBQW1CYkMsOEJBQTRCLGlDQW5CZjtBQW9CYkMsZ0NBQThCLG1DQXBCakI7QUFxQmJDLHVDQUFxQywyQ0FyQnhCO0FBc0JiQywrQkFBNkIsa0NBdEJoQjtBQXVCYkMsbUNBQWlDLHlCQXZCcEI7QUF3QmJDLDBDQUF3Qyx3Q0F4QjNCO0FBeUJiQyxpREFBK0MsaURBekJsQztBQTBCYkMsOEJBQTRCLDZCQTFCZjtBQTJCYkMsa0NBQWdDLHVDQTNCbkI7QUE0QmJDLCtCQUE2QixvQ0E1QmhCO0FBNkJiQywwQkFBd0IsK0JBN0JYO0FBOEJiQyx5QkFBdUIsOEJBOUJWO0FBK0JiQywwQkFBd0IsOEJBL0JYO0FBZ0NiQywwQkFBd0IsOEJBaENYO0FBaUNiQyxnQkFBYyx3QkFqQ0Q7QUFrQ2JDLDZCQUEyQiw0QkFsQ2Q7QUFtQ2JDLDBCQUF3QiwyQkFuQ1g7QUFvQ2JDLHlCQUF1QixzQ0FwQ1Y7QUFxQ2JDLHlCQUF1QiwwQkFyQ1Y7QUFzQ2JDLHdCQUFzQixxQ0F0Q1Q7QUF1Q2JDLG9CQUFrQiwwQkF2Q0w7QUF3Q2JDLG1CQUFpQixvQkF4Q0o7QUF5Q2JDLHNCQUFvQiwyQkF6Q1A7QUEwQ2I7QUFDQUMsMkJBQXlCLGdDQTNDWjtBQTRDYkMsK0JBQTZCLG9DQTVDaEI7QUE2Q2JDLGlCQUFlLHFCQTdDRjtBQThDYkMsaUJBQWUsMEJBOUNGO0FBK0NiQyxvQkFBa0IsOEJBL0NMO0FBZ0RiQyxpQkFBZSxxQkFoREY7QUFpRGJDLHNCQUFvQiwyQkFqRFA7QUFrRGJDLHlCQUF1Qiw2QkFsRFY7QUFtRGJDLDJCQUF5QiwrQkFuRFo7QUFvRGJDLCtCQUE2QixtQ0FwRGhCO0FBcURiQywrQkFBNkIsbUNBckRoQjtBQXNEYkMsK0JBQTZCLGtIQXREaEI7QUF1RGJDLGlDQUErQixzREF2RGxCO0FBd0RiQyxtQ0FBaUMsaURBeERwQjtBQXlEYkMseUNBQXVDLDZDQXpEMUI7QUEwRGJDLG9CQUFrQiwwQkFBQ0MsU0FBRDtBQUFBLDhCQUFnQ0EsU0FBaEM7QUFBQSxHQTFETDtBQTJEYkMsMEJBQXdCLGdDQUFDRCxTQUFEO0FBQUEsa0NBQW9DQSxTQUFwQztBQUFBLEdBM0RYO0FBNERiRSxxQkFBbUIsZ0JBNUROO0FBNkRiQyx3QkFBc0IseUJBN0RUO0FBOERiQyx3QkFBc0IseUJBOURUO0FBK0RiQyxpQ0FBK0Isc0NBL0RsQjtBQWdFYkMsaUNBQStCLHNDQWhFbEI7QUFpRWJDLGtDQUFnQyxrREFqRW5CO0FBa0ViQyxzQkFBb0IsNEJBbEVQO0FBbUViQyxrQkFBZ0Isd0JBQUNULFNBQUQ7QUFBQSw4QkFBZ0NBLFNBQWhDO0FBQUEsR0FuRUg7QUFvRWJVLGlCQUFlLGdCQXBFRjtBQXFFYkMsb0JBQWtCLHdCQXJFTDtBQXNFYkMsdUJBQXFCLHNCQXRFUjtBQXVFYkMsdUJBQXFCLHlCQXZFUjtBQXdFYkMsaUJBQWUscUJBeEVGO0FBeUViQyxzQkFBb0IseUJBekVQO0FBMEViQyxrQ0FBZ0MsK0JBMUVuQjtBQTJFYkMsc0NBQW9DLDhDQTNFdkI7QUE0RWJDLHFCQUFtQiw2QkE1RU47QUE2RWJDLDBCQUF3QiwyQkE3RVg7QUE4RWJDLCtCQUE2Qix5QkE5RWhCO0FBK0ViQyxnQ0FBOEIsMEJBL0VqQjtBQWdGYkMsK0JBQTZCLHFDQWhGaEI7QUFpRmJDLCtCQUE2QixxQ0FqRmhCO0FBa0ZiQywyQkFBeUIsMkJBbEZaO0FBbUZiQywyQkFBeUIsc0JBbkZaO0FBb0ZiQywwQkFBd0IscUJBcEZYO0FBcUZiQyw0QkFBMEIsdUJBckZiO0FBc0ZiQywyQkFBeUIsMEJBdEZaO0FBdUZiQyxnQ0FBOEIsZ0NBdkZqQjtBQXdGYkMsNEJBQTBCLDJCQXhGYjtBQXlGYkMsc0JBQW9CLHFCQXpGUDtBQTBGYkMsd0JBQXNCLHVCQTFGVDtBQTJGYkMsMEJBQXdCLDhCQTNGWDtBQTRGYkMsa0JBQWdCLGlCQTVGSDtBQTZGYkMsb0JBQWtCLGlCQTdGTDtBQThGYkMsbUJBQWlCLGtCQTlGSjtBQStGYkMsd0JBQXNCLHVCQS9GVDtBQWdHYkMsdUJBQXFCLHNCQWhHUjtBQWlHYkMsZ0NBQThCLCtCQWpHakI7QUFrR2JDLHlCQUF1Qix3QkFsR1Y7QUFtR2JDLGdDQUE4QiwwQkFuR2pCO0FBb0diQyxnQ0FBOEIsMEJBcEdqQjtBQXFHYkMsNEJBQTBCLHFCQXJHYjtBQXNHYkMsNEJBQTBCLHNCQXRHYjtBQXVHYkMsMkJBQXlCLHNCQXZHWjtBQXdHYkMsNEJBQTBCLHVCQXhHYjtBQXlHYkMsNkJBQTJCLHdCQXpHZDtBQTBHYjtBQUNBQyx1QkFBcUI7QUFDbkIxTCxVQUFNO0FBRGEsR0EzR1I7QUE4R2I7QUFDQTJMLG9CQUFrQjtBQUNoQnZKLFdBQU8scUJBRFM7QUFFaEJ3SixXQUFPLGlDQUZTO0FBR2hCQyxVQUFNLGtFQUhVO0FBSWhCQyxjQUFVLDBCQUpNO0FBS2hCQyxhQUFTO0FBQ1BDLFdBQUssdUJBREU7QUFFUEMsWUFBTSxzQkFGQztBQUdQclIsWUFBTSxrQ0FIQztBQUlQc1IsV0FBSyx1Q0FKRTtBQUtQQyxtQkFBYSxnREFMTjtBQU1QQyxnQkFBVSx3QkFOSDtBQU9QQyx5QkFBbUI7QUFQWjtBQUxPLEdBL0dMO0FBOEhiO0FBQ0FDLHNCQUFvQixxQkEvSFA7QUFnSWJDLGdDQUE4QixrQ0FoSWpCO0FBaUliQyx1QkFBcUIsc0JBaklSO0FBa0liQyxzQkFBb0IscUJBbElQO0FBbUliQywrQkFBNkIsaUNBbkloQjtBQW9JYkMsc0JBQW9CLHFCQXBJUDtBQXFJYkMsbUJBQWlCLGtCQXJJSjtBQXNJYkMsY0FBWSxhQXRJQztBQXVJYkMsMEJBQXdCLGtCQXZJWDtBQXdJYjtBQUNBQyxpQkFBZTtBQUNiQyxVQUFNLDZCQURPO0FBRWJDLGFBQVM7QUFDUEMsYUFBTyxxQ0FEQTtBQUVQQyxZQUFNLHNCQUZDO0FBR1BDLHFCQUFlLCtCQUhSO0FBSVBDLHNCQUFnQixnQ0FKVDtBQUtQQyxxQkFBZSwrQkFMUjtBQU1QQyxzQkFBZ0I7QUFOVCxLQUZJO0FBVWJDLFlBQVE7QUFDTnBCLGdCQUFVLGdDQURKO0FBRU5xQixjQUFRLDhCQUZGO0FBR05DLGdCQUFVO0FBSEosS0FWSztBQWViOUIsV0FBTztBQUNMK0IsWUFBTSxzQkFERDtBQUVML0osY0FBUSw2QkFGSDtBQUdMZ0ssZUFBUztBQUhKLEtBZk07QUFvQmJDLGdCQUFZO0FBQ1ZDLGVBQVMseUJBREM7QUFFVkMsa0JBQVksNkJBRkY7QUFHVkMsZUFBUztBQUhDLEtBcEJDO0FBeUJiQyxZQUFRO0FBQ05DLHlCQUFtQjtBQUNqQkMsdUJBQWUsZ0NBREU7QUFFakJDLHNDQUE4QixnQ0FGYjtBQUdqQkMsOEJBQXNCO0FBSEw7QUFEYixLQXpCSztBQWdDYkMsWUFBUTtBQUNObEIscUJBQWUsb0ZBRFQ7QUFFTkMsc0JBQWdCLG9IQUZWO0FBR05DLHFCQUFlLDZGQUhUO0FBSU5DLHNCQUFnQjtBQUpWO0FBaENLLEdBeklGO0FBZ0xiZ0IsNEJBQTBCLDJCQWhMYjtBQWlMYkMsc0JBQW9CLDRCQWpMUDtBQWtMYkMsa0JBQWdCLHVCQWxMSDtBQW1MYkMsa0JBQWdCLHFCQW5MSDtBQW9MYkMsc0JBQW9CLHFCQXBMUDtBQXFMYkMscUNBQW1DO0FBckx0QixDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDSGY7Ozs7OztjQUVZL04sTTtJQUFMQyxDLFdBQUFBLEM7O0FBRVA7OztBQTVCQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztJQStCcUIrTixrQjtBQUNuQixnQ0FBYztBQUFBOztBQUNaLFNBQUtDLGNBQUw7QUFDRDs7OztxQ0FFZ0I7QUFDZixXQUFLQyw0QkFBTDtBQUNBLFdBQUtDLDZCQUFMO0FBQ0EsV0FBS0MsNEJBQUw7QUFDRDs7O21EQUU4QjtBQUM3Qm5PLFFBQUUsMkJBQUYsRUFBK0JNLEVBQS9CLENBQWtDLE9BQWxDLEVBQTJDLFVBQUM4TixLQUFELEVBQVc7QUFDcERBLGNBQU1DLGNBQU47QUFDQSxZQUFNQyxPQUFPdE8sRUFBRW9PLE1BQU0zTixhQUFSLENBQWI7QUFDQSxZQUFNOE4sV0FBV0QsS0FBS3pOLE9BQUwsQ0FBYSxJQUFiLEVBQW1CMk4sSUFBbkIsRUFBakI7O0FBRUFELGlCQUFTRSxXQUFULENBQXFCLFFBQXJCO0FBQ0QsT0FORDtBQU9EOzs7b0RBRStCO0FBQzlCek8sUUFBRSw2QkFBRixFQUFpQ00sRUFBakMsQ0FBb0MsT0FBcEMsRUFBNkMsVUFBQzhOLEtBQUQsRUFBVztBQUN0RHBPLFVBQUVvTyxNQUFNM04sYUFBUixFQUF1QkksT0FBdkIsQ0FBK0IsSUFBL0IsRUFBcUM2TixRQUFyQyxDQUE4QyxRQUE5QztBQUNELE9BRkQ7QUFHRDs7O21EQUU4QjtBQUM3QjFPLFFBQUUsdUJBQUYsRUFBMkJNLEVBQTNCLENBQThCLE9BQTlCLEVBQXVDLFVBQUM4TixLQUFELEVBQVc7QUFDaEQsWUFBTUUsT0FBT3RPLEVBQUVvTyxNQUFNM04sYUFBUixDQUFiO0FBQ0EsWUFBTWtPLGdCQUFnQkwsS0FBSzNOLElBQUwsQ0FBVSxnQkFBVixDQUF0Qjs7QUFFQVgsVUFBRTRPLDJCQUFpQnJLLHNCQUFuQixFQUEyQ3NLLEdBQTNDLENBQStDLENBQS9DLEVBQWtEQyxjQUFsRCxDQUFpRSxFQUFDQyxVQUFVLFFBQVgsRUFBakU7QUFDQS9PLFVBQUU0TywyQkFBaUJ2SywyQkFBbkIsRUFBZ0R6RCxHQUFoRCxDQUFvRCtOLGFBQXBEO0FBQ0QsT0FORDtBQU9EOzs7OztrQkFuQ2tCWixrQjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ05yQjs7Ozs7O2NBRVloTyxNO0lBQUxDLEMsV0FBQUEsQzs7QUFFUDs7O0FBN0JBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0lBZ0NxQmdQLDRCO0FBQ25CLDBDQUFjO0FBQUE7O0FBQUE7O0FBQ1osU0FBS0MsMEJBQUwsR0FBa0NqUCxFQUFFNE8sMkJBQWlCeEkseUJBQW5CLENBQWxDO0FBQ0EsU0FBSzhJLGtCQUFMLEdBQTBCbFAsRUFBRTRPLDJCQUFpQjFJLHNCQUFuQixDQUExQjs7QUFFQSxXQUFPO0FBQ0xpSiwyQ0FBcUM7QUFBQSxlQUFNLE1BQUtDLGdDQUFMLEVBQU47QUFBQSxPQURoQztBQUVMQyxpQ0FBMkI7QUFBQSxlQUFNLE1BQUtDLGtCQUFMLEVBQU47QUFBQTtBQUZ0QixLQUFQO0FBSUQ7O0FBRUQ7Ozs7Ozs7Ozt1REFLbUM7QUFBQTs7QUFDakN0UCxRQUFFSyxRQUFGLEVBQVlDLEVBQVosQ0FBZSxRQUFmLEVBQXlCc08sMkJBQWlCM0ksc0JBQTFDLEVBQWtFLFVBQUMxRixDQUFELEVBQU87QUFDdkUsWUFBTWdQLGVBQWV2UCxFQUFFTyxFQUFFRSxhQUFKLENBQXJCO0FBQ0EsWUFBTStPLFVBQVVELGFBQWEzTyxHQUFiLEVBQWhCOztBQUVBLFlBQUksQ0FBQzRPLE9BQUwsRUFBYztBQUNaO0FBQ0Q7O0FBRUQsWUFBTTNWLFVBQVUsT0FBS3FWLGtCQUFMLENBQXdCcE8sSUFBeEIsa0JBQTRDME8sT0FBNUMsUUFBd0R6TyxJQUF4RCxHQUErRDBPLElBQS9ELEVBQWhCO0FBQ0EsWUFBTUMsZ0JBQWdCMVAsRUFBRTRPLDJCQUFpQnpJLFlBQW5CLENBQXRCO0FBQ0EsWUFBTXdKLGdCQUFnQkQsY0FBYzlPLEdBQWQsR0FBb0I2TyxJQUFwQixPQUErQjVWLE9BQXJEOztBQUVBLFlBQUk4VixhQUFKLEVBQW1CO0FBQ2pCO0FBQ0Q7O0FBRUQsWUFBSUQsY0FBYzlPLEdBQWQsTUFBdUIsQ0FBQ2IsT0FBTzZQLE9BQVAsQ0FBZSxPQUFLWCwwQkFBTCxDQUFnQ2xPLElBQWhDLEVBQWYsQ0FBNUIsRUFBb0Y7QUFDbEY7QUFDRDs7QUFFRDJPLHNCQUFjOU8sR0FBZCxDQUFrQi9HLE9BQWxCO0FBQ0E2VixzQkFBY0csT0FBZCxDQUFzQixPQUF0QjtBQUNELE9BdEJEO0FBdUJEOztBQUVEOzs7Ozs7Ozt5Q0FLcUI7QUFBQTs7QUFDbkI3UCxRQUFFSyxRQUFGLEVBQVlDLEVBQVosQ0FBZSxPQUFmLEVBQXdCc08sMkJBQWlCakksa0JBQXpDLEVBQTZEO0FBQUEsZUFBTSxPQUFLbUoscUJBQUwsRUFBTjtBQUFBLE9BQTdEO0FBQ0Q7O0FBRUQ7Ozs7Ozs7OzRDQUt3QjtBQUN0QixVQUFNQyxZQUFZL1AsRUFBRTRPLDJCQUFpQm5JLGdCQUFuQixDQUFsQjtBQUNBLFVBQU11SixVQUFVM1AsU0FBUzBCLGFBQVQsQ0FBdUI2TSwyQkFBaUJsSSxlQUF4QyxDQUFoQjs7QUFFQSxVQUFNdUoscUJBQXFCbFEsT0FBT21RLFdBQVAsQ0FBbUIsWUFBTTtBQUNsRCxZQUFJSCxVQUFVSSxRQUFWLENBQW1CLE1BQW5CLENBQUosRUFBZ0M7QUFDOUJILGtCQUFRSSxTQUFSLEdBQW9CSixRQUFRSyxZQUE1QjtBQUNBQyx3QkFBY0wsa0JBQWQ7QUFDRDtBQUNGLE9BTDBCLEVBS3hCLEVBTHdCLENBQTNCO0FBTUQ7Ozs7O2tCQWxFa0JqQiw0Qjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ1JyQjs7Ozs7O2NBRVlqUCxNO0lBQUxDLEMsV0FBQUEsQyxFQTFCUDs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztJQTRCcUJ1USxvQjtBQUNuQixrQ0FBYztBQUFBOztBQUNaLFNBQUtDLG1DQUFMO0FBQ0Q7Ozs7MERBRXFDO0FBQ3BDeFEsUUFBRTRPLDJCQUFpQnpLLE9BQW5CLEVBQTRCN0QsRUFBNUIsQ0FBK0IsT0FBL0IsRUFBd0NzTywyQkFBaUJuSiwrQkFBekQsRUFBMEYsVUFBQzJJLEtBQUQsRUFBVztBQUNuRyxZQUFNRSxPQUFPdE8sRUFBRW9PLE1BQU0zTixhQUFSLENBQWI7O0FBRUFULFVBQUU0TywyQkFBaUJsSixzQ0FBbkIsRUFBMkQ5RSxHQUEzRCxDQUErRDBOLEtBQUszTixJQUFMLENBQVUsdUJBQVYsQ0FBL0Q7QUFDQVgsVUFBRTRPLDJCQUFpQmpKLDZDQUFuQixFQUFrRS9FLEdBQWxFLENBQXNFME4sS0FBSzNOLElBQUwsQ0FBVSxrQkFBVixDQUF0RTtBQUNELE9BTEQ7QUFNRDs7Ozs7a0JBWmtCNFAsb0I7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNIckI7Ozs7QUFDQTs7Ozs7O0FBMUJBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O2NBNEJZeFEsTTtJQUFMQyxDLFdBQUFBLEM7O0lBRWN5USx1QjtBQUNuQixxQ0FBYztBQUFBOztBQUNaLFNBQUtDLE1BQUwsR0FBYyxJQUFJbE4sZ0JBQUosRUFBZDtBQUNEOzs7OzRCQUVPbU4sTyxFQUFTO0FBQ2YzUSxRQUFFNFEsSUFBRixDQUFPLEtBQUtGLE1BQUwsQ0FBWXhNLFFBQVosQ0FBcUIsNEJBQXJCLEVBQW1ELEVBQUN5TSxnQkFBRCxFQUFuRCxDQUFQLEVBQ0dFLElBREgsQ0FDUSxVQUFDQyxRQUFELEVBQWM7QUFDbEI5USxVQUFFNE8sMkJBQWlCaEUsbUJBQWpCLENBQXFDMUwsSUFBdkMsRUFBNkM2UixXQUE3QyxDQUF5REQsUUFBekQ7QUFDRCxPQUhIO0FBSUQ7Ozs7O2tCQVZrQkwsdUI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNMckI7Ozs7QUFDQTs7OztBQUNBOzs7Ozs7Y0FFWTFRLE07SUFBTEMsQyxXQUFBQSxDLEVBN0JQOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0lBK0JxQmdSLHVCO0FBQ25CLHFDQUFjO0FBQUE7O0FBQ1osU0FBS04sTUFBTCxHQUFjLElBQUlsTixnQkFBSixFQUFkO0FBQ0EsU0FBS3lOLGtCQUFMLEdBQTBCLElBQUlsRCw0QkFBSixFQUExQjtBQUNEOzs7OzRCQUVPNEMsTyxFQUFTO0FBQUE7O0FBQ2YzUSxRQUFFa1IsT0FBRixDQUFVLEtBQUtSLE1BQUwsQ0FBWXhNLFFBQVosQ0FBcUIsNEJBQXJCLEVBQW1ELEVBQUN5TSxnQkFBRCxFQUFuRCxDQUFWLEVBQ0dFLElBREgsQ0FDUSxVQUFDQyxRQUFELEVBQWM7QUFDbEI5USxVQUFFNE8sMkJBQWlCdkksc0JBQW5CLEVBQTJDdEYsSUFBM0MsQ0FBZ0QrUCxTQUFTSyxLQUF6RDtBQUNBblIsVUFBRTRPLDJCQUFpQnRJLHFCQUFuQixFQUEwQzhLLElBQTFDLENBQStDTixTQUFTTSxJQUF4RDtBQUNBLGNBQUtILGtCQUFMLENBQXdCakQsY0FBeEI7QUFDRCxPQUxIO0FBTUQ7Ozs7O2tCQWJrQmdELHVCOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ05yQjs7OztBQUNBOzs7Ozs7QUExQkE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Y0E0QllqUixNO0lBQUxDLEMsV0FBQUEsQzs7SUFFY3FSLHNCO0FBQ25CLG9DQUFjO0FBQUE7O0FBQ1osU0FBS1gsTUFBTCxHQUFjLElBQUlsTixnQkFBSixFQUFkO0FBQ0Q7Ozs7NEJBRU9tTixPLEVBQVM7QUFDZjNRLFFBQUVrUixPQUFGLENBQVUsS0FBS1IsTUFBTCxDQUFZeE0sUUFBWixDQUFxQiwyQkFBckIsRUFBa0QsRUFBQ3lNLGdCQUFELEVBQWxELENBQVYsRUFDR0UsSUFESCxDQUNRLFVBQUNDLFFBQUQsRUFBYztBQUNsQixZQUFJLENBQUNBLFFBQUQsSUFBYSxDQUFDQSxTQUFTUSxRQUF2QixJQUFtQyxvQkFBWVIsU0FBU1EsUUFBckIsRUFBK0I3VSxNQUEvQixJQUF5QyxDQUFoRixFQUFtRjtBQUNqRjtBQUNEOztBQUVELFlBQU04VSx3QkFBd0J2UixFQUFFNE8sMkJBQWlCdEsseUJBQW5CLENBQTlCO0FBQ0EsWUFBTWtOLDJCQUEyQnhSLEVBQUU0TywyQkFBaUJwRix1QkFBbkIsQ0FBakM7QUFDQSxZQUFNaUkseUJBQXlCRCx5QkFBeUIxUSxJQUF6QixDQUE4QixnQkFBOUIsQ0FBL0I7QUFDQSxZQUFNNFEsNEJBQTRCMVIsRUFBRTRPLDJCQUFpQnJFLHdCQUFuQixDQUFsQztBQUNBLFlBQU1vSCw0QkFBNEIzUixFQUFFNE8sMkJBQWlCOUosMEJBQW5CLENBQWxDO0FBQ0EyTSwrQkFBdUJHLEtBQXZCO0FBQ0FMLDhCQUFzQkssS0FBdEI7QUFDQUYsa0NBQTBCRSxLQUExQjtBQUNBRCxrQ0FBMEJDLEtBQTFCOztBQUVBLDRCQUFZZCxTQUFTUSxRQUFyQixFQUErQjFVLE9BQS9CLENBQXVDLFVBQUNpVixXQUFELEVBQWlCO0FBQ3RELGNBQU1DLFlBQVloQixTQUFTUSxRQUFULENBQWtCTyxXQUFsQixDQUFsQjtBQUNBLGNBQU1FLDBCQUEwQkYsWUFBWTVWLEtBQVosQ0FBa0IsS0FBbEIsRUFBeUIsQ0FBekIsQ0FBaEM7O0FBRUF3VixpQ0FBdUJsTyxNQUF2QixxQkFBZ0R1TyxTQUFoRCxVQUE4REMsdUJBQTlEO0FBQ0FSLGdDQUFzQmhPLE1BQXRCLHFCQUErQ3VPLFNBQS9DLFVBQTZEQyx1QkFBN0Q7QUFDQUwsb0NBQTBCbk8sTUFBMUIscUJBQW1EdU8sU0FBbkQsVUFBaUVDLHVCQUFqRTtBQUNBSixvQ0FBMEJwTyxNQUExQixxQkFBbUR1TyxTQUFuRCxVQUFpRUQsV0FBakU7QUFDRCxTQVJEOztBQVVBeFIsaUJBQVMwQixhQUFULENBQXVCNk0sMkJBQWlCcEYsdUJBQXhDLEVBQWlFd0ksYUFBakUsR0FBaUYsQ0FBakY7QUFDRCxPQTNCSDtBQTRCRDs7Ozs7a0JBbENrQlgsc0I7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNMckI7Ozs7QUFDQTs7Ozs7O0FBMUJBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O2NBNEJZdFIsTTtJQUFMQyxDLFdBQUFBLEM7O0lBRWNpUyxzQjtBQUNuQixvQ0FBYztBQUFBOztBQUNaLFNBQUt2QixNQUFMLEdBQWMsSUFBSWxOLGdCQUFKLEVBQWQ7QUFDRDs7Ozs0QkFFT21OLE8sRUFBUztBQUNmM1EsUUFBRTRRLElBQUYsQ0FBTyxLQUFLRixNQUFMLENBQVl4TSxRQUFaLENBQXFCLDJCQUFyQixFQUFrRCxFQUFDeU0sZ0JBQUQsRUFBbEQsQ0FBUCxFQUNHRSxJQURILENBRUksVUFBQ0MsUUFBRCxFQUFjO0FBQ1o5USxVQUFFNE8sMkJBQWlCcEssc0JBQW5CLEVBQTJDeEMsTUFBM0M7QUFDQWhDLFVBQUs0TywyQkFBaUJySyxzQkFBdEIsa0JBQTJEMk4sT0FBM0QsQ0FBbUVwQixRQUFuRTtBQUNELE9BTEwsRUFNSSxVQUFDQSxRQUFELEVBQWM7QUFDWixZQUFJQSxTQUFTcUIsWUFBVCxJQUF5QnJCLFNBQVNxQixZQUFULENBQXNCdFksT0FBbkQsRUFBNEQ7QUFDMURtRyxZQUFFb1MsS0FBRixDQUFRQyxLQUFSLENBQWMsRUFBQ3hZLFNBQVNpWCxTQUFTcUIsWUFBVCxDQUFzQnRZLE9BQWhDLEVBQWQ7QUFDRDtBQUNGLE9BVkw7QUFZRDs7Ozs7a0JBbEJrQm9ZLHNCOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDTHJCOzs7O0FBQ0E7Ozs7OztBQTFCQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztjQTRCWWxTLE07SUFBTEMsQyxXQUFBQSxDOztJQUVjc1Msb0I7QUFDbkIsa0NBQWM7QUFBQTs7QUFDWixTQUFLNUIsTUFBTCxHQUFjLElBQUlsTixnQkFBSixFQUFkO0FBQ0Q7Ozs7NEJBRU9tTixPLEVBQVM7QUFDZjNRLFFBQUVrUixPQUFGLENBQVUsS0FBS1IsTUFBTCxDQUFZeE0sUUFBWixDQUFxQix5QkFBckIsRUFBZ0QsRUFBQ3lNLGdCQUFELEVBQWhELENBQVYsRUFBc0VFLElBQXRFLENBQTJFLFVBQUNDLFFBQUQsRUFBYztBQUN2RjlRLFVBQUU0TywyQkFBaUI3QyxVQUFuQixFQUErQmhMLElBQS9CLENBQW9DK1AsU0FBU3lCLG1CQUE3QztBQUNBdlMsVUFBRTRPLDJCQUFpQmxELG1CQUFuQixFQUF3QzNLLElBQXhDLE9BQWlEK1AsU0FBUzBCLHdCQUExRDtBQUNBeFMsVUFBRTRPLDJCQUFpQm5ELDRCQUFuQixFQUFpRGdILFdBQWpELENBQTZELFFBQTdELEVBQXVFLENBQUMzQixTQUFTNEIsd0JBQWpGO0FBQ0ExUyxVQUFFNE8sMkJBQWlCcEQsa0JBQW5CLEVBQXVDekssSUFBdkMsQ0FBNEMrUCxTQUFTNkIsc0JBQXJEO0FBQ0EzUyxVQUFFNE8sMkJBQWlCL0Msa0JBQW5CLEVBQXVDOUssSUFBdkMsQ0FBNEMrUCxTQUFTOEIsc0JBQXJEO0FBQ0E1UyxVQUFFNE8sMkJBQWlCaEQsMkJBQW5CLEVBQWdENkcsV0FBaEQsQ0FBNEQsUUFBNUQsRUFBc0UsQ0FBQzNCLFNBQVMrQixzQkFBaEY7QUFDQTdTLFVBQUU0TywyQkFBaUI5QyxlQUFuQixFQUFvQy9LLElBQXBDLENBQXlDK1AsU0FBU2dDLG1CQUFsRDtBQUNELE9BUkQ7QUFTRDs7O3lDQUVvQm5DLE8sRUFBUztBQUM1QjNRLFFBQUVrUixPQUFGLENBQVUsS0FBS1IsTUFBTCxDQUFZeE0sUUFBWixDQUFxQiw2QkFBckIsRUFBb0QsRUFBQ3lNLGdCQUFELEVBQXBELENBQVYsRUFBMEVFLElBQTFFLENBQStFLFVBQUNrQyxpQkFBRCxFQUF1QjtBQUNwR0EsMEJBQWtCblcsT0FBbEIsQ0FBMEIsVUFBQ3lRLGFBQUQsRUFBbUI7QUFDM0MsY0FBTTJGLG1CQUFtQnBFLDJCQUFpQmpILGdCQUFqQixDQUFrQzBGLGNBQWM0RixhQUFoRCxDQUF6QjtBQUNBLGNBQUlDLFlBQVlsVCxFQUFFcU4sY0FBYy9CLFFBQWhCLENBQWhCOztBQUVBLGNBQUkrQixjQUFjL0IsUUFBZCxHQUF5QixDQUE3QixFQUFnQztBQUM5QjRILHdCQUFZQSxVQUFVQyxJQUFWLENBQWUsNERBQWYsQ0FBWjtBQUNEOztBQUVEblQsWUFBS2dULGdCQUFMLFNBQXlCcEUsMkJBQWlCM0Usb0JBQTFDLEVBQWtFbEosSUFBbEUsQ0FBdUVzTSxjQUFjK0YsU0FBckY7QUFDQXBULFlBQUtnVCxnQkFBTCxTQUF5QnBFLDJCQUFpQjFFLG1CQUExQyxFQUFpRWtILElBQWpFLENBQXNFOEIsVUFBVTlCLElBQVYsRUFBdEU7QUFDQXBSLFlBQUtnVCxnQkFBTCxTQUF5QnBFLDJCQUFpQnpFLDRCQUExQyxFQUEwRXBKLElBQTFFLENBQStFc00sY0FBYzlCLGlCQUE3RjtBQUNBdkwsWUFBS2dULGdCQUFMLFNBQXlCcEUsMkJBQWlCeEUscUJBQTFDLEVBQW1FckosSUFBbkUsQ0FBd0VzTSxjQUFjZ0csVUFBdEY7O0FBRUE7QUFDQSxjQUFNQyxvQkFBb0J0VCxFQUFFNE8sMkJBQWlCdkcsY0FBakIsQ0FBZ0NnRixjQUFjNEYsYUFBOUMsQ0FBRixDQUExQjs7QUFFQUssNEJBQWtCM1MsSUFBbEIsQ0FBdUIsd0JBQXZCLEVBQWlEME0sY0FBY2tHLG1CQUEvRDtBQUNBRCw0QkFBa0IzUyxJQUFsQixDQUF1Qix3QkFBdkIsRUFBaUQwTSxjQUFjbUcsbUJBQS9EO0FBQ0FGLDRCQUFrQjNTLElBQWxCLENBQXVCLGtCQUF2QixFQUEyQzBNLGNBQWMvQixRQUF6RDtBQUNELFNBbkJEO0FBb0JELE9BckJEO0FBc0JEOztBQUVEOzs7Ozs7Ozs7OztpREFRNkJtSSxVLEVBQVk3TCxTLEVBQVc4TCxhLEVBQWU1QixTLEVBQVdtQixhLEVBQWU7QUFDM0YsVUFBTVUsY0FBY3RULFNBQVN1VCxnQkFBVCxDQUEwQixnQkFBMUIsQ0FBcEI7QUFDQTtBQUNBLFVBQU1DLG9CQUFvQkMsT0FBT2xNLFNBQVAsQ0FBMUI7QUFDQSxVQUFNbU0sd0JBQXdCRCxPQUFPSixhQUFQLENBQTlCO0FBQ0EsVUFBTU0scUJBQXFCRixPQUFPTCxVQUFQLENBQTNCO0FBQ0EsVUFBSVEsK0JBQStCLEtBQW5DO0FBQ0EsVUFBSUMsK0JBQStCLEtBQW5DOztBQUVBUCxrQkFBWS9XLE9BQVosQ0FBb0IsVUFBQ3VYLFVBQUQsRUFBZ0I7QUFDbEMsWUFBTUMsZUFBZXBVLEVBQUVtVSxVQUFGLEVBQWNFLElBQWQsQ0FBbUIsSUFBbkIsQ0FBckI7O0FBRUE7QUFDQSxZQUFJcEIsaUJBQWlCbUIsbUNBQWlDbkIsYUFBdEQsRUFBdUU7QUFDckU7QUFDRDs7QUFFRCxZQUFNNUssaUJBQWlCckksUUFBTW9VLFlBQU4sU0FBc0J4RiwyQkFBaUJ4RyxrQkFBdkMsQ0FBdkI7QUFDQSxZQUFNa00sd0JBQXdCUixPQUFPekwsZUFBZTFILElBQWYsQ0FBb0Isa0JBQXBCLENBQVAsQ0FBOUI7O0FBRUEsWUFBTTRULG1CQUFtQlQsT0FBT3pMLGVBQWUxSCxJQUFmLENBQW9CLFlBQXBCLENBQVAsQ0FBekI7QUFDQSxZQUFNNlQsdUJBQXVCVixPQUFPekwsZUFBZTFILElBQWYsQ0FBb0IsZ0JBQXBCLENBQVAsQ0FBN0I7O0FBRUEsWUFBSTRULHFCQUFxQlYsaUJBQXJCLElBQTBDVyx5QkFBeUJULHFCQUF2RSxFQUE4RjtBQUM1RjtBQUNEOztBQUVELFlBQUlDLHVCQUF1QkYsT0FBT3pMLGVBQWUxSCxJQUFmLENBQW9CLHdCQUFwQixDQUFQLENBQTNCLEVBQWtGO0FBQ2hGLGNBQUltUixjQUFjLEVBQWQsSUFBcUJBLGFBQWF3QyxxQkFBYixJQUFzQ3hDLGNBQWN3QyxxQkFBN0UsRUFBcUc7QUFDbkdKLDJDQUErQixJQUEvQjtBQUNELFdBRkQsTUFFTztBQUNMRCwyQ0FBK0IsSUFBL0I7QUFDRDtBQUNGO0FBQ0YsT0F6QkQ7O0FBMkJBLFVBQUlBLDRCQUFKLEVBQWtDO0FBQ2hDLGVBQU8sU0FBUDtBQUNEO0FBQ0QsVUFBSUMsNEJBQUosRUFBa0M7QUFDaEMsZUFBTyxTQUFQO0FBQ0Q7O0FBRUQsYUFBTyxJQUFQO0FBQ0Q7Ozs7O2tCQTlGa0I1QixvQjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzlCckI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7SUF5QnFCbUMsVzs7Ozs7Ozt5Q0FDRUMsVyxFQUFhQyxjLEVBQWdCQyxpQixFQUFtQjtBQUNuRSxVQUFJQyxlQUFlQyxXQUFXSixXQUFYLENBQW5COztBQUVBLFVBQUlHLGVBQWUsQ0FBZixJQUFvQixxQkFBYUEsWUFBYixDQUF4QixFQUFvRDtBQUNsREEsdUJBQWUsQ0FBZjtBQUNEO0FBQ0QsVUFBTUUsVUFBVUosaUJBQWlCLEdBQWpCLEdBQXVCLENBQXZDOztBQUVBLGFBQU81VSxPQUFPaVYsUUFBUCxDQUFnQkgsZUFBZUUsT0FBL0IsRUFBd0NILGlCQUF4QyxDQUFQO0FBQ0Q7Ozt5Q0FFb0JLLFcsRUFBYU4sYyxFQUFnQkMsaUIsRUFBbUI7QUFDbkUsVUFBSU0sZUFBZUosV0FBV0csV0FBWCxDQUFuQjs7QUFFQSxVQUFJQyxlQUFlLENBQWYsSUFBb0IscUJBQWFBLFlBQWIsQ0FBeEIsRUFBb0Q7QUFDbERBLHVCQUFlLENBQWY7QUFDRDtBQUNELFVBQU1ILFVBQVVKLGlCQUFpQixHQUFqQixHQUF1QixDQUF2Qzs7QUFFQSxhQUFPNVUsT0FBT2lWLFFBQVAsQ0FBZ0JFLGVBQWVILE9BQS9CLEVBQXdDSCxpQkFBeEMsQ0FBUDtBQUNEOzs7d0NBRW1CdEosUSxFQUFVOEgsUyxFQUFXd0IsaUIsRUFBbUI7QUFDMUQsYUFBTzdVLE9BQU9pVixRQUFQLENBQWdCNUIsWUFBWTlILFFBQTVCLEVBQXNDc0osaUJBQXRDLENBQVA7QUFDRDs7Ozs7a0JBekJrQkgsVzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDRHJCOzs7O0FBQ0E7Ozs7OztBQXpCQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O2NBMkJZMVUsTTtJQUFMQyxDLFdBQUFBLEM7O0lBRWNtVix3QjtBQUNuQixvQ0FBWUMsS0FBWixFQUFtQjtBQUFBOztBQUNqQixTQUFLQyxtQkFBTCxHQUEyQixJQUEzQjtBQUNBLFNBQUszRSxNQUFMLEdBQWMsSUFBSWxOLGdCQUFKLEVBQWQ7QUFDQSxTQUFLNFIsS0FBTCxHQUFhQSxLQUFiO0FBQ0EsU0FBS0UsT0FBTCxHQUFlLEVBQWY7QUFDQSxTQUFLQyxZQUFMLEdBQW9CdlYsRUFBRTRPLDJCQUFpQi9GLGtDQUFuQixDQUFwQjtBQUNBOzs7QUFHQSxTQUFLMk0scUJBQUwsR0FBNkIsWUFBTSxDQUFFLENBQXJDO0FBQ0Q7Ozs7c0NBRWlCO0FBQUE7O0FBQ2hCLFdBQUtKLEtBQUwsQ0FBVzlVLEVBQVgsQ0FBYyxPQUFkLEVBQXVCLFVBQUM4TixLQUFELEVBQVc7QUFDaENBLGNBQU1xSCx3QkFBTjtBQUNBLGNBQUtDLGFBQUwsQ0FBbUIsTUFBS0osT0FBeEI7QUFDRCxPQUhEOztBQUtBLFdBQUtGLEtBQUwsQ0FBVzlVLEVBQVgsQ0FBYyxPQUFkLEVBQXVCLFVBQUM4TixLQUFEO0FBQUEsZUFBVyxNQUFLdUgsV0FBTCxDQUFpQnZILE1BQU0zTixhQUF2QixDQUFYO0FBQUEsT0FBdkI7QUFDQVQsUUFBRUssUUFBRixFQUFZQyxFQUFaLENBQWUsT0FBZixFQUF3QjtBQUFBLGVBQU0sTUFBS2lWLFlBQUwsQ0FBa0JLLElBQWxCLEVBQU47QUFBQSxPQUF4QjtBQUNEOzs7Z0NBRVdSLEssRUFBTztBQUFBOztBQUNqQlMsbUJBQWEsS0FBS0MsZUFBbEI7O0FBRUE7QUFDQSxVQUFJVixNQUFNVyxLQUFOLENBQVl0WixNQUFaLEdBQXFCLENBQXpCLEVBQTRCO0FBQzFCO0FBQ0Q7O0FBRUQsV0FBS3FaLGVBQUwsR0FBdUJFLFdBQVcsWUFBTTtBQUN0QyxlQUFLQyxNQUFMLENBQVliLE1BQU1XLEtBQWxCLEVBQXlCL1YsRUFBRW9WLEtBQUYsRUFBU3pVLElBQVQsQ0FBYyxVQUFkLENBQXpCLEVBQW9EWCxFQUFFb1YsS0FBRixFQUFTelUsSUFBVCxDQUFjLE9BQWQsQ0FBcEQ7QUFDRCxPQUZzQixFQUVwQixHQUZvQixDQUF2QjtBQUdEOzs7MkJBRU1zVixPLEVBQVFDLFEsRUFBVXZGLE8sRUFBUztBQUFBOztBQUNoQyxVQUFNMVAsU0FBUyxFQUFDa1YsZUFBZUYsT0FBaEIsRUFBZjs7QUFFQSxVQUFJQyxRQUFKLEVBQWM7QUFDWmpWLGVBQU9tVixXQUFQLEdBQXFCRixRQUFyQjtBQUNEOztBQUVELFVBQUl2RixPQUFKLEVBQWE7QUFDWDFQLGVBQU9vVixRQUFQLEdBQWtCMUYsT0FBbEI7QUFDRDs7QUFFRCxVQUFJLEtBQUswRSxtQkFBTCxLQUE2QixJQUFqQyxFQUF1QztBQUNyQyxhQUFLQSxtQkFBTCxDQUF5QmpKLEtBQXpCO0FBQ0Q7O0FBRUQsV0FBS2lKLG1CQUFMLEdBQTJCclYsRUFBRTZPLEdBQUYsQ0FBTSxLQUFLNkIsTUFBTCxDQUFZeE0sUUFBWixDQUFxQiw4QkFBckIsRUFBcURqRCxNQUFyRCxDQUFOLENBQTNCO0FBQ0EsV0FBS29VLG1CQUFMLENBQ0d4RSxJQURILENBQ1EsVUFBQ0MsUUFBRDtBQUFBLGVBQWMsT0FBSzRFLGFBQUwsQ0FBbUI1RSxRQUFuQixDQUFkO0FBQUEsT0FEUixFQUVHd0YsTUFGSCxDQUVVLFlBQU07QUFDWixlQUFLakIsbUJBQUwsR0FBMkIsSUFBM0I7QUFDRCxPQUpIO0FBS0Q7OztrQ0FFYUMsTyxFQUFTO0FBQUE7O0FBQ3JCLFdBQUtDLFlBQUwsQ0FBa0IzRCxLQUFsQjs7QUFFQSxVQUFJLENBQUMwRCxPQUFELElBQVksQ0FBQ0EsUUFBUWlCLFFBQXJCLElBQWlDLG9CQUFZakIsUUFBUWlCLFFBQXBCLEVBQThCOVosTUFBOUIsSUFBd0MsQ0FBN0UsRUFBZ0Y7QUFDOUUsYUFBSzhZLFlBQUwsQ0FBa0JLLElBQWxCO0FBQ0E7QUFDRDs7QUFFRCxXQUFLTixPQUFMLEdBQWVBLFFBQVFpQixRQUF2Qjs7QUFFQSw0QkFBYyxLQUFLakIsT0FBbkIsRUFBNEIxWSxPQUE1QixDQUFvQyxVQUFDZ0UsR0FBRCxFQUFTO0FBQzNDLFlBQU11SyxPQUFPbkwseUNBQXVDWSxJQUFJZ0gsU0FBM0MsbUJBQWtFaEgsSUFBSTlHLElBQXRFLFVBQWI7O0FBRUFxUixhQUFLN0ssRUFBTCxDQUFRLE9BQVIsRUFBaUIsVUFBQzhOLEtBQUQsRUFBVztBQUMxQkEsZ0JBQU1DLGNBQU47QUFDQSxpQkFBS21JLGFBQUwsQ0FBbUJ4VyxFQUFFb08sTUFBTXFJLE1BQVIsRUFBZ0I5VixJQUFoQixDQUFxQixJQUFyQixDQUFuQjtBQUNELFNBSEQ7O0FBS0EsZUFBSzRVLFlBQUwsQ0FBa0JoUyxNQUFsQixDQUF5QjRILElBQXpCO0FBQ0QsT0FURDs7QUFXQSxXQUFLb0ssWUFBTCxDQUFrQjdULElBQWxCO0FBQ0Q7OztrQ0FFYU4sRSxFQUFJO0FBQ2hCLFVBQU1zVixrQkFBa0IsS0FBS3BCLE9BQUwsQ0FBYXFCLE1BQWIsQ0FBb0IsVUFBQzFMLE9BQUQ7QUFBQSxlQUFhQSxRQUFRckQsU0FBUixLQUFzQnhHLEVBQW5DO0FBQUEsT0FBcEIsQ0FBeEI7O0FBRUEsVUFBSXNWLGdCQUFnQmphLE1BQWhCLEtBQTJCLENBQS9CLEVBQWtDO0FBQ2hDLGFBQUsyWSxLQUFMLENBQVd4VSxHQUFYLENBQWU4VixnQkFBZ0IsQ0FBaEIsRUFBbUI1YyxJQUFsQztBQUNBLGFBQUswYixxQkFBTCxDQUEyQmtCLGdCQUFnQixDQUFoQixDQUEzQjtBQUNEO0FBQ0Y7Ozs7O2tCQTFGa0J2Qix3Qjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDSnJCOzs7O0FBQ0E7Ozs7QUFDQTs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7Ozs7QUFoQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Y0FrQ1lwVixNO0lBQUxDLEMsV0FBQUEsQzs7SUFFYzRXLGU7QUFDbkIsNkJBQWM7QUFBQTs7QUFDWixTQUFLbEcsTUFBTCxHQUFjLElBQUlsTixnQkFBSixFQUFkO0FBQ0EsU0FBS2dGLG1CQUFMLEdBQTJCeEksRUFBRTRPLDJCQUFpQnBHLG1CQUFuQixDQUEzQjtBQUNBLFNBQUtxTyxjQUFMLEdBQXNCN1csRUFBRTRPLDJCQUFpQjlGLGlCQUFuQixDQUF0QjtBQUNBLFNBQUtnTyxpQkFBTCxHQUF5QjlXLEVBQUU0TywyQkFBaUI1RiwyQkFBbkIsQ0FBekI7QUFDQSxTQUFLK04sa0JBQUwsR0FBMEIvVyxFQUFFNE8sMkJBQWlCM0YsNEJBQW5CLENBQTFCO0FBQ0EsU0FBSytOLHFCQUFMLEdBQTZCaFgsRUFBRTRPLDJCQUFpQnpGLDJCQUFuQixDQUE3QjtBQUNBLFNBQUs4TixxQkFBTCxHQUE2QmpYLEVBQUU0TywyQkFBaUIxRiwyQkFBbkIsQ0FBN0I7QUFDQSxTQUFLZ08sWUFBTCxHQUFvQmxYLEVBQUU0TywyQkFBaUI3RixzQkFBbkIsQ0FBcEI7QUFDQSxTQUFLb08sYUFBTCxHQUFxQm5YLEVBQUU0TywyQkFBaUJ4Rix1QkFBbkIsQ0FBckI7QUFDQSxTQUFLZ08sYUFBTCxHQUFxQnBYLEVBQUU0TywyQkFBaUJ2Rix1QkFBbkIsQ0FBckI7QUFDQSxTQUFLZ08sWUFBTCxHQUFvQnJYLEVBQUU0TywyQkFBaUJ0RixzQkFBbkIsQ0FBcEI7QUFDQSxTQUFLZ08sY0FBTCxHQUFzQnRYLEVBQUU0TywyQkFBaUJyRix3QkFBbkIsQ0FBdEI7QUFDQSxTQUFLZ08sYUFBTCxHQUFxQnZYLEVBQUU0TywyQkFBaUJwRix1QkFBbkIsQ0FBckI7QUFDQSxTQUFLZ08sa0JBQUwsR0FBMEJ4WCxFQUFFNE8sMkJBQWlCbkYsNEJBQW5CLENBQTFCO0FBQ0EsU0FBS2dPLGlCQUFMLEdBQXlCelgsRUFBRTRPLDJCQUFpQnRHLGFBQW5CLENBQXpCO0FBQ0EsU0FBS29QLFNBQUwsR0FBaUIsSUFBakI7QUFDQSxTQUFLQyxhQUFMO0FBQ0EsU0FBSzFNLE9BQUwsR0FBZSxFQUFmO0FBQ0EsU0FBSzJKLGlCQUFMLEdBQXlCNVUsRUFBRTRPLDJCQUFpQjNILGFBQW5CLEVBQWtDdEcsSUFBbEMsQ0FBdUMsbUJBQXZDLENBQXpCO0FBQ0EsU0FBS2lYLGtCQUFMLEdBQTBCLElBQUluRCxxQkFBSixFQUExQjtBQUNBLFNBQUtvRCxvQkFBTCxHQUE0QixJQUFJQyw4QkFBSixFQUE1QjtBQUNBLFNBQUtDLG9CQUFMLEdBQTRCLElBQUl6Riw4QkFBSixFQUE1QjtBQUNBLFNBQUswRixrQkFBTCxHQUEwQmhZLEVBQUU0TywyQkFBaUJsRyxhQUFuQixFQUFrQy9ILElBQWxDLENBQXVDLG9CQUF2QyxDQUExQjtBQUNBLFNBQUtzVSxXQUFMLEdBQW1CLElBQW5CO0FBQ0EsU0FBS1AsV0FBTCxHQUFtQixJQUFuQjtBQUNEOzs7O29DQUVlO0FBQUE7O0FBQ2QsV0FBS3FDLGtCQUFMLENBQXdCelcsRUFBeEIsQ0FBMkIsUUFBM0IsRUFBcUMsVUFBQzhOLEtBQUQsRUFBVztBQUM5QyxZQUFNNkcsY0FBY2xWLE9BQU9pVixRQUFQLENBQ2xCaFYsRUFBRW9PLE1BQU0zTixhQUFSLEVBQ0dLLElBREgsQ0FDUSxXQURSLEVBRUdILElBRkgsQ0FFUSxrQkFGUixDQURrQixFQUlsQixNQUFLaVUsaUJBSmEsQ0FBcEI7QUFNQSxjQUFLcUMscUJBQUwsQ0FBMkJyVyxHQUEzQixDQUErQnFVLFdBQS9CO0FBQ0EsY0FBS0EsV0FBTCxHQUFtQkgsV0FBV0csV0FBWCxDQUFuQjs7QUFFQSxZQUFNUCxjQUFjM1UsT0FBT2lWLFFBQVAsQ0FDbEJoVixFQUFFb08sTUFBTTNOLGFBQVIsRUFDR0ssSUFESCxDQUNRLFdBRFIsRUFFR0gsSUFGSCxDQUVRLGtCQUZSLENBRGtCLEVBSWxCLE1BQUtpVSxpQkFKYSxDQUFwQjtBQU1BLGNBQUtvQyxxQkFBTCxDQUEyQnBXLEdBQTNCLENBQStCOFQsV0FBL0I7QUFDQSxjQUFLQSxXQUFMLEdBQW1CSSxXQUFXSixXQUFYLENBQW5COztBQUVBLGNBQUsyQyxZQUFMLENBQWtCakcsSUFBbEIsQ0FDRXBSLEVBQUVvTyxNQUFNM04sYUFBUixFQUNHSyxJQURILENBQ1EsV0FEUixFQUVHSCxJQUZILENBRVEsVUFGUixDQURGOztBQU1BLGNBQUsrVyxTQUFMLEdBQWlCMVgsRUFBRW9PLE1BQU0zTixhQUFSLEVBQ2RLLElBRGMsQ0FDVCxXQURTLEVBRWRILElBRmMsQ0FFVCxPQUZTLENBQWpCOztBQUlBLGNBQUt3VyxhQUFMLENBQW1CdEgsT0FBbkIsQ0FBMkIsUUFBM0I7QUFDQSxjQUFLZ0ksb0JBQUwsQ0FBMEJJLFlBQTFCLENBQXVDckosMkJBQWlCN0csb0JBQXhEO0FBQ0QsT0EvQkQ7O0FBaUNBLFdBQUtvUCxhQUFMLENBQW1CN1csRUFBbkIsQ0FBc0IsY0FBdEIsRUFBc0MsVUFBQzhOLEtBQUQsRUFBVztBQUMvQyxZQUFJLE1BQUtzSixTQUFMLEtBQW1CLElBQXZCLEVBQTZCO0FBQzNCLGNBQU1RLGNBQWNwRSxPQUFPMUYsTUFBTXFJLE1BQU4sQ0FBYVYsS0FBcEIsQ0FBcEI7QUFDQSxjQUFNb0MscUJBQXFCLE1BQUtULFNBQUwsR0FBaUJRLFdBQTVDO0FBQ0EsY0FBTUUsc0JBQXNCLE1BQUtoQixhQUFMLENBQW1CelcsSUFBbkIsQ0FBd0IscUJBQXhCLENBQTVCO0FBQ0EsZ0JBQUt5VyxhQUFMLENBQW1CclcsSUFBbkIsQ0FBd0JvWCxrQkFBeEI7QUFDQSxnQkFBS2YsYUFBTCxDQUFtQjNFLFdBQW5CLENBQStCLDhCQUEvQixFQUErRDBGLHFCQUFxQixDQUFwRjtBQUNBLGNBQU1FLHNCQUFzQkgsZUFBZSxDQUFmLElBQXFCQyxxQkFBcUIsQ0FBckIsSUFBMEIsQ0FBQ0MsbUJBQTVFO0FBQ0EsZ0JBQUs1UCxtQkFBTCxDQUF5QjhQLElBQXpCLENBQThCLFVBQTlCLEVBQTBDRCxtQkFBMUM7QUFDQSxnQkFBS2QsYUFBTCxDQUFtQmUsSUFBbkIsQ0FBd0IsVUFBeEIsRUFBb0MsQ0FBQ0YsbUJBQUQsSUFBd0JELHFCQUFxQixDQUFqRjs7QUFFQSxnQkFBS3pELFdBQUwsR0FBbUJJLFdBQVcsTUFBS2tDLHFCQUFMLENBQTJCcFcsR0FBM0IsRUFBWCxDQUFuQjtBQUNBLGdCQUFLMFcsY0FBTCxDQUFvQmxHLElBQXBCLENBQ0UsTUFBS3dHLGtCQUFMLENBQXdCVyxtQkFBeEIsQ0FDRUwsV0FERixFQUVFLE1BQUtGLGtCQUFMLEdBQTBCLE1BQUt0RCxXQUEvQixHQUE2QyxNQUFLTyxXQUZwRCxFQUdFLE1BQUtMLGlCQUhQLENBREY7QUFPRDtBQUNGLE9BcEJEOztBQXNCQSxXQUFLaUMsY0FBTCxDQUFvQnZXLEVBQXBCLENBQXVCLFFBQXZCLEVBQWlDLFlBQU07QUFDckMsY0FBS2tJLG1CQUFMLENBQXlCZ1EsVUFBekIsQ0FBb0MsVUFBcEM7QUFDQSxjQUFLakIsYUFBTCxDQUFtQmlCLFVBQW5CLENBQThCLFVBQTlCO0FBQ0QsT0FIRDs7QUFLQSxXQUFLeEIscUJBQUwsQ0FBMkIxVyxFQUEzQixDQUE4QixjQUE5QixFQUE4QyxVQUFDOE4sS0FBRCxFQUFXO0FBQ3ZELGNBQUtzRyxXQUFMLEdBQW1CSSxXQUFXMUcsTUFBTXFJLE1BQU4sQ0FBYVYsS0FBeEIsQ0FBbkI7QUFDQSxjQUFLZCxXQUFMLEdBQW1CLE1BQUsyQyxrQkFBTCxDQUF3QmEsb0JBQXhCLENBQ2pCLE1BQUsvRCxXQURZLEVBRWpCLE1BQUt3QyxZQUFMLENBQWtCdFcsR0FBbEIsRUFGaUIsRUFHakIsTUFBS2dVLGlCQUhZLENBQW5CO0FBS0EsWUFBTXRKLFdBQVc1TSxTQUFTLE1BQUt5WSxhQUFMLENBQW1CdlcsR0FBbkIsRUFBVCxFQUFtQyxFQUFuQyxDQUFqQjs7QUFFQSxjQUFLcVcscUJBQUwsQ0FBMkJyVyxHQUEzQixDQUErQixNQUFLcVUsV0FBcEM7QUFDQSxjQUFLcUMsY0FBTCxDQUFvQmxHLElBQXBCLENBQ0UsTUFBS3dHLGtCQUFMLENBQXdCVyxtQkFBeEIsQ0FDRWpOLFFBREYsRUFFRSxNQUFLME0sa0JBQUwsR0FBMEIsTUFBS3RELFdBQS9CLEdBQTZDLE1BQUtPLFdBRnBELEVBR0UsTUFBS0wsaUJBSFAsQ0FERjtBQU9ELE9BakJEOztBQW1CQSxXQUFLcUMscUJBQUwsQ0FBMkIzVyxFQUEzQixDQUE4QixjQUE5QixFQUE4QyxVQUFDOE4sS0FBRCxFQUFXO0FBQ3ZELGNBQUs2RyxXQUFMLEdBQW1CSCxXQUFXMUcsTUFBTXFJLE1BQU4sQ0FBYVYsS0FBeEIsQ0FBbkI7QUFDQSxjQUFLckIsV0FBTCxHQUFtQixNQUFLa0Qsa0JBQUwsQ0FBd0JjLG9CQUF4QixDQUNqQixNQUFLekQsV0FEWSxFQUVqQixNQUFLaUMsWUFBTCxDQUFrQnRXLEdBQWxCLEVBRmlCLEVBR2pCLE1BQUtnVSxpQkFIWSxDQUFuQjtBQUtBLFlBQU10SixXQUFXNU0sU0FBUyxNQUFLeVksYUFBTCxDQUFtQnZXLEdBQW5CLEVBQVQsRUFBbUMsRUFBbkMsQ0FBakI7O0FBRUEsY0FBS29XLHFCQUFMLENBQTJCcFcsR0FBM0IsQ0FBK0IsTUFBSzhULFdBQXBDO0FBQ0EsY0FBSzRDLGNBQUwsQ0FBb0JsRyxJQUFwQixDQUNFLE1BQUt3RyxrQkFBTCxDQUF3QlcsbUJBQXhCLENBQ0VqTixRQURGLEVBRUUsTUFBSzBNLGtCQUFMLEdBQTBCLE1BQUt0RCxXQUEvQixHQUE2QyxNQUFLTyxXQUZwRCxFQUdFLE1BQUtMLGlCQUhQLENBREY7QUFPRCxPQWpCRDs7QUFtQkEsV0FBS3BNLG1CQUFMLENBQXlCbEksRUFBekIsQ0FBNEIsT0FBNUIsRUFBcUMsVUFBQzhOLEtBQUQ7QUFBQSxlQUFXLE1BQUt1SyxpQkFBTCxDQUF1QnZLLEtBQXZCLENBQVg7QUFBQSxPQUFyQztBQUNBLFdBQUttSixhQUFMLENBQW1CalgsRUFBbkIsQ0FBc0IsUUFBdEIsRUFBZ0M7QUFBQSxlQUFNLE1BQUt1WCxvQkFBTCxDQUEwQmUsOEJBQTFCLEVBQU47QUFBQSxPQUFoQztBQUNEOzs7K0JBRVUzTixPLEVBQVM7QUFDbEIsV0FBSzRMLGNBQUwsQ0FBb0JqVyxHQUFwQixDQUF3QnFLLFFBQVFyRCxTQUFoQyxFQUEyQ2lJLE9BQTNDLENBQW1ELFFBQW5EOztBQUVBLFVBQU1vRixjQUFjbFYsT0FBT2lWLFFBQVAsQ0FBZ0IvSixRQUFRaUssWUFBeEIsRUFBc0MsS0FBS04saUJBQTNDLENBQXBCO0FBQ0EsV0FBS3FDLHFCQUFMLENBQTJCclcsR0FBM0IsQ0FBK0JxVSxXQUEvQjtBQUNBLFdBQUtBLFdBQUwsR0FBbUJILFdBQVdHLFdBQVgsQ0FBbkI7O0FBRUEsVUFBTVAsY0FBYzNVLE9BQU9pVixRQUFQLENBQWdCL0osUUFBUTRKLFlBQXhCLEVBQXNDLEtBQUtELGlCQUEzQyxDQUFwQjtBQUNBLFdBQUtvQyxxQkFBTCxDQUEyQnBXLEdBQTNCLENBQStCOFQsV0FBL0I7QUFDQSxXQUFLQSxXQUFMLEdBQW1CSSxXQUFXSixXQUFYLENBQW5COztBQUVBLFdBQUt3QyxZQUFMLENBQWtCdFcsR0FBbEIsQ0FBc0JxSyxRQUFROEosT0FBOUI7QUFDQSxXQUFLc0MsWUFBTCxDQUFrQmpHLElBQWxCLENBQXVCbkcsUUFBUTROLFFBQS9CO0FBQ0EsV0FBS25CLFNBQUwsR0FBaUJ6TSxRQUFRNk4sS0FBekI7QUFDQSxXQUFLMUIsYUFBTCxDQUFtQnpXLElBQW5CLENBQXdCLHFCQUF4QixFQUErQ3NLLFFBQVFtTixtQkFBdkQ7QUFDQSxXQUFLakIsYUFBTCxDQUFtQnZXLEdBQW5CLENBQXVCLENBQXZCO0FBQ0EsV0FBS3VXLGFBQUwsQ0FBbUJ0SCxPQUFuQixDQUEyQixRQUEzQjtBQUNBLFdBQUtrSixlQUFMLENBQXFCOU4sUUFBUStOLFlBQTdCO0FBQ0EsV0FBS25CLG9CQUFMLENBQTBCSSxZQUExQixDQUF1Q3JKLDJCQUFpQjdHLG9CQUF4RDtBQUNEOzs7b0NBRWVpUixZLEVBQWM7QUFBQTs7QUFDNUIsV0FBS2pDLGtCQUFMLENBQXdCbkYsS0FBeEI7O0FBRUEsNEJBQWNvSCxZQUFkLEVBQTRCcGMsT0FBNUIsQ0FBb0MsVUFBQ2dFLEdBQUQsRUFBUztBQUMzQyxlQUFLbVcsa0JBQUwsQ0FBd0J4VCxNQUF4QjtBQUNFO0FBREYsNEJBRW9CM0MsSUFBSXFZLHNCQUZ4QixtQ0FFNEVyWSxJQUFJc1ksZ0JBRmhGLG1DQUU4SHRZLElBQUl1WSxnQkFGbEksc0JBRW1LdlksSUFBSWtZLEtBRnZLLHlCQUVnTWxZLElBQUlpWSxRQUZwTSxVQUVpTmpZLElBQUl3WSxTQUZyTjtBQUlELE9BTEQ7O0FBT0EsV0FBS3RDLGlCQUFMLENBQXVCckUsV0FBdkIsQ0FBbUMsUUFBbkMsRUFBNkMsb0JBQVl1RyxZQUFaLEVBQTBCdmMsTUFBMUIsS0FBcUMsQ0FBbEY7O0FBRUEsVUFBSSxvQkFBWXVjLFlBQVosRUFBMEJ2YyxNQUExQixHQUFtQyxDQUF2QyxFQUEwQztBQUN4QyxhQUFLc2Esa0JBQUwsQ0FBd0JsSCxPQUF4QixDQUFnQyxRQUFoQztBQUNEO0FBQ0Y7OzsrQkFFVWMsTyxFQUFTO0FBQUE7O0FBQ2xCLFdBQUtuSSxtQkFBTCxDQUF5QjhQLElBQXpCLENBQThCLFVBQTlCLEVBQTBDLElBQTFDO0FBQ0EsV0FBS2YsYUFBTCxDQUFtQmUsSUFBbkIsQ0FBd0IsVUFBeEIsRUFBb0MsSUFBcEM7QUFDQSxXQUFLdkIsa0JBQUwsQ0FBd0J1QixJQUF4QixDQUE2QixVQUE3QixFQUF5QyxJQUF6Qzs7QUFFQSxVQUFNclgsU0FBUztBQUNib1ksb0JBQVksS0FBS3hDLGNBQUwsQ0FBb0JqVyxHQUFwQixFQURDO0FBRWIwWSx3QkFBZ0J0WixFQUFFLFdBQUYsRUFBZSxLQUFLK1csa0JBQXBCLEVBQXdDblcsR0FBeEMsRUFGSDtBQUdiMlksd0JBQWdCLEtBQUt2QyxxQkFBTCxDQUEyQnBXLEdBQTNCLEVBSEg7QUFJYjRZLHdCQUFnQixLQUFLdkMscUJBQUwsQ0FBMkJyVyxHQUEzQixFQUpIO0FBS2IwSyxrQkFBVSxLQUFLNkwsYUFBTCxDQUFtQnZXLEdBQW5CLEVBTEc7QUFNYjZZLG9CQUFZLEtBQUtsQyxhQUFMLENBQW1CM1csR0FBbkIsRUFOQztBQU9iOFksdUJBQWUsS0FBS2xDLGtCQUFMLENBQXdCYyxJQUF4QixDQUE2QixTQUE3QjtBQVBGLE9BQWY7O0FBVUF0WSxRQUFFNFEsSUFBRixDQUFPO0FBQ0wrSSxhQUFLLEtBQUtqSixNQUFMLENBQVl4TSxRQUFaLENBQXFCLDBCQUFyQixFQUFpRCxFQUFDeU0sZ0JBQUQsRUFBakQsQ0FEQTtBQUVMaUosZ0JBQVEsTUFGSDtBQUdMalosY0FBTU07QUFIRCxPQUFQLEVBSUc0UCxJQUpILENBS0UsVUFBQ0MsUUFBRCxFQUFjO0FBQ1pqUixtQ0FBYWdhLElBQWIsQ0FBa0JDLDRCQUFrQkMsbUJBQXBDLEVBQXlEO0FBQ3ZEcEosMEJBRHVEO0FBRXZEcUosMEJBQWdCL1ksT0FBT29ZLFVBRmdDO0FBR3ZEWSxrQkFBUW5KO0FBSCtDLFNBQXpEO0FBS0QsT0FYSCxFQVlFLFVBQUNBLFFBQUQsRUFBYztBQUNaLGVBQUt0SSxtQkFBTCxDQUF5QjhQLElBQXpCLENBQThCLFVBQTlCLEVBQTBDLEtBQTFDO0FBQ0EsZUFBS2YsYUFBTCxDQUFtQmUsSUFBbkIsQ0FBd0IsVUFBeEIsRUFBb0MsS0FBcEM7QUFDQSxlQUFLdkIsa0JBQUwsQ0FBd0J1QixJQUF4QixDQUE2QixVQUE3QixFQUF5QyxLQUF6Qzs7QUFFQSxZQUFJeEgsU0FBU3FCLFlBQVQsSUFBeUJyQixTQUFTcUIsWUFBVCxDQUFzQnRZLE9BQW5ELEVBQTREO0FBQzFEbUcsWUFBRW9TLEtBQUYsQ0FBUUMsS0FBUixDQUFjLEVBQUN4WSxTQUFTaVgsU0FBU3FCLFlBQVQsQ0FBc0J0WSxPQUFoQyxFQUFkO0FBQ0Q7QUFDRixPQXBCSDtBQXNCRDs7O3NDQUVpQnVVLEssRUFBTztBQUFBOztBQUN2QixVQUFNMEQsWUFBWXBULFNBQVMsS0FBSzZZLGFBQUwsQ0FBbUIzVyxHQUFuQixFQUFULEVBQW1DLEVBQW5DLENBQWxCO0FBQ0EsVUFBTStQLFVBQVUzUSxFQUFFb08sTUFBTTNOLGFBQVIsRUFBdUJFLElBQXZCLENBQTRCLFNBQTVCLENBQWhCOztBQUVBO0FBQ0EsVUFBSW1SLGNBQWMsQ0FBbEIsRUFBcUI7QUFDbkIsWUFBTXhRLFFBQVEsSUFBSU4sZUFBSixDQUNaO0FBQ0VJLGNBQUksMkJBRE47QUFFRWUsd0JBQWMsS0FBS29WLGFBQUwsQ0FBbUI1VyxJQUFuQixDQUF3QixhQUF4QixDQUZoQjtBQUdFeUIsMEJBQWdCLEtBQUttVixhQUFMLENBQW1CNVcsSUFBbkIsQ0FBd0IsWUFBeEIsQ0FIbEI7QUFJRTJCLDhCQUFvQixLQUFLaVYsYUFBTCxDQUFtQjVXLElBQW5CLENBQXdCLGFBQXhCLENBSnRCO0FBS0UwQiw0QkFBa0IsS0FBS2tWLGFBQUwsQ0FBbUI1VyxJQUFuQixDQUF3QixjQUF4QjtBQUxwQixTQURZLEVBUVosWUFBTTtBQUNKLGlCQUFLdVosZUFBTCxDQUFxQnZKLE9BQXJCLEVBQThCbUIsU0FBOUI7QUFDRCxTQVZXLENBQWQ7QUFZQXhRLGNBQU1JLElBQU47QUFDRCxPQWRELE1BY087QUFDTDtBQUNBLGFBQUt5WSxVQUFMLENBQWdCeEosT0FBaEI7QUFDRDtBQUNGOzs7b0NBRWVBLE8sRUFBU21CLFMsRUFBVztBQUFBOztBQUNsQyxVQUFNc0ksbUJBQW1CcGEsRUFBRSxXQUFGLEVBQWUsS0FBSytXLGtCQUFwQixFQUF3Q25XLEdBQXhDLEVBQXpCO0FBQ0EsVUFBTThTLGdCQUFnQixPQUFPMEcsZ0JBQVAsS0FBNEIsV0FBNUIsR0FBMEMsQ0FBMUMsR0FBOENBLGdCQUFwRTtBQUNBLFVBQU1DLG9CQUFvQixLQUFLdEMsb0JBQUwsQ0FBMEJ1Qyw0QkFBMUIsQ0FDeEIsS0FBS3RELHFCQUFMLENBQTJCcFcsR0FBM0IsRUFEd0IsRUFFeEIsS0FBS2lXLGNBQUwsQ0FBb0JqVyxHQUFwQixFQUZ3QixFQUd4QjhTLGFBSHdCLEVBSXhCNUIsU0FKd0IsQ0FBMUI7O0FBT0EsVUFBSXVJLHNCQUFzQixTQUExQixFQUFxQztBQUNuQyxZQUFNRSxpQkFBaUIsSUFBSXZaLGVBQUosQ0FDckI7QUFDRUksY0FBSSx5QkFETjtBQUVFZSx3QkFBYyxLQUFLb1YsYUFBTCxDQUFtQjVXLElBQW5CLENBQXdCLHdCQUF4QixDQUZoQjtBQUdFeUIsMEJBQWdCLEtBQUttVixhQUFMLENBQW1CNVcsSUFBbkIsQ0FBd0IsdUJBQXhCLENBSGxCO0FBSUUyQiw4QkFBb0IsS0FBS2lWLGFBQUwsQ0FBbUI1VyxJQUFuQixDQUF3Qix3QkFBeEIsQ0FKdEI7QUFLRTBCLDRCQUFrQixLQUFLa1YsYUFBTCxDQUFtQjVXLElBQW5CLENBQXdCLHlCQUF4QjtBQUxwQixTQURxQixFQVFyQixZQUFNO0FBQ0osaUJBQUt3WixVQUFMLENBQWdCeEosT0FBaEI7QUFDRCxTQVZvQixDQUF2QjtBQVlBNEosdUJBQWU3WSxJQUFmO0FBQ0QsT0FkRCxNQWNPO0FBQ0wsYUFBS3lZLFVBQUwsQ0FBZ0J4SixPQUFoQjtBQUNEO0FBQ0Y7Ozs7O2tCQXJRa0JpRyxlOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ1hyQjs7OztBQUNBOzs7O0FBQ0E7Ozs7Y0FFWTdXLE07SUFBTEMsQyxXQUFBQSxDOztBQUVQOzs7QUEvQkE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7SUFrQ3FCd2Esa0I7QUFDbkIsZ0NBQWM7QUFBQTs7QUFDWixTQUFLOUosTUFBTCxHQUFjLElBQUlsTixnQkFBSixFQUFkO0FBQ0EsU0FBS2lYLGlCQUFMLEdBQXlCemEsRUFBRTRPLDJCQUFpQjNDLGFBQWpCLENBQStCQyxJQUFqQyxDQUF6QjtBQUNBLFNBQUt5RSxPQUFMLEdBQWUsS0FBSzhKLGlCQUFMLENBQXVCOVosSUFBdkIsQ0FBNEIsU0FBNUIsQ0FBZjtBQUNBLFNBQUsrWixjQUFMLEdBQXNCaGMsU0FBUyxLQUFLK2IsaUJBQUwsQ0FBdUI5WixJQUF2QixDQUE0QixhQUE1QixDQUFULEVBQXFELEVBQXJELE1BQTZELENBQW5GO0FBQ0EsU0FBS2dhLGFBQUwsR0FBcUJqYyxTQUFTLEtBQUsrYixpQkFBTCxDQUF1QjlaLElBQXZCLENBQTRCLGVBQTVCLENBQVQsRUFBdUQsRUFBdkQsTUFBK0QsQ0FBcEY7QUFDQSxTQUFLaWEsZUFBTCxHQUF1QjlGLFdBQVcsS0FBSzJGLGlCQUFMLENBQXVCOVosSUFBdkIsQ0FBNEIsaUJBQTVCLENBQVgsQ0FBdkI7QUFDQSxTQUFLa2EsaUJBQUwsR0FBeUI1Z0Isc0JBQWdCNmdCLEtBQWhCLENBQ3ZCLEtBQUtMLGlCQUFMLENBQXVCOVosSUFBdkIsQ0FBNEIsb0JBQTVCLENBRHVCLENBQXpCO0FBR0EsU0FBS29hLGVBQUwsR0FBdUIsSUFBdkI7QUFDQSxTQUFLQyxlQUFMO0FBQ0Q7Ozs7d0NBRW1CO0FBQ2xCO0FBQ0EsV0FBS0Msa0JBQUw7QUFDQWpiLFFBQUU0TywyQkFBaUIzQyxhQUFqQixDQUErQnVCLE1BQS9CLENBQXNDbEIsYUFBeEMsRUFBdUQ1SyxJQUF2RDtBQUNBLFdBQUtxWixlQUFMLEdBQXVCLElBQXZCO0FBQ0EsV0FBS0csUUFBTCxDQUNFbGIsRUFBRTRPLDJCQUFpQjNDLGFBQWpCLENBQStCRSxPQUEvQixDQUF1Q0UsSUFBekMsRUFBK0MxTCxJQUEvQyxDQUFvRCxvQkFBcEQsQ0FERixFQUVFLEtBQUsrUCxNQUFMLENBQVl4TSxRQUFaLENBQXFCLDZCQUFyQixFQUFvRDtBQUNsRHlNLGlCQUFTLEtBQUtBO0FBRG9DLE9BQXBELENBRkYsRUFLRSxnQkFMRjtBQU9EOzs7eUNBRW9CO0FBQ25CO0FBQ0EsV0FBS3NLLGtCQUFMO0FBQ0FqYixRQUFFNE8sMkJBQWlCM0MsYUFBakIsQ0FBK0J1QixNQUEvQixDQUFzQ2pCLGNBQXhDLEVBQXdEN0ssSUFBeEQ7QUFDQSxXQUFLcVosZUFBTCxHQUF1QixLQUF2QjtBQUNBLFdBQUtHLFFBQUwsQ0FDRWxiLEVBQUU0TywyQkFBaUIzQyxhQUFqQixDQUErQkUsT0FBL0IsQ0FBdUNFLElBQXpDLEVBQStDMUwsSUFBL0MsQ0FBb0QscUJBQXBELENBREYsRUFFRSxLQUFLK1AsTUFBTCxDQUFZeE0sUUFBWixDQUFxQiw4QkFBckIsRUFBcUQ7QUFDbkR5TSxpQkFBUyxLQUFLQTtBQURxQyxPQUFyRCxDQUZGLEVBS0UsaUJBTEY7QUFPRDs7O3dDQUVtQjtBQUNsQjtBQUNBLFdBQUtzSyxrQkFBTDtBQUNBamIsUUFBRTRPLDJCQUFpQjNDLGFBQWpCLENBQStCdUIsTUFBL0IsQ0FBc0NoQixhQUF4QyxFQUF1RDlLLElBQXZEO0FBQ0EsV0FBS3FaLGVBQUwsR0FBdUIsS0FBdkI7QUFDQSxXQUFLRyxRQUFMLENBQ0VsYixFQUFFNE8sMkJBQWlCM0MsYUFBakIsQ0FBK0JFLE9BQS9CLENBQXVDRSxJQUF6QyxFQUErQzFMLElBQS9DLENBQW9ELG9CQUFwRCxDQURGLEVBRUUsS0FBSytQLE1BQUwsQ0FBWXhNLFFBQVosQ0FBcUIsNkJBQXJCLEVBQW9EO0FBQ2xEeU0saUJBQVMsS0FBS0E7QUFEb0MsT0FBcEQsQ0FGRixFQUtFLGdCQUxGO0FBT0Q7OztpQ0FFWTtBQUNYLFdBQUtzSyxrQkFBTDtBQUNBamIsUUFBRTRPLDJCQUFpQjNDLGFBQWpCLENBQStCbkIsS0FBL0IsQ0FBcUNnQyxPQUF2QyxFQUFnRHBMLElBQWhEO0FBQ0Q7Ozt5Q0FFb0I7QUFDbkIxQixRQUFFNE8sMkJBQWlCM0MsYUFBakIsQ0FBK0J1QixNQUEvQixDQUFzQ2pCLGNBQXhDLEVBQXdEcUosSUFBeEQ7QUFDQTVWLFFBQUU0TywyQkFBaUIzQyxhQUFqQixDQUErQnVCLE1BQS9CLENBQXNDbEIsYUFBeEMsRUFBdURzSixJQUF2RDtBQUNBNVYsUUFBRTRPLDJCQUFpQjNDLGFBQWpCLENBQStCdUIsTUFBL0IsQ0FBc0NoQixhQUF4QyxFQUF1RG9KLElBQXZEO0FBQ0E1VixRQUFFNE8sMkJBQWlCM0MsYUFBakIsQ0FBK0JuQixLQUEvQixDQUFxQ2dDLE9BQXZDLEVBQWdEOEksSUFBaEQ7QUFDRDs7OzZCQUVRdUYsVSxFQUFZQyxVLEVBQVlDLFMsRUFBVztBQUMxQyxXQUFLQyxtQkFBTDs7QUFFQSxXQUFLYixpQkFBTCxDQUF1Qm5DLElBQXZCLENBQTRCLFFBQTVCLEVBQXNDOEMsVUFBdEM7QUFDQSxXQUFLWCxpQkFBTCxDQUNHaE0sV0FESCxDQUNlLDhEQURmLEVBRUdDLFFBRkgsQ0FFWTJNLFNBRlo7QUFHQXJiLFFBQUU0TywyQkFBaUIzQyxhQUFqQixDQUErQkUsT0FBL0IsQ0FBdUNFLElBQXpDLEVBQStDK0UsSUFBL0MsQ0FBb0QrSixVQUFwRDtBQUNBbmIsUUFBRTRPLDJCQUFpQjNDLGFBQWpCLENBQStCbkIsS0FBL0IsQ0FBcUNoSSxNQUF2QyxFQUErQ3NPLElBQS9DLENBQW9EK0osVUFBcEQ7QUFDQW5iLFFBQUU0TywyQkFBaUIzQyxhQUFqQixDQUErQmMsVUFBL0IsQ0FBMENDLE9BQTVDLEVBQXFEc0wsSUFBckQsQ0FBMEQsU0FBMUQsRUFBcUUsS0FBS29DLGNBQTFFO0FBQ0ExYSxRQUFFNE8sMkJBQWlCM0MsYUFBakIsQ0FBK0JjLFVBQS9CLENBQTBDRSxVQUE1QyxFQUF3RHFMLElBQXhELENBQTZELFNBQTdELEVBQXdFLElBQXhFO0FBQ0F0WSxRQUFFNE8sMkJBQWlCM0MsYUFBakIsQ0FBK0JjLFVBQS9CLENBQTBDRyxPQUE1QyxFQUFxRG9MLElBQXJELENBQTBELFNBQTFELEVBQXFFLEtBQXJFO0FBQ0Q7OztzQ0FFaUI7QUFBQTs7QUFDaEJ0WSxRQUFFSyxRQUFGLEVBQVlDLEVBQVosQ0FBZSxRQUFmLEVBQXlCc08sMkJBQWlCM0MsYUFBakIsQ0FBK0JTLE1BQS9CLENBQXNDcEIsUUFBL0QsRUFBeUUsVUFBQzhDLEtBQUQsRUFBVztBQUNsRixZQUFNbU4sd0JBQXdCdmIsRUFBRW9PLE1BQU1xSSxNQUFSLENBQTlCO0FBQ0EsWUFBTStFLGNBQWNELHNCQUFzQkUsT0FBdEIsQ0FBOEI3TSwyQkFBaUIzQyxhQUFqQixDQUErQm5CLEtBQS9CLENBQXFDK0IsSUFBbkUsQ0FBcEI7QUFDQSxZQUFNNk8saUJBQWlCRixZQUFZMWEsSUFBWixDQUFpQjhOLDJCQUFpQjNDLGFBQWpCLENBQStCUyxNQUEvQixDQUFzQ0MsTUFBdkQsQ0FBdkI7QUFDQSxZQUFNZ1Asa0JBQWtCamQsU0FBUzZjLHNCQUFzQjNhLEdBQXRCLEVBQVQsRUFBc0MsRUFBdEMsQ0FBeEI7O0FBRUEsWUFBSSthLG1CQUFtQixDQUF2QixFQUEwQjtBQUN4QkQseUJBQWU5YSxHQUFmLENBQW1CLENBQW5CO0FBQ0EsZ0JBQUswYSxtQkFBTDs7QUFFQTtBQUNEO0FBQ0QsWUFBTU0saUJBQWlCLE1BQUtqQixhQUFMLEdBQXFCLHFCQUFyQixHQUE2QyxxQkFBcEU7QUFDQSxZQUFNa0IsbUJBQW1CL0csV0FBV3lHLHNCQUFzQjVhLElBQXRCLENBQTJCaWIsY0FBM0IsQ0FBWCxDQUF6QjtBQUNBLFlBQU1FLG1CQUFtQmhILFdBQVd5RyxzQkFBc0I1YSxJQUF0QixDQUEyQixrQkFBM0IsQ0FBWCxDQUF6QjtBQUNBLFlBQU1vYixnQkFBZ0JGLG1CQUFtQkYsZUFBbkIsR0FBcUNHLGdCQUFyQyxHQUNsQkQsbUJBQW1CRixlQURELEdBRWxCRyxnQkFGSjtBQUdBLFlBQU1FLGNBQWNsSCxXQUFXNEcsZUFBZTlhLEdBQWYsRUFBWCxDQUFwQjs7QUFFQSxZQUFJLE1BQUttYSxlQUFULEVBQTBCO0FBQ3hCLGdCQUFLa0IsaUJBQUwsQ0FBdUJWLHFCQUF2QjtBQUNEOztBQUVELFlBQUlHLGVBQWU5YSxHQUFmLE9BQXlCLEVBQXpCLElBQStCb2IsZ0JBQWdCLENBQS9DLElBQW9EQSxjQUFjRCxhQUF0RSxFQUFxRjtBQUNuRkwseUJBQWU5YSxHQUFmLENBQW1CbWIsYUFBbkI7QUFDQSxnQkFBS1QsbUJBQUw7QUFDRDtBQUNGLE9BNUJEOztBQThCQXRiLFFBQUVLLFFBQUYsRUFBWUMsRUFBWixDQUFlLFFBQWYsRUFBeUJzTywyQkFBaUIzQyxhQUFqQixDQUErQlMsTUFBL0IsQ0FBc0NDLE1BQS9ELEVBQXVFLFlBQU07QUFDM0UsY0FBSzJPLG1CQUFMO0FBQ0QsT0FGRDs7QUFJQXRiLFFBQUVLLFFBQUYsRUFBWUMsRUFBWixDQUFlLFFBQWYsRUFBeUJzTywyQkFBaUIzQyxhQUFqQixDQUErQlMsTUFBL0IsQ0FBc0NFLFFBQS9ELEVBQXlFLFVBQUN3QixLQUFELEVBQVc7QUFDbEYsWUFBTThOLG1CQUFtQmxjLEVBQUVvTyxNQUFNcUksTUFBUixDQUF6QjtBQUNBLFlBQU0rRSxjQUFjVSxpQkFBaUJULE9BQWpCLENBQXlCN00sMkJBQWlCM0MsYUFBakIsQ0FBK0JuQixLQUEvQixDQUFxQytCLElBQTlELENBQXBCO0FBQ0EsWUFBTXNQLHVCQUF1QlgsWUFBWTFhLElBQVosQ0FBaUI4TiwyQkFBaUIzQyxhQUFqQixDQUErQlMsTUFBL0IsQ0FBc0NwQixRQUF2RCxDQUE3QjtBQUNBLFlBQU04USxxQkFBcUIxZCxTQUFTeWQscUJBQXFCeGIsSUFBckIsQ0FBMEIsb0JBQTFCLENBQVQsRUFBMEQsRUFBMUQsQ0FBM0I7QUFDQSxZQUFNZ2Isa0JBQWtCamQsU0FBU3lkLHFCQUFxQnZiLEdBQXJCLEVBQVQsRUFBcUMsRUFBckMsQ0FBeEI7O0FBRUEsWUFBSSxDQUFDc2IsaUJBQWlCRyxFQUFqQixDQUFvQixVQUFwQixDQUFMLEVBQXNDO0FBQ3BDRiwrQkFBcUJ2YixHQUFyQixDQUF5QixDQUF6QjtBQUNELFNBRkQsTUFFTyxJQUFJLHFCQUFhK2EsZUFBYixLQUFpQ0Esb0JBQW9CLENBQXpELEVBQTREO0FBQ2pFUSwrQkFBcUJ2YixHQUFyQixDQUF5QndiLGtCQUF6QjtBQUNEO0FBQ0QsY0FBS2QsbUJBQUw7QUFDRCxPQWJEO0FBY0Q7OztzQ0FFaUJDLHFCLEVBQXVCO0FBQ3ZDLFVBQU1DLGNBQWNELHNCQUFzQkUsT0FBdEIsQ0FBOEI3TSwyQkFBaUIzQyxhQUFqQixDQUErQm5CLEtBQS9CLENBQXFDK0IsSUFBbkUsQ0FBcEI7QUFDQSxVQUFNNk8saUJBQWlCRixZQUFZMWEsSUFBWixDQUFpQjhOLDJCQUFpQjNDLGFBQWpCLENBQStCUyxNQUEvQixDQUFzQ0MsTUFBdkQsQ0FBdkI7QUFDQSxVQUFNZ1Asa0JBQWtCamQsU0FBUzZjLHNCQUFzQjNhLEdBQXRCLEVBQVQsRUFBc0MsRUFBdEMsQ0FBeEI7O0FBRUEsVUFBSSthLG1CQUFtQixDQUF2QixFQUEwQjtBQUN4QkQsdUJBQWU5YSxHQUFmLENBQW1CLENBQW5COztBQUVBO0FBQ0Q7O0FBRUQsVUFBTWdiLGlCQUFpQixLQUFLakIsYUFBTCxHQUFxQixxQkFBckIsR0FBNkMscUJBQXBFO0FBQ0EsVUFBTWtCLG1CQUFtQi9HLFdBQVd5RyxzQkFBc0I1YSxJQUF0QixDQUEyQmliLGNBQTNCLENBQVgsQ0FBekI7QUFDQSxVQUFNRSxtQkFBbUJoSCxXQUFXeUcsc0JBQXNCNWEsSUFBdEIsQ0FBMkIsa0JBQTNCLENBQVgsQ0FBekI7QUFDQSxVQUFNb2IsZ0JBQWdCRixtQkFBbUJGLGVBQW5CLEdBQXFDRyxnQkFBckMsR0FDbEJELG1CQUFtQkYsZUFERCxHQUVsQkcsZ0JBRko7QUFHQSxVQUFNRSxjQUFjbEgsV0FBVzRHLGVBQWU5YSxHQUFmLEVBQVgsQ0FBcEI7O0FBRUEsVUFBSThhLGVBQWU5YSxHQUFmLE9BQXlCLEVBQXpCLElBQStCb2IsZ0JBQWdCLENBQS9DLElBQW9EQSxjQUFjRCxhQUF0RSxFQUFxRjtBQUNuRkwsdUJBQWU5YSxHQUFmLENBQW1CbWIsYUFBbkI7QUFDRDtBQUNGOzs7c0NBRWlCO0FBQUE7O0FBQ2hCLFVBQUlPLGNBQWMsQ0FBbEI7O0FBRUEsVUFBSSxLQUFLdkIsZUFBVCxFQUEwQjtBQUN4Qi9hLFVBQUU0TywyQkFBaUIzQyxhQUFqQixDQUErQlMsTUFBL0IsQ0FBc0NDLE1BQXhDLEVBQWdENFAsSUFBaEQsQ0FBcUQsVUFBQ0MsS0FBRCxFQUFRN1AsTUFBUixFQUFtQjtBQUN0RSxjQUFNOFAsYUFBYTNILFdBQVduSSxPQUFPb0osS0FBbEIsQ0FBbkI7QUFDQXVHLHlCQUFlLENBQUMscUJBQWFHLFVBQWIsQ0FBRCxHQUE0QkEsVUFBNUIsR0FBeUMsQ0FBeEQ7QUFDRCxTQUhEO0FBSUQsT0FMRCxNQUtPO0FBQ0x6YyxVQUFFNE8sMkJBQWlCM0MsYUFBakIsQ0FBK0JTLE1BQS9CLENBQXNDcEIsUUFBeEMsRUFBa0RpUixJQUFsRCxDQUF1RCxVQUFDQyxLQUFELEVBQVFsUixRQUFSLEVBQXFCO0FBQzFFLGNBQU1vUixpQkFBaUIxYyxFQUFFc0wsUUFBRixDQUF2QjtBQUNBLGNBQU1zUSxpQkFBaUIsT0FBS2pCLGFBQUwsR0FBcUIscUJBQXJCLEdBQTZDLHFCQUFwRTtBQUNBLGNBQU1rQixtQkFBbUIvRyxXQUFXNEgsZUFBZS9iLElBQWYsQ0FBb0JpYixjQUFwQixDQUFYLENBQXpCO0FBQ0EsY0FBTUQsa0JBQWtCamQsU0FBU2dlLGVBQWU5YixHQUFmLEVBQVQsRUFBK0IsRUFBL0IsQ0FBeEI7QUFDQTBiLHlCQUFlWCxrQkFBa0JFLGdCQUFqQztBQUNELFNBTkQ7QUFPRDs7QUFFRCxhQUFPUyxXQUFQO0FBQ0Q7OzswQ0FFcUI7QUFDcEIsVUFBTUssZUFBZSxLQUFLQyxlQUFMLEVBQXJCOztBQUVBLFdBQUtDLDRCQUFMLENBQ0U3YyxFQUFFNE8sMkJBQWlCM0MsYUFBakIsQ0FBK0JrQixNQUEvQixDQUFzQ0MsaUJBQXRDLENBQXdEQyxhQUExRCxDQURGLEVBRUVzUCxZQUZGO0FBSUEsVUFBTUcsd0JBQXdCSCxlQUFlLEtBQUsvQixlQUFsRDtBQUNBLFdBQUtpQyw0QkFBTCxDQUNFN2MsRUFBRTRPLDJCQUFpQjNDLGFBQWpCLENBQStCa0IsTUFBL0IsQ0FBc0NDLGlCQUF0QyxDQUF3REUsNEJBQTFELENBREYsRUFFRXdQLHFCQUZGOztBQUtBO0FBQ0EsVUFBSUEsd0JBQXdCLENBQTVCLEVBQStCO0FBQzdCOWMsVUFBRTRPLDJCQUFpQjNDLGFBQWpCLENBQStCa0IsTUFBL0IsQ0FBc0NDLGlCQUF0QyxDQUF3REUsNEJBQTFELEVBQ0dnTCxJQURILENBQ1EsU0FEUixFQUNtQixLQURuQixFQUVHQSxJQUZILENBRVEsVUFGUixFQUVvQixJQUZwQjtBQUdBdFksVUFBRTRPLDJCQUFpQjNDLGFBQWpCLENBQStCa0IsTUFBL0IsQ0FBc0NDLGlCQUF0QyxDQUF3REMsYUFBMUQsRUFBeUVpTCxJQUF6RSxDQUNFLFNBREYsRUFFRSxJQUZGO0FBSUF0WSxVQUFFNE8sMkJBQWlCM0MsYUFBakIsQ0FBK0JrQixNQUEvQixDQUFzQ0MsaUJBQXRDLENBQXdERyxvQkFBMUQsRUFBZ0Y3TCxJQUFoRjtBQUNELE9BVEQsTUFTTztBQUNMMUIsVUFBRTRPLDJCQUFpQjNDLGFBQWpCLENBQStCa0IsTUFBL0IsQ0FBc0NDLGlCQUF0QyxDQUF3REUsNEJBQTFELEVBQXdGZ0wsSUFBeEYsQ0FDRSxVQURGLEVBRUUsS0FGRjtBQUlBdFksVUFBRTRPLDJCQUFpQjNDLGFBQWpCLENBQStCa0IsTUFBL0IsQ0FBc0NDLGlCQUF0QyxDQUF3REcsb0JBQTFELEVBQWdGcUksSUFBaEY7QUFDRDtBQUNGOzs7aURBRTRCcFYsTSxFQUFRbWMsWSxFQUFjO0FBQ2pELFVBQU1JLGVBQWV2YyxPQUFPRyxJQUFQLENBQVksY0FBWixDQUFyQjtBQUNBLFVBQU1xYyxTQUFTeGMsT0FBT2liLE9BQVAsQ0FBZSxPQUFmLENBQWY7QUFDQSxVQUFNd0Isa0JBQWtCLEtBQUtwQyxpQkFBTCxDQUF1QnFDLE1BQXZCLENBQThCUCxZQUE5QixDQUF4Qjs7QUFFQTtBQUNBSyxhQUFPbk8sR0FBUCxDQUFXLENBQVgsRUFBY3NPLFNBQWQsQ0FBd0JDLFNBQXhCLGNBQ0VMLFlBREYsU0FDa0JFLGVBRGxCO0FBRUQ7Ozs0Q0FFdUI7QUFDdEIsVUFBTUkscUJBQXFCLEtBQUszTSxNQUFMLENBQVl4TSxRQUFaLENBQXFCLDJCQUFyQixFQUFrRCxFQUFDeU0sU0FBUyxLQUFLQSxPQUFmLEVBQWxELENBQTNCO0FBQ0EsV0FBS3VLLFFBQUwsQ0FDRWxiLEVBQUU0TywyQkFBaUIzQyxhQUFqQixDQUErQkUsT0FBL0IsQ0FBdUNFLElBQXpDLEVBQStDMUwsSUFBL0MsQ0FBb0QsYUFBcEQsQ0FERixFQUVFMGMsa0JBRkYsRUFHRSxnQkFIRjtBQUtBLFdBQUtwQyxrQkFBTDtBQUNBamIsUUFBRTRPLDJCQUFpQjNDLGFBQWpCLENBQStCdUIsTUFBL0IsQ0FBc0NmLGNBQXhDLEVBQXdEL0ssSUFBeEQ7QUFDRDs7Ozs7a0JBdE9rQjhZLGtCOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDVHJCOzs7O0FBQ0E7Ozs7QUFDQTs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7Ozs7O2NBRVl6YSxNO0lBQUxDLEMsV0FBQUEsQyxFQWpDUDs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztJQW1DcUJzZCxnQjtBQUNuQiw0QkFBWXJLLGFBQVosRUFBMkI7QUFBQTs7QUFDekIsU0FBS3ZDLE1BQUwsR0FBYyxJQUFJbE4sZ0JBQUosRUFBZDtBQUNBLFNBQUt5UCxhQUFMLEdBQXFCQSxhQUFyQjtBQUNBLFNBQUtrQixVQUFMLEdBQWtCblUscUJBQW1CLEtBQUtpVCxhQUF4QixDQUFsQjtBQUNBLFNBQUtoSSxPQUFMLEdBQWUsRUFBZjtBQUNBLFNBQUsySixpQkFBTCxHQUF5QjVVLEVBQUU0TywyQkFBaUIzSCxhQUFuQixFQUFrQ3RHLElBQWxDLENBQXVDLG1CQUF2QyxDQUF6QjtBQUNBLFNBQUtpWCxrQkFBTCxHQUEwQixJQUFJbkQscUJBQUosRUFBMUI7QUFDQSxTQUFLOUssa0JBQUwsR0FBMEIzSixFQUFFNE8sMkJBQWlCakYsa0JBQW5CLENBQTFCO0FBQ0EsU0FBS3dOLGFBQUwsR0FBcUJuWCxFQUFFNE8sMkJBQWlCcEUsd0JBQW5CLENBQXJCO0FBQ0EsU0FBS3VOLG9CQUFMLEdBQTRCLElBQUl6Riw4QkFBSixFQUE1QjtBQUNEOzs7O29DQUVlO0FBQUE7O0FBQ2QsV0FBSzZFLGFBQUwsQ0FBbUI3VyxFQUFuQixDQUFzQixjQUF0QixFQUFzQyxVQUFDOE4sS0FBRCxFQUFXO0FBQy9DLFlBQU04SixjQUFjcEUsT0FBTzFGLE1BQU1xSSxNQUFOLENBQWFWLEtBQXBCLENBQXBCO0FBQ0EsWUFBTXhLLG9CQUFvQjdNLFNBQVNzQixFQUFFb08sTUFBTTNOLGFBQVIsRUFBdUJFLElBQXZCLENBQTRCLG1CQUE1QixDQUFULEVBQTJELEVBQTNELENBQTFCO0FBQ0EsWUFBTTRjLG1CQUFtQjdlLFNBQVMsTUFBS3lZLGFBQUwsQ0FBbUJ4VyxJQUFuQixDQUF3QixrQkFBeEIsQ0FBVCxFQUFzRCxFQUF0RCxDQUF6QjtBQUNBLFlBQU13WCxxQkFBcUI1TSxxQkFBcUIyTSxjQUFjcUYsZ0JBQW5DLENBQTNCO0FBQ0EsWUFBTW5GLHNCQUFzQixNQUFLaEIsYUFBTCxDQUFtQnpXLElBQW5CLENBQXdCLHFCQUF4QixDQUE1QjtBQUNBLGNBQUsySyxRQUFMLEdBQWdCNE0sV0FBaEI7QUFDQSxjQUFLZCxhQUFMLENBQW1CclcsSUFBbkIsQ0FBd0JvWCxrQkFBeEI7QUFDQSxjQUFLZixhQUFMLENBQW1CM0UsV0FBbkIsQ0FBK0IsOEJBQS9CLEVBQStEMEYscUJBQXFCLENBQXBGO0FBQ0EsY0FBS3FGLFdBQUw7QUFDQSxZQUFNQyx1QkFBdUJ2RixlQUFlLENBQWYsSUFBcUJDLHFCQUFxQixDQUFyQixJQUEwQixDQUFDQyxtQkFBN0U7QUFDQSxjQUFLek8sa0JBQUwsQ0FBd0IyTyxJQUF4QixDQUE2QixVQUE3QixFQUF5Q21GLG9CQUF6QztBQUNELE9BWkQ7O0FBY0EsV0FBS2xULHdCQUFMLENBQThCakssRUFBOUIsQ0FBaUMsUUFBakMsRUFBMkMsWUFBTTtBQUMvQyxjQUFLcUosa0JBQUwsQ0FBd0IyTyxJQUF4QixDQUE2QixVQUE3QixFQUF5QyxLQUF6QztBQUNELE9BRkQ7O0FBSUEsV0FBS3RCLHFCQUFMLENBQTJCMVcsRUFBM0IsQ0FBOEIsY0FBOUIsRUFBOEMsVUFBQzhOLEtBQUQsRUFBVztBQUN2RCxjQUFLc0csV0FBTCxHQUFtQkksV0FBVzFHLE1BQU1xSSxNQUFOLENBQWFWLEtBQXhCLENBQW5CO0FBQ0EsY0FBS2QsV0FBTCxHQUFtQixNQUFLMkMsa0JBQUwsQ0FBd0JhLG9CQUF4QixDQUNqQixNQUFLL0QsV0FEWSxFQUVqQixNQUFLSyxPQUZZLEVBR2pCLE1BQUtILGlCQUhZLENBQW5CO0FBS0EsY0FBS3FDLHFCQUFMLENBQTJCclcsR0FBM0IsQ0FBK0IsTUFBS3FVLFdBQXBDO0FBQ0EsY0FBS3VJLFdBQUw7QUFDRCxPQVREOztBQVdBLFdBQUt2RyxxQkFBTCxDQUEyQjNXLEVBQTNCLENBQThCLGNBQTlCLEVBQThDLFVBQUM4TixLQUFELEVBQVc7QUFDdkQsY0FBSzZHLFdBQUwsR0FBbUJILFdBQVcxRyxNQUFNcUksTUFBTixDQUFhVixLQUF4QixDQUFuQjtBQUNBLGNBQUtyQixXQUFMLEdBQW1CLE1BQUtrRCxrQkFBTCxDQUF3QmMsb0JBQXhCLENBQ2pCLE1BQUt6RCxXQURZLEVBRWpCLE1BQUtGLE9BRlksRUFHakIsTUFBS0gsaUJBSFksQ0FBbkI7QUFLQSxjQUFLb0MscUJBQUwsQ0FBMkJwVyxHQUEzQixDQUErQixNQUFLOFQsV0FBcEM7QUFDQSxjQUFLOEksV0FBTDtBQUNELE9BVEQ7O0FBV0EsV0FBSzdULGtCQUFMLENBQXdCckosRUFBeEIsQ0FBMkIsT0FBM0IsRUFBb0MsVUFBQzhOLEtBQUQsRUFBVztBQUM3QyxZQUFNRSxPQUFPdE8sRUFBRW9PLE1BQU0zTixhQUFSLENBQWI7QUFDQSxZQUFNaWQsWUFBWTNkLE9BQU82UCxPQUFQLENBQWV0QixLQUFLM04sSUFBTCxDQUFVLGVBQVYsQ0FBZixDQUFsQjs7QUFFQSxZQUFJLENBQUMrYyxTQUFMLEVBQWdCO0FBQ2Q7QUFDRDs7QUFFRHBQLGFBQUtnSyxJQUFMLENBQVUsVUFBVixFQUFzQixJQUF0QjtBQUNBLGNBQUtxRixzQ0FBTCxDQUE0Q3ZQLEtBQTVDO0FBQ0QsT0FWRDs7QUFZQSxXQUFLeEUsb0JBQUwsQ0FBMEJ0SixFQUExQixDQUE2QixPQUE3QixFQUFzQyxZQUFNO0FBQzFDVCxtQ0FBYWdhLElBQWIsQ0FBa0JDLDRCQUFrQjhELHNCQUFwQyxFQUE0RDtBQUMxRDNLLHlCQUFlLE1BQUtBO0FBRHNDLFNBQTVEO0FBR0QsT0FKRDtBQUtEOzs7a0NBRWE7QUFDWixVQUFNNEssZUFBZSxLQUFLakcsa0JBQUwsQ0FBd0JXLG1CQUF4QixDQUNuQixLQUFLak4sUUFEYyxFQUVuQixLQUFLME0sa0JBQUwsR0FBMEIsS0FBS3RELFdBQS9CLEdBQTZDLEtBQUtPLFdBRi9CLEVBR25CLEtBQUtMLGlCQUhjLENBQXJCO0FBS0EsV0FBS2tKLGNBQUwsQ0FBb0IxTSxJQUFwQixDQUF5QnlNLFlBQXpCO0FBQ0EsV0FBS2xVLGtCQUFMLENBQXdCMk8sSUFBeEIsQ0FBNkIsVUFBN0IsRUFBeUN1RixpQkFBaUIsS0FBS0UsWUFBL0Q7QUFDRDs7O21DQUVjOVMsTyxFQUFTO0FBQ3RCLFdBQUsrUyxjQUFMLEdBQXNCaGUsRUFBRTRPLDJCQUFpQi9FLHNCQUFuQixFQUEyQ29VLEtBQTNDLENBQWlELElBQWpELENBQXRCO0FBQ0EsV0FBS0QsY0FBTCxDQUFvQjNKLElBQXBCLENBQXlCLElBQXpCLHdCQUFtRCxLQUFLcEIsYUFBeEQ7QUFDQSxXQUFLK0ssY0FBTCxDQUFvQmxkLElBQXBCLENBQXlCLE9BQXpCLEVBQWtDeWIsSUFBbEMsQ0FBdUMsU0FBUzJCLFlBQVQsR0FBd0I7QUFDN0RsZSxVQUFFLElBQUYsRUFBUXdZLFVBQVIsQ0FBbUIsSUFBbkI7QUFDRCxPQUZEOztBQUlBO0FBQ0EsV0FBSzdPLGtCQUFMLEdBQTBCLEtBQUtxVSxjQUFMLENBQW9CbGQsSUFBcEIsQ0FBeUI4TiwyQkFBaUJqRixrQkFBMUMsQ0FBMUI7QUFDQSxXQUFLQyxvQkFBTCxHQUE0QixLQUFLb1UsY0FBTCxDQUFvQmxkLElBQXBCLENBQXlCOE4sMkJBQWlCaEYsb0JBQTFDLENBQTVCO0FBQ0EsV0FBS1csd0JBQUwsR0FBZ0MsS0FBS3lULGNBQUwsQ0FBb0JsZCxJQUFwQixDQUF5QjhOLDJCQUFpQnJFLHdCQUExQyxDQUFoQztBQUNBLFdBQUtSLGdCQUFMLEdBQXdCLEtBQUtpVSxjQUFMLENBQW9CbGQsSUFBcEIsQ0FBeUI4TiwyQkFBaUI3RSxnQkFBMUMsQ0FBeEI7QUFDQSxXQUFLQyxlQUFMLEdBQXVCLEtBQUtnVSxjQUFMLENBQW9CbGQsSUFBcEIsQ0FBeUI4TiwyQkFBaUI1RSxlQUExQyxDQUF2QjtBQUNBLFdBQUtnTixxQkFBTCxHQUE2QixLQUFLZ0gsY0FBTCxDQUFvQmxkLElBQXBCLENBQXlCOE4sMkJBQWlCdEUsNEJBQTFDLENBQTdCO0FBQ0EsV0FBSzJNLHFCQUFMLEdBQTZCLEtBQUsrRyxjQUFMLENBQW9CbGQsSUFBcEIsQ0FBeUI4TiwyQkFBaUJ2RSw0QkFBMUMsQ0FBN0I7QUFDQSxXQUFLOE0sYUFBTCxHQUFxQixLQUFLNkcsY0FBTCxDQUFvQmxkLElBQXBCLENBQXlCOE4sMkJBQWlCcEUsd0JBQTFDLENBQXJCO0FBQ0EsV0FBSzZNLFlBQUwsR0FBb0IsS0FBSzJHLGNBQUwsQ0FBb0JsZCxJQUFwQixDQUF5QjhOLDJCQUFpQm5FLHVCQUExQyxDQUFwQjtBQUNBLFdBQUsyTSxhQUFMLEdBQXFCLEtBQUs0RyxjQUFMLENBQW9CbGQsSUFBcEIsQ0FBeUI4TiwyQkFBaUJsRSx3QkFBMUMsQ0FBckI7QUFDQSxXQUFLb1QsY0FBTCxHQUFzQixLQUFLRSxjQUFMLENBQW9CbGQsSUFBcEIsQ0FBeUI4TiwyQkFBaUJqRSx5QkFBMUMsQ0FBdEI7O0FBRUE7QUFDQSxXQUFLc00scUJBQUwsQ0FBMkJyVyxHQUEzQixDQUNFYixPQUFPaVYsUUFBUCxDQUFnQi9KLFFBQVF1TyxjQUF4QixFQUF3QyxLQUFLNUUsaUJBQTdDLENBREY7QUFHQSxXQUFLb0MscUJBQUwsQ0FBMkJwVyxHQUEzQixDQUNFYixPQUFPaVYsUUFBUCxDQUFnQi9KLFFBQVFzTyxjQUF4QixFQUF3QyxLQUFLM0UsaUJBQTdDLENBREY7QUFHQSxXQUFLdUMsYUFBTCxDQUFtQnZXLEdBQW5CLENBQXVCcUssUUFBUUssUUFBL0IsRUFDRzNLLElBREgsQ0FDUSxtQkFEUixFQUM2QnNLLFFBQVFNLGlCQURyQyxFQUVHNUssSUFGSCxDQUVRLGtCQUZSLEVBRTRCc0ssUUFBUUssUUFGcEM7QUFHQSxXQUFLOEwsYUFBTCxDQUFtQnpXLElBQW5CLENBQXdCLHFCQUF4QixFQUErQ3NLLFFBQVFtTixtQkFBdkQ7O0FBRUE7QUFDQSxVQUFJbk4sUUFBUWtULGNBQVosRUFBNEI7QUFDMUIsYUFBSzVULHdCQUFMLENBQThCM0osR0FBOUIsQ0FBa0NxSyxRQUFRa1QsY0FBMUM7QUFDRDs7QUFFRDtBQUNBLFdBQUtwSixPQUFMLEdBQWU5SixRQUFRbVQsUUFBdkI7QUFDQSxXQUFLTCxZQUFMLEdBQW9CLEtBQUtuRyxrQkFBTCxDQUF3QlcsbUJBQXhCLENBQ2xCdE4sUUFBUUssUUFEVSxFQUVsQkwsUUFBUStNLGtCQUFSLEdBQTZCL00sUUFBUXNPLGNBQXJDLEdBQXNEdE8sUUFBUXVPLGNBRjVDLEVBR2xCLEtBQUs1RSxpQkFIYSxDQUFwQjtBQUtBLFdBQUtvRCxrQkFBTCxHQUEwQi9NLFFBQVErTSxrQkFBbEM7QUFDQSxXQUFLMU0sUUFBTCxHQUFnQkwsUUFBUUssUUFBeEI7QUFDQSxXQUFLb0osV0FBTCxHQUFtQnpKLFFBQVFzTyxjQUEzQjtBQUNBLFdBQUt0RSxXQUFMLEdBQW1CaEssUUFBUXVPLGNBQTNCOztBQUVBO0FBQ0EsV0FBS3pQLGdCQUFMLENBQXNCcUgsSUFBdEIsQ0FDRSxLQUFLK0MsVUFBTCxDQUFnQnJULElBQWhCLENBQXFCOE4sMkJBQWlCN0UsZ0JBQXRDLEVBQXdEcUgsSUFBeEQsRUFERjtBQUdBLFdBQUtwSCxlQUFMLENBQXFCb0gsSUFBckIsQ0FDRSxLQUFLK0MsVUFBTCxDQUFnQnJULElBQWhCLENBQXFCOE4sMkJBQWlCNUUsZUFBdEMsRUFBdURvSCxJQUF2RCxFQURGO0FBR0EsV0FBS2lHLFlBQUwsQ0FBa0JqRyxJQUFsQixDQUF1Qm5HLFFBQVE0TixRQUEvQjtBQUNBLFdBQUt6QixhQUFMLENBQW1CaEcsSUFBbkIsQ0FBd0JuRyxRQUFRTSxpQkFBaEM7QUFDQSxXQUFLdVMsY0FBTCxDQUFvQjFNLElBQXBCLENBQXlCLEtBQUsyTSxZQUE5QjtBQUNBLFdBQUs1SixVQUFMLENBQWdCekYsUUFBaEIsQ0FBeUIsUUFBekIsRUFBbUMyUCxLQUFuQyxDQUF5QyxLQUFLTCxjQUFMLENBQW9CdlAsV0FBcEIsQ0FBZ0MsUUFBaEMsQ0FBekM7O0FBRUEsV0FBS2tKLGFBQUw7QUFDRDs7OzJEQUVzQ3ZKLEssRUFBTztBQUFBOztBQUM1QyxVQUFNL0YsaUJBQWlCckkscUJBQW1CLEtBQUtpVCxhQUF4QixTQUF5Q3JFLDJCQUFpQnhHLGtCQUExRCxDQUF2QjtBQUNBLFVBQU1SLFlBQVlTLGVBQWUxSCxJQUFmLENBQW9CLFlBQXBCLENBQWxCO0FBQ0EsVUFBTStTLGdCQUFnQnJMLGVBQWUxSCxJQUFmLENBQW9CLGdCQUFwQixDQUF0QjtBQUNBLFVBQU13ZCxpQkFBaUI5VixlQUFlMUgsSUFBZixDQUFvQixrQkFBcEIsQ0FBdkI7QUFDQSxVQUFNMFosb0JBQW9CLEtBQUt0QyxvQkFBTCxDQUEwQnVDLDRCQUExQixDQUN4QixLQUFLdEQscUJBQUwsQ0FBMkJwVyxHQUEzQixFQUR3QixFQUV4QmdILFNBRndCLEVBR3hCOEwsYUFId0IsRUFJeEJ5SyxjQUp3QixFQUt4QixLQUFLbEwsYUFMbUIsQ0FBMUI7O0FBUUEsVUFBSW9ILHNCQUFzQixJQUExQixFQUFnQztBQUM5QixhQUFLaUUsV0FBTCxDQUFpQnRlLEVBQUVvTyxNQUFNM04sYUFBUixFQUF1QkUsSUFBdkIsQ0FBNEIsU0FBNUIsQ0FBakIsRUFBeUQsS0FBS3NTLGFBQTlEOztBQUVBO0FBQ0Q7O0FBRUQsVUFBTXNMLGVBQWVsRSxzQkFBc0IsU0FBdEIsR0FBa0MsS0FBS3BELHFCQUF2QyxHQUErRCxLQUFLMU0sd0JBQXpGOztBQUVBLFVBQU1nUSxpQkFBaUIsSUFBSXZaLGVBQUosQ0FDckI7QUFDRUksWUFBSSx5QkFETjtBQUVFZSxzQkFBY29jLGFBQWE1ZCxJQUFiLENBQWtCLHdCQUFsQixDQUZoQjtBQUdFeUIsd0JBQWdCbWMsYUFBYTVkLElBQWIsQ0FBa0IsdUJBQWxCLENBSGxCO0FBSUUyQiw0QkFBb0JpYyxhQUFhNWQsSUFBYixDQUFrQix3QkFBbEIsQ0FKdEI7QUFLRTBCLDBCQUFrQmtjLGFBQWE1ZCxJQUFiLENBQWtCLHlCQUFsQjtBQUxwQixPQURxQixFQVFyQixZQUFNO0FBQ0osZUFBSzJkLFdBQUwsQ0FBaUJ0ZSxFQUFFb08sTUFBTTNOLGFBQVIsRUFBdUJFLElBQXZCLENBQTRCLFNBQTVCLENBQWpCLEVBQXlELE9BQUtzUyxhQUE5RDtBQUNELE9BVm9CLENBQXZCOztBQWFBc0gscUJBQWU3WSxJQUFmO0FBQ0Q7OztnQ0FFV2lQLE8sRUFBU3NDLGEsRUFBZTtBQUNsQyxVQUFNaFMsU0FBUztBQUNic1ksd0JBQWdCLEtBQUt2QyxxQkFBTCxDQUEyQnBXLEdBQTNCLEVBREg7QUFFYjRZLHdCQUFnQixLQUFLdkMscUJBQUwsQ0FBMkJyVyxHQUEzQixFQUZIO0FBR2IwSyxrQkFBVSxLQUFLNkwsYUFBTCxDQUFtQnZXLEdBQW5CLEVBSEc7QUFJYjRkLGlCQUFTLEtBQUtqVSx3QkFBTCxDQUE4QjNKLEdBQTlCO0FBSkksT0FBZjs7QUFPQVosUUFBRTRRLElBQUYsQ0FBTztBQUNMK0ksYUFBSyxLQUFLakosTUFBTCxDQUFZeE0sUUFBWixDQUFxQiw2QkFBckIsRUFBb0Q7QUFDdkR5TSwwQkFEdUQ7QUFFdkRzQztBQUZ1RCxTQUFwRCxDQURBO0FBS0wyRyxnQkFBUSxNQUxIO0FBTUxqWixjQUFNTTtBQU5ELE9BQVAsRUFPRzRQLElBUEgsQ0FRRSxZQUFNO0FBQ0poUixtQ0FBYWdhLElBQWIsQ0FBa0JDLDRCQUFrQjJFLGNBQXBDLEVBQW9EO0FBQ2xEOU4sMEJBRGtEO0FBRWxEc0M7QUFGa0QsU0FBcEQ7QUFJRCxPQWJILEVBY0UsVUFBQ25DLFFBQUQsRUFBYztBQUNaLFlBQUlBLFNBQVNxQixZQUFULElBQXlCckIsU0FBU3FCLFlBQVQsQ0FBc0J0WSxPQUFuRCxFQUE0RDtBQUMxRG1HLFlBQUVvUyxLQUFGLENBQVFDLEtBQVIsQ0FBYyxFQUFDeFksU0FBU2lYLFNBQVNxQixZQUFULENBQXNCdFksT0FBaEMsRUFBZDtBQUNEO0FBQ0YsT0FsQkg7QUFvQkQ7Ozs7O2tCQXBOa0J5akIsZ0I7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNWckI7Ozs7QUFDQTs7QUFDQTs7Ozs7O2NBRVl2ZCxNO0lBQUxDLEMsV0FBQUEsQyxFQTdCUDs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztJQStCcUIwZSxtQjtBQUNuQixpQ0FBYztBQUFBOztBQUNaLFNBQUtoTyxNQUFMLEdBQWMsSUFBSWxOLGdCQUFKLEVBQWQ7QUFDRDs7Ozs2Q0FFd0I0SyxLLEVBQU87QUFDOUJBLFlBQU1DLGNBQU47O0FBRUEsVUFBTUMsT0FBT3RPLEVBQUVvTyxNQUFNM04sYUFBUixDQUFiO0FBQ0EsVUFBTWlkLFlBQVkzZCxPQUFPNlAsT0FBUCxDQUFldEIsS0FBSzNOLElBQUwsQ0FBVSxlQUFWLENBQWYsQ0FBbEI7O0FBRUEsVUFBSSxDQUFDK2MsU0FBTCxFQUFnQjtBQUNkO0FBQ0Q7O0FBRURwUCxXQUFLcVEsU0FBTCxDQUFlLFNBQWY7QUFDQXJRLFdBQUtnSyxJQUFMLENBQVUsVUFBVixFQUFzQixJQUF0QjtBQUNBLFdBQUtzRyxhQUFMLENBQW1CdFEsS0FBSzNOLElBQUwsQ0FBVSxTQUFWLENBQW5CLEVBQXlDMk4sS0FBSzNOLElBQUwsQ0FBVSxlQUFWLENBQXpDO0FBQ0Q7OztrQ0FFYWdRLE8sRUFBU3NDLGEsRUFBZTtBQUNwQ2pULFFBQUU0USxJQUFGLENBQU8sS0FBS0YsTUFBTCxDQUFZeE0sUUFBWixDQUFxQiw2QkFBckIsRUFBb0QsRUFBQ3lNLGdCQUFELEVBQVVzQyw0QkFBVixFQUFwRCxDQUFQLEVBQXNGO0FBQ3BGMkcsZ0JBQVE7QUFENEUsT0FBdEYsRUFFRy9JLElBRkgsQ0FFUSxZQUFNO0FBQ1poUixtQ0FBYWdhLElBQWIsQ0FBa0JDLDRCQUFrQitFLHVCQUFwQyxFQUE2RDtBQUMzREMsNEJBQWtCN0wsYUFEeUM7QUFFM0R0QztBQUYyRCxTQUE3RDtBQUlELE9BUEQsRUFPRyxVQUFDRyxRQUFELEVBQWM7QUFDZixZQUFJQSxTQUFTcUIsWUFBVCxJQUF5QnJCLFNBQVNxQixZQUFULENBQXNCdFksT0FBbkQsRUFBNEQ7QUFDMURtRyxZQUFFb1MsS0FBRixDQUFRQyxLQUFSLENBQWMsRUFBQ3hZLFNBQVNpWCxTQUFTcUIsWUFBVCxDQUFzQnRZLE9BQWhDLEVBQWQ7QUFDRDtBQUNGLE9BWEQ7QUFZRDs7Ozs7a0JBakNrQjZrQixtQjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ05yQjs7OztBQUNBOzs7O0FBQ0E7Ozs7OztjQUVZM2UsTTtJQUFMQyxDLFdBQUFBLEMsRUE3QlA7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7SUErQnFCOFgsb0I7QUFDbkIsa0NBQWM7QUFBQTs7QUFDWixTQUFLcEgsTUFBTCxHQUFjLElBQUlsTixnQkFBSixFQUFkO0FBQ0Q7Ozs7NkNBRXdCdWIsVyxFQUFhOUUsTSxFQUFRO0FBQzVDLFVBQUk4RSxZQUFZdGlCLE1BQVosR0FBcUIsQ0FBekIsRUFBNEI7QUFDMUJzaUIsb0JBQVkzTixJQUFaLENBQWlCcFIsRUFBRWlhLE1BQUYsRUFBVTdJLElBQVYsRUFBakI7QUFDRCxPQUZELE1BRU87QUFDTHBSLFVBQUU0TywyQkFBaUJsRyxhQUFuQixFQUFrQ3NXLE1BQWxDLENBQ0VoZixFQUFFaWEsTUFBRixFQUNHckUsSUFESCxHQUVHcUosTUFGSCxFQURGO0FBS0Q7QUFDRjs7O3NDQUVpQkMsVyxFQUFhO0FBQzdCbGYsUUFBRTRPLDJCQUFpQjdILGFBQW5CLEVBQWtDcUssSUFBbEMsQ0FBdUM4TixXQUF2QztBQUNEOzs7d0NBR0NqTSxhLEVBQ0EzSCxRLEVBQ0F1SixZLEVBQ0FLLFksRUFDQUgsTyxFQUNBOEQsUSxFQUNBdE4saUIsRUFDQTZNLG1CLEVBQ0ErRixjLEVBQ0FuRyxrQixFQUNBO0FBQ0EsVUFBTW1ILGFBQWEsSUFBSTdCLDBCQUFKLENBQXFCckssYUFBckIsQ0FBbkI7QUFDQWtNLGlCQUFXQyxjQUFYLENBQTBCO0FBQ3hCNUYsd0JBQWdCdEUsWUFEUTtBQUV4QnFFLHdCQUFnQjFFLFlBRlE7QUFHeEJ1SixrQkFBVXJKLE9BSGM7QUFJeEJ6SiwwQkFKd0I7QUFLeEJ1TiwwQkFMd0I7QUFNeEJ0Tiw0Q0FOd0I7QUFPeEI2TSxnREFQd0I7QUFReEIrRixzQ0FSd0I7QUFTeEJuRztBQVR3QixPQUExQjtBQVdBaFksUUFBRTRPLDJCQUFpQnBHLG1CQUFuQixFQUF3Q2tHLFFBQXhDLENBQWlELFFBQWpEO0FBQ0ExTyxRQUFFNE8sMkJBQWlCbEcsYUFBbkIsRUFBa0NnRyxRQUFsQyxDQUEyQyxRQUEzQztBQUNEOzs7OERBRThEO0FBQUEsVUFBdkIyUSxZQUF1Qix1RUFBUixNQUFROztBQUM3RHJmLFFBQUU0TywyQkFBaUJyRyxnQkFBbkIsRUFBcUNtRyxRQUFyQyxDQUE4QyxRQUE5QztBQUNBMU8sUUFDSzRPLDJCQUFpQnBHLG1CQUR0QixVQUM4Q29HLDJCQUFpQmxHLGFBRC9ELEVBRUUrRixXQUZGLENBRWMsUUFGZDtBQUdBLFdBQUs2USxxQkFBTCxDQUEyQkQsWUFBM0I7QUFDRDs7O3dEQUVtQztBQUNsQyxXQUFLRSxnQkFBTDtBQUNBdmY7QUFDRTtBQUNHNE8saUNBQWlCcEcsbUJBRnRCLFVBRThDb0csMkJBQWlCbEcsYUFGL0QsVUFFaUZrRywyQkFBaUJyRyxnQkFGbEcsRUFHRW1HLFFBSEYsQ0FHVyxRQUhYO0FBSUEsV0FBSzRRLHFCQUFMO0FBQ0Q7Ozs0Q0FFNEM7QUFBQSxVQUF2QkQsWUFBdUIsdUVBQVIsTUFBUTs7QUFDM0MsVUFBTUcsd0JBQXdCeGYsRUFDNUI0TywyQkFBaUIvSCwyQkFEVyxDQUE5Qjs7QUFJQSxVQUFJMlksc0JBQXNCMWUsSUFBdEIsQ0FBMkI4TiwyQkFBaUI5SCxhQUE1QyxFQUEyRHJLLE1BQTNELEdBQW9FLENBQXhFLEVBQTJFO0FBQ3pFO0FBQ0Q7QUFDRHVELFFBQUU0TywyQkFBaUI5SCxhQUFuQixFQUNHMlksTUFESCxHQUVHQyxRQUZILENBRVlGLHFCQUZaO0FBR0FBLDRCQUFzQjNlLE9BQXRCLENBQThCLE1BQTlCLEVBQXNDNE4sV0FBdEMsQ0FBa0QsUUFBbEQ7O0FBRUE7QUFDQSxXQUFLd0osWUFBTCxDQUFrQnJKLDJCQUFpQjdHLG9CQUFuQztBQUNBLFdBQUtrUSxZQUFMLENBQWtCckosMkJBQWlCNUcsb0JBQW5DOztBQUVBO0FBQ0EsVUFBTTJYLFFBQVEzZixFQUFFNE8sMkJBQWlCM0gsYUFBbkIsRUFBa0NuRyxJQUFsQyxDQUNaLHlCQURZLENBQWQ7QUFHQTZlLFlBQU1sUixXQUFOLENBQWtCLFFBQWxCO0FBQ0F6TyxRQUFFNE8sMkJBQWlCMUgsa0JBQW5CLEVBQXVDd0gsUUFBdkMsQ0FBZ0QsUUFBaEQ7O0FBRUEsVUFBTWtSLGNBQWM1ZixFQUFFcWYsWUFBRixFQUFnQlEsTUFBaEIsR0FBeUJDLEdBQXpCLEdBQStCOWYsRUFBRSxpQkFBRixFQUFxQitmLE1BQXJCLEVBQS9CLEdBQStELEdBQW5GO0FBQ0EvZixRQUFFLFdBQUYsRUFBZWdnQixPQUFmLENBQXVCLEVBQUM1UCxXQUFXd1AsV0FBWixFQUF2QixFQUFpRCxNQUFqRDtBQUNEOzs7eURBRW9DO0FBQ25DNWYsUUFBRTRPLDJCQUFpQmxGLHdCQUFuQixFQUE2Q2dGLFFBQTdDLENBQXNELFFBQXREO0FBQ0ExTyxRQUFFNE8sMkJBQWlCL0gsMkJBQW5CLEVBQ0doRyxPQURILENBQ1csTUFEWCxFQUVHNk4sUUFGSCxDQUVZLFFBRlo7O0FBSUExTyxRQUFFNE8sMkJBQWlCOUgsYUFBbkIsRUFDRzJZLE1BREgsR0FFR0MsUUFGSCxDQUVZOVEsMkJBQWlCaEksdUJBRjdCOztBQUlBNUcsUUFBRTRPLDJCQUFpQjFILGtCQUFuQixFQUF1Q3VILFdBQXZDLENBQW1ELFFBQW5EO0FBQ0F6TyxRQUFFNE8sMkJBQWlCckcsZ0JBQW5CLEVBQXFDa0csV0FBckMsQ0FBaUQsUUFBakQ7QUFDQXpPLFFBQ0s0TywyQkFBaUJwRyxtQkFEdEIsVUFDOENvRywyQkFBaUJsRyxhQUQvRCxFQUVFZ0csUUFGRixDQUVXLFFBRlg7O0FBSUE7QUFDQSxXQUFLdVIsUUFBTCxDQUFjLENBQWQ7QUFDRDs7O2tDQUVhO0FBQ1pqZ0IsUUFBRTRPLDJCQUFpQjlGLGlCQUFuQixFQUFzQ2xJLEdBQXRDLENBQTBDLEVBQTFDO0FBQ0FaLFFBQUU0TywyQkFBaUJqRyxrQkFBbkIsRUFBdUMvSCxHQUF2QyxDQUEyQyxFQUEzQztBQUNBWixRQUFFNE8sMkJBQWlCNUYsMkJBQW5CLEVBQWdEMEYsUUFBaEQsQ0FBeUQsUUFBekQ7QUFDQTFPLFFBQUU0TywyQkFBaUIzRiw0QkFBbkIsRUFBaURySSxHQUFqRCxDQUFxRCxFQUFyRDtBQUNBWixRQUFFNE8sMkJBQWlCM0YsNEJBQW5CLEVBQWlEcVAsSUFBakQsQ0FBc0QsVUFBdEQsRUFBa0UsS0FBbEU7QUFDQXRZLFFBQUU0TywyQkFBaUIxRiwyQkFBbkIsRUFBZ0R0SSxHQUFoRCxDQUFvRCxFQUFwRDtBQUNBWixRQUFFNE8sMkJBQWlCekYsMkJBQW5CLEVBQWdEdkksR0FBaEQsQ0FBb0QsRUFBcEQ7QUFDQVosUUFBRTRPLDJCQUFpQnhGLHVCQUFuQixFQUE0Q3hJLEdBQTVDLENBQWdELEVBQWhEO0FBQ0FaLFFBQUU0TywyQkFBaUJ2Rix1QkFBbkIsRUFBNEMrSCxJQUE1QyxDQUFpRCxFQUFqRDtBQUNBcFIsUUFBRTRPLDJCQUFpQnRGLHNCQUFuQixFQUEyQzhILElBQTNDLENBQWdELEVBQWhEO0FBQ0FwUixRQUFFNE8sMkJBQWlCbEYsd0JBQW5CLEVBQTZDZ0YsUUFBN0MsQ0FBc0QsUUFBdEQ7QUFDQTFPLFFBQUU0TywyQkFBaUJwRyxtQkFBbkIsRUFBd0M4UCxJQUF4QyxDQUE2QyxVQUE3QyxFQUF5RCxJQUF6RDtBQUNEOzs7dUNBRWtCO0FBQUE7O0FBQ2pCdFksUUFBRTRPLDJCQUFpQnhHLGtCQUFuQixFQUF1Q21VLElBQXZDLENBQTRDLFVBQUMyRCxHQUFELEVBQU1DLFVBQU4sRUFBcUI7QUFDL0QsY0FBS0MsWUFBTCxDQUFrQnBnQixFQUFFbWdCLFVBQUYsRUFBY3hmLElBQWQsQ0FBbUIsZUFBbkIsQ0FBbEI7QUFDRCxPQUZEO0FBR0Q7OztpQ0FFWXFaLGMsRUFBZ0I7QUFDM0IsVUFBTStFLGNBQWMvZSxFQUFFNE8sMkJBQWlCakgsZ0JBQWpCLENBQWtDcVMsY0FBbEMsQ0FBRixDQUFwQjtBQUNBLFVBQU1xRyxrQkFBa0JyZ0IsRUFDdEI0TywyQkFBaUIvRyxzQkFBakIsQ0FBd0NtUyxjQUF4QyxDQURzQixDQUF4QjtBQUdBcUcsc0JBQWdCcmUsTUFBaEI7QUFDQStjLGtCQUFZdFEsV0FBWixDQUF3QixRQUF4QjtBQUNEOzs7NkJBRVE2UixlLEVBQWlCO0FBQ3hCLFVBQU1YLFFBQVEzZixFQUFFNE8sMkJBQWlCM0gsYUFBbkIsRUFBa0NuRyxJQUFsQyxDQUNaLHlCQURZLENBQWQ7QUFHQSxVQUFNeWYscUJBQXFCdmdCLEVBQ3pCNE8sMkJBQWlCekcsOEJBRFEsQ0FBM0I7QUFHQSxVQUFNcVksbUJBQW1CeGdCLEVBQUU0TywyQkFBaUJ4SCx1QkFBbkIsQ0FBekI7QUFDQSxVQUFNcVosaUJBQWlCL2hCLFNBQVM4aEIsaUJBQWlCN2YsSUFBakIsQ0FBc0IsWUFBdEIsQ0FBVCxFQUE4QyxFQUE5QyxDQUF2QjtBQUNBLFVBQU0rZixVQUFVMWxCLEtBQUsybEIsSUFBTCxDQUFVaEIsTUFBTWxqQixNQUFOLEdBQWVna0IsY0FBekIsQ0FBaEI7QUFDQSxVQUFNRyxVQUFVNWxCLEtBQUs2bEIsR0FBTCxDQUFTLENBQVQsRUFBWTdsQixLQUFLOGxCLEdBQUwsQ0FBU1IsZUFBVCxFQUEwQkksT0FBMUIsQ0FBWixDQUFoQjtBQUNBLFdBQUtLLHNCQUFMLENBQTRCSCxPQUE1Qjs7QUFFQTtBQUNBakIsWUFBTWpSLFFBQU4sQ0FBZSxRQUFmO0FBQ0E2Uix5QkFBbUI3UixRQUFuQixDQUE0QixRQUE1QjtBQUNBOztBQUVBLFVBQU1zUyxXQUFXLENBQUNKLFVBQVUsQ0FBWCxJQUFnQkgsY0FBaEIsR0FBaUMsQ0FBbEQ7QUFDQSxVQUFNUSxTQUFTTCxVQUFVSCxjQUF6Qjs7QUFFQSxXQUFLLElBQUlTLElBQUlGLFdBQVcsQ0FBeEIsRUFBMkJFLElBQUlsbUIsS0FBSzhsQixHQUFMLENBQVNHLE1BQVQsRUFBaUJ0QixNQUFNbGpCLE1BQXZCLENBQS9CLEVBQStEeWtCLEtBQUssQ0FBcEUsRUFBdUU7QUFDckVsaEIsVUFBRTJmLE1BQU11QixDQUFOLENBQUYsRUFBWXpTLFdBQVosQ0FBd0IsUUFBeEI7QUFDRDs7QUFFRDhSLHlCQUFtQmhFLElBQW5CLENBQXdCLFlBQVk7QUFDbEMsWUFDRSxDQUFDdmMsRUFBRSxJQUFGLEVBQ0VtaEIsSUFERixHQUVFaFIsUUFGRixDQUVXLFFBRlgsQ0FESCxFQUlFO0FBQ0FuUSxZQUFFLElBQUYsRUFBUXlPLFdBQVIsQ0FBb0IsUUFBcEI7QUFDRDtBQUNGLE9BUkQ7O0FBVUE7QUFDQXpPLFFBQUU0TywyQkFBaUI5RSxjQUFuQixFQUNHc1gsR0FESCxDQUNPeFMsMkJBQWlCL0Usc0JBRHhCLEVBRUc3SCxNQUZIOztBQUlBO0FBQ0EsV0FBS2lXLFlBQUwsQ0FBa0JySiwyQkFBaUIzRyw2QkFBbkM7QUFDQSxXQUFLZ1EsWUFBTCxDQUFrQnJKLDJCQUFpQjFHLDZCQUFuQztBQUNEOzs7MkNBRXNCMFksTyxFQUFTO0FBQzlCO0FBQ0EsVUFBTVMsWUFBWXJoQixFQUFFNE8sMkJBQWlCeEgsdUJBQW5CLEVBQTRDdEcsSUFBNUMsQ0FBaUQsY0FBakQsRUFBaUVyRSxNQUFqRSxHQUNkLENBREo7QUFFQXVELFFBQUU0TywyQkFBaUJ4SCx1QkFBbkIsRUFDR3RHLElBREgsQ0FDUSxTQURSLEVBRUcyTixXQUZILENBRWUsUUFGZjtBQUdBek8sUUFBRTRPLDJCQUFpQnhILHVCQUFuQixFQUNHdEcsSUFESCwyQkFDZ0M4ZixPQURoQyxVQUVHbFMsUUFGSCxDQUVZLFFBRlo7QUFHQTFPLFFBQUU0TywyQkFBaUJ0SCwyQkFBbkIsRUFBZ0RtSCxXQUFoRCxDQUE0RCxVQUE1RDtBQUNBLFVBQUltUyxZQUFZLENBQWhCLEVBQW1CO0FBQ2pCNWdCLFVBQUU0TywyQkFBaUJ0SCwyQkFBbkIsRUFBZ0RvSCxRQUFoRCxDQUF5RCxVQUF6RDtBQUNEO0FBQ0QxTyxRQUFFNE8sMkJBQWlCdkgsMkJBQW5CLEVBQWdEb0gsV0FBaEQsQ0FBNEQsVUFBNUQ7QUFDQSxVQUFJbVMsWUFBWVMsU0FBaEIsRUFBMkI7QUFDekJyaEIsVUFBRTRPLDJCQUFpQnZILDJCQUFuQixFQUFnRHFILFFBQWhELENBQXlELFVBQXpEO0FBQ0Q7QUFDRCxXQUFLNFMsd0JBQUw7QUFDRDs7O3FDQUVnQkMsVSxFQUFZO0FBQzNCdmhCLFFBQUU0TywyQkFBaUJ4SCx1QkFBbkIsRUFBNEN6RyxJQUE1QyxDQUFpRCxZQUFqRCxFQUErRDRnQixVQUEvRDtBQUNBLFdBQUtDLHdCQUFMO0FBQ0Q7OzsrQ0FFMEI7QUFDekI7QUFDQSxVQUFNSCxZQUFZcmhCLEVBQUU0TywyQkFBaUJ4SCx1QkFBbkIsRUFBNEN0RyxJQUE1QyxDQUFpRCxjQUFqRCxFQUFpRXJFLE1BQWpFLEdBQ2QsQ0FESjtBQUVBdUQsUUFBRTRPLDJCQUFpQnpILHFCQUFuQixFQUEwQ3NMLFdBQTFDLENBQ0UsUUFERixFQUVFNE8sYUFBYSxDQUZmO0FBSUQ7OztxREFFZ0M7QUFDL0JyaEIsUUFBRTRPLDJCQUFpQmxGLHdCQUFuQixFQUE2QytJLFdBQTdDLENBQ0UsUUFERixFQUVFL1QsU0FBU3NCLEVBQUU0TywyQkFBaUJwRix1QkFBbkIsRUFBNEM1SSxHQUE1QyxFQUFULEVBQTRELEVBQTVELE1BQW9FLENBRnRFO0FBSUQ7OztpQ0FFWTZWLE0sRUFBNkI7QUFBQSxVQUFyQmdMLFlBQXFCLHVFQUFOLElBQU07O0FBQ3hDLFVBQUlDLG9CQUFvQixLQUF4Qjs7QUFFQSxVQUFJRCxpQkFBaUIsSUFBckIsRUFBMkI7QUFDekJ6aEIsVUFBRXlXLE1BQUYsRUFDR0UsTUFESCxDQUNVLElBRFYsRUFFRzRGLElBRkgsQ0FFUSxZQUFZO0FBQ2hCLGNBQUl2YyxFQUFFLElBQUYsRUFBUW9SLElBQVIsT0FBbUIsRUFBdkIsRUFBMkI7QUFDekJzUSxnQ0FBb0IsSUFBcEI7QUFDQSxtQkFBTyxLQUFQO0FBQ0Q7QUFDRCxpQkFBTyxJQUFQO0FBQ0QsU0FSSDtBQVNELE9BVkQsTUFVTztBQUNMQSw0QkFBb0JELFlBQXBCO0FBQ0Q7QUFDRHpoQixRQUFFeVcsTUFBRixFQUFVaEUsV0FBVixDQUFzQixRQUF0QixFQUFnQyxDQUFDaVAsaUJBQWpDO0FBQ0Q7OzsrQ0FFMEI7QUFDekIsVUFBTWxCLG1CQUFtQnhnQixFQUFFNE8sMkJBQWlCeEgsdUJBQW5CLENBQXpCO0FBQ0EsVUFBTW1hLGFBQWFmLGlCQUFpQjdmLElBQWpCLENBQXNCLFlBQXRCLENBQW5CO0FBQ0EsVUFBTWdmLFFBQVEzZixFQUFFNE8sMkJBQWlCM0gsYUFBbkIsRUFBa0NuRyxJQUFsQyxDQUF1Qyx5QkFBdkMsQ0FBZDtBQUNBLFVBQU02Z0IsV0FBVzNtQixLQUFLMmxCLElBQUwsQ0FBVWhCLE1BQU1sakIsTUFBTixHQUFlOGtCLFVBQXpCLENBQWpCOztBQUVBO0FBQ0FmLHVCQUFpQjdmLElBQWpCLENBQXNCLFVBQXRCLEVBQWtDZ2hCLFFBQWxDOztBQUVBO0FBQ0EsVUFBTUMsMEJBQTBCNWhCLEVBQUU0TywyQkFBaUJuSCwrQkFBbkIsQ0FBaEM7QUFDQXpILFFBQUU0TywyQkFBaUJ4SCx1QkFBbkIsRUFBNEN0RyxJQUE1QyxDQUFpRCx1QkFBakQsRUFBMEVrQixNQUExRTtBQUNBaEMsUUFBRTRPLDJCQUFpQnZILDJCQUFuQixFQUFnRDJYLE1BQWhELENBQXVENEMsdUJBQXZEOztBQUVBO0FBQ0EsV0FBSyxJQUFJVixJQUFJLENBQWIsRUFBZ0JBLEtBQUtTLFFBQXJCLEVBQStCVCxLQUFLLENBQXBDLEVBQXVDO0FBQ3JDLFlBQU1XLGtCQUFrQkQsd0JBQXdCM0QsS0FBeEIsRUFBeEI7QUFDQTRELHdCQUFnQi9nQixJQUFoQixDQUFxQixNQUFyQixFQUE2QnVULElBQTdCLENBQWtDLFdBQWxDLEVBQStDNk0sQ0FBL0M7QUFDQVcsd0JBQWdCL2dCLElBQWhCLENBQXFCLE1BQXJCLEVBQTZCc1EsSUFBN0IsQ0FBa0M4UCxDQUFsQztBQUNBVSxnQ0FBd0I1QyxNQUF4QixDQUErQjZDLGdCQUFnQnBULFdBQWhCLENBQTRCLFFBQTVCLENBQS9CO0FBQ0Q7O0FBRUQsV0FBSzZTLHdCQUFMO0FBQ0Q7Ozs7O2tCQWxSa0J4SixvQjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ05yQjs7OztBQUNBOzs7Ozs7QUExQkE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Y0E0QlkvWCxNO0lBQUxDLEMsV0FBQUEsQzs7SUFFYzhoQixzQjtBQUNuQixvQ0FBYztBQUFBOztBQUNaLFNBQUtwUixNQUFMLEdBQWMsSUFBSWxOLGdCQUFKLEVBQWQ7QUFDRDs7Ozs0QkFFT21OLE8sRUFBUztBQUNmM1EsUUFBRWtSLE9BQUYsQ0FBVSxLQUFLUixNQUFMLENBQVl4TSxRQUFaLENBQXFCLDJCQUFyQixFQUFrRCxFQUFDeU0sZ0JBQUQsRUFBbEQsQ0FBVixFQUNHRSxJQURILENBQ1EsVUFBQ0MsUUFBRCxFQUFjO0FBQ2xCOVEsVUFBRTRPLDJCQUFpQnJJLHFCQUFuQixFQUEwQ3hGLElBQTFDLENBQStDK1AsU0FBU0ssS0FBeEQ7QUFDQW5SLFVBQUU0TywyQkFBaUJwSSxvQkFBbkIsRUFBeUM0SyxJQUF6QyxDQUE4Q04sU0FBU00sSUFBdkQ7QUFDRCxPQUpIO0FBS0Q7Ozs7O2tCQVhrQjBRLHNCOzs7Ozs7Ozs7Ozs7Ozs7O0FDOUJyQjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztrQkF5QmU7QUFDYmpELDJCQUF5Qix5QkFEWjtBQUViOUUsdUJBQXFCLHFCQUZSO0FBR2IwRSxrQkFBZ0IsZ0JBSEg7QUFJYmIsMEJBQXdCLHdCQUpYO0FBS2JtRSx3QkFBc0Isc0JBTFQ7QUFNYkMsNEJBQTBCO0FBTmIsQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ0FmOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7Ozs7O2NBRVlqaUIsTTtJQUFMQyxDLFdBQUFBLEMsRUF2Q1A7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7SUF5Q3FCaWlCLGE7QUFDbkIsMkJBQWM7QUFBQTs7QUFDWixTQUFLQyx1QkFBTCxHQUErQixJQUFJelIsaUNBQUosRUFBL0I7QUFDQSxTQUFLMFIsbUJBQUwsR0FBMkIsSUFBSXpELDZCQUFKLEVBQTNCO0FBQ0EsU0FBSzdHLG9CQUFMLEdBQTRCLElBQUlDLDhCQUFKLEVBQTVCO0FBQ0EsU0FBS0Msb0JBQUwsR0FBNEIsSUFBSXpGLDhCQUFKLEVBQTVCO0FBQ0EsU0FBSzhQLHNCQUFMLEdBQThCLElBQUluUSxnQ0FBSixFQUE5QjtBQUNBLFNBQUtvUSxzQkFBTCxHQUE4QixJQUFJUCxnQ0FBSixFQUE5QjtBQUNBLFNBQUtRLHVCQUFMLEdBQStCLElBQUl0UixpQ0FBSixFQUEvQjtBQUNBLFNBQUt1UixzQkFBTCxHQUE4QixJQUFJbFIsZ0NBQUosRUFBOUI7QUFDQSxTQUFLbVIsa0JBQUwsR0FBMEIsSUFBSWhJLDRCQUFKLEVBQTFCO0FBQ0EsU0FBSzlKLE1BQUwsR0FBYyxJQUFJbE4sZ0JBQUosRUFBZDtBQUNBLFNBQUtpZixjQUFMO0FBQ0Q7Ozs7cUNBRWdCO0FBQUE7O0FBQ2Z6aUIsUUFBRTRPLDJCQUFpQjVJLHFCQUFuQixFQUEwQzBjLFFBQTFDLENBQW1EO0FBQ2pEQyxjQUFNLFFBRDJDO0FBRWpEQyxlQUFPLEtBRjBDO0FBR2pEN0MsZ0JBQVE7QUFIeUMsT0FBbkQ7QUFLQS9mLFFBQUU0TywyQkFBaUI3SSxzQkFBbkIsRUFBMkMyYyxRQUEzQyxDQUFvRDtBQUNsREMsY0FBTSxRQUQ0QztBQUVsREMsZUFBTyxLQUYyQztBQUdsRDdDLGdCQUFRO0FBSDBDLE9BQXBEOztBQU1BbGdCLGlDQUFhUyxFQUFiLENBQWdCd1osNEJBQWtCK0UsdUJBQWxDLEVBQTJELFVBQUN6USxLQUFELEVBQVc7QUFDcEUsY0FBSzJKLG9CQUFMLENBQTBCOEssT0FBMUIsQ0FBa0N6VSxNQUFNdUMsT0FBeEM7QUFDQSxjQUFLeVIsc0JBQUwsQ0FBNEJTLE9BQTVCLENBQW9DelUsTUFBTXVDLE9BQTFDO0FBQ0EsY0FBS21TLG1CQUFMLENBQXlCMVUsTUFBTXVDLE9BQS9CO0FBQ0EsY0FBS3VSLHVCQUFMLENBQTZCVyxPQUE3QixDQUFxQ3pVLE1BQU11QyxPQUEzQztBQUNBLGNBQUsyUix1QkFBTCxDQUE2Qk8sT0FBN0IsQ0FBcUN6VSxNQUFNdUMsT0FBM0M7QUFDQSxjQUFLMFIsc0JBQUwsQ0FBNEJRLE9BQTVCLENBQW9DelUsTUFBTXVDLE9BQTFDO0FBQ0QsT0FQRDs7QUFTQTlRLGlDQUFhUyxFQUFiLENBQWdCd1osNEJBQWtCOEQsc0JBQWxDLEVBQTBELFVBQUN4UCxLQUFELEVBQVc7QUFDbkUsY0FBS3lKLG9CQUFMLENBQTBCdUksWUFBMUIsQ0FBdUNoUyxNQUFNNkUsYUFBN0M7QUFDQSxZQUFNOFAsZUFBZS9pQixFQUFFNE8sMkJBQWlCOUUsY0FBbkIsRUFBbUNzWCxHQUFuQyxDQUF1Q3hTLDJCQUFpQi9FLHNCQUF4RCxFQUFnRnBOLE1BQXJHOztBQUVBLFlBQUlzbUIsZUFBZSxDQUFuQixFQUFzQjtBQUNwQjtBQUNEO0FBQ0QsY0FBS2xMLG9CQUFMLENBQTBCbUwsa0NBQTFCO0FBQ0QsT0FSRDs7QUFVQW5qQixpQ0FBYVMsRUFBYixDQUFnQndaLDRCQUFrQjJFLGNBQWxDLEVBQWtELFVBQUNyUSxLQUFELEVBQVc7QUFDM0QsY0FBS3lKLG9CQUFMLENBQTBCdUksWUFBMUIsQ0FBdUNoUyxNQUFNNkUsYUFBN0M7QUFDQSxjQUFLOEUsb0JBQUwsQ0FBMEI4SyxPQUExQixDQUFrQ3pVLE1BQU11QyxPQUF4QztBQUNBLGNBQUtvSCxvQkFBTCxDQUEwQmtMLG9CQUExQixDQUErQzdVLE1BQU11QyxPQUFyRDtBQUNBLGNBQUttUyxtQkFBTCxDQUF5QjFVLE1BQU11QyxPQUEvQjtBQUNBLGNBQUt5UixzQkFBTCxDQUE0QlMsT0FBNUIsQ0FBb0N6VSxNQUFNdUMsT0FBMUM7QUFDQSxjQUFLdVIsdUJBQUwsQ0FBNkJXLE9BQTdCLENBQXFDelUsTUFBTXVDLE9BQTNDO0FBQ0EsY0FBSzRSLHNCQUFMLENBQTRCTSxPQUE1QixDQUFvQ3pVLE1BQU11QyxPQUExQztBQUNBLGNBQUsyUix1QkFBTCxDQUE2Qk8sT0FBN0IsQ0FBcUN6VSxNQUFNdUMsT0FBM0M7QUFDQSxjQUFLMFIsc0JBQUwsQ0FBNEJRLE9BQTVCLENBQW9DelUsTUFBTXVDLE9BQTFDO0FBQ0EsY0FBS3VTLHNCQUFMO0FBQ0EsY0FBS0Msb0JBQUw7QUFDQSxjQUFLQyxhQUFMOztBQUVBLFlBQU1MLGVBQWUvaUIsRUFBRTRPLDJCQUFpQjlFLGNBQW5CLEVBQW1Dc1gsR0FBbkMsQ0FBdUN4UywyQkFBaUIvRSxzQkFBeEQsRUFBZ0ZwTixNQUFyRzs7QUFFQSxZQUFJc21CLGVBQWUsQ0FBbkIsRUFBc0I7QUFDcEI7QUFDRDtBQUNELGNBQUtsTCxvQkFBTCxDQUEwQm1MLGtDQUExQjtBQUNELE9BcEJEOztBQXNCQW5qQixpQ0FBYVMsRUFBYixDQUFnQndaLDRCQUFrQkMsbUJBQWxDLEVBQXVELFVBQUMzTCxLQUFELEVBQVc7QUFDaEUsY0FBS3lKLG9CQUFMLENBQTBCd0wsV0FBMUI7QUFDQSxjQUFLdEwsb0JBQUwsQ0FBMEJrTCxvQkFBMUIsQ0FBK0M3VSxNQUFNdUMsT0FBckQ7QUFDQSxjQUFLb0gsb0JBQUwsQ0FBMEI4SyxPQUExQixDQUFrQ3pVLE1BQU11QyxPQUF4QztBQUNBLGNBQUttUyxtQkFBTCxDQUF5QjFVLE1BQU11QyxPQUEvQjtBQUNBLGNBQUt5UixzQkFBTCxDQUE0QlMsT0FBNUIsQ0FBb0N6VSxNQUFNdUMsT0FBMUM7QUFDQSxjQUFLdVIsdUJBQUwsQ0FBNkJXLE9BQTdCLENBQXFDelUsTUFBTXVDLE9BQTNDO0FBQ0EsY0FBSzRSLHNCQUFMLENBQTRCTSxPQUE1QixDQUFvQ3pVLE1BQU11QyxPQUExQztBQUNBLGNBQUsyUix1QkFBTCxDQUE2Qk8sT0FBN0IsQ0FBcUN6VSxNQUFNdUMsT0FBM0M7QUFDQSxjQUFLMFIsc0JBQUwsQ0FBNEJRLE9BQTVCLENBQW9DelUsTUFBTXVDLE9BQTFDO0FBQ0EsY0FBS2tILG9CQUFMLENBQTBCbUwsa0NBQTFCO0FBQ0QsT0FYRDtBQVlEOzs7NkNBRXdCO0FBQUE7O0FBQ3ZCaGpCLFFBQUU0TywyQkFBaUI1SCxnQkFBbkIsRUFDR3NjLEdBREgsQ0FDTyxPQURQLEVBRUdoakIsRUFGSCxDQUVNLE9BRk4sRUFFZSxVQUFDOE4sS0FBRDtBQUFBLGVBQVcsT0FBSytULG1CQUFMLENBQXlCb0Isd0JBQXpCLENBQWtEblYsS0FBbEQsQ0FBWDtBQUFBLE9BRmY7QUFHRDs7O29DQUVlO0FBQ2RwTyxRQUFFNE8sMkJBQWlCeEcsa0JBQW5CLEVBQXVDdVcsU0FBdkM7QUFDQTNlLFFBQUU0TywyQkFBaUI1SCxnQkFBbkIsRUFBcUMyWCxTQUFyQztBQUNEOzs7MkNBRXNCO0FBQUE7O0FBQ3JCM2UsUUFBRTRPLDJCQUFpQnhHLGtCQUFuQixFQUF1Q2tiLEdBQXZDLENBQTJDLE9BQTNDLEVBQW9EaGpCLEVBQXBELENBQXVELE9BQXZELEVBQWdFLFVBQUM4TixLQUFELEVBQVc7QUFDekUsWUFBTUUsT0FBT3RPLEVBQUVvTyxNQUFNM04sYUFBUixDQUFiO0FBQ0EsZUFBS29YLG9CQUFMLENBQTBCMkwsdUNBQTFCO0FBQ0EsZUFBSzNMLG9CQUFMLENBQTBCNEwsbUJBQTFCLENBQ0VuVixLQUFLM04sSUFBTCxDQUFVLGVBQVYsQ0FERixFQUVFMk4sS0FBSzNOLElBQUwsQ0FBVSxpQkFBVixDQUZGLEVBR0UyTixLQUFLM04sSUFBTCxDQUFVLHFCQUFWLENBSEYsRUFJRTJOLEtBQUszTixJQUFMLENBQVUscUJBQVYsQ0FKRixFQUtFMk4sS0FBSzNOLElBQUwsQ0FBVSxTQUFWLENBTEYsRUFNRTJOLEtBQUszTixJQUFMLENBQVUsVUFBVixDQU5GLEVBT0UyTixLQUFLM04sSUFBTCxDQUFVLG1CQUFWLENBUEYsRUFRRTJOLEtBQUszTixJQUFMLENBQVUscUJBQVYsQ0FSRixFQVNFMk4sS0FBSzNOLElBQUwsQ0FBVSxnQkFBVixDQVRGLEVBVUUyTixLQUFLM04sSUFBTCxDQUFVLG9CQUFWLENBVkY7QUFZRCxPQWZEO0FBZ0JEOzs7MkNBRXNCO0FBQUE7O0FBQ3JCWCxRQUFFNE8sMkJBQWlCL0QsZ0JBQWpCLENBQWtDdkosS0FBcEMsRUFBMkNoQixFQUEzQyxDQUE4QyxlQUE5QyxFQUErRCxVQUFDOE4sS0FBRCxFQUFXO0FBQ3hFLFlBQU1zVixTQUFTMWpCLEVBQUVvTyxNQUFNdVYsYUFBUixDQUFmO0FBQ0EsWUFBTUMsWUFBWUYsT0FBTy9pQixJQUFQLENBQVksV0FBWixDQUFsQjtBQUNBWCxVQUFFNE8sMkJBQWlCL0QsZ0JBQWpCLENBQWtDRSxJQUFwQyxFQUEwQy9JLE1BQTFDO0FBQ0E0aEIsa0JBQVVobkIsT0FBVixDQUFrQixVQUFDaW5CLElBQUQsRUFBVTtBQUMxQixjQUFNQyxRQUFROWpCLEVBQUU0TywyQkFBaUIvRCxnQkFBakIsQ0FBa0NHLFFBQXBDLEVBQThDaVQsS0FBOUMsRUFBZDtBQUNBNkYsZ0JBQU16UCxJQUFOLENBQVcsSUFBWCxtQkFBZ0N3UCxLQUFLemlCLEVBQXJDLEVBQTJDcU4sV0FBM0MsQ0FBdUQsUUFBdkQ7QUFDQXFWLGdCQUFNaGpCLElBQU4sQ0FBVzhOLDJCQUFpQi9ELGdCQUFqQixDQUFrQ0ksT0FBbEMsQ0FBMENDLEdBQXJELEVBQTBEbUosSUFBMUQsQ0FBK0QsS0FBL0QsRUFBc0V3UCxLQUFLRSxTQUEzRTtBQUNBRCxnQkFBTWhqQixJQUFOLENBQVc4TiwyQkFBaUIvRCxnQkFBakIsQ0FBa0NJLE9BQWxDLENBQTBDblIsSUFBckQsRUFBMkRzWCxJQUEzRCxDQUFnRXlTLEtBQUsvcEIsSUFBckU7QUFDQWdxQixnQkFBTWhqQixJQUFOLENBQVc4TiwyQkFBaUIvRCxnQkFBakIsQ0FBa0NJLE9BQWxDLENBQTBDRSxJQUFyRCxFQUEyRGtKLElBQTNELENBQ0UsTUFERixFQUVFLE9BQUszRCxNQUFMLENBQVl4TSxRQUFaLENBQXFCLG9CQUFyQixFQUEyQyxFQUFDOUMsSUFBSXlpQixLQUFLemlCLEVBQVYsRUFBM0MsQ0FGRjtBQUlBLGNBQUl5aUIsS0FBS0csU0FBTCxLQUFtQixFQUF2QixFQUEyQjtBQUN6QkYsa0JBQU1oakIsSUFBTixDQUFXOE4sMkJBQWlCL0QsZ0JBQWpCLENBQWtDSSxPQUFsQyxDQUEwQ0csR0FBckQsRUFBMEQ3SCxNQUExRCxDQUFpRXNnQixLQUFLRyxTQUF0RTtBQUNELFdBRkQsTUFFTztBQUNMRixrQkFBTWhqQixJQUFOLENBQVc4TiwyQkFBaUIvRCxnQkFBakIsQ0FBa0NJLE9BQWxDLENBQTBDRyxHQUFyRCxFQUEwRHBKLE1BQTFEO0FBQ0Q7QUFDRCxjQUFJNmhCLEtBQUtJLGlCQUFMLEtBQTJCLEVBQS9CLEVBQW1DO0FBQ2pDSCxrQkFBTWhqQixJQUFOLENBQVc4TiwyQkFBaUIvRCxnQkFBakIsQ0FBa0NJLE9BQWxDLENBQTBDSSxXQUFyRCxFQUFrRTlILE1BQWxFLENBQXlFc2dCLEtBQUtJLGlCQUE5RTtBQUNELFdBRkQsTUFFTztBQUNMSCxrQkFBTWhqQixJQUFOLENBQVc4TiwyQkFBaUIvRCxnQkFBakIsQ0FBa0NJLE9BQWxDLENBQTBDSSxXQUFyRCxFQUFrRXJKLE1BQWxFO0FBQ0Q7QUFDRCxjQUFJNmhCLEtBQUt2WSxRQUFMLEdBQWdCLENBQXBCLEVBQXVCO0FBQ3JCd1ksa0JBQU1oakIsSUFBTixDQUFjOE4sMkJBQWlCL0QsZ0JBQWpCLENBQWtDSSxPQUFsQyxDQUEwQ0ssUUFBeEQsWUFBeUU4RixJQUF6RSxDQUE4RXlTLEtBQUt2WSxRQUFuRjtBQUNELFdBRkQsTUFFTztBQUNMd1ksa0JBQU1oakIsSUFBTixDQUFXOE4sMkJBQWlCL0QsZ0JBQWpCLENBQWtDSSxPQUFsQyxDQUEwQ0ssUUFBckQsRUFBK0Q4RixJQUEvRCxDQUFvRXlTLEtBQUt2WSxRQUF6RTtBQUNEO0FBQ0R3WSxnQkFBTWhqQixJQUFOLENBQVc4TiwyQkFBaUIvRCxnQkFBakIsQ0FBa0NJLE9BQWxDLENBQTBDTSxpQkFBckQsRUFBd0U2RixJQUF4RSxDQUE2RXlTLEtBQUt0WSxpQkFBbEY7QUFDQXZMLFlBQUU0TywyQkFBaUIvRCxnQkFBakIsQ0FBa0NHLFFBQXBDLEVBQThDZ1UsTUFBOUMsQ0FBcUQ4RSxLQUFyRDtBQUNELFNBMUJEO0FBMkJELE9BL0JEO0FBZ0NEOzs7MENBRXFCO0FBQUE7O0FBQ3BCOWpCLFFBQUU0TywyQkFBaUJ0RyxhQUFuQixFQUFrQ2hJLEVBQWxDLENBQ0UsT0FERixFQUVFLFlBQU07QUFDSixlQUFLdVgsb0JBQUwsQ0FBMEJlLDhCQUExQjtBQUNBLGVBQUtmLG9CQUFMLENBQTBCMkwsdUNBQTFCLENBQWtFNVUsMkJBQWlCakcsa0JBQW5GO0FBQ0QsT0FMSDtBQU9BM0ksUUFBRTRPLDJCQUFpQm5HLG1CQUFuQixFQUF3Q25JLEVBQXhDLENBQ0UsT0FERixFQUNXO0FBQUEsZUFBTSxPQUFLdVgsb0JBQUwsQ0FBMEJtTCxrQ0FBMUIsRUFBTjtBQUFBLE9BRFg7QUFHRDs7O2lEQUU0QjtBQUFBOztBQUMzQmhqQixRQUFFNE8sMkJBQWlCeEgsdUJBQW5CLEVBQTRDOUcsRUFBNUMsQ0FBK0MsT0FBL0MsRUFBd0RzTywyQkFBaUJySCwyQkFBekUsRUFBc0csVUFBQzZHLEtBQUQsRUFBVztBQUMvR0EsY0FBTUMsY0FBTjtBQUNBLFlBQU1DLE9BQU90TyxFQUFFb08sTUFBTTNOLGFBQVIsQ0FBYjtBQUNBWixtQ0FBYWdhLElBQWIsQ0FBa0JDLDRCQUFrQmlJLG9CQUFwQyxFQUEwRDtBQUN4RG5CLG1CQUFTdFMsS0FBSzNOLElBQUwsQ0FBVSxNQUFWO0FBRCtDLFNBQTFEO0FBR0QsT0FORDtBQU9BWCxRQUFFNE8sMkJBQWlCdkgsMkJBQW5CLEVBQWdEL0csRUFBaEQsQ0FBbUQsT0FBbkQsRUFBNEQsVUFBQzhOLEtBQUQsRUFBVztBQUNyRUEsY0FBTUMsY0FBTjtBQUNBLFlBQU1DLE9BQU90TyxFQUFFb08sTUFBTTNOLGFBQVIsQ0FBYjs7QUFFQSxZQUFJNk4sS0FBSzZCLFFBQUwsQ0FBYyxVQUFkLENBQUosRUFBK0I7QUFDN0I7QUFDRDtBQUNELFlBQU0rVCxhQUFhLE9BQUtDLGFBQUwsRUFBbkI7QUFDQXRrQixtQ0FBYWdhLElBQWIsQ0FBa0JDLDRCQUFrQmlJLG9CQUFwQyxFQUEwRDtBQUN4RG5CLG1CQUFTbGlCLFNBQVNzQixFQUFFa2tCLFVBQUYsRUFBYzlTLElBQWQsRUFBVCxFQUErQixFQUEvQixJQUFxQztBQURVLFNBQTFEO0FBR0QsT0FYRDtBQVlBcFIsUUFBRTRPLDJCQUFpQnRILDJCQUFuQixFQUFnRGhILEVBQWhELENBQW1ELE9BQW5ELEVBQTRELFVBQUM4TixLQUFELEVBQVc7QUFDckVBLGNBQU1DLGNBQU47QUFDQSxZQUFNQyxPQUFPdE8sRUFBRW9PLE1BQU0zTixhQUFSLENBQWI7O0FBRUEsWUFBSTZOLEtBQUs2QixRQUFMLENBQWMsVUFBZCxDQUFKLEVBQStCO0FBQzdCO0FBQ0Q7QUFDRCxZQUFNK1QsYUFBYSxPQUFLQyxhQUFMLEVBQW5CO0FBQ0F0a0IsbUNBQWFnYSxJQUFiLENBQWtCQyw0QkFBa0JpSSxvQkFBcEMsRUFBMEQ7QUFDeERuQixtQkFBU2xpQixTQUFTc0IsRUFBRWtrQixVQUFGLEVBQWM5UyxJQUFkLEVBQVQsRUFBK0IsRUFBL0IsSUFBcUM7QUFEVSxTQUExRDtBQUdELE9BWEQ7QUFZQXBSLFFBQUU0TywyQkFBaUJsSCxxQ0FBbkIsRUFBMERwSCxFQUExRCxDQUE2RCxRQUE3RCxFQUF1RSxVQUFDOE4sS0FBRCxFQUFXO0FBQ2hGQSxjQUFNQyxjQUFOO0FBQ0EsWUFBTStWLFVBQVVwa0IsRUFBRW9PLE1BQU0zTixhQUFSLENBQWhCO0FBQ0EsWUFBTThnQixhQUFhN2lCLFNBQVMwbEIsUUFBUXhqQixHQUFSLEVBQVQsRUFBd0IsRUFBeEIsQ0FBbkI7QUFDQWYsbUNBQWFnYSxJQUFiLENBQWtCQyw0QkFBa0JrSSx3QkFBcEMsRUFBOEQ7QUFDNURUO0FBRDRELFNBQTlEO0FBR0QsT0FQRDs7QUFTQTFoQixpQ0FBYVMsRUFBYixDQUFnQndaLDRCQUFrQmlJLG9CQUFsQyxFQUF3RCxVQUFDM1QsS0FBRCxFQUFXO0FBQ2pFLGVBQUt5SixvQkFBTCxDQUEwQm9JLFFBQTFCLENBQW1DN1IsTUFBTXdTLE9BQXpDO0FBQ0EsZUFBS3NDLHNCQUFMO0FBQ0EsZUFBS0Msb0JBQUw7QUFDQSxlQUFLQyxhQUFMO0FBQ0QsT0FMRDs7QUFPQXZqQixpQ0FBYVMsRUFBYixDQUFnQndaLDRCQUFrQmtJLHdCQUFsQyxFQUE0RCxVQUFDNVQsS0FBRCxFQUFXO0FBQ3JFO0FBQ0EsZUFBS3lKLG9CQUFMLENBQTBCd00sZ0JBQTFCLENBQTJDalcsTUFBTW1ULFVBQWpEOztBQUVBO0FBQ0ExaEIsbUNBQWFnYSxJQUFiLENBQWtCQyw0QkFBa0JpSSxvQkFBcEMsRUFBMEQ7QUFDeERuQixtQkFBUztBQUQrQyxTQUExRDs7QUFJQTtBQUNBNWdCLFVBQUU0USxJQUFGLENBQU87QUFDTCtJLGVBQUssT0FBS2pKLE1BQUwsQ0FBWXhNLFFBQVosQ0FBcUIsMkNBQXJCLENBREE7QUFFTDBWLGtCQUFRLE1BRkg7QUFHTGpaLGdCQUFNLEVBQUM0Z0IsWUFBWW5ULE1BQU1tVCxVQUFuQjtBQUhELFNBQVA7QUFLRCxPQWZEO0FBZ0JEOzs7c0NBRWlCO0FBQUE7O0FBQ2hCdmhCLFFBQUU0TywyQkFBaUIzQyxhQUFqQixDQUErQkUsT0FBL0IsQ0FBdUNHLGFBQXpDLEVBQXdEaE0sRUFBeEQsQ0FBMkQsT0FBM0QsRUFBb0UsWUFBTTtBQUN4RSxlQUFLdVgsb0JBQUwsQ0FBMEJ5TSxpQ0FBMUI7QUFDQSxlQUFLOUIsa0JBQUwsQ0FBd0IrQixpQkFBeEI7QUFDRCxPQUhEOztBQUtBdmtCLFFBQUU0TywyQkFBaUIzQyxhQUFqQixDQUErQkUsT0FBL0IsQ0FBdUNJLGNBQXpDLEVBQXlEak0sRUFBekQsQ0FBNEQsT0FBNUQsRUFBcUUsWUFBTTtBQUN6RSxlQUFLdVgsb0JBQUwsQ0FBMEJ5TSxpQ0FBMUI7QUFDQSxlQUFLOUIsa0JBQUwsQ0FBd0JnQyxrQkFBeEI7QUFDRCxPQUhEOztBQUtBeGtCLFFBQUU0TywyQkFBaUIzQyxhQUFqQixDQUErQkUsT0FBL0IsQ0FBdUNLLGFBQXpDLEVBQXdEbE0sRUFBeEQsQ0FBMkQsT0FBM0QsRUFBb0UsWUFBTTtBQUN4RSxlQUFLdVgsb0JBQUwsQ0FBMEJ5TSxpQ0FBMUI7QUFDQSxlQUFLOUIsa0JBQUwsQ0FBd0JpQyxpQkFBeEI7QUFDRCxPQUhEOztBQUtBemtCLFFBQUU0TywyQkFBaUIzQyxhQUFqQixDQUErQkUsT0FBL0IsQ0FBdUNDLEtBQXpDLEVBQWdEOUwsRUFBaEQsQ0FBbUQsT0FBbkQsRUFBNEQsWUFBTTtBQUNoRSxlQUFLdVgsb0JBQUwsQ0FBMEJtTCxrQ0FBMUI7QUFDQSxlQUFLUixrQkFBTCxDQUF3QmtDLFVBQXhCO0FBQ0QsT0FIRDtBQUlEOzs7NkNBRXdCO0FBQUE7O0FBQ3ZCMWtCLFFBQUU0TywyQkFBaUIzQyxhQUFqQixDQUErQkUsT0FBL0IsQ0FBdUNNLGNBQXpDLEVBQXlEbk0sRUFBekQsQ0FBNEQsT0FBNUQsRUFBcUUsWUFBTTtBQUN6RSxlQUFLdVgsb0JBQUwsQ0FBMEJ5TSxpQ0FBMUI7QUFDQSxlQUFLOUIsa0JBQUwsQ0FBd0JtQyxxQkFBeEI7QUFDRCxPQUhEO0FBSUQ7OztvQ0FFZTtBQUNkLGFBQU8za0IsRUFBRTRPLDJCQUFpQnhILHVCQUFuQixFQUE0Q3RHLElBQTVDLENBQWlELGNBQWpELEVBQWlFK04sR0FBakUsQ0FBcUUsQ0FBckUsQ0FBUDtBQUNEOzs7d0NBRW1COEIsTyxFQUFTO0FBQUE7O0FBQzNCM1EsUUFBRTRPLDJCQUFpQmQsaUNBQW5CLEVBQXNEcE0sSUFBdEQ7O0FBRUEsVUFBTThlLG1CQUFtQnhnQixFQUFFNE8sMkJBQWlCeEgsdUJBQW5CLENBQXpCO0FBQ0EsVUFBTXFaLGlCQUFpQkQsaUJBQWlCN2YsSUFBakIsQ0FBc0IsWUFBdEIsQ0FBdkI7QUFDQSxVQUFNaWtCLHFCQUFxQjVrQixFQUFFNE8sMkJBQWlCOUcsaUJBQW5CLEVBQXNDckwsTUFBakU7QUFDQSxVQUFNb29CLGNBQWNubUIsU0FBU3NCLEVBQUU0TywyQkFBaUJwSCw2QkFBbkIsRUFBa0Q0SixJQUFsRCxFQUFULEVBQW1FLEVBQW5FLENBQXBCOztBQUVBcFIsUUFBRTRRLElBQUYsQ0FBTyxLQUFLRixNQUFMLENBQVl4TSxRQUFaLENBQXFCLDJCQUFyQixFQUFrRCxFQUFDeU0sZ0JBQUQsRUFBbEQsQ0FBUCxFQUNHbVUsSUFESCxDQUNRLFVBQUNoVSxRQUFELEVBQWM7QUFDbEI7QUFDQTlRLFVBQUU0TywyQkFBaUIzSCxhQUFuQixFQUFrQ25HLElBQWxDLENBQXVDOE4sMkJBQWlCOUcsaUJBQXhELEVBQTJFOUYsTUFBM0U7QUFDQWhDLFVBQUU0TywyQkFBaUJ6Ryw4QkFBbkIsRUFBbURuRyxNQUFuRDs7QUFFQWhDLFVBQUs0TywyQkFBaUIzSCxhQUF0QixhQUE2Q2lMLE9BQTdDLENBQXFEcEIsUUFBckQ7O0FBRUE5USxVQUFFNE8sMkJBQWlCZCxpQ0FBbkIsRUFBc0Q4SCxJQUF0RDs7QUFFQSxZQUFNbVAsaUJBQWlCL2tCLEVBQUU0TywyQkFBaUI5RyxpQkFBbkIsRUFBc0NyTCxNQUE3RDtBQUNBLFlBQU11b0IsY0FBY2hxQixLQUFLMmxCLElBQUwsQ0FBVW9FLGlCQUFpQnRFLGNBQTNCLENBQXBCOztBQUVBLGVBQUs1SSxvQkFBTCxDQUEwQm9OLGlCQUExQixDQUE0Q0YsY0FBNUM7QUFDQSxlQUFLbE4sb0JBQUwsQ0FBMEIySix3QkFBMUI7O0FBRUEsWUFBSVosVUFBVSxDQUFkO0FBQ0EsWUFBSS9tQixVQUFVLEVBQWQ7O0FBRUE7QUFDQSxZQUFJK3FCLHFCQUFxQkcsY0FBekIsRUFBeUM7QUFBRTtBQUN6Q2xyQixvQkFBVytxQixxQkFBcUJHLGNBQXJCLEtBQXdDLENBQXpDLEdBQ05obEIsT0FBT21sQixxQkFBUCxDQUE2Qix1Q0FBN0IsQ0FETSxHQUVObmxCLE9BQU9tbEIscUJBQVAsQ0FBNkIseUNBQTdCLEVBQ0Nub0IsT0FERCxDQUNTLEtBRFQsRUFDaUI2bkIscUJBQXFCRyxjQUR0QyxDQUZKOztBQUtBO0FBQ0FuRSxvQkFBV29FLGdCQUFnQixDQUFqQixHQUFzQixDQUF0QixHQUEwQkgsV0FBcEM7QUFDRCxTQVJELE1BUU8sSUFBSUQscUJBQXFCRyxjQUF6QixFQUF5QztBQUFFO0FBQ2hEbHJCLG9CQUFXa3JCLGlCQUFpQkgsa0JBQWpCLEtBQXdDLENBQXpDLEdBQ043a0IsT0FBT21sQixxQkFBUCxDQUE2QixxQ0FBN0IsQ0FETSxHQUVObmxCLE9BQU9tbEIscUJBQVAsQ0FBNkIsdUNBQTdCLEVBQ0Nub0IsT0FERCxDQUNTLEtBRFQsRUFDaUJnb0IsaUJBQWlCSCxrQkFEbEMsQ0FGSjs7QUFLQTtBQUNBaEUsb0JBQVUsQ0FBVjtBQUNEOztBQUVELFlBQUkvbUIsWUFBWSxFQUFoQixFQUFvQjtBQUNsQm1HLFlBQUVvUyxLQUFGLENBQVErUyxNQUFSLENBQWU7QUFDYnBpQixtQkFBTyxFQURNO0FBRWJsSjtBQUZhLFdBQWY7QUFJRDs7QUFFRDtBQUNBZ0csbUNBQWFnYSxJQUFiLENBQWtCQyw0QkFBa0JpSSxvQkFBcEMsRUFBMEQ7QUFDeERuQjtBQUR3RCxTQUExRDs7QUFJQTtBQUNBLGVBQUt3QyxhQUFMO0FBQ0QsT0FwREgsRUFxREdnQyxJQXJESCxDQXFEUSxZQUFNO0FBQ1ZwbEIsVUFBRW9TLEtBQUYsQ0FBUUMsS0FBUixDQUFjO0FBQ1p0UCxpQkFBTyxFQURLO0FBRVpsSixtQkFBUztBQUZHLFNBQWQ7QUFJRCxPQTFESDtBQTJERDs7Ozs7a0JBdFVrQm9vQixhOzs7Ozs7Ozs7O0FDekNyQixrQkFBa0IsWUFBWSxtQkFBTyxDQUFDLHNGQUErQixzQjs7Ozs7Ozs7OztBQ0FyRSxrQkFBa0IsWUFBWSxtQkFBTyxDQUFDLDBGQUFpQyxzQjs7Ozs7Ozs7OztBQ0F2RSxrQkFBa0IsWUFBWSxtQkFBTyxDQUFDLHdGQUFnQyxzQjs7Ozs7Ozs7OztBQ0F0RSxrQkFBa0IsWUFBWSxtQkFBTyxDQUFDLDRGQUFrQyxzQjs7Ozs7Ozs7OztBQ0F4RSxrQkFBa0IsWUFBWSxtQkFBTyxDQUFDLDRGQUFrQyxzQjs7Ozs7Ozs7OztBQ0F4RSxrQkFBa0IsWUFBWSxtQkFBTyxDQUFDLDRGQUFrQyxzQjs7Ozs7Ozs7OztBQ0F4RSxrQkFBa0IsWUFBWSxtQkFBTyxDQUFDLDhHQUEyQyxzQjs7Ozs7Ozs7OztBQ0FqRixrQkFBa0IsWUFBWSxtQkFBTyxDQUFDLGdIQUE0QyxzQjs7Ozs7Ozs7OztBQ0FsRixrQkFBa0IsWUFBWSxtQkFBTyxDQUFDLHdGQUFnQyxzQjs7Ozs7Ozs7OztBQ0F0RSxrQkFBa0IsWUFBWSxtQkFBTyxDQUFDLGdIQUE0QyxzQjs7Ozs7Ozs7OztBQ0FsRixrQkFBa0IsWUFBWSxtQkFBTyxDQUFDLDRGQUFrQyxzQjs7Ozs7Ozs7OztBQ0F4RSxrQkFBa0IsWUFBWSxtQkFBTyxDQUFDLG9GQUEyQixzQjs7Ozs7Ozs7OztBQ0FqRSxrQkFBa0IsWUFBWSxtQkFBTyxDQUFDLGdHQUFvQyxzQjs7Ozs7Ozs7Ozs7QUNBN0Q7O0FBRWIsa0JBQWtCOztBQUVsQixlQUFlO0FBQ2Y7QUFDQTtBQUNBO0FBQ0EsRTs7Ozs7Ozs7Ozs7QUNSYTs7QUFFYixrQkFBa0I7O0FBRWxCLHNCQUFzQixtQkFBTyxDQUFDLHlHQUFtQzs7QUFFakU7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGLGVBQWU7QUFDZjtBQUNBLG1CQUFtQixrQkFBa0I7QUFDckM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUMsRzs7Ozs7Ozs7Ozs7QUMxQlk7O0FBRWIsa0JBQWtCOztBQUVsQixzQkFBc0IsbUJBQU8sQ0FBQywyR0FBb0M7O0FBRWxFOztBQUVBLGNBQWMsbUJBQU8sQ0FBQyx1RkFBMEI7O0FBRWhEOztBQUVBLGVBQWUsbUJBQU8sQ0FBQyx5RUFBbUI7O0FBRTFDOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixlQUFlO0FBQ2Y7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsRTs7Ozs7Ozs7Ozs7QUNoQ2E7O0FBRWIsa0JBQWtCOztBQUVsQixlQUFlLG1CQUFPLENBQUMseUVBQW1COztBQUUxQzs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0YsZUFBZTtBQUNmO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLEU7Ozs7Ozs7Ozs7O0FDaEJhOztBQUViLGtCQUFrQjs7QUFFbEIsbUJBQW1CLG1CQUFPLENBQUMsbUZBQXdCOztBQUVuRDs7QUFFQSxvQkFBb0IsbUJBQU8sQ0FBQyxxRkFBeUI7O0FBRXJEOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixlQUFlO0FBQ2Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLHdEQUF3RCwrQkFBK0I7QUFDdkY7O0FBRUE7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLENBQUMsRzs7Ozs7Ozs7Ozs7QUNsRFk7O0FBRWIsa0JBQWtCOztBQUVsQixZQUFZLG1CQUFPLENBQUMsaUZBQXVCOztBQUUzQzs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0YsZUFBZTtBQUNmO0FBQ0EsNkNBQTZDLGdCQUFnQjtBQUM3RDtBQUNBOztBQUVBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQSxFOzs7Ozs7Ozs7OztBQ3BCYTs7QUFFYixrQkFBa0I7O0FBRWxCLGdCQUFnQixtQkFBTyxDQUFDLDJGQUE0Qjs7QUFFcEQ7O0FBRUEsY0FBYyxtQkFBTyxDQUFDLHlFQUFtQjs7QUFFekM7O0FBRUEsaUhBQWlILG1CQUFtQixFQUFFLG1CQUFtQiw0SkFBNEo7O0FBRXJULHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixlQUFlO0FBQ2Y7QUFDQSxDQUFDO0FBQ0Q7QUFDQSxFOzs7Ozs7Ozs7O0FDcEJBLG1CQUFPLENBQUMsd0dBQW1DO0FBQzNDLG1CQUFPLENBQUMsOEZBQThCO0FBQ3RDLDZIQUEwRDs7Ozs7Ozs7Ozs7QUNGMUQsbUJBQU8sQ0FBQywrRkFBNkI7QUFDckMsbUJBQU8sQ0FBQyxxR0FBZ0M7QUFDeEMsdUlBQXdEOzs7Ozs7Ozs7OztBQ0Z4RCxtQkFBTyxDQUFDLCtGQUE2QjtBQUNyQyxtQkFBTyxDQUFDLHFHQUFnQztBQUN4QyxxSUFBdUQ7Ozs7Ozs7Ozs7O0FDRnZELG1CQUFPLENBQUMsb0dBQWlDO0FBQ3pDLCtIQUE0RDs7Ozs7Ozs7Ozs7QUNENUQsbUJBQU8sQ0FBQyxvR0FBaUM7QUFDekMsZ0lBQTZEOzs7Ozs7Ozs7OztBQ0Q3RCxtQkFBTyxDQUFDLG9HQUFpQztBQUN6QyxjQUFjLHdHQUFxQztBQUNuRDtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7O0FDSkEsbUJBQU8sQ0FBQyxzSEFBMEM7QUFDbEQsY0FBYyx3R0FBcUM7QUFDbkQ7QUFDQTtBQUNBOzs7Ozs7Ozs7OztBQ0pBLG1CQUFPLENBQUMsd0hBQTJDO0FBQ25ELHdJQUFxRTs7Ozs7Ozs7Ozs7QUNEckUsbUJBQU8sQ0FBQyxnR0FBK0I7QUFDdkMsOEhBQTJEOzs7Ozs7Ozs7OztBQ0QzRCxtQkFBTyxDQUFDLHdIQUEyQztBQUNuRCx3SUFBcUU7Ozs7Ozs7Ozs7O0FDRHJFLG1CQUFPLENBQUMsb0dBQWlDO0FBQ3pDLGdJQUE2RDs7Ozs7Ozs7Ozs7QUNEN0QsbUJBQU8sQ0FBQyxzRkFBMEI7QUFDbEMsbUJBQU8sQ0FBQywwR0FBb0M7QUFDNUMsbUJBQU8sQ0FBQyxvSEFBeUM7QUFDakQsbUJBQU8sQ0FBQyw0R0FBcUM7QUFDN0MseUhBQXNEOzs7Ozs7Ozs7OztBQ0p0RCxtQkFBTyxDQUFDLHdHQUFtQztBQUMzQyxtQkFBTyxDQUFDLGtHQUFnQztBQUN4QyxpQkFBaUIseUdBQW1DOzs7Ozs7Ozs7OztBQ0ZwRDtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNIQSw4QkFBOEI7Ozs7Ozs7Ozs7O0FDQTlCLGVBQWUsbUJBQU8sQ0FBQywwRUFBYztBQUNyQztBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNKQTtBQUNBO0FBQ0EsZ0JBQWdCLG1CQUFPLENBQUMsNEVBQWU7QUFDdkMsZUFBZSxtQkFBTyxDQUFDLDBFQUFjO0FBQ3JDLHNCQUFzQixtQkFBTyxDQUFDLDBGQUFzQjtBQUNwRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUssWUFBWSxlQUFlO0FBQ2hDO0FBQ0EsS0FBSztBQUNMO0FBQ0E7Ozs7Ozs7Ozs7O0FDdEJBO0FBQ0EsVUFBVSxtQkFBTyxDQUFDLDhEQUFRO0FBQzFCLFVBQVUsbUJBQU8sQ0FBQyw4REFBUTtBQUMxQjtBQUNBLDJCQUEyQixrQkFBa0IsRUFBRTs7QUFFL0M7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHLFlBQVk7QUFDZjs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUN0QkEsaUJBQWlCOztBQUVqQjtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7O0FDSkEsNkJBQTZCO0FBQzdCLHVDQUF1Qzs7Ozs7Ozs7Ozs7O0FDRDFCO0FBQ2Isc0JBQXNCLG1CQUFPLENBQUMsMEVBQWM7QUFDNUMsaUJBQWlCLG1CQUFPLENBQUMsa0ZBQWtCOztBQUUzQztBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNQQTtBQUNBLGdCQUFnQixtQkFBTyxDQUFDLDRFQUFlO0FBQ3ZDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNuQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNKQTtBQUNBLGtCQUFrQixtQkFBTyxDQUFDLGtFQUFVO0FBQ3BDLGlDQUFpQyxRQUFRLG1CQUFtQixVQUFVLEVBQUUsRUFBRTtBQUMxRSxDQUFDOzs7Ozs7Ozs7OztBQ0hELGVBQWUsbUJBQU8sQ0FBQywwRUFBYztBQUNyQyxlQUFlLGtHQUE2QjtBQUM1QztBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7OztBQ05BO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7OztBQ0hBO0FBQ0EsY0FBYyxtQkFBTyxDQUFDLDhFQUFnQjtBQUN0QyxXQUFXLG1CQUFPLENBQUMsOEVBQWdCO0FBQ25DLFVBQVUsbUJBQU8sQ0FBQyw0RUFBZTtBQUNqQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7Ozs7Ozs7Ozs7O0FDZEEsYUFBYSxtQkFBTyxDQUFDLG9FQUFXO0FBQ2hDLFdBQVcsbUJBQU8sQ0FBQyxnRUFBUztBQUM1QixVQUFVLG1CQUFPLENBQUMsOERBQVE7QUFDMUIsV0FBVyxtQkFBTyxDQUFDLGdFQUFTO0FBQzVCLFVBQVUsbUJBQU8sQ0FBQyw4REFBUTtBQUMxQjs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlFQUFpRTtBQUNqRTtBQUNBLGtGQUFrRjtBQUNsRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVztBQUNYLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsK0NBQStDO0FBQy9DO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGNBQWM7QUFDZCxjQUFjO0FBQ2QsY0FBYztBQUNkLGNBQWM7QUFDZCxlQUFlO0FBQ2YsZUFBZTtBQUNmLGVBQWU7QUFDZixnQkFBZ0I7QUFDaEI7Ozs7Ozs7Ozs7O0FDN0RBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7O0FDTkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHlDQUF5Qzs7Ozs7Ozs7Ozs7QUNMekMsdUJBQXVCO0FBQ3ZCO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNIQSxTQUFTLG1CQUFPLENBQUMsMEVBQWM7QUFDL0IsaUJBQWlCLG1CQUFPLENBQUMsa0ZBQWtCO0FBQzNDLGlCQUFpQixtQkFBTyxDQUFDLDhFQUFnQjtBQUN6QztBQUNBLENBQUM7QUFDRDtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7O0FDUEEsZUFBZSxrR0FBNkI7QUFDNUM7Ozs7Ozs7Ozs7O0FDREEsa0JBQWtCLG1CQUFPLENBQUMsOEVBQWdCLE1BQU0sbUJBQU8sQ0FBQyxrRUFBVTtBQUNsRSwrQkFBK0IsbUJBQU8sQ0FBQyw0RUFBZSxnQkFBZ0IsbUJBQW1CLFVBQVUsRUFBRSxFQUFFO0FBQ3ZHLENBQUM7Ozs7Ozs7Ozs7O0FDRkQ7QUFDQSxVQUFVLG1CQUFPLENBQUMsOERBQVE7QUFDMUI7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7O0FDTEE7QUFDQSxnQkFBZ0IsbUJBQU8sQ0FBQywwRUFBYztBQUN0QyxlQUFlLG1CQUFPLENBQUMsOERBQVE7QUFDL0I7O0FBRUE7QUFDQTtBQUNBOzs7Ozs7Ozs7OztBQ1BBO0FBQ0EsVUFBVSxtQkFBTyxDQUFDLDhEQUFRO0FBQzFCO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNKQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7O0FDRkE7QUFDQSxlQUFlLG1CQUFPLENBQUMsMEVBQWM7QUFDckM7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7O0FDWGE7QUFDYixhQUFhLG1CQUFPLENBQUMsa0ZBQWtCO0FBQ3ZDLGlCQUFpQixtQkFBTyxDQUFDLGtGQUFrQjtBQUMzQyxxQkFBcUIsbUJBQU8sQ0FBQywwRkFBc0I7QUFDbkQ7O0FBRUE7QUFDQSxtQkFBTyxDQUFDLGdFQUFTLHFCQUFxQixtQkFBTyxDQUFDLDhEQUFRLDRCQUE0QixhQUFhLEVBQUU7O0FBRWpHO0FBQ0EscURBQXFELDRCQUE0QjtBQUNqRjtBQUNBOzs7Ozs7Ozs7Ozs7QUNaYTtBQUNiLGNBQWMsbUJBQU8sQ0FBQyxzRUFBWTtBQUNsQyxjQUFjLG1CQUFPLENBQUMsb0VBQVc7QUFDakMsZUFBZSxtQkFBTyxDQUFDLHdFQUFhO0FBQ3BDLFdBQVcsbUJBQU8sQ0FBQyxnRUFBUztBQUM1QixnQkFBZ0IsbUJBQU8sQ0FBQywwRUFBYztBQUN0QyxrQkFBa0IsbUJBQU8sQ0FBQyw4RUFBZ0I7QUFDMUMscUJBQXFCLG1CQUFPLENBQUMsMEZBQXNCO0FBQ25ELHFCQUFxQixtQkFBTyxDQUFDLDRFQUFlO0FBQzVDLGVBQWUsbUJBQU8sQ0FBQyw4REFBUTtBQUMvQiw4Q0FBOEM7QUFDOUM7QUFDQTtBQUNBOztBQUVBLDhCQUE4QixhQUFhOztBQUUzQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EseUNBQXlDLG9DQUFvQztBQUM3RSw2Q0FBNkMsb0NBQW9DO0FBQ2pGLEtBQUssNEJBQTRCLG9DQUFvQztBQUNyRTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZ0JBQWdCLG1CQUFtQjtBQUNuQztBQUNBO0FBQ0Esa0NBQWtDLDJCQUEyQjtBQUM3RDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7O0FDcEVBLGVBQWUsbUJBQU8sQ0FBQyw4REFBUTtBQUMvQjs7QUFFQTtBQUNBO0FBQ0EsaUNBQWlDLHFCQUFxQjtBQUN0RDtBQUNBLGlDQUFpQyxTQUFTLEVBQUU7QUFDNUMsQ0FBQyxZQUFZOztBQUViO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDZCQUE2QixTQUFTLHFCQUFxQjtBQUMzRCxpQ0FBaUMsYUFBYTtBQUM5QztBQUNBLEdBQUcsWUFBWTtBQUNmO0FBQ0E7Ozs7Ozs7Ozs7O0FDckJBO0FBQ0EsVUFBVTtBQUNWOzs7Ozs7Ozs7OztBQ0ZBOzs7Ozs7Ozs7OztBQ0FBOzs7Ozs7Ozs7OztBQ0FBLFdBQVcsbUJBQU8sQ0FBQyw4REFBUTtBQUMzQixlQUFlLG1CQUFPLENBQUMsMEVBQWM7QUFDckMsVUFBVSxtQkFBTyxDQUFDLDhEQUFRO0FBQzFCLGNBQWMsaUdBQXlCO0FBQ3ZDO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsY0FBYyxtQkFBTyxDQUFDLGtFQUFVO0FBQ2hDLGlEQUFpRDtBQUNqRCxDQUFDO0FBQ0Q7QUFDQSxxQkFBcUI7QUFDckI7QUFDQSxTQUFTO0FBQ1QsR0FBRyxFQUFFO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7QUNwRGE7QUFDYjtBQUNBLGtCQUFrQixtQkFBTyxDQUFDLDhFQUFnQjtBQUMxQyxjQUFjLG1CQUFPLENBQUMsOEVBQWdCO0FBQ3RDLFdBQVcsbUJBQU8sQ0FBQyw4RUFBZ0I7QUFDbkMsVUFBVSxtQkFBTyxDQUFDLDRFQUFlO0FBQ2pDLGVBQWUsbUJBQU8sQ0FBQywwRUFBYztBQUNyQyxjQUFjLG1CQUFPLENBQUMsc0VBQVk7QUFDbEM7O0FBRUE7QUFDQSw2QkFBNkIsbUJBQU8sQ0FBQyxrRUFBVTtBQUMvQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxvQ0FBb0MsVUFBVSxFQUFFO0FBQ2hELG1CQUFtQixzQ0FBc0M7QUFDekQsQ0FBQyxxQ0FBcUM7QUFDdEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNILENBQUM7Ozs7Ozs7Ozs7O0FDckNEO0FBQ0EsZUFBZSxtQkFBTyxDQUFDLDBFQUFjO0FBQ3JDLFVBQVUsbUJBQU8sQ0FBQyw0RUFBZTtBQUNqQyxrQkFBa0IsbUJBQU8sQ0FBQyxrRkFBa0I7QUFDNUMsZUFBZSxtQkFBTyxDQUFDLDRFQUFlO0FBQ3RDLHlCQUF5QjtBQUN6Qjs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxlQUFlLG1CQUFPLENBQUMsNEVBQWU7QUFDdEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEVBQUUsaUdBQThCO0FBQ2hDLDZCQUE2QjtBQUM3QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7Ozs7Ozs7Ozs7O0FDeENBLGVBQWUsbUJBQU8sQ0FBQywwRUFBYztBQUNyQyxxQkFBcUIsbUJBQU8sQ0FBQyxvRkFBbUI7QUFDaEQsa0JBQWtCLG1CQUFPLENBQUMsZ0ZBQWlCO0FBQzNDOztBQUVBLFNBQVMsR0FBRyxtQkFBTyxDQUFDLDhFQUFnQjtBQUNwQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRyxZQUFZO0FBQ2Y7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7O0FDZkEsU0FBUyxtQkFBTyxDQUFDLDBFQUFjO0FBQy9CLGVBQWUsbUJBQU8sQ0FBQywwRUFBYztBQUNyQyxjQUFjLG1CQUFPLENBQUMsOEVBQWdCOztBQUV0QyxpQkFBaUIsbUJBQU8sQ0FBQyw4RUFBZ0I7QUFDekM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNaQSxVQUFVLG1CQUFPLENBQUMsNEVBQWU7QUFDakMsaUJBQWlCLG1CQUFPLENBQUMsa0ZBQWtCO0FBQzNDLGdCQUFnQixtQkFBTyxDQUFDLDRFQUFlO0FBQ3ZDLGtCQUFrQixtQkFBTyxDQUFDLGdGQUFpQjtBQUMzQyxVQUFVLG1CQUFPLENBQUMsOERBQVE7QUFDMUIscUJBQXFCLG1CQUFPLENBQUMsb0ZBQW1CO0FBQ2hEOztBQUVBLFNBQVMsR0FBRyxtQkFBTyxDQUFDLDhFQUFnQjtBQUNwQztBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUcsWUFBWTtBQUNmO0FBQ0E7Ozs7Ozs7Ozs7O0FDZkE7QUFDQSxnQkFBZ0IsbUJBQU8sQ0FBQyw0RUFBZTtBQUN2QyxXQUFXLHFHQUEyQjtBQUN0QyxpQkFBaUI7O0FBRWpCO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTs7QUFFQSxnQkFBZ0I7QUFDaEI7QUFDQTs7Ozs7Ozs7Ozs7QUNsQkE7QUFDQSxZQUFZLG1CQUFPLENBQUMsZ0dBQXlCO0FBQzdDLGlCQUFpQiw4R0FBa0M7O0FBRW5ELFNBQVM7QUFDVDtBQUNBOzs7Ozs7Ozs7OztBQ05BLFNBQVM7Ozs7Ozs7Ozs7O0FDQVQ7QUFDQSxVQUFVLG1CQUFPLENBQUMsOERBQVE7QUFDMUIsZUFBZSxtQkFBTyxDQUFDLDBFQUFjO0FBQ3JDLGVBQWUsbUJBQU8sQ0FBQyw0RUFBZTtBQUN0Qzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIOzs7Ozs7Ozs7OztBQ1pBLFVBQVUsbUJBQU8sQ0FBQyw4REFBUTtBQUMxQixnQkFBZ0IsbUJBQU8sQ0FBQyw0RUFBZTtBQUN2QyxtQkFBbUIsbUJBQU8sQ0FBQyxvRkFBbUI7QUFDOUMsZUFBZSxtQkFBTyxDQUFDLDRFQUFlOztBQUV0QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7O0FDaEJBO0FBQ0EsWUFBWSxtQkFBTyxDQUFDLGdHQUF5QjtBQUM3QyxrQkFBa0IsbUJBQU8sQ0FBQyxrRkFBa0I7O0FBRTVDO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNOQSxTQUFTLEtBQUs7Ozs7Ozs7Ozs7O0FDQWQ7QUFDQSxjQUFjLG1CQUFPLENBQUMsb0VBQVc7QUFDakMsV0FBVyxtQkFBTyxDQUFDLGdFQUFTO0FBQzVCLFlBQVksbUJBQU8sQ0FBQyxrRUFBVTtBQUM5QjtBQUNBLDZCQUE2QjtBQUM3QjtBQUNBO0FBQ0EscURBQXFELE9BQU8sRUFBRTtBQUM5RDs7Ozs7Ozs7Ozs7QUNUQSxrQkFBa0IsbUJBQU8sQ0FBQyw4RUFBZ0I7QUFDMUMsY0FBYyxtQkFBTyxDQUFDLDhFQUFnQjtBQUN0QyxnQkFBZ0IsbUJBQU8sQ0FBQyw0RUFBZTtBQUN2QyxhQUFhLG1HQUEwQjtBQUN2QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7OztBQ3BCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7OztBQ1BBLHNHQUFtQzs7Ozs7Ozs7Ozs7QUNBbkM7QUFDQTtBQUNBLGVBQWUsbUJBQU8sQ0FBQywwRUFBYztBQUNyQyxlQUFlLG1CQUFPLENBQUMsMEVBQWM7QUFDckM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGtEQUFrRDtBQUNsRDtBQUNBO0FBQ0EsY0FBYyxtQkFBTyxDQUFDLDhEQUFRLGlCQUFpQixxR0FBMkI7QUFDMUU7QUFDQTtBQUNBLE9BQU8sWUFBWSxjQUFjO0FBQ2pDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUssR0FBRztBQUNSO0FBQ0E7Ozs7Ozs7Ozs7O0FDeEJBLFVBQVUsaUdBQXlCO0FBQ25DLFVBQVUsbUJBQU8sQ0FBQyw4REFBUTtBQUMxQixVQUFVLG1CQUFPLENBQUMsOERBQVE7O0FBRTFCO0FBQ0Esb0VBQW9FLGlDQUFpQztBQUNyRzs7Ozs7Ozs7Ozs7QUNOQSxhQUFhLG1CQUFPLENBQUMsb0VBQVc7QUFDaEMsVUFBVSxtQkFBTyxDQUFDLDhEQUFRO0FBQzFCO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNKQSxXQUFXLG1CQUFPLENBQUMsZ0VBQVM7QUFDNUIsYUFBYSxtQkFBTyxDQUFDLG9FQUFXO0FBQ2hDO0FBQ0Esa0RBQWtEOztBQUVsRDtBQUNBLHFFQUFxRTtBQUNyRSxDQUFDO0FBQ0Q7QUFDQSxRQUFRLG1CQUFPLENBQUMsc0VBQVk7QUFDNUI7QUFDQSxDQUFDOzs7Ozs7Ozs7OztBQ1hELGdCQUFnQixtQkFBTyxDQUFDLDRFQUFlO0FBQ3ZDLGNBQWMsbUJBQU8sQ0FBQyxzRUFBWTtBQUNsQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7O0FDaEJBLGdCQUFnQixtQkFBTyxDQUFDLDRFQUFlO0FBQ3ZDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNOQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7O0FDTEE7QUFDQSxjQUFjLG1CQUFPLENBQUMsc0VBQVk7QUFDbEMsY0FBYyxtQkFBTyxDQUFDLHNFQUFZO0FBQ2xDO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNMQTtBQUNBLGdCQUFnQixtQkFBTyxDQUFDLDRFQUFlO0FBQ3ZDO0FBQ0E7QUFDQSwyREFBMkQ7QUFDM0Q7Ozs7Ozs7Ozs7O0FDTEE7QUFDQSxjQUFjLG1CQUFPLENBQUMsc0VBQVk7QUFDbEM7QUFDQTtBQUNBOzs7Ozs7Ozs7OztBQ0pBO0FBQ0EsZUFBZSxtQkFBTyxDQUFDLDBFQUFjO0FBQ3JDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7OztBQ1hBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7O0FDSkEsYUFBYSxtQkFBTyxDQUFDLG9FQUFXO0FBQ2hDLFdBQVcsbUJBQU8sQ0FBQyxnRUFBUztBQUM1QixjQUFjLG1CQUFPLENBQUMsc0VBQVk7QUFDbEMsYUFBYSxtQkFBTyxDQUFDLHNFQUFZO0FBQ2pDLHFCQUFxQixpR0FBeUI7QUFDOUM7QUFDQSwwREFBMEQsc0JBQXNCO0FBQ2hGLGtGQUFrRix3QkFBd0I7QUFDMUc7Ozs7Ozs7Ozs7O0FDUkEsK0ZBQTZCOzs7Ozs7Ozs7OztBQ0E3QixZQUFZLG1CQUFPLENBQUMsb0VBQVc7QUFDL0IsVUFBVSxtQkFBTyxDQUFDLDhEQUFRO0FBQzFCLGFBQWEsZ0dBQTJCO0FBQ3hDOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOzs7Ozs7Ozs7OztBQ1ZBLGNBQWMsbUJBQU8sQ0FBQyxzRUFBWTtBQUNsQyxlQUFlLG1CQUFPLENBQUMsOERBQVE7QUFDL0IsZ0JBQWdCLG1CQUFPLENBQUMsMEVBQWM7QUFDdEMsaUJBQWlCLHVHQUFvQztBQUNyRDtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNQQSxlQUFlLG1CQUFPLENBQUMsMEVBQWM7QUFDckMsVUFBVSxtQkFBTyxDQUFDLHNHQUE0QjtBQUM5QyxpQkFBaUIsaUdBQThCO0FBQy9DO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7OztBQ05BLGNBQWMsbUJBQU8sQ0FBQyxzRUFBWTtBQUNsQyxlQUFlLG1CQUFPLENBQUMsOERBQVE7QUFDL0IsZ0JBQWdCLG1CQUFPLENBQUMsMEVBQWM7QUFDdEMsaUJBQWlCLGdHQUE2QjtBQUM5QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7OztBQ1RhO0FBQ2IsVUFBVSxtQkFBTyxDQUFDLDhEQUFRO0FBQzFCLGNBQWMsbUJBQU8sQ0FBQyxvRUFBVztBQUNqQyxlQUFlLG1CQUFPLENBQUMsMEVBQWM7QUFDckMsV0FBVyxtQkFBTyxDQUFDLDBFQUFjO0FBQ2pDLGtCQUFrQixtQkFBTyxDQUFDLGtGQUFrQjtBQUM1QyxlQUFlLG1CQUFPLENBQUMsMEVBQWM7QUFDckMscUJBQXFCLG1CQUFPLENBQUMsc0ZBQW9CO0FBQ2pELGdCQUFnQixtQkFBTyxDQUFDLHNHQUE0Qjs7QUFFcEQsaUNBQWlDLG1CQUFPLENBQUMsOEVBQWdCLG1CQUFtQixrQkFBa0IsRUFBRTtBQUNoRztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHVEQUF1RCxnQ0FBZ0M7QUFDdkY7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBLGtDQUFrQyxnQkFBZ0I7QUFDbEQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7Ozs7Ozs7Ozs7O0FDcENZO0FBQ2IsdUJBQXVCLG1CQUFPLENBQUMsNEZBQXVCO0FBQ3RELFdBQVcsbUJBQU8sQ0FBQywwRUFBYztBQUNqQyxnQkFBZ0IsbUJBQU8sQ0FBQywwRUFBYztBQUN0QyxnQkFBZ0IsbUJBQU8sQ0FBQyw0RUFBZTs7QUFFdkM7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUIsbUJBQU8sQ0FBQyw4RUFBZ0I7QUFDekMsZ0NBQWdDO0FBQ2hDLGNBQWM7QUFDZCxpQkFBaUI7QUFDakI7QUFDQSxDQUFDO0FBQ0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDOztBQUVEO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOzs7Ozs7Ozs7OztBQ2pDQTtBQUNBLGNBQWMsbUJBQU8sQ0FBQyxvRUFBVzs7QUFFakM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUM7Ozs7Ozs7Ozs7O0FDUkQ7QUFDQSxjQUFjLG1CQUFPLENBQUMsb0VBQVc7O0FBRWpDLDBDQUEwQyxTQUFTLG1CQUFPLENBQUMsa0ZBQWtCLEdBQUc7Ozs7Ozs7Ozs7O0FDSGhGLGNBQWMsbUJBQU8sQ0FBQyxvRUFBVztBQUNqQztBQUNBLDhCQUE4QixTQUFTLG1CQUFPLENBQUMsa0ZBQWtCLEdBQUc7Ozs7Ozs7Ozs7O0FDRnBFLGNBQWMsbUJBQU8sQ0FBQyxvRUFBVztBQUNqQztBQUNBLGlDQUFpQyxtQkFBTyxDQUFDLDhFQUFnQixjQUFjLGlCQUFpQixpR0FBeUIsRUFBRTs7Ozs7Ozs7Ozs7QUNGbkg7QUFDQSxlQUFlLG1CQUFPLENBQUMsMEVBQWM7QUFDckMsc0JBQXNCLG1CQUFPLENBQUMsNEVBQWU7O0FBRTdDLG1CQUFPLENBQUMsNEVBQWU7QUFDdkI7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7Ozs7Ozs7Ozs7QUNSRDtBQUNBLGVBQWUsbUJBQU8sQ0FBQywwRUFBYztBQUNyQyxZQUFZLG1CQUFPLENBQUMsOEVBQWdCOztBQUVwQyxtQkFBTyxDQUFDLDRFQUFlO0FBQ3ZCO0FBQ0E7QUFDQTtBQUNBLENBQUM7Ozs7Ozs7Ozs7O0FDUkQ7QUFDQSxjQUFjLG1CQUFPLENBQUMsb0VBQVc7QUFDakMsOEJBQThCLGlCQUFpQixtR0FBMkIsRUFBRTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ0YvRDtBQUNiLFVBQVUsbUJBQU8sQ0FBQywwRUFBYzs7QUFFaEM7QUFDQSxtQkFBTyxDQUFDLDhFQUFnQjtBQUN4Qiw2QkFBNkI7QUFDN0IsY0FBYztBQUNkO0FBQ0EsQ0FBQztBQUNEO0FBQ0E7QUFDQTtBQUNBLGlDQUFpQztBQUNqQztBQUNBO0FBQ0EsVUFBVTtBQUNWLENBQUM7Ozs7Ozs7Ozs7OztBQ2hCWTtBQUNiO0FBQ0EsYUFBYSxtQkFBTyxDQUFDLG9FQUFXO0FBQ2hDLFVBQVUsbUJBQU8sQ0FBQyw4REFBUTtBQUMxQixrQkFBa0IsbUJBQU8sQ0FBQyw4RUFBZ0I7QUFDMUMsY0FBYyxtQkFBTyxDQUFDLG9FQUFXO0FBQ2pDLGVBQWUsbUJBQU8sQ0FBQyx3RUFBYTtBQUNwQyxXQUFXLHlGQUFzQjtBQUNqQyxhQUFhLG1CQUFPLENBQUMsa0VBQVU7QUFDL0IsYUFBYSxtQkFBTyxDQUFDLG9FQUFXO0FBQ2hDLHFCQUFxQixtQkFBTyxDQUFDLDBGQUFzQjtBQUNuRCxVQUFVLG1CQUFPLENBQUMsOERBQVE7QUFDMUIsVUFBVSxtQkFBTyxDQUFDLDhEQUFRO0FBQzFCLGFBQWEsbUJBQU8sQ0FBQyxzRUFBWTtBQUNqQyxnQkFBZ0IsbUJBQU8sQ0FBQyw0RUFBZTtBQUN2QyxlQUFlLG1CQUFPLENBQUMsMEVBQWM7QUFDckMsY0FBYyxtQkFBTyxDQUFDLHdFQUFhO0FBQ25DLGVBQWUsbUJBQU8sQ0FBQywwRUFBYztBQUNyQyxlQUFlLG1CQUFPLENBQUMsMEVBQWM7QUFDckMsZUFBZSxtQkFBTyxDQUFDLDBFQUFjO0FBQ3JDLGdCQUFnQixtQkFBTyxDQUFDLDRFQUFlO0FBQ3ZDLGtCQUFrQixtQkFBTyxDQUFDLGdGQUFpQjtBQUMzQyxpQkFBaUIsbUJBQU8sQ0FBQyxrRkFBa0I7QUFDM0MsY0FBYyxtQkFBTyxDQUFDLGtGQUFrQjtBQUN4QyxjQUFjLG1CQUFPLENBQUMsc0ZBQW9CO0FBQzFDLFlBQVksbUJBQU8sQ0FBQyw4RUFBZ0I7QUFDcEMsWUFBWSxtQkFBTyxDQUFDLDhFQUFnQjtBQUNwQyxVQUFVLG1CQUFPLENBQUMsMEVBQWM7QUFDaEMsWUFBWSxtQkFBTyxDQUFDLDhFQUFnQjtBQUNwQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlO0FBQ2Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0Esc0JBQXNCO0FBQ3RCLHNCQUFzQix1QkFBdUIsV0FBVyxJQUFJO0FBQzVELEdBQUc7QUFDSCxDQUFDO0FBQ0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDOztBQUVEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLENBQUM7QUFDRDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsMkRBQTJEO0FBQzNEO0FBQ0EsS0FBSztBQUNMO0FBQ0Esc0JBQXNCLG1DQUFtQztBQUN6RCxLQUFLO0FBQ0wsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxnRUFBZ0UsZ0NBQWdDO0FBQ2hHO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRzs7QUFFSDtBQUNBO0FBQ0EsRUFBRSxxR0FBMkI7QUFDN0IsRUFBRSxtR0FBMEI7QUFDNUI7O0FBRUEsc0JBQXNCLG1CQUFPLENBQUMsc0VBQVk7QUFDMUM7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSwwREFBMEQsa0JBQWtCOztBQUU1RTtBQUNBO0FBQ0E7QUFDQSxvQkFBb0IsdUJBQXVCOztBQUUzQyxvREFBb0QsNkJBQTZCOztBQUVqRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0gsMEJBQTBCLGVBQWUsRUFBRTtBQUMzQywwQkFBMEIsZ0JBQWdCO0FBQzFDLENBQUM7O0FBRUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDOztBQUVEO0FBQ0E7QUFDQSw4Q0FBOEMsWUFBWSxFQUFFOztBQUU1RDtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUM7O0FBRUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esb0RBQW9ELE9BQU8sUUFBUSxpQ0FBaUM7QUFDcEcsQ0FBQztBQUNEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHdFQUF3RTtBQUN4RTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUM7O0FBRUQ7QUFDQSxvQ0FBb0MsbUJBQU8sQ0FBQyxnRUFBUztBQUNyRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7O0FDclBBO0FBQ0EsY0FBYyxtQkFBTyxDQUFDLG9FQUFXO0FBQ2pDLGNBQWMsbUJBQU8sQ0FBQyxzRkFBb0I7O0FBRTFDO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7Ozs7Ozs7Ozs7QUNSRCxtQkFBTyxDQUFDLDRFQUFlOzs7Ozs7Ozs7OztBQ0F2QixtQkFBTyxDQUFDLDRFQUFlOzs7Ozs7Ozs7OztBQ0F2QixtQkFBTyxDQUFDLDBGQUFzQjtBQUM5QixhQUFhLG1CQUFPLENBQUMsb0VBQVc7QUFDaEMsV0FBVyxtQkFBTyxDQUFDLGdFQUFTO0FBQzVCLGdCQUFnQixtQkFBTyxDQUFDLDBFQUFjO0FBQ3RDLG9CQUFvQixtQkFBTyxDQUFDLDhEQUFROztBQUVwQztBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLGVBQWUseUJBQXlCO0FBQ3hDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7O0FDbEJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRWE7O0FBRWI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLENBQUM7QUFDRDtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUM7QUFDRDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsaUJBQWlCLHNCQUFzQjtBQUN2Qzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlO0FBQ2Y7QUFDQTtBQUNBO0FBQ0E7QUFDQSxjQUFjO0FBQ2Q7O0FBRUE7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQSxtQkFBbUIsU0FBUztBQUM1QjtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBLEtBQUs7QUFDTDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0EsZUFBZTtBQUNmO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQOztBQUVBLGlDQUFpQyxRQUFRO0FBQ3pDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtQkFBbUIsaUJBQWlCO0FBQ3BDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0Esc0NBQXNDLFFBQVE7QUFDOUM7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLGlCQUFpQixPQUFPO0FBQ3hCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLFFBQVEseUJBQXlCO0FBQ2pDO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsaUJBQWlCLGdCQUFnQjtBQUNqQztBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7O0FDN2JhLHdDQUF3QyxjQUFjLG1CQUFtQix5RkFBeUYsU0FBUyxpRkFBaUYsZ0JBQWdCLGFBQWEscUdBQXFHLDhCQUE4Qiw4RUFBOEUseUJBQXlCLFdBQVcsbURBQW1ELHNCQUFzQiwyQkFBMkIsdUJBQXVCLDZCQUE2Qiw0QkFBNEIsNEJBQTRCLGlDQUFpQyw0QkFBNEIsMEJBQTBCLDRCQUE0QiwwQkFBMEIsMkJBQTJCLCtCQUErQiwwQkFBMEIsd0JBQXdCLHlCQUF5Qiw2QkFBNkIsdUNBQXVDLHlCQUF5QiwyQ0FBMkMsb0hBQW9ILCtGQUErRiw4Q0FBOEMsU0FBUywyQkFBMkIsZ0NBQWdDLGtEQUFrRCxpRkFBaUYsMEJBQTBCLCtCQUErQiwyQkFBMkIsY0FBYywrQkFBK0Isc0NBQXNDLDRDQUE0QyxzQkFBc0IscUJBQXFCLFFBQVEsb0JBQW9CLHFDQUFxQyxNQUFNLFNBQVMsaUNBQWlDLDZCQUE2QixLQUFLLFlBQVksd0VBQXdFLDZCQUE2QixXQUFXLGdEQUFnRCx3Q0FBd0MsS0FBSyx1QkFBdUIsT0FBTywrREFBK0Qsd0RBQXdELE1BQU0sa0VBQWtFLHVGQUF1RixzUEFBc1AseUJBQXlCLFFBQVEsc0dBQXNHLG1DQUFtQyxvQ0FBb0MsMENBQTBDLFNBQVMsMEJBQTBCLDJIQUEySCxzQkFBc0IsMENBQTBDLDJCOzs7Ozs7Ozs7O0FDQXZyRztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG9DQUFvQztBQUNwQzs7QUFFQTtBQUNBLHdCQUF3QixxQkFBTSxnQkFBZ0IscUJBQU0sSUFBSSxxQkFBTSxzQkFBc0IscUJBQU07O0FBRTFGO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVyxFQUFFO0FBQ2IsYUFBYSxPQUFPO0FBQ3BCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVyxFQUFFO0FBQ2IsYUFBYSxRQUFRO0FBQ3JCO0FBQ0E7QUFDQSxvQkFBb0I7QUFDcEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsRUFBRTtBQUNiLGFBQWEsUUFBUTtBQUNyQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsRUFBRTtBQUNiLGFBQWEsT0FBTztBQUNwQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSw4QkFBOEIsS0FBSztBQUNuQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVyxPQUFPO0FBQ2xCLGFBQWEsT0FBTztBQUNwQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOzs7Ozs7O1VDcktBO1VBQ0E7O1VBRUE7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7O1VBRUE7VUFDQTs7VUFFQTtVQUNBO1VBQ0E7Ozs7O1dDdEJBO1dBQ0E7V0FDQTtXQUNBO1dBQ0EsRUFBRTtXQUNGO1dBQ0E7V0FDQSxDQUFDLEk7Ozs7Ozs7Ozs7Ozs7QUNrQkQ7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7Ozs7O0FBaENBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O2NBa0NZbGlCLE07SUFBTEMsQyxXQUFBQSxDOzs7QUFFUEEsRUFBRSxZQUFNO0FBQ04sTUFBTXFsQix1QkFBdUIsUUFBN0I7QUFDQSxNQUFNQyx3QkFBd0IsU0FBOUI7QUFDQSxNQUFNQyw4QkFBOEIsZUFBcEM7O0FBRUEsTUFBSWhWLDhCQUFKO0FBQ0EsTUFBSXRRLCtCQUFKO0FBQ0EsTUFBTXVsQixnQkFBZ0IsSUFBSXZELHVCQUFKLEVBQXRCO0FBQ0EsTUFBTXdELHVCQUF1QixJQUFJdFEscUNBQUosQ0FBNkJuVixFQUFFNE8sMkJBQWlCakcsa0JBQW5CLENBQTdCLENBQTdCO0FBQ0EsTUFBTStjLFdBQVcsSUFBSTlPLHlCQUFKLEVBQWpCOztBQUVBNE8sZ0JBQWNHLG9CQUFkO0FBQ0FILGdCQUFjdEMsc0JBQWQ7QUFDQXNDLGdCQUFjckMsb0JBQWQ7QUFDQXFDLGdCQUFjSSxtQkFBZDtBQUNBSixnQkFBY0ssMEJBQWQ7QUFDQUwsZ0JBQWNNLGVBQWQ7QUFDQU4sZ0JBQWNPLHNCQUFkOztBQUVBTix1QkFBcUJPLGVBQXJCO0FBQ0FQLHVCQUFxQmpRLHFCQUFyQixHQUE2QyxVQUFDdkssT0FBRDtBQUFBLFdBQWF5YSxTQUFTTyxVQUFULENBQW9CaGIsT0FBcEIsQ0FBYjtBQUFBLEdBQTdDOztBQUVBaWI7QUFDQUM7QUFDQUM7QUFDQUM7O0FBRUEsTUFBSXRZLDRCQUFKO0FBQ0EsTUFBTXVZLDhCQUE4QixJQUFJdFgsc0NBQUosRUFBcEM7QUFDQXNYLDhCQUE0Qm5YLG1DQUE1QjtBQUNBbVgsOEJBQTRCalgseUJBQTVCO0FBQ0FyUCxJQUFFNE8sMkJBQWlCbkssb0JBQW5CLEVBQXlDbkUsRUFBekMsQ0FBNEMsT0FBNUMsRUFBcUQsVUFBQzhOLEtBQUQsRUFBVztBQUM5REEsVUFBTUMsY0FBTjtBQUNBa1k7QUFDRCxHQUhEOztBQUtBdm1CLElBQUU0TywyQkFBaUJsQixrQkFBbkIsRUFBdUNwTixFQUF2QyxDQUEwQyxPQUExQyxFQUFtRCxVQUFDOE4sS0FBRCxFQUFXO0FBQzVEQSxVQUFNQyxjQUFOO0FBQ0FtWTtBQUNELEdBSEQ7O0FBS0F4bUIsSUFBRTRPLDJCQUFpQm5CLHdCQUFuQixFQUE2Q25OLEVBQTdDLENBQWdELE9BQWhELEVBQXlELFlBQU07QUFDN0QsUUFBTW1tQixZQUFZcG1CLFNBQVMwQyxLQUEzQjtBQUNBMUMsYUFBUzBDLEtBQVQsR0FBaUIvQyxFQUFFNE8sMkJBQWlCekssT0FBbkIsRUFBNEJ4RCxJQUE1QixDQUFpQyxZQUFqQyxDQUFqQjtBQUNBWixXQUFPMm1CLEtBQVA7QUFDQXJtQixhQUFTMEMsS0FBVCxHQUFpQjBqQixTQUFqQjtBQUNELEdBTEQ7O0FBT0FFO0FBQ0FDO0FBQ0FDOztBQUVBLFdBQVNBLFlBQVQsR0FBd0I7QUFDdEI3bUIsTUFBRTRPLDJCQUFpQjVDLHNCQUFuQixFQUNHbEwsSUFESCxDQUNRLDRCQURSLEVBRUdnbUIsR0FGSCxDQUVPLE1BRlA7QUFHRDs7QUFFRCxXQUFTWiwwQkFBVCxHQUFzQztBQUNwQ2xtQixNQUFFNE8sMkJBQWlCeEssc0JBQW5CLEVBQTJDOUQsRUFBM0MsQ0FBOEMsT0FBOUMsRUFBdUQsVUFBQzhOLEtBQUQsRUFBVztBQUNoRSxVQUFNMlksb0JBQW9CL21CLEVBQUVvTyxNQUFNM04sYUFBUixFQUN2QkksT0FEdUIsQ0FDZixJQURlLEVBRXZCMk4sSUFGdUIsQ0FFbEIsUUFGa0IsQ0FBMUI7O0FBSUF1WSx3QkFBa0J0VSxXQUFsQixDQUE4QixRQUE5QjtBQUNELEtBTkQ7QUFPRDs7QUFFRCxXQUFTOFQsc0JBQVQsR0FBa0M7QUFDaEMsUUFBTVMsU0FBU2huQixFQUFFNE8sMkJBQWlCbEssZ0JBQW5CLENBQWY7QUFDQSxRQUFNNEosT0FBT3RPLEVBQUU0TywyQkFBaUJuSyxvQkFBbkIsQ0FBYjtBQUNBLFFBQU13aUIsc0JBQXNCM1ksS0FBSzZCLFFBQUwsQ0FBYyxXQUFkLENBQTVCOztBQUVBLFFBQUk4VyxtQkFBSixFQUF5QjtBQUN2QjNZLFdBQUtHLFdBQUwsQ0FBaUIsV0FBakI7QUFDQXVZLGFBQU90WSxRQUFQLENBQWdCLFFBQWhCO0FBQ0QsS0FIRCxNQUdPO0FBQ0xKLFdBQUtJLFFBQUwsQ0FBYyxXQUFkO0FBQ0FzWSxhQUFPdlksV0FBUCxDQUFtQixRQUFuQjtBQUNEOztBQUVELFFBQU15WSxRQUFRNVksS0FBS3hOLElBQUwsQ0FBVSxpQkFBVixDQUFkO0FBQ0FvbUIsVUFBTW5tQixJQUFOLENBQVdrbUIsc0JBQXNCLEtBQXRCLEdBQThCLFFBQXpDO0FBQ0Q7O0FBRUQsV0FBU2QsdUJBQVQsR0FBbUM7QUFDakMsUUFBTWdCLGFBQWFubkIsRUFBRTRPLDJCQUFpQmhLLG9CQUFuQixDQUFuQjs7QUFFQTVFLE1BQUU0TywyQkFBaUJqSyxnQkFBbkIsRUFBcUNyRSxFQUFyQyxDQUF3QyxPQUF4QyxFQUFpRCxZQUFNO0FBQ3JENm1CLGlCQUFXN08sSUFBWCxDQUFnQixVQUFoQixFQUE0QixLQUE1QjtBQUNELEtBRkQ7QUFHRDs7QUFFRCxXQUFTa08sb0JBQVQsR0FBZ0M7QUFDOUIsUUFBTVEsU0FBU2huQixFQUFFNE8sMkJBQWlCakIsY0FBbkIsQ0FBZjtBQUNBLFFBQU1XLE9BQU90TyxFQUFFNE8sMkJBQWlCbEIsa0JBQW5CLENBQWI7QUFDQSxRQUFNMFosZUFBZTlZLEtBQUs2QixRQUFMLENBQWMsV0FBZCxDQUFyQjs7QUFFQTdCLFNBQUttRSxXQUFMLENBQWlCLFdBQWpCLEVBQThCLENBQUMyVSxZQUEvQjtBQUNBSixXQUFPdlUsV0FBUCxDQUFtQixRQUFuQixFQUE2QjJVLFlBQTdCOztBQUVBLFFBQU1GLFFBQVE1WSxLQUFLeE4sSUFBTCxDQUFVLGlCQUFWLENBQWQ7QUFDQW9tQixVQUFNbm1CLElBQU4sQ0FBV3FtQixlQUFlLEtBQWYsR0FBdUIsUUFBbEM7QUFDRDs7QUFFRCxXQUFTaEIscUJBQVQsR0FBaUM7QUFDL0IsUUFBTWUsYUFBYW5uQixFQUFFNE8sMkJBQWlCZixrQkFBbkIsQ0FBbkI7O0FBRUE3TixNQUFFNE8sMkJBQWlCaEIsY0FBbkIsRUFBbUN0TixFQUFuQyxDQUFzQyxPQUF0QyxFQUErQyxZQUFNO0FBQ25ENm1CLGlCQUFXN08sSUFBWCxDQUFnQixVQUFoQixFQUE0QixLQUE1QjtBQUNELEtBRkQ7QUFHRDs7QUFFRCxXQUFTcU8sMEJBQVQsR0FBc0M7QUFDcEMsUUFBTW5sQixTQUFTeEIsRUFBRTRPLDJCQUFpQi9KLGdCQUFuQixDQUFmO0FBQ0EsUUFBTXdpQixRQUFRN2xCLE9BQU9WLElBQVAsQ0FBWSxNQUFaLENBQWQ7QUFDQSxRQUFNd21CLGlCQUFpQjlsQixPQUFPVixJQUFQLENBQVk4TiwyQkFBaUI5SiwwQkFBN0IsQ0FBdkI7QUFDQSxRQUFNeWlCLGFBQWEvbEIsT0FBT1YsSUFBUCxDQUFZOE4sMkJBQWlCeEosZ0JBQTdCLENBQW5CO0FBQ0EsUUFBTW9pQixjQUFjSCxNQUFNdm1CLElBQU4sQ0FBVzhOLDJCQUFpQjNKLHFCQUE1QixDQUFwQjtBQUNBLFFBQU13aUIsa0JBQWtCRCxZQUFZM21CLE9BQVosQ0FBb0IsYUFBcEIsQ0FBeEI7O0FBRUFXLFdBQU9sQixFQUFQLENBQVUsZ0JBQVYsRUFBNEIsWUFBTTtBQUNoQ04sUUFBRTRPLDJCQUFpQnpKLGlCQUFuQixFQUFzQ2tQLElBQXRDLENBQTJDLFVBQTNDLEVBQXVELElBQXZEO0FBQ0QsS0FGRDs7QUFJQWdULFVBQU12bUIsSUFBTixDQUFXOE4sMkJBQWlCN0osb0JBQTVCLEVBQWtEekUsRUFBbEQsQ0FBcUQsT0FBckQsRUFBOEQsVUFBQzhOLEtBQUQsRUFBVztBQUN2RSxVQUFNc1osZUFBZTFuQixFQUFFb08sTUFBTTNOLGFBQVIsRUFBdUJHLEdBQXZCLEVBQXJCO0FBQ0FaLFFBQUU0TywyQkFBaUJ6SixpQkFBbkIsRUFBc0NrUCxJQUF0QyxDQUEyQyxVQUEzQyxFQUF1RHFULGFBQWFqWSxJQUFiLEdBQW9CaFQsTUFBcEIsS0FBK0IsQ0FBdEY7QUFDRCxLQUhEOztBQUtBNHFCLFVBQU12bUIsSUFBTixDQUFXOE4sMkJBQWlCK1kscUNBQTVCLEVBQW1Fcm5CLEVBQW5FLENBQXNFLFFBQXRFLEVBQWdGLFVBQUM4TixLQUFELEVBQVc7QUFDekYsVUFBTXdaLFlBQVk1bkIsRUFBRW9PLE1BQU0zTixhQUFSLEVBQXVCNGIsRUFBdkIsQ0FBMEIsVUFBMUIsQ0FBbEI7QUFDQWlMLHFCQUFlalQsSUFBZixDQUFvQixVQUFwQixFQUFnQ3VULFNBQWhDO0FBQ0QsS0FIRDs7QUFLQVAsVUFBTXZtQixJQUFOLENBQVc4TiwyQkFBaUI1SixxQkFBNUIsRUFBbUQxRSxFQUFuRCxDQUFzRCxRQUF0RCxFQUFnRSxVQUFDOE4sS0FBRCxFQUFXO0FBQ3pFLFVBQU15Wix1QkFBdUI3bkIsRUFBRW9PLE1BQU0zTixhQUFSLEVBQXVCRyxHQUF2QixFQUE3QjtBQUNBLFVBQU1rbkIsYUFBYVQsTUFBTXZtQixJQUFOLENBQVc4TiwyQkFBaUIxSixvQkFBNUIsQ0FBbkI7O0FBRUEsVUFBSTJpQix5QkFBeUJ4QyxvQkFBN0IsRUFBbUQ7QUFDakRrQyxtQkFBVzlZLFdBQVgsQ0FBdUIsUUFBdkI7QUFDQXFaLG1CQUFXMVcsSUFBWCxDQUFnQjBXLFdBQVdubkIsSUFBWCxDQUFnQixnQkFBaEIsQ0FBaEI7QUFDRCxPQUhELE1BR087QUFDTDRtQixtQkFBVzdZLFFBQVgsQ0FBb0IsUUFBcEI7QUFDRDs7QUFFRCxVQUFJbVoseUJBQXlCdkMscUJBQTdCLEVBQW9EO0FBQ2xEd0MsbUJBQVcxVyxJQUFYLENBQWdCLEdBQWhCO0FBQ0Q7O0FBRUQsVUFBSXlXLHlCQUF5QnRDLDJCQUE3QixFQUEwRDtBQUN4RGtDLHdCQUFnQi9ZLFFBQWhCLENBQXlCLFFBQXpCO0FBQ0E4WSxvQkFBWW5ULElBQVosQ0FBaUIsVUFBakIsRUFBNkIsSUFBN0I7QUFDRCxPQUhELE1BR087QUFDTG9ULHdCQUFnQmhaLFdBQWhCLENBQTRCLFFBQTVCO0FBQ0ErWSxvQkFBWW5ULElBQVosQ0FBaUIsVUFBakIsRUFBNkIsS0FBN0I7QUFDRDtBQUNGLEtBdEJEO0FBdUJEOztBQUVELFdBQVNnUyw2QkFBVCxHQUF5QztBQUN2QyxRQUFNL1gsT0FBT3RPLEVBQUU0TywyQkFBaUJ2SiwwQkFBbkIsQ0FBYjtBQUNBLFFBQU0waUIsV0FBVy9uQixFQUFFNE8sMkJBQWlCckosbUNBQW5CLENBQWpCOztBQUVBdkYsTUFBRTRPLDJCQUFpQnRKLDRCQUFuQixFQUFpRGhGLEVBQWpELENBQW9ELFFBQXBELEVBQThELFVBQUM4TixLQUFELEVBQVc7QUFDdkUsVUFBTTRaLFdBQVdob0IsRUFBRW9PLE1BQU0zTixhQUFSLENBQWpCO0FBQ0EsVUFBTXduQixVQUFVam9CLEVBQUUsaUJBQUYsRUFBcUJnb0IsUUFBckIsQ0FBaEI7QUFDQSxVQUFNRSx3QkFBd0JGLFNBQVNwbkIsR0FBVCxFQUE5Qjs7QUFFQW1uQixlQUFTSSxHQUFULENBQWEsa0JBQWIsRUFBaUNGLFFBQVF0bkIsSUFBUixDQUFhLGtCQUFiLENBQWpDO0FBQ0FvbkIsZUFBU3RWLFdBQVQsQ0FBcUIsV0FBckIsRUFBa0N3VixRQUFRdG5CLElBQVIsQ0FBYSxXQUFiLE1BQThCN0YsU0FBaEU7O0FBRUF3VCxXQUFLZ0ssSUFBTCxDQUFVLFVBQVYsRUFBc0I1WixTQUFTd3BCLHFCQUFULEVBQWdDLEVBQWhDLE1BQXdDNVosS0FBSzNOLElBQUwsQ0FBVSxlQUFWLENBQTlEO0FBQ0QsS0FURDtBQVVEOztBQUVELFdBQVNpbUIsNEJBQVQsR0FBd0M7QUFDdEMsUUFBTXBsQixTQUFTeEIsRUFBRTRPLDJCQUFpQmhKLDBCQUFuQixDQUFmOztBQUVBNUYsTUFBRTRPLDJCQUFpQi9JLDhCQUFuQixFQUFtRHZGLEVBQW5ELENBQXNELE9BQXRELEVBQStELFVBQUM4TixLQUFELEVBQVc7QUFDeEU1TSxhQUFPVixJQUFQLENBQVk4TiwyQkFBaUI5SSwyQkFBN0IsRUFBMERsRixHQUExRCxDQUE4RFosRUFBRW9PLE1BQU0zTixhQUFSLEVBQXVCRSxJQUF2QixDQUE0QixhQUE1QixDQUE5RDtBQUNELEtBRkQ7QUFHRDtBQUNGLENBdkxELEUiLCJmaWxlIjoib3JkZXJfdmlldy5idW5kbGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIENvcHlyaWdodCBzaW5jZSAyMDA3IFByZXN0YVNob3AgU0EgYW5kIENvbnRyaWJ1dG9yc1xuICogUHJlc3RhU2hvcCBpcyBhbiBJbnRlcm5hdGlvbmFsIFJlZ2lzdGVyZWQgVHJhZGVtYXJrICYgUHJvcGVydHkgb2YgUHJlc3RhU2hvcCBTQVxuICpcbiAqIE5PVElDRSBPRiBMSUNFTlNFXG4gKlxuICogVGhpcyBzb3VyY2UgZmlsZSBpcyBzdWJqZWN0IHRvIHRoZSBPcGVuIFNvZnR3YXJlIExpY2Vuc2UgKE9TTCAzLjApXG4gKiB0aGF0IGlzIGJ1bmRsZWQgd2l0aCB0aGlzIHBhY2thZ2UgaW4gdGhlIGZpbGUgTElDRU5TRS5tZC5cbiAqIEl0IGlzIGFsc28gYXZhaWxhYmxlIHRocm91Z2ggdGhlIHdvcmxkLXdpZGUtd2ViIGF0IHRoaXMgVVJMOlxuICogaHR0cHM6Ly9vcGVuc291cmNlLm9yZy9saWNlbnNlcy9PU0wtMy4wXG4gKiBJZiB5b3UgZGlkIG5vdCByZWNlaXZlIGEgY29weSBvZiB0aGUgbGljZW5zZSBhbmQgYXJlIHVuYWJsZSB0b1xuICogb2J0YWluIGl0IHRocm91Z2ggdGhlIHdvcmxkLXdpZGUtd2ViLCBwbGVhc2Ugc2VuZCBhbiBlbWFpbFxuICogdG8gbGljZW5zZUBwcmVzdGFzaG9wLmNvbSBzbyB3ZSBjYW4gc2VuZCB5b3UgYSBjb3B5IGltbWVkaWF0ZWx5LlxuICpcbiAqIERJU0NMQUlNRVJcbiAqXG4gKiBEbyBub3QgZWRpdCBvciBhZGQgdG8gdGhpcyBmaWxlIGlmIHlvdSB3aXNoIHRvIHVwZ3JhZGUgUHJlc3RhU2hvcCB0byBuZXdlclxuICogdmVyc2lvbnMgaW4gdGhlIGZ1dHVyZS4gSWYgeW91IHdpc2ggdG8gY3VzdG9taXplIFByZXN0YVNob3AgZm9yIHlvdXJcbiAqIG5lZWRzIHBsZWFzZSByZWZlciB0byBodHRwczovL2RldmRvY3MucHJlc3Rhc2hvcC5jb20vIGZvciBtb3JlIGluZm9ybWF0aW9uLlxuICpcbiAqIEBhdXRob3IgICAgUHJlc3RhU2hvcCBTQSBhbmQgQ29udHJpYnV0b3JzIDxjb250YWN0QHByZXN0YXNob3AuY29tPlxuICogQGNvcHlyaWdodCBTaW5jZSAyMDA3IFByZXN0YVNob3AgU0EgYW5kIENvbnRyaWJ1dG9yc1xuICogQGxpY2Vuc2UgICBodHRwczovL29wZW5zb3VyY2Uub3JnL2xpY2Vuc2VzL09TTC0zLjAgT3BlbiBTb2Z0d2FyZSBMaWNlbnNlIChPU0wgMy4wKVxuICovXG5jbGFzcyBMb2NhbGl6YXRpb25FeGNlcHRpb24ge1xuICBjb25zdHJ1Y3RvcihtZXNzYWdlKSB7XG4gICAgdGhpcy5tZXNzYWdlID0gbWVzc2FnZTtcbiAgICB0aGlzLm5hbWUgPSAnTG9jYWxpemF0aW9uRXhjZXB0aW9uJztcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBMb2NhbGl6YXRpb25FeGNlcHRpb247XG4iLCIvKipcbiAqIENvcHlyaWdodCBzaW5jZSAyMDA3IFByZXN0YVNob3AgU0EgYW5kIENvbnRyaWJ1dG9yc1xuICogUHJlc3RhU2hvcCBpcyBhbiBJbnRlcm5hdGlvbmFsIFJlZ2lzdGVyZWQgVHJhZGVtYXJrICYgUHJvcGVydHkgb2YgUHJlc3RhU2hvcCBTQVxuICpcbiAqIE5PVElDRSBPRiBMSUNFTlNFXG4gKlxuICogVGhpcyBzb3VyY2UgZmlsZSBpcyBzdWJqZWN0IHRvIHRoZSBPcGVuIFNvZnR3YXJlIExpY2Vuc2UgKE9TTCAzLjApXG4gKiB0aGF0IGlzIGJ1bmRsZWQgd2l0aCB0aGlzIHBhY2thZ2UgaW4gdGhlIGZpbGUgTElDRU5TRS5tZC5cbiAqIEl0IGlzIGFsc28gYXZhaWxhYmxlIHRocm91Z2ggdGhlIHdvcmxkLXdpZGUtd2ViIGF0IHRoaXMgVVJMOlxuICogaHR0cHM6Ly9vcGVuc291cmNlLm9yZy9saWNlbnNlcy9PU0wtMy4wXG4gKiBJZiB5b3UgZGlkIG5vdCByZWNlaXZlIGEgY29weSBvZiB0aGUgbGljZW5zZSBhbmQgYXJlIHVuYWJsZSB0b1xuICogb2J0YWluIGl0IHRocm91Z2ggdGhlIHdvcmxkLXdpZGUtd2ViLCBwbGVhc2Ugc2VuZCBhbiBlbWFpbFxuICogdG8gbGljZW5zZUBwcmVzdGFzaG9wLmNvbSBzbyB3ZSBjYW4gc2VuZCB5b3UgYSBjb3B5IGltbWVkaWF0ZWx5LlxuICpcbiAqIERJU0NMQUlNRVJcbiAqXG4gKiBEbyBub3QgZWRpdCBvciBhZGQgdG8gdGhpcyBmaWxlIGlmIHlvdSB3aXNoIHRvIHVwZ3JhZGUgUHJlc3RhU2hvcCB0byBuZXdlclxuICogdmVyc2lvbnMgaW4gdGhlIGZ1dHVyZS4gSWYgeW91IHdpc2ggdG8gY3VzdG9taXplIFByZXN0YVNob3AgZm9yIHlvdXJcbiAqIG5lZWRzIHBsZWFzZSByZWZlciB0byBodHRwczovL2RldmRvY3MucHJlc3Rhc2hvcC5jb20vIGZvciBtb3JlIGluZm9ybWF0aW9uLlxuICpcbiAqIEBhdXRob3IgICAgUHJlc3RhU2hvcCBTQSBhbmQgQ29udHJpYnV0b3JzIDxjb250YWN0QHByZXN0YXNob3AuY29tPlxuICogQGNvcHlyaWdodCBTaW5jZSAyMDA3IFByZXN0YVNob3AgU0EgYW5kIENvbnRyaWJ1dG9yc1xuICogQGxpY2Vuc2UgICBodHRwczovL29wZW5zb3VyY2Uub3JnL2xpY2Vuc2VzL09TTC0zLjAgT3BlbiBTb2Z0d2FyZSBMaWNlbnNlIChPU0wgMy4wKVxuICovXG5pbXBvcnQgTnVtYmVyRm9ybWF0dGVyIGZyb20gJ0BhcHAvY2xkci9udW1iZXItZm9ybWF0dGVyJztcbmltcG9ydCBOdW1iZXJTeW1ib2wgZnJvbSAnQGFwcC9jbGRyL251bWJlci1zeW1ib2wnO1xuaW1wb3J0IFByaWNlU3BlY2lmaWNhdGlvbiBmcm9tICdAYXBwL2NsZHIvc3BlY2lmaWNhdGlvbnMvcHJpY2UnO1xuaW1wb3J0IE51bWJlclNwZWNpZmljYXRpb24gZnJvbSAnQGFwcC9jbGRyL3NwZWNpZmljYXRpb25zL251bWJlcic7XG5cbmV4cG9ydCB7XG4gIFByaWNlU3BlY2lmaWNhdGlvbixcbiAgTnVtYmVyU3BlY2lmaWNhdGlvbixcbiAgTnVtYmVyRm9ybWF0dGVyLFxuICBOdW1iZXJTeW1ib2wsXG59O1xuIiwiLyoqXG4gKiBDb3B5cmlnaHQgc2luY2UgMjAwNyBQcmVzdGFTaG9wIFNBIGFuZCBDb250cmlidXRvcnNcbiAqIFByZXN0YVNob3AgaXMgYW4gSW50ZXJuYXRpb25hbCBSZWdpc3RlcmVkIFRyYWRlbWFyayAmIFByb3BlcnR5IG9mIFByZXN0YVNob3AgU0FcbiAqXG4gKiBOT1RJQ0UgT0YgTElDRU5TRVxuICpcbiAqIFRoaXMgc291cmNlIGZpbGUgaXMgc3ViamVjdCB0byB0aGUgT3BlbiBTb2Z0d2FyZSBMaWNlbnNlIChPU0wgMy4wKVxuICogdGhhdCBpcyBidW5kbGVkIHdpdGggdGhpcyBwYWNrYWdlIGluIHRoZSBmaWxlIExJQ0VOU0UubWQuXG4gKiBJdCBpcyBhbHNvIGF2YWlsYWJsZSB0aHJvdWdoIHRoZSB3b3JsZC13aWRlLXdlYiBhdCB0aGlzIFVSTDpcbiAqIGh0dHBzOi8vb3BlbnNvdXJjZS5vcmcvbGljZW5zZXMvT1NMLTMuMFxuICogSWYgeW91IGRpZCBub3QgcmVjZWl2ZSBhIGNvcHkgb2YgdGhlIGxpY2Vuc2UgYW5kIGFyZSB1bmFibGUgdG9cbiAqIG9idGFpbiBpdCB0aHJvdWdoIHRoZSB3b3JsZC13aWRlLXdlYiwgcGxlYXNlIHNlbmQgYW4gZW1haWxcbiAqIHRvIGxpY2Vuc2VAcHJlc3Rhc2hvcC5jb20gc28gd2UgY2FuIHNlbmQgeW91IGEgY29weSBpbW1lZGlhdGVseS5cbiAqXG4gKiBESVNDTEFJTUVSXG4gKlxuICogRG8gbm90IGVkaXQgb3IgYWRkIHRvIHRoaXMgZmlsZSBpZiB5b3Ugd2lzaCB0byB1cGdyYWRlIFByZXN0YVNob3AgdG8gbmV3ZXJcbiAqIHZlcnNpb25zIGluIHRoZSBmdXR1cmUuIElmIHlvdSB3aXNoIHRvIGN1c3RvbWl6ZSBQcmVzdGFTaG9wIGZvciB5b3VyXG4gKiBuZWVkcyBwbGVhc2UgcmVmZXIgdG8gaHR0cHM6Ly9kZXZkb2NzLnByZXN0YXNob3AuY29tLyBmb3IgbW9yZSBpbmZvcm1hdGlvbi5cbiAqXG4gKiBAYXV0aG9yICAgIFByZXN0YVNob3AgU0EgYW5kIENvbnRyaWJ1dG9ycyA8Y29udGFjdEBwcmVzdGFzaG9wLmNvbT5cbiAqIEBjb3B5cmlnaHQgU2luY2UgMjAwNyBQcmVzdGFTaG9wIFNBIGFuZCBDb250cmlidXRvcnNcbiAqIEBsaWNlbnNlICAgaHR0cHM6Ly9vcGVuc291cmNlLm9yZy9saWNlbnNlcy9PU0wtMy4wIE9wZW4gU29mdHdhcmUgTGljZW5zZSAoT1NMIDMuMClcbiAqL1xuLyoqXG4gKiBUaGVzZSBwbGFjZWhvbGRlcnMgYXJlIHVzZWQgaW4gQ0xEUiBudW1iZXIgZm9ybWF0dGluZyB0ZW1wbGF0ZXMuXG4gKiBUaGV5IGFyZSBtZWFudCB0byBiZSByZXBsYWNlZCBieSB0aGUgY29ycmVjdCBsb2NhbGl6ZWQgc3ltYm9scyBpbiB0aGUgbnVtYmVyIGZvcm1hdHRpbmcgcHJvY2Vzcy5cbiAqL1xuaW1wb3J0IE51bWJlclN5bWJvbCBmcm9tICdAYXBwL2NsZHIvbnVtYmVyLXN5bWJvbCc7XG5pbXBvcnQgUHJpY2VTcGVjaWZpY2F0aW9uIGZyb20gJ0BhcHAvY2xkci9zcGVjaWZpY2F0aW9ucy9wcmljZSc7XG5pbXBvcnQgTnVtYmVyU3BlY2lmaWNhdGlvbiBmcm9tICdAYXBwL2NsZHIvc3BlY2lmaWNhdGlvbnMvbnVtYmVyJztcblxuY29uc3QgZXNjYXBlUkUgPSByZXF1aXJlKCdsb2Rhc2guZXNjYXBlcmVnZXhwJyk7XG5cbmNvbnN0IENVUlJFTkNZX1NZTUJPTF9QTEFDRUhPTERFUiA9ICfCpCc7XG5jb25zdCBERUNJTUFMX1NFUEFSQVRPUl9QTEFDRUhPTERFUiA9ICcuJztcbmNvbnN0IEdST1VQX1NFUEFSQVRPUl9QTEFDRUhPTERFUiA9ICcsJztcbmNvbnN0IE1JTlVTX1NJR05fUExBQ0VIT0xERVIgPSAnLSc7XG5jb25zdCBQRVJDRU5UX1NZTUJPTF9QTEFDRUhPTERFUiA9ICclJztcbmNvbnN0IFBMVVNfU0lHTl9QTEFDRUhPTERFUiA9ICcrJztcblxuY2xhc3MgTnVtYmVyRm9ybWF0dGVyIHtcbiAgLyoqXG4gICAqIEBwYXJhbSBOdW1iZXJTcGVjaWZpY2F0aW9uIHNwZWNpZmljYXRpb24gTnVtYmVyIHNwZWNpZmljYXRpb24gdG8gYmUgdXNlZFxuICAgKiAgIChjYW4gYmUgYSBudW1iZXIgc3BlYywgYSBwcmljZSBzcGVjLCBhIHBlcmNlbnRhZ2Ugc3BlYylcbiAgICovXG4gIGNvbnN0cnVjdG9yKHNwZWNpZmljYXRpb24pIHtcbiAgICB0aGlzLm51bWJlclNwZWNpZmljYXRpb24gPSBzcGVjaWZpY2F0aW9uO1xuICB9XG5cbiAgLyoqXG4gICAqIEZvcm1hdHMgdGhlIHBhc3NlZCBudW1iZXIgYWNjb3JkaW5nIHRvIHNwZWNpZmljYXRpb25zLlxuICAgKlxuICAgKiBAcGFyYW0gaW50fGZsb2F0fHN0cmluZyBudW1iZXIgVGhlIG51bWJlciB0byBmb3JtYXRcbiAgICogQHBhcmFtIE51bWJlclNwZWNpZmljYXRpb24gc3BlY2lmaWNhdGlvbiBOdW1iZXIgc3BlY2lmaWNhdGlvbiB0byBiZSB1c2VkXG4gICAqICAgKGNhbiBiZSBhIG51bWJlciBzcGVjLCBhIHByaWNlIHNwZWMsIGEgcGVyY2VudGFnZSBzcGVjKVxuICAgKlxuICAgKiBAcmV0dXJuIHN0cmluZyBUaGUgZm9ybWF0dGVkIG51bWJlclxuICAgKiAgICAgICAgICAgICAgICBZb3Ugc2hvdWxkIHVzZSB0aGlzIHRoaXMgdmFsdWUgZm9yIGRpc3BsYXksIHdpdGhvdXQgbW9kaWZ5aW5nIGl0XG4gICAqL1xuICBmb3JtYXQobnVtYmVyLCBzcGVjaWZpY2F0aW9uKSB7XG4gICAgaWYgKHNwZWNpZmljYXRpb24gIT09IHVuZGVmaW5lZCkge1xuICAgICAgdGhpcy5udW1iZXJTcGVjaWZpY2F0aW9uID0gc3BlY2lmaWNhdGlvbjtcbiAgICB9XG5cbiAgICAvKlxuICAgICAqIFdlIG5lZWQgdG8gd29yayBvbiB0aGUgYWJzb2x1dGUgdmFsdWUgZmlyc3QuXG4gICAgICogVGhlbiB0aGUgQ0xEUiBwYXR0ZXJuIHdpbGwgYWRkIHRoZSBzaWduIGlmIHJlbGV2YW50IChhdCB0aGUgZW5kKS5cbiAgICAgKi9cbiAgICBjb25zdCBudW0gPSBNYXRoLmFicyhudW1iZXIpLnRvRml4ZWQodGhpcy5udW1iZXJTcGVjaWZpY2F0aW9uLmdldE1heEZyYWN0aW9uRGlnaXRzKCkpO1xuXG4gICAgbGV0IFttYWpvckRpZ2l0cywgbWlub3JEaWdpdHNdID0gdGhpcy5leHRyYWN0TWFqb3JNaW5vckRpZ2l0cyhudW0pO1xuICAgIG1ham9yRGlnaXRzID0gdGhpcy5zcGxpdE1ham9yR3JvdXBzKG1ham9yRGlnaXRzKTtcbiAgICBtaW5vckRpZ2l0cyA9IHRoaXMuYWRqdXN0TWlub3JEaWdpdHNaZXJvZXMobWlub3JEaWdpdHMpO1xuXG4gICAgLy8gQXNzZW1ibGUgdGhlIGZpbmFsIG51bWJlclxuICAgIGxldCBmb3JtYXR0ZWROdW1iZXIgPSBtYWpvckRpZ2l0cztcblxuICAgIGlmIChtaW5vckRpZ2l0cykge1xuICAgICAgZm9ybWF0dGVkTnVtYmVyICs9IERFQ0lNQUxfU0VQQVJBVE9SX1BMQUNFSE9MREVSICsgbWlub3JEaWdpdHM7XG4gICAgfVxuXG4gICAgLy8gR2V0IHRoZSBnb29kIENMRFIgZm9ybWF0dGluZyBwYXR0ZXJuLiBTaWduIGlzIGltcG9ydGFudCBoZXJlICFcbiAgICBjb25zdCBwYXR0ZXJuID0gdGhpcy5nZXRDbGRyUGF0dGVybihudW1iZXIgPCAwKTtcbiAgICBmb3JtYXR0ZWROdW1iZXIgPSB0aGlzLmFkZFBsYWNlaG9sZGVycyhmb3JtYXR0ZWROdW1iZXIsIHBhdHRlcm4pO1xuICAgIGZvcm1hdHRlZE51bWJlciA9IHRoaXMucmVwbGFjZVN5bWJvbHMoZm9ybWF0dGVkTnVtYmVyKTtcblxuICAgIGZvcm1hdHRlZE51bWJlciA9IHRoaXMucGVyZm9ybVNwZWNpZmljUmVwbGFjZW1lbnRzKGZvcm1hdHRlZE51bWJlcik7XG5cbiAgICByZXR1cm4gZm9ybWF0dGVkTnVtYmVyO1xuICB9XG5cbiAgLyoqXG4gICAqIEdldCBudW1iZXIncyBtYWpvciBhbmQgbWlub3IgZGlnaXRzLlxuICAgKlxuICAgKiBNYWpvciBkaWdpdHMgYXJlIHRoZSBcImludGVnZXJcIiBwYXJ0IChiZWZvcmUgZGVjaW1hbCBzZXBhcmF0b3IpLFxuICAgKiBtaW5vciBkaWdpdHMgYXJlIHRoZSBmcmFjdGlvbmFsIHBhcnRcbiAgICogUmVzdWx0IHdpbGwgYmUgYW4gYXJyYXkgb2YgZXhhY3RseSAyIGl0ZW1zOiBbbWFqb3JEaWdpdHMsIG1pbm9yRGlnaXRzXVxuICAgKlxuICAgKiBVc2FnZSBleGFtcGxlOlxuICAgKiAgbGlzdChtYWpvckRpZ2l0cywgbWlub3JEaWdpdHMpID0gdGhpcy5nZXRNYWpvck1pbm9yRGlnaXRzKGRlY2ltYWxOdW1iZXIpO1xuICAgKlxuICAgKiBAcGFyYW0gRGVjaW1hbE51bWJlciBudW1iZXJcbiAgICpcbiAgICogQHJldHVybiBzdHJpbmdbXVxuICAgKi9cbiAgZXh0cmFjdE1ham9yTWlub3JEaWdpdHMobnVtYmVyKSB7XG4gICAgLy8gR2V0IHRoZSBudW1iZXIncyBtYWpvciBhbmQgbWlub3IgZGlnaXRzLlxuICAgIGNvbnN0IHJlc3VsdCA9IG51bWJlci50b1N0cmluZygpLnNwbGl0KCcuJyk7XG4gICAgY29uc3QgbWFqb3JEaWdpdHMgPSByZXN1bHRbMF07XG4gICAgY29uc3QgbWlub3JEaWdpdHMgPSAocmVzdWx0WzFdID09PSB1bmRlZmluZWQpID8gJycgOiByZXN1bHRbMV07XG5cbiAgICByZXR1cm4gW21ham9yRGlnaXRzLCBtaW5vckRpZ2l0c107XG4gIH1cblxuICAvKipcbiAgICogU3BsaXRzIG1ham9yIGRpZ2l0cyBpbnRvIGdyb3Vwcy5cbiAgICpcbiAgICogZS5nLjogR2l2ZW4gdGhlIG1ham9yIGRpZ2l0cyBcIjEyMzQ1NjdcIiwgYW5kIG1ham9yIGdyb3VwIHNpemVcbiAgICogIGNvbmZpZ3VyZWQgdG8gMyBkaWdpdHMsIHRoZSByZXN1bHQgd291bGQgYmUgXCIxIDIzNCA1NjdcIlxuICAgKlxuICAgKiBAcGFyYW0gc3RyaW5nIG1ham9yRGlnaXRzIFRoZSBtYWpvciBkaWdpdHMgdG8gYmUgZ3JvdXBlZFxuICAgKlxuICAgKiBAcmV0dXJuIHN0cmluZyBUaGUgZ3JvdXBlZCBtYWpvciBkaWdpdHNcbiAgICovXG4gIHNwbGl0TWFqb3JHcm91cHMoZGlnaXQpIHtcbiAgICBpZiAoIXRoaXMubnVtYmVyU3BlY2lmaWNhdGlvbi5pc0dyb3VwaW5nVXNlZCgpKSB7XG4gICAgICByZXR1cm4gZGlnaXQ7XG4gICAgfVxuXG4gICAgLy8gUmV2ZXJzZSB0aGUgbWFqb3IgZGlnaXRzLCBzaW5jZSB0aGV5IGFyZSBncm91cGVkIGZyb20gdGhlIHJpZ2h0LlxuICAgIGNvbnN0IG1ham9yRGlnaXRzID0gZGlnaXQuc3BsaXQoJycpLnJldmVyc2UoKTtcblxuICAgIC8vIEdyb3VwIHRoZSBtYWpvciBkaWdpdHMuXG4gICAgbGV0IGdyb3VwcyA9IFtdO1xuICAgIGdyb3Vwcy5wdXNoKG1ham9yRGlnaXRzLnNwbGljZSgwLCB0aGlzLm51bWJlclNwZWNpZmljYXRpb24uZ2V0UHJpbWFyeUdyb3VwU2l6ZSgpKSk7XG4gICAgd2hpbGUgKG1ham9yRGlnaXRzLmxlbmd0aCkge1xuICAgICAgZ3JvdXBzLnB1c2gobWFqb3JEaWdpdHMuc3BsaWNlKDAsIHRoaXMubnVtYmVyU3BlY2lmaWNhdGlvbi5nZXRTZWNvbmRhcnlHcm91cFNpemUoKSkpO1xuICAgIH1cblxuICAgIC8vIFJldmVyc2UgYmFjayB0aGUgZGlnaXRzIGFuZCB0aGUgZ3JvdXBzXG4gICAgZ3JvdXBzID0gZ3JvdXBzLnJldmVyc2UoKTtcbiAgICBjb25zdCBuZXdHcm91cHMgPSBbXTtcbiAgICBncm91cHMuZm9yRWFjaCgoZ3JvdXApID0+IHtcbiAgICAgIG5ld0dyb3Vwcy5wdXNoKGdyb3VwLnJldmVyc2UoKS5qb2luKCcnKSk7XG4gICAgfSk7XG5cbiAgICAvLyBSZWNvbnN0cnVjdCB0aGUgbWFqb3IgZGlnaXRzLlxuICAgIHJldHVybiBuZXdHcm91cHMuam9pbihHUk9VUF9TRVBBUkFUT1JfUExBQ0VIT0xERVIpO1xuICB9XG5cbiAgLyoqXG4gICAqIEFkZHMgb3IgcmVtb3ZlIHRyYWlsaW5nIHplcm9lcywgZGVwZW5kaW5nIG9uIHNwZWNpZmllZCBtaW4gYW5kIG1heCBmcmFjdGlvbiBkaWdpdHMgbnVtYmVycy5cbiAgICpcbiAgICogQHBhcmFtIHN0cmluZyBtaW5vckRpZ2l0cyBEaWdpdHMgdG8gYmUgYWRqdXN0ZWQgd2l0aCAodHJpbW1lZCBvciBwYWRkZWQpIHplcm9lc1xuICAgKlxuICAgKiBAcmV0dXJuIHN0cmluZyBUaGUgYWRqdXN0ZWQgbWlub3IgZGlnaXRzXG4gICAqL1xuICBhZGp1c3RNaW5vckRpZ2l0c1plcm9lcyhtaW5vckRpZ2l0cykge1xuICAgIGxldCBkaWdpdCA9IG1pbm9yRGlnaXRzO1xuXG4gICAgaWYgKGRpZ2l0Lmxlbmd0aCA+IHRoaXMubnVtYmVyU3BlY2lmaWNhdGlvbi5nZXRNYXhGcmFjdGlvbkRpZ2l0cygpKSB7XG4gICAgICAvLyBTdHJpcCBhbnkgdHJhaWxpbmcgemVyb2VzLlxuICAgICAgZGlnaXQgPSBkaWdpdC5yZXBsYWNlKC8wKyQvLCAnJyk7XG4gICAgfVxuXG4gICAgaWYgKGRpZ2l0Lmxlbmd0aCA8IHRoaXMubnVtYmVyU3BlY2lmaWNhdGlvbi5nZXRNaW5GcmFjdGlvbkRpZ2l0cygpKSB7XG4gICAgICAvLyBSZS1hZGQgbmVlZGVkIHplcm9lc1xuICAgICAgZGlnaXQgPSBkaWdpdC5wYWRFbmQoXG4gICAgICAgIHRoaXMubnVtYmVyU3BlY2lmaWNhdGlvbi5nZXRNaW5GcmFjdGlvbkRpZ2l0cygpLFxuICAgICAgICAnMCcsXG4gICAgICApO1xuICAgIH1cblxuICAgIHJldHVybiBkaWdpdDtcbiAgfVxuXG4gIC8qKlxuICAgKiBHZXQgdGhlIENMRFIgZm9ybWF0dGluZyBwYXR0ZXJuLlxuICAgKlxuICAgKiBAc2VlIGh0dHA6Ly9jbGRyLnVuaWNvZGUub3JnL3RyYW5zbGF0aW9uL251bWJlci1wYXR0ZXJuc1xuICAgKlxuICAgKiBAcGFyYW0gYm9vbCBpc05lZ2F0aXZlIElmIHRydWUsIHRoZSBuZWdhdGl2ZSBwYXR0ZXJuXG4gICAqIHdpbGwgYmUgcmV0dXJuZWQgaW5zdGVhZCBvZiB0aGUgcG9zaXRpdmUgb25lXG4gICAqXG4gICAqIEByZXR1cm4gc3RyaW5nIFRoZSBDTERSIGZvcm1hdHRpbmcgcGF0dGVyblxuICAgKi9cbiAgZ2V0Q2xkclBhdHRlcm4oaXNOZWdhdGl2ZSkge1xuICAgIGlmIChpc05lZ2F0aXZlKSB7XG4gICAgICByZXR1cm4gdGhpcy5udW1iZXJTcGVjaWZpY2F0aW9uLmdldE5lZ2F0aXZlUGF0dGVybigpO1xuICAgIH1cblxuICAgIHJldHVybiB0aGlzLm51bWJlclNwZWNpZmljYXRpb24uZ2V0UG9zaXRpdmVQYXR0ZXJuKCk7XG4gIH1cblxuICAvKipcbiAgICogUmVwbGFjZSBwbGFjZWhvbGRlciBudW1iZXIgc3ltYm9scyB3aXRoIHJlbGV2YW50IG51bWJlcmluZyBzeXN0ZW0ncyBzeW1ib2xzLlxuICAgKlxuICAgKiBAcGFyYW0gc3RyaW5nIG51bWJlclxuICAgKiAgICAgICAgICAgICAgICAgICAgICAgVGhlIG51bWJlciB0byBwcm9jZXNzXG4gICAqXG4gICAqIEByZXR1cm4gc3RyaW5nXG4gICAqICAgICAgICAgICAgICAgIFRoZSBudW1iZXIgd2l0aCByZXBsYWNlZCBzeW1ib2xzXG4gICAqL1xuICByZXBsYWNlU3ltYm9scyhudW1iZXIpIHtcbiAgICBjb25zdCBzeW1ib2xzID0gdGhpcy5udW1iZXJTcGVjaWZpY2F0aW9uLmdldFN5bWJvbCgpO1xuXG4gICAgY29uc3QgbWFwID0ge307XG4gICAgbWFwW0RFQ0lNQUxfU0VQQVJBVE9SX1BMQUNFSE9MREVSXSA9IHN5bWJvbHMuZ2V0RGVjaW1hbCgpO1xuICAgIG1hcFtHUk9VUF9TRVBBUkFUT1JfUExBQ0VIT0xERVJdID0gc3ltYm9scy5nZXRHcm91cCgpO1xuICAgIG1hcFtNSU5VU19TSUdOX1BMQUNFSE9MREVSXSA9IHN5bWJvbHMuZ2V0TWludXNTaWduKCk7XG4gICAgbWFwW1BFUkNFTlRfU1lNQk9MX1BMQUNFSE9MREVSXSA9IHN5bWJvbHMuZ2V0UGVyY2VudFNpZ24oKTtcbiAgICBtYXBbUExVU19TSUdOX1BMQUNFSE9MREVSXSA9IHN5bWJvbHMuZ2V0UGx1c1NpZ24oKTtcblxuICAgIHJldHVybiB0aGlzLnN0cnRyKG51bWJlciwgbWFwKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBzdHJ0cigpIGZvciBKYXZhU2NyaXB0XG4gICAqIFRyYW5zbGF0ZSBjaGFyYWN0ZXJzIG9yIHJlcGxhY2Ugc3Vic3RyaW5nc1xuICAgKlxuICAgKiBAcGFyYW0gc3RyXG4gICAqICBTdHJpbmcgdG8gcGFyc2VcbiAgICogQHBhcmFtIHBhaXJzXG4gICAqICBIYXNoIG9mICgnZnJvbScgPT4gJ3RvJywgLi4uKS5cbiAgICpcbiAgICogQHJldHVybiBzdHJpbmdcbiAgICovXG4gIHN0cnRyKHN0ciwgcGFpcnMpIHtcbiAgICBjb25zdCBzdWJzdHJzID0gT2JqZWN0LmtleXMocGFpcnMpLm1hcChlc2NhcGVSRSk7XG5cbiAgICByZXR1cm4gc3RyLnNwbGl0KFJlZ0V4cChgKCR7c3Vic3Rycy5qb2luKCd8Jyl9KWApKVxuICAgICAgLm1hcCgocGFydCkgPT4gcGFpcnNbcGFydF0gfHwgcGFydClcbiAgICAgIC5qb2luKCcnKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBBZGQgbWlzc2luZyBwbGFjZWhvbGRlcnMgdG8gdGhlIG51bWJlciB1c2luZyB0aGUgcGFzc2VkIENMRFIgcGF0dGVybi5cbiAgICpcbiAgICogTWlzc2luZyBwbGFjZWhvbGRlcnMgY2FuIGJlIHRoZSBwZXJjZW50IHNpZ24sIGN1cnJlbmN5IHN5bWJvbCwgZXRjLlxuICAgKlxuICAgKiBlLmcuIHdpdGggYSBjdXJyZW5jeSBDTERSIHBhdHRlcm46XG4gICAqICAtIFBhc3NlZCBudW1iZXIgKHBhcnRpYWxseSBmb3JtYXR0ZWQpOiAxLDIzNC41NjdcbiAgICogIC0gUmV0dXJuZWQgbnVtYmVyOiAxLDIzNC41NjcgwqRcbiAgICogIChcIsKkXCIgc3ltYm9sIGlzIHRoZSBjdXJyZW5jeSBzeW1ib2wgcGxhY2Vob2xkZXIpXG4gICAqXG4gICAqIEBzZWUgaHR0cDovL2NsZHIudW5pY29kZS5vcmcvdHJhbnNsYXRpb24vbnVtYmVyLXBhdHRlcm5zXG4gICAqXG4gICAqIEBwYXJhbSBmb3JtYXR0ZWROdW1iZXJcbiAgICogIE51bWJlciB0byBwcm9jZXNzXG4gICAqIEBwYXJhbSBwYXR0ZXJuXG4gICAqICBDTERSIGZvcm1hdHRpbmcgcGF0dGVybiB0byB1c2VcbiAgICpcbiAgICogQHJldHVybiBzdHJpbmdcbiAgICovXG4gIGFkZFBsYWNlaG9sZGVycyhmb3JtYXR0ZWROdW1iZXIsIHBhdHRlcm4pIHtcbiAgICAvKlxuICAgICAqIFJlZ2V4IGdyb3VwcyBleHBsYW5hdGlvbjpcbiAgICAgKiAjICAgICAgICAgIDogbGl0ZXJhbCBcIiNcIiBjaGFyYWN0ZXIuIE9uY2UuXG4gICAgICogKCwjKykqICAgICA6IGFueSBvdGhlciBcIiNcIiBjaGFyYWN0ZXJzIGdyb3VwLCBzZXBhcmF0ZWQgYnkgXCIsXCIuIFplcm8gdG8gaW5maW5pdHkgdGltZXMuXG4gICAgICogMCAgICAgICAgICA6IGxpdGVyYWwgXCIwXCIgY2hhcmFjdGVyLiBPbmNlLlxuICAgICAqIChcXC5bMCNdKykqIDogYW55IGNvbWJpbmF0aW9uIG9mIFwiMFwiIGFuZCBcIiNcIiBjaGFyYWN0ZXJzIGdyb3Vwcywgc2VwYXJhdGVkIGJ5ICcuJy5cbiAgICAgKiAgICAgICAgICAgICAgWmVybyB0byBpbmZpbml0eSB0aW1lcy5cbiAgICAgKi9cbiAgICByZXR1cm4gcGF0dGVybi5yZXBsYWNlKC8jPygsIyspKjAoXFwuWzAjXSspKi8sIGZvcm1hdHRlZE51bWJlcik7XG4gIH1cblxuICAvKipcbiAgICogUGVyZm9ybSBzb21lIG1vcmUgc3BlY2lmaWMgcmVwbGFjZW1lbnRzLlxuICAgKlxuICAgKiBTcGVjaWZpYyByZXBsYWNlbWVudHMgYXJlIG5lZWRlZCB3aGVuIG51bWJlciBzcGVjaWZpY2F0aW9uIGlzIGV4dGVuZGVkLlxuICAgKiBGb3IgaW5zdGFuY2UsIHByaWNlcyBoYXZlIGFuIGV4dGVuZGVkIG51bWJlciBzcGVjaWZpY2F0aW9uIGluIG9yZGVyIHRvXG4gICAqIGFkZCBjdXJyZW5jeSBzeW1ib2wgdG8gdGhlIGZvcm1hdHRlZCBudW1iZXIuXG4gICAqXG4gICAqIEBwYXJhbSBzdHJpbmcgZm9ybWF0dGVkTnVtYmVyXG4gICAqXG4gICAqIEByZXR1cm4gbWl4ZWRcbiAgICovXG4gIHBlcmZvcm1TcGVjaWZpY1JlcGxhY2VtZW50cyhmb3JtYXR0ZWROdW1iZXIpIHtcbiAgICBpZiAodGhpcy5udW1iZXJTcGVjaWZpY2F0aW9uIGluc3RhbmNlb2YgUHJpY2VTcGVjaWZpY2F0aW9uKSB7XG4gICAgICByZXR1cm4gZm9ybWF0dGVkTnVtYmVyXG4gICAgICAgIC5zcGxpdChDVVJSRU5DWV9TWU1CT0xfUExBQ0VIT0xERVIpXG4gICAgICAgIC5qb2luKHRoaXMubnVtYmVyU3BlY2lmaWNhdGlvbi5nZXRDdXJyZW5jeVN5bWJvbCgpKTtcbiAgICB9XG5cbiAgICByZXR1cm4gZm9ybWF0dGVkTnVtYmVyO1xuICB9XG5cbiAgc3RhdGljIGJ1aWxkKHNwZWNpZmljYXRpb25zKSB7XG4gICAgbGV0IHN5bWJvbDtcblxuICAgIGlmICh1bmRlZmluZWQgIT09IHNwZWNpZmljYXRpb25zLm51bWJlclN5bWJvbHMpIHtcbiAgICAgIHN5bWJvbCA9IG5ldyBOdW1iZXJTeW1ib2woLi4uc3BlY2lmaWNhdGlvbnMubnVtYmVyU3ltYm9scyk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHN5bWJvbCA9IG5ldyBOdW1iZXJTeW1ib2woLi4uc3BlY2lmaWNhdGlvbnMuc3ltYm9sKTtcbiAgICB9XG5cbiAgICBsZXQgc3BlY2lmaWNhdGlvbjtcblxuICAgIGlmIChzcGVjaWZpY2F0aW9ucy5jdXJyZW5jeVN5bWJvbCkge1xuICAgICAgc3BlY2lmaWNhdGlvbiA9IG5ldyBQcmljZVNwZWNpZmljYXRpb24oXG4gICAgICAgIHNwZWNpZmljYXRpb25zLnBvc2l0aXZlUGF0dGVybixcbiAgICAgICAgc3BlY2lmaWNhdGlvbnMubmVnYXRpdmVQYXR0ZXJuLFxuICAgICAgICBzeW1ib2wsXG4gICAgICAgIHBhcnNlSW50KHNwZWNpZmljYXRpb25zLm1heEZyYWN0aW9uRGlnaXRzLCAxMCksXG4gICAgICAgIHBhcnNlSW50KHNwZWNpZmljYXRpb25zLm1pbkZyYWN0aW9uRGlnaXRzLCAxMCksXG4gICAgICAgIHNwZWNpZmljYXRpb25zLmdyb3VwaW5nVXNlZCxcbiAgICAgICAgc3BlY2lmaWNhdGlvbnMucHJpbWFyeUdyb3VwU2l6ZSxcbiAgICAgICAgc3BlY2lmaWNhdGlvbnMuc2Vjb25kYXJ5R3JvdXBTaXplLFxuICAgICAgICBzcGVjaWZpY2F0aW9ucy5jdXJyZW5jeVN5bWJvbCxcbiAgICAgICAgc3BlY2lmaWNhdGlvbnMuY3VycmVuY3lDb2RlLFxuICAgICAgKTtcbiAgICB9IGVsc2Uge1xuICAgICAgc3BlY2lmaWNhdGlvbiA9IG5ldyBOdW1iZXJTcGVjaWZpY2F0aW9uKFxuICAgICAgICBzcGVjaWZpY2F0aW9ucy5wb3NpdGl2ZVBhdHRlcm4sXG4gICAgICAgIHNwZWNpZmljYXRpb25zLm5lZ2F0aXZlUGF0dGVybixcbiAgICAgICAgc3ltYm9sLFxuICAgICAgICBwYXJzZUludChzcGVjaWZpY2F0aW9ucy5tYXhGcmFjdGlvbkRpZ2l0cywgMTApLFxuICAgICAgICBwYXJzZUludChzcGVjaWZpY2F0aW9ucy5taW5GcmFjdGlvbkRpZ2l0cywgMTApLFxuICAgICAgICBzcGVjaWZpY2F0aW9ucy5ncm91cGluZ1VzZWQsXG4gICAgICAgIHNwZWNpZmljYXRpb25zLnByaW1hcnlHcm91cFNpemUsXG4gICAgICAgIHNwZWNpZmljYXRpb25zLnNlY29uZGFyeUdyb3VwU2l6ZSxcbiAgICAgICk7XG4gICAgfVxuXG4gICAgcmV0dXJuIG5ldyBOdW1iZXJGb3JtYXR0ZXIoc3BlY2lmaWNhdGlvbik7XG4gIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgTnVtYmVyRm9ybWF0dGVyO1xuIiwiLyoqXG4gKiBDb3B5cmlnaHQgc2luY2UgMjAwNyBQcmVzdGFTaG9wIFNBIGFuZCBDb250cmlidXRvcnNcbiAqIFByZXN0YVNob3AgaXMgYW4gSW50ZXJuYXRpb25hbCBSZWdpc3RlcmVkIFRyYWRlbWFyayAmIFByb3BlcnR5IG9mIFByZXN0YVNob3AgU0FcbiAqXG4gKiBOT1RJQ0UgT0YgTElDRU5TRVxuICpcbiAqIFRoaXMgc291cmNlIGZpbGUgaXMgc3ViamVjdCB0byB0aGUgT3BlbiBTb2Z0d2FyZSBMaWNlbnNlIChPU0wgMy4wKVxuICogdGhhdCBpcyBidW5kbGVkIHdpdGggdGhpcyBwYWNrYWdlIGluIHRoZSBmaWxlIExJQ0VOU0UubWQuXG4gKiBJdCBpcyBhbHNvIGF2YWlsYWJsZSB0aHJvdWdoIHRoZSB3b3JsZC13aWRlLXdlYiBhdCB0aGlzIFVSTDpcbiAqIGh0dHBzOi8vb3BlbnNvdXJjZS5vcmcvbGljZW5zZXMvT1NMLTMuMFxuICogSWYgeW91IGRpZCBub3QgcmVjZWl2ZSBhIGNvcHkgb2YgdGhlIGxpY2Vuc2UgYW5kIGFyZSB1bmFibGUgdG9cbiAqIG9idGFpbiBpdCB0aHJvdWdoIHRoZSB3b3JsZC13aWRlLXdlYiwgcGxlYXNlIHNlbmQgYW4gZW1haWxcbiAqIHRvIGxpY2Vuc2VAcHJlc3Rhc2hvcC5jb20gc28gd2UgY2FuIHNlbmQgeW91IGEgY29weSBpbW1lZGlhdGVseS5cbiAqXG4gKiBESVNDTEFJTUVSXG4gKlxuICogRG8gbm90IGVkaXQgb3IgYWRkIHRvIHRoaXMgZmlsZSBpZiB5b3Ugd2lzaCB0byB1cGdyYWRlIFByZXN0YVNob3AgdG8gbmV3ZXJcbiAqIHZlcnNpb25zIGluIHRoZSBmdXR1cmUuIElmIHlvdSB3aXNoIHRvIGN1c3RvbWl6ZSBQcmVzdGFTaG9wIGZvciB5b3VyXG4gKiBuZWVkcyBwbGVhc2UgcmVmZXIgdG8gaHR0cHM6Ly9kZXZkb2NzLnByZXN0YXNob3AuY29tLyBmb3IgbW9yZSBpbmZvcm1hdGlvbi5cbiAqXG4gKiBAYXV0aG9yICAgIFByZXN0YVNob3AgU0EgYW5kIENvbnRyaWJ1dG9ycyA8Y29udGFjdEBwcmVzdGFzaG9wLmNvbT5cbiAqIEBjb3B5cmlnaHQgU2luY2UgMjAwNyBQcmVzdGFTaG9wIFNBIGFuZCBDb250cmlidXRvcnNcbiAqIEBsaWNlbnNlICAgaHR0cHM6Ly9vcGVuc291cmNlLm9yZy9saWNlbnNlcy9PU0wtMy4wIE9wZW4gU29mdHdhcmUgTGljZW5zZSAoT1NMIDMuMClcbiAqL1xuaW1wb3J0IExvY2FsaXphdGlvbkV4Y2VwdGlvbiBmcm9tICdAYXBwL2NsZHIvZXhjZXB0aW9uL2xvY2FsaXphdGlvbic7XG5cbmNsYXNzIE51bWJlclN5bWJvbCB7XG4gIC8qKlxuICAgKiBOdW1iZXJTeW1ib2xMaXN0IGNvbnN0cnVjdG9yLlxuICAgKlxuICAgKiBAcGFyYW0gc3RyaW5nIGRlY2ltYWwgRGVjaW1hbCBzZXBhcmF0b3IgY2hhcmFjdGVyXG4gICAqIEBwYXJhbSBzdHJpbmcgZ3JvdXAgRGlnaXRzIGdyb3VwIHNlcGFyYXRvciBjaGFyYWN0ZXJcbiAgICogQHBhcmFtIHN0cmluZyBsaXN0IExpc3QgZWxlbWVudHMgc2VwYXJhdG9yIGNoYXJhY3RlclxuICAgKiBAcGFyYW0gc3RyaW5nIHBlcmNlbnRTaWduIFBlcmNlbnQgc2lnbiBjaGFyYWN0ZXJcbiAgICogQHBhcmFtIHN0cmluZyBtaW51c1NpZ24gTWludXMgc2lnbiBjaGFyYWN0ZXJcbiAgICogQHBhcmFtIHN0cmluZyBwbHVzU2lnbiBQbHVzIHNpZ24gY2hhcmFjdGVyXG4gICAqIEBwYXJhbSBzdHJpbmcgZXhwb25lbnRpYWwgRXhwb25lbnRpYWwgY2hhcmFjdGVyXG4gICAqIEBwYXJhbSBzdHJpbmcgc3VwZXJzY3JpcHRpbmdFeHBvbmVudCBTdXBlcnNjcmlwdGluZyBleHBvbmVudCBjaGFyYWN0ZXJcbiAgICogQHBhcmFtIHN0cmluZyBwZXJNaWxsZSBQZXJtaWxsZSBzaWduIGNoYXJhY3RlclxuICAgKiBAcGFyYW0gc3RyaW5nIGluZmluaXR5IFRoZSBpbmZpbml0eSBzaWduLiBDb3JyZXNwb25kcyB0byB0aGUgSUVFRSBpbmZpbml0eSBiaXQgcGF0dGVybi5cbiAgICogQHBhcmFtIHN0cmluZyBuYW4gVGhlIE5hTiAoTm90IEEgTnVtYmVyKSBzaWduLiBDb3JyZXNwb25kcyB0byB0aGUgSUVFRSBOYU4gYml0IHBhdHRlcm4uXG4gICAqXG4gICAqIEB0aHJvd3MgTG9jYWxpemF0aW9uRXhjZXB0aW9uXG4gICAqL1xuICBjb25zdHJ1Y3RvcihcbiAgICBkZWNpbWFsLFxuICAgIGdyb3VwLFxuICAgIGxpc3QsXG4gICAgcGVyY2VudFNpZ24sXG4gICAgbWludXNTaWduLFxuICAgIHBsdXNTaWduLFxuICAgIGV4cG9uZW50aWFsLFxuICAgIHN1cGVyc2NyaXB0aW5nRXhwb25lbnQsXG4gICAgcGVyTWlsbGUsXG4gICAgaW5maW5pdHksXG4gICAgbmFuLFxuICApIHtcbiAgICB0aGlzLmRlY2ltYWwgPSBkZWNpbWFsO1xuICAgIHRoaXMuZ3JvdXAgPSBncm91cDtcbiAgICB0aGlzLmxpc3QgPSBsaXN0O1xuICAgIHRoaXMucGVyY2VudFNpZ24gPSBwZXJjZW50U2lnbjtcbiAgICB0aGlzLm1pbnVzU2lnbiA9IG1pbnVzU2lnbjtcbiAgICB0aGlzLnBsdXNTaWduID0gcGx1c1NpZ247XG4gICAgdGhpcy5leHBvbmVudGlhbCA9IGV4cG9uZW50aWFsO1xuICAgIHRoaXMuc3VwZXJzY3JpcHRpbmdFeHBvbmVudCA9IHN1cGVyc2NyaXB0aW5nRXhwb25lbnQ7XG4gICAgdGhpcy5wZXJNaWxsZSA9IHBlck1pbGxlO1xuICAgIHRoaXMuaW5maW5pdHkgPSBpbmZpbml0eTtcbiAgICB0aGlzLm5hbiA9IG5hbjtcblxuICAgIHRoaXMudmFsaWRhdGVEYXRhKCk7XG4gIH1cblxuICAvKipcbiAgICogR2V0IHRoZSBkZWNpbWFsIHNlcGFyYXRvci5cbiAgICpcbiAgICogQHJldHVybiBzdHJpbmdcbiAgICovXG4gIGdldERlY2ltYWwoKSB7XG4gICAgcmV0dXJuIHRoaXMuZGVjaW1hbDtcbiAgfVxuXG4gIC8qKlxuICAgKiBHZXQgdGhlIGRpZ2l0IGdyb3VwcyBzZXBhcmF0b3IuXG4gICAqXG4gICAqIEByZXR1cm4gc3RyaW5nXG4gICAqL1xuICBnZXRHcm91cCgpIHtcbiAgICByZXR1cm4gdGhpcy5ncm91cDtcbiAgfVxuXG4gIC8qKlxuICAgKiBHZXQgdGhlIGxpc3QgZWxlbWVudHMgc2VwYXJhdG9yLlxuICAgKlxuICAgKiBAcmV0dXJuIHN0cmluZ1xuICAgKi9cbiAgZ2V0TGlzdCgpIHtcbiAgICByZXR1cm4gdGhpcy5saXN0O1xuICB9XG5cbiAgLyoqXG4gICAqIEdldCB0aGUgcGVyY2VudCBzaWduLlxuICAgKlxuICAgKiBAcmV0dXJuIHN0cmluZ1xuICAgKi9cbiAgZ2V0UGVyY2VudFNpZ24oKSB7XG4gICAgcmV0dXJuIHRoaXMucGVyY2VudFNpZ247XG4gIH1cblxuICAvKipcbiAgICogR2V0IHRoZSBtaW51cyBzaWduLlxuICAgKlxuICAgKiBAcmV0dXJuIHN0cmluZ1xuICAgKi9cbiAgZ2V0TWludXNTaWduKCkge1xuICAgIHJldHVybiB0aGlzLm1pbnVzU2lnbjtcbiAgfVxuXG4gIC8qKlxuICAgKiBHZXQgdGhlIHBsdXMgc2lnbi5cbiAgICpcbiAgICogQHJldHVybiBzdHJpbmdcbiAgICovXG4gIGdldFBsdXNTaWduKCkge1xuICAgIHJldHVybiB0aGlzLnBsdXNTaWduO1xuICB9XG5cbiAgLyoqXG4gICAqIEdldCB0aGUgZXhwb25lbnRpYWwgY2hhcmFjdGVyLlxuICAgKlxuICAgKiBAcmV0dXJuIHN0cmluZ1xuICAgKi9cbiAgZ2V0RXhwb25lbnRpYWwoKSB7XG4gICAgcmV0dXJuIHRoaXMuZXhwb25lbnRpYWw7XG4gIH1cblxuICAvKipcbiAgICogR2V0IHRoZSBleHBvbmVudCBjaGFyYWN0ZXIuXG4gICAqXG4gICAqIEByZXR1cm4gc3RyaW5nXG4gICAqL1xuICBnZXRTdXBlcnNjcmlwdGluZ0V4cG9uZW50KCkge1xuICAgIHJldHVybiB0aGlzLnN1cGVyc2NyaXB0aW5nRXhwb25lbnQ7XG4gIH1cblxuICAvKipcbiAgICogR2VydCB0aGUgcGVyIG1pbGxlIHN5bWJvbCAob2Z0ZW4gXCLigLBcIikuXG4gICAqXG4gICAqIEBzZWUgaHR0cHM6Ly9lbi53aWtpcGVkaWEub3JnL3dpa2kvUGVyX21pbGxlXG4gICAqXG4gICAqIEByZXR1cm4gc3RyaW5nXG4gICAqL1xuICBnZXRQZXJNaWxsZSgpIHtcbiAgICByZXR1cm4gdGhpcy5wZXJNaWxsZTtcbiAgfVxuXG4gIC8qKlxuICAgKiBHZXQgdGhlIGluZmluaXR5IHN5bWJvbCAob2Z0ZW4gXCLiiJ5cIikuXG4gICAqXG4gICAqIEBzZWUgaHR0cHM6Ly9lbi53aWtpcGVkaWEub3JnL3dpa2kvSW5maW5pdHlfc3ltYm9sXG4gICAqXG4gICAqIEByZXR1cm4gc3RyaW5nXG4gICAqL1xuICBnZXRJbmZpbml0eSgpIHtcbiAgICByZXR1cm4gdGhpcy5pbmZpbml0eTtcbiAgfVxuXG4gIC8qKlxuICAgKiBHZXQgdGhlIE5hTiAobm90IGEgbnVtYmVyKSBzaWduLlxuICAgKlxuICAgKiBAcmV0dXJuIHN0cmluZ1xuICAgKi9cbiAgZ2V0TmFuKCkge1xuICAgIHJldHVybiB0aGlzLm5hbjtcbiAgfVxuXG4gIC8qKlxuICAgKiBTeW1ib2xzIGxpc3QgdmFsaWRhdGlvbi5cbiAgICpcbiAgICogQHRocm93cyBMb2NhbGl6YXRpb25FeGNlcHRpb25cbiAgICovXG4gIHZhbGlkYXRlRGF0YSgpIHtcbiAgICBpZiAoIXRoaXMuZGVjaW1hbCB8fCB0eXBlb2YgdGhpcy5kZWNpbWFsICE9PSAnc3RyaW5nJykge1xuICAgICAgdGhyb3cgbmV3IExvY2FsaXphdGlvbkV4Y2VwdGlvbignSW52YWxpZCBkZWNpbWFsJyk7XG4gICAgfVxuXG4gICAgaWYgKCF0aGlzLmdyb3VwIHx8IHR5cGVvZiB0aGlzLmdyb3VwICE9PSAnc3RyaW5nJykge1xuICAgICAgdGhyb3cgbmV3IExvY2FsaXphdGlvbkV4Y2VwdGlvbignSW52YWxpZCBncm91cCcpO1xuICAgIH1cblxuICAgIGlmICghdGhpcy5saXN0IHx8IHR5cGVvZiB0aGlzLmxpc3QgIT09ICdzdHJpbmcnKSB7XG4gICAgICB0aHJvdyBuZXcgTG9jYWxpemF0aW9uRXhjZXB0aW9uKCdJbnZhbGlkIHN5bWJvbCBsaXN0Jyk7XG4gICAgfVxuXG4gICAgaWYgKCF0aGlzLnBlcmNlbnRTaWduIHx8IHR5cGVvZiB0aGlzLnBlcmNlbnRTaWduICE9PSAnc3RyaW5nJykge1xuICAgICAgdGhyb3cgbmV3IExvY2FsaXphdGlvbkV4Y2VwdGlvbignSW52YWxpZCBwZXJjZW50U2lnbicpO1xuICAgIH1cblxuICAgIGlmICghdGhpcy5taW51c1NpZ24gfHwgdHlwZW9mIHRoaXMubWludXNTaWduICE9PSAnc3RyaW5nJykge1xuICAgICAgdGhyb3cgbmV3IExvY2FsaXphdGlvbkV4Y2VwdGlvbignSW52YWxpZCBtaW51c1NpZ24nKTtcbiAgICB9XG5cbiAgICBpZiAoIXRoaXMucGx1c1NpZ24gfHwgdHlwZW9mIHRoaXMucGx1c1NpZ24gIT09ICdzdHJpbmcnKSB7XG4gICAgICB0aHJvdyBuZXcgTG9jYWxpemF0aW9uRXhjZXB0aW9uKCdJbnZhbGlkIHBsdXNTaWduJyk7XG4gICAgfVxuXG4gICAgaWYgKCF0aGlzLmV4cG9uZW50aWFsIHx8IHR5cGVvZiB0aGlzLmV4cG9uZW50aWFsICE9PSAnc3RyaW5nJykge1xuICAgICAgdGhyb3cgbmV3IExvY2FsaXphdGlvbkV4Y2VwdGlvbignSW52YWxpZCBleHBvbmVudGlhbCcpO1xuICAgIH1cblxuICAgIGlmICghdGhpcy5zdXBlcnNjcmlwdGluZ0V4cG9uZW50IHx8IHR5cGVvZiB0aGlzLnN1cGVyc2NyaXB0aW5nRXhwb25lbnQgIT09ICdzdHJpbmcnKSB7XG4gICAgICB0aHJvdyBuZXcgTG9jYWxpemF0aW9uRXhjZXB0aW9uKCdJbnZhbGlkIHN1cGVyc2NyaXB0aW5nRXhwb25lbnQnKTtcbiAgICB9XG5cbiAgICBpZiAoIXRoaXMucGVyTWlsbGUgfHwgdHlwZW9mIHRoaXMucGVyTWlsbGUgIT09ICdzdHJpbmcnKSB7XG4gICAgICB0aHJvdyBuZXcgTG9jYWxpemF0aW9uRXhjZXB0aW9uKCdJbnZhbGlkIHBlck1pbGxlJyk7XG4gICAgfVxuXG4gICAgaWYgKCF0aGlzLmluZmluaXR5IHx8IHR5cGVvZiB0aGlzLmluZmluaXR5ICE9PSAnc3RyaW5nJykge1xuICAgICAgdGhyb3cgbmV3IExvY2FsaXphdGlvbkV4Y2VwdGlvbignSW52YWxpZCBpbmZpbml0eScpO1xuICAgIH1cblxuICAgIGlmICghdGhpcy5uYW4gfHwgdHlwZW9mIHRoaXMubmFuICE9PSAnc3RyaW5nJykge1xuICAgICAgdGhyb3cgbmV3IExvY2FsaXphdGlvbkV4Y2VwdGlvbignSW52YWxpZCBuYW4nKTtcbiAgICB9XG4gIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgTnVtYmVyU3ltYm9sO1xuIiwiLyoqXG4gKiBDb3B5cmlnaHQgc2luY2UgMjAwNyBQcmVzdGFTaG9wIFNBIGFuZCBDb250cmlidXRvcnNcbiAqIFByZXN0YVNob3AgaXMgYW4gSW50ZXJuYXRpb25hbCBSZWdpc3RlcmVkIFRyYWRlbWFyayAmIFByb3BlcnR5IG9mIFByZXN0YVNob3AgU0FcbiAqXG4gKiBOT1RJQ0UgT0YgTElDRU5TRVxuICpcbiAqIFRoaXMgc291cmNlIGZpbGUgaXMgc3ViamVjdCB0byB0aGUgT3BlbiBTb2Z0d2FyZSBMaWNlbnNlIChPU0wgMy4wKVxuICogdGhhdCBpcyBidW5kbGVkIHdpdGggdGhpcyBwYWNrYWdlIGluIHRoZSBmaWxlIExJQ0VOU0UubWQuXG4gKiBJdCBpcyBhbHNvIGF2YWlsYWJsZSB0aHJvdWdoIHRoZSB3b3JsZC13aWRlLXdlYiBhdCB0aGlzIFVSTDpcbiAqIGh0dHBzOi8vb3BlbnNvdXJjZS5vcmcvbGljZW5zZXMvT1NMLTMuMFxuICogSWYgeW91IGRpZCBub3QgcmVjZWl2ZSBhIGNvcHkgb2YgdGhlIGxpY2Vuc2UgYW5kIGFyZSB1bmFibGUgdG9cbiAqIG9idGFpbiBpdCB0aHJvdWdoIHRoZSB3b3JsZC13aWRlLXdlYiwgcGxlYXNlIHNlbmQgYW4gZW1haWxcbiAqIHRvIGxpY2Vuc2VAcHJlc3Rhc2hvcC5jb20gc28gd2UgY2FuIHNlbmQgeW91IGEgY29weSBpbW1lZGlhdGVseS5cbiAqXG4gKiBESVNDTEFJTUVSXG4gKlxuICogRG8gbm90IGVkaXQgb3IgYWRkIHRvIHRoaXMgZmlsZSBpZiB5b3Ugd2lzaCB0byB1cGdyYWRlIFByZXN0YVNob3AgdG8gbmV3ZXJcbiAqIHZlcnNpb25zIGluIHRoZSBmdXR1cmUuIElmIHlvdSB3aXNoIHRvIGN1c3RvbWl6ZSBQcmVzdGFTaG9wIGZvciB5b3VyXG4gKiBuZWVkcyBwbGVhc2UgcmVmZXIgdG8gaHR0cHM6Ly9kZXZkb2NzLnByZXN0YXNob3AuY29tLyBmb3IgbW9yZSBpbmZvcm1hdGlvbi5cbiAqXG4gKiBAYXV0aG9yICAgIFByZXN0YVNob3AgU0EgYW5kIENvbnRyaWJ1dG9ycyA8Y29udGFjdEBwcmVzdGFzaG9wLmNvbT5cbiAqIEBjb3B5cmlnaHQgU2luY2UgMjAwNyBQcmVzdGFTaG9wIFNBIGFuZCBDb250cmlidXRvcnNcbiAqIEBsaWNlbnNlICAgaHR0cHM6Ly9vcGVuc291cmNlLm9yZy9saWNlbnNlcy9PU0wtMy4wIE9wZW4gU29mdHdhcmUgTGljZW5zZSAoT1NMIDMuMClcbiAqL1xuaW1wb3J0IExvY2FsaXphdGlvbkV4Y2VwdGlvbiBmcm9tICdAYXBwL2NsZHIvZXhjZXB0aW9uL2xvY2FsaXphdGlvbic7XG5pbXBvcnQgTnVtYmVyU3ltYm9sIGZyb20gJ0BhcHAvY2xkci9udW1iZXItc3ltYm9sJztcblxuY2xhc3MgTnVtYmVyU3BlY2lmaWNhdGlvbiB7XG4gIC8qKlxuICAgKiBOdW1iZXIgc3BlY2lmaWNhdGlvbiBjb25zdHJ1Y3Rvci5cbiAgICpcbiAgICogQHBhcmFtIHN0cmluZyBwb3NpdGl2ZVBhdHRlcm4gQ0xEUiBmb3JtYXR0aW5nIHBhdHRlcm4gZm9yIHBvc2l0aXZlIGFtb3VudHNcbiAgICogQHBhcmFtIHN0cmluZyBuZWdhdGl2ZVBhdHRlcm4gQ0xEUiBmb3JtYXR0aW5nIHBhdHRlcm4gZm9yIG5lZ2F0aXZlIGFtb3VudHNcbiAgICogQHBhcmFtIE51bWJlclN5bWJvbCBzeW1ib2wgTnVtYmVyIHN5bWJvbFxuICAgKiBAcGFyYW0gaW50IG1heEZyYWN0aW9uRGlnaXRzIE1heGltdW0gbnVtYmVyIG9mIGRpZ2l0cyBhZnRlciBkZWNpbWFsIHNlcGFyYXRvclxuICAgKiBAcGFyYW0gaW50IG1pbkZyYWN0aW9uRGlnaXRzIE1pbmltdW0gbnVtYmVyIG9mIGRpZ2l0cyBhZnRlciBkZWNpbWFsIHNlcGFyYXRvclxuICAgKiBAcGFyYW0gYm9vbCBncm91cGluZ1VzZWQgSXMgZGlnaXRzIGdyb3VwaW5nIHVzZWQgP1xuICAgKiBAcGFyYW0gaW50IHByaW1hcnlHcm91cFNpemUgU2l6ZSBvZiBwcmltYXJ5IGRpZ2l0cyBncm91cCBpbiB0aGUgbnVtYmVyXG4gICAqIEBwYXJhbSBpbnQgc2Vjb25kYXJ5R3JvdXBTaXplIFNpemUgb2Ygc2Vjb25kYXJ5IGRpZ2l0cyBncm91cCBpbiB0aGUgbnVtYmVyXG4gICAqXG4gICAqIEB0aHJvd3MgTG9jYWxpemF0aW9uRXhjZXB0aW9uXG4gICAqL1xuICBjb25zdHJ1Y3RvcihcbiAgICBwb3NpdGl2ZVBhdHRlcm4sXG4gICAgbmVnYXRpdmVQYXR0ZXJuLFxuICAgIHN5bWJvbCxcbiAgICBtYXhGcmFjdGlvbkRpZ2l0cyxcbiAgICBtaW5GcmFjdGlvbkRpZ2l0cyxcbiAgICBncm91cGluZ1VzZWQsXG4gICAgcHJpbWFyeUdyb3VwU2l6ZSxcbiAgICBzZWNvbmRhcnlHcm91cFNpemUsXG4gICkge1xuICAgIHRoaXMucG9zaXRpdmVQYXR0ZXJuID0gcG9zaXRpdmVQYXR0ZXJuO1xuICAgIHRoaXMubmVnYXRpdmVQYXR0ZXJuID0gbmVnYXRpdmVQYXR0ZXJuO1xuICAgIHRoaXMuc3ltYm9sID0gc3ltYm9sO1xuXG4gICAgdGhpcy5tYXhGcmFjdGlvbkRpZ2l0cyA9IG1heEZyYWN0aW9uRGlnaXRzO1xuICAgIC8vIGVzbGludC1kaXNhYmxlLW5leHQtbGluZVxuICAgIHRoaXMubWluRnJhY3Rpb25EaWdpdHMgPSBtYXhGcmFjdGlvbkRpZ2l0cyA8IG1pbkZyYWN0aW9uRGlnaXRzID8gbWF4RnJhY3Rpb25EaWdpdHMgOiBtaW5GcmFjdGlvbkRpZ2l0cztcblxuICAgIHRoaXMuZ3JvdXBpbmdVc2VkID0gZ3JvdXBpbmdVc2VkO1xuICAgIHRoaXMucHJpbWFyeUdyb3VwU2l6ZSA9IHByaW1hcnlHcm91cFNpemU7XG4gICAgdGhpcy5zZWNvbmRhcnlHcm91cFNpemUgPSBzZWNvbmRhcnlHcm91cFNpemU7XG5cbiAgICBpZiAoIXRoaXMucG9zaXRpdmVQYXR0ZXJuIHx8IHR5cGVvZiB0aGlzLnBvc2l0aXZlUGF0dGVybiAhPT0gJ3N0cmluZycpIHtcbiAgICAgIHRocm93IG5ldyBMb2NhbGl6YXRpb25FeGNlcHRpb24oJ0ludmFsaWQgcG9zaXRpdmVQYXR0ZXJuJyk7XG4gICAgfVxuXG4gICAgaWYgKCF0aGlzLm5lZ2F0aXZlUGF0dGVybiB8fCB0eXBlb2YgdGhpcy5uZWdhdGl2ZVBhdHRlcm4gIT09ICdzdHJpbmcnKSB7XG4gICAgICB0aHJvdyBuZXcgTG9jYWxpemF0aW9uRXhjZXB0aW9uKCdJbnZhbGlkIG5lZ2F0aXZlUGF0dGVybicpO1xuICAgIH1cblxuICAgIGlmICghdGhpcy5zeW1ib2wgfHwgISh0aGlzLnN5bWJvbCBpbnN0YW5jZW9mIE51bWJlclN5bWJvbCkpIHtcbiAgICAgIHRocm93IG5ldyBMb2NhbGl6YXRpb25FeGNlcHRpb24oJ0ludmFsaWQgc3ltYm9sJyk7XG4gICAgfVxuXG4gICAgaWYgKHR5cGVvZiB0aGlzLm1heEZyYWN0aW9uRGlnaXRzICE9PSAnbnVtYmVyJykge1xuICAgICAgdGhyb3cgbmV3IExvY2FsaXphdGlvbkV4Y2VwdGlvbignSW52YWxpZCBtYXhGcmFjdGlvbkRpZ2l0cycpO1xuICAgIH1cblxuICAgIGlmICh0eXBlb2YgdGhpcy5taW5GcmFjdGlvbkRpZ2l0cyAhPT0gJ251bWJlcicpIHtcbiAgICAgIHRocm93IG5ldyBMb2NhbGl6YXRpb25FeGNlcHRpb24oJ0ludmFsaWQgbWluRnJhY3Rpb25EaWdpdHMnKTtcbiAgICB9XG5cbiAgICBpZiAodHlwZW9mIHRoaXMuZ3JvdXBpbmdVc2VkICE9PSAnYm9vbGVhbicpIHtcbiAgICAgIHRocm93IG5ldyBMb2NhbGl6YXRpb25FeGNlcHRpb24oJ0ludmFsaWQgZ3JvdXBpbmdVc2VkJyk7XG4gICAgfVxuXG4gICAgaWYgKHR5cGVvZiB0aGlzLnByaW1hcnlHcm91cFNpemUgIT09ICdudW1iZXInKSB7XG4gICAgICB0aHJvdyBuZXcgTG9jYWxpemF0aW9uRXhjZXB0aW9uKCdJbnZhbGlkIHByaW1hcnlHcm91cFNpemUnKTtcbiAgICB9XG5cbiAgICBpZiAodHlwZW9mIHRoaXMuc2Vjb25kYXJ5R3JvdXBTaXplICE9PSAnbnVtYmVyJykge1xuICAgICAgdGhyb3cgbmV3IExvY2FsaXphdGlvbkV4Y2VwdGlvbignSW52YWxpZCBzZWNvbmRhcnlHcm91cFNpemUnKTtcbiAgICB9XG4gIH1cblxuICAvKipcbiAgICogR2V0IHN5bWJvbC5cbiAgICpcbiAgICogQHJldHVybiBOdW1iZXJTeW1ib2xcbiAgICovXG4gIGdldFN5bWJvbCgpIHtcbiAgICByZXR1cm4gdGhpcy5zeW1ib2w7XG4gIH1cblxuICAvKipcbiAgICogR2V0IHRoZSBmb3JtYXR0aW5nIHJ1bGVzIGZvciB0aGlzIG51bWJlciAod2hlbiBwb3NpdGl2ZSkuXG4gICAqXG4gICAqIFRoaXMgcGF0dGVybiB1c2VzIHRoZSBVbmljb2RlIENMRFIgbnVtYmVyIHBhdHRlcm4gc3ludGF4XG4gICAqXG4gICAqIEByZXR1cm4gc3RyaW5nXG4gICAqL1xuICBnZXRQb3NpdGl2ZVBhdHRlcm4oKSB7XG4gICAgcmV0dXJuIHRoaXMucG9zaXRpdmVQYXR0ZXJuO1xuICB9XG5cbiAgLyoqXG4gICAqIEdldCB0aGUgZm9ybWF0dGluZyBydWxlcyBmb3IgdGhpcyBudW1iZXIgKHdoZW4gbmVnYXRpdmUpLlxuICAgKlxuICAgKiBUaGlzIHBhdHRlcm4gdXNlcyB0aGUgVW5pY29kZSBDTERSIG51bWJlciBwYXR0ZXJuIHN5bnRheFxuICAgKlxuICAgKiBAcmV0dXJuIHN0cmluZ1xuICAgKi9cbiAgZ2V0TmVnYXRpdmVQYXR0ZXJuKCkge1xuICAgIHJldHVybiB0aGlzLm5lZ2F0aXZlUGF0dGVybjtcbiAgfVxuXG4gIC8qKlxuICAgKiBHZXQgdGhlIG1heGltdW0gbnVtYmVyIG9mIGRpZ2l0cyBhZnRlciBkZWNpbWFsIHNlcGFyYXRvciAocm91bmRpbmcgaWYgbmVlZGVkKS5cbiAgICpcbiAgICogQHJldHVybiBpbnRcbiAgICovXG4gIGdldE1heEZyYWN0aW9uRGlnaXRzKCkge1xuICAgIHJldHVybiB0aGlzLm1heEZyYWN0aW9uRGlnaXRzO1xuICB9XG5cbiAgLyoqXG4gICAqIEdldCB0aGUgbWluaW11bSBudW1iZXIgb2YgZGlnaXRzIGFmdGVyIGRlY2ltYWwgc2VwYXJhdG9yIChmaWxsIHdpdGggXCIwXCIgaWYgbmVlZGVkKS5cbiAgICpcbiAgICogQHJldHVybiBpbnRcbiAgICovXG4gIGdldE1pbkZyYWN0aW9uRGlnaXRzKCkge1xuICAgIHJldHVybiB0aGlzLm1pbkZyYWN0aW9uRGlnaXRzO1xuICB9XG5cbiAgLyoqXG4gICAqIEdldCB0aGUgXCJncm91cGluZ1wiIGZsYWcuIFRoaXMgZmxhZyBkZWZpbmVzIGlmIGRpZ2l0c1xuICAgKiBncm91cGluZyBzaG91bGQgYmUgdXNlZCB3aGVuIGZvcm1hdHRpbmcgdGhpcyBudW1iZXIuXG4gICAqXG4gICAqIEByZXR1cm4gYm9vbFxuICAgKi9cbiAgaXNHcm91cGluZ1VzZWQoKSB7XG4gICAgcmV0dXJuIHRoaXMuZ3JvdXBpbmdVc2VkO1xuICB9XG5cbiAgLyoqXG4gICAqIEdldCB0aGUgc2l6ZSBvZiBwcmltYXJ5IGRpZ2l0cyBncm91cCBpbiB0aGUgbnVtYmVyLlxuICAgKlxuICAgKiBAcmV0dXJuIGludFxuICAgKi9cbiAgZ2V0UHJpbWFyeUdyb3VwU2l6ZSgpIHtcbiAgICByZXR1cm4gdGhpcy5wcmltYXJ5R3JvdXBTaXplO1xuICB9XG5cbiAgLyoqXG4gICAqIEdldCB0aGUgc2l6ZSBvZiBzZWNvbmRhcnkgZGlnaXRzIGdyb3VwcyBpbiB0aGUgbnVtYmVyLlxuICAgKlxuICAgKiBAcmV0dXJuIGludFxuICAgKi9cbiAgZ2V0U2Vjb25kYXJ5R3JvdXBTaXplKCkge1xuICAgIHJldHVybiB0aGlzLnNlY29uZGFyeUdyb3VwU2l6ZTtcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBOdW1iZXJTcGVjaWZpY2F0aW9uO1xuIiwiLyoqXG4gKiBDb3B5cmlnaHQgc2luY2UgMjAwNyBQcmVzdGFTaG9wIFNBIGFuZCBDb250cmlidXRvcnNcbiAqIFByZXN0YVNob3AgaXMgYW4gSW50ZXJuYXRpb25hbCBSZWdpc3RlcmVkIFRyYWRlbWFyayAmIFByb3BlcnR5IG9mIFByZXN0YVNob3AgU0FcbiAqXG4gKiBOT1RJQ0UgT0YgTElDRU5TRVxuICpcbiAqIFRoaXMgc291cmNlIGZpbGUgaXMgc3ViamVjdCB0byB0aGUgT3BlbiBTb2Z0d2FyZSBMaWNlbnNlIChPU0wgMy4wKVxuICogdGhhdCBpcyBidW5kbGVkIHdpdGggdGhpcyBwYWNrYWdlIGluIHRoZSBmaWxlIExJQ0VOU0UubWQuXG4gKiBJdCBpcyBhbHNvIGF2YWlsYWJsZSB0aHJvdWdoIHRoZSB3b3JsZC13aWRlLXdlYiBhdCB0aGlzIFVSTDpcbiAqIGh0dHBzOi8vb3BlbnNvdXJjZS5vcmcvbGljZW5zZXMvT1NMLTMuMFxuICogSWYgeW91IGRpZCBub3QgcmVjZWl2ZSBhIGNvcHkgb2YgdGhlIGxpY2Vuc2UgYW5kIGFyZSB1bmFibGUgdG9cbiAqIG9idGFpbiBpdCB0aHJvdWdoIHRoZSB3b3JsZC13aWRlLXdlYiwgcGxlYXNlIHNlbmQgYW4gZW1haWxcbiAqIHRvIGxpY2Vuc2VAcHJlc3Rhc2hvcC5jb20gc28gd2UgY2FuIHNlbmQgeW91IGEgY29weSBpbW1lZGlhdGVseS5cbiAqXG4gKiBESVNDTEFJTUVSXG4gKlxuICogRG8gbm90IGVkaXQgb3IgYWRkIHRvIHRoaXMgZmlsZSBpZiB5b3Ugd2lzaCB0byB1cGdyYWRlIFByZXN0YVNob3AgdG8gbmV3ZXJcbiAqIHZlcnNpb25zIGluIHRoZSBmdXR1cmUuIElmIHlvdSB3aXNoIHRvIGN1c3RvbWl6ZSBQcmVzdGFTaG9wIGZvciB5b3VyXG4gKiBuZWVkcyBwbGVhc2UgcmVmZXIgdG8gaHR0cHM6Ly9kZXZkb2NzLnByZXN0YXNob3AuY29tLyBmb3IgbW9yZSBpbmZvcm1hdGlvbi5cbiAqXG4gKiBAYXV0aG9yICAgIFByZXN0YVNob3AgU0EgYW5kIENvbnRyaWJ1dG9ycyA8Y29udGFjdEBwcmVzdGFzaG9wLmNvbT5cbiAqIEBjb3B5cmlnaHQgU2luY2UgMjAwNyBQcmVzdGFTaG9wIFNBIGFuZCBDb250cmlidXRvcnNcbiAqIEBsaWNlbnNlICAgaHR0cHM6Ly9vcGVuc291cmNlLm9yZy9saWNlbnNlcy9PU0wtMy4wIE9wZW4gU29mdHdhcmUgTGljZW5zZSAoT1NMIDMuMClcbiAqL1xuaW1wb3J0IExvY2FsaXphdGlvbkV4Y2VwdGlvbiBmcm9tICdAYXBwL2NsZHIvZXhjZXB0aW9uL2xvY2FsaXphdGlvbic7XG5pbXBvcnQgTnVtYmVyU3BlY2lmaWNhdGlvbiBmcm9tICdAYXBwL2NsZHIvc3BlY2lmaWNhdGlvbnMvbnVtYmVyJztcblxuLyoqXG4gKiBDdXJyZW5jeSBkaXNwbGF5IG9wdGlvbjogc3ltYm9sIG5vdGF0aW9uLlxuICovXG5jb25zdCBDVVJSRU5DWV9ESVNQTEFZX1NZTUJPTCA9ICdzeW1ib2wnO1xuXG5jbGFzcyBQcmljZVNwZWNpZmljYXRpb24gZXh0ZW5kcyBOdW1iZXJTcGVjaWZpY2F0aW9uIHtcbiAgLyoqXG4gICAqIFByaWNlIHNwZWNpZmljYXRpb24gY29uc3RydWN0b3IuXG4gICAqXG4gICAqIEBwYXJhbSBzdHJpbmcgcG9zaXRpdmVQYXR0ZXJuIENMRFIgZm9ybWF0dGluZyBwYXR0ZXJuIGZvciBwb3NpdGl2ZSBhbW91bnRzXG4gICAqIEBwYXJhbSBzdHJpbmcgbmVnYXRpdmVQYXR0ZXJuIENMRFIgZm9ybWF0dGluZyBwYXR0ZXJuIGZvciBuZWdhdGl2ZSBhbW91bnRzXG4gICAqIEBwYXJhbSBOdW1iZXJTeW1ib2wgc3ltYm9sIE51bWJlciBzeW1ib2xcbiAgICogQHBhcmFtIGludCBtYXhGcmFjdGlvbkRpZ2l0cyBNYXhpbXVtIG51bWJlciBvZiBkaWdpdHMgYWZ0ZXIgZGVjaW1hbCBzZXBhcmF0b3JcbiAgICogQHBhcmFtIGludCBtaW5GcmFjdGlvbkRpZ2l0cyBNaW5pbXVtIG51bWJlciBvZiBkaWdpdHMgYWZ0ZXIgZGVjaW1hbCBzZXBhcmF0b3JcbiAgICogQHBhcmFtIGJvb2wgZ3JvdXBpbmdVc2VkIElzIGRpZ2l0cyBncm91cGluZyB1c2VkID9cbiAgICogQHBhcmFtIGludCBwcmltYXJ5R3JvdXBTaXplIFNpemUgb2YgcHJpbWFyeSBkaWdpdHMgZ3JvdXAgaW4gdGhlIG51bWJlclxuICAgKiBAcGFyYW0gaW50IHNlY29uZGFyeUdyb3VwU2l6ZSBTaXplIG9mIHNlY29uZGFyeSBkaWdpdHMgZ3JvdXAgaW4gdGhlIG51bWJlclxuICAgKiBAcGFyYW0gc3RyaW5nIGN1cnJlbmN5U3ltYm9sIEN1cnJlbmN5IHN5bWJvbCBvZiB0aGlzIHByaWNlIChlZy4gOiDigqwpXG4gICAqIEBwYXJhbSBjdXJyZW5jeUNvZGUgQ3VycmVuY3kgY29kZSBvZiB0aGlzIHByaWNlIChlLmcuOiBFVVIpXG4gICAqXG4gICAqIEB0aHJvd3MgTG9jYWxpemF0aW9uRXhjZXB0aW9uXG4gICAqL1xuICBjb25zdHJ1Y3RvcihcbiAgICBwb3NpdGl2ZVBhdHRlcm4sXG4gICAgbmVnYXRpdmVQYXR0ZXJuLFxuICAgIHN5bWJvbCxcbiAgICBtYXhGcmFjdGlvbkRpZ2l0cyxcbiAgICBtaW5GcmFjdGlvbkRpZ2l0cyxcbiAgICBncm91cGluZ1VzZWQsXG4gICAgcHJpbWFyeUdyb3VwU2l6ZSxcbiAgICBzZWNvbmRhcnlHcm91cFNpemUsXG4gICAgY3VycmVuY3lTeW1ib2wsXG4gICAgY3VycmVuY3lDb2RlLFxuICApIHtcbiAgICBzdXBlcihcbiAgICAgIHBvc2l0aXZlUGF0dGVybixcbiAgICAgIG5lZ2F0aXZlUGF0dGVybixcbiAgICAgIHN5bWJvbCxcbiAgICAgIG1heEZyYWN0aW9uRGlnaXRzLFxuICAgICAgbWluRnJhY3Rpb25EaWdpdHMsXG4gICAgICBncm91cGluZ1VzZWQsXG4gICAgICBwcmltYXJ5R3JvdXBTaXplLFxuICAgICAgc2Vjb25kYXJ5R3JvdXBTaXplLFxuICAgICk7XG4gICAgdGhpcy5jdXJyZW5jeVN5bWJvbCA9IGN1cnJlbmN5U3ltYm9sO1xuICAgIHRoaXMuY3VycmVuY3lDb2RlID0gY3VycmVuY3lDb2RlO1xuXG4gICAgaWYgKCF0aGlzLmN1cnJlbmN5U3ltYm9sIHx8IHR5cGVvZiB0aGlzLmN1cnJlbmN5U3ltYm9sICE9PSAnc3RyaW5nJykge1xuICAgICAgdGhyb3cgbmV3IExvY2FsaXphdGlvbkV4Y2VwdGlvbignSW52YWxpZCBjdXJyZW5jeVN5bWJvbCcpO1xuICAgIH1cblxuICAgIGlmICghdGhpcy5jdXJyZW5jeUNvZGUgfHwgdHlwZW9mIHRoaXMuY3VycmVuY3lDb2RlICE9PSAnc3RyaW5nJykge1xuICAgICAgdGhyb3cgbmV3IExvY2FsaXphdGlvbkV4Y2VwdGlvbignSW52YWxpZCBjdXJyZW5jeUNvZGUnKTtcbiAgICB9XG4gIH1cblxuICAvKipcbiAgICogR2V0IHR5cGUgb2YgZGlzcGxheSBmb3IgY3VycmVuY3kgc3ltYm9sLlxuICAgKlxuICAgKiBAcmV0dXJuIHN0cmluZ1xuICAgKi9cbiAgc3RhdGljIGdldEN1cnJlbmN5RGlzcGxheSgpIHtcbiAgICByZXR1cm4gQ1VSUkVOQ1lfRElTUExBWV9TWU1CT0w7XG4gIH1cblxuICAvKipcbiAgICogR2V0IHRoZSBjdXJyZW5jeSBzeW1ib2xcbiAgICogZS5nLjog4oKsLlxuICAgKlxuICAgKiBAcmV0dXJuIHN0cmluZ1xuICAgKi9cbiAgZ2V0Q3VycmVuY3lTeW1ib2woKSB7XG4gICAgcmV0dXJuIHRoaXMuY3VycmVuY3lTeW1ib2w7XG4gIH1cblxuICAvKipcbiAgICogR2V0IHRoZSBjdXJyZW5jeSBJU08gY29kZVxuICAgKiBlLmcuOiBFVVIuXG4gICAqXG4gICAqIEByZXR1cm4gc3RyaW5nXG4gICAqL1xuICBnZXRDdXJyZW5jeUNvZGUoKSB7XG4gICAgcmV0dXJuIHRoaXMuY3VycmVuY3lDb2RlO1xuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IFByaWNlU3BlY2lmaWNhdGlvbjtcbiIsIi8qKlxuICogQ29weXJpZ2h0IHNpbmNlIDIwMDcgUHJlc3RhU2hvcCBTQSBhbmQgQ29udHJpYnV0b3JzXG4gKiBQcmVzdGFTaG9wIGlzIGFuIEludGVybmF0aW9uYWwgUmVnaXN0ZXJlZCBUcmFkZW1hcmsgJiBQcm9wZXJ0eSBvZiBQcmVzdGFTaG9wIFNBXG4gKlxuICogTk9USUNFIE9GIExJQ0VOU0VcbiAqXG4gKiBUaGlzIHNvdXJjZSBmaWxlIGlzIHN1YmplY3QgdG8gdGhlIE9wZW4gU29mdHdhcmUgTGljZW5zZSAoT1NMIDMuMClcbiAqIHRoYXQgaXMgYnVuZGxlZCB3aXRoIHRoaXMgcGFja2FnZSBpbiB0aGUgZmlsZSBMSUNFTlNFLm1kLlxuICogSXQgaXMgYWxzbyBhdmFpbGFibGUgdGhyb3VnaCB0aGUgd29ybGQtd2lkZS13ZWIgYXQgdGhpcyBVUkw6XG4gKiBodHRwczovL29wZW5zb3VyY2Uub3JnL2xpY2Vuc2VzL09TTC0zLjBcbiAqIElmIHlvdSBkaWQgbm90IHJlY2VpdmUgYSBjb3B5IG9mIHRoZSBsaWNlbnNlIGFuZCBhcmUgdW5hYmxlIHRvXG4gKiBvYnRhaW4gaXQgdGhyb3VnaCB0aGUgd29ybGQtd2lkZS13ZWIsIHBsZWFzZSBzZW5kIGFuIGVtYWlsXG4gKiB0byBsaWNlbnNlQHByZXN0YXNob3AuY29tIHNvIHdlIGNhbiBzZW5kIHlvdSBhIGNvcHkgaW1tZWRpYXRlbHkuXG4gKlxuICogRElTQ0xBSU1FUlxuICpcbiAqIERvIG5vdCBlZGl0IG9yIGFkZCB0byB0aGlzIGZpbGUgaWYgeW91IHdpc2ggdG8gdXBncmFkZSBQcmVzdGFTaG9wIHRvIG5ld2VyXG4gKiB2ZXJzaW9ucyBpbiB0aGUgZnV0dXJlLiBJZiB5b3Ugd2lzaCB0byBjdXN0b21pemUgUHJlc3RhU2hvcCBmb3IgeW91clxuICogbmVlZHMgcGxlYXNlIHJlZmVyIHRvIGh0dHBzOi8vZGV2ZG9jcy5wcmVzdGFzaG9wLmNvbS8gZm9yIG1vcmUgaW5mb3JtYXRpb24uXG4gKlxuICogQGF1dGhvciAgICBQcmVzdGFTaG9wIFNBIGFuZCBDb250cmlidXRvcnMgPGNvbnRhY3RAcHJlc3Rhc2hvcC5jb20+XG4gKiBAY29weXJpZ2h0IFNpbmNlIDIwMDcgUHJlc3RhU2hvcCBTQSBhbmQgQ29udHJpYnV0b3JzXG4gKiBAbGljZW5zZSAgIGh0dHBzOi8vb3BlbnNvdXJjZS5vcmcvbGljZW5zZXMvT1NMLTMuMCBPcGVuIFNvZnR3YXJlIExpY2Vuc2UgKE9TTCAzLjApXG4gKi9cblxuaW1wb3J0IEV2ZW50RW1pdHRlckNsYXNzIGZyb20gJ2V2ZW50cyc7XG5cbi8qKlxuICogV2UgaW5zdGFuY2lhdGUgb25lIEV2ZW50RW1pdHRlciAocmVzdHJpY3RlZCB2aWEgYSBjb25zdCkgc28gdGhhdCBldmVyeSBjb21wb25lbnRzXG4gKiByZWdpc3Rlci9kaXNwYXRjaCBvbiB0aGUgc2FtZSBvbmUgYW5kIGNhbiBjb21tdW5pY2F0ZSB3aXRoIGVhY2ggb3RoZXIuXG4gKi9cbmV4cG9ydCBjb25zdCBFdmVudEVtaXR0ZXIgPSBuZXcgRXZlbnRFbWl0dGVyQ2xhc3MoKTtcblxuZXhwb3J0IGRlZmF1bHQgRXZlbnRFbWl0dGVyO1xuIiwiLyoqXG4gKiBDb3B5cmlnaHQgc2luY2UgMjAwNyBQcmVzdGFTaG9wIFNBIGFuZCBDb250cmlidXRvcnNcbiAqIFByZXN0YVNob3AgaXMgYW4gSW50ZXJuYXRpb25hbCBSZWdpc3RlcmVkIFRyYWRlbWFyayAmIFByb3BlcnR5IG9mIFByZXN0YVNob3AgU0FcbiAqXG4gKiBOT1RJQ0UgT0YgTElDRU5TRVxuICpcbiAqIFRoaXMgc291cmNlIGZpbGUgaXMgc3ViamVjdCB0byB0aGUgT3BlbiBTb2Z0d2FyZSBMaWNlbnNlIChPU0wgMy4wKVxuICogdGhhdCBpcyBidW5kbGVkIHdpdGggdGhpcyBwYWNrYWdlIGluIHRoZSBmaWxlIExJQ0VOU0UubWQuXG4gKiBJdCBpcyBhbHNvIGF2YWlsYWJsZSB0aHJvdWdoIHRoZSB3b3JsZC13aWRlLXdlYiBhdCB0aGlzIFVSTDpcbiAqIGh0dHBzOi8vb3BlbnNvdXJjZS5vcmcvbGljZW5zZXMvT1NMLTMuMFxuICogSWYgeW91IGRpZCBub3QgcmVjZWl2ZSBhIGNvcHkgb2YgdGhlIGxpY2Vuc2UgYW5kIGFyZSB1bmFibGUgdG9cbiAqIG9idGFpbiBpdCB0aHJvdWdoIHRoZSB3b3JsZC13aWRlLXdlYiwgcGxlYXNlIHNlbmQgYW4gZW1haWxcbiAqIHRvIGxpY2Vuc2VAcHJlc3Rhc2hvcC5jb20gc28gd2UgY2FuIHNlbmQgeW91IGEgY29weSBpbW1lZGlhdGVseS5cbiAqXG4gKiBESVNDTEFJTUVSXG4gKlxuICogRG8gbm90IGVkaXQgb3IgYWRkIHRvIHRoaXMgZmlsZSBpZiB5b3Ugd2lzaCB0byB1cGdyYWRlIFByZXN0YVNob3AgdG8gbmV3ZXJcbiAqIHZlcnNpb25zIGluIHRoZSBmdXR1cmUuIElmIHlvdSB3aXNoIHRvIGN1c3RvbWl6ZSBQcmVzdGFTaG9wIGZvciB5b3VyXG4gKiBuZWVkcyBwbGVhc2UgcmVmZXIgdG8gaHR0cHM6Ly9kZXZkb2NzLnByZXN0YXNob3AuY29tLyBmb3IgbW9yZSBpbmZvcm1hdGlvbi5cbiAqXG4gKiBAYXV0aG9yICAgIFByZXN0YVNob3AgU0EgYW5kIENvbnRyaWJ1dG9ycyA8Y29udGFjdEBwcmVzdGFzaG9wLmNvbT5cbiAqIEBjb3B5cmlnaHQgU2luY2UgMjAwNyBQcmVzdGFTaG9wIFNBIGFuZCBDb250cmlidXRvcnNcbiAqIEBsaWNlbnNlICAgaHR0cHM6Ly9vcGVuc291cmNlLm9yZy9saWNlbnNlcy9PU0wtMy4wIE9wZW4gU29mdHdhcmUgTGljZW5zZSAoT1NMIDMuMClcbiAqL1xuXG5jb25zdCB7JH0gPSB3aW5kb3c7XG5cbi8qKlxuICogVGV4dFdpdGhMZW5ndGhDb3VudGVyIGhhbmRsZXMgaW5wdXQgd2l0aCBsZW5ndGggY291bnRlciBVSS5cbiAqXG4gKiBVc2FnZTpcbiAqXG4gKiBUaGVyZSBtdXN0IGJlIGFuIGVsZW1lbnQgdGhhdCB3cmFwcyBib3RoIGlucHV0ICYgY291bnRlciBkaXNwbGF5IHdpdGggXCIuanMtdGV4dC13aXRoLWxlbmd0aC1jb3VudGVyXCIgY2xhc3MuXG4gKiBDb3VudGVyIGRpc3BsYXkgbXVzdCBoYXZlIFwiLmpzLWNvdW50YWJsZS10ZXh0LWRpc3BsYXlcIiBjbGFzcyBhbmQgaW5wdXQgbXVzdCBoYXZlIFwiLmpzLWNvdW50YWJsZS10ZXh0LWlucHV0XCIgY2xhc3MuXG4gKiBUZXh0IGlucHV0IG11c3QgaGF2ZSBcImRhdGEtbWF4LWxlbmd0aFwiIGF0dHJpYnV0ZS5cbiAqXG4gKiA8ZGl2IGNsYXNzPVwianMtdGV4dC13aXRoLWxlbmd0aC1jb3VudGVyXCI+XG4gKiAgPHNwYW4gY2xhc3M9XCJqcy1jb3VudGFibGUtdGV4dFwiPjwvc3Bhbj5cbiAqICA8aW5wdXQgY2xhc3M9XCJqcy1jb3VudGFibGUtaW5wdXRcIiBkYXRhLW1heC1sZW5ndGg9XCIyNTVcIj5cbiAqIDwvZGl2PlxuICpcbiAqIEluIEphdmFzY3JpcHQgeW91IG11c3QgZW5hYmxlIHRoaXMgY29tcG9uZW50OlxuICpcbiAqIG5ldyBUZXh0V2l0aExlbmd0aENvdW50ZXIoKTtcbiAqL1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgVGV4dFdpdGhMZW5ndGhDb3VudGVyIHtcbiAgY29uc3RydWN0b3IoKSB7XG4gICAgdGhpcy53cmFwcGVyU2VsZWN0b3IgPSAnLmpzLXRleHQtd2l0aC1sZW5ndGgtY291bnRlcic7XG4gICAgdGhpcy50ZXh0U2VsZWN0b3IgPSAnLmpzLWNvdW50YWJsZS10ZXh0JztcbiAgICB0aGlzLmlucHV0U2VsZWN0b3IgPSAnLmpzLWNvdW50YWJsZS1pbnB1dCc7XG5cbiAgICAkKGRvY3VtZW50KS5vbignaW5wdXQnLCBgJHt0aGlzLndyYXBwZXJTZWxlY3Rvcn0gJHt0aGlzLmlucHV0U2VsZWN0b3J9YCwgKGUpID0+IHtcbiAgICAgIGNvbnN0ICRpbnB1dCA9ICQoZS5jdXJyZW50VGFyZ2V0KTtcbiAgICAgIGNvbnN0IHJlbWFpbmluZ0xlbmd0aCA9ICRpbnB1dC5kYXRhKCdtYXgtbGVuZ3RoJykgLSAkaW5wdXQudmFsKCkubGVuZ3RoO1xuXG4gICAgICAkaW5wdXQuY2xvc2VzdCh0aGlzLndyYXBwZXJTZWxlY3RvcikuZmluZCh0aGlzLnRleHRTZWxlY3RvcikudGV4dChyZW1haW5pbmdMZW5ndGgpO1xuICAgIH0pO1xuICB9XG59XG4iLCIvKipcbiAqIENvcHlyaWdodCBzaW5jZSAyMDA3IFByZXN0YVNob3AgU0EgYW5kIENvbnRyaWJ1dG9yc1xuICogUHJlc3RhU2hvcCBpcyBhbiBJbnRlcm5hdGlvbmFsIFJlZ2lzdGVyZWQgVHJhZGVtYXJrICYgUHJvcGVydHkgb2YgUHJlc3RhU2hvcCBTQVxuICpcbiAqIE5PVElDRSBPRiBMSUNFTlNFXG4gKlxuICogVGhpcyBzb3VyY2UgZmlsZSBpcyBzdWJqZWN0IHRvIHRoZSBPcGVuIFNvZnR3YXJlIExpY2Vuc2UgKE9TTCAzLjApXG4gKiB0aGF0IGlzIGJ1bmRsZWQgd2l0aCB0aGlzIHBhY2thZ2UgaW4gdGhlIGZpbGUgTElDRU5TRS5tZC5cbiAqIEl0IGlzIGFsc28gYXZhaWxhYmxlIHRocm91Z2ggdGhlIHdvcmxkLXdpZGUtd2ViIGF0IHRoaXMgVVJMOlxuICogaHR0cHM6Ly9vcGVuc291cmNlLm9yZy9saWNlbnNlcy9PU0wtMy4wXG4gKiBJZiB5b3UgZGlkIG5vdCByZWNlaXZlIGEgY29weSBvZiB0aGUgbGljZW5zZSBhbmQgYXJlIHVuYWJsZSB0b1xuICogb2J0YWluIGl0IHRocm91Z2ggdGhlIHdvcmxkLXdpZGUtd2ViLCBwbGVhc2Ugc2VuZCBhbiBlbWFpbFxuICogdG8gbGljZW5zZUBwcmVzdGFzaG9wLmNvbSBzbyB3ZSBjYW4gc2VuZCB5b3UgYSBjb3B5IGltbWVkaWF0ZWx5LlxuICpcbiAqIERJU0NMQUlNRVJcbiAqXG4gKiBEbyBub3QgZWRpdCBvciBhZGQgdG8gdGhpcyBmaWxlIGlmIHlvdSB3aXNoIHRvIHVwZ3JhZGUgUHJlc3RhU2hvcCB0byBuZXdlclxuICogdmVyc2lvbnMgaW4gdGhlIGZ1dHVyZS4gSWYgeW91IHdpc2ggdG8gY3VzdG9taXplIFByZXN0YVNob3AgZm9yIHlvdXJcbiAqIG5lZWRzIHBsZWFzZSByZWZlciB0byBodHRwczovL2RldmRvY3MucHJlc3Rhc2hvcC5jb20vIGZvciBtb3JlIGluZm9ybWF0aW9uLlxuICpcbiAqIEBhdXRob3IgICAgUHJlc3RhU2hvcCBTQSBhbmQgQ29udHJpYnV0b3JzIDxjb250YWN0QHByZXN0YXNob3AuY29tPlxuICogQGNvcHlyaWdodCBTaW5jZSAyMDA3IFByZXN0YVNob3AgU0EgYW5kIENvbnRyaWJ1dG9yc1xuICogQGxpY2Vuc2UgICBodHRwczovL29wZW5zb3VyY2Uub3JnL2xpY2Vuc2VzL09TTC0zLjAgT3BlbiBTb2Z0d2FyZSBMaWNlbnNlIChPU0wgMy4wKVxuICovXG5cbmNvbnN0IHskfSA9IHdpbmRvdztcblxuLyoqXG4gKiBDb25maXJtTW9kYWwgY29tcG9uZW50XG4gKlxuICogQHBhcmFtIHtTdHJpbmd9IGlkXG4gKiBAcGFyYW0ge1N0cmluZ30gY29uZmlybVRpdGxlXG4gKiBAcGFyYW0ge1N0cmluZ30gY29uZmlybU1lc3NhZ2VcbiAqIEBwYXJhbSB7U3RyaW5nfSBjbG9zZUJ1dHRvbkxhYmVsXG4gKiBAcGFyYW0ge1N0cmluZ30gY29uZmlybUJ1dHRvbkxhYmVsXG4gKiBAcGFyYW0ge1N0cmluZ30gY29uZmlybUJ1dHRvbkNsYXNzXG4gKiBAcGFyYW0ge0FycmF5fSBjdXN0b21CdXR0b25zXG4gKiBAcGFyYW0ge0Jvb2xlYW59IGNsb3NhYmxlXG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSBjb25maXJtQ2FsbGJhY2tcbiAqIEBwYXJhbSB7RnVuY3Rpb259IGNhbmNlbENhbGxiYWNrXG4gKlxuICovXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiBDb25maXJtTW9kYWwocGFyYW1zLCBjb25maXJtQ2FsbGJhY2ssIGNhbmNlbENhbGxiYWNrKSB7XG4gIC8vIENvbnN0cnVjdCB0aGUgbW9kYWxcbiAgY29uc3Qge2lkLCBjbG9zYWJsZX0gPSBwYXJhbXM7XG4gIHRoaXMubW9kYWwgPSBNb2RhbChwYXJhbXMpO1xuXG4gIC8vIGpRdWVyeSBtb2RhbCBvYmplY3RcbiAgdGhpcy4kbW9kYWwgPSAkKHRoaXMubW9kYWwuY29udGFpbmVyKTtcblxuICB0aGlzLnNob3cgPSAoKSA9PiB7XG4gICAgdGhpcy4kbW9kYWwubW9kYWwoKTtcbiAgfTtcblxuICB0aGlzLm1vZGFsLmNvbmZpcm1CdXR0b24uYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCBjb25maXJtQ2FsbGJhY2spO1xuXG4gIHRoaXMuJG1vZGFsLm1vZGFsKHtcbiAgICBiYWNrZHJvcDogY2xvc2FibGUgPyB0cnVlIDogJ3N0YXRpYycsXG4gICAga2V5Ym9hcmQ6IGNsb3NhYmxlICE9PSB1bmRlZmluZWQgPyBjbG9zYWJsZSA6IHRydWUsXG4gICAgY2xvc2FibGU6IGNsb3NhYmxlICE9PSB1bmRlZmluZWQgPyBjbG9zYWJsZSA6IHRydWUsXG4gICAgc2hvdzogZmFsc2UsXG4gIH0pO1xuXG4gIHRoaXMuJG1vZGFsLm9uKCdoaWRkZW4uYnMubW9kYWwnLCAoKSA9PiB7XG4gICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcihgIyR7aWR9YCkucmVtb3ZlKCk7XG4gICAgaWYgKGNhbmNlbENhbGxiYWNrKSB7XG4gICAgICBjYW5jZWxDYWxsYmFjaygpO1xuICAgIH1cbiAgfSk7XG5cbiAgZG9jdW1lbnQuYm9keS5hcHBlbmRDaGlsZCh0aGlzLm1vZGFsLmNvbnRhaW5lcik7XG59XG5cbi8qKlxuICogTW9kYWwgY29tcG9uZW50IHRvIGltcHJvdmUgbGlzaWJpbGl0eSBieSBjb25zdHJ1Y3RpbmcgdGhlIG1vZGFsIG91dHNpZGUgdGhlIG1haW4gZnVuY3Rpb25cbiAqXG4gKiBAcGFyYW0ge09iamVjdH0gcGFyYW1zXG4gKlxuICovXG5mdW5jdGlvbiBNb2RhbCh7XG4gIGlkID0gJ2NvbmZpcm0tbW9kYWwnLFxuICBjb25maXJtVGl0bGUsXG4gIGNvbmZpcm1NZXNzYWdlID0gJycsXG4gIGNsb3NlQnV0dG9uTGFiZWwgPSAnQ2xvc2UnLFxuICBjb25maXJtQnV0dG9uTGFiZWwgPSAnQWNjZXB0JyxcbiAgY29uZmlybUJ1dHRvbkNsYXNzID0gJ2J0bi1wcmltYXJ5JyxcbiAgY3VzdG9tQnV0dG9ucyA9IFtdLFxufSkge1xuICBjb25zdCBtb2RhbCA9IHt9O1xuXG4gIC8vIE1haW4gbW9kYWwgZWxlbWVudFxuICBtb2RhbC5jb250YWluZXIgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdkaXYnKTtcbiAgbW9kYWwuY29udGFpbmVyLmNsYXNzTGlzdC5hZGQoJ21vZGFsJywgJ2ZhZGUnKTtcbiAgbW9kYWwuY29udGFpbmVyLmlkID0gaWQ7XG5cbiAgLy8gTW9kYWwgZGlhbG9nIGVsZW1lbnRcbiAgbW9kYWwuZGlhbG9nID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnZGl2Jyk7XG4gIG1vZGFsLmRpYWxvZy5jbGFzc0xpc3QuYWRkKCdtb2RhbC1kaWFsb2cnKTtcblxuICAvLyBNb2RhbCBjb250ZW50IGVsZW1lbnRcbiAgbW9kYWwuY29udGVudCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2RpdicpO1xuICBtb2RhbC5jb250ZW50LmNsYXNzTGlzdC5hZGQoJ21vZGFsLWNvbnRlbnQnKTtcblxuICAvLyBNb2RhbCBoZWFkZXIgZWxlbWVudFxuICBtb2RhbC5oZWFkZXIgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdkaXYnKTtcbiAgbW9kYWwuaGVhZGVyLmNsYXNzTGlzdC5hZGQoJ21vZGFsLWhlYWRlcicpO1xuXG4gIC8vIE1vZGFsIHRpdGxlIGVsZW1lbnRcbiAgaWYgKGNvbmZpcm1UaXRsZSkge1xuICAgIG1vZGFsLnRpdGxlID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnaDQnKTtcbiAgICBtb2RhbC50aXRsZS5jbGFzc0xpc3QuYWRkKCdtb2RhbC10aXRsZScpO1xuICAgIG1vZGFsLnRpdGxlLmlubmVySFRNTCA9IGNvbmZpcm1UaXRsZTtcbiAgfVxuXG4gIC8vIE1vZGFsIGNsb3NlIGJ1dHRvbiBpY29uXG4gIG1vZGFsLmNsb3NlSWNvbiA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2J1dHRvbicpO1xuICBtb2RhbC5jbG9zZUljb24uY2xhc3NMaXN0LmFkZCgnY2xvc2UnKTtcbiAgbW9kYWwuY2xvc2VJY29uLnNldEF0dHJpYnV0ZSgndHlwZScsICdidXR0b24nKTtcbiAgbW9kYWwuY2xvc2VJY29uLmRhdGFzZXQuZGlzbWlzcyA9ICdtb2RhbCc7XG4gIG1vZGFsLmNsb3NlSWNvbi5pbm5lckhUTUwgPSAnw5cnO1xuXG4gIC8vIE1vZGFsIGJvZHkgZWxlbWVudFxuICBtb2RhbC5ib2R5ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnZGl2Jyk7XG4gIG1vZGFsLmJvZHkuY2xhc3NMaXN0LmFkZCgnbW9kYWwtYm9keScsICd0ZXh0LWxlZnQnLCAnZm9udC13ZWlnaHQtbm9ybWFsJyk7XG5cbiAgLy8gTW9kYWwgbWVzc2FnZSBlbGVtZW50XG4gIG1vZGFsLm1lc3NhZ2UgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdwJyk7XG4gIG1vZGFsLm1lc3NhZ2UuY2xhc3NMaXN0LmFkZCgnY29uZmlybS1tZXNzYWdlJyk7XG4gIG1vZGFsLm1lc3NhZ2UuaW5uZXJIVE1MID0gY29uZmlybU1lc3NhZ2U7XG5cbiAgLy8gTW9kYWwgZm9vdGVyIGVsZW1lbnRcbiAgbW9kYWwuZm9vdGVyID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnZGl2Jyk7XG4gIG1vZGFsLmZvb3Rlci5jbGFzc0xpc3QuYWRkKCdtb2RhbC1mb290ZXInKTtcblxuICAvLyBNb2RhbCBjbG9zZSBidXR0b24gZWxlbWVudFxuICBtb2RhbC5jbG9zZUJ1dHRvbiA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2J1dHRvbicpO1xuICBtb2RhbC5jbG9zZUJ1dHRvbi5zZXRBdHRyaWJ1dGUoJ3R5cGUnLCAnYnV0dG9uJyk7XG4gIG1vZGFsLmNsb3NlQnV0dG9uLmNsYXNzTGlzdC5hZGQoJ2J0bicsICdidG4tb3V0bGluZS1zZWNvbmRhcnknLCAnYnRuLWxnJyk7XG4gIG1vZGFsLmNsb3NlQnV0dG9uLmRhdGFzZXQuZGlzbWlzcyA9ICdtb2RhbCc7XG4gIG1vZGFsLmNsb3NlQnV0dG9uLmlubmVySFRNTCA9IGNsb3NlQnV0dG9uTGFiZWw7XG5cbiAgLy8gTW9kYWwgY29uZmlybSBidXR0b24gZWxlbWVudFxuICBtb2RhbC5jb25maXJtQnV0dG9uID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnYnV0dG9uJyk7XG4gIG1vZGFsLmNvbmZpcm1CdXR0b24uc2V0QXR0cmlidXRlKCd0eXBlJywgJ2J1dHRvbicpO1xuICBtb2RhbC5jb25maXJtQnV0dG9uLmNsYXNzTGlzdC5hZGQoJ2J0bicsIGNvbmZpcm1CdXR0b25DbGFzcywgJ2J0bi1sZycsICdidG4tY29uZmlybS1zdWJtaXQnKTtcbiAgbW9kYWwuY29uZmlybUJ1dHRvbi5kYXRhc2V0LmRpc21pc3MgPSAnbW9kYWwnO1xuICBtb2RhbC5jb25maXJtQnV0dG9uLmlubmVySFRNTCA9IGNvbmZpcm1CdXR0b25MYWJlbDtcblxuICAvLyBDb25zdHJ1Y3RpbmcgdGhlIG1vZGFsXG4gIGlmIChjb25maXJtVGl0bGUpIHtcbiAgICBtb2RhbC5oZWFkZXIuYXBwZW5kKG1vZGFsLnRpdGxlLCBtb2RhbC5jbG9zZUljb24pO1xuICB9IGVsc2Uge1xuICAgIG1vZGFsLmhlYWRlci5hcHBlbmRDaGlsZChtb2RhbC5jbG9zZUljb24pO1xuICB9XG5cbiAgbW9kYWwuYm9keS5hcHBlbmRDaGlsZChtb2RhbC5tZXNzYWdlKTtcbiAgbW9kYWwuZm9vdGVyLmFwcGVuZChtb2RhbC5jbG9zZUJ1dHRvbiwgLi4uY3VzdG9tQnV0dG9ucywgbW9kYWwuY29uZmlybUJ1dHRvbik7XG4gIG1vZGFsLmNvbnRlbnQuYXBwZW5kKG1vZGFsLmhlYWRlciwgbW9kYWwuYm9keSwgbW9kYWwuZm9vdGVyKTtcbiAgbW9kYWwuZGlhbG9nLmFwcGVuZENoaWxkKG1vZGFsLmNvbnRlbnQpO1xuICBtb2RhbC5jb250YWluZXIuYXBwZW5kQ2hpbGQobW9kYWwuZGlhbG9nKTtcblxuICByZXR1cm4gbW9kYWw7XG59XG4iLCIvKipcbiAqIENvcHlyaWdodCBzaW5jZSAyMDA3IFByZXN0YVNob3AgU0EgYW5kIENvbnRyaWJ1dG9yc1xuICogUHJlc3RhU2hvcCBpcyBhbiBJbnRlcm5hdGlvbmFsIFJlZ2lzdGVyZWQgVHJhZGVtYXJrICYgUHJvcGVydHkgb2YgUHJlc3RhU2hvcCBTQVxuICpcbiAqIE5PVElDRSBPRiBMSUNFTlNFXG4gKlxuICogVGhpcyBzb3VyY2UgZmlsZSBpcyBzdWJqZWN0IHRvIHRoZSBPcGVuIFNvZnR3YXJlIExpY2Vuc2UgKE9TTCAzLjApXG4gKiB0aGF0IGlzIGJ1bmRsZWQgd2l0aCB0aGlzIHBhY2thZ2UgaW4gdGhlIGZpbGUgTElDRU5TRS5tZC5cbiAqIEl0IGlzIGFsc28gYXZhaWxhYmxlIHRocm91Z2ggdGhlIHdvcmxkLXdpZGUtd2ViIGF0IHRoaXMgVVJMOlxuICogaHR0cHM6Ly9vcGVuc291cmNlLm9yZy9saWNlbnNlcy9PU0wtMy4wXG4gKiBJZiB5b3UgZGlkIG5vdCByZWNlaXZlIGEgY29weSBvZiB0aGUgbGljZW5zZSBhbmQgYXJlIHVuYWJsZSB0b1xuICogb2J0YWluIGl0IHRocm91Z2ggdGhlIHdvcmxkLXdpZGUtd2ViLCBwbGVhc2Ugc2VuZCBhbiBlbWFpbFxuICogdG8gbGljZW5zZUBwcmVzdGFzaG9wLmNvbSBzbyB3ZSBjYW4gc2VuZCB5b3UgYSBjb3B5IGltbWVkaWF0ZWx5LlxuICpcbiAqIERJU0NMQUlNRVJcbiAqXG4gKiBEbyBub3QgZWRpdCBvciBhZGQgdG8gdGhpcyBmaWxlIGlmIHlvdSB3aXNoIHRvIHVwZ3JhZGUgUHJlc3RhU2hvcCB0byBuZXdlclxuICogdmVyc2lvbnMgaW4gdGhlIGZ1dHVyZS4gSWYgeW91IHdpc2ggdG8gY3VzdG9taXplIFByZXN0YVNob3AgZm9yIHlvdXJcbiAqIG5lZWRzIHBsZWFzZSByZWZlciB0byBodHRwczovL2RldmRvY3MucHJlc3Rhc2hvcC5jb20vIGZvciBtb3JlIGluZm9ybWF0aW9uLlxuICpcbiAqIEBhdXRob3IgICAgUHJlc3RhU2hvcCBTQSBhbmQgQ29udHJpYnV0b3JzIDxjb250YWN0QHByZXN0YXNob3AuY29tPlxuICogQGNvcHlyaWdodCBTaW5jZSAyMDA3IFByZXN0YVNob3AgU0EgYW5kIENvbnRyaWJ1dG9yc1xuICogQGxpY2Vuc2UgICBodHRwczovL29wZW5zb3VyY2Uub3JnL2xpY2Vuc2VzL09TTC0zLjAgT3BlbiBTb2Z0d2FyZSBMaWNlbnNlIChPU0wgMy4wKVxuICovXG5cbmltcG9ydCBSb3V0aW5nIGZyb20gJ2Zvcy1yb3V0aW5nJztcbmltcG9ydCByb3V0ZXMgZnJvbSAnQGpzL2Zvc19qc19yb3V0ZXMuanNvbic7XG5cbmNvbnN0IHskfSA9IHdpbmRvdztcblxuLyoqXG4gKiBXcmFwcyBGT1NKc1JvdXRpbmdidW5kbGUgd2l0aCBleHBvc2VkIHJvdXRlcy5cbiAqIFRvIGV4cG9zZSByb3V0ZSBhZGQgb3B0aW9uIGBleHBvc2U6IHRydWVgIGluIC55bWwgcm91dGluZyBjb25maWdcbiAqXG4gKiBlLmcuXG4gKlxuICogYG15X3JvdXRlXG4gKiAgICBwYXRoOiAvbXktcGF0aFxuICogICAgb3B0aW9uczpcbiAqICAgICAgZXhwb3NlOiB0cnVlXG4gKiBgXG4gKiBBbmQgcnVuIGBiaW4vY29uc29sZSBmb3M6anMtcm91dGluZzpkdW1wIC0tZm9ybWF0PWpzb24gLS10YXJnZXQ9YWRtaW4tZGV2L3RoZW1lcy9uZXctdGhlbWUvanMvZm9zX2pzX3JvdXRlcy5qc29uYFxuICovXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBSb3V0ZXIge1xuICBjb25zdHJ1Y3RvcigpIHtcbiAgICBpZiAod2luZG93LnByZXN0YXNob3AgJiYgd2luZG93LnByZXN0YXNob3AuY3VzdG9tUm91dGVzKSB7XG4gICAgICBPYmplY3QuYXNzaWduKHJvdXRlcy5yb3V0ZXMsIHdpbmRvdy5wcmVzdGFzaG9wLmN1c3RvbVJvdXRlcyk7XG4gICAgfVxuXG4gICAgUm91dGluZy5zZXREYXRhKHJvdXRlcyk7XG4gICAgUm91dGluZy5zZXRCYXNlVXJsKFxuICAgICAgJChkb2N1bWVudClcbiAgICAgICAgLmZpbmQoJ2JvZHknKVxuICAgICAgICAuZGF0YSgnYmFzZS11cmwnKSxcbiAgICApO1xuXG4gICAgcmV0dXJuIHRoaXM7XG4gIH1cblxuICAvKipcbiAgICogRGVjb3JhdGVkIFwiZ2VuZXJhdGVcIiBtZXRob2QsIHdpdGggcHJlZGVmaW5lZCBzZWN1cml0eSB0b2tlbiBpbiBwYXJhbXNcbiAgICpcbiAgICogQHBhcmFtIHJvdXRlXG4gICAqIEBwYXJhbSBwYXJhbXNcbiAgICpcbiAgICogQHJldHVybnMge1N0cmluZ31cbiAgICovXG4gIGdlbmVyYXRlKHJvdXRlLCBwYXJhbXMgPSB7fSkge1xuICAgIGNvbnN0IHRva2VuaXplZFBhcmFtcyA9IE9iamVjdC5hc3NpZ24ocGFyYW1zLCB7XG4gICAgICBfdG9rZW46ICQoZG9jdW1lbnQpXG4gICAgICAgIC5maW5kKCdib2R5JylcbiAgICAgICAgLmRhdGEoJ3Rva2VuJyksXG4gICAgfSk7XG5cbiAgICByZXR1cm4gUm91dGluZy5nZW5lcmF0ZShyb3V0ZSwgdG9rZW5pemVkUGFyYW1zKTtcbiAgfVxufVxuIiwiLyoqXG4gKiBDb3B5cmlnaHQgc2luY2UgMjAwNyBQcmVzdGFTaG9wIFNBIGFuZCBDb250cmlidXRvcnNcbiAqIFByZXN0YVNob3AgaXMgYW4gSW50ZXJuYXRpb25hbCBSZWdpc3RlcmVkIFRyYWRlbWFyayAmIFByb3BlcnR5IG9mIFByZXN0YVNob3AgU0FcbiAqXG4gKiBOT1RJQ0UgT0YgTElDRU5TRVxuICpcbiAqIFRoaXMgc291cmNlIGZpbGUgaXMgc3ViamVjdCB0byB0aGUgT3BlbiBTb2Z0d2FyZSBMaWNlbnNlIChPU0wgMy4wKVxuICogdGhhdCBpcyBidW5kbGVkIHdpdGggdGhpcyBwYWNrYWdlIGluIHRoZSBmaWxlIExJQ0VOU0UubWQuXG4gKiBJdCBpcyBhbHNvIGF2YWlsYWJsZSB0aHJvdWdoIHRoZSB3b3JsZC13aWRlLXdlYiBhdCB0aGlzIFVSTDpcbiAqIGh0dHBzOi8vb3BlbnNvdXJjZS5vcmcvbGljZW5zZXMvT1NMLTMuMFxuICogSWYgeW91IGRpZCBub3QgcmVjZWl2ZSBhIGNvcHkgb2YgdGhlIGxpY2Vuc2UgYW5kIGFyZSB1bmFibGUgdG9cbiAqIG9idGFpbiBpdCB0aHJvdWdoIHRoZSB3b3JsZC13aWRlLXdlYiwgcGxlYXNlIHNlbmQgYW4gZW1haWxcbiAqIHRvIGxpY2Vuc2VAcHJlc3Rhc2hvcC5jb20gc28gd2UgY2FuIHNlbmQgeW91IGEgY29weSBpbW1lZGlhdGVseS5cbiAqXG4gKiBESVNDTEFJTUVSXG4gKlxuICogRG8gbm90IGVkaXQgb3IgYWRkIHRvIHRoaXMgZmlsZSBpZiB5b3Ugd2lzaCB0byB1cGdyYWRlIFByZXN0YVNob3AgdG8gbmV3ZXJcbiAqIHZlcnNpb25zIGluIHRoZSBmdXR1cmUuIElmIHlvdSB3aXNoIHRvIGN1c3RvbWl6ZSBQcmVzdGFTaG9wIGZvciB5b3VyXG4gKiBuZWVkcyBwbGVhc2UgcmVmZXIgdG8gaHR0cHM6Ly9kZXZkb2NzLnByZXN0YXNob3AuY29tLyBmb3IgbW9yZSBpbmZvcm1hdGlvbi5cbiAqXG4gKiBAYXV0aG9yICAgIFByZXN0YVNob3AgU0EgYW5kIENvbnRyaWJ1dG9ycyA8Y29udGFjdEBwcmVzdGFzaG9wLmNvbT5cbiAqIEBjb3B5cmlnaHQgU2luY2UgMjAwNyBQcmVzdGFTaG9wIFNBIGFuZCBDb250cmlidXRvcnNcbiAqIEBsaWNlbnNlICAgaHR0cHM6Ly9vcGVuc291cmNlLm9yZy9saWNlbnNlcy9PU0wtMy4wIE9wZW4gU29mdHdhcmUgTGljZW5zZSAoT1NMIDMuMClcbiAqL1xuXG4vKiBlc2xpbnQtZGlzYWJsZSBtYXgtbGVuICovXG5cbmV4cG9ydCBkZWZhdWx0IHtcbiAgbWFpbkRpdjogJyNvcmRlci12aWV3LXBhZ2UnLFxuICBvcmRlclBheW1lbnREZXRhaWxzQnRuOiAnLmpzLXBheW1lbnQtZGV0YWlscy1idG4nLFxuICBvcmRlclBheW1lbnRGb3JtQW1vdW50SW5wdXQ6ICcjb3JkZXJfcGF5bWVudF9hbW91bnQnLFxuICBvcmRlclBheW1lbnRJbnZvaWNlU2VsZWN0OiAnI29yZGVyX3BheW1lbnRfaWRfaW52b2ljZScsXG4gIHZpZXdPcmRlclBheW1lbnRzQmxvY2s6ICcjdmlld19vcmRlcl9wYXltZW50c19ibG9jaycsXG4gIHZpZXdPcmRlclBheW1lbnRzQWxlcnQ6ICcuanMtdmlldy1vcmRlci1wYXltZW50cy1hbGVydCcsXG4gIHByaXZhdGVOb3RlVG9nZ2xlQnRuOiAnLmpzLXByaXZhdGUtbm90ZS10b2dnbGUtYnRuJyxcbiAgcHJpdmF0ZU5vdGVCbG9jazogJy5qcy1wcml2YXRlLW5vdGUtYmxvY2snLFxuICBwcml2YXRlTm90ZUlucHV0OiAnI3ByaXZhdGVfbm90ZV9ub3RlJyxcbiAgcHJpdmF0ZU5vdGVTdWJtaXRCdG46ICcuanMtcHJpdmF0ZS1ub3RlLWJ0bicsXG4gIGFkZENhcnRSdWxlTW9kYWw6ICcjYWRkT3JkZXJEaXNjb3VudE1vZGFsJyxcbiAgYWRkQ2FydFJ1bGVJbnZvaWNlSWRTZWxlY3Q6ICcjYWRkX29yZGVyX2NhcnRfcnVsZV9pbnZvaWNlX2lkJyxcbiAgYWRkQ2FydFJ1bGVOYW1lSW5wdXQ6ICcjYWRkX29yZGVyX2NhcnRfcnVsZV9uYW1lJyxcbiAgYWRkQ2FydFJ1bGVUeXBlU2VsZWN0OiAnI2FkZF9vcmRlcl9jYXJ0X3J1bGVfdHlwZScsXG4gIGFkZENhcnRSdWxlVmFsdWVJbnB1dDogJyNhZGRfb3JkZXJfY2FydF9ydWxlX3ZhbHVlJyxcbiAgYWRkQ2FydFJ1bGVWYWx1ZVVuaXQ6ICcjYWRkX29yZGVyX2NhcnRfcnVsZV92YWx1ZV91bml0JyxcbiAgYWRkQ2FydFJ1bGVTdWJtaXQ6ICcjYWRkX29yZGVyX2NhcnRfcnVsZV9zdWJtaXQnLFxuICBjYXJ0UnVsZUhlbHBUZXh0OiAnLmpzLWNhcnQtcnVsZS12YWx1ZS1oZWxwJyxcbiAgdXBkYXRlT3JkZXJTdGF0dXNBY3Rpb25CdG46ICcjdXBkYXRlX29yZGVyX3N0YXR1c19hY3Rpb25fYnRuJyxcbiAgdXBkYXRlT3JkZXJTdGF0dXNBY3Rpb25JbnB1dDogJyN1cGRhdGVfb3JkZXJfc3RhdHVzX2FjdGlvbl9pbnB1dCcsXG4gIHVwZGF0ZU9yZGVyU3RhdHVzQWN0aW9uSW5wdXRXcmFwcGVyOiAnI3VwZGF0ZV9vcmRlcl9zdGF0dXNfYWN0aW9uX2lucHV0X3dyYXBwZXInLFxuICB1cGRhdGVPcmRlclN0YXR1c0FjdGlvbkZvcm06ICcjdXBkYXRlX29yZGVyX3N0YXR1c19hY3Rpb25fZm9ybScsXG4gIHNob3dPcmRlclNoaXBwaW5nVXBkYXRlTW9kYWxCdG46ICcuanMtdXBkYXRlLXNoaXBwaW5nLWJ0bicsXG4gIHVwZGF0ZU9yZGVyU2hpcHBpbmdUcmFja2luZ051bWJlcklucHV0OiAnI3VwZGF0ZV9vcmRlcl9zaGlwcGluZ190cmFja2luZ19udW1iZXInLFxuICB1cGRhdGVPcmRlclNoaXBwaW5nQ3VycmVudE9yZGVyQ2FycmllcklkSW5wdXQ6ICcjdXBkYXRlX29yZGVyX3NoaXBwaW5nX2N1cnJlbnRfb3JkZXJfY2Fycmllcl9pZCcsXG4gIHVwZGF0ZUN1c3RvbWVyQWRkcmVzc01vZGFsOiAnI3VwZGF0ZUN1c3RvbWVyQWRkcmVzc01vZGFsJyxcbiAgb3Blbk9yZGVyQWRkcmVzc1VwZGF0ZU1vZGFsQnRuOiAnLmpzLXVwZGF0ZS1jdXN0b21lci1hZGRyZXNzLW1vZGFsLWJ0bicsXG4gIHVwZGF0ZU9yZGVyQWRkcmVzc1R5cGVJbnB1dDogJyNjaGFuZ2Vfb3JkZXJfYWRkcmVzc19hZGRyZXNzX3R5cGUnLFxuICBkZWxpdmVyeUFkZHJlc3NFZGl0QnRuOiAnI2pzLWRlbGl2ZXJ5LWFkZHJlc3MtZWRpdC1idG4nLFxuICBpbnZvaWNlQWRkcmVzc0VkaXRCdG46ICcjanMtaW52b2ljZS1hZGRyZXNzLWVkaXQtYnRuJyxcbiAgb3JkZXJNZXNzYWdlTmFtZVNlbGVjdDogJyNvcmRlcl9tZXNzYWdlX29yZGVyX21lc3NhZ2UnLFxuICBvcmRlck1lc3NhZ2VzQ29udGFpbmVyOiAnLmpzLW9yZGVyLW1lc3NhZ2VzLWNvbnRhaW5lcicsXG4gIG9yZGVyTWVzc2FnZTogJyNvcmRlcl9tZXNzYWdlX21lc3NhZ2UnLFxuICBvcmRlck1lc3NhZ2VDaGFuZ2VXYXJuaW5nOiAnLmpzLW1lc3NhZ2UtY2hhbmdlLXdhcm5pbmcnLFxuICBvcmRlckRvY3VtZW50c1RhYkNvdW50OiAnI29yZGVyRG9jdW1lbnRzVGFiIC5jb3VudCcsXG4gIG9yZGVyRG9jdW1lbnRzVGFiQm9keTogJyNvcmRlckRvY3VtZW50c1RhYkNvbnRlbnQgLmNhcmQtYm9keScsXG4gIG9yZGVyU2hpcHBpbmdUYWJDb3VudDogJyNvcmRlclNoaXBwaW5nVGFiIC5jb3VudCcsXG4gIG9yZGVyU2hpcHBpbmdUYWJCb2R5OiAnI29yZGVyU2hpcHBpbmdUYWJDb250ZW50IC5jYXJkLWJvZHknLFxuICBhbGxNZXNzYWdlc01vZGFsOiAnI3ZpZXdfYWxsX21lc3NhZ2VzX21vZGFsJyxcbiAgYWxsTWVzc2FnZXNMaXN0OiAnI2FsbC1tZXNzYWdlcy1saXN0JyxcbiAgb3BlbkFsbE1lc3NhZ2VzQnRuOiAnLmpzLW9wZW4tYWxsLW1lc3NhZ2VzLWJ0bicsXG4gIC8vIFByb2R1Y3RzIHRhYmxlIGVsZW1lbnRzXG4gIHByb2R1Y3RPcmlnaW5hbFBvc2l0aW9uOiAnI29yZGVyUHJvZHVjdHNPcmlnaW5hbFBvc2l0aW9uJyxcbiAgcHJvZHVjdE1vZGlmaWNhdGlvblBvc2l0aW9uOiAnI29yZGVyUHJvZHVjdHNNb2RpZmljYXRpb25Qb3NpdGlvbicsXG4gIHByb2R1Y3RzUGFuZWw6ICcjb3JkZXJQcm9kdWN0c1BhbmVsJyxcbiAgcHJvZHVjdHNDb3VudDogJyNvcmRlclByb2R1Y3RzUGFuZWxDb3VudCcsXG4gIHByb2R1Y3REZWxldGVCdG46ICcuanMtb3JkZXItcHJvZHVjdC1kZWxldGUtYnRuJyxcbiAgcHJvZHVjdHNUYWJsZTogJyNvcmRlclByb2R1Y3RzVGFibGUnLFxuICBwcm9kdWN0c1BhZ2luYXRpb246ICcub3JkZXItcHJvZHVjdC1wYWdpbmF0aW9uJyxcbiAgcHJvZHVjdHNOYXZQYWdpbmF0aW9uOiAnI29yZGVyUHJvZHVjdHNOYXZQYWdpbmF0aW9uJyxcbiAgcHJvZHVjdHNUYWJsZVBhZ2luYXRpb246ICcjb3JkZXJQcm9kdWN0c1RhYmxlUGFnaW5hdGlvbicsXG4gIHByb2R1Y3RzVGFibGVQYWdpbmF0aW9uTmV4dDogJyNvcmRlclByb2R1Y3RzVGFibGVQYWdpbmF0aW9uTmV4dCcsXG4gIHByb2R1Y3RzVGFibGVQYWdpbmF0aW9uUHJldjogJyNvcmRlclByb2R1Y3RzVGFibGVQYWdpbmF0aW9uUHJldicsXG4gIHByb2R1Y3RzVGFibGVQYWdpbmF0aW9uTGluazogJy5wYWdlLWl0ZW06bm90KC5kLW5vbmUpOm5vdCgjb3JkZXJQcm9kdWN0c1RhYmxlUGFnaW5hdGlvbk5leHQpOm5vdCgjb3JkZXJQcm9kdWN0c1RhYmxlUGFnaW5hdGlvblByZXYpIC5wYWdlLWxpbmsnLFxuICBwcm9kdWN0c1RhYmxlUGFnaW5hdGlvbkFjdGl2ZTogJyNvcmRlclByb2R1Y3RzVGFibGVQYWdpbmF0aW9uIC5wYWdlLWl0ZW0uYWN0aXZlIHNwYW4nLFxuICBwcm9kdWN0c1RhYmxlUGFnaW5hdGlvblRlbXBsYXRlOiAnI29yZGVyUHJvZHVjdHNUYWJsZVBhZ2luYXRpb24gLnBhZ2UtaXRlbS5kLW5vbmUnLFxuICBwcm9kdWN0c1RhYmxlUGFnaW5hdGlvbk51bWJlclNlbGVjdG9yOiAnI29yZGVyUHJvZHVjdHNUYWJsZVBhZ2luYXRpb25OdW1iZXJTZWxlY3RvcicsXG4gIHByb2R1Y3RzVGFibGVSb3c6IChwcm9kdWN0SWQpID0+IGAjb3JkZXJQcm9kdWN0XyR7cHJvZHVjdElkfWAsXG4gIHByb2R1Y3RzVGFibGVSb3dFZGl0ZWQ6IChwcm9kdWN0SWQpID0+IGAjZWRpdE9yZGVyUHJvZHVjdF8ke3Byb2R1Y3RJZH1gLFxuICBwcm9kdWN0c1RhYmxlUm93czogJ3RyLmNlbGxQcm9kdWN0JyxcbiAgcHJvZHVjdHNDZWxsTG9jYXRpb246ICd0ciAuY2VsbFByb2R1Y3RMb2NhdGlvbicsXG4gIHByb2R1Y3RzQ2VsbFJlZnVuZGVkOiAndHIgLmNlbGxQcm9kdWN0UmVmdW5kZWQnLFxuICBwcm9kdWN0c0NlbGxMb2NhdGlvbkRpc3BsYXllZDogJ3RyOm5vdCguZC1ub25lKSAuY2VsbFByb2R1Y3RMb2NhdGlvbicsXG4gIHByb2R1Y3RzQ2VsbFJlZnVuZGVkRGlzcGxheWVkOiAndHI6bm90KC5kLW5vbmUpIC5jZWxsUHJvZHVjdFJlZnVuZGVkJyxcbiAgcHJvZHVjdHNUYWJsZUN1c3RvbWl6YXRpb25Sb3dzOiAnI29yZGVyUHJvZHVjdHNUYWJsZSAub3JkZXItcHJvZHVjdC1jdXN0b21pemF0aW9uJyxcbiAgcHJvZHVjdEVkaXRCdXR0b25zOiAnLmpzLW9yZGVyLXByb2R1Y3QtZWRpdC1idG4nLFxuICBwcm9kdWN0RWRpdEJ0bjogKHByb2R1Y3RJZCkgPT4gYCNvcmRlclByb2R1Y3RfJHtwcm9kdWN0SWR9IC5qcy1vcmRlci1wcm9kdWN0LWVkaXQtYnRuYCxcbiAgcHJvZHVjdEFkZEJ0bjogJyNhZGRQcm9kdWN0QnRuJyxcbiAgcHJvZHVjdEFjdGlvbkJ0bjogJy5qcy1wcm9kdWN0LWFjdGlvbi1idG4nLFxuICBwcm9kdWN0QWRkQWN0aW9uQnRuOiAnI2FkZF9wcm9kdWN0X3Jvd19hZGQnLFxuICBwcm9kdWN0Q2FuY2VsQWRkQnRuOiAnI2FkZF9wcm9kdWN0X3Jvd19jYW5jZWwnLFxuICBwcm9kdWN0QWRkUm93OiAnI2FkZFByb2R1Y3RUYWJsZVJvdycsXG4gIHByb2R1Y3RTZWFyY2hJbnB1dDogJyNhZGRfcHJvZHVjdF9yb3dfc2VhcmNoJyxcbiAgcHJvZHVjdFNlYXJjaElucHV0QXV0b2NvbXBsZXRlOiAnI2FkZFByb2R1Y3RUYWJsZVJvdyAuZHJvcGRvd24nLFxuICBwcm9kdWN0U2VhcmNoSW5wdXRBdXRvY29tcGxldGVNZW51OiAnI2FkZFByb2R1Y3RUYWJsZVJvdyAuZHJvcGRvd24gLmRyb3Bkb3duLW1lbnUnLFxuICBwcm9kdWN0QWRkSWRJbnB1dDogJyNhZGRfcHJvZHVjdF9yb3dfcHJvZHVjdF9pZCcsXG4gIHByb2R1Y3RBZGRUYXhSYXRlSW5wdXQ6ICcjYWRkX3Byb2R1Y3Rfcm93X3RheF9yYXRlJyxcbiAgcHJvZHVjdEFkZENvbWJpbmF0aW9uc0Jsb2NrOiAnI2FkZFByb2R1Y3RDb21iaW5hdGlvbnMnLFxuICBwcm9kdWN0QWRkQ29tYmluYXRpb25zU2VsZWN0OiAnI2FkZFByb2R1Y3RDb21iaW5hdGlvbklkJyxcbiAgcHJvZHVjdEFkZFByaWNlVGF4RXhjbElucHV0OiAnI2FkZF9wcm9kdWN0X3Jvd19wcmljZV90YXhfZXhjbHVkZWQnLFxuICBwcm9kdWN0QWRkUHJpY2VUYXhJbmNsSW5wdXQ6ICcjYWRkX3Byb2R1Y3Rfcm93X3ByaWNlX3RheF9pbmNsdWRlZCcsXG4gIHByb2R1Y3RBZGRRdWFudGl0eUlucHV0OiAnI2FkZF9wcm9kdWN0X3Jvd19xdWFudGl0eScsXG4gIHByb2R1Y3RBZGRBdmFpbGFibGVUZXh0OiAnI2FkZFByb2R1Y3RBdmFpbGFibGUnLFxuICBwcm9kdWN0QWRkTG9jYXRpb25UZXh0OiAnI2FkZFByb2R1Y3RMb2NhdGlvbicsXG4gIHByb2R1Y3RBZGRUb3RhbFByaWNlVGV4dDogJyNhZGRQcm9kdWN0VG90YWxQcmljZScsXG4gIHByb2R1Y3RBZGRJbnZvaWNlU2VsZWN0OiAnI2FkZF9wcm9kdWN0X3Jvd19pbnZvaWNlJyxcbiAgcHJvZHVjdEFkZEZyZWVTaGlwcGluZ1NlbGVjdDogJyNhZGRfcHJvZHVjdF9yb3dfZnJlZV9zaGlwcGluZycsXG4gIHByb2R1Y3RBZGROZXdJbnZvaWNlSW5mbzogJyNhZGRQcm9kdWN0TmV3SW52b2ljZUluZm8nLFxuICBwcm9kdWN0RWRpdFNhdmVCdG46ICcucHJvZHVjdEVkaXRTYXZlQnRuJyxcbiAgcHJvZHVjdEVkaXRDYW5jZWxCdG46ICcucHJvZHVjdEVkaXRDYW5jZWxCdG4nLFxuICBwcm9kdWN0RWRpdFJvd1RlbXBsYXRlOiAnI2VkaXRQcm9kdWN0VGFibGVSb3dUZW1wbGF0ZScsXG4gIHByb2R1Y3RFZGl0Um93OiAnLmVkaXRQcm9kdWN0Um93JyxcbiAgcHJvZHVjdEVkaXRJbWFnZTogJy5jZWxsUHJvZHVjdEltZycsXG4gIHByb2R1Y3RFZGl0TmFtZTogJy5jZWxsUHJvZHVjdE5hbWUnLFxuICBwcm9kdWN0RWRpdFVuaXRQcmljZTogJy5jZWxsUHJvZHVjdFVuaXRQcmljZScsXG4gIHByb2R1Y3RFZGl0UXVhbnRpdHk6ICcuY2VsbFByb2R1Y3RRdWFudGl0eScsXG4gIHByb2R1Y3RFZGl0QXZhaWxhYmxlUXVhbnRpdHk6ICcuY2VsbFByb2R1Y3RBdmFpbGFibGVRdWFudGl0eScsXG4gIHByb2R1Y3RFZGl0VG90YWxQcmljZTogJy5jZWxsUHJvZHVjdFRvdGFsUHJpY2UnLFxuICBwcm9kdWN0RWRpdFByaWNlVGF4RXhjbElucHV0OiAnLmVkaXRQcm9kdWN0UHJpY2VUYXhFeGNsJyxcbiAgcHJvZHVjdEVkaXRQcmljZVRheEluY2xJbnB1dDogJy5lZGl0UHJvZHVjdFByaWNlVGF4SW5jbCcsXG4gIHByb2R1Y3RFZGl0SW52b2ljZVNlbGVjdDogJy5lZGl0UHJvZHVjdEludm9pY2UnLFxuICBwcm9kdWN0RWRpdFF1YW50aXR5SW5wdXQ6ICcuZWRpdFByb2R1Y3RRdWFudGl0eScsXG4gIHByb2R1Y3RFZGl0TG9jYXRpb25UZXh0OiAnLmVkaXRQcm9kdWN0TG9jYXRpb24nLFxuICBwcm9kdWN0RWRpdEF2YWlsYWJsZVRleHQ6ICcuZWRpdFByb2R1Y3RBdmFpbGFibGUnLFxuICBwcm9kdWN0RWRpdFRvdGFsUHJpY2VUZXh0OiAnLmVkaXRQcm9kdWN0VG90YWxQcmljZScsXG4gIC8vIFByb2R1Y3QgRGlzY291bnQgTGlzdFxuICBwcm9kdWN0RGlzY291bnRMaXN0OiB7XG4gICAgbGlzdDogJy50YWJsZS5kaXNjb3VudExpc3QnLFxuICB9LFxuICAvLyBQcm9kdWN0IFBhY2sgTW9kYWxcbiAgcHJvZHVjdFBhY2tNb2RhbDoge1xuICAgIG1vZGFsOiAnI3Byb2R1Y3QtcGFjay1tb2RhbCcsXG4gICAgdGFibGU6ICcjcHJvZHVjdC1wYWNrLW1vZGFsLXRhYmxlIHRib2R5JyxcbiAgICByb3dzOiAnI3Byb2R1Y3QtcGFjay1tb2RhbC10YWJsZSB0Ym9keSB0cjpub3QoI3RlbXBsYXRlLXBhY2stdGFibGUtcm93KScsXG4gICAgdGVtcGxhdGU6ICcjdGVtcGxhdGUtcGFjay10YWJsZS1yb3cnLFxuICAgIHByb2R1Y3Q6IHtcbiAgICAgIGltZzogJy5jZWxsLXByb2R1Y3QtaW1nIGltZycsXG4gICAgICBsaW5rOiAnLmNlbGwtcHJvZHVjdC1uYW1lIGEnLFxuICAgICAgbmFtZTogJy5jZWxsLXByb2R1Y3QtbmFtZSAucHJvZHVjdC1uYW1lJyxcbiAgICAgIHJlZjogJy5jZWxsLXByb2R1Y3QtbmFtZSAucHJvZHVjdC1yZWZlcmVuY2UnLFxuICAgICAgc3VwcGxpZXJSZWY6ICcuY2VsbC1wcm9kdWN0LW5hbWUgLnByb2R1Y3Qtc3VwcGxpZXItcmVmZXJlbmNlJyxcbiAgICAgIHF1YW50aXR5OiAnLmNlbGwtcHJvZHVjdC1xdWFudGl0eScsXG4gICAgICBhdmFpbGFibGVRdWFudGl0eTogJy5jZWxsLXByb2R1Y3QtYXZhaWxhYmxlLXF1YW50aXR5JyxcbiAgICB9LFxuICB9LFxuICAvLyBPcmRlciBwcmljZSBlbGVtZW50c1xuICBvcmRlclByb2R1Y3RzVG90YWw6ICcjb3JkZXJQcm9kdWN0c1RvdGFsJyxcbiAgb3JkZXJEaXNjb3VudHNUb3RhbENvbnRhaW5lcjogJyNvcmRlci1kaXNjb3VudHMtdG90YWwtY29udGFpbmVyJyxcbiAgb3JkZXJEaXNjb3VudHNUb3RhbDogJyNvcmRlckRpc2NvdW50c1RvdGFsJyxcbiAgb3JkZXJXcmFwcGluZ1RvdGFsOiAnI29yZGVyV3JhcHBpbmdUb3RhbCcsXG4gIG9yZGVyU2hpcHBpbmdUb3RhbENvbnRhaW5lcjogJyNvcmRlci1zaGlwcGluZy10b3RhbC1jb250YWluZXInLFxuICBvcmRlclNoaXBwaW5nVG90YWw6ICcjb3JkZXJTaGlwcGluZ1RvdGFsJyxcbiAgb3JkZXJUYXhlc1RvdGFsOiAnI29yZGVyVGF4ZXNUb3RhbCcsXG4gIG9yZGVyVG90YWw6ICcjb3JkZXJUb3RhbCcsXG4gIG9yZGVySG9va1RhYnNDb250YWluZXI6ICcjb3JkZXJfaG9va190YWJzJyxcbiAgLy8gUHJvZHVjdCBjYW5jZWwvcmVmdW5kIGVsZW1lbnRzXG4gIGNhbmNlbFByb2R1Y3Q6IHtcbiAgICBmb3JtOiAnZm9ybVtuYW1lPVwiY2FuY2VsX3Byb2R1Y3RcIl0nLFxuICAgIGJ1dHRvbnM6IHtcbiAgICAgIGFib3J0OiAnYnV0dG9uLmNhbmNlbC1wcm9kdWN0LWVsZW1lbnQtYWJvcnQnLFxuICAgICAgc2F2ZTogJyNjYW5jZWxfcHJvZHVjdF9zYXZlJyxcbiAgICAgIHBhcnRpYWxSZWZ1bmQ6ICdidXR0b24ucGFydGlhbC1yZWZ1bmQtZGlzcGxheScsXG4gICAgICBzdGFuZGFyZFJlZnVuZDogJ2J1dHRvbi5zdGFuZGFyZC1yZWZ1bmQtZGlzcGxheScsXG4gICAgICByZXR1cm5Qcm9kdWN0OiAnYnV0dG9uLnJldHVybi1wcm9kdWN0LWRpc3BsYXknLFxuICAgICAgY2FuY2VsUHJvZHVjdHM6ICdidXR0b24uY2FuY2VsLXByb2R1Y3QtZGlzcGxheScsXG4gICAgfSxcbiAgICBpbnB1dHM6IHtcbiAgICAgIHF1YW50aXR5OiAnLmNhbmNlbC1wcm9kdWN0LXF1YW50aXR5IGlucHV0JyxcbiAgICAgIGFtb3VudDogJy5jYW5jZWwtcHJvZHVjdC1hbW91bnQgaW5wdXQnLFxuICAgICAgc2VsZWN0b3I6ICcuY2FuY2VsLXByb2R1Y3Qtc2VsZWN0b3IgaW5wdXQnLFxuICAgIH0sXG4gICAgdGFibGU6IHtcbiAgICAgIGNlbGw6ICcuY2FuY2VsLXByb2R1Y3QtY2VsbCcsXG4gICAgICBoZWFkZXI6ICd0aC5jYW5jZWwtcHJvZHVjdC1lbGVtZW50IHAnLFxuICAgICAgYWN0aW9uczogJ3RkLmNlbGxQcm9kdWN0QWN0aW9ucywgdGgucHJvZHVjdF9hY3Rpb25zJyxcbiAgICB9LFxuICAgIGNoZWNrYm94ZXM6IHtcbiAgICAgIHJlc3RvY2s6ICcjY2FuY2VsX3Byb2R1Y3RfcmVzdG9jaycsXG4gICAgICBjcmVkaXRTbGlwOiAnI2NhbmNlbF9wcm9kdWN0X2NyZWRpdF9zbGlwJyxcbiAgICAgIHZvdWNoZXI6ICcjY2FuY2VsX3Byb2R1Y3Rfdm91Y2hlcicsXG4gICAgfSxcbiAgICByYWRpb3M6IHtcbiAgICAgIHZvdWNoZXJSZWZ1bmRUeXBlOiB7XG4gICAgICAgIHByb2R1Y3RQcmljZXM6ICdpbnB1dFt2b3VjaGVyLXJlZnVuZC10eXBlPVwiMFwiXScsXG4gICAgICAgIHByb2R1Y3RQcmljZXNWb3VjaGVyRXhjbHVkZWQ6ICdpbnB1dFt2b3VjaGVyLXJlZnVuZC10eXBlPVwiMVwiXScsXG4gICAgICAgIG5lZ2F0aXZlRXJyb3JNZXNzYWdlOiAnLnZvdWNoZXItcmVmdW5kLXR5cGUtbmVnYXRpdmUtZXJyb3InLFxuICAgICAgfSxcbiAgICB9LFxuICAgIHRvZ2dsZToge1xuICAgICAgcGFydGlhbFJlZnVuZDogJy5jYW5jZWwtcHJvZHVjdC1lbGVtZW50Om5vdCguaGlkZGVuKTpub3QoLnNoaXBwaW5nLXJlZnVuZCksIC5jYW5jZWwtcHJvZHVjdC1hbW91bnQnLFxuICAgICAgc3RhbmRhcmRSZWZ1bmQ6ICcuY2FuY2VsLXByb2R1Y3QtZWxlbWVudDpub3QoLmhpZGRlbik6bm90KC5zaGlwcGluZy1yZWZ1bmQtYW1vdW50KTpub3QoLnJlc3RvY2stcHJvZHVjdHMpLCAuY2FuY2VsLXByb2R1Y3Qtc2VsZWN0b3InLFxuICAgICAgcmV0dXJuUHJvZHVjdDogJy5jYW5jZWwtcHJvZHVjdC1lbGVtZW50Om5vdCguaGlkZGVuKTpub3QoLnNoaXBwaW5nLXJlZnVuZC1hbW91bnQpLCAuY2FuY2VsLXByb2R1Y3Qtc2VsZWN0b3InLFxuICAgICAgY2FuY2VsUHJvZHVjdHM6ICcuY2FuY2VsLXByb2R1Y3QtZWxlbWVudDpub3QoLmhpZGRlbik6bm90KC5zaGlwcGluZy1yZWZ1bmQtYW1vdW50KTpub3QoLnNoaXBwaW5nLXJlZnVuZCk6bm90KC5yZXN0b2NrLXByb2R1Y3RzKTpub3QoLnJlZnVuZC1jcmVkaXQtc2xpcCk6bm90KC5yZWZ1bmQtdm91Y2hlcik6bm90KC52b3VjaGVyLXJlZnVuZC10eXBlKSwgLmNhbmNlbC1wcm9kdWN0LXNlbGVjdG9yJyxcbiAgICB9LFxuICB9LFxuICBwcmludE9yZGVyVmlld1BhZ2VCdXR0b246ICcuanMtcHJpbnQtb3JkZXItdmlldy1wYWdlJyxcbiAgb3JkZXJOb3RlVG9nZ2xlQnRuOiAnLmpzLW9yZGVyLW5vdGVzLXRvZ2dsZS1idG4nLFxuICBvcmRlck5vdGVCbG9jazogJy5qcy1vcmRlci1ub3Rlcy1ibG9jaycsXG4gIG9yZGVyTm90ZUlucHV0OiAnI2ludGVybmFsX25vdGVfbm90ZScsXG4gIG9yZGVyTm90ZVN1Ym1pdEJ0bjogJy5qcy1vcmRlci1ub3Rlcy1idG4nLFxuICByZWZyZXNoUHJvZHVjdHNMaXN0TG9hZGluZ1NwaW5uZXI6ICcjb3JkZXJQcm9kdWN0c1BhbmVsIC5zcGlubmVyLW9yZGVyLXByb2R1Y3RzLWNvbnRhaW5lciNvcmRlclByb2R1Y3RzTG9hZGluZycsXG59O1xuIiwiLyoqXG4gKiBDb3B5cmlnaHQgc2luY2UgMjAwNyBQcmVzdGFTaG9wIFNBIGFuZCBDb250cmlidXRvcnNcbiAqIFByZXN0YVNob3AgaXMgYW4gSW50ZXJuYXRpb25hbCBSZWdpc3RlcmVkIFRyYWRlbWFyayAmIFByb3BlcnR5IG9mIFByZXN0YVNob3AgU0FcbiAqXG4gKiBOT1RJQ0UgT0YgTElDRU5TRVxuICpcbiAqIFRoaXMgc291cmNlIGZpbGUgaXMgc3ViamVjdCB0byB0aGUgT3BlbiBTb2Z0d2FyZSBMaWNlbnNlIChPU0wgMy4wKVxuICogdGhhdCBpcyBidW5kbGVkIHdpdGggdGhpcyBwYWNrYWdlIGluIHRoZSBmaWxlIExJQ0VOU0UubWQuXG4gKiBJdCBpcyBhbHNvIGF2YWlsYWJsZSB0aHJvdWdoIHRoZSB3b3JsZC13aWRlLXdlYiBhdCB0aGlzIFVSTDpcbiAqIGh0dHBzOi8vb3BlbnNvdXJjZS5vcmcvbGljZW5zZXMvT1NMLTMuMFxuICogSWYgeW91IGRpZCBub3QgcmVjZWl2ZSBhIGNvcHkgb2YgdGhlIGxpY2Vuc2UgYW5kIGFyZSB1bmFibGUgdG9cbiAqIG9idGFpbiBpdCB0aHJvdWdoIHRoZSB3b3JsZC13aWRlLXdlYiwgcGxlYXNlIHNlbmQgYW4gZW1haWxcbiAqIHRvIGxpY2Vuc2VAcHJlc3Rhc2hvcC5jb20gc28gd2UgY2FuIHNlbmQgeW91IGEgY29weSBpbW1lZGlhdGVseS5cbiAqXG4gKiBESVNDTEFJTUVSXG4gKlxuICogRG8gbm90IGVkaXQgb3IgYWRkIHRvIHRoaXMgZmlsZSBpZiB5b3Ugd2lzaCB0byB1cGdyYWRlIFByZXN0YVNob3AgdG8gbmV3ZXJcbiAqIHZlcnNpb25zIGluIHRoZSBmdXR1cmUuIElmIHlvdSB3aXNoIHRvIGN1c3RvbWl6ZSBQcmVzdGFTaG9wIGZvciB5b3VyXG4gKiBuZWVkcyBwbGVhc2UgcmVmZXIgdG8gaHR0cHM6Ly9kZXZkb2NzLnByZXN0YXNob3AuY29tLyBmb3IgbW9yZSBpbmZvcm1hdGlvbi5cbiAqXG4gKiBAYXV0aG9yICAgIFByZXN0YVNob3AgU0EgYW5kIENvbnRyaWJ1dG9ycyA8Y29udGFjdEBwcmVzdGFzaG9wLmNvbT5cbiAqIEBjb3B5cmlnaHQgU2luY2UgMjAwNyBQcmVzdGFTaG9wIFNBIGFuZCBDb250cmlidXRvcnNcbiAqIEBsaWNlbnNlICAgaHR0cHM6Ly9vcGVuc291cmNlLm9yZy9saWNlbnNlcy9PU0wtMy4wIE9wZW4gU29mdHdhcmUgTGljZW5zZSAoT1NMIDMuMClcbiAqL1xuaW1wb3J0IE9yZGVyVmlld1BhZ2VNYXAgZnJvbSAnLi9PcmRlclZpZXdQYWdlTWFwJztcblxuY29uc3QgeyR9ID0gd2luZG93O1xuXG4vKipcbiAqIE1hbmFnZXMgYWRkaW5nL2VkaXRpbmcgbm90ZSBmb3IgaW52b2ljZSBkb2N1bWVudHMuXG4gKi9cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIEludm9pY2VOb3RlTWFuYWdlciB7XG4gIGNvbnN0cnVjdG9yKCkge1xuICAgIHRoaXMuc2V0dXBMaXN0ZW5lcnMoKTtcbiAgfVxuXG4gIHNldHVwTGlzdGVuZXJzKCkge1xuICAgIHRoaXMuaW5pdFNob3dOb3RlRm9ybUV2ZW50SGFuZGxlcigpO1xuICAgIHRoaXMuaW5pdENsb3NlTm90ZUZvcm1FdmVudEhhbmRsZXIoKTtcbiAgICB0aGlzLmluaXRFbnRlclBheW1lbnRFdmVudEhhbmRsZXIoKTtcbiAgfVxuXG4gIGluaXRTaG93Tm90ZUZvcm1FdmVudEhhbmRsZXIoKSB7XG4gICAgJCgnLmpzLW9wZW4taW52b2ljZS1ub3RlLWJ0bicpLm9uKCdjbGljaycsIChldmVudCkgPT4ge1xuICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgIGNvbnN0ICRidG4gPSAkKGV2ZW50LmN1cnJlbnRUYXJnZXQpO1xuICAgICAgY29uc3QgJG5vdGVSb3cgPSAkYnRuLmNsb3Nlc3QoJ3RyJykubmV4dCgpO1xuXG4gICAgICAkbm90ZVJvdy5yZW1vdmVDbGFzcygnZC1ub25lJyk7XG4gICAgfSk7XG4gIH1cblxuICBpbml0Q2xvc2VOb3RlRm9ybUV2ZW50SGFuZGxlcigpIHtcbiAgICAkKCcuanMtY2FuY2VsLWludm9pY2Utbm90ZS1idG4nKS5vbignY2xpY2snLCAoZXZlbnQpID0+IHtcbiAgICAgICQoZXZlbnQuY3VycmVudFRhcmdldCkuY2xvc2VzdCgndHInKS5hZGRDbGFzcygnZC1ub25lJyk7XG4gICAgfSk7XG4gIH1cblxuICBpbml0RW50ZXJQYXltZW50RXZlbnRIYW5kbGVyKCkge1xuICAgICQoJy5qcy1lbnRlci1wYXltZW50LWJ0bicpLm9uKCdjbGljaycsIChldmVudCkgPT4ge1xuICAgICAgY29uc3QgJGJ0biA9ICQoZXZlbnQuY3VycmVudFRhcmdldCk7XG4gICAgICBjb25zdCBwYXltZW50QW1vdW50ID0gJGJ0bi5kYXRhKCdwYXltZW50LWFtb3VudCcpO1xuXG4gICAgICAkKE9yZGVyVmlld1BhZ2VNYXAudmlld09yZGVyUGF5bWVudHNCbG9jaykuZ2V0KDApLnNjcm9sbEludG9WaWV3KHtiZWhhdmlvcjogJ3Ntb290aCd9KTtcbiAgICAgICQoT3JkZXJWaWV3UGFnZU1hcC5vcmRlclBheW1lbnRGb3JtQW1vdW50SW5wdXQpLnZhbChwYXltZW50QW1vdW50KTtcbiAgICB9KTtcbiAgfVxufVxuIiwiLyoqXG4gKiBDb3B5cmlnaHQgc2luY2UgMjAwNyBQcmVzdGFTaG9wIFNBIGFuZCBDb250cmlidXRvcnNcbiAqIFByZXN0YVNob3AgaXMgYW4gSW50ZXJuYXRpb25hbCBSZWdpc3RlcmVkIFRyYWRlbWFyayAmIFByb3BlcnR5IG9mIFByZXN0YVNob3AgU0FcbiAqXG4gKiBOT1RJQ0UgT0YgTElDRU5TRVxuICpcbiAqIFRoaXMgc291cmNlIGZpbGUgaXMgc3ViamVjdCB0byB0aGUgT3BlbiBTb2Z0d2FyZSBMaWNlbnNlIChPU0wgMy4wKVxuICogdGhhdCBpcyBidW5kbGVkIHdpdGggdGhpcyBwYWNrYWdlIGluIHRoZSBmaWxlIExJQ0VOU0UubWQuXG4gKiBJdCBpcyBhbHNvIGF2YWlsYWJsZSB0aHJvdWdoIHRoZSB3b3JsZC13aWRlLXdlYiBhdCB0aGlzIFVSTDpcbiAqIGh0dHBzOi8vb3BlbnNvdXJjZS5vcmcvbGljZW5zZXMvT1NMLTMuMFxuICogSWYgeW91IGRpZCBub3QgcmVjZWl2ZSBhIGNvcHkgb2YgdGhlIGxpY2Vuc2UgYW5kIGFyZSB1bmFibGUgdG9cbiAqIG9idGFpbiBpdCB0aHJvdWdoIHRoZSB3b3JsZC13aWRlLXdlYiwgcGxlYXNlIHNlbmQgYW4gZW1haWxcbiAqIHRvIGxpY2Vuc2VAcHJlc3Rhc2hvcC5jb20gc28gd2UgY2FuIHNlbmQgeW91IGEgY29weSBpbW1lZGlhdGVseS5cbiAqXG4gKiBESVNDTEFJTUVSXG4gKlxuICogRG8gbm90IGVkaXQgb3IgYWRkIHRvIHRoaXMgZmlsZSBpZiB5b3Ugd2lzaCB0byB1cGdyYWRlIFByZXN0YVNob3AgdG8gbmV3ZXJcbiAqIHZlcnNpb25zIGluIHRoZSBmdXR1cmUuIElmIHlvdSB3aXNoIHRvIGN1c3RvbWl6ZSBQcmVzdGFTaG9wIGZvciB5b3VyXG4gKiBuZWVkcyBwbGVhc2UgcmVmZXIgdG8gaHR0cHM6Ly9kZXZkb2NzLnByZXN0YXNob3AuY29tLyBmb3IgbW9yZSBpbmZvcm1hdGlvbi5cbiAqXG4gKiBAYXV0aG9yICAgIFByZXN0YVNob3AgU0EgYW5kIENvbnRyaWJ1dG9ycyA8Y29udGFjdEBwcmVzdGFzaG9wLmNvbT5cbiAqIEBjb3B5cmlnaHQgU2luY2UgMjAwNyBQcmVzdGFTaG9wIFNBIGFuZCBDb250cmlidXRvcnNcbiAqIEBsaWNlbnNlICAgaHR0cHM6Ly9vcGVuc291cmNlLm9yZy9saWNlbnNlcy9PU0wtMy4wIE9wZW4gU29mdHdhcmUgTGljZW5zZSAoT1NMIDMuMClcbiAqL1xuXG5pbXBvcnQgT3JkZXJWaWV3UGFnZU1hcCBmcm9tICcuLi9PcmRlclZpZXdQYWdlTWFwJztcblxuY29uc3QgeyR9ID0gd2luZG93O1xuXG4vKipcbiAqIEFsbCBhY3Rpb25zIGZvciBvcmRlciB2aWV3IHBhZ2UgbWVzc2FnZXMgYXJlIHJlZ2lzdGVyZWQgaW4gdGhpcyBjbGFzcy5cbiAqL1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgT3JkZXJWaWV3UGFnZU1lc3NhZ2VzSGFuZGxlciB7XG4gIGNvbnN0cnVjdG9yKCkge1xuICAgIHRoaXMuJG9yZGVyTWVzc2FnZUNoYW5nZVdhcm5pbmcgPSAkKE9yZGVyVmlld1BhZ2VNYXAub3JkZXJNZXNzYWdlQ2hhbmdlV2FybmluZyk7XG4gICAgdGhpcy4kbWVzc2FnZXNDb250YWluZXIgPSAkKE9yZGVyVmlld1BhZ2VNYXAub3JkZXJNZXNzYWdlc0NvbnRhaW5lcik7XG5cbiAgICByZXR1cm4ge1xuICAgICAgbGlzdGVuRm9yUHJlZGVmaW5lZE1lc3NhZ2VTZWxlY3Rpb246ICgpID0+IHRoaXMuaGFuZGxlUHJlZGVmaW5lZE1lc3NhZ2VTZWxlY3Rpb24oKSxcbiAgICAgIGxpc3RlbkZvckZ1bGxNZXNzYWdlc09wZW46ICgpID0+IHRoaXMub25GdWxsTWVzc2FnZXNPcGVuKCksXG4gICAgfTtcbiAgfVxuXG4gIC8qKlxuICAgKiBIYW5kbGVzIHByZWRlZmluZWQgb3JkZXIgbWVzc2FnZSBzZWxlY3Rpb24uXG4gICAqXG4gICAqIEBwcml2YXRlXG4gICAqL1xuICBoYW5kbGVQcmVkZWZpbmVkTWVzc2FnZVNlbGVjdGlvbigpIHtcbiAgICAkKGRvY3VtZW50KS5vbignY2hhbmdlJywgT3JkZXJWaWV3UGFnZU1hcC5vcmRlck1lc3NhZ2VOYW1lU2VsZWN0LCAoZSkgPT4ge1xuICAgICAgY29uc3QgJGN1cnJlbnRJdGVtID0gJChlLmN1cnJlbnRUYXJnZXQpO1xuICAgICAgY29uc3QgdmFsdWVJZCA9ICRjdXJyZW50SXRlbS52YWwoKTtcblxuICAgICAgaWYgKCF2YWx1ZUlkKSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cblxuICAgICAgY29uc3QgbWVzc2FnZSA9IHRoaXMuJG1lc3NhZ2VzQ29udGFpbmVyLmZpbmQoYGRpdltkYXRhLWlkPSR7dmFsdWVJZH1dYCkudGV4dCgpLnRyaW0oKTtcbiAgICAgIGNvbnN0ICRvcmRlck1lc3NhZ2UgPSAkKE9yZGVyVmlld1BhZ2VNYXAub3JkZXJNZXNzYWdlKTtcbiAgICAgIGNvbnN0IGlzU2FtZU1lc3NhZ2UgPSAkb3JkZXJNZXNzYWdlLnZhbCgpLnRyaW0oKSA9PT0gbWVzc2FnZTtcblxuICAgICAgaWYgKGlzU2FtZU1lc3NhZ2UpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICBpZiAoJG9yZGVyTWVzc2FnZS52YWwoKSAmJiAhd2luZG93LmNvbmZpcm0odGhpcy4kb3JkZXJNZXNzYWdlQ2hhbmdlV2FybmluZy50ZXh0KCkpKSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cblxuICAgICAgJG9yZGVyTWVzc2FnZS52YWwobWVzc2FnZSk7XG4gICAgICAkb3JkZXJNZXNzYWdlLnRyaWdnZXIoJ2lucHV0Jyk7XG4gICAgfSk7XG4gIH1cblxuICAvKipcbiAgICogTGlzdGVucyBmb3IgZXZlbnQgd2hlbiBhbGwgbWVzc2FnZXMgbW9kYWwgaXMgYmVpbmcgb3BlbmVkXG4gICAqXG4gICAqIEBwcml2YXRlXG4gICAqL1xuICBvbkZ1bGxNZXNzYWdlc09wZW4oKSB7XG4gICAgJChkb2N1bWVudCkub24oJ2NsaWNrJywgT3JkZXJWaWV3UGFnZU1hcC5vcGVuQWxsTWVzc2FnZXNCdG4sICgpID0+IHRoaXMuc2Nyb2xsVG9Nc2dMaXN0Qm90dG9tKCkpO1xuICB9XG5cbiAgLyoqXG4gICAqIFNjcm9sbHMgZG93biB0byB0aGUgYm90dG9tIG9mIGFsbCBtZXNzYWdlcyBsaXN0XG4gICAqXG4gICAqIEBwcml2YXRlXG4gICAqL1xuICBzY3JvbGxUb01zZ0xpc3RCb3R0b20oKSB7XG4gICAgY29uc3QgJG1zZ01vZGFsID0gJChPcmRlclZpZXdQYWdlTWFwLmFsbE1lc3NhZ2VzTW9kYWwpO1xuICAgIGNvbnN0IG1zZ0xpc3QgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKE9yZGVyVmlld1BhZ2VNYXAuYWxsTWVzc2FnZXNMaXN0KTtcblxuICAgIGNvbnN0IGNsYXNzQ2hlY2tJbnRlcnZhbCA9IHdpbmRvdy5zZXRJbnRlcnZhbCgoKSA9PiB7XG4gICAgICBpZiAoJG1zZ01vZGFsLmhhc0NsYXNzKCdzaG93JykpIHtcbiAgICAgICAgbXNnTGlzdC5zY3JvbGxUb3AgPSBtc2dMaXN0LnNjcm9sbEhlaWdodDtcbiAgICAgICAgY2xlYXJJbnRlcnZhbChjbGFzc0NoZWNrSW50ZXJ2YWwpO1xuICAgICAgfVxuICAgIH0sIDEwKTtcbiAgfVxufVxuIiwiLyoqXG4gKiBDb3B5cmlnaHQgc2luY2UgMjAwNyBQcmVzdGFTaG9wIFNBIGFuZCBDb250cmlidXRvcnNcbiAqIFByZXN0YVNob3AgaXMgYW4gSW50ZXJuYXRpb25hbCBSZWdpc3RlcmVkIFRyYWRlbWFyayAmIFByb3BlcnR5IG9mIFByZXN0YVNob3AgU0FcbiAqXG4gKiBOT1RJQ0UgT0YgTElDRU5TRVxuICpcbiAqIFRoaXMgc291cmNlIGZpbGUgaXMgc3ViamVjdCB0byB0aGUgT3BlbiBTb2Z0d2FyZSBMaWNlbnNlIChPU0wgMy4wKVxuICogdGhhdCBpcyBidW5kbGVkIHdpdGggdGhpcyBwYWNrYWdlIGluIHRoZSBmaWxlIExJQ0VOU0UubWQuXG4gKiBJdCBpcyBhbHNvIGF2YWlsYWJsZSB0aHJvdWdoIHRoZSB3b3JsZC13aWRlLXdlYiBhdCB0aGlzIFVSTDpcbiAqIGh0dHBzOi8vb3BlbnNvdXJjZS5vcmcvbGljZW5zZXMvT1NMLTMuMFxuICogSWYgeW91IGRpZCBub3QgcmVjZWl2ZSBhIGNvcHkgb2YgdGhlIGxpY2Vuc2UgYW5kIGFyZSB1bmFibGUgdG9cbiAqIG9idGFpbiBpdCB0aHJvdWdoIHRoZSB3b3JsZC13aWRlLXdlYiwgcGxlYXNlIHNlbmQgYW4gZW1haWxcbiAqIHRvIGxpY2Vuc2VAcHJlc3Rhc2hvcC5jb20gc28gd2UgY2FuIHNlbmQgeW91IGEgY29weSBpbW1lZGlhdGVseS5cbiAqXG4gKiBESVNDTEFJTUVSXG4gKlxuICogRG8gbm90IGVkaXQgb3IgYWRkIHRvIHRoaXMgZmlsZSBpZiB5b3Ugd2lzaCB0byB1cGdyYWRlIFByZXN0YVNob3AgdG8gbmV3ZXJcbiAqIHZlcnNpb25zIGluIHRoZSBmdXR1cmUuIElmIHlvdSB3aXNoIHRvIGN1c3RvbWl6ZSBQcmVzdGFTaG9wIGZvciB5b3VyXG4gKiBuZWVkcyBwbGVhc2UgcmVmZXIgdG8gaHR0cHM6Ly9kZXZkb2NzLnByZXN0YXNob3AuY29tLyBmb3IgbW9yZSBpbmZvcm1hdGlvbi5cbiAqXG4gKiBAYXV0aG9yICAgIFByZXN0YVNob3AgU0EgYW5kIENvbnRyaWJ1dG9ycyA8Y29udGFjdEBwcmVzdGFzaG9wLmNvbT5cbiAqIEBjb3B5cmlnaHQgU2luY2UgMjAwNyBQcmVzdGFTaG9wIFNBIGFuZCBDb250cmlidXRvcnNcbiAqIEBsaWNlbnNlICAgaHR0cHM6Ly9vcGVuc291cmNlLm9yZy9saWNlbnNlcy9PU0wtMy4wIE9wZW4gU29mdHdhcmUgTGljZW5zZSAoT1NMIDMuMClcbiAqL1xuaW1wb3J0IE9yZGVyVmlld1BhZ2VNYXAgZnJvbSAnLi9PcmRlclZpZXdQYWdlTWFwJztcblxuY29uc3QgeyR9ID0gd2luZG93O1xuXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBPcmRlclNoaXBwaW5nTWFuYWdlciB7XG4gIGNvbnN0cnVjdG9yKCkge1xuICAgIHRoaXMuaW5pdE9yZGVyU2hpcHBpbmdVcGRhdGVFdmVudEhhbmRsZXIoKTtcbiAgfVxuXG4gIGluaXRPcmRlclNoaXBwaW5nVXBkYXRlRXZlbnRIYW5kbGVyKCkge1xuICAgICQoT3JkZXJWaWV3UGFnZU1hcC5tYWluRGl2KS5vbignY2xpY2snLCBPcmRlclZpZXdQYWdlTWFwLnNob3dPcmRlclNoaXBwaW5nVXBkYXRlTW9kYWxCdG4sIChldmVudCkgPT4ge1xuICAgICAgY29uc3QgJGJ0biA9ICQoZXZlbnQuY3VycmVudFRhcmdldCk7XG5cbiAgICAgICQoT3JkZXJWaWV3UGFnZU1hcC51cGRhdGVPcmRlclNoaXBwaW5nVHJhY2tpbmdOdW1iZXJJbnB1dCkudmFsKCRidG4uZGF0YSgnb3JkZXItdHJhY2tpbmctbnVtYmVyJykpO1xuICAgICAgJChPcmRlclZpZXdQYWdlTWFwLnVwZGF0ZU9yZGVyU2hpcHBpbmdDdXJyZW50T3JkZXJDYXJyaWVySWRJbnB1dCkudmFsKCRidG4uZGF0YSgnb3JkZXItY2Fycmllci1pZCcpKTtcbiAgICB9KTtcbiAgfVxufVxuIiwiLyoqXG4gKiBDb3B5cmlnaHQgc2luY2UgMjAwNyBQcmVzdGFTaG9wIFNBIGFuZCBDb250cmlidXRvcnNcbiAqIFByZXN0YVNob3AgaXMgYW4gSW50ZXJuYXRpb25hbCBSZWdpc3RlcmVkIFRyYWRlbWFyayAmIFByb3BlcnR5IG9mIFByZXN0YVNob3AgU0FcbiAqXG4gKiBOT1RJQ0UgT0YgTElDRU5TRVxuICpcbiAqIFRoaXMgc291cmNlIGZpbGUgaXMgc3ViamVjdCB0byB0aGUgT3BlbiBTb2Z0d2FyZSBMaWNlbnNlIChPU0wgMy4wKVxuICogdGhhdCBpcyBidW5kbGVkIHdpdGggdGhpcyBwYWNrYWdlIGluIHRoZSBmaWxlIExJQ0VOU0UubWQuXG4gKiBJdCBpcyBhbHNvIGF2YWlsYWJsZSB0aHJvdWdoIHRoZSB3b3JsZC13aWRlLXdlYiBhdCB0aGlzIFVSTDpcbiAqIGh0dHBzOi8vb3BlbnNvdXJjZS5vcmcvbGljZW5zZXMvT1NMLTMuMFxuICogSWYgeW91IGRpZCBub3QgcmVjZWl2ZSBhIGNvcHkgb2YgdGhlIGxpY2Vuc2UgYW5kIGFyZSB1bmFibGUgdG9cbiAqIG9idGFpbiBpdCB0aHJvdWdoIHRoZSB3b3JsZC13aWRlLXdlYiwgcGxlYXNlIHNlbmQgYW4gZW1haWxcbiAqIHRvIGxpY2Vuc2VAcHJlc3Rhc2hvcC5jb20gc28gd2UgY2FuIHNlbmQgeW91IGEgY29weSBpbW1lZGlhdGVseS5cbiAqXG4gKiBESVNDTEFJTUVSXG4gKlxuICogRG8gbm90IGVkaXQgb3IgYWRkIHRvIHRoaXMgZmlsZSBpZiB5b3Ugd2lzaCB0byB1cGdyYWRlIFByZXN0YVNob3AgdG8gbmV3ZXJcbiAqIHZlcnNpb25zIGluIHRoZSBmdXR1cmUuIElmIHlvdSB3aXNoIHRvIGN1c3RvbWl6ZSBQcmVzdGFTaG9wIGZvciB5b3VyXG4gKiBuZWVkcyBwbGVhc2UgcmVmZXIgdG8gaHR0cHM6Ly9kZXZkb2NzLnByZXN0YXNob3AuY29tLyBmb3IgbW9yZSBpbmZvcm1hdGlvbi5cbiAqXG4gKiBAYXV0aG9yICAgIFByZXN0YVNob3AgU0EgYW5kIENvbnRyaWJ1dG9ycyA8Y29udGFjdEBwcmVzdGFzaG9wLmNvbT5cbiAqIEBjb3B5cmlnaHQgU2luY2UgMjAwNyBQcmVzdGFTaG9wIFNBIGFuZCBDb250cmlidXRvcnNcbiAqIEBsaWNlbnNlICAgaHR0cHM6Ly9vcGVuc291cmNlLm9yZy9saWNlbnNlcy9PU0wtMy4wIE9wZW4gU29mdHdhcmUgTGljZW5zZSAoT1NMIDMuMClcbiAqL1xuXG5pbXBvcnQgUm91dGVyIGZyb20gJ0Bjb21wb25lbnRzL3JvdXRlcic7XG5pbXBvcnQgT3JkZXJWaWV3UGFnZU1hcCBmcm9tICdAcGFnZXMvb3JkZXIvT3JkZXJWaWV3UGFnZU1hcCc7XG5cbmNvbnN0IHskfSA9IHdpbmRvdztcblxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgT3JkZXJEaXNjb3VudHNSZWZyZXNoZXIge1xuICBjb25zdHJ1Y3RvcigpIHtcbiAgICB0aGlzLnJvdXRlciA9IG5ldyBSb3V0ZXIoKTtcbiAgfVxuXG4gIHJlZnJlc2gob3JkZXJJZCkge1xuICAgICQuYWpheCh0aGlzLnJvdXRlci5nZW5lcmF0ZSgnYWRtaW5fb3JkZXJzX2dldF9kaXNjb3VudHMnLCB7b3JkZXJJZH0pKVxuICAgICAgLnRoZW4oKHJlc3BvbnNlKSA9PiB7XG4gICAgICAgICQoT3JkZXJWaWV3UGFnZU1hcC5wcm9kdWN0RGlzY291bnRMaXN0Lmxpc3QpLnJlcGxhY2VXaXRoKHJlc3BvbnNlKTtcbiAgICAgIH0pO1xuICB9XG59XG4iLCIvKipcbiAqIENvcHlyaWdodCBzaW5jZSAyMDA3IFByZXN0YVNob3AgU0EgYW5kIENvbnRyaWJ1dG9yc1xuICogUHJlc3RhU2hvcCBpcyBhbiBJbnRlcm5hdGlvbmFsIFJlZ2lzdGVyZWQgVHJhZGVtYXJrICYgUHJvcGVydHkgb2YgUHJlc3RhU2hvcCBTQVxuICpcbiAqIE5PVElDRSBPRiBMSUNFTlNFXG4gKlxuICogVGhpcyBzb3VyY2UgZmlsZSBpcyBzdWJqZWN0IHRvIHRoZSBPcGVuIFNvZnR3YXJlIExpY2Vuc2UgKE9TTCAzLjApXG4gKiB0aGF0IGlzIGJ1bmRsZWQgd2l0aCB0aGlzIHBhY2thZ2UgaW4gdGhlIGZpbGUgTElDRU5TRS5tZC5cbiAqIEl0IGlzIGFsc28gYXZhaWxhYmxlIHRocm91Z2ggdGhlIHdvcmxkLXdpZGUtd2ViIGF0IHRoaXMgVVJMOlxuICogaHR0cHM6Ly9vcGVuc291cmNlLm9yZy9saWNlbnNlcy9PU0wtMy4wXG4gKiBJZiB5b3UgZGlkIG5vdCByZWNlaXZlIGEgY29weSBvZiB0aGUgbGljZW5zZSBhbmQgYXJlIHVuYWJsZSB0b1xuICogb2J0YWluIGl0IHRocm91Z2ggdGhlIHdvcmxkLXdpZGUtd2ViLCBwbGVhc2Ugc2VuZCBhbiBlbWFpbFxuICogdG8gbGljZW5zZUBwcmVzdGFzaG9wLmNvbSBzbyB3ZSBjYW4gc2VuZCB5b3UgYSBjb3B5IGltbWVkaWF0ZWx5LlxuICpcbiAqIERJU0NMQUlNRVJcbiAqXG4gKiBEbyBub3QgZWRpdCBvciBhZGQgdG8gdGhpcyBmaWxlIGlmIHlvdSB3aXNoIHRvIHVwZ3JhZGUgUHJlc3RhU2hvcCB0byBuZXdlclxuICogdmVyc2lvbnMgaW4gdGhlIGZ1dHVyZS4gSWYgeW91IHdpc2ggdG8gY3VzdG9taXplIFByZXN0YVNob3AgZm9yIHlvdXJcbiAqIG5lZWRzIHBsZWFzZSByZWZlciB0byBodHRwczovL2RldmRvY3MucHJlc3Rhc2hvcC5jb20vIGZvciBtb3JlIGluZm9ybWF0aW9uLlxuICpcbiAqIEBhdXRob3IgICAgUHJlc3RhU2hvcCBTQSBhbmQgQ29udHJpYnV0b3JzIDxjb250YWN0QHByZXN0YXNob3AuY29tPlxuICogQGNvcHlyaWdodCBTaW5jZSAyMDA3IFByZXN0YVNob3AgU0EgYW5kIENvbnRyaWJ1dG9yc1xuICogQGxpY2Vuc2UgICBodHRwczovL29wZW5zb3VyY2Uub3JnL2xpY2Vuc2VzL09TTC0zLjAgT3BlbiBTb2Z0d2FyZSBMaWNlbnNlIChPU0wgMy4wKVxuICovXG5cbmltcG9ydCBSb3V0ZXIgZnJvbSAnQGNvbXBvbmVudHMvcm91dGVyJztcbmltcG9ydCBPcmRlclZpZXdQYWdlTWFwIGZyb20gJ0BwYWdlcy9vcmRlci9PcmRlclZpZXdQYWdlTWFwJztcbmltcG9ydCBJbnZvaWNlTm90ZU1hbmFnZXIgZnJvbSAnLi4vaW52b2ljZS1ub3RlLW1hbmFnZXInO1xuXG5jb25zdCB7JH0gPSB3aW5kb3c7XG5cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIE9yZGVyRG9jdW1lbnRzUmVmcmVzaGVyIHtcbiAgY29uc3RydWN0b3IoKSB7XG4gICAgdGhpcy5yb3V0ZXIgPSBuZXcgUm91dGVyKCk7XG4gICAgdGhpcy5pbnZvaWNlTm90ZU1hbmFnZXIgPSBuZXcgSW52b2ljZU5vdGVNYW5hZ2VyKCk7XG4gIH1cblxuICByZWZyZXNoKG9yZGVySWQpIHtcbiAgICAkLmdldEpTT04odGhpcy5yb3V0ZXIuZ2VuZXJhdGUoJ2FkbWluX29yZGVyc19nZXRfZG9jdW1lbnRzJywge29yZGVySWR9KSlcbiAgICAgIC50aGVuKChyZXNwb25zZSkgPT4ge1xuICAgICAgICAkKE9yZGVyVmlld1BhZ2VNYXAub3JkZXJEb2N1bWVudHNUYWJDb3VudCkudGV4dChyZXNwb25zZS50b3RhbCk7XG4gICAgICAgICQoT3JkZXJWaWV3UGFnZU1hcC5vcmRlckRvY3VtZW50c1RhYkJvZHkpLmh0bWwocmVzcG9uc2UuaHRtbCk7XG4gICAgICAgIHRoaXMuaW52b2ljZU5vdGVNYW5hZ2VyLnNldHVwTGlzdGVuZXJzKCk7XG4gICAgICB9KTtcbiAgfVxufVxuIiwiLyoqXG4gKiBDb3B5cmlnaHQgc2luY2UgMjAwNyBQcmVzdGFTaG9wIFNBIGFuZCBDb250cmlidXRvcnNcbiAqIFByZXN0YVNob3AgaXMgYW4gSW50ZXJuYXRpb25hbCBSZWdpc3RlcmVkIFRyYWRlbWFyayAmIFByb3BlcnR5IG9mIFByZXN0YVNob3AgU0FcbiAqXG4gKiBOT1RJQ0UgT0YgTElDRU5TRVxuICpcbiAqIFRoaXMgc291cmNlIGZpbGUgaXMgc3ViamVjdCB0byB0aGUgT3BlbiBTb2Z0d2FyZSBMaWNlbnNlIChPU0wgMy4wKVxuICogdGhhdCBpcyBidW5kbGVkIHdpdGggdGhpcyBwYWNrYWdlIGluIHRoZSBmaWxlIExJQ0VOU0UubWQuXG4gKiBJdCBpcyBhbHNvIGF2YWlsYWJsZSB0aHJvdWdoIHRoZSB3b3JsZC13aWRlLXdlYiBhdCB0aGlzIFVSTDpcbiAqIGh0dHBzOi8vb3BlbnNvdXJjZS5vcmcvbGljZW5zZXMvT1NMLTMuMFxuICogSWYgeW91IGRpZCBub3QgcmVjZWl2ZSBhIGNvcHkgb2YgdGhlIGxpY2Vuc2UgYW5kIGFyZSB1bmFibGUgdG9cbiAqIG9idGFpbiBpdCB0aHJvdWdoIHRoZSB3b3JsZC13aWRlLXdlYiwgcGxlYXNlIHNlbmQgYW4gZW1haWxcbiAqIHRvIGxpY2Vuc2VAcHJlc3Rhc2hvcC5jb20gc28gd2UgY2FuIHNlbmQgeW91IGEgY29weSBpbW1lZGlhdGVseS5cbiAqXG4gKiBESVNDTEFJTUVSXG4gKlxuICogRG8gbm90IGVkaXQgb3IgYWRkIHRvIHRoaXMgZmlsZSBpZiB5b3Ugd2lzaCB0byB1cGdyYWRlIFByZXN0YVNob3AgdG8gbmV3ZXJcbiAqIHZlcnNpb25zIGluIHRoZSBmdXR1cmUuIElmIHlvdSB3aXNoIHRvIGN1c3RvbWl6ZSBQcmVzdGFTaG9wIGZvciB5b3VyXG4gKiBuZWVkcyBwbGVhc2UgcmVmZXIgdG8gaHR0cHM6Ly9kZXZkb2NzLnByZXN0YXNob3AuY29tLyBmb3IgbW9yZSBpbmZvcm1hdGlvbi5cbiAqXG4gKiBAYXV0aG9yICAgIFByZXN0YVNob3AgU0EgYW5kIENvbnRyaWJ1dG9ycyA8Y29udGFjdEBwcmVzdGFzaG9wLmNvbT5cbiAqIEBjb3B5cmlnaHQgU2luY2UgMjAwNyBQcmVzdGFTaG9wIFNBIGFuZCBDb250cmlidXRvcnNcbiAqIEBsaWNlbnNlICAgaHR0cHM6Ly9vcGVuc291cmNlLm9yZy9saWNlbnNlcy9PU0wtMy4wIE9wZW4gU29mdHdhcmUgTGljZW5zZSAoT1NMIDMuMClcbiAqL1xuXG5pbXBvcnQgUm91dGVyIGZyb20gJ0Bjb21wb25lbnRzL3JvdXRlcic7XG5pbXBvcnQgT3JkZXJWaWV3UGFnZU1hcCBmcm9tICdAcGFnZXMvb3JkZXIvT3JkZXJWaWV3UGFnZU1hcCc7XG5cbmNvbnN0IHskfSA9IHdpbmRvdztcblxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgT3JkZXJJbnZvaWNlc1JlZnJlc2hlciB7XG4gIGNvbnN0cnVjdG9yKCkge1xuICAgIHRoaXMucm91dGVyID0gbmV3IFJvdXRlcigpO1xuICB9XG5cbiAgcmVmcmVzaChvcmRlcklkKSB7XG4gICAgJC5nZXRKU09OKHRoaXMucm91dGVyLmdlbmVyYXRlKCdhZG1pbl9vcmRlcnNfZ2V0X2ludm9pY2VzJywge29yZGVySWR9KSlcbiAgICAgIC50aGVuKChyZXNwb25zZSkgPT4ge1xuICAgICAgICBpZiAoIXJlc3BvbnNlIHx8ICFyZXNwb25zZS5pbnZvaWNlcyB8fCBPYmplY3Qua2V5cyhyZXNwb25zZS5pbnZvaWNlcykubGVuZ3RoIDw9IDApIHtcbiAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cblxuICAgICAgICBjb25zdCAkcGF5bWVudEludm9pY2VTZWxlY3QgPSAkKE9yZGVyVmlld1BhZ2VNYXAub3JkZXJQYXltZW50SW52b2ljZVNlbGVjdCk7XG4gICAgICAgIGNvbnN0ICRhZGRQcm9kdWN0SW52b2ljZVNlbGVjdCA9ICQoT3JkZXJWaWV3UGFnZU1hcC5wcm9kdWN0QWRkSW52b2ljZVNlbGVjdCk7XG4gICAgICAgIGNvbnN0ICRleGlzdGluZ0ludm9pY2VzR3JvdXAgPSAkYWRkUHJvZHVjdEludm9pY2VTZWxlY3QuZmluZCgnb3B0Z3JvdXA6Zmlyc3QnKTtcbiAgICAgICAgY29uc3QgJHByb2R1Y3RFZGl0SW52b2ljZVNlbGVjdCA9ICQoT3JkZXJWaWV3UGFnZU1hcC5wcm9kdWN0RWRpdEludm9pY2VTZWxlY3QpO1xuICAgICAgICBjb25zdCAkYWRkRGlzY291bnRJbnZvaWNlU2VsZWN0ID0gJChPcmRlclZpZXdQYWdlTWFwLmFkZENhcnRSdWxlSW52b2ljZUlkU2VsZWN0KTtcbiAgICAgICAgJGV4aXN0aW5nSW52b2ljZXNHcm91cC5lbXB0eSgpO1xuICAgICAgICAkcGF5bWVudEludm9pY2VTZWxlY3QuZW1wdHkoKTtcbiAgICAgICAgJHByb2R1Y3RFZGl0SW52b2ljZVNlbGVjdC5lbXB0eSgpO1xuICAgICAgICAkYWRkRGlzY291bnRJbnZvaWNlU2VsZWN0LmVtcHR5KCk7XG5cbiAgICAgICAgT2JqZWN0LmtleXMocmVzcG9uc2UuaW52b2ljZXMpLmZvckVhY2goKGludm9pY2VOYW1lKSA9PiB7XG4gICAgICAgICAgY29uc3QgaW52b2ljZUlkID0gcmVzcG9uc2UuaW52b2ljZXNbaW52b2ljZU5hbWVdO1xuICAgICAgICAgIGNvbnN0IGludm9pY2VOYW1lV2l0aG91dFByaWNlID0gaW52b2ljZU5hbWUuc3BsaXQoJyAtICcpWzBdO1xuXG4gICAgICAgICAgJGV4aXN0aW5nSW52b2ljZXNHcm91cC5hcHBlbmQoYDxvcHRpb24gdmFsdWU9XCIke2ludm9pY2VJZH1cIj4ke2ludm9pY2VOYW1lV2l0aG91dFByaWNlfTwvb3B0aW9uPmApO1xuICAgICAgICAgICRwYXltZW50SW52b2ljZVNlbGVjdC5hcHBlbmQoYDxvcHRpb24gdmFsdWU9XCIke2ludm9pY2VJZH1cIj4ke2ludm9pY2VOYW1lV2l0aG91dFByaWNlfTwvb3B0aW9uPmApO1xuICAgICAgICAgICRwcm9kdWN0RWRpdEludm9pY2VTZWxlY3QuYXBwZW5kKGA8b3B0aW9uIHZhbHVlPVwiJHtpbnZvaWNlSWR9XCI+JHtpbnZvaWNlTmFtZVdpdGhvdXRQcmljZX08L29wdGlvbj5gKTtcbiAgICAgICAgICAkYWRkRGlzY291bnRJbnZvaWNlU2VsZWN0LmFwcGVuZChgPG9wdGlvbiB2YWx1ZT1cIiR7aW52b2ljZUlkfVwiPiR7aW52b2ljZU5hbWV9PC9vcHRpb24+YCk7XG4gICAgICAgIH0pO1xuXG4gICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoT3JkZXJWaWV3UGFnZU1hcC5wcm9kdWN0QWRkSW52b2ljZVNlbGVjdCkuc2VsZWN0ZWRJbmRleCA9IDA7XG4gICAgICB9KTtcbiAgfVxufVxuIiwiLyoqXG4gKiBDb3B5cmlnaHQgc2luY2UgMjAwNyBQcmVzdGFTaG9wIFNBIGFuZCBDb250cmlidXRvcnNcbiAqIFByZXN0YVNob3AgaXMgYW4gSW50ZXJuYXRpb25hbCBSZWdpc3RlcmVkIFRyYWRlbWFyayAmIFByb3BlcnR5IG9mIFByZXN0YVNob3AgU0FcbiAqXG4gKiBOT1RJQ0UgT0YgTElDRU5TRVxuICpcbiAqIFRoaXMgc291cmNlIGZpbGUgaXMgc3ViamVjdCB0byB0aGUgT3BlbiBTb2Z0d2FyZSBMaWNlbnNlIChPU0wgMy4wKVxuICogdGhhdCBpcyBidW5kbGVkIHdpdGggdGhpcyBwYWNrYWdlIGluIHRoZSBmaWxlIExJQ0VOU0UubWQuXG4gKiBJdCBpcyBhbHNvIGF2YWlsYWJsZSB0aHJvdWdoIHRoZSB3b3JsZC13aWRlLXdlYiBhdCB0aGlzIFVSTDpcbiAqIGh0dHBzOi8vb3BlbnNvdXJjZS5vcmcvbGljZW5zZXMvT1NMLTMuMFxuICogSWYgeW91IGRpZCBub3QgcmVjZWl2ZSBhIGNvcHkgb2YgdGhlIGxpY2Vuc2UgYW5kIGFyZSB1bmFibGUgdG9cbiAqIG9idGFpbiBpdCB0aHJvdWdoIHRoZSB3b3JsZC13aWRlLXdlYiwgcGxlYXNlIHNlbmQgYW4gZW1haWxcbiAqIHRvIGxpY2Vuc2VAcHJlc3Rhc2hvcC5jb20gc28gd2UgY2FuIHNlbmQgeW91IGEgY29weSBpbW1lZGlhdGVseS5cbiAqXG4gKiBESVNDTEFJTUVSXG4gKlxuICogRG8gbm90IGVkaXQgb3IgYWRkIHRvIHRoaXMgZmlsZSBpZiB5b3Ugd2lzaCB0byB1cGdyYWRlIFByZXN0YVNob3AgdG8gbmV3ZXJcbiAqIHZlcnNpb25zIGluIHRoZSBmdXR1cmUuIElmIHlvdSB3aXNoIHRvIGN1c3RvbWl6ZSBQcmVzdGFTaG9wIGZvciB5b3VyXG4gKiBuZWVkcyBwbGVhc2UgcmVmZXIgdG8gaHR0cHM6Ly9kZXZkb2NzLnByZXN0YXNob3AuY29tLyBmb3IgbW9yZSBpbmZvcm1hdGlvbi5cbiAqXG4gKiBAYXV0aG9yICAgIFByZXN0YVNob3AgU0EgYW5kIENvbnRyaWJ1dG9ycyA8Y29udGFjdEBwcmVzdGFzaG9wLmNvbT5cbiAqIEBjb3B5cmlnaHQgU2luY2UgMjAwNyBQcmVzdGFTaG9wIFNBIGFuZCBDb250cmlidXRvcnNcbiAqIEBsaWNlbnNlICAgaHR0cHM6Ly9vcGVuc291cmNlLm9yZy9saWNlbnNlcy9PU0wtMy4wIE9wZW4gU29mdHdhcmUgTGljZW5zZSAoT1NMIDMuMClcbiAqL1xuXG5pbXBvcnQgUm91dGVyIGZyb20gJ0Bjb21wb25lbnRzL3JvdXRlcic7XG5pbXBvcnQgT3JkZXJWaWV3UGFnZU1hcCBmcm9tICdAcGFnZXMvb3JkZXIvT3JkZXJWaWV3UGFnZU1hcCc7XG5cbmNvbnN0IHskfSA9IHdpbmRvdztcblxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgT3JkZXJQYXltZW50c1JlZnJlc2hlciB7XG4gIGNvbnN0cnVjdG9yKCkge1xuICAgIHRoaXMucm91dGVyID0gbmV3IFJvdXRlcigpO1xuICB9XG5cbiAgcmVmcmVzaChvcmRlcklkKSB7XG4gICAgJC5hamF4KHRoaXMucm91dGVyLmdlbmVyYXRlKCdhZG1pbl9vcmRlcnNfZ2V0X3BheW1lbnRzJywge29yZGVySWR9KSlcbiAgICAgIC50aGVuKFxuICAgICAgICAocmVzcG9uc2UpID0+IHtcbiAgICAgICAgICAkKE9yZGVyVmlld1BhZ2VNYXAudmlld09yZGVyUGF5bWVudHNBbGVydCkucmVtb3ZlKCk7XG4gICAgICAgICAgJChgJHtPcmRlclZpZXdQYWdlTWFwLnZpZXdPcmRlclBheW1lbnRzQmxvY2t9IC5jYXJkLWJvZHlgKS5wcmVwZW5kKHJlc3BvbnNlKTtcbiAgICAgICAgfSxcbiAgICAgICAgKHJlc3BvbnNlKSA9PiB7XG4gICAgICAgICAgaWYgKHJlc3BvbnNlLnJlc3BvbnNlSlNPTiAmJiByZXNwb25zZS5yZXNwb25zZUpTT04ubWVzc2FnZSkge1xuICAgICAgICAgICAgJC5ncm93bC5lcnJvcih7bWVzc2FnZTogcmVzcG9uc2UucmVzcG9uc2VKU09OLm1lc3NhZ2V9KTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICApO1xuICB9XG59XG4iLCIvKipcbiAqIENvcHlyaWdodCBzaW5jZSAyMDA3IFByZXN0YVNob3AgU0EgYW5kIENvbnRyaWJ1dG9yc1xuICogUHJlc3RhU2hvcCBpcyBhbiBJbnRlcm5hdGlvbmFsIFJlZ2lzdGVyZWQgVHJhZGVtYXJrICYgUHJvcGVydHkgb2YgUHJlc3RhU2hvcCBTQVxuICpcbiAqIE5PVElDRSBPRiBMSUNFTlNFXG4gKlxuICogVGhpcyBzb3VyY2UgZmlsZSBpcyBzdWJqZWN0IHRvIHRoZSBPcGVuIFNvZnR3YXJlIExpY2Vuc2UgKE9TTCAzLjApXG4gKiB0aGF0IGlzIGJ1bmRsZWQgd2l0aCB0aGlzIHBhY2thZ2UgaW4gdGhlIGZpbGUgTElDRU5TRS5tZC5cbiAqIEl0IGlzIGFsc28gYXZhaWxhYmxlIHRocm91Z2ggdGhlIHdvcmxkLXdpZGUtd2ViIGF0IHRoaXMgVVJMOlxuICogaHR0cHM6Ly9vcGVuc291cmNlLm9yZy9saWNlbnNlcy9PU0wtMy4wXG4gKiBJZiB5b3UgZGlkIG5vdCByZWNlaXZlIGEgY29weSBvZiB0aGUgbGljZW5zZSBhbmQgYXJlIHVuYWJsZSB0b1xuICogb2J0YWluIGl0IHRocm91Z2ggdGhlIHdvcmxkLXdpZGUtd2ViLCBwbGVhc2Ugc2VuZCBhbiBlbWFpbFxuICogdG8gbGljZW5zZUBwcmVzdGFzaG9wLmNvbSBzbyB3ZSBjYW4gc2VuZCB5b3UgYSBjb3B5IGltbWVkaWF0ZWx5LlxuICpcbiAqIERJU0NMQUlNRVJcbiAqXG4gKiBEbyBub3QgZWRpdCBvciBhZGQgdG8gdGhpcyBmaWxlIGlmIHlvdSB3aXNoIHRvIHVwZ3JhZGUgUHJlc3RhU2hvcCB0byBuZXdlclxuICogdmVyc2lvbnMgaW4gdGhlIGZ1dHVyZS4gSWYgeW91IHdpc2ggdG8gY3VzdG9taXplIFByZXN0YVNob3AgZm9yIHlvdXJcbiAqIG5lZWRzIHBsZWFzZSByZWZlciB0byBodHRwczovL2RldmRvY3MucHJlc3Rhc2hvcC5jb20vIGZvciBtb3JlIGluZm9ybWF0aW9uLlxuICpcbiAqIEBhdXRob3IgICAgUHJlc3RhU2hvcCBTQSBhbmQgQ29udHJpYnV0b3JzIDxjb250YWN0QHByZXN0YXNob3AuY29tPlxuICogQGNvcHlyaWdodCBTaW5jZSAyMDA3IFByZXN0YVNob3AgU0EgYW5kIENvbnRyaWJ1dG9yc1xuICogQGxpY2Vuc2UgICBodHRwczovL29wZW5zb3VyY2Uub3JnL2xpY2Vuc2VzL09TTC0zLjAgT3BlbiBTb2Z0d2FyZSBMaWNlbnNlIChPU0wgMy4wKVxuICovXG5cbmltcG9ydCBSb3V0ZXIgZnJvbSAnQGNvbXBvbmVudHMvcm91dGVyJztcbmltcG9ydCBPcmRlclZpZXdQYWdlTWFwIGZyb20gJ0BwYWdlcy9vcmRlci9PcmRlclZpZXdQYWdlTWFwJztcblxuY29uc3QgeyR9ID0gd2luZG93O1xuXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBPcmRlclByaWNlc1JlZnJlc2hlciB7XG4gIGNvbnN0cnVjdG9yKCkge1xuICAgIHRoaXMucm91dGVyID0gbmV3IFJvdXRlcigpO1xuICB9XG5cbiAgcmVmcmVzaChvcmRlcklkKSB7XG4gICAgJC5nZXRKU09OKHRoaXMucm91dGVyLmdlbmVyYXRlKCdhZG1pbl9vcmRlcnNfZ2V0X3ByaWNlcycsIHtvcmRlcklkfSkpLnRoZW4oKHJlc3BvbnNlKSA9PiB7XG4gICAgICAkKE9yZGVyVmlld1BhZ2VNYXAub3JkZXJUb3RhbCkudGV4dChyZXNwb25zZS5vcmRlclRvdGFsRm9ybWF0dGVkKTtcbiAgICAgICQoT3JkZXJWaWV3UGFnZU1hcC5vcmRlckRpc2NvdW50c1RvdGFsKS50ZXh0KGAtJHtyZXNwb25zZS5kaXNjb3VudHNBbW91bnRGb3JtYXR0ZWR9YCk7XG4gICAgICAkKE9yZGVyVmlld1BhZ2VNYXAub3JkZXJEaXNjb3VudHNUb3RhbENvbnRhaW5lcikudG9nZ2xlQ2xhc3MoJ2Qtbm9uZScsICFyZXNwb25zZS5kaXNjb3VudHNBbW91bnREaXNwbGF5ZWQpO1xuICAgICAgJChPcmRlclZpZXdQYWdlTWFwLm9yZGVyUHJvZHVjdHNUb3RhbCkudGV4dChyZXNwb25zZS5wcm9kdWN0c1RvdGFsRm9ybWF0dGVkKTtcbiAgICAgICQoT3JkZXJWaWV3UGFnZU1hcC5vcmRlclNoaXBwaW5nVG90YWwpLnRleHQocmVzcG9uc2Uuc2hpcHBpbmdUb3RhbEZvcm1hdHRlZCk7XG4gICAgICAkKE9yZGVyVmlld1BhZ2VNYXAub3JkZXJTaGlwcGluZ1RvdGFsQ29udGFpbmVyKS50b2dnbGVDbGFzcygnZC1ub25lJywgIXJlc3BvbnNlLnNoaXBwaW5nVG90YWxEaXNwbGF5ZWQpO1xuICAgICAgJChPcmRlclZpZXdQYWdlTWFwLm9yZGVyVGF4ZXNUb3RhbCkudGV4dChyZXNwb25zZS50YXhlc1RvdGFsRm9ybWF0dGVkKTtcbiAgICB9KTtcbiAgfVxuXG4gIHJlZnJlc2hQcm9kdWN0UHJpY2VzKG9yZGVySWQpIHtcbiAgICAkLmdldEpTT04odGhpcy5yb3V0ZXIuZ2VuZXJhdGUoJ2FkbWluX29yZGVyc19wcm9kdWN0X3ByaWNlcycsIHtvcmRlcklkfSkpLnRoZW4oKHByb2R1Y3RQcmljZXNMaXN0KSA9PiB7XG4gICAgICBwcm9kdWN0UHJpY2VzTGlzdC5mb3JFYWNoKChwcm9kdWN0UHJpY2VzKSA9PiB7XG4gICAgICAgIGNvbnN0IG9yZGVyUHJvZHVjdFRySWQgPSBPcmRlclZpZXdQYWdlTWFwLnByb2R1Y3RzVGFibGVSb3cocHJvZHVjdFByaWNlcy5vcmRlckRldGFpbElkKTtcbiAgICAgICAgbGV0ICRxdWFudGl0eSA9ICQocHJvZHVjdFByaWNlcy5xdWFudGl0eSk7XG5cbiAgICAgICAgaWYgKHByb2R1Y3RQcmljZXMucXVhbnRpdHkgPiAxKSB7XG4gICAgICAgICAgJHF1YW50aXR5ID0gJHF1YW50aXR5LndyYXAoJzxzcGFuIGNsYXNzPVwiYmFkZ2UgYmFkZ2Utc2Vjb25kYXJ5IHJvdW5kZWQtY2lyY2xlXCI+PC9zcGFuPicpO1xuICAgICAgICB9XG5cbiAgICAgICAgJChgJHtvcmRlclByb2R1Y3RUcklkfSAke09yZGVyVmlld1BhZ2VNYXAucHJvZHVjdEVkaXRVbml0UHJpY2V9YCkudGV4dChwcm9kdWN0UHJpY2VzLnVuaXRQcmljZSk7XG4gICAgICAgICQoYCR7b3JkZXJQcm9kdWN0VHJJZH0gJHtPcmRlclZpZXdQYWdlTWFwLnByb2R1Y3RFZGl0UXVhbnRpdHl9YCkuaHRtbCgkcXVhbnRpdHkuaHRtbCgpKTtcbiAgICAgICAgJChgJHtvcmRlclByb2R1Y3RUcklkfSAke09yZGVyVmlld1BhZ2VNYXAucHJvZHVjdEVkaXRBdmFpbGFibGVRdWFudGl0eX1gKS50ZXh0KHByb2R1Y3RQcmljZXMuYXZhaWxhYmxlUXVhbnRpdHkpO1xuICAgICAgICAkKGAke29yZGVyUHJvZHVjdFRySWR9ICR7T3JkZXJWaWV3UGFnZU1hcC5wcm9kdWN0RWRpdFRvdGFsUHJpY2V9YCkudGV4dChwcm9kdWN0UHJpY2VzLnRvdGFsUHJpY2UpO1xuXG4gICAgICAgIC8vIHVwZGF0ZSBvcmRlciByb3cgcHJpY2UgdmFsdWVzXG4gICAgICAgIGNvbnN0IHByb2R1Y3RFZGl0QnV0dG9uID0gJChPcmRlclZpZXdQYWdlTWFwLnByb2R1Y3RFZGl0QnRuKHByb2R1Y3RQcmljZXMub3JkZXJEZXRhaWxJZCkpO1xuXG4gICAgICAgIHByb2R1Y3RFZGl0QnV0dG9uLmRhdGEoJ3Byb2R1Y3QtcHJpY2UtdGF4LWluY2wnLCBwcm9kdWN0UHJpY2VzLnVuaXRQcmljZVRheEluY2xSYXcpO1xuICAgICAgICBwcm9kdWN0RWRpdEJ1dHRvbi5kYXRhKCdwcm9kdWN0LXByaWNlLXRheC1leGNsJywgcHJvZHVjdFByaWNlcy51bml0UHJpY2VUYXhFeGNsUmF3KTtcbiAgICAgICAgcHJvZHVjdEVkaXRCdXR0b24uZGF0YSgncHJvZHVjdC1xdWFudGl0eScsIHByb2R1Y3RQcmljZXMucXVhbnRpdHkpO1xuICAgICAgfSk7XG4gICAgfSk7XG4gIH1cblxuICAvKipcbiAgICogVGhpcyBtZXRob2Qgd2lsbCBjaGVjayBpZiB0aGUgc2FtZSBwcm9kdWN0IGlzIGFscmVhZHkgcHJlc2VudCBpbiB0aGUgb3JkZXJcbiAgICogYW5kIGlmIHNvIGFuZCBpZiB0aGUgcHJpY2Ugb2YgdGhlIDIgcHJvZHVjdHMgZG9lc24ndCBtYXRjaCB3aWxsIHJldHVybiBlaXRoZXJcbiAgICogJ2ludm9pY2UnIGlmIHRoZSAyIHByb2R1Y3RzIGFyZSBpbiAyIGRpZmZlcmVudCBpbnZvaWNlcyBvciAncHJvZHVjdCcgaWYgdGhlIDIgcHJvZHVjdHNcbiAgICogYXJlIGluIHRoZSBzYW1lIGludm9pY2UgKG9yIG5vIGludm9pY2UgeWV0KS4gT25seSBwcm9kdWN0cyB0aGF0IGhhdmUgZGlmZmVyZW50IGN1c3RvbWl6YXRpb25zXG4gICAqIGNhbiBiZSB0d2ljZSBpbiBhIHNhbWUgaW52b2ljZS5cbiAgICogV2lsbCByZXR1cm4gbnVsbCBpZiBubyBtYXRjaGluZyBwcm9kdWN0cyBhcmUgZm91bmQuXG4gICAqL1xuICBjaGVja090aGVyUHJvZHVjdFByaWNlc01hdGNoKGdpdmVuUHJpY2UsIHByb2R1Y3RJZCwgY29tYmluYXRpb25JZCwgaW52b2ljZUlkLCBvcmRlckRldGFpbElkKSB7XG4gICAgY29uc3QgcHJvZHVjdFJvd3MgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKCd0ci5jZWxsUHJvZHVjdCcpO1xuICAgIC8vIFdlIGNvbnZlcnQgdGhlIGV4cGVjdGVkIHZhbHVlcyBpbnRvIGludC9mbG9hdCB0byBhdm9pZCBhIHR5cGUgbWlzbWF0Y2ggdGhhdCB3b3VsZCBiZSB3cm9uZ2x5IGludGVycHJldGVkXG4gICAgY29uc3QgZXhwZWN0ZWRQcm9kdWN0SWQgPSBOdW1iZXIocHJvZHVjdElkKTtcbiAgICBjb25zdCBleHBlY3RlZENvbWJpbmF0aW9uSWQgPSBOdW1iZXIoY29tYmluYXRpb25JZCk7XG4gICAgY29uc3QgZXhwZWN0ZWRHaXZlblByaWNlID0gTnVtYmVyKGdpdmVuUHJpY2UpO1xuICAgIGxldCB1bm1hdGNoaW5nSW52b2ljZVByaWNlRXhpc3RzID0gZmFsc2U7XG4gICAgbGV0IHVubWF0Y2hpbmdQcm9kdWN0UHJpY2VFeGlzdHMgPSBmYWxzZTtcblxuICAgIHByb2R1Y3RSb3dzLmZvckVhY2goKHByb2R1Y3RSb3cpID0+IHtcbiAgICAgIGNvbnN0IHByb2R1Y3RSb3dJZCA9ICQocHJvZHVjdFJvdykuYXR0cignaWQnKTtcblxuICAgICAgLy8gTm8gbmVlZCB0byBjaGVjayBlZGl0ZWQgcm93IChlc3BlY2lhbGx5IGlmIGl0J3MgdGhlIG9ubHkgb25lIGZvciB0aGlzIHByb2R1Y3QpXG4gICAgICBpZiAob3JkZXJEZXRhaWxJZCAmJiBwcm9kdWN0Um93SWQgPT09IGBvcmRlclByb2R1Y3RfJHtvcmRlckRldGFpbElkfWApIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICBjb25zdCBwcm9kdWN0RWRpdEJ0biA9ICQoYCMke3Byb2R1Y3RSb3dJZH0gJHtPcmRlclZpZXdQYWdlTWFwLnByb2R1Y3RFZGl0QnV0dG9uc31gKTtcbiAgICAgIGNvbnN0IGN1cnJlbnRPcmRlckludm9pY2VJZCA9IE51bWJlcihwcm9kdWN0RWRpdEJ0bi5kYXRhKCdvcmRlci1pbnZvaWNlLWlkJykpO1xuXG4gICAgICBjb25zdCBjdXJyZW50UHJvZHVjdElkID0gTnVtYmVyKHByb2R1Y3RFZGl0QnRuLmRhdGEoJ3Byb2R1Y3QtaWQnKSk7XG4gICAgICBjb25zdCBjdXJyZW50Q29tYmluYXRpb25JZCA9IE51bWJlcihwcm9kdWN0RWRpdEJ0bi5kYXRhKCdjb21iaW5hdGlvbi1pZCcpKTtcblxuICAgICAgaWYgKGN1cnJlbnRQcm9kdWN0SWQgIT09IGV4cGVjdGVkUHJvZHVjdElkIHx8IGN1cnJlbnRDb21iaW5hdGlvbklkICE9PSBleHBlY3RlZENvbWJpbmF0aW9uSWQpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICBpZiAoZXhwZWN0ZWRHaXZlblByaWNlICE9PSBOdW1iZXIocHJvZHVjdEVkaXRCdG4uZGF0YSgncHJvZHVjdC1wcmljZS10YXgtaW5jbCcpKSkge1xuICAgICAgICBpZiAoaW52b2ljZUlkID09PSAnJyB8fCAoaW52b2ljZUlkICYmIGN1cnJlbnRPcmRlckludm9pY2VJZCAmJiBpbnZvaWNlSWQgPT09IGN1cnJlbnRPcmRlckludm9pY2VJZCkpIHtcbiAgICAgICAgICB1bm1hdGNoaW5nUHJvZHVjdFByaWNlRXhpc3RzID0gdHJ1ZTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICB1bm1hdGNoaW5nSW52b2ljZVByaWNlRXhpc3RzID0gdHJ1ZTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH0pO1xuXG4gICAgaWYgKHVubWF0Y2hpbmdJbnZvaWNlUHJpY2VFeGlzdHMpIHtcbiAgICAgIHJldHVybiAnaW52b2ljZSc7XG4gICAgfVxuICAgIGlmICh1bm1hdGNoaW5nUHJvZHVjdFByaWNlRXhpc3RzKSB7XG4gICAgICByZXR1cm4gJ3Byb2R1Y3QnO1xuICAgIH1cblxuICAgIHJldHVybiBudWxsO1xuICB9XG59XG4iLCIvKipcbiAqIENvcHlyaWdodCBzaW5jZSAyMDA3IFByZXN0YVNob3AgU0EgYW5kIENvbnRyaWJ1dG9yc1xuICogUHJlc3RhU2hvcCBpcyBhbiBJbnRlcm5hdGlvbmFsIFJlZ2lzdGVyZWQgVHJhZGVtYXJrICYgUHJvcGVydHkgb2YgUHJlc3RhU2hvcCBTQVxuICpcbiAqIE5PVElDRSBPRiBMSUNFTlNFXG4gKlxuICogVGhpcyBzb3VyY2UgZmlsZSBpcyBzdWJqZWN0IHRvIHRoZSBPcGVuIFNvZnR3YXJlIExpY2Vuc2UgKE9TTCAzLjApXG4gKiB0aGF0IGlzIGJ1bmRsZWQgd2l0aCB0aGlzIHBhY2thZ2UgaW4gdGhlIGZpbGUgTElDRU5TRS5tZC5cbiAqIEl0IGlzIGFsc28gYXZhaWxhYmxlIHRocm91Z2ggdGhlIHdvcmxkLXdpZGUtd2ViIGF0IHRoaXMgVVJMOlxuICogaHR0cHM6Ly9vcGVuc291cmNlLm9yZy9saWNlbnNlcy9PU0wtMy4wXG4gKiBJZiB5b3UgZGlkIG5vdCByZWNlaXZlIGEgY29weSBvZiB0aGUgbGljZW5zZSBhbmQgYXJlIHVuYWJsZSB0b1xuICogb2J0YWluIGl0IHRocm91Z2ggdGhlIHdvcmxkLXdpZGUtd2ViLCBwbGVhc2Ugc2VuZCBhbiBlbWFpbFxuICogdG8gbGljZW5zZUBwcmVzdGFzaG9wLmNvbSBzbyB3ZSBjYW4gc2VuZCB5b3UgYSBjb3B5IGltbWVkaWF0ZWx5LlxuICpcbiAqIERJU0NMQUlNRVJcbiAqXG4gKiBEbyBub3QgZWRpdCBvciBhZGQgdG8gdGhpcyBmaWxlIGlmIHlvdSB3aXNoIHRvIHVwZ3JhZGUgUHJlc3RhU2hvcCB0byBuZXdlclxuICogdmVyc2lvbnMgaW4gdGhlIGZ1dHVyZS4gSWYgeW91IHdpc2ggdG8gY3VzdG9taXplIFByZXN0YVNob3AgZm9yIHlvdXJcbiAqIG5lZWRzIHBsZWFzZSByZWZlciB0byBodHRwczovL2RldmRvY3MucHJlc3Rhc2hvcC5jb20vIGZvciBtb3JlIGluZm9ybWF0aW9uLlxuICpcbiAqIEBhdXRob3IgICAgUHJlc3RhU2hvcCBTQSBhbmQgQ29udHJpYnV0b3JzIDxjb250YWN0QHByZXN0YXNob3AuY29tPlxuICogQGNvcHlyaWdodCBTaW5jZSAyMDA3IFByZXN0YVNob3AgU0EgYW5kIENvbnRyaWJ1dG9yc1xuICogQGxpY2Vuc2UgICBodHRwczovL29wZW5zb3VyY2Uub3JnL2xpY2Vuc2VzL09TTC0zLjAgT3BlbiBTb2Z0d2FyZSBMaWNlbnNlIChPU0wgMy4wKVxuICovXG5cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIE9yZGVyUHJpY2VzIHtcbiAgY2FsY3VsYXRlVGF4RXhjbHVkZWQodGF4SW5jbHVkZWQsIHRheFJhdGVQZXJDZW50LCBjdXJyZW5jeVByZWNpc2lvbikge1xuICAgIGxldCBwcmljZVRheEluY2wgPSBwYXJzZUZsb2F0KHRheEluY2x1ZGVkKTtcblxuICAgIGlmIChwcmljZVRheEluY2wgPCAwIHx8IE51bWJlci5pc05hTihwcmljZVRheEluY2wpKSB7XG4gICAgICBwcmljZVRheEluY2wgPSAwO1xuICAgIH1cbiAgICBjb25zdCB0YXhSYXRlID0gdGF4UmF0ZVBlckNlbnQgLyAxMDAgKyAxO1xuXG4gICAgcmV0dXJuIHdpbmRvdy5wc19yb3VuZChwcmljZVRheEluY2wgLyB0YXhSYXRlLCBjdXJyZW5jeVByZWNpc2lvbik7XG4gIH1cblxuICBjYWxjdWxhdGVUYXhJbmNsdWRlZCh0YXhFeGNsdWRlZCwgdGF4UmF0ZVBlckNlbnQsIGN1cnJlbmN5UHJlY2lzaW9uKSB7XG4gICAgbGV0IHByaWNlVGF4RXhjbCA9IHBhcnNlRmxvYXQodGF4RXhjbHVkZWQpO1xuXG4gICAgaWYgKHByaWNlVGF4RXhjbCA8IDAgfHwgTnVtYmVyLmlzTmFOKHByaWNlVGF4RXhjbCkpIHtcbiAgICAgIHByaWNlVGF4RXhjbCA9IDA7XG4gICAgfVxuICAgIGNvbnN0IHRheFJhdGUgPSB0YXhSYXRlUGVyQ2VudCAvIDEwMCArIDE7XG5cbiAgICByZXR1cm4gd2luZG93LnBzX3JvdW5kKHByaWNlVGF4RXhjbCAqIHRheFJhdGUsIGN1cnJlbmN5UHJlY2lzaW9uKTtcbiAgfVxuXG4gIGNhbGN1bGF0ZVRvdGFsUHJpY2UocXVhbnRpdHksIHVuaXRQcmljZSwgY3VycmVuY3lQcmVjaXNpb24pIHtcbiAgICByZXR1cm4gd2luZG93LnBzX3JvdW5kKHVuaXRQcmljZSAqIHF1YW50aXR5LCBjdXJyZW5jeVByZWNpc2lvbik7XG4gIH1cbn1cbiIsIi8qKlxuICogQ29weXJpZ2h0IHNpbmNlIDIwMDcgUHJlc3RhU2hvcCBTQSBhbmQgQ29udHJpYnV0b3JzXG4gKiBQcmVzdGFTaG9wIGlzIGFuIEludGVybmF0aW9uYWwgUmVnaXN0ZXJlZCBUcmFkZW1hcmsgJiBQcm9wZXJ0eSBvZiBQcmVzdGFTaG9wIFNBXG4gKlxuICogTk9USUNFIE9GIExJQ0VOU0VcbiAqXG4gKiBUaGlzIHNvdXJjZSBmaWxlIGlzIHN1YmplY3QgdG8gdGhlIE9wZW4gU29mdHdhcmUgTGljZW5zZSAoT1NMIDMuMClcbiAqIHRoYXQgaXMgYnVuZGxlZCB3aXRoIHRoaXMgcGFja2FnZSBpbiB0aGUgZmlsZSBMSUNFTlNFLm1kLlxuICogSXQgaXMgYWxzbyBhdmFpbGFibGUgdGhyb3VnaCB0aGUgd29ybGQtd2lkZS13ZWIgYXQgdGhpcyBVUkw6XG4gKiBodHRwczovL29wZW5zb3VyY2Uub3JnL2xpY2Vuc2VzL09TTC0zLjBcbiAqIElmIHlvdSBkaWQgbm90IHJlY2VpdmUgYSBjb3B5IG9mIHRoZSBsaWNlbnNlIGFuZCBhcmUgdW5hYmxlIHRvXG4gKiBvYnRhaW4gaXQgdGhyb3VnaCB0aGUgd29ybGQtd2lkZS13ZWIsIHBsZWFzZSBzZW5kIGFuIGVtYWlsXG4gKiB0byBsaWNlbnNlQHByZXN0YXNob3AuY29tIHNvIHdlIGNhbiBzZW5kIHlvdSBhIGNvcHkgaW1tZWRpYXRlbHkuXG4gKlxuICogRElTQ0xBSU1FUlxuICpcbiAqIERvIG5vdCBlZGl0IG9yIGFkZCB0byB0aGlzIGZpbGUgaWYgeW91IHdpc2ggdG8gdXBncmFkZSBQcmVzdGFTaG9wIHRvIG5ld2VyXG4gKiB2ZXJzaW9ucyBpbiB0aGUgZnV0dXJlLiBJZiB5b3Ugd2lzaCB0byBjdXN0b21pemUgUHJlc3RhU2hvcCBmb3IgeW91clxuICogbmVlZHMgcGxlYXNlIHJlZmVyIHRvIGh0dHBzOi8vZGV2ZG9jcy5wcmVzdGFzaG9wLmNvbS8gZm9yIG1vcmUgaW5mb3JtYXRpb24uXG4gKlxuICogQGF1dGhvciAgICBQcmVzdGFTaG9wIFNBIGFuZCBDb250cmlidXRvcnMgPGNvbnRhY3RAcHJlc3Rhc2hvcC5jb20+XG4gKiBAY29weXJpZ2h0IFNpbmNlIDIwMDcgUHJlc3RhU2hvcCBTQSBhbmQgQ29udHJpYnV0b3JzXG4gKiBAbGljZW5zZSAgIGh0dHBzOi8vb3BlbnNvdXJjZS5vcmcvbGljZW5zZXMvT1NMLTMuMCBPcGVuIFNvZnR3YXJlIExpY2Vuc2UgKE9TTCAzLjApXG4gKi9cbmltcG9ydCBSb3V0ZXIgZnJvbSAnQGNvbXBvbmVudHMvcm91dGVyJztcbmltcG9ydCBPcmRlclZpZXdQYWdlTWFwIGZyb20gJ0BwYWdlcy9vcmRlci9PcmRlclZpZXdQYWdlTWFwJztcblxuY29uc3QgeyR9ID0gd2luZG93O1xuXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBPcmRlclByb2R1Y3RBdXRvY29tcGxldGUge1xuICBjb25zdHJ1Y3RvcihpbnB1dCkge1xuICAgIHRoaXMuYWN0aXZlU2VhcmNoUmVxdWVzdCA9IG51bGw7XG4gICAgdGhpcy5yb3V0ZXIgPSBuZXcgUm91dGVyKCk7XG4gICAgdGhpcy5pbnB1dCA9IGlucHV0O1xuICAgIHRoaXMucmVzdWx0cyA9IFtdO1xuICAgIHRoaXMuZHJvcGRvd25NZW51ID0gJChPcmRlclZpZXdQYWdlTWFwLnByb2R1Y3RTZWFyY2hJbnB1dEF1dG9jb21wbGV0ZU1lbnUpO1xuICAgIC8qKlxuICAgICAqIFBlcm1pdCB0byBsaW5rIHRvIGVhY2ggdmFsdWUgb2YgZHJvcGRvd24gYSBjYWxsYmFjayBhZnRlciBpdGVtIGlzIGNsaWNrZWRcbiAgICAgKi9cbiAgICB0aGlzLm9uSXRlbUNsaWNrZWRDYWxsYmFjayA9ICgpID0+IHt9O1xuICB9XG5cbiAgbGlzdGVuRm9yU2VhcmNoKCkge1xuICAgIHRoaXMuaW5wdXQub24oJ2NsaWNrJywgKGV2ZW50KSA9PiB7XG4gICAgICBldmVudC5zdG9wSW1tZWRpYXRlUHJvcGFnYXRpb24oKTtcbiAgICAgIHRoaXMudXBkYXRlUmVzdWx0cyh0aGlzLnJlc3VsdHMpO1xuICAgIH0pO1xuXG4gICAgdGhpcy5pbnB1dC5vbigna2V5dXAnLCAoZXZlbnQpID0+IHRoaXMuZGVsYXlTZWFyY2goZXZlbnQuY3VycmVudFRhcmdldCkpO1xuICAgICQoZG9jdW1lbnQpLm9uKCdjbGljaycsICgpID0+IHRoaXMuZHJvcGRvd25NZW51LmhpZGUoKSk7XG4gIH1cblxuICBkZWxheVNlYXJjaChpbnB1dCkge1xuICAgIGNsZWFyVGltZW91dCh0aGlzLnNlYXJjaFRpbWVvdXRJZCk7XG5cbiAgICAvLyBTZWFyY2ggb25seSBpZiB0aGUgc2VhcmNoIHBocmFzZSBsZW5ndGggaXMgZ3JlYXRlciB0aGFuIDIgY2hhcmFjdGVyc1xuICAgIGlmIChpbnB1dC52YWx1ZS5sZW5ndGggPCAyKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgdGhpcy5zZWFyY2hUaW1lb3V0SWQgPSBzZXRUaW1lb3V0KCgpID0+IHtcbiAgICAgIHRoaXMuc2VhcmNoKGlucHV0LnZhbHVlLCAkKGlucHV0KS5kYXRhKCdjdXJyZW5jeScpLCAkKGlucHV0KS5kYXRhKCdvcmRlcicpKTtcbiAgICB9LCAzMDApO1xuICB9XG5cbiAgc2VhcmNoKHNlYXJjaCwgY3VycmVuY3ksIG9yZGVySWQpIHtcbiAgICBjb25zdCBwYXJhbXMgPSB7c2VhcmNoX3BocmFzZTogc2VhcmNofTtcblxuICAgIGlmIChjdXJyZW5jeSkge1xuICAgICAgcGFyYW1zLmN1cnJlbmN5X2lkID0gY3VycmVuY3k7XG4gICAgfVxuXG4gICAgaWYgKG9yZGVySWQpIHtcbiAgICAgIHBhcmFtcy5vcmRlcl9pZCA9IG9yZGVySWQ7XG4gICAgfVxuXG4gICAgaWYgKHRoaXMuYWN0aXZlU2VhcmNoUmVxdWVzdCAhPT0gbnVsbCkge1xuICAgICAgdGhpcy5hY3RpdmVTZWFyY2hSZXF1ZXN0LmFib3J0KCk7XG4gICAgfVxuXG4gICAgdGhpcy5hY3RpdmVTZWFyY2hSZXF1ZXN0ID0gJC5nZXQodGhpcy5yb3V0ZXIuZ2VuZXJhdGUoJ2FkbWluX29yZGVyc19wcm9kdWN0c19zZWFyY2gnLCBwYXJhbXMpKTtcbiAgICB0aGlzLmFjdGl2ZVNlYXJjaFJlcXVlc3RcbiAgICAgIC50aGVuKChyZXNwb25zZSkgPT4gdGhpcy51cGRhdGVSZXN1bHRzKHJlc3BvbnNlKSlcbiAgICAgIC5hbHdheXMoKCkgPT4ge1xuICAgICAgICB0aGlzLmFjdGl2ZVNlYXJjaFJlcXVlc3QgPSBudWxsO1xuICAgICAgfSk7XG4gIH1cblxuICB1cGRhdGVSZXN1bHRzKHJlc3VsdHMpIHtcbiAgICB0aGlzLmRyb3Bkb3duTWVudS5lbXB0eSgpO1xuXG4gICAgaWYgKCFyZXN1bHRzIHx8ICFyZXN1bHRzLnByb2R1Y3RzIHx8IE9iamVjdC5rZXlzKHJlc3VsdHMucHJvZHVjdHMpLmxlbmd0aCA8PSAwKSB7XG4gICAgICB0aGlzLmRyb3Bkb3duTWVudS5oaWRlKCk7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgdGhpcy5yZXN1bHRzID0gcmVzdWx0cy5wcm9kdWN0cztcblxuICAgIE9iamVjdC52YWx1ZXModGhpcy5yZXN1bHRzKS5mb3JFYWNoKCh2YWwpID0+IHtcbiAgICAgIGNvbnN0IGxpbmsgPSAkKGA8YSBjbGFzcz1cImRyb3Bkb3duLWl0ZW1cIiBkYXRhLWlkPVwiJHt2YWwucHJvZHVjdElkfVwiIGhyZWY9XCIjXCI+JHt2YWwubmFtZX08L2E+YCk7XG5cbiAgICAgIGxpbmsub24oJ2NsaWNrJywgKGV2ZW50KSA9PiB7XG4gICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgIHRoaXMub25JdGVtQ2xpY2tlZCgkKGV2ZW50LnRhcmdldCkuZGF0YSgnaWQnKSk7XG4gICAgICB9KTtcblxuICAgICAgdGhpcy5kcm9wZG93bk1lbnUuYXBwZW5kKGxpbmspO1xuICAgIH0pO1xuXG4gICAgdGhpcy5kcm9wZG93bk1lbnUuc2hvdygpO1xuICB9XG5cbiAgb25JdGVtQ2xpY2tlZChpZCkge1xuICAgIGNvbnN0IHNlbGVjdGVkUHJvZHVjdCA9IHRoaXMucmVzdWx0cy5maWx0ZXIoKHByb2R1Y3QpID0+IHByb2R1Y3QucHJvZHVjdElkID09PSBpZCk7XG5cbiAgICBpZiAoc2VsZWN0ZWRQcm9kdWN0Lmxlbmd0aCAhPT0gMCkge1xuICAgICAgdGhpcy5pbnB1dC52YWwoc2VsZWN0ZWRQcm9kdWN0WzBdLm5hbWUpO1xuICAgICAgdGhpcy5vbkl0ZW1DbGlja2VkQ2FsbGJhY2soc2VsZWN0ZWRQcm9kdWN0WzBdKTtcbiAgICB9XG4gIH1cbn1cbiIsIi8qKlxuICogQ29weXJpZ2h0IHNpbmNlIDIwMDcgUHJlc3RhU2hvcCBTQSBhbmQgQ29udHJpYnV0b3JzXG4gKiBQcmVzdGFTaG9wIGlzIGFuIEludGVybmF0aW9uYWwgUmVnaXN0ZXJlZCBUcmFkZW1hcmsgJiBQcm9wZXJ0eSBvZiBQcmVzdGFTaG9wIFNBXG4gKlxuICogTk9USUNFIE9GIExJQ0VOU0VcbiAqXG4gKiBUaGlzIHNvdXJjZSBmaWxlIGlzIHN1YmplY3QgdG8gdGhlIE9wZW4gU29mdHdhcmUgTGljZW5zZSAoT1NMIDMuMClcbiAqIHRoYXQgaXMgYnVuZGxlZCB3aXRoIHRoaXMgcGFja2FnZSBpbiB0aGUgZmlsZSBMSUNFTlNFLm1kLlxuICogSXQgaXMgYWxzbyBhdmFpbGFibGUgdGhyb3VnaCB0aGUgd29ybGQtd2lkZS13ZWIgYXQgdGhpcyBVUkw6XG4gKiBodHRwczovL29wZW5zb3VyY2Uub3JnL2xpY2Vuc2VzL09TTC0zLjBcbiAqIElmIHlvdSBkaWQgbm90IHJlY2VpdmUgYSBjb3B5IG9mIHRoZSBsaWNlbnNlIGFuZCBhcmUgdW5hYmxlIHRvXG4gKiBvYnRhaW4gaXQgdGhyb3VnaCB0aGUgd29ybGQtd2lkZS13ZWIsIHBsZWFzZSBzZW5kIGFuIGVtYWlsXG4gKiB0byBsaWNlbnNlQHByZXN0YXNob3AuY29tIHNvIHdlIGNhbiBzZW5kIHlvdSBhIGNvcHkgaW1tZWRpYXRlbHkuXG4gKlxuICogRElTQ0xBSU1FUlxuICpcbiAqIERvIG5vdCBlZGl0IG9yIGFkZCB0byB0aGlzIGZpbGUgaWYgeW91IHdpc2ggdG8gdXBncmFkZSBQcmVzdGFTaG9wIHRvIG5ld2VyXG4gKiB2ZXJzaW9ucyBpbiB0aGUgZnV0dXJlLiBJZiB5b3Ugd2lzaCB0byBjdXN0b21pemUgUHJlc3RhU2hvcCBmb3IgeW91clxuICogbmVlZHMgcGxlYXNlIHJlZmVyIHRvIGh0dHBzOi8vZGV2ZG9jcy5wcmVzdGFzaG9wLmNvbS8gZm9yIG1vcmUgaW5mb3JtYXRpb24uXG4gKlxuICogQGF1dGhvciAgICBQcmVzdGFTaG9wIFNBIGFuZCBDb250cmlidXRvcnMgPGNvbnRhY3RAcHJlc3Rhc2hvcC5jb20+XG4gKiBAY29weXJpZ2h0IFNpbmNlIDIwMDcgUHJlc3RhU2hvcCBTQSBhbmQgQ29udHJpYnV0b3JzXG4gKiBAbGljZW5zZSAgIGh0dHBzOi8vb3BlbnNvdXJjZS5vcmcvbGljZW5zZXMvT1NMLTMuMCBPcGVuIFNvZnR3YXJlIExpY2Vuc2UgKE9TTCAzLjApXG4gKi9cblxuaW1wb3J0IFJvdXRlciBmcm9tICdAY29tcG9uZW50cy9yb3V0ZXInO1xuaW1wb3J0IE9yZGVyVmlld1BhZ2VNYXAgZnJvbSAnQHBhZ2VzL29yZGVyL09yZGVyVmlld1BhZ2VNYXAnO1xuaW1wb3J0IHtFdmVudEVtaXR0ZXJ9IGZyb20gJ0Bjb21wb25lbnRzL2V2ZW50LWVtaXR0ZXInO1xuaW1wb3J0IE9yZGVyVmlld0V2ZW50TWFwIGZyb20gJ0BwYWdlcy9vcmRlci92aWV3L29yZGVyLXZpZXctZXZlbnQtbWFwJztcbmltcG9ydCBPcmRlclByaWNlcyBmcm9tICdAcGFnZXMvb3JkZXIvdmlldy9vcmRlci1wcmljZXMnO1xuaW1wb3J0IE9yZGVyUHJvZHVjdFJlbmRlcmVyIGZyb20gJ0BwYWdlcy9vcmRlci92aWV3L29yZGVyLXByb2R1Y3QtcmVuZGVyZXInO1xuaW1wb3J0IENvbmZpcm1Nb2RhbCBmcm9tICdAY29tcG9uZW50cy9tb2RhbCc7XG5pbXBvcnQgT3JkZXJQcmljZXNSZWZyZXNoZXIgZnJvbSAnQHBhZ2VzL29yZGVyL3ZpZXcvb3JkZXItcHJpY2VzLXJlZnJlc2hlcic7XG5cbmNvbnN0IHskfSA9IHdpbmRvdztcblxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgT3JkZXJQcm9kdWN0QWRkIHtcbiAgY29uc3RydWN0b3IoKSB7XG4gICAgdGhpcy5yb3V0ZXIgPSBuZXcgUm91dGVyKCk7XG4gICAgdGhpcy5wcm9kdWN0QWRkQWN0aW9uQnRuID0gJChPcmRlclZpZXdQYWdlTWFwLnByb2R1Y3RBZGRBY3Rpb25CdG4pO1xuICAgIHRoaXMucHJvZHVjdElkSW5wdXQgPSAkKE9yZGVyVmlld1BhZ2VNYXAucHJvZHVjdEFkZElkSW5wdXQpO1xuICAgIHRoaXMuY29tYmluYXRpb25zQmxvY2sgPSAkKE9yZGVyVmlld1BhZ2VNYXAucHJvZHVjdEFkZENvbWJpbmF0aW9uc0Jsb2NrKTtcbiAgICB0aGlzLmNvbWJpbmF0aW9uc1NlbGVjdCA9ICQoT3JkZXJWaWV3UGFnZU1hcC5wcm9kdWN0QWRkQ29tYmluYXRpb25zU2VsZWN0KTtcbiAgICB0aGlzLnByaWNlVGF4SW5jbHVkZWRJbnB1dCA9ICQoT3JkZXJWaWV3UGFnZU1hcC5wcm9kdWN0QWRkUHJpY2VUYXhJbmNsSW5wdXQpO1xuICAgIHRoaXMucHJpY2VUYXhFeGNsdWRlZElucHV0ID0gJChPcmRlclZpZXdQYWdlTWFwLnByb2R1Y3RBZGRQcmljZVRheEV4Y2xJbnB1dCk7XG4gICAgdGhpcy50YXhSYXRlSW5wdXQgPSAkKE9yZGVyVmlld1BhZ2VNYXAucHJvZHVjdEFkZFRheFJhdGVJbnB1dCk7XG4gICAgdGhpcy5xdWFudGl0eUlucHV0ID0gJChPcmRlclZpZXdQYWdlTWFwLnByb2R1Y3RBZGRRdWFudGl0eUlucHV0KTtcbiAgICB0aGlzLmF2YWlsYWJsZVRleHQgPSAkKE9yZGVyVmlld1BhZ2VNYXAucHJvZHVjdEFkZEF2YWlsYWJsZVRleHQpO1xuICAgIHRoaXMubG9jYXRpb25UZXh0ID0gJChPcmRlclZpZXdQYWdlTWFwLnByb2R1Y3RBZGRMb2NhdGlvblRleHQpO1xuICAgIHRoaXMudG90YWxQcmljZVRleHQgPSAkKE9yZGVyVmlld1BhZ2VNYXAucHJvZHVjdEFkZFRvdGFsUHJpY2VUZXh0KTtcbiAgICB0aGlzLmludm9pY2VTZWxlY3QgPSAkKE9yZGVyVmlld1BhZ2VNYXAucHJvZHVjdEFkZEludm9pY2VTZWxlY3QpO1xuICAgIHRoaXMuZnJlZVNoaXBwaW5nU2VsZWN0ID0gJChPcmRlclZpZXdQYWdlTWFwLnByb2R1Y3RBZGRGcmVlU2hpcHBpbmdTZWxlY3QpO1xuICAgIHRoaXMucHJvZHVjdEFkZE1lbnVCdG4gPSAkKE9yZGVyVmlld1BhZ2VNYXAucHJvZHVjdEFkZEJ0bik7XG4gICAgdGhpcy5hdmFpbGFibGUgPSBudWxsO1xuICAgIHRoaXMuc2V0dXBMaXN0ZW5lcigpO1xuICAgIHRoaXMucHJvZHVjdCA9IHt9O1xuICAgIHRoaXMuY3VycmVuY3lQcmVjaXNpb24gPSAkKE9yZGVyVmlld1BhZ2VNYXAucHJvZHVjdHNUYWJsZSkuZGF0YSgnY3VycmVuY3lQcmVjaXNpb24nKTtcbiAgICB0aGlzLnByaWNlVGF4Q2FsY3VsYXRvciA9IG5ldyBPcmRlclByaWNlcygpO1xuICAgIHRoaXMub3JkZXJQcm9kdWN0UmVuZGVyZXIgPSBuZXcgT3JkZXJQcm9kdWN0UmVuZGVyZXIoKTtcbiAgICB0aGlzLm9yZGVyUHJpY2VzUmVmcmVzaGVyID0gbmV3IE9yZGVyUHJpY2VzUmVmcmVzaGVyKCk7XG4gICAgdGhpcy5pc09yZGVyVGF4SW5jbHVkZWQgPSAkKE9yZGVyVmlld1BhZ2VNYXAucHJvZHVjdEFkZFJvdykuZGF0YSgnaXNPcmRlclRheEluY2x1ZGVkJyk7XG4gICAgdGhpcy50YXhFeGNsdWRlZCA9IG51bGw7XG4gICAgdGhpcy50YXhJbmNsdWRlZCA9IG51bGw7XG4gIH1cblxuICBzZXR1cExpc3RlbmVyKCkge1xuICAgIHRoaXMuY29tYmluYXRpb25zU2VsZWN0Lm9uKCdjaGFuZ2UnLCAoZXZlbnQpID0+IHtcbiAgICAgIGNvbnN0IHRheEV4Y2x1ZGVkID0gd2luZG93LnBzX3JvdW5kKFxuICAgICAgICAkKGV2ZW50LmN1cnJlbnRUYXJnZXQpXG4gICAgICAgICAgLmZpbmQoJzpzZWxlY3RlZCcpXG4gICAgICAgICAgLmRhdGEoJ3ByaWNlVGF4RXhjbHVkZWQnKSxcbiAgICAgICAgdGhpcy5jdXJyZW5jeVByZWNpc2lvbixcbiAgICAgICk7XG4gICAgICB0aGlzLnByaWNlVGF4RXhjbHVkZWRJbnB1dC52YWwodGF4RXhjbHVkZWQpO1xuICAgICAgdGhpcy50YXhFeGNsdWRlZCA9IHBhcnNlRmxvYXQodGF4RXhjbHVkZWQpO1xuXG4gICAgICBjb25zdCB0YXhJbmNsdWRlZCA9IHdpbmRvdy5wc19yb3VuZChcbiAgICAgICAgJChldmVudC5jdXJyZW50VGFyZ2V0KVxuICAgICAgICAgIC5maW5kKCc6c2VsZWN0ZWQnKVxuICAgICAgICAgIC5kYXRhKCdwcmljZVRheEluY2x1ZGVkJyksXG4gICAgICAgIHRoaXMuY3VycmVuY3lQcmVjaXNpb24sXG4gICAgICApO1xuICAgICAgdGhpcy5wcmljZVRheEluY2x1ZGVkSW5wdXQudmFsKHRheEluY2x1ZGVkKTtcbiAgICAgIHRoaXMudGF4SW5jbHVkZWQgPSBwYXJzZUZsb2F0KHRheEluY2x1ZGVkKTtcblxuICAgICAgdGhpcy5sb2NhdGlvblRleHQuaHRtbChcbiAgICAgICAgJChldmVudC5jdXJyZW50VGFyZ2V0KVxuICAgICAgICAgIC5maW5kKCc6c2VsZWN0ZWQnKVxuICAgICAgICAgIC5kYXRhKCdsb2NhdGlvbicpLFxuICAgICAgKTtcblxuICAgICAgdGhpcy5hdmFpbGFibGUgPSAkKGV2ZW50LmN1cnJlbnRUYXJnZXQpXG4gICAgICAgIC5maW5kKCc6c2VsZWN0ZWQnKVxuICAgICAgICAuZGF0YSgnc3RvY2snKTtcblxuICAgICAgdGhpcy5xdWFudGl0eUlucHV0LnRyaWdnZXIoJ2NoYW5nZScpO1xuICAgICAgdGhpcy5vcmRlclByb2R1Y3RSZW5kZXJlci50b2dnbGVDb2x1bW4oT3JkZXJWaWV3UGFnZU1hcC5wcm9kdWN0c0NlbGxMb2NhdGlvbik7XG4gICAgfSk7XG5cbiAgICB0aGlzLnF1YW50aXR5SW5wdXQub24oJ2NoYW5nZSBrZXl1cCcsIChldmVudCkgPT4ge1xuICAgICAgaWYgKHRoaXMuYXZhaWxhYmxlICE9PSBudWxsKSB7XG4gICAgICAgIGNvbnN0IG5ld1F1YW50aXR5ID0gTnVtYmVyKGV2ZW50LnRhcmdldC52YWx1ZSk7XG4gICAgICAgIGNvbnN0IHJlbWFpbmluZ0F2YWlsYWJsZSA9IHRoaXMuYXZhaWxhYmxlIC0gbmV3UXVhbnRpdHk7XG4gICAgICAgIGNvbnN0IGF2YWlsYWJsZU91dE9mU3RvY2sgPSB0aGlzLmF2YWlsYWJsZVRleHQuZGF0YSgnYXZhaWxhYmxlT3V0T2ZTdG9jaycpO1xuICAgICAgICB0aGlzLmF2YWlsYWJsZVRleHQudGV4dChyZW1haW5pbmdBdmFpbGFibGUpO1xuICAgICAgICB0aGlzLmF2YWlsYWJsZVRleHQudG9nZ2xlQ2xhc3MoJ3RleHQtZGFuZ2VyIGZvbnQtd2VpZ2h0LWJvbGQnLCByZW1haW5pbmdBdmFpbGFibGUgPCAwKTtcbiAgICAgICAgY29uc3QgZGlzYWJsZUFkZEFjdGlvbkJ0biA9IG5ld1F1YW50aXR5IDw9IDAgfHwgKHJlbWFpbmluZ0F2YWlsYWJsZSA8IDAgJiYgIWF2YWlsYWJsZU91dE9mU3RvY2spO1xuICAgICAgICB0aGlzLnByb2R1Y3RBZGRBY3Rpb25CdG4ucHJvcCgnZGlzYWJsZWQnLCBkaXNhYmxlQWRkQWN0aW9uQnRuKTtcbiAgICAgICAgdGhpcy5pbnZvaWNlU2VsZWN0LnByb3AoJ2Rpc2FibGVkJywgIWF2YWlsYWJsZU91dE9mU3RvY2sgJiYgcmVtYWluaW5nQXZhaWxhYmxlIDwgMCk7XG5cbiAgICAgICAgdGhpcy50YXhJbmNsdWRlZCA9IHBhcnNlRmxvYXQodGhpcy5wcmljZVRheEluY2x1ZGVkSW5wdXQudmFsKCkpO1xuICAgICAgICB0aGlzLnRvdGFsUHJpY2VUZXh0Lmh0bWwoXG4gICAgICAgICAgdGhpcy5wcmljZVRheENhbGN1bGF0b3IuY2FsY3VsYXRlVG90YWxQcmljZShcbiAgICAgICAgICAgIG5ld1F1YW50aXR5LFxuICAgICAgICAgICAgdGhpcy5pc09yZGVyVGF4SW5jbHVkZWQgPyB0aGlzLnRheEluY2x1ZGVkIDogdGhpcy50YXhFeGNsdWRlZCxcbiAgICAgICAgICAgIHRoaXMuY3VycmVuY3lQcmVjaXNpb24sXG4gICAgICAgICAgKSxcbiAgICAgICAgKTtcbiAgICAgIH1cbiAgICB9KTtcblxuICAgIHRoaXMucHJvZHVjdElkSW5wdXQub24oJ2NoYW5nZScsICgpID0+IHtcbiAgICAgIHRoaXMucHJvZHVjdEFkZEFjdGlvbkJ0bi5yZW1vdmVBdHRyKCdkaXNhYmxlZCcpO1xuICAgICAgdGhpcy5pbnZvaWNlU2VsZWN0LnJlbW92ZUF0dHIoJ2Rpc2FibGVkJyk7XG4gICAgfSk7XG5cbiAgICB0aGlzLnByaWNlVGF4SW5jbHVkZWRJbnB1dC5vbignY2hhbmdlIGtleXVwJywgKGV2ZW50KSA9PiB7XG4gICAgICB0aGlzLnRheEluY2x1ZGVkID0gcGFyc2VGbG9hdChldmVudC50YXJnZXQudmFsdWUpO1xuICAgICAgdGhpcy50YXhFeGNsdWRlZCA9IHRoaXMucHJpY2VUYXhDYWxjdWxhdG9yLmNhbGN1bGF0ZVRheEV4Y2x1ZGVkKFxuICAgICAgICB0aGlzLnRheEluY2x1ZGVkLFxuICAgICAgICB0aGlzLnRheFJhdGVJbnB1dC52YWwoKSxcbiAgICAgICAgdGhpcy5jdXJyZW5jeVByZWNpc2lvbixcbiAgICAgICk7XG4gICAgICBjb25zdCBxdWFudGl0eSA9IHBhcnNlSW50KHRoaXMucXVhbnRpdHlJbnB1dC52YWwoKSwgMTApO1xuXG4gICAgICB0aGlzLnByaWNlVGF4RXhjbHVkZWRJbnB1dC52YWwodGhpcy50YXhFeGNsdWRlZCk7XG4gICAgICB0aGlzLnRvdGFsUHJpY2VUZXh0Lmh0bWwoXG4gICAgICAgIHRoaXMucHJpY2VUYXhDYWxjdWxhdG9yLmNhbGN1bGF0ZVRvdGFsUHJpY2UoXG4gICAgICAgICAgcXVhbnRpdHksXG4gICAgICAgICAgdGhpcy5pc09yZGVyVGF4SW5jbHVkZWQgPyB0aGlzLnRheEluY2x1ZGVkIDogdGhpcy50YXhFeGNsdWRlZCxcbiAgICAgICAgICB0aGlzLmN1cnJlbmN5UHJlY2lzaW9uLFxuICAgICAgICApLFxuICAgICAgKTtcbiAgICB9KTtcblxuICAgIHRoaXMucHJpY2VUYXhFeGNsdWRlZElucHV0Lm9uKCdjaGFuZ2Uga2V5dXAnLCAoZXZlbnQpID0+IHtcbiAgICAgIHRoaXMudGF4RXhjbHVkZWQgPSBwYXJzZUZsb2F0KGV2ZW50LnRhcmdldC52YWx1ZSk7XG4gICAgICB0aGlzLnRheEluY2x1ZGVkID0gdGhpcy5wcmljZVRheENhbGN1bGF0b3IuY2FsY3VsYXRlVGF4SW5jbHVkZWQoXG4gICAgICAgIHRoaXMudGF4RXhjbHVkZWQsXG4gICAgICAgIHRoaXMudGF4UmF0ZUlucHV0LnZhbCgpLFxuICAgICAgICB0aGlzLmN1cnJlbmN5UHJlY2lzaW9uLFxuICAgICAgKTtcbiAgICAgIGNvbnN0IHF1YW50aXR5ID0gcGFyc2VJbnQodGhpcy5xdWFudGl0eUlucHV0LnZhbCgpLCAxMCk7XG5cbiAgICAgIHRoaXMucHJpY2VUYXhJbmNsdWRlZElucHV0LnZhbCh0aGlzLnRheEluY2x1ZGVkKTtcbiAgICAgIHRoaXMudG90YWxQcmljZVRleHQuaHRtbChcbiAgICAgICAgdGhpcy5wcmljZVRheENhbGN1bGF0b3IuY2FsY3VsYXRlVG90YWxQcmljZShcbiAgICAgICAgICBxdWFudGl0eSxcbiAgICAgICAgICB0aGlzLmlzT3JkZXJUYXhJbmNsdWRlZCA/IHRoaXMudGF4SW5jbHVkZWQgOiB0aGlzLnRheEV4Y2x1ZGVkLFxuICAgICAgICAgIHRoaXMuY3VycmVuY3lQcmVjaXNpb24sXG4gICAgICAgICksXG4gICAgICApO1xuICAgIH0pO1xuXG4gICAgdGhpcy5wcm9kdWN0QWRkQWN0aW9uQnRuLm9uKCdjbGljaycsIChldmVudCkgPT4gdGhpcy5jb25maXJtTmV3SW52b2ljZShldmVudCkpO1xuICAgIHRoaXMuaW52b2ljZVNlbGVjdC5vbignY2hhbmdlJywgKCkgPT4gdGhpcy5vcmRlclByb2R1Y3RSZW5kZXJlci50b2dnbGVQcm9kdWN0QWRkTmV3SW52b2ljZUluZm8oKSk7XG4gIH1cblxuICBzZXRQcm9kdWN0KHByb2R1Y3QpIHtcbiAgICB0aGlzLnByb2R1Y3RJZElucHV0LnZhbChwcm9kdWN0LnByb2R1Y3RJZCkudHJpZ2dlcignY2hhbmdlJyk7XG5cbiAgICBjb25zdCB0YXhFeGNsdWRlZCA9IHdpbmRvdy5wc19yb3VuZChwcm9kdWN0LnByaWNlVGF4RXhjbCwgdGhpcy5jdXJyZW5jeVByZWNpc2lvbik7XG4gICAgdGhpcy5wcmljZVRheEV4Y2x1ZGVkSW5wdXQudmFsKHRheEV4Y2x1ZGVkKTtcbiAgICB0aGlzLnRheEV4Y2x1ZGVkID0gcGFyc2VGbG9hdCh0YXhFeGNsdWRlZCk7XG5cbiAgICBjb25zdCB0YXhJbmNsdWRlZCA9IHdpbmRvdy5wc19yb3VuZChwcm9kdWN0LnByaWNlVGF4SW5jbCwgdGhpcy5jdXJyZW5jeVByZWNpc2lvbik7XG4gICAgdGhpcy5wcmljZVRheEluY2x1ZGVkSW5wdXQudmFsKHRheEluY2x1ZGVkKTtcbiAgICB0aGlzLnRheEluY2x1ZGVkID0gcGFyc2VGbG9hdCh0YXhJbmNsdWRlZCk7XG5cbiAgICB0aGlzLnRheFJhdGVJbnB1dC52YWwocHJvZHVjdC50YXhSYXRlKTtcbiAgICB0aGlzLmxvY2F0aW9uVGV4dC5odG1sKHByb2R1Y3QubG9jYXRpb24pO1xuICAgIHRoaXMuYXZhaWxhYmxlID0gcHJvZHVjdC5zdG9jaztcbiAgICB0aGlzLmF2YWlsYWJsZVRleHQuZGF0YSgnYXZhaWxhYmxlT3V0T2ZTdG9jaycsIHByb2R1Y3QuYXZhaWxhYmxlT3V0T2ZTdG9jayk7XG4gICAgdGhpcy5xdWFudGl0eUlucHV0LnZhbCgxKTtcbiAgICB0aGlzLnF1YW50aXR5SW5wdXQudHJpZ2dlcignY2hhbmdlJyk7XG4gICAgdGhpcy5zZXRDb21iaW5hdGlvbnMocHJvZHVjdC5jb21iaW5hdGlvbnMpO1xuICAgIHRoaXMub3JkZXJQcm9kdWN0UmVuZGVyZXIudG9nZ2xlQ29sdW1uKE9yZGVyVmlld1BhZ2VNYXAucHJvZHVjdHNDZWxsTG9jYXRpb24pO1xuICB9XG5cbiAgc2V0Q29tYmluYXRpb25zKGNvbWJpbmF0aW9ucykge1xuICAgIHRoaXMuY29tYmluYXRpb25zU2VsZWN0LmVtcHR5KCk7XG5cbiAgICBPYmplY3QudmFsdWVzKGNvbWJpbmF0aW9ucykuZm9yRWFjaCgodmFsKSA9PiB7XG4gICAgICB0aGlzLmNvbWJpbmF0aW9uc1NlbGVjdC5hcHBlbmQoXG4gICAgICAgIC8qIGVzbGludC1kaXNhYmxlLW5leHQtbGluZSBtYXgtbGVuICovXG4gICAgICAgIGA8b3B0aW9uIHZhbHVlPVwiJHt2YWwuYXR0cmlidXRlQ29tYmluYXRpb25JZH1cIiBkYXRhLXByaWNlLXRheC1leGNsdWRlZD1cIiR7dmFsLnByaWNlVGF4RXhjbHVkZWR9XCIgZGF0YS1wcmljZS10YXgtaW5jbHVkZWQ9XCIke3ZhbC5wcmljZVRheEluY2x1ZGVkfVwiIGRhdGEtc3RvY2s9XCIke3ZhbC5zdG9ja31cIiBkYXRhLWxvY2F0aW9uPVwiJHt2YWwubG9jYXRpb259XCI+JHt2YWwuYXR0cmlidXRlfTwvb3B0aW9uPmAsXG4gICAgICApO1xuICAgIH0pO1xuXG4gICAgdGhpcy5jb21iaW5hdGlvbnNCbG9jay50b2dnbGVDbGFzcygnZC1ub25lJywgT2JqZWN0LmtleXMoY29tYmluYXRpb25zKS5sZW5ndGggPT09IDApO1xuXG4gICAgaWYgKE9iamVjdC5rZXlzKGNvbWJpbmF0aW9ucykubGVuZ3RoID4gMCkge1xuICAgICAgdGhpcy5jb21iaW5hdGlvbnNTZWxlY3QudHJpZ2dlcignY2hhbmdlJyk7XG4gICAgfVxuICB9XG5cbiAgYWRkUHJvZHVjdChvcmRlcklkKSB7XG4gICAgdGhpcy5wcm9kdWN0QWRkQWN0aW9uQnRuLnByb3AoJ2Rpc2FibGVkJywgdHJ1ZSk7XG4gICAgdGhpcy5pbnZvaWNlU2VsZWN0LnByb3AoJ2Rpc2FibGVkJywgdHJ1ZSk7XG4gICAgdGhpcy5jb21iaW5hdGlvbnNTZWxlY3QucHJvcCgnZGlzYWJsZWQnLCB0cnVlKTtcblxuICAgIGNvbnN0IHBhcmFtcyA9IHtcbiAgICAgIHByb2R1Y3RfaWQ6IHRoaXMucHJvZHVjdElkSW5wdXQudmFsKCksXG4gICAgICBjb21iaW5hdGlvbl9pZDogJCgnOnNlbGVjdGVkJywgdGhpcy5jb21iaW5hdGlvbnNTZWxlY3QpLnZhbCgpLFxuICAgICAgcHJpY2VfdGF4X2luY2w6IHRoaXMucHJpY2VUYXhJbmNsdWRlZElucHV0LnZhbCgpLFxuICAgICAgcHJpY2VfdGF4X2V4Y2w6IHRoaXMucHJpY2VUYXhFeGNsdWRlZElucHV0LnZhbCgpLFxuICAgICAgcXVhbnRpdHk6IHRoaXMucXVhbnRpdHlJbnB1dC52YWwoKSxcbiAgICAgIGludm9pY2VfaWQ6IHRoaXMuaW52b2ljZVNlbGVjdC52YWwoKSxcbiAgICAgIGZyZWVfc2hpcHBpbmc6IHRoaXMuZnJlZVNoaXBwaW5nU2VsZWN0LnByb3AoJ2NoZWNrZWQnKSxcbiAgICB9O1xuXG4gICAgJC5hamF4KHtcbiAgICAgIHVybDogdGhpcy5yb3V0ZXIuZ2VuZXJhdGUoJ2FkbWluX29yZGVyc19hZGRfcHJvZHVjdCcsIHtvcmRlcklkfSksXG4gICAgICBtZXRob2Q6ICdQT1NUJyxcbiAgICAgIGRhdGE6IHBhcmFtcyxcbiAgICB9KS50aGVuKFxuICAgICAgKHJlc3BvbnNlKSA9PiB7XG4gICAgICAgIEV2ZW50RW1pdHRlci5lbWl0KE9yZGVyVmlld0V2ZW50TWFwLnByb2R1Y3RBZGRlZFRvT3JkZXIsIHtcbiAgICAgICAgICBvcmRlcklkLFxuICAgICAgICAgIG9yZGVyUHJvZHVjdElkOiBwYXJhbXMucHJvZHVjdF9pZCxcbiAgICAgICAgICBuZXdSb3c6IHJlc3BvbnNlLFxuICAgICAgICB9KTtcbiAgICAgIH0sXG4gICAgICAocmVzcG9uc2UpID0+IHtcbiAgICAgICAgdGhpcy5wcm9kdWN0QWRkQWN0aW9uQnRuLnByb3AoJ2Rpc2FibGVkJywgZmFsc2UpO1xuICAgICAgICB0aGlzLmludm9pY2VTZWxlY3QucHJvcCgnZGlzYWJsZWQnLCBmYWxzZSk7XG4gICAgICAgIHRoaXMuY29tYmluYXRpb25zU2VsZWN0LnByb3AoJ2Rpc2FibGVkJywgZmFsc2UpO1xuXG4gICAgICAgIGlmIChyZXNwb25zZS5yZXNwb25zZUpTT04gJiYgcmVzcG9uc2UucmVzcG9uc2VKU09OLm1lc3NhZ2UpIHtcbiAgICAgICAgICAkLmdyb3dsLmVycm9yKHttZXNzYWdlOiByZXNwb25zZS5yZXNwb25zZUpTT04ubWVzc2FnZX0pO1xuICAgICAgICB9XG4gICAgICB9LFxuICAgICk7XG4gIH1cblxuICBjb25maXJtTmV3SW52b2ljZShldmVudCkge1xuICAgIGNvbnN0IGludm9pY2VJZCA9IHBhcnNlSW50KHRoaXMuaW52b2ljZVNlbGVjdC52YWwoKSwgMTApO1xuICAgIGNvbnN0IG9yZGVySWQgPSAkKGV2ZW50LmN1cnJlbnRUYXJnZXQpLmRhdGEoJ29yZGVySWQnKTtcblxuICAgIC8vIEV4cGxpY2l0IDAgdmFsdWUgaXMgdXNlZCB3aGVuIHdlIHRoZSB1c2VyIHNlbGVjdGVkIE5ldyBJbnZvaWNlXG4gICAgaWYgKGludm9pY2VJZCA9PT0gMCkge1xuICAgICAgY29uc3QgbW9kYWwgPSBuZXcgQ29uZmlybU1vZGFsKFxuICAgICAgICB7XG4gICAgICAgICAgaWQ6ICdtb2RhbC1jb25maXJtLW5ldy1pbnZvaWNlJyxcbiAgICAgICAgICBjb25maXJtVGl0bGU6IHRoaXMuaW52b2ljZVNlbGVjdC5kYXRhKCdtb2RhbC10aXRsZScpLFxuICAgICAgICAgIGNvbmZpcm1NZXNzYWdlOiB0aGlzLmludm9pY2VTZWxlY3QuZGF0YSgnbW9kYWwtYm9keScpLFxuICAgICAgICAgIGNvbmZpcm1CdXR0b25MYWJlbDogdGhpcy5pbnZvaWNlU2VsZWN0LmRhdGEoJ21vZGFsLWFwcGx5JyksXG4gICAgICAgICAgY2xvc2VCdXR0b25MYWJlbDogdGhpcy5pbnZvaWNlU2VsZWN0LmRhdGEoJ21vZGFsLWNhbmNlbCcpLFxuICAgICAgICB9LFxuICAgICAgICAoKSA9PiB7XG4gICAgICAgICAgdGhpcy5jb25maXJtTmV3UHJpY2Uob3JkZXJJZCwgaW52b2ljZUlkKTtcbiAgICAgICAgfSxcbiAgICAgICk7XG4gICAgICBtb2RhbC5zaG93KCk7XG4gICAgfSBlbHNlIHtcbiAgICAgIC8vIExhc3QgY2FzZSBpcyBOYW4sIHRoZSBzZWxlY3RvciBpcyBub3QgZXZlbiBwcmVzZW50LCB3ZSBzaW1wbHkgYWRkIHByb2R1Y3QgYW5kIGxldCB0aGUgQk8gaGFuZGxlIGl0XG4gICAgICB0aGlzLmFkZFByb2R1Y3Qob3JkZXJJZCk7XG4gICAgfVxuICB9XG5cbiAgY29uZmlybU5ld1ByaWNlKG9yZGVySWQsIGludm9pY2VJZCkge1xuICAgIGNvbnN0IGNvbWJpbmF0aW9uVmFsdWUgPSAkKCc6c2VsZWN0ZWQnLCB0aGlzLmNvbWJpbmF0aW9uc1NlbGVjdCkudmFsKCk7XG4gICAgY29uc3QgY29tYmluYXRpb25JZCA9IHR5cGVvZiBjb21iaW5hdGlvblZhbHVlID09PSAndW5kZWZpbmVkJyA/IDAgOiBjb21iaW5hdGlvblZhbHVlO1xuICAgIGNvbnN0IHByb2R1Y3RQcmljZU1hdGNoID0gdGhpcy5vcmRlclByaWNlc1JlZnJlc2hlci5jaGVja090aGVyUHJvZHVjdFByaWNlc01hdGNoKFxuICAgICAgdGhpcy5wcmljZVRheEluY2x1ZGVkSW5wdXQudmFsKCksXG4gICAgICB0aGlzLnByb2R1Y3RJZElucHV0LnZhbCgpLFxuICAgICAgY29tYmluYXRpb25JZCxcbiAgICAgIGludm9pY2VJZCxcbiAgICApO1xuXG4gICAgaWYgKHByb2R1Y3RQcmljZU1hdGNoID09PSAnaW52b2ljZScpIHtcbiAgICAgIGNvbnN0IG1vZGFsRWRpdFByaWNlID0gbmV3IENvbmZpcm1Nb2RhbChcbiAgICAgICAge1xuICAgICAgICAgIGlkOiAnbW9kYWwtY29uZmlybS1uZXctcHJpY2UnLFxuICAgICAgICAgIGNvbmZpcm1UaXRsZTogdGhpcy5pbnZvaWNlU2VsZWN0LmRhdGEoJ21vZGFsLWVkaXQtcHJpY2UtdGl0bGUnKSxcbiAgICAgICAgICBjb25maXJtTWVzc2FnZTogdGhpcy5pbnZvaWNlU2VsZWN0LmRhdGEoJ21vZGFsLWVkaXQtcHJpY2UtYm9keScpLFxuICAgICAgICAgIGNvbmZpcm1CdXR0b25MYWJlbDogdGhpcy5pbnZvaWNlU2VsZWN0LmRhdGEoJ21vZGFsLWVkaXQtcHJpY2UtYXBwbHknKSxcbiAgICAgICAgICBjbG9zZUJ1dHRvbkxhYmVsOiB0aGlzLmludm9pY2VTZWxlY3QuZGF0YSgnbW9kYWwtZWRpdC1wcmljZS1jYW5jZWwnKSxcbiAgICAgICAgfSxcbiAgICAgICAgKCkgPT4ge1xuICAgICAgICAgIHRoaXMuYWRkUHJvZHVjdChvcmRlcklkKTtcbiAgICAgICAgfSxcbiAgICAgICk7XG4gICAgICBtb2RhbEVkaXRQcmljZS5zaG93KCk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMuYWRkUHJvZHVjdChvcmRlcklkKTtcbiAgICB9XG4gIH1cbn1cbiIsIi8qKlxuICogQ29weXJpZ2h0IHNpbmNlIDIwMDcgUHJlc3RhU2hvcCBTQSBhbmQgQ29udHJpYnV0b3JzXG4gKiBQcmVzdGFTaG9wIGlzIGFuIEludGVybmF0aW9uYWwgUmVnaXN0ZXJlZCBUcmFkZW1hcmsgJiBQcm9wZXJ0eSBvZiBQcmVzdGFTaG9wIFNBXG4gKlxuICogTk9USUNFIE9GIExJQ0VOU0VcbiAqXG4gKiBUaGlzIHNvdXJjZSBmaWxlIGlzIHN1YmplY3QgdG8gdGhlIE9wZW4gU29mdHdhcmUgTGljZW5zZSAoT1NMIDMuMClcbiAqIHRoYXQgaXMgYnVuZGxlZCB3aXRoIHRoaXMgcGFja2FnZSBpbiB0aGUgZmlsZSBMSUNFTlNFLm1kLlxuICogSXQgaXMgYWxzbyBhdmFpbGFibGUgdGhyb3VnaCB0aGUgd29ybGQtd2lkZS13ZWIgYXQgdGhpcyBVUkw6XG4gKiBodHRwczovL29wZW5zb3VyY2Uub3JnL2xpY2Vuc2VzL09TTC0zLjBcbiAqIElmIHlvdSBkaWQgbm90IHJlY2VpdmUgYSBjb3B5IG9mIHRoZSBsaWNlbnNlIGFuZCBhcmUgdW5hYmxlIHRvXG4gKiBvYnRhaW4gaXQgdGhyb3VnaCB0aGUgd29ybGQtd2lkZS13ZWIsIHBsZWFzZSBzZW5kIGFuIGVtYWlsXG4gKiB0byBsaWNlbnNlQHByZXN0YXNob3AuY29tIHNvIHdlIGNhbiBzZW5kIHlvdSBhIGNvcHkgaW1tZWRpYXRlbHkuXG4gKlxuICogRElTQ0xBSU1FUlxuICpcbiAqIERvIG5vdCBlZGl0IG9yIGFkZCB0byB0aGlzIGZpbGUgaWYgeW91IHdpc2ggdG8gdXBncmFkZSBQcmVzdGFTaG9wIHRvIG5ld2VyXG4gKiB2ZXJzaW9ucyBpbiB0aGUgZnV0dXJlLiBJZiB5b3Ugd2lzaCB0byBjdXN0b21pemUgUHJlc3RhU2hvcCBmb3IgeW91clxuICogbmVlZHMgcGxlYXNlIHJlZmVyIHRvIGh0dHBzOi8vZGV2ZG9jcy5wcmVzdGFzaG9wLmNvbS8gZm9yIG1vcmUgaW5mb3JtYXRpb24uXG4gKlxuICogQGF1dGhvciAgICBQcmVzdGFTaG9wIFNBIGFuZCBDb250cmlidXRvcnMgPGNvbnRhY3RAcHJlc3Rhc2hvcC5jb20+XG4gKiBAY29weXJpZ2h0IFNpbmNlIDIwMDcgUHJlc3RhU2hvcCBTQSBhbmQgQ29udHJpYnV0b3JzXG4gKiBAbGljZW5zZSAgIGh0dHBzOi8vb3BlbnNvdXJjZS5vcmcvbGljZW5zZXMvT1NMLTMuMCBPcGVuIFNvZnR3YXJlIExpY2Vuc2UgKE9TTCAzLjApXG4gKi9cblxuaW1wb3J0IFJvdXRlciBmcm9tICdAY29tcG9uZW50cy9yb3V0ZXInO1xuaW1wb3J0IE9yZGVyVmlld1BhZ2VNYXAgZnJvbSAnQHBhZ2VzL29yZGVyL09yZGVyVmlld1BhZ2VNYXAnO1xuaW1wb3J0IHtOdW1iZXJGb3JtYXR0ZXJ9IGZyb20gJ0BhcHAvY2xkcic7XG5cbmNvbnN0IHskfSA9IHdpbmRvdztcblxuLyoqXG4gKiBtYW5hZ2VzIGFsbCBwcm9kdWN0IGNhbmNlbCBhY3Rpb25zLCB0aGF0IGluY2x1ZGVzIGFsbCByZWZ1bmQgb3BlcmF0aW9uc1xuICovXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBPcmRlclByb2R1Y3RDYW5jZWwge1xuICBjb25zdHJ1Y3RvcigpIHtcbiAgICB0aGlzLnJvdXRlciA9IG5ldyBSb3V0ZXIoKTtcbiAgICB0aGlzLmNhbmNlbFByb2R1Y3RGb3JtID0gJChPcmRlclZpZXdQYWdlTWFwLmNhbmNlbFByb2R1Y3QuZm9ybSk7XG4gICAgdGhpcy5vcmRlcklkID0gdGhpcy5jYW5jZWxQcm9kdWN0Rm9ybS5kYXRhKCdvcmRlcklkJyk7XG4gICAgdGhpcy5vcmRlckRlbGl2ZXJlZCA9IHBhcnNlSW50KHRoaXMuY2FuY2VsUHJvZHVjdEZvcm0uZGF0YSgnaXNEZWxpdmVyZWQnKSwgMTApID09PSAxO1xuICAgIHRoaXMuaXNUYXhJbmNsdWRlZCA9IHBhcnNlSW50KHRoaXMuY2FuY2VsUHJvZHVjdEZvcm0uZGF0YSgnaXNUYXhJbmNsdWRlZCcpLCAxMCkgPT09IDE7XG4gICAgdGhpcy5kaXNjb3VudHNBbW91bnQgPSBwYXJzZUZsb2F0KHRoaXMuY2FuY2VsUHJvZHVjdEZvcm0uZGF0YSgnZGlzY291bnRzQW1vdW50JykpO1xuICAgIHRoaXMuY3VycmVuY3lGb3JtYXR0ZXIgPSBOdW1iZXJGb3JtYXR0ZXIuYnVpbGQoXG4gICAgICB0aGlzLmNhbmNlbFByb2R1Y3RGb3JtLmRhdGEoJ3ByaWNlU3BlY2lmaWNhdGlvbicpLFxuICAgICk7XG4gICAgdGhpcy51c2VBbW91bnRJbnB1dHMgPSB0cnVlO1xuICAgIHRoaXMubGlzdGVuRm9ySW5wdXRzKCk7XG4gIH1cblxuICBzaG93UGFydGlhbFJlZnVuZCgpIHtcbiAgICAvLyBBbHdheXMgc3RhcnQgYnkgaGlkaW5nIGVsZW1lbnRzIHRoZW4gc2hvdyB0aGUgb3RoZXJzLCBzaW5jZSBzb21lIGVsZW1lbnRzIGFyZSBjb21tb25cbiAgICB0aGlzLmhpZGVDYW5jZWxFbGVtZW50cygpO1xuICAgICQoT3JkZXJWaWV3UGFnZU1hcC5jYW5jZWxQcm9kdWN0LnRvZ2dsZS5wYXJ0aWFsUmVmdW5kKS5zaG93KCk7XG4gICAgdGhpcy51c2VBbW91bnRJbnB1dHMgPSB0cnVlO1xuICAgIHRoaXMuaW5pdEZvcm0oXG4gICAgICAkKE9yZGVyVmlld1BhZ2VNYXAuY2FuY2VsUHJvZHVjdC5idXR0b25zLnNhdmUpLmRhdGEoJ3BhcnRpYWxSZWZ1bmRMYWJlbCcpLFxuICAgICAgdGhpcy5yb3V0ZXIuZ2VuZXJhdGUoJ2FkbWluX29yZGVyc19wYXJ0aWFsX3JlZnVuZCcsIHtcbiAgICAgICAgb3JkZXJJZDogdGhpcy5vcmRlcklkLFxuICAgICAgfSksXG4gICAgICAncGFydGlhbC1yZWZ1bmQnLFxuICAgICk7XG4gIH1cblxuICBzaG93U3RhbmRhcmRSZWZ1bmQoKSB7XG4gICAgLy8gQWx3YXlzIHN0YXJ0IGJ5IGhpZGluZyBlbGVtZW50cyB0aGVuIHNob3cgdGhlIG90aGVycywgc2luY2Ugc29tZSBlbGVtZW50cyBhcmUgY29tbW9uXG4gICAgdGhpcy5oaWRlQ2FuY2VsRWxlbWVudHMoKTtcbiAgICAkKE9yZGVyVmlld1BhZ2VNYXAuY2FuY2VsUHJvZHVjdC50b2dnbGUuc3RhbmRhcmRSZWZ1bmQpLnNob3coKTtcbiAgICB0aGlzLnVzZUFtb3VudElucHV0cyA9IGZhbHNlO1xuICAgIHRoaXMuaW5pdEZvcm0oXG4gICAgICAkKE9yZGVyVmlld1BhZ2VNYXAuY2FuY2VsUHJvZHVjdC5idXR0b25zLnNhdmUpLmRhdGEoJ3N0YW5kYXJkUmVmdW5kTGFiZWwnKSxcbiAgICAgIHRoaXMucm91dGVyLmdlbmVyYXRlKCdhZG1pbl9vcmRlcnNfc3RhbmRhcmRfcmVmdW5kJywge1xuICAgICAgICBvcmRlcklkOiB0aGlzLm9yZGVySWQsXG4gICAgICB9KSxcbiAgICAgICdzdGFuZGFyZC1yZWZ1bmQnLFxuICAgICk7XG4gIH1cblxuICBzaG93UmV0dXJuUHJvZHVjdCgpIHtcbiAgICAvLyBBbHdheXMgc3RhcnQgYnkgaGlkaW5nIGVsZW1lbnRzIHRoZW4gc2hvdyB0aGUgb3RoZXJzLCBzaW5jZSBzb21lIGVsZW1lbnRzIGFyZSBjb21tb25cbiAgICB0aGlzLmhpZGVDYW5jZWxFbGVtZW50cygpO1xuICAgICQoT3JkZXJWaWV3UGFnZU1hcC5jYW5jZWxQcm9kdWN0LnRvZ2dsZS5yZXR1cm5Qcm9kdWN0KS5zaG93KCk7XG4gICAgdGhpcy51c2VBbW91bnRJbnB1dHMgPSBmYWxzZTtcbiAgICB0aGlzLmluaXRGb3JtKFxuICAgICAgJChPcmRlclZpZXdQYWdlTWFwLmNhbmNlbFByb2R1Y3QuYnV0dG9ucy5zYXZlKS5kYXRhKCdyZXR1cm5Qcm9kdWN0TGFiZWwnKSxcbiAgICAgIHRoaXMucm91dGVyLmdlbmVyYXRlKCdhZG1pbl9vcmRlcnNfcmV0dXJuX3Byb2R1Y3QnLCB7XG4gICAgICAgIG9yZGVySWQ6IHRoaXMub3JkZXJJZCxcbiAgICAgIH0pLFxuICAgICAgJ3JldHVybi1wcm9kdWN0JyxcbiAgICApO1xuICB9XG5cbiAgaGlkZVJlZnVuZCgpIHtcbiAgICB0aGlzLmhpZGVDYW5jZWxFbGVtZW50cygpO1xuICAgICQoT3JkZXJWaWV3UGFnZU1hcC5jYW5jZWxQcm9kdWN0LnRhYmxlLmFjdGlvbnMpLnNob3coKTtcbiAgfVxuXG4gIGhpZGVDYW5jZWxFbGVtZW50cygpIHtcbiAgICAkKE9yZGVyVmlld1BhZ2VNYXAuY2FuY2VsUHJvZHVjdC50b2dnbGUuc3RhbmRhcmRSZWZ1bmQpLmhpZGUoKTtcbiAgICAkKE9yZGVyVmlld1BhZ2VNYXAuY2FuY2VsUHJvZHVjdC50b2dnbGUucGFydGlhbFJlZnVuZCkuaGlkZSgpO1xuICAgICQoT3JkZXJWaWV3UGFnZU1hcC5jYW5jZWxQcm9kdWN0LnRvZ2dsZS5yZXR1cm5Qcm9kdWN0KS5oaWRlKCk7XG4gICAgJChPcmRlclZpZXdQYWdlTWFwLmNhbmNlbFByb2R1Y3QudGFibGUuYWN0aW9ucykuaGlkZSgpO1xuICB9XG5cbiAgaW5pdEZvcm0oYWN0aW9uTmFtZSwgZm9ybUFjdGlvbiwgZm9ybUNsYXNzKSB7XG4gICAgdGhpcy51cGRhdGVWb3VjaGVyUmVmdW5kKCk7XG5cbiAgICB0aGlzLmNhbmNlbFByb2R1Y3RGb3JtLnByb3AoJ2FjdGlvbicsIGZvcm1BY3Rpb24pO1xuICAgIHRoaXMuY2FuY2VsUHJvZHVjdEZvcm1cbiAgICAgIC5yZW1vdmVDbGFzcygnc3RhbmRhcmQtcmVmdW5kIHBhcnRpYWwtcmVmdW5kIHJldHVybi1wcm9kdWN0IGNhbmNlbC1wcm9kdWN0JylcbiAgICAgIC5hZGRDbGFzcyhmb3JtQ2xhc3MpO1xuICAgICQoT3JkZXJWaWV3UGFnZU1hcC5jYW5jZWxQcm9kdWN0LmJ1dHRvbnMuc2F2ZSkuaHRtbChhY3Rpb25OYW1lKTtcbiAgICAkKE9yZGVyVmlld1BhZ2VNYXAuY2FuY2VsUHJvZHVjdC50YWJsZS5oZWFkZXIpLmh0bWwoYWN0aW9uTmFtZSk7XG4gICAgJChPcmRlclZpZXdQYWdlTWFwLmNhbmNlbFByb2R1Y3QuY2hlY2tib3hlcy5yZXN0b2NrKS5wcm9wKCdjaGVja2VkJywgdGhpcy5vcmRlckRlbGl2ZXJlZCk7XG4gICAgJChPcmRlclZpZXdQYWdlTWFwLmNhbmNlbFByb2R1Y3QuY2hlY2tib3hlcy5jcmVkaXRTbGlwKS5wcm9wKCdjaGVja2VkJywgdHJ1ZSk7XG4gICAgJChPcmRlclZpZXdQYWdlTWFwLmNhbmNlbFByb2R1Y3QuY2hlY2tib3hlcy52b3VjaGVyKS5wcm9wKCdjaGVja2VkJywgZmFsc2UpO1xuICB9XG5cbiAgbGlzdGVuRm9ySW5wdXRzKCkge1xuICAgICQoZG9jdW1lbnQpLm9uKCdjaGFuZ2UnLCBPcmRlclZpZXdQYWdlTWFwLmNhbmNlbFByb2R1Y3QuaW5wdXRzLnF1YW50aXR5LCAoZXZlbnQpID0+IHtcbiAgICAgIGNvbnN0ICRwcm9kdWN0UXVhbnRpdHlJbnB1dCA9ICQoZXZlbnQudGFyZ2V0KTtcbiAgICAgIGNvbnN0ICRwYXJlbnRDZWxsID0gJHByb2R1Y3RRdWFudGl0eUlucHV0LnBhcmVudHMoT3JkZXJWaWV3UGFnZU1hcC5jYW5jZWxQcm9kdWN0LnRhYmxlLmNlbGwpO1xuICAgICAgY29uc3QgJHByb2R1Y3RBbW91bnQgPSAkcGFyZW50Q2VsbC5maW5kKE9yZGVyVmlld1BhZ2VNYXAuY2FuY2VsUHJvZHVjdC5pbnB1dHMuYW1vdW50KTtcbiAgICAgIGNvbnN0IHByb2R1Y3RRdWFudGl0eSA9IHBhcnNlSW50KCRwcm9kdWN0UXVhbnRpdHlJbnB1dC52YWwoKSwgMTApO1xuXG4gICAgICBpZiAocHJvZHVjdFF1YW50aXR5IDw9IDApIHtcbiAgICAgICAgJHByb2R1Y3RBbW91bnQudmFsKDApO1xuICAgICAgICB0aGlzLnVwZGF0ZVZvdWNoZXJSZWZ1bmQoKTtcblxuICAgICAgICByZXR1cm47XG4gICAgICB9XG4gICAgICBjb25zdCBwcmljZUZpZWxkTmFtZSA9IHRoaXMuaXNUYXhJbmNsdWRlZCA/ICdwcm9kdWN0UHJpY2VUYXhJbmNsJyA6ICdwcm9kdWN0UHJpY2VUYXhFeGNsJztcbiAgICAgIGNvbnN0IHByb2R1Y3RVbml0UHJpY2UgPSBwYXJzZUZsb2F0KCRwcm9kdWN0UXVhbnRpdHlJbnB1dC5kYXRhKHByaWNlRmllbGROYW1lKSk7XG4gICAgICBjb25zdCBhbW91bnRSZWZ1bmRhYmxlID0gcGFyc2VGbG9hdCgkcHJvZHVjdFF1YW50aXR5SW5wdXQuZGF0YSgnYW1vdW50UmVmdW5kYWJsZScpKTtcbiAgICAgIGNvbnN0IGd1ZXNzZWRBbW91bnQgPSBwcm9kdWN0VW5pdFByaWNlICogcHJvZHVjdFF1YW50aXR5IDwgYW1vdW50UmVmdW5kYWJsZVxuICAgICAgICA/IHByb2R1Y3RVbml0UHJpY2UgKiBwcm9kdWN0UXVhbnRpdHlcbiAgICAgICAgOiBhbW91bnRSZWZ1bmRhYmxlO1xuICAgICAgY29uc3QgYW1vdW50VmFsdWUgPSBwYXJzZUZsb2F0KCRwcm9kdWN0QW1vdW50LnZhbCgpKTtcblxuICAgICAgaWYgKHRoaXMudXNlQW1vdW50SW5wdXRzKSB7XG4gICAgICAgIHRoaXMudXBkYXRlQW1vdW50SW5wdXQoJHByb2R1Y3RRdWFudGl0eUlucHV0KTtcbiAgICAgIH1cblxuICAgICAgaWYgKCRwcm9kdWN0QW1vdW50LnZhbCgpID09PSAnJyB8fCBhbW91bnRWYWx1ZSA9PT0gMCB8fCBhbW91bnRWYWx1ZSA+IGd1ZXNzZWRBbW91bnQpIHtcbiAgICAgICAgJHByb2R1Y3RBbW91bnQudmFsKGd1ZXNzZWRBbW91bnQpO1xuICAgICAgICB0aGlzLnVwZGF0ZVZvdWNoZXJSZWZ1bmQoKTtcbiAgICAgIH1cbiAgICB9KTtcblxuICAgICQoZG9jdW1lbnQpLm9uKCdjaGFuZ2UnLCBPcmRlclZpZXdQYWdlTWFwLmNhbmNlbFByb2R1Y3QuaW5wdXRzLmFtb3VudCwgKCkgPT4ge1xuICAgICAgdGhpcy51cGRhdGVWb3VjaGVyUmVmdW5kKCk7XG4gICAgfSk7XG5cbiAgICAkKGRvY3VtZW50KS5vbignY2hhbmdlJywgT3JkZXJWaWV3UGFnZU1hcC5jYW5jZWxQcm9kdWN0LmlucHV0cy5zZWxlY3RvciwgKGV2ZW50KSA9PiB7XG4gICAgICBjb25zdCAkcHJvZHVjdENoZWNrYm94ID0gJChldmVudC50YXJnZXQpO1xuICAgICAgY29uc3QgJHBhcmVudENlbGwgPSAkcHJvZHVjdENoZWNrYm94LnBhcmVudHMoT3JkZXJWaWV3UGFnZU1hcC5jYW5jZWxQcm9kdWN0LnRhYmxlLmNlbGwpO1xuICAgICAgY29uc3QgcHJvZHVjdFF1YW50aXR5SW5wdXQgPSAkcGFyZW50Q2VsbC5maW5kKE9yZGVyVmlld1BhZ2VNYXAuY2FuY2VsUHJvZHVjdC5pbnB1dHMucXVhbnRpdHkpO1xuICAgICAgY29uc3QgcmVmdW5kYWJsZVF1YW50aXR5ID0gcGFyc2VJbnQocHJvZHVjdFF1YW50aXR5SW5wdXQuZGF0YSgncXVhbnRpdHlSZWZ1bmRhYmxlJyksIDEwKTtcbiAgICAgIGNvbnN0IHByb2R1Y3RRdWFudGl0eSA9IHBhcnNlSW50KHByb2R1Y3RRdWFudGl0eUlucHV0LnZhbCgpLCAxMCk7XG5cbiAgICAgIGlmICghJHByb2R1Y3RDaGVja2JveC5pcygnOmNoZWNrZWQnKSkge1xuICAgICAgICBwcm9kdWN0UXVhbnRpdHlJbnB1dC52YWwoMCk7XG4gICAgICB9IGVsc2UgaWYgKE51bWJlci5pc05hTihwcm9kdWN0UXVhbnRpdHkpIHx8IHByb2R1Y3RRdWFudGl0eSA9PT0gMCkge1xuICAgICAgICBwcm9kdWN0UXVhbnRpdHlJbnB1dC52YWwocmVmdW5kYWJsZVF1YW50aXR5KTtcbiAgICAgIH1cbiAgICAgIHRoaXMudXBkYXRlVm91Y2hlclJlZnVuZCgpO1xuICAgIH0pO1xuICB9XG5cbiAgdXBkYXRlQW1vdW50SW5wdXQoJHByb2R1Y3RRdWFudGl0eUlucHV0KSB7XG4gICAgY29uc3QgJHBhcmVudENlbGwgPSAkcHJvZHVjdFF1YW50aXR5SW5wdXQucGFyZW50cyhPcmRlclZpZXdQYWdlTWFwLmNhbmNlbFByb2R1Y3QudGFibGUuY2VsbCk7XG4gICAgY29uc3QgJHByb2R1Y3RBbW91bnQgPSAkcGFyZW50Q2VsbC5maW5kKE9yZGVyVmlld1BhZ2VNYXAuY2FuY2VsUHJvZHVjdC5pbnB1dHMuYW1vdW50KTtcbiAgICBjb25zdCBwcm9kdWN0UXVhbnRpdHkgPSBwYXJzZUludCgkcHJvZHVjdFF1YW50aXR5SW5wdXQudmFsKCksIDEwKTtcblxuICAgIGlmIChwcm9kdWN0UXVhbnRpdHkgPD0gMCkge1xuICAgICAgJHByb2R1Y3RBbW91bnQudmFsKDApO1xuXG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgY29uc3QgcHJpY2VGaWVsZE5hbWUgPSB0aGlzLmlzVGF4SW5jbHVkZWQgPyAncHJvZHVjdFByaWNlVGF4SW5jbCcgOiAncHJvZHVjdFByaWNlVGF4RXhjbCc7XG4gICAgY29uc3QgcHJvZHVjdFVuaXRQcmljZSA9IHBhcnNlRmxvYXQoJHByb2R1Y3RRdWFudGl0eUlucHV0LmRhdGEocHJpY2VGaWVsZE5hbWUpKTtcbiAgICBjb25zdCBhbW91bnRSZWZ1bmRhYmxlID0gcGFyc2VGbG9hdCgkcHJvZHVjdFF1YW50aXR5SW5wdXQuZGF0YSgnYW1vdW50UmVmdW5kYWJsZScpKTtcbiAgICBjb25zdCBndWVzc2VkQW1vdW50ID0gcHJvZHVjdFVuaXRQcmljZSAqIHByb2R1Y3RRdWFudGl0eSA8IGFtb3VudFJlZnVuZGFibGVcbiAgICAgID8gcHJvZHVjdFVuaXRQcmljZSAqIHByb2R1Y3RRdWFudGl0eVxuICAgICAgOiBhbW91bnRSZWZ1bmRhYmxlO1xuICAgIGNvbnN0IGFtb3VudFZhbHVlID0gcGFyc2VGbG9hdCgkcHJvZHVjdEFtb3VudC52YWwoKSk7XG5cbiAgICBpZiAoJHByb2R1Y3RBbW91bnQudmFsKCkgPT09ICcnIHx8IGFtb3VudFZhbHVlID09PSAwIHx8IGFtb3VudFZhbHVlID4gZ3Vlc3NlZEFtb3VudCkge1xuICAgICAgJHByb2R1Y3RBbW91bnQudmFsKGd1ZXNzZWRBbW91bnQpO1xuICAgIH1cbiAgfVxuXG4gIGdldFJlZnVuZEFtb3VudCgpIHtcbiAgICBsZXQgdG90YWxBbW91bnQgPSAwO1xuXG4gICAgaWYgKHRoaXMudXNlQW1vdW50SW5wdXRzKSB7XG4gICAgICAkKE9yZGVyVmlld1BhZ2VNYXAuY2FuY2VsUHJvZHVjdC5pbnB1dHMuYW1vdW50KS5lYWNoKChpbmRleCwgYW1vdW50KSA9PiB7XG4gICAgICAgIGNvbnN0IGZsb2F0VmFsdWUgPSBwYXJzZUZsb2F0KGFtb3VudC52YWx1ZSk7XG4gICAgICAgIHRvdGFsQW1vdW50ICs9ICFOdW1iZXIuaXNOYU4oZmxvYXRWYWx1ZSkgPyBmbG9hdFZhbHVlIDogMDtcbiAgICAgIH0pO1xuICAgIH0gZWxzZSB7XG4gICAgICAkKE9yZGVyVmlld1BhZ2VNYXAuY2FuY2VsUHJvZHVjdC5pbnB1dHMucXVhbnRpdHkpLmVhY2goKGluZGV4LCBxdWFudGl0eSkgPT4ge1xuICAgICAgICBjb25zdCAkcXVhbnRpdHlJbnB1dCA9ICQocXVhbnRpdHkpO1xuICAgICAgICBjb25zdCBwcmljZUZpZWxkTmFtZSA9IHRoaXMuaXNUYXhJbmNsdWRlZCA/ICdwcm9kdWN0UHJpY2VUYXhJbmNsJyA6ICdwcm9kdWN0UHJpY2VUYXhFeGNsJztcbiAgICAgICAgY29uc3QgcHJvZHVjdFVuaXRQcmljZSA9IHBhcnNlRmxvYXQoJHF1YW50aXR5SW5wdXQuZGF0YShwcmljZUZpZWxkTmFtZSkpO1xuICAgICAgICBjb25zdCBwcm9kdWN0UXVhbnRpdHkgPSBwYXJzZUludCgkcXVhbnRpdHlJbnB1dC52YWwoKSwgMTApO1xuICAgICAgICB0b3RhbEFtb3VudCArPSBwcm9kdWN0UXVhbnRpdHkgKiBwcm9kdWN0VW5pdFByaWNlO1xuICAgICAgfSk7XG4gICAgfVxuXG4gICAgcmV0dXJuIHRvdGFsQW1vdW50O1xuICB9XG5cbiAgdXBkYXRlVm91Y2hlclJlZnVuZCgpIHtcbiAgICBjb25zdCByZWZ1bmRBbW91bnQgPSB0aGlzLmdldFJlZnVuZEFtb3VudCgpO1xuXG4gICAgdGhpcy51cGRhdGVWb3VjaGVyUmVmdW5kVHlwZUxhYmVsKFxuICAgICAgJChPcmRlclZpZXdQYWdlTWFwLmNhbmNlbFByb2R1Y3QucmFkaW9zLnZvdWNoZXJSZWZ1bmRUeXBlLnByb2R1Y3RQcmljZXMpLFxuICAgICAgcmVmdW5kQW1vdW50LFxuICAgICk7XG4gICAgY29uc3QgcmVmdW5kVm91Y2hlckV4Y2x1ZGVkID0gcmVmdW5kQW1vdW50IC0gdGhpcy5kaXNjb3VudHNBbW91bnQ7XG4gICAgdGhpcy51cGRhdGVWb3VjaGVyUmVmdW5kVHlwZUxhYmVsKFxuICAgICAgJChPcmRlclZpZXdQYWdlTWFwLmNhbmNlbFByb2R1Y3QucmFkaW9zLnZvdWNoZXJSZWZ1bmRUeXBlLnByb2R1Y3RQcmljZXNWb3VjaGVyRXhjbHVkZWQpLFxuICAgICAgcmVmdW5kVm91Y2hlckV4Y2x1ZGVkLFxuICAgICk7XG5cbiAgICAvLyBEaXNhYmxlIHZvdWNoZXIgZXhjbHVkZWQgb3B0aW9uIHdoZW4gdGhlIHZvdWNoZXIgYW1vdW50IGlzIHRvbyBoaWdoXG4gICAgaWYgKHJlZnVuZFZvdWNoZXJFeGNsdWRlZCA8IDApIHtcbiAgICAgICQoT3JkZXJWaWV3UGFnZU1hcC5jYW5jZWxQcm9kdWN0LnJhZGlvcy52b3VjaGVyUmVmdW5kVHlwZS5wcm9kdWN0UHJpY2VzVm91Y2hlckV4Y2x1ZGVkKVxuICAgICAgICAucHJvcCgnY2hlY2tlZCcsIGZhbHNlKVxuICAgICAgICAucHJvcCgnZGlzYWJsZWQnLCB0cnVlKTtcbiAgICAgICQoT3JkZXJWaWV3UGFnZU1hcC5jYW5jZWxQcm9kdWN0LnJhZGlvcy52b3VjaGVyUmVmdW5kVHlwZS5wcm9kdWN0UHJpY2VzKS5wcm9wKFxuICAgICAgICAnY2hlY2tlZCcsXG4gICAgICAgIHRydWUsXG4gICAgICApO1xuICAgICAgJChPcmRlclZpZXdQYWdlTWFwLmNhbmNlbFByb2R1Y3QucmFkaW9zLnZvdWNoZXJSZWZ1bmRUeXBlLm5lZ2F0aXZlRXJyb3JNZXNzYWdlKS5zaG93KCk7XG4gICAgfSBlbHNlIHtcbiAgICAgICQoT3JkZXJWaWV3UGFnZU1hcC5jYW5jZWxQcm9kdWN0LnJhZGlvcy52b3VjaGVyUmVmdW5kVHlwZS5wcm9kdWN0UHJpY2VzVm91Y2hlckV4Y2x1ZGVkKS5wcm9wKFxuICAgICAgICAnZGlzYWJsZWQnLFxuICAgICAgICBmYWxzZSxcbiAgICAgICk7XG4gICAgICAkKE9yZGVyVmlld1BhZ2VNYXAuY2FuY2VsUHJvZHVjdC5yYWRpb3Mudm91Y2hlclJlZnVuZFR5cGUubmVnYXRpdmVFcnJvck1lc3NhZ2UpLmhpZGUoKTtcbiAgICB9XG4gIH1cblxuICB1cGRhdGVWb3VjaGVyUmVmdW5kVHlwZUxhYmVsKCRpbnB1dCwgcmVmdW5kQW1vdW50KSB7XG4gICAgY29uc3QgZGVmYXVsdExhYmVsID0gJGlucHV0LmRhdGEoJ2RlZmF1bHRMYWJlbCcpO1xuICAgIGNvbnN0ICRsYWJlbCA9ICRpbnB1dC5wYXJlbnRzKCdsYWJlbCcpO1xuICAgIGNvbnN0IGZvcm1hdHRlZEFtb3VudCA9IHRoaXMuY3VycmVuY3lGb3JtYXR0ZXIuZm9ybWF0KHJlZnVuZEFtb3VudCk7XG5cbiAgICAvLyBDaGFuZ2UgdGhlIGVuZGluZyB0ZXh0IHBhcnQgb25seSB0byBhdm9pZCByZW1vdmluZyB0aGUgaW5wdXQgKHRoZSBFT0wgaXMgb24gcHVycG9zZSBmb3IgYmV0dGVyIGRpc3BsYXkpXG4gICAgJGxhYmVsLmdldCgwKS5sYXN0Q2hpbGQubm9kZVZhbHVlID0gYFxuICAgICR7ZGVmYXVsdExhYmVsfSAke2Zvcm1hdHRlZEFtb3VudH1gO1xuICB9XG5cbiAgc2hvd0NhbmNlbFByb2R1Y3RGb3JtKCkge1xuICAgIGNvbnN0IGNhbmNlbFByb2R1Y3RSb3V0ZSA9IHRoaXMucm91dGVyLmdlbmVyYXRlKCdhZG1pbl9vcmRlcnNfY2FuY2VsbGF0aW9uJywge29yZGVySWQ6IHRoaXMub3JkZXJJZH0pO1xuICAgIHRoaXMuaW5pdEZvcm0oXG4gICAgICAkKE9yZGVyVmlld1BhZ2VNYXAuY2FuY2VsUHJvZHVjdC5idXR0b25zLnNhdmUpLmRhdGEoJ2NhbmNlbExhYmVsJyksXG4gICAgICBjYW5jZWxQcm9kdWN0Um91dGUsXG4gICAgICAnY2FuY2VsLXByb2R1Y3QnLFxuICAgICk7XG4gICAgdGhpcy5oaWRlQ2FuY2VsRWxlbWVudHMoKTtcbiAgICAkKE9yZGVyVmlld1BhZ2VNYXAuY2FuY2VsUHJvZHVjdC50b2dnbGUuY2FuY2VsUHJvZHVjdHMpLnNob3coKTtcbiAgfVxufVxuIiwiLyoqXG4gKiBDb3B5cmlnaHQgc2luY2UgMjAwNyBQcmVzdGFTaG9wIFNBIGFuZCBDb250cmlidXRvcnNcbiAqIFByZXN0YVNob3AgaXMgYW4gSW50ZXJuYXRpb25hbCBSZWdpc3RlcmVkIFRyYWRlbWFyayAmIFByb3BlcnR5IG9mIFByZXN0YVNob3AgU0FcbiAqXG4gKiBOT1RJQ0UgT0YgTElDRU5TRVxuICpcbiAqIFRoaXMgc291cmNlIGZpbGUgaXMgc3ViamVjdCB0byB0aGUgT3BlbiBTb2Z0d2FyZSBMaWNlbnNlIChPU0wgMy4wKVxuICogdGhhdCBpcyBidW5kbGVkIHdpdGggdGhpcyBwYWNrYWdlIGluIHRoZSBmaWxlIExJQ0VOU0UubWQuXG4gKiBJdCBpcyBhbHNvIGF2YWlsYWJsZSB0aHJvdWdoIHRoZSB3b3JsZC13aWRlLXdlYiBhdCB0aGlzIFVSTDpcbiAqIGh0dHBzOi8vb3BlbnNvdXJjZS5vcmcvbGljZW5zZXMvT1NMLTMuMFxuICogSWYgeW91IGRpZCBub3QgcmVjZWl2ZSBhIGNvcHkgb2YgdGhlIGxpY2Vuc2UgYW5kIGFyZSB1bmFibGUgdG9cbiAqIG9idGFpbiBpdCB0aHJvdWdoIHRoZSB3b3JsZC13aWRlLXdlYiwgcGxlYXNlIHNlbmQgYW4gZW1haWxcbiAqIHRvIGxpY2Vuc2VAcHJlc3Rhc2hvcC5jb20gc28gd2UgY2FuIHNlbmQgeW91IGEgY29weSBpbW1lZGlhdGVseS5cbiAqXG4gKiBESVNDTEFJTUVSXG4gKlxuICogRG8gbm90IGVkaXQgb3IgYWRkIHRvIHRoaXMgZmlsZSBpZiB5b3Ugd2lzaCB0byB1cGdyYWRlIFByZXN0YVNob3AgdG8gbmV3ZXJcbiAqIHZlcnNpb25zIGluIHRoZSBmdXR1cmUuIElmIHlvdSB3aXNoIHRvIGN1c3RvbWl6ZSBQcmVzdGFTaG9wIGZvciB5b3VyXG4gKiBuZWVkcyBwbGVhc2UgcmVmZXIgdG8gaHR0cHM6Ly9kZXZkb2NzLnByZXN0YXNob3AuY29tLyBmb3IgbW9yZSBpbmZvcm1hdGlvbi5cbiAqXG4gKiBAYXV0aG9yICAgIFByZXN0YVNob3AgU0EgYW5kIENvbnRyaWJ1dG9ycyA8Y29udGFjdEBwcmVzdGFzaG9wLmNvbT5cbiAqIEBjb3B5cmlnaHQgU2luY2UgMjAwNyBQcmVzdGFTaG9wIFNBIGFuZCBDb250cmlidXRvcnNcbiAqIEBsaWNlbnNlICAgaHR0cHM6Ly9vcGVuc291cmNlLm9yZy9saWNlbnNlcy9PU0wtMy4wIE9wZW4gU29mdHdhcmUgTGljZW5zZSAoT1NMIDMuMClcbiAqL1xuXG5pbXBvcnQgUm91dGVyIGZyb20gJ0Bjb21wb25lbnRzL3JvdXRlcic7XG5pbXBvcnQgT3JkZXJWaWV3UGFnZU1hcCBmcm9tICdAcGFnZXMvb3JkZXIvT3JkZXJWaWV3UGFnZU1hcCc7XG5pbXBvcnQge0V2ZW50RW1pdHRlcn0gZnJvbSAnQGNvbXBvbmVudHMvZXZlbnQtZW1pdHRlcic7XG5pbXBvcnQgT3JkZXJWaWV3RXZlbnRNYXAgZnJvbSAnQHBhZ2VzL29yZGVyL3ZpZXcvb3JkZXItdmlldy1ldmVudC1tYXAnO1xuaW1wb3J0IE9yZGVyUHJpY2VzIGZyb20gJ0BwYWdlcy9vcmRlci92aWV3L29yZGVyLXByaWNlcyc7XG5pbXBvcnQgQ29uZmlybU1vZGFsIGZyb20gJ0Bjb21wb25lbnRzL21vZGFsJztcbmltcG9ydCBPcmRlclByaWNlc1JlZnJlc2hlciBmcm9tICdAcGFnZXMvb3JkZXIvdmlldy9vcmRlci1wcmljZXMtcmVmcmVzaGVyJztcblxuY29uc3QgeyR9ID0gd2luZG93O1xuXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBPcmRlclByb2R1Y3RFZGl0IHtcbiAgY29uc3RydWN0b3Iob3JkZXJEZXRhaWxJZCkge1xuICAgIHRoaXMucm91dGVyID0gbmV3IFJvdXRlcigpO1xuICAgIHRoaXMub3JkZXJEZXRhaWxJZCA9IG9yZGVyRGV0YWlsSWQ7XG4gICAgdGhpcy5wcm9kdWN0Um93ID0gJChgI29yZGVyUHJvZHVjdF8ke3RoaXMub3JkZXJEZXRhaWxJZH1gKTtcbiAgICB0aGlzLnByb2R1Y3QgPSB7fTtcbiAgICB0aGlzLmN1cnJlbmN5UHJlY2lzaW9uID0gJChPcmRlclZpZXdQYWdlTWFwLnByb2R1Y3RzVGFibGUpLmRhdGEoJ2N1cnJlbmN5UHJlY2lzaW9uJyk7XG4gICAgdGhpcy5wcmljZVRheENhbGN1bGF0b3IgPSBuZXcgT3JkZXJQcmljZXMoKTtcbiAgICB0aGlzLnByb2R1Y3RFZGl0U2F2ZUJ0biA9ICQoT3JkZXJWaWV3UGFnZU1hcC5wcm9kdWN0RWRpdFNhdmVCdG4pO1xuICAgIHRoaXMucXVhbnRpdHlJbnB1dCA9ICQoT3JkZXJWaWV3UGFnZU1hcC5wcm9kdWN0RWRpdFF1YW50aXR5SW5wdXQpO1xuICAgIHRoaXMub3JkZXJQcmljZXNSZWZyZXNoZXIgPSBuZXcgT3JkZXJQcmljZXNSZWZyZXNoZXIoKTtcbiAgfVxuXG4gIHNldHVwTGlzdGVuZXIoKSB7XG4gICAgdGhpcy5xdWFudGl0eUlucHV0Lm9uKCdjaGFuZ2Uga2V5dXAnLCAoZXZlbnQpID0+IHtcbiAgICAgIGNvbnN0IG5ld1F1YW50aXR5ID0gTnVtYmVyKGV2ZW50LnRhcmdldC52YWx1ZSk7XG4gICAgICBjb25zdCBhdmFpbGFibGVRdWFudGl0eSA9IHBhcnNlSW50KCQoZXZlbnQuY3VycmVudFRhcmdldCkuZGF0YSgnYXZhaWxhYmxlUXVhbnRpdHknKSwgMTApO1xuICAgICAgY29uc3QgcHJldmlvdXNRdWFudGl0eSA9IHBhcnNlSW50KHRoaXMucXVhbnRpdHlJbnB1dC5kYXRhKCdwcmV2aW91c1F1YW50aXR5JyksIDEwKTtcbiAgICAgIGNvbnN0IHJlbWFpbmluZ0F2YWlsYWJsZSA9IGF2YWlsYWJsZVF1YW50aXR5IC0gKG5ld1F1YW50aXR5IC0gcHJldmlvdXNRdWFudGl0eSk7XG4gICAgICBjb25zdCBhdmFpbGFibGVPdXRPZlN0b2NrID0gdGhpcy5hdmFpbGFibGVUZXh0LmRhdGEoJ2F2YWlsYWJsZU91dE9mU3RvY2snKTtcbiAgICAgIHRoaXMucXVhbnRpdHkgPSBuZXdRdWFudGl0eTtcbiAgICAgIHRoaXMuYXZhaWxhYmxlVGV4dC50ZXh0KHJlbWFpbmluZ0F2YWlsYWJsZSk7XG4gICAgICB0aGlzLmF2YWlsYWJsZVRleHQudG9nZ2xlQ2xhc3MoJ3RleHQtZGFuZ2VyIGZvbnQtd2VpZ2h0LWJvbGQnLCByZW1haW5pbmdBdmFpbGFibGUgPCAwKTtcbiAgICAgIHRoaXMudXBkYXRlVG90YWwoKTtcbiAgICAgIGNvbnN0IGRpc2FibGVFZGl0QWN0aW9uQnRuID0gbmV3UXVhbnRpdHkgPD0gMCB8fCAocmVtYWluaW5nQXZhaWxhYmxlIDwgMCAmJiAhYXZhaWxhYmxlT3V0T2ZTdG9jayk7XG4gICAgICB0aGlzLnByb2R1Y3RFZGl0U2F2ZUJ0bi5wcm9wKCdkaXNhYmxlZCcsIGRpc2FibGVFZGl0QWN0aW9uQnRuKTtcbiAgICB9KTtcblxuICAgIHRoaXMucHJvZHVjdEVkaXRJbnZvaWNlU2VsZWN0Lm9uKCdjaGFuZ2UnLCAoKSA9PiB7XG4gICAgICB0aGlzLnByb2R1Y3RFZGl0U2F2ZUJ0bi5wcm9wKCdkaXNhYmxlZCcsIGZhbHNlKTtcbiAgICB9KTtcblxuICAgIHRoaXMucHJpY2VUYXhJbmNsdWRlZElucHV0Lm9uKCdjaGFuZ2Uga2V5dXAnLCAoZXZlbnQpID0+IHtcbiAgICAgIHRoaXMudGF4SW5jbHVkZWQgPSBwYXJzZUZsb2F0KGV2ZW50LnRhcmdldC52YWx1ZSk7XG4gICAgICB0aGlzLnRheEV4Y2x1ZGVkID0gdGhpcy5wcmljZVRheENhbGN1bGF0b3IuY2FsY3VsYXRlVGF4RXhjbHVkZWQoXG4gICAgICAgIHRoaXMudGF4SW5jbHVkZWQsXG4gICAgICAgIHRoaXMudGF4UmF0ZSxcbiAgICAgICAgdGhpcy5jdXJyZW5jeVByZWNpc2lvbixcbiAgICAgICk7XG4gICAgICB0aGlzLnByaWNlVGF4RXhjbHVkZWRJbnB1dC52YWwodGhpcy50YXhFeGNsdWRlZCk7XG4gICAgICB0aGlzLnVwZGF0ZVRvdGFsKCk7XG4gICAgfSk7XG5cbiAgICB0aGlzLnByaWNlVGF4RXhjbHVkZWRJbnB1dC5vbignY2hhbmdlIGtleXVwJywgKGV2ZW50KSA9PiB7XG4gICAgICB0aGlzLnRheEV4Y2x1ZGVkID0gcGFyc2VGbG9hdChldmVudC50YXJnZXQudmFsdWUpO1xuICAgICAgdGhpcy50YXhJbmNsdWRlZCA9IHRoaXMucHJpY2VUYXhDYWxjdWxhdG9yLmNhbGN1bGF0ZVRheEluY2x1ZGVkKFxuICAgICAgICB0aGlzLnRheEV4Y2x1ZGVkLFxuICAgICAgICB0aGlzLnRheFJhdGUsXG4gICAgICAgIHRoaXMuY3VycmVuY3lQcmVjaXNpb24sXG4gICAgICApO1xuICAgICAgdGhpcy5wcmljZVRheEluY2x1ZGVkSW5wdXQudmFsKHRoaXMudGF4SW5jbHVkZWQpO1xuICAgICAgdGhpcy51cGRhdGVUb3RhbCgpO1xuICAgIH0pO1xuXG4gICAgdGhpcy5wcm9kdWN0RWRpdFNhdmVCdG4ub24oJ2NsaWNrJywgKGV2ZW50KSA9PiB7XG4gICAgICBjb25zdCAkYnRuID0gJChldmVudC5jdXJyZW50VGFyZ2V0KTtcbiAgICAgIGNvbnN0IGNvbmZpcm1lZCA9IHdpbmRvdy5jb25maXJtKCRidG4uZGF0YSgndXBkYXRlTWVzc2FnZScpKTtcblxuICAgICAgaWYgKCFjb25maXJtZWQpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICAkYnRuLnByb3AoJ2Rpc2FibGVkJywgdHJ1ZSk7XG4gICAgICB0aGlzLmhhbmRsZUVkaXRQcm9kdWN0V2l0aENvbmZpcm1hdGlvbk1vZGFsKGV2ZW50KTtcbiAgICB9KTtcblxuICAgIHRoaXMucHJvZHVjdEVkaXRDYW5jZWxCdG4ub24oJ2NsaWNrJywgKCkgPT4ge1xuICAgICAgRXZlbnRFbWl0dGVyLmVtaXQoT3JkZXJWaWV3RXZlbnRNYXAucHJvZHVjdEVkaXRpb25DYW5jZWxlZCwge1xuICAgICAgICBvcmRlckRldGFpbElkOiB0aGlzLm9yZGVyRGV0YWlsSWQsXG4gICAgICB9KTtcbiAgICB9KTtcbiAgfVxuXG4gIHVwZGF0ZVRvdGFsKCkge1xuICAgIGNvbnN0IHVwZGF0ZWRUb3RhbCA9IHRoaXMucHJpY2VUYXhDYWxjdWxhdG9yLmNhbGN1bGF0ZVRvdGFsUHJpY2UoXG4gICAgICB0aGlzLnF1YW50aXR5LFxuICAgICAgdGhpcy5pc09yZGVyVGF4SW5jbHVkZWQgPyB0aGlzLnRheEluY2x1ZGVkIDogdGhpcy50YXhFeGNsdWRlZCxcbiAgICAgIHRoaXMuY3VycmVuY3lQcmVjaXNpb24sXG4gICAgKTtcbiAgICB0aGlzLnByaWNlVG90YWxUZXh0Lmh0bWwodXBkYXRlZFRvdGFsKTtcbiAgICB0aGlzLnByb2R1Y3RFZGl0U2F2ZUJ0bi5wcm9wKCdkaXNhYmxlZCcsIHVwZGF0ZWRUb3RhbCA9PT0gdGhpcy5pbml0aWFsVG90YWwpO1xuICB9XG5cbiAgZGlzcGxheVByb2R1Y3QocHJvZHVjdCkge1xuICAgIHRoaXMucHJvZHVjdFJvd0VkaXQgPSAkKE9yZGVyVmlld1BhZ2VNYXAucHJvZHVjdEVkaXRSb3dUZW1wbGF0ZSkuY2xvbmUodHJ1ZSk7XG4gICAgdGhpcy5wcm9kdWN0Um93RWRpdC5hdHRyKCdpZCcsIGBlZGl0T3JkZXJQcm9kdWN0XyR7dGhpcy5vcmRlckRldGFpbElkfWApO1xuICAgIHRoaXMucHJvZHVjdFJvd0VkaXQuZmluZCgnKltpZF0nKS5lYWNoKGZ1bmN0aW9uIHJlbW92ZUFsbElkcygpIHtcbiAgICAgICQodGhpcykucmVtb3ZlQXR0cignaWQnKTtcbiAgICB9KTtcblxuICAgIC8vIEZpbmQgY29udHJvbHNcbiAgICB0aGlzLnByb2R1Y3RFZGl0U2F2ZUJ0biA9IHRoaXMucHJvZHVjdFJvd0VkaXQuZmluZChPcmRlclZpZXdQYWdlTWFwLnByb2R1Y3RFZGl0U2F2ZUJ0bik7XG4gICAgdGhpcy5wcm9kdWN0RWRpdENhbmNlbEJ0biA9IHRoaXMucHJvZHVjdFJvd0VkaXQuZmluZChPcmRlclZpZXdQYWdlTWFwLnByb2R1Y3RFZGl0Q2FuY2VsQnRuKTtcbiAgICB0aGlzLnByb2R1Y3RFZGl0SW52b2ljZVNlbGVjdCA9IHRoaXMucHJvZHVjdFJvd0VkaXQuZmluZChPcmRlclZpZXdQYWdlTWFwLnByb2R1Y3RFZGl0SW52b2ljZVNlbGVjdCk7XG4gICAgdGhpcy5wcm9kdWN0RWRpdEltYWdlID0gdGhpcy5wcm9kdWN0Um93RWRpdC5maW5kKE9yZGVyVmlld1BhZ2VNYXAucHJvZHVjdEVkaXRJbWFnZSk7XG4gICAgdGhpcy5wcm9kdWN0RWRpdE5hbWUgPSB0aGlzLnByb2R1Y3RSb3dFZGl0LmZpbmQoT3JkZXJWaWV3UGFnZU1hcC5wcm9kdWN0RWRpdE5hbWUpO1xuICAgIHRoaXMucHJpY2VUYXhJbmNsdWRlZElucHV0ID0gdGhpcy5wcm9kdWN0Um93RWRpdC5maW5kKE9yZGVyVmlld1BhZ2VNYXAucHJvZHVjdEVkaXRQcmljZVRheEluY2xJbnB1dCk7XG4gICAgdGhpcy5wcmljZVRheEV4Y2x1ZGVkSW5wdXQgPSB0aGlzLnByb2R1Y3RSb3dFZGl0LmZpbmQoT3JkZXJWaWV3UGFnZU1hcC5wcm9kdWN0RWRpdFByaWNlVGF4RXhjbElucHV0KTtcbiAgICB0aGlzLnF1YW50aXR5SW5wdXQgPSB0aGlzLnByb2R1Y3RSb3dFZGl0LmZpbmQoT3JkZXJWaWV3UGFnZU1hcC5wcm9kdWN0RWRpdFF1YW50aXR5SW5wdXQpO1xuICAgIHRoaXMubG9jYXRpb25UZXh0ID0gdGhpcy5wcm9kdWN0Um93RWRpdC5maW5kKE9yZGVyVmlld1BhZ2VNYXAucHJvZHVjdEVkaXRMb2NhdGlvblRleHQpO1xuICAgIHRoaXMuYXZhaWxhYmxlVGV4dCA9IHRoaXMucHJvZHVjdFJvd0VkaXQuZmluZChPcmRlclZpZXdQYWdlTWFwLnByb2R1Y3RFZGl0QXZhaWxhYmxlVGV4dCk7XG4gICAgdGhpcy5wcmljZVRvdGFsVGV4dCA9IHRoaXMucHJvZHVjdFJvd0VkaXQuZmluZChPcmRlclZpZXdQYWdlTWFwLnByb2R1Y3RFZGl0VG90YWxQcmljZVRleHQpO1xuXG4gICAgLy8gSW5pdCBpbnB1dCB2YWx1ZXNcbiAgICB0aGlzLnByaWNlVGF4RXhjbHVkZWRJbnB1dC52YWwoXG4gICAgICB3aW5kb3cucHNfcm91bmQocHJvZHVjdC5wcmljZV90YXhfZXhjbCwgdGhpcy5jdXJyZW5jeVByZWNpc2lvbiksXG4gICAgKTtcbiAgICB0aGlzLnByaWNlVGF4SW5jbHVkZWRJbnB1dC52YWwoXG4gICAgICB3aW5kb3cucHNfcm91bmQocHJvZHVjdC5wcmljZV90YXhfaW5jbCwgdGhpcy5jdXJyZW5jeVByZWNpc2lvbiksXG4gICAgKTtcbiAgICB0aGlzLnF1YW50aXR5SW5wdXQudmFsKHByb2R1Y3QucXVhbnRpdHkpXG4gICAgICAuZGF0YSgnYXZhaWxhYmxlUXVhbnRpdHknLCBwcm9kdWN0LmF2YWlsYWJsZVF1YW50aXR5KVxuICAgICAgLmRhdGEoJ3ByZXZpb3VzUXVhbnRpdHknLCBwcm9kdWN0LnF1YW50aXR5KTtcbiAgICB0aGlzLmF2YWlsYWJsZVRleHQuZGF0YSgnYXZhaWxhYmxlT3V0T2ZTdG9jaycsIHByb2R1Y3QuYXZhaWxhYmxlT3V0T2ZTdG9jayk7XG5cbiAgICAvLyBzZXQgdGhpcyBwcm9kdWN0J3Mgb3JkZXJJbnZvaWNlSWQgYXMgc2VsZWN0ZWRcbiAgICBpZiAocHJvZHVjdC5vcmRlckludm9pY2VJZCkge1xuICAgICAgdGhpcy5wcm9kdWN0RWRpdEludm9pY2VTZWxlY3QudmFsKHByb2R1Y3Qub3JkZXJJbnZvaWNlSWQpO1xuICAgIH1cblxuICAgIC8vIEluaXQgZWRpdG9yIGRhdGFcbiAgICB0aGlzLnRheFJhdGUgPSBwcm9kdWN0LnRheF9yYXRlO1xuICAgIHRoaXMuaW5pdGlhbFRvdGFsID0gdGhpcy5wcmljZVRheENhbGN1bGF0b3IuY2FsY3VsYXRlVG90YWxQcmljZShcbiAgICAgIHByb2R1Y3QucXVhbnRpdHksXG4gICAgICBwcm9kdWN0LmlzT3JkZXJUYXhJbmNsdWRlZCA/IHByb2R1Y3QucHJpY2VfdGF4X2luY2wgOiBwcm9kdWN0LnByaWNlX3RheF9leGNsLFxuICAgICAgdGhpcy5jdXJyZW5jeVByZWNpc2lvbixcbiAgICApO1xuICAgIHRoaXMuaXNPcmRlclRheEluY2x1ZGVkID0gcHJvZHVjdC5pc09yZGVyVGF4SW5jbHVkZWQ7XG4gICAgdGhpcy5xdWFudGl0eSA9IHByb2R1Y3QucXVhbnRpdHk7XG4gICAgdGhpcy50YXhJbmNsdWRlZCA9IHByb2R1Y3QucHJpY2VfdGF4X2luY2w7XG4gICAgdGhpcy50YXhFeGNsdWRlZCA9IHByb2R1Y3QucHJpY2VfdGF4X2V4Y2w7XG5cbiAgICAvLyBDb3B5IHByb2R1Y3QgY29udGVudCBpbiBjZWxsc1xuICAgIHRoaXMucHJvZHVjdEVkaXRJbWFnZS5odG1sKFxuICAgICAgdGhpcy5wcm9kdWN0Um93LmZpbmQoT3JkZXJWaWV3UGFnZU1hcC5wcm9kdWN0RWRpdEltYWdlKS5odG1sKCksXG4gICAgKTtcbiAgICB0aGlzLnByb2R1Y3RFZGl0TmFtZS5odG1sKFxuICAgICAgdGhpcy5wcm9kdWN0Um93LmZpbmQoT3JkZXJWaWV3UGFnZU1hcC5wcm9kdWN0RWRpdE5hbWUpLmh0bWwoKSxcbiAgICApO1xuICAgIHRoaXMubG9jYXRpb25UZXh0Lmh0bWwocHJvZHVjdC5sb2NhdGlvbik7XG4gICAgdGhpcy5hdmFpbGFibGVUZXh0Lmh0bWwocHJvZHVjdC5hdmFpbGFibGVRdWFudGl0eSk7XG4gICAgdGhpcy5wcmljZVRvdGFsVGV4dC5odG1sKHRoaXMuaW5pdGlhbFRvdGFsKTtcbiAgICB0aGlzLnByb2R1Y3RSb3cuYWRkQ2xhc3MoJ2Qtbm9uZScpLmFmdGVyKHRoaXMucHJvZHVjdFJvd0VkaXQucmVtb3ZlQ2xhc3MoJ2Qtbm9uZScpKTtcblxuICAgIHRoaXMuc2V0dXBMaXN0ZW5lcigpO1xuICB9XG5cbiAgaGFuZGxlRWRpdFByb2R1Y3RXaXRoQ29uZmlybWF0aW9uTW9kYWwoZXZlbnQpIHtcbiAgICBjb25zdCBwcm9kdWN0RWRpdEJ0biA9ICQoYCNvcmRlclByb2R1Y3RfJHt0aGlzLm9yZGVyRGV0YWlsSWR9ICR7T3JkZXJWaWV3UGFnZU1hcC5wcm9kdWN0RWRpdEJ1dHRvbnN9YCk7XG4gICAgY29uc3QgcHJvZHVjdElkID0gcHJvZHVjdEVkaXRCdG4uZGF0YSgncHJvZHVjdC1pZCcpO1xuICAgIGNvbnN0IGNvbWJpbmF0aW9uSWQgPSBwcm9kdWN0RWRpdEJ0bi5kYXRhKCdjb21iaW5hdGlvbi1pZCcpO1xuICAgIGNvbnN0IG9yZGVySW52b2ljZUlkID0gcHJvZHVjdEVkaXRCdG4uZGF0YSgnb3JkZXItaW52b2ljZS1pZCcpO1xuICAgIGNvbnN0IHByb2R1Y3RQcmljZU1hdGNoID0gdGhpcy5vcmRlclByaWNlc1JlZnJlc2hlci5jaGVja090aGVyUHJvZHVjdFByaWNlc01hdGNoKFxuICAgICAgdGhpcy5wcmljZVRheEluY2x1ZGVkSW5wdXQudmFsKCksXG4gICAgICBwcm9kdWN0SWQsXG4gICAgICBjb21iaW5hdGlvbklkLFxuICAgICAgb3JkZXJJbnZvaWNlSWQsXG4gICAgICB0aGlzLm9yZGVyRGV0YWlsSWQsXG4gICAgKTtcblxuICAgIGlmIChwcm9kdWN0UHJpY2VNYXRjaCA9PT0gbnVsbCkge1xuICAgICAgdGhpcy5lZGl0UHJvZHVjdCgkKGV2ZW50LmN1cnJlbnRUYXJnZXQpLmRhdGEoJ29yZGVySWQnKSwgdGhpcy5vcmRlckRldGFpbElkKTtcblxuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIGNvbnN0IGRhdGFTZWxlY3RvciA9IHByb2R1Y3RQcmljZU1hdGNoID09PSAncHJvZHVjdCcgPyB0aGlzLnByaWNlVGF4RXhjbHVkZWRJbnB1dCA6IHRoaXMucHJvZHVjdEVkaXRJbnZvaWNlU2VsZWN0O1xuXG4gICAgY29uc3QgbW9kYWxFZGl0UHJpY2UgPSBuZXcgQ29uZmlybU1vZGFsKFxuICAgICAge1xuICAgICAgICBpZDogJ21vZGFsLWNvbmZpcm0tbmV3LXByaWNlJyxcbiAgICAgICAgY29uZmlybVRpdGxlOiBkYXRhU2VsZWN0b3IuZGF0YSgnbW9kYWwtZWRpdC1wcmljZS10aXRsZScpLFxuICAgICAgICBjb25maXJtTWVzc2FnZTogZGF0YVNlbGVjdG9yLmRhdGEoJ21vZGFsLWVkaXQtcHJpY2UtYm9keScpLFxuICAgICAgICBjb25maXJtQnV0dG9uTGFiZWw6IGRhdGFTZWxlY3Rvci5kYXRhKCdtb2RhbC1lZGl0LXByaWNlLWFwcGx5JyksXG4gICAgICAgIGNsb3NlQnV0dG9uTGFiZWw6IGRhdGFTZWxlY3Rvci5kYXRhKCdtb2RhbC1lZGl0LXByaWNlLWNhbmNlbCcpLFxuICAgICAgfSxcbiAgICAgICgpID0+IHtcbiAgICAgICAgdGhpcy5lZGl0UHJvZHVjdCgkKGV2ZW50LmN1cnJlbnRUYXJnZXQpLmRhdGEoJ29yZGVySWQnKSwgdGhpcy5vcmRlckRldGFpbElkKTtcbiAgICAgIH0sXG4gICAgKTtcblxuICAgIG1vZGFsRWRpdFByaWNlLnNob3coKTtcbiAgfVxuXG4gIGVkaXRQcm9kdWN0KG9yZGVySWQsIG9yZGVyRGV0YWlsSWQpIHtcbiAgICBjb25zdCBwYXJhbXMgPSB7XG4gICAgICBwcmljZV90YXhfaW5jbDogdGhpcy5wcmljZVRheEluY2x1ZGVkSW5wdXQudmFsKCksXG4gICAgICBwcmljZV90YXhfZXhjbDogdGhpcy5wcmljZVRheEV4Y2x1ZGVkSW5wdXQudmFsKCksXG4gICAgICBxdWFudGl0eTogdGhpcy5xdWFudGl0eUlucHV0LnZhbCgpLFxuICAgICAgaW52b2ljZTogdGhpcy5wcm9kdWN0RWRpdEludm9pY2VTZWxlY3QudmFsKCksXG4gICAgfTtcblxuICAgICQuYWpheCh7XG4gICAgICB1cmw6IHRoaXMucm91dGVyLmdlbmVyYXRlKCdhZG1pbl9vcmRlcnNfdXBkYXRlX3Byb2R1Y3QnLCB7XG4gICAgICAgIG9yZGVySWQsXG4gICAgICAgIG9yZGVyRGV0YWlsSWQsXG4gICAgICB9KSxcbiAgICAgIG1ldGhvZDogJ1BPU1QnLFxuICAgICAgZGF0YTogcGFyYW1zLFxuICAgIH0pLnRoZW4oXG4gICAgICAoKSA9PiB7XG4gICAgICAgIEV2ZW50RW1pdHRlci5lbWl0KE9yZGVyVmlld0V2ZW50TWFwLnByb2R1Y3RVcGRhdGVkLCB7XG4gICAgICAgICAgb3JkZXJJZCxcbiAgICAgICAgICBvcmRlckRldGFpbElkLFxuICAgICAgICB9KTtcbiAgICAgIH0sXG4gICAgICAocmVzcG9uc2UpID0+IHtcbiAgICAgICAgaWYgKHJlc3BvbnNlLnJlc3BvbnNlSlNPTiAmJiByZXNwb25zZS5yZXNwb25zZUpTT04ubWVzc2FnZSkge1xuICAgICAgICAgICQuZ3Jvd2wuZXJyb3Ioe21lc3NhZ2U6IHJlc3BvbnNlLnJlc3BvbnNlSlNPTi5tZXNzYWdlfSk7XG4gICAgICAgIH1cbiAgICAgIH0sXG4gICAgKTtcbiAgfVxufVxuIiwiLyoqXG4gKiBDb3B5cmlnaHQgc2luY2UgMjAwNyBQcmVzdGFTaG9wIFNBIGFuZCBDb250cmlidXRvcnNcbiAqIFByZXN0YVNob3AgaXMgYW4gSW50ZXJuYXRpb25hbCBSZWdpc3RlcmVkIFRyYWRlbWFyayAmIFByb3BlcnR5IG9mIFByZXN0YVNob3AgU0FcbiAqXG4gKiBOT1RJQ0UgT0YgTElDRU5TRVxuICpcbiAqIFRoaXMgc291cmNlIGZpbGUgaXMgc3ViamVjdCB0byB0aGUgT3BlbiBTb2Z0d2FyZSBMaWNlbnNlIChPU0wgMy4wKVxuICogdGhhdCBpcyBidW5kbGVkIHdpdGggdGhpcyBwYWNrYWdlIGluIHRoZSBmaWxlIExJQ0VOU0UubWQuXG4gKiBJdCBpcyBhbHNvIGF2YWlsYWJsZSB0aHJvdWdoIHRoZSB3b3JsZC13aWRlLXdlYiBhdCB0aGlzIFVSTDpcbiAqIGh0dHBzOi8vb3BlbnNvdXJjZS5vcmcvbGljZW5zZXMvT1NMLTMuMFxuICogSWYgeW91IGRpZCBub3QgcmVjZWl2ZSBhIGNvcHkgb2YgdGhlIGxpY2Vuc2UgYW5kIGFyZSB1bmFibGUgdG9cbiAqIG9idGFpbiBpdCB0aHJvdWdoIHRoZSB3b3JsZC13aWRlLXdlYiwgcGxlYXNlIHNlbmQgYW4gZW1haWxcbiAqIHRvIGxpY2Vuc2VAcHJlc3Rhc2hvcC5jb20gc28gd2UgY2FuIHNlbmQgeW91IGEgY29weSBpbW1lZGlhdGVseS5cbiAqXG4gKiBESVNDTEFJTUVSXG4gKlxuICogRG8gbm90IGVkaXQgb3IgYWRkIHRvIHRoaXMgZmlsZSBpZiB5b3Ugd2lzaCB0byB1cGdyYWRlIFByZXN0YVNob3AgdG8gbmV3ZXJcbiAqIHZlcnNpb25zIGluIHRoZSBmdXR1cmUuIElmIHlvdSB3aXNoIHRvIGN1c3RvbWl6ZSBQcmVzdGFTaG9wIGZvciB5b3VyXG4gKiBuZWVkcyBwbGVhc2UgcmVmZXIgdG8gaHR0cHM6Ly9kZXZkb2NzLnByZXN0YXNob3AuY29tLyBmb3IgbW9yZSBpbmZvcm1hdGlvbi5cbiAqXG4gKiBAYXV0aG9yICAgIFByZXN0YVNob3AgU0EgYW5kIENvbnRyaWJ1dG9ycyA8Y29udGFjdEBwcmVzdGFzaG9wLmNvbT5cbiAqIEBjb3B5cmlnaHQgU2luY2UgMjAwNyBQcmVzdGFTaG9wIFNBIGFuZCBDb250cmlidXRvcnNcbiAqIEBsaWNlbnNlICAgaHR0cHM6Ly9vcGVuc291cmNlLm9yZy9saWNlbnNlcy9PU0wtMy4wIE9wZW4gU29mdHdhcmUgTGljZW5zZSAoT1NMIDMuMClcbiAqL1xuXG5pbXBvcnQgUm91dGVyIGZyb20gJ0Bjb21wb25lbnRzL3JvdXRlcic7XG5pbXBvcnQge0V2ZW50RW1pdHRlcn0gZnJvbSAnQGNvbXBvbmVudHMvZXZlbnQtZW1pdHRlcic7XG5pbXBvcnQgT3JkZXJWaWV3RXZlbnRNYXAgZnJvbSAnQHBhZ2VzL29yZGVyL3ZpZXcvb3JkZXItdmlldy1ldmVudC1tYXAnO1xuXG5jb25zdCB7JH0gPSB3aW5kb3c7XG5cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIE9yZGVyUHJvZHVjdE1hbmFnZXIge1xuICBjb25zdHJ1Y3RvcigpIHtcbiAgICB0aGlzLnJvdXRlciA9IG5ldyBSb3V0ZXIoKTtcbiAgfVxuXG4gIGhhbmRsZURlbGV0ZVByb2R1Y3RFdmVudChldmVudCkge1xuICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG5cbiAgICBjb25zdCAkYnRuID0gJChldmVudC5jdXJyZW50VGFyZ2V0KTtcbiAgICBjb25zdCBjb25maXJtZWQgPSB3aW5kb3cuY29uZmlybSgkYnRuLmRhdGEoJ2RlbGV0ZU1lc3NhZ2UnKSk7XG5cbiAgICBpZiAoIWNvbmZpcm1lZCkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgICRidG4ucHN0b29sdGlwKCdkaXNwb3NlJyk7XG4gICAgJGJ0bi5wcm9wKCdkaXNhYmxlZCcsIHRydWUpO1xuICAgIHRoaXMuZGVsZXRlUHJvZHVjdCgkYnRuLmRhdGEoJ29yZGVySWQnKSwgJGJ0bi5kYXRhKCdvcmRlckRldGFpbElkJykpO1xuICB9XG5cbiAgZGVsZXRlUHJvZHVjdChvcmRlcklkLCBvcmRlckRldGFpbElkKSB7XG4gICAgJC5hamF4KHRoaXMucm91dGVyLmdlbmVyYXRlKCdhZG1pbl9vcmRlcnNfZGVsZXRlX3Byb2R1Y3QnLCB7b3JkZXJJZCwgb3JkZXJEZXRhaWxJZH0pLCB7XG4gICAgICBtZXRob2Q6ICdQT1NUJyxcbiAgICB9KS50aGVuKCgpID0+IHtcbiAgICAgIEV2ZW50RW1pdHRlci5lbWl0KE9yZGVyVmlld0V2ZW50TWFwLnByb2R1Y3REZWxldGVkRnJvbU9yZGVyLCB7XG4gICAgICAgIG9sZE9yZGVyRGV0YWlsSWQ6IG9yZGVyRGV0YWlsSWQsXG4gICAgICAgIG9yZGVySWQsXG4gICAgICB9KTtcbiAgICB9LCAocmVzcG9uc2UpID0+IHtcbiAgICAgIGlmIChyZXNwb25zZS5yZXNwb25zZUpTT04gJiYgcmVzcG9uc2UucmVzcG9uc2VKU09OLm1lc3NhZ2UpIHtcbiAgICAgICAgJC5ncm93bC5lcnJvcih7bWVzc2FnZTogcmVzcG9uc2UucmVzcG9uc2VKU09OLm1lc3NhZ2V9KTtcbiAgICAgIH1cbiAgICB9KTtcbiAgfVxufVxuIiwiLyoqXG4gKiBDb3B5cmlnaHQgc2luY2UgMjAwNyBQcmVzdGFTaG9wIFNBIGFuZCBDb250cmlidXRvcnNcbiAqIFByZXN0YVNob3AgaXMgYW4gSW50ZXJuYXRpb25hbCBSZWdpc3RlcmVkIFRyYWRlbWFyayAmIFByb3BlcnR5IG9mIFByZXN0YVNob3AgU0FcbiAqXG4gKiBOT1RJQ0UgT0YgTElDRU5TRVxuICpcbiAqIFRoaXMgc291cmNlIGZpbGUgaXMgc3ViamVjdCB0byB0aGUgT3BlbiBTb2Z0d2FyZSBMaWNlbnNlIChPU0wgMy4wKVxuICogdGhhdCBpcyBidW5kbGVkIHdpdGggdGhpcyBwYWNrYWdlIGluIHRoZSBmaWxlIExJQ0VOU0UubWQuXG4gKiBJdCBpcyBhbHNvIGF2YWlsYWJsZSB0aHJvdWdoIHRoZSB3b3JsZC13aWRlLXdlYiBhdCB0aGlzIFVSTDpcbiAqIGh0dHBzOi8vb3BlbnNvdXJjZS5vcmcvbGljZW5zZXMvT1NMLTMuMFxuICogSWYgeW91IGRpZCBub3QgcmVjZWl2ZSBhIGNvcHkgb2YgdGhlIGxpY2Vuc2UgYW5kIGFyZSB1bmFibGUgdG9cbiAqIG9idGFpbiBpdCB0aHJvdWdoIHRoZSB3b3JsZC13aWRlLXdlYiwgcGxlYXNlIHNlbmQgYW4gZW1haWxcbiAqIHRvIGxpY2Vuc2VAcHJlc3Rhc2hvcC5jb20gc28gd2UgY2FuIHNlbmQgeW91IGEgY29weSBpbW1lZGlhdGVseS5cbiAqXG4gKiBESVNDTEFJTUVSXG4gKlxuICogRG8gbm90IGVkaXQgb3IgYWRkIHRvIHRoaXMgZmlsZSBpZiB5b3Ugd2lzaCB0byB1cGdyYWRlIFByZXN0YVNob3AgdG8gbmV3ZXJcbiAqIHZlcnNpb25zIGluIHRoZSBmdXR1cmUuIElmIHlvdSB3aXNoIHRvIGN1c3RvbWl6ZSBQcmVzdGFTaG9wIGZvciB5b3VyXG4gKiBuZWVkcyBwbGVhc2UgcmVmZXIgdG8gaHR0cHM6Ly9kZXZkb2NzLnByZXN0YXNob3AuY29tLyBmb3IgbW9yZSBpbmZvcm1hdGlvbi5cbiAqXG4gKiBAYXV0aG9yICAgIFByZXN0YVNob3AgU0EgYW5kIENvbnRyaWJ1dG9ycyA8Y29udGFjdEBwcmVzdGFzaG9wLmNvbT5cbiAqIEBjb3B5cmlnaHQgU2luY2UgMjAwNyBQcmVzdGFTaG9wIFNBIGFuZCBDb250cmlidXRvcnNcbiAqIEBsaWNlbnNlICAgaHR0cHM6Ly9vcGVuc291cmNlLm9yZy9saWNlbnNlcy9PU0wtMy4wIE9wZW4gU29mdHdhcmUgTGljZW5zZSAoT1NMIDMuMClcbiAqL1xuXG5pbXBvcnQgT3JkZXJWaWV3UGFnZU1hcCBmcm9tICdAcGFnZXMvb3JkZXIvT3JkZXJWaWV3UGFnZU1hcCc7XG5pbXBvcnQgT3JkZXJQcm9kdWN0RWRpdCBmcm9tICdAcGFnZXMvb3JkZXIvdmlldy9vcmRlci1wcm9kdWN0LWVkaXQnO1xuaW1wb3J0IFJvdXRlciBmcm9tICdAY29tcG9uZW50cy9yb3V0ZXInO1xuXG5jb25zdCB7JH0gPSB3aW5kb3c7XG5cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIE9yZGVyUHJvZHVjdFJlbmRlcmVyIHtcbiAgY29uc3RydWN0b3IoKSB7XG4gICAgdGhpcy5yb3V0ZXIgPSBuZXcgUm91dGVyKCk7XG4gIH1cblxuICBhZGRPclVwZGF0ZVByb2R1Y3RUb0xpc3QoJHByb2R1Y3RSb3csIG5ld1Jvdykge1xuICAgIGlmICgkcHJvZHVjdFJvdy5sZW5ndGggPiAwKSB7XG4gICAgICAkcHJvZHVjdFJvdy5odG1sKCQobmV3Um93KS5odG1sKCkpO1xuICAgIH0gZWxzZSB7XG4gICAgICAkKE9yZGVyVmlld1BhZ2VNYXAucHJvZHVjdEFkZFJvdykuYmVmb3JlKFxuICAgICAgICAkKG5ld1JvdylcbiAgICAgICAgICAuaGlkZSgpXG4gICAgICAgICAgLmZhZGVJbigpLFxuICAgICAgKTtcbiAgICB9XG4gIH1cblxuICB1cGRhdGVOdW1Qcm9kdWN0cyhudW1Qcm9kdWN0cykge1xuICAgICQoT3JkZXJWaWV3UGFnZU1hcC5wcm9kdWN0c0NvdW50KS5odG1sKG51bVByb2R1Y3RzKTtcbiAgfVxuXG4gIGVkaXRQcm9kdWN0RnJvbUxpc3QoXG4gICAgb3JkZXJEZXRhaWxJZCxcbiAgICBxdWFudGl0eSxcbiAgICBwcmljZVRheEluY2wsXG4gICAgcHJpY2VUYXhFeGNsLFxuICAgIHRheFJhdGUsXG4gICAgbG9jYXRpb24sXG4gICAgYXZhaWxhYmxlUXVhbnRpdHksXG4gICAgYXZhaWxhYmxlT3V0T2ZTdG9jayxcbiAgICBvcmRlckludm9pY2VJZCxcbiAgICBpc09yZGVyVGF4SW5jbHVkZWQsXG4gICkge1xuICAgIGNvbnN0ICRvcmRlckVkaXQgPSBuZXcgT3JkZXJQcm9kdWN0RWRpdChvcmRlckRldGFpbElkKTtcbiAgICAkb3JkZXJFZGl0LmRpc3BsYXlQcm9kdWN0KHtcbiAgICAgIHByaWNlX3RheF9leGNsOiBwcmljZVRheEV4Y2wsXG4gICAgICBwcmljZV90YXhfaW5jbDogcHJpY2VUYXhJbmNsLFxuICAgICAgdGF4X3JhdGU6IHRheFJhdGUsXG4gICAgICBxdWFudGl0eSxcbiAgICAgIGxvY2F0aW9uLFxuICAgICAgYXZhaWxhYmxlUXVhbnRpdHksXG4gICAgICBhdmFpbGFibGVPdXRPZlN0b2NrLFxuICAgICAgb3JkZXJJbnZvaWNlSWQsXG4gICAgICBpc09yZGVyVGF4SW5jbHVkZWQsXG4gICAgfSk7XG4gICAgJChPcmRlclZpZXdQYWdlTWFwLnByb2R1Y3RBZGRBY3Rpb25CdG4pLmFkZENsYXNzKCdkLW5vbmUnKTtcbiAgICAkKE9yZGVyVmlld1BhZ2VNYXAucHJvZHVjdEFkZFJvdykuYWRkQ2xhc3MoJ2Qtbm9uZScpO1xuICB9XG5cbiAgbW92ZVByb2R1Y3RzUGFuZWxUb01vZGlmaWNhdGlvblBvc2l0aW9uKHNjcm9sbFRhcmdldCA9ICdib2R5Jykge1xuICAgICQoT3JkZXJWaWV3UGFnZU1hcC5wcm9kdWN0QWN0aW9uQnRuKS5hZGRDbGFzcygnZC1ub25lJyk7XG4gICAgJChcbiAgICAgIGAke09yZGVyVmlld1BhZ2VNYXAucHJvZHVjdEFkZEFjdGlvbkJ0bn0sICR7T3JkZXJWaWV3UGFnZU1hcC5wcm9kdWN0QWRkUm93fWAsXG4gICAgKS5yZW1vdmVDbGFzcygnZC1ub25lJyk7XG4gICAgdGhpcy5tb3ZlUHJvZHVjdFBhbmVsVG9Ub3Aoc2Nyb2xsVGFyZ2V0KTtcbiAgfVxuXG4gIG1vdmVQcm9kdWN0c1BhbmVsVG9SZWZ1bmRQb3NpdGlvbigpIHtcbiAgICB0aGlzLnJlc2V0QWxsRWRpdFJvd3MoKTtcbiAgICAkKFxuICAgICAgLyogZXNsaW50LWRpc2FibGUtbmV4dC1saW5lIG1heC1sZW4gKi9cbiAgICAgIGAke09yZGVyVmlld1BhZ2VNYXAucHJvZHVjdEFkZEFjdGlvbkJ0bn0sICR7T3JkZXJWaWV3UGFnZU1hcC5wcm9kdWN0QWRkUm93fSwgJHtPcmRlclZpZXdQYWdlTWFwLnByb2R1Y3RBY3Rpb25CdG59YCxcbiAgICApLmFkZENsYXNzKCdkLW5vbmUnKTtcbiAgICB0aGlzLm1vdmVQcm9kdWN0UGFuZWxUb1RvcCgpO1xuICB9XG5cbiAgbW92ZVByb2R1Y3RQYW5lbFRvVG9wKHNjcm9sbFRhcmdldCA9ICdib2R5Jykge1xuICAgIGNvbnN0ICRtb2RpZmljYXRpb25Qb3NpdGlvbiA9ICQoXG4gICAgICBPcmRlclZpZXdQYWdlTWFwLnByb2R1Y3RNb2RpZmljYXRpb25Qb3NpdGlvbixcbiAgICApO1xuXG4gICAgaWYgKCRtb2RpZmljYXRpb25Qb3NpdGlvbi5maW5kKE9yZGVyVmlld1BhZ2VNYXAucHJvZHVjdHNQYW5lbCkubGVuZ3RoID4gMCkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cbiAgICAkKE9yZGVyVmlld1BhZ2VNYXAucHJvZHVjdHNQYW5lbClcbiAgICAgIC5kZXRhY2goKVxuICAgICAgLmFwcGVuZFRvKCRtb2RpZmljYXRpb25Qb3NpdGlvbik7XG4gICAgJG1vZGlmaWNhdGlvblBvc2l0aW9uLmNsb3Nlc3QoJy5yb3cnKS5yZW1vdmVDbGFzcygnZC1ub25lJyk7XG5cbiAgICAvLyBTaG93IGNvbHVtbiBsb2NhdGlvbiAmIHJlZnVuZGVkXG4gICAgdGhpcy50b2dnbGVDb2x1bW4oT3JkZXJWaWV3UGFnZU1hcC5wcm9kdWN0c0NlbGxMb2NhdGlvbik7XG4gICAgdGhpcy50b2dnbGVDb2x1bW4oT3JkZXJWaWV3UGFnZU1hcC5wcm9kdWN0c0NlbGxSZWZ1bmRlZCk7XG5cbiAgICAvLyBTaG93IGFsbCByb3dzLCBoaWRlIHBhZ2luYXRpb24gY29udHJvbHNcbiAgICBjb25zdCAkcm93cyA9ICQoT3JkZXJWaWV3UGFnZU1hcC5wcm9kdWN0c1RhYmxlKS5maW5kKFxuICAgICAgJ3RyW2lkXj1cIm9yZGVyUHJvZHVjdF9cIl0nLFxuICAgICk7XG4gICAgJHJvd3MucmVtb3ZlQ2xhc3MoJ2Qtbm9uZScpO1xuICAgICQoT3JkZXJWaWV3UGFnZU1hcC5wcm9kdWN0c1BhZ2luYXRpb24pLmFkZENsYXNzKCdkLW5vbmUnKTtcblxuICAgIGNvbnN0IHNjcm9sbFZhbHVlID0gJChzY3JvbGxUYXJnZXQpLm9mZnNldCgpLnRvcCAtICQoJy5oZWFkZXItdG9vbGJhcicpLmhlaWdodCgpIC0gMTAwO1xuICAgICQoJ2h0bWwsYm9keScpLmFuaW1hdGUoe3Njcm9sbFRvcDogc2Nyb2xsVmFsdWV9LCAnc2xvdycpO1xuICB9XG5cbiAgbW92ZVByb2R1Y3RQYW5lbFRvT3JpZ2luYWxQb3NpdGlvbigpIHtcbiAgICAkKE9yZGVyVmlld1BhZ2VNYXAucHJvZHVjdEFkZE5ld0ludm9pY2VJbmZvKS5hZGRDbGFzcygnZC1ub25lJyk7XG4gICAgJChPcmRlclZpZXdQYWdlTWFwLnByb2R1Y3RNb2RpZmljYXRpb25Qb3NpdGlvbilcbiAgICAgIC5jbG9zZXN0KCcucm93JylcbiAgICAgIC5hZGRDbGFzcygnZC1ub25lJyk7XG5cbiAgICAkKE9yZGVyVmlld1BhZ2VNYXAucHJvZHVjdHNQYW5lbClcbiAgICAgIC5kZXRhY2goKVxuICAgICAgLmFwcGVuZFRvKE9yZGVyVmlld1BhZ2VNYXAucHJvZHVjdE9yaWdpbmFsUG9zaXRpb24pO1xuXG4gICAgJChPcmRlclZpZXdQYWdlTWFwLnByb2R1Y3RzUGFnaW5hdGlvbikucmVtb3ZlQ2xhc3MoJ2Qtbm9uZScpO1xuICAgICQoT3JkZXJWaWV3UGFnZU1hcC5wcm9kdWN0QWN0aW9uQnRuKS5yZW1vdmVDbGFzcygnZC1ub25lJyk7XG4gICAgJChcbiAgICAgIGAke09yZGVyVmlld1BhZ2VNYXAucHJvZHVjdEFkZEFjdGlvbkJ0bn0sICR7T3JkZXJWaWV3UGFnZU1hcC5wcm9kdWN0QWRkUm93fWAsXG4gICAgKS5hZGRDbGFzcygnZC1ub25lJyk7XG5cbiAgICAvLyBSZXN0b3JlIHBhZ2luYXRpb25cbiAgICB0aGlzLnBhZ2luYXRlKDEpO1xuICB9XG5cbiAgcmVzZXRBZGRSb3coKSB7XG4gICAgJChPcmRlclZpZXdQYWdlTWFwLnByb2R1Y3RBZGRJZElucHV0KS52YWwoJycpO1xuICAgICQoT3JkZXJWaWV3UGFnZU1hcC5wcm9kdWN0U2VhcmNoSW5wdXQpLnZhbCgnJyk7XG4gICAgJChPcmRlclZpZXdQYWdlTWFwLnByb2R1Y3RBZGRDb21iaW5hdGlvbnNCbG9jaykuYWRkQ2xhc3MoJ2Qtbm9uZScpO1xuICAgICQoT3JkZXJWaWV3UGFnZU1hcC5wcm9kdWN0QWRkQ29tYmluYXRpb25zU2VsZWN0KS52YWwoJycpO1xuICAgICQoT3JkZXJWaWV3UGFnZU1hcC5wcm9kdWN0QWRkQ29tYmluYXRpb25zU2VsZWN0KS5wcm9wKCdkaXNhYmxlZCcsIGZhbHNlKTtcbiAgICAkKE9yZGVyVmlld1BhZ2VNYXAucHJvZHVjdEFkZFByaWNlVGF4RXhjbElucHV0KS52YWwoJycpO1xuICAgICQoT3JkZXJWaWV3UGFnZU1hcC5wcm9kdWN0QWRkUHJpY2VUYXhJbmNsSW5wdXQpLnZhbCgnJyk7XG4gICAgJChPcmRlclZpZXdQYWdlTWFwLnByb2R1Y3RBZGRRdWFudGl0eUlucHV0KS52YWwoJycpO1xuICAgICQoT3JkZXJWaWV3UGFnZU1hcC5wcm9kdWN0QWRkQXZhaWxhYmxlVGV4dCkuaHRtbCgnJyk7XG4gICAgJChPcmRlclZpZXdQYWdlTWFwLnByb2R1Y3RBZGRMb2NhdGlvblRleHQpLmh0bWwoJycpO1xuICAgICQoT3JkZXJWaWV3UGFnZU1hcC5wcm9kdWN0QWRkTmV3SW52b2ljZUluZm8pLmFkZENsYXNzKCdkLW5vbmUnKTtcbiAgICAkKE9yZGVyVmlld1BhZ2VNYXAucHJvZHVjdEFkZEFjdGlvbkJ0bikucHJvcCgnZGlzYWJsZWQnLCB0cnVlKTtcbiAgfVxuXG4gIHJlc2V0QWxsRWRpdFJvd3MoKSB7XG4gICAgJChPcmRlclZpZXdQYWdlTWFwLnByb2R1Y3RFZGl0QnV0dG9ucykuZWFjaCgoa2V5LCBlZGl0QnV0dG9uKSA9PiB7XG4gICAgICB0aGlzLnJlc2V0RWRpdFJvdygkKGVkaXRCdXR0b24pLmRhdGEoJ29yZGVyRGV0YWlsSWQnKSk7XG4gICAgfSk7XG4gIH1cblxuICByZXNldEVkaXRSb3cob3JkZXJQcm9kdWN0SWQpIHtcbiAgICBjb25zdCAkcHJvZHVjdFJvdyA9ICQoT3JkZXJWaWV3UGFnZU1hcC5wcm9kdWN0c1RhYmxlUm93KG9yZGVyUHJvZHVjdElkKSk7XG4gICAgY29uc3QgJHByb2R1Y3RFZGl0Um93ID0gJChcbiAgICAgIE9yZGVyVmlld1BhZ2VNYXAucHJvZHVjdHNUYWJsZVJvd0VkaXRlZChvcmRlclByb2R1Y3RJZCksXG4gICAgKTtcbiAgICAkcHJvZHVjdEVkaXRSb3cucmVtb3ZlKCk7XG4gICAgJHByb2R1Y3RSb3cucmVtb3ZlQ2xhc3MoJ2Qtbm9uZScpO1xuICB9XG5cbiAgcGFnaW5hdGUob3JpZ2luYWxOdW1QYWdlKSB7XG4gICAgY29uc3QgJHJvd3MgPSAkKE9yZGVyVmlld1BhZ2VNYXAucHJvZHVjdHNUYWJsZSkuZmluZChcbiAgICAgICd0cltpZF49XCJvcmRlclByb2R1Y3RfXCJdJyxcbiAgICApO1xuICAgIGNvbnN0ICRjdXN0b21pemF0aW9uUm93cyA9ICQoXG4gICAgICBPcmRlclZpZXdQYWdlTWFwLnByb2R1Y3RzVGFibGVDdXN0b21pemF0aW9uUm93cyxcbiAgICApO1xuICAgIGNvbnN0ICR0YWJsZVBhZ2luYXRpb24gPSAkKE9yZGVyVmlld1BhZ2VNYXAucHJvZHVjdHNUYWJsZVBhZ2luYXRpb24pO1xuICAgIGNvbnN0IG51bVJvd3NQZXJQYWdlID0gcGFyc2VJbnQoJHRhYmxlUGFnaW5hdGlvbi5kYXRhKCdudW1QZXJQYWdlJyksIDEwKTtcbiAgICBjb25zdCBtYXhQYWdlID0gTWF0aC5jZWlsKCRyb3dzLmxlbmd0aCAvIG51bVJvd3NQZXJQYWdlKTtcbiAgICBjb25zdCBudW1QYWdlID0gTWF0aC5tYXgoMSwgTWF0aC5taW4ob3JpZ2luYWxOdW1QYWdlLCBtYXhQYWdlKSk7XG4gICAgdGhpcy5wYWdpbmF0ZVVwZGF0ZUNvbnRyb2xzKG51bVBhZ2UpO1xuXG4gICAgLy8gSGlkZSBhbGwgcm93cy4uLlxuICAgICRyb3dzLmFkZENsYXNzKCdkLW5vbmUnKTtcbiAgICAkY3VzdG9taXphdGlvblJvd3MuYWRkQ2xhc3MoJ2Qtbm9uZScpO1xuICAgIC8vIC4uLiBhbmQgZGlzcGxheSBnb29kIG9uZXNcblxuICAgIGNvbnN0IHN0YXJ0Um93ID0gKG51bVBhZ2UgLSAxKSAqIG51bVJvd3NQZXJQYWdlICsgMTtcbiAgICBjb25zdCBlbmRSb3cgPSBudW1QYWdlICogbnVtUm93c1BlclBhZ2U7XG5cbiAgICBmb3IgKGxldCBpID0gc3RhcnRSb3cgLSAxOyBpIDwgTWF0aC5taW4oZW5kUm93LCAkcm93cy5sZW5ndGgpOyBpICs9IDEpIHtcbiAgICAgICQoJHJvd3NbaV0pLnJlbW92ZUNsYXNzKCdkLW5vbmUnKTtcbiAgICB9XG5cbiAgICAkY3VzdG9taXphdGlvblJvd3MuZWFjaChmdW5jdGlvbiAoKSB7XG4gICAgICBpZiAoXG4gICAgICAgICEkKHRoaXMpXG4gICAgICAgICAgLnByZXYoKVxuICAgICAgICAgIC5oYXNDbGFzcygnZC1ub25lJylcbiAgICAgICkge1xuICAgICAgICAkKHRoaXMpLnJlbW92ZUNsYXNzKCdkLW5vbmUnKTtcbiAgICAgIH1cbiAgICB9KTtcblxuICAgIC8vIFJlbW92ZSBhbGwgZWRpdGlvbiByb3dzIChjYXJlZnVsIG5vdCB0byByZW1vdmUgdGhlIHRlbXBsYXRlKVxuICAgICQoT3JkZXJWaWV3UGFnZU1hcC5wcm9kdWN0RWRpdFJvdylcbiAgICAgIC5ub3QoT3JkZXJWaWV3UGFnZU1hcC5wcm9kdWN0RWRpdFJvd1RlbXBsYXRlKVxuICAgICAgLnJlbW92ZSgpO1xuXG4gICAgLy8gVG9nZ2xlIENvbHVtbiBMb2NhdGlvbiAmIFJlZnVuZGVkXG4gICAgdGhpcy50b2dnbGVDb2x1bW4oT3JkZXJWaWV3UGFnZU1hcC5wcm9kdWN0c0NlbGxMb2NhdGlvbkRpc3BsYXllZCk7XG4gICAgdGhpcy50b2dnbGVDb2x1bW4oT3JkZXJWaWV3UGFnZU1hcC5wcm9kdWN0c0NlbGxSZWZ1bmRlZERpc3BsYXllZCk7XG4gIH1cblxuICBwYWdpbmF0ZVVwZGF0ZUNvbnRyb2xzKG51bVBhZ2UpIHtcbiAgICAvLyBXaHkgMyA/IE5leHQgJiBQcmV2ICYgVGVtcGxhdGVcbiAgICBjb25zdCB0b3RhbFBhZ2UgPSAkKE9yZGVyVmlld1BhZ2VNYXAucHJvZHVjdHNUYWJsZVBhZ2luYXRpb24pLmZpbmQoJ2xpLnBhZ2UtaXRlbScpLmxlbmd0aFxuICAgICAgLSAzO1xuICAgICQoT3JkZXJWaWV3UGFnZU1hcC5wcm9kdWN0c1RhYmxlUGFnaW5hdGlvbilcbiAgICAgIC5maW5kKCcuYWN0aXZlJylcbiAgICAgIC5yZW1vdmVDbGFzcygnYWN0aXZlJyk7XG4gICAgJChPcmRlclZpZXdQYWdlTWFwLnByb2R1Y3RzVGFibGVQYWdpbmF0aW9uKVxuICAgICAgLmZpbmQoYGxpOmhhcyg+IFtkYXRhLXBhZ2U9XCIke251bVBhZ2V9XCJdKWApXG4gICAgICAuYWRkQ2xhc3MoJ2FjdGl2ZScpO1xuICAgICQoT3JkZXJWaWV3UGFnZU1hcC5wcm9kdWN0c1RhYmxlUGFnaW5hdGlvblByZXYpLnJlbW92ZUNsYXNzKCdkaXNhYmxlZCcpO1xuICAgIGlmIChudW1QYWdlID09PSAxKSB7XG4gICAgICAkKE9yZGVyVmlld1BhZ2VNYXAucHJvZHVjdHNUYWJsZVBhZ2luYXRpb25QcmV2KS5hZGRDbGFzcygnZGlzYWJsZWQnKTtcbiAgICB9XG4gICAgJChPcmRlclZpZXdQYWdlTWFwLnByb2R1Y3RzVGFibGVQYWdpbmF0aW9uTmV4dCkucmVtb3ZlQ2xhc3MoJ2Rpc2FibGVkJyk7XG4gICAgaWYgKG51bVBhZ2UgPT09IHRvdGFsUGFnZSkge1xuICAgICAgJChPcmRlclZpZXdQYWdlTWFwLnByb2R1Y3RzVGFibGVQYWdpbmF0aW9uTmV4dCkuYWRkQ2xhc3MoJ2Rpc2FibGVkJyk7XG4gICAgfVxuICAgIHRoaXMudG9nZ2xlUGFnaW5hdGlvbkNvbnRyb2xzKCk7XG4gIH1cblxuICB1cGRhdGVOdW1QZXJQYWdlKG51bVBlclBhZ2UpIHtcbiAgICAkKE9yZGVyVmlld1BhZ2VNYXAucHJvZHVjdHNUYWJsZVBhZ2luYXRpb24pLmRhdGEoJ251bVBlclBhZ2UnLCBudW1QZXJQYWdlKTtcbiAgICB0aGlzLnVwZGF0ZVBhZ2luYXRpb25Db250cm9scygpO1xuICB9XG5cbiAgdG9nZ2xlUGFnaW5hdGlvbkNvbnRyb2xzKCkge1xuICAgIC8vIFdoeSAzID8gTmV4dCAmIFByZXYgJiBUZW1wbGF0ZVxuICAgIGNvbnN0IHRvdGFsUGFnZSA9ICQoT3JkZXJWaWV3UGFnZU1hcC5wcm9kdWN0c1RhYmxlUGFnaW5hdGlvbikuZmluZCgnbGkucGFnZS1pdGVtJykubGVuZ3RoXG4gICAgICAtIDM7XG4gICAgJChPcmRlclZpZXdQYWdlTWFwLnByb2R1Y3RzTmF2UGFnaW5hdGlvbikudG9nZ2xlQ2xhc3MoXG4gICAgICAnZC1ub25lJyxcbiAgICAgIHRvdGFsUGFnZSA8PSAxLFxuICAgICk7XG4gIH1cblxuICB0b2dnbGVQcm9kdWN0QWRkTmV3SW52b2ljZUluZm8oKSB7XG4gICAgJChPcmRlclZpZXdQYWdlTWFwLnByb2R1Y3RBZGROZXdJbnZvaWNlSW5mbykudG9nZ2xlQ2xhc3MoXG4gICAgICAnZC1ub25lJyxcbiAgICAgIHBhcnNlSW50KCQoT3JkZXJWaWV3UGFnZU1hcC5wcm9kdWN0QWRkSW52b2ljZVNlbGVjdCkudmFsKCksIDEwKSAhPT0gMCxcbiAgICApO1xuICB9XG5cbiAgdG9nZ2xlQ29sdW1uKHRhcmdldCwgZm9yY2VEaXNwbGF5ID0gbnVsbCkge1xuICAgIGxldCBpc0NvbHVtbkRpc3BsYXllZCA9IGZhbHNlO1xuXG4gICAgaWYgKGZvcmNlRGlzcGxheSA9PT0gbnVsbCkge1xuICAgICAgJCh0YXJnZXQpXG4gICAgICAgIC5maWx0ZXIoJ3RkJylcbiAgICAgICAgLmVhY2goZnVuY3Rpb24gKCkge1xuICAgICAgICAgIGlmICgkKHRoaXMpLmh0bWwoKSAhPT0gJycpIHtcbiAgICAgICAgICAgIGlzQ29sdW1uRGlzcGxheWVkID0gdHJ1ZTtcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICB9XG4gICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgIH0pO1xuICAgIH0gZWxzZSB7XG4gICAgICBpc0NvbHVtbkRpc3BsYXllZCA9IGZvcmNlRGlzcGxheTtcbiAgICB9XG4gICAgJCh0YXJnZXQpLnRvZ2dsZUNsYXNzKCdkLW5vbmUnLCAhaXNDb2x1bW5EaXNwbGF5ZWQpO1xuICB9XG5cbiAgdXBkYXRlUGFnaW5hdGlvbkNvbnRyb2xzKCkge1xuICAgIGNvbnN0ICR0YWJsZVBhZ2luYXRpb24gPSAkKE9yZGVyVmlld1BhZ2VNYXAucHJvZHVjdHNUYWJsZVBhZ2luYXRpb24pO1xuICAgIGNvbnN0IG51bVBlclBhZ2UgPSAkdGFibGVQYWdpbmF0aW9uLmRhdGEoJ251bVBlclBhZ2UnKTtcbiAgICBjb25zdCAkcm93cyA9ICQoT3JkZXJWaWV3UGFnZU1hcC5wcm9kdWN0c1RhYmxlKS5maW5kKCd0cltpZF49XCJvcmRlclByb2R1Y3RfXCJdJyk7XG4gICAgY29uc3QgbnVtUGFnZXMgPSBNYXRoLmNlaWwoJHJvd3MubGVuZ3RoIC8gbnVtUGVyUGFnZSk7XG5cbiAgICAvLyBVcGRhdGUgdGFibGUgZGF0YSBmaWVsZHNcbiAgICAkdGFibGVQYWdpbmF0aW9uLmRhdGEoJ251bVBhZ2VzJywgbnVtUGFnZXMpO1xuXG4gICAgLy8gQ2xlYW4gYWxsIHBhZ2UgbGlua3MsIHJlaW5zZXJ0IHRoZSByZW1vdmVkIHRlbXBsYXRlXG4gICAgY29uc3QgJGxpbmtQYWdpbmF0aW9uVGVtcGxhdGUgPSAkKE9yZGVyVmlld1BhZ2VNYXAucHJvZHVjdHNUYWJsZVBhZ2luYXRpb25UZW1wbGF0ZSk7XG4gICAgJChPcmRlclZpZXdQYWdlTWFwLnByb2R1Y3RzVGFibGVQYWdpbmF0aW9uKS5maW5kKCdsaTpoYXMoPiBbZGF0YS1wYWdlXSknKS5yZW1vdmUoKTtcbiAgICAkKE9yZGVyVmlld1BhZ2VNYXAucHJvZHVjdHNUYWJsZVBhZ2luYXRpb25OZXh0KS5iZWZvcmUoJGxpbmtQYWdpbmF0aW9uVGVtcGxhdGUpO1xuXG4gICAgLy8gQWRkIGFwcHJvcHJpYXRlIHBhZ2VzXG4gICAgZm9yIChsZXQgaSA9IDE7IGkgPD0gbnVtUGFnZXM7IGkgKz0gMSkge1xuICAgICAgY29uc3QgJGxpbmtQYWdpbmF0aW9uID0gJGxpbmtQYWdpbmF0aW9uVGVtcGxhdGUuY2xvbmUoKTtcbiAgICAgICRsaW5rUGFnaW5hdGlvbi5maW5kKCdzcGFuJykuYXR0cignZGF0YS1wYWdlJywgaSk7XG4gICAgICAkbGlua1BhZ2luYXRpb24uZmluZCgnc3BhbicpLmh0bWwoaSk7XG4gICAgICAkbGlua1BhZ2luYXRpb25UZW1wbGF0ZS5iZWZvcmUoJGxpbmtQYWdpbmF0aW9uLnJlbW92ZUNsYXNzKCdkLW5vbmUnKSk7XG4gICAgfVxuXG4gICAgdGhpcy50b2dnbGVQYWdpbmF0aW9uQ29udHJvbHMoKTtcbiAgfVxufVxuIiwiLyoqXG4gKiBDb3B5cmlnaHQgc2luY2UgMjAwNyBQcmVzdGFTaG9wIFNBIGFuZCBDb250cmlidXRvcnNcbiAqIFByZXN0YVNob3AgaXMgYW4gSW50ZXJuYXRpb25hbCBSZWdpc3RlcmVkIFRyYWRlbWFyayAmIFByb3BlcnR5IG9mIFByZXN0YVNob3AgU0FcbiAqXG4gKiBOT1RJQ0UgT0YgTElDRU5TRVxuICpcbiAqIFRoaXMgc291cmNlIGZpbGUgaXMgc3ViamVjdCB0byB0aGUgT3BlbiBTb2Z0d2FyZSBMaWNlbnNlIChPU0wgMy4wKVxuICogdGhhdCBpcyBidW5kbGVkIHdpdGggdGhpcyBwYWNrYWdlIGluIHRoZSBmaWxlIExJQ0VOU0UubWQuXG4gKiBJdCBpcyBhbHNvIGF2YWlsYWJsZSB0aHJvdWdoIHRoZSB3b3JsZC13aWRlLXdlYiBhdCB0aGlzIFVSTDpcbiAqIGh0dHBzOi8vb3BlbnNvdXJjZS5vcmcvbGljZW5zZXMvT1NMLTMuMFxuICogSWYgeW91IGRpZCBub3QgcmVjZWl2ZSBhIGNvcHkgb2YgdGhlIGxpY2Vuc2UgYW5kIGFyZSB1bmFibGUgdG9cbiAqIG9idGFpbiBpdCB0aHJvdWdoIHRoZSB3b3JsZC13aWRlLXdlYiwgcGxlYXNlIHNlbmQgYW4gZW1haWxcbiAqIHRvIGxpY2Vuc2VAcHJlc3Rhc2hvcC5jb20gc28gd2UgY2FuIHNlbmQgeW91IGEgY29weSBpbW1lZGlhdGVseS5cbiAqXG4gKiBESVNDTEFJTUVSXG4gKlxuICogRG8gbm90IGVkaXQgb3IgYWRkIHRvIHRoaXMgZmlsZSBpZiB5b3Ugd2lzaCB0byB1cGdyYWRlIFByZXN0YVNob3AgdG8gbmV3ZXJcbiAqIHZlcnNpb25zIGluIHRoZSBmdXR1cmUuIElmIHlvdSB3aXNoIHRvIGN1c3RvbWl6ZSBQcmVzdGFTaG9wIGZvciB5b3VyXG4gKiBuZWVkcyBwbGVhc2UgcmVmZXIgdG8gaHR0cHM6Ly9kZXZkb2NzLnByZXN0YXNob3AuY29tLyBmb3IgbW9yZSBpbmZvcm1hdGlvbi5cbiAqXG4gKiBAYXV0aG9yICAgIFByZXN0YVNob3AgU0EgYW5kIENvbnRyaWJ1dG9ycyA8Y29udGFjdEBwcmVzdGFzaG9wLmNvbT5cbiAqIEBjb3B5cmlnaHQgU2luY2UgMjAwNyBQcmVzdGFTaG9wIFNBIGFuZCBDb250cmlidXRvcnNcbiAqIEBsaWNlbnNlICAgaHR0cHM6Ly9vcGVuc291cmNlLm9yZy9saWNlbnNlcy9PU0wtMy4wIE9wZW4gU29mdHdhcmUgTGljZW5zZSAoT1NMIDMuMClcbiAqL1xuXG5pbXBvcnQgUm91dGVyIGZyb20gJ0Bjb21wb25lbnRzL3JvdXRlcic7XG5pbXBvcnQgT3JkZXJWaWV3UGFnZU1hcCBmcm9tICdAcGFnZXMvb3JkZXIvT3JkZXJWaWV3UGFnZU1hcCc7XG5cbmNvbnN0IHskfSA9IHdpbmRvdztcblxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgT3JkZXJTaGlwcGluZ1JlZnJlc2hlciB7XG4gIGNvbnN0cnVjdG9yKCkge1xuICAgIHRoaXMucm91dGVyID0gbmV3IFJvdXRlcigpO1xuICB9XG5cbiAgcmVmcmVzaChvcmRlcklkKSB7XG4gICAgJC5nZXRKU09OKHRoaXMucm91dGVyLmdlbmVyYXRlKCdhZG1pbl9vcmRlcnNfZ2V0X3NoaXBwaW5nJywge29yZGVySWR9KSlcbiAgICAgIC50aGVuKChyZXNwb25zZSkgPT4ge1xuICAgICAgICAkKE9yZGVyVmlld1BhZ2VNYXAub3JkZXJTaGlwcGluZ1RhYkNvdW50KS50ZXh0KHJlc3BvbnNlLnRvdGFsKTtcbiAgICAgICAgJChPcmRlclZpZXdQYWdlTWFwLm9yZGVyU2hpcHBpbmdUYWJCb2R5KS5odG1sKHJlc3BvbnNlLmh0bWwpO1xuICAgICAgfSk7XG4gIH1cbn1cbiIsIi8qKlxuICogQ29weXJpZ2h0IHNpbmNlIDIwMDcgUHJlc3RhU2hvcCBTQSBhbmQgQ29udHJpYnV0b3JzXG4gKiBQcmVzdGFTaG9wIGlzIGFuIEludGVybmF0aW9uYWwgUmVnaXN0ZXJlZCBUcmFkZW1hcmsgJiBQcm9wZXJ0eSBvZiBQcmVzdGFTaG9wIFNBXG4gKlxuICogTk9USUNFIE9GIExJQ0VOU0VcbiAqXG4gKiBUaGlzIHNvdXJjZSBmaWxlIGlzIHN1YmplY3QgdG8gdGhlIE9wZW4gU29mdHdhcmUgTGljZW5zZSAoT1NMIDMuMClcbiAqIHRoYXQgaXMgYnVuZGxlZCB3aXRoIHRoaXMgcGFja2FnZSBpbiB0aGUgZmlsZSBMSUNFTlNFLm1kLlxuICogSXQgaXMgYWxzbyBhdmFpbGFibGUgdGhyb3VnaCB0aGUgd29ybGQtd2lkZS13ZWIgYXQgdGhpcyBVUkw6XG4gKiBodHRwczovL29wZW5zb3VyY2Uub3JnL2xpY2Vuc2VzL09TTC0zLjBcbiAqIElmIHlvdSBkaWQgbm90IHJlY2VpdmUgYSBjb3B5IG9mIHRoZSBsaWNlbnNlIGFuZCBhcmUgdW5hYmxlIHRvXG4gKiBvYnRhaW4gaXQgdGhyb3VnaCB0aGUgd29ybGQtd2lkZS13ZWIsIHBsZWFzZSBzZW5kIGFuIGVtYWlsXG4gKiB0byBsaWNlbnNlQHByZXN0YXNob3AuY29tIHNvIHdlIGNhbiBzZW5kIHlvdSBhIGNvcHkgaW1tZWRpYXRlbHkuXG4gKlxuICogRElTQ0xBSU1FUlxuICpcbiAqIERvIG5vdCBlZGl0IG9yIGFkZCB0byB0aGlzIGZpbGUgaWYgeW91IHdpc2ggdG8gdXBncmFkZSBQcmVzdGFTaG9wIHRvIG5ld2VyXG4gKiB2ZXJzaW9ucyBpbiB0aGUgZnV0dXJlLiBJZiB5b3Ugd2lzaCB0byBjdXN0b21pemUgUHJlc3RhU2hvcCBmb3IgeW91clxuICogbmVlZHMgcGxlYXNlIHJlZmVyIHRvIGh0dHBzOi8vZGV2ZG9jcy5wcmVzdGFzaG9wLmNvbS8gZm9yIG1vcmUgaW5mb3JtYXRpb24uXG4gKlxuICogQGF1dGhvciAgICBQcmVzdGFTaG9wIFNBIGFuZCBDb250cmlidXRvcnMgPGNvbnRhY3RAcHJlc3Rhc2hvcC5jb20+XG4gKiBAY29weXJpZ2h0IFNpbmNlIDIwMDcgUHJlc3RhU2hvcCBTQSBhbmQgQ29udHJpYnV0b3JzXG4gKiBAbGljZW5zZSAgIGh0dHBzOi8vb3BlbnNvdXJjZS5vcmcvbGljZW5zZXMvT1NMLTMuMCBPcGVuIFNvZnR3YXJlIExpY2Vuc2UgKE9TTCAzLjApXG4gKi9cblxuZXhwb3J0IGRlZmF1bHQge1xuICBwcm9kdWN0RGVsZXRlZEZyb21PcmRlcjogJ3Byb2R1Y3REZWxldGVkRnJvbU9yZGVyJyxcbiAgcHJvZHVjdEFkZGVkVG9PcmRlcjogJ3Byb2R1Y3RBZGRlZFRvT3JkZXInLFxuICBwcm9kdWN0VXBkYXRlZDogJ3Byb2R1Y3RVcGRhdGVkJyxcbiAgcHJvZHVjdEVkaXRpb25DYW5jZWxlZDogJ3Byb2R1Y3RFZGl0aW9uQ2FuY2VsZWQnLFxuICBwcm9kdWN0TGlzdFBhZ2luYXRlZDogJ3Byb2R1Y3RMaXN0UGFnaW5hdGVkJyxcbiAgcHJvZHVjdExpc3ROdW1iZXJQZXJQYWdlOiAncHJvZHVjdExpc3ROdW1iZXJQZXJQYWdlJyxcbn07XG4iLCIvKipcbiAqIENvcHlyaWdodCBzaW5jZSAyMDA3IFByZXN0YVNob3AgU0EgYW5kIENvbnRyaWJ1dG9yc1xuICogUHJlc3RhU2hvcCBpcyBhbiBJbnRlcm5hdGlvbmFsIFJlZ2lzdGVyZWQgVHJhZGVtYXJrICYgUHJvcGVydHkgb2YgUHJlc3RhU2hvcCBTQVxuICpcbiAqIE5PVElDRSBPRiBMSUNFTlNFXG4gKlxuICogVGhpcyBzb3VyY2UgZmlsZSBpcyBzdWJqZWN0IHRvIHRoZSBPcGVuIFNvZnR3YXJlIExpY2Vuc2UgKE9TTCAzLjApXG4gKiB0aGF0IGlzIGJ1bmRsZWQgd2l0aCB0aGlzIHBhY2thZ2UgaW4gdGhlIGZpbGUgTElDRU5TRS5tZC5cbiAqIEl0IGlzIGFsc28gYXZhaWxhYmxlIHRocm91Z2ggdGhlIHdvcmxkLXdpZGUtd2ViIGF0IHRoaXMgVVJMOlxuICogaHR0cHM6Ly9vcGVuc291cmNlLm9yZy9saWNlbnNlcy9PU0wtMy4wXG4gKiBJZiB5b3UgZGlkIG5vdCByZWNlaXZlIGEgY29weSBvZiB0aGUgbGljZW5zZSBhbmQgYXJlIHVuYWJsZSB0b1xuICogb2J0YWluIGl0IHRocm91Z2ggdGhlIHdvcmxkLXdpZGUtd2ViLCBwbGVhc2Ugc2VuZCBhbiBlbWFpbFxuICogdG8gbGljZW5zZUBwcmVzdGFzaG9wLmNvbSBzbyB3ZSBjYW4gc2VuZCB5b3UgYSBjb3B5IGltbWVkaWF0ZWx5LlxuICpcbiAqIERJU0NMQUlNRVJcbiAqXG4gKiBEbyBub3QgZWRpdCBvciBhZGQgdG8gdGhpcyBmaWxlIGlmIHlvdSB3aXNoIHRvIHVwZ3JhZGUgUHJlc3RhU2hvcCB0byBuZXdlclxuICogdmVyc2lvbnMgaW4gdGhlIGZ1dHVyZS4gSWYgeW91IHdpc2ggdG8gY3VzdG9taXplIFByZXN0YVNob3AgZm9yIHlvdXJcbiAqIG5lZWRzIHBsZWFzZSByZWZlciB0byBodHRwczovL2RldmRvY3MucHJlc3Rhc2hvcC5jb20vIGZvciBtb3JlIGluZm9ybWF0aW9uLlxuICpcbiAqIEBhdXRob3IgICAgUHJlc3RhU2hvcCBTQSBhbmQgQ29udHJpYnV0b3JzIDxjb250YWN0QHByZXN0YXNob3AuY29tPlxuICogQGNvcHlyaWdodCBTaW5jZSAyMDA3IFByZXN0YVNob3AgU0EgYW5kIENvbnRyaWJ1dG9yc1xuICogQGxpY2Vuc2UgICBodHRwczovL29wZW5zb3VyY2Uub3JnL2xpY2Vuc2VzL09TTC0zLjAgT3BlbiBTb2Z0d2FyZSBMaWNlbnNlIChPU0wgMy4wKVxuICovXG5cbmltcG9ydCBPcmRlclByb2R1Y3RNYW5hZ2VyIGZyb20gJ0BwYWdlcy9vcmRlci92aWV3L29yZGVyLXByb2R1Y3QtbWFuYWdlcic7XG5pbXBvcnQgT3JkZXJWaWV3UGFnZU1hcCBmcm9tICdAcGFnZXMvb3JkZXIvT3JkZXJWaWV3UGFnZU1hcCc7XG5pbXBvcnQgT3JkZXJWaWV3RXZlbnRNYXAgZnJvbSAnQHBhZ2VzL29yZGVyL3ZpZXcvb3JkZXItdmlldy1ldmVudC1tYXAnO1xuaW1wb3J0IHtFdmVudEVtaXR0ZXJ9IGZyb20gJ0Bjb21wb25lbnRzL2V2ZW50LWVtaXR0ZXInO1xuaW1wb3J0IE9yZGVyRGlzY291bnRzUmVmcmVzaGVyIGZyb20gJ0BwYWdlcy9vcmRlci92aWV3L29yZGVyLWRpc2NvdW50cy1yZWZyZXNoZXInO1xuaW1wb3J0IE9yZGVyUHJvZHVjdFJlbmRlcmVyIGZyb20gJ0BwYWdlcy9vcmRlci92aWV3L29yZGVyLXByb2R1Y3QtcmVuZGVyZXInO1xuaW1wb3J0IE9yZGVyUHJpY2VzUmVmcmVzaGVyIGZyb20gJ0BwYWdlcy9vcmRlci92aWV3L29yZGVyLXByaWNlcy1yZWZyZXNoZXInO1xuaW1wb3J0IE9yZGVyUGF5bWVudHNSZWZyZXNoZXIgZnJvbSAnQHBhZ2VzL29yZGVyL3ZpZXcvb3JkZXItcGF5bWVudHMtcmVmcmVzaGVyJztcbmltcG9ydCBPcmRlclNoaXBwaW5nUmVmcmVzaGVyIGZyb20gJ0BwYWdlcy9vcmRlci92aWV3L29yZGVyLXNoaXBwaW5nLXJlZnJlc2hlcic7XG5pbXBvcnQgUm91dGVyIGZyb20gJ0Bjb21wb25lbnRzL3JvdXRlcic7XG5pbXBvcnQgT3JkZXJJbnZvaWNlc1JlZnJlc2hlciBmcm9tICcuL29yZGVyLWludm9pY2VzLXJlZnJlc2hlcic7XG5pbXBvcnQgT3JkZXJQcm9kdWN0Q2FuY2VsIGZyb20gJy4vb3JkZXItcHJvZHVjdC1jYW5jZWwnO1xuaW1wb3J0IE9yZGVyRG9jdW1lbnRzUmVmcmVzaGVyIGZyb20gJy4vb3JkZXItZG9jdW1lbnRzLXJlZnJlc2hlcic7XG5cbmNvbnN0IHskfSA9IHdpbmRvdztcblxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgT3JkZXJWaWV3UGFnZSB7XG4gIGNvbnN0cnVjdG9yKCkge1xuICAgIHRoaXMub3JkZXJEaXNjb3VudHNSZWZyZXNoZXIgPSBuZXcgT3JkZXJEaXNjb3VudHNSZWZyZXNoZXIoKTtcbiAgICB0aGlzLm9yZGVyUHJvZHVjdE1hbmFnZXIgPSBuZXcgT3JkZXJQcm9kdWN0TWFuYWdlcigpO1xuICAgIHRoaXMub3JkZXJQcm9kdWN0UmVuZGVyZXIgPSBuZXcgT3JkZXJQcm9kdWN0UmVuZGVyZXIoKTtcbiAgICB0aGlzLm9yZGVyUHJpY2VzUmVmcmVzaGVyID0gbmV3IE9yZGVyUHJpY2VzUmVmcmVzaGVyKCk7XG4gICAgdGhpcy5vcmRlclBheW1lbnRzUmVmcmVzaGVyID0gbmV3IE9yZGVyUGF5bWVudHNSZWZyZXNoZXIoKTtcbiAgICB0aGlzLm9yZGVyU2hpcHBpbmdSZWZyZXNoZXIgPSBuZXcgT3JkZXJTaGlwcGluZ1JlZnJlc2hlcigpO1xuICAgIHRoaXMub3JkZXJEb2N1bWVudHNSZWZyZXNoZXIgPSBuZXcgT3JkZXJEb2N1bWVudHNSZWZyZXNoZXIoKTtcbiAgICB0aGlzLm9yZGVySW52b2ljZXNSZWZyZXNoZXIgPSBuZXcgT3JkZXJJbnZvaWNlc1JlZnJlc2hlcigpO1xuICAgIHRoaXMub3JkZXJQcm9kdWN0Q2FuY2VsID0gbmV3IE9yZGVyUHJvZHVjdENhbmNlbCgpO1xuICAgIHRoaXMucm91dGVyID0gbmV3IFJvdXRlcigpO1xuICAgIHRoaXMubGlzdGVuVG9FdmVudHMoKTtcbiAgfVxuXG4gIGxpc3RlblRvRXZlbnRzKCkge1xuICAgICQoT3JkZXJWaWV3UGFnZU1hcC5pbnZvaWNlQWRkcmVzc0VkaXRCdG4pLmZhbmN5Ym94KHtcbiAgICAgIHR5cGU6ICdpZnJhbWUnLFxuICAgICAgd2lkdGg6ICc5MCUnLFxuICAgICAgaGVpZ2h0OiAnOTAlJyxcbiAgICB9KTtcbiAgICAkKE9yZGVyVmlld1BhZ2VNYXAuZGVsaXZlcnlBZGRyZXNzRWRpdEJ0bikuZmFuY3lib3goe1xuICAgICAgdHlwZTogJ2lmcmFtZScsXG4gICAgICB3aWR0aDogJzkwJScsXG4gICAgICBoZWlnaHQ6ICc5MCUnLFxuICAgIH0pO1xuXG4gICAgRXZlbnRFbWl0dGVyLm9uKE9yZGVyVmlld0V2ZW50TWFwLnByb2R1Y3REZWxldGVkRnJvbU9yZGVyLCAoZXZlbnQpID0+IHtcbiAgICAgIHRoaXMub3JkZXJQcmljZXNSZWZyZXNoZXIucmVmcmVzaChldmVudC5vcmRlcklkKTtcbiAgICAgIHRoaXMub3JkZXJQYXltZW50c1JlZnJlc2hlci5yZWZyZXNoKGV2ZW50Lm9yZGVySWQpO1xuICAgICAgdGhpcy5yZWZyZXNoUHJvZHVjdHNMaXN0KGV2ZW50Lm9yZGVySWQpO1xuICAgICAgdGhpcy5vcmRlckRpc2NvdW50c1JlZnJlc2hlci5yZWZyZXNoKGV2ZW50Lm9yZGVySWQpO1xuICAgICAgdGhpcy5vcmRlckRvY3VtZW50c1JlZnJlc2hlci5yZWZyZXNoKGV2ZW50Lm9yZGVySWQpO1xuICAgICAgdGhpcy5vcmRlclNoaXBwaW5nUmVmcmVzaGVyLnJlZnJlc2goZXZlbnQub3JkZXJJZCk7XG4gICAgfSk7XG5cbiAgICBFdmVudEVtaXR0ZXIub24oT3JkZXJWaWV3RXZlbnRNYXAucHJvZHVjdEVkaXRpb25DYW5jZWxlZCwgKGV2ZW50KSA9PiB7XG4gICAgICB0aGlzLm9yZGVyUHJvZHVjdFJlbmRlcmVyLnJlc2V0RWRpdFJvdyhldmVudC5vcmRlckRldGFpbElkKTtcbiAgICAgIGNvbnN0IGVkaXRSb3dzTGVmdCA9ICQoT3JkZXJWaWV3UGFnZU1hcC5wcm9kdWN0RWRpdFJvdykubm90KE9yZGVyVmlld1BhZ2VNYXAucHJvZHVjdEVkaXRSb3dUZW1wbGF0ZSkubGVuZ3RoO1xuXG4gICAgICBpZiAoZWRpdFJvd3NMZWZ0ID4gMCkge1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG4gICAgICB0aGlzLm9yZGVyUHJvZHVjdFJlbmRlcmVyLm1vdmVQcm9kdWN0UGFuZWxUb09yaWdpbmFsUG9zaXRpb24oKTtcbiAgICB9KTtcblxuICAgIEV2ZW50RW1pdHRlci5vbihPcmRlclZpZXdFdmVudE1hcC5wcm9kdWN0VXBkYXRlZCwgKGV2ZW50KSA9PiB7XG4gICAgICB0aGlzLm9yZGVyUHJvZHVjdFJlbmRlcmVyLnJlc2V0RWRpdFJvdyhldmVudC5vcmRlckRldGFpbElkKTtcbiAgICAgIHRoaXMub3JkZXJQcmljZXNSZWZyZXNoZXIucmVmcmVzaChldmVudC5vcmRlcklkKTtcbiAgICAgIHRoaXMub3JkZXJQcmljZXNSZWZyZXNoZXIucmVmcmVzaFByb2R1Y3RQcmljZXMoZXZlbnQub3JkZXJJZCk7XG4gICAgICB0aGlzLnJlZnJlc2hQcm9kdWN0c0xpc3QoZXZlbnQub3JkZXJJZCk7XG4gICAgICB0aGlzLm9yZGVyUGF5bWVudHNSZWZyZXNoZXIucmVmcmVzaChldmVudC5vcmRlcklkKTtcbiAgICAgIHRoaXMub3JkZXJEaXNjb3VudHNSZWZyZXNoZXIucmVmcmVzaChldmVudC5vcmRlcklkKTtcbiAgICAgIHRoaXMub3JkZXJJbnZvaWNlc1JlZnJlc2hlci5yZWZyZXNoKGV2ZW50Lm9yZGVySWQpO1xuICAgICAgdGhpcy5vcmRlckRvY3VtZW50c1JlZnJlc2hlci5yZWZyZXNoKGV2ZW50Lm9yZGVySWQpO1xuICAgICAgdGhpcy5vcmRlclNoaXBwaW5nUmVmcmVzaGVyLnJlZnJlc2goZXZlbnQub3JkZXJJZCk7XG4gICAgICB0aGlzLmxpc3RlbkZvclByb2R1Y3REZWxldGUoKTtcbiAgICAgIHRoaXMubGlzdGVuRm9yUHJvZHVjdEVkaXQoKTtcbiAgICAgIHRoaXMucmVzZXRUb29sVGlwcygpO1xuXG4gICAgICBjb25zdCBlZGl0Um93c0xlZnQgPSAkKE9yZGVyVmlld1BhZ2VNYXAucHJvZHVjdEVkaXRSb3cpLm5vdChPcmRlclZpZXdQYWdlTWFwLnByb2R1Y3RFZGl0Um93VGVtcGxhdGUpLmxlbmd0aDtcblxuICAgICAgaWYgKGVkaXRSb3dzTGVmdCA+IDApIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuICAgICAgdGhpcy5vcmRlclByb2R1Y3RSZW5kZXJlci5tb3ZlUHJvZHVjdFBhbmVsVG9PcmlnaW5hbFBvc2l0aW9uKCk7XG4gICAgfSk7XG5cbiAgICBFdmVudEVtaXR0ZXIub24oT3JkZXJWaWV3RXZlbnRNYXAucHJvZHVjdEFkZGVkVG9PcmRlciwgKGV2ZW50KSA9PiB7XG4gICAgICB0aGlzLm9yZGVyUHJvZHVjdFJlbmRlcmVyLnJlc2V0QWRkUm93KCk7XG4gICAgICB0aGlzLm9yZGVyUHJpY2VzUmVmcmVzaGVyLnJlZnJlc2hQcm9kdWN0UHJpY2VzKGV2ZW50Lm9yZGVySWQpO1xuICAgICAgdGhpcy5vcmRlclByaWNlc1JlZnJlc2hlci5yZWZyZXNoKGV2ZW50Lm9yZGVySWQpO1xuICAgICAgdGhpcy5yZWZyZXNoUHJvZHVjdHNMaXN0KGV2ZW50Lm9yZGVySWQpO1xuICAgICAgdGhpcy5vcmRlclBheW1lbnRzUmVmcmVzaGVyLnJlZnJlc2goZXZlbnQub3JkZXJJZCk7XG4gICAgICB0aGlzLm9yZGVyRGlzY291bnRzUmVmcmVzaGVyLnJlZnJlc2goZXZlbnQub3JkZXJJZCk7XG4gICAgICB0aGlzLm9yZGVySW52b2ljZXNSZWZyZXNoZXIucmVmcmVzaChldmVudC5vcmRlcklkKTtcbiAgICAgIHRoaXMub3JkZXJEb2N1bWVudHNSZWZyZXNoZXIucmVmcmVzaChldmVudC5vcmRlcklkKTtcbiAgICAgIHRoaXMub3JkZXJTaGlwcGluZ1JlZnJlc2hlci5yZWZyZXNoKGV2ZW50Lm9yZGVySWQpO1xuICAgICAgdGhpcy5vcmRlclByb2R1Y3RSZW5kZXJlci5tb3ZlUHJvZHVjdFBhbmVsVG9PcmlnaW5hbFBvc2l0aW9uKCk7XG4gICAgfSk7XG4gIH1cblxuICBsaXN0ZW5Gb3JQcm9kdWN0RGVsZXRlKCkge1xuICAgICQoT3JkZXJWaWV3UGFnZU1hcC5wcm9kdWN0RGVsZXRlQnRuKVxuICAgICAgLm9mZignY2xpY2snKVxuICAgICAgLm9uKCdjbGljaycsIChldmVudCkgPT4gdGhpcy5vcmRlclByb2R1Y3RNYW5hZ2VyLmhhbmRsZURlbGV0ZVByb2R1Y3RFdmVudChldmVudCkpO1xuICB9XG5cbiAgcmVzZXRUb29sVGlwcygpIHtcbiAgICAkKE9yZGVyVmlld1BhZ2VNYXAucHJvZHVjdEVkaXRCdXR0b25zKS5wc3Rvb2x0aXAoKTtcbiAgICAkKE9yZGVyVmlld1BhZ2VNYXAucHJvZHVjdERlbGV0ZUJ0bikucHN0b29sdGlwKCk7XG4gIH1cblxuICBsaXN0ZW5Gb3JQcm9kdWN0RWRpdCgpIHtcbiAgICAkKE9yZGVyVmlld1BhZ2VNYXAucHJvZHVjdEVkaXRCdXR0b25zKS5vZmYoJ2NsaWNrJykub24oJ2NsaWNrJywgKGV2ZW50KSA9PiB7XG4gICAgICBjb25zdCAkYnRuID0gJChldmVudC5jdXJyZW50VGFyZ2V0KTtcbiAgICAgIHRoaXMub3JkZXJQcm9kdWN0UmVuZGVyZXIubW92ZVByb2R1Y3RzUGFuZWxUb01vZGlmaWNhdGlvblBvc2l0aW9uKCk7XG4gICAgICB0aGlzLm9yZGVyUHJvZHVjdFJlbmRlcmVyLmVkaXRQcm9kdWN0RnJvbUxpc3QoXG4gICAgICAgICRidG4uZGF0YSgnb3JkZXJEZXRhaWxJZCcpLFxuICAgICAgICAkYnRuLmRhdGEoJ3Byb2R1Y3RRdWFudGl0eScpLFxuICAgICAgICAkYnRuLmRhdGEoJ3Byb2R1Y3RQcmljZVRheEluY2wnKSxcbiAgICAgICAgJGJ0bi5kYXRhKCdwcm9kdWN0UHJpY2VUYXhFeGNsJyksXG4gICAgICAgICRidG4uZGF0YSgndGF4UmF0ZScpLFxuICAgICAgICAkYnRuLmRhdGEoJ2xvY2F0aW9uJyksXG4gICAgICAgICRidG4uZGF0YSgnYXZhaWxhYmxlUXVhbnRpdHknKSxcbiAgICAgICAgJGJ0bi5kYXRhKCdhdmFpbGFibGVPdXRPZlN0b2NrJyksXG4gICAgICAgICRidG4uZGF0YSgnb3JkZXJJbnZvaWNlSWQnKSxcbiAgICAgICAgJGJ0bi5kYXRhKCdpc09yZGVyVGF4SW5jbHVkZWQnKSxcbiAgICAgICk7XG4gICAgfSk7XG4gIH1cblxuICBsaXN0ZW5Gb3JQcm9kdWN0UGFjaygpIHtcbiAgICAkKE9yZGVyVmlld1BhZ2VNYXAucHJvZHVjdFBhY2tNb2RhbC5tb2RhbCkub24oJ3Nob3cuYnMubW9kYWwnLCAoZXZlbnQpID0+IHtcbiAgICAgIGNvbnN0IGJ1dHRvbiA9ICQoZXZlbnQucmVsYXRlZFRhcmdldCk7XG4gICAgICBjb25zdCBwYWNrSXRlbXMgPSBidXR0b24uZGF0YSgncGFja0l0ZW1zJyk7XG4gICAgICAkKE9yZGVyVmlld1BhZ2VNYXAucHJvZHVjdFBhY2tNb2RhbC5yb3dzKS5yZW1vdmUoKTtcbiAgICAgIHBhY2tJdGVtcy5mb3JFYWNoKChpdGVtKSA9PiB7XG4gICAgICAgIGNvbnN0ICRpdGVtID0gJChPcmRlclZpZXdQYWdlTWFwLnByb2R1Y3RQYWNrTW9kYWwudGVtcGxhdGUpLmNsb25lKCk7XG4gICAgICAgICRpdGVtLmF0dHIoJ2lkJywgYHByb2R1Y3RwYWNrXyR7aXRlbS5pZH1gKS5yZW1vdmVDbGFzcygnZC1ub25lJyk7XG4gICAgICAgICRpdGVtLmZpbmQoT3JkZXJWaWV3UGFnZU1hcC5wcm9kdWN0UGFja01vZGFsLnByb2R1Y3QuaW1nKS5hdHRyKCdzcmMnLCBpdGVtLmltYWdlUGF0aCk7XG4gICAgICAgICRpdGVtLmZpbmQoT3JkZXJWaWV3UGFnZU1hcC5wcm9kdWN0UGFja01vZGFsLnByb2R1Y3QubmFtZSkuaHRtbChpdGVtLm5hbWUpO1xuICAgICAgICAkaXRlbS5maW5kKE9yZGVyVmlld1BhZ2VNYXAucHJvZHVjdFBhY2tNb2RhbC5wcm9kdWN0LmxpbmspLmF0dHIoXG4gICAgICAgICAgJ2hyZWYnLFxuICAgICAgICAgIHRoaXMucm91dGVyLmdlbmVyYXRlKCdhZG1pbl9wcm9kdWN0X2Zvcm0nLCB7aWQ6IGl0ZW0uaWR9KSxcbiAgICAgICAgKTtcbiAgICAgICAgaWYgKGl0ZW0ucmVmZXJlbmNlICE9PSAnJykge1xuICAgICAgICAgICRpdGVtLmZpbmQoT3JkZXJWaWV3UGFnZU1hcC5wcm9kdWN0UGFja01vZGFsLnByb2R1Y3QucmVmKS5hcHBlbmQoaXRlbS5yZWZlcmVuY2UpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICRpdGVtLmZpbmQoT3JkZXJWaWV3UGFnZU1hcC5wcm9kdWN0UGFja01vZGFsLnByb2R1Y3QucmVmKS5yZW1vdmUoKTtcbiAgICAgICAgfVxuICAgICAgICBpZiAoaXRlbS5zdXBwbGllclJlZmVyZW5jZSAhPT0gJycpIHtcbiAgICAgICAgICAkaXRlbS5maW5kKE9yZGVyVmlld1BhZ2VNYXAucHJvZHVjdFBhY2tNb2RhbC5wcm9kdWN0LnN1cHBsaWVyUmVmKS5hcHBlbmQoaXRlbS5zdXBwbGllclJlZmVyZW5jZSk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgJGl0ZW0uZmluZChPcmRlclZpZXdQYWdlTWFwLnByb2R1Y3RQYWNrTW9kYWwucHJvZHVjdC5zdXBwbGllclJlZikucmVtb3ZlKCk7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKGl0ZW0ucXVhbnRpdHkgPiAxKSB7XG4gICAgICAgICAgJGl0ZW0uZmluZChgJHtPcmRlclZpZXdQYWdlTWFwLnByb2R1Y3RQYWNrTW9kYWwucHJvZHVjdC5xdWFudGl0eX0gc3BhbmApLmh0bWwoaXRlbS5xdWFudGl0eSk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgJGl0ZW0uZmluZChPcmRlclZpZXdQYWdlTWFwLnByb2R1Y3RQYWNrTW9kYWwucHJvZHVjdC5xdWFudGl0eSkuaHRtbChpdGVtLnF1YW50aXR5KTtcbiAgICAgICAgfVxuICAgICAgICAkaXRlbS5maW5kKE9yZGVyVmlld1BhZ2VNYXAucHJvZHVjdFBhY2tNb2RhbC5wcm9kdWN0LmF2YWlsYWJsZVF1YW50aXR5KS5odG1sKGl0ZW0uYXZhaWxhYmxlUXVhbnRpdHkpO1xuICAgICAgICAkKE9yZGVyVmlld1BhZ2VNYXAucHJvZHVjdFBhY2tNb2RhbC50ZW1wbGF0ZSkuYmVmb3JlKCRpdGVtKTtcbiAgICAgIH0pO1xuICAgIH0pO1xuICB9XG5cbiAgbGlzdGVuRm9yUHJvZHVjdEFkZCgpIHtcbiAgICAkKE9yZGVyVmlld1BhZ2VNYXAucHJvZHVjdEFkZEJ0bikub24oXG4gICAgICAnY2xpY2snLFxuICAgICAgKCkgPT4ge1xuICAgICAgICB0aGlzLm9yZGVyUHJvZHVjdFJlbmRlcmVyLnRvZ2dsZVByb2R1Y3RBZGROZXdJbnZvaWNlSW5mbygpO1xuICAgICAgICB0aGlzLm9yZGVyUHJvZHVjdFJlbmRlcmVyLm1vdmVQcm9kdWN0c1BhbmVsVG9Nb2RpZmljYXRpb25Qb3NpdGlvbihPcmRlclZpZXdQYWdlTWFwLnByb2R1Y3RTZWFyY2hJbnB1dCk7XG4gICAgICB9LFxuICAgICk7XG4gICAgJChPcmRlclZpZXdQYWdlTWFwLnByb2R1Y3RDYW5jZWxBZGRCdG4pLm9uKFxuICAgICAgJ2NsaWNrJywgKCkgPT4gdGhpcy5vcmRlclByb2R1Y3RSZW5kZXJlci5tb3ZlUHJvZHVjdFBhbmVsVG9PcmlnaW5hbFBvc2l0aW9uKCksXG4gICAgKTtcbiAgfVxuXG4gIGxpc3RlbkZvclByb2R1Y3RQYWdpbmF0aW9uKCkge1xuICAgICQoT3JkZXJWaWV3UGFnZU1hcC5wcm9kdWN0c1RhYmxlUGFnaW5hdGlvbikub24oJ2NsaWNrJywgT3JkZXJWaWV3UGFnZU1hcC5wcm9kdWN0c1RhYmxlUGFnaW5hdGlvbkxpbmssIChldmVudCkgPT4ge1xuICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgIGNvbnN0ICRidG4gPSAkKGV2ZW50LmN1cnJlbnRUYXJnZXQpO1xuICAgICAgRXZlbnRFbWl0dGVyLmVtaXQoT3JkZXJWaWV3RXZlbnRNYXAucHJvZHVjdExpc3RQYWdpbmF0ZWQsIHtcbiAgICAgICAgbnVtUGFnZTogJGJ0bi5kYXRhKCdwYWdlJyksXG4gICAgICB9KTtcbiAgICB9KTtcbiAgICAkKE9yZGVyVmlld1BhZ2VNYXAucHJvZHVjdHNUYWJsZVBhZ2luYXRpb25OZXh0KS5vbignY2xpY2snLCAoZXZlbnQpID0+IHtcbiAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICBjb25zdCAkYnRuID0gJChldmVudC5jdXJyZW50VGFyZ2V0KTtcblxuICAgICAgaWYgKCRidG4uaGFzQ2xhc3MoJ2Rpc2FibGVkJykpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuICAgICAgY29uc3QgYWN0aXZlUGFnZSA9IHRoaXMuZ2V0QWN0aXZlUGFnZSgpO1xuICAgICAgRXZlbnRFbWl0dGVyLmVtaXQoT3JkZXJWaWV3RXZlbnRNYXAucHJvZHVjdExpc3RQYWdpbmF0ZWQsIHtcbiAgICAgICAgbnVtUGFnZTogcGFyc2VJbnQoJChhY3RpdmVQYWdlKS5odG1sKCksIDEwKSArIDEsXG4gICAgICB9KTtcbiAgICB9KTtcbiAgICAkKE9yZGVyVmlld1BhZ2VNYXAucHJvZHVjdHNUYWJsZVBhZ2luYXRpb25QcmV2KS5vbignY2xpY2snLCAoZXZlbnQpID0+IHtcbiAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICBjb25zdCAkYnRuID0gJChldmVudC5jdXJyZW50VGFyZ2V0KTtcblxuICAgICAgaWYgKCRidG4uaGFzQ2xhc3MoJ2Rpc2FibGVkJykpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuICAgICAgY29uc3QgYWN0aXZlUGFnZSA9IHRoaXMuZ2V0QWN0aXZlUGFnZSgpO1xuICAgICAgRXZlbnRFbWl0dGVyLmVtaXQoT3JkZXJWaWV3RXZlbnRNYXAucHJvZHVjdExpc3RQYWdpbmF0ZWQsIHtcbiAgICAgICAgbnVtUGFnZTogcGFyc2VJbnQoJChhY3RpdmVQYWdlKS5odG1sKCksIDEwKSAtIDEsXG4gICAgICB9KTtcbiAgICB9KTtcbiAgICAkKE9yZGVyVmlld1BhZ2VNYXAucHJvZHVjdHNUYWJsZVBhZ2luYXRpb25OdW1iZXJTZWxlY3Rvcikub24oJ2NoYW5nZScsIChldmVudCkgPT4ge1xuICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgIGNvbnN0ICRzZWxlY3QgPSAkKGV2ZW50LmN1cnJlbnRUYXJnZXQpO1xuICAgICAgY29uc3QgbnVtUGVyUGFnZSA9IHBhcnNlSW50KCRzZWxlY3QudmFsKCksIDEwKTtcbiAgICAgIEV2ZW50RW1pdHRlci5lbWl0KE9yZGVyVmlld0V2ZW50TWFwLnByb2R1Y3RMaXN0TnVtYmVyUGVyUGFnZSwge1xuICAgICAgICBudW1QZXJQYWdlLFxuICAgICAgfSk7XG4gICAgfSk7XG5cbiAgICBFdmVudEVtaXR0ZXIub24oT3JkZXJWaWV3RXZlbnRNYXAucHJvZHVjdExpc3RQYWdpbmF0ZWQsIChldmVudCkgPT4ge1xuICAgICAgdGhpcy5vcmRlclByb2R1Y3RSZW5kZXJlci5wYWdpbmF0ZShldmVudC5udW1QYWdlKTtcbiAgICAgIHRoaXMubGlzdGVuRm9yUHJvZHVjdERlbGV0ZSgpO1xuICAgICAgdGhpcy5saXN0ZW5Gb3JQcm9kdWN0RWRpdCgpO1xuICAgICAgdGhpcy5yZXNldFRvb2xUaXBzKCk7XG4gICAgfSk7XG5cbiAgICBFdmVudEVtaXR0ZXIub24oT3JkZXJWaWV3RXZlbnRNYXAucHJvZHVjdExpc3ROdW1iZXJQZXJQYWdlLCAoZXZlbnQpID0+IHtcbiAgICAgIC8vIFVwZGF0ZSBwYWdpbmF0aW9uIG51bSBwZXIgcGFnZSAocGFnZSBsaW5rcyBhcmUgcmVnZW5lcmF0ZWQpXG4gICAgICB0aGlzLm9yZGVyUHJvZHVjdFJlbmRlcmVyLnVwZGF0ZU51bVBlclBhZ2UoZXZlbnQubnVtUGVyUGFnZSk7XG5cbiAgICAgIC8vIFBhZ2luYXRlIHRvIHBhZ2UgMVxuICAgICAgRXZlbnRFbWl0dGVyLmVtaXQoT3JkZXJWaWV3RXZlbnRNYXAucHJvZHVjdExpc3RQYWdpbmF0ZWQsIHtcbiAgICAgICAgbnVtUGFnZTogMSxcbiAgICAgIH0pO1xuXG4gICAgICAvLyBTYXZlIG5ldyBjb25maWdcbiAgICAgICQuYWpheCh7XG4gICAgICAgIHVybDogdGhpcy5yb3V0ZXIuZ2VuZXJhdGUoJ2FkbWluX29yZGVyc19jb25maWd1cmVfcHJvZHVjdF9wYWdpbmF0aW9uJyksXG4gICAgICAgIG1ldGhvZDogJ1BPU1QnLFxuICAgICAgICBkYXRhOiB7bnVtUGVyUGFnZTogZXZlbnQubnVtUGVyUGFnZX0sXG4gICAgICB9KTtcbiAgICB9KTtcbiAgfVxuXG4gIGxpc3RlbkZvclJlZnVuZCgpIHtcbiAgICAkKE9yZGVyVmlld1BhZ2VNYXAuY2FuY2VsUHJvZHVjdC5idXR0b25zLnBhcnRpYWxSZWZ1bmQpLm9uKCdjbGljaycsICgpID0+IHtcbiAgICAgIHRoaXMub3JkZXJQcm9kdWN0UmVuZGVyZXIubW92ZVByb2R1Y3RzUGFuZWxUb1JlZnVuZFBvc2l0aW9uKCk7XG4gICAgICB0aGlzLm9yZGVyUHJvZHVjdENhbmNlbC5zaG93UGFydGlhbFJlZnVuZCgpO1xuICAgIH0pO1xuXG4gICAgJChPcmRlclZpZXdQYWdlTWFwLmNhbmNlbFByb2R1Y3QuYnV0dG9ucy5zdGFuZGFyZFJlZnVuZCkub24oJ2NsaWNrJywgKCkgPT4ge1xuICAgICAgdGhpcy5vcmRlclByb2R1Y3RSZW5kZXJlci5tb3ZlUHJvZHVjdHNQYW5lbFRvUmVmdW5kUG9zaXRpb24oKTtcbiAgICAgIHRoaXMub3JkZXJQcm9kdWN0Q2FuY2VsLnNob3dTdGFuZGFyZFJlZnVuZCgpO1xuICAgIH0pO1xuXG4gICAgJChPcmRlclZpZXdQYWdlTWFwLmNhbmNlbFByb2R1Y3QuYnV0dG9ucy5yZXR1cm5Qcm9kdWN0KS5vbignY2xpY2snLCAoKSA9PiB7XG4gICAgICB0aGlzLm9yZGVyUHJvZHVjdFJlbmRlcmVyLm1vdmVQcm9kdWN0c1BhbmVsVG9SZWZ1bmRQb3NpdGlvbigpO1xuICAgICAgdGhpcy5vcmRlclByb2R1Y3RDYW5jZWwuc2hvd1JldHVyblByb2R1Y3QoKTtcbiAgICB9KTtcblxuICAgICQoT3JkZXJWaWV3UGFnZU1hcC5jYW5jZWxQcm9kdWN0LmJ1dHRvbnMuYWJvcnQpLm9uKCdjbGljaycsICgpID0+IHtcbiAgICAgIHRoaXMub3JkZXJQcm9kdWN0UmVuZGVyZXIubW92ZVByb2R1Y3RQYW5lbFRvT3JpZ2luYWxQb3NpdGlvbigpO1xuICAgICAgdGhpcy5vcmRlclByb2R1Y3RDYW5jZWwuaGlkZVJlZnVuZCgpO1xuICAgIH0pO1xuICB9XG5cbiAgbGlzdGVuRm9yQ2FuY2VsUHJvZHVjdCgpIHtcbiAgICAkKE9yZGVyVmlld1BhZ2VNYXAuY2FuY2VsUHJvZHVjdC5idXR0b25zLmNhbmNlbFByb2R1Y3RzKS5vbignY2xpY2snLCAoKSA9PiB7XG4gICAgICB0aGlzLm9yZGVyUHJvZHVjdFJlbmRlcmVyLm1vdmVQcm9kdWN0c1BhbmVsVG9SZWZ1bmRQb3NpdGlvbigpO1xuICAgICAgdGhpcy5vcmRlclByb2R1Y3RDYW5jZWwuc2hvd0NhbmNlbFByb2R1Y3RGb3JtKCk7XG4gICAgfSk7XG4gIH1cblxuICBnZXRBY3RpdmVQYWdlKCkge1xuICAgIHJldHVybiAkKE9yZGVyVmlld1BhZ2VNYXAucHJvZHVjdHNUYWJsZVBhZ2luYXRpb24pLmZpbmQoJy5hY3RpdmUgc3BhbicpLmdldCgwKTtcbiAgfVxuXG4gIHJlZnJlc2hQcm9kdWN0c0xpc3Qob3JkZXJJZCkge1xuICAgICQoT3JkZXJWaWV3UGFnZU1hcC5yZWZyZXNoUHJvZHVjdHNMaXN0TG9hZGluZ1NwaW5uZXIpLnNob3coKTtcblxuICAgIGNvbnN0ICR0YWJsZVBhZ2luYXRpb24gPSAkKE9yZGVyVmlld1BhZ2VNYXAucHJvZHVjdHNUYWJsZVBhZ2luYXRpb24pO1xuICAgIGNvbnN0IG51bVJvd3NQZXJQYWdlID0gJHRhYmxlUGFnaW5hdGlvbi5kYXRhKCdudW1QZXJQYWdlJyk7XG4gICAgY29uc3QgaW5pdGlhbE51bVByb2R1Y3RzID0gJChPcmRlclZpZXdQYWdlTWFwLnByb2R1Y3RzVGFibGVSb3dzKS5sZW5ndGg7XG4gICAgY29uc3QgY3VycmVudFBhZ2UgPSBwYXJzZUludCgkKE9yZGVyVmlld1BhZ2VNYXAucHJvZHVjdHNUYWJsZVBhZ2luYXRpb25BY3RpdmUpLmh0bWwoKSwgMTApO1xuXG4gICAgJC5hamF4KHRoaXMucm91dGVyLmdlbmVyYXRlKCdhZG1pbl9vcmRlcnNfZ2V0X3Byb2R1Y3RzJywge29yZGVySWR9KSlcbiAgICAgIC5kb25lKChyZXNwb25zZSkgPT4ge1xuICAgICAgICAvLyBEZWxldGUgcHJldmlvdXMgcHJvZHVjdCBsaW5lc1xuICAgICAgICAkKE9yZGVyVmlld1BhZ2VNYXAucHJvZHVjdHNUYWJsZSkuZmluZChPcmRlclZpZXdQYWdlTWFwLnByb2R1Y3RzVGFibGVSb3dzKS5yZW1vdmUoKTtcbiAgICAgICAgJChPcmRlclZpZXdQYWdlTWFwLnByb2R1Y3RzVGFibGVDdXN0b21pemF0aW9uUm93cykucmVtb3ZlKCk7XG5cbiAgICAgICAgJChgJHtPcmRlclZpZXdQYWdlTWFwLnByb2R1Y3RzVGFibGV9IHRib2R5YCkucHJlcGVuZChyZXNwb25zZSk7XG5cbiAgICAgICAgJChPcmRlclZpZXdQYWdlTWFwLnJlZnJlc2hQcm9kdWN0c0xpc3RMb2FkaW5nU3Bpbm5lcikuaGlkZSgpO1xuXG4gICAgICAgIGNvbnN0IG5ld051bVByb2R1Y3RzID0gJChPcmRlclZpZXdQYWdlTWFwLnByb2R1Y3RzVGFibGVSb3dzKS5sZW5ndGg7XG4gICAgICAgIGNvbnN0IG5ld1BhZ2VzTnVtID0gTWF0aC5jZWlsKG5ld051bVByb2R1Y3RzIC8gbnVtUm93c1BlclBhZ2UpO1xuXG4gICAgICAgIHRoaXMub3JkZXJQcm9kdWN0UmVuZGVyZXIudXBkYXRlTnVtUHJvZHVjdHMobmV3TnVtUHJvZHVjdHMpO1xuICAgICAgICB0aGlzLm9yZGVyUHJvZHVjdFJlbmRlcmVyLnVwZGF0ZVBhZ2luYXRpb25Db250cm9scygpO1xuXG4gICAgICAgIGxldCBudW1QYWdlID0gMTtcbiAgICAgICAgbGV0IG1lc3NhZ2UgPSAnJztcblxuICAgICAgICAvLyBEaXNwbGF5IGFsZXJ0XG4gICAgICAgIGlmIChpbml0aWFsTnVtUHJvZHVjdHMgPiBuZXdOdW1Qcm9kdWN0cykgeyAvLyBwcm9kdWN0IGRlbGV0ZWRcbiAgICAgICAgICBtZXNzYWdlID0gKGluaXRpYWxOdW1Qcm9kdWN0cyAtIG5ld051bVByb2R1Y3RzID09PSAxKVxuICAgICAgICAgICAgPyB3aW5kb3cudHJhbnNsYXRlX2phdmFzY3JpcHRzWydUaGUgcHJvZHVjdCB3YXMgc3VjY2Vzc2Z1bGx5IHJlbW92ZWQuJ11cbiAgICAgICAgICAgIDogd2luZG93LnRyYW5zbGF0ZV9qYXZhc2NyaXB0c1snWzFdIHByb2R1Y3RzIHdlcmUgc3VjY2Vzc2Z1bGx5IHJlbW92ZWQuJ11cbiAgICAgICAgICAgICAgLnJlcGxhY2UoJ1sxXScsIChpbml0aWFsTnVtUHJvZHVjdHMgLSBuZXdOdW1Qcm9kdWN0cykpO1xuXG4gICAgICAgICAgLy8gU2V0IHRhcmdldCBwYWdlIHRvIHRoZSBwYWdlIG9mIHRoZSBkZWxldGVkIGl0ZW1cbiAgICAgICAgICBudW1QYWdlID0gKG5ld1BhZ2VzTnVtID09PSAxKSA/IDEgOiBjdXJyZW50UGFnZTtcbiAgICAgICAgfSBlbHNlIGlmIChpbml0aWFsTnVtUHJvZHVjdHMgPCBuZXdOdW1Qcm9kdWN0cykgeyAvLyBwcm9kdWN0IGFkZGVkXG4gICAgICAgICAgbWVzc2FnZSA9IChuZXdOdW1Qcm9kdWN0cyAtIGluaXRpYWxOdW1Qcm9kdWN0cyA9PT0gMSlcbiAgICAgICAgICAgID8gd2luZG93LnRyYW5zbGF0ZV9qYXZhc2NyaXB0c1snVGhlIHByb2R1Y3Qgd2FzIHN1Y2Nlc3NmdWxseSBhZGRlZC4nXVxuICAgICAgICAgICAgOiB3aW5kb3cudHJhbnNsYXRlX2phdmFzY3JpcHRzWydbMV0gcHJvZHVjdHMgd2VyZSBzdWNjZXNzZnVsbHkgYWRkZWQuJ11cbiAgICAgICAgICAgICAgLnJlcGxhY2UoJ1sxXScsIChuZXdOdW1Qcm9kdWN0cyAtIGluaXRpYWxOdW1Qcm9kdWN0cykpO1xuXG4gICAgICAgICAgLy8gTW92ZSB0byBmaXJzdCBwYWdlIHRvIHNlZSB0aGUgYWRkZWQgcHJvZHVjdFxuICAgICAgICAgIG51bVBhZ2UgPSAxO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKG1lc3NhZ2UgIT09ICcnKSB7XG4gICAgICAgICAgJC5ncm93bC5ub3RpY2Uoe1xuICAgICAgICAgICAgdGl0bGU6ICcnLFxuICAgICAgICAgICAgbWVzc2FnZSxcbiAgICAgICAgICB9KTtcbiAgICAgICAgfVxuXG4gICAgICAgIC8vIE1vdmUgdG8gcGFnZSBvZiB0aGUgbW9kaWZpZWQgaXRlbVxuICAgICAgICBFdmVudEVtaXR0ZXIuZW1pdChPcmRlclZpZXdFdmVudE1hcC5wcm9kdWN0TGlzdFBhZ2luYXRlZCwge1xuICAgICAgICAgIG51bVBhZ2UsXG4gICAgICAgIH0pO1xuXG4gICAgICAgIC8vIEJpbmQgaG92ZXIgb24gcHJvZHVjdCByb3dzIGJ1dHRvbnNcbiAgICAgICAgdGhpcy5yZXNldFRvb2xUaXBzKCk7XG4gICAgICB9KVxuICAgICAgLmZhaWwoKCkgPT4ge1xuICAgICAgICAkLmdyb3dsLmVycm9yKHtcbiAgICAgICAgICB0aXRsZTogJycsXG4gICAgICAgICAgbWVzc2FnZTogJ0ZhaWxlZCB0byByZWxvYWQgdGhlIHByb2R1Y3RzIGxpc3QuIFBsZWFzZSByZWxvYWQgdGhlIHBhZ2UnLFxuICAgICAgICB9KTtcbiAgICAgIH0pO1xuICB9XG59XG4iLCJtb2R1bGUuZXhwb3J0cyA9IHsgXCJkZWZhdWx0XCI6IHJlcXVpcmUoXCJjb3JlLWpzL2xpYnJhcnkvZm4vYXJyYXkvZnJvbVwiKSwgX19lc01vZHVsZTogdHJ1ZSB9OyIsIm1vZHVsZS5leHBvcnRzID0geyBcImRlZmF1bHRcIjogcmVxdWlyZShcImNvcmUtanMvbGlicmFyeS9mbi9nZXQtaXRlcmF0b3JcIiksIF9fZXNNb2R1bGU6IHRydWUgfTsiLCJtb2R1bGUuZXhwb3J0cyA9IHsgXCJkZWZhdWx0XCI6IHJlcXVpcmUoXCJjb3JlLWpzL2xpYnJhcnkvZm4vaXMtaXRlcmFibGVcIiksIF9fZXNNb2R1bGU6IHRydWUgfTsiLCJtb2R1bGUuZXhwb3J0cyA9IHsgXCJkZWZhdWx0XCI6IHJlcXVpcmUoXCJjb3JlLWpzL2xpYnJhcnkvZm4vbnVtYmVyL2lzLW5hblwiKSwgX19lc01vZHVsZTogdHJ1ZSB9OyIsIm1vZHVsZS5leHBvcnRzID0geyBcImRlZmF1bHRcIjogcmVxdWlyZShcImNvcmUtanMvbGlicmFyeS9mbi9vYmplY3QvYXNzaWduXCIpLCBfX2VzTW9kdWxlOiB0cnVlIH07IiwibW9kdWxlLmV4cG9ydHMgPSB7IFwiZGVmYXVsdFwiOiByZXF1aXJlKFwiY29yZS1qcy9saWJyYXJ5L2ZuL29iamVjdC9jcmVhdGVcIiksIF9fZXNNb2R1bGU6IHRydWUgfTsiLCJtb2R1bGUuZXhwb3J0cyA9IHsgXCJkZWZhdWx0XCI6IHJlcXVpcmUoXCJjb3JlLWpzL2xpYnJhcnkvZm4vb2JqZWN0L2RlZmluZS1wcm9wZXJ0eVwiKSwgX19lc01vZHVsZTogdHJ1ZSB9OyIsIm1vZHVsZS5leHBvcnRzID0geyBcImRlZmF1bHRcIjogcmVxdWlyZShcImNvcmUtanMvbGlicmFyeS9mbi9vYmplY3QvZ2V0LXByb3RvdHlwZS1vZlwiKSwgX19lc01vZHVsZTogdHJ1ZSB9OyIsIm1vZHVsZS5leHBvcnRzID0geyBcImRlZmF1bHRcIjogcmVxdWlyZShcImNvcmUtanMvbGlicmFyeS9mbi9vYmplY3Qva2V5c1wiKSwgX19lc01vZHVsZTogdHJ1ZSB9OyIsIm1vZHVsZS5leHBvcnRzID0geyBcImRlZmF1bHRcIjogcmVxdWlyZShcImNvcmUtanMvbGlicmFyeS9mbi9vYmplY3Qvc2V0LXByb3RvdHlwZS1vZlwiKSwgX19lc01vZHVsZTogdHJ1ZSB9OyIsIm1vZHVsZS5leHBvcnRzID0geyBcImRlZmF1bHRcIjogcmVxdWlyZShcImNvcmUtanMvbGlicmFyeS9mbi9vYmplY3QvdmFsdWVzXCIpLCBfX2VzTW9kdWxlOiB0cnVlIH07IiwibW9kdWxlLmV4cG9ydHMgPSB7IFwiZGVmYXVsdFwiOiByZXF1aXJlKFwiY29yZS1qcy9saWJyYXJ5L2ZuL3N5bWJvbFwiKSwgX19lc01vZHVsZTogdHJ1ZSB9OyIsIm1vZHVsZS5leHBvcnRzID0geyBcImRlZmF1bHRcIjogcmVxdWlyZShcImNvcmUtanMvbGlicmFyeS9mbi9zeW1ib2wvaXRlcmF0b3JcIiksIF9fZXNNb2R1bGU6IHRydWUgfTsiLCJcInVzZSBzdHJpY3RcIjtcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gZnVuY3Rpb24gKGluc3RhbmNlLCBDb25zdHJ1Y3Rvcikge1xuICBpZiAoIShpbnN0YW5jZSBpbnN0YW5jZW9mIENvbnN0cnVjdG9yKSkge1xuICAgIHRocm93IG5ldyBUeXBlRXJyb3IoXCJDYW5ub3QgY2FsbCBhIGNsYXNzIGFzIGEgZnVuY3Rpb25cIik7XG4gIH1cbn07IiwiXCJ1c2Ugc3RyaWN0XCI7XG5cbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG5cbnZhciBfZGVmaW5lUHJvcGVydHkgPSByZXF1aXJlKFwiLi4vY29yZS1qcy9vYmplY3QvZGVmaW5lLXByb3BlcnR5XCIpO1xuXG52YXIgX2RlZmluZVByb3BlcnR5MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2RlZmluZVByb3BlcnR5KTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxuZXhwb3J0cy5kZWZhdWx0ID0gZnVuY3Rpb24gKCkge1xuICBmdW5jdGlvbiBkZWZpbmVQcm9wZXJ0aWVzKHRhcmdldCwgcHJvcHMpIHtcbiAgICBmb3IgKHZhciBpID0gMDsgaSA8IHByb3BzLmxlbmd0aDsgaSsrKSB7XG4gICAgICB2YXIgZGVzY3JpcHRvciA9IHByb3BzW2ldO1xuICAgICAgZGVzY3JpcHRvci5lbnVtZXJhYmxlID0gZGVzY3JpcHRvci5lbnVtZXJhYmxlIHx8IGZhbHNlO1xuICAgICAgZGVzY3JpcHRvci5jb25maWd1cmFibGUgPSB0cnVlO1xuICAgICAgaWYgKFwidmFsdWVcIiBpbiBkZXNjcmlwdG9yKSBkZXNjcmlwdG9yLndyaXRhYmxlID0gdHJ1ZTtcbiAgICAgICgwLCBfZGVmaW5lUHJvcGVydHkyLmRlZmF1bHQpKHRhcmdldCwgZGVzY3JpcHRvci5rZXksIGRlc2NyaXB0b3IpO1xuICAgIH1cbiAgfVxuXG4gIHJldHVybiBmdW5jdGlvbiAoQ29uc3RydWN0b3IsIHByb3RvUHJvcHMsIHN0YXRpY1Byb3BzKSB7XG4gICAgaWYgKHByb3RvUHJvcHMpIGRlZmluZVByb3BlcnRpZXMoQ29uc3RydWN0b3IucHJvdG90eXBlLCBwcm90b1Byb3BzKTtcbiAgICBpZiAoc3RhdGljUHJvcHMpIGRlZmluZVByb3BlcnRpZXMoQ29uc3RydWN0b3IsIHN0YXRpY1Byb3BzKTtcbiAgICByZXR1cm4gQ29uc3RydWN0b3I7XG4gIH07XG59KCk7IiwiXCJ1c2Ugc3RyaWN0XCI7XG5cbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG5cbnZhciBfc2V0UHJvdG90eXBlT2YgPSByZXF1aXJlKFwiLi4vY29yZS1qcy9vYmplY3Qvc2V0LXByb3RvdHlwZS1vZlwiKTtcblxudmFyIF9zZXRQcm90b3R5cGVPZjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9zZXRQcm90b3R5cGVPZik7XG5cbnZhciBfY3JlYXRlID0gcmVxdWlyZShcIi4uL2NvcmUtanMvb2JqZWN0L2NyZWF0ZVwiKTtcblxudmFyIF9jcmVhdGUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY3JlYXRlKTtcblxudmFyIF90eXBlb2YyID0gcmVxdWlyZShcIi4uL2hlbHBlcnMvdHlwZW9mXCIpO1xuXG52YXIgX3R5cGVvZjMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF90eXBlb2YyKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxuZXhwb3J0cy5kZWZhdWx0ID0gZnVuY3Rpb24gKHN1YkNsYXNzLCBzdXBlckNsYXNzKSB7XG4gIGlmICh0eXBlb2Ygc3VwZXJDbGFzcyAhPT0gXCJmdW5jdGlvblwiICYmIHN1cGVyQ2xhc3MgIT09IG51bGwpIHtcbiAgICB0aHJvdyBuZXcgVHlwZUVycm9yKFwiU3VwZXIgZXhwcmVzc2lvbiBtdXN0IGVpdGhlciBiZSBudWxsIG9yIGEgZnVuY3Rpb24sIG5vdCBcIiArICh0eXBlb2Ygc3VwZXJDbGFzcyA9PT0gXCJ1bmRlZmluZWRcIiA/IFwidW5kZWZpbmVkXCIgOiAoMCwgX3R5cGVvZjMuZGVmYXVsdCkoc3VwZXJDbGFzcykpKTtcbiAgfVxuXG4gIHN1YkNsYXNzLnByb3RvdHlwZSA9ICgwLCBfY3JlYXRlMi5kZWZhdWx0KShzdXBlckNsYXNzICYmIHN1cGVyQ2xhc3MucHJvdG90eXBlLCB7XG4gICAgY29uc3RydWN0b3I6IHtcbiAgICAgIHZhbHVlOiBzdWJDbGFzcyxcbiAgICAgIGVudW1lcmFibGU6IGZhbHNlLFxuICAgICAgd3JpdGFibGU6IHRydWUsXG4gICAgICBjb25maWd1cmFibGU6IHRydWVcbiAgICB9XG4gIH0pO1xuICBpZiAoc3VwZXJDbGFzcykgX3NldFByb3RvdHlwZU9mMi5kZWZhdWx0ID8gKDAsIF9zZXRQcm90b3R5cGVPZjIuZGVmYXVsdCkoc3ViQ2xhc3MsIHN1cGVyQ2xhc3MpIDogc3ViQ2xhc3MuX19wcm90b19fID0gc3VwZXJDbGFzcztcbn07IiwiXCJ1c2Ugc3RyaWN0XCI7XG5cbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG5cbnZhciBfdHlwZW9mMiA9IHJlcXVpcmUoXCIuLi9oZWxwZXJzL3R5cGVvZlwiKTtcblxudmFyIF90eXBlb2YzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfdHlwZW9mMik7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbmV4cG9ydHMuZGVmYXVsdCA9IGZ1bmN0aW9uIChzZWxmLCBjYWxsKSB7XG4gIGlmICghc2VsZikge1xuICAgIHRocm93IG5ldyBSZWZlcmVuY2VFcnJvcihcInRoaXMgaGFzbid0IGJlZW4gaW5pdGlhbGlzZWQgLSBzdXBlcigpIGhhc24ndCBiZWVuIGNhbGxlZFwiKTtcbiAgfVxuXG4gIHJldHVybiBjYWxsICYmICgodHlwZW9mIGNhbGwgPT09IFwidW5kZWZpbmVkXCIgPyBcInVuZGVmaW5lZFwiIDogKDAsIF90eXBlb2YzLmRlZmF1bHQpKGNhbGwpKSA9PT0gXCJvYmplY3RcIiB8fCB0eXBlb2YgY2FsbCA9PT0gXCJmdW5jdGlvblwiKSA/IGNhbGwgOiBzZWxmO1xufTsiLCJcInVzZSBzdHJpY3RcIjtcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcblxudmFyIF9pc0l0ZXJhYmxlMiA9IHJlcXVpcmUoXCIuLi9jb3JlLWpzL2lzLWl0ZXJhYmxlXCIpO1xuXG52YXIgX2lzSXRlcmFibGUzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfaXNJdGVyYWJsZTIpO1xuXG52YXIgX2dldEl0ZXJhdG9yMiA9IHJlcXVpcmUoXCIuLi9jb3JlLWpzL2dldC1pdGVyYXRvclwiKTtcblxudmFyIF9nZXRJdGVyYXRvcjMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9nZXRJdGVyYXRvcjIpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG5leHBvcnRzLmRlZmF1bHQgPSBmdW5jdGlvbiAoKSB7XG4gIGZ1bmN0aW9uIHNsaWNlSXRlcmF0b3IoYXJyLCBpKSB7XG4gICAgdmFyIF9hcnIgPSBbXTtcbiAgICB2YXIgX24gPSB0cnVlO1xuICAgIHZhciBfZCA9IGZhbHNlO1xuICAgIHZhciBfZSA9IHVuZGVmaW5lZDtcblxuICAgIHRyeSB7XG4gICAgICBmb3IgKHZhciBfaSA9ICgwLCBfZ2V0SXRlcmF0b3IzLmRlZmF1bHQpKGFyciksIF9zOyAhKF9uID0gKF9zID0gX2kubmV4dCgpKS5kb25lKTsgX24gPSB0cnVlKSB7XG4gICAgICAgIF9hcnIucHVzaChfcy52YWx1ZSk7XG5cbiAgICAgICAgaWYgKGkgJiYgX2Fyci5sZW5ndGggPT09IGkpIGJyZWFrO1xuICAgICAgfVxuICAgIH0gY2F0Y2ggKGVycikge1xuICAgICAgX2QgPSB0cnVlO1xuICAgICAgX2UgPSBlcnI7XG4gICAgfSBmaW5hbGx5IHtcbiAgICAgIHRyeSB7XG4gICAgICAgIGlmICghX24gJiYgX2lbXCJyZXR1cm5cIl0pIF9pW1wicmV0dXJuXCJdKCk7XG4gICAgICB9IGZpbmFsbHkge1xuICAgICAgICBpZiAoX2QpIHRocm93IF9lO1xuICAgICAgfVxuICAgIH1cblxuICAgIHJldHVybiBfYXJyO1xuICB9XG5cbiAgcmV0dXJuIGZ1bmN0aW9uIChhcnIsIGkpIHtcbiAgICBpZiAoQXJyYXkuaXNBcnJheShhcnIpKSB7XG4gICAgICByZXR1cm4gYXJyO1xuICAgIH0gZWxzZSBpZiAoKDAsIF9pc0l0ZXJhYmxlMy5kZWZhdWx0KShPYmplY3QoYXJyKSkpIHtcbiAgICAgIHJldHVybiBzbGljZUl0ZXJhdG9yKGFyciwgaSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHRocm93IG5ldyBUeXBlRXJyb3IoXCJJbnZhbGlkIGF0dGVtcHQgdG8gZGVzdHJ1Y3R1cmUgbm9uLWl0ZXJhYmxlIGluc3RhbmNlXCIpO1xuICAgIH1cbiAgfTtcbn0oKTsiLCJcInVzZSBzdHJpY3RcIjtcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcblxudmFyIF9mcm9tID0gcmVxdWlyZShcIi4uL2NvcmUtanMvYXJyYXkvZnJvbVwiKTtcblxudmFyIF9mcm9tMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2Zyb20pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG5leHBvcnRzLmRlZmF1bHQgPSBmdW5jdGlvbiAoYXJyKSB7XG4gIGlmIChBcnJheS5pc0FycmF5KGFycikpIHtcbiAgICBmb3IgKHZhciBpID0gMCwgYXJyMiA9IEFycmF5KGFyci5sZW5ndGgpOyBpIDwgYXJyLmxlbmd0aDsgaSsrKSB7XG4gICAgICBhcnIyW2ldID0gYXJyW2ldO1xuICAgIH1cblxuICAgIHJldHVybiBhcnIyO1xuICB9IGVsc2Uge1xuICAgIHJldHVybiAoMCwgX2Zyb20yLmRlZmF1bHQpKGFycik7XG4gIH1cbn07IiwiXCJ1c2Ugc3RyaWN0XCI7XG5cbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG5cbnZhciBfaXRlcmF0b3IgPSByZXF1aXJlKFwiLi4vY29yZS1qcy9zeW1ib2wvaXRlcmF0b3JcIik7XG5cbnZhciBfaXRlcmF0b3IyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfaXRlcmF0b3IpO1xuXG52YXIgX3N5bWJvbCA9IHJlcXVpcmUoXCIuLi9jb3JlLWpzL3N5bWJvbFwiKTtcblxudmFyIF9zeW1ib2wyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfc3ltYm9sKTtcblxudmFyIF90eXBlb2YgPSB0eXBlb2YgX3N5bWJvbDIuZGVmYXVsdCA9PT0gXCJmdW5jdGlvblwiICYmIHR5cGVvZiBfaXRlcmF0b3IyLmRlZmF1bHQgPT09IFwic3ltYm9sXCIgPyBmdW5jdGlvbiAob2JqKSB7IHJldHVybiB0eXBlb2Ygb2JqOyB9IDogZnVuY3Rpb24gKG9iaikgeyByZXR1cm4gb2JqICYmIHR5cGVvZiBfc3ltYm9sMi5kZWZhdWx0ID09PSBcImZ1bmN0aW9uXCIgJiYgb2JqLmNvbnN0cnVjdG9yID09PSBfc3ltYm9sMi5kZWZhdWx0ICYmIG9iaiAhPT0gX3N5bWJvbDIuZGVmYXVsdC5wcm90b3R5cGUgPyBcInN5bWJvbFwiIDogdHlwZW9mIG9iajsgfTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxuZXhwb3J0cy5kZWZhdWx0ID0gdHlwZW9mIF9zeW1ib2wyLmRlZmF1bHQgPT09IFwiZnVuY3Rpb25cIiAmJiBfdHlwZW9mKF9pdGVyYXRvcjIuZGVmYXVsdCkgPT09IFwic3ltYm9sXCIgPyBmdW5jdGlvbiAob2JqKSB7XG4gIHJldHVybiB0eXBlb2Ygb2JqID09PSBcInVuZGVmaW5lZFwiID8gXCJ1bmRlZmluZWRcIiA6IF90eXBlb2Yob2JqKTtcbn0gOiBmdW5jdGlvbiAob2JqKSB7XG4gIHJldHVybiBvYmogJiYgdHlwZW9mIF9zeW1ib2wyLmRlZmF1bHQgPT09IFwiZnVuY3Rpb25cIiAmJiBvYmouY29uc3RydWN0b3IgPT09IF9zeW1ib2wyLmRlZmF1bHQgJiYgb2JqICE9PSBfc3ltYm9sMi5kZWZhdWx0LnByb3RvdHlwZSA/IFwic3ltYm9sXCIgOiB0eXBlb2Ygb2JqID09PSBcInVuZGVmaW5lZFwiID8gXCJ1bmRlZmluZWRcIiA6IF90eXBlb2Yob2JqKTtcbn07IiwicmVxdWlyZSgnLi4vLi4vbW9kdWxlcy9lczYuc3RyaW5nLml0ZXJhdG9yJyk7XG5yZXF1aXJlKCcuLi8uLi9tb2R1bGVzL2VzNi5hcnJheS5mcm9tJyk7XG5tb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoJy4uLy4uL21vZHVsZXMvX2NvcmUnKS5BcnJheS5mcm9tO1xuIiwicmVxdWlyZSgnLi4vbW9kdWxlcy93ZWIuZG9tLml0ZXJhYmxlJyk7XG5yZXF1aXJlKCcuLi9tb2R1bGVzL2VzNi5zdHJpbmcuaXRlcmF0b3InKTtcbm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZSgnLi4vbW9kdWxlcy9jb3JlLmdldC1pdGVyYXRvcicpO1xuIiwicmVxdWlyZSgnLi4vbW9kdWxlcy93ZWIuZG9tLml0ZXJhYmxlJyk7XG5yZXF1aXJlKCcuLi9tb2R1bGVzL2VzNi5zdHJpbmcuaXRlcmF0b3InKTtcbm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZSgnLi4vbW9kdWxlcy9jb3JlLmlzLWl0ZXJhYmxlJyk7XG4iLCJyZXF1aXJlKCcuLi8uLi9tb2R1bGVzL2VzNi5udW1iZXIuaXMtbmFuJyk7XG5tb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoJy4uLy4uL21vZHVsZXMvX2NvcmUnKS5OdW1iZXIuaXNOYU47XG4iLCJyZXF1aXJlKCcuLi8uLi9tb2R1bGVzL2VzNi5vYmplY3QuYXNzaWduJyk7XG5tb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoJy4uLy4uL21vZHVsZXMvX2NvcmUnKS5PYmplY3QuYXNzaWduO1xuIiwicmVxdWlyZSgnLi4vLi4vbW9kdWxlcy9lczYub2JqZWN0LmNyZWF0ZScpO1xudmFyICRPYmplY3QgPSByZXF1aXJlKCcuLi8uLi9tb2R1bGVzL19jb3JlJykuT2JqZWN0O1xubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBjcmVhdGUoUCwgRCkge1xuICByZXR1cm4gJE9iamVjdC5jcmVhdGUoUCwgRCk7XG59O1xuIiwicmVxdWlyZSgnLi4vLi4vbW9kdWxlcy9lczYub2JqZWN0LmRlZmluZS1wcm9wZXJ0eScpO1xudmFyICRPYmplY3QgPSByZXF1aXJlKCcuLi8uLi9tb2R1bGVzL19jb3JlJykuT2JqZWN0O1xubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBkZWZpbmVQcm9wZXJ0eShpdCwga2V5LCBkZXNjKSB7XG4gIHJldHVybiAkT2JqZWN0LmRlZmluZVByb3BlcnR5KGl0LCBrZXksIGRlc2MpO1xufTtcbiIsInJlcXVpcmUoJy4uLy4uL21vZHVsZXMvZXM2Lm9iamVjdC5nZXQtcHJvdG90eXBlLW9mJyk7XG5tb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoJy4uLy4uL21vZHVsZXMvX2NvcmUnKS5PYmplY3QuZ2V0UHJvdG90eXBlT2Y7XG4iLCJyZXF1aXJlKCcuLi8uLi9tb2R1bGVzL2VzNi5vYmplY3Qua2V5cycpO1xubW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKCcuLi8uLi9tb2R1bGVzL19jb3JlJykuT2JqZWN0LmtleXM7XG4iLCJyZXF1aXJlKCcuLi8uLi9tb2R1bGVzL2VzNi5vYmplY3Quc2V0LXByb3RvdHlwZS1vZicpO1xubW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKCcuLi8uLi9tb2R1bGVzL19jb3JlJykuT2JqZWN0LnNldFByb3RvdHlwZU9mO1xuIiwicmVxdWlyZSgnLi4vLi4vbW9kdWxlcy9lczcub2JqZWN0LnZhbHVlcycpO1xubW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKCcuLi8uLi9tb2R1bGVzL19jb3JlJykuT2JqZWN0LnZhbHVlcztcbiIsInJlcXVpcmUoJy4uLy4uL21vZHVsZXMvZXM2LnN5bWJvbCcpO1xucmVxdWlyZSgnLi4vLi4vbW9kdWxlcy9lczYub2JqZWN0LnRvLXN0cmluZycpO1xucmVxdWlyZSgnLi4vLi4vbW9kdWxlcy9lczcuc3ltYm9sLmFzeW5jLWl0ZXJhdG9yJyk7XG5yZXF1aXJlKCcuLi8uLi9tb2R1bGVzL2VzNy5zeW1ib2wub2JzZXJ2YWJsZScpO1xubW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKCcuLi8uLi9tb2R1bGVzL19jb3JlJykuU3ltYm9sO1xuIiwicmVxdWlyZSgnLi4vLi4vbW9kdWxlcy9lczYuc3RyaW5nLml0ZXJhdG9yJyk7XG5yZXF1aXJlKCcuLi8uLi9tb2R1bGVzL3dlYi5kb20uaXRlcmFibGUnKTtcbm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZSgnLi4vLi4vbW9kdWxlcy9fd2tzLWV4dCcpLmYoJ2l0ZXJhdG9yJyk7XG4iLCJtb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIChpdCkge1xuICBpZiAodHlwZW9mIGl0ICE9ICdmdW5jdGlvbicpIHRocm93IFR5cGVFcnJvcihpdCArICcgaXMgbm90IGEgZnVuY3Rpb24hJyk7XG4gIHJldHVybiBpdDtcbn07XG4iLCJtb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uICgpIHsgLyogZW1wdHkgKi8gfTtcbiIsInZhciBpc09iamVjdCA9IHJlcXVpcmUoJy4vX2lzLW9iamVjdCcpO1xubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAoaXQpIHtcbiAgaWYgKCFpc09iamVjdChpdCkpIHRocm93IFR5cGVFcnJvcihpdCArICcgaXMgbm90IGFuIG9iamVjdCEnKTtcbiAgcmV0dXJuIGl0O1xufTtcbiIsIi8vIGZhbHNlIC0+IEFycmF5I2luZGV4T2Zcbi8vIHRydWUgIC0+IEFycmF5I2luY2x1ZGVzXG52YXIgdG9JT2JqZWN0ID0gcmVxdWlyZSgnLi9fdG8taW9iamVjdCcpO1xudmFyIHRvTGVuZ3RoID0gcmVxdWlyZSgnLi9fdG8tbGVuZ3RoJyk7XG52YXIgdG9BYnNvbHV0ZUluZGV4ID0gcmVxdWlyZSgnLi9fdG8tYWJzb2x1dGUtaW5kZXgnKTtcbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKElTX0lOQ0xVREVTKSB7XG4gIHJldHVybiBmdW5jdGlvbiAoJHRoaXMsIGVsLCBmcm9tSW5kZXgpIHtcbiAgICB2YXIgTyA9IHRvSU9iamVjdCgkdGhpcyk7XG4gICAgdmFyIGxlbmd0aCA9IHRvTGVuZ3RoKE8ubGVuZ3RoKTtcbiAgICB2YXIgaW5kZXggPSB0b0Fic29sdXRlSW5kZXgoZnJvbUluZGV4LCBsZW5ndGgpO1xuICAgIHZhciB2YWx1ZTtcbiAgICAvLyBBcnJheSNpbmNsdWRlcyB1c2VzIFNhbWVWYWx1ZVplcm8gZXF1YWxpdHkgYWxnb3JpdGhtXG4gICAgLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lIG5vLXNlbGYtY29tcGFyZVxuICAgIGlmIChJU19JTkNMVURFUyAmJiBlbCAhPSBlbCkgd2hpbGUgKGxlbmd0aCA+IGluZGV4KSB7XG4gICAgICB2YWx1ZSA9IE9baW5kZXgrK107XG4gICAgICAvLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmUgbm8tc2VsZi1jb21wYXJlXG4gICAgICBpZiAodmFsdWUgIT0gdmFsdWUpIHJldHVybiB0cnVlO1xuICAgIC8vIEFycmF5I2luZGV4T2YgaWdub3JlcyBob2xlcywgQXJyYXkjaW5jbHVkZXMgLSBub3RcbiAgICB9IGVsc2UgZm9yICg7bGVuZ3RoID4gaW5kZXg7IGluZGV4KyspIGlmIChJU19JTkNMVURFUyB8fCBpbmRleCBpbiBPKSB7XG4gICAgICBpZiAoT1tpbmRleF0gPT09IGVsKSByZXR1cm4gSVNfSU5DTFVERVMgfHwgaW5kZXggfHwgMDtcbiAgICB9IHJldHVybiAhSVNfSU5DTFVERVMgJiYgLTE7XG4gIH07XG59O1xuIiwiLy8gZ2V0dGluZyB0YWcgZnJvbSAxOS4xLjMuNiBPYmplY3QucHJvdG90eXBlLnRvU3RyaW5nKClcbnZhciBjb2YgPSByZXF1aXJlKCcuL19jb2YnKTtcbnZhciBUQUcgPSByZXF1aXJlKCcuL193a3MnKSgndG9TdHJpbmdUYWcnKTtcbi8vIEVTMyB3cm9uZyBoZXJlXG52YXIgQVJHID0gY29mKGZ1bmN0aW9uICgpIHsgcmV0dXJuIGFyZ3VtZW50czsgfSgpKSA9PSAnQXJndW1lbnRzJztcblxuLy8gZmFsbGJhY2sgZm9yIElFMTEgU2NyaXB0IEFjY2VzcyBEZW5pZWQgZXJyb3JcbnZhciB0cnlHZXQgPSBmdW5jdGlvbiAoaXQsIGtleSkge1xuICB0cnkge1xuICAgIHJldHVybiBpdFtrZXldO1xuICB9IGNhdGNoIChlKSB7IC8qIGVtcHR5ICovIH1cbn07XG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKGl0KSB7XG4gIHZhciBPLCBULCBCO1xuICByZXR1cm4gaXQgPT09IHVuZGVmaW5lZCA/ICdVbmRlZmluZWQnIDogaXQgPT09IG51bGwgPyAnTnVsbCdcbiAgICAvLyBAQHRvU3RyaW5nVGFnIGNhc2VcbiAgICA6IHR5cGVvZiAoVCA9IHRyeUdldChPID0gT2JqZWN0KGl0KSwgVEFHKSkgPT0gJ3N0cmluZycgPyBUXG4gICAgLy8gYnVpbHRpblRhZyBjYXNlXG4gICAgOiBBUkcgPyBjb2YoTylcbiAgICAvLyBFUzMgYXJndW1lbnRzIGZhbGxiYWNrXG4gICAgOiAoQiA9IGNvZihPKSkgPT0gJ09iamVjdCcgJiYgdHlwZW9mIE8uY2FsbGVlID09ICdmdW5jdGlvbicgPyAnQXJndW1lbnRzJyA6IEI7XG59O1xuIiwidmFyIHRvU3RyaW5nID0ge30udG9TdHJpbmc7XG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKGl0KSB7XG4gIHJldHVybiB0b1N0cmluZy5jYWxsKGl0KS5zbGljZSg4LCAtMSk7XG59O1xuIiwidmFyIGNvcmUgPSBtb2R1bGUuZXhwb3J0cyA9IHsgdmVyc2lvbjogJzIuNi4xMScgfTtcbmlmICh0eXBlb2YgX19lID09ICdudW1iZXInKSBfX2UgPSBjb3JlOyAvLyBlc2xpbnQtZGlzYWJsZS1saW5lIG5vLXVuZGVmXG4iLCIndXNlIHN0cmljdCc7XG52YXIgJGRlZmluZVByb3BlcnR5ID0gcmVxdWlyZSgnLi9fb2JqZWN0LWRwJyk7XG52YXIgY3JlYXRlRGVzYyA9IHJlcXVpcmUoJy4vX3Byb3BlcnR5LWRlc2MnKTtcblxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAob2JqZWN0LCBpbmRleCwgdmFsdWUpIHtcbiAgaWYgKGluZGV4IGluIG9iamVjdCkgJGRlZmluZVByb3BlcnR5LmYob2JqZWN0LCBpbmRleCwgY3JlYXRlRGVzYygwLCB2YWx1ZSkpO1xuICBlbHNlIG9iamVjdFtpbmRleF0gPSB2YWx1ZTtcbn07XG4iLCIvLyBvcHRpb25hbCAvIHNpbXBsZSBjb250ZXh0IGJpbmRpbmdcbnZhciBhRnVuY3Rpb24gPSByZXF1aXJlKCcuL19hLWZ1bmN0aW9uJyk7XG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIChmbiwgdGhhdCwgbGVuZ3RoKSB7XG4gIGFGdW5jdGlvbihmbik7XG4gIGlmICh0aGF0ID09PSB1bmRlZmluZWQpIHJldHVybiBmbjtcbiAgc3dpdGNoIChsZW5ndGgpIHtcbiAgICBjYXNlIDE6IHJldHVybiBmdW5jdGlvbiAoYSkge1xuICAgICAgcmV0dXJuIGZuLmNhbGwodGhhdCwgYSk7XG4gICAgfTtcbiAgICBjYXNlIDI6IHJldHVybiBmdW5jdGlvbiAoYSwgYikge1xuICAgICAgcmV0dXJuIGZuLmNhbGwodGhhdCwgYSwgYik7XG4gICAgfTtcbiAgICBjYXNlIDM6IHJldHVybiBmdW5jdGlvbiAoYSwgYiwgYykge1xuICAgICAgcmV0dXJuIGZuLmNhbGwodGhhdCwgYSwgYiwgYyk7XG4gICAgfTtcbiAgfVxuICByZXR1cm4gZnVuY3Rpb24gKC8qIC4uLmFyZ3MgKi8pIHtcbiAgICByZXR1cm4gZm4uYXBwbHkodGhhdCwgYXJndW1lbnRzKTtcbiAgfTtcbn07XG4iLCIvLyA3LjIuMSBSZXF1aXJlT2JqZWN0Q29lcmNpYmxlKGFyZ3VtZW50KVxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAoaXQpIHtcbiAgaWYgKGl0ID09IHVuZGVmaW5lZCkgdGhyb3cgVHlwZUVycm9yKFwiQ2FuJ3QgY2FsbCBtZXRob2Qgb24gIFwiICsgaXQpO1xuICByZXR1cm4gaXQ7XG59O1xuIiwiLy8gVGhhbmsncyBJRTggZm9yIGhpcyBmdW5ueSBkZWZpbmVQcm9wZXJ0eVxubW9kdWxlLmV4cG9ydHMgPSAhcmVxdWlyZSgnLi9fZmFpbHMnKShmdW5jdGlvbiAoKSB7XG4gIHJldHVybiBPYmplY3QuZGVmaW5lUHJvcGVydHkoe30sICdhJywgeyBnZXQ6IGZ1bmN0aW9uICgpIHsgcmV0dXJuIDc7IH0gfSkuYSAhPSA3O1xufSk7XG4iLCJ2YXIgaXNPYmplY3QgPSByZXF1aXJlKCcuL19pcy1vYmplY3QnKTtcbnZhciBkb2N1bWVudCA9IHJlcXVpcmUoJy4vX2dsb2JhbCcpLmRvY3VtZW50O1xuLy8gdHlwZW9mIGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQgaXMgJ29iamVjdCcgaW4gb2xkIElFXG52YXIgaXMgPSBpc09iamVjdChkb2N1bWVudCkgJiYgaXNPYmplY3QoZG9jdW1lbnQuY3JlYXRlRWxlbWVudCk7XG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIChpdCkge1xuICByZXR1cm4gaXMgPyBkb2N1bWVudC5jcmVhdGVFbGVtZW50KGl0KSA6IHt9O1xufTtcbiIsIi8vIElFIDgtIGRvbid0IGVudW0gYnVnIGtleXNcbm1vZHVsZS5leHBvcnRzID0gKFxuICAnY29uc3RydWN0b3IsaGFzT3duUHJvcGVydHksaXNQcm90b3R5cGVPZixwcm9wZXJ0eUlzRW51bWVyYWJsZSx0b0xvY2FsZVN0cmluZyx0b1N0cmluZyx2YWx1ZU9mJ1xuKS5zcGxpdCgnLCcpO1xuIiwiLy8gYWxsIGVudW1lcmFibGUgb2JqZWN0IGtleXMsIGluY2x1ZGVzIHN5bWJvbHNcbnZhciBnZXRLZXlzID0gcmVxdWlyZSgnLi9fb2JqZWN0LWtleXMnKTtcbnZhciBnT1BTID0gcmVxdWlyZSgnLi9fb2JqZWN0LWdvcHMnKTtcbnZhciBwSUUgPSByZXF1aXJlKCcuL19vYmplY3QtcGllJyk7XG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIChpdCkge1xuICB2YXIgcmVzdWx0ID0gZ2V0S2V5cyhpdCk7XG4gIHZhciBnZXRTeW1ib2xzID0gZ09QUy5mO1xuICBpZiAoZ2V0U3ltYm9scykge1xuICAgIHZhciBzeW1ib2xzID0gZ2V0U3ltYm9scyhpdCk7XG4gICAgdmFyIGlzRW51bSA9IHBJRS5mO1xuICAgIHZhciBpID0gMDtcbiAgICB2YXIga2V5O1xuICAgIHdoaWxlIChzeW1ib2xzLmxlbmd0aCA+IGkpIGlmIChpc0VudW0uY2FsbChpdCwga2V5ID0gc3ltYm9sc1tpKytdKSkgcmVzdWx0LnB1c2goa2V5KTtcbiAgfSByZXR1cm4gcmVzdWx0O1xufTtcbiIsInZhciBnbG9iYWwgPSByZXF1aXJlKCcuL19nbG9iYWwnKTtcbnZhciBjb3JlID0gcmVxdWlyZSgnLi9fY29yZScpO1xudmFyIGN0eCA9IHJlcXVpcmUoJy4vX2N0eCcpO1xudmFyIGhpZGUgPSByZXF1aXJlKCcuL19oaWRlJyk7XG52YXIgaGFzID0gcmVxdWlyZSgnLi9faGFzJyk7XG52YXIgUFJPVE9UWVBFID0gJ3Byb3RvdHlwZSc7XG5cbnZhciAkZXhwb3J0ID0gZnVuY3Rpb24gKHR5cGUsIG5hbWUsIHNvdXJjZSkge1xuICB2YXIgSVNfRk9SQ0VEID0gdHlwZSAmICRleHBvcnQuRjtcbiAgdmFyIElTX0dMT0JBTCA9IHR5cGUgJiAkZXhwb3J0Lkc7XG4gIHZhciBJU19TVEFUSUMgPSB0eXBlICYgJGV4cG9ydC5TO1xuICB2YXIgSVNfUFJPVE8gPSB0eXBlICYgJGV4cG9ydC5QO1xuICB2YXIgSVNfQklORCA9IHR5cGUgJiAkZXhwb3J0LkI7XG4gIHZhciBJU19XUkFQID0gdHlwZSAmICRleHBvcnQuVztcbiAgdmFyIGV4cG9ydHMgPSBJU19HTE9CQUwgPyBjb3JlIDogY29yZVtuYW1lXSB8fCAoY29yZVtuYW1lXSA9IHt9KTtcbiAgdmFyIGV4cFByb3RvID0gZXhwb3J0c1tQUk9UT1RZUEVdO1xuICB2YXIgdGFyZ2V0ID0gSVNfR0xPQkFMID8gZ2xvYmFsIDogSVNfU1RBVElDID8gZ2xvYmFsW25hbWVdIDogKGdsb2JhbFtuYW1lXSB8fCB7fSlbUFJPVE9UWVBFXTtcbiAgdmFyIGtleSwgb3duLCBvdXQ7XG4gIGlmIChJU19HTE9CQUwpIHNvdXJjZSA9IG5hbWU7XG4gIGZvciAoa2V5IGluIHNvdXJjZSkge1xuICAgIC8vIGNvbnRhaW5zIGluIG5hdGl2ZVxuICAgIG93biA9ICFJU19GT1JDRUQgJiYgdGFyZ2V0ICYmIHRhcmdldFtrZXldICE9PSB1bmRlZmluZWQ7XG4gICAgaWYgKG93biAmJiBoYXMoZXhwb3J0cywga2V5KSkgY29udGludWU7XG4gICAgLy8gZXhwb3J0IG5hdGl2ZSBvciBwYXNzZWRcbiAgICBvdXQgPSBvd24gPyB0YXJnZXRba2V5XSA6IHNvdXJjZVtrZXldO1xuICAgIC8vIHByZXZlbnQgZ2xvYmFsIHBvbGx1dGlvbiBmb3IgbmFtZXNwYWNlc1xuICAgIGV4cG9ydHNba2V5XSA9IElTX0dMT0JBTCAmJiB0eXBlb2YgdGFyZ2V0W2tleV0gIT0gJ2Z1bmN0aW9uJyA/IHNvdXJjZVtrZXldXG4gICAgLy8gYmluZCB0aW1lcnMgdG8gZ2xvYmFsIGZvciBjYWxsIGZyb20gZXhwb3J0IGNvbnRleHRcbiAgICA6IElTX0JJTkQgJiYgb3duID8gY3R4KG91dCwgZ2xvYmFsKVxuICAgIC8vIHdyYXAgZ2xvYmFsIGNvbnN0cnVjdG9ycyBmb3IgcHJldmVudCBjaGFuZ2UgdGhlbSBpbiBsaWJyYXJ5XG4gICAgOiBJU19XUkFQICYmIHRhcmdldFtrZXldID09IG91dCA/IChmdW5jdGlvbiAoQykge1xuICAgICAgdmFyIEYgPSBmdW5jdGlvbiAoYSwgYiwgYykge1xuICAgICAgICBpZiAodGhpcyBpbnN0YW5jZW9mIEMpIHtcbiAgICAgICAgICBzd2l0Y2ggKGFyZ3VtZW50cy5sZW5ndGgpIHtcbiAgICAgICAgICAgIGNhc2UgMDogcmV0dXJuIG5ldyBDKCk7XG4gICAgICAgICAgICBjYXNlIDE6IHJldHVybiBuZXcgQyhhKTtcbiAgICAgICAgICAgIGNhc2UgMjogcmV0dXJuIG5ldyBDKGEsIGIpO1xuICAgICAgICAgIH0gcmV0dXJuIG5ldyBDKGEsIGIsIGMpO1xuICAgICAgICB9IHJldHVybiBDLmFwcGx5KHRoaXMsIGFyZ3VtZW50cyk7XG4gICAgICB9O1xuICAgICAgRltQUk9UT1RZUEVdID0gQ1tQUk9UT1RZUEVdO1xuICAgICAgcmV0dXJuIEY7XG4gICAgLy8gbWFrZSBzdGF0aWMgdmVyc2lvbnMgZm9yIHByb3RvdHlwZSBtZXRob2RzXG4gICAgfSkob3V0KSA6IElTX1BST1RPICYmIHR5cGVvZiBvdXQgPT0gJ2Z1bmN0aW9uJyA/IGN0eChGdW5jdGlvbi5jYWxsLCBvdXQpIDogb3V0O1xuICAgIC8vIGV4cG9ydCBwcm90byBtZXRob2RzIHRvIGNvcmUuJUNPTlNUUlVDVE9SJS5tZXRob2RzLiVOQU1FJVxuICAgIGlmIChJU19QUk9UTykge1xuICAgICAgKGV4cG9ydHMudmlydHVhbCB8fCAoZXhwb3J0cy52aXJ0dWFsID0ge30pKVtrZXldID0gb3V0O1xuICAgICAgLy8gZXhwb3J0IHByb3RvIG1ldGhvZHMgdG8gY29yZS4lQ09OU1RSVUNUT1IlLnByb3RvdHlwZS4lTkFNRSVcbiAgICAgIGlmICh0eXBlICYgJGV4cG9ydC5SICYmIGV4cFByb3RvICYmICFleHBQcm90b1trZXldKSBoaWRlKGV4cFByb3RvLCBrZXksIG91dCk7XG4gICAgfVxuICB9XG59O1xuLy8gdHlwZSBiaXRtYXBcbiRleHBvcnQuRiA9IDE7ICAgLy8gZm9yY2VkXG4kZXhwb3J0LkcgPSAyOyAgIC8vIGdsb2JhbFxuJGV4cG9ydC5TID0gNDsgICAvLyBzdGF0aWNcbiRleHBvcnQuUCA9IDg7ICAgLy8gcHJvdG9cbiRleHBvcnQuQiA9IDE2OyAgLy8gYmluZFxuJGV4cG9ydC5XID0gMzI7ICAvLyB3cmFwXG4kZXhwb3J0LlUgPSA2NDsgIC8vIHNhZmVcbiRleHBvcnQuUiA9IDEyODsgLy8gcmVhbCBwcm90byBtZXRob2QgZm9yIGBsaWJyYXJ5YFxubW9kdWxlLmV4cG9ydHMgPSAkZXhwb3J0O1xuIiwibW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAoZXhlYykge1xuICB0cnkge1xuICAgIHJldHVybiAhIWV4ZWMoKTtcbiAgfSBjYXRjaCAoZSkge1xuICAgIHJldHVybiB0cnVlO1xuICB9XG59O1xuIiwiLy8gaHR0cHM6Ly9naXRodWIuY29tL3psb2lyb2NrL2NvcmUtanMvaXNzdWVzLzg2I2lzc3VlY29tbWVudC0xMTU3NTkwMjhcbnZhciBnbG9iYWwgPSBtb2R1bGUuZXhwb3J0cyA9IHR5cGVvZiB3aW5kb3cgIT0gJ3VuZGVmaW5lZCcgJiYgd2luZG93Lk1hdGggPT0gTWF0aFxuICA/IHdpbmRvdyA6IHR5cGVvZiBzZWxmICE9ICd1bmRlZmluZWQnICYmIHNlbGYuTWF0aCA9PSBNYXRoID8gc2VsZlxuICAvLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmUgbm8tbmV3LWZ1bmNcbiAgOiBGdW5jdGlvbigncmV0dXJuIHRoaXMnKSgpO1xuaWYgKHR5cGVvZiBfX2cgPT0gJ251bWJlcicpIF9fZyA9IGdsb2JhbDsgLy8gZXNsaW50LWRpc2FibGUtbGluZSBuby11bmRlZlxuIiwidmFyIGhhc093blByb3BlcnR5ID0ge30uaGFzT3duUHJvcGVydHk7XG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIChpdCwga2V5KSB7XG4gIHJldHVybiBoYXNPd25Qcm9wZXJ0eS5jYWxsKGl0LCBrZXkpO1xufTtcbiIsInZhciBkUCA9IHJlcXVpcmUoJy4vX29iamVjdC1kcCcpO1xudmFyIGNyZWF0ZURlc2MgPSByZXF1aXJlKCcuL19wcm9wZXJ0eS1kZXNjJyk7XG5tb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoJy4vX2Rlc2NyaXB0b3JzJykgPyBmdW5jdGlvbiAob2JqZWN0LCBrZXksIHZhbHVlKSB7XG4gIHJldHVybiBkUC5mKG9iamVjdCwga2V5LCBjcmVhdGVEZXNjKDEsIHZhbHVlKSk7XG59IDogZnVuY3Rpb24gKG9iamVjdCwga2V5LCB2YWx1ZSkge1xuICBvYmplY3Rba2V5XSA9IHZhbHVlO1xuICByZXR1cm4gb2JqZWN0O1xufTtcbiIsInZhciBkb2N1bWVudCA9IHJlcXVpcmUoJy4vX2dsb2JhbCcpLmRvY3VtZW50O1xubW9kdWxlLmV4cG9ydHMgPSBkb2N1bWVudCAmJiBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQ7XG4iLCJtb2R1bGUuZXhwb3J0cyA9ICFyZXF1aXJlKCcuL19kZXNjcmlwdG9ycycpICYmICFyZXF1aXJlKCcuL19mYWlscycpKGZ1bmN0aW9uICgpIHtcbiAgcmV0dXJuIE9iamVjdC5kZWZpbmVQcm9wZXJ0eShyZXF1aXJlKCcuL19kb20tY3JlYXRlJykoJ2RpdicpLCAnYScsIHsgZ2V0OiBmdW5jdGlvbiAoKSB7IHJldHVybiA3OyB9IH0pLmEgIT0gNztcbn0pO1xuIiwiLy8gZmFsbGJhY2sgZm9yIG5vbi1hcnJheS1saWtlIEVTMyBhbmQgbm9uLWVudW1lcmFibGUgb2xkIFY4IHN0cmluZ3NcbnZhciBjb2YgPSByZXF1aXJlKCcuL19jb2YnKTtcbi8vIGVzbGludC1kaXNhYmxlLW5leHQtbGluZSBuby1wcm90b3R5cGUtYnVpbHRpbnNcbm1vZHVsZS5leHBvcnRzID0gT2JqZWN0KCd6JykucHJvcGVydHlJc0VudW1lcmFibGUoMCkgPyBPYmplY3QgOiBmdW5jdGlvbiAoaXQpIHtcbiAgcmV0dXJuIGNvZihpdCkgPT0gJ1N0cmluZycgPyBpdC5zcGxpdCgnJykgOiBPYmplY3QoaXQpO1xufTtcbiIsIi8vIGNoZWNrIG9uIGRlZmF1bHQgQXJyYXkgaXRlcmF0b3JcbnZhciBJdGVyYXRvcnMgPSByZXF1aXJlKCcuL19pdGVyYXRvcnMnKTtcbnZhciBJVEVSQVRPUiA9IHJlcXVpcmUoJy4vX3drcycpKCdpdGVyYXRvcicpO1xudmFyIEFycmF5UHJvdG8gPSBBcnJheS5wcm90b3R5cGU7XG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKGl0KSB7XG4gIHJldHVybiBpdCAhPT0gdW5kZWZpbmVkICYmIChJdGVyYXRvcnMuQXJyYXkgPT09IGl0IHx8IEFycmF5UHJvdG9bSVRFUkFUT1JdID09PSBpdCk7XG59O1xuIiwiLy8gNy4yLjIgSXNBcnJheShhcmd1bWVudClcbnZhciBjb2YgPSByZXF1aXJlKCcuL19jb2YnKTtcbm1vZHVsZS5leHBvcnRzID0gQXJyYXkuaXNBcnJheSB8fCBmdW5jdGlvbiBpc0FycmF5KGFyZykge1xuICByZXR1cm4gY29mKGFyZykgPT0gJ0FycmF5Jztcbn07XG4iLCJtb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIChpdCkge1xuICByZXR1cm4gdHlwZW9mIGl0ID09PSAnb2JqZWN0JyA/IGl0ICE9PSBudWxsIDogdHlwZW9mIGl0ID09PSAnZnVuY3Rpb24nO1xufTtcbiIsIi8vIGNhbGwgc29tZXRoaW5nIG9uIGl0ZXJhdG9yIHN0ZXAgd2l0aCBzYWZlIGNsb3Npbmcgb24gZXJyb3JcbnZhciBhbk9iamVjdCA9IHJlcXVpcmUoJy4vX2FuLW9iamVjdCcpO1xubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAoaXRlcmF0b3IsIGZuLCB2YWx1ZSwgZW50cmllcykge1xuICB0cnkge1xuICAgIHJldHVybiBlbnRyaWVzID8gZm4oYW5PYmplY3QodmFsdWUpWzBdLCB2YWx1ZVsxXSkgOiBmbih2YWx1ZSk7XG4gIC8vIDcuNC42IEl0ZXJhdG9yQ2xvc2UoaXRlcmF0b3IsIGNvbXBsZXRpb24pXG4gIH0gY2F0Y2ggKGUpIHtcbiAgICB2YXIgcmV0ID0gaXRlcmF0b3JbJ3JldHVybiddO1xuICAgIGlmIChyZXQgIT09IHVuZGVmaW5lZCkgYW5PYmplY3QocmV0LmNhbGwoaXRlcmF0b3IpKTtcbiAgICB0aHJvdyBlO1xuICB9XG59O1xuIiwiJ3VzZSBzdHJpY3QnO1xudmFyIGNyZWF0ZSA9IHJlcXVpcmUoJy4vX29iamVjdC1jcmVhdGUnKTtcbnZhciBkZXNjcmlwdG9yID0gcmVxdWlyZSgnLi9fcHJvcGVydHktZGVzYycpO1xudmFyIHNldFRvU3RyaW5nVGFnID0gcmVxdWlyZSgnLi9fc2V0LXRvLXN0cmluZy10YWcnKTtcbnZhciBJdGVyYXRvclByb3RvdHlwZSA9IHt9O1xuXG4vLyAyNS4xLjIuMS4xICVJdGVyYXRvclByb3RvdHlwZSVbQEBpdGVyYXRvcl0oKVxucmVxdWlyZSgnLi9faGlkZScpKEl0ZXJhdG9yUHJvdG90eXBlLCByZXF1aXJlKCcuL193a3MnKSgnaXRlcmF0b3InKSwgZnVuY3Rpb24gKCkgeyByZXR1cm4gdGhpczsgfSk7XG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKENvbnN0cnVjdG9yLCBOQU1FLCBuZXh0KSB7XG4gIENvbnN0cnVjdG9yLnByb3RvdHlwZSA9IGNyZWF0ZShJdGVyYXRvclByb3RvdHlwZSwgeyBuZXh0OiBkZXNjcmlwdG9yKDEsIG5leHQpIH0pO1xuICBzZXRUb1N0cmluZ1RhZyhDb25zdHJ1Y3RvciwgTkFNRSArICcgSXRlcmF0b3InKTtcbn07XG4iLCIndXNlIHN0cmljdCc7XG52YXIgTElCUkFSWSA9IHJlcXVpcmUoJy4vX2xpYnJhcnknKTtcbnZhciAkZXhwb3J0ID0gcmVxdWlyZSgnLi9fZXhwb3J0Jyk7XG52YXIgcmVkZWZpbmUgPSByZXF1aXJlKCcuL19yZWRlZmluZScpO1xudmFyIGhpZGUgPSByZXF1aXJlKCcuL19oaWRlJyk7XG52YXIgSXRlcmF0b3JzID0gcmVxdWlyZSgnLi9faXRlcmF0b3JzJyk7XG52YXIgJGl0ZXJDcmVhdGUgPSByZXF1aXJlKCcuL19pdGVyLWNyZWF0ZScpO1xudmFyIHNldFRvU3RyaW5nVGFnID0gcmVxdWlyZSgnLi9fc2V0LXRvLXN0cmluZy10YWcnKTtcbnZhciBnZXRQcm90b3R5cGVPZiA9IHJlcXVpcmUoJy4vX29iamVjdC1ncG8nKTtcbnZhciBJVEVSQVRPUiA9IHJlcXVpcmUoJy4vX3drcycpKCdpdGVyYXRvcicpO1xudmFyIEJVR0dZID0gIShbXS5rZXlzICYmICduZXh0JyBpbiBbXS5rZXlzKCkpOyAvLyBTYWZhcmkgaGFzIGJ1Z2d5IGl0ZXJhdG9ycyB3L28gYG5leHRgXG52YXIgRkZfSVRFUkFUT1IgPSAnQEBpdGVyYXRvcic7XG52YXIgS0VZUyA9ICdrZXlzJztcbnZhciBWQUxVRVMgPSAndmFsdWVzJztcblxudmFyIHJldHVyblRoaXMgPSBmdW5jdGlvbiAoKSB7IHJldHVybiB0aGlzOyB9O1xuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIChCYXNlLCBOQU1FLCBDb25zdHJ1Y3RvciwgbmV4dCwgREVGQVVMVCwgSVNfU0VULCBGT1JDRUQpIHtcbiAgJGl0ZXJDcmVhdGUoQ29uc3RydWN0b3IsIE5BTUUsIG5leHQpO1xuICB2YXIgZ2V0TWV0aG9kID0gZnVuY3Rpb24gKGtpbmQpIHtcbiAgICBpZiAoIUJVR0dZICYmIGtpbmQgaW4gcHJvdG8pIHJldHVybiBwcm90b1traW5kXTtcbiAgICBzd2l0Y2ggKGtpbmQpIHtcbiAgICAgIGNhc2UgS0VZUzogcmV0dXJuIGZ1bmN0aW9uIGtleXMoKSB7IHJldHVybiBuZXcgQ29uc3RydWN0b3IodGhpcywga2luZCk7IH07XG4gICAgICBjYXNlIFZBTFVFUzogcmV0dXJuIGZ1bmN0aW9uIHZhbHVlcygpIHsgcmV0dXJuIG5ldyBDb25zdHJ1Y3Rvcih0aGlzLCBraW5kKTsgfTtcbiAgICB9IHJldHVybiBmdW5jdGlvbiBlbnRyaWVzKCkgeyByZXR1cm4gbmV3IENvbnN0cnVjdG9yKHRoaXMsIGtpbmQpOyB9O1xuICB9O1xuICB2YXIgVEFHID0gTkFNRSArICcgSXRlcmF0b3InO1xuICB2YXIgREVGX1ZBTFVFUyA9IERFRkFVTFQgPT0gVkFMVUVTO1xuICB2YXIgVkFMVUVTX0JVRyA9IGZhbHNlO1xuICB2YXIgcHJvdG8gPSBCYXNlLnByb3RvdHlwZTtcbiAgdmFyICRuYXRpdmUgPSBwcm90b1tJVEVSQVRPUl0gfHwgcHJvdG9bRkZfSVRFUkFUT1JdIHx8IERFRkFVTFQgJiYgcHJvdG9bREVGQVVMVF07XG4gIHZhciAkZGVmYXVsdCA9ICRuYXRpdmUgfHwgZ2V0TWV0aG9kKERFRkFVTFQpO1xuICB2YXIgJGVudHJpZXMgPSBERUZBVUxUID8gIURFRl9WQUxVRVMgPyAkZGVmYXVsdCA6IGdldE1ldGhvZCgnZW50cmllcycpIDogdW5kZWZpbmVkO1xuICB2YXIgJGFueU5hdGl2ZSA9IE5BTUUgPT0gJ0FycmF5JyA/IHByb3RvLmVudHJpZXMgfHwgJG5hdGl2ZSA6ICRuYXRpdmU7XG4gIHZhciBtZXRob2RzLCBrZXksIEl0ZXJhdG9yUHJvdG90eXBlO1xuICAvLyBGaXggbmF0aXZlXG4gIGlmICgkYW55TmF0aXZlKSB7XG4gICAgSXRlcmF0b3JQcm90b3R5cGUgPSBnZXRQcm90b3R5cGVPZigkYW55TmF0aXZlLmNhbGwobmV3IEJhc2UoKSkpO1xuICAgIGlmIChJdGVyYXRvclByb3RvdHlwZSAhPT0gT2JqZWN0LnByb3RvdHlwZSAmJiBJdGVyYXRvclByb3RvdHlwZS5uZXh0KSB7XG4gICAgICAvLyBTZXQgQEB0b1N0cmluZ1RhZyB0byBuYXRpdmUgaXRlcmF0b3JzXG4gICAgICBzZXRUb1N0cmluZ1RhZyhJdGVyYXRvclByb3RvdHlwZSwgVEFHLCB0cnVlKTtcbiAgICAgIC8vIGZpeCBmb3Igc29tZSBvbGQgZW5naW5lc1xuICAgICAgaWYgKCFMSUJSQVJZICYmIHR5cGVvZiBJdGVyYXRvclByb3RvdHlwZVtJVEVSQVRPUl0gIT0gJ2Z1bmN0aW9uJykgaGlkZShJdGVyYXRvclByb3RvdHlwZSwgSVRFUkFUT1IsIHJldHVyblRoaXMpO1xuICAgIH1cbiAgfVxuICAvLyBmaXggQXJyYXkje3ZhbHVlcywgQEBpdGVyYXRvcn0ubmFtZSBpbiBWOCAvIEZGXG4gIGlmIChERUZfVkFMVUVTICYmICRuYXRpdmUgJiYgJG5hdGl2ZS5uYW1lICE9PSBWQUxVRVMpIHtcbiAgICBWQUxVRVNfQlVHID0gdHJ1ZTtcbiAgICAkZGVmYXVsdCA9IGZ1bmN0aW9uIHZhbHVlcygpIHsgcmV0dXJuICRuYXRpdmUuY2FsbCh0aGlzKTsgfTtcbiAgfVxuICAvLyBEZWZpbmUgaXRlcmF0b3JcbiAgaWYgKCghTElCUkFSWSB8fCBGT1JDRUQpICYmIChCVUdHWSB8fCBWQUxVRVNfQlVHIHx8ICFwcm90b1tJVEVSQVRPUl0pKSB7XG4gICAgaGlkZShwcm90bywgSVRFUkFUT1IsICRkZWZhdWx0KTtcbiAgfVxuICAvLyBQbHVnIGZvciBsaWJyYXJ5XG4gIEl0ZXJhdG9yc1tOQU1FXSA9ICRkZWZhdWx0O1xuICBJdGVyYXRvcnNbVEFHXSA9IHJldHVyblRoaXM7XG4gIGlmIChERUZBVUxUKSB7XG4gICAgbWV0aG9kcyA9IHtcbiAgICAgIHZhbHVlczogREVGX1ZBTFVFUyA/ICRkZWZhdWx0IDogZ2V0TWV0aG9kKFZBTFVFUyksXG4gICAgICBrZXlzOiBJU19TRVQgPyAkZGVmYXVsdCA6IGdldE1ldGhvZChLRVlTKSxcbiAgICAgIGVudHJpZXM6ICRlbnRyaWVzXG4gICAgfTtcbiAgICBpZiAoRk9SQ0VEKSBmb3IgKGtleSBpbiBtZXRob2RzKSB7XG4gICAgICBpZiAoIShrZXkgaW4gcHJvdG8pKSByZWRlZmluZShwcm90bywga2V5LCBtZXRob2RzW2tleV0pO1xuICAgIH0gZWxzZSAkZXhwb3J0KCRleHBvcnQuUCArICRleHBvcnQuRiAqIChCVUdHWSB8fCBWQUxVRVNfQlVHKSwgTkFNRSwgbWV0aG9kcyk7XG4gIH1cbiAgcmV0dXJuIG1ldGhvZHM7XG59O1xuIiwidmFyIElURVJBVE9SID0gcmVxdWlyZSgnLi9fd2tzJykoJ2l0ZXJhdG9yJyk7XG52YXIgU0FGRV9DTE9TSU5HID0gZmFsc2U7XG5cbnRyeSB7XG4gIHZhciByaXRlciA9IFs3XVtJVEVSQVRPUl0oKTtcbiAgcml0ZXJbJ3JldHVybiddID0gZnVuY3Rpb24gKCkgeyBTQUZFX0NMT1NJTkcgPSB0cnVlOyB9O1xuICAvLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmUgbm8tdGhyb3ctbGl0ZXJhbFxuICBBcnJheS5mcm9tKHJpdGVyLCBmdW5jdGlvbiAoKSB7IHRocm93IDI7IH0pO1xufSBjYXRjaCAoZSkgeyAvKiBlbXB0eSAqLyB9XG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKGV4ZWMsIHNraXBDbG9zaW5nKSB7XG4gIGlmICghc2tpcENsb3NpbmcgJiYgIVNBRkVfQ0xPU0lORykgcmV0dXJuIGZhbHNlO1xuICB2YXIgc2FmZSA9IGZhbHNlO1xuICB0cnkge1xuICAgIHZhciBhcnIgPSBbN107XG4gICAgdmFyIGl0ZXIgPSBhcnJbSVRFUkFUT1JdKCk7XG4gICAgaXRlci5uZXh0ID0gZnVuY3Rpb24gKCkgeyByZXR1cm4geyBkb25lOiBzYWZlID0gdHJ1ZSB9OyB9O1xuICAgIGFycltJVEVSQVRPUl0gPSBmdW5jdGlvbiAoKSB7IHJldHVybiBpdGVyOyB9O1xuICAgIGV4ZWMoYXJyKTtcbiAgfSBjYXRjaCAoZSkgeyAvKiBlbXB0eSAqLyB9XG4gIHJldHVybiBzYWZlO1xufTtcbiIsIm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKGRvbmUsIHZhbHVlKSB7XG4gIHJldHVybiB7IHZhbHVlOiB2YWx1ZSwgZG9uZTogISFkb25lIH07XG59O1xuIiwibW9kdWxlLmV4cG9ydHMgPSB7fTtcbiIsIm1vZHVsZS5leHBvcnRzID0gdHJ1ZTtcbiIsInZhciBNRVRBID0gcmVxdWlyZSgnLi9fdWlkJykoJ21ldGEnKTtcbnZhciBpc09iamVjdCA9IHJlcXVpcmUoJy4vX2lzLW9iamVjdCcpO1xudmFyIGhhcyA9IHJlcXVpcmUoJy4vX2hhcycpO1xudmFyIHNldERlc2MgPSByZXF1aXJlKCcuL19vYmplY3QtZHAnKS5mO1xudmFyIGlkID0gMDtcbnZhciBpc0V4dGVuc2libGUgPSBPYmplY3QuaXNFeHRlbnNpYmxlIHx8IGZ1bmN0aW9uICgpIHtcbiAgcmV0dXJuIHRydWU7XG59O1xudmFyIEZSRUVaRSA9ICFyZXF1aXJlKCcuL19mYWlscycpKGZ1bmN0aW9uICgpIHtcbiAgcmV0dXJuIGlzRXh0ZW5zaWJsZShPYmplY3QucHJldmVudEV4dGVuc2lvbnMoe30pKTtcbn0pO1xudmFyIHNldE1ldGEgPSBmdW5jdGlvbiAoaXQpIHtcbiAgc2V0RGVzYyhpdCwgTUVUQSwgeyB2YWx1ZToge1xuICAgIGk6ICdPJyArICsraWQsIC8vIG9iamVjdCBJRFxuICAgIHc6IHt9ICAgICAgICAgIC8vIHdlYWsgY29sbGVjdGlvbnMgSURzXG4gIH0gfSk7XG59O1xudmFyIGZhc3RLZXkgPSBmdW5jdGlvbiAoaXQsIGNyZWF0ZSkge1xuICAvLyByZXR1cm4gcHJpbWl0aXZlIHdpdGggcHJlZml4XG4gIGlmICghaXNPYmplY3QoaXQpKSByZXR1cm4gdHlwZW9mIGl0ID09ICdzeW1ib2wnID8gaXQgOiAodHlwZW9mIGl0ID09ICdzdHJpbmcnID8gJ1MnIDogJ1AnKSArIGl0O1xuICBpZiAoIWhhcyhpdCwgTUVUQSkpIHtcbiAgICAvLyBjYW4ndCBzZXQgbWV0YWRhdGEgdG8gdW5jYXVnaHQgZnJvemVuIG9iamVjdFxuICAgIGlmICghaXNFeHRlbnNpYmxlKGl0KSkgcmV0dXJuICdGJztcbiAgICAvLyBub3QgbmVjZXNzYXJ5IHRvIGFkZCBtZXRhZGF0YVxuICAgIGlmICghY3JlYXRlKSByZXR1cm4gJ0UnO1xuICAgIC8vIGFkZCBtaXNzaW5nIG1ldGFkYXRhXG4gICAgc2V0TWV0YShpdCk7XG4gIC8vIHJldHVybiBvYmplY3QgSURcbiAgfSByZXR1cm4gaXRbTUVUQV0uaTtcbn07XG52YXIgZ2V0V2VhayA9IGZ1bmN0aW9uIChpdCwgY3JlYXRlKSB7XG4gIGlmICghaGFzKGl0LCBNRVRBKSkge1xuICAgIC8vIGNhbid0IHNldCBtZXRhZGF0YSB0byB1bmNhdWdodCBmcm96ZW4gb2JqZWN0XG4gICAgaWYgKCFpc0V4dGVuc2libGUoaXQpKSByZXR1cm4gdHJ1ZTtcbiAgICAvLyBub3QgbmVjZXNzYXJ5IHRvIGFkZCBtZXRhZGF0YVxuICAgIGlmICghY3JlYXRlKSByZXR1cm4gZmFsc2U7XG4gICAgLy8gYWRkIG1pc3NpbmcgbWV0YWRhdGFcbiAgICBzZXRNZXRhKGl0KTtcbiAgLy8gcmV0dXJuIGhhc2ggd2VhayBjb2xsZWN0aW9ucyBJRHNcbiAgfSByZXR1cm4gaXRbTUVUQV0udztcbn07XG4vLyBhZGQgbWV0YWRhdGEgb24gZnJlZXplLWZhbWlseSBtZXRob2RzIGNhbGxpbmdcbnZhciBvbkZyZWV6ZSA9IGZ1bmN0aW9uIChpdCkge1xuICBpZiAoRlJFRVpFICYmIG1ldGEuTkVFRCAmJiBpc0V4dGVuc2libGUoaXQpICYmICFoYXMoaXQsIE1FVEEpKSBzZXRNZXRhKGl0KTtcbiAgcmV0dXJuIGl0O1xufTtcbnZhciBtZXRhID0gbW9kdWxlLmV4cG9ydHMgPSB7XG4gIEtFWTogTUVUQSxcbiAgTkVFRDogZmFsc2UsXG4gIGZhc3RLZXk6IGZhc3RLZXksXG4gIGdldFdlYWs6IGdldFdlYWssXG4gIG9uRnJlZXplOiBvbkZyZWV6ZVxufTtcbiIsIid1c2Ugc3RyaWN0Jztcbi8vIDE5LjEuMi4xIE9iamVjdC5hc3NpZ24odGFyZ2V0LCBzb3VyY2UsIC4uLilcbnZhciBERVNDUklQVE9SUyA9IHJlcXVpcmUoJy4vX2Rlc2NyaXB0b3JzJyk7XG52YXIgZ2V0S2V5cyA9IHJlcXVpcmUoJy4vX29iamVjdC1rZXlzJyk7XG52YXIgZ09QUyA9IHJlcXVpcmUoJy4vX29iamVjdC1nb3BzJyk7XG52YXIgcElFID0gcmVxdWlyZSgnLi9fb2JqZWN0LXBpZScpO1xudmFyIHRvT2JqZWN0ID0gcmVxdWlyZSgnLi9fdG8tb2JqZWN0Jyk7XG52YXIgSU9iamVjdCA9IHJlcXVpcmUoJy4vX2lvYmplY3QnKTtcbnZhciAkYXNzaWduID0gT2JqZWN0LmFzc2lnbjtcblxuLy8gc2hvdWxkIHdvcmsgd2l0aCBzeW1ib2xzIGFuZCBzaG91bGQgaGF2ZSBkZXRlcm1pbmlzdGljIHByb3BlcnR5IG9yZGVyIChWOCBidWcpXG5tb2R1bGUuZXhwb3J0cyA9ICEkYXNzaWduIHx8IHJlcXVpcmUoJy4vX2ZhaWxzJykoZnVuY3Rpb24gKCkge1xuICB2YXIgQSA9IHt9O1xuICB2YXIgQiA9IHt9O1xuICAvLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmUgbm8tdW5kZWZcbiAgdmFyIFMgPSBTeW1ib2woKTtcbiAgdmFyIEsgPSAnYWJjZGVmZ2hpamtsbW5vcHFyc3QnO1xuICBBW1NdID0gNztcbiAgSy5zcGxpdCgnJykuZm9yRWFjaChmdW5jdGlvbiAoaykgeyBCW2tdID0gazsgfSk7XG4gIHJldHVybiAkYXNzaWduKHt9LCBBKVtTXSAhPSA3IHx8IE9iamVjdC5rZXlzKCRhc3NpZ24oe30sIEIpKS5qb2luKCcnKSAhPSBLO1xufSkgPyBmdW5jdGlvbiBhc3NpZ24odGFyZ2V0LCBzb3VyY2UpIHsgLy8gZXNsaW50LWRpc2FibGUtbGluZSBuby11bnVzZWQtdmFyc1xuICB2YXIgVCA9IHRvT2JqZWN0KHRhcmdldCk7XG4gIHZhciBhTGVuID0gYXJndW1lbnRzLmxlbmd0aDtcbiAgdmFyIGluZGV4ID0gMTtcbiAgdmFyIGdldFN5bWJvbHMgPSBnT1BTLmY7XG4gIHZhciBpc0VudW0gPSBwSUUuZjtcbiAgd2hpbGUgKGFMZW4gPiBpbmRleCkge1xuICAgIHZhciBTID0gSU9iamVjdChhcmd1bWVudHNbaW5kZXgrK10pO1xuICAgIHZhciBrZXlzID0gZ2V0U3ltYm9scyA/IGdldEtleXMoUykuY29uY2F0KGdldFN5bWJvbHMoUykpIDogZ2V0S2V5cyhTKTtcbiAgICB2YXIgbGVuZ3RoID0ga2V5cy5sZW5ndGg7XG4gICAgdmFyIGogPSAwO1xuICAgIHZhciBrZXk7XG4gICAgd2hpbGUgKGxlbmd0aCA+IGopIHtcbiAgICAgIGtleSA9IGtleXNbaisrXTtcbiAgICAgIGlmICghREVTQ1JJUFRPUlMgfHwgaXNFbnVtLmNhbGwoUywga2V5KSkgVFtrZXldID0gU1trZXldO1xuICAgIH1cbiAgfSByZXR1cm4gVDtcbn0gOiAkYXNzaWduO1xuIiwiLy8gMTkuMS4yLjIgLyAxNS4yLjMuNSBPYmplY3QuY3JlYXRlKE8gWywgUHJvcGVydGllc10pXG52YXIgYW5PYmplY3QgPSByZXF1aXJlKCcuL19hbi1vYmplY3QnKTtcbnZhciBkUHMgPSByZXF1aXJlKCcuL19vYmplY3QtZHBzJyk7XG52YXIgZW51bUJ1Z0tleXMgPSByZXF1aXJlKCcuL19lbnVtLWJ1Zy1rZXlzJyk7XG52YXIgSUVfUFJPVE8gPSByZXF1aXJlKCcuL19zaGFyZWQta2V5JykoJ0lFX1BST1RPJyk7XG52YXIgRW1wdHkgPSBmdW5jdGlvbiAoKSB7IC8qIGVtcHR5ICovIH07XG52YXIgUFJPVE9UWVBFID0gJ3Byb3RvdHlwZSc7XG5cbi8vIENyZWF0ZSBvYmplY3Qgd2l0aCBmYWtlIGBudWxsYCBwcm90b3R5cGU6IHVzZSBpZnJhbWUgT2JqZWN0IHdpdGggY2xlYXJlZCBwcm90b3R5cGVcbnZhciBjcmVhdGVEaWN0ID0gZnVuY3Rpb24gKCkge1xuICAvLyBUaHJhc2gsIHdhc3RlIGFuZCBzb2RvbXk6IElFIEdDIGJ1Z1xuICB2YXIgaWZyYW1lID0gcmVxdWlyZSgnLi9fZG9tLWNyZWF0ZScpKCdpZnJhbWUnKTtcbiAgdmFyIGkgPSBlbnVtQnVnS2V5cy5sZW5ndGg7XG4gIHZhciBsdCA9ICc8JztcbiAgdmFyIGd0ID0gJz4nO1xuICB2YXIgaWZyYW1lRG9jdW1lbnQ7XG4gIGlmcmFtZS5zdHlsZS5kaXNwbGF5ID0gJ25vbmUnO1xuICByZXF1aXJlKCcuL19odG1sJykuYXBwZW5kQ2hpbGQoaWZyYW1lKTtcbiAgaWZyYW1lLnNyYyA9ICdqYXZhc2NyaXB0Oic7IC8vIGVzbGludC1kaXNhYmxlLWxpbmUgbm8tc2NyaXB0LXVybFxuICAvLyBjcmVhdGVEaWN0ID0gaWZyYW1lLmNvbnRlbnRXaW5kb3cuT2JqZWN0O1xuICAvLyBodG1sLnJlbW92ZUNoaWxkKGlmcmFtZSk7XG4gIGlmcmFtZURvY3VtZW50ID0gaWZyYW1lLmNvbnRlbnRXaW5kb3cuZG9jdW1lbnQ7XG4gIGlmcmFtZURvY3VtZW50Lm9wZW4oKTtcbiAgaWZyYW1lRG9jdW1lbnQud3JpdGUobHQgKyAnc2NyaXB0JyArIGd0ICsgJ2RvY3VtZW50LkY9T2JqZWN0JyArIGx0ICsgJy9zY3JpcHQnICsgZ3QpO1xuICBpZnJhbWVEb2N1bWVudC5jbG9zZSgpO1xuICBjcmVhdGVEaWN0ID0gaWZyYW1lRG9jdW1lbnQuRjtcbiAgd2hpbGUgKGktLSkgZGVsZXRlIGNyZWF0ZURpY3RbUFJPVE9UWVBFXVtlbnVtQnVnS2V5c1tpXV07XG4gIHJldHVybiBjcmVhdGVEaWN0KCk7XG59O1xuXG5tb2R1bGUuZXhwb3J0cyA9IE9iamVjdC5jcmVhdGUgfHwgZnVuY3Rpb24gY3JlYXRlKE8sIFByb3BlcnRpZXMpIHtcbiAgdmFyIHJlc3VsdDtcbiAgaWYgKE8gIT09IG51bGwpIHtcbiAgICBFbXB0eVtQUk9UT1RZUEVdID0gYW5PYmplY3QoTyk7XG4gICAgcmVzdWx0ID0gbmV3IEVtcHR5KCk7XG4gICAgRW1wdHlbUFJPVE9UWVBFXSA9IG51bGw7XG4gICAgLy8gYWRkIFwiX19wcm90b19fXCIgZm9yIE9iamVjdC5nZXRQcm90b3R5cGVPZiBwb2x5ZmlsbFxuICAgIHJlc3VsdFtJRV9QUk9UT10gPSBPO1xuICB9IGVsc2UgcmVzdWx0ID0gY3JlYXRlRGljdCgpO1xuICByZXR1cm4gUHJvcGVydGllcyA9PT0gdW5kZWZpbmVkID8gcmVzdWx0IDogZFBzKHJlc3VsdCwgUHJvcGVydGllcyk7XG59O1xuIiwidmFyIGFuT2JqZWN0ID0gcmVxdWlyZSgnLi9fYW4tb2JqZWN0Jyk7XG52YXIgSUU4X0RPTV9ERUZJTkUgPSByZXF1aXJlKCcuL19pZTgtZG9tLWRlZmluZScpO1xudmFyIHRvUHJpbWl0aXZlID0gcmVxdWlyZSgnLi9fdG8tcHJpbWl0aXZlJyk7XG52YXIgZFAgPSBPYmplY3QuZGVmaW5lUHJvcGVydHk7XG5cbmV4cG9ydHMuZiA9IHJlcXVpcmUoJy4vX2Rlc2NyaXB0b3JzJykgPyBPYmplY3QuZGVmaW5lUHJvcGVydHkgOiBmdW5jdGlvbiBkZWZpbmVQcm9wZXJ0eShPLCBQLCBBdHRyaWJ1dGVzKSB7XG4gIGFuT2JqZWN0KE8pO1xuICBQID0gdG9QcmltaXRpdmUoUCwgdHJ1ZSk7XG4gIGFuT2JqZWN0KEF0dHJpYnV0ZXMpO1xuICBpZiAoSUU4X0RPTV9ERUZJTkUpIHRyeSB7XG4gICAgcmV0dXJuIGRQKE8sIFAsIEF0dHJpYnV0ZXMpO1xuICB9IGNhdGNoIChlKSB7IC8qIGVtcHR5ICovIH1cbiAgaWYgKCdnZXQnIGluIEF0dHJpYnV0ZXMgfHwgJ3NldCcgaW4gQXR0cmlidXRlcykgdGhyb3cgVHlwZUVycm9yKCdBY2Nlc3NvcnMgbm90IHN1cHBvcnRlZCEnKTtcbiAgaWYgKCd2YWx1ZScgaW4gQXR0cmlidXRlcykgT1tQXSA9IEF0dHJpYnV0ZXMudmFsdWU7XG4gIHJldHVybiBPO1xufTtcbiIsInZhciBkUCA9IHJlcXVpcmUoJy4vX29iamVjdC1kcCcpO1xudmFyIGFuT2JqZWN0ID0gcmVxdWlyZSgnLi9fYW4tb2JqZWN0Jyk7XG52YXIgZ2V0S2V5cyA9IHJlcXVpcmUoJy4vX29iamVjdC1rZXlzJyk7XG5cbm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZSgnLi9fZGVzY3JpcHRvcnMnKSA/IE9iamVjdC5kZWZpbmVQcm9wZXJ0aWVzIDogZnVuY3Rpb24gZGVmaW5lUHJvcGVydGllcyhPLCBQcm9wZXJ0aWVzKSB7XG4gIGFuT2JqZWN0KE8pO1xuICB2YXIga2V5cyA9IGdldEtleXMoUHJvcGVydGllcyk7XG4gIHZhciBsZW5ndGggPSBrZXlzLmxlbmd0aDtcbiAgdmFyIGkgPSAwO1xuICB2YXIgUDtcbiAgd2hpbGUgKGxlbmd0aCA+IGkpIGRQLmYoTywgUCA9IGtleXNbaSsrXSwgUHJvcGVydGllc1tQXSk7XG4gIHJldHVybiBPO1xufTtcbiIsInZhciBwSUUgPSByZXF1aXJlKCcuL19vYmplY3QtcGllJyk7XG52YXIgY3JlYXRlRGVzYyA9IHJlcXVpcmUoJy4vX3Byb3BlcnR5LWRlc2MnKTtcbnZhciB0b0lPYmplY3QgPSByZXF1aXJlKCcuL190by1pb2JqZWN0Jyk7XG52YXIgdG9QcmltaXRpdmUgPSByZXF1aXJlKCcuL190by1wcmltaXRpdmUnKTtcbnZhciBoYXMgPSByZXF1aXJlKCcuL19oYXMnKTtcbnZhciBJRThfRE9NX0RFRklORSA9IHJlcXVpcmUoJy4vX2llOC1kb20tZGVmaW5lJyk7XG52YXIgZ09QRCA9IE9iamVjdC5nZXRPd25Qcm9wZXJ0eURlc2NyaXB0b3I7XG5cbmV4cG9ydHMuZiA9IHJlcXVpcmUoJy4vX2Rlc2NyaXB0b3JzJykgPyBnT1BEIDogZnVuY3Rpb24gZ2V0T3duUHJvcGVydHlEZXNjcmlwdG9yKE8sIFApIHtcbiAgTyA9IHRvSU9iamVjdChPKTtcbiAgUCA9IHRvUHJpbWl0aXZlKFAsIHRydWUpO1xuICBpZiAoSUU4X0RPTV9ERUZJTkUpIHRyeSB7XG4gICAgcmV0dXJuIGdPUEQoTywgUCk7XG4gIH0gY2F0Y2ggKGUpIHsgLyogZW1wdHkgKi8gfVxuICBpZiAoaGFzKE8sIFApKSByZXR1cm4gY3JlYXRlRGVzYyghcElFLmYuY2FsbChPLCBQKSwgT1tQXSk7XG59O1xuIiwiLy8gZmFsbGJhY2sgZm9yIElFMTEgYnVnZ3kgT2JqZWN0LmdldE93blByb3BlcnR5TmFtZXMgd2l0aCBpZnJhbWUgYW5kIHdpbmRvd1xudmFyIHRvSU9iamVjdCA9IHJlcXVpcmUoJy4vX3RvLWlvYmplY3QnKTtcbnZhciBnT1BOID0gcmVxdWlyZSgnLi9fb2JqZWN0LWdvcG4nKS5mO1xudmFyIHRvU3RyaW5nID0ge30udG9TdHJpbmc7XG5cbnZhciB3aW5kb3dOYW1lcyA9IHR5cGVvZiB3aW5kb3cgPT0gJ29iamVjdCcgJiYgd2luZG93ICYmIE9iamVjdC5nZXRPd25Qcm9wZXJ0eU5hbWVzXG4gID8gT2JqZWN0LmdldE93blByb3BlcnR5TmFtZXMod2luZG93KSA6IFtdO1xuXG52YXIgZ2V0V2luZG93TmFtZXMgPSBmdW5jdGlvbiAoaXQpIHtcbiAgdHJ5IHtcbiAgICByZXR1cm4gZ09QTihpdCk7XG4gIH0gY2F0Y2ggKGUpIHtcbiAgICByZXR1cm4gd2luZG93TmFtZXMuc2xpY2UoKTtcbiAgfVxufTtcblxubW9kdWxlLmV4cG9ydHMuZiA9IGZ1bmN0aW9uIGdldE93blByb3BlcnR5TmFtZXMoaXQpIHtcbiAgcmV0dXJuIHdpbmRvd05hbWVzICYmIHRvU3RyaW5nLmNhbGwoaXQpID09ICdbb2JqZWN0IFdpbmRvd10nID8gZ2V0V2luZG93TmFtZXMoaXQpIDogZ09QTih0b0lPYmplY3QoaXQpKTtcbn07XG4iLCIvLyAxOS4xLjIuNyAvIDE1LjIuMy40IE9iamVjdC5nZXRPd25Qcm9wZXJ0eU5hbWVzKE8pXG52YXIgJGtleXMgPSByZXF1aXJlKCcuL19vYmplY3Qta2V5cy1pbnRlcm5hbCcpO1xudmFyIGhpZGRlbktleXMgPSByZXF1aXJlKCcuL19lbnVtLWJ1Zy1rZXlzJykuY29uY2F0KCdsZW5ndGgnLCAncHJvdG90eXBlJyk7XG5cbmV4cG9ydHMuZiA9IE9iamVjdC5nZXRPd25Qcm9wZXJ0eU5hbWVzIHx8IGZ1bmN0aW9uIGdldE93blByb3BlcnR5TmFtZXMoTykge1xuICByZXR1cm4gJGtleXMoTywgaGlkZGVuS2V5cyk7XG59O1xuIiwiZXhwb3J0cy5mID0gT2JqZWN0LmdldE93blByb3BlcnR5U3ltYm9scztcbiIsIi8vIDE5LjEuMi45IC8gMTUuMi4zLjIgT2JqZWN0LmdldFByb3RvdHlwZU9mKE8pXG52YXIgaGFzID0gcmVxdWlyZSgnLi9faGFzJyk7XG52YXIgdG9PYmplY3QgPSByZXF1aXJlKCcuL190by1vYmplY3QnKTtcbnZhciBJRV9QUk9UTyA9IHJlcXVpcmUoJy4vX3NoYXJlZC1rZXknKSgnSUVfUFJPVE8nKTtcbnZhciBPYmplY3RQcm90byA9IE9iamVjdC5wcm90b3R5cGU7XG5cbm1vZHVsZS5leHBvcnRzID0gT2JqZWN0LmdldFByb3RvdHlwZU9mIHx8IGZ1bmN0aW9uIChPKSB7XG4gIE8gPSB0b09iamVjdChPKTtcbiAgaWYgKGhhcyhPLCBJRV9QUk9UTykpIHJldHVybiBPW0lFX1BST1RPXTtcbiAgaWYgKHR5cGVvZiBPLmNvbnN0cnVjdG9yID09ICdmdW5jdGlvbicgJiYgTyBpbnN0YW5jZW9mIE8uY29uc3RydWN0b3IpIHtcbiAgICByZXR1cm4gTy5jb25zdHJ1Y3Rvci5wcm90b3R5cGU7XG4gIH0gcmV0dXJuIE8gaW5zdGFuY2VvZiBPYmplY3QgPyBPYmplY3RQcm90byA6IG51bGw7XG59O1xuIiwidmFyIGhhcyA9IHJlcXVpcmUoJy4vX2hhcycpO1xudmFyIHRvSU9iamVjdCA9IHJlcXVpcmUoJy4vX3RvLWlvYmplY3QnKTtcbnZhciBhcnJheUluZGV4T2YgPSByZXF1aXJlKCcuL19hcnJheS1pbmNsdWRlcycpKGZhbHNlKTtcbnZhciBJRV9QUk9UTyA9IHJlcXVpcmUoJy4vX3NoYXJlZC1rZXknKSgnSUVfUFJPVE8nKTtcblxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAob2JqZWN0LCBuYW1lcykge1xuICB2YXIgTyA9IHRvSU9iamVjdChvYmplY3QpO1xuICB2YXIgaSA9IDA7XG4gIHZhciByZXN1bHQgPSBbXTtcbiAgdmFyIGtleTtcbiAgZm9yIChrZXkgaW4gTykgaWYgKGtleSAhPSBJRV9QUk9UTykgaGFzKE8sIGtleSkgJiYgcmVzdWx0LnB1c2goa2V5KTtcbiAgLy8gRG9uJ3QgZW51bSBidWcgJiBoaWRkZW4ga2V5c1xuICB3aGlsZSAobmFtZXMubGVuZ3RoID4gaSkgaWYgKGhhcyhPLCBrZXkgPSBuYW1lc1tpKytdKSkge1xuICAgIH5hcnJheUluZGV4T2YocmVzdWx0LCBrZXkpIHx8IHJlc3VsdC5wdXNoKGtleSk7XG4gIH1cbiAgcmV0dXJuIHJlc3VsdDtcbn07XG4iLCIvLyAxOS4xLjIuMTQgLyAxNS4yLjMuMTQgT2JqZWN0LmtleXMoTylcbnZhciAka2V5cyA9IHJlcXVpcmUoJy4vX29iamVjdC1rZXlzLWludGVybmFsJyk7XG52YXIgZW51bUJ1Z0tleXMgPSByZXF1aXJlKCcuL19lbnVtLWJ1Zy1rZXlzJyk7XG5cbm1vZHVsZS5leHBvcnRzID0gT2JqZWN0LmtleXMgfHwgZnVuY3Rpb24ga2V5cyhPKSB7XG4gIHJldHVybiAka2V5cyhPLCBlbnVtQnVnS2V5cyk7XG59O1xuIiwiZXhwb3J0cy5mID0ge30ucHJvcGVydHlJc0VudW1lcmFibGU7XG4iLCIvLyBtb3N0IE9iamVjdCBtZXRob2RzIGJ5IEVTNiBzaG91bGQgYWNjZXB0IHByaW1pdGl2ZXNcbnZhciAkZXhwb3J0ID0gcmVxdWlyZSgnLi9fZXhwb3J0Jyk7XG52YXIgY29yZSA9IHJlcXVpcmUoJy4vX2NvcmUnKTtcbnZhciBmYWlscyA9IHJlcXVpcmUoJy4vX2ZhaWxzJyk7XG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIChLRVksIGV4ZWMpIHtcbiAgdmFyIGZuID0gKGNvcmUuT2JqZWN0IHx8IHt9KVtLRVldIHx8IE9iamVjdFtLRVldO1xuICB2YXIgZXhwID0ge307XG4gIGV4cFtLRVldID0gZXhlYyhmbik7XG4gICRleHBvcnQoJGV4cG9ydC5TICsgJGV4cG9ydC5GICogZmFpbHMoZnVuY3Rpb24gKCkgeyBmbigxKTsgfSksICdPYmplY3QnLCBleHApO1xufTtcbiIsInZhciBERVNDUklQVE9SUyA9IHJlcXVpcmUoJy4vX2Rlc2NyaXB0b3JzJyk7XG52YXIgZ2V0S2V5cyA9IHJlcXVpcmUoJy4vX29iamVjdC1rZXlzJyk7XG52YXIgdG9JT2JqZWN0ID0gcmVxdWlyZSgnLi9fdG8taW9iamVjdCcpO1xudmFyIGlzRW51bSA9IHJlcXVpcmUoJy4vX29iamVjdC1waWUnKS5mO1xubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAoaXNFbnRyaWVzKSB7XG4gIHJldHVybiBmdW5jdGlvbiAoaXQpIHtcbiAgICB2YXIgTyA9IHRvSU9iamVjdChpdCk7XG4gICAgdmFyIGtleXMgPSBnZXRLZXlzKE8pO1xuICAgIHZhciBsZW5ndGggPSBrZXlzLmxlbmd0aDtcbiAgICB2YXIgaSA9IDA7XG4gICAgdmFyIHJlc3VsdCA9IFtdO1xuICAgIHZhciBrZXk7XG4gICAgd2hpbGUgKGxlbmd0aCA+IGkpIHtcbiAgICAgIGtleSA9IGtleXNbaSsrXTtcbiAgICAgIGlmICghREVTQ1JJUFRPUlMgfHwgaXNFbnVtLmNhbGwoTywga2V5KSkge1xuICAgICAgICByZXN1bHQucHVzaChpc0VudHJpZXMgPyBba2V5LCBPW2tleV1dIDogT1trZXldKTtcbiAgICAgIH1cbiAgICB9XG4gICAgcmV0dXJuIHJlc3VsdDtcbiAgfTtcbn07XG4iLCJtb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIChiaXRtYXAsIHZhbHVlKSB7XG4gIHJldHVybiB7XG4gICAgZW51bWVyYWJsZTogIShiaXRtYXAgJiAxKSxcbiAgICBjb25maWd1cmFibGU6ICEoYml0bWFwICYgMiksXG4gICAgd3JpdGFibGU6ICEoYml0bWFwICYgNCksXG4gICAgdmFsdWU6IHZhbHVlXG4gIH07XG59O1xuIiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKCcuL19oaWRlJyk7XG4iLCIvLyBXb3JrcyB3aXRoIF9fcHJvdG9fXyBvbmx5LiBPbGQgdjggY2FuJ3Qgd29yayB3aXRoIG51bGwgcHJvdG8gb2JqZWN0cy5cbi8qIGVzbGludC1kaXNhYmxlIG5vLXByb3RvICovXG52YXIgaXNPYmplY3QgPSByZXF1aXJlKCcuL19pcy1vYmplY3QnKTtcbnZhciBhbk9iamVjdCA9IHJlcXVpcmUoJy4vX2FuLW9iamVjdCcpO1xudmFyIGNoZWNrID0gZnVuY3Rpb24gKE8sIHByb3RvKSB7XG4gIGFuT2JqZWN0KE8pO1xuICBpZiAoIWlzT2JqZWN0KHByb3RvKSAmJiBwcm90byAhPT0gbnVsbCkgdGhyb3cgVHlwZUVycm9yKHByb3RvICsgXCI6IGNhbid0IHNldCBhcyBwcm90b3R5cGUhXCIpO1xufTtcbm1vZHVsZS5leHBvcnRzID0ge1xuICBzZXQ6IE9iamVjdC5zZXRQcm90b3R5cGVPZiB8fCAoJ19fcHJvdG9fXycgaW4ge30gPyAvLyBlc2xpbnQtZGlzYWJsZS1saW5lXG4gICAgZnVuY3Rpb24gKHRlc3QsIGJ1Z2d5LCBzZXQpIHtcbiAgICAgIHRyeSB7XG4gICAgICAgIHNldCA9IHJlcXVpcmUoJy4vX2N0eCcpKEZ1bmN0aW9uLmNhbGwsIHJlcXVpcmUoJy4vX29iamVjdC1nb3BkJykuZihPYmplY3QucHJvdG90eXBlLCAnX19wcm90b19fJykuc2V0LCAyKTtcbiAgICAgICAgc2V0KHRlc3QsIFtdKTtcbiAgICAgICAgYnVnZ3kgPSAhKHRlc3QgaW5zdGFuY2VvZiBBcnJheSk7XG4gICAgICB9IGNhdGNoIChlKSB7IGJ1Z2d5ID0gdHJ1ZTsgfVxuICAgICAgcmV0dXJuIGZ1bmN0aW9uIHNldFByb3RvdHlwZU9mKE8sIHByb3RvKSB7XG4gICAgICAgIGNoZWNrKE8sIHByb3RvKTtcbiAgICAgICAgaWYgKGJ1Z2d5KSBPLl9fcHJvdG9fXyA9IHByb3RvO1xuICAgICAgICBlbHNlIHNldChPLCBwcm90byk7XG4gICAgICAgIHJldHVybiBPO1xuICAgICAgfTtcbiAgICB9KHt9LCBmYWxzZSkgOiB1bmRlZmluZWQpLFxuICBjaGVjazogY2hlY2tcbn07XG4iLCJ2YXIgZGVmID0gcmVxdWlyZSgnLi9fb2JqZWN0LWRwJykuZjtcbnZhciBoYXMgPSByZXF1aXJlKCcuL19oYXMnKTtcbnZhciBUQUcgPSByZXF1aXJlKCcuL193a3MnKSgndG9TdHJpbmdUYWcnKTtcblxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAoaXQsIHRhZywgc3RhdCkge1xuICBpZiAoaXQgJiYgIWhhcyhpdCA9IHN0YXQgPyBpdCA6IGl0LnByb3RvdHlwZSwgVEFHKSkgZGVmKGl0LCBUQUcsIHsgY29uZmlndXJhYmxlOiB0cnVlLCB2YWx1ZTogdGFnIH0pO1xufTtcbiIsInZhciBzaGFyZWQgPSByZXF1aXJlKCcuL19zaGFyZWQnKSgna2V5cycpO1xudmFyIHVpZCA9IHJlcXVpcmUoJy4vX3VpZCcpO1xubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAoa2V5KSB7XG4gIHJldHVybiBzaGFyZWRba2V5XSB8fCAoc2hhcmVkW2tleV0gPSB1aWQoa2V5KSk7XG59O1xuIiwidmFyIGNvcmUgPSByZXF1aXJlKCcuL19jb3JlJyk7XG52YXIgZ2xvYmFsID0gcmVxdWlyZSgnLi9fZ2xvYmFsJyk7XG52YXIgU0hBUkVEID0gJ19fY29yZS1qc19zaGFyZWRfXyc7XG52YXIgc3RvcmUgPSBnbG9iYWxbU0hBUkVEXSB8fCAoZ2xvYmFsW1NIQVJFRF0gPSB7fSk7XG5cbihtb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIChrZXksIHZhbHVlKSB7XG4gIHJldHVybiBzdG9yZVtrZXldIHx8IChzdG9yZVtrZXldID0gdmFsdWUgIT09IHVuZGVmaW5lZCA/IHZhbHVlIDoge30pO1xufSkoJ3ZlcnNpb25zJywgW10pLnB1c2goe1xuICB2ZXJzaW9uOiBjb3JlLnZlcnNpb24sXG4gIG1vZGU6IHJlcXVpcmUoJy4vX2xpYnJhcnknKSA/ICdwdXJlJyA6ICdnbG9iYWwnLFxuICBjb3B5cmlnaHQ6ICfCqSAyMDE5IERlbmlzIFB1c2hrYXJldiAoemxvaXJvY2sucnUpJ1xufSk7XG4iLCJ2YXIgdG9JbnRlZ2VyID0gcmVxdWlyZSgnLi9fdG8taW50ZWdlcicpO1xudmFyIGRlZmluZWQgPSByZXF1aXJlKCcuL19kZWZpbmVkJyk7XG4vLyB0cnVlICAtPiBTdHJpbmcjYXRcbi8vIGZhbHNlIC0+IFN0cmluZyNjb2RlUG9pbnRBdFxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAoVE9fU1RSSU5HKSB7XG4gIHJldHVybiBmdW5jdGlvbiAodGhhdCwgcG9zKSB7XG4gICAgdmFyIHMgPSBTdHJpbmcoZGVmaW5lZCh0aGF0KSk7XG4gICAgdmFyIGkgPSB0b0ludGVnZXIocG9zKTtcbiAgICB2YXIgbCA9IHMubGVuZ3RoO1xuICAgIHZhciBhLCBiO1xuICAgIGlmIChpIDwgMCB8fCBpID49IGwpIHJldHVybiBUT19TVFJJTkcgPyAnJyA6IHVuZGVmaW5lZDtcbiAgICBhID0gcy5jaGFyQ29kZUF0KGkpO1xuICAgIHJldHVybiBhIDwgMHhkODAwIHx8IGEgPiAweGRiZmYgfHwgaSArIDEgPT09IGwgfHwgKGIgPSBzLmNoYXJDb2RlQXQoaSArIDEpKSA8IDB4ZGMwMCB8fCBiID4gMHhkZmZmXG4gICAgICA/IFRPX1NUUklORyA/IHMuY2hhckF0KGkpIDogYVxuICAgICAgOiBUT19TVFJJTkcgPyBzLnNsaWNlKGksIGkgKyAyKSA6IChhIC0gMHhkODAwIDw8IDEwKSArIChiIC0gMHhkYzAwKSArIDB4MTAwMDA7XG4gIH07XG59O1xuIiwidmFyIHRvSW50ZWdlciA9IHJlcXVpcmUoJy4vX3RvLWludGVnZXInKTtcbnZhciBtYXggPSBNYXRoLm1heDtcbnZhciBtaW4gPSBNYXRoLm1pbjtcbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKGluZGV4LCBsZW5ndGgpIHtcbiAgaW5kZXggPSB0b0ludGVnZXIoaW5kZXgpO1xuICByZXR1cm4gaW5kZXggPCAwID8gbWF4KGluZGV4ICsgbGVuZ3RoLCAwKSA6IG1pbihpbmRleCwgbGVuZ3RoKTtcbn07XG4iLCIvLyA3LjEuNCBUb0ludGVnZXJcbnZhciBjZWlsID0gTWF0aC5jZWlsO1xudmFyIGZsb29yID0gTWF0aC5mbG9vcjtcbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKGl0KSB7XG4gIHJldHVybiBpc05hTihpdCA9ICtpdCkgPyAwIDogKGl0ID4gMCA/IGZsb29yIDogY2VpbCkoaXQpO1xufTtcbiIsIi8vIHRvIGluZGV4ZWQgb2JqZWN0LCB0b09iamVjdCB3aXRoIGZhbGxiYWNrIGZvciBub24tYXJyYXktbGlrZSBFUzMgc3RyaW5nc1xudmFyIElPYmplY3QgPSByZXF1aXJlKCcuL19pb2JqZWN0Jyk7XG52YXIgZGVmaW5lZCA9IHJlcXVpcmUoJy4vX2RlZmluZWQnKTtcbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKGl0KSB7XG4gIHJldHVybiBJT2JqZWN0KGRlZmluZWQoaXQpKTtcbn07XG4iLCIvLyA3LjEuMTUgVG9MZW5ndGhcbnZhciB0b0ludGVnZXIgPSByZXF1aXJlKCcuL190by1pbnRlZ2VyJyk7XG52YXIgbWluID0gTWF0aC5taW47XG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIChpdCkge1xuICByZXR1cm4gaXQgPiAwID8gbWluKHRvSW50ZWdlcihpdCksIDB4MWZmZmZmZmZmZmZmZmYpIDogMDsgLy8gcG93KDIsIDUzKSAtIDEgPT0gOTAwNzE5OTI1NDc0MDk5MVxufTtcbiIsIi8vIDcuMS4xMyBUb09iamVjdChhcmd1bWVudClcbnZhciBkZWZpbmVkID0gcmVxdWlyZSgnLi9fZGVmaW5lZCcpO1xubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAoaXQpIHtcbiAgcmV0dXJuIE9iamVjdChkZWZpbmVkKGl0KSk7XG59O1xuIiwiLy8gNy4xLjEgVG9QcmltaXRpdmUoaW5wdXQgWywgUHJlZmVycmVkVHlwZV0pXG52YXIgaXNPYmplY3QgPSByZXF1aXJlKCcuL19pcy1vYmplY3QnKTtcbi8vIGluc3RlYWQgb2YgdGhlIEVTNiBzcGVjIHZlcnNpb24sIHdlIGRpZG4ndCBpbXBsZW1lbnQgQEB0b1ByaW1pdGl2ZSBjYXNlXG4vLyBhbmQgdGhlIHNlY29uZCBhcmd1bWVudCAtIGZsYWcgLSBwcmVmZXJyZWQgdHlwZSBpcyBhIHN0cmluZ1xubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAoaXQsIFMpIHtcbiAgaWYgKCFpc09iamVjdChpdCkpIHJldHVybiBpdDtcbiAgdmFyIGZuLCB2YWw7XG4gIGlmIChTICYmIHR5cGVvZiAoZm4gPSBpdC50b1N0cmluZykgPT0gJ2Z1bmN0aW9uJyAmJiAhaXNPYmplY3QodmFsID0gZm4uY2FsbChpdCkpKSByZXR1cm4gdmFsO1xuICBpZiAodHlwZW9mIChmbiA9IGl0LnZhbHVlT2YpID09ICdmdW5jdGlvbicgJiYgIWlzT2JqZWN0KHZhbCA9IGZuLmNhbGwoaXQpKSkgcmV0dXJuIHZhbDtcbiAgaWYgKCFTICYmIHR5cGVvZiAoZm4gPSBpdC50b1N0cmluZykgPT0gJ2Z1bmN0aW9uJyAmJiAhaXNPYmplY3QodmFsID0gZm4uY2FsbChpdCkpKSByZXR1cm4gdmFsO1xuICB0aHJvdyBUeXBlRXJyb3IoXCJDYW4ndCBjb252ZXJ0IG9iamVjdCB0byBwcmltaXRpdmUgdmFsdWVcIik7XG59O1xuIiwidmFyIGlkID0gMDtcbnZhciBweCA9IE1hdGgucmFuZG9tKCk7XG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIChrZXkpIHtcbiAgcmV0dXJuICdTeW1ib2woJy5jb25jYXQoa2V5ID09PSB1bmRlZmluZWQgPyAnJyA6IGtleSwgJylfJywgKCsraWQgKyBweCkudG9TdHJpbmcoMzYpKTtcbn07XG4iLCJ2YXIgZ2xvYmFsID0gcmVxdWlyZSgnLi9fZ2xvYmFsJyk7XG52YXIgY29yZSA9IHJlcXVpcmUoJy4vX2NvcmUnKTtcbnZhciBMSUJSQVJZID0gcmVxdWlyZSgnLi9fbGlicmFyeScpO1xudmFyIHdrc0V4dCA9IHJlcXVpcmUoJy4vX3drcy1leHQnKTtcbnZhciBkZWZpbmVQcm9wZXJ0eSA9IHJlcXVpcmUoJy4vX29iamVjdC1kcCcpLmY7XG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIChuYW1lKSB7XG4gIHZhciAkU3ltYm9sID0gY29yZS5TeW1ib2wgfHwgKGNvcmUuU3ltYm9sID0gTElCUkFSWSA/IHt9IDogZ2xvYmFsLlN5bWJvbCB8fCB7fSk7XG4gIGlmIChuYW1lLmNoYXJBdCgwKSAhPSAnXycgJiYgIShuYW1lIGluICRTeW1ib2wpKSBkZWZpbmVQcm9wZXJ0eSgkU3ltYm9sLCBuYW1lLCB7IHZhbHVlOiB3a3NFeHQuZihuYW1lKSB9KTtcbn07XG4iLCJleHBvcnRzLmYgPSByZXF1aXJlKCcuL193a3MnKTtcbiIsInZhciBzdG9yZSA9IHJlcXVpcmUoJy4vX3NoYXJlZCcpKCd3a3MnKTtcbnZhciB1aWQgPSByZXF1aXJlKCcuL191aWQnKTtcbnZhciBTeW1ib2wgPSByZXF1aXJlKCcuL19nbG9iYWwnKS5TeW1ib2w7XG52YXIgVVNFX1NZTUJPTCA9IHR5cGVvZiBTeW1ib2wgPT0gJ2Z1bmN0aW9uJztcblxudmFyICRleHBvcnRzID0gbW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAobmFtZSkge1xuICByZXR1cm4gc3RvcmVbbmFtZV0gfHwgKHN0b3JlW25hbWVdID1cbiAgICBVU0VfU1lNQk9MICYmIFN5bWJvbFtuYW1lXSB8fCAoVVNFX1NZTUJPTCA/IFN5bWJvbCA6IHVpZCkoJ1N5bWJvbC4nICsgbmFtZSkpO1xufTtcblxuJGV4cG9ydHMuc3RvcmUgPSBzdG9yZTtcbiIsInZhciBjbGFzc29mID0gcmVxdWlyZSgnLi9fY2xhc3NvZicpO1xudmFyIElURVJBVE9SID0gcmVxdWlyZSgnLi9fd2tzJykoJ2l0ZXJhdG9yJyk7XG52YXIgSXRlcmF0b3JzID0gcmVxdWlyZSgnLi9faXRlcmF0b3JzJyk7XG5tb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoJy4vX2NvcmUnKS5nZXRJdGVyYXRvck1ldGhvZCA9IGZ1bmN0aW9uIChpdCkge1xuICBpZiAoaXQgIT0gdW5kZWZpbmVkKSByZXR1cm4gaXRbSVRFUkFUT1JdXG4gICAgfHwgaXRbJ0BAaXRlcmF0b3InXVxuICAgIHx8IEl0ZXJhdG9yc1tjbGFzc29mKGl0KV07XG59O1xuIiwidmFyIGFuT2JqZWN0ID0gcmVxdWlyZSgnLi9fYW4tb2JqZWN0Jyk7XG52YXIgZ2V0ID0gcmVxdWlyZSgnLi9jb3JlLmdldC1pdGVyYXRvci1tZXRob2QnKTtcbm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZSgnLi9fY29yZScpLmdldEl0ZXJhdG9yID0gZnVuY3Rpb24gKGl0KSB7XG4gIHZhciBpdGVyRm4gPSBnZXQoaXQpO1xuICBpZiAodHlwZW9mIGl0ZXJGbiAhPSAnZnVuY3Rpb24nKSB0aHJvdyBUeXBlRXJyb3IoaXQgKyAnIGlzIG5vdCBpdGVyYWJsZSEnKTtcbiAgcmV0dXJuIGFuT2JqZWN0KGl0ZXJGbi5jYWxsKGl0KSk7XG59O1xuIiwidmFyIGNsYXNzb2YgPSByZXF1aXJlKCcuL19jbGFzc29mJyk7XG52YXIgSVRFUkFUT1IgPSByZXF1aXJlKCcuL193a3MnKSgnaXRlcmF0b3InKTtcbnZhciBJdGVyYXRvcnMgPSByZXF1aXJlKCcuL19pdGVyYXRvcnMnKTtcbm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZSgnLi9fY29yZScpLmlzSXRlcmFibGUgPSBmdW5jdGlvbiAoaXQpIHtcbiAgdmFyIE8gPSBPYmplY3QoaXQpO1xuICByZXR1cm4gT1tJVEVSQVRPUl0gIT09IHVuZGVmaW5lZFxuICAgIHx8ICdAQGl0ZXJhdG9yJyBpbiBPXG4gICAgLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lIG5vLXByb3RvdHlwZS1idWlsdGluc1xuICAgIHx8IEl0ZXJhdG9ycy5oYXNPd25Qcm9wZXJ0eShjbGFzc29mKE8pKTtcbn07XG4iLCIndXNlIHN0cmljdCc7XG52YXIgY3R4ID0gcmVxdWlyZSgnLi9fY3R4Jyk7XG52YXIgJGV4cG9ydCA9IHJlcXVpcmUoJy4vX2V4cG9ydCcpO1xudmFyIHRvT2JqZWN0ID0gcmVxdWlyZSgnLi9fdG8tb2JqZWN0Jyk7XG52YXIgY2FsbCA9IHJlcXVpcmUoJy4vX2l0ZXItY2FsbCcpO1xudmFyIGlzQXJyYXlJdGVyID0gcmVxdWlyZSgnLi9faXMtYXJyYXktaXRlcicpO1xudmFyIHRvTGVuZ3RoID0gcmVxdWlyZSgnLi9fdG8tbGVuZ3RoJyk7XG52YXIgY3JlYXRlUHJvcGVydHkgPSByZXF1aXJlKCcuL19jcmVhdGUtcHJvcGVydHknKTtcbnZhciBnZXRJdGVyRm4gPSByZXF1aXJlKCcuL2NvcmUuZ2V0LWl0ZXJhdG9yLW1ldGhvZCcpO1xuXG4kZXhwb3J0KCRleHBvcnQuUyArICRleHBvcnQuRiAqICFyZXF1aXJlKCcuL19pdGVyLWRldGVjdCcpKGZ1bmN0aW9uIChpdGVyKSB7IEFycmF5LmZyb20oaXRlcik7IH0pLCAnQXJyYXknLCB7XG4gIC8vIDIyLjEuMi4xIEFycmF5LmZyb20oYXJyYXlMaWtlLCBtYXBmbiA9IHVuZGVmaW5lZCwgdGhpc0FyZyA9IHVuZGVmaW5lZClcbiAgZnJvbTogZnVuY3Rpb24gZnJvbShhcnJheUxpa2UgLyogLCBtYXBmbiA9IHVuZGVmaW5lZCwgdGhpc0FyZyA9IHVuZGVmaW5lZCAqLykge1xuICAgIHZhciBPID0gdG9PYmplY3QoYXJyYXlMaWtlKTtcbiAgICB2YXIgQyA9IHR5cGVvZiB0aGlzID09ICdmdW5jdGlvbicgPyB0aGlzIDogQXJyYXk7XG4gICAgdmFyIGFMZW4gPSBhcmd1bWVudHMubGVuZ3RoO1xuICAgIHZhciBtYXBmbiA9IGFMZW4gPiAxID8gYXJndW1lbnRzWzFdIDogdW5kZWZpbmVkO1xuICAgIHZhciBtYXBwaW5nID0gbWFwZm4gIT09IHVuZGVmaW5lZDtcbiAgICB2YXIgaW5kZXggPSAwO1xuICAgIHZhciBpdGVyRm4gPSBnZXRJdGVyRm4oTyk7XG4gICAgdmFyIGxlbmd0aCwgcmVzdWx0LCBzdGVwLCBpdGVyYXRvcjtcbiAgICBpZiAobWFwcGluZykgbWFwZm4gPSBjdHgobWFwZm4sIGFMZW4gPiAyID8gYXJndW1lbnRzWzJdIDogdW5kZWZpbmVkLCAyKTtcbiAgICAvLyBpZiBvYmplY3QgaXNuJ3QgaXRlcmFibGUgb3IgaXQncyBhcnJheSB3aXRoIGRlZmF1bHQgaXRlcmF0b3IgLSB1c2Ugc2ltcGxlIGNhc2VcbiAgICBpZiAoaXRlckZuICE9IHVuZGVmaW5lZCAmJiAhKEMgPT0gQXJyYXkgJiYgaXNBcnJheUl0ZXIoaXRlckZuKSkpIHtcbiAgICAgIGZvciAoaXRlcmF0b3IgPSBpdGVyRm4uY2FsbChPKSwgcmVzdWx0ID0gbmV3IEMoKTsgIShzdGVwID0gaXRlcmF0b3IubmV4dCgpKS5kb25lOyBpbmRleCsrKSB7XG4gICAgICAgIGNyZWF0ZVByb3BlcnR5KHJlc3VsdCwgaW5kZXgsIG1hcHBpbmcgPyBjYWxsKGl0ZXJhdG9yLCBtYXBmbiwgW3N0ZXAudmFsdWUsIGluZGV4XSwgdHJ1ZSkgOiBzdGVwLnZhbHVlKTtcbiAgICAgIH1cbiAgICB9IGVsc2Uge1xuICAgICAgbGVuZ3RoID0gdG9MZW5ndGgoTy5sZW5ndGgpO1xuICAgICAgZm9yIChyZXN1bHQgPSBuZXcgQyhsZW5ndGgpOyBsZW5ndGggPiBpbmRleDsgaW5kZXgrKykge1xuICAgICAgICBjcmVhdGVQcm9wZXJ0eShyZXN1bHQsIGluZGV4LCBtYXBwaW5nID8gbWFwZm4oT1tpbmRleF0sIGluZGV4KSA6IE9baW5kZXhdKTtcbiAgICAgIH1cbiAgICB9XG4gICAgcmVzdWx0Lmxlbmd0aCA9IGluZGV4O1xuICAgIHJldHVybiByZXN1bHQ7XG4gIH1cbn0pO1xuIiwiJ3VzZSBzdHJpY3QnO1xudmFyIGFkZFRvVW5zY29wYWJsZXMgPSByZXF1aXJlKCcuL19hZGQtdG8tdW5zY29wYWJsZXMnKTtcbnZhciBzdGVwID0gcmVxdWlyZSgnLi9faXRlci1zdGVwJyk7XG52YXIgSXRlcmF0b3JzID0gcmVxdWlyZSgnLi9faXRlcmF0b3JzJyk7XG52YXIgdG9JT2JqZWN0ID0gcmVxdWlyZSgnLi9fdG8taW9iamVjdCcpO1xuXG4vLyAyMi4xLjMuNCBBcnJheS5wcm90b3R5cGUuZW50cmllcygpXG4vLyAyMi4xLjMuMTMgQXJyYXkucHJvdG90eXBlLmtleXMoKVxuLy8gMjIuMS4zLjI5IEFycmF5LnByb3RvdHlwZS52YWx1ZXMoKVxuLy8gMjIuMS4zLjMwIEFycmF5LnByb3RvdHlwZVtAQGl0ZXJhdG9yXSgpXG5tb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoJy4vX2l0ZXItZGVmaW5lJykoQXJyYXksICdBcnJheScsIGZ1bmN0aW9uIChpdGVyYXRlZCwga2luZCkge1xuICB0aGlzLl90ID0gdG9JT2JqZWN0KGl0ZXJhdGVkKTsgLy8gdGFyZ2V0XG4gIHRoaXMuX2kgPSAwOyAgICAgICAgICAgICAgICAgICAvLyBuZXh0IGluZGV4XG4gIHRoaXMuX2sgPSBraW5kOyAgICAgICAgICAgICAgICAvLyBraW5kXG4vLyAyMi4xLjUuMi4xICVBcnJheUl0ZXJhdG9yUHJvdG90eXBlJS5uZXh0KClcbn0sIGZ1bmN0aW9uICgpIHtcbiAgdmFyIE8gPSB0aGlzLl90O1xuICB2YXIga2luZCA9IHRoaXMuX2s7XG4gIHZhciBpbmRleCA9IHRoaXMuX2krKztcbiAgaWYgKCFPIHx8IGluZGV4ID49IE8ubGVuZ3RoKSB7XG4gICAgdGhpcy5fdCA9IHVuZGVmaW5lZDtcbiAgICByZXR1cm4gc3RlcCgxKTtcbiAgfVxuICBpZiAoa2luZCA9PSAna2V5cycpIHJldHVybiBzdGVwKDAsIGluZGV4KTtcbiAgaWYgKGtpbmQgPT0gJ3ZhbHVlcycpIHJldHVybiBzdGVwKDAsIE9baW5kZXhdKTtcbiAgcmV0dXJuIHN0ZXAoMCwgW2luZGV4LCBPW2luZGV4XV0pO1xufSwgJ3ZhbHVlcycpO1xuXG4vLyBhcmd1bWVudHNMaXN0W0BAaXRlcmF0b3JdIGlzICVBcnJheVByb3RvX3ZhbHVlcyUgKDkuNC40LjYsIDkuNC40LjcpXG5JdGVyYXRvcnMuQXJndW1lbnRzID0gSXRlcmF0b3JzLkFycmF5O1xuXG5hZGRUb1Vuc2NvcGFibGVzKCdrZXlzJyk7XG5hZGRUb1Vuc2NvcGFibGVzKCd2YWx1ZXMnKTtcbmFkZFRvVW5zY29wYWJsZXMoJ2VudHJpZXMnKTtcbiIsIi8vIDIwLjEuMi40IE51bWJlci5pc05hTihudW1iZXIpXG52YXIgJGV4cG9ydCA9IHJlcXVpcmUoJy4vX2V4cG9ydCcpO1xuXG4kZXhwb3J0KCRleHBvcnQuUywgJ051bWJlcicsIHtcbiAgaXNOYU46IGZ1bmN0aW9uIGlzTmFOKG51bWJlcikge1xuICAgIC8vIGVzbGludC1kaXNhYmxlLW5leHQtbGluZSBuby1zZWxmLWNvbXBhcmVcbiAgICByZXR1cm4gbnVtYmVyICE9IG51bWJlcjtcbiAgfVxufSk7XG4iLCIvLyAxOS4xLjMuMSBPYmplY3QuYXNzaWduKHRhcmdldCwgc291cmNlKVxudmFyICRleHBvcnQgPSByZXF1aXJlKCcuL19leHBvcnQnKTtcblxuJGV4cG9ydCgkZXhwb3J0LlMgKyAkZXhwb3J0LkYsICdPYmplY3QnLCB7IGFzc2lnbjogcmVxdWlyZSgnLi9fb2JqZWN0LWFzc2lnbicpIH0pO1xuIiwidmFyICRleHBvcnQgPSByZXF1aXJlKCcuL19leHBvcnQnKTtcbi8vIDE5LjEuMi4yIC8gMTUuMi4zLjUgT2JqZWN0LmNyZWF0ZShPIFssIFByb3BlcnRpZXNdKVxuJGV4cG9ydCgkZXhwb3J0LlMsICdPYmplY3QnLCB7IGNyZWF0ZTogcmVxdWlyZSgnLi9fb2JqZWN0LWNyZWF0ZScpIH0pO1xuIiwidmFyICRleHBvcnQgPSByZXF1aXJlKCcuL19leHBvcnQnKTtcbi8vIDE5LjEuMi40IC8gMTUuMi4zLjYgT2JqZWN0LmRlZmluZVByb3BlcnR5KE8sIFAsIEF0dHJpYnV0ZXMpXG4kZXhwb3J0KCRleHBvcnQuUyArICRleHBvcnQuRiAqICFyZXF1aXJlKCcuL19kZXNjcmlwdG9ycycpLCAnT2JqZWN0JywgeyBkZWZpbmVQcm9wZXJ0eTogcmVxdWlyZSgnLi9fb2JqZWN0LWRwJykuZiB9KTtcbiIsIi8vIDE5LjEuMi45IE9iamVjdC5nZXRQcm90b3R5cGVPZihPKVxudmFyIHRvT2JqZWN0ID0gcmVxdWlyZSgnLi9fdG8tb2JqZWN0Jyk7XG52YXIgJGdldFByb3RvdHlwZU9mID0gcmVxdWlyZSgnLi9fb2JqZWN0LWdwbycpO1xuXG5yZXF1aXJlKCcuL19vYmplY3Qtc2FwJykoJ2dldFByb3RvdHlwZU9mJywgZnVuY3Rpb24gKCkge1xuICByZXR1cm4gZnVuY3Rpb24gZ2V0UHJvdG90eXBlT2YoaXQpIHtcbiAgICByZXR1cm4gJGdldFByb3RvdHlwZU9mKHRvT2JqZWN0KGl0KSk7XG4gIH07XG59KTtcbiIsIi8vIDE5LjEuMi4xNCBPYmplY3Qua2V5cyhPKVxudmFyIHRvT2JqZWN0ID0gcmVxdWlyZSgnLi9fdG8tb2JqZWN0Jyk7XG52YXIgJGtleXMgPSByZXF1aXJlKCcuL19vYmplY3Qta2V5cycpO1xuXG5yZXF1aXJlKCcuL19vYmplY3Qtc2FwJykoJ2tleXMnLCBmdW5jdGlvbiAoKSB7XG4gIHJldHVybiBmdW5jdGlvbiBrZXlzKGl0KSB7XG4gICAgcmV0dXJuICRrZXlzKHRvT2JqZWN0KGl0KSk7XG4gIH07XG59KTtcbiIsIi8vIDE5LjEuMy4xOSBPYmplY3Quc2V0UHJvdG90eXBlT2YoTywgcHJvdG8pXG52YXIgJGV4cG9ydCA9IHJlcXVpcmUoJy4vX2V4cG9ydCcpO1xuJGV4cG9ydCgkZXhwb3J0LlMsICdPYmplY3QnLCB7IHNldFByb3RvdHlwZU9mOiByZXF1aXJlKCcuL19zZXQtcHJvdG8nKS5zZXQgfSk7XG4iLCIndXNlIHN0cmljdCc7XG52YXIgJGF0ID0gcmVxdWlyZSgnLi9fc3RyaW5nLWF0JykodHJ1ZSk7XG5cbi8vIDIxLjEuMy4yNyBTdHJpbmcucHJvdG90eXBlW0BAaXRlcmF0b3JdKClcbnJlcXVpcmUoJy4vX2l0ZXItZGVmaW5lJykoU3RyaW5nLCAnU3RyaW5nJywgZnVuY3Rpb24gKGl0ZXJhdGVkKSB7XG4gIHRoaXMuX3QgPSBTdHJpbmcoaXRlcmF0ZWQpOyAvLyB0YXJnZXRcbiAgdGhpcy5faSA9IDA7ICAgICAgICAgICAgICAgIC8vIG5leHQgaW5kZXhcbi8vIDIxLjEuNS4yLjEgJVN0cmluZ0l0ZXJhdG9yUHJvdG90eXBlJS5uZXh0KClcbn0sIGZ1bmN0aW9uICgpIHtcbiAgdmFyIE8gPSB0aGlzLl90O1xuICB2YXIgaW5kZXggPSB0aGlzLl9pO1xuICB2YXIgcG9pbnQ7XG4gIGlmIChpbmRleCA+PSBPLmxlbmd0aCkgcmV0dXJuIHsgdmFsdWU6IHVuZGVmaW5lZCwgZG9uZTogdHJ1ZSB9O1xuICBwb2ludCA9ICRhdChPLCBpbmRleCk7XG4gIHRoaXMuX2kgKz0gcG9pbnQubGVuZ3RoO1xuICByZXR1cm4geyB2YWx1ZTogcG9pbnQsIGRvbmU6IGZhbHNlIH07XG59KTtcbiIsIid1c2Ugc3RyaWN0Jztcbi8vIEVDTUFTY3JpcHQgNiBzeW1ib2xzIHNoaW1cbnZhciBnbG9iYWwgPSByZXF1aXJlKCcuL19nbG9iYWwnKTtcbnZhciBoYXMgPSByZXF1aXJlKCcuL19oYXMnKTtcbnZhciBERVNDUklQVE9SUyA9IHJlcXVpcmUoJy4vX2Rlc2NyaXB0b3JzJyk7XG52YXIgJGV4cG9ydCA9IHJlcXVpcmUoJy4vX2V4cG9ydCcpO1xudmFyIHJlZGVmaW5lID0gcmVxdWlyZSgnLi9fcmVkZWZpbmUnKTtcbnZhciBNRVRBID0gcmVxdWlyZSgnLi9fbWV0YScpLktFWTtcbnZhciAkZmFpbHMgPSByZXF1aXJlKCcuL19mYWlscycpO1xudmFyIHNoYXJlZCA9IHJlcXVpcmUoJy4vX3NoYXJlZCcpO1xudmFyIHNldFRvU3RyaW5nVGFnID0gcmVxdWlyZSgnLi9fc2V0LXRvLXN0cmluZy10YWcnKTtcbnZhciB1aWQgPSByZXF1aXJlKCcuL191aWQnKTtcbnZhciB3a3MgPSByZXF1aXJlKCcuL193a3MnKTtcbnZhciB3a3NFeHQgPSByZXF1aXJlKCcuL193a3MtZXh0Jyk7XG52YXIgd2tzRGVmaW5lID0gcmVxdWlyZSgnLi9fd2tzLWRlZmluZScpO1xudmFyIGVudW1LZXlzID0gcmVxdWlyZSgnLi9fZW51bS1rZXlzJyk7XG52YXIgaXNBcnJheSA9IHJlcXVpcmUoJy4vX2lzLWFycmF5Jyk7XG52YXIgYW5PYmplY3QgPSByZXF1aXJlKCcuL19hbi1vYmplY3QnKTtcbnZhciBpc09iamVjdCA9IHJlcXVpcmUoJy4vX2lzLW9iamVjdCcpO1xudmFyIHRvT2JqZWN0ID0gcmVxdWlyZSgnLi9fdG8tb2JqZWN0Jyk7XG52YXIgdG9JT2JqZWN0ID0gcmVxdWlyZSgnLi9fdG8taW9iamVjdCcpO1xudmFyIHRvUHJpbWl0aXZlID0gcmVxdWlyZSgnLi9fdG8tcHJpbWl0aXZlJyk7XG52YXIgY3JlYXRlRGVzYyA9IHJlcXVpcmUoJy4vX3Byb3BlcnR5LWRlc2MnKTtcbnZhciBfY3JlYXRlID0gcmVxdWlyZSgnLi9fb2JqZWN0LWNyZWF0ZScpO1xudmFyIGdPUE5FeHQgPSByZXF1aXJlKCcuL19vYmplY3QtZ29wbi1leHQnKTtcbnZhciAkR09QRCA9IHJlcXVpcmUoJy4vX29iamVjdC1nb3BkJyk7XG52YXIgJEdPUFMgPSByZXF1aXJlKCcuL19vYmplY3QtZ29wcycpO1xudmFyICREUCA9IHJlcXVpcmUoJy4vX29iamVjdC1kcCcpO1xudmFyICRrZXlzID0gcmVxdWlyZSgnLi9fb2JqZWN0LWtleXMnKTtcbnZhciBnT1BEID0gJEdPUEQuZjtcbnZhciBkUCA9ICREUC5mO1xudmFyIGdPUE4gPSBnT1BORXh0LmY7XG52YXIgJFN5bWJvbCA9IGdsb2JhbC5TeW1ib2w7XG52YXIgJEpTT04gPSBnbG9iYWwuSlNPTjtcbnZhciBfc3RyaW5naWZ5ID0gJEpTT04gJiYgJEpTT04uc3RyaW5naWZ5O1xudmFyIFBST1RPVFlQRSA9ICdwcm90b3R5cGUnO1xudmFyIEhJRERFTiA9IHdrcygnX2hpZGRlbicpO1xudmFyIFRPX1BSSU1JVElWRSA9IHdrcygndG9QcmltaXRpdmUnKTtcbnZhciBpc0VudW0gPSB7fS5wcm9wZXJ0eUlzRW51bWVyYWJsZTtcbnZhciBTeW1ib2xSZWdpc3RyeSA9IHNoYXJlZCgnc3ltYm9sLXJlZ2lzdHJ5Jyk7XG52YXIgQWxsU3ltYm9scyA9IHNoYXJlZCgnc3ltYm9scycpO1xudmFyIE9QU3ltYm9scyA9IHNoYXJlZCgnb3Atc3ltYm9scycpO1xudmFyIE9iamVjdFByb3RvID0gT2JqZWN0W1BST1RPVFlQRV07XG52YXIgVVNFX05BVElWRSA9IHR5cGVvZiAkU3ltYm9sID09ICdmdW5jdGlvbicgJiYgISEkR09QUy5mO1xudmFyIFFPYmplY3QgPSBnbG9iYWwuUU9iamVjdDtcbi8vIERvbid0IHVzZSBzZXR0ZXJzIGluIFF0IFNjcmlwdCwgaHR0cHM6Ly9naXRodWIuY29tL3psb2lyb2NrL2NvcmUtanMvaXNzdWVzLzE3M1xudmFyIHNldHRlciA9ICFRT2JqZWN0IHx8ICFRT2JqZWN0W1BST1RPVFlQRV0gfHwgIVFPYmplY3RbUFJPVE9UWVBFXS5maW5kQ2hpbGQ7XG5cbi8vIGZhbGxiYWNrIGZvciBvbGQgQW5kcm9pZCwgaHR0cHM6Ly9jb2RlLmdvb2dsZS5jb20vcC92OC9pc3N1ZXMvZGV0YWlsP2lkPTY4N1xudmFyIHNldFN5bWJvbERlc2MgPSBERVNDUklQVE9SUyAmJiAkZmFpbHMoZnVuY3Rpb24gKCkge1xuICByZXR1cm4gX2NyZWF0ZShkUCh7fSwgJ2EnLCB7XG4gICAgZ2V0OiBmdW5jdGlvbiAoKSB7IHJldHVybiBkUCh0aGlzLCAnYScsIHsgdmFsdWU6IDcgfSkuYTsgfVxuICB9KSkuYSAhPSA3O1xufSkgPyBmdW5jdGlvbiAoaXQsIGtleSwgRCkge1xuICB2YXIgcHJvdG9EZXNjID0gZ09QRChPYmplY3RQcm90bywga2V5KTtcbiAgaWYgKHByb3RvRGVzYykgZGVsZXRlIE9iamVjdFByb3RvW2tleV07XG4gIGRQKGl0LCBrZXksIEQpO1xuICBpZiAocHJvdG9EZXNjICYmIGl0ICE9PSBPYmplY3RQcm90bykgZFAoT2JqZWN0UHJvdG8sIGtleSwgcHJvdG9EZXNjKTtcbn0gOiBkUDtcblxudmFyIHdyYXAgPSBmdW5jdGlvbiAodGFnKSB7XG4gIHZhciBzeW0gPSBBbGxTeW1ib2xzW3RhZ10gPSBfY3JlYXRlKCRTeW1ib2xbUFJPVE9UWVBFXSk7XG4gIHN5bS5fayA9IHRhZztcbiAgcmV0dXJuIHN5bTtcbn07XG5cbnZhciBpc1N5bWJvbCA9IFVTRV9OQVRJVkUgJiYgdHlwZW9mICRTeW1ib2wuaXRlcmF0b3IgPT0gJ3N5bWJvbCcgPyBmdW5jdGlvbiAoaXQpIHtcbiAgcmV0dXJuIHR5cGVvZiBpdCA9PSAnc3ltYm9sJztcbn0gOiBmdW5jdGlvbiAoaXQpIHtcbiAgcmV0dXJuIGl0IGluc3RhbmNlb2YgJFN5bWJvbDtcbn07XG5cbnZhciAkZGVmaW5lUHJvcGVydHkgPSBmdW5jdGlvbiBkZWZpbmVQcm9wZXJ0eShpdCwga2V5LCBEKSB7XG4gIGlmIChpdCA9PT0gT2JqZWN0UHJvdG8pICRkZWZpbmVQcm9wZXJ0eShPUFN5bWJvbHMsIGtleSwgRCk7XG4gIGFuT2JqZWN0KGl0KTtcbiAga2V5ID0gdG9QcmltaXRpdmUoa2V5LCB0cnVlKTtcbiAgYW5PYmplY3QoRCk7XG4gIGlmIChoYXMoQWxsU3ltYm9scywga2V5KSkge1xuICAgIGlmICghRC5lbnVtZXJhYmxlKSB7XG4gICAgICBpZiAoIWhhcyhpdCwgSElEREVOKSkgZFAoaXQsIEhJRERFTiwgY3JlYXRlRGVzYygxLCB7fSkpO1xuICAgICAgaXRbSElEREVOXVtrZXldID0gdHJ1ZTtcbiAgICB9IGVsc2Uge1xuICAgICAgaWYgKGhhcyhpdCwgSElEREVOKSAmJiBpdFtISURERU5dW2tleV0pIGl0W0hJRERFTl1ba2V5XSA9IGZhbHNlO1xuICAgICAgRCA9IF9jcmVhdGUoRCwgeyBlbnVtZXJhYmxlOiBjcmVhdGVEZXNjKDAsIGZhbHNlKSB9KTtcbiAgICB9IHJldHVybiBzZXRTeW1ib2xEZXNjKGl0LCBrZXksIEQpO1xuICB9IHJldHVybiBkUChpdCwga2V5LCBEKTtcbn07XG52YXIgJGRlZmluZVByb3BlcnRpZXMgPSBmdW5jdGlvbiBkZWZpbmVQcm9wZXJ0aWVzKGl0LCBQKSB7XG4gIGFuT2JqZWN0KGl0KTtcbiAgdmFyIGtleXMgPSBlbnVtS2V5cyhQID0gdG9JT2JqZWN0KFApKTtcbiAgdmFyIGkgPSAwO1xuICB2YXIgbCA9IGtleXMubGVuZ3RoO1xuICB2YXIga2V5O1xuICB3aGlsZSAobCA+IGkpICRkZWZpbmVQcm9wZXJ0eShpdCwga2V5ID0ga2V5c1tpKytdLCBQW2tleV0pO1xuICByZXR1cm4gaXQ7XG59O1xudmFyICRjcmVhdGUgPSBmdW5jdGlvbiBjcmVhdGUoaXQsIFApIHtcbiAgcmV0dXJuIFAgPT09IHVuZGVmaW5lZCA/IF9jcmVhdGUoaXQpIDogJGRlZmluZVByb3BlcnRpZXMoX2NyZWF0ZShpdCksIFApO1xufTtcbnZhciAkcHJvcGVydHlJc0VudW1lcmFibGUgPSBmdW5jdGlvbiBwcm9wZXJ0eUlzRW51bWVyYWJsZShrZXkpIHtcbiAgdmFyIEUgPSBpc0VudW0uY2FsbCh0aGlzLCBrZXkgPSB0b1ByaW1pdGl2ZShrZXksIHRydWUpKTtcbiAgaWYgKHRoaXMgPT09IE9iamVjdFByb3RvICYmIGhhcyhBbGxTeW1ib2xzLCBrZXkpICYmICFoYXMoT1BTeW1ib2xzLCBrZXkpKSByZXR1cm4gZmFsc2U7XG4gIHJldHVybiBFIHx8ICFoYXModGhpcywga2V5KSB8fCAhaGFzKEFsbFN5bWJvbHMsIGtleSkgfHwgaGFzKHRoaXMsIEhJRERFTikgJiYgdGhpc1tISURERU5dW2tleV0gPyBFIDogdHJ1ZTtcbn07XG52YXIgJGdldE93blByb3BlcnR5RGVzY3JpcHRvciA9IGZ1bmN0aW9uIGdldE93blByb3BlcnR5RGVzY3JpcHRvcihpdCwga2V5KSB7XG4gIGl0ID0gdG9JT2JqZWN0KGl0KTtcbiAga2V5ID0gdG9QcmltaXRpdmUoa2V5LCB0cnVlKTtcbiAgaWYgKGl0ID09PSBPYmplY3RQcm90byAmJiBoYXMoQWxsU3ltYm9scywga2V5KSAmJiAhaGFzKE9QU3ltYm9scywga2V5KSkgcmV0dXJuO1xuICB2YXIgRCA9IGdPUEQoaXQsIGtleSk7XG4gIGlmIChEICYmIGhhcyhBbGxTeW1ib2xzLCBrZXkpICYmICEoaGFzKGl0LCBISURERU4pICYmIGl0W0hJRERFTl1ba2V5XSkpIEQuZW51bWVyYWJsZSA9IHRydWU7XG4gIHJldHVybiBEO1xufTtcbnZhciAkZ2V0T3duUHJvcGVydHlOYW1lcyA9IGZ1bmN0aW9uIGdldE93blByb3BlcnR5TmFtZXMoaXQpIHtcbiAgdmFyIG5hbWVzID0gZ09QTih0b0lPYmplY3QoaXQpKTtcbiAgdmFyIHJlc3VsdCA9IFtdO1xuICB2YXIgaSA9IDA7XG4gIHZhciBrZXk7XG4gIHdoaWxlIChuYW1lcy5sZW5ndGggPiBpKSB7XG4gICAgaWYgKCFoYXMoQWxsU3ltYm9scywga2V5ID0gbmFtZXNbaSsrXSkgJiYga2V5ICE9IEhJRERFTiAmJiBrZXkgIT0gTUVUQSkgcmVzdWx0LnB1c2goa2V5KTtcbiAgfSByZXR1cm4gcmVzdWx0O1xufTtcbnZhciAkZ2V0T3duUHJvcGVydHlTeW1ib2xzID0gZnVuY3Rpb24gZ2V0T3duUHJvcGVydHlTeW1ib2xzKGl0KSB7XG4gIHZhciBJU19PUCA9IGl0ID09PSBPYmplY3RQcm90bztcbiAgdmFyIG5hbWVzID0gZ09QTihJU19PUCA/IE9QU3ltYm9scyA6IHRvSU9iamVjdChpdCkpO1xuICB2YXIgcmVzdWx0ID0gW107XG4gIHZhciBpID0gMDtcbiAgdmFyIGtleTtcbiAgd2hpbGUgKG5hbWVzLmxlbmd0aCA+IGkpIHtcbiAgICBpZiAoaGFzKEFsbFN5bWJvbHMsIGtleSA9IG5hbWVzW2krK10pICYmIChJU19PUCA/IGhhcyhPYmplY3RQcm90bywga2V5KSA6IHRydWUpKSByZXN1bHQucHVzaChBbGxTeW1ib2xzW2tleV0pO1xuICB9IHJldHVybiByZXN1bHQ7XG59O1xuXG4vLyAxOS40LjEuMSBTeW1ib2woW2Rlc2NyaXB0aW9uXSlcbmlmICghVVNFX05BVElWRSkge1xuICAkU3ltYm9sID0gZnVuY3Rpb24gU3ltYm9sKCkge1xuICAgIGlmICh0aGlzIGluc3RhbmNlb2YgJFN5bWJvbCkgdGhyb3cgVHlwZUVycm9yKCdTeW1ib2wgaXMgbm90IGEgY29uc3RydWN0b3IhJyk7XG4gICAgdmFyIHRhZyA9IHVpZChhcmd1bWVudHMubGVuZ3RoID4gMCA/IGFyZ3VtZW50c1swXSA6IHVuZGVmaW5lZCk7XG4gICAgdmFyICRzZXQgPSBmdW5jdGlvbiAodmFsdWUpIHtcbiAgICAgIGlmICh0aGlzID09PSBPYmplY3RQcm90bykgJHNldC5jYWxsKE9QU3ltYm9scywgdmFsdWUpO1xuICAgICAgaWYgKGhhcyh0aGlzLCBISURERU4pICYmIGhhcyh0aGlzW0hJRERFTl0sIHRhZykpIHRoaXNbSElEREVOXVt0YWddID0gZmFsc2U7XG4gICAgICBzZXRTeW1ib2xEZXNjKHRoaXMsIHRhZywgY3JlYXRlRGVzYygxLCB2YWx1ZSkpO1xuICAgIH07XG4gICAgaWYgKERFU0NSSVBUT1JTICYmIHNldHRlcikgc2V0U3ltYm9sRGVzYyhPYmplY3RQcm90bywgdGFnLCB7IGNvbmZpZ3VyYWJsZTogdHJ1ZSwgc2V0OiAkc2V0IH0pO1xuICAgIHJldHVybiB3cmFwKHRhZyk7XG4gIH07XG4gIHJlZGVmaW5lKCRTeW1ib2xbUFJPVE9UWVBFXSwgJ3RvU3RyaW5nJywgZnVuY3Rpb24gdG9TdHJpbmcoKSB7XG4gICAgcmV0dXJuIHRoaXMuX2s7XG4gIH0pO1xuXG4gICRHT1BELmYgPSAkZ2V0T3duUHJvcGVydHlEZXNjcmlwdG9yO1xuICAkRFAuZiA9ICRkZWZpbmVQcm9wZXJ0eTtcbiAgcmVxdWlyZSgnLi9fb2JqZWN0LWdvcG4nKS5mID0gZ09QTkV4dC5mID0gJGdldE93blByb3BlcnR5TmFtZXM7XG4gIHJlcXVpcmUoJy4vX29iamVjdC1waWUnKS5mID0gJHByb3BlcnR5SXNFbnVtZXJhYmxlO1xuICAkR09QUy5mID0gJGdldE93blByb3BlcnR5U3ltYm9scztcblxuICBpZiAoREVTQ1JJUFRPUlMgJiYgIXJlcXVpcmUoJy4vX2xpYnJhcnknKSkge1xuICAgIHJlZGVmaW5lKE9iamVjdFByb3RvLCAncHJvcGVydHlJc0VudW1lcmFibGUnLCAkcHJvcGVydHlJc0VudW1lcmFibGUsIHRydWUpO1xuICB9XG5cbiAgd2tzRXh0LmYgPSBmdW5jdGlvbiAobmFtZSkge1xuICAgIHJldHVybiB3cmFwKHdrcyhuYW1lKSk7XG4gIH07XG59XG5cbiRleHBvcnQoJGV4cG9ydC5HICsgJGV4cG9ydC5XICsgJGV4cG9ydC5GICogIVVTRV9OQVRJVkUsIHsgU3ltYm9sOiAkU3ltYm9sIH0pO1xuXG5mb3IgKHZhciBlczZTeW1ib2xzID0gKFxuICAvLyAxOS40LjIuMiwgMTkuNC4yLjMsIDE5LjQuMi40LCAxOS40LjIuNiwgMTkuNC4yLjgsIDE5LjQuMi45LCAxOS40LjIuMTAsIDE5LjQuMi4xMSwgMTkuNC4yLjEyLCAxOS40LjIuMTMsIDE5LjQuMi4xNFxuICAnaGFzSW5zdGFuY2UsaXNDb25jYXRTcHJlYWRhYmxlLGl0ZXJhdG9yLG1hdGNoLHJlcGxhY2Usc2VhcmNoLHNwZWNpZXMsc3BsaXQsdG9QcmltaXRpdmUsdG9TdHJpbmdUYWcsdW5zY29wYWJsZXMnXG4pLnNwbGl0KCcsJyksIGogPSAwOyBlczZTeW1ib2xzLmxlbmd0aCA+IGo7KXdrcyhlczZTeW1ib2xzW2orK10pO1xuXG5mb3IgKHZhciB3ZWxsS25vd25TeW1ib2xzID0gJGtleXMod2tzLnN0b3JlKSwgayA9IDA7IHdlbGxLbm93blN5bWJvbHMubGVuZ3RoID4gazspIHdrc0RlZmluZSh3ZWxsS25vd25TeW1ib2xzW2srK10pO1xuXG4kZXhwb3J0KCRleHBvcnQuUyArICRleHBvcnQuRiAqICFVU0VfTkFUSVZFLCAnU3ltYm9sJywge1xuICAvLyAxOS40LjIuMSBTeW1ib2wuZm9yKGtleSlcbiAgJ2Zvcic6IGZ1bmN0aW9uIChrZXkpIHtcbiAgICByZXR1cm4gaGFzKFN5bWJvbFJlZ2lzdHJ5LCBrZXkgKz0gJycpXG4gICAgICA/IFN5bWJvbFJlZ2lzdHJ5W2tleV1cbiAgICAgIDogU3ltYm9sUmVnaXN0cnlba2V5XSA9ICRTeW1ib2woa2V5KTtcbiAgfSxcbiAgLy8gMTkuNC4yLjUgU3ltYm9sLmtleUZvcihzeW0pXG4gIGtleUZvcjogZnVuY3Rpb24ga2V5Rm9yKHN5bSkge1xuICAgIGlmICghaXNTeW1ib2woc3ltKSkgdGhyb3cgVHlwZUVycm9yKHN5bSArICcgaXMgbm90IGEgc3ltYm9sIScpO1xuICAgIGZvciAodmFyIGtleSBpbiBTeW1ib2xSZWdpc3RyeSkgaWYgKFN5bWJvbFJlZ2lzdHJ5W2tleV0gPT09IHN5bSkgcmV0dXJuIGtleTtcbiAgfSxcbiAgdXNlU2V0dGVyOiBmdW5jdGlvbiAoKSB7IHNldHRlciA9IHRydWU7IH0sXG4gIHVzZVNpbXBsZTogZnVuY3Rpb24gKCkgeyBzZXR0ZXIgPSBmYWxzZTsgfVxufSk7XG5cbiRleHBvcnQoJGV4cG9ydC5TICsgJGV4cG9ydC5GICogIVVTRV9OQVRJVkUsICdPYmplY3QnLCB7XG4gIC8vIDE5LjEuMi4yIE9iamVjdC5jcmVhdGUoTyBbLCBQcm9wZXJ0aWVzXSlcbiAgY3JlYXRlOiAkY3JlYXRlLFxuICAvLyAxOS4xLjIuNCBPYmplY3QuZGVmaW5lUHJvcGVydHkoTywgUCwgQXR0cmlidXRlcylcbiAgZGVmaW5lUHJvcGVydHk6ICRkZWZpbmVQcm9wZXJ0eSxcbiAgLy8gMTkuMS4yLjMgT2JqZWN0LmRlZmluZVByb3BlcnRpZXMoTywgUHJvcGVydGllcylcbiAgZGVmaW5lUHJvcGVydGllczogJGRlZmluZVByb3BlcnRpZXMsXG4gIC8vIDE5LjEuMi42IE9iamVjdC5nZXRPd25Qcm9wZXJ0eURlc2NyaXB0b3IoTywgUClcbiAgZ2V0T3duUHJvcGVydHlEZXNjcmlwdG9yOiAkZ2V0T3duUHJvcGVydHlEZXNjcmlwdG9yLFxuICAvLyAxOS4xLjIuNyBPYmplY3QuZ2V0T3duUHJvcGVydHlOYW1lcyhPKVxuICBnZXRPd25Qcm9wZXJ0eU5hbWVzOiAkZ2V0T3duUHJvcGVydHlOYW1lcyxcbiAgLy8gMTkuMS4yLjggT2JqZWN0LmdldE93blByb3BlcnR5U3ltYm9scyhPKVxuICBnZXRPd25Qcm9wZXJ0eVN5bWJvbHM6ICRnZXRPd25Qcm9wZXJ0eVN5bWJvbHNcbn0pO1xuXG4vLyBDaHJvbWUgMzggYW5kIDM5IGBPYmplY3QuZ2V0T3duUHJvcGVydHlTeW1ib2xzYCBmYWlscyBvbiBwcmltaXRpdmVzXG4vLyBodHRwczovL2J1Z3MuY2hyb21pdW0ub3JnL3AvdjgvaXNzdWVzL2RldGFpbD9pZD0zNDQzXG52YXIgRkFJTFNfT05fUFJJTUlUSVZFUyA9ICRmYWlscyhmdW5jdGlvbiAoKSB7ICRHT1BTLmYoMSk7IH0pO1xuXG4kZXhwb3J0KCRleHBvcnQuUyArICRleHBvcnQuRiAqIEZBSUxTX09OX1BSSU1JVElWRVMsICdPYmplY3QnLCB7XG4gIGdldE93blByb3BlcnR5U3ltYm9sczogZnVuY3Rpb24gZ2V0T3duUHJvcGVydHlTeW1ib2xzKGl0KSB7XG4gICAgcmV0dXJuICRHT1BTLmYodG9PYmplY3QoaXQpKTtcbiAgfVxufSk7XG5cbi8vIDI0LjMuMiBKU09OLnN0cmluZ2lmeSh2YWx1ZSBbLCByZXBsYWNlciBbLCBzcGFjZV1dKVxuJEpTT04gJiYgJGV4cG9ydCgkZXhwb3J0LlMgKyAkZXhwb3J0LkYgKiAoIVVTRV9OQVRJVkUgfHwgJGZhaWxzKGZ1bmN0aW9uICgpIHtcbiAgdmFyIFMgPSAkU3ltYm9sKCk7XG4gIC8vIE1TIEVkZ2UgY29udmVydHMgc3ltYm9sIHZhbHVlcyB0byBKU09OIGFzIHt9XG4gIC8vIFdlYktpdCBjb252ZXJ0cyBzeW1ib2wgdmFsdWVzIHRvIEpTT04gYXMgbnVsbFxuICAvLyBWOCB0aHJvd3Mgb24gYm94ZWQgc3ltYm9sc1xuICByZXR1cm4gX3N0cmluZ2lmeShbU10pICE9ICdbbnVsbF0nIHx8IF9zdHJpbmdpZnkoeyBhOiBTIH0pICE9ICd7fScgfHwgX3N0cmluZ2lmeShPYmplY3QoUykpICE9ICd7fSc7XG59KSksICdKU09OJywge1xuICBzdHJpbmdpZnk6IGZ1bmN0aW9uIHN0cmluZ2lmeShpdCkge1xuICAgIHZhciBhcmdzID0gW2l0XTtcbiAgICB2YXIgaSA9IDE7XG4gICAgdmFyIHJlcGxhY2VyLCAkcmVwbGFjZXI7XG4gICAgd2hpbGUgKGFyZ3VtZW50cy5sZW5ndGggPiBpKSBhcmdzLnB1c2goYXJndW1lbnRzW2krK10pO1xuICAgICRyZXBsYWNlciA9IHJlcGxhY2VyID0gYXJnc1sxXTtcbiAgICBpZiAoIWlzT2JqZWN0KHJlcGxhY2VyKSAmJiBpdCA9PT0gdW5kZWZpbmVkIHx8IGlzU3ltYm9sKGl0KSkgcmV0dXJuOyAvLyBJRTggcmV0dXJucyBzdHJpbmcgb24gdW5kZWZpbmVkXG4gICAgaWYgKCFpc0FycmF5KHJlcGxhY2VyKSkgcmVwbGFjZXIgPSBmdW5jdGlvbiAoa2V5LCB2YWx1ZSkge1xuICAgICAgaWYgKHR5cGVvZiAkcmVwbGFjZXIgPT0gJ2Z1bmN0aW9uJykgdmFsdWUgPSAkcmVwbGFjZXIuY2FsbCh0aGlzLCBrZXksIHZhbHVlKTtcbiAgICAgIGlmICghaXNTeW1ib2wodmFsdWUpKSByZXR1cm4gdmFsdWU7XG4gICAgfTtcbiAgICBhcmdzWzFdID0gcmVwbGFjZXI7XG4gICAgcmV0dXJuIF9zdHJpbmdpZnkuYXBwbHkoJEpTT04sIGFyZ3MpO1xuICB9XG59KTtcblxuLy8gMTkuNC4zLjQgU3ltYm9sLnByb3RvdHlwZVtAQHRvUHJpbWl0aXZlXShoaW50KVxuJFN5bWJvbFtQUk9UT1RZUEVdW1RPX1BSSU1JVElWRV0gfHwgcmVxdWlyZSgnLi9faGlkZScpKCRTeW1ib2xbUFJPVE9UWVBFXSwgVE9fUFJJTUlUSVZFLCAkU3ltYm9sW1BST1RPVFlQRV0udmFsdWVPZik7XG4vLyAxOS40LjMuNSBTeW1ib2wucHJvdG90eXBlW0BAdG9TdHJpbmdUYWddXG5zZXRUb1N0cmluZ1RhZygkU3ltYm9sLCAnU3ltYm9sJyk7XG4vLyAyMC4yLjEuOSBNYXRoW0BAdG9TdHJpbmdUYWddXG5zZXRUb1N0cmluZ1RhZyhNYXRoLCAnTWF0aCcsIHRydWUpO1xuLy8gMjQuMy4zIEpTT05bQEB0b1N0cmluZ1RhZ11cbnNldFRvU3RyaW5nVGFnKGdsb2JhbC5KU09OLCAnSlNPTicsIHRydWUpO1xuIiwiLy8gaHR0cHM6Ly9naXRodWIuY29tL3RjMzkvcHJvcG9zYWwtb2JqZWN0LXZhbHVlcy1lbnRyaWVzXG52YXIgJGV4cG9ydCA9IHJlcXVpcmUoJy4vX2V4cG9ydCcpO1xudmFyICR2YWx1ZXMgPSByZXF1aXJlKCcuL19vYmplY3QtdG8tYXJyYXknKShmYWxzZSk7XG5cbiRleHBvcnQoJGV4cG9ydC5TLCAnT2JqZWN0Jywge1xuICB2YWx1ZXM6IGZ1bmN0aW9uIHZhbHVlcyhpdCkge1xuICAgIHJldHVybiAkdmFsdWVzKGl0KTtcbiAgfVxufSk7XG4iLCJyZXF1aXJlKCcuL193a3MtZGVmaW5lJykoJ2FzeW5jSXRlcmF0b3InKTtcbiIsInJlcXVpcmUoJy4vX3drcy1kZWZpbmUnKSgnb2JzZXJ2YWJsZScpO1xuIiwicmVxdWlyZSgnLi9lczYuYXJyYXkuaXRlcmF0b3InKTtcbnZhciBnbG9iYWwgPSByZXF1aXJlKCcuL19nbG9iYWwnKTtcbnZhciBoaWRlID0gcmVxdWlyZSgnLi9faGlkZScpO1xudmFyIEl0ZXJhdG9ycyA9IHJlcXVpcmUoJy4vX2l0ZXJhdG9ycycpO1xudmFyIFRPX1NUUklOR19UQUcgPSByZXF1aXJlKCcuL193a3MnKSgndG9TdHJpbmdUYWcnKTtcblxudmFyIERPTUl0ZXJhYmxlcyA9ICgnQ1NTUnVsZUxpc3QsQ1NTU3R5bGVEZWNsYXJhdGlvbixDU1NWYWx1ZUxpc3QsQ2xpZW50UmVjdExpc3QsRE9NUmVjdExpc3QsRE9NU3RyaW5nTGlzdCwnICtcbiAgJ0RPTVRva2VuTGlzdCxEYXRhVHJhbnNmZXJJdGVtTGlzdCxGaWxlTGlzdCxIVE1MQWxsQ29sbGVjdGlvbixIVE1MQ29sbGVjdGlvbixIVE1MRm9ybUVsZW1lbnQsSFRNTFNlbGVjdEVsZW1lbnQsJyArXG4gICdNZWRpYUxpc3QsTWltZVR5cGVBcnJheSxOYW1lZE5vZGVNYXAsTm9kZUxpc3QsUGFpbnRSZXF1ZXN0TGlzdCxQbHVnaW4sUGx1Z2luQXJyYXksU1ZHTGVuZ3RoTGlzdCxTVkdOdW1iZXJMaXN0LCcgK1xuICAnU1ZHUGF0aFNlZ0xpc3QsU1ZHUG9pbnRMaXN0LFNWR1N0cmluZ0xpc3QsU1ZHVHJhbnNmb3JtTGlzdCxTb3VyY2VCdWZmZXJMaXN0LFN0eWxlU2hlZXRMaXN0LFRleHRUcmFja0N1ZUxpc3QsJyArXG4gICdUZXh0VHJhY2tMaXN0LFRvdWNoTGlzdCcpLnNwbGl0KCcsJyk7XG5cbmZvciAodmFyIGkgPSAwOyBpIDwgRE9NSXRlcmFibGVzLmxlbmd0aDsgaSsrKSB7XG4gIHZhciBOQU1FID0gRE9NSXRlcmFibGVzW2ldO1xuICB2YXIgQ29sbGVjdGlvbiA9IGdsb2JhbFtOQU1FXTtcbiAgdmFyIHByb3RvID0gQ29sbGVjdGlvbiAmJiBDb2xsZWN0aW9uLnByb3RvdHlwZTtcbiAgaWYgKHByb3RvICYmICFwcm90b1tUT19TVFJJTkdfVEFHXSkgaGlkZShwcm90bywgVE9fU1RSSU5HX1RBRywgTkFNRSk7XG4gIEl0ZXJhdG9yc1tOQU1FXSA9IEl0ZXJhdG9ycy5BcnJheTtcbn1cbiIsIi8vIENvcHlyaWdodCBKb3llbnQsIEluYy4gYW5kIG90aGVyIE5vZGUgY29udHJpYnV0b3JzLlxuLy9cbi8vIFBlcm1pc3Npb24gaXMgaGVyZWJ5IGdyYW50ZWQsIGZyZWUgb2YgY2hhcmdlLCB0byBhbnkgcGVyc29uIG9idGFpbmluZyBhXG4vLyBjb3B5IG9mIHRoaXMgc29mdHdhcmUgYW5kIGFzc29jaWF0ZWQgZG9jdW1lbnRhdGlvbiBmaWxlcyAodGhlXG4vLyBcIlNvZnR3YXJlXCIpLCB0byBkZWFsIGluIHRoZSBTb2Z0d2FyZSB3aXRob3V0IHJlc3RyaWN0aW9uLCBpbmNsdWRpbmdcbi8vIHdpdGhvdXQgbGltaXRhdGlvbiB0aGUgcmlnaHRzIHRvIHVzZSwgY29weSwgbW9kaWZ5LCBtZXJnZSwgcHVibGlzaCxcbi8vIGRpc3RyaWJ1dGUsIHN1YmxpY2Vuc2UsIGFuZC9vciBzZWxsIGNvcGllcyBvZiB0aGUgU29mdHdhcmUsIGFuZCB0byBwZXJtaXRcbi8vIHBlcnNvbnMgdG8gd2hvbSB0aGUgU29mdHdhcmUgaXMgZnVybmlzaGVkIHRvIGRvIHNvLCBzdWJqZWN0IHRvIHRoZVxuLy8gZm9sbG93aW5nIGNvbmRpdGlvbnM6XG4vL1xuLy8gVGhlIGFib3ZlIGNvcHlyaWdodCBub3RpY2UgYW5kIHRoaXMgcGVybWlzc2lvbiBub3RpY2Ugc2hhbGwgYmUgaW5jbHVkZWRcbi8vIGluIGFsbCBjb3BpZXMgb3Igc3Vic3RhbnRpYWwgcG9ydGlvbnMgb2YgdGhlIFNvZnR3YXJlLlxuLy9cbi8vIFRIRSBTT0ZUV0FSRSBJUyBQUk9WSURFRCBcIkFTIElTXCIsIFdJVEhPVVQgV0FSUkFOVFkgT0YgQU5ZIEtJTkQsIEVYUFJFU1Ncbi8vIE9SIElNUExJRUQsIElOQ0xVRElORyBCVVQgTk9UIExJTUlURUQgVE8gVEhFIFdBUlJBTlRJRVMgT0Zcbi8vIE1FUkNIQU5UQUJJTElUWSwgRklUTkVTUyBGT1IgQSBQQVJUSUNVTEFSIFBVUlBPU0UgQU5EIE5PTklORlJJTkdFTUVOVC4gSU5cbi8vIE5PIEVWRU5UIFNIQUxMIFRIRSBBVVRIT1JTIE9SIENPUFlSSUdIVCBIT0xERVJTIEJFIExJQUJMRSBGT1IgQU5ZIENMQUlNLFxuLy8gREFNQUdFUyBPUiBPVEhFUiBMSUFCSUxJVFksIFdIRVRIRVIgSU4gQU4gQUNUSU9OIE9GIENPTlRSQUNULCBUT1JUIE9SXG4vLyBPVEhFUldJU0UsIEFSSVNJTkcgRlJPTSwgT1VUIE9GIE9SIElOIENPTk5FQ1RJT04gV0lUSCBUSEUgU09GVFdBUkUgT1IgVEhFXG4vLyBVU0UgT1IgT1RIRVIgREVBTElOR1MgSU4gVEhFIFNPRlRXQVJFLlxuXG4ndXNlIHN0cmljdCc7XG5cbnZhciBSID0gdHlwZW9mIFJlZmxlY3QgPT09ICdvYmplY3QnID8gUmVmbGVjdCA6IG51bGxcbnZhciBSZWZsZWN0QXBwbHkgPSBSICYmIHR5cGVvZiBSLmFwcGx5ID09PSAnZnVuY3Rpb24nXG4gID8gUi5hcHBseVxuICA6IGZ1bmN0aW9uIFJlZmxlY3RBcHBseSh0YXJnZXQsIHJlY2VpdmVyLCBhcmdzKSB7XG4gICAgcmV0dXJuIEZ1bmN0aW9uLnByb3RvdHlwZS5hcHBseS5jYWxsKHRhcmdldCwgcmVjZWl2ZXIsIGFyZ3MpO1xuICB9XG5cbnZhciBSZWZsZWN0T3duS2V5c1xuaWYgKFIgJiYgdHlwZW9mIFIub3duS2V5cyA9PT0gJ2Z1bmN0aW9uJykge1xuICBSZWZsZWN0T3duS2V5cyA9IFIub3duS2V5c1xufSBlbHNlIGlmIChPYmplY3QuZ2V0T3duUHJvcGVydHlTeW1ib2xzKSB7XG4gIFJlZmxlY3RPd25LZXlzID0gZnVuY3Rpb24gUmVmbGVjdE93bktleXModGFyZ2V0KSB7XG4gICAgcmV0dXJuIE9iamVjdC5nZXRPd25Qcm9wZXJ0eU5hbWVzKHRhcmdldClcbiAgICAgIC5jb25jYXQoT2JqZWN0LmdldE93blByb3BlcnR5U3ltYm9scyh0YXJnZXQpKTtcbiAgfTtcbn0gZWxzZSB7XG4gIFJlZmxlY3RPd25LZXlzID0gZnVuY3Rpb24gUmVmbGVjdE93bktleXModGFyZ2V0KSB7XG4gICAgcmV0dXJuIE9iamVjdC5nZXRPd25Qcm9wZXJ0eU5hbWVzKHRhcmdldCk7XG4gIH07XG59XG5cbmZ1bmN0aW9uIFByb2Nlc3NFbWl0V2FybmluZyh3YXJuaW5nKSB7XG4gIGlmIChjb25zb2xlICYmIGNvbnNvbGUud2FybikgY29uc29sZS53YXJuKHdhcm5pbmcpO1xufVxuXG52YXIgTnVtYmVySXNOYU4gPSBOdW1iZXIuaXNOYU4gfHwgZnVuY3Rpb24gTnVtYmVySXNOYU4odmFsdWUpIHtcbiAgcmV0dXJuIHZhbHVlICE9PSB2YWx1ZTtcbn1cblxuZnVuY3Rpb24gRXZlbnRFbWl0dGVyKCkge1xuICBFdmVudEVtaXR0ZXIuaW5pdC5jYWxsKHRoaXMpO1xufVxubW9kdWxlLmV4cG9ydHMgPSBFdmVudEVtaXR0ZXI7XG5cbi8vIEJhY2t3YXJkcy1jb21wYXQgd2l0aCBub2RlIDAuMTAueFxuRXZlbnRFbWl0dGVyLkV2ZW50RW1pdHRlciA9IEV2ZW50RW1pdHRlcjtcblxuRXZlbnRFbWl0dGVyLnByb3RvdHlwZS5fZXZlbnRzID0gdW5kZWZpbmVkO1xuRXZlbnRFbWl0dGVyLnByb3RvdHlwZS5fZXZlbnRzQ291bnQgPSAwO1xuRXZlbnRFbWl0dGVyLnByb3RvdHlwZS5fbWF4TGlzdGVuZXJzID0gdW5kZWZpbmVkO1xuXG4vLyBCeSBkZWZhdWx0IEV2ZW50RW1pdHRlcnMgd2lsbCBwcmludCBhIHdhcm5pbmcgaWYgbW9yZSB0aGFuIDEwIGxpc3RlbmVycyBhcmVcbi8vIGFkZGVkIHRvIGl0LiBUaGlzIGlzIGEgdXNlZnVsIGRlZmF1bHQgd2hpY2ggaGVscHMgZmluZGluZyBtZW1vcnkgbGVha3MuXG52YXIgZGVmYXVsdE1heExpc3RlbmVycyA9IDEwO1xuXG5mdW5jdGlvbiBjaGVja0xpc3RlbmVyKGxpc3RlbmVyKSB7XG4gIGlmICh0eXBlb2YgbGlzdGVuZXIgIT09ICdmdW5jdGlvbicpIHtcbiAgICB0aHJvdyBuZXcgVHlwZUVycm9yKCdUaGUgXCJsaXN0ZW5lclwiIGFyZ3VtZW50IG11c3QgYmUgb2YgdHlwZSBGdW5jdGlvbi4gUmVjZWl2ZWQgdHlwZSAnICsgdHlwZW9mIGxpc3RlbmVyKTtcbiAgfVxufVxuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoRXZlbnRFbWl0dGVyLCAnZGVmYXVsdE1heExpc3RlbmVycycsIHtcbiAgZW51bWVyYWJsZTogdHJ1ZSxcbiAgZ2V0OiBmdW5jdGlvbigpIHtcbiAgICByZXR1cm4gZGVmYXVsdE1heExpc3RlbmVycztcbiAgfSxcbiAgc2V0OiBmdW5jdGlvbihhcmcpIHtcbiAgICBpZiAodHlwZW9mIGFyZyAhPT0gJ251bWJlcicgfHwgYXJnIDwgMCB8fCBOdW1iZXJJc05hTihhcmcpKSB7XG4gICAgICB0aHJvdyBuZXcgUmFuZ2VFcnJvcignVGhlIHZhbHVlIG9mIFwiZGVmYXVsdE1heExpc3RlbmVyc1wiIGlzIG91dCBvZiByYW5nZS4gSXQgbXVzdCBiZSBhIG5vbi1uZWdhdGl2ZSBudW1iZXIuIFJlY2VpdmVkICcgKyBhcmcgKyAnLicpO1xuICAgIH1cbiAgICBkZWZhdWx0TWF4TGlzdGVuZXJzID0gYXJnO1xuICB9XG59KTtcblxuRXZlbnRFbWl0dGVyLmluaXQgPSBmdW5jdGlvbigpIHtcblxuICBpZiAodGhpcy5fZXZlbnRzID09PSB1bmRlZmluZWQgfHxcbiAgICAgIHRoaXMuX2V2ZW50cyA9PT0gT2JqZWN0LmdldFByb3RvdHlwZU9mKHRoaXMpLl9ldmVudHMpIHtcbiAgICB0aGlzLl9ldmVudHMgPSBPYmplY3QuY3JlYXRlKG51bGwpO1xuICAgIHRoaXMuX2V2ZW50c0NvdW50ID0gMDtcbiAgfVxuXG4gIHRoaXMuX21heExpc3RlbmVycyA9IHRoaXMuX21heExpc3RlbmVycyB8fCB1bmRlZmluZWQ7XG59O1xuXG4vLyBPYnZpb3VzbHkgbm90IGFsbCBFbWl0dGVycyBzaG91bGQgYmUgbGltaXRlZCB0byAxMC4gVGhpcyBmdW5jdGlvbiBhbGxvd3Ncbi8vIHRoYXQgdG8gYmUgaW5jcmVhc2VkLiBTZXQgdG8gemVybyBmb3IgdW5saW1pdGVkLlxuRXZlbnRFbWl0dGVyLnByb3RvdHlwZS5zZXRNYXhMaXN0ZW5lcnMgPSBmdW5jdGlvbiBzZXRNYXhMaXN0ZW5lcnMobikge1xuICBpZiAodHlwZW9mIG4gIT09ICdudW1iZXInIHx8IG4gPCAwIHx8IE51bWJlcklzTmFOKG4pKSB7XG4gICAgdGhyb3cgbmV3IFJhbmdlRXJyb3IoJ1RoZSB2YWx1ZSBvZiBcIm5cIiBpcyBvdXQgb2YgcmFuZ2UuIEl0IG11c3QgYmUgYSBub24tbmVnYXRpdmUgbnVtYmVyLiBSZWNlaXZlZCAnICsgbiArICcuJyk7XG4gIH1cbiAgdGhpcy5fbWF4TGlzdGVuZXJzID0gbjtcbiAgcmV0dXJuIHRoaXM7XG59O1xuXG5mdW5jdGlvbiBfZ2V0TWF4TGlzdGVuZXJzKHRoYXQpIHtcbiAgaWYgKHRoYXQuX21heExpc3RlbmVycyA9PT0gdW5kZWZpbmVkKVxuICAgIHJldHVybiBFdmVudEVtaXR0ZXIuZGVmYXVsdE1heExpc3RlbmVycztcbiAgcmV0dXJuIHRoYXQuX21heExpc3RlbmVycztcbn1cblxuRXZlbnRFbWl0dGVyLnByb3RvdHlwZS5nZXRNYXhMaXN0ZW5lcnMgPSBmdW5jdGlvbiBnZXRNYXhMaXN0ZW5lcnMoKSB7XG4gIHJldHVybiBfZ2V0TWF4TGlzdGVuZXJzKHRoaXMpO1xufTtcblxuRXZlbnRFbWl0dGVyLnByb3RvdHlwZS5lbWl0ID0gZnVuY3Rpb24gZW1pdCh0eXBlKSB7XG4gIHZhciBhcmdzID0gW107XG4gIGZvciAodmFyIGkgPSAxOyBpIDwgYXJndW1lbnRzLmxlbmd0aDsgaSsrKSBhcmdzLnB1c2goYXJndW1lbnRzW2ldKTtcbiAgdmFyIGRvRXJyb3IgPSAodHlwZSA9PT0gJ2Vycm9yJyk7XG5cbiAgdmFyIGV2ZW50cyA9IHRoaXMuX2V2ZW50cztcbiAgaWYgKGV2ZW50cyAhPT0gdW5kZWZpbmVkKVxuICAgIGRvRXJyb3IgPSAoZG9FcnJvciAmJiBldmVudHMuZXJyb3IgPT09IHVuZGVmaW5lZCk7XG4gIGVsc2UgaWYgKCFkb0Vycm9yKVxuICAgIHJldHVybiBmYWxzZTtcblxuICAvLyBJZiB0aGVyZSBpcyBubyAnZXJyb3InIGV2ZW50IGxpc3RlbmVyIHRoZW4gdGhyb3cuXG4gIGlmIChkb0Vycm9yKSB7XG4gICAgdmFyIGVyO1xuICAgIGlmIChhcmdzLmxlbmd0aCA+IDApXG4gICAgICBlciA9IGFyZ3NbMF07XG4gICAgaWYgKGVyIGluc3RhbmNlb2YgRXJyb3IpIHtcbiAgICAgIC8vIE5vdGU6IFRoZSBjb21tZW50cyBvbiB0aGUgYHRocm93YCBsaW5lcyBhcmUgaW50ZW50aW9uYWwsIHRoZXkgc2hvd1xuICAgICAgLy8gdXAgaW4gTm9kZSdzIG91dHB1dCBpZiB0aGlzIHJlc3VsdHMgaW4gYW4gdW5oYW5kbGVkIGV4Y2VwdGlvbi5cbiAgICAgIHRocm93IGVyOyAvLyBVbmhhbmRsZWQgJ2Vycm9yJyBldmVudFxuICAgIH1cbiAgICAvLyBBdCBsZWFzdCBnaXZlIHNvbWUga2luZCBvZiBjb250ZXh0IHRvIHRoZSB1c2VyXG4gICAgdmFyIGVyciA9IG5ldyBFcnJvcignVW5oYW5kbGVkIGVycm9yLicgKyAoZXIgPyAnICgnICsgZXIubWVzc2FnZSArICcpJyA6ICcnKSk7XG4gICAgZXJyLmNvbnRleHQgPSBlcjtcbiAgICB0aHJvdyBlcnI7IC8vIFVuaGFuZGxlZCAnZXJyb3InIGV2ZW50XG4gIH1cblxuICB2YXIgaGFuZGxlciA9IGV2ZW50c1t0eXBlXTtcblxuICBpZiAoaGFuZGxlciA9PT0gdW5kZWZpbmVkKVxuICAgIHJldHVybiBmYWxzZTtcblxuICBpZiAodHlwZW9mIGhhbmRsZXIgPT09ICdmdW5jdGlvbicpIHtcbiAgICBSZWZsZWN0QXBwbHkoaGFuZGxlciwgdGhpcywgYXJncyk7XG4gIH0gZWxzZSB7XG4gICAgdmFyIGxlbiA9IGhhbmRsZXIubGVuZ3RoO1xuICAgIHZhciBsaXN0ZW5lcnMgPSBhcnJheUNsb25lKGhhbmRsZXIsIGxlbik7XG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBsZW47ICsraSlcbiAgICAgIFJlZmxlY3RBcHBseShsaXN0ZW5lcnNbaV0sIHRoaXMsIGFyZ3MpO1xuICB9XG5cbiAgcmV0dXJuIHRydWU7XG59O1xuXG5mdW5jdGlvbiBfYWRkTGlzdGVuZXIodGFyZ2V0LCB0eXBlLCBsaXN0ZW5lciwgcHJlcGVuZCkge1xuICB2YXIgbTtcbiAgdmFyIGV2ZW50cztcbiAgdmFyIGV4aXN0aW5nO1xuXG4gIGNoZWNrTGlzdGVuZXIobGlzdGVuZXIpO1xuXG4gIGV2ZW50cyA9IHRhcmdldC5fZXZlbnRzO1xuICBpZiAoZXZlbnRzID09PSB1bmRlZmluZWQpIHtcbiAgICBldmVudHMgPSB0YXJnZXQuX2V2ZW50cyA9IE9iamVjdC5jcmVhdGUobnVsbCk7XG4gICAgdGFyZ2V0Ll9ldmVudHNDb3VudCA9IDA7XG4gIH0gZWxzZSB7XG4gICAgLy8gVG8gYXZvaWQgcmVjdXJzaW9uIGluIHRoZSBjYXNlIHRoYXQgdHlwZSA9PT0gXCJuZXdMaXN0ZW5lclwiISBCZWZvcmVcbiAgICAvLyBhZGRpbmcgaXQgdG8gdGhlIGxpc3RlbmVycywgZmlyc3QgZW1pdCBcIm5ld0xpc3RlbmVyXCIuXG4gICAgaWYgKGV2ZW50cy5uZXdMaXN0ZW5lciAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICB0YXJnZXQuZW1pdCgnbmV3TGlzdGVuZXInLCB0eXBlLFxuICAgICAgICAgICAgICAgICAgbGlzdGVuZXIubGlzdGVuZXIgPyBsaXN0ZW5lci5saXN0ZW5lciA6IGxpc3RlbmVyKTtcblxuICAgICAgLy8gUmUtYXNzaWduIGBldmVudHNgIGJlY2F1c2UgYSBuZXdMaXN0ZW5lciBoYW5kbGVyIGNvdWxkIGhhdmUgY2F1c2VkIHRoZVxuICAgICAgLy8gdGhpcy5fZXZlbnRzIHRvIGJlIGFzc2lnbmVkIHRvIGEgbmV3IG9iamVjdFxuICAgICAgZXZlbnRzID0gdGFyZ2V0Ll9ldmVudHM7XG4gICAgfVxuICAgIGV4aXN0aW5nID0gZXZlbnRzW3R5cGVdO1xuICB9XG5cbiAgaWYgKGV4aXN0aW5nID09PSB1bmRlZmluZWQpIHtcbiAgICAvLyBPcHRpbWl6ZSB0aGUgY2FzZSBvZiBvbmUgbGlzdGVuZXIuIERvbid0IG5lZWQgdGhlIGV4dHJhIGFycmF5IG9iamVjdC5cbiAgICBleGlzdGluZyA9IGV2ZW50c1t0eXBlXSA9IGxpc3RlbmVyO1xuICAgICsrdGFyZ2V0Ll9ldmVudHNDb3VudDtcbiAgfSBlbHNlIHtcbiAgICBpZiAodHlwZW9mIGV4aXN0aW5nID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICAvLyBBZGRpbmcgdGhlIHNlY29uZCBlbGVtZW50LCBuZWVkIHRvIGNoYW5nZSB0byBhcnJheS5cbiAgICAgIGV4aXN0aW5nID0gZXZlbnRzW3R5cGVdID1cbiAgICAgICAgcHJlcGVuZCA/IFtsaXN0ZW5lciwgZXhpc3RpbmddIDogW2V4aXN0aW5nLCBsaXN0ZW5lcl07XG4gICAgICAvLyBJZiB3ZSd2ZSBhbHJlYWR5IGdvdCBhbiBhcnJheSwganVzdCBhcHBlbmQuXG4gICAgfSBlbHNlIGlmIChwcmVwZW5kKSB7XG4gICAgICBleGlzdGluZy51bnNoaWZ0KGxpc3RlbmVyKTtcbiAgICB9IGVsc2Uge1xuICAgICAgZXhpc3RpbmcucHVzaChsaXN0ZW5lcik7XG4gICAgfVxuXG4gICAgLy8gQ2hlY2sgZm9yIGxpc3RlbmVyIGxlYWtcbiAgICBtID0gX2dldE1heExpc3RlbmVycyh0YXJnZXQpO1xuICAgIGlmIChtID4gMCAmJiBleGlzdGluZy5sZW5ndGggPiBtICYmICFleGlzdGluZy53YXJuZWQpIHtcbiAgICAgIGV4aXN0aW5nLndhcm5lZCA9IHRydWU7XG4gICAgICAvLyBObyBlcnJvciBjb2RlIGZvciB0aGlzIHNpbmNlIGl0IGlzIGEgV2FybmluZ1xuICAgICAgLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lIG5vLXJlc3RyaWN0ZWQtc3ludGF4XG4gICAgICB2YXIgdyA9IG5ldyBFcnJvcignUG9zc2libGUgRXZlbnRFbWl0dGVyIG1lbW9yeSBsZWFrIGRldGVjdGVkLiAnICtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgZXhpc3RpbmcubGVuZ3RoICsgJyAnICsgU3RyaW5nKHR5cGUpICsgJyBsaXN0ZW5lcnMgJyArXG4gICAgICAgICAgICAgICAgICAgICAgICAgICdhZGRlZC4gVXNlIGVtaXR0ZXIuc2V0TWF4TGlzdGVuZXJzKCkgdG8gJyArXG4gICAgICAgICAgICAgICAgICAgICAgICAgICdpbmNyZWFzZSBsaW1pdCcpO1xuICAgICAgdy5uYW1lID0gJ01heExpc3RlbmVyc0V4Y2VlZGVkV2FybmluZyc7XG4gICAgICB3LmVtaXR0ZXIgPSB0YXJnZXQ7XG4gICAgICB3LnR5cGUgPSB0eXBlO1xuICAgICAgdy5jb3VudCA9IGV4aXN0aW5nLmxlbmd0aDtcbiAgICAgIFByb2Nlc3NFbWl0V2FybmluZyh3KTtcbiAgICB9XG4gIH1cblxuICByZXR1cm4gdGFyZ2V0O1xufVxuXG5FdmVudEVtaXR0ZXIucHJvdG90eXBlLmFkZExpc3RlbmVyID0gZnVuY3Rpb24gYWRkTGlzdGVuZXIodHlwZSwgbGlzdGVuZXIpIHtcbiAgcmV0dXJuIF9hZGRMaXN0ZW5lcih0aGlzLCB0eXBlLCBsaXN0ZW5lciwgZmFsc2UpO1xufTtcblxuRXZlbnRFbWl0dGVyLnByb3RvdHlwZS5vbiA9IEV2ZW50RW1pdHRlci5wcm90b3R5cGUuYWRkTGlzdGVuZXI7XG5cbkV2ZW50RW1pdHRlci5wcm90b3R5cGUucHJlcGVuZExpc3RlbmVyID1cbiAgICBmdW5jdGlvbiBwcmVwZW5kTGlzdGVuZXIodHlwZSwgbGlzdGVuZXIpIHtcbiAgICAgIHJldHVybiBfYWRkTGlzdGVuZXIodGhpcywgdHlwZSwgbGlzdGVuZXIsIHRydWUpO1xuICAgIH07XG5cbmZ1bmN0aW9uIG9uY2VXcmFwcGVyKCkge1xuICBpZiAoIXRoaXMuZmlyZWQpIHtcbiAgICB0aGlzLnRhcmdldC5yZW1vdmVMaXN0ZW5lcih0aGlzLnR5cGUsIHRoaXMud3JhcEZuKTtcbiAgICB0aGlzLmZpcmVkID0gdHJ1ZTtcbiAgICBpZiAoYXJndW1lbnRzLmxlbmd0aCA9PT0gMClcbiAgICAgIHJldHVybiB0aGlzLmxpc3RlbmVyLmNhbGwodGhpcy50YXJnZXQpO1xuICAgIHJldHVybiB0aGlzLmxpc3RlbmVyLmFwcGx5KHRoaXMudGFyZ2V0LCBhcmd1bWVudHMpO1xuICB9XG59XG5cbmZ1bmN0aW9uIF9vbmNlV3JhcCh0YXJnZXQsIHR5cGUsIGxpc3RlbmVyKSB7XG4gIHZhciBzdGF0ZSA9IHsgZmlyZWQ6IGZhbHNlLCB3cmFwRm46IHVuZGVmaW5lZCwgdGFyZ2V0OiB0YXJnZXQsIHR5cGU6IHR5cGUsIGxpc3RlbmVyOiBsaXN0ZW5lciB9O1xuICB2YXIgd3JhcHBlZCA9IG9uY2VXcmFwcGVyLmJpbmQoc3RhdGUpO1xuICB3cmFwcGVkLmxpc3RlbmVyID0gbGlzdGVuZXI7XG4gIHN0YXRlLndyYXBGbiA9IHdyYXBwZWQ7XG4gIHJldHVybiB3cmFwcGVkO1xufVxuXG5FdmVudEVtaXR0ZXIucHJvdG90eXBlLm9uY2UgPSBmdW5jdGlvbiBvbmNlKHR5cGUsIGxpc3RlbmVyKSB7XG4gIGNoZWNrTGlzdGVuZXIobGlzdGVuZXIpO1xuICB0aGlzLm9uKHR5cGUsIF9vbmNlV3JhcCh0aGlzLCB0eXBlLCBsaXN0ZW5lcikpO1xuICByZXR1cm4gdGhpcztcbn07XG5cbkV2ZW50RW1pdHRlci5wcm90b3R5cGUucHJlcGVuZE9uY2VMaXN0ZW5lciA9XG4gICAgZnVuY3Rpb24gcHJlcGVuZE9uY2VMaXN0ZW5lcih0eXBlLCBsaXN0ZW5lcikge1xuICAgICAgY2hlY2tMaXN0ZW5lcihsaXN0ZW5lcik7XG4gICAgICB0aGlzLnByZXBlbmRMaXN0ZW5lcih0eXBlLCBfb25jZVdyYXAodGhpcywgdHlwZSwgbGlzdGVuZXIpKTtcbiAgICAgIHJldHVybiB0aGlzO1xuICAgIH07XG5cbi8vIEVtaXRzIGEgJ3JlbW92ZUxpc3RlbmVyJyBldmVudCBpZiBhbmQgb25seSBpZiB0aGUgbGlzdGVuZXIgd2FzIHJlbW92ZWQuXG5FdmVudEVtaXR0ZXIucHJvdG90eXBlLnJlbW92ZUxpc3RlbmVyID1cbiAgICBmdW5jdGlvbiByZW1vdmVMaXN0ZW5lcih0eXBlLCBsaXN0ZW5lcikge1xuICAgICAgdmFyIGxpc3QsIGV2ZW50cywgcG9zaXRpb24sIGksIG9yaWdpbmFsTGlzdGVuZXI7XG5cbiAgICAgIGNoZWNrTGlzdGVuZXIobGlzdGVuZXIpO1xuXG4gICAgICBldmVudHMgPSB0aGlzLl9ldmVudHM7XG4gICAgICBpZiAoZXZlbnRzID09PSB1bmRlZmluZWQpXG4gICAgICAgIHJldHVybiB0aGlzO1xuXG4gICAgICBsaXN0ID0gZXZlbnRzW3R5cGVdO1xuICAgICAgaWYgKGxpc3QgPT09IHVuZGVmaW5lZClcbiAgICAgICAgcmV0dXJuIHRoaXM7XG5cbiAgICAgIGlmIChsaXN0ID09PSBsaXN0ZW5lciB8fCBsaXN0Lmxpc3RlbmVyID09PSBsaXN0ZW5lcikge1xuICAgICAgICBpZiAoLS10aGlzLl9ldmVudHNDb3VudCA9PT0gMClcbiAgICAgICAgICB0aGlzLl9ldmVudHMgPSBPYmplY3QuY3JlYXRlKG51bGwpO1xuICAgICAgICBlbHNlIHtcbiAgICAgICAgICBkZWxldGUgZXZlbnRzW3R5cGVdO1xuICAgICAgICAgIGlmIChldmVudHMucmVtb3ZlTGlzdGVuZXIpXG4gICAgICAgICAgICB0aGlzLmVtaXQoJ3JlbW92ZUxpc3RlbmVyJywgdHlwZSwgbGlzdC5saXN0ZW5lciB8fCBsaXN0ZW5lcik7XG4gICAgICAgIH1cbiAgICAgIH0gZWxzZSBpZiAodHlwZW9mIGxpc3QgIT09ICdmdW5jdGlvbicpIHtcbiAgICAgICAgcG9zaXRpb24gPSAtMTtcblxuICAgICAgICBmb3IgKGkgPSBsaXN0Lmxlbmd0aCAtIDE7IGkgPj0gMDsgaS0tKSB7XG4gICAgICAgICAgaWYgKGxpc3RbaV0gPT09IGxpc3RlbmVyIHx8IGxpc3RbaV0ubGlzdGVuZXIgPT09IGxpc3RlbmVyKSB7XG4gICAgICAgICAgICBvcmlnaW5hbExpc3RlbmVyID0gbGlzdFtpXS5saXN0ZW5lcjtcbiAgICAgICAgICAgIHBvc2l0aW9uID0gaTtcbiAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChwb3NpdGlvbiA8IDApXG4gICAgICAgICAgcmV0dXJuIHRoaXM7XG5cbiAgICAgICAgaWYgKHBvc2l0aW9uID09PSAwKVxuICAgICAgICAgIGxpc3Quc2hpZnQoKTtcbiAgICAgICAgZWxzZSB7XG4gICAgICAgICAgc3BsaWNlT25lKGxpc3QsIHBvc2l0aW9uKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChsaXN0Lmxlbmd0aCA9PT0gMSlcbiAgICAgICAgICBldmVudHNbdHlwZV0gPSBsaXN0WzBdO1xuXG4gICAgICAgIGlmIChldmVudHMucmVtb3ZlTGlzdGVuZXIgIT09IHVuZGVmaW5lZClcbiAgICAgICAgICB0aGlzLmVtaXQoJ3JlbW92ZUxpc3RlbmVyJywgdHlwZSwgb3JpZ2luYWxMaXN0ZW5lciB8fCBsaXN0ZW5lcik7XG4gICAgICB9XG5cbiAgICAgIHJldHVybiB0aGlzO1xuICAgIH07XG5cbkV2ZW50RW1pdHRlci5wcm90b3R5cGUub2ZmID0gRXZlbnRFbWl0dGVyLnByb3RvdHlwZS5yZW1vdmVMaXN0ZW5lcjtcblxuRXZlbnRFbWl0dGVyLnByb3RvdHlwZS5yZW1vdmVBbGxMaXN0ZW5lcnMgPVxuICAgIGZ1bmN0aW9uIHJlbW92ZUFsbExpc3RlbmVycyh0eXBlKSB7XG4gICAgICB2YXIgbGlzdGVuZXJzLCBldmVudHMsIGk7XG5cbiAgICAgIGV2ZW50cyA9IHRoaXMuX2V2ZW50cztcbiAgICAgIGlmIChldmVudHMgPT09IHVuZGVmaW5lZClcbiAgICAgICAgcmV0dXJuIHRoaXM7XG5cbiAgICAgIC8vIG5vdCBsaXN0ZW5pbmcgZm9yIHJlbW92ZUxpc3RlbmVyLCBubyBuZWVkIHRvIGVtaXRcbiAgICAgIGlmIChldmVudHMucmVtb3ZlTGlzdGVuZXIgPT09IHVuZGVmaW5lZCkge1xuICAgICAgICBpZiAoYXJndW1lbnRzLmxlbmd0aCA9PT0gMCkge1xuICAgICAgICAgIHRoaXMuX2V2ZW50cyA9IE9iamVjdC5jcmVhdGUobnVsbCk7XG4gICAgICAgICAgdGhpcy5fZXZlbnRzQ291bnQgPSAwO1xuICAgICAgICB9IGVsc2UgaWYgKGV2ZW50c1t0eXBlXSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgaWYgKC0tdGhpcy5fZXZlbnRzQ291bnQgPT09IDApXG4gICAgICAgICAgICB0aGlzLl9ldmVudHMgPSBPYmplY3QuY3JlYXRlKG51bGwpO1xuICAgICAgICAgIGVsc2VcbiAgICAgICAgICAgIGRlbGV0ZSBldmVudHNbdHlwZV07XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHRoaXM7XG4gICAgICB9XG5cbiAgICAgIC8vIGVtaXQgcmVtb3ZlTGlzdGVuZXIgZm9yIGFsbCBsaXN0ZW5lcnMgb24gYWxsIGV2ZW50c1xuICAgICAgaWYgKGFyZ3VtZW50cy5sZW5ndGggPT09IDApIHtcbiAgICAgICAgdmFyIGtleXMgPSBPYmplY3Qua2V5cyhldmVudHMpO1xuICAgICAgICB2YXIga2V5O1xuICAgICAgICBmb3IgKGkgPSAwOyBpIDwga2V5cy5sZW5ndGg7ICsraSkge1xuICAgICAgICAgIGtleSA9IGtleXNbaV07XG4gICAgICAgICAgaWYgKGtleSA9PT0gJ3JlbW92ZUxpc3RlbmVyJykgY29udGludWU7XG4gICAgICAgICAgdGhpcy5yZW1vdmVBbGxMaXN0ZW5lcnMoa2V5KTtcbiAgICAgICAgfVxuICAgICAgICB0aGlzLnJlbW92ZUFsbExpc3RlbmVycygncmVtb3ZlTGlzdGVuZXInKTtcbiAgICAgICAgdGhpcy5fZXZlbnRzID0gT2JqZWN0LmNyZWF0ZShudWxsKTtcbiAgICAgICAgdGhpcy5fZXZlbnRzQ291bnQgPSAwO1xuICAgICAgICByZXR1cm4gdGhpcztcbiAgICAgIH1cblxuICAgICAgbGlzdGVuZXJzID0gZXZlbnRzW3R5cGVdO1xuXG4gICAgICBpZiAodHlwZW9mIGxpc3RlbmVycyA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgICB0aGlzLnJlbW92ZUxpc3RlbmVyKHR5cGUsIGxpc3RlbmVycyk7XG4gICAgICB9IGVsc2UgaWYgKGxpc3RlbmVycyAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgIC8vIExJRk8gb3JkZXJcbiAgICAgICAgZm9yIChpID0gbGlzdGVuZXJzLmxlbmd0aCAtIDE7IGkgPj0gMDsgaS0tKSB7XG4gICAgICAgICAgdGhpcy5yZW1vdmVMaXN0ZW5lcih0eXBlLCBsaXN0ZW5lcnNbaV0pO1xuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIHJldHVybiB0aGlzO1xuICAgIH07XG5cbmZ1bmN0aW9uIF9saXN0ZW5lcnModGFyZ2V0LCB0eXBlLCB1bndyYXApIHtcbiAgdmFyIGV2ZW50cyA9IHRhcmdldC5fZXZlbnRzO1xuXG4gIGlmIChldmVudHMgPT09IHVuZGVmaW5lZClcbiAgICByZXR1cm4gW107XG5cbiAgdmFyIGV2bGlzdGVuZXIgPSBldmVudHNbdHlwZV07XG4gIGlmIChldmxpc3RlbmVyID09PSB1bmRlZmluZWQpXG4gICAgcmV0dXJuIFtdO1xuXG4gIGlmICh0eXBlb2YgZXZsaXN0ZW5lciA9PT0gJ2Z1bmN0aW9uJylcbiAgICByZXR1cm4gdW53cmFwID8gW2V2bGlzdGVuZXIubGlzdGVuZXIgfHwgZXZsaXN0ZW5lcl0gOiBbZXZsaXN0ZW5lcl07XG5cbiAgcmV0dXJuIHVud3JhcCA/XG4gICAgdW53cmFwTGlzdGVuZXJzKGV2bGlzdGVuZXIpIDogYXJyYXlDbG9uZShldmxpc3RlbmVyLCBldmxpc3RlbmVyLmxlbmd0aCk7XG59XG5cbkV2ZW50RW1pdHRlci5wcm90b3R5cGUubGlzdGVuZXJzID0gZnVuY3Rpb24gbGlzdGVuZXJzKHR5cGUpIHtcbiAgcmV0dXJuIF9saXN0ZW5lcnModGhpcywgdHlwZSwgdHJ1ZSk7XG59O1xuXG5FdmVudEVtaXR0ZXIucHJvdG90eXBlLnJhd0xpc3RlbmVycyA9IGZ1bmN0aW9uIHJhd0xpc3RlbmVycyh0eXBlKSB7XG4gIHJldHVybiBfbGlzdGVuZXJzKHRoaXMsIHR5cGUsIGZhbHNlKTtcbn07XG5cbkV2ZW50RW1pdHRlci5saXN0ZW5lckNvdW50ID0gZnVuY3Rpb24oZW1pdHRlciwgdHlwZSkge1xuICBpZiAodHlwZW9mIGVtaXR0ZXIubGlzdGVuZXJDb3VudCA9PT0gJ2Z1bmN0aW9uJykge1xuICAgIHJldHVybiBlbWl0dGVyLmxpc3RlbmVyQ291bnQodHlwZSk7XG4gIH0gZWxzZSB7XG4gICAgcmV0dXJuIGxpc3RlbmVyQ291bnQuY2FsbChlbWl0dGVyLCB0eXBlKTtcbiAgfVxufTtcblxuRXZlbnRFbWl0dGVyLnByb3RvdHlwZS5saXN0ZW5lckNvdW50ID0gbGlzdGVuZXJDb3VudDtcbmZ1bmN0aW9uIGxpc3RlbmVyQ291bnQodHlwZSkge1xuICB2YXIgZXZlbnRzID0gdGhpcy5fZXZlbnRzO1xuXG4gIGlmIChldmVudHMgIT09IHVuZGVmaW5lZCkge1xuICAgIHZhciBldmxpc3RlbmVyID0gZXZlbnRzW3R5cGVdO1xuXG4gICAgaWYgKHR5cGVvZiBldmxpc3RlbmVyID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICByZXR1cm4gMTtcbiAgICB9IGVsc2UgaWYgKGV2bGlzdGVuZXIgIT09IHVuZGVmaW5lZCkge1xuICAgICAgcmV0dXJuIGV2bGlzdGVuZXIubGVuZ3RoO1xuICAgIH1cbiAgfVxuXG4gIHJldHVybiAwO1xufVxuXG5FdmVudEVtaXR0ZXIucHJvdG90eXBlLmV2ZW50TmFtZXMgPSBmdW5jdGlvbiBldmVudE5hbWVzKCkge1xuICByZXR1cm4gdGhpcy5fZXZlbnRzQ291bnQgPiAwID8gUmVmbGVjdE93bktleXModGhpcy5fZXZlbnRzKSA6IFtdO1xufTtcblxuZnVuY3Rpb24gYXJyYXlDbG9uZShhcnIsIG4pIHtcbiAgdmFyIGNvcHkgPSBuZXcgQXJyYXkobik7XG4gIGZvciAodmFyIGkgPSAwOyBpIDwgbjsgKytpKVxuICAgIGNvcHlbaV0gPSBhcnJbaV07XG4gIHJldHVybiBjb3B5O1xufVxuXG5mdW5jdGlvbiBzcGxpY2VPbmUobGlzdCwgaW5kZXgpIHtcbiAgZm9yICg7IGluZGV4ICsgMSA8IGxpc3QubGVuZ3RoOyBpbmRleCsrKVxuICAgIGxpc3RbaW5kZXhdID0gbGlzdFtpbmRleCArIDFdO1xuICBsaXN0LnBvcCgpO1xufVxuXG5mdW5jdGlvbiB1bndyYXBMaXN0ZW5lcnMoYXJyKSB7XG4gIHZhciByZXQgPSBuZXcgQXJyYXkoYXJyLmxlbmd0aCk7XG4gIGZvciAodmFyIGkgPSAwOyBpIDwgcmV0Lmxlbmd0aDsgKytpKSB7XG4gICAgcmV0W2ldID0gYXJyW2ldLmxpc3RlbmVyIHx8IGFycltpXTtcbiAgfVxuICByZXR1cm4gcmV0O1xufVxuIiwiJ3VzZSBzdHJpY3QnO3ZhciBfZXh0ZW5kcz1PYmplY3QuYXNzaWdufHxmdW5jdGlvbihhKXtmb3IodmFyIGIsYz0xO2M8YXJndW1lbnRzLmxlbmd0aDtjKyspZm9yKHZhciBkIGluIGI9YXJndW1lbnRzW2NdLGIpT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKGIsZCkmJihhW2RdPWJbZF0pO3JldHVybiBhfSxfdHlwZW9mPSdmdW5jdGlvbic9PXR5cGVvZiBTeW1ib2wmJidzeW1ib2wnPT10eXBlb2YgU3ltYm9sLml0ZXJhdG9yP2Z1bmN0aW9uKGEpe3JldHVybiB0eXBlb2YgYX06ZnVuY3Rpb24oYSl7cmV0dXJuIGEmJidmdW5jdGlvbic9PXR5cGVvZiBTeW1ib2wmJmEuY29uc3RydWN0b3I9PT1TeW1ib2wmJmEhPT1TeW1ib2wucHJvdG90eXBlPydzeW1ib2wnOnR5cGVvZiBhfTtmdW5jdGlvbiBfY2xhc3NDYWxsQ2hlY2soYSxiKXtpZighKGEgaW5zdGFuY2VvZiBiKSl0aHJvdyBuZXcgVHlwZUVycm9yKCdDYW5ub3QgY2FsbCBhIGNsYXNzIGFzIGEgZnVuY3Rpb24nKX12YXIgUm91dGluZz1mdW5jdGlvbiBhKCl7dmFyIGI9dGhpcztfY2xhc3NDYWxsQ2hlY2sodGhpcyxhKSx0aGlzLnNldFJvdXRlcz1mdW5jdGlvbihhKXtiLnJvdXRlc1JvdXRpbmc9YXx8W119LHRoaXMuZ2V0Um91dGVzPWZ1bmN0aW9uKCl7cmV0dXJuIGIucm91dGVzUm91dGluZ30sdGhpcy5zZXRCYXNlVXJsPWZ1bmN0aW9uKGEpe2IuY29udGV4dFJvdXRpbmcuYmFzZV91cmw9YX0sdGhpcy5nZXRCYXNlVXJsPWZ1bmN0aW9uKCl7cmV0dXJuIGIuY29udGV4dFJvdXRpbmcuYmFzZV91cmx9LHRoaXMuc2V0UHJlZml4PWZ1bmN0aW9uKGEpe2IuY29udGV4dFJvdXRpbmcucHJlZml4PWF9LHRoaXMuc2V0U2NoZW1lPWZ1bmN0aW9uKGEpe2IuY29udGV4dFJvdXRpbmcuc2NoZW1lPWF9LHRoaXMuZ2V0U2NoZW1lPWZ1bmN0aW9uKCl7cmV0dXJuIGIuY29udGV4dFJvdXRpbmcuc2NoZW1lfSx0aGlzLnNldEhvc3Q9ZnVuY3Rpb24oYSl7Yi5jb250ZXh0Um91dGluZy5ob3N0PWF9LHRoaXMuZ2V0SG9zdD1mdW5jdGlvbigpe3JldHVybiBiLmNvbnRleHRSb3V0aW5nLmhvc3R9LHRoaXMuYnVpbGRRdWVyeVBhcmFtcz1mdW5jdGlvbihhLGMsZCl7dmFyIGU9bmV3IFJlZ0V4cCgvXFxbXSQvKTtjIGluc3RhbmNlb2YgQXJyYXk/Yy5mb3JFYWNoKGZ1bmN0aW9uKGMsZil7ZS50ZXN0KGEpP2QoYSxjKTpiLmJ1aWxkUXVlcnlQYXJhbXMoYSsnWycrKCdvYmplY3QnPT09KCd1bmRlZmluZWQnPT10eXBlb2YgYz8ndW5kZWZpbmVkJzpfdHlwZW9mKGMpKT9mOicnKSsnXScsYyxkKX0pOidvYmplY3QnPT09KCd1bmRlZmluZWQnPT10eXBlb2YgYz8ndW5kZWZpbmVkJzpfdHlwZW9mKGMpKT9PYmplY3Qua2V5cyhjKS5mb3JFYWNoKGZ1bmN0aW9uKGUpe3JldHVybiBiLmJ1aWxkUXVlcnlQYXJhbXMoYSsnWycrZSsnXScsY1tlXSxkKX0pOmQoYSxjKX0sdGhpcy5nZXRSb3V0ZT1mdW5jdGlvbihhKXt2YXIgYz1iLmNvbnRleHRSb3V0aW5nLnByZWZpeCthO2lmKCEhYi5yb3V0ZXNSb3V0aW5nW2NdKXJldHVybiBiLnJvdXRlc1JvdXRpbmdbY107ZWxzZSBpZighYi5yb3V0ZXNSb3V0aW5nW2FdKXRocm93IG5ldyBFcnJvcignVGhlIHJvdXRlIFwiJythKydcIiBkb2VzIG5vdCBleGlzdC4nKTtyZXR1cm4gYi5yb3V0ZXNSb3V0aW5nW2FdfSx0aGlzLmdlbmVyYXRlPWZ1bmN0aW9uKGEsYyxkKXt2YXIgZT1iLmdldFJvdXRlKGEpLGY9Y3x8e30sZz1fZXh0ZW5kcyh7fSxmKSxoPSdfc2NoZW1lJyxpPScnLGo9ITAsaz0nJztpZigoZS50b2tlbnN8fFtdKS5mb3JFYWNoKGZ1bmN0aW9uKGIpe2lmKCd0ZXh0Jz09PWJbMF0pcmV0dXJuIGk9YlsxXStpLHZvaWQoaj0hMSk7aWYoJ3ZhcmlhYmxlJz09PWJbMF0pe3ZhciBjPShlLmRlZmF1bHRzfHx7fSlbYlszXV07aWYoITE9PWp8fCFjfHwoZnx8e30pW2JbM11dJiZmW2JbM11dIT09ZS5kZWZhdWx0c1tiWzNdXSl7dmFyIGQ7aWYoKGZ8fHt9KVtiWzNdXSlkPWZbYlszXV0sZGVsZXRlIGdbYlszXV07ZWxzZSBpZihjKWQ9ZS5kZWZhdWx0c1tiWzNdXTtlbHNle2lmKGopcmV0dXJuO3Rocm93IG5ldyBFcnJvcignVGhlIHJvdXRlIFwiJythKydcIiByZXF1aXJlcyB0aGUgcGFyYW1ldGVyIFwiJytiWzNdKydcIi4nKX12YXIgaD0hMD09PWR8fCExPT09ZHx8Jyc9PT1kO2lmKCFofHwhail7dmFyIGs9ZW5jb2RlVVJJQ29tcG9uZW50KGQpLnJlcGxhY2UoLyUyRi9nLCcvJyk7J251bGwnPT09ayYmbnVsbD09PWQmJihrPScnKSxpPWJbMV0raytpfWo9ITF9ZWxzZSBjJiZkZWxldGUgZ1tiWzNdXTtyZXR1cm59dGhyb3cgbmV3IEVycm9yKCdUaGUgdG9rZW4gdHlwZSBcIicrYlswXSsnXCIgaXMgbm90IHN1cHBvcnRlZC4nKX0pLCcnPT1pJiYoaT0nLycpLChlLmhvc3R0b2tlbnN8fFtdKS5mb3JFYWNoKGZ1bmN0aW9uKGEpe3ZhciBiO3JldHVybid0ZXh0Jz09PWFbMF0/dm9pZChrPWFbMV0rayk6dm9pZCgndmFyaWFibGUnPT09YVswXSYmKChmfHx7fSlbYVszXV0/KGI9ZlthWzNdXSxkZWxldGUgZ1thWzNdXSk6ZS5kZWZhdWx0c1thWzNdXSYmKGI9ZS5kZWZhdWx0c1thWzNdXSksaz1hWzFdK2IraykpfSksaT1iLmNvbnRleHRSb3V0aW5nLmJhc2VfdXJsK2ksZS5yZXF1aXJlbWVudHNbaF0mJmIuZ2V0U2NoZW1lKCkhPT1lLnJlcXVpcmVtZW50c1toXT9pPWUucmVxdWlyZW1lbnRzW2hdKyc6Ly8nKyhrfHxiLmdldEhvc3QoKSkraTprJiZiLmdldEhvc3QoKSE9PWs/aT1iLmdldFNjaGVtZSgpKyc6Ly8nK2sraTohMD09PWQmJihpPWIuZ2V0U2NoZW1lKCkrJzovLycrYi5nZXRIb3N0KCkraSksMDxPYmplY3Qua2V5cyhnKS5sZW5ndGgpe3ZhciBsPVtdLG09ZnVuY3Rpb24oYSxiKXt2YXIgYz1iO2M9J2Z1bmN0aW9uJz09dHlwZW9mIGM/YygpOmMsYz1udWxsPT09Yz8nJzpjLGwucHVzaChlbmNvZGVVUklDb21wb25lbnQoYSkrJz0nK2VuY29kZVVSSUNvbXBvbmVudChjKSl9O09iamVjdC5rZXlzKGcpLmZvckVhY2goZnVuY3Rpb24oYSl7cmV0dXJuIGIuYnVpbGRRdWVyeVBhcmFtcyhhLGdbYV0sbSl9KSxpPWkrJz8nK2wuam9pbignJicpLnJlcGxhY2UoLyUyMC9nLCcrJyl9cmV0dXJuIGl9LHRoaXMuc2V0RGF0YT1mdW5jdGlvbihhKXtiLnNldEJhc2VVcmwoYS5iYXNlX3VybCksYi5zZXRSb3V0ZXMoYS5yb3V0ZXMpLCdwcmVmaXgnaW4gYSYmYi5zZXRQcmVmaXgoYS5wcmVmaXgpLGIuc2V0SG9zdChhLmhvc3QpLGIuc2V0U2NoZW1lKGEuc2NoZW1lKX0sdGhpcy5jb250ZXh0Um91dGluZz17YmFzZV91cmw6JycscHJlZml4OicnLGhvc3Q6Jycsc2NoZW1lOicnfX07bW9kdWxlLmV4cG9ydHM9bmV3IFJvdXRpbmc7IiwiLyoqXG4gKiBsb2Rhc2ggKEN1c3RvbSBCdWlsZCkgPGh0dHBzOi8vbG9kYXNoLmNvbS8+XG4gKiBCdWlsZDogYGxvZGFzaCBtb2R1bGFyaXplIGV4cG9ydHM9XCJucG1cIiAtbyAuL2BcbiAqIENvcHlyaWdodCBqUXVlcnkgRm91bmRhdGlvbiBhbmQgb3RoZXIgY29udHJpYnV0b3JzIDxodHRwczovL2pxdWVyeS5vcmcvPlxuICogUmVsZWFzZWQgdW5kZXIgTUlUIGxpY2Vuc2UgPGh0dHBzOi8vbG9kYXNoLmNvbS9saWNlbnNlPlxuICogQmFzZWQgb24gVW5kZXJzY29yZS5qcyAxLjguMyA8aHR0cDovL3VuZGVyc2NvcmVqcy5vcmcvTElDRU5TRT5cbiAqIENvcHlyaWdodCBKZXJlbXkgQXNoa2VuYXMsIERvY3VtZW50Q2xvdWQgYW5kIEludmVzdGlnYXRpdmUgUmVwb3J0ZXJzICYgRWRpdG9yc1xuICovXG5cbi8qKiBVc2VkIGFzIHJlZmVyZW5jZXMgZm9yIHZhcmlvdXMgYE51bWJlcmAgY29uc3RhbnRzLiAqL1xudmFyIElORklOSVRZID0gMSAvIDA7XG5cbi8qKiBgT2JqZWN0I3RvU3RyaW5nYCByZXN1bHQgcmVmZXJlbmNlcy4gKi9cbnZhciBzeW1ib2xUYWcgPSAnW29iamVjdCBTeW1ib2xdJztcblxuLyoqXG4gKiBVc2VkIHRvIG1hdGNoIGBSZWdFeHBgXG4gKiBbc3ludGF4IGNoYXJhY3RlcnNdKGh0dHA6Ly9lY21hLWludGVybmF0aW9uYWwub3JnL2VjbWEtMjYyLzYuMC8jc2VjLXBhdHRlcm5zKS5cbiAqL1xudmFyIHJlUmVnRXhwQ2hhciA9IC9bXFxcXF4kLiorPygpW1xcXXt9fF0vZyxcbiAgICByZUhhc1JlZ0V4cENoYXIgPSBSZWdFeHAocmVSZWdFeHBDaGFyLnNvdXJjZSk7XG5cbi8qKiBEZXRlY3QgZnJlZSB2YXJpYWJsZSBgZ2xvYmFsYCBmcm9tIE5vZGUuanMuICovXG52YXIgZnJlZUdsb2JhbCA9IHR5cGVvZiBnbG9iYWwgPT0gJ29iamVjdCcgJiYgZ2xvYmFsICYmIGdsb2JhbC5PYmplY3QgPT09IE9iamVjdCAmJiBnbG9iYWw7XG5cbi8qKiBEZXRlY3QgZnJlZSB2YXJpYWJsZSBgc2VsZmAuICovXG52YXIgZnJlZVNlbGYgPSB0eXBlb2Ygc2VsZiA9PSAnb2JqZWN0JyAmJiBzZWxmICYmIHNlbGYuT2JqZWN0ID09PSBPYmplY3QgJiYgc2VsZjtcblxuLyoqIFVzZWQgYXMgYSByZWZlcmVuY2UgdG8gdGhlIGdsb2JhbCBvYmplY3QuICovXG52YXIgcm9vdCA9IGZyZWVHbG9iYWwgfHwgZnJlZVNlbGYgfHwgRnVuY3Rpb24oJ3JldHVybiB0aGlzJykoKTtcblxuLyoqIFVzZWQgZm9yIGJ1aWx0LWluIG1ldGhvZCByZWZlcmVuY2VzLiAqL1xudmFyIG9iamVjdFByb3RvID0gT2JqZWN0LnByb3RvdHlwZTtcblxuLyoqXG4gKiBVc2VkIHRvIHJlc29sdmUgdGhlXG4gKiBbYHRvU3RyaW5nVGFnYF0oaHR0cDovL2VjbWEtaW50ZXJuYXRpb25hbC5vcmcvZWNtYS0yNjIvNi4wLyNzZWMtb2JqZWN0LnByb3RvdHlwZS50b3N0cmluZylcbiAqIG9mIHZhbHVlcy5cbiAqL1xudmFyIG9iamVjdFRvU3RyaW5nID0gb2JqZWN0UHJvdG8udG9TdHJpbmc7XG5cbi8qKiBCdWlsdC1pbiB2YWx1ZSByZWZlcmVuY2VzLiAqL1xudmFyIFN5bWJvbCA9IHJvb3QuU3ltYm9sO1xuXG4vKiogVXNlZCB0byBjb252ZXJ0IHN5bWJvbHMgdG8gcHJpbWl0aXZlcyBhbmQgc3RyaW5ncy4gKi9cbnZhciBzeW1ib2xQcm90byA9IFN5bWJvbCA/IFN5bWJvbC5wcm90b3R5cGUgOiB1bmRlZmluZWQsXG4gICAgc3ltYm9sVG9TdHJpbmcgPSBzeW1ib2xQcm90byA/IHN5bWJvbFByb3RvLnRvU3RyaW5nIDogdW5kZWZpbmVkO1xuXG4vKipcbiAqIFRoZSBiYXNlIGltcGxlbWVudGF0aW9uIG9mIGBfLnRvU3RyaW5nYCB3aGljaCBkb2Vzbid0IGNvbnZlcnQgbnVsbGlzaFxuICogdmFsdWVzIHRvIGVtcHR5IHN0cmluZ3MuXG4gKlxuICogQHByaXZhdGVcbiAqIEBwYXJhbSB7Kn0gdmFsdWUgVGhlIHZhbHVlIHRvIHByb2Nlc3MuXG4gKiBAcmV0dXJucyB7c3RyaW5nfSBSZXR1cm5zIHRoZSBzdHJpbmcuXG4gKi9cbmZ1bmN0aW9uIGJhc2VUb1N0cmluZyh2YWx1ZSkge1xuICAvLyBFeGl0IGVhcmx5IGZvciBzdHJpbmdzIHRvIGF2b2lkIGEgcGVyZm9ybWFuY2UgaGl0IGluIHNvbWUgZW52aXJvbm1lbnRzLlxuICBpZiAodHlwZW9mIHZhbHVlID09ICdzdHJpbmcnKSB7XG4gICAgcmV0dXJuIHZhbHVlO1xuICB9XG4gIGlmIChpc1N5bWJvbCh2YWx1ZSkpIHtcbiAgICByZXR1cm4gc3ltYm9sVG9TdHJpbmcgPyBzeW1ib2xUb1N0cmluZy5jYWxsKHZhbHVlKSA6ICcnO1xuICB9XG4gIHZhciByZXN1bHQgPSAodmFsdWUgKyAnJyk7XG4gIHJldHVybiAocmVzdWx0ID09ICcwJyAmJiAoMSAvIHZhbHVlKSA9PSAtSU5GSU5JVFkpID8gJy0wJyA6IHJlc3VsdDtcbn1cblxuLyoqXG4gKiBDaGVja3MgaWYgYHZhbHVlYCBpcyBvYmplY3QtbGlrZS4gQSB2YWx1ZSBpcyBvYmplY3QtbGlrZSBpZiBpdCdzIG5vdCBgbnVsbGBcbiAqIGFuZCBoYXMgYSBgdHlwZW9mYCByZXN1bHQgb2YgXCJvYmplY3RcIi5cbiAqXG4gKiBAc3RhdGljXG4gKiBAbWVtYmVyT2YgX1xuICogQHNpbmNlIDQuMC4wXG4gKiBAY2F0ZWdvcnkgTGFuZ1xuICogQHBhcmFtIHsqfSB2YWx1ZSBUaGUgdmFsdWUgdG8gY2hlY2suXG4gKiBAcmV0dXJucyB7Ym9vbGVhbn0gUmV0dXJucyBgdHJ1ZWAgaWYgYHZhbHVlYCBpcyBvYmplY3QtbGlrZSwgZWxzZSBgZmFsc2VgLlxuICogQGV4YW1wbGVcbiAqXG4gKiBfLmlzT2JqZWN0TGlrZSh7fSk7XG4gKiAvLyA9PiB0cnVlXG4gKlxuICogXy5pc09iamVjdExpa2UoWzEsIDIsIDNdKTtcbiAqIC8vID0+IHRydWVcbiAqXG4gKiBfLmlzT2JqZWN0TGlrZShfLm5vb3ApO1xuICogLy8gPT4gZmFsc2VcbiAqXG4gKiBfLmlzT2JqZWN0TGlrZShudWxsKTtcbiAqIC8vID0+IGZhbHNlXG4gKi9cbmZ1bmN0aW9uIGlzT2JqZWN0TGlrZSh2YWx1ZSkge1xuICByZXR1cm4gISF2YWx1ZSAmJiB0eXBlb2YgdmFsdWUgPT0gJ29iamVjdCc7XG59XG5cbi8qKlxuICogQ2hlY2tzIGlmIGB2YWx1ZWAgaXMgY2xhc3NpZmllZCBhcyBhIGBTeW1ib2xgIHByaW1pdGl2ZSBvciBvYmplY3QuXG4gKlxuICogQHN0YXRpY1xuICogQG1lbWJlck9mIF9cbiAqIEBzaW5jZSA0LjAuMFxuICogQGNhdGVnb3J5IExhbmdcbiAqIEBwYXJhbSB7Kn0gdmFsdWUgVGhlIHZhbHVlIHRvIGNoZWNrLlxuICogQHJldHVybnMge2Jvb2xlYW59IFJldHVybnMgYHRydWVgIGlmIGB2YWx1ZWAgaXMgYSBzeW1ib2wsIGVsc2UgYGZhbHNlYC5cbiAqIEBleGFtcGxlXG4gKlxuICogXy5pc1N5bWJvbChTeW1ib2wuaXRlcmF0b3IpO1xuICogLy8gPT4gdHJ1ZVxuICpcbiAqIF8uaXNTeW1ib2woJ2FiYycpO1xuICogLy8gPT4gZmFsc2VcbiAqL1xuZnVuY3Rpb24gaXNTeW1ib2wodmFsdWUpIHtcbiAgcmV0dXJuIHR5cGVvZiB2YWx1ZSA9PSAnc3ltYm9sJyB8fFxuICAgIChpc09iamVjdExpa2UodmFsdWUpICYmIG9iamVjdFRvU3RyaW5nLmNhbGwodmFsdWUpID09IHN5bWJvbFRhZyk7XG59XG5cbi8qKlxuICogQ29udmVydHMgYHZhbHVlYCB0byBhIHN0cmluZy4gQW4gZW1wdHkgc3RyaW5nIGlzIHJldHVybmVkIGZvciBgbnVsbGBcbiAqIGFuZCBgdW5kZWZpbmVkYCB2YWx1ZXMuIFRoZSBzaWduIG9mIGAtMGAgaXMgcHJlc2VydmVkLlxuICpcbiAqIEBzdGF0aWNcbiAqIEBtZW1iZXJPZiBfXG4gKiBAc2luY2UgNC4wLjBcbiAqIEBjYXRlZ29yeSBMYW5nXG4gKiBAcGFyYW0geyp9IHZhbHVlIFRoZSB2YWx1ZSB0byBwcm9jZXNzLlxuICogQHJldHVybnMge3N0cmluZ30gUmV0dXJucyB0aGUgc3RyaW5nLlxuICogQGV4YW1wbGVcbiAqXG4gKiBfLnRvU3RyaW5nKG51bGwpO1xuICogLy8gPT4gJydcbiAqXG4gKiBfLnRvU3RyaW5nKC0wKTtcbiAqIC8vID0+ICctMCdcbiAqXG4gKiBfLnRvU3RyaW5nKFsxLCAyLCAzXSk7XG4gKiAvLyA9PiAnMSwyLDMnXG4gKi9cbmZ1bmN0aW9uIHRvU3RyaW5nKHZhbHVlKSB7XG4gIHJldHVybiB2YWx1ZSA9PSBudWxsID8gJycgOiBiYXNlVG9TdHJpbmcodmFsdWUpO1xufVxuXG4vKipcbiAqIEVzY2FwZXMgdGhlIGBSZWdFeHBgIHNwZWNpYWwgY2hhcmFjdGVycyBcIl5cIiwgXCIkXCIsIFwiXFxcIiwgXCIuXCIsIFwiKlwiLCBcIitcIixcbiAqIFwiP1wiLCBcIihcIiwgXCIpXCIsIFwiW1wiLCBcIl1cIiwgXCJ7XCIsIFwifVwiLCBhbmQgXCJ8XCIgaW4gYHN0cmluZ2AuXG4gKlxuICogQHN0YXRpY1xuICogQG1lbWJlck9mIF9cbiAqIEBzaW5jZSAzLjAuMFxuICogQGNhdGVnb3J5IFN0cmluZ1xuICogQHBhcmFtIHtzdHJpbmd9IFtzdHJpbmc9JyddIFRoZSBzdHJpbmcgdG8gZXNjYXBlLlxuICogQHJldHVybnMge3N0cmluZ30gUmV0dXJucyB0aGUgZXNjYXBlZCBzdHJpbmcuXG4gKiBAZXhhbXBsZVxuICpcbiAqIF8uZXNjYXBlUmVnRXhwKCdbbG9kYXNoXShodHRwczovL2xvZGFzaC5jb20vKScpO1xuICogLy8gPT4gJ1xcW2xvZGFzaFxcXVxcKGh0dHBzOi8vbG9kYXNoXFwuY29tL1xcKSdcbiAqL1xuZnVuY3Rpb24gZXNjYXBlUmVnRXhwKHN0cmluZykge1xuICBzdHJpbmcgPSB0b1N0cmluZyhzdHJpbmcpO1xuICByZXR1cm4gKHN0cmluZyAmJiByZUhhc1JlZ0V4cENoYXIudGVzdChzdHJpbmcpKVxuICAgID8gc3RyaW5nLnJlcGxhY2UocmVSZWdFeHBDaGFyLCAnXFxcXCQmJylcbiAgICA6IHN0cmluZztcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBlc2NhcGVSZWdFeHA7XG4iLCIvLyBUaGUgbW9kdWxlIGNhY2hlXG52YXIgX193ZWJwYWNrX21vZHVsZV9jYWNoZV9fID0ge307XG5cbi8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG5mdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuXHR2YXIgY2FjaGVkTW9kdWxlID0gX193ZWJwYWNrX21vZHVsZV9jYWNoZV9fW21vZHVsZUlkXTtcblx0aWYgKGNhY2hlZE1vZHVsZSAhPT0gdW5kZWZpbmVkKSB7XG5cdFx0cmV0dXJuIGNhY2hlZE1vZHVsZS5leHBvcnRzO1xuXHR9XG5cdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG5cdHZhciBtb2R1bGUgPSBfX3dlYnBhY2tfbW9kdWxlX2NhY2hlX19bbW9kdWxlSWRdID0ge1xuXHRcdC8vIG5vIG1vZHVsZS5pZCBuZWVkZWRcblx0XHQvLyBubyBtb2R1bGUubG9hZGVkIG5lZWRlZFxuXHRcdGV4cG9ydHM6IHt9XG5cdH07XG5cblx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG5cdF9fd2VicGFja19tb2R1bGVzX19bbW9kdWxlSWRdKG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG5cdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG5cdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbn1cblxuIiwiX193ZWJwYWNrX3JlcXVpcmVfXy5nID0gKGZ1bmN0aW9uKCkge1xuXHRpZiAodHlwZW9mIGdsb2JhbFRoaXMgPT09ICdvYmplY3QnKSByZXR1cm4gZ2xvYmFsVGhpcztcblx0dHJ5IHtcblx0XHRyZXR1cm4gdGhpcyB8fCBuZXcgRnVuY3Rpb24oJ3JldHVybiB0aGlzJykoKTtcblx0fSBjYXRjaCAoZSkge1xuXHRcdGlmICh0eXBlb2Ygd2luZG93ID09PSAnb2JqZWN0JykgcmV0dXJuIHdpbmRvdztcblx0fVxufSkoKTsiLCIvKipcbiAqIENvcHlyaWdodCBzaW5jZSAyMDA3IFByZXN0YVNob3AgU0EgYW5kIENvbnRyaWJ1dG9yc1xuICogUHJlc3RhU2hvcCBpcyBhbiBJbnRlcm5hdGlvbmFsIFJlZ2lzdGVyZWQgVHJhZGVtYXJrICYgUHJvcGVydHkgb2YgUHJlc3RhU2hvcCBTQVxuICpcbiAqIE5PVElDRSBPRiBMSUNFTlNFXG4gKlxuICogVGhpcyBzb3VyY2UgZmlsZSBpcyBzdWJqZWN0IHRvIHRoZSBPcGVuIFNvZnR3YXJlIExpY2Vuc2UgKE9TTCAzLjApXG4gKiB0aGF0IGlzIGJ1bmRsZWQgd2l0aCB0aGlzIHBhY2thZ2UgaW4gdGhlIGZpbGUgTElDRU5TRS5tZC5cbiAqIEl0IGlzIGFsc28gYXZhaWxhYmxlIHRocm91Z2ggdGhlIHdvcmxkLXdpZGUtd2ViIGF0IHRoaXMgVVJMOlxuICogaHR0cHM6Ly9vcGVuc291cmNlLm9yZy9saWNlbnNlcy9PU0wtMy4wXG4gKiBJZiB5b3UgZGlkIG5vdCByZWNlaXZlIGEgY29weSBvZiB0aGUgbGljZW5zZSBhbmQgYXJlIHVuYWJsZSB0b1xuICogb2J0YWluIGl0IHRocm91Z2ggdGhlIHdvcmxkLXdpZGUtd2ViLCBwbGVhc2Ugc2VuZCBhbiBlbWFpbFxuICogdG8gbGljZW5zZUBwcmVzdGFzaG9wLmNvbSBzbyB3ZSBjYW4gc2VuZCB5b3UgYSBjb3B5IGltbWVkaWF0ZWx5LlxuICpcbiAqIERJU0NMQUlNRVJcbiAqXG4gKiBEbyBub3QgZWRpdCBvciBhZGQgdG8gdGhpcyBmaWxlIGlmIHlvdSB3aXNoIHRvIHVwZ3JhZGUgUHJlc3RhU2hvcCB0byBuZXdlclxuICogdmVyc2lvbnMgaW4gdGhlIGZ1dHVyZS4gSWYgeW91IHdpc2ggdG8gY3VzdG9taXplIFByZXN0YVNob3AgZm9yIHlvdXJcbiAqIG5lZWRzIHBsZWFzZSByZWZlciB0byBodHRwczovL2RldmRvY3MucHJlc3Rhc2hvcC5jb20vIGZvciBtb3JlIGluZm9ybWF0aW9uLlxuICpcbiAqIEBhdXRob3IgICAgUHJlc3RhU2hvcCBTQSBhbmQgQ29udHJpYnV0b3JzIDxjb250YWN0QHByZXN0YXNob3AuY29tPlxuICogQGNvcHlyaWdodCBTaW5jZSAyMDA3IFByZXN0YVNob3AgU0EgYW5kIENvbnRyaWJ1dG9yc1xuICogQGxpY2Vuc2UgICBodHRwczovL29wZW5zb3VyY2Uub3JnL2xpY2Vuc2VzL09TTC0zLjAgT3BlbiBTb2Z0d2FyZSBMaWNlbnNlIChPU0wgMy4wKVxuICovXG5cbmltcG9ydCBPcmRlclZpZXdQYWdlTWFwIGZyb20gJ0BwYWdlcy9vcmRlci9PcmRlclZpZXdQYWdlTWFwJztcbmltcG9ydCBPcmRlclNoaXBwaW5nTWFuYWdlciBmcm9tICdAcGFnZXMvb3JkZXIvb3JkZXItc2hpcHBpbmctbWFuYWdlcic7XG5pbXBvcnQgSW52b2ljZU5vdGVNYW5hZ2VyIGZyb20gJ0BwYWdlcy9vcmRlci9pbnZvaWNlLW5vdGUtbWFuYWdlcic7XG5pbXBvcnQgT3JkZXJWaWV3UGFnZSBmcm9tICdAcGFnZXMvb3JkZXIvdmlldy9vcmRlci12aWV3LXBhZ2UnO1xuaW1wb3J0IE9yZGVyUHJvZHVjdEF1dG9jb21wbGV0ZSBmcm9tICdAcGFnZXMvb3JkZXIvdmlldy9vcmRlci1wcm9kdWN0LWFkZC1hdXRvY29tcGxldGUnO1xuaW1wb3J0IE9yZGVyUHJvZHVjdEFkZCBmcm9tICdAcGFnZXMvb3JkZXIvdmlldy9vcmRlci1wcm9kdWN0LWFkZCc7XG5pbXBvcnQgVGV4dFdpdGhMZW5ndGhDb3VudGVyIGZyb20gJ0Bjb21wb25lbnRzL2Zvcm0vdGV4dC13aXRoLWxlbmd0aC1jb3VudGVyJztcbmltcG9ydCBPcmRlclZpZXdQYWdlTWVzc2FnZXNIYW5kbGVyIGZyb20gJy4vbWVzc2FnZS9vcmRlci12aWV3LXBhZ2UtbWVzc2FnZXMtaGFuZGxlcic7XG5cbmNvbnN0IHskfSA9IHdpbmRvdztcblxuJCgoKSA9PiB7XG4gIGNvbnN0IERJU0NPVU5UX1RZUEVfQU1PVU5UID0gJ2Ftb3VudCc7XG4gIGNvbnN0IERJU0NPVU5UX1RZUEVfUEVSQ0VOVCA9ICdwZXJjZW50JztcbiAgY29uc3QgRElTQ09VTlRfVFlQRV9GUkVFX1NISVBQSU5HID0gJ2ZyZWVfc2hpcHBpbmcnO1xuXG4gIG5ldyBPcmRlclNoaXBwaW5nTWFuYWdlcigpO1xuICBuZXcgVGV4dFdpdGhMZW5ndGhDb3VudGVyKCk7XG4gIGNvbnN0IG9yZGVyVmlld1BhZ2UgPSBuZXcgT3JkZXJWaWV3UGFnZSgpO1xuICBjb25zdCBvcmRlckFkZEF1dG9jb21wbGV0ZSA9IG5ldyBPcmRlclByb2R1Y3RBdXRvY29tcGxldGUoJChPcmRlclZpZXdQYWdlTWFwLnByb2R1Y3RTZWFyY2hJbnB1dCkpO1xuICBjb25zdCBvcmRlckFkZCA9IG5ldyBPcmRlclByb2R1Y3RBZGQoKTtcblxuICBvcmRlclZpZXdQYWdlLmxpc3RlbkZvclByb2R1Y3RQYWNrKCk7XG4gIG9yZGVyVmlld1BhZ2UubGlzdGVuRm9yUHJvZHVjdERlbGV0ZSgpO1xuICBvcmRlclZpZXdQYWdlLmxpc3RlbkZvclByb2R1Y3RFZGl0KCk7XG4gIG9yZGVyVmlld1BhZ2UubGlzdGVuRm9yUHJvZHVjdEFkZCgpO1xuICBvcmRlclZpZXdQYWdlLmxpc3RlbkZvclByb2R1Y3RQYWdpbmF0aW9uKCk7XG4gIG9yZGVyVmlld1BhZ2UubGlzdGVuRm9yUmVmdW5kKCk7XG4gIG9yZGVyVmlld1BhZ2UubGlzdGVuRm9yQ2FuY2VsUHJvZHVjdCgpO1xuXG4gIG9yZGVyQWRkQXV0b2NvbXBsZXRlLmxpc3RlbkZvclNlYXJjaCgpO1xuICBvcmRlckFkZEF1dG9jb21wbGV0ZS5vbkl0ZW1DbGlja2VkQ2FsbGJhY2sgPSAocHJvZHVjdCkgPT4gb3JkZXJBZGQuc2V0UHJvZHVjdChwcm9kdWN0KTtcblxuICBoYW5kbGVQYXltZW50RGV0YWlsc1RvZ2dsZSgpO1xuICBoYW5kbGVQcml2YXRlTm90ZUNoYW5nZSgpO1xuICBoYW5kbGVPcmRlck5vdGVDaGFuZ2UoKTtcbiAgaGFuZGxlVXBkYXRlT3JkZXJTdGF0dXNCdXR0b24oKTtcblxuICBuZXcgSW52b2ljZU5vdGVNYW5hZ2VyKCk7XG4gIGNvbnN0IG9yZGVyVmlld1BhZ2VNZXNzYWdlSGFuZGxlciA9IG5ldyBPcmRlclZpZXdQYWdlTWVzc2FnZXNIYW5kbGVyKCk7XG4gIG9yZGVyVmlld1BhZ2VNZXNzYWdlSGFuZGxlci5saXN0ZW5Gb3JQcmVkZWZpbmVkTWVzc2FnZVNlbGVjdGlvbigpO1xuICBvcmRlclZpZXdQYWdlTWVzc2FnZUhhbmRsZXIubGlzdGVuRm9yRnVsbE1lc3NhZ2VzT3BlbigpO1xuICAkKE9yZGVyVmlld1BhZ2VNYXAucHJpdmF0ZU5vdGVUb2dnbGVCdG4pLm9uKCdjbGljaycsIChldmVudCkgPT4ge1xuICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgdG9nZ2xlUHJpdmF0ZU5vdGVCbG9jaygpO1xuICB9KTtcblxuICAkKE9yZGVyVmlld1BhZ2VNYXAub3JkZXJOb3RlVG9nZ2xlQnRuKS5vbignY2xpY2snLCAoZXZlbnQpID0+IHtcbiAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgIHRvZ2dsZU9yZGVyTm90ZUJsb2NrKCk7XG4gIH0pO1xuXG4gICQoT3JkZXJWaWV3UGFnZU1hcC5wcmludE9yZGVyVmlld1BhZ2VCdXR0b24pLm9uKCdjbGljaycsICgpID0+IHtcbiAgICBjb25zdCB0ZW1wVGl0bGUgPSBkb2N1bWVudC50aXRsZTtcbiAgICBkb2N1bWVudC50aXRsZSA9ICQoT3JkZXJWaWV3UGFnZU1hcC5tYWluRGl2KS5kYXRhKCdvcmRlclRpdGxlJyk7XG4gICAgd2luZG93LnByaW50KCk7XG4gICAgZG9jdW1lbnQudGl0bGUgPSB0ZW1wVGl0bGU7XG4gIH0pO1xuXG4gIGluaXRBZGRDYXJ0UnVsZUZvcm1IYW5kbGVyKCk7XG4gIGluaXRDaGFuZ2VBZGRyZXNzRm9ybUhhbmRsZXIoKTtcbiAgaW5pdEhvb2tUYWJzKCk7XG5cbiAgZnVuY3Rpb24gaW5pdEhvb2tUYWJzKCkge1xuICAgICQoT3JkZXJWaWV3UGFnZU1hcC5vcmRlckhvb2tUYWJzQ29udGFpbmVyKVxuICAgICAgLmZpbmQoJy5uYXYtdGFicyBsaTpmaXJzdC1jaGlsZCBhJylcbiAgICAgIC50YWIoJ3Nob3cnKTtcbiAgfVxuXG4gIGZ1bmN0aW9uIGhhbmRsZVBheW1lbnREZXRhaWxzVG9nZ2xlKCkge1xuICAgICQoT3JkZXJWaWV3UGFnZU1hcC5vcmRlclBheW1lbnREZXRhaWxzQnRuKS5vbignY2xpY2snLCAoZXZlbnQpID0+IHtcbiAgICAgIGNvbnN0ICRwYXltZW50RGV0YWlsUm93ID0gJChldmVudC5jdXJyZW50VGFyZ2V0KVxuICAgICAgICAuY2xvc2VzdCgndHInKVxuICAgICAgICAubmV4dCgnOmZpcnN0Jyk7XG5cbiAgICAgICRwYXltZW50RGV0YWlsUm93LnRvZ2dsZUNsYXNzKCdkLW5vbmUnKTtcbiAgICB9KTtcbiAgfVxuXG4gIGZ1bmN0aW9uIHRvZ2dsZVByaXZhdGVOb3RlQmxvY2soKSB7XG4gICAgY29uc3QgJGJsb2NrID0gJChPcmRlclZpZXdQYWdlTWFwLnByaXZhdGVOb3RlQmxvY2spO1xuICAgIGNvbnN0ICRidG4gPSAkKE9yZGVyVmlld1BhZ2VNYXAucHJpdmF0ZU5vdGVUb2dnbGVCdG4pO1xuICAgIGNvbnN0IGlzUHJpdmF0ZU5vdGVPcGVuZWQgPSAkYnRuLmhhc0NsYXNzKCdpcy1vcGVuZWQnKTtcblxuICAgIGlmIChpc1ByaXZhdGVOb3RlT3BlbmVkKSB7XG4gICAgICAkYnRuLnJlbW92ZUNsYXNzKCdpcy1vcGVuZWQnKTtcbiAgICAgICRibG9jay5hZGRDbGFzcygnZC1ub25lJyk7XG4gICAgfSBlbHNlIHtcbiAgICAgICRidG4uYWRkQ2xhc3MoJ2lzLW9wZW5lZCcpO1xuICAgICAgJGJsb2NrLnJlbW92ZUNsYXNzKCdkLW5vbmUnKTtcbiAgICB9XG5cbiAgICBjb25zdCAkaWNvbiA9ICRidG4uZmluZCgnLm1hdGVyaWFsLWljb25zJyk7XG4gICAgJGljb24udGV4dChpc1ByaXZhdGVOb3RlT3BlbmVkID8gJ2FkZCcgOiAncmVtb3ZlJyk7XG4gIH1cblxuICBmdW5jdGlvbiBoYW5kbGVQcml2YXRlTm90ZUNoYW5nZSgpIHtcbiAgICBjb25zdCAkc3VibWl0QnRuID0gJChPcmRlclZpZXdQYWdlTWFwLnByaXZhdGVOb3RlU3VibWl0QnRuKTtcblxuICAgICQoT3JkZXJWaWV3UGFnZU1hcC5wcml2YXRlTm90ZUlucHV0KS5vbignaW5wdXQnLCAoKSA9PiB7XG4gICAgICAkc3VibWl0QnRuLnByb3AoJ2Rpc2FibGVkJywgZmFsc2UpO1xuICAgIH0pO1xuICB9XG5cbiAgZnVuY3Rpb24gdG9nZ2xlT3JkZXJOb3RlQmxvY2soKSB7XG4gICAgY29uc3QgJGJsb2NrID0gJChPcmRlclZpZXdQYWdlTWFwLm9yZGVyTm90ZUJsb2NrKTtcbiAgICBjb25zdCAkYnRuID0gJChPcmRlclZpZXdQYWdlTWFwLm9yZGVyTm90ZVRvZ2dsZUJ0bik7XG4gICAgY29uc3QgaXNOb3RlT3BlbmVkID0gJGJ0bi5oYXNDbGFzcygnaXMtb3BlbmVkJyk7XG5cbiAgICAkYnRuLnRvZ2dsZUNsYXNzKCdpcy1vcGVuZWQnLCAhaXNOb3RlT3BlbmVkKTtcbiAgICAkYmxvY2sudG9nZ2xlQ2xhc3MoJ2Qtbm9uZScsIGlzTm90ZU9wZW5lZCk7XG5cbiAgICBjb25zdCAkaWNvbiA9ICRidG4uZmluZCgnLm1hdGVyaWFsLWljb25zJyk7XG4gICAgJGljb24udGV4dChpc05vdGVPcGVuZWQgPyAnYWRkJyA6ICdyZW1vdmUnKTtcbiAgfVxuXG4gIGZ1bmN0aW9uIGhhbmRsZU9yZGVyTm90ZUNoYW5nZSgpIHtcbiAgICBjb25zdCAkc3VibWl0QnRuID0gJChPcmRlclZpZXdQYWdlTWFwLm9yZGVyTm90ZVN1Ym1pdEJ0bik7XG5cbiAgICAkKE9yZGVyVmlld1BhZ2VNYXAub3JkZXJOb3RlSW5wdXQpLm9uKCdpbnB1dCcsICgpID0+IHtcbiAgICAgICRzdWJtaXRCdG4ucHJvcCgnZGlzYWJsZWQnLCBmYWxzZSk7XG4gICAgfSk7XG4gIH1cblxuICBmdW5jdGlvbiBpbml0QWRkQ2FydFJ1bGVGb3JtSGFuZGxlcigpIHtcbiAgICBjb25zdCAkbW9kYWwgPSAkKE9yZGVyVmlld1BhZ2VNYXAuYWRkQ2FydFJ1bGVNb2RhbCk7XG4gICAgY29uc3QgJGZvcm0gPSAkbW9kYWwuZmluZCgnZm9ybScpO1xuICAgIGNvbnN0ICRpbnZvaWNlU2VsZWN0ID0gJG1vZGFsLmZpbmQoT3JkZXJWaWV3UGFnZU1hcC5hZGRDYXJ0UnVsZUludm9pY2VJZFNlbGVjdCk7XG4gICAgY29uc3QgJHZhbHVlSGVscCA9ICRtb2RhbC5maW5kKE9yZGVyVmlld1BhZ2VNYXAuY2FydFJ1bGVIZWxwVGV4dCk7XG4gICAgY29uc3QgJHZhbHVlSW5wdXQgPSAkZm9ybS5maW5kKE9yZGVyVmlld1BhZ2VNYXAuYWRkQ2FydFJ1bGVWYWx1ZUlucHV0KTtcbiAgICBjb25zdCAkdmFsdWVGb3JtR3JvdXAgPSAkdmFsdWVJbnB1dC5jbG9zZXN0KCcuZm9ybS1ncm91cCcpO1xuXG4gICAgJG1vZGFsLm9uKCdzaG93bi5icy5tb2RhbCcsICgpID0+IHtcbiAgICAgICQoT3JkZXJWaWV3UGFnZU1hcC5hZGRDYXJ0UnVsZVN1Ym1pdCkuYXR0cignZGlzYWJsZWQnLCB0cnVlKTtcbiAgICB9KTtcblxuICAgICRmb3JtLmZpbmQoT3JkZXJWaWV3UGFnZU1hcC5hZGRDYXJ0UnVsZU5hbWVJbnB1dCkub24oJ2tleXVwJywgKGV2ZW50KSA9PiB7XG4gICAgICBjb25zdCBjYXJ0UnVsZU5hbWUgPSAkKGV2ZW50LmN1cnJlbnRUYXJnZXQpLnZhbCgpO1xuICAgICAgJChPcmRlclZpZXdQYWdlTWFwLmFkZENhcnRSdWxlU3VibWl0KS5hdHRyKCdkaXNhYmxlZCcsIGNhcnRSdWxlTmFtZS50cmltKCkubGVuZ3RoID09PSAwKTtcbiAgICB9KTtcblxuICAgICRmb3JtLmZpbmQoT3JkZXJWaWV3UGFnZU1hcC5hZGRDYXJ0UnVsZUFwcGx5T25BbGxJbnZvaWNlc0NoZWNrYm94KS5vbignY2hhbmdlJywgKGV2ZW50KSA9PiB7XG4gICAgICBjb25zdCBpc0NoZWNrZWQgPSAkKGV2ZW50LmN1cnJlbnRUYXJnZXQpLmlzKCc6Y2hlY2tlZCcpO1xuICAgICAgJGludm9pY2VTZWxlY3QuYXR0cignZGlzYWJsZWQnLCBpc0NoZWNrZWQpO1xuICAgIH0pO1xuXG4gICAgJGZvcm0uZmluZChPcmRlclZpZXdQYWdlTWFwLmFkZENhcnRSdWxlVHlwZVNlbGVjdCkub24oJ2NoYW5nZScsIChldmVudCkgPT4ge1xuICAgICAgY29uc3Qgc2VsZWN0ZWRDYXJ0UnVsZVR5cGUgPSAkKGV2ZW50LmN1cnJlbnRUYXJnZXQpLnZhbCgpO1xuICAgICAgY29uc3QgJHZhbHVlVW5pdCA9ICRmb3JtLmZpbmQoT3JkZXJWaWV3UGFnZU1hcC5hZGRDYXJ0UnVsZVZhbHVlVW5pdCk7XG5cbiAgICAgIGlmIChzZWxlY3RlZENhcnRSdWxlVHlwZSA9PT0gRElTQ09VTlRfVFlQRV9BTU9VTlQpIHtcbiAgICAgICAgJHZhbHVlSGVscC5yZW1vdmVDbGFzcygnZC1ub25lJyk7XG4gICAgICAgICR2YWx1ZVVuaXQuaHRtbCgkdmFsdWVVbml0LmRhdGEoJ2N1cnJlbmN5U3ltYm9sJykpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgJHZhbHVlSGVscC5hZGRDbGFzcygnZC1ub25lJyk7XG4gICAgICB9XG5cbiAgICAgIGlmIChzZWxlY3RlZENhcnRSdWxlVHlwZSA9PT0gRElTQ09VTlRfVFlQRV9QRVJDRU5UKSB7XG4gICAgICAgICR2YWx1ZVVuaXQuaHRtbCgnJScpO1xuICAgICAgfVxuXG4gICAgICBpZiAoc2VsZWN0ZWRDYXJ0UnVsZVR5cGUgPT09IERJU0NPVU5UX1RZUEVfRlJFRV9TSElQUElORykge1xuICAgICAgICAkdmFsdWVGb3JtR3JvdXAuYWRkQ2xhc3MoJ2Qtbm9uZScpO1xuICAgICAgICAkdmFsdWVJbnB1dC5hdHRyKCdkaXNhYmxlZCcsIHRydWUpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgJHZhbHVlRm9ybUdyb3VwLnJlbW92ZUNsYXNzKCdkLW5vbmUnKTtcbiAgICAgICAgJHZhbHVlSW5wdXQuYXR0cignZGlzYWJsZWQnLCBmYWxzZSk7XG4gICAgICB9XG4gICAgfSk7XG4gIH1cblxuICBmdW5jdGlvbiBoYW5kbGVVcGRhdGVPcmRlclN0YXR1c0J1dHRvbigpIHtcbiAgICBjb25zdCAkYnRuID0gJChPcmRlclZpZXdQYWdlTWFwLnVwZGF0ZU9yZGVyU3RhdHVzQWN0aW9uQnRuKTtcbiAgICBjb25zdCAkd3JhcHBlciA9ICQoT3JkZXJWaWV3UGFnZU1hcC51cGRhdGVPcmRlclN0YXR1c0FjdGlvbklucHV0V3JhcHBlcik7XG5cbiAgICAkKE9yZGVyVmlld1BhZ2VNYXAudXBkYXRlT3JkZXJTdGF0dXNBY3Rpb25JbnB1dCkub24oJ2NoYW5nZScsIChldmVudCkgPT4ge1xuICAgICAgY29uc3QgJGVsZW1lbnQgPSAkKGV2ZW50LmN1cnJlbnRUYXJnZXQpO1xuICAgICAgY29uc3QgJG9wdGlvbiA9ICQoJ29wdGlvbjpzZWxlY3RlZCcsICRlbGVtZW50KTtcbiAgICAgIGNvbnN0IHNlbGVjdGVkT3JkZXJTdGF0dXNJZCA9ICRlbGVtZW50LnZhbCgpO1xuXG4gICAgICAkd3JhcHBlci5jc3MoJ2JhY2tncm91bmQtY29sb3InLCAkb3B0aW9uLmRhdGEoJ2JhY2tncm91bmQtY29sb3InKSk7XG4gICAgICAkd3JhcHBlci50b2dnbGVDbGFzcygnaXMtYnJpZ2h0JywgJG9wdGlvbi5kYXRhKCdpcy1icmlnaHQnKSAhPT0gdW5kZWZpbmVkKTtcblxuICAgICAgJGJ0bi5wcm9wKCdkaXNhYmxlZCcsIHBhcnNlSW50KHNlbGVjdGVkT3JkZXJTdGF0dXNJZCwgMTApID09PSAkYnRuLmRhdGEoJ29yZGVyU3RhdHVzSWQnKSk7XG4gICAgfSk7XG4gIH1cblxuICBmdW5jdGlvbiBpbml0Q2hhbmdlQWRkcmVzc0Zvcm1IYW5kbGVyKCkge1xuICAgIGNvbnN0ICRtb2RhbCA9ICQoT3JkZXJWaWV3UGFnZU1hcC51cGRhdGVDdXN0b21lckFkZHJlc3NNb2RhbCk7XG5cbiAgICAkKE9yZGVyVmlld1BhZ2VNYXAub3Blbk9yZGVyQWRkcmVzc1VwZGF0ZU1vZGFsQnRuKS5vbignY2xpY2snLCAoZXZlbnQpID0+IHtcbiAgICAgICRtb2RhbC5maW5kKE9yZGVyVmlld1BhZ2VNYXAudXBkYXRlT3JkZXJBZGRyZXNzVHlwZUlucHV0KS52YWwoJChldmVudC5jdXJyZW50VGFyZ2V0KS5kYXRhKCdhZGRyZXNzVHlwZScpKTtcbiAgICB9KTtcbiAgfVxufSk7XG4iXSwic291cmNlUm9vdCI6IiJ9