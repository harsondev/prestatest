/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./js/app/utils/colorpicker.js":
/*!*************************************!*\
  !*** ./js/app/utils/colorpicker.js ***!
  \*************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

"use strict";


Object.defineProperty(exports, "__esModule", ({
  value: true
}));

__webpack_require__(/*! bootstrap-colorpicker */ "./node_modules/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.js");

var _window = window,
    $ = _window.$;

/**
 * Enable all colorpickers.
 */
/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 */

var init = function initDatePickers() {
  $('.colorpicker input[type="text"]').each(function (i, picker) {
    $(picker).colorpicker();
  });
};

exports.default = init;

/***/ }),

/***/ "./js/components/event-emitter.js":
/*!****************************************!*\
  !*** ./js/components/event-emitter.js ***!
  \****************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

"use strict";


Object.defineProperty(exports, "__esModule", ({
  value: true
}));
exports.EventEmitter = undefined;

var _events = __webpack_require__(/*! events */ "./node_modules/events/events.js");

var _events2 = _interopRequireDefault(_events);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * We instanciate one EventEmitter (restricted via a const) so that every components
 * register/dispatch on the same one and can communicate with each other.
 */
var EventEmitter = exports.EventEmitter = new _events2.default(); /**
                                                                   * Copyright since 2007 PrestaShop SA and Contributors
                                                                   * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
                                                                   *
                                                                   * NOTICE OF LICENSE
                                                                   *
                                                                   * This source file is subject to the Open Software License (OSL 3.0)
                                                                   * that is bundled with this package in the file LICENSE.md.
                                                                   * It is also available through the world-wide-web at this URL:
                                                                   * https://opensource.org/licenses/OSL-3.0
                                                                   * If you did not receive a copy of the license and are unable to
                                                                   * obtain it through the world-wide-web, please send an email
                                                                   * to license@prestashop.com so we can send you a copy immediately.
                                                                   *
                                                                   * DISCLAIMER
                                                                   *
                                                                   * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
                                                                   * versions in the future. If you wish to customize PrestaShop for your
                                                                   * needs please refer to https://devdocs.prestashop.com/ for more information.
                                                                   *
                                                                   * @author    PrestaShop SA and Contributors <contact@prestashop.com>
                                                                   * @copyright Since 2007 PrestaShop SA and Contributors
                                                                   * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
                                                                   */

exports.default = EventEmitter;

/***/ }),

/***/ "./js/components/translatable-input.js":
/*!*********************************************!*\
  !*** ./js/components/translatable-input.js ***!
  \*********************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

"use strict";


Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _eventEmitter = __webpack_require__(/*! ./event-emitter */ "./js/components/event-emitter.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _window = window,
    $ = _window.$;

/**
 * This class is used to automatically toggle translated inputs (displayed with one
 * input and a language selector using the TranslatableType Symfony form type).
 * Also compatible with TranslatableField changes.
 */
/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 */

var TranslatableInput = function () {
  function TranslatableInput(options) {
    var _this = this;

    (0, _classCallCheck3.default)(this, TranslatableInput);

    var opts = options || {};

    this.localeItemSelector = opts.localeItemSelector || '.js-locale-item';
    this.localeButtonSelector = opts.localeButtonSelector || '.js-locale-btn';
    this.localeInputSelector = opts.localeInputSelector || '.js-locale-input';
    this.selectedLocale = $(this.localeItemSelector).data('locale');

    $('body').on('click', this.localeItemSelector, this.toggleLanguage.bind(this));
    _eventEmitter.EventEmitter.on('languageSelected', this.toggleInputs.bind(this));

    return {
      localeItemSelector: this.localeItemSelector,
      localeButtonSelector: this.localeButtonSelector,
      localeInputSelector: this.localeInputSelector,

      /**
       * @param {jQuery} form
       */
      refreshFormInputs: function refreshFormInputs(form) {
        _this.refreshInputs(form);
      },

      /**
       * @returns {string|undefined}
       */
      getSelectedLocale: function getSelectedLocale() {
        return _this.selectedLocale;
      }
    };
  }

  /**
   * @param {jQuery} form
   *
   * @private
   */


  (0, _createClass3.default)(TranslatableInput, [{
    key: 'refreshInputs',
    value: function refreshInputs(form) {
      if (!this.selectedLocale) {
        return;
      }

      _eventEmitter.EventEmitter.emit('languageSelected', {
        selectedLocale: this.selectedLocale,
        form: form
      });
    }

    /**
     * Dispatch event on language selection to update inputs and other components which depend on the locale.
     *
     * @param event
     *
     * @private
     */

  }, {
    key: 'toggleLanguage',
    value: function toggleLanguage(event) {
      var localeItem = $(event.target);
      var form = localeItem.closest('form');
      this.selectedLocale = localeItem.data('locale');
      this.refreshInputs(form);
    }

    /**
     * Toggle all translatable inputs in form in which locale was changed
     *
     * @param {Event} event
     *
     * @private
     */

  }, {
    key: 'toggleInputs',
    value: function toggleInputs(event) {
      var form = event.form;

      this.selectedLocale = event.selectedLocale;
      var localeButton = form.find(this.localeButtonSelector);
      var changeLanguageUrl = localeButton.data('change-language-url');

      localeButton.text(this.selectedLocale);
      form.find(this.localeInputSelector).addClass('d-none');
      form.find(this.localeInputSelector + '.js-locale-' + this.selectedLocale).removeClass('d-none');

      if (changeLanguageUrl) {
        this.saveSelectedLanguage(changeLanguageUrl, this.selectedLocale);
      }
    }

    /**
     * Save language choice for employee forms.
     *
     * @param {String} changeLanguageUrl
     * @param {String} selectedLocale
     *
     * @private
     */

  }, {
    key: 'saveSelectedLanguage',
    value: function saveSelectedLanguage(changeLanguageUrl, selectedLocale) {
      $.post({
        url: changeLanguageUrl,
        data: {
          language_iso_code: selectedLocale
        }
      });
    }
  }]);
  return TranslatableInput;
}();

exports.default = TranslatableInput;

/***/ }),

/***/ "./node_modules/babel-runtime/core-js/object/define-property.js":
/*!**********************************************************************!*\
  !*** ./node_modules/babel-runtime/core-js/object/define-property.js ***!
  \**********************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

module.exports = { "default": __webpack_require__(/*! core-js/library/fn/object/define-property */ "./node_modules/core-js/library/fn/object/define-property.js"), __esModule: true };

/***/ }),

/***/ "./node_modules/babel-runtime/helpers/classCallCheck.js":
/*!**************************************************************!*\
  !*** ./node_modules/babel-runtime/helpers/classCallCheck.js ***!
  \**************************************************************/
/***/ ((__unused_webpack_module, exports) => {

"use strict";


exports.__esModule = true;

exports.default = function (instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
};

/***/ }),

/***/ "./node_modules/babel-runtime/helpers/createClass.js":
/*!***********************************************************!*\
  !*** ./node_modules/babel-runtime/helpers/createClass.js ***!
  \***********************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

"use strict";


exports.__esModule = true;

var _defineProperty = __webpack_require__(/*! ../core-js/object/define-property */ "./node_modules/babel-runtime/core-js/object/define-property.js");

var _defineProperty2 = _interopRequireDefault(_defineProperty);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      (0, _defineProperty2.default)(target, descriptor.key, descriptor);
    }
  }

  return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);
    if (staticProps) defineProperties(Constructor, staticProps);
    return Constructor;
  };
}();

/***/ }),

/***/ "./node_modules/core-js/library/fn/object/define-property.js":
/*!*******************************************************************!*\
  !*** ./node_modules/core-js/library/fn/object/define-property.js ***!
  \*******************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

__webpack_require__(/*! ../../modules/es6.object.define-property */ "./node_modules/core-js/library/modules/es6.object.define-property.js");
var $Object = __webpack_require__(/*! ../../modules/_core */ "./node_modules/core-js/library/modules/_core.js").Object;
module.exports = function defineProperty(it, key, desc) {
  return $Object.defineProperty(it, key, desc);
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_a-function.js":
/*!*************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_a-function.js ***!
  \*************************************************************/
/***/ ((module) => {

module.exports = function (it) {
  if (typeof it != 'function') throw TypeError(it + ' is not a function!');
  return it;
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_an-object.js":
/*!************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_an-object.js ***!
  \************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var isObject = __webpack_require__(/*! ./_is-object */ "./node_modules/core-js/library/modules/_is-object.js");
module.exports = function (it) {
  if (!isObject(it)) throw TypeError(it + ' is not an object!');
  return it;
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_core.js":
/*!*******************************************************!*\
  !*** ./node_modules/core-js/library/modules/_core.js ***!
  \*******************************************************/
/***/ ((module) => {

var core = module.exports = { version: '2.6.11' };
if (typeof __e == 'number') __e = core; // eslint-disable-line no-undef


/***/ }),

/***/ "./node_modules/core-js/library/modules/_ctx.js":
/*!******************************************************!*\
  !*** ./node_modules/core-js/library/modules/_ctx.js ***!
  \******************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// optional / simple context binding
var aFunction = __webpack_require__(/*! ./_a-function */ "./node_modules/core-js/library/modules/_a-function.js");
module.exports = function (fn, that, length) {
  aFunction(fn);
  if (that === undefined) return fn;
  switch (length) {
    case 1: return function (a) {
      return fn.call(that, a);
    };
    case 2: return function (a, b) {
      return fn.call(that, a, b);
    };
    case 3: return function (a, b, c) {
      return fn.call(that, a, b, c);
    };
  }
  return function (/* ...args */) {
    return fn.apply(that, arguments);
  };
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_descriptors.js":
/*!**************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_descriptors.js ***!
  \**************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// Thank's IE8 for his funny defineProperty
module.exports = !__webpack_require__(/*! ./_fails */ "./node_modules/core-js/library/modules/_fails.js")(function () {
  return Object.defineProperty({}, 'a', { get: function () { return 7; } }).a != 7;
});


/***/ }),

/***/ "./node_modules/core-js/library/modules/_dom-create.js":
/*!*************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_dom-create.js ***!
  \*************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var isObject = __webpack_require__(/*! ./_is-object */ "./node_modules/core-js/library/modules/_is-object.js");
var document = __webpack_require__(/*! ./_global */ "./node_modules/core-js/library/modules/_global.js").document;
// typeof document.createElement is 'object' in old IE
var is = isObject(document) && isObject(document.createElement);
module.exports = function (it) {
  return is ? document.createElement(it) : {};
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_export.js":
/*!*********************************************************!*\
  !*** ./node_modules/core-js/library/modules/_export.js ***!
  \*********************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var global = __webpack_require__(/*! ./_global */ "./node_modules/core-js/library/modules/_global.js");
var core = __webpack_require__(/*! ./_core */ "./node_modules/core-js/library/modules/_core.js");
var ctx = __webpack_require__(/*! ./_ctx */ "./node_modules/core-js/library/modules/_ctx.js");
var hide = __webpack_require__(/*! ./_hide */ "./node_modules/core-js/library/modules/_hide.js");
var has = __webpack_require__(/*! ./_has */ "./node_modules/core-js/library/modules/_has.js");
var PROTOTYPE = 'prototype';

var $export = function (type, name, source) {
  var IS_FORCED = type & $export.F;
  var IS_GLOBAL = type & $export.G;
  var IS_STATIC = type & $export.S;
  var IS_PROTO = type & $export.P;
  var IS_BIND = type & $export.B;
  var IS_WRAP = type & $export.W;
  var exports = IS_GLOBAL ? core : core[name] || (core[name] = {});
  var expProto = exports[PROTOTYPE];
  var target = IS_GLOBAL ? global : IS_STATIC ? global[name] : (global[name] || {})[PROTOTYPE];
  var key, own, out;
  if (IS_GLOBAL) source = name;
  for (key in source) {
    // contains in native
    own = !IS_FORCED && target && target[key] !== undefined;
    if (own && has(exports, key)) continue;
    // export native or passed
    out = own ? target[key] : source[key];
    // prevent global pollution for namespaces
    exports[key] = IS_GLOBAL && typeof target[key] != 'function' ? source[key]
    // bind timers to global for call from export context
    : IS_BIND && own ? ctx(out, global)
    // wrap global constructors for prevent change them in library
    : IS_WRAP && target[key] == out ? (function (C) {
      var F = function (a, b, c) {
        if (this instanceof C) {
          switch (arguments.length) {
            case 0: return new C();
            case 1: return new C(a);
            case 2: return new C(a, b);
          } return new C(a, b, c);
        } return C.apply(this, arguments);
      };
      F[PROTOTYPE] = C[PROTOTYPE];
      return F;
    // make static versions for prototype methods
    })(out) : IS_PROTO && typeof out == 'function' ? ctx(Function.call, out) : out;
    // export proto methods to core.%CONSTRUCTOR%.methods.%NAME%
    if (IS_PROTO) {
      (exports.virtual || (exports.virtual = {}))[key] = out;
      // export proto methods to core.%CONSTRUCTOR%.prototype.%NAME%
      if (type & $export.R && expProto && !expProto[key]) hide(expProto, key, out);
    }
  }
};
// type bitmap
$export.F = 1;   // forced
$export.G = 2;   // global
$export.S = 4;   // static
$export.P = 8;   // proto
$export.B = 16;  // bind
$export.W = 32;  // wrap
$export.U = 64;  // safe
$export.R = 128; // real proto method for `library`
module.exports = $export;


/***/ }),

/***/ "./node_modules/core-js/library/modules/_fails.js":
/*!********************************************************!*\
  !*** ./node_modules/core-js/library/modules/_fails.js ***!
  \********************************************************/
/***/ ((module) => {

module.exports = function (exec) {
  try {
    return !!exec();
  } catch (e) {
    return true;
  }
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_global.js":
/*!*********************************************************!*\
  !*** ./node_modules/core-js/library/modules/_global.js ***!
  \*********************************************************/
/***/ ((module) => {

// https://github.com/zloirock/core-js/issues/86#issuecomment-115759028
var global = module.exports = typeof window != 'undefined' && window.Math == Math
  ? window : typeof self != 'undefined' && self.Math == Math ? self
  // eslint-disable-next-line no-new-func
  : Function('return this')();
if (typeof __g == 'number') __g = global; // eslint-disable-line no-undef


/***/ }),

/***/ "./node_modules/core-js/library/modules/_has.js":
/*!******************************************************!*\
  !*** ./node_modules/core-js/library/modules/_has.js ***!
  \******************************************************/
/***/ ((module) => {

var hasOwnProperty = {}.hasOwnProperty;
module.exports = function (it, key) {
  return hasOwnProperty.call(it, key);
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_hide.js":
/*!*******************************************************!*\
  !*** ./node_modules/core-js/library/modules/_hide.js ***!
  \*******************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var dP = __webpack_require__(/*! ./_object-dp */ "./node_modules/core-js/library/modules/_object-dp.js");
var createDesc = __webpack_require__(/*! ./_property-desc */ "./node_modules/core-js/library/modules/_property-desc.js");
module.exports = __webpack_require__(/*! ./_descriptors */ "./node_modules/core-js/library/modules/_descriptors.js") ? function (object, key, value) {
  return dP.f(object, key, createDesc(1, value));
} : function (object, key, value) {
  object[key] = value;
  return object;
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_ie8-dom-define.js":
/*!*****************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_ie8-dom-define.js ***!
  \*****************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

module.exports = !__webpack_require__(/*! ./_descriptors */ "./node_modules/core-js/library/modules/_descriptors.js") && !__webpack_require__(/*! ./_fails */ "./node_modules/core-js/library/modules/_fails.js")(function () {
  return Object.defineProperty(__webpack_require__(/*! ./_dom-create */ "./node_modules/core-js/library/modules/_dom-create.js")('div'), 'a', { get: function () { return 7; } }).a != 7;
});


/***/ }),

/***/ "./node_modules/core-js/library/modules/_is-object.js":
/*!************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_is-object.js ***!
  \************************************************************/
/***/ ((module) => {

module.exports = function (it) {
  return typeof it === 'object' ? it !== null : typeof it === 'function';
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_object-dp.js":
/*!************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_object-dp.js ***!
  \************************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

var anObject = __webpack_require__(/*! ./_an-object */ "./node_modules/core-js/library/modules/_an-object.js");
var IE8_DOM_DEFINE = __webpack_require__(/*! ./_ie8-dom-define */ "./node_modules/core-js/library/modules/_ie8-dom-define.js");
var toPrimitive = __webpack_require__(/*! ./_to-primitive */ "./node_modules/core-js/library/modules/_to-primitive.js");
var dP = Object.defineProperty;

exports.f = __webpack_require__(/*! ./_descriptors */ "./node_modules/core-js/library/modules/_descriptors.js") ? Object.defineProperty : function defineProperty(O, P, Attributes) {
  anObject(O);
  P = toPrimitive(P, true);
  anObject(Attributes);
  if (IE8_DOM_DEFINE) try {
    return dP(O, P, Attributes);
  } catch (e) { /* empty */ }
  if ('get' in Attributes || 'set' in Attributes) throw TypeError('Accessors not supported!');
  if ('value' in Attributes) O[P] = Attributes.value;
  return O;
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_property-desc.js":
/*!****************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_property-desc.js ***!
  \****************************************************************/
/***/ ((module) => {

module.exports = function (bitmap, value) {
  return {
    enumerable: !(bitmap & 1),
    configurable: !(bitmap & 2),
    writable: !(bitmap & 4),
    value: value
  };
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_to-primitive.js":
/*!***************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_to-primitive.js ***!
  \***************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// 7.1.1 ToPrimitive(input [, PreferredType])
var isObject = __webpack_require__(/*! ./_is-object */ "./node_modules/core-js/library/modules/_is-object.js");
// instead of the ES6 spec version, we didn't implement @@toPrimitive case
// and the second argument - flag - preferred type is a string
module.exports = function (it, S) {
  if (!isObject(it)) return it;
  var fn, val;
  if (S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;
  if (typeof (fn = it.valueOf) == 'function' && !isObject(val = fn.call(it))) return val;
  if (!S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;
  throw TypeError("Can't convert object to primitive value");
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/es6.object.define-property.js":
/*!****************************************************************************!*\
  !*** ./node_modules/core-js/library/modules/es6.object.define-property.js ***!
  \****************************************************************************/
/***/ ((__unused_webpack_module, __unused_webpack_exports, __webpack_require__) => {

var $export = __webpack_require__(/*! ./_export */ "./node_modules/core-js/library/modules/_export.js");
// 19.1.2.4 / 15.2.3.6 Object.defineProperty(O, P, Attributes)
$export($export.S + $export.F * !__webpack_require__(/*! ./_descriptors */ "./node_modules/core-js/library/modules/_descriptors.js"), 'Object', { defineProperty: __webpack_require__(/*! ./_object-dp */ "./node_modules/core-js/library/modules/_object-dp.js").f });


/***/ }),

/***/ "./node_modules/events/events.js":
/*!***************************************!*\
  !*** ./node_modules/events/events.js ***!
  \***************************************/
/***/ ((module) => {

"use strict";
// Copyright Joyent, Inc. and other Node contributors.
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.



var R = typeof Reflect === 'object' ? Reflect : null
var ReflectApply = R && typeof R.apply === 'function'
  ? R.apply
  : function ReflectApply(target, receiver, args) {
    return Function.prototype.apply.call(target, receiver, args);
  }

var ReflectOwnKeys
if (R && typeof R.ownKeys === 'function') {
  ReflectOwnKeys = R.ownKeys
} else if (Object.getOwnPropertySymbols) {
  ReflectOwnKeys = function ReflectOwnKeys(target) {
    return Object.getOwnPropertyNames(target)
      .concat(Object.getOwnPropertySymbols(target));
  };
} else {
  ReflectOwnKeys = function ReflectOwnKeys(target) {
    return Object.getOwnPropertyNames(target);
  };
}

function ProcessEmitWarning(warning) {
  if (console && console.warn) console.warn(warning);
}

var NumberIsNaN = Number.isNaN || function NumberIsNaN(value) {
  return value !== value;
}

function EventEmitter() {
  EventEmitter.init.call(this);
}
module.exports = EventEmitter;

// Backwards-compat with node 0.10.x
EventEmitter.EventEmitter = EventEmitter;

EventEmitter.prototype._events = undefined;
EventEmitter.prototype._eventsCount = 0;
EventEmitter.prototype._maxListeners = undefined;

// By default EventEmitters will print a warning if more than 10 listeners are
// added to it. This is a useful default which helps finding memory leaks.
var defaultMaxListeners = 10;

function checkListener(listener) {
  if (typeof listener !== 'function') {
    throw new TypeError('The "listener" argument must be of type Function. Received type ' + typeof listener);
  }
}

Object.defineProperty(EventEmitter, 'defaultMaxListeners', {
  enumerable: true,
  get: function() {
    return defaultMaxListeners;
  },
  set: function(arg) {
    if (typeof arg !== 'number' || arg < 0 || NumberIsNaN(arg)) {
      throw new RangeError('The value of "defaultMaxListeners" is out of range. It must be a non-negative number. Received ' + arg + '.');
    }
    defaultMaxListeners = arg;
  }
});

EventEmitter.init = function() {

  if (this._events === undefined ||
      this._events === Object.getPrototypeOf(this)._events) {
    this._events = Object.create(null);
    this._eventsCount = 0;
  }

  this._maxListeners = this._maxListeners || undefined;
};

// Obviously not all Emitters should be limited to 10. This function allows
// that to be increased. Set to zero for unlimited.
EventEmitter.prototype.setMaxListeners = function setMaxListeners(n) {
  if (typeof n !== 'number' || n < 0 || NumberIsNaN(n)) {
    throw new RangeError('The value of "n" is out of range. It must be a non-negative number. Received ' + n + '.');
  }
  this._maxListeners = n;
  return this;
};

function _getMaxListeners(that) {
  if (that._maxListeners === undefined)
    return EventEmitter.defaultMaxListeners;
  return that._maxListeners;
}

EventEmitter.prototype.getMaxListeners = function getMaxListeners() {
  return _getMaxListeners(this);
};

EventEmitter.prototype.emit = function emit(type) {
  var args = [];
  for (var i = 1; i < arguments.length; i++) args.push(arguments[i]);
  var doError = (type === 'error');

  var events = this._events;
  if (events !== undefined)
    doError = (doError && events.error === undefined);
  else if (!doError)
    return false;

  // If there is no 'error' event listener then throw.
  if (doError) {
    var er;
    if (args.length > 0)
      er = args[0];
    if (er instanceof Error) {
      // Note: The comments on the `throw` lines are intentional, they show
      // up in Node's output if this results in an unhandled exception.
      throw er; // Unhandled 'error' event
    }
    // At least give some kind of context to the user
    var err = new Error('Unhandled error.' + (er ? ' (' + er.message + ')' : ''));
    err.context = er;
    throw err; // Unhandled 'error' event
  }

  var handler = events[type];

  if (handler === undefined)
    return false;

  if (typeof handler === 'function') {
    ReflectApply(handler, this, args);
  } else {
    var len = handler.length;
    var listeners = arrayClone(handler, len);
    for (var i = 0; i < len; ++i)
      ReflectApply(listeners[i], this, args);
  }

  return true;
};

function _addListener(target, type, listener, prepend) {
  var m;
  var events;
  var existing;

  checkListener(listener);

  events = target._events;
  if (events === undefined) {
    events = target._events = Object.create(null);
    target._eventsCount = 0;
  } else {
    // To avoid recursion in the case that type === "newListener"! Before
    // adding it to the listeners, first emit "newListener".
    if (events.newListener !== undefined) {
      target.emit('newListener', type,
                  listener.listener ? listener.listener : listener);

      // Re-assign `events` because a newListener handler could have caused the
      // this._events to be assigned to a new object
      events = target._events;
    }
    existing = events[type];
  }

  if (existing === undefined) {
    // Optimize the case of one listener. Don't need the extra array object.
    existing = events[type] = listener;
    ++target._eventsCount;
  } else {
    if (typeof existing === 'function') {
      // Adding the second element, need to change to array.
      existing = events[type] =
        prepend ? [listener, existing] : [existing, listener];
      // If we've already got an array, just append.
    } else if (prepend) {
      existing.unshift(listener);
    } else {
      existing.push(listener);
    }

    // Check for listener leak
    m = _getMaxListeners(target);
    if (m > 0 && existing.length > m && !existing.warned) {
      existing.warned = true;
      // No error code for this since it is a Warning
      // eslint-disable-next-line no-restricted-syntax
      var w = new Error('Possible EventEmitter memory leak detected. ' +
                          existing.length + ' ' + String(type) + ' listeners ' +
                          'added. Use emitter.setMaxListeners() to ' +
                          'increase limit');
      w.name = 'MaxListenersExceededWarning';
      w.emitter = target;
      w.type = type;
      w.count = existing.length;
      ProcessEmitWarning(w);
    }
  }

  return target;
}

EventEmitter.prototype.addListener = function addListener(type, listener) {
  return _addListener(this, type, listener, false);
};

EventEmitter.prototype.on = EventEmitter.prototype.addListener;

EventEmitter.prototype.prependListener =
    function prependListener(type, listener) {
      return _addListener(this, type, listener, true);
    };

function onceWrapper() {
  if (!this.fired) {
    this.target.removeListener(this.type, this.wrapFn);
    this.fired = true;
    if (arguments.length === 0)
      return this.listener.call(this.target);
    return this.listener.apply(this.target, arguments);
  }
}

function _onceWrap(target, type, listener) {
  var state = { fired: false, wrapFn: undefined, target: target, type: type, listener: listener };
  var wrapped = onceWrapper.bind(state);
  wrapped.listener = listener;
  state.wrapFn = wrapped;
  return wrapped;
}

EventEmitter.prototype.once = function once(type, listener) {
  checkListener(listener);
  this.on(type, _onceWrap(this, type, listener));
  return this;
};

EventEmitter.prototype.prependOnceListener =
    function prependOnceListener(type, listener) {
      checkListener(listener);
      this.prependListener(type, _onceWrap(this, type, listener));
      return this;
    };

// Emits a 'removeListener' event if and only if the listener was removed.
EventEmitter.prototype.removeListener =
    function removeListener(type, listener) {
      var list, events, position, i, originalListener;

      checkListener(listener);

      events = this._events;
      if (events === undefined)
        return this;

      list = events[type];
      if (list === undefined)
        return this;

      if (list === listener || list.listener === listener) {
        if (--this._eventsCount === 0)
          this._events = Object.create(null);
        else {
          delete events[type];
          if (events.removeListener)
            this.emit('removeListener', type, list.listener || listener);
        }
      } else if (typeof list !== 'function') {
        position = -1;

        for (i = list.length - 1; i >= 0; i--) {
          if (list[i] === listener || list[i].listener === listener) {
            originalListener = list[i].listener;
            position = i;
            break;
          }
        }

        if (position < 0)
          return this;

        if (position === 0)
          list.shift();
        else {
          spliceOne(list, position);
        }

        if (list.length === 1)
          events[type] = list[0];

        if (events.removeListener !== undefined)
          this.emit('removeListener', type, originalListener || listener);
      }

      return this;
    };

EventEmitter.prototype.off = EventEmitter.prototype.removeListener;

EventEmitter.prototype.removeAllListeners =
    function removeAllListeners(type) {
      var listeners, events, i;

      events = this._events;
      if (events === undefined)
        return this;

      // not listening for removeListener, no need to emit
      if (events.removeListener === undefined) {
        if (arguments.length === 0) {
          this._events = Object.create(null);
          this._eventsCount = 0;
        } else if (events[type] !== undefined) {
          if (--this._eventsCount === 0)
            this._events = Object.create(null);
          else
            delete events[type];
        }
        return this;
      }

      // emit removeListener for all listeners on all events
      if (arguments.length === 0) {
        var keys = Object.keys(events);
        var key;
        for (i = 0; i < keys.length; ++i) {
          key = keys[i];
          if (key === 'removeListener') continue;
          this.removeAllListeners(key);
        }
        this.removeAllListeners('removeListener');
        this._events = Object.create(null);
        this._eventsCount = 0;
        return this;
      }

      listeners = events[type];

      if (typeof listeners === 'function') {
        this.removeListener(type, listeners);
      } else if (listeners !== undefined) {
        // LIFO order
        for (i = listeners.length - 1; i >= 0; i--) {
          this.removeListener(type, listeners[i]);
        }
      }

      return this;
    };

function _listeners(target, type, unwrap) {
  var events = target._events;

  if (events === undefined)
    return [];

  var evlistener = events[type];
  if (evlistener === undefined)
    return [];

  if (typeof evlistener === 'function')
    return unwrap ? [evlistener.listener || evlistener] : [evlistener];

  return unwrap ?
    unwrapListeners(evlistener) : arrayClone(evlistener, evlistener.length);
}

EventEmitter.prototype.listeners = function listeners(type) {
  return _listeners(this, type, true);
};

EventEmitter.prototype.rawListeners = function rawListeners(type) {
  return _listeners(this, type, false);
};

EventEmitter.listenerCount = function(emitter, type) {
  if (typeof emitter.listenerCount === 'function') {
    return emitter.listenerCount(type);
  } else {
    return listenerCount.call(emitter, type);
  }
};

EventEmitter.prototype.listenerCount = listenerCount;
function listenerCount(type) {
  var events = this._events;

  if (events !== undefined) {
    var evlistener = events[type];

    if (typeof evlistener === 'function') {
      return 1;
    } else if (evlistener !== undefined) {
      return evlistener.length;
    }
  }

  return 0;
}

EventEmitter.prototype.eventNames = function eventNames() {
  return this._eventsCount > 0 ? ReflectOwnKeys(this._events) : [];
};

function arrayClone(arr, n) {
  var copy = new Array(n);
  for (var i = 0; i < n; ++i)
    copy[i] = arr[i];
  return copy;
}

function spliceOne(list, index) {
  for (; index + 1 < list.length; index++)
    list[index] = list[index + 1];
  list.pop();
}

function unwrapListeners(arr) {
  var ret = new Array(arr.length);
  for (var i = 0; i < ret.length; ++i) {
    ret[i] = arr[i].listener || arr[i];
  }
  return ret;
}


/***/ }),

/***/ "./node_modules/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.js":
/*!*****************************************************************************!*\
  !*** ./node_modules/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.js ***!
  \*****************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

/*** IMPORTS FROM imports-loader ***/

(function(define, exports) {
/*!
 * Bootstrap Colorpicker - Bootstrap Colorpicker is a modular color picker plugin for Bootstrap 4.
 * @package bootstrap-colorpicker
 * @version v3.2.0
 * @license MIT
 * @link https://itsjavi.com/bootstrap-colorpicker/
 * @link https://github.com/itsjavi/bootstrap-colorpicker.git
 */
(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && "object" === 'object')
		module.exports = factory(__webpack_require__(/*! jquery */ "jquery"));
	else if(typeof define === 'function' && define.amd)
		define("bootstrap-colorpicker", ["jquery"], factory);
	else if(typeof exports === 'object')
		exports["bootstrap-colorpicker"] = factory(__webpack_require__(/*! jquery */ "jquery"));
	else
		root["bootstrap-colorpicker"] = factory(root["jQuery"]);
})(window, function(__WEBPACK_EXTERNAL_MODULE__0__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __nested_webpack_require_1036__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __nested_webpack_require_1036__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__nested_webpack_require_1036__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__nested_webpack_require_1036__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__nested_webpack_require_1036__.d = function(exports, name, getter) {
/******/ 		if(!__nested_webpack_require_1036__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__nested_webpack_require_1036__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__nested_webpack_require_1036__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __nested_webpack_require_1036__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__nested_webpack_require_1036__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __nested_webpack_require_1036__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__nested_webpack_require_1036__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__nested_webpack_require_1036__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__nested_webpack_require_1036__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__nested_webpack_require_1036__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __nested_webpack_require_1036__(__nested_webpack_require_1036__.s = 7);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE__0__;

/***/ }),
/* 1 */
/***/ (function(module, exports, __nested_webpack_require_4604__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _jquery = __nested_webpack_require_4604__(0);

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * Colorpicker extension class.
 */
var Extension = function () {
  /**
   * @param {Colorpicker} colorpicker
   * @param {Object} options
   */
  function Extension(colorpicker) {
    var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

    _classCallCheck(this, Extension);

    /**
     * The colorpicker instance
     * @type {Colorpicker}
     */
    this.colorpicker = colorpicker;
    /**
     * Extension options
     *
     * @type {Object}
     */
    this.options = options;

    if (!(this.colorpicker.element && this.colorpicker.element.length)) {
      throw new Error('Extension: this.colorpicker.element is not valid');
    }

    this.colorpicker.element.on('colorpickerCreate.colorpicker-ext', _jquery2.default.proxy(this.onCreate, this));
    this.colorpicker.element.on('colorpickerDestroy.colorpicker-ext', _jquery2.default.proxy(this.onDestroy, this));
    this.colorpicker.element.on('colorpickerUpdate.colorpicker-ext', _jquery2.default.proxy(this.onUpdate, this));
    this.colorpicker.element.on('colorpickerChange.colorpicker-ext', _jquery2.default.proxy(this.onChange, this));
    this.colorpicker.element.on('colorpickerInvalid.colorpicker-ext', _jquery2.default.proxy(this.onInvalid, this));
    this.colorpicker.element.on('colorpickerShow.colorpicker-ext', _jquery2.default.proxy(this.onShow, this));
    this.colorpicker.element.on('colorpickerHide.colorpicker-ext', _jquery2.default.proxy(this.onHide, this));
    this.colorpicker.element.on('colorpickerEnable.colorpicker-ext', _jquery2.default.proxy(this.onEnable, this));
    this.colorpicker.element.on('colorpickerDisable.colorpicker-ext', _jquery2.default.proxy(this.onDisable, this));
  }

  /**
   * Function called every time a new color needs to be created.
   * Return false to skip this resolver and continue with other extensions' ones
   * or return anything else to consider the color resolved.
   *
   * @param {ColorItem|String|*} color
   * @param {boolean} realColor if true, the color should resolve into a real (not named) color code
   * @return {ColorItem|String|*}
   */


  _createClass(Extension, [{
    key: 'resolveColor',
    value: function resolveColor(color) {
      var realColor = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

      return false;
    }

    /**
     * Method called after the colorpicker is created
     *
     * @listens Colorpicker#colorpickerCreate
     * @param {Event} event
     */

  }, {
    key: 'onCreate',
    value: function onCreate(event) {}
    // to be extended


    /**
     * Method called after the colorpicker is destroyed
     *
     * @listens Colorpicker#colorpickerDestroy
     * @param {Event} event
     */

  }, {
    key: 'onDestroy',
    value: function onDestroy(event) {
      this.colorpicker.element.off('.colorpicker-ext');
    }

    /**
     * Method called after the colorpicker is updated
     *
     * @listens Colorpicker#colorpickerUpdate
     * @param {Event} event
     */

  }, {
    key: 'onUpdate',
    value: function onUpdate(event) {}
    // to be extended


    /**
     * Method called after the colorpicker color is changed
     *
     * @listens Colorpicker#colorpickerChange
     * @param {Event} event
     */

  }, {
    key: 'onChange',
    value: function onChange(event) {}
    // to be extended


    /**
     * Method called when the colorpicker color is invalid
     *
     * @listens Colorpicker#colorpickerInvalid
     * @param {Event} event
     */

  }, {
    key: 'onInvalid',
    value: function onInvalid(event) {}
    // to be extended


    /**
     * Method called after the colorpicker is hidden
     *
     * @listens Colorpicker#colorpickerHide
     * @param {Event} event
     */

  }, {
    key: 'onHide',
    value: function onHide(event) {}
    // to be extended


    /**
     * Method called after the colorpicker is shown
     *
     * @listens Colorpicker#colorpickerShow
     * @param {Event} event
     */

  }, {
    key: 'onShow',
    value: function onShow(event) {}
    // to be extended


    /**
     * Method called after the colorpicker is disabled
     *
     * @listens Colorpicker#colorpickerDisable
     * @param {Event} event
     */

  }, {
    key: 'onDisable',
    value: function onDisable(event) {}
    // to be extended


    /**
     * Method called after the colorpicker is enabled
     *
     * @listens Colorpicker#colorpickerEnable
     * @param {Event} event
     */

  }, {
    key: 'onEnable',
    value: function onEnable(event) {
      // to be extended
    }
  }]);

  return Extension;
}();

exports.default = Extension;
module.exports = exports.default;

/***/ }),
/* 2 */
/***/ (function(module, exports, __nested_webpack_require_10342__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ColorItem = exports.HSVAColor = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }(); /**
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      * Color manipulation class, specific for Bootstrap Colorpicker
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      */


var _color = __nested_webpack_require_10342__(16);

var _color2 = _interopRequireDefault(_color);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * HSVA color data class, containing the hue, saturation, value and alpha
 * information.
 */
var HSVAColor = function () {
  /**
   * @param {number|int} h
   * @param {number|int} s
   * @param {number|int} v
   * @param {number|int} a
   */
  function HSVAColor(h, s, v, a) {
    _classCallCheck(this, HSVAColor);

    this.h = isNaN(h) ? 0 : h;
    this.s = isNaN(s) ? 0 : s;
    this.v = isNaN(v) ? 0 : v;
    this.a = isNaN(h) ? 1 : a;
  }

  _createClass(HSVAColor, [{
    key: 'toString',
    value: function toString() {
      return this.h + ', ' + this.s + '%, ' + this.v + '%, ' + this.a;
    }
  }]);

  return HSVAColor;
}();

/**
 * HSVA color manipulation
 */


var ColorItem = function () {
  _createClass(ColorItem, [{
    key: 'api',


    /**
     * Applies a method of the QixColor API and returns a new Color object or
     * the return value of the method call.
     *
     * If no argument is provided, the internal QixColor object is returned.
     *
     * @param {String} fn QixColor function name
     * @param args QixColor function arguments
     * @example let darkerColor = color.api('darken', 0.25);
     * @example let luminosity = color.api('luminosity');
     * @example color = color.api('negate');
     * @example let qColor = color.api().negate();
     * @returns {ColorItem|QixColor|*}
     */
    value: function api(fn) {
      for (var _len = arguments.length, args = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
        args[_key - 1] = arguments[_key];
      }

      if (arguments.length === 0) {
        return this._color;
      }

      var result = this._color[fn].apply(this._color, args);

      if (!(result instanceof _color2.default)) {
        // return result of the method call
        return result;
      }

      return new ColorItem(result, this.format);
    }

    /**
     * Returns the original ColorItem constructor data,
     * plus a 'valid' flag to know if it's valid or not.
     *
     * @returns {{color: *, format: String, valid: boolean}}
     */

  }, {
    key: 'original',
    get: function get() {
      return this._original;
    }

    /**
     * @param {ColorItem|HSVAColor|QixColor|String|*|null} color Color data
     * @param {String|null} format Color model to convert to by default. Supported: 'rgb', 'hsl', 'hex'.
     */

  }], [{
    key: 'HSVAColor',


    /**
     * Returns the HSVAColor class
     *
     * @static
     * @example let colorData = new ColorItem.HSVAColor(360, 100, 100, 1);
     * @returns {HSVAColor}
     */
    get: function get() {
      return HSVAColor;
    }
  }]);

  function ColorItem() {
    var color = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
    var format = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;

    _classCallCheck(this, ColorItem);

    this.replace(color, format);
  }

  /**
   * Replaces the internal QixColor object with a new one.
   * This also replaces the internal original color data.
   *
   * @param {ColorItem|HSVAColor|QixColor|String|*|null} color Color data to be parsed (if needed)
   * @param {String|null} format Color model to convert to by default. Supported: 'rgb', 'hsl', 'hex'.
   * @example color.replace('rgb(255,0,0)', 'hsl');
   * @example color.replace(hsvaColorData);
   */


  _createClass(ColorItem, [{
    key: 'replace',
    value: function replace(color) {
      var format = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;

      format = ColorItem.sanitizeFormat(format);

      /**
       * @type {{color: *, format: String}}
       * @private
       */
      this._original = {
        color: color,
        format: format,
        valid: true
      };
      /**
       * @type {QixColor}
       * @private
       */
      this._color = ColorItem.parse(color);

      if (this._color === null) {
        this._color = (0, _color2.default)();
        this._original.valid = false;
        return;
      }

      /**
       * @type {*|string}
       * @private
       */
      this._format = format ? format : ColorItem.isHex(color) ? 'hex' : this._color.model;
    }

    /**
     * Parses the color returning a Qix Color object or null if cannot be
     * parsed.
     *
     * @param {ColorItem|HSVAColor|QixColor|String|*|null} color Color data
     * @example let qColor = ColorItem.parse('rgb(255,0,0)');
     * @static
     * @returns {QixColor|null}
     */

  }, {
    key: 'isValid',


    /**
     * Returns true if the color is valid, false if not.
     *
     * @returns {boolean}
     */
    value: function isValid() {
      return this._original.valid === true;
    }

    /**
     * Hue value from 0 to 360
     *
     * @returns {int}
     */

  }, {
    key: 'setHueRatio',


    /**
     * Sets the hue ratio, where 1.0 is 0, 0.5 is 180 and 0.0 is 360.
     *
     * @ignore
     * @param {number} h Ratio from 1.0 to 0.0
     */
    value: function setHueRatio(h) {
      this.hue = (1 - h) * 360;
    }

    /**
     * Sets the saturation value
     *
     * @param {int} value Integer from 0 to 100
     */

  }, {
    key: 'setSaturationRatio',


    /**
     * Sets the saturation ratio, where 1.0 is 100 and 0.0 is 0.
     *
     * @ignore
     * @param {number} s Ratio from 0.0 to 1.0
     */
    value: function setSaturationRatio(s) {
      this.saturation = s * 100;
    }

    /**
     * Sets the 'value' channel value
     *
     * @param {int} value Integer from 0 to 100
     */

  }, {
    key: 'setValueRatio',


    /**
     * Sets the value ratio, where 1.0 is 0 and 0.0 is 100.
     *
     * @ignore
     * @param {number} v Ratio from 1.0 to 0.0
     */
    value: function setValueRatio(v) {
      this.value = (1 - v) * 100;
    }

    /**
     * Sets the alpha value. It will be rounded to 2 decimals.
     *
     * @param {int} value Float from 0.0 to 1.0
     */

  }, {
    key: 'setAlphaRatio',


    /**
     * Sets the alpha ratio, where 1.0 is 0.0 and 0.0 is 1.0.
     *
     * @ignore
     * @param {number} a Ratio from 1.0 to 0.0
     */
    value: function setAlphaRatio(a) {
      this.alpha = 1 - a;
    }

    /**
     * Sets the default color format
     *
     * @param {String} value Supported: 'rgb', 'hsl', 'hex'
     */

  }, {
    key: 'isDesaturated',


    /**
     * Returns true if the saturation value is zero, false otherwise
     *
     * @returns {boolean}
     */
    value: function isDesaturated() {
      return this.saturation === 0;
    }

    /**
     * Returns true if the alpha value is zero, false otherwise
     *
     * @returns {boolean}
     */

  }, {
    key: 'isTransparent',
    value: function isTransparent() {
      return this.alpha === 0;
    }

    /**
     * Returns true if the alpha value is numeric and less than 1, false otherwise
     *
     * @returns {boolean}
     */

  }, {
    key: 'hasTransparency',
    value: function hasTransparency() {
      return this.hasAlpha() && this.alpha < 1;
    }

    /**
     * Returns true if the alpha value is numeric, false otherwise
     *
     * @returns {boolean}
     */

  }, {
    key: 'hasAlpha',
    value: function hasAlpha() {
      return !isNaN(this.alpha);
    }

    /**
     * Returns a new HSVAColor object, based on the current color
     *
     * @returns {HSVAColor}
     */

  }, {
    key: 'toObject',
    value: function toObject() {
      return new HSVAColor(this.hue, this.saturation, this.value, this.alpha);
    }

    /**
     * Alias of toObject()
     *
     * @returns {HSVAColor}
     */

  }, {
    key: 'toHsva',
    value: function toHsva() {
      return this.toObject();
    }

    /**
     * Returns a new HSVAColor object with the ratio values (from 0.0 to 1.0),
     * based on the current color.
     *
     * @ignore
     * @returns {HSVAColor}
     */

  }, {
    key: 'toHsvaRatio',
    value: function toHsvaRatio() {
      return new HSVAColor(this.hue / 360, this.saturation / 100, this.value / 100, this.alpha);
    }

    /**
     * Converts the current color to its string representation,
     * using the internal format of this instance.
     *
     * @returns {String}
     */

  }, {
    key: 'toString',
    value: function toString() {
      return this.string();
    }

    /**
     * Converts the current color to its string representation,
     * using the given format.
     *
     * @param {String|null} format Format to convert to. If empty or null, the internal format will be used.
     * @returns {String}
     */

  }, {
    key: 'string',
    value: function string() {
      var format = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;

      format = ColorItem.sanitizeFormat(format ? format : this.format);

      if (!format) {
        return this._color.round().string();
      }

      if (this._color[format] === undefined) {
        throw new Error('Unsupported color format: \'' + format + '\'');
      }

      var str = this._color[format]();

      return str.round ? str.round().string() : str;
    }

    /**
     * Returns true if the given color values equals this one, false otherwise.
     * The format is not compared.
     * If any of the colors is invalid, the result will be false.
     *
     * @param {ColorItem|HSVAColor|QixColor|String|*|null} color Color data
     *
     * @returns {boolean}
     */

  }, {
    key: 'equals',
    value: function equals(color) {
      color = color instanceof ColorItem ? color : new ColorItem(color);

      if (!color.isValid() || !this.isValid()) {
        return false;
      }

      return this.hue === color.hue && this.saturation === color.saturation && this.value === color.value && this.alpha === color.alpha;
    }

    /**
     * Creates a copy of this instance
     *
     * @returns {ColorItem}
     */

  }, {
    key: 'getClone',
    value: function getClone() {
      return new ColorItem(this._color, this.format);
    }

    /**
     * Creates a copy of this instance, only copying the hue value,
     * and setting the others to its max value.
     *
     * @returns {ColorItem}
     */

  }, {
    key: 'getCloneHueOnly',
    value: function getCloneHueOnly() {
      return new ColorItem([this.hue, 100, 100, 1], this.format);
    }

    /**
     * Creates a copy of this instance setting the alpha to the max.
     *
     * @returns {ColorItem}
     */

  }, {
    key: 'getCloneOpaque',
    value: function getCloneOpaque() {
      return new ColorItem(this._color.alpha(1), this.format);
    }

    /**
     * Converts the color to a RGB string
     *
     * @returns {String}
     */

  }, {
    key: 'toRgbString',
    value: function toRgbString() {
      return this.string('rgb');
    }

    /**
     * Converts the color to a Hexadecimal string
     *
     * @returns {String}
     */

  }, {
    key: 'toHexString',
    value: function toHexString() {
      return this.string('hex');
    }

    /**
     * Converts the color to a HSL string
     *
     * @returns {String}
     */

  }, {
    key: 'toHslString',
    value: function toHslString() {
      return this.string('hsl');
    }

    /**
     * Returns true if the color is dark, false otherwhise.
     * This is useful to decide a text color.
     *
     * @returns {boolean}
     */

  }, {
    key: 'isDark',
    value: function isDark() {
      return this._color.isDark();
    }

    /**
     * Returns true if the color is light, false otherwhise.
     * This is useful to decide a text color.
     *
     * @returns {boolean}
     */

  }, {
    key: 'isLight',
    value: function isLight() {
      return this._color.isLight();
    }

    /**
     * Generates a list of colors using the given hue-based formula or the given array of hue values.
     * Hue formulas can be extended using ColorItem.colorFormulas static property.
     *
     * @param {String|Number[]} formula Examples: 'complementary', 'triad', 'tetrad', 'splitcomplement', [180, 270]
     * @example let colors = color.generate('triad');
     * @example let colors = color.generate([45, 80, 112, 200]);
     * @returns {ColorItem[]}
     */

  }, {
    key: 'generate',
    value: function generate(formula) {
      var hues = [];

      if (Array.isArray(formula)) {
        hues = formula;
      } else if (!ColorItem.colorFormulas.hasOwnProperty(formula)) {
        throw new Error('No color formula found with the name \'' + formula + '\'.');
      } else {
        hues = ColorItem.colorFormulas[formula];
      }

      var colors = [],
          mainColor = this._color,
          format = this.format;

      hues.forEach(function (hue) {
        var levels = [hue ? (mainColor.hue() + hue) % 360 : mainColor.hue(), mainColor.saturationv(), mainColor.value(), mainColor.alpha()];

        colors.push(new ColorItem(levels, format));
      });

      return colors;
    }
  }, {
    key: 'hue',
    get: function get() {
      return this._color.hue();
    }

    /**
     * Saturation value from 0 to 100
     *
     * @returns {int}
     */
    ,


    /**
     * Sets the hue value
     *
     * @param {int} value Integer from 0 to 360
     */
    set: function set(value) {
      this._color = this._color.hue(value);
    }
  }, {
    key: 'saturation',
    get: function get() {
      return this._color.saturationv();
    }

    /**
     * Value channel value from 0 to 100
     *
     * @returns {int}
     */
    ,
    set: function set(value) {
      this._color = this._color.saturationv(value);
    }
  }, {
    key: 'value',
    get: function get() {
      return this._color.value();
    }

    /**
     * Alpha value from 0.0 to 1.0
     *
     * @returns {number}
     */
    ,
    set: function set(value) {
      this._color = this._color.value(value);
    }
  }, {
    key: 'alpha',
    get: function get() {
      var a = this._color.alpha();

      return isNaN(a) ? 1 : a;
    }

    /**
     * Default color format to convert to when calling toString() or string()
     *
     * @returns {String} 'rgb', 'hsl', 'hex' or ''
     */
    ,
    set: function set(value) {
      // 2 decimals max
      this._color = this._color.alpha(Math.round(value * 100) / 100);
    }
  }, {
    key: 'format',
    get: function get() {
      return this._format ? this._format : this._color.model;
    },
    set: function set(value) {
      this._format = ColorItem.sanitizeFormat(value);
    }
  }], [{
    key: 'parse',
    value: function parse(color) {
      if (color instanceof _color2.default) {
        return color;
      }

      if (color instanceof ColorItem) {
        return color._color;
      }

      var format = null;

      if (color instanceof HSVAColor) {
        color = [color.h, color.s, color.v, isNaN(color.a) ? 1 : color.a];
      } else {
        color = ColorItem.sanitizeString(color);
      }

      if (color === null) {
        return null;
      }

      if (Array.isArray(color)) {
        format = 'hsv';
      }

      try {
        return (0, _color2.default)(color, format);
      } catch (e) {
        return null;
      }
    }

    /**
     * Sanitizes a color string, adding missing hash to hexadecimal colors
     * and converting 'transparent' to a color code.
     *
     * @param {String|*} str Color string
     * @example let colorStr = ColorItem.sanitizeString('ffaa00');
     * @static
     * @returns {String|*}
     */

  }, {
    key: 'sanitizeString',
    value: function sanitizeString(str) {
      if (!(typeof str === 'string' || str instanceof String)) {
        return str;
      }

      if (str.match(/^[0-9a-f]{2,}$/i)) {
        return '#' + str;
      }

      if (str.toLowerCase() === 'transparent') {
        return '#FFFFFF00';
      }

      return str;
    }

    /**
     * Detects if a value is a string and a color in hexadecimal format (in any variant).
     *
     * @param {String} str
     * @example ColorItem.isHex('rgba(0,0,0)'); // false
     * @example ColorItem.isHex('ffaa00'); // true
     * @example ColorItem.isHex('#ffaa00'); // true
     * @static
     * @returns {boolean}
     */

  }, {
    key: 'isHex',
    value: function isHex(str) {
      if (!(typeof str === 'string' || str instanceof String)) {
        return false;
      }

      return !!str.match(/^#?[0-9a-f]{2,}$/i);
    }

    /**
     * Sanitizes a color format to one supported by web browsers.
     * Returns an empty string of the format can't be recognised.
     *
     * @param {String|*} format
     * @example ColorItem.sanitizeFormat('rgba'); // 'rgb'
     * @example ColorItem.isHex('hex8'); // 'hex'
     * @example ColorItem.isHex('invalid'); // ''
     * @static
     * @returns {String} 'rgb', 'hsl', 'hex' or ''.
     */

  }, {
    key: 'sanitizeFormat',
    value: function sanitizeFormat(format) {
      switch (format) {
        case 'hex':
        case 'hex3':
        case 'hex4':
        case 'hex6':
        case 'hex8':
          return 'hex';
        case 'rgb':
        case 'rgba':
        case 'keyword':
        case 'name':
          return 'rgb';
        case 'hsl':
        case 'hsla':
        case 'hsv':
        case 'hsva':
        case 'hwb': // HWB this is supported by Qix Color, but not by browsers
        case 'hwba':
          return 'hsl';
        default:
          return '';
      }
    }
  }]);

  return ColorItem;
}();

/**
 * List of hue-based color formulas used by ColorItem.prototype.generate()
 *
 * @static
 * @type {{complementary: number[], triad: number[], tetrad: number[], splitcomplement: number[]}}
 */


ColorItem.colorFormulas = {
  complementary: [180],
  triad: [0, 120, 240],
  tetrad: [0, 90, 180, 270],
  splitcomplement: [0, 72, 216]
};

exports.default = ColorItem;
exports.HSVAColor = HSVAColor;
exports.ColorItem = ColorItem;

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/**
 * @module
 */

// adjust these values accordingly to the sass vars

Object.defineProperty(exports, "__esModule", {
  value: true
});
var sassVars = {
  'bar_size_short': 16,
  'base_margin': 6,
  'columns': 6
};

var sliderSize = sassVars.bar_size_short * sassVars.columns + sassVars.base_margin * (sassVars.columns - 1);

/**
 * Colorpicker default options
 */
exports.default = {
  /**
   * Custom class to be added to the `.colorpicker-element` element
   *
   * @type {String|null}
   * @default null
   */
  customClass: null,
  /**
   * Sets a initial color, ignoring the one from the element/input value or the data-color attribute.
   *
   * @type {(String|ColorItem|boolean)}
   * @default false
   */
  color: false,
  /**
   * Fallback color to use when the given color is invalid.
   * If false, the latest valid color will be used as a fallback.
   *
   * @type {String|ColorItem|boolean}
   * @default false
   */
  fallbackColor: false,
  /**
   * Forces an specific color format. If 'auto', it will be automatically detected the first time only,
   * but if null it will be always recalculated.
   *
   * Note that the ending 'a' of the format meaning "alpha" has currently no effect, meaning that rgb is the same as
   * rgba excepting if the alpha channel is disabled (see useAlpha).
   *
   * @type {('rgb'|'hex'|'hsl'|'auto'|null)}
   * @default 'auto'
   */
  format: 'auto',
  /**
   * Horizontal mode layout.
   *
   * If true, the hue and alpha channel bars will be rendered horizontally, above the saturation selector.
   *
   * @type {boolean}
   * @default false
   */
  horizontal: false,
  /**
   * Forces to show the colorpicker as an inline element.
   *
   * Note that if there is no container specified, the inline element
   * will be added to the body, so you may want to set the container option.
   *
   * @type {boolean}
   * @default false
   */
  inline: false,
  /**
   * Container where the colorpicker is appended to in the DOM.
   *
   * If is a string (CSS selector), the colorpicker will be placed inside this container.
   * If true, the `.colorpicker-element` element itself will be used as the container.
   * If false, the document body is used as the container, unless it is a popover (in this case it is appended to the
   * popover body instead).
   *
   * @type {String|boolean}
   * @default false
   */
  container: false,
  /**
   * Bootstrap Popover options.
   * The trigger, content and html options are always ignored.
   *
   * @type {boolean}
   * @default Object
   */
  popover: {
    animation: true,
    placement: 'bottom',
    fallbackPlacement: 'flip'
  },
  /**
   * If true, loads the 'debugger' extension automatically, which logs the events in the console
   * @type {boolean}
   * @default false
   */
  debug: false,
  /**
   * Child CSS selector for the colorpicker input.
   *
   * @type {String}
   * @default 'input'
   */
  input: 'input',
  /**
   * Child CSS selector for the colorpicker addon.
   * If it exists, the child <i> element background will be changed on color change.
   *
   * @type {String}
   * @default '.colorpicker-trigger, .colorpicker-input-addon'
   */
  addon: '.colorpicker-input-addon',
  /**
   * If true, the input content will be replaced always with a valid color,
   * if false, the invalid color will be left in the input,
   *   while the internal color object will still resolve into a valid one.
   *
   * @type {boolean}
   * @default true
   */
  autoInputFallback: true,
  /**
   * If true a hash will be prepended to hexadecimal colors.
   * If false, the hash will be removed.
   * This only affects the input values in hexadecimal format.
   *
   * @type {boolean}
   * @default true
   */
  useHashPrefix: true,
  /**
   * If true, the alpha channel bar will be displayed no matter what.
   *
   * If false, it will be always hidden and alpha channel will be disabled also programmatically, meaning that
   * the selected or typed color will be always opaque.
   *
   * If null, the alpha channel will be automatically disabled/enabled depending if the initial color format supports
   * alpha or not.
   *
   * @type {boolean}
   * @default true
   */
  useAlpha: true,
  /**
   * Colorpicker widget template
   * @type {String}
   * @example
   * <!-- This is the default template: -->
   * <div class="colorpicker">
   *   <div class="colorpicker-saturation"><i class="colorpicker-guide"></i></div>
   *   <div class="colorpicker-hue"><i class="colorpicker-guide"></i></div>
   *   <div class="colorpicker-alpha">
   *     <div class="colorpicker-alpha-color"></div>
   *     <i class="colorpicker-guide"></i>
   *   </div>
   * </div>
   */
  template: '<div class="colorpicker">\n      <div class="colorpicker-saturation"><i class="colorpicker-guide"></i></div>\n      <div class="colorpicker-hue"><i class="colorpicker-guide"></i></div>\n      <div class="colorpicker-alpha">\n        <div class="colorpicker-alpha-color"></div>\n        <i class="colorpicker-guide"></i>\n      </div>\n    </div>',
  /**
   *
   * Associative object with the extension class name and its config.
   * Colorpicker comes with many bundled extensions: debugger, palette, preview and swatches (a superset of palette).
   *
   * @type {Object[]}
   * @example
   *   extensions: [
   *     {
   *       name: 'swatches'
   *       options: {
   *         colors: {
   *           'primary': '#337ab7',
   *           'success': '#5cb85c',
   *           'info': '#5bc0de',
   *           'warning': '#f0ad4e',
   *           'danger': '#d9534f'
   *         },
   *         namesAsValues: true
   *       }
   *     }
   *   ]
   */
  extensions: [{
    name: 'preview',
    options: {
      showText: true
    }
  }],
  /**
   * Vertical sliders configuration
   * @type {Object}
   */
  sliders: {
    saturation: {
      selector: '.colorpicker-saturation',
      maxLeft: sliderSize,
      maxTop: sliderSize,
      callLeft: 'setSaturationRatio',
      callTop: 'setValueRatio'
    },
    hue: {
      selector: '.colorpicker-hue',
      maxLeft: 0,
      maxTop: sliderSize,
      callLeft: false,
      callTop: 'setHueRatio'
    },
    alpha: {
      selector: '.colorpicker-alpha',
      childSelector: '.colorpicker-alpha-color',
      maxLeft: 0,
      maxTop: sliderSize,
      callLeft: false,
      callTop: 'setAlphaRatio'
    }
  },
  /**
   * Horizontal sliders configuration
   * @type {Object}
   */
  slidersHorz: {
    saturation: {
      selector: '.colorpicker-saturation',
      maxLeft: sliderSize,
      maxTop: sliderSize,
      callLeft: 'setSaturationRatio',
      callTop: 'setValueRatio'
    },
    hue: {
      selector: '.colorpicker-hue',
      maxLeft: sliderSize,
      maxTop: 0,
      callLeft: 'setHueRatio',
      callTop: false
    },
    alpha: {
      selector: '.colorpicker-alpha',
      childSelector: '.colorpicker-alpha-color',
      maxLeft: sliderSize,
      maxTop: 0,
      callLeft: 'setAlphaRatio',
      callTop: false
    }
  }
};
module.exports = exports.default;

/***/ }),
/* 4 */
/***/ (function(module, exports, __nested_webpack_require_37509__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Extension2 = __nested_webpack_require_37509__(1);

var _Extension3 = _interopRequireDefault(_Extension2);

var _jquery = __nested_webpack_require_37509__(0);

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var defaults = {
  /**
   * Key-value pairs defining a color alias and its CSS color representation.
   *
   * They can also be just an array of values. In that case, no special names are used, only the real colors.
   *
   * @type {Object|Array}
   * @default null
   * @example
   *  {
   *   'black': '#000000',
   *   'white': '#ffffff',
   *   'red': '#FF0000',
   *   'default': '#777777',
   *   'primary': '#337ab7',
   *   'success': '#5cb85c',
   *   'info': '#5bc0de',
   *   'warning': '#f0ad4e',
   *   'danger': '#d9534f'
   *  }
   *
   * @example ['#f0ad4e', '#337ab7', '#5cb85c']
   */
  colors: null,
  /**
   * If true, when a color swatch is selected the name (alias) will be used as input value,
   * otherwise the swatch real color value will be used.
   *
   * @type {boolean}
   * @default true
   */
  namesAsValues: true
};

/**
 * Palette extension
 * @ignore
 */

var Palette = function (_Extension) {
  _inherits(Palette, _Extension);

  _createClass(Palette, [{
    key: 'colors',


    /**
     * @returns {Object|Array}
     */
    get: function get() {
      return this.options.colors;
    }
  }]);

  function Palette(colorpicker) {
    var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

    _classCallCheck(this, Palette);

    var _this = _possibleConstructorReturn(this, (Palette.__proto__ || Object.getPrototypeOf(Palette)).call(this, colorpicker, _jquery2.default.extend(true, {}, defaults, options)));

    if (!Array.isArray(_this.options.colors) && _typeof(_this.options.colors) !== 'object') {
      _this.options.colors = null;
    }
    return _this;
  }

  /**
   * @returns {int}
   */


  _createClass(Palette, [{
    key: 'getLength',
    value: function getLength() {
      if (!this.options.colors) {
        return 0;
      }

      if (Array.isArray(this.options.colors)) {
        return this.options.colors.length;
      }

      if (_typeof(this.options.colors) === 'object') {
        return Object.keys(this.options.colors).length;
      }

      return 0;
    }
  }, {
    key: 'resolveColor',
    value: function resolveColor(color) {
      var realColor = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

      if (this.getLength() <= 0) {
        return false;
      }

      // Array of colors
      if (Array.isArray(this.options.colors)) {
        if (this.options.colors.indexOf(color) >= 0) {
          return color;
        }
        if (this.options.colors.indexOf(color.toUpperCase()) >= 0) {
          return color.toUpperCase();
        }
        if (this.options.colors.indexOf(color.toLowerCase()) >= 0) {
          return color.toLowerCase();
        }
        return false;
      }

      if (_typeof(this.options.colors) !== 'object') {
        return false;
      }

      // Map of objects
      if (!this.options.namesAsValues || realColor) {
        return this.getValue(color, false);
      }
      return this.getName(color, this.getName('#' + color));
    }

    /**
     * Given a color value, returns the corresponding color name or defaultValue.
     *
     * @param {String} value
     * @param {*} defaultValue
     * @returns {*}
     */

  }, {
    key: 'getName',
    value: function getName(value) {
      var defaultValue = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

      if (!(typeof value === 'string') || !this.options.colors) {
        return defaultValue;
      }
      for (var name in this.options.colors) {
        if (!this.options.colors.hasOwnProperty(name)) {
          continue;
        }
        if (this.options.colors[name].toLowerCase() === value.toLowerCase()) {
          return name;
        }
      }
      return defaultValue;
    }

    /**
     * Given a color name, returns the corresponding color value or defaultValue.
     *
     * @param {String} name
     * @param {*} defaultValue
     * @returns {*}
     */

  }, {
    key: 'getValue',
    value: function getValue(name) {
      var defaultValue = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

      if (!(typeof name === 'string') || !this.options.colors) {
        return defaultValue;
      }
      if (this.options.colors.hasOwnProperty(name)) {
        return this.options.colors[name];
      }
      return defaultValue;
    }
  }]);

  return Palette;
}(_Extension3.default);

exports.default = Palette;
module.exports = exports.default;

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = {
	"aliceblue": [240, 248, 255],
	"antiquewhite": [250, 235, 215],
	"aqua": [0, 255, 255],
	"aquamarine": [127, 255, 212],
	"azure": [240, 255, 255],
	"beige": [245, 245, 220],
	"bisque": [255, 228, 196],
	"black": [0, 0, 0],
	"blanchedalmond": [255, 235, 205],
	"blue": [0, 0, 255],
	"blueviolet": [138, 43, 226],
	"brown": [165, 42, 42],
	"burlywood": [222, 184, 135],
	"cadetblue": [95, 158, 160],
	"chartreuse": [127, 255, 0],
	"chocolate": [210, 105, 30],
	"coral": [255, 127, 80],
	"cornflowerblue": [100, 149, 237],
	"cornsilk": [255, 248, 220],
	"crimson": [220, 20, 60],
	"cyan": [0, 255, 255],
	"darkblue": [0, 0, 139],
	"darkcyan": [0, 139, 139],
	"darkgoldenrod": [184, 134, 11],
	"darkgray": [169, 169, 169],
	"darkgreen": [0, 100, 0],
	"darkgrey": [169, 169, 169],
	"darkkhaki": [189, 183, 107],
	"darkmagenta": [139, 0, 139],
	"darkolivegreen": [85, 107, 47],
	"darkorange": [255, 140, 0],
	"darkorchid": [153, 50, 204],
	"darkred": [139, 0, 0],
	"darksalmon": [233, 150, 122],
	"darkseagreen": [143, 188, 143],
	"darkslateblue": [72, 61, 139],
	"darkslategray": [47, 79, 79],
	"darkslategrey": [47, 79, 79],
	"darkturquoise": [0, 206, 209],
	"darkviolet": [148, 0, 211],
	"deeppink": [255, 20, 147],
	"deepskyblue": [0, 191, 255],
	"dimgray": [105, 105, 105],
	"dimgrey": [105, 105, 105],
	"dodgerblue": [30, 144, 255],
	"firebrick": [178, 34, 34],
	"floralwhite": [255, 250, 240],
	"forestgreen": [34, 139, 34],
	"fuchsia": [255, 0, 255],
	"gainsboro": [220, 220, 220],
	"ghostwhite": [248, 248, 255],
	"gold": [255, 215, 0],
	"goldenrod": [218, 165, 32],
	"gray": [128, 128, 128],
	"green": [0, 128, 0],
	"greenyellow": [173, 255, 47],
	"grey": [128, 128, 128],
	"honeydew": [240, 255, 240],
	"hotpink": [255, 105, 180],
	"indianred": [205, 92, 92],
	"indigo": [75, 0, 130],
	"ivory": [255, 255, 240],
	"khaki": [240, 230, 140],
	"lavender": [230, 230, 250],
	"lavenderblush": [255, 240, 245],
	"lawngreen": [124, 252, 0],
	"lemonchiffon": [255, 250, 205],
	"lightblue": [173, 216, 230],
	"lightcoral": [240, 128, 128],
	"lightcyan": [224, 255, 255],
	"lightgoldenrodyellow": [250, 250, 210],
	"lightgray": [211, 211, 211],
	"lightgreen": [144, 238, 144],
	"lightgrey": [211, 211, 211],
	"lightpink": [255, 182, 193],
	"lightsalmon": [255, 160, 122],
	"lightseagreen": [32, 178, 170],
	"lightskyblue": [135, 206, 250],
	"lightslategray": [119, 136, 153],
	"lightslategrey": [119, 136, 153],
	"lightsteelblue": [176, 196, 222],
	"lightyellow": [255, 255, 224],
	"lime": [0, 255, 0],
	"limegreen": [50, 205, 50],
	"linen": [250, 240, 230],
	"magenta": [255, 0, 255],
	"maroon": [128, 0, 0],
	"mediumaquamarine": [102, 205, 170],
	"mediumblue": [0, 0, 205],
	"mediumorchid": [186, 85, 211],
	"mediumpurple": [147, 112, 219],
	"mediumseagreen": [60, 179, 113],
	"mediumslateblue": [123, 104, 238],
	"mediumspringgreen": [0, 250, 154],
	"mediumturquoise": [72, 209, 204],
	"mediumvioletred": [199, 21, 133],
	"midnightblue": [25, 25, 112],
	"mintcream": [245, 255, 250],
	"mistyrose": [255, 228, 225],
	"moccasin": [255, 228, 181],
	"navajowhite": [255, 222, 173],
	"navy": [0, 0, 128],
	"oldlace": [253, 245, 230],
	"olive": [128, 128, 0],
	"olivedrab": [107, 142, 35],
	"orange": [255, 165, 0],
	"orangered": [255, 69, 0],
	"orchid": [218, 112, 214],
	"palegoldenrod": [238, 232, 170],
	"palegreen": [152, 251, 152],
	"paleturquoise": [175, 238, 238],
	"palevioletred": [219, 112, 147],
	"papayawhip": [255, 239, 213],
	"peachpuff": [255, 218, 185],
	"peru": [205, 133, 63],
	"pink": [255, 192, 203],
	"plum": [221, 160, 221],
	"powderblue": [176, 224, 230],
	"purple": [128, 0, 128],
	"rebeccapurple": [102, 51, 153],
	"red": [255, 0, 0],
	"rosybrown": [188, 143, 143],
	"royalblue": [65, 105, 225],
	"saddlebrown": [139, 69, 19],
	"salmon": [250, 128, 114],
	"sandybrown": [244, 164, 96],
	"seagreen": [46, 139, 87],
	"seashell": [255, 245, 238],
	"sienna": [160, 82, 45],
	"silver": [192, 192, 192],
	"skyblue": [135, 206, 235],
	"slateblue": [106, 90, 205],
	"slategray": [112, 128, 144],
	"slategrey": [112, 128, 144],
	"snow": [255, 250, 250],
	"springgreen": [0, 255, 127],
	"steelblue": [70, 130, 180],
	"tan": [210, 180, 140],
	"teal": [0, 128, 128],
	"thistle": [216, 191, 216],
	"tomato": [255, 99, 71],
	"turquoise": [64, 224, 208],
	"violet": [238, 130, 238],
	"wheat": [245, 222, 179],
	"white": [255, 255, 255],
	"whitesmoke": [245, 245, 245],
	"yellow": [255, 255, 0],
	"yellowgreen": [154, 205, 50]
};


/***/ }),
/* 6 */
/***/ (function(module, exports, __nested_webpack_require_48826__) {

/* MIT license */
var cssKeywords = __nested_webpack_require_48826__(5);

// NOTE: conversions should only return primitive values (i.e. arrays, or
//       values that give correct `typeof` results).
//       do not use box values types (i.e. Number(), String(), etc.)

var reverseKeywords = {};
for (var key in cssKeywords) {
	if (cssKeywords.hasOwnProperty(key)) {
		reverseKeywords[cssKeywords[key]] = key;
	}
}

var convert = module.exports = {
	rgb: {channels: 3, labels: 'rgb'},
	hsl: {channels: 3, labels: 'hsl'},
	hsv: {channels: 3, labels: 'hsv'},
	hwb: {channels: 3, labels: 'hwb'},
	cmyk: {channels: 4, labels: 'cmyk'},
	xyz: {channels: 3, labels: 'xyz'},
	lab: {channels: 3, labels: 'lab'},
	lch: {channels: 3, labels: 'lch'},
	hex: {channels: 1, labels: ['hex']},
	keyword: {channels: 1, labels: ['keyword']},
	ansi16: {channels: 1, labels: ['ansi16']},
	ansi256: {channels: 1, labels: ['ansi256']},
	hcg: {channels: 3, labels: ['h', 'c', 'g']},
	apple: {channels: 3, labels: ['r16', 'g16', 'b16']},
	gray: {channels: 1, labels: ['gray']}
};

// hide .channels and .labels properties
for (var model in convert) {
	if (convert.hasOwnProperty(model)) {
		if (!('channels' in convert[model])) {
			throw new Error('missing channels property: ' + model);
		}

		if (!('labels' in convert[model])) {
			throw new Error('missing channel labels property: ' + model);
		}

		if (convert[model].labels.length !== convert[model].channels) {
			throw new Error('channel and label counts mismatch: ' + model);
		}

		var channels = convert[model].channels;
		var labels = convert[model].labels;
		delete convert[model].channels;
		delete convert[model].labels;
		Object.defineProperty(convert[model], 'channels', {value: channels});
		Object.defineProperty(convert[model], 'labels', {value: labels});
	}
}

convert.rgb.hsl = function (rgb) {
	var r = rgb[0] / 255;
	var g = rgb[1] / 255;
	var b = rgb[2] / 255;
	var min = Math.min(r, g, b);
	var max = Math.max(r, g, b);
	var delta = max - min;
	var h;
	var s;
	var l;

	if (max === min) {
		h = 0;
	} else if (r === max) {
		h = (g - b) / delta;
	} else if (g === max) {
		h = 2 + (b - r) / delta;
	} else if (b === max) {
		h = 4 + (r - g) / delta;
	}

	h = Math.min(h * 60, 360);

	if (h < 0) {
		h += 360;
	}

	l = (min + max) / 2;

	if (max === min) {
		s = 0;
	} else if (l <= 0.5) {
		s = delta / (max + min);
	} else {
		s = delta / (2 - max - min);
	}

	return [h, s * 100, l * 100];
};

convert.rgb.hsv = function (rgb) {
	var rdif;
	var gdif;
	var bdif;
	var h;
	var s;

	var r = rgb[0] / 255;
	var g = rgb[1] / 255;
	var b = rgb[2] / 255;
	var v = Math.max(r, g, b);
	var diff = v - Math.min(r, g, b);
	var diffc = function (c) {
		return (v - c) / 6 / diff + 1 / 2;
	};

	if (diff === 0) {
		h = s = 0;
	} else {
		s = diff / v;
		rdif = diffc(r);
		gdif = diffc(g);
		bdif = diffc(b);

		if (r === v) {
			h = bdif - gdif;
		} else if (g === v) {
			h = (1 / 3) + rdif - bdif;
		} else if (b === v) {
			h = (2 / 3) + gdif - rdif;
		}
		if (h < 0) {
			h += 1;
		} else if (h > 1) {
			h -= 1;
		}
	}

	return [
		h * 360,
		s * 100,
		v * 100
	];
};

convert.rgb.hwb = function (rgb) {
	var r = rgb[0];
	var g = rgb[1];
	var b = rgb[2];
	var h = convert.rgb.hsl(rgb)[0];
	var w = 1 / 255 * Math.min(r, Math.min(g, b));

	b = 1 - 1 / 255 * Math.max(r, Math.max(g, b));

	return [h, w * 100, b * 100];
};

convert.rgb.cmyk = function (rgb) {
	var r = rgb[0] / 255;
	var g = rgb[1] / 255;
	var b = rgb[2] / 255;
	var c;
	var m;
	var y;
	var k;

	k = Math.min(1 - r, 1 - g, 1 - b);
	c = (1 - r - k) / (1 - k) || 0;
	m = (1 - g - k) / (1 - k) || 0;
	y = (1 - b - k) / (1 - k) || 0;

	return [c * 100, m * 100, y * 100, k * 100];
};

/**
 * See https://en.m.wikipedia.org/wiki/Euclidean_distance#Squared_Euclidean_distance
 * */
function comparativeDistance(x, y) {
	return (
		Math.pow(x[0] - y[0], 2) +
		Math.pow(x[1] - y[1], 2) +
		Math.pow(x[2] - y[2], 2)
	);
}

convert.rgb.keyword = function (rgb) {
	var reversed = reverseKeywords[rgb];
	if (reversed) {
		return reversed;
	}

	var currentClosestDistance = Infinity;
	var currentClosestKeyword;

	for (var keyword in cssKeywords) {
		if (cssKeywords.hasOwnProperty(keyword)) {
			var value = cssKeywords[keyword];

			// Compute comparative distance
			var distance = comparativeDistance(rgb, value);

			// Check if its less, if so set as closest
			if (distance < currentClosestDistance) {
				currentClosestDistance = distance;
				currentClosestKeyword = keyword;
			}
		}
	}

	return currentClosestKeyword;
};

convert.keyword.rgb = function (keyword) {
	return cssKeywords[keyword];
};

convert.rgb.xyz = function (rgb) {
	var r = rgb[0] / 255;
	var g = rgb[1] / 255;
	var b = rgb[2] / 255;

	// assume sRGB
	r = r > 0.04045 ? Math.pow(((r + 0.055) / 1.055), 2.4) : (r / 12.92);
	g = g > 0.04045 ? Math.pow(((g + 0.055) / 1.055), 2.4) : (g / 12.92);
	b = b > 0.04045 ? Math.pow(((b + 0.055) / 1.055), 2.4) : (b / 12.92);

	var x = (r * 0.4124) + (g * 0.3576) + (b * 0.1805);
	var y = (r * 0.2126) + (g * 0.7152) + (b * 0.0722);
	var z = (r * 0.0193) + (g * 0.1192) + (b * 0.9505);

	return [x * 100, y * 100, z * 100];
};

convert.rgb.lab = function (rgb) {
	var xyz = convert.rgb.xyz(rgb);
	var x = xyz[0];
	var y = xyz[1];
	var z = xyz[2];
	var l;
	var a;
	var b;

	x /= 95.047;
	y /= 100;
	z /= 108.883;

	x = x > 0.008856 ? Math.pow(x, 1 / 3) : (7.787 * x) + (16 / 116);
	y = y > 0.008856 ? Math.pow(y, 1 / 3) : (7.787 * y) + (16 / 116);
	z = z > 0.008856 ? Math.pow(z, 1 / 3) : (7.787 * z) + (16 / 116);

	l = (116 * y) - 16;
	a = 500 * (x - y);
	b = 200 * (y - z);

	return [l, a, b];
};

convert.hsl.rgb = function (hsl) {
	var h = hsl[0] / 360;
	var s = hsl[1] / 100;
	var l = hsl[2] / 100;
	var t1;
	var t2;
	var t3;
	var rgb;
	var val;

	if (s === 0) {
		val = l * 255;
		return [val, val, val];
	}

	if (l < 0.5) {
		t2 = l * (1 + s);
	} else {
		t2 = l + s - l * s;
	}

	t1 = 2 * l - t2;

	rgb = [0, 0, 0];
	for (var i = 0; i < 3; i++) {
		t3 = h + 1 / 3 * -(i - 1);
		if (t3 < 0) {
			t3++;
		}
		if (t3 > 1) {
			t3--;
		}

		if (6 * t3 < 1) {
			val = t1 + (t2 - t1) * 6 * t3;
		} else if (2 * t3 < 1) {
			val = t2;
		} else if (3 * t3 < 2) {
			val = t1 + (t2 - t1) * (2 / 3 - t3) * 6;
		} else {
			val = t1;
		}

		rgb[i] = val * 255;
	}

	return rgb;
};

convert.hsl.hsv = function (hsl) {
	var h = hsl[0];
	var s = hsl[1] / 100;
	var l = hsl[2] / 100;
	var smin = s;
	var lmin = Math.max(l, 0.01);
	var sv;
	var v;

	l *= 2;
	s *= (l <= 1) ? l : 2 - l;
	smin *= lmin <= 1 ? lmin : 2 - lmin;
	v = (l + s) / 2;
	sv = l === 0 ? (2 * smin) / (lmin + smin) : (2 * s) / (l + s);

	return [h, sv * 100, v * 100];
};

convert.hsv.rgb = function (hsv) {
	var h = hsv[0] / 60;
	var s = hsv[1] / 100;
	var v = hsv[2] / 100;
	var hi = Math.floor(h) % 6;

	var f = h - Math.floor(h);
	var p = 255 * v * (1 - s);
	var q = 255 * v * (1 - (s * f));
	var t = 255 * v * (1 - (s * (1 - f)));
	v *= 255;

	switch (hi) {
		case 0:
			return [v, t, p];
		case 1:
			return [q, v, p];
		case 2:
			return [p, v, t];
		case 3:
			return [p, q, v];
		case 4:
			return [t, p, v];
		case 5:
			return [v, p, q];
	}
};

convert.hsv.hsl = function (hsv) {
	var h = hsv[0];
	var s = hsv[1] / 100;
	var v = hsv[2] / 100;
	var vmin = Math.max(v, 0.01);
	var lmin;
	var sl;
	var l;

	l = (2 - s) * v;
	lmin = (2 - s) * vmin;
	sl = s * vmin;
	sl /= (lmin <= 1) ? lmin : 2 - lmin;
	sl = sl || 0;
	l /= 2;

	return [h, sl * 100, l * 100];
};

// http://dev.w3.org/csswg/css-color/#hwb-to-rgb
convert.hwb.rgb = function (hwb) {
	var h = hwb[0] / 360;
	var wh = hwb[1] / 100;
	var bl = hwb[2] / 100;
	var ratio = wh + bl;
	var i;
	var v;
	var f;
	var n;

	// wh + bl cant be > 1
	if (ratio > 1) {
		wh /= ratio;
		bl /= ratio;
	}

	i = Math.floor(6 * h);
	v = 1 - bl;
	f = 6 * h - i;

	if ((i & 0x01) !== 0) {
		f = 1 - f;
	}

	n = wh + f * (v - wh); // linear interpolation

	var r;
	var g;
	var b;
	switch (i) {
		default:
		case 6:
		case 0: r = v; g = n; b = wh; break;
		case 1: r = n; g = v; b = wh; break;
		case 2: r = wh; g = v; b = n; break;
		case 3: r = wh; g = n; b = v; break;
		case 4: r = n; g = wh; b = v; break;
		case 5: r = v; g = wh; b = n; break;
	}

	return [r * 255, g * 255, b * 255];
};

convert.cmyk.rgb = function (cmyk) {
	var c = cmyk[0] / 100;
	var m = cmyk[1] / 100;
	var y = cmyk[2] / 100;
	var k = cmyk[3] / 100;
	var r;
	var g;
	var b;

	r = 1 - Math.min(1, c * (1 - k) + k);
	g = 1 - Math.min(1, m * (1 - k) + k);
	b = 1 - Math.min(1, y * (1 - k) + k);

	return [r * 255, g * 255, b * 255];
};

convert.xyz.rgb = function (xyz) {
	var x = xyz[0] / 100;
	var y = xyz[1] / 100;
	var z = xyz[2] / 100;
	var r;
	var g;
	var b;

	r = (x * 3.2406) + (y * -1.5372) + (z * -0.4986);
	g = (x * -0.9689) + (y * 1.8758) + (z * 0.0415);
	b = (x * 0.0557) + (y * -0.2040) + (z * 1.0570);

	// assume sRGB
	r = r > 0.0031308
		? ((1.055 * Math.pow(r, 1.0 / 2.4)) - 0.055)
		: r * 12.92;

	g = g > 0.0031308
		? ((1.055 * Math.pow(g, 1.0 / 2.4)) - 0.055)
		: g * 12.92;

	b = b > 0.0031308
		? ((1.055 * Math.pow(b, 1.0 / 2.4)) - 0.055)
		: b * 12.92;

	r = Math.min(Math.max(0, r), 1);
	g = Math.min(Math.max(0, g), 1);
	b = Math.min(Math.max(0, b), 1);

	return [r * 255, g * 255, b * 255];
};

convert.xyz.lab = function (xyz) {
	var x = xyz[0];
	var y = xyz[1];
	var z = xyz[2];
	var l;
	var a;
	var b;

	x /= 95.047;
	y /= 100;
	z /= 108.883;

	x = x > 0.008856 ? Math.pow(x, 1 / 3) : (7.787 * x) + (16 / 116);
	y = y > 0.008856 ? Math.pow(y, 1 / 3) : (7.787 * y) + (16 / 116);
	z = z > 0.008856 ? Math.pow(z, 1 / 3) : (7.787 * z) + (16 / 116);

	l = (116 * y) - 16;
	a = 500 * (x - y);
	b = 200 * (y - z);

	return [l, a, b];
};

convert.lab.xyz = function (lab) {
	var l = lab[0];
	var a = lab[1];
	var b = lab[2];
	var x;
	var y;
	var z;

	y = (l + 16) / 116;
	x = a / 500 + y;
	z = y - b / 200;

	var y2 = Math.pow(y, 3);
	var x2 = Math.pow(x, 3);
	var z2 = Math.pow(z, 3);
	y = y2 > 0.008856 ? y2 : (y - 16 / 116) / 7.787;
	x = x2 > 0.008856 ? x2 : (x - 16 / 116) / 7.787;
	z = z2 > 0.008856 ? z2 : (z - 16 / 116) / 7.787;

	x *= 95.047;
	y *= 100;
	z *= 108.883;

	return [x, y, z];
};

convert.lab.lch = function (lab) {
	var l = lab[0];
	var a = lab[1];
	var b = lab[2];
	var hr;
	var h;
	var c;

	hr = Math.atan2(b, a);
	h = hr * 360 / 2 / Math.PI;

	if (h < 0) {
		h += 360;
	}

	c = Math.sqrt(a * a + b * b);

	return [l, c, h];
};

convert.lch.lab = function (lch) {
	var l = lch[0];
	var c = lch[1];
	var h = lch[2];
	var a;
	var b;
	var hr;

	hr = h / 360 * 2 * Math.PI;
	a = c * Math.cos(hr);
	b = c * Math.sin(hr);

	return [l, a, b];
};

convert.rgb.ansi16 = function (args) {
	var r = args[0];
	var g = args[1];
	var b = args[2];
	var value = 1 in arguments ? arguments[1] : convert.rgb.hsv(args)[2]; // hsv -> ansi16 optimization

	value = Math.round(value / 50);

	if (value === 0) {
		return 30;
	}

	var ansi = 30
		+ ((Math.round(b / 255) << 2)
		| (Math.round(g / 255) << 1)
		| Math.round(r / 255));

	if (value === 2) {
		ansi += 60;
	}

	return ansi;
};

convert.hsv.ansi16 = function (args) {
	// optimization here; we already know the value and don't need to get
	// it converted for us.
	return convert.rgb.ansi16(convert.hsv.rgb(args), args[2]);
};

convert.rgb.ansi256 = function (args) {
	var r = args[0];
	var g = args[1];
	var b = args[2];

	// we use the extended greyscale palette here, with the exception of
	// black and white. normal palette only has 4 greyscale shades.
	if (r === g && g === b) {
		if (r < 8) {
			return 16;
		}

		if (r > 248) {
			return 231;
		}

		return Math.round(((r - 8) / 247) * 24) + 232;
	}

	var ansi = 16
		+ (36 * Math.round(r / 255 * 5))
		+ (6 * Math.round(g / 255 * 5))
		+ Math.round(b / 255 * 5);

	return ansi;
};

convert.ansi16.rgb = function (args) {
	var color = args % 10;

	// handle greyscale
	if (color === 0 || color === 7) {
		if (args > 50) {
			color += 3.5;
		}

		color = color / 10.5 * 255;

		return [color, color, color];
	}

	var mult = (~~(args > 50) + 1) * 0.5;
	var r = ((color & 1) * mult) * 255;
	var g = (((color >> 1) & 1) * mult) * 255;
	var b = (((color >> 2) & 1) * mult) * 255;

	return [r, g, b];
};

convert.ansi256.rgb = function (args) {
	// handle greyscale
	if (args >= 232) {
		var c = (args - 232) * 10 + 8;
		return [c, c, c];
	}

	args -= 16;

	var rem;
	var r = Math.floor(args / 36) / 5 * 255;
	var g = Math.floor((rem = args % 36) / 6) / 5 * 255;
	var b = (rem % 6) / 5 * 255;

	return [r, g, b];
};

convert.rgb.hex = function (args) {
	var integer = ((Math.round(args[0]) & 0xFF) << 16)
		+ ((Math.round(args[1]) & 0xFF) << 8)
		+ (Math.round(args[2]) & 0xFF);

	var string = integer.toString(16).toUpperCase();
	return '000000'.substring(string.length) + string;
};

convert.hex.rgb = function (args) {
	var match = args.toString(16).match(/[a-f0-9]{6}|[a-f0-9]{3}/i);
	if (!match) {
		return [0, 0, 0];
	}

	var colorString = match[0];

	if (match[0].length === 3) {
		colorString = colorString.split('').map(function (char) {
			return char + char;
		}).join('');
	}

	var integer = parseInt(colorString, 16);
	var r = (integer >> 16) & 0xFF;
	var g = (integer >> 8) & 0xFF;
	var b = integer & 0xFF;

	return [r, g, b];
};

convert.rgb.hcg = function (rgb) {
	var r = rgb[0] / 255;
	var g = rgb[1] / 255;
	var b = rgb[2] / 255;
	var max = Math.max(Math.max(r, g), b);
	var min = Math.min(Math.min(r, g), b);
	var chroma = (max - min);
	var grayscale;
	var hue;

	if (chroma < 1) {
		grayscale = min / (1 - chroma);
	} else {
		grayscale = 0;
	}

	if (chroma <= 0) {
		hue = 0;
	} else
	if (max === r) {
		hue = ((g - b) / chroma) % 6;
	} else
	if (max === g) {
		hue = 2 + (b - r) / chroma;
	} else {
		hue = 4 + (r - g) / chroma + 4;
	}

	hue /= 6;
	hue %= 1;

	return [hue * 360, chroma * 100, grayscale * 100];
};

convert.hsl.hcg = function (hsl) {
	var s = hsl[1] / 100;
	var l = hsl[2] / 100;
	var c = 1;
	var f = 0;

	if (l < 0.5) {
		c = 2.0 * s * l;
	} else {
		c = 2.0 * s * (1.0 - l);
	}

	if (c < 1.0) {
		f = (l - 0.5 * c) / (1.0 - c);
	}

	return [hsl[0], c * 100, f * 100];
};

convert.hsv.hcg = function (hsv) {
	var s = hsv[1] / 100;
	var v = hsv[2] / 100;

	var c = s * v;
	var f = 0;

	if (c < 1.0) {
		f = (v - c) / (1 - c);
	}

	return [hsv[0], c * 100, f * 100];
};

convert.hcg.rgb = function (hcg) {
	var h = hcg[0] / 360;
	var c = hcg[1] / 100;
	var g = hcg[2] / 100;

	if (c === 0.0) {
		return [g * 255, g * 255, g * 255];
	}

	var pure = [0, 0, 0];
	var hi = (h % 1) * 6;
	var v = hi % 1;
	var w = 1 - v;
	var mg = 0;

	switch (Math.floor(hi)) {
		case 0:
			pure[0] = 1; pure[1] = v; pure[2] = 0; break;
		case 1:
			pure[0] = w; pure[1] = 1; pure[2] = 0; break;
		case 2:
			pure[0] = 0; pure[1] = 1; pure[2] = v; break;
		case 3:
			pure[0] = 0; pure[1] = w; pure[2] = 1; break;
		case 4:
			pure[0] = v; pure[1] = 0; pure[2] = 1; break;
		default:
			pure[0] = 1; pure[1] = 0; pure[2] = w;
	}

	mg = (1.0 - c) * g;

	return [
		(c * pure[0] + mg) * 255,
		(c * pure[1] + mg) * 255,
		(c * pure[2] + mg) * 255
	];
};

convert.hcg.hsv = function (hcg) {
	var c = hcg[1] / 100;
	var g = hcg[2] / 100;

	var v = c + g * (1.0 - c);
	var f = 0;

	if (v > 0.0) {
		f = c / v;
	}

	return [hcg[0], f * 100, v * 100];
};

convert.hcg.hsl = function (hcg) {
	var c = hcg[1] / 100;
	var g = hcg[2] / 100;

	var l = g * (1.0 - c) + 0.5 * c;
	var s = 0;

	if (l > 0.0 && l < 0.5) {
		s = c / (2 * l);
	} else
	if (l >= 0.5 && l < 1.0) {
		s = c / (2 * (1 - l));
	}

	return [hcg[0], s * 100, l * 100];
};

convert.hcg.hwb = function (hcg) {
	var c = hcg[1] / 100;
	var g = hcg[2] / 100;
	var v = c + g * (1.0 - c);
	return [hcg[0], (v - c) * 100, (1 - v) * 100];
};

convert.hwb.hcg = function (hwb) {
	var w = hwb[1] / 100;
	var b = hwb[2] / 100;
	var v = 1 - b;
	var c = v - w;
	var g = 0;

	if (c < 1) {
		g = (v - c) / (1 - c);
	}

	return [hwb[0], c * 100, g * 100];
};

convert.apple.rgb = function (apple) {
	return [(apple[0] / 65535) * 255, (apple[1] / 65535) * 255, (apple[2] / 65535) * 255];
};

convert.rgb.apple = function (rgb) {
	return [(rgb[0] / 255) * 65535, (rgb[1] / 255) * 65535, (rgb[2] / 255) * 65535];
};

convert.gray.rgb = function (args) {
	return [args[0] / 100 * 255, args[0] / 100 * 255, args[0] / 100 * 255];
};

convert.gray.hsl = convert.gray.hsv = function (args) {
	return [0, 0, args[0]];
};

convert.gray.hwb = function (gray) {
	return [0, 100, gray[0]];
};

convert.gray.cmyk = function (gray) {
	return [0, 0, 0, gray[0]];
};

convert.gray.lab = function (gray) {
	return [gray[0], 0, 0];
};

convert.gray.hex = function (gray) {
	var val = Math.round(gray[0] / 100 * 255) & 0xFF;
	var integer = (val << 16) + (val << 8) + val;

	var string = integer.toString(16).toUpperCase();
	return '000000'.substring(string.length) + string;
};

convert.rgb.gray = function (rgb) {
	var val = (rgb[0] + rgb[1] + rgb[2]) / 3;
	return [val / 255 * 100];
};


/***/ }),
/* 7 */
/***/ (function(module, exports, __nested_webpack_require_65754__) {

"use strict";


var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _Colorpicker = __nested_webpack_require_65754__(8);

var _Colorpicker2 = _interopRequireDefault(_Colorpicker);

var _jquery = __nested_webpack_require_65754__(0);

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var plugin = 'colorpicker';

_jquery2.default[plugin] = _Colorpicker2.default;

// Colorpicker jQuery Plugin API
_jquery2.default.fn[plugin] = function (option) {
  var fnArgs = Array.prototype.slice.call(arguments, 1),
      isSingleElement = this.length === 1,
      returnValue = null;

  var $elements = this.each(function () {
    var $this = (0, _jquery2.default)(this),
        inst = $this.data(plugin),
        options = (typeof option === 'undefined' ? 'undefined' : _typeof(option)) === 'object' ? option : {};

    // Create instance if does not exist
    if (!inst) {
      inst = new _Colorpicker2.default(this, options);
      $this.data(plugin, inst);
    }

    if (!isSingleElement) {
      return;
    }

    returnValue = $this;

    if (typeof option === 'string') {
      if (option === 'colorpicker') {
        // Return colorpicker instance: e.g. .colorpicker('colorpicker')
        returnValue = inst;
      } else if (_jquery2.default.isFunction(inst[option])) {
        // Return method call return value: e.g. .colorpicker('isEnabled')
        returnValue = inst[option].apply(inst, fnArgs);
      } else {
        // Return property value: e.g. .colorpicker('element')
        returnValue = inst[option];
      }
    }
  });

  return isSingleElement ? returnValue : $elements;
};

_jquery2.default.fn[plugin].constructor = _Colorpicker2.default;

/***/ }),
/* 8 */
/***/ (function(module, exports, __nested_webpack_require_67781__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Extension = __nested_webpack_require_67781__(1);

var _Extension2 = _interopRequireDefault(_Extension);

var _options = __nested_webpack_require_67781__(3);

var _options2 = _interopRequireDefault(_options);

var _extensions = __nested_webpack_require_67781__(9);

var _extensions2 = _interopRequireDefault(_extensions);

var _jquery = __nested_webpack_require_67781__(0);

var _jquery2 = _interopRequireDefault(_jquery);

var _SliderHandler = __nested_webpack_require_67781__(13);

var _SliderHandler2 = _interopRequireDefault(_SliderHandler);

var _PopupHandler = __nested_webpack_require_67781__(14);

var _PopupHandler2 = _interopRequireDefault(_PopupHandler);

var _InputHandler = __nested_webpack_require_67781__(15);

var _InputHandler2 = _interopRequireDefault(_InputHandler);

var _ColorHandler = __nested_webpack_require_67781__(22);

var _ColorHandler2 = _interopRequireDefault(_ColorHandler);

var _PickerHandler = __nested_webpack_require_67781__(23);

var _PickerHandler2 = _interopRequireDefault(_PickerHandler);

var _AddonHandler = __nested_webpack_require_67781__(24);

var _AddonHandler2 = _interopRequireDefault(_AddonHandler);

var _ColorItem = __nested_webpack_require_67781__(2);

var _ColorItem2 = _interopRequireDefault(_ColorItem);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var colorPickerIdCounter = 0;

var root = typeof self !== 'undefined' ? self : undefined; // window

/**
 * Colorpicker widget class
 */

var Colorpicker = function () {
  _createClass(Colorpicker, [{
    key: 'color',


    /**
     * Internal color object
     *
     * @type {Color|null}
     */
    get: function get() {
      return this.colorHandler.color;
    }

    /**
     * Internal color format
     *
     * @type {String|null}
     */

  }, {
    key: 'format',
    get: function get() {
      return this.colorHandler.format;
    }

    /**
     * Getter of the picker element
     *
     * @returns {jQuery|HTMLElement}
     */

  }, {
    key: 'picker',
    get: function get() {
      return this.pickerHandler.picker;
    }

    /**
     * @fires Colorpicker#colorpickerCreate
     * @param {Object|String} element
     * @param {Object} options
     * @constructor
     */

  }], [{
    key: 'Color',

    /**
     * Color class
     *
     * @static
     * @type {Color}
     */
    get: function get() {
      return _ColorItem2.default;
    }

    /**
     * Extension class
     *
     * @static
     * @type {Extension}
     */

  }, {
    key: 'Extension',
    get: function get() {
      return _Extension2.default;
    }
  }]);

  function Colorpicker(element, options) {
    _classCallCheck(this, Colorpicker);

    colorPickerIdCounter += 1;
    /**
     * The colorpicker instance number
     * @type {number}
     */
    this.id = colorPickerIdCounter;

    /**
     * Latest colorpicker event
     *
     * @type {{name: String, e: *}}
     */
    this.lastEvent = {
      alias: null,
      e: null
    };

    /**
     * The element that the colorpicker is bound to
     *
     * @type {*|jQuery}
     */
    this.element = (0, _jquery2.default)(element).addClass('colorpicker-element').attr('data-colorpicker-id', this.id);

    /**
     * @type {defaults}
     */
    this.options = _jquery2.default.extend(true, {}, _options2.default, options, this.element.data());

    /**
     * @type {boolean}
     * @private
     */
    this.disabled = false;

    /**
     * Extensions added to this instance
     *
     * @type {Extension[]}
     */
    this.extensions = [];

    /**
     * The element where the
     * @type {*|jQuery}
     */
    this.container = this.options.container === true || this.options.container !== true && this.options.inline === true ? this.element : this.options.container;

    this.container = this.container !== false ? (0, _jquery2.default)(this.container) : false;

    /**
     * @type {InputHandler}
     */
    this.inputHandler = new _InputHandler2.default(this);
    /**
     * @type {ColorHandler}
     */
    this.colorHandler = new _ColorHandler2.default(this);
    /**
     * @type {SliderHandler}
     */
    this.sliderHandler = new _SliderHandler2.default(this);
    /**
     * @type {PopupHandler}
     */
    this.popupHandler = new _PopupHandler2.default(this, root);
    /**
     * @type {PickerHandler}
     */
    this.pickerHandler = new _PickerHandler2.default(this);
    /**
     * @type {AddonHandler}
     */
    this.addonHandler = new _AddonHandler2.default(this);

    this.init();

    // Emit a create event
    (0, _jquery2.default)(_jquery2.default.proxy(function () {
      /**
       * (Colorpicker) When the Colorpicker instance has been created and the DOM is ready.
       *
       * @event Colorpicker#colorpickerCreate
       */
      this.trigger('colorpickerCreate');
    }, this));
  }

  /**
   * Initializes the plugin
   * @private
   */


  _createClass(Colorpicker, [{
    key: 'init',
    value: function init() {
      // Init addon
      this.addonHandler.bind();

      // Init input
      this.inputHandler.bind();

      // Init extensions (before initializing the color)
      this.initExtensions();

      // Init color
      this.colorHandler.bind();

      // Init picker
      this.pickerHandler.bind();

      // Init sliders and popup
      this.sliderHandler.bind();
      this.popupHandler.bind();

      // Inject into the DOM (this may make it visible)
      this.pickerHandler.attach();

      // Update all components
      this.update();

      if (this.inputHandler.isDisabled()) {
        this.disable();
      }
    }

    /**
     * Initializes the plugin extensions
     * @private
     */

  }, {
    key: 'initExtensions',
    value: function initExtensions() {
      var _this = this;

      if (!Array.isArray(this.options.extensions)) {
        this.options.extensions = [];
      }

      if (this.options.debug) {
        this.options.extensions.push({ name: 'debugger' });
      }

      // Register and instantiate extensions
      this.options.extensions.forEach(function (ext) {
        _this.registerExtension(Colorpicker.extensions[ext.name.toLowerCase()], ext.options || {});
      });
    }

    /**
     * Creates and registers the given extension
     *
     * @param {Extension} ExtensionClass The extension class to instantiate
     * @param {Object} [config] Extension configuration
     * @returns {Extension}
     */

  }, {
    key: 'registerExtension',
    value: function registerExtension(ExtensionClass) {
      var config = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

      var ext = new ExtensionClass(this, config);

      this.extensions.push(ext);
      return ext;
    }

    /**
     * Destroys the current instance
     *
     * @fires Colorpicker#colorpickerDestroy
     */

  }, {
    key: 'destroy',
    value: function destroy() {
      var color = this.color;

      this.sliderHandler.unbind();
      this.inputHandler.unbind();
      this.popupHandler.unbind();
      this.colorHandler.unbind();
      this.addonHandler.unbind();
      this.pickerHandler.unbind();

      this.element.removeClass('colorpicker-element').removeData('colorpicker', 'color').off('.colorpicker');

      /**
       * (Colorpicker) When the instance is destroyed with all events unbound.
       *
       * @event Colorpicker#colorpickerDestroy
       */
      this.trigger('colorpickerDestroy', color);
    }

    /**
     * Shows the colorpicker widget if hidden.
     * If the colorpicker is disabled this call will be ignored.
     *
     * @fires Colorpicker#colorpickerShow
     * @param {Event} [e]
     */

  }, {
    key: 'show',
    value: function show(e) {
      this.popupHandler.show(e);
    }

    /**
     * Hides the colorpicker widget.
     *
     * @fires Colorpicker#colorpickerHide
     * @param {Event} [e]
     */

  }, {
    key: 'hide',
    value: function hide(e) {
      this.popupHandler.hide(e);
    }

    /**
     * Toggles the colorpicker between visible and hidden.
     *
     * @fires Colorpicker#colorpickerShow
     * @fires Colorpicker#colorpickerHide
     * @param {Event} [e]
     */

  }, {
    key: 'toggle',
    value: function toggle(e) {
      this.popupHandler.toggle(e);
    }

    /**
     * Returns the current color value as string
     *
     * @param {String|*} [defaultValue]
     * @returns {String|*}
     */

  }, {
    key: 'getValue',
    value: function getValue() {
      var defaultValue = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;

      var val = this.colorHandler.color;

      val = val instanceof _ColorItem2.default ? val : defaultValue;

      if (val instanceof _ColorItem2.default) {
        return val.string(this.format);
      }

      return val;
    }

    /**
     * Sets the color manually
     *
     * @fires Colorpicker#colorpickerChange
     * @param {String|Color} val
     */

  }, {
    key: 'setValue',
    value: function setValue(val) {
      if (this.isDisabled()) {
        return;
      }
      var ch = this.colorHandler;

      if (ch.hasColor() && !!val && ch.color.equals(val) || !ch.hasColor() && !val) {
        // same color or still empty
        return;
      }

      ch.color = val ? ch.createColor(val, this.options.autoInputFallback) : null;

      /**
       * (Colorpicker) When the color is set programmatically with setValue().
       *
       * @event Colorpicker#colorpickerChange
       */
      this.trigger('colorpickerChange', ch.color, val);

      // force update if color has changed to empty
      this.update();
    }

    /**
     * Updates the UI and the input color according to the internal color.
     *
     * @fires Colorpicker#colorpickerUpdate
     */

  }, {
    key: 'update',
    value: function update() {
      if (this.colorHandler.hasColor()) {
        this.inputHandler.update();
      } else {
        this.colorHandler.assureColor();
      }

      this.addonHandler.update();
      this.pickerHandler.update();

      /**
       * (Colorpicker) Fired when the widget is updated.
       *
       * @event Colorpicker#colorpickerUpdate
       */
      this.trigger('colorpickerUpdate');
    }

    /**
     * Enables the widget and the input if any
     *
     * @fires Colorpicker#colorpickerEnable
     * @returns {boolean}
     */

  }, {
    key: 'enable',
    value: function enable() {
      this.inputHandler.enable();
      this.disabled = false;
      this.picker.removeClass('colorpicker-disabled');

      /**
       * (Colorpicker) When the widget has been enabled.
       *
       * @event Colorpicker#colorpickerEnable
       */
      this.trigger('colorpickerEnable');
      return true;
    }

    /**
     * Disables the widget and the input if any
     *
     * @fires Colorpicker#colorpickerDisable
     * @returns {boolean}
     */

  }, {
    key: 'disable',
    value: function disable() {
      this.inputHandler.disable();
      this.disabled = true;
      this.picker.addClass('colorpicker-disabled');

      /**
       * (Colorpicker) When the widget has been disabled.
       *
       * @event Colorpicker#colorpickerDisable
       */
      this.trigger('colorpickerDisable');
      return true;
    }

    /**
     * Returns true if this instance is enabled
     * @returns {boolean}
     */

  }, {
    key: 'isEnabled',
    value: function isEnabled() {
      return !this.isDisabled();
    }

    /**
     * Returns true if this instance is disabled
     * @returns {boolean}
     */

  }, {
    key: 'isDisabled',
    value: function isDisabled() {
      return this.disabled === true;
    }

    /**
     * Triggers a Colorpicker event.
     *
     * @param eventName
     * @param color
     * @param value
     */

  }, {
    key: 'trigger',
    value: function trigger(eventName) {
      var color = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
      var value = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;

      this.element.trigger({
        type: eventName,
        colorpicker: this,
        color: color ? color : this.color,
        value: value ? value : this.getValue()
      });
    }
  }]);

  return Colorpicker;
}();

/**
 * Colorpicker extension classes, indexed by extension name
 *
 * @static
 * @type {Object} a map between the extension name and its class
 */


Colorpicker.extensions = _extensions2.default;

exports.default = Colorpicker;
module.exports = exports.default;

/***/ }),
/* 9 */
/***/ (function(module, exports, __nested_webpack_require_81019__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Palette = exports.Swatches = exports.Preview = exports.Debugger = undefined;

var _Debugger = __nested_webpack_require_81019__(10);

var _Debugger2 = _interopRequireDefault(_Debugger);

var _Preview = __nested_webpack_require_81019__(11);

var _Preview2 = _interopRequireDefault(_Preview);

var _Swatches = __nested_webpack_require_81019__(12);

var _Swatches2 = _interopRequireDefault(_Swatches);

var _Palette = __nested_webpack_require_81019__(4);

var _Palette2 = _interopRequireDefault(_Palette);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.Debugger = _Debugger2.default;
exports.Preview = _Preview2.default;
exports.Swatches = _Swatches2.default;
exports.Palette = _Palette2.default;
exports.default = {
  'debugger': _Debugger2.default,
  'preview': _Preview2.default,
  'swatches': _Swatches2.default,
  'palette': _Palette2.default
};

/***/ }),
/* 10 */
/***/ (function(module, exports, __nested_webpack_require_82039__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _Extension2 = __nested_webpack_require_82039__(1);

var _Extension3 = _interopRequireDefault(_Extension2);

var _jquery = __nested_webpack_require_82039__(0);

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/**
 * Debugger extension class
 * @alias DebuggerExtension
 * @ignore
 */
var Debugger = function (_Extension) {
  _inherits(Debugger, _Extension);

  function Debugger(colorpicker) {
    var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

    _classCallCheck(this, Debugger);

    /**
     * @type {number}
     */
    var _this = _possibleConstructorReturn(this, (Debugger.__proto__ || Object.getPrototypeOf(Debugger)).call(this, colorpicker, options));

    _this.eventCounter = 0;
    if (_this.colorpicker.inputHandler.hasInput()) {
      _this.colorpicker.inputHandler.input.on('change.colorpicker-ext', _jquery2.default.proxy(_this.onChangeInput, _this));
    }
    return _this;
  }

  /**
   * @fires DebuggerExtension#colorpickerDebug
   * @param {string} eventName
   * @param {*} args
   */


  _createClass(Debugger, [{
    key: 'log',
    value: function log(eventName) {
      var _console;

      for (var _len = arguments.length, args = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
        args[_key - 1] = arguments[_key];
      }

      this.eventCounter += 1;

      var logMessage = '#' + this.eventCounter + ': Colorpicker#' + this.colorpicker.id + ' [' + eventName + ']';

      (_console = console).debug.apply(_console, [logMessage].concat(args));

      /**
       * Whenever the debugger logs an event, this other event is emitted.
       *
       * @event DebuggerExtension#colorpickerDebug
       * @type {object} The event object
       * @property {Colorpicker} colorpicker The Colorpicker instance
       * @property {ColorItem} color The color instance
       * @property {{debugger: DebuggerExtension, eventName: String, logArgs: Array, logMessage: String}} debug
       *  The debug info
       */
      this.colorpicker.element.trigger({
        type: 'colorpickerDebug',
        colorpicker: this.colorpicker,
        color: this.color,
        value: null,
        debug: {
          debugger: this,
          eventName: eventName,
          logArgs: args,
          logMessage: logMessage
        }
      });
    }
  }, {
    key: 'resolveColor',
    value: function resolveColor(color) {
      var realColor = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

      this.log('resolveColor()', color, realColor);
      return false;
    }
  }, {
    key: 'onCreate',
    value: function onCreate(event) {
      this.log('colorpickerCreate');
      return _get(Debugger.prototype.__proto__ || Object.getPrototypeOf(Debugger.prototype), 'onCreate', this).call(this, event);
    }
  }, {
    key: 'onDestroy',
    value: function onDestroy(event) {
      this.log('colorpickerDestroy');
      this.eventCounter = 0;

      if (this.colorpicker.inputHandler.hasInput()) {
        this.colorpicker.inputHandler.input.off('.colorpicker-ext');
      }

      return _get(Debugger.prototype.__proto__ || Object.getPrototypeOf(Debugger.prototype), 'onDestroy', this).call(this, event);
    }
  }, {
    key: 'onUpdate',
    value: function onUpdate(event) {
      this.log('colorpickerUpdate');
    }

    /**
     * @listens Colorpicker#change
     * @param {Event} event
     */

  }, {
    key: 'onChangeInput',
    value: function onChangeInput(event) {
      this.log('input:change.colorpicker', event.value, event.color);
    }
  }, {
    key: 'onChange',
    value: function onChange(event) {
      this.log('colorpickerChange', event.value, event.color);
    }
  }, {
    key: 'onInvalid',
    value: function onInvalid(event) {
      this.log('colorpickerInvalid', event.value, event.color);
    }
  }, {
    key: 'onHide',
    value: function onHide(event) {
      this.log('colorpickerHide');
      this.eventCounter = 0;
    }
  }, {
    key: 'onShow',
    value: function onShow(event) {
      this.log('colorpickerShow');
    }
  }, {
    key: 'onDisable',
    value: function onDisable(event) {
      this.log('colorpickerDisable');
    }
  }, {
    key: 'onEnable',
    value: function onEnable(event) {
      this.log('colorpickerEnable');
    }
  }]);

  return Debugger;
}(_Extension3.default);

exports.default = Debugger;
module.exports = exports.default;

/***/ }),
/* 11 */
/***/ (function(module, exports, __nested_webpack_require_88622__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _Extension2 = __nested_webpack_require_88622__(1);

var _Extension3 = _interopRequireDefault(_Extension2);

var _jquery = __nested_webpack_require_88622__(0);

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/**
 * Color preview extension
 * @ignore
 */
var Preview = function (_Extension) {
  _inherits(Preview, _Extension);

  function Preview(colorpicker) {
    var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

    _classCallCheck(this, Preview);

    var _this = _possibleConstructorReturn(this, (Preview.__proto__ || Object.getPrototypeOf(Preview)).call(this, colorpicker, _jquery2.default.extend(true, {}, {
      template: '<div class="colorpicker-bar colorpicker-preview"><div /></div>',
      showText: true,
      format: colorpicker.format
    }, options)));

    _this.element = (0, _jquery2.default)(_this.options.template);
    _this.elementInner = _this.element.find('div');
    return _this;
  }

  _createClass(Preview, [{
    key: 'onCreate',
    value: function onCreate(event) {
      _get(Preview.prototype.__proto__ || Object.getPrototypeOf(Preview.prototype), 'onCreate', this).call(this, event);
      this.colorpicker.picker.append(this.element);
    }
  }, {
    key: 'onUpdate',
    value: function onUpdate(event) {
      _get(Preview.prototype.__proto__ || Object.getPrototypeOf(Preview.prototype), 'onUpdate', this).call(this, event);

      if (!event.color) {
        this.elementInner.css('backgroundColor', null).css('color', null).html('');
        return;
      }

      this.elementInner.css('backgroundColor', event.color.toRgbString());

      if (this.options.showText) {
        this.elementInner.html(event.color.string(this.options.format || this.colorpicker.format));

        if (event.color.isDark() && event.color.alpha > 0.5) {
          this.elementInner.css('color', 'white');
        } else {
          this.elementInner.css('color', 'black');
        }
      }
    }
  }]);

  return Preview;
}(_Extension3.default);

exports.default = Preview;
module.exports = exports.default;

/***/ }),
/* 12 */
/***/ (function(module, exports, __nested_webpack_require_92878__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _Palette2 = __nested_webpack_require_92878__(4);

var _Palette3 = _interopRequireDefault(_Palette2);

var _jquery = __nested_webpack_require_92878__(0);

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var defaults = {
  barTemplate: '<div class="colorpicker-bar colorpicker-swatches">\n                    <div class="colorpicker-swatches--inner"></div>\n                </div>',
  swatchTemplate: '<i class="colorpicker-swatch"><i class="colorpicker-swatch--inner"></i></i>'
};

/**
 * Color swatches extension
 * @ignore
 */

var Swatches = function (_Palette) {
  _inherits(Swatches, _Palette);

  function Swatches(colorpicker) {
    var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

    _classCallCheck(this, Swatches);

    var _this = _possibleConstructorReturn(this, (Swatches.__proto__ || Object.getPrototypeOf(Swatches)).call(this, colorpicker, _jquery2.default.extend(true, {}, defaults, options)));

    _this.element = null;
    return _this;
  }

  _createClass(Swatches, [{
    key: 'isEnabled',
    value: function isEnabled() {
      return this.getLength() > 0;
    }
  }, {
    key: 'onCreate',
    value: function onCreate(event) {
      _get(Swatches.prototype.__proto__ || Object.getPrototypeOf(Swatches.prototype), 'onCreate', this).call(this, event);

      if (!this.isEnabled()) {
        return;
      }

      this.element = (0, _jquery2.default)(this.options.barTemplate);
      this.load();
      this.colorpicker.picker.append(this.element);
    }
  }, {
    key: 'load',
    value: function load() {
      var _this2 = this;

      var colorpicker = this.colorpicker,
          swatchContainer = this.element.find('.colorpicker-swatches--inner'),
          isAliased = this.options.namesAsValues === true && !Array.isArray(this.colors);

      swatchContainer.empty();

      _jquery2.default.each(this.colors, function (name, value) {
        var $swatch = (0, _jquery2.default)(_this2.options.swatchTemplate).attr('data-name', name).attr('data-value', value).attr('title', isAliased ? name + ': ' + value : value).on('mousedown.colorpicker touchstart.colorpicker', function (e) {
          var $sw = (0, _jquery2.default)(this);

          // e.preventDefault();

          colorpicker.setValue(isAliased ? $sw.attr('data-name') : $sw.attr('data-value'));
        });

        $swatch.find('.colorpicker-swatch--inner').css('background-color', value);

        swatchContainer.append($swatch);
      });

      swatchContainer.append((0, _jquery2.default)('<i class="colorpicker-clear"></i>'));
    }
  }]);

  return Swatches;
}(_Palette3.default);

exports.default = Swatches;
module.exports = exports.default;

/***/ }),
/* 13 */
/***/ (function(module, exports, __nested_webpack_require_97750__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _jquery = __nested_webpack_require_97750__(0);

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * Class that handles all configured sliders on mouse or touch events.
 * @ignore
 */
var SliderHandler = function () {
  /**
   * @param {Colorpicker} colorpicker
   */
  function SliderHandler(colorpicker) {
    _classCallCheck(this, SliderHandler);

    /**
     * @type {Colorpicker}
     */
    this.colorpicker = colorpicker;
    /**
     * @type {*|String}
     * @private
     */
    this.currentSlider = null;
    /**
     * @type {{left: number, top: number}}
     * @private
     */
    this.mousePointer = {
      left: 0,
      top: 0
    };

    /**
     * @type {Function}
     */
    this.onMove = _jquery2.default.proxy(this.defaultOnMove, this);
  }

  /**
   * This function is called every time a slider guide is moved
   * The scope of "this" is the SliderHandler object.
   *
   * @param {int} top
   * @param {int} left
   */


  _createClass(SliderHandler, [{
    key: 'defaultOnMove',
    value: function defaultOnMove(top, left) {
      if (!this.currentSlider) {
        return;
      }

      var slider = this.currentSlider,
          cp = this.colorpicker,
          ch = cp.colorHandler;

      // Create a color object
      var color = !ch.hasColor() ? ch.getFallbackColor() : ch.color.getClone();

      // Adjust the guide position
      slider.guideStyle.left = left + 'px';
      slider.guideStyle.top = top + 'px';

      // Adjust the color
      if (slider.callLeft) {
        color[slider.callLeft](left / slider.maxLeft);
      }
      if (slider.callTop) {
        color[slider.callTop](top / slider.maxTop);
      }

      // Set the new color
      cp.setValue(color);
      cp.popupHandler.focus();
    }

    /**
     * Binds the colorpicker sliders to the mouse/touch events
     */

  }, {
    key: 'bind',
    value: function bind() {
      var sliders = this.colorpicker.options.horizontal ? this.colorpicker.options.slidersHorz : this.colorpicker.options.sliders;

      var sliderClasses = [];

      for (var sliderName in sliders) {
        if (!sliders.hasOwnProperty(sliderName)) {
          continue;
        }

        sliderClasses.push(sliders[sliderName].selector);
      }

      this.colorpicker.picker.find(sliderClasses.join(', ')).on('mousedown.colorpicker touchstart.colorpicker', _jquery2.default.proxy(this.pressed, this));
    }

    /**
     * Unbinds any event bound by this handler
     */

  }, {
    key: 'unbind',
    value: function unbind() {
      (0, _jquery2.default)(this.colorpicker.picker).off({
        'mousemove.colorpicker': _jquery2.default.proxy(this.moved, this),
        'touchmove.colorpicker': _jquery2.default.proxy(this.moved, this),
        'mouseup.colorpicker': _jquery2.default.proxy(this.released, this),
        'touchend.colorpicker': _jquery2.default.proxy(this.released, this)
      });
    }

    /**
     * Function triggered when clicking in one of the color adjustment bars
     *
     * @private
     * @fires Colorpicker#mousemove
     * @param {Event} e
     */

  }, {
    key: 'pressed',
    value: function pressed(e) {
      if (this.colorpicker.isDisabled()) {
        return;
      }
      this.colorpicker.lastEvent.alias = 'pressed';
      this.colorpicker.lastEvent.e = e;

      if (!e.pageX && !e.pageY && e.originalEvent && e.originalEvent.touches) {
        e.pageX = e.originalEvent.touches[0].pageX;
        e.pageY = e.originalEvent.touches[0].pageY;
      }
      // e.stopPropagation();
      // e.preventDefault();

      var target = (0, _jquery2.default)(e.target);

      // detect the slider and set the limits and callbacks
      var zone = target.closest('div');

      var sliders = this.colorpicker.options.horizontal ? this.colorpicker.options.slidersHorz : this.colorpicker.options.sliders;

      if (zone.is('.colorpicker')) {
        return;
      }

      this.currentSlider = null;

      for (var sliderName in sliders) {
        if (!sliders.hasOwnProperty(sliderName)) {
          continue;
        }

        var slider = sliders[sliderName];

        if (zone.is(slider.selector)) {
          this.currentSlider = _jquery2.default.extend({}, slider, { name: sliderName });
          break;
        } else if (slider.childSelector !== undefined && zone.is(slider.childSelector)) {
          this.currentSlider = _jquery2.default.extend({}, slider, { name: sliderName });
          zone = zone.parent(); // zone.parents(slider.selector).first() ?
          break;
        }
      }

      var guide = zone.find('.colorpicker-guide').get(0);

      if (this.currentSlider === null || guide === null) {
        return;
      }

      var offset = zone.offset();

      // reference to guide's style
      this.currentSlider.guideStyle = guide.style;
      this.currentSlider.left = e.pageX - offset.left;
      this.currentSlider.top = e.pageY - offset.top;
      this.mousePointer = {
        left: e.pageX,
        top: e.pageY
      };

      // TODO: fix moving outside the picker makes the guides to keep moving. The event needs to be bound to the window.
      /**
       * (window.document) Triggered on mousedown for the document object,
       * so the color adjustment guide is moved to the clicked position.
       *
       * @event Colorpicker#mousemove
       */
      (0, _jquery2.default)(this.colorpicker.picker).on({
        'mousemove.colorpicker': _jquery2.default.proxy(this.moved, this),
        'touchmove.colorpicker': _jquery2.default.proxy(this.moved, this),
        'mouseup.colorpicker': _jquery2.default.proxy(this.released, this),
        'touchend.colorpicker': _jquery2.default.proxy(this.released, this)
      }).trigger('mousemove');
    }

    /**
     * Function triggered when dragging a guide inside one of the color adjustment bars.
     *
     * @private
     * @param {Event} e
     */

  }, {
    key: 'moved',
    value: function moved(e) {
      this.colorpicker.lastEvent.alias = 'moved';
      this.colorpicker.lastEvent.e = e;

      if (!e.pageX && !e.pageY && e.originalEvent && e.originalEvent.touches) {
        e.pageX = e.originalEvent.touches[0].pageX;
        e.pageY = e.originalEvent.touches[0].pageY;
      }

      // e.stopPropagation();
      e.preventDefault(); // prevents scrolling on mobile

      var left = Math.max(0, Math.min(this.currentSlider.maxLeft, this.currentSlider.left + ((e.pageX || this.mousePointer.left) - this.mousePointer.left)));

      var top = Math.max(0, Math.min(this.currentSlider.maxTop, this.currentSlider.top + ((e.pageY || this.mousePointer.top) - this.mousePointer.top)));

      this.onMove(top, left);
    }

    /**
     * Function triggered when releasing the click in one of the color adjustment bars.
     *
     * @private
     * @param {Event} e
     */

  }, {
    key: 'released',
    value: function released(e) {
      this.colorpicker.lastEvent.alias = 'released';
      this.colorpicker.lastEvent.e = e;

      // e.stopPropagation();
      // e.preventDefault();

      (0, _jquery2.default)(this.colorpicker.picker).off({
        'mousemove.colorpicker': this.moved,
        'touchmove.colorpicker': this.moved,
        'mouseup.colorpicker': this.released,
        'touchend.colorpicker': this.released
      });
    }
  }]);

  return SliderHandler;
}();

exports.default = SliderHandler;
module.exports = exports.default;

/***/ }),
/* 14 */
/***/ (function(module, exports, __nested_webpack_require_106103__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _jquery = __nested_webpack_require_106103__(0);

var _jquery2 = _interopRequireDefault(_jquery);

var _options = __nested_webpack_require_106103__(3);

var _options2 = _interopRequireDefault(_options);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * Handles everything related to the UI of the colorpicker popup: show, hide, position,...
 * @ignore
 */
var PopupHandler = function () {
  /**
   * @param {Colorpicker} colorpicker
   * @param {Window} root
   */
  function PopupHandler(colorpicker, root) {
    _classCallCheck(this, PopupHandler);

    /**
     * @type {Window}
     */
    this.root = root;
    /**
     * @type {Colorpicker}
     */
    this.colorpicker = colorpicker;
    /**
     * @type {jQuery}
     */
    this.popoverTarget = null;
    /**
     * @type {jQuery}
     */
    this.popoverTip = null;

    /**
     * If true, the latest click was inside the popover
     * @type {boolean}
     */
    this.clicking = false;
    /**
     * @type {boolean}
     */
    this.hidding = false;
    /**
     * @type {boolean}
     */
    this.showing = false;
  }

  /**
   * @private
   * @returns {jQuery|false}
   */


  _createClass(PopupHandler, [{
    key: 'bind',


    /**
     * Binds the different colorpicker elements to the focus/mouse/touch events so it reacts in order to show or
     * hide the colorpicker popup accordingly. It also adds the proper classes.
     */
    value: function bind() {
      var cp = this.colorpicker;

      if (cp.options.inline) {
        cp.picker.addClass('colorpicker-inline colorpicker-visible');
        return; // no need to bind show/hide events for inline elements
      }

      cp.picker.addClass('colorpicker-popup colorpicker-hidden');

      // there is no input or addon
      if (!this.hasInput && !this.hasAddon) {
        return;
      }

      // create Bootstrap 4 popover
      if (cp.options.popover) {
        this.createPopover();
      }

      // bind addon show/hide events
      if (this.hasAddon) {
        // enable focus on addons
        if (!this.addon.attr('tabindex')) {
          this.addon.attr('tabindex', 0);
        }

        this.addon.on({
          'mousedown.colorpicker touchstart.colorpicker': _jquery2.default.proxy(this.toggle, this)
        });

        this.addon.on({
          'focus.colorpicker': _jquery2.default.proxy(this.show, this)
        });

        this.addon.on({
          'focusout.colorpicker': _jquery2.default.proxy(this.hide, this)
        });
      }

      // bind input show/hide events
      if (this.hasInput && !this.hasAddon) {
        this.input.on({
          'mousedown.colorpicker touchstart.colorpicker': _jquery2.default.proxy(this.show, this),
          'focus.colorpicker': _jquery2.default.proxy(this.show, this)
        });

        this.input.on({
          'focusout.colorpicker': _jquery2.default.proxy(this.hide, this)
        });
      }

      // reposition popup on window resize
      (0, _jquery2.default)(this.root).on('resize.colorpicker', _jquery2.default.proxy(this.reposition, this));
    }

    /**
     * Unbinds any event bound by this handler
     */

  }, {
    key: 'unbind',
    value: function unbind() {
      if (this.hasInput) {
        this.input.off({
          'mousedown.colorpicker touchstart.colorpicker': _jquery2.default.proxy(this.show, this),
          'focus.colorpicker': _jquery2.default.proxy(this.show, this)
        });
        this.input.off({
          'focusout.colorpicker': _jquery2.default.proxy(this.hide, this)
        });
      }

      if (this.hasAddon) {
        this.addon.off({
          'mousedown.colorpicker touchstart.colorpicker': _jquery2.default.proxy(this.toggle, this)
        });
        this.addon.off({
          'focus.colorpicker': _jquery2.default.proxy(this.show, this)
        });
        this.addon.off({
          'focusout.colorpicker': _jquery2.default.proxy(this.hide, this)
        });
      }

      if (this.popoverTarget) {
        this.popoverTarget.popover('dispose');
      }

      (0, _jquery2.default)(this.root).off('resize.colorpicker', _jquery2.default.proxy(this.reposition, this));
      (0, _jquery2.default)(this.root.document).off('mousedown.colorpicker touchstart.colorpicker', _jquery2.default.proxy(this.hide, this));
      (0, _jquery2.default)(this.root.document).off('mousedown.colorpicker touchstart.colorpicker', _jquery2.default.proxy(this.onClickingInside, this));
    }
  }, {
    key: 'isClickingInside',
    value: function isClickingInside(e) {
      if (!e) {
        return false;
      }

      return this.isOrIsInside(this.popoverTip, e.currentTarget) || this.isOrIsInside(this.popoverTip, e.target) || this.isOrIsInside(this.colorpicker.picker, e.currentTarget) || this.isOrIsInside(this.colorpicker.picker, e.target);
    }
  }, {
    key: 'isOrIsInside',
    value: function isOrIsInside(container, element) {
      if (!container || !element) {
        return false;
      }

      element = (0, _jquery2.default)(element);

      return element.is(container) || container.find(element).length > 0;
    }
  }, {
    key: 'onClickingInside',
    value: function onClickingInside(e) {
      this.clicking = this.isClickingInside(e);
    }
  }, {
    key: 'createPopover',
    value: function createPopover() {
      var cp = this.colorpicker;

      this.popoverTarget = this.hasAddon ? this.addon : this.input;

      cp.picker.addClass('colorpicker-bs-popover-content');

      this.popoverTarget.popover(_jquery2.default.extend(true, {}, _options2.default.popover, cp.options.popover, { trigger: 'manual', content: cp.picker, html: true }));

      this.popoverTip = (0, _jquery2.default)(this.popoverTarget.popover('getTipElement').data('bs.popover').tip);
      this.popoverTip.addClass('colorpicker-bs-popover');

      this.popoverTarget.on('shown.bs.popover', _jquery2.default.proxy(this.fireShow, this));
      this.popoverTarget.on('hidden.bs.popover', _jquery2.default.proxy(this.fireHide, this));
    }

    /**
     * If the widget is not inside a container or inline, rearranges its position relative to its element offset.
     *
     * @param {Event} [e]
     * @private
     */

  }, {
    key: 'reposition',
    value: function reposition(e) {
      if (this.popoverTarget && this.isVisible()) {
        this.popoverTarget.popover('update');
      }
    }

    /**
     * Toggles the colorpicker between visible or hidden
     *
     * @fires Colorpicker#colorpickerShow
     * @fires Colorpicker#colorpickerHide
     * @param {Event} [e]
     */

  }, {
    key: 'toggle',
    value: function toggle(e) {
      if (this.isVisible()) {
        this.hide(e);
      } else {
        this.show(e);
      }
    }

    /**
     * Shows the colorpicker widget if hidden.
     *
     * @fires Colorpicker#colorpickerShow
     * @param {Event} [e]
     */

  }, {
    key: 'show',
    value: function show(e) {
      if (this.isVisible() || this.showing || this.hidding) {
        return;
      }

      this.showing = true;
      this.hidding = false;
      this.clicking = false;

      var cp = this.colorpicker;

      cp.lastEvent.alias = 'show';
      cp.lastEvent.e = e;

      // Prevent showing browser native HTML5 colorpicker
      if (e && (!this.hasInput || this.input.attr('type') === 'color') && e && e.preventDefault) {
        e.stopPropagation();
        e.preventDefault();
      }

      // If it's a popover, add event to the document to hide the picker when clicking outside of it
      if (this.isPopover) {
        (0, _jquery2.default)(this.root).on('resize.colorpicker', _jquery2.default.proxy(this.reposition, this));
      }

      // add visible class before popover is shown
      cp.picker.addClass('colorpicker-visible').removeClass('colorpicker-hidden');

      if (this.popoverTarget) {
        this.popoverTarget.popover('show');
      } else {
        this.fireShow();
      }
    }
  }, {
    key: 'fireShow',
    value: function fireShow() {
      this.hidding = false;
      this.showing = false;

      if (this.isPopover) {
        // Add event to hide on outside click
        (0, _jquery2.default)(this.root.document).on('mousedown.colorpicker touchstart.colorpicker', _jquery2.default.proxy(this.hide, this));
        (0, _jquery2.default)(this.root.document).on('mousedown.colorpicker touchstart.colorpicker', _jquery2.default.proxy(this.onClickingInside, this));
      }

      /**
       * (Colorpicker) When show() is called and the widget can be shown.
       *
       * @event Colorpicker#colorpickerShow
       */
      this.colorpicker.trigger('colorpickerShow');
    }

    /**
     * Hides the colorpicker widget.
     * Hide is prevented when it is triggered by an event whose target element has been clicked/touched.
     *
     * @fires Colorpicker#colorpickerHide
     * @param {Event} [e]
     */

  }, {
    key: 'hide',
    value: function hide(e) {
      if (this.isHidden() || this.showing || this.hidding) {
        return;
      }

      var cp = this.colorpicker,
          clicking = this.clicking || this.isClickingInside(e);

      this.hidding = true;
      this.showing = false;
      this.clicking = false;

      cp.lastEvent.alias = 'hide';
      cp.lastEvent.e = e;

      // TODO: fix having to click twice outside when losing focus and last 2 clicks where inside the colorpicker

      // Prevent hide if triggered by an event and an element inside the colorpicker has been clicked/touched
      if (clicking) {
        this.hidding = false;
        return;
      }

      if (this.popoverTarget) {
        this.popoverTarget.popover('hide');
      } else {
        this.fireHide();
      }
    }
  }, {
    key: 'fireHide',
    value: function fireHide() {
      this.hidding = false;
      this.showing = false;

      var cp = this.colorpicker;

      // add hidden class after popover is hidden
      cp.picker.addClass('colorpicker-hidden').removeClass('colorpicker-visible');

      // Unbind window and document events, since there is no need to keep them while the popup is hidden
      (0, _jquery2.default)(this.root).off('resize.colorpicker', _jquery2.default.proxy(this.reposition, this));
      (0, _jquery2.default)(this.root.document).off('mousedown.colorpicker touchstart.colorpicker', _jquery2.default.proxy(this.hide, this));
      (0, _jquery2.default)(this.root.document).off('mousedown.colorpicker touchstart.colorpicker', _jquery2.default.proxy(this.onClickingInside, this));

      /**
       * (Colorpicker) When hide() is called and the widget can be hidden.
       *
       * @event Colorpicker#colorpickerHide
       */
      cp.trigger('colorpickerHide');
    }
  }, {
    key: 'focus',
    value: function focus() {
      if (this.hasAddon) {
        return this.addon.focus();
      }
      if (this.hasInput) {
        return this.input.focus();
      }
      return false;
    }

    /**
     * Returns true if the colorpicker element has the colorpicker-visible class and not the colorpicker-hidden one.
     * False otherwise.
     *
     * @returns {boolean}
     */

  }, {
    key: 'isVisible',
    value: function isVisible() {
      return this.colorpicker.picker.hasClass('colorpicker-visible') && !this.colorpicker.picker.hasClass('colorpicker-hidden');
    }

    /**
     * Returns true if the colorpicker element has the colorpicker-hidden class and not the colorpicker-visible one.
     * False otherwise.
     *
     * @returns {boolean}
     */

  }, {
    key: 'isHidden',
    value: function isHidden() {
      return this.colorpicker.picker.hasClass('colorpicker-hidden') && !this.colorpicker.picker.hasClass('colorpicker-visible');
    }
  }, {
    key: 'input',
    get: function get() {
      return this.colorpicker.inputHandler.input;
    }

    /**
     * @private
     * @returns {boolean}
     */

  }, {
    key: 'hasInput',
    get: function get() {
      return this.colorpicker.inputHandler.hasInput();
    }

    /**
     * @private
     * @returns {jQuery|false}
     */

  }, {
    key: 'addon',
    get: function get() {
      return this.colorpicker.addonHandler.addon;
    }

    /**
     * @private
     * @returns {boolean}
     */

  }, {
    key: 'hasAddon',
    get: function get() {
      return this.colorpicker.addonHandler.hasAddon();
    }

    /**
     * @private
     * @returns {boolean}
     */

  }, {
    key: 'isPopover',
    get: function get() {
      return !this.colorpicker.options.inline && !!this.popoverTip;
    }
  }]);

  return PopupHandler;
}();

exports.default = PopupHandler;
module.exports = exports.default;

/***/ }),
/* 15 */
/***/ (function(module, exports, __nested_webpack_require_119576__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _jquery = __nested_webpack_require_119576__(0);

var _jquery2 = _interopRequireDefault(_jquery);

var _ColorItem = __nested_webpack_require_119576__(2);

var _ColorItem2 = _interopRequireDefault(_ColorItem);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * Handles everything related to the colorpicker input
 * @ignore
 */
var InputHandler = function () {
  /**
   * @param {Colorpicker} colorpicker
   */
  function InputHandler(colorpicker) {
    _classCallCheck(this, InputHandler);

    /**
     * @type {Colorpicker}
     */
    this.colorpicker = colorpicker;
    /**
     * @type {jQuery|false}
     */
    this.input = this.colorpicker.element.is('input') ? this.colorpicker.element : this.colorpicker.options.input ? this.colorpicker.element.find(this.colorpicker.options.input) : false;

    if (this.input && this.input.length === 0) {
      this.input = false;
    }

    this._initValue();
  }

  _createClass(InputHandler, [{
    key: 'bind',
    value: function bind() {
      if (!this.hasInput()) {
        return;
      }
      this.input.on({
        'keyup.colorpicker': _jquery2.default.proxy(this.onkeyup, this)
      });
      this.input.on({
        'change.colorpicker': _jquery2.default.proxy(this.onchange, this)
      });
    }
  }, {
    key: 'unbind',
    value: function unbind() {
      if (!this.hasInput()) {
        return;
      }
      this.input.off('.colorpicker');
    }
  }, {
    key: '_initValue',
    value: function _initValue() {
      if (!this.hasInput()) {
        return;
      }

      var val = '';

      [
      // candidates:
      this.input.val(), this.input.data('color'), this.input.attr('data-color')].map(function (item) {
        if (item && val === '') {
          val = item;
        }
      });

      if (val instanceof _ColorItem2.default) {
        val = this.getFormattedColor(val.string(this.colorpicker.format));
      } else if (!(typeof val === 'string' || val instanceof String)) {
        val = '';
      }

      this.input.prop('value', val);
    }

    /**
     * Returns the color string from the input value.
     * If there is no input the return value is false.
     *
     * @returns {String|boolean}
     */

  }, {
    key: 'getValue',
    value: function getValue() {
      if (!this.hasInput()) {
        return false;
      }

      return this.input.val();
    }

    /**
     * If the input element is present, it updates the value with the current color object color string.
     * If the value is changed, this method fires a "change" event on the input element.
     *
     * @param {String} val
     *
     * @fires Colorpicker#change
     */

  }, {
    key: 'setValue',
    value: function setValue(val) {
      if (!this.hasInput()) {
        return;
      }

      var inputVal = this.input.prop('value');

      val = val ? val : '';

      if (val === (inputVal ? inputVal : '')) {
        // No need to set value or trigger any event if nothing changed
        return;
      }

      this.input.prop('value', val);

      /**
       * (Input) Triggered on the input element when a new color is selected.
       *
       * @event Colorpicker#change
       */
      this.input.trigger({
        type: 'change',
        colorpicker: this.colorpicker,
        color: this.colorpicker.color,
        value: val
      });
    }

    /**
     * Returns the formatted color string, with the formatting options applied
     * (e.g. useHashPrefix)
     *
     * @param {String|null} val
     *
     * @returns {String}
     */

  }, {
    key: 'getFormattedColor',
    value: function getFormattedColor() {
      var val = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;

      val = val ? val : this.colorpicker.colorHandler.getColorString();

      if (!val) {
        return '';
      }

      val = this.colorpicker.colorHandler.resolveColorDelegate(val, false);

      if (this.colorpicker.options.useHashPrefix === false) {
        val = val.replace(/^#/g, '');
      }

      return val;
    }

    /**
     * Returns true if the widget has an associated input element, false otherwise
     * @returns {boolean}
     */

  }, {
    key: 'hasInput',
    value: function hasInput() {
      return this.input !== false;
    }

    /**
     * Returns true if the input exists and is disabled
     * @returns {boolean}
     */

  }, {
    key: 'isEnabled',
    value: function isEnabled() {
      return this.hasInput() && !this.isDisabled();
    }

    /**
     * Returns true if the input exists and is disabled
     * @returns {boolean}
     */

  }, {
    key: 'isDisabled',
    value: function isDisabled() {
      return this.hasInput() && this.input.prop('disabled') === true;
    }

    /**
     * Disables the input if any
     *
     * @fires Colorpicker#colorpickerDisable
     * @returns {boolean}
     */

  }, {
    key: 'disable',
    value: function disable() {
      if (this.hasInput()) {
        this.input.prop('disabled', true);
      }
    }

    /**
     * Enables the input if any
     *
     * @fires Colorpicker#colorpickerEnable
     * @returns {boolean}
     */

  }, {
    key: 'enable',
    value: function enable() {
      if (this.hasInput()) {
        this.input.prop('disabled', false);
      }
    }

    /**
     * Calls setValue with the current internal color value
     *
     * @fires Colorpicker#change
     */

  }, {
    key: 'update',
    value: function update() {
      if (!this.hasInput()) {
        return;
      }

      if (this.colorpicker.options.autoInputFallback === false && this.colorpicker.colorHandler.isInvalidColor()) {
        // prevent update if color is invalid, autoInputFallback is disabled and the last event is keyup.
        return;
      }

      this.setValue(this.getFormattedColor());
    }

    /**
     * Function triggered when the input has changed, so the colorpicker gets updated.
     *
     * @private
     * @param {Event} e
     * @returns {boolean}
     */

  }, {
    key: 'onchange',
    value: function onchange(e) {
      this.colorpicker.lastEvent.alias = 'input.change';
      this.colorpicker.lastEvent.e = e;

      var val = this.getValue();

      if (val !== e.value) {
        this.colorpicker.setValue(val);
      }
    }

    /**
     * Function triggered after a keyboard key has been released.
     *
     * @private
     * @param {Event} e
     * @returns {boolean}
     */

  }, {
    key: 'onkeyup',
    value: function onkeyup(e) {
      this.colorpicker.lastEvent.alias = 'input.keyup';
      this.colorpicker.lastEvent.e = e;

      var val = this.getValue();

      if (val !== e.value) {
        this.colorpicker.setValue(val);
      }
    }
  }]);

  return InputHandler;
}();

exports.default = InputHandler;
module.exports = exports.default;

/***/ }),
/* 16 */
/***/ (function(module, exports, __nested_webpack_require_127262__) {

"use strict";


var colorString = __nested_webpack_require_127262__(17);
var convert = __nested_webpack_require_127262__(20);

var _slice = [].slice;

var skippedModels = [
	// to be honest, I don't really feel like keyword belongs in color convert, but eh.
	'keyword',

	// gray conflicts with some method names, and has its own method defined.
	'gray',

	// shouldn't really be in color-convert either...
	'hex'
];

var hashedModelKeys = {};
Object.keys(convert).forEach(function (model) {
	hashedModelKeys[_slice.call(convert[model].labels).sort().join('')] = model;
});

var limiters = {};

function Color(obj, model) {
	if (!(this instanceof Color)) {
		return new Color(obj, model);
	}

	if (model && model in skippedModels) {
		model = null;
	}

	if (model && !(model in convert)) {
		throw new Error('Unknown model: ' + model);
	}

	var i;
	var channels;

	if (obj == null) { // eslint-disable-line no-eq-null,eqeqeq
		this.model = 'rgb';
		this.color = [0, 0, 0];
		this.valpha = 1;
	} else if (obj instanceof Color) {
		this.model = obj.model;
		this.color = obj.color.slice();
		this.valpha = obj.valpha;
	} else if (typeof obj === 'string') {
		var result = colorString.get(obj);
		if (result === null) {
			throw new Error('Unable to parse color from string: ' + obj);
		}

		this.model = result.model;
		channels = convert[this.model].channels;
		this.color = result.value.slice(0, channels);
		this.valpha = typeof result.value[channels] === 'number' ? result.value[channels] : 1;
	} else if (obj.length) {
		this.model = model || 'rgb';
		channels = convert[this.model].channels;
		var newArr = _slice.call(obj, 0, channels);
		this.color = zeroArray(newArr, channels);
		this.valpha = typeof obj[channels] === 'number' ? obj[channels] : 1;
	} else if (typeof obj === 'number') {
		// this is always RGB - can be converted later on.
		obj &= 0xFFFFFF;
		this.model = 'rgb';
		this.color = [
			(obj >> 16) & 0xFF,
			(obj >> 8) & 0xFF,
			obj & 0xFF
		];
		this.valpha = 1;
	} else {
		this.valpha = 1;

		var keys = Object.keys(obj);
		if ('alpha' in obj) {
			keys.splice(keys.indexOf('alpha'), 1);
			this.valpha = typeof obj.alpha === 'number' ? obj.alpha : 0;
		}

		var hashedKeys = keys.sort().join('');
		if (!(hashedKeys in hashedModelKeys)) {
			throw new Error('Unable to parse color from object: ' + JSON.stringify(obj));
		}

		this.model = hashedModelKeys[hashedKeys];

		var labels = convert[this.model].labels;
		var color = [];
		for (i = 0; i < labels.length; i++) {
			color.push(obj[labels[i]]);
		}

		this.color = zeroArray(color);
	}

	// perform limitations (clamping, etc.)
	if (limiters[this.model]) {
		channels = convert[this.model].channels;
		for (i = 0; i < channels; i++) {
			var limit = limiters[this.model][i];
			if (limit) {
				this.color[i] = limit(this.color[i]);
			}
		}
	}

	this.valpha = Math.max(0, Math.min(1, this.valpha));

	if (Object.freeze) {
		Object.freeze(this);
	}
}

Color.prototype = {
	toString: function () {
		return this.string();
	},

	toJSON: function () {
		return this[this.model]();
	},

	string: function (places) {
		var self = this.model in colorString.to ? this : this.rgb();
		self = self.round(typeof places === 'number' ? places : 1);
		var args = self.valpha === 1 ? self.color : self.color.concat(this.valpha);
		return colorString.to[self.model](args);
	},

	percentString: function (places) {
		var self = this.rgb().round(typeof places === 'number' ? places : 1);
		var args = self.valpha === 1 ? self.color : self.color.concat(this.valpha);
		return colorString.to.rgb.percent(args);
	},

	array: function () {
		return this.valpha === 1 ? this.color.slice() : this.color.concat(this.valpha);
	},

	object: function () {
		var result = {};
		var channels = convert[this.model].channels;
		var labels = convert[this.model].labels;

		for (var i = 0; i < channels; i++) {
			result[labels[i]] = this.color[i];
		}

		if (this.valpha !== 1) {
			result.alpha = this.valpha;
		}

		return result;
	},

	unitArray: function () {
		var rgb = this.rgb().color;
		rgb[0] /= 255;
		rgb[1] /= 255;
		rgb[2] /= 255;

		if (this.valpha !== 1) {
			rgb.push(this.valpha);
		}

		return rgb;
	},

	unitObject: function () {
		var rgb = this.rgb().object();
		rgb.r /= 255;
		rgb.g /= 255;
		rgb.b /= 255;

		if (this.valpha !== 1) {
			rgb.alpha = this.valpha;
		}

		return rgb;
	},

	round: function (places) {
		places = Math.max(places || 0, 0);
		return new Color(this.color.map(roundToPlace(places)).concat(this.valpha), this.model);
	},

	alpha: function (val) {
		if (arguments.length) {
			return new Color(this.color.concat(Math.max(0, Math.min(1, val))), this.model);
		}

		return this.valpha;
	},

	// rgb
	red: getset('rgb', 0, maxfn(255)),
	green: getset('rgb', 1, maxfn(255)),
	blue: getset('rgb', 2, maxfn(255)),

	hue: getset(['hsl', 'hsv', 'hsl', 'hwb', 'hcg'], 0, function (val) { return ((val % 360) + 360) % 360; }), // eslint-disable-line brace-style

	saturationl: getset('hsl', 1, maxfn(100)),
	lightness: getset('hsl', 2, maxfn(100)),

	saturationv: getset('hsv', 1, maxfn(100)),
	value: getset('hsv', 2, maxfn(100)),

	chroma: getset('hcg', 1, maxfn(100)),
	gray: getset('hcg', 2, maxfn(100)),

	white: getset('hwb', 1, maxfn(100)),
	wblack: getset('hwb', 2, maxfn(100)),

	cyan: getset('cmyk', 0, maxfn(100)),
	magenta: getset('cmyk', 1, maxfn(100)),
	yellow: getset('cmyk', 2, maxfn(100)),
	black: getset('cmyk', 3, maxfn(100)),

	x: getset('xyz', 0, maxfn(100)),
	y: getset('xyz', 1, maxfn(100)),
	z: getset('xyz', 2, maxfn(100)),

	l: getset('lab', 0, maxfn(100)),
	a: getset('lab', 1),
	b: getset('lab', 2),

	keyword: function (val) {
		if (arguments.length) {
			return new Color(val);
		}

		return convert[this.model].keyword(this.color);
	},

	hex: function (val) {
		if (arguments.length) {
			return new Color(val);
		}

		return colorString.to.hex(this.rgb().round().color);
	},

	rgbNumber: function () {
		var rgb = this.rgb().color;
		return ((rgb[0] & 0xFF) << 16) | ((rgb[1] & 0xFF) << 8) | (rgb[2] & 0xFF);
	},

	luminosity: function () {
		// http://www.w3.org/TR/WCAG20/#relativeluminancedef
		var rgb = this.rgb().color;

		var lum = [];
		for (var i = 0; i < rgb.length; i++) {
			var chan = rgb[i] / 255;
			lum[i] = (chan <= 0.03928) ? chan / 12.92 : Math.pow(((chan + 0.055) / 1.055), 2.4);
		}

		return 0.2126 * lum[0] + 0.7152 * lum[1] + 0.0722 * lum[2];
	},

	contrast: function (color2) {
		// http://www.w3.org/TR/WCAG20/#contrast-ratiodef
		var lum1 = this.luminosity();
		var lum2 = color2.luminosity();

		if (lum1 > lum2) {
			return (lum1 + 0.05) / (lum2 + 0.05);
		}

		return (lum2 + 0.05) / (lum1 + 0.05);
	},

	level: function (color2) {
		var contrastRatio = this.contrast(color2);
		if (contrastRatio >= 7.1) {
			return 'AAA';
		}

		return (contrastRatio >= 4.5) ? 'AA' : '';
	},

	isDark: function () {
		// YIQ equation from http://24ways.org/2010/calculating-color-contrast
		var rgb = this.rgb().color;
		var yiq = (rgb[0] * 299 + rgb[1] * 587 + rgb[2] * 114) / 1000;
		return yiq < 128;
	},

	isLight: function () {
		return !this.isDark();
	},

	negate: function () {
		var rgb = this.rgb();
		for (var i = 0; i < 3; i++) {
			rgb.color[i] = 255 - rgb.color[i];
		}
		return rgb;
	},

	lighten: function (ratio) {
		var hsl = this.hsl();
		hsl.color[2] += hsl.color[2] * ratio;
		return hsl;
	},

	darken: function (ratio) {
		var hsl = this.hsl();
		hsl.color[2] -= hsl.color[2] * ratio;
		return hsl;
	},

	saturate: function (ratio) {
		var hsl = this.hsl();
		hsl.color[1] += hsl.color[1] * ratio;
		return hsl;
	},

	desaturate: function (ratio) {
		var hsl = this.hsl();
		hsl.color[1] -= hsl.color[1] * ratio;
		return hsl;
	},

	whiten: function (ratio) {
		var hwb = this.hwb();
		hwb.color[1] += hwb.color[1] * ratio;
		return hwb;
	},

	blacken: function (ratio) {
		var hwb = this.hwb();
		hwb.color[2] += hwb.color[2] * ratio;
		return hwb;
	},

	grayscale: function () {
		// http://en.wikipedia.org/wiki/Grayscale#Converting_color_to_grayscale
		var rgb = this.rgb().color;
		var val = rgb[0] * 0.3 + rgb[1] * 0.59 + rgb[2] * 0.11;
		return Color.rgb(val, val, val);
	},

	fade: function (ratio) {
		return this.alpha(this.valpha - (this.valpha * ratio));
	},

	opaquer: function (ratio) {
		return this.alpha(this.valpha + (this.valpha * ratio));
	},

	rotate: function (degrees) {
		var hsl = this.hsl();
		var hue = hsl.color[0];
		hue = (hue + degrees) % 360;
		hue = hue < 0 ? 360 + hue : hue;
		hsl.color[0] = hue;
		return hsl;
	},

	mix: function (mixinColor, weight) {
		// ported from sass implementation in C
		// https://github.com/sass/libsass/blob/0e6b4a2850092356aa3ece07c6b249f0221caced/functions.cpp#L209
		if (!mixinColor || !mixinColor.rgb) {
			throw new Error('Argument to "mix" was not a Color instance, but rather an instance of ' + typeof mixinColor);
		}
		var color1 = mixinColor.rgb();
		var color2 = this.rgb();
		var p = weight === undefined ? 0.5 : weight;

		var w = 2 * p - 1;
		var a = color1.alpha() - color2.alpha();

		var w1 = (((w * a === -1) ? w : (w + a) / (1 + w * a)) + 1) / 2.0;
		var w2 = 1 - w1;

		return Color.rgb(
				w1 * color1.red() + w2 * color2.red(),
				w1 * color1.green() + w2 * color2.green(),
				w1 * color1.blue() + w2 * color2.blue(),
				color1.alpha() * p + color2.alpha() * (1 - p));
	}
};

// model conversion methods and static constructors
Object.keys(convert).forEach(function (model) {
	if (skippedModels.indexOf(model) !== -1) {
		return;
	}

	var channels = convert[model].channels;

	// conversion methods
	Color.prototype[model] = function () {
		if (this.model === model) {
			return new Color(this);
		}

		if (arguments.length) {
			return new Color(arguments, model);
		}

		var newAlpha = typeof arguments[channels] === 'number' ? channels : this.valpha;
		return new Color(assertArray(convert[this.model][model].raw(this.color)).concat(newAlpha), model);
	};

	// 'static' construction methods
	Color[model] = function (color) {
		if (typeof color === 'number') {
			color = zeroArray(_slice.call(arguments), channels);
		}
		return new Color(color, model);
	};
});

function roundTo(num, places) {
	return Number(num.toFixed(places));
}

function roundToPlace(places) {
	return function (num) {
		return roundTo(num, places);
	};
}

function getset(model, channel, modifier) {
	model = Array.isArray(model) ? model : [model];

	model.forEach(function (m) {
		(limiters[m] || (limiters[m] = []))[channel] = modifier;
	});

	model = model[0];

	return function (val) {
		var result;

		if (arguments.length) {
			if (modifier) {
				val = modifier(val);
			}

			result = this[model]();
			result.color[channel] = val;
			return result;
		}

		result = this[model]().color[channel];
		if (modifier) {
			result = modifier(result);
		}

		return result;
	};
}

function maxfn(max) {
	return function (v) {
		return Math.max(0, Math.min(max, v));
	};
}

function assertArray(val) {
	return Array.isArray(val) ? val : [val];
}

function zeroArray(arr, length) {
	for (var i = 0; i < length; i++) {
		if (typeof arr[i] !== 'number') {
			arr[i] = 0;
		}
	}

	return arr;
}

module.exports = Color;


/***/ }),
/* 17 */
/***/ (function(module, exports, __nested_webpack_require_138459__) {

/* MIT license */
var colorNames = __nested_webpack_require_138459__(5);
var swizzle = __nested_webpack_require_138459__(18);

var reverseNames = {};

// create a list of reverse color names
for (var name in colorNames) {
	if (colorNames.hasOwnProperty(name)) {
		reverseNames[colorNames[name]] = name;
	}
}

var cs = module.exports = {
	to: {},
	get: {}
};

cs.get = function (string) {
	var prefix = string.substring(0, 3).toLowerCase();
	var val;
	var model;
	switch (prefix) {
		case 'hsl':
			val = cs.get.hsl(string);
			model = 'hsl';
			break;
		case 'hwb':
			val = cs.get.hwb(string);
			model = 'hwb';
			break;
		default:
			val = cs.get.rgb(string);
			model = 'rgb';
			break;
	}

	if (!val) {
		return null;
	}

	return {model: model, value: val};
};

cs.get.rgb = function (string) {
	if (!string) {
		return null;
	}

	var abbr = /^#([a-f0-9]{3,4})$/i;
	var hex = /^#([a-f0-9]{6})([a-f0-9]{2})?$/i;
	var rgba = /^rgba?\(\s*([+-]?\d+)\s*,\s*([+-]?\d+)\s*,\s*([+-]?\d+)\s*(?:,\s*([+-]?[\d\.]+)\s*)?\)$/;
	var per = /^rgba?\(\s*([+-]?[\d\.]+)\%\s*,\s*([+-]?[\d\.]+)\%\s*,\s*([+-]?[\d\.]+)\%\s*(?:,\s*([+-]?[\d\.]+)\s*)?\)$/;
	var keyword = /(\D+)/;

	var rgb = [0, 0, 0, 1];
	var match;
	var i;
	var hexAlpha;

	if (match = string.match(hex)) {
		hexAlpha = match[2];
		match = match[1];

		for (i = 0; i < 3; i++) {
			// https://jsperf.com/slice-vs-substr-vs-substring-methods-long-string/19
			var i2 = i * 2;
			rgb[i] = parseInt(match.slice(i2, i2 + 2), 16);
		}

		if (hexAlpha) {
			rgb[3] = Math.round((parseInt(hexAlpha, 16) / 255) * 100) / 100;
		}
	} else if (match = string.match(abbr)) {
		match = match[1];
		hexAlpha = match[3];

		for (i = 0; i < 3; i++) {
			rgb[i] = parseInt(match[i] + match[i], 16);
		}

		if (hexAlpha) {
			rgb[3] = Math.round((parseInt(hexAlpha + hexAlpha, 16) / 255) * 100) / 100;
		}
	} else if (match = string.match(rgba)) {
		for (i = 0; i < 3; i++) {
			rgb[i] = parseInt(match[i + 1], 0);
		}

		if (match[4]) {
			rgb[3] = parseFloat(match[4]);
		}
	} else if (match = string.match(per)) {
		for (i = 0; i < 3; i++) {
			rgb[i] = Math.round(parseFloat(match[i + 1]) * 2.55);
		}

		if (match[4]) {
			rgb[3] = parseFloat(match[4]);
		}
	} else if (match = string.match(keyword)) {
		if (match[1] === 'transparent') {
			return [0, 0, 0, 0];
		}

		rgb = colorNames[match[1]];

		if (!rgb) {
			return null;
		}

		rgb[3] = 1;

		return rgb;
	} else {
		return null;
	}

	for (i = 0; i < 3; i++) {
		rgb[i] = clamp(rgb[i], 0, 255);
	}
	rgb[3] = clamp(rgb[3], 0, 1);

	return rgb;
};

cs.get.hsl = function (string) {
	if (!string) {
		return null;
	}

	var hsl = /^hsla?\(\s*([+-]?(?:\d*\.)?\d+)(?:deg)?\s*,\s*([+-]?[\d\.]+)%\s*,\s*([+-]?[\d\.]+)%\s*(?:,\s*([+-]?[\d\.]+)\s*)?\)$/;
	var match = string.match(hsl);

	if (match) {
		var alpha = parseFloat(match[4]);
		var h = (parseFloat(match[1]) + 360) % 360;
		var s = clamp(parseFloat(match[2]), 0, 100);
		var l = clamp(parseFloat(match[3]), 0, 100);
		var a = clamp(isNaN(alpha) ? 1 : alpha, 0, 1);

		return [h, s, l, a];
	}

	return null;
};

cs.get.hwb = function (string) {
	if (!string) {
		return null;
	}

	var hwb = /^hwb\(\s*([+-]?\d*[\.]?\d+)(?:deg)?\s*,\s*([+-]?[\d\.]+)%\s*,\s*([+-]?[\d\.]+)%\s*(?:,\s*([+-]?[\d\.]+)\s*)?\)$/;
	var match = string.match(hwb);

	if (match) {
		var alpha = parseFloat(match[4]);
		var h = ((parseFloat(match[1]) % 360) + 360) % 360;
		var w = clamp(parseFloat(match[2]), 0, 100);
		var b = clamp(parseFloat(match[3]), 0, 100);
		var a = clamp(isNaN(alpha) ? 1 : alpha, 0, 1);
		return [h, w, b, a];
	}

	return null;
};

cs.to.hex = function () {
	var rgba = swizzle(arguments);

	return (
		'#' +
		hexDouble(rgba[0]) +
		hexDouble(rgba[1]) +
		hexDouble(rgba[2]) +
		(rgba[3] < 1
			? (hexDouble(Math.round(rgba[3] * 255)))
			: '')
	);
};

cs.to.rgb = function () {
	var rgba = swizzle(arguments);

	return rgba.length < 4 || rgba[3] === 1
		? 'rgb(' + Math.round(rgba[0]) + ', ' + Math.round(rgba[1]) + ', ' + Math.round(rgba[2]) + ')'
		: 'rgba(' + Math.round(rgba[0]) + ', ' + Math.round(rgba[1]) + ', ' + Math.round(rgba[2]) + ', ' + rgba[3] + ')';
};

cs.to.rgb.percent = function () {
	var rgba = swizzle(arguments);

	var r = Math.round(rgba[0] / 255 * 100);
	var g = Math.round(rgba[1] / 255 * 100);
	var b = Math.round(rgba[2] / 255 * 100);

	return rgba.length < 4 || rgba[3] === 1
		? 'rgb(' + r + '%, ' + g + '%, ' + b + '%)'
		: 'rgba(' + r + '%, ' + g + '%, ' + b + '%, ' + rgba[3] + ')';
};

cs.to.hsl = function () {
	var hsla = swizzle(arguments);
	return hsla.length < 4 || hsla[3] === 1
		? 'hsl(' + hsla[0] + ', ' + hsla[1] + '%, ' + hsla[2] + '%)'
		: 'hsla(' + hsla[0] + ', ' + hsla[1] + '%, ' + hsla[2] + '%, ' + hsla[3] + ')';
};

// hwb is a bit different than rgb(a) & hsl(a) since there is no alpha specific syntax
// (hwb have alpha optional & 1 is default value)
cs.to.hwb = function () {
	var hwba = swizzle(arguments);

	var a = '';
	if (hwba.length >= 4 && hwba[3] !== 1) {
		a = ', ' + hwba[3];
	}

	return 'hwb(' + hwba[0] + ', ' + hwba[1] + '%, ' + hwba[2] + '%' + a + ')';
};

cs.to.keyword = function (rgb) {
	return reverseNames[rgb.slice(0, 3)];
};

// helpers
function clamp(num, min, max) {
	return Math.min(Math.max(min, num), max);
}

function hexDouble(num) {
	var str = num.toString(16).toUpperCase();
	return (str.length < 2) ? '0' + str : str;
}


/***/ }),
/* 18 */
/***/ (function(module, exports, __nested_webpack_require_143863__) {

"use strict";


var isArrayish = __nested_webpack_require_143863__(19);

var concat = Array.prototype.concat;
var slice = Array.prototype.slice;

var swizzle = module.exports = function swizzle(args) {
	var results = [];

	for (var i = 0, len = args.length; i < len; i++) {
		var arg = args[i];

		if (isArrayish(arg)) {
			// http://jsperf.com/javascript-array-concat-vs-push/98
			results = concat.call(results, slice.call(arg));
		} else {
			results.push(arg);
		}
	}

	return results;
};

swizzle.wrap = function (fn) {
	return function () {
		return fn(swizzle(arguments));
	};
};


/***/ }),
/* 19 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function isArrayish(obj) {
	if (!obj) {
		return false;
	}

	return obj instanceof Array || Array.isArray(obj) ||
		(obj.length >= 0 && obj.splice instanceof Function);
};


/***/ }),
/* 20 */
/***/ (function(module, exports, __nested_webpack_require_144797__) {

var conversions = __nested_webpack_require_144797__(6);
var route = __nested_webpack_require_144797__(21);

var convert = {};

var models = Object.keys(conversions);

function wrapRaw(fn) {
	var wrappedFn = function (args) {
		if (args === undefined || args === null) {
			return args;
		}

		if (arguments.length > 1) {
			args = Array.prototype.slice.call(arguments);
		}

		return fn(args);
	};

	// preserve .conversion property if there is one
	if ('conversion' in fn) {
		wrappedFn.conversion = fn.conversion;
	}

	return wrappedFn;
}

function wrapRounded(fn) {
	var wrappedFn = function (args) {
		if (args === undefined || args === null) {
			return args;
		}

		if (arguments.length > 1) {
			args = Array.prototype.slice.call(arguments);
		}

		var result = fn(args);

		// we're assuming the result is an array here.
		// see notice in conversions.js; don't use box types
		// in conversion functions.
		if (typeof result === 'object') {
			for (var len = result.length, i = 0; i < len; i++) {
				result[i] = Math.round(result[i]);
			}
		}

		return result;
	};

	// preserve .conversion property if there is one
	if ('conversion' in fn) {
		wrappedFn.conversion = fn.conversion;
	}

	return wrappedFn;
}

models.forEach(function (fromModel) {
	convert[fromModel] = {};

	Object.defineProperty(convert[fromModel], 'channels', {value: conversions[fromModel].channels});
	Object.defineProperty(convert[fromModel], 'labels', {value: conversions[fromModel].labels});

	var routes = route(fromModel);
	var routeModels = Object.keys(routes);

	routeModels.forEach(function (toModel) {
		var fn = routes[toModel];

		convert[fromModel][toModel] = wrapRounded(fn);
		convert[fromModel][toModel].raw = wrapRaw(fn);
	});
});

module.exports = convert;


/***/ }),
/* 21 */
/***/ (function(module, exports, __nested_webpack_require_146603__) {

var conversions = __nested_webpack_require_146603__(6);

/*
	this function routes a model to all other models.

	all functions that are routed have a property `.conversion` attached
	to the returned synthetic function. This property is an array
	of strings, each with the steps in between the 'from' and 'to'
	color models (inclusive).

	conversions that are not possible simply are not included.
*/

function buildGraph() {
	var graph = {};
	// https://jsperf.com/object-keys-vs-for-in-with-closure/3
	var models = Object.keys(conversions);

	for (var len = models.length, i = 0; i < len; i++) {
		graph[models[i]] = {
			// http://jsperf.com/1-vs-infinity
			// micro-opt, but this is simple.
			distance: -1,
			parent: null
		};
	}

	return graph;
}

// https://en.wikipedia.org/wiki/Breadth-first_search
function deriveBFS(fromModel) {
	var graph = buildGraph();
	var queue = [fromModel]; // unshift -> queue -> pop

	graph[fromModel].distance = 0;

	while (queue.length) {
		var current = queue.pop();
		var adjacents = Object.keys(conversions[current]);

		for (var len = adjacents.length, i = 0; i < len; i++) {
			var adjacent = adjacents[i];
			var node = graph[adjacent];

			if (node.distance === -1) {
				node.distance = graph[current].distance + 1;
				node.parent = current;
				queue.unshift(adjacent);
			}
		}
	}

	return graph;
}

function link(from, to) {
	return function (args) {
		return to(from(args));
	};
}

function wrapConversion(toModel, graph) {
	var path = [graph[toModel].parent, toModel];
	var fn = conversions[graph[toModel].parent][toModel];

	var cur = graph[toModel].parent;
	while (graph[cur].parent) {
		path.unshift(graph[cur].parent);
		fn = link(conversions[graph[cur].parent][cur], fn);
		cur = graph[cur].parent;
	}

	fn.conversion = path;
	return fn;
}

module.exports = function (fromModel) {
	var graph = deriveBFS(fromModel);
	var conversion = {};

	var models = Object.keys(graph);
	for (var len = models.length, i = 0; i < len; i++) {
		var toModel = models[i];
		var node = graph[toModel];

		if (node.parent === null) {
			// no possible conversion, or this node is the source model.
			continue;
		}

		conversion[toModel] = wrapConversion(toModel, graph);
	}

	return conversion;
};



/***/ }),
/* 22 */
/***/ (function(module, exports, __nested_webpack_require_148906__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _jquery = __nested_webpack_require_148906__(0);

var _jquery2 = _interopRequireDefault(_jquery);

var _ColorItem = __nested_webpack_require_148906__(2);

var _ColorItem2 = _interopRequireDefault(_ColorItem);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * Handles everything related to the colorpicker color
 * @ignore
 */
var ColorHandler = function () {
  /**
   * @param {Colorpicker} colorpicker
   */
  function ColorHandler(colorpicker) {
    _classCallCheck(this, ColorHandler);

    /**
     * @type {Colorpicker}
     */
    this.colorpicker = colorpicker;
  }

  /**
   * @returns {*|String|ColorItem}
   */


  _createClass(ColorHandler, [{
    key: 'bind',
    value: function bind() {
      // if the color option is set
      if (this.colorpicker.options.color) {
        this.color = this.createColor(this.colorpicker.options.color);
        return;
      }

      // if element[color] is empty and the input has a value
      if (!this.color && !!this.colorpicker.inputHandler.getValue()) {
        this.color = this.createColor(this.colorpicker.inputHandler.getValue(), this.colorpicker.options.autoInputFallback);
      }
    }
  }, {
    key: 'unbind',
    value: function unbind() {
      this.colorpicker.element.removeData('color');
    }

    /**
     * Returns the color string from the input value or the 'data-color' attribute of the input or element.
     * If empty, it returns the defaultValue parameter.
     *
     * @returns {String|*}
     */

  }, {
    key: 'getColorString',
    value: function getColorString() {
      if (!this.hasColor()) {
        return '';
      }

      return this.color.string(this.format);
    }

    /**
     * Sets the color value
     *
     * @param {String|ColorItem} val
     */

  }, {
    key: 'setColorString',
    value: function setColorString(val) {
      var color = val ? this.createColor(val) : null;

      this.color = color ? color : null;
    }

    /**
     * Creates a new color using the widget instance options (fallbackColor, format).
     *
     * @fires Colorpicker#colorpickerInvalid
     * @param {*} val
     * @param {boolean} fallbackOnInvalid
     * @returns {ColorItem}
     */

  }, {
    key: 'createColor',
    value: function createColor(val) {
      var fallbackOnInvalid = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

      var color = new _ColorItem2.default(this.resolveColorDelegate(val), this.format);

      if (!color.isValid()) {
        if (fallbackOnInvalid) {
          color = this.getFallbackColor();
        }

        /**
         * (Colorpicker) Fired when the color is invalid and the fallback color is going to be used.
         *
         * @event Colorpicker#colorpickerInvalid
         */
        this.colorpicker.trigger('colorpickerInvalid', color, val);
      }

      if (!this.isAlphaEnabled()) {
        // Alpha is disabled
        color.alpha = 1;
      }

      return color;
    }
  }, {
    key: 'getFallbackColor',
    value: function getFallbackColor() {
      if (this.fallback && this.fallback === this.color) {
        return this.color;
      }

      var fallback = this.resolveColorDelegate(this.fallback);

      var color = new _ColorItem2.default(fallback, this.format);

      if (!color.isValid()) {
        console.warn('The fallback color is invalid. Falling back to the previous color or black if any.');
        return this.color ? this.color : new _ColorItem2.default('#000000', this.format);
      }

      return color;
    }

    /**
     * @returns {ColorItem}
     */

  }, {
    key: 'assureColor',
    value: function assureColor() {
      if (!this.hasColor()) {
        this.color = this.getFallbackColor();
      }

      return this.color;
    }

    /**
     * Delegates the color resolution to the colorpicker extensions.
     *
     * @param {String|*} color
     * @param {boolean} realColor if true, the color should resolve into a real (not named) color code
     * @returns {ColorItem|String|*|null}
     */

  }, {
    key: 'resolveColorDelegate',
    value: function resolveColorDelegate(color) {
      var realColor = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

      var extResolvedColor = false;

      _jquery2.default.each(this.colorpicker.extensions, function (name, ext) {
        if (extResolvedColor !== false) {
          // skip if resolved
          return;
        }
        extResolvedColor = ext.resolveColor(color, realColor);
      });

      return extResolvedColor ? extResolvedColor : color;
    }

    /**
     * Checks if there is a color object, that it is valid and it is not a fallback
     * @returns {boolean}
     */

  }, {
    key: 'isInvalidColor',
    value: function isInvalidColor() {
      return !this.hasColor() || !this.color.isValid();
    }

    /**
     * Returns true if the useAlpha option is exactly true, false otherwise
     * @returns {boolean}
     */

  }, {
    key: 'isAlphaEnabled',
    value: function isAlphaEnabled() {
      return this.colorpicker.options.useAlpha !== false;
    }

    /**
     * Returns true if the current color object is an instance of Color, false otherwise.
     * @returns {boolean}
     */

  }, {
    key: 'hasColor',
    value: function hasColor() {
      return this.color instanceof _ColorItem2.default;
    }
  }, {
    key: 'fallback',
    get: function get() {
      return this.colorpicker.options.fallbackColor ? this.colorpicker.options.fallbackColor : this.hasColor() ? this.color : null;
    }

    /**
     * @returns {String|null}
     */

  }, {
    key: 'format',
    get: function get() {
      if (this.colorpicker.options.format) {
        return this.colorpicker.options.format;
      }

      if (this.hasColor() && this.color.hasTransparency() && this.color.format.match(/^hex/)) {
        return this.isAlphaEnabled() ? 'rgba' : 'hex';
      }

      if (this.hasColor()) {
        return this.color.format;
      }

      return 'rgb';
    }

    /**
     * Internal color getter
     *
     * @type {ColorItem|null}
     */

  }, {
    key: 'color',
    get: function get() {
      return this.colorpicker.element.data('color');
    }

    /**
     * Internal color setter
     *
     * @ignore
     * @param {ColorItem|null} value
     */
    ,
    set: function set(value) {
      this.colorpicker.element.data('color', value);

      if (value instanceof _ColorItem2.default && this.colorpicker.options.format === 'auto') {
        // If format is 'auto', use the first parsed one from now on
        this.colorpicker.options.format = this.color.format;
      }
    }
  }]);

  return ColorHandler;
}();

exports.default = ColorHandler;
module.exports = exports.default;

/***/ }),
/* 23 */
/***/ (function(module, exports, __nested_webpack_require_156531__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _jquery = __nested_webpack_require_156531__(0);

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * Handles everything related to the colorpicker UI
 * @ignore
 */
var PickerHandler = function () {
  /**
   * @param {Colorpicker} colorpicker
   */
  function PickerHandler(colorpicker) {
    _classCallCheck(this, PickerHandler);

    /**
     * @type {Colorpicker}
     */
    this.colorpicker = colorpicker;
    /**
     * @type {jQuery}
     */
    this.picker = null;
  }

  _createClass(PickerHandler, [{
    key: 'bind',
    value: function bind() {
      /**
       * @type {jQuery|HTMLElement}
       */
      var picker = this.picker = (0, _jquery2.default)(this.options.template);

      if (this.options.customClass) {
        picker.addClass(this.options.customClass);
      }

      if (this.options.horizontal) {
        picker.addClass('colorpicker-horizontal');
      }

      if (this._supportsAlphaBar()) {
        this.options.useAlpha = true;
        picker.addClass('colorpicker-with-alpha');
      } else {
        this.options.useAlpha = false;
      }
    }
  }, {
    key: 'attach',
    value: function attach() {
      // Inject the colorpicker element into the DOM
      var pickerParent = this.colorpicker.container ? this.colorpicker.container : null;

      if (pickerParent) {
        this.picker.appendTo(pickerParent);
      }
    }
  }, {
    key: 'unbind',
    value: function unbind() {
      this.picker.remove();
    }
  }, {
    key: '_supportsAlphaBar',
    value: function _supportsAlphaBar() {
      return (this.options.useAlpha || this.colorpicker.colorHandler.hasColor() && this.color.hasTransparency()) && this.options.useAlpha !== false && (!this.options.format || this.options.format && !this.options.format.match(/^hex([36])?$/i));
    }

    /**
     * Changes the color adjustment bars using the current color object information.
     */

  }, {
    key: 'update',
    value: function update() {
      if (!this.colorpicker.colorHandler.hasColor()) {
        return;
      }

      var vertical = this.options.horizontal !== true,
          slider = vertical ? this.options.sliders : this.options.slidersHorz;

      var saturationGuide = this.picker.find('.colorpicker-saturation .colorpicker-guide'),
          hueGuide = this.picker.find('.colorpicker-hue .colorpicker-guide'),
          alphaGuide = this.picker.find('.colorpicker-alpha .colorpicker-guide');

      var hsva = this.color.toHsvaRatio();

      // Set guides position
      if (hueGuide.length) {
        hueGuide.css(vertical ? 'top' : 'left', (vertical ? slider.hue.maxTop : slider.hue.maxLeft) * (1 - hsva.h));
      }
      if (alphaGuide.length) {
        alphaGuide.css(vertical ? 'top' : 'left', (vertical ? slider.alpha.maxTop : slider.alpha.maxLeft) * (1 - hsva.a));
      }
      if (saturationGuide.length) {
        saturationGuide.css({
          'top': slider.saturation.maxTop - hsva.v * slider.saturation.maxTop,
          'left': hsva.s * slider.saturation.maxLeft
        });
      }

      // Set saturation hue background
      this.picker.find('.colorpicker-saturation').css('backgroundColor', this.color.getCloneHueOnly().toHexString()); // we only need hue

      // Set alpha color gradient
      var hexColor = this.color.toHexString();

      var alphaBg = '';

      if (this.options.horizontal) {
        alphaBg = 'linear-gradient(to right, ' + hexColor + ' 0%, transparent 100%)';
      } else {
        alphaBg = 'linear-gradient(to bottom, ' + hexColor + ' 0%, transparent 100%)';
      }

      this.picker.find('.colorpicker-alpha-color').css('background', alphaBg);
    }
  }, {
    key: 'options',
    get: function get() {
      return this.colorpicker.options;
    }
  }, {
    key: 'color',
    get: function get() {
      return this.colorpicker.colorHandler.color;
    }
  }]);

  return PickerHandler;
}();

exports.default = PickerHandler;
module.exports = exports.default;

/***/ }),
/* 24 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * Handles everything related to the colorpicker addon
 * @ignore
 */

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var AddonHandler = function () {
  /**
   * @param {Colorpicker} colorpicker
   */
  function AddonHandler(colorpicker) {
    _classCallCheck(this, AddonHandler);

    /**
     * @type {Colorpicker}
     */
    this.colorpicker = colorpicker;
    /**
     * @type {jQuery}
     */
    this.addon = null;
  }

  _createClass(AddonHandler, [{
    key: 'hasAddon',
    value: function hasAddon() {
      return !!this.addon;
    }
  }, {
    key: 'bind',
    value: function bind() {
      /**
       * @type {*|jQuery}
       */
      this.addon = this.colorpicker.options.addon ? this.colorpicker.element.find(this.colorpicker.options.addon) : null;

      if (this.addon && this.addon.length === 0) {
        // not found
        this.addon = null;
      }
    }
  }, {
    key: 'unbind',
    value: function unbind() {
      if (this.hasAddon()) {
        this.addon.off('.colorpicker');
      }
    }

    /**
     * If the addon element is present, its background color is updated
     */

  }, {
    key: 'update',
    value: function update() {
      if (!this.colorpicker.colorHandler.hasColor() || !this.hasAddon()) {
        return;
      }

      var colorStr = this.colorpicker.colorHandler.getColorString();

      var styles = { 'background': colorStr };

      var icn = this.addon.find('i').eq(0);

      if (icn.length > 0) {
        icn.css(styles);
      } else {
        this.addon.css(styles);
      }
    }
  }]);

  return AddonHandler;
}();

exports.default = AddonHandler;
module.exports = exports.default;

/***/ })
/******/ ]);
});
//# sourceMappingURL=bootstrap-colorpicker.js.map
}.call(window, false, false));


/***/ }),

/***/ "jquery":
/*!*************************!*\
  !*** external "jQuery" ***!
  \*************************/
/***/ ((module) => {

"use strict";
module.exports = window["jQuery"];

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be in strict mode.
(() => {
"use strict";
/*!**********************************************!*\
  !*** ./js/pages/order-return-states/form.js ***!
  \**********************************************/


var _colorpicker = __webpack_require__(/*! @app/utils/colorpicker */ "./js/app/utils/colorpicker.js");

var _colorpicker2 = _interopRequireDefault(_colorpicker);

var _translatableInput = __webpack_require__(/*! @components/translatable-input */ "./js/components/translatable-input.js");

var _translatableInput2 = _interopRequireDefault(_translatableInput);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 */

var _window = window,
    $ = _window.$;


$(function () {
  (0, _colorpicker2.default)();
  new _translatableInput2.default();
});
})();

window.order_return_states_form = __webpack_exports__;
/******/ })()
;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9bbmFtZV0vLi9qcy9hcHAvdXRpbHMvY29sb3JwaWNrZXIuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vanMvY29tcG9uZW50cy9ldmVudC1lbWl0dGVyLmpzIiwid2VicGFjazovL1tuYW1lXS8uL2pzL2NvbXBvbmVudHMvdHJhbnNsYXRhYmxlLWlucHV0LmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9iYWJlbC1ydW50aW1lL2NvcmUtanMvb2JqZWN0L2RlZmluZS1wcm9wZXJ0eS5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvYmFiZWwtcnVudGltZS9oZWxwZXJzL2NsYXNzQ2FsbENoZWNrLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9iYWJlbC1ydW50aW1lL2hlbHBlcnMvY3JlYXRlQ2xhc3MuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9mbi9vYmplY3QvZGVmaW5lLXByb3BlcnR5LmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fYS1mdW5jdGlvbi5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX2FuLW9iamVjdC5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX2NvcmUuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19jdHguanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19kZXNjcmlwdG9ycy5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX2RvbS1jcmVhdGUuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19leHBvcnQuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19mYWlscy5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX2dsb2JhbC5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX2hhcy5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX2hpZGUuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19pZTgtZG9tLWRlZmluZS5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX2lzLW9iamVjdC5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX29iamVjdC1kcC5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX3Byb3BlcnR5LWRlc2MuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL190by1wcmltaXRpdmUuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL2VzNi5vYmplY3QuZGVmaW5lLXByb3BlcnR5LmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9ldmVudHMvZXZlbnRzLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9ib290c3RyYXAtY29sb3JwaWNrZXIvZGlzdC9qcy9ib290c3RyYXAtY29sb3JwaWNrZXIuanMiLCJ3ZWJwYWNrOi8vW25hbWVdL2V4dGVybmFsIFwialF1ZXJ5XCIiLCJ3ZWJwYWNrOi8vW25hbWVdL3dlYnBhY2svYm9vdHN0cmFwIiwid2VicGFjazovL1tuYW1lXS8uL2pzL3BhZ2VzL29yZGVyLXJldHVybi1zdGF0ZXMvZm9ybS5qcyJdLCJuYW1lcyI6WyJ3aW5kb3ciLCIkIiwiaW5pdCIsImluaXREYXRlUGlja2VycyIsImVhY2giLCJpIiwicGlja2VyIiwiY29sb3JwaWNrZXIiLCJFdmVudEVtaXR0ZXIiLCJFdmVudEVtaXR0ZXJDbGFzcyIsIlRyYW5zbGF0YWJsZUlucHV0Iiwib3B0aW9ucyIsIm9wdHMiLCJsb2NhbGVJdGVtU2VsZWN0b3IiLCJsb2NhbGVCdXR0b25TZWxlY3RvciIsImxvY2FsZUlucHV0U2VsZWN0b3IiLCJzZWxlY3RlZExvY2FsZSIsImRhdGEiLCJvbiIsInRvZ2dsZUxhbmd1YWdlIiwiYmluZCIsInRvZ2dsZUlucHV0cyIsInJlZnJlc2hGb3JtSW5wdXRzIiwiZm9ybSIsInJlZnJlc2hJbnB1dHMiLCJnZXRTZWxlY3RlZExvY2FsZSIsImVtaXQiLCJldmVudCIsImxvY2FsZUl0ZW0iLCJ0YXJnZXQiLCJjbG9zZXN0IiwibG9jYWxlQnV0dG9uIiwiZmluZCIsImNoYW5nZUxhbmd1YWdlVXJsIiwidGV4dCIsImFkZENsYXNzIiwicmVtb3ZlQ2xhc3MiLCJzYXZlU2VsZWN0ZWRMYW5ndWFnZSIsInBvc3QiLCJ1cmwiLCJsYW5ndWFnZV9pc29fY29kZSJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7OztBQXdCQTs7Y0FFWUEsTTtJQUFMQyxDLFdBQUFBLEM7O0FBRVA7OztBQTVCQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQStCQSxJQUFNQyxPQUFPLFNBQVNDLGVBQVQsR0FBMkI7QUFDdENGLElBQUUsaUNBQUYsRUFBcUNHLElBQXJDLENBQTBDLFVBQUNDLENBQUQsRUFBSUMsTUFBSixFQUFlO0FBQ3ZETCxNQUFFSyxNQUFGLEVBQVVDLFdBQVY7QUFDRCxHQUZEO0FBR0QsQ0FKRDs7a0JBTWVMLEk7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ1pmOzs7Ozs7QUFFQTs7OztBQUlPLElBQU1NLGVBQWVBLHVCQUFBLElBQUlDLGdCQUFKLEVBQXJCLEMsQ0EvQlA7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7a0JBaUNlRCxZOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDUmY7Ozs7Y0FFWVIsTTtJQUFMQyxDLFdBQUFBLEM7O0FBRVA7Ozs7O0FBN0JBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0lBa0NNUyxpQjtBQUNKLDZCQUFZQyxPQUFaLEVBQXFCO0FBQUE7O0FBQUE7O0FBQ25CLFFBQU1DLE9BQU9ELFdBQVcsRUFBeEI7O0FBRUEsU0FBS0Usa0JBQUwsR0FBMEJELEtBQUtDLGtCQUFMLElBQTJCLGlCQUFyRDtBQUNBLFNBQUtDLG9CQUFMLEdBQTRCRixLQUFLRSxvQkFBTCxJQUE2QixnQkFBekQ7QUFDQSxTQUFLQyxtQkFBTCxHQUEyQkgsS0FBS0csbUJBQUwsSUFBNEIsa0JBQXZEO0FBQ0EsU0FBS0MsY0FBTCxHQUFzQmYsRUFBRSxLQUFLWSxrQkFBUCxFQUEyQkksSUFBM0IsQ0FBZ0MsUUFBaEMsQ0FBdEI7O0FBRUFoQixNQUFFLE1BQUYsRUFBVWlCLEVBQVYsQ0FDRSxPQURGLEVBRUUsS0FBS0wsa0JBRlAsRUFHRSxLQUFLTSxjQUFMLENBQW9CQyxJQUFwQixDQUF5QixJQUF6QixDQUhGO0FBS0FaLCtCQUFhVSxFQUFiLENBQWdCLGtCQUFoQixFQUFvQyxLQUFLRyxZQUFMLENBQWtCRCxJQUFsQixDQUF1QixJQUF2QixDQUFwQzs7QUFFQSxXQUFPO0FBQ0xQLDBCQUFvQixLQUFLQSxrQkFEcEI7QUFFTEMsNEJBQXNCLEtBQUtBLG9CQUZ0QjtBQUdMQywyQkFBcUIsS0FBS0EsbUJBSHJCOztBQUtMOzs7QUFHQU8seUJBQW1CLDJCQUFDQyxJQUFELEVBQVU7QUFBRSxjQUFLQyxhQUFMLENBQW1CRCxJQUFuQjtBQUEyQixPQVJyRDs7QUFVTDs7O0FBR0FFLHlCQUFtQjtBQUFBLGVBQU0sTUFBS1QsY0FBWDtBQUFBO0FBYmQsS0FBUDtBQWVEOztBQUVEOzs7Ozs7Ozs7a0NBS2NPLEksRUFBTTtBQUNsQixVQUFJLENBQUMsS0FBS1AsY0FBVixFQUEwQjtBQUN4QjtBQUNEOztBQUVEUixpQ0FBYWtCLElBQWIsQ0FBa0Isa0JBQWxCLEVBQXNDO0FBQ3BDVix3QkFBZ0IsS0FBS0EsY0FEZTtBQUVwQ087QUFGb0MsT0FBdEM7QUFJRDs7QUFFRDs7Ozs7Ozs7OzttQ0FPZUksSyxFQUFPO0FBQ3BCLFVBQU1DLGFBQWEzQixFQUFFMEIsTUFBTUUsTUFBUixDQUFuQjtBQUNBLFVBQU1OLE9BQU9LLFdBQVdFLE9BQVgsQ0FBbUIsTUFBbkIsQ0FBYjtBQUNBLFdBQUtkLGNBQUwsR0FBc0JZLFdBQVdYLElBQVgsQ0FBZ0IsUUFBaEIsQ0FBdEI7QUFDQSxXQUFLTyxhQUFMLENBQW1CRCxJQUFuQjtBQUNEOztBQUVEOzs7Ozs7Ozs7O2lDQU9hSSxLLEVBQU87QUFBQSxVQUNYSixJQURXLEdBQ0hJLEtBREcsQ0FDWEosSUFEVzs7QUFFbEIsV0FBS1AsY0FBTCxHQUFzQlcsTUFBTVgsY0FBNUI7QUFDQSxVQUFNZSxlQUFlUixLQUFLUyxJQUFMLENBQVUsS0FBS2xCLG9CQUFmLENBQXJCO0FBQ0EsVUFBTW1CLG9CQUFvQkYsYUFBYWQsSUFBYixDQUFrQixxQkFBbEIsQ0FBMUI7O0FBRUFjLG1CQUFhRyxJQUFiLENBQWtCLEtBQUtsQixjQUF2QjtBQUNBTyxXQUFLUyxJQUFMLENBQVUsS0FBS2pCLG1CQUFmLEVBQW9Db0IsUUFBcEMsQ0FBNkMsUUFBN0M7QUFDQVosV0FDR1MsSUFESCxDQUNXLEtBQUtqQixtQkFEaEIsbUJBQ2lELEtBQUtDLGNBRHRELEVBRUdvQixXQUZILENBRWUsUUFGZjs7QUFJQSxVQUFJSCxpQkFBSixFQUF1QjtBQUNyQixhQUFLSSxvQkFBTCxDQUEwQkosaUJBQTFCLEVBQTZDLEtBQUtqQixjQUFsRDtBQUNEO0FBQ0Y7O0FBRUQ7Ozs7Ozs7Ozs7O3lDQVFxQmlCLGlCLEVBQW1CakIsYyxFQUFnQjtBQUN0RGYsUUFBRXFDLElBQUYsQ0FBTztBQUNMQyxhQUFLTixpQkFEQTtBQUVMaEIsY0FBTTtBQUNKdUIsNkJBQW1CeEI7QUFEZjtBQUZELE9BQVA7QUFNRDs7Ozs7a0JBR1lOLGlCOzs7Ozs7Ozs7O0FDM0lmLGtCQUFrQixZQUFZLG1CQUFPLENBQUMsOEdBQTJDLHNCOzs7Ozs7Ozs7OztBQ0FwRTs7QUFFYixrQkFBa0I7O0FBRWxCLGVBQWU7QUFDZjtBQUNBO0FBQ0E7QUFDQSxFOzs7Ozs7Ozs7OztBQ1JhOztBQUViLGtCQUFrQjs7QUFFbEIsc0JBQXNCLG1CQUFPLENBQUMseUdBQW1DOztBQUVqRTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0YsZUFBZTtBQUNmO0FBQ0EsbUJBQW1CLGtCQUFrQjtBQUNyQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQyxHOzs7Ozs7Ozs7O0FDMUJELG1CQUFPLENBQUMsc0hBQTBDO0FBQ2xELGNBQWMsd0dBQXFDO0FBQ25EO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNKQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNIQSxlQUFlLG1CQUFPLENBQUMsMEVBQWM7QUFDckM7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7O0FDSkEsNkJBQTZCO0FBQzdCLHVDQUF1Qzs7Ozs7Ozs7Ozs7QUNEdkM7QUFDQSxnQkFBZ0IsbUJBQU8sQ0FBQyw0RUFBZTtBQUN2QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7O0FDbkJBO0FBQ0Esa0JBQWtCLG1CQUFPLENBQUMsa0VBQVU7QUFDcEMsaUNBQWlDLFFBQVEsbUJBQW1CLFVBQVUsRUFBRSxFQUFFO0FBQzFFLENBQUM7Ozs7Ozs7Ozs7O0FDSEQsZUFBZSxtQkFBTyxDQUFDLDBFQUFjO0FBQ3JDLGVBQWUsa0dBQTZCO0FBQzVDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7O0FDTkEsYUFBYSxtQkFBTyxDQUFDLG9FQUFXO0FBQ2hDLFdBQVcsbUJBQU8sQ0FBQyxnRUFBUztBQUM1QixVQUFVLG1CQUFPLENBQUMsOERBQVE7QUFDMUIsV0FBVyxtQkFBTyxDQUFDLGdFQUFTO0FBQzVCLFVBQVUsbUJBQU8sQ0FBQyw4REFBUTtBQUMxQjs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlFQUFpRTtBQUNqRTtBQUNBLGtGQUFrRjtBQUNsRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVztBQUNYLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsK0NBQStDO0FBQy9DO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGNBQWM7QUFDZCxjQUFjO0FBQ2QsY0FBYztBQUNkLGNBQWM7QUFDZCxlQUFlO0FBQ2YsZUFBZTtBQUNmLGVBQWU7QUFDZixnQkFBZ0I7QUFDaEI7Ozs7Ozs7Ozs7O0FDN0RBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7O0FDTkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHlDQUF5Qzs7Ozs7Ozs7Ozs7QUNMekMsdUJBQXVCO0FBQ3ZCO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNIQSxTQUFTLG1CQUFPLENBQUMsMEVBQWM7QUFDL0IsaUJBQWlCLG1CQUFPLENBQUMsa0ZBQWtCO0FBQzNDLGlCQUFpQixtQkFBTyxDQUFDLDhFQUFnQjtBQUN6QztBQUNBLENBQUM7QUFDRDtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7O0FDUEEsa0JBQWtCLG1CQUFPLENBQUMsOEVBQWdCLE1BQU0sbUJBQU8sQ0FBQyxrRUFBVTtBQUNsRSwrQkFBK0IsbUJBQU8sQ0FBQyw0RUFBZSxnQkFBZ0IsbUJBQW1CLFVBQVUsRUFBRSxFQUFFO0FBQ3ZHLENBQUM7Ozs7Ozs7Ozs7O0FDRkQ7QUFDQTtBQUNBOzs7Ozs7Ozs7OztBQ0ZBLGVBQWUsbUJBQU8sQ0FBQywwRUFBYztBQUNyQyxxQkFBcUIsbUJBQU8sQ0FBQyxvRkFBbUI7QUFDaEQsa0JBQWtCLG1CQUFPLENBQUMsZ0ZBQWlCO0FBQzNDOztBQUVBLFNBQVMsR0FBRyxtQkFBTyxDQUFDLDhFQUFnQjtBQUNwQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRyxZQUFZO0FBQ2Y7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7O0FDZkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNQQTtBQUNBLGVBQWUsbUJBQU8sQ0FBQywwRUFBYztBQUNyQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNYQSxjQUFjLG1CQUFPLENBQUMsb0VBQVc7QUFDakM7QUFDQSxpQ0FBaUMsbUJBQU8sQ0FBQyw4RUFBZ0IsY0FBYyxpQkFBaUIsaUdBQXlCLEVBQUU7Ozs7Ozs7Ozs7OztBQ0ZuSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVhOztBQUViO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLGlCQUFpQixzQkFBc0I7QUFDdkM7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZTtBQUNmO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsY0FBYztBQUNkOztBQUVBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0EsbUJBQW1CLFNBQVM7QUFDNUI7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQSxLQUFLO0FBQ0w7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLGVBQWU7QUFDZjtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDs7QUFFQSxpQ0FBaUMsUUFBUTtBQUN6QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsbUJBQW1CLGlCQUFpQjtBQUNwQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBLHNDQUFzQyxRQUFRO0FBQzlDO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxpQkFBaUIsT0FBTztBQUN4QjtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxRQUFRLHlCQUF5QjtBQUNqQztBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLGlCQUFpQixnQkFBZ0I7QUFDakM7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7O0FDN2JBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsbUNBQW1DLFFBQWE7QUFDaEQsMkJBQTJCLG1CQUFPLENBQUMsc0JBQVE7QUFDM0M7QUFDQTtBQUNBO0FBQ0EsNkNBQTZDLG1CQUFPLENBQUMsc0JBQVE7QUFDN0Q7QUFDQTtBQUNBLENBQUM7QUFDRCxvQ0FBb0M7QUFDcEM7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtQkFBbUIsK0JBQW1CO0FBQ3RDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsMEVBQTBFLCtCQUFtQjtBQUM3RjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFVBQVUsK0JBQW1CO0FBQzdCO0FBQ0E7QUFDQSxVQUFVLCtCQUFtQjtBQUM3QjtBQUNBO0FBQ0EsVUFBVSwrQkFBbUI7QUFDN0IsZUFBZSwrQkFBbUI7QUFDbEMsa0RBQWtELGdDQUFnQztBQUNsRjtBQUNBO0FBQ0E7QUFDQTtBQUNBLFVBQVUsK0JBQW1CO0FBQzdCO0FBQ0EsZ0VBQWdFLGtCQUFrQjtBQUNsRjtBQUNBLHlEQUF5RCxjQUFjO0FBQ3ZFO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsVUFBVSwrQkFBbUI7QUFDN0IsZ0NBQWdDLCtCQUFtQjtBQUNuRDtBQUNBO0FBQ0E7QUFDQSxXQUFXLCtCQUFtQjtBQUM5QixpREFBaUQsaUNBQWlDO0FBQ2xGLDBFQUEwRSwrQkFBbUIsMkJBQTJCLG1CQUFtQixFQUFFO0FBQzdJO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsVUFBVSwrQkFBbUI7QUFDN0I7QUFDQSxtQ0FBbUMsMEJBQTBCLEVBQUU7QUFDL0QseUNBQXlDLGVBQWU7QUFDeEQsV0FBVywrQkFBbUI7QUFDOUI7QUFDQTtBQUNBO0FBQ0E7QUFDQSxVQUFVLCtCQUFtQixpQ0FBaUMsK0RBQStEO0FBQzdIO0FBQ0E7QUFDQSxVQUFVLCtCQUFtQjtBQUM3QjtBQUNBO0FBQ0E7QUFDQSxpQkFBaUIsK0JBQW1CLENBQUMsK0JBQW1CO0FBQ3hELFVBQVU7QUFDVjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQSxPQUFPO0FBQ1A7QUFDQSxpQ0FBaUMsK0JBQW1COztBQUVwRDs7O0FBR0E7QUFDQTtBQUNBLENBQUM7O0FBRUQsZ0NBQWdDLDJDQUEyQyxnQkFBZ0Isa0JBQWtCLE9BQU8sMkJBQTJCLHdEQUF3RCxnQ0FBZ0MsdURBQXVELDJEQUEyRCxFQUFFLEVBQUUseURBQXlELHFFQUFxRSw2REFBNkQsb0JBQW9CLEdBQUcsRUFBRTs7QUFFampCLGNBQWMsK0JBQW1COztBQUVqQzs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0YsaURBQWlELDBDQUEwQywwREFBMEQsRUFBRTs7QUFFdko7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWEsWUFBWTtBQUN6QixhQUFhLE9BQU87QUFDcEI7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQSxjQUFjO0FBQ2Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGNBQWM7QUFDZDtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYSxtQkFBbUI7QUFDaEMsYUFBYSxRQUFRO0FBQ3JCLGNBQWM7QUFDZDs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWUsTUFBTTtBQUNyQjs7QUFFQSxHQUFHO0FBQ0g7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWUsTUFBTTtBQUNyQjs7QUFFQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlLE1BQU07QUFDckI7O0FBRUEsR0FBRztBQUNIO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlLE1BQU07QUFDckI7O0FBRUEsR0FBRztBQUNIO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlLE1BQU07QUFDckI7O0FBRUEsR0FBRztBQUNIO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlLE1BQU07QUFDckI7O0FBRUEsR0FBRztBQUNIO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlLE1BQU07QUFDckI7O0FBRUEsR0FBRztBQUNIO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlLE1BQU07QUFDckI7O0FBRUEsR0FBRztBQUNIO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlLE1BQU07QUFDckI7O0FBRUEsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRzs7QUFFSDtBQUNBLENBQUM7O0FBRUQ7QUFDQTs7QUFFQSxPQUFPO0FBQ1A7QUFDQSxpQ0FBaUMsZ0NBQW1COztBQUVwRDs7O0FBR0E7QUFDQTtBQUNBLENBQUM7QUFDRDs7QUFFQSxnQ0FBZ0MsMkNBQTJDLGdCQUFnQixrQkFBa0IsT0FBTywyQkFBMkIsd0RBQXdELGdDQUFnQyx1REFBdUQsMkRBQTJELEVBQUUsRUFBRSx5REFBeUQscUVBQXFFLDZEQUE2RCxvQkFBb0IsR0FBRyxFQUFFLEdBQUc7QUFDcGpCO0FBQ0E7OztBQUdBLGFBQWEsZ0NBQW1COztBQUVoQzs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0YsaURBQWlELDBDQUEwQywwREFBMEQsRUFBRTs7QUFFdko7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYSxXQUFXO0FBQ3hCLGFBQWEsV0FBVztBQUN4QixhQUFhLFdBQVc7QUFDeEIsYUFBYSxXQUFXO0FBQ3hCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHOztBQUVIO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZSxPQUFPO0FBQ3RCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7QUFDQTtBQUNBLHdGQUF3RixhQUFhO0FBQ3JHO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxrQkFBa0I7QUFDbEI7O0FBRUEsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0EsZUFBZSwyQ0FBMkM7QUFDMUQsZUFBZSxZQUFZO0FBQzNCOztBQUVBLEdBQUc7QUFDSDs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7O0FBRUg7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLDJDQUEyQztBQUN4RCxhQUFhLFlBQVk7QUFDekI7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBLGlCQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZ0JBQWdCO0FBQ2hCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0EsZ0JBQWdCO0FBQ2hCO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZSwyQ0FBMkM7QUFDMUQ7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjs7QUFFQSxHQUFHO0FBQ0g7OztBQUdBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7O0FBRUEsR0FBRztBQUNIOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWUsT0FBTztBQUN0QjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxlQUFlLElBQUk7QUFDbkI7O0FBRUEsR0FBRztBQUNIOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWUsT0FBTztBQUN0QjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxlQUFlLElBQUk7QUFDbkI7O0FBRUEsR0FBRztBQUNIOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWUsT0FBTztBQUN0QjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxlQUFlLElBQUk7QUFDbkI7O0FBRUEsR0FBRztBQUNIOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWUsT0FBTztBQUN0QjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxlQUFlLE9BQU87QUFDdEI7O0FBRUEsR0FBRztBQUNIOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCOztBQUVBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7O0FBRUEsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjs7QUFFQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCOztBQUVBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7O0FBRUEsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7O0FBRUEsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCOztBQUVBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWUsWUFBWTtBQUMzQixpQkFBaUI7QUFDakI7O0FBRUEsR0FBRztBQUNIO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWUsMkNBQTJDO0FBQzFEO0FBQ0EsaUJBQWlCO0FBQ2pCOztBQUVBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCOztBQUVBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjs7QUFFQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCOztBQUVBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7O0FBRUEsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjs7QUFFQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCOztBQUVBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjs7QUFFQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7O0FBRUEsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZSxnQkFBZ0I7QUFDL0I7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjs7QUFFQSxHQUFHO0FBQ0g7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQSxPQUFPO0FBQ1A7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBLE9BQU87O0FBRVA7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0EsZUFBZSxJQUFJO0FBQ25CO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQixPQUFPO0FBQ3hCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZSxTQUFTO0FBQ3hCO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7O0FBRUEsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsK0JBQStCLEdBQUc7QUFDbEM7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxlQUFlLE9BQU87QUFDdEIsK0NBQStDO0FBQy9DLDBDQUEwQztBQUMxQywyQ0FBMkM7QUFDM0M7QUFDQSxpQkFBaUI7QUFDakI7O0FBRUEsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsc0NBQXNDLEdBQUc7QUFDekM7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlLFNBQVM7QUFDeEIsaURBQWlEO0FBQ2pELHdDQUF3QztBQUN4QywyQ0FBMkM7QUFDM0M7QUFDQSxpQkFBaUIsT0FBTztBQUN4Qjs7QUFFQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHOztBQUVIO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVc7QUFDWDs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQSxPQUFPO0FBQ1A7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBLENBQUM7QUFDRDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsWUFBWTtBQUNaO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFlBQVk7QUFDWjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFlBQVk7QUFDWjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFlBQVk7QUFDWjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsWUFBWTtBQUNaO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFlBQVk7QUFDWjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsWUFBWTtBQUNaO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsWUFBWTtBQUNaO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0EsWUFBWTtBQUNaO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFlBQVk7QUFDWjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFlBQVk7QUFDWjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsWUFBWTtBQUNaO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxZQUFZO0FBQ1o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsWUFBWTtBQUNaO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxZQUFZO0FBQ1o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFlBQVk7QUFDWjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsY0FBYztBQUNkO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQSxZQUFZO0FBQ1o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0EsWUFBWTtBQUNaO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsT0FBTztBQUNQO0FBQ0EsaUNBQWlDLGdDQUFtQjs7QUFFcEQ7OztBQUdBO0FBQ0E7QUFDQSxDQUFDOztBQUVELG9HQUFvRyxtQkFBbUIsRUFBRSxtQkFBbUIsOEhBQThIOztBQUUxUSxnQ0FBZ0MsMkNBQTJDLGdCQUFnQixrQkFBa0IsT0FBTywyQkFBMkIsd0RBQXdELGdDQUFnQyx1REFBdUQsMkRBQTJELEVBQUUsRUFBRSx5REFBeUQscUVBQXFFLDZEQUE2RCxvQkFBb0IsR0FBRyxFQUFFOztBQUVqakIsa0JBQWtCLGdDQUFtQjs7QUFFckM7O0FBRUEsY0FBYyxnQ0FBbUI7O0FBRWpDOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixpREFBaUQsMENBQTBDLDBEQUEwRCxFQUFFOztBQUV2SixpREFBaUQsYUFBYSx1RkFBdUYsRUFBRSx1RkFBdUY7O0FBRTlPLDBDQUEwQywrREFBK0QscUdBQXFHLEVBQUUseUVBQXlFLGVBQWUseUVBQXlFLEVBQUUsRUFBRSx1SEFBdUg7O0FBRTVlO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFlBQVk7QUFDWjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxZQUFZO0FBQ1o7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOzs7QUFHQTtBQUNBLGlCQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7O0FBRUg7QUFDQTs7QUFFQTs7QUFFQSwrSkFBK0o7O0FBRS9KO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxlQUFlO0FBQ2Y7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGVBQWUsT0FBTztBQUN0QixlQUFlLEVBQUU7QUFDakIsaUJBQWlCO0FBQ2pCOztBQUVBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsZUFBZSxPQUFPO0FBQ3RCLGVBQWUsRUFBRTtBQUNqQixpQkFBaUI7QUFDakI7O0FBRUEsR0FBRztBQUNIO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRzs7QUFFSDtBQUNBLENBQUM7O0FBRUQ7QUFDQTs7QUFFQSxPQUFPO0FBQ1A7QUFDQTs7QUFFQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQSxPQUFPO0FBQ1A7QUFDQSxpQ0FBaUMsZ0NBQW1COztBQUVwRDtBQUNBLGtCQUFrQixnQ0FBbUI7O0FBRXJDO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxPQUFPLDJCQUEyQjtBQUNsQyxPQUFPLDJCQUEyQjtBQUNsQyxPQUFPLDJCQUEyQjtBQUNsQyxPQUFPLDJCQUEyQjtBQUNsQyxRQUFRLDRCQUE0QjtBQUNwQyxPQUFPLDJCQUEyQjtBQUNsQyxPQUFPLDJCQUEyQjtBQUNsQyxPQUFPLDJCQUEyQjtBQUNsQyxPQUFPLDZCQUE2QjtBQUNwQyxXQUFXLGlDQUFpQztBQUM1QyxVQUFVLGdDQUFnQztBQUMxQyxXQUFXLGlDQUFpQztBQUM1QyxPQUFPLHFDQUFxQztBQUM1QyxTQUFTLDJDQUEyQztBQUNwRCxRQUFRO0FBQ1I7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxREFBcUQsZ0JBQWdCO0FBQ3JFLG1EQUFtRCxjQUFjO0FBQ2pFO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEVBQUU7QUFDRjtBQUNBLEVBQUU7QUFDRjtBQUNBLEVBQUU7QUFDRjtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0EsRUFBRTtBQUNGO0FBQ0EsRUFBRTtBQUNGO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsRUFBRTtBQUNGO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEVBQUU7QUFDRjtBQUNBOztBQUVBOztBQUVBO0FBQ0EsZ0JBQWdCLE9BQU87QUFDdkI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBLHVCQUF1Qjs7QUFFdkI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZ0JBQWdCLE9BQU8sUUFBUTtBQUMvQixnQkFBZ0IsT0FBTyxRQUFRO0FBQy9CLGlCQUFpQixPQUFPLE9BQU87QUFDL0IsaUJBQWlCLE9BQU8sT0FBTztBQUMvQixnQkFBZ0IsUUFBUSxPQUFPO0FBQy9CLGdCQUFnQixRQUFRLE9BQU87QUFDL0I7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esc0VBQXNFOztBQUV0RTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQSxzQkFBc0I7QUFDdEI7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0EsK0NBQStDLEVBQUUsVUFBVSxFQUFFO0FBQzdEO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEVBQUU7QUFDRjtBQUNBOztBQUVBO0FBQ0E7QUFDQSxFQUFFO0FBQ0Y7QUFDQTtBQUNBLEVBQUU7QUFDRjtBQUNBO0FBQ0EsRUFBRTtBQUNGO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEVBQUU7QUFDRjtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxlQUFlLGFBQWEsYUFBYTtBQUN6QztBQUNBLGVBQWUsYUFBYSxhQUFhO0FBQ3pDO0FBQ0EsZUFBZSxhQUFhLGFBQWE7QUFDekM7QUFDQSxlQUFlLGFBQWEsYUFBYTtBQUN6QztBQUNBLGVBQWUsYUFBYSxhQUFhO0FBQ3pDO0FBQ0EsZUFBZSxhQUFhO0FBQzVCOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsRUFBRTtBQUNGO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7OztBQUdBLE9BQU87QUFDUDtBQUNBLGlDQUFpQyxnQ0FBbUI7O0FBRXBEOzs7QUFHQSxvR0FBb0csbUJBQW1CLEVBQUUsbUJBQW1CLDhIQUE4SDs7QUFFMVEsbUJBQW1CLGdDQUFtQjs7QUFFdEM7O0FBRUEsY0FBYyxnQ0FBbUI7O0FBRWpDOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3Rjs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHOztBQUVIO0FBQ0E7O0FBRUE7O0FBRUEsT0FBTztBQUNQO0FBQ0EsaUNBQWlDLGdDQUFtQjs7QUFFcEQ7OztBQUdBO0FBQ0E7QUFDQSxDQUFDOztBQUVELGdDQUFnQywyQ0FBMkMsZ0JBQWdCLGtCQUFrQixPQUFPLDJCQUEyQix3REFBd0QsZ0NBQWdDLHVEQUF1RCwyREFBMkQsRUFBRSxFQUFFLHlEQUF5RCxxRUFBcUUsNkRBQTZELG9CQUFvQixHQUFHLEVBQUU7O0FBRWpqQixpQkFBaUIsZ0NBQW1COztBQUVwQzs7QUFFQSxlQUFlLGdDQUFtQjs7QUFFbEM7O0FBRUEsa0JBQWtCLGdDQUFtQjs7QUFFckM7O0FBRUEsY0FBYyxnQ0FBbUI7O0FBRWpDOztBQUVBLHFCQUFxQixnQ0FBbUI7O0FBRXhDOztBQUVBLG9CQUFvQixnQ0FBbUI7O0FBRXZDOztBQUVBLG9CQUFvQixnQ0FBbUI7O0FBRXZDOztBQUVBLG9CQUFvQixnQ0FBbUI7O0FBRXZDOztBQUVBLHFCQUFxQixnQ0FBbUI7O0FBRXhDOztBQUVBLG9CQUFvQixnQ0FBbUI7O0FBRXZDOztBQUVBLGlCQUFpQixnQ0FBbUI7O0FBRXBDOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixpREFBaUQsMENBQTBDLDBEQUEwRCxFQUFFOztBQUV2Sjs7QUFFQSwwREFBMEQ7O0FBRTFEO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBLGNBQWM7QUFDZDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxjQUFjO0FBQ2Q7O0FBRUEsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjs7QUFFQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLGVBQWUsY0FBYztBQUM3QixlQUFlLE9BQU87QUFDdEI7QUFDQTs7QUFFQSxHQUFHO0FBQ0g7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxjQUFjO0FBQ2Q7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxjQUFjO0FBQ2Q7O0FBRUEsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRzs7QUFFSDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGNBQWM7QUFDZDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGVBQWU7QUFDZjtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGNBQWM7QUFDZDtBQUNBOztBQUVBO0FBQ0EsY0FBYztBQUNkO0FBQ0EsbURBQW1EOztBQUVuRDtBQUNBLGNBQWM7QUFDZDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsY0FBYztBQUNkO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLGNBQWM7QUFDZDtBQUNBOztBQUVBOztBQUVBO0FBQ0EsY0FBYztBQUNkO0FBQ0E7QUFDQTtBQUNBLGNBQWM7QUFDZDtBQUNBO0FBQ0E7QUFDQSxjQUFjO0FBQ2Q7QUFDQTtBQUNBO0FBQ0EsY0FBYztBQUNkO0FBQ0E7QUFDQTtBQUNBLGNBQWM7QUFDZDtBQUNBO0FBQ0E7QUFDQSxjQUFjO0FBQ2Q7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMOztBQUVBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxHQUFHO0FBQ0g7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLHNDQUFzQyxtQkFBbUI7QUFDekQ7O0FBRUE7QUFDQTtBQUNBLGlHQUFpRztBQUNqRyxPQUFPO0FBQ1A7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsZUFBZSxVQUFVO0FBQ3pCLGVBQWUsT0FBTztBQUN0QixpQkFBaUI7QUFDakI7O0FBRUEsR0FBRztBQUNIO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxHQUFHO0FBQ0g7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZSxNQUFNO0FBQ3JCOztBQUVBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWUsTUFBTTtBQUNyQjs7QUFFQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWUsTUFBTTtBQUNyQjs7QUFFQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsZUFBZSxTQUFTO0FBQ3hCLGlCQUFpQjtBQUNqQjs7QUFFQSxHQUFHO0FBQ0g7QUFDQTtBQUNBOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZSxhQUFhO0FBQzVCOztBQUVBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7O0FBRUEsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjs7QUFFQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7O0FBRUEsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7O0FBRUEsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0EsR0FBRzs7QUFFSDtBQUNBLENBQUM7O0FBRUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQSxVQUFVLE9BQU87QUFDakI7OztBQUdBOztBQUVBO0FBQ0E7O0FBRUEsT0FBTztBQUNQO0FBQ0EsaUNBQWlDLGdDQUFtQjs7QUFFcEQ7OztBQUdBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7O0FBRUEsZ0JBQWdCLGdDQUFtQjs7QUFFbkM7O0FBRUEsZUFBZSxnQ0FBbUI7O0FBRWxDOztBQUVBLGdCQUFnQixnQ0FBbUI7O0FBRW5DOztBQUVBLGVBQWUsZ0NBQW1COztBQUVsQzs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsT0FBTztBQUNQO0FBQ0EsaUNBQWlDLGdDQUFtQjs7QUFFcEQ7OztBQUdBO0FBQ0E7QUFDQSxDQUFDOztBQUVELGdDQUFnQywyQ0FBMkMsZ0JBQWdCLGtCQUFrQixPQUFPLDJCQUEyQix3REFBd0QsZ0NBQWdDLHVEQUF1RCwyREFBMkQsRUFBRSxFQUFFLHlEQUF5RCxxRUFBcUUsNkRBQTZELG9CQUFvQixHQUFHLEVBQUU7O0FBRWpqQixxREFBcUQsa0RBQWtELDhEQUE4RCwwQkFBMEIsNENBQTRDLHVCQUF1QixrQkFBa0IsRUFBRSxPQUFPLHdDQUF3QyxFQUFFLEVBQUUsNEJBQTRCLG1CQUFtQixFQUFFLE9BQU8sdUJBQXVCLDRCQUE0QixrQkFBa0IsRUFBRSw4QkFBOEIsRUFBRTs7QUFFeGUsa0JBQWtCLGdDQUFtQjs7QUFFckM7O0FBRUEsY0FBYyxnQ0FBbUI7O0FBRWpDOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixpREFBaUQsMENBQTBDLDBEQUEwRCxFQUFFOztBQUV2SixpREFBaUQsYUFBYSx1RkFBdUYsRUFBRSx1RkFBdUY7O0FBRTlPLDBDQUEwQywrREFBK0QscUdBQXFHLEVBQUUseUVBQXlFLGVBQWUseUVBQXlFLEVBQUUsRUFBRSx1SEFBdUg7O0FBRTVlO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQSxjQUFjO0FBQ2Q7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLGFBQWEsT0FBTztBQUNwQixhQUFhLEVBQUU7QUFDZjs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsd0ZBQXdGLGFBQWE7QUFDckc7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGdCQUFnQixPQUFPO0FBQ3ZCLG9CQUFvQixZQUFZO0FBQ2hDLG9CQUFvQixVQUFVO0FBQzlCLHFCQUFxQixvRkFBb0Y7QUFDekc7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxlQUFlLE1BQU07QUFDckI7O0FBRUEsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHOztBQUVIO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBOztBQUVBLE9BQU87QUFDUDtBQUNBLGlDQUFpQyxnQ0FBbUI7O0FBRXBEOzs7QUFHQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRCxnQ0FBZ0MsMkNBQTJDLGdCQUFnQixrQkFBa0IsT0FBTywyQkFBMkIsd0RBQXdELGdDQUFnQyx1REFBdUQsMkRBQTJELEVBQUUsRUFBRSx5REFBeUQscUVBQXFFLDZEQUE2RCxvQkFBb0IsR0FBRyxFQUFFOztBQUVqakIscURBQXFELGtEQUFrRCw4REFBOEQsMEJBQTBCLDRDQUE0Qyx1QkFBdUIsa0JBQWtCLEVBQUUsT0FBTyx3Q0FBd0MsRUFBRSxFQUFFLDRCQUE0QixtQkFBbUIsRUFBRSxPQUFPLHVCQUF1Qiw0QkFBNEIsa0JBQWtCLEVBQUUsOEJBQThCLEVBQUU7O0FBRXhlLGtCQUFrQixnQ0FBbUI7O0FBRXJDOztBQUVBLGNBQWMsZ0NBQW1COztBQUVqQzs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0YsaURBQWlELDBDQUEwQywwREFBMEQsRUFBRTs7QUFFdkosaURBQWlELGFBQWEsdUZBQXVGLEVBQUUsdUZBQXVGOztBQUU5TywwQ0FBMEMsK0RBQStELHFHQUFxRyxFQUFFLHlFQUF5RSxlQUFlLHlFQUF5RSxFQUFFLEVBQUUsdUhBQXVIOztBQUU1ZTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQSwrSkFBK0o7QUFDL0o7QUFDQTtBQUNBO0FBQ0EsS0FBSzs7QUFFTDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7O0FBRUg7QUFDQSxDQUFDOztBQUVEO0FBQ0E7O0FBRUEsT0FBTztBQUNQO0FBQ0EsaUNBQWlDLGdDQUFtQjs7QUFFcEQ7OztBQUdBO0FBQ0E7QUFDQSxDQUFDOztBQUVELGdDQUFnQywyQ0FBMkMsZ0JBQWdCLGtCQUFrQixPQUFPLDJCQUEyQix3REFBd0QsZ0NBQWdDLHVEQUF1RCwyREFBMkQsRUFBRSxFQUFFLHlEQUF5RCxxRUFBcUUsNkRBQTZELG9CQUFvQixHQUFHLEVBQUU7O0FBRWpqQixxREFBcUQsa0RBQWtELDhEQUE4RCwwQkFBMEIsNENBQTRDLHVCQUF1QixrQkFBa0IsRUFBRSxPQUFPLHdDQUF3QyxFQUFFLEVBQUUsNEJBQTRCLG1CQUFtQixFQUFFLE9BQU8sdUJBQXVCLDRCQUE0QixrQkFBa0IsRUFBRSw4QkFBOEIsRUFBRTs7QUFFeGUsZ0JBQWdCLGdDQUFtQjs7QUFFbkM7O0FBRUEsY0FBYyxnQ0FBbUI7O0FBRWpDOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixpREFBaUQsMENBQTBDLDBEQUEwRCxFQUFFOztBQUV2SixpREFBaUQsYUFBYSx1RkFBdUYsRUFBRSx1RkFBdUY7O0FBRTlPLDBDQUEwQywrREFBK0QscUdBQXFHLEVBQUUseUVBQXlFLGVBQWUseUVBQXlFLEVBQUUsRUFBRSx1SEFBdUg7O0FBRTVlO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQSxpS0FBaUs7O0FBRWpLO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQSxTQUFTOztBQUVUOztBQUVBO0FBQ0EsT0FBTzs7QUFFUDtBQUNBO0FBQ0EsR0FBRzs7QUFFSDtBQUNBLENBQUM7O0FBRUQ7QUFDQTs7QUFFQSxPQUFPO0FBQ1A7QUFDQSxpQ0FBaUMsZ0NBQW1COztBQUVwRDs7O0FBR0E7QUFDQTtBQUNBLENBQUM7O0FBRUQsZ0NBQWdDLDJDQUEyQyxnQkFBZ0Isa0JBQWtCLE9BQU8sMkJBQTJCLHdEQUF3RCxnQ0FBZ0MsdURBQXVELDJEQUEyRCxFQUFFLEVBQUUseURBQXlELHFFQUFxRSw2REFBNkQsb0JBQW9CLEdBQUcsRUFBRTs7QUFFampCLGNBQWMsZ0NBQW1COztBQUVqQzs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0YsaURBQWlELDBDQUEwQywwREFBMEQsRUFBRTs7QUFFdko7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYSxZQUFZO0FBQ3pCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLGNBQWM7QUFDZDtBQUNBO0FBQ0E7QUFDQSxjQUFjO0FBQ2Q7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlO0FBQ2Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0EsY0FBYztBQUNkO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWEsSUFBSTtBQUNqQixhQUFhLElBQUk7QUFDakI7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWUsTUFBTTtBQUNyQjs7QUFFQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0EseURBQXlELFdBQVcsbUJBQW1CO0FBQ3ZGO0FBQ0EsU0FBUztBQUNULHlEQUF5RCxXQUFXLG1CQUFtQjtBQUN2RiwrQkFBK0I7QUFDL0I7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWUsTUFBTTtBQUNyQjs7QUFFQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSx5QkFBeUI7O0FBRXpCOztBQUVBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlLE1BQU07QUFDckI7O0FBRUEsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBLEdBQUc7O0FBRUg7QUFDQSxDQUFDOztBQUVEO0FBQ0E7O0FBRUEsT0FBTztBQUNQO0FBQ0EsaUNBQWlDLGlDQUFtQjs7QUFFcEQ7OztBQUdBO0FBQ0E7QUFDQSxDQUFDOztBQUVELGdDQUFnQywyQ0FBMkMsZ0JBQWdCLGtCQUFrQixPQUFPLDJCQUEyQix3REFBd0QsZ0NBQWdDLHVEQUF1RCwyREFBMkQsRUFBRSxFQUFFLHlEQUF5RCxxRUFBcUUsNkRBQTZELG9CQUFvQixHQUFHLEVBQUU7O0FBRWpqQixjQUFjLGlDQUFtQjs7QUFFakM7O0FBRUEsZUFBZSxpQ0FBbUI7O0FBRWxDOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixpREFBaUQsMENBQTBDLDBEQUEwRCxFQUFFOztBQUV2SjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLFlBQVk7QUFDekIsYUFBYSxPQUFPO0FBQ3BCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLGNBQWM7QUFDZDtBQUNBO0FBQ0E7QUFDQSxjQUFjO0FBQ2Q7QUFDQTtBQUNBO0FBQ0EsY0FBYztBQUNkO0FBQ0E7QUFDQTtBQUNBLGNBQWM7QUFDZDtBQUNBOztBQUVBO0FBQ0E7QUFDQSxjQUFjO0FBQ2Q7QUFDQTtBQUNBO0FBQ0EsY0FBYztBQUNkO0FBQ0E7QUFDQTtBQUNBLGNBQWM7QUFDZDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLGVBQWU7QUFDZjs7O0FBR0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxlQUFlO0FBQ2Y7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxTQUFTOztBQUVUO0FBQ0E7QUFDQSxTQUFTOztBQUVUO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7O0FBRVQ7QUFDQTtBQUNBLFNBQVM7QUFDVDs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBLFNBQVM7QUFDVDs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0EsU0FBUztBQUNUOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQSxpRUFBaUUsa0RBQWtELG9EQUFvRDs7QUFFdks7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsZUFBZSxNQUFNO0FBQ3JCO0FBQ0E7O0FBRUEsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZSxNQUFNO0FBQ3JCOztBQUVBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlLE1BQU07QUFDckI7O0FBRUEsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWUsTUFBTTtBQUNyQjs7QUFFQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjs7QUFFQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7O0FBRUEsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7O0FBRUEsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7O0FBRUEsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7O0FBRUEsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7O0FBRUEsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRzs7QUFFSDtBQUNBLENBQUM7O0FBRUQ7QUFDQTs7QUFFQSxPQUFPO0FBQ1A7QUFDQSxpQ0FBaUMsaUNBQW1COztBQUVwRDs7O0FBR0E7QUFDQTtBQUNBLENBQUM7O0FBRUQsZ0NBQWdDLDJDQUEyQyxnQkFBZ0Isa0JBQWtCLE9BQU8sMkJBQTJCLHdEQUF3RCxnQ0FBZ0MsdURBQXVELDJEQUEyRCxFQUFFLEVBQUUseURBQXlELHFFQUFxRSw2REFBNkQsb0JBQW9CLEdBQUcsRUFBRTs7QUFFampCLGNBQWMsaUNBQW1COztBQUVqQzs7QUFFQSxpQkFBaUIsaUNBQW1COztBQUVwQzs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0YsaURBQWlELDBDQUEwQywwREFBMEQsRUFBRTs7QUFFdko7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYSxZQUFZO0FBQ3pCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLGNBQWM7QUFDZDtBQUNBO0FBQ0E7QUFDQSxjQUFjO0FBQ2Q7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPOztBQUVQO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCOztBQUVBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlLE9BQU87QUFDdEI7QUFDQTtBQUNBOztBQUVBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZSxZQUFZO0FBQzNCO0FBQ0EsaUJBQWlCO0FBQ2pCOztBQUVBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7O0FBRUEsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7O0FBRUEsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7O0FBRUEsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCOztBQUVBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7O0FBRUEsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlLE1BQU07QUFDckIsaUJBQWlCO0FBQ2pCOztBQUVBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWUsTUFBTTtBQUNyQixpQkFBaUI7QUFDakI7O0FBRUEsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRzs7QUFFSDtBQUNBLENBQUM7O0FBRUQ7QUFDQTs7QUFFQSxPQUFPO0FBQ1A7QUFDQSxpQ0FBaUMsaUNBQW1COztBQUVwRDs7O0FBR0Esa0JBQWtCLGlDQUFtQjtBQUNyQyxjQUFjLGlDQUFtQjs7QUFFakM7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBLG1CQUFtQjtBQUNuQjtBQUNBO0FBQ0E7QUFDQSxFQUFFO0FBQ0Y7QUFDQTtBQUNBO0FBQ0EsRUFBRTtBQUNGO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsRUFBRTtBQUNGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxFQUFFO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsRUFBRTtBQUNGOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBLGFBQWEsbUJBQW1CO0FBQ2hDO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLGNBQWM7QUFDM0I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEVBQUU7O0FBRUY7QUFDQTtBQUNBLEVBQUU7O0FBRUY7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEVBQUU7O0FBRUY7QUFDQTtBQUNBO0FBQ0E7QUFDQSxFQUFFOztBQUVGO0FBQ0E7QUFDQSxFQUFFOztBQUVGO0FBQ0E7QUFDQTtBQUNBOztBQUVBLGlCQUFpQixjQUFjO0FBQy9CO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0EsRUFBRTs7QUFFRjtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLEVBQUU7O0FBRUY7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxFQUFFOztBQUVGO0FBQ0E7QUFDQTtBQUNBLEVBQUU7O0FBRUY7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxFQUFFOztBQUVGO0FBQ0E7QUFDQTtBQUNBOztBQUVBLHFFQUFxRSxrQ0FBa0MsRUFBRTs7QUFFekc7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0EsRUFBRTs7QUFFRjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLEVBQUU7O0FBRUY7QUFDQTtBQUNBO0FBQ0EsRUFBRTs7QUFFRjtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxpQkFBaUIsZ0JBQWdCO0FBQ2pDO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLEVBQUU7O0FBRUY7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0EsRUFBRTs7QUFFRjtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0EsRUFBRTs7QUFFRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsRUFBRTs7QUFFRjtBQUNBO0FBQ0EsRUFBRTs7QUFFRjtBQUNBO0FBQ0EsaUJBQWlCLE9BQU87QUFDeEI7QUFDQTtBQUNBO0FBQ0EsRUFBRTs7QUFFRjtBQUNBO0FBQ0E7QUFDQTtBQUNBLEVBQUU7O0FBRUY7QUFDQTtBQUNBO0FBQ0E7QUFDQSxFQUFFOztBQUVGO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsRUFBRTs7QUFFRjtBQUNBO0FBQ0E7QUFDQTtBQUNBLEVBQUU7O0FBRUY7QUFDQTtBQUNBO0FBQ0E7QUFDQSxFQUFFOztBQUVGO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsRUFBRTs7QUFFRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsRUFBRTs7QUFFRjtBQUNBO0FBQ0EsRUFBRTs7QUFFRjtBQUNBO0FBQ0EsRUFBRTs7QUFFRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEVBQUU7O0FBRUY7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUM7O0FBRUQ7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsRUFBRTs7QUFFRjs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLGdCQUFnQixZQUFZO0FBQzVCO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7OztBQUdBLE9BQU87QUFDUDtBQUNBLGlDQUFpQyxpQ0FBbUI7O0FBRXBEO0FBQ0EsaUJBQWlCLGlDQUFtQjtBQUNwQyxjQUFjLGlDQUFtQjs7QUFFakM7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQSxTQUFTO0FBQ1Q7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUEseUJBQXlCLElBQUk7QUFDN0Isd0JBQXdCLEVBQUUsV0FBVyxFQUFFO0FBQ3ZDO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUEsYUFBYSxPQUFPO0FBQ3BCO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEVBQUU7QUFDRjtBQUNBOztBQUVBLGFBQWEsT0FBTztBQUNwQjtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEVBQUU7QUFDRixhQUFhLE9BQU87QUFDcEI7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxFQUFFO0FBQ0YsYUFBYSxPQUFPO0FBQ3BCO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsRUFBRTtBQUNGO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQSxFQUFFO0FBQ0Y7QUFDQTs7QUFFQSxZQUFZLE9BQU87QUFDbkI7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7OztBQUdBLE9BQU87QUFDUDtBQUNBLGlDQUFpQyxpQ0FBbUI7O0FBRXBEOzs7QUFHQSxpQkFBaUIsaUNBQW1COztBQUVwQztBQUNBOztBQUVBO0FBQ0E7O0FBRUEsbUNBQW1DLFNBQVM7QUFDNUM7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBLE9BQU87QUFDUDtBQUNBOztBQUVBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7OztBQUdBLE9BQU87QUFDUDtBQUNBLGlDQUFpQyxpQ0FBbUI7O0FBRXBELGtCQUFrQixpQ0FBbUI7QUFDckMsWUFBWSxpQ0FBbUI7O0FBRS9COztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBLGtDQUFrQztBQUNsQztBQUNBO0FBQ0EsdUNBQXVDLFNBQVM7QUFDaEQ7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBLHdEQUF3RCx1Q0FBdUM7QUFDL0Ysc0RBQXNELHFDQUFxQzs7QUFFM0Y7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxFQUFFO0FBQ0YsQ0FBQzs7QUFFRDs7O0FBR0EsT0FBTztBQUNQO0FBQ0EsaUNBQWlDLGlDQUFtQjs7QUFFcEQsa0JBQWtCLGlDQUFtQjs7QUFFckM7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLHFDQUFxQyxTQUFTO0FBQzlDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EseUJBQXlCOztBQUV6Qjs7QUFFQTtBQUNBO0FBQ0E7O0FBRUEseUNBQXlDLFNBQVM7QUFDbEQ7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0EscUNBQXFDLFNBQVM7QUFDOUM7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7Ozs7QUFJQSxPQUFPO0FBQ1A7QUFDQSxpQ0FBaUMsaUNBQW1COztBQUVwRDs7O0FBR0E7QUFDQTtBQUNBLENBQUM7O0FBRUQsZ0NBQWdDLDJDQUEyQyxnQkFBZ0Isa0JBQWtCLE9BQU8sMkJBQTJCLHdEQUF3RCxnQ0FBZ0MsdURBQXVELDJEQUEyRCxFQUFFLEVBQUUseURBQXlELHFFQUFxRSw2REFBNkQsb0JBQW9CLEdBQUcsRUFBRTs7QUFFampCLGNBQWMsaUNBQW1COztBQUVqQzs7QUFFQSxpQkFBaUIsaUNBQW1COztBQUVwQzs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0YsaURBQWlELDBDQUEwQywwREFBMEQsRUFBRTs7QUFFdko7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYSxZQUFZO0FBQ3pCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLGNBQWM7QUFDZDtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxlQUFlO0FBQ2Y7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjs7QUFFQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGVBQWUsaUJBQWlCO0FBQ2hDOztBQUVBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWUsRUFBRTtBQUNqQixlQUFlLFFBQVE7QUFDdkIsaUJBQWlCO0FBQ2pCOztBQUVBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQSxpQkFBaUI7QUFDakI7O0FBRUEsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxlQUFlLFNBQVM7QUFDeEIsZUFBZSxRQUFRO0FBQ3ZCLGlCQUFpQjtBQUNqQjs7QUFFQSxHQUFHO0FBQ0g7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87O0FBRVA7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCOztBQUVBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCOztBQUVBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCOztBQUVBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLGlCQUFpQjtBQUNqQjs7QUFFQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsY0FBYztBQUNkOztBQUVBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWUsZUFBZTtBQUM5QjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRzs7QUFFSDtBQUNBLENBQUM7O0FBRUQ7QUFDQTs7QUFFQSxPQUFPO0FBQ1A7QUFDQSxpQ0FBaUMsaUNBQW1COztBQUVwRDs7O0FBR0E7QUFDQTtBQUNBLENBQUM7O0FBRUQsZ0NBQWdDLDJDQUEyQyxnQkFBZ0Isa0JBQWtCLE9BQU8sMkJBQTJCLHdEQUF3RCxnQ0FBZ0MsdURBQXVELDJEQUEyRCxFQUFFLEVBQUUseURBQXlELHFFQUFxRSw2REFBNkQsb0JBQW9CLEdBQUcsRUFBRTs7QUFFampCLGNBQWMsaUNBQW1COztBQUVqQzs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0YsaURBQWlELDBDQUEwQywwREFBMEQsRUFBRTs7QUFFdko7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYSxZQUFZO0FBQ3pCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLGNBQWM7QUFDZDtBQUNBO0FBQ0E7QUFDQSxjQUFjO0FBQ2Q7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZ0JBQWdCO0FBQ2hCO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUOztBQUVBO0FBQ0EscUhBQXFIOztBQUVySDtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRzs7QUFFSDtBQUNBLENBQUM7O0FBRUQ7QUFDQTs7QUFFQSxPQUFPO0FBQ1A7QUFDQTs7QUFFQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQsZ0NBQWdDLDJDQUEyQyxnQkFBZ0Isa0JBQWtCLE9BQU8sMkJBQTJCLHdEQUF3RCxnQ0FBZ0MsdURBQXVELDJEQUEyRCxFQUFFLEVBQUUseURBQXlELHFFQUFxRSw2REFBNkQsb0JBQW9CLEdBQUcsRUFBRTs7QUFFampCLGlEQUFpRCwwQ0FBMEMsMERBQTBELEVBQUU7O0FBRXZKO0FBQ0E7QUFDQSxhQUFhLFlBQVk7QUFDekI7QUFDQTtBQUNBOztBQUVBO0FBQ0EsY0FBYztBQUNkO0FBQ0E7QUFDQTtBQUNBLGNBQWM7QUFDZDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQSxnQkFBZ0I7QUFDaEI7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUEsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUEsb0JBQW9COztBQUVwQjs7QUFFQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBLEdBQUc7O0FBRUg7QUFDQSxDQUFDOztBQUVEO0FBQ0E7O0FBRUEsT0FBTztBQUNQO0FBQ0EsQ0FBQztBQUNEO0FBQ0EsQ0FBQzs7Ozs7Ozs7Ozs7O0FDL21NRCxrQzs7Ozs7O1VDQUE7VUFDQTs7VUFFQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTs7VUFFQTtVQUNBOztVQUVBO1VBQ0E7VUFDQTs7Ozs7Ozs7Ozs7O0FDR0E7Ozs7QUFDQTs7Ozs7O0FBMUJBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O2NBNEJZVixNO0lBQUxDLEMsV0FBQUEsQzs7O0FBRVBBLEVBQUUsWUFBTTtBQUNOO0FBQ0EsTUFBSVMsMkJBQUo7QUFDRCxDQUhELEUiLCJmaWxlIjoib3JkZXJfcmV0dXJuX3N0YXRlc19mb3JtLmJ1bmRsZS5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQ29weXJpZ2h0IHNpbmNlIDIwMDcgUHJlc3RhU2hvcCBTQSBhbmQgQ29udHJpYnV0b3JzXG4gKiBQcmVzdGFTaG9wIGlzIGFuIEludGVybmF0aW9uYWwgUmVnaXN0ZXJlZCBUcmFkZW1hcmsgJiBQcm9wZXJ0eSBvZiBQcmVzdGFTaG9wIFNBXG4gKlxuICogTk9USUNFIE9GIExJQ0VOU0VcbiAqXG4gKiBUaGlzIHNvdXJjZSBmaWxlIGlzIHN1YmplY3QgdG8gdGhlIE9wZW4gU29mdHdhcmUgTGljZW5zZSAoT1NMIDMuMClcbiAqIHRoYXQgaXMgYnVuZGxlZCB3aXRoIHRoaXMgcGFja2FnZSBpbiB0aGUgZmlsZSBMSUNFTlNFLm1kLlxuICogSXQgaXMgYWxzbyBhdmFpbGFibGUgdGhyb3VnaCB0aGUgd29ybGQtd2lkZS13ZWIgYXQgdGhpcyBVUkw6XG4gKiBodHRwczovL29wZW5zb3VyY2Uub3JnL2xpY2Vuc2VzL09TTC0zLjBcbiAqIElmIHlvdSBkaWQgbm90IHJlY2VpdmUgYSBjb3B5IG9mIHRoZSBsaWNlbnNlIGFuZCBhcmUgdW5hYmxlIHRvXG4gKiBvYnRhaW4gaXQgdGhyb3VnaCB0aGUgd29ybGQtd2lkZS13ZWIsIHBsZWFzZSBzZW5kIGFuIGVtYWlsXG4gKiB0byBsaWNlbnNlQHByZXN0YXNob3AuY29tIHNvIHdlIGNhbiBzZW5kIHlvdSBhIGNvcHkgaW1tZWRpYXRlbHkuXG4gKlxuICogRElTQ0xBSU1FUlxuICpcbiAqIERvIG5vdCBlZGl0IG9yIGFkZCB0byB0aGlzIGZpbGUgaWYgeW91IHdpc2ggdG8gdXBncmFkZSBQcmVzdGFTaG9wIHRvIG5ld2VyXG4gKiB2ZXJzaW9ucyBpbiB0aGUgZnV0dXJlLiBJZiB5b3Ugd2lzaCB0byBjdXN0b21pemUgUHJlc3RhU2hvcCBmb3IgeW91clxuICogbmVlZHMgcGxlYXNlIHJlZmVyIHRvIGh0dHBzOi8vZGV2ZG9jcy5wcmVzdGFzaG9wLmNvbS8gZm9yIG1vcmUgaW5mb3JtYXRpb24uXG4gKlxuICogQGF1dGhvciAgICBQcmVzdGFTaG9wIFNBIGFuZCBDb250cmlidXRvcnMgPGNvbnRhY3RAcHJlc3Rhc2hvcC5jb20+XG4gKiBAY29weXJpZ2h0IFNpbmNlIDIwMDcgUHJlc3RhU2hvcCBTQSBhbmQgQ29udHJpYnV0b3JzXG4gKiBAbGljZW5zZSAgIGh0dHBzOi8vb3BlbnNvdXJjZS5vcmcvbGljZW5zZXMvT1NMLTMuMCBPcGVuIFNvZnR3YXJlIExpY2Vuc2UgKE9TTCAzLjApXG4gKi9cbmltcG9ydCAnYm9vdHN0cmFwLWNvbG9ycGlja2VyJztcblxuY29uc3QgeyR9ID0gd2luZG93O1xuXG4vKipcbiAqIEVuYWJsZSBhbGwgY29sb3JwaWNrZXJzLlxuICovXG5jb25zdCBpbml0ID0gZnVuY3Rpb24gaW5pdERhdGVQaWNrZXJzKCkge1xuICAkKCcuY29sb3JwaWNrZXIgaW5wdXRbdHlwZT1cInRleHRcIl0nKS5lYWNoKChpLCBwaWNrZXIpID0+IHtcbiAgICAkKHBpY2tlcikuY29sb3JwaWNrZXIoKTtcbiAgfSk7XG59O1xuXG5leHBvcnQgZGVmYXVsdCBpbml0O1xuIiwiLyoqXG4gKiBDb3B5cmlnaHQgc2luY2UgMjAwNyBQcmVzdGFTaG9wIFNBIGFuZCBDb250cmlidXRvcnNcbiAqIFByZXN0YVNob3AgaXMgYW4gSW50ZXJuYXRpb25hbCBSZWdpc3RlcmVkIFRyYWRlbWFyayAmIFByb3BlcnR5IG9mIFByZXN0YVNob3AgU0FcbiAqXG4gKiBOT1RJQ0UgT0YgTElDRU5TRVxuICpcbiAqIFRoaXMgc291cmNlIGZpbGUgaXMgc3ViamVjdCB0byB0aGUgT3BlbiBTb2Z0d2FyZSBMaWNlbnNlIChPU0wgMy4wKVxuICogdGhhdCBpcyBidW5kbGVkIHdpdGggdGhpcyBwYWNrYWdlIGluIHRoZSBmaWxlIExJQ0VOU0UubWQuXG4gKiBJdCBpcyBhbHNvIGF2YWlsYWJsZSB0aHJvdWdoIHRoZSB3b3JsZC13aWRlLXdlYiBhdCB0aGlzIFVSTDpcbiAqIGh0dHBzOi8vb3BlbnNvdXJjZS5vcmcvbGljZW5zZXMvT1NMLTMuMFxuICogSWYgeW91IGRpZCBub3QgcmVjZWl2ZSBhIGNvcHkgb2YgdGhlIGxpY2Vuc2UgYW5kIGFyZSB1bmFibGUgdG9cbiAqIG9idGFpbiBpdCB0aHJvdWdoIHRoZSB3b3JsZC13aWRlLXdlYiwgcGxlYXNlIHNlbmQgYW4gZW1haWxcbiAqIHRvIGxpY2Vuc2VAcHJlc3Rhc2hvcC5jb20gc28gd2UgY2FuIHNlbmQgeW91IGEgY29weSBpbW1lZGlhdGVseS5cbiAqXG4gKiBESVNDTEFJTUVSXG4gKlxuICogRG8gbm90IGVkaXQgb3IgYWRkIHRvIHRoaXMgZmlsZSBpZiB5b3Ugd2lzaCB0byB1cGdyYWRlIFByZXN0YVNob3AgdG8gbmV3ZXJcbiAqIHZlcnNpb25zIGluIHRoZSBmdXR1cmUuIElmIHlvdSB3aXNoIHRvIGN1c3RvbWl6ZSBQcmVzdGFTaG9wIGZvciB5b3VyXG4gKiBuZWVkcyBwbGVhc2UgcmVmZXIgdG8gaHR0cHM6Ly9kZXZkb2NzLnByZXN0YXNob3AuY29tLyBmb3IgbW9yZSBpbmZvcm1hdGlvbi5cbiAqXG4gKiBAYXV0aG9yICAgIFByZXN0YVNob3AgU0EgYW5kIENvbnRyaWJ1dG9ycyA8Y29udGFjdEBwcmVzdGFzaG9wLmNvbT5cbiAqIEBjb3B5cmlnaHQgU2luY2UgMjAwNyBQcmVzdGFTaG9wIFNBIGFuZCBDb250cmlidXRvcnNcbiAqIEBsaWNlbnNlICAgaHR0cHM6Ly9vcGVuc291cmNlLm9yZy9saWNlbnNlcy9PU0wtMy4wIE9wZW4gU29mdHdhcmUgTGljZW5zZSAoT1NMIDMuMClcbiAqL1xuXG5pbXBvcnQgRXZlbnRFbWl0dGVyQ2xhc3MgZnJvbSAnZXZlbnRzJztcblxuLyoqXG4gKiBXZSBpbnN0YW5jaWF0ZSBvbmUgRXZlbnRFbWl0dGVyIChyZXN0cmljdGVkIHZpYSBhIGNvbnN0KSBzbyB0aGF0IGV2ZXJ5IGNvbXBvbmVudHNcbiAqIHJlZ2lzdGVyL2Rpc3BhdGNoIG9uIHRoZSBzYW1lIG9uZSBhbmQgY2FuIGNvbW11bmljYXRlIHdpdGggZWFjaCBvdGhlci5cbiAqL1xuZXhwb3J0IGNvbnN0IEV2ZW50RW1pdHRlciA9IG5ldyBFdmVudEVtaXR0ZXJDbGFzcygpO1xuXG5leHBvcnQgZGVmYXVsdCBFdmVudEVtaXR0ZXI7XG4iLCIvKipcbiAqIENvcHlyaWdodCBzaW5jZSAyMDA3IFByZXN0YVNob3AgU0EgYW5kIENvbnRyaWJ1dG9yc1xuICogUHJlc3RhU2hvcCBpcyBhbiBJbnRlcm5hdGlvbmFsIFJlZ2lzdGVyZWQgVHJhZGVtYXJrICYgUHJvcGVydHkgb2YgUHJlc3RhU2hvcCBTQVxuICpcbiAqIE5PVElDRSBPRiBMSUNFTlNFXG4gKlxuICogVGhpcyBzb3VyY2UgZmlsZSBpcyBzdWJqZWN0IHRvIHRoZSBPcGVuIFNvZnR3YXJlIExpY2Vuc2UgKE9TTCAzLjApXG4gKiB0aGF0IGlzIGJ1bmRsZWQgd2l0aCB0aGlzIHBhY2thZ2UgaW4gdGhlIGZpbGUgTElDRU5TRS5tZC5cbiAqIEl0IGlzIGFsc28gYXZhaWxhYmxlIHRocm91Z2ggdGhlIHdvcmxkLXdpZGUtd2ViIGF0IHRoaXMgVVJMOlxuICogaHR0cHM6Ly9vcGVuc291cmNlLm9yZy9saWNlbnNlcy9PU0wtMy4wXG4gKiBJZiB5b3UgZGlkIG5vdCByZWNlaXZlIGEgY29weSBvZiB0aGUgbGljZW5zZSBhbmQgYXJlIHVuYWJsZSB0b1xuICogb2J0YWluIGl0IHRocm91Z2ggdGhlIHdvcmxkLXdpZGUtd2ViLCBwbGVhc2Ugc2VuZCBhbiBlbWFpbFxuICogdG8gbGljZW5zZUBwcmVzdGFzaG9wLmNvbSBzbyB3ZSBjYW4gc2VuZCB5b3UgYSBjb3B5IGltbWVkaWF0ZWx5LlxuICpcbiAqIERJU0NMQUlNRVJcbiAqXG4gKiBEbyBub3QgZWRpdCBvciBhZGQgdG8gdGhpcyBmaWxlIGlmIHlvdSB3aXNoIHRvIHVwZ3JhZGUgUHJlc3RhU2hvcCB0byBuZXdlclxuICogdmVyc2lvbnMgaW4gdGhlIGZ1dHVyZS4gSWYgeW91IHdpc2ggdG8gY3VzdG9taXplIFByZXN0YVNob3AgZm9yIHlvdXJcbiAqIG5lZWRzIHBsZWFzZSByZWZlciB0byBodHRwczovL2RldmRvY3MucHJlc3Rhc2hvcC5jb20vIGZvciBtb3JlIGluZm9ybWF0aW9uLlxuICpcbiAqIEBhdXRob3IgICAgUHJlc3RhU2hvcCBTQSBhbmQgQ29udHJpYnV0b3JzIDxjb250YWN0QHByZXN0YXNob3AuY29tPlxuICogQGNvcHlyaWdodCBTaW5jZSAyMDA3IFByZXN0YVNob3AgU0EgYW5kIENvbnRyaWJ1dG9yc1xuICogQGxpY2Vuc2UgICBodHRwczovL29wZW5zb3VyY2Uub3JnL2xpY2Vuc2VzL09TTC0zLjAgT3BlbiBTb2Z0d2FyZSBMaWNlbnNlIChPU0wgMy4wKVxuICovXG5cbmltcG9ydCB7RXZlbnRFbWl0dGVyfSBmcm9tICcuL2V2ZW50LWVtaXR0ZXInO1xuXG5jb25zdCB7JH0gPSB3aW5kb3c7XG5cbi8qKlxuICogVGhpcyBjbGFzcyBpcyB1c2VkIHRvIGF1dG9tYXRpY2FsbHkgdG9nZ2xlIHRyYW5zbGF0ZWQgaW5wdXRzIChkaXNwbGF5ZWQgd2l0aCBvbmVcbiAqIGlucHV0IGFuZCBhIGxhbmd1YWdlIHNlbGVjdG9yIHVzaW5nIHRoZSBUcmFuc2xhdGFibGVUeXBlIFN5bWZvbnkgZm9ybSB0eXBlKS5cbiAqIEFsc28gY29tcGF0aWJsZSB3aXRoIFRyYW5zbGF0YWJsZUZpZWxkIGNoYW5nZXMuXG4gKi9cbmNsYXNzIFRyYW5zbGF0YWJsZUlucHV0IHtcbiAgY29uc3RydWN0b3Iob3B0aW9ucykge1xuICAgIGNvbnN0IG9wdHMgPSBvcHRpb25zIHx8IHt9O1xuXG4gICAgdGhpcy5sb2NhbGVJdGVtU2VsZWN0b3IgPSBvcHRzLmxvY2FsZUl0ZW1TZWxlY3RvciB8fCAnLmpzLWxvY2FsZS1pdGVtJztcbiAgICB0aGlzLmxvY2FsZUJ1dHRvblNlbGVjdG9yID0gb3B0cy5sb2NhbGVCdXR0b25TZWxlY3RvciB8fCAnLmpzLWxvY2FsZS1idG4nO1xuICAgIHRoaXMubG9jYWxlSW5wdXRTZWxlY3RvciA9IG9wdHMubG9jYWxlSW5wdXRTZWxlY3RvciB8fCAnLmpzLWxvY2FsZS1pbnB1dCc7XG4gICAgdGhpcy5zZWxlY3RlZExvY2FsZSA9ICQodGhpcy5sb2NhbGVJdGVtU2VsZWN0b3IpLmRhdGEoJ2xvY2FsZScpO1xuXG4gICAgJCgnYm9keScpLm9uKFxuICAgICAgJ2NsaWNrJyxcbiAgICAgIHRoaXMubG9jYWxlSXRlbVNlbGVjdG9yLFxuICAgICAgdGhpcy50b2dnbGVMYW5ndWFnZS5iaW5kKHRoaXMpLFxuICAgICk7XG4gICAgRXZlbnRFbWl0dGVyLm9uKCdsYW5ndWFnZVNlbGVjdGVkJywgdGhpcy50b2dnbGVJbnB1dHMuYmluZCh0aGlzKSk7XG5cbiAgICByZXR1cm4ge1xuICAgICAgbG9jYWxlSXRlbVNlbGVjdG9yOiB0aGlzLmxvY2FsZUl0ZW1TZWxlY3RvcixcbiAgICAgIGxvY2FsZUJ1dHRvblNlbGVjdG9yOiB0aGlzLmxvY2FsZUJ1dHRvblNlbGVjdG9yLFxuICAgICAgbG9jYWxlSW5wdXRTZWxlY3RvcjogdGhpcy5sb2NhbGVJbnB1dFNlbGVjdG9yLFxuXG4gICAgICAvKipcbiAgICAgICAqIEBwYXJhbSB7alF1ZXJ5fSBmb3JtXG4gICAgICAgKi9cbiAgICAgIHJlZnJlc2hGb3JtSW5wdXRzOiAoZm9ybSkgPT4geyB0aGlzLnJlZnJlc2hJbnB1dHMoZm9ybSk7IH0sXG5cbiAgICAgIC8qKlxuICAgICAgICogQHJldHVybnMge3N0cmluZ3x1bmRlZmluZWR9XG4gICAgICAgKi9cbiAgICAgIGdldFNlbGVjdGVkTG9jYWxlOiAoKSA9PiB0aGlzLnNlbGVjdGVkTG9jYWxlLFxuICAgIH07XG4gIH1cblxuICAvKipcbiAgICogQHBhcmFtIHtqUXVlcnl9IGZvcm1cbiAgICpcbiAgICogQHByaXZhdGVcbiAgICovXG4gIHJlZnJlc2hJbnB1dHMoZm9ybSkge1xuICAgIGlmICghdGhpcy5zZWxlY3RlZExvY2FsZSkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIEV2ZW50RW1pdHRlci5lbWl0KCdsYW5ndWFnZVNlbGVjdGVkJywge1xuICAgICAgc2VsZWN0ZWRMb2NhbGU6IHRoaXMuc2VsZWN0ZWRMb2NhbGUsXG4gICAgICBmb3JtLFxuICAgIH0pO1xuICB9XG5cbiAgLyoqXG4gICAqIERpc3BhdGNoIGV2ZW50IG9uIGxhbmd1YWdlIHNlbGVjdGlvbiB0byB1cGRhdGUgaW5wdXRzIGFuZCBvdGhlciBjb21wb25lbnRzIHdoaWNoIGRlcGVuZCBvbiB0aGUgbG9jYWxlLlxuICAgKlxuICAgKiBAcGFyYW0gZXZlbnRcbiAgICpcbiAgICogQHByaXZhdGVcbiAgICovXG4gIHRvZ2dsZUxhbmd1YWdlKGV2ZW50KSB7XG4gICAgY29uc3QgbG9jYWxlSXRlbSA9ICQoZXZlbnQudGFyZ2V0KTtcbiAgICBjb25zdCBmb3JtID0gbG9jYWxlSXRlbS5jbG9zZXN0KCdmb3JtJyk7XG4gICAgdGhpcy5zZWxlY3RlZExvY2FsZSA9IGxvY2FsZUl0ZW0uZGF0YSgnbG9jYWxlJyk7XG4gICAgdGhpcy5yZWZyZXNoSW5wdXRzKGZvcm0pO1xuICB9XG5cbiAgLyoqXG4gICAqIFRvZ2dsZSBhbGwgdHJhbnNsYXRhYmxlIGlucHV0cyBpbiBmb3JtIGluIHdoaWNoIGxvY2FsZSB3YXMgY2hhbmdlZFxuICAgKlxuICAgKiBAcGFyYW0ge0V2ZW50fSBldmVudFxuICAgKlxuICAgKiBAcHJpdmF0ZVxuICAgKi9cbiAgdG9nZ2xlSW5wdXRzKGV2ZW50KSB7XG4gICAgY29uc3Qge2Zvcm19ID0gZXZlbnQ7XG4gICAgdGhpcy5zZWxlY3RlZExvY2FsZSA9IGV2ZW50LnNlbGVjdGVkTG9jYWxlO1xuICAgIGNvbnN0IGxvY2FsZUJ1dHRvbiA9IGZvcm0uZmluZCh0aGlzLmxvY2FsZUJ1dHRvblNlbGVjdG9yKTtcbiAgICBjb25zdCBjaGFuZ2VMYW5ndWFnZVVybCA9IGxvY2FsZUJ1dHRvbi5kYXRhKCdjaGFuZ2UtbGFuZ3VhZ2UtdXJsJyk7XG5cbiAgICBsb2NhbGVCdXR0b24udGV4dCh0aGlzLnNlbGVjdGVkTG9jYWxlKTtcbiAgICBmb3JtLmZpbmQodGhpcy5sb2NhbGVJbnB1dFNlbGVjdG9yKS5hZGRDbGFzcygnZC1ub25lJyk7XG4gICAgZm9ybVxuICAgICAgLmZpbmQoYCR7dGhpcy5sb2NhbGVJbnB1dFNlbGVjdG9yfS5qcy1sb2NhbGUtJHt0aGlzLnNlbGVjdGVkTG9jYWxlfWApXG4gICAgICAucmVtb3ZlQ2xhc3MoJ2Qtbm9uZScpO1xuXG4gICAgaWYgKGNoYW5nZUxhbmd1YWdlVXJsKSB7XG4gICAgICB0aGlzLnNhdmVTZWxlY3RlZExhbmd1YWdlKGNoYW5nZUxhbmd1YWdlVXJsLCB0aGlzLnNlbGVjdGVkTG9jYWxlKTtcbiAgICB9XG4gIH1cblxuICAvKipcbiAgICogU2F2ZSBsYW5ndWFnZSBjaG9pY2UgZm9yIGVtcGxveWVlIGZvcm1zLlxuICAgKlxuICAgKiBAcGFyYW0ge1N0cmluZ30gY2hhbmdlTGFuZ3VhZ2VVcmxcbiAgICogQHBhcmFtIHtTdHJpbmd9IHNlbGVjdGVkTG9jYWxlXG4gICAqXG4gICAqIEBwcml2YXRlXG4gICAqL1xuICBzYXZlU2VsZWN0ZWRMYW5ndWFnZShjaGFuZ2VMYW5ndWFnZVVybCwgc2VsZWN0ZWRMb2NhbGUpIHtcbiAgICAkLnBvc3Qoe1xuICAgICAgdXJsOiBjaGFuZ2VMYW5ndWFnZVVybCxcbiAgICAgIGRhdGE6IHtcbiAgICAgICAgbGFuZ3VhZ2VfaXNvX2NvZGU6IHNlbGVjdGVkTG9jYWxlLFxuICAgICAgfSxcbiAgICB9KTtcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBUcmFuc2xhdGFibGVJbnB1dDtcbiIsIm1vZHVsZS5leHBvcnRzID0geyBcImRlZmF1bHRcIjogcmVxdWlyZShcImNvcmUtanMvbGlicmFyeS9mbi9vYmplY3QvZGVmaW5lLXByb3BlcnR5XCIpLCBfX2VzTW9kdWxlOiB0cnVlIH07IiwiXCJ1c2Ugc3RyaWN0XCI7XG5cbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IGZ1bmN0aW9uIChpbnN0YW5jZSwgQ29uc3RydWN0b3IpIHtcbiAgaWYgKCEoaW5zdGFuY2UgaW5zdGFuY2VvZiBDb25zdHJ1Y3RvcikpIHtcbiAgICB0aHJvdyBuZXcgVHlwZUVycm9yKFwiQ2Fubm90IGNhbGwgYSBjbGFzcyBhcyBhIGZ1bmN0aW9uXCIpO1xuICB9XG59OyIsIlwidXNlIHN0cmljdFwiO1xuXG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xuXG52YXIgX2RlZmluZVByb3BlcnR5ID0gcmVxdWlyZShcIi4uL2NvcmUtanMvb2JqZWN0L2RlZmluZS1wcm9wZXJ0eVwiKTtcblxudmFyIF9kZWZpbmVQcm9wZXJ0eTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9kZWZpbmVQcm9wZXJ0eSk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbmV4cG9ydHMuZGVmYXVsdCA9IGZ1bmN0aW9uICgpIHtcbiAgZnVuY3Rpb24gZGVmaW5lUHJvcGVydGllcyh0YXJnZXQsIHByb3BzKSB7XG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBwcm9wcy5sZW5ndGg7IGkrKykge1xuICAgICAgdmFyIGRlc2NyaXB0b3IgPSBwcm9wc1tpXTtcbiAgICAgIGRlc2NyaXB0b3IuZW51bWVyYWJsZSA9IGRlc2NyaXB0b3IuZW51bWVyYWJsZSB8fCBmYWxzZTtcbiAgICAgIGRlc2NyaXB0b3IuY29uZmlndXJhYmxlID0gdHJ1ZTtcbiAgICAgIGlmIChcInZhbHVlXCIgaW4gZGVzY3JpcHRvcikgZGVzY3JpcHRvci53cml0YWJsZSA9IHRydWU7XG4gICAgICAoMCwgX2RlZmluZVByb3BlcnR5Mi5kZWZhdWx0KSh0YXJnZXQsIGRlc2NyaXB0b3Iua2V5LCBkZXNjcmlwdG9yKTtcbiAgICB9XG4gIH1cblxuICByZXR1cm4gZnVuY3Rpb24gKENvbnN0cnVjdG9yLCBwcm90b1Byb3BzLCBzdGF0aWNQcm9wcykge1xuICAgIGlmIChwcm90b1Byb3BzKSBkZWZpbmVQcm9wZXJ0aWVzKENvbnN0cnVjdG9yLnByb3RvdHlwZSwgcHJvdG9Qcm9wcyk7XG4gICAgaWYgKHN0YXRpY1Byb3BzKSBkZWZpbmVQcm9wZXJ0aWVzKENvbnN0cnVjdG9yLCBzdGF0aWNQcm9wcyk7XG4gICAgcmV0dXJuIENvbnN0cnVjdG9yO1xuICB9O1xufSgpOyIsInJlcXVpcmUoJy4uLy4uL21vZHVsZXMvZXM2Lm9iamVjdC5kZWZpbmUtcHJvcGVydHknKTtcbnZhciAkT2JqZWN0ID0gcmVxdWlyZSgnLi4vLi4vbW9kdWxlcy9fY29yZScpLk9iamVjdDtcbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gZGVmaW5lUHJvcGVydHkoaXQsIGtleSwgZGVzYykge1xuICByZXR1cm4gJE9iamVjdC5kZWZpbmVQcm9wZXJ0eShpdCwga2V5LCBkZXNjKTtcbn07XG4iLCJtb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIChpdCkge1xuICBpZiAodHlwZW9mIGl0ICE9ICdmdW5jdGlvbicpIHRocm93IFR5cGVFcnJvcihpdCArICcgaXMgbm90IGEgZnVuY3Rpb24hJyk7XG4gIHJldHVybiBpdDtcbn07XG4iLCJ2YXIgaXNPYmplY3QgPSByZXF1aXJlKCcuL19pcy1vYmplY3QnKTtcbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKGl0KSB7XG4gIGlmICghaXNPYmplY3QoaXQpKSB0aHJvdyBUeXBlRXJyb3IoaXQgKyAnIGlzIG5vdCBhbiBvYmplY3QhJyk7XG4gIHJldHVybiBpdDtcbn07XG4iLCJ2YXIgY29yZSA9IG1vZHVsZS5leHBvcnRzID0geyB2ZXJzaW9uOiAnMi42LjExJyB9O1xuaWYgKHR5cGVvZiBfX2UgPT0gJ251bWJlcicpIF9fZSA9IGNvcmU7IC8vIGVzbGludC1kaXNhYmxlLWxpbmUgbm8tdW5kZWZcbiIsIi8vIG9wdGlvbmFsIC8gc2ltcGxlIGNvbnRleHQgYmluZGluZ1xudmFyIGFGdW5jdGlvbiA9IHJlcXVpcmUoJy4vX2EtZnVuY3Rpb24nKTtcbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKGZuLCB0aGF0LCBsZW5ndGgpIHtcbiAgYUZ1bmN0aW9uKGZuKTtcbiAgaWYgKHRoYXQgPT09IHVuZGVmaW5lZCkgcmV0dXJuIGZuO1xuICBzd2l0Y2ggKGxlbmd0aCkge1xuICAgIGNhc2UgMTogcmV0dXJuIGZ1bmN0aW9uIChhKSB7XG4gICAgICByZXR1cm4gZm4uY2FsbCh0aGF0LCBhKTtcbiAgICB9O1xuICAgIGNhc2UgMjogcmV0dXJuIGZ1bmN0aW9uIChhLCBiKSB7XG4gICAgICByZXR1cm4gZm4uY2FsbCh0aGF0LCBhLCBiKTtcbiAgICB9O1xuICAgIGNhc2UgMzogcmV0dXJuIGZ1bmN0aW9uIChhLCBiLCBjKSB7XG4gICAgICByZXR1cm4gZm4uY2FsbCh0aGF0LCBhLCBiLCBjKTtcbiAgICB9O1xuICB9XG4gIHJldHVybiBmdW5jdGlvbiAoLyogLi4uYXJncyAqLykge1xuICAgIHJldHVybiBmbi5hcHBseSh0aGF0LCBhcmd1bWVudHMpO1xuICB9O1xufTtcbiIsIi8vIFRoYW5rJ3MgSUU4IGZvciBoaXMgZnVubnkgZGVmaW5lUHJvcGVydHlcbm1vZHVsZS5leHBvcnRzID0gIXJlcXVpcmUoJy4vX2ZhaWxzJykoZnVuY3Rpb24gKCkge1xuICByZXR1cm4gT2JqZWN0LmRlZmluZVByb3BlcnR5KHt9LCAnYScsIHsgZ2V0OiBmdW5jdGlvbiAoKSB7IHJldHVybiA3OyB9IH0pLmEgIT0gNztcbn0pO1xuIiwidmFyIGlzT2JqZWN0ID0gcmVxdWlyZSgnLi9faXMtb2JqZWN0Jyk7XG52YXIgZG9jdW1lbnQgPSByZXF1aXJlKCcuL19nbG9iYWwnKS5kb2N1bWVudDtcbi8vIHR5cGVvZiBkb2N1bWVudC5jcmVhdGVFbGVtZW50IGlzICdvYmplY3QnIGluIG9sZCBJRVxudmFyIGlzID0gaXNPYmplY3QoZG9jdW1lbnQpICYmIGlzT2JqZWN0KGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQpO1xubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAoaXQpIHtcbiAgcmV0dXJuIGlzID8gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChpdCkgOiB7fTtcbn07XG4iLCJ2YXIgZ2xvYmFsID0gcmVxdWlyZSgnLi9fZ2xvYmFsJyk7XG52YXIgY29yZSA9IHJlcXVpcmUoJy4vX2NvcmUnKTtcbnZhciBjdHggPSByZXF1aXJlKCcuL19jdHgnKTtcbnZhciBoaWRlID0gcmVxdWlyZSgnLi9faGlkZScpO1xudmFyIGhhcyA9IHJlcXVpcmUoJy4vX2hhcycpO1xudmFyIFBST1RPVFlQRSA9ICdwcm90b3R5cGUnO1xuXG52YXIgJGV4cG9ydCA9IGZ1bmN0aW9uICh0eXBlLCBuYW1lLCBzb3VyY2UpIHtcbiAgdmFyIElTX0ZPUkNFRCA9IHR5cGUgJiAkZXhwb3J0LkY7XG4gIHZhciBJU19HTE9CQUwgPSB0eXBlICYgJGV4cG9ydC5HO1xuICB2YXIgSVNfU1RBVElDID0gdHlwZSAmICRleHBvcnQuUztcbiAgdmFyIElTX1BST1RPID0gdHlwZSAmICRleHBvcnQuUDtcbiAgdmFyIElTX0JJTkQgPSB0eXBlICYgJGV4cG9ydC5CO1xuICB2YXIgSVNfV1JBUCA9IHR5cGUgJiAkZXhwb3J0Llc7XG4gIHZhciBleHBvcnRzID0gSVNfR0xPQkFMID8gY29yZSA6IGNvcmVbbmFtZV0gfHwgKGNvcmVbbmFtZV0gPSB7fSk7XG4gIHZhciBleHBQcm90byA9IGV4cG9ydHNbUFJPVE9UWVBFXTtcbiAgdmFyIHRhcmdldCA9IElTX0dMT0JBTCA/IGdsb2JhbCA6IElTX1NUQVRJQyA/IGdsb2JhbFtuYW1lXSA6IChnbG9iYWxbbmFtZV0gfHwge30pW1BST1RPVFlQRV07XG4gIHZhciBrZXksIG93biwgb3V0O1xuICBpZiAoSVNfR0xPQkFMKSBzb3VyY2UgPSBuYW1lO1xuICBmb3IgKGtleSBpbiBzb3VyY2UpIHtcbiAgICAvLyBjb250YWlucyBpbiBuYXRpdmVcbiAgICBvd24gPSAhSVNfRk9SQ0VEICYmIHRhcmdldCAmJiB0YXJnZXRba2V5XSAhPT0gdW5kZWZpbmVkO1xuICAgIGlmIChvd24gJiYgaGFzKGV4cG9ydHMsIGtleSkpIGNvbnRpbnVlO1xuICAgIC8vIGV4cG9ydCBuYXRpdmUgb3IgcGFzc2VkXG4gICAgb3V0ID0gb3duID8gdGFyZ2V0W2tleV0gOiBzb3VyY2Vba2V5XTtcbiAgICAvLyBwcmV2ZW50IGdsb2JhbCBwb2xsdXRpb24gZm9yIG5hbWVzcGFjZXNcbiAgICBleHBvcnRzW2tleV0gPSBJU19HTE9CQUwgJiYgdHlwZW9mIHRhcmdldFtrZXldICE9ICdmdW5jdGlvbicgPyBzb3VyY2Vba2V5XVxuICAgIC8vIGJpbmQgdGltZXJzIHRvIGdsb2JhbCBmb3IgY2FsbCBmcm9tIGV4cG9ydCBjb250ZXh0XG4gICAgOiBJU19CSU5EICYmIG93biA/IGN0eChvdXQsIGdsb2JhbClcbiAgICAvLyB3cmFwIGdsb2JhbCBjb25zdHJ1Y3RvcnMgZm9yIHByZXZlbnQgY2hhbmdlIHRoZW0gaW4gbGlicmFyeVxuICAgIDogSVNfV1JBUCAmJiB0YXJnZXRba2V5XSA9PSBvdXQgPyAoZnVuY3Rpb24gKEMpIHtcbiAgICAgIHZhciBGID0gZnVuY3Rpb24gKGEsIGIsIGMpIHtcbiAgICAgICAgaWYgKHRoaXMgaW5zdGFuY2VvZiBDKSB7XG4gICAgICAgICAgc3dpdGNoIChhcmd1bWVudHMubGVuZ3RoKSB7XG4gICAgICAgICAgICBjYXNlIDA6IHJldHVybiBuZXcgQygpO1xuICAgICAgICAgICAgY2FzZSAxOiByZXR1cm4gbmV3IEMoYSk7XG4gICAgICAgICAgICBjYXNlIDI6IHJldHVybiBuZXcgQyhhLCBiKTtcbiAgICAgICAgICB9IHJldHVybiBuZXcgQyhhLCBiLCBjKTtcbiAgICAgICAgfSByZXR1cm4gQy5hcHBseSh0aGlzLCBhcmd1bWVudHMpO1xuICAgICAgfTtcbiAgICAgIEZbUFJPVE9UWVBFXSA9IENbUFJPVE9UWVBFXTtcbiAgICAgIHJldHVybiBGO1xuICAgIC8vIG1ha2Ugc3RhdGljIHZlcnNpb25zIGZvciBwcm90b3R5cGUgbWV0aG9kc1xuICAgIH0pKG91dCkgOiBJU19QUk9UTyAmJiB0eXBlb2Ygb3V0ID09ICdmdW5jdGlvbicgPyBjdHgoRnVuY3Rpb24uY2FsbCwgb3V0KSA6IG91dDtcbiAgICAvLyBleHBvcnQgcHJvdG8gbWV0aG9kcyB0byBjb3JlLiVDT05TVFJVQ1RPUiUubWV0aG9kcy4lTkFNRSVcbiAgICBpZiAoSVNfUFJPVE8pIHtcbiAgICAgIChleHBvcnRzLnZpcnR1YWwgfHwgKGV4cG9ydHMudmlydHVhbCA9IHt9KSlba2V5XSA9IG91dDtcbiAgICAgIC8vIGV4cG9ydCBwcm90byBtZXRob2RzIHRvIGNvcmUuJUNPTlNUUlVDVE9SJS5wcm90b3R5cGUuJU5BTUUlXG4gICAgICBpZiAodHlwZSAmICRleHBvcnQuUiAmJiBleHBQcm90byAmJiAhZXhwUHJvdG9ba2V5XSkgaGlkZShleHBQcm90bywga2V5LCBvdXQpO1xuICAgIH1cbiAgfVxufTtcbi8vIHR5cGUgYml0bWFwXG4kZXhwb3J0LkYgPSAxOyAgIC8vIGZvcmNlZFxuJGV4cG9ydC5HID0gMjsgICAvLyBnbG9iYWxcbiRleHBvcnQuUyA9IDQ7ICAgLy8gc3RhdGljXG4kZXhwb3J0LlAgPSA4OyAgIC8vIHByb3RvXG4kZXhwb3J0LkIgPSAxNjsgIC8vIGJpbmRcbiRleHBvcnQuVyA9IDMyOyAgLy8gd3JhcFxuJGV4cG9ydC5VID0gNjQ7ICAvLyBzYWZlXG4kZXhwb3J0LlIgPSAxMjg7IC8vIHJlYWwgcHJvdG8gbWV0aG9kIGZvciBgbGlicmFyeWBcbm1vZHVsZS5leHBvcnRzID0gJGV4cG9ydDtcbiIsIm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKGV4ZWMpIHtcbiAgdHJ5IHtcbiAgICByZXR1cm4gISFleGVjKCk7XG4gIH0gY2F0Y2ggKGUpIHtcbiAgICByZXR1cm4gdHJ1ZTtcbiAgfVxufTtcbiIsIi8vIGh0dHBzOi8vZ2l0aHViLmNvbS96bG9pcm9jay9jb3JlLWpzL2lzc3Vlcy84NiNpc3N1ZWNvbW1lbnQtMTE1NzU5MDI4XG52YXIgZ2xvYmFsID0gbW9kdWxlLmV4cG9ydHMgPSB0eXBlb2Ygd2luZG93ICE9ICd1bmRlZmluZWQnICYmIHdpbmRvdy5NYXRoID09IE1hdGhcbiAgPyB3aW5kb3cgOiB0eXBlb2Ygc2VsZiAhPSAndW5kZWZpbmVkJyAmJiBzZWxmLk1hdGggPT0gTWF0aCA/IHNlbGZcbiAgLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lIG5vLW5ldy1mdW5jXG4gIDogRnVuY3Rpb24oJ3JldHVybiB0aGlzJykoKTtcbmlmICh0eXBlb2YgX19nID09ICdudW1iZXInKSBfX2cgPSBnbG9iYWw7IC8vIGVzbGludC1kaXNhYmxlLWxpbmUgbm8tdW5kZWZcbiIsInZhciBoYXNPd25Qcm9wZXJ0eSA9IHt9Lmhhc093blByb3BlcnR5O1xubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAoaXQsIGtleSkge1xuICByZXR1cm4gaGFzT3duUHJvcGVydHkuY2FsbChpdCwga2V5KTtcbn07XG4iLCJ2YXIgZFAgPSByZXF1aXJlKCcuL19vYmplY3QtZHAnKTtcbnZhciBjcmVhdGVEZXNjID0gcmVxdWlyZSgnLi9fcHJvcGVydHktZGVzYycpO1xubW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKCcuL19kZXNjcmlwdG9ycycpID8gZnVuY3Rpb24gKG9iamVjdCwga2V5LCB2YWx1ZSkge1xuICByZXR1cm4gZFAuZihvYmplY3QsIGtleSwgY3JlYXRlRGVzYygxLCB2YWx1ZSkpO1xufSA6IGZ1bmN0aW9uIChvYmplY3QsIGtleSwgdmFsdWUpIHtcbiAgb2JqZWN0W2tleV0gPSB2YWx1ZTtcbiAgcmV0dXJuIG9iamVjdDtcbn07XG4iLCJtb2R1bGUuZXhwb3J0cyA9ICFyZXF1aXJlKCcuL19kZXNjcmlwdG9ycycpICYmICFyZXF1aXJlKCcuL19mYWlscycpKGZ1bmN0aW9uICgpIHtcbiAgcmV0dXJuIE9iamVjdC5kZWZpbmVQcm9wZXJ0eShyZXF1aXJlKCcuL19kb20tY3JlYXRlJykoJ2RpdicpLCAnYScsIHsgZ2V0OiBmdW5jdGlvbiAoKSB7IHJldHVybiA3OyB9IH0pLmEgIT0gNztcbn0pO1xuIiwibW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAoaXQpIHtcbiAgcmV0dXJuIHR5cGVvZiBpdCA9PT0gJ29iamVjdCcgPyBpdCAhPT0gbnVsbCA6IHR5cGVvZiBpdCA9PT0gJ2Z1bmN0aW9uJztcbn07XG4iLCJ2YXIgYW5PYmplY3QgPSByZXF1aXJlKCcuL19hbi1vYmplY3QnKTtcbnZhciBJRThfRE9NX0RFRklORSA9IHJlcXVpcmUoJy4vX2llOC1kb20tZGVmaW5lJyk7XG52YXIgdG9QcmltaXRpdmUgPSByZXF1aXJlKCcuL190by1wcmltaXRpdmUnKTtcbnZhciBkUCA9IE9iamVjdC5kZWZpbmVQcm9wZXJ0eTtcblxuZXhwb3J0cy5mID0gcmVxdWlyZSgnLi9fZGVzY3JpcHRvcnMnKSA/IE9iamVjdC5kZWZpbmVQcm9wZXJ0eSA6IGZ1bmN0aW9uIGRlZmluZVByb3BlcnR5KE8sIFAsIEF0dHJpYnV0ZXMpIHtcbiAgYW5PYmplY3QoTyk7XG4gIFAgPSB0b1ByaW1pdGl2ZShQLCB0cnVlKTtcbiAgYW5PYmplY3QoQXR0cmlidXRlcyk7XG4gIGlmIChJRThfRE9NX0RFRklORSkgdHJ5IHtcbiAgICByZXR1cm4gZFAoTywgUCwgQXR0cmlidXRlcyk7XG4gIH0gY2F0Y2ggKGUpIHsgLyogZW1wdHkgKi8gfVxuICBpZiAoJ2dldCcgaW4gQXR0cmlidXRlcyB8fCAnc2V0JyBpbiBBdHRyaWJ1dGVzKSB0aHJvdyBUeXBlRXJyb3IoJ0FjY2Vzc29ycyBub3Qgc3VwcG9ydGVkIScpO1xuICBpZiAoJ3ZhbHVlJyBpbiBBdHRyaWJ1dGVzKSBPW1BdID0gQXR0cmlidXRlcy52YWx1ZTtcbiAgcmV0dXJuIE87XG59O1xuIiwibW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAoYml0bWFwLCB2YWx1ZSkge1xuICByZXR1cm4ge1xuICAgIGVudW1lcmFibGU6ICEoYml0bWFwICYgMSksXG4gICAgY29uZmlndXJhYmxlOiAhKGJpdG1hcCAmIDIpLFxuICAgIHdyaXRhYmxlOiAhKGJpdG1hcCAmIDQpLFxuICAgIHZhbHVlOiB2YWx1ZVxuICB9O1xufTtcbiIsIi8vIDcuMS4xIFRvUHJpbWl0aXZlKGlucHV0IFssIFByZWZlcnJlZFR5cGVdKVxudmFyIGlzT2JqZWN0ID0gcmVxdWlyZSgnLi9faXMtb2JqZWN0Jyk7XG4vLyBpbnN0ZWFkIG9mIHRoZSBFUzYgc3BlYyB2ZXJzaW9uLCB3ZSBkaWRuJ3QgaW1wbGVtZW50IEBAdG9QcmltaXRpdmUgY2FzZVxuLy8gYW5kIHRoZSBzZWNvbmQgYXJndW1lbnQgLSBmbGFnIC0gcHJlZmVycmVkIHR5cGUgaXMgYSBzdHJpbmdcbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKGl0LCBTKSB7XG4gIGlmICghaXNPYmplY3QoaXQpKSByZXR1cm4gaXQ7XG4gIHZhciBmbiwgdmFsO1xuICBpZiAoUyAmJiB0eXBlb2YgKGZuID0gaXQudG9TdHJpbmcpID09ICdmdW5jdGlvbicgJiYgIWlzT2JqZWN0KHZhbCA9IGZuLmNhbGwoaXQpKSkgcmV0dXJuIHZhbDtcbiAgaWYgKHR5cGVvZiAoZm4gPSBpdC52YWx1ZU9mKSA9PSAnZnVuY3Rpb24nICYmICFpc09iamVjdCh2YWwgPSBmbi5jYWxsKGl0KSkpIHJldHVybiB2YWw7XG4gIGlmICghUyAmJiB0eXBlb2YgKGZuID0gaXQudG9TdHJpbmcpID09ICdmdW5jdGlvbicgJiYgIWlzT2JqZWN0KHZhbCA9IGZuLmNhbGwoaXQpKSkgcmV0dXJuIHZhbDtcbiAgdGhyb3cgVHlwZUVycm9yKFwiQ2FuJ3QgY29udmVydCBvYmplY3QgdG8gcHJpbWl0aXZlIHZhbHVlXCIpO1xufTtcbiIsInZhciAkZXhwb3J0ID0gcmVxdWlyZSgnLi9fZXhwb3J0Jyk7XG4vLyAxOS4xLjIuNCAvIDE1LjIuMy42IE9iamVjdC5kZWZpbmVQcm9wZXJ0eShPLCBQLCBBdHRyaWJ1dGVzKVxuJGV4cG9ydCgkZXhwb3J0LlMgKyAkZXhwb3J0LkYgKiAhcmVxdWlyZSgnLi9fZGVzY3JpcHRvcnMnKSwgJ09iamVjdCcsIHsgZGVmaW5lUHJvcGVydHk6IHJlcXVpcmUoJy4vX29iamVjdC1kcCcpLmYgfSk7XG4iLCIvLyBDb3B5cmlnaHQgSm95ZW50LCBJbmMuIGFuZCBvdGhlciBOb2RlIGNvbnRyaWJ1dG9ycy5cbi8vXG4vLyBQZXJtaXNzaW9uIGlzIGhlcmVieSBncmFudGVkLCBmcmVlIG9mIGNoYXJnZSwgdG8gYW55IHBlcnNvbiBvYnRhaW5pbmcgYVxuLy8gY29weSBvZiB0aGlzIHNvZnR3YXJlIGFuZCBhc3NvY2lhdGVkIGRvY3VtZW50YXRpb24gZmlsZXMgKHRoZVxuLy8gXCJTb2Z0d2FyZVwiKSwgdG8gZGVhbCBpbiB0aGUgU29mdHdhcmUgd2l0aG91dCByZXN0cmljdGlvbiwgaW5jbHVkaW5nXG4vLyB3aXRob3V0IGxpbWl0YXRpb24gdGhlIHJpZ2h0cyB0byB1c2UsIGNvcHksIG1vZGlmeSwgbWVyZ2UsIHB1Ymxpc2gsXG4vLyBkaXN0cmlidXRlLCBzdWJsaWNlbnNlLCBhbmQvb3Igc2VsbCBjb3BpZXMgb2YgdGhlIFNvZnR3YXJlLCBhbmQgdG8gcGVybWl0XG4vLyBwZXJzb25zIHRvIHdob20gdGhlIFNvZnR3YXJlIGlzIGZ1cm5pc2hlZCB0byBkbyBzbywgc3ViamVjdCB0byB0aGVcbi8vIGZvbGxvd2luZyBjb25kaXRpb25zOlxuLy9cbi8vIFRoZSBhYm92ZSBjb3B5cmlnaHQgbm90aWNlIGFuZCB0aGlzIHBlcm1pc3Npb24gbm90aWNlIHNoYWxsIGJlIGluY2x1ZGVkXG4vLyBpbiBhbGwgY29waWVzIG9yIHN1YnN0YW50aWFsIHBvcnRpb25zIG9mIHRoZSBTb2Z0d2FyZS5cbi8vXG4vLyBUSEUgU09GVFdBUkUgSVMgUFJPVklERUQgXCJBUyBJU1wiLCBXSVRIT1VUIFdBUlJBTlRZIE9GIEFOWSBLSU5ELCBFWFBSRVNTXG4vLyBPUiBJTVBMSUVELCBJTkNMVURJTkcgQlVUIE5PVCBMSU1JVEVEIFRPIFRIRSBXQVJSQU5USUVTIE9GXG4vLyBNRVJDSEFOVEFCSUxJVFksIEZJVE5FU1MgRk9SIEEgUEFSVElDVUxBUiBQVVJQT1NFIEFORCBOT05JTkZSSU5HRU1FTlQuIElOXG4vLyBOTyBFVkVOVCBTSEFMTCBUSEUgQVVUSE9SUyBPUiBDT1BZUklHSFQgSE9MREVSUyBCRSBMSUFCTEUgRk9SIEFOWSBDTEFJTSxcbi8vIERBTUFHRVMgT1IgT1RIRVIgTElBQklMSVRZLCBXSEVUSEVSIElOIEFOIEFDVElPTiBPRiBDT05UUkFDVCwgVE9SVCBPUlxuLy8gT1RIRVJXSVNFLCBBUklTSU5HIEZST00sIE9VVCBPRiBPUiBJTiBDT05ORUNUSU9OIFdJVEggVEhFIFNPRlRXQVJFIE9SIFRIRVxuLy8gVVNFIE9SIE9USEVSIERFQUxJTkdTIElOIFRIRSBTT0ZUV0FSRS5cblxuJ3VzZSBzdHJpY3QnO1xuXG52YXIgUiA9IHR5cGVvZiBSZWZsZWN0ID09PSAnb2JqZWN0JyA/IFJlZmxlY3QgOiBudWxsXG52YXIgUmVmbGVjdEFwcGx5ID0gUiAmJiB0eXBlb2YgUi5hcHBseSA9PT0gJ2Z1bmN0aW9uJ1xuICA/IFIuYXBwbHlcbiAgOiBmdW5jdGlvbiBSZWZsZWN0QXBwbHkodGFyZ2V0LCByZWNlaXZlciwgYXJncykge1xuICAgIHJldHVybiBGdW5jdGlvbi5wcm90b3R5cGUuYXBwbHkuY2FsbCh0YXJnZXQsIHJlY2VpdmVyLCBhcmdzKTtcbiAgfVxuXG52YXIgUmVmbGVjdE93bktleXNcbmlmIChSICYmIHR5cGVvZiBSLm93bktleXMgPT09ICdmdW5jdGlvbicpIHtcbiAgUmVmbGVjdE93bktleXMgPSBSLm93bktleXNcbn0gZWxzZSBpZiAoT2JqZWN0LmdldE93blByb3BlcnR5U3ltYm9scykge1xuICBSZWZsZWN0T3duS2V5cyA9IGZ1bmN0aW9uIFJlZmxlY3RPd25LZXlzKHRhcmdldCkge1xuICAgIHJldHVybiBPYmplY3QuZ2V0T3duUHJvcGVydHlOYW1lcyh0YXJnZXQpXG4gICAgICAuY29uY2F0KE9iamVjdC5nZXRPd25Qcm9wZXJ0eVN5bWJvbHModGFyZ2V0KSk7XG4gIH07XG59IGVsc2Uge1xuICBSZWZsZWN0T3duS2V5cyA9IGZ1bmN0aW9uIFJlZmxlY3RPd25LZXlzKHRhcmdldCkge1xuICAgIHJldHVybiBPYmplY3QuZ2V0T3duUHJvcGVydHlOYW1lcyh0YXJnZXQpO1xuICB9O1xufVxuXG5mdW5jdGlvbiBQcm9jZXNzRW1pdFdhcm5pbmcod2FybmluZykge1xuICBpZiAoY29uc29sZSAmJiBjb25zb2xlLndhcm4pIGNvbnNvbGUud2Fybih3YXJuaW5nKTtcbn1cblxudmFyIE51bWJlcklzTmFOID0gTnVtYmVyLmlzTmFOIHx8IGZ1bmN0aW9uIE51bWJlcklzTmFOKHZhbHVlKSB7XG4gIHJldHVybiB2YWx1ZSAhPT0gdmFsdWU7XG59XG5cbmZ1bmN0aW9uIEV2ZW50RW1pdHRlcigpIHtcbiAgRXZlbnRFbWl0dGVyLmluaXQuY2FsbCh0aGlzKTtcbn1cbm1vZHVsZS5leHBvcnRzID0gRXZlbnRFbWl0dGVyO1xuXG4vLyBCYWNrd2FyZHMtY29tcGF0IHdpdGggbm9kZSAwLjEwLnhcbkV2ZW50RW1pdHRlci5FdmVudEVtaXR0ZXIgPSBFdmVudEVtaXR0ZXI7XG5cbkV2ZW50RW1pdHRlci5wcm90b3R5cGUuX2V2ZW50cyA9IHVuZGVmaW5lZDtcbkV2ZW50RW1pdHRlci5wcm90b3R5cGUuX2V2ZW50c0NvdW50ID0gMDtcbkV2ZW50RW1pdHRlci5wcm90b3R5cGUuX21heExpc3RlbmVycyA9IHVuZGVmaW5lZDtcblxuLy8gQnkgZGVmYXVsdCBFdmVudEVtaXR0ZXJzIHdpbGwgcHJpbnQgYSB3YXJuaW5nIGlmIG1vcmUgdGhhbiAxMCBsaXN0ZW5lcnMgYXJlXG4vLyBhZGRlZCB0byBpdC4gVGhpcyBpcyBhIHVzZWZ1bCBkZWZhdWx0IHdoaWNoIGhlbHBzIGZpbmRpbmcgbWVtb3J5IGxlYWtzLlxudmFyIGRlZmF1bHRNYXhMaXN0ZW5lcnMgPSAxMDtcblxuZnVuY3Rpb24gY2hlY2tMaXN0ZW5lcihsaXN0ZW5lcikge1xuICBpZiAodHlwZW9mIGxpc3RlbmVyICE9PSAnZnVuY3Rpb24nKSB7XG4gICAgdGhyb3cgbmV3IFR5cGVFcnJvcignVGhlIFwibGlzdGVuZXJcIiBhcmd1bWVudCBtdXN0IGJlIG9mIHR5cGUgRnVuY3Rpb24uIFJlY2VpdmVkIHR5cGUgJyArIHR5cGVvZiBsaXN0ZW5lcik7XG4gIH1cbn1cblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KEV2ZW50RW1pdHRlciwgJ2RlZmF1bHRNYXhMaXN0ZW5lcnMnLCB7XG4gIGVudW1lcmFibGU6IHRydWUsXG4gIGdldDogZnVuY3Rpb24oKSB7XG4gICAgcmV0dXJuIGRlZmF1bHRNYXhMaXN0ZW5lcnM7XG4gIH0sXG4gIHNldDogZnVuY3Rpb24oYXJnKSB7XG4gICAgaWYgKHR5cGVvZiBhcmcgIT09ICdudW1iZXInIHx8IGFyZyA8IDAgfHwgTnVtYmVySXNOYU4oYXJnKSkge1xuICAgICAgdGhyb3cgbmV3IFJhbmdlRXJyb3IoJ1RoZSB2YWx1ZSBvZiBcImRlZmF1bHRNYXhMaXN0ZW5lcnNcIiBpcyBvdXQgb2YgcmFuZ2UuIEl0IG11c3QgYmUgYSBub24tbmVnYXRpdmUgbnVtYmVyLiBSZWNlaXZlZCAnICsgYXJnICsgJy4nKTtcbiAgICB9XG4gICAgZGVmYXVsdE1heExpc3RlbmVycyA9IGFyZztcbiAgfVxufSk7XG5cbkV2ZW50RW1pdHRlci5pbml0ID0gZnVuY3Rpb24oKSB7XG5cbiAgaWYgKHRoaXMuX2V2ZW50cyA9PT0gdW5kZWZpbmVkIHx8XG4gICAgICB0aGlzLl9ldmVudHMgPT09IE9iamVjdC5nZXRQcm90b3R5cGVPZih0aGlzKS5fZXZlbnRzKSB7XG4gICAgdGhpcy5fZXZlbnRzID0gT2JqZWN0LmNyZWF0ZShudWxsKTtcbiAgICB0aGlzLl9ldmVudHNDb3VudCA9IDA7XG4gIH1cblxuICB0aGlzLl9tYXhMaXN0ZW5lcnMgPSB0aGlzLl9tYXhMaXN0ZW5lcnMgfHwgdW5kZWZpbmVkO1xufTtcblxuLy8gT2J2aW91c2x5IG5vdCBhbGwgRW1pdHRlcnMgc2hvdWxkIGJlIGxpbWl0ZWQgdG8gMTAuIFRoaXMgZnVuY3Rpb24gYWxsb3dzXG4vLyB0aGF0IHRvIGJlIGluY3JlYXNlZC4gU2V0IHRvIHplcm8gZm9yIHVubGltaXRlZC5cbkV2ZW50RW1pdHRlci5wcm90b3R5cGUuc2V0TWF4TGlzdGVuZXJzID0gZnVuY3Rpb24gc2V0TWF4TGlzdGVuZXJzKG4pIHtcbiAgaWYgKHR5cGVvZiBuICE9PSAnbnVtYmVyJyB8fCBuIDwgMCB8fCBOdW1iZXJJc05hTihuKSkge1xuICAgIHRocm93IG5ldyBSYW5nZUVycm9yKCdUaGUgdmFsdWUgb2YgXCJuXCIgaXMgb3V0IG9mIHJhbmdlLiBJdCBtdXN0IGJlIGEgbm9uLW5lZ2F0aXZlIG51bWJlci4gUmVjZWl2ZWQgJyArIG4gKyAnLicpO1xuICB9XG4gIHRoaXMuX21heExpc3RlbmVycyA9IG47XG4gIHJldHVybiB0aGlzO1xufTtcblxuZnVuY3Rpb24gX2dldE1heExpc3RlbmVycyh0aGF0KSB7XG4gIGlmICh0aGF0Ll9tYXhMaXN0ZW5lcnMgPT09IHVuZGVmaW5lZClcbiAgICByZXR1cm4gRXZlbnRFbWl0dGVyLmRlZmF1bHRNYXhMaXN0ZW5lcnM7XG4gIHJldHVybiB0aGF0Ll9tYXhMaXN0ZW5lcnM7XG59XG5cbkV2ZW50RW1pdHRlci5wcm90b3R5cGUuZ2V0TWF4TGlzdGVuZXJzID0gZnVuY3Rpb24gZ2V0TWF4TGlzdGVuZXJzKCkge1xuICByZXR1cm4gX2dldE1heExpc3RlbmVycyh0aGlzKTtcbn07XG5cbkV2ZW50RW1pdHRlci5wcm90b3R5cGUuZW1pdCA9IGZ1bmN0aW9uIGVtaXQodHlwZSkge1xuICB2YXIgYXJncyA9IFtdO1xuICBmb3IgKHZhciBpID0gMTsgaSA8IGFyZ3VtZW50cy5sZW5ndGg7IGkrKykgYXJncy5wdXNoKGFyZ3VtZW50c1tpXSk7XG4gIHZhciBkb0Vycm9yID0gKHR5cGUgPT09ICdlcnJvcicpO1xuXG4gIHZhciBldmVudHMgPSB0aGlzLl9ldmVudHM7XG4gIGlmIChldmVudHMgIT09IHVuZGVmaW5lZClcbiAgICBkb0Vycm9yID0gKGRvRXJyb3IgJiYgZXZlbnRzLmVycm9yID09PSB1bmRlZmluZWQpO1xuICBlbHNlIGlmICghZG9FcnJvcilcbiAgICByZXR1cm4gZmFsc2U7XG5cbiAgLy8gSWYgdGhlcmUgaXMgbm8gJ2Vycm9yJyBldmVudCBsaXN0ZW5lciB0aGVuIHRocm93LlxuICBpZiAoZG9FcnJvcikge1xuICAgIHZhciBlcjtcbiAgICBpZiAoYXJncy5sZW5ndGggPiAwKVxuICAgICAgZXIgPSBhcmdzWzBdO1xuICAgIGlmIChlciBpbnN0YW5jZW9mIEVycm9yKSB7XG4gICAgICAvLyBOb3RlOiBUaGUgY29tbWVudHMgb24gdGhlIGB0aHJvd2AgbGluZXMgYXJlIGludGVudGlvbmFsLCB0aGV5IHNob3dcbiAgICAgIC8vIHVwIGluIE5vZGUncyBvdXRwdXQgaWYgdGhpcyByZXN1bHRzIGluIGFuIHVuaGFuZGxlZCBleGNlcHRpb24uXG4gICAgICB0aHJvdyBlcjsgLy8gVW5oYW5kbGVkICdlcnJvcicgZXZlbnRcbiAgICB9XG4gICAgLy8gQXQgbGVhc3QgZ2l2ZSBzb21lIGtpbmQgb2YgY29udGV4dCB0byB0aGUgdXNlclxuICAgIHZhciBlcnIgPSBuZXcgRXJyb3IoJ1VuaGFuZGxlZCBlcnJvci4nICsgKGVyID8gJyAoJyArIGVyLm1lc3NhZ2UgKyAnKScgOiAnJykpO1xuICAgIGVyci5jb250ZXh0ID0gZXI7XG4gICAgdGhyb3cgZXJyOyAvLyBVbmhhbmRsZWQgJ2Vycm9yJyBldmVudFxuICB9XG5cbiAgdmFyIGhhbmRsZXIgPSBldmVudHNbdHlwZV07XG5cbiAgaWYgKGhhbmRsZXIgPT09IHVuZGVmaW5lZClcbiAgICByZXR1cm4gZmFsc2U7XG5cbiAgaWYgKHR5cGVvZiBoYW5kbGVyID09PSAnZnVuY3Rpb24nKSB7XG4gICAgUmVmbGVjdEFwcGx5KGhhbmRsZXIsIHRoaXMsIGFyZ3MpO1xuICB9IGVsc2Uge1xuICAgIHZhciBsZW4gPSBoYW5kbGVyLmxlbmd0aDtcbiAgICB2YXIgbGlzdGVuZXJzID0gYXJyYXlDbG9uZShoYW5kbGVyLCBsZW4pO1xuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgbGVuOyArK2kpXG4gICAgICBSZWZsZWN0QXBwbHkobGlzdGVuZXJzW2ldLCB0aGlzLCBhcmdzKTtcbiAgfVxuXG4gIHJldHVybiB0cnVlO1xufTtcblxuZnVuY3Rpb24gX2FkZExpc3RlbmVyKHRhcmdldCwgdHlwZSwgbGlzdGVuZXIsIHByZXBlbmQpIHtcbiAgdmFyIG07XG4gIHZhciBldmVudHM7XG4gIHZhciBleGlzdGluZztcblxuICBjaGVja0xpc3RlbmVyKGxpc3RlbmVyKTtcblxuICBldmVudHMgPSB0YXJnZXQuX2V2ZW50cztcbiAgaWYgKGV2ZW50cyA9PT0gdW5kZWZpbmVkKSB7XG4gICAgZXZlbnRzID0gdGFyZ2V0Ll9ldmVudHMgPSBPYmplY3QuY3JlYXRlKG51bGwpO1xuICAgIHRhcmdldC5fZXZlbnRzQ291bnQgPSAwO1xuICB9IGVsc2Uge1xuICAgIC8vIFRvIGF2b2lkIHJlY3Vyc2lvbiBpbiB0aGUgY2FzZSB0aGF0IHR5cGUgPT09IFwibmV3TGlzdGVuZXJcIiEgQmVmb3JlXG4gICAgLy8gYWRkaW5nIGl0IHRvIHRoZSBsaXN0ZW5lcnMsIGZpcnN0IGVtaXQgXCJuZXdMaXN0ZW5lclwiLlxuICAgIGlmIChldmVudHMubmV3TGlzdGVuZXIgIT09IHVuZGVmaW5lZCkge1xuICAgICAgdGFyZ2V0LmVtaXQoJ25ld0xpc3RlbmVyJywgdHlwZSxcbiAgICAgICAgICAgICAgICAgIGxpc3RlbmVyLmxpc3RlbmVyID8gbGlzdGVuZXIubGlzdGVuZXIgOiBsaXN0ZW5lcik7XG5cbiAgICAgIC8vIFJlLWFzc2lnbiBgZXZlbnRzYCBiZWNhdXNlIGEgbmV3TGlzdGVuZXIgaGFuZGxlciBjb3VsZCBoYXZlIGNhdXNlZCB0aGVcbiAgICAgIC8vIHRoaXMuX2V2ZW50cyB0byBiZSBhc3NpZ25lZCB0byBhIG5ldyBvYmplY3RcbiAgICAgIGV2ZW50cyA9IHRhcmdldC5fZXZlbnRzO1xuICAgIH1cbiAgICBleGlzdGluZyA9IGV2ZW50c1t0eXBlXTtcbiAgfVxuXG4gIGlmIChleGlzdGluZyA9PT0gdW5kZWZpbmVkKSB7XG4gICAgLy8gT3B0aW1pemUgdGhlIGNhc2Ugb2Ygb25lIGxpc3RlbmVyLiBEb24ndCBuZWVkIHRoZSBleHRyYSBhcnJheSBvYmplY3QuXG4gICAgZXhpc3RpbmcgPSBldmVudHNbdHlwZV0gPSBsaXN0ZW5lcjtcbiAgICArK3RhcmdldC5fZXZlbnRzQ291bnQ7XG4gIH0gZWxzZSB7XG4gICAgaWYgKHR5cGVvZiBleGlzdGluZyA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgLy8gQWRkaW5nIHRoZSBzZWNvbmQgZWxlbWVudCwgbmVlZCB0byBjaGFuZ2UgdG8gYXJyYXkuXG4gICAgICBleGlzdGluZyA9IGV2ZW50c1t0eXBlXSA9XG4gICAgICAgIHByZXBlbmQgPyBbbGlzdGVuZXIsIGV4aXN0aW5nXSA6IFtleGlzdGluZywgbGlzdGVuZXJdO1xuICAgICAgLy8gSWYgd2UndmUgYWxyZWFkeSBnb3QgYW4gYXJyYXksIGp1c3QgYXBwZW5kLlxuICAgIH0gZWxzZSBpZiAocHJlcGVuZCkge1xuICAgICAgZXhpc3RpbmcudW5zaGlmdChsaXN0ZW5lcik7XG4gICAgfSBlbHNlIHtcbiAgICAgIGV4aXN0aW5nLnB1c2gobGlzdGVuZXIpO1xuICAgIH1cblxuICAgIC8vIENoZWNrIGZvciBsaXN0ZW5lciBsZWFrXG4gICAgbSA9IF9nZXRNYXhMaXN0ZW5lcnModGFyZ2V0KTtcbiAgICBpZiAobSA+IDAgJiYgZXhpc3RpbmcubGVuZ3RoID4gbSAmJiAhZXhpc3Rpbmcud2FybmVkKSB7XG4gICAgICBleGlzdGluZy53YXJuZWQgPSB0cnVlO1xuICAgICAgLy8gTm8gZXJyb3IgY29kZSBmb3IgdGhpcyBzaW5jZSBpdCBpcyBhIFdhcm5pbmdcbiAgICAgIC8vIGVzbGludC1kaXNhYmxlLW5leHQtbGluZSBuby1yZXN0cmljdGVkLXN5bnRheFxuICAgICAgdmFyIHcgPSBuZXcgRXJyb3IoJ1Bvc3NpYmxlIEV2ZW50RW1pdHRlciBtZW1vcnkgbGVhayBkZXRlY3RlZC4gJyArXG4gICAgICAgICAgICAgICAgICAgICAgICAgIGV4aXN0aW5nLmxlbmd0aCArICcgJyArIFN0cmluZyh0eXBlKSArICcgbGlzdGVuZXJzICcgK1xuICAgICAgICAgICAgICAgICAgICAgICAgICAnYWRkZWQuIFVzZSBlbWl0dGVyLnNldE1heExpc3RlbmVycygpIHRvICcgK1xuICAgICAgICAgICAgICAgICAgICAgICAgICAnaW5jcmVhc2UgbGltaXQnKTtcbiAgICAgIHcubmFtZSA9ICdNYXhMaXN0ZW5lcnNFeGNlZWRlZFdhcm5pbmcnO1xuICAgICAgdy5lbWl0dGVyID0gdGFyZ2V0O1xuICAgICAgdy50eXBlID0gdHlwZTtcbiAgICAgIHcuY291bnQgPSBleGlzdGluZy5sZW5ndGg7XG4gICAgICBQcm9jZXNzRW1pdFdhcm5pbmcodyk7XG4gICAgfVxuICB9XG5cbiAgcmV0dXJuIHRhcmdldDtcbn1cblxuRXZlbnRFbWl0dGVyLnByb3RvdHlwZS5hZGRMaXN0ZW5lciA9IGZ1bmN0aW9uIGFkZExpc3RlbmVyKHR5cGUsIGxpc3RlbmVyKSB7XG4gIHJldHVybiBfYWRkTGlzdGVuZXIodGhpcywgdHlwZSwgbGlzdGVuZXIsIGZhbHNlKTtcbn07XG5cbkV2ZW50RW1pdHRlci5wcm90b3R5cGUub24gPSBFdmVudEVtaXR0ZXIucHJvdG90eXBlLmFkZExpc3RlbmVyO1xuXG5FdmVudEVtaXR0ZXIucHJvdG90eXBlLnByZXBlbmRMaXN0ZW5lciA9XG4gICAgZnVuY3Rpb24gcHJlcGVuZExpc3RlbmVyKHR5cGUsIGxpc3RlbmVyKSB7XG4gICAgICByZXR1cm4gX2FkZExpc3RlbmVyKHRoaXMsIHR5cGUsIGxpc3RlbmVyLCB0cnVlKTtcbiAgICB9O1xuXG5mdW5jdGlvbiBvbmNlV3JhcHBlcigpIHtcbiAgaWYgKCF0aGlzLmZpcmVkKSB7XG4gICAgdGhpcy50YXJnZXQucmVtb3ZlTGlzdGVuZXIodGhpcy50eXBlLCB0aGlzLndyYXBGbik7XG4gICAgdGhpcy5maXJlZCA9IHRydWU7XG4gICAgaWYgKGFyZ3VtZW50cy5sZW5ndGggPT09IDApXG4gICAgICByZXR1cm4gdGhpcy5saXN0ZW5lci5jYWxsKHRoaXMudGFyZ2V0KTtcbiAgICByZXR1cm4gdGhpcy5saXN0ZW5lci5hcHBseSh0aGlzLnRhcmdldCwgYXJndW1lbnRzKTtcbiAgfVxufVxuXG5mdW5jdGlvbiBfb25jZVdyYXAodGFyZ2V0LCB0eXBlLCBsaXN0ZW5lcikge1xuICB2YXIgc3RhdGUgPSB7IGZpcmVkOiBmYWxzZSwgd3JhcEZuOiB1bmRlZmluZWQsIHRhcmdldDogdGFyZ2V0LCB0eXBlOiB0eXBlLCBsaXN0ZW5lcjogbGlzdGVuZXIgfTtcbiAgdmFyIHdyYXBwZWQgPSBvbmNlV3JhcHBlci5iaW5kKHN0YXRlKTtcbiAgd3JhcHBlZC5saXN0ZW5lciA9IGxpc3RlbmVyO1xuICBzdGF0ZS53cmFwRm4gPSB3cmFwcGVkO1xuICByZXR1cm4gd3JhcHBlZDtcbn1cblxuRXZlbnRFbWl0dGVyLnByb3RvdHlwZS5vbmNlID0gZnVuY3Rpb24gb25jZSh0eXBlLCBsaXN0ZW5lcikge1xuICBjaGVja0xpc3RlbmVyKGxpc3RlbmVyKTtcbiAgdGhpcy5vbih0eXBlLCBfb25jZVdyYXAodGhpcywgdHlwZSwgbGlzdGVuZXIpKTtcbiAgcmV0dXJuIHRoaXM7XG59O1xuXG5FdmVudEVtaXR0ZXIucHJvdG90eXBlLnByZXBlbmRPbmNlTGlzdGVuZXIgPVxuICAgIGZ1bmN0aW9uIHByZXBlbmRPbmNlTGlzdGVuZXIodHlwZSwgbGlzdGVuZXIpIHtcbiAgICAgIGNoZWNrTGlzdGVuZXIobGlzdGVuZXIpO1xuICAgICAgdGhpcy5wcmVwZW5kTGlzdGVuZXIodHlwZSwgX29uY2VXcmFwKHRoaXMsIHR5cGUsIGxpc3RlbmVyKSk7XG4gICAgICByZXR1cm4gdGhpcztcbiAgICB9O1xuXG4vLyBFbWl0cyBhICdyZW1vdmVMaXN0ZW5lcicgZXZlbnQgaWYgYW5kIG9ubHkgaWYgdGhlIGxpc3RlbmVyIHdhcyByZW1vdmVkLlxuRXZlbnRFbWl0dGVyLnByb3RvdHlwZS5yZW1vdmVMaXN0ZW5lciA9XG4gICAgZnVuY3Rpb24gcmVtb3ZlTGlzdGVuZXIodHlwZSwgbGlzdGVuZXIpIHtcbiAgICAgIHZhciBsaXN0LCBldmVudHMsIHBvc2l0aW9uLCBpLCBvcmlnaW5hbExpc3RlbmVyO1xuXG4gICAgICBjaGVja0xpc3RlbmVyKGxpc3RlbmVyKTtcblxuICAgICAgZXZlbnRzID0gdGhpcy5fZXZlbnRzO1xuICAgICAgaWYgKGV2ZW50cyA9PT0gdW5kZWZpbmVkKVxuICAgICAgICByZXR1cm4gdGhpcztcblxuICAgICAgbGlzdCA9IGV2ZW50c1t0eXBlXTtcbiAgICAgIGlmIChsaXN0ID09PSB1bmRlZmluZWQpXG4gICAgICAgIHJldHVybiB0aGlzO1xuXG4gICAgICBpZiAobGlzdCA9PT0gbGlzdGVuZXIgfHwgbGlzdC5saXN0ZW5lciA9PT0gbGlzdGVuZXIpIHtcbiAgICAgICAgaWYgKC0tdGhpcy5fZXZlbnRzQ291bnQgPT09IDApXG4gICAgICAgICAgdGhpcy5fZXZlbnRzID0gT2JqZWN0LmNyZWF0ZShudWxsKTtcbiAgICAgICAgZWxzZSB7XG4gICAgICAgICAgZGVsZXRlIGV2ZW50c1t0eXBlXTtcbiAgICAgICAgICBpZiAoZXZlbnRzLnJlbW92ZUxpc3RlbmVyKVxuICAgICAgICAgICAgdGhpcy5lbWl0KCdyZW1vdmVMaXN0ZW5lcicsIHR5cGUsIGxpc3QubGlzdGVuZXIgfHwgbGlzdGVuZXIpO1xuICAgICAgICB9XG4gICAgICB9IGVsc2UgaWYgKHR5cGVvZiBsaXN0ICE9PSAnZnVuY3Rpb24nKSB7XG4gICAgICAgIHBvc2l0aW9uID0gLTE7XG5cbiAgICAgICAgZm9yIChpID0gbGlzdC5sZW5ndGggLSAxOyBpID49IDA7IGktLSkge1xuICAgICAgICAgIGlmIChsaXN0W2ldID09PSBsaXN0ZW5lciB8fCBsaXN0W2ldLmxpc3RlbmVyID09PSBsaXN0ZW5lcikge1xuICAgICAgICAgICAgb3JpZ2luYWxMaXN0ZW5lciA9IGxpc3RbaV0ubGlzdGVuZXI7XG4gICAgICAgICAgICBwb3NpdGlvbiA9IGk7XG4gICAgICAgICAgICBicmVhaztcbiAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICBpZiAocG9zaXRpb24gPCAwKVxuICAgICAgICAgIHJldHVybiB0aGlzO1xuXG4gICAgICAgIGlmIChwb3NpdGlvbiA9PT0gMClcbiAgICAgICAgICBsaXN0LnNoaWZ0KCk7XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgIHNwbGljZU9uZShsaXN0LCBwb3NpdGlvbik7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAobGlzdC5sZW5ndGggPT09IDEpXG4gICAgICAgICAgZXZlbnRzW3R5cGVdID0gbGlzdFswXTtcblxuICAgICAgICBpZiAoZXZlbnRzLnJlbW92ZUxpc3RlbmVyICE9PSB1bmRlZmluZWQpXG4gICAgICAgICAgdGhpcy5lbWl0KCdyZW1vdmVMaXN0ZW5lcicsIHR5cGUsIG9yaWdpbmFsTGlzdGVuZXIgfHwgbGlzdGVuZXIpO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gdGhpcztcbiAgICB9O1xuXG5FdmVudEVtaXR0ZXIucHJvdG90eXBlLm9mZiA9IEV2ZW50RW1pdHRlci5wcm90b3R5cGUucmVtb3ZlTGlzdGVuZXI7XG5cbkV2ZW50RW1pdHRlci5wcm90b3R5cGUucmVtb3ZlQWxsTGlzdGVuZXJzID1cbiAgICBmdW5jdGlvbiByZW1vdmVBbGxMaXN0ZW5lcnModHlwZSkge1xuICAgICAgdmFyIGxpc3RlbmVycywgZXZlbnRzLCBpO1xuXG4gICAgICBldmVudHMgPSB0aGlzLl9ldmVudHM7XG4gICAgICBpZiAoZXZlbnRzID09PSB1bmRlZmluZWQpXG4gICAgICAgIHJldHVybiB0aGlzO1xuXG4gICAgICAvLyBub3QgbGlzdGVuaW5nIGZvciByZW1vdmVMaXN0ZW5lciwgbm8gbmVlZCB0byBlbWl0XG4gICAgICBpZiAoZXZlbnRzLnJlbW92ZUxpc3RlbmVyID09PSB1bmRlZmluZWQpIHtcbiAgICAgICAgaWYgKGFyZ3VtZW50cy5sZW5ndGggPT09IDApIHtcbiAgICAgICAgICB0aGlzLl9ldmVudHMgPSBPYmplY3QuY3JlYXRlKG51bGwpO1xuICAgICAgICAgIHRoaXMuX2V2ZW50c0NvdW50ID0gMDtcbiAgICAgICAgfSBlbHNlIGlmIChldmVudHNbdHlwZV0gIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgIGlmICgtLXRoaXMuX2V2ZW50c0NvdW50ID09PSAwKVxuICAgICAgICAgICAgdGhpcy5fZXZlbnRzID0gT2JqZWN0LmNyZWF0ZShudWxsKTtcbiAgICAgICAgICBlbHNlXG4gICAgICAgICAgICBkZWxldGUgZXZlbnRzW3R5cGVdO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiB0aGlzO1xuICAgICAgfVxuXG4gICAgICAvLyBlbWl0IHJlbW92ZUxpc3RlbmVyIGZvciBhbGwgbGlzdGVuZXJzIG9uIGFsbCBldmVudHNcbiAgICAgIGlmIChhcmd1bWVudHMubGVuZ3RoID09PSAwKSB7XG4gICAgICAgIHZhciBrZXlzID0gT2JqZWN0LmtleXMoZXZlbnRzKTtcbiAgICAgICAgdmFyIGtleTtcbiAgICAgICAgZm9yIChpID0gMDsgaSA8IGtleXMubGVuZ3RoOyArK2kpIHtcbiAgICAgICAgICBrZXkgPSBrZXlzW2ldO1xuICAgICAgICAgIGlmIChrZXkgPT09ICdyZW1vdmVMaXN0ZW5lcicpIGNvbnRpbnVlO1xuICAgICAgICAgIHRoaXMucmVtb3ZlQWxsTGlzdGVuZXJzKGtleSk7XG4gICAgICAgIH1cbiAgICAgICAgdGhpcy5yZW1vdmVBbGxMaXN0ZW5lcnMoJ3JlbW92ZUxpc3RlbmVyJyk7XG4gICAgICAgIHRoaXMuX2V2ZW50cyA9IE9iamVjdC5jcmVhdGUobnVsbCk7XG4gICAgICAgIHRoaXMuX2V2ZW50c0NvdW50ID0gMDtcbiAgICAgICAgcmV0dXJuIHRoaXM7XG4gICAgICB9XG5cbiAgICAgIGxpc3RlbmVycyA9IGV2ZW50c1t0eXBlXTtcblxuICAgICAgaWYgKHR5cGVvZiBsaXN0ZW5lcnMgPT09ICdmdW5jdGlvbicpIHtcbiAgICAgICAgdGhpcy5yZW1vdmVMaXN0ZW5lcih0eXBlLCBsaXN0ZW5lcnMpO1xuICAgICAgfSBlbHNlIGlmIChsaXN0ZW5lcnMgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAvLyBMSUZPIG9yZGVyXG4gICAgICAgIGZvciAoaSA9IGxpc3RlbmVycy5sZW5ndGggLSAxOyBpID49IDA7IGktLSkge1xuICAgICAgICAgIHRoaXMucmVtb3ZlTGlzdGVuZXIodHlwZSwgbGlzdGVuZXJzW2ldKTtcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICByZXR1cm4gdGhpcztcbiAgICB9O1xuXG5mdW5jdGlvbiBfbGlzdGVuZXJzKHRhcmdldCwgdHlwZSwgdW53cmFwKSB7XG4gIHZhciBldmVudHMgPSB0YXJnZXQuX2V2ZW50cztcblxuICBpZiAoZXZlbnRzID09PSB1bmRlZmluZWQpXG4gICAgcmV0dXJuIFtdO1xuXG4gIHZhciBldmxpc3RlbmVyID0gZXZlbnRzW3R5cGVdO1xuICBpZiAoZXZsaXN0ZW5lciA9PT0gdW5kZWZpbmVkKVxuICAgIHJldHVybiBbXTtcblxuICBpZiAodHlwZW9mIGV2bGlzdGVuZXIgPT09ICdmdW5jdGlvbicpXG4gICAgcmV0dXJuIHVud3JhcCA/IFtldmxpc3RlbmVyLmxpc3RlbmVyIHx8IGV2bGlzdGVuZXJdIDogW2V2bGlzdGVuZXJdO1xuXG4gIHJldHVybiB1bndyYXAgP1xuICAgIHVud3JhcExpc3RlbmVycyhldmxpc3RlbmVyKSA6IGFycmF5Q2xvbmUoZXZsaXN0ZW5lciwgZXZsaXN0ZW5lci5sZW5ndGgpO1xufVxuXG5FdmVudEVtaXR0ZXIucHJvdG90eXBlLmxpc3RlbmVycyA9IGZ1bmN0aW9uIGxpc3RlbmVycyh0eXBlKSB7XG4gIHJldHVybiBfbGlzdGVuZXJzKHRoaXMsIHR5cGUsIHRydWUpO1xufTtcblxuRXZlbnRFbWl0dGVyLnByb3RvdHlwZS5yYXdMaXN0ZW5lcnMgPSBmdW5jdGlvbiByYXdMaXN0ZW5lcnModHlwZSkge1xuICByZXR1cm4gX2xpc3RlbmVycyh0aGlzLCB0eXBlLCBmYWxzZSk7XG59O1xuXG5FdmVudEVtaXR0ZXIubGlzdGVuZXJDb3VudCA9IGZ1bmN0aW9uKGVtaXR0ZXIsIHR5cGUpIHtcbiAgaWYgKHR5cGVvZiBlbWl0dGVyLmxpc3RlbmVyQ291bnQgPT09ICdmdW5jdGlvbicpIHtcbiAgICByZXR1cm4gZW1pdHRlci5saXN0ZW5lckNvdW50KHR5cGUpO1xuICB9IGVsc2Uge1xuICAgIHJldHVybiBsaXN0ZW5lckNvdW50LmNhbGwoZW1pdHRlciwgdHlwZSk7XG4gIH1cbn07XG5cbkV2ZW50RW1pdHRlci5wcm90b3R5cGUubGlzdGVuZXJDb3VudCA9IGxpc3RlbmVyQ291bnQ7XG5mdW5jdGlvbiBsaXN0ZW5lckNvdW50KHR5cGUpIHtcbiAgdmFyIGV2ZW50cyA9IHRoaXMuX2V2ZW50cztcblxuICBpZiAoZXZlbnRzICE9PSB1bmRlZmluZWQpIHtcbiAgICB2YXIgZXZsaXN0ZW5lciA9IGV2ZW50c1t0eXBlXTtcblxuICAgIGlmICh0eXBlb2YgZXZsaXN0ZW5lciA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgcmV0dXJuIDE7XG4gICAgfSBlbHNlIGlmIChldmxpc3RlbmVyICE9PSB1bmRlZmluZWQpIHtcbiAgICAgIHJldHVybiBldmxpc3RlbmVyLmxlbmd0aDtcbiAgICB9XG4gIH1cblxuICByZXR1cm4gMDtcbn1cblxuRXZlbnRFbWl0dGVyLnByb3RvdHlwZS5ldmVudE5hbWVzID0gZnVuY3Rpb24gZXZlbnROYW1lcygpIHtcbiAgcmV0dXJuIHRoaXMuX2V2ZW50c0NvdW50ID4gMCA/IFJlZmxlY3RPd25LZXlzKHRoaXMuX2V2ZW50cykgOiBbXTtcbn07XG5cbmZ1bmN0aW9uIGFycmF5Q2xvbmUoYXJyLCBuKSB7XG4gIHZhciBjb3B5ID0gbmV3IEFycmF5KG4pO1xuICBmb3IgKHZhciBpID0gMDsgaSA8IG47ICsraSlcbiAgICBjb3B5W2ldID0gYXJyW2ldO1xuICByZXR1cm4gY29weTtcbn1cblxuZnVuY3Rpb24gc3BsaWNlT25lKGxpc3QsIGluZGV4KSB7XG4gIGZvciAoOyBpbmRleCArIDEgPCBsaXN0Lmxlbmd0aDsgaW5kZXgrKylcbiAgICBsaXN0W2luZGV4XSA9IGxpc3RbaW5kZXggKyAxXTtcbiAgbGlzdC5wb3AoKTtcbn1cblxuZnVuY3Rpb24gdW53cmFwTGlzdGVuZXJzKGFycikge1xuICB2YXIgcmV0ID0gbmV3IEFycmF5KGFyci5sZW5ndGgpO1xuICBmb3IgKHZhciBpID0gMDsgaSA8IHJldC5sZW5ndGg7ICsraSkge1xuICAgIHJldFtpXSA9IGFycltpXS5saXN0ZW5lciB8fCBhcnJbaV07XG4gIH1cbiAgcmV0dXJuIHJldDtcbn1cbiIsIi8qKiogSU1QT1JUUyBGUk9NIGltcG9ydHMtbG9hZGVyICoqKi9cblxuKGZ1bmN0aW9uKGRlZmluZSwgZXhwb3J0cykge1xuLyohXG4gKiBCb290c3RyYXAgQ29sb3JwaWNrZXIgLSBCb290c3RyYXAgQ29sb3JwaWNrZXIgaXMgYSBtb2R1bGFyIGNvbG9yIHBpY2tlciBwbHVnaW4gZm9yIEJvb3RzdHJhcCA0LlxuICogQHBhY2thZ2UgYm9vdHN0cmFwLWNvbG9ycGlja2VyXG4gKiBAdmVyc2lvbiB2My4yLjBcbiAqIEBsaWNlbnNlIE1JVFxuICogQGxpbmsgaHR0cHM6Ly9pdHNqYXZpLmNvbS9ib290c3RyYXAtY29sb3JwaWNrZXIvXG4gKiBAbGluayBodHRwczovL2dpdGh1Yi5jb20vaXRzamF2aS9ib290c3RyYXAtY29sb3JwaWNrZXIuZ2l0XG4gKi9cbihmdW5jdGlvbiB3ZWJwYWNrVW5pdmVyc2FsTW9kdWxlRGVmaW5pdGlvbihyb290LCBmYWN0b3J5KSB7XG5cdGlmKHR5cGVvZiBleHBvcnRzID09PSAnb2JqZWN0JyAmJiB0eXBlb2YgbW9kdWxlID09PSAnb2JqZWN0Jylcblx0XHRtb2R1bGUuZXhwb3J0cyA9IGZhY3RvcnkocmVxdWlyZShcImpxdWVyeVwiKSk7XG5cdGVsc2UgaWYodHlwZW9mIGRlZmluZSA9PT0gJ2Z1bmN0aW9uJyAmJiBkZWZpbmUuYW1kKVxuXHRcdGRlZmluZShcImJvb3RzdHJhcC1jb2xvcnBpY2tlclwiLCBbXCJqcXVlcnlcIl0sIGZhY3RvcnkpO1xuXHRlbHNlIGlmKHR5cGVvZiBleHBvcnRzID09PSAnb2JqZWN0Jylcblx0XHRleHBvcnRzW1wiYm9vdHN0cmFwLWNvbG9ycGlja2VyXCJdID0gZmFjdG9yeShyZXF1aXJlKFwianF1ZXJ5XCIpKTtcblx0ZWxzZVxuXHRcdHJvb3RbXCJib290c3RyYXAtY29sb3JwaWNrZXJcIl0gPSBmYWN0b3J5KHJvb3RbXCJqUXVlcnlcIl0pO1xufSkod2luZG93LCBmdW5jdGlvbihfX1dFQlBBQ0tfRVhURVJOQUxfTU9EVUxFX18wX18pIHtcbnJldHVybiAvKioqKioqLyAoZnVuY3Rpb24obW9kdWxlcykgeyAvLyB3ZWJwYWNrQm9vdHN0cmFwXG4vKioqKioqLyBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbi8qKioqKiovIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSB7fTtcbi8qKioqKiovXG4vKioqKioqLyBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4vKioqKioqLyBcdGZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcbi8qKioqKiovXG4vKioqKioqLyBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4vKioqKioqLyBcdFx0aWYoaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0pIHtcbi8qKioqKiovIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuLyoqKioqKi8gXHRcdH1cbi8qKioqKiovIFx0XHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuLyoqKioqKi8gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbi8qKioqKiovIFx0XHRcdGk6IG1vZHVsZUlkLFxuLyoqKioqKi8gXHRcdFx0bDogZmFsc2UsXG4vKioqKioqLyBcdFx0XHRleHBvcnRzOiB7fVxuLyoqKioqKi8gXHRcdH07XG4vKioqKioqL1xuLyoqKioqKi8gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuLyoqKioqKi8gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuLyoqKioqKi9cbi8qKioqKiovIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4vKioqKioqLyBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuLyoqKioqKi9cbi8qKioqKiovIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuLyoqKioqKi8gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbi8qKioqKiovIFx0fVxuLyoqKioqKi9cbi8qKioqKiovXG4vKioqKioqLyBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4vKioqKioqLyBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG4vKioqKioqL1xuLyoqKioqKi8gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuLyoqKioqKi8gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuLyoqKioqKi9cbi8qKioqKiovIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4vKioqKioqLyBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuLyoqKioqKi8gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbi8qKioqKiovIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7IGVudW1lcmFibGU6IHRydWUsIGdldDogZ2V0dGVyIH0pO1xuLyoqKioqKi8gXHRcdH1cbi8qKioqKiovIFx0fTtcbi8qKioqKiovXG4vKioqKioqLyBcdC8vIGRlZmluZSBfX2VzTW9kdWxlIG9uIGV4cG9ydHNcbi8qKioqKiovIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yID0gZnVuY3Rpb24oZXhwb3J0cykge1xuLyoqKioqKi8gXHRcdGlmKHR5cGVvZiBTeW1ib2wgIT09ICd1bmRlZmluZWQnICYmIFN5bWJvbC50b1N0cmluZ1RhZykge1xuLyoqKioqKi8gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFN5bWJvbC50b1N0cmluZ1RhZywgeyB2YWx1ZTogJ01vZHVsZScgfSk7XG4vKioqKioqLyBcdFx0fVxuLyoqKioqKi8gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnX19lc01vZHVsZScsIHsgdmFsdWU6IHRydWUgfSk7XG4vKioqKioqLyBcdH07XG4vKioqKioqL1xuLyoqKioqKi8gXHQvLyBjcmVhdGUgYSBmYWtlIG5hbWVzcGFjZSBvYmplY3Rcbi8qKioqKiovIFx0Ly8gbW9kZSAmIDE6IHZhbHVlIGlzIGEgbW9kdWxlIGlkLCByZXF1aXJlIGl0XG4vKioqKioqLyBcdC8vIG1vZGUgJiAyOiBtZXJnZSBhbGwgcHJvcGVydGllcyBvZiB2YWx1ZSBpbnRvIHRoZSBuc1xuLyoqKioqKi8gXHQvLyBtb2RlICYgNDogcmV0dXJuIHZhbHVlIHdoZW4gYWxyZWFkeSBucyBvYmplY3Rcbi8qKioqKiovIFx0Ly8gbW9kZSAmIDh8MTogYmVoYXZlIGxpa2UgcmVxdWlyZVxuLyoqKioqKi8gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnQgPSBmdW5jdGlvbih2YWx1ZSwgbW9kZSkge1xuLyoqKioqKi8gXHRcdGlmKG1vZGUgJiAxKSB2YWx1ZSA9IF9fd2VicGFja19yZXF1aXJlX18odmFsdWUpO1xuLyoqKioqKi8gXHRcdGlmKG1vZGUgJiA4KSByZXR1cm4gdmFsdWU7XG4vKioqKioqLyBcdFx0aWYoKG1vZGUgJiA0KSAmJiB0eXBlb2YgdmFsdWUgPT09ICdvYmplY3QnICYmIHZhbHVlICYmIHZhbHVlLl9fZXNNb2R1bGUpIHJldHVybiB2YWx1ZTtcbi8qKioqKiovIFx0XHR2YXIgbnMgPSBPYmplY3QuY3JlYXRlKG51bGwpO1xuLyoqKioqKi8gXHRcdF9fd2VicGFja19yZXF1aXJlX18ucihucyk7XG4vKioqKioqLyBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KG5zLCAnZGVmYXVsdCcsIHsgZW51bWVyYWJsZTogdHJ1ZSwgdmFsdWU6IHZhbHVlIH0pO1xuLyoqKioqKi8gXHRcdGlmKG1vZGUgJiAyICYmIHR5cGVvZiB2YWx1ZSAhPSAnc3RyaW5nJykgZm9yKHZhciBrZXkgaW4gdmFsdWUpIF9fd2VicGFja19yZXF1aXJlX18uZChucywga2V5LCBmdW5jdGlvbihrZXkpIHsgcmV0dXJuIHZhbHVlW2tleV07IH0uYmluZChudWxsLCBrZXkpKTtcbi8qKioqKiovIFx0XHRyZXR1cm4gbnM7XG4vKioqKioqLyBcdH07XG4vKioqKioqL1xuLyoqKioqKi8gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuLyoqKioqKi8gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbi8qKioqKiovIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbi8qKioqKiovIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4vKioqKioqLyBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuLyoqKioqKi8gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbi8qKioqKiovIFx0XHRyZXR1cm4gZ2V0dGVyO1xuLyoqKioqKi8gXHR9O1xuLyoqKioqKi9cbi8qKioqKiovIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4vKioqKioqLyBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcbi8qKioqKiovXG4vKioqKioqLyBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4vKioqKioqLyBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG4vKioqKioqL1xuLyoqKioqKi9cbi8qKioqKiovIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4vKioqKioqLyBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IDcpO1xuLyoqKioqKi8gfSlcbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG4vKioqKioqLyAoW1xuLyogMCAqL1xuLyoqKi8gKGZ1bmN0aW9uKG1vZHVsZSwgZXhwb3J0cykge1xuXG5tb2R1bGUuZXhwb3J0cyA9IF9fV0VCUEFDS19FWFRFUk5BTF9NT0RVTEVfXzBfXztcblxuLyoqKi8gfSksXG4vKiAxICovXG4vKioqLyAoZnVuY3Rpb24obW9kdWxlLCBleHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKSB7XG5cblwidXNlIHN0cmljdFwiO1xuXG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfY3JlYXRlQ2xhc3MgPSBmdW5jdGlvbiAoKSB7IGZ1bmN0aW9uIGRlZmluZVByb3BlcnRpZXModGFyZ2V0LCBwcm9wcykgeyBmb3IgKHZhciBpID0gMDsgaSA8IHByb3BzLmxlbmd0aDsgaSsrKSB7IHZhciBkZXNjcmlwdG9yID0gcHJvcHNbaV07IGRlc2NyaXB0b3IuZW51bWVyYWJsZSA9IGRlc2NyaXB0b3IuZW51bWVyYWJsZSB8fCBmYWxzZTsgZGVzY3JpcHRvci5jb25maWd1cmFibGUgPSB0cnVlOyBpZiAoXCJ2YWx1ZVwiIGluIGRlc2NyaXB0b3IpIGRlc2NyaXB0b3Iud3JpdGFibGUgPSB0cnVlOyBPYmplY3QuZGVmaW5lUHJvcGVydHkodGFyZ2V0LCBkZXNjcmlwdG9yLmtleSwgZGVzY3JpcHRvcik7IH0gfSByZXR1cm4gZnVuY3Rpb24gKENvbnN0cnVjdG9yLCBwcm90b1Byb3BzLCBzdGF0aWNQcm9wcykgeyBpZiAocHJvdG9Qcm9wcykgZGVmaW5lUHJvcGVydGllcyhDb25zdHJ1Y3Rvci5wcm90b3R5cGUsIHByb3RvUHJvcHMpOyBpZiAoc3RhdGljUHJvcHMpIGRlZmluZVByb3BlcnRpZXMoQ29uc3RydWN0b3IsIHN0YXRpY1Byb3BzKTsgcmV0dXJuIENvbnN0cnVjdG9yOyB9OyB9KCk7XG5cbnZhciBfanF1ZXJ5ID0gX193ZWJwYWNrX3JlcXVpcmVfXygwKTtcblxudmFyIF9qcXVlcnkyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfanF1ZXJ5KTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxuZnVuY3Rpb24gX2NsYXNzQ2FsbENoZWNrKGluc3RhbmNlLCBDb25zdHJ1Y3RvcikgeyBpZiAoIShpbnN0YW5jZSBpbnN0YW5jZW9mIENvbnN0cnVjdG9yKSkgeyB0aHJvdyBuZXcgVHlwZUVycm9yKFwiQ2Fubm90IGNhbGwgYSBjbGFzcyBhcyBhIGZ1bmN0aW9uXCIpOyB9IH1cblxuLyoqXG4gKiBDb2xvcnBpY2tlciBleHRlbnNpb24gY2xhc3MuXG4gKi9cbnZhciBFeHRlbnNpb24gPSBmdW5jdGlvbiAoKSB7XG4gIC8qKlxuICAgKiBAcGFyYW0ge0NvbG9ycGlja2VyfSBjb2xvcnBpY2tlclxuICAgKiBAcGFyYW0ge09iamVjdH0gb3B0aW9uc1xuICAgKi9cbiAgZnVuY3Rpb24gRXh0ZW5zaW9uKGNvbG9ycGlja2VyKSB7XG4gICAgdmFyIG9wdGlvbnMgPSBhcmd1bWVudHMubGVuZ3RoID4gMSAmJiBhcmd1bWVudHNbMV0gIT09IHVuZGVmaW5lZCA/IGFyZ3VtZW50c1sxXSA6IHt9O1xuXG4gICAgX2NsYXNzQ2FsbENoZWNrKHRoaXMsIEV4dGVuc2lvbik7XG5cbiAgICAvKipcbiAgICAgKiBUaGUgY29sb3JwaWNrZXIgaW5zdGFuY2VcbiAgICAgKiBAdHlwZSB7Q29sb3JwaWNrZXJ9XG4gICAgICovXG4gICAgdGhpcy5jb2xvcnBpY2tlciA9IGNvbG9ycGlja2VyO1xuICAgIC8qKlxuICAgICAqIEV4dGVuc2lvbiBvcHRpb25zXG4gICAgICpcbiAgICAgKiBAdHlwZSB7T2JqZWN0fVxuICAgICAqL1xuICAgIHRoaXMub3B0aW9ucyA9IG9wdGlvbnM7XG5cbiAgICBpZiAoISh0aGlzLmNvbG9ycGlja2VyLmVsZW1lbnQgJiYgdGhpcy5jb2xvcnBpY2tlci5lbGVtZW50Lmxlbmd0aCkpIHtcbiAgICAgIHRocm93IG5ldyBFcnJvcignRXh0ZW5zaW9uOiB0aGlzLmNvbG9ycGlja2VyLmVsZW1lbnQgaXMgbm90IHZhbGlkJyk7XG4gICAgfVxuXG4gICAgdGhpcy5jb2xvcnBpY2tlci5lbGVtZW50Lm9uKCdjb2xvcnBpY2tlckNyZWF0ZS5jb2xvcnBpY2tlci1leHQnLCBfanF1ZXJ5Mi5kZWZhdWx0LnByb3h5KHRoaXMub25DcmVhdGUsIHRoaXMpKTtcbiAgICB0aGlzLmNvbG9ycGlja2VyLmVsZW1lbnQub24oJ2NvbG9ycGlja2VyRGVzdHJveS5jb2xvcnBpY2tlci1leHQnLCBfanF1ZXJ5Mi5kZWZhdWx0LnByb3h5KHRoaXMub25EZXN0cm95LCB0aGlzKSk7XG4gICAgdGhpcy5jb2xvcnBpY2tlci5lbGVtZW50Lm9uKCdjb2xvcnBpY2tlclVwZGF0ZS5jb2xvcnBpY2tlci1leHQnLCBfanF1ZXJ5Mi5kZWZhdWx0LnByb3h5KHRoaXMub25VcGRhdGUsIHRoaXMpKTtcbiAgICB0aGlzLmNvbG9ycGlja2VyLmVsZW1lbnQub24oJ2NvbG9ycGlja2VyQ2hhbmdlLmNvbG9ycGlja2VyLWV4dCcsIF9qcXVlcnkyLmRlZmF1bHQucHJveHkodGhpcy5vbkNoYW5nZSwgdGhpcykpO1xuICAgIHRoaXMuY29sb3JwaWNrZXIuZWxlbWVudC5vbignY29sb3JwaWNrZXJJbnZhbGlkLmNvbG9ycGlja2VyLWV4dCcsIF9qcXVlcnkyLmRlZmF1bHQucHJveHkodGhpcy5vbkludmFsaWQsIHRoaXMpKTtcbiAgICB0aGlzLmNvbG9ycGlja2VyLmVsZW1lbnQub24oJ2NvbG9ycGlja2VyU2hvdy5jb2xvcnBpY2tlci1leHQnLCBfanF1ZXJ5Mi5kZWZhdWx0LnByb3h5KHRoaXMub25TaG93LCB0aGlzKSk7XG4gICAgdGhpcy5jb2xvcnBpY2tlci5lbGVtZW50Lm9uKCdjb2xvcnBpY2tlckhpZGUuY29sb3JwaWNrZXItZXh0JywgX2pxdWVyeTIuZGVmYXVsdC5wcm94eSh0aGlzLm9uSGlkZSwgdGhpcykpO1xuICAgIHRoaXMuY29sb3JwaWNrZXIuZWxlbWVudC5vbignY29sb3JwaWNrZXJFbmFibGUuY29sb3JwaWNrZXItZXh0JywgX2pxdWVyeTIuZGVmYXVsdC5wcm94eSh0aGlzLm9uRW5hYmxlLCB0aGlzKSk7XG4gICAgdGhpcy5jb2xvcnBpY2tlci5lbGVtZW50Lm9uKCdjb2xvcnBpY2tlckRpc2FibGUuY29sb3JwaWNrZXItZXh0JywgX2pxdWVyeTIuZGVmYXVsdC5wcm94eSh0aGlzLm9uRGlzYWJsZSwgdGhpcykpO1xuICB9XG5cbiAgLyoqXG4gICAqIEZ1bmN0aW9uIGNhbGxlZCBldmVyeSB0aW1lIGEgbmV3IGNvbG9yIG5lZWRzIHRvIGJlIGNyZWF0ZWQuXG4gICAqIFJldHVybiBmYWxzZSB0byBza2lwIHRoaXMgcmVzb2x2ZXIgYW5kIGNvbnRpbnVlIHdpdGggb3RoZXIgZXh0ZW5zaW9ucycgb25lc1xuICAgKiBvciByZXR1cm4gYW55dGhpbmcgZWxzZSB0byBjb25zaWRlciB0aGUgY29sb3IgcmVzb2x2ZWQuXG4gICAqXG4gICAqIEBwYXJhbSB7Q29sb3JJdGVtfFN0cmluZ3wqfSBjb2xvclxuICAgKiBAcGFyYW0ge2Jvb2xlYW59IHJlYWxDb2xvciBpZiB0cnVlLCB0aGUgY29sb3Igc2hvdWxkIHJlc29sdmUgaW50byBhIHJlYWwgKG5vdCBuYW1lZCkgY29sb3IgY29kZVxuICAgKiBAcmV0dXJuIHtDb2xvckl0ZW18U3RyaW5nfCp9XG4gICAqL1xuXG5cbiAgX2NyZWF0ZUNsYXNzKEV4dGVuc2lvbiwgW3tcbiAgICBrZXk6ICdyZXNvbHZlQ29sb3InLFxuICAgIHZhbHVlOiBmdW5jdGlvbiByZXNvbHZlQ29sb3IoY29sb3IpIHtcbiAgICAgIHZhciByZWFsQ29sb3IgPSBhcmd1bWVudHMubGVuZ3RoID4gMSAmJiBhcmd1bWVudHNbMV0gIT09IHVuZGVmaW5lZCA/IGFyZ3VtZW50c1sxXSA6IHRydWU7XG5cbiAgICAgIHJldHVybiBmYWxzZTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBNZXRob2QgY2FsbGVkIGFmdGVyIHRoZSBjb2xvcnBpY2tlciBpcyBjcmVhdGVkXG4gICAgICpcbiAgICAgKiBAbGlzdGVucyBDb2xvcnBpY2tlciNjb2xvcnBpY2tlckNyZWF0ZVxuICAgICAqIEBwYXJhbSB7RXZlbnR9IGV2ZW50XG4gICAgICovXG5cbiAgfSwge1xuICAgIGtleTogJ29uQ3JlYXRlJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gb25DcmVhdGUoZXZlbnQpIHt9XG4gICAgLy8gdG8gYmUgZXh0ZW5kZWRcblxuXG4gICAgLyoqXG4gICAgICogTWV0aG9kIGNhbGxlZCBhZnRlciB0aGUgY29sb3JwaWNrZXIgaXMgZGVzdHJveWVkXG4gICAgICpcbiAgICAgKiBAbGlzdGVucyBDb2xvcnBpY2tlciNjb2xvcnBpY2tlckRlc3Ryb3lcbiAgICAgKiBAcGFyYW0ge0V2ZW50fSBldmVudFxuICAgICAqL1xuXG4gIH0sIHtcbiAgICBrZXk6ICdvbkRlc3Ryb3knLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBvbkRlc3Ryb3koZXZlbnQpIHtcbiAgICAgIHRoaXMuY29sb3JwaWNrZXIuZWxlbWVudC5vZmYoJy5jb2xvcnBpY2tlci1leHQnKTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBNZXRob2QgY2FsbGVkIGFmdGVyIHRoZSBjb2xvcnBpY2tlciBpcyB1cGRhdGVkXG4gICAgICpcbiAgICAgKiBAbGlzdGVucyBDb2xvcnBpY2tlciNjb2xvcnBpY2tlclVwZGF0ZVxuICAgICAqIEBwYXJhbSB7RXZlbnR9IGV2ZW50XG4gICAgICovXG5cbiAgfSwge1xuICAgIGtleTogJ29uVXBkYXRlJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gb25VcGRhdGUoZXZlbnQpIHt9XG4gICAgLy8gdG8gYmUgZXh0ZW5kZWRcblxuXG4gICAgLyoqXG4gICAgICogTWV0aG9kIGNhbGxlZCBhZnRlciB0aGUgY29sb3JwaWNrZXIgY29sb3IgaXMgY2hhbmdlZFxuICAgICAqXG4gICAgICogQGxpc3RlbnMgQ29sb3JwaWNrZXIjY29sb3JwaWNrZXJDaGFuZ2VcbiAgICAgKiBAcGFyYW0ge0V2ZW50fSBldmVudFxuICAgICAqL1xuXG4gIH0sIHtcbiAgICBrZXk6ICdvbkNoYW5nZScsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIG9uQ2hhbmdlKGV2ZW50KSB7fVxuICAgIC8vIHRvIGJlIGV4dGVuZGVkXG5cblxuICAgIC8qKlxuICAgICAqIE1ldGhvZCBjYWxsZWQgd2hlbiB0aGUgY29sb3JwaWNrZXIgY29sb3IgaXMgaW52YWxpZFxuICAgICAqXG4gICAgICogQGxpc3RlbnMgQ29sb3JwaWNrZXIjY29sb3JwaWNrZXJJbnZhbGlkXG4gICAgICogQHBhcmFtIHtFdmVudH0gZXZlbnRcbiAgICAgKi9cblxuICB9LCB7XG4gICAga2V5OiAnb25JbnZhbGlkJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gb25JbnZhbGlkKGV2ZW50KSB7fVxuICAgIC8vIHRvIGJlIGV4dGVuZGVkXG5cblxuICAgIC8qKlxuICAgICAqIE1ldGhvZCBjYWxsZWQgYWZ0ZXIgdGhlIGNvbG9ycGlja2VyIGlzIGhpZGRlblxuICAgICAqXG4gICAgICogQGxpc3RlbnMgQ29sb3JwaWNrZXIjY29sb3JwaWNrZXJIaWRlXG4gICAgICogQHBhcmFtIHtFdmVudH0gZXZlbnRcbiAgICAgKi9cblxuICB9LCB7XG4gICAga2V5OiAnb25IaWRlJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gb25IaWRlKGV2ZW50KSB7fVxuICAgIC8vIHRvIGJlIGV4dGVuZGVkXG5cblxuICAgIC8qKlxuICAgICAqIE1ldGhvZCBjYWxsZWQgYWZ0ZXIgdGhlIGNvbG9ycGlja2VyIGlzIHNob3duXG4gICAgICpcbiAgICAgKiBAbGlzdGVucyBDb2xvcnBpY2tlciNjb2xvcnBpY2tlclNob3dcbiAgICAgKiBAcGFyYW0ge0V2ZW50fSBldmVudFxuICAgICAqL1xuXG4gIH0sIHtcbiAgICBrZXk6ICdvblNob3cnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBvblNob3coZXZlbnQpIHt9XG4gICAgLy8gdG8gYmUgZXh0ZW5kZWRcblxuXG4gICAgLyoqXG4gICAgICogTWV0aG9kIGNhbGxlZCBhZnRlciB0aGUgY29sb3JwaWNrZXIgaXMgZGlzYWJsZWRcbiAgICAgKlxuICAgICAqIEBsaXN0ZW5zIENvbG9ycGlja2VyI2NvbG9ycGlja2VyRGlzYWJsZVxuICAgICAqIEBwYXJhbSB7RXZlbnR9IGV2ZW50XG4gICAgICovXG5cbiAgfSwge1xuICAgIGtleTogJ29uRGlzYWJsZScsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIG9uRGlzYWJsZShldmVudCkge31cbiAgICAvLyB0byBiZSBleHRlbmRlZFxuXG5cbiAgICAvKipcbiAgICAgKiBNZXRob2QgY2FsbGVkIGFmdGVyIHRoZSBjb2xvcnBpY2tlciBpcyBlbmFibGVkXG4gICAgICpcbiAgICAgKiBAbGlzdGVucyBDb2xvcnBpY2tlciNjb2xvcnBpY2tlckVuYWJsZVxuICAgICAqIEBwYXJhbSB7RXZlbnR9IGV2ZW50XG4gICAgICovXG5cbiAgfSwge1xuICAgIGtleTogJ29uRW5hYmxlJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gb25FbmFibGUoZXZlbnQpIHtcbiAgICAgIC8vIHRvIGJlIGV4dGVuZGVkXG4gICAgfVxuICB9XSk7XG5cbiAgcmV0dXJuIEV4dGVuc2lvbjtcbn0oKTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gRXh0ZW5zaW9uO1xubW9kdWxlLmV4cG9ydHMgPSBleHBvcnRzLmRlZmF1bHQ7XG5cbi8qKiovIH0pLFxuLyogMiAqL1xuLyoqKi8gKGZ1bmN0aW9uKG1vZHVsZSwgZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXykge1xuXG5cInVzZSBzdHJpY3RcIjtcblxuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuZXhwb3J0cy5Db2xvckl0ZW0gPSBleHBvcnRzLkhTVkFDb2xvciA9IHVuZGVmaW5lZDtcblxudmFyIF9jcmVhdGVDbGFzcyA9IGZ1bmN0aW9uICgpIHsgZnVuY3Rpb24gZGVmaW5lUHJvcGVydGllcyh0YXJnZXQsIHByb3BzKSB7IGZvciAodmFyIGkgPSAwOyBpIDwgcHJvcHMubGVuZ3RoOyBpKyspIHsgdmFyIGRlc2NyaXB0b3IgPSBwcm9wc1tpXTsgZGVzY3JpcHRvci5lbnVtZXJhYmxlID0gZGVzY3JpcHRvci5lbnVtZXJhYmxlIHx8IGZhbHNlOyBkZXNjcmlwdG9yLmNvbmZpZ3VyYWJsZSA9IHRydWU7IGlmIChcInZhbHVlXCIgaW4gZGVzY3JpcHRvcikgZGVzY3JpcHRvci53cml0YWJsZSA9IHRydWU7IE9iamVjdC5kZWZpbmVQcm9wZXJ0eSh0YXJnZXQsIGRlc2NyaXB0b3Iua2V5LCBkZXNjcmlwdG9yKTsgfSB9IHJldHVybiBmdW5jdGlvbiAoQ29uc3RydWN0b3IsIHByb3RvUHJvcHMsIHN0YXRpY1Byb3BzKSB7IGlmIChwcm90b1Byb3BzKSBkZWZpbmVQcm9wZXJ0aWVzKENvbnN0cnVjdG9yLnByb3RvdHlwZSwgcHJvdG9Qcm9wcyk7IGlmIChzdGF0aWNQcm9wcykgZGVmaW5lUHJvcGVydGllcyhDb25zdHJ1Y3Rvciwgc3RhdGljUHJvcHMpOyByZXR1cm4gQ29uc3RydWN0b3I7IH07IH0oKTsgLyoqXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICogQ29sb3IgbWFuaXB1bGF0aW9uIGNsYXNzLCBzcGVjaWZpYyBmb3IgQm9vdHN0cmFwIENvbG9ycGlja2VyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICovXG5cblxudmFyIF9jb2xvciA9IF9fd2VicGFja19yZXF1aXJlX18oMTYpO1xuXG52YXIgX2NvbG9yMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NvbG9yKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxuZnVuY3Rpb24gX2NsYXNzQ2FsbENoZWNrKGluc3RhbmNlLCBDb25zdHJ1Y3RvcikgeyBpZiAoIShpbnN0YW5jZSBpbnN0YW5jZW9mIENvbnN0cnVjdG9yKSkgeyB0aHJvdyBuZXcgVHlwZUVycm9yKFwiQ2Fubm90IGNhbGwgYSBjbGFzcyBhcyBhIGZ1bmN0aW9uXCIpOyB9IH1cblxuLyoqXG4gKiBIU1ZBIGNvbG9yIGRhdGEgY2xhc3MsIGNvbnRhaW5pbmcgdGhlIGh1ZSwgc2F0dXJhdGlvbiwgdmFsdWUgYW5kIGFscGhhXG4gKiBpbmZvcm1hdGlvbi5cbiAqL1xudmFyIEhTVkFDb2xvciA9IGZ1bmN0aW9uICgpIHtcbiAgLyoqXG4gICAqIEBwYXJhbSB7bnVtYmVyfGludH0gaFxuICAgKiBAcGFyYW0ge251bWJlcnxpbnR9IHNcbiAgICogQHBhcmFtIHtudW1iZXJ8aW50fSB2XG4gICAqIEBwYXJhbSB7bnVtYmVyfGludH0gYVxuICAgKi9cbiAgZnVuY3Rpb24gSFNWQUNvbG9yKGgsIHMsIHYsIGEpIHtcbiAgICBfY2xhc3NDYWxsQ2hlY2sodGhpcywgSFNWQUNvbG9yKTtcblxuICAgIHRoaXMuaCA9IGlzTmFOKGgpID8gMCA6IGg7XG4gICAgdGhpcy5zID0gaXNOYU4ocykgPyAwIDogcztcbiAgICB0aGlzLnYgPSBpc05hTih2KSA/IDAgOiB2O1xuICAgIHRoaXMuYSA9IGlzTmFOKGgpID8gMSA6IGE7XG4gIH1cblxuICBfY3JlYXRlQ2xhc3MoSFNWQUNvbG9yLCBbe1xuICAgIGtleTogJ3RvU3RyaW5nJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gdG9TdHJpbmcoKSB7XG4gICAgICByZXR1cm4gdGhpcy5oICsgJywgJyArIHRoaXMucyArICclLCAnICsgdGhpcy52ICsgJyUsICcgKyB0aGlzLmE7XG4gICAgfVxuICB9XSk7XG5cbiAgcmV0dXJuIEhTVkFDb2xvcjtcbn0oKTtcblxuLyoqXG4gKiBIU1ZBIGNvbG9yIG1hbmlwdWxhdGlvblxuICovXG5cblxudmFyIENvbG9ySXRlbSA9IGZ1bmN0aW9uICgpIHtcbiAgX2NyZWF0ZUNsYXNzKENvbG9ySXRlbSwgW3tcbiAgICBrZXk6ICdhcGknLFxuXG5cbiAgICAvKipcbiAgICAgKiBBcHBsaWVzIGEgbWV0aG9kIG9mIHRoZSBRaXhDb2xvciBBUEkgYW5kIHJldHVybnMgYSBuZXcgQ29sb3Igb2JqZWN0IG9yXG4gICAgICogdGhlIHJldHVybiB2YWx1ZSBvZiB0aGUgbWV0aG9kIGNhbGwuXG4gICAgICpcbiAgICAgKiBJZiBubyBhcmd1bWVudCBpcyBwcm92aWRlZCwgdGhlIGludGVybmFsIFFpeENvbG9yIG9iamVjdCBpcyByZXR1cm5lZC5cbiAgICAgKlxuICAgICAqIEBwYXJhbSB7U3RyaW5nfSBmbiBRaXhDb2xvciBmdW5jdGlvbiBuYW1lXG4gICAgICogQHBhcmFtIGFyZ3MgUWl4Q29sb3IgZnVuY3Rpb24gYXJndW1lbnRzXG4gICAgICogQGV4YW1wbGUgbGV0IGRhcmtlckNvbG9yID0gY29sb3IuYXBpKCdkYXJrZW4nLCAwLjI1KTtcbiAgICAgKiBAZXhhbXBsZSBsZXQgbHVtaW5vc2l0eSA9IGNvbG9yLmFwaSgnbHVtaW5vc2l0eScpO1xuICAgICAqIEBleGFtcGxlIGNvbG9yID0gY29sb3IuYXBpKCduZWdhdGUnKTtcbiAgICAgKiBAZXhhbXBsZSBsZXQgcUNvbG9yID0gY29sb3IuYXBpKCkubmVnYXRlKCk7XG4gICAgICogQHJldHVybnMge0NvbG9ySXRlbXxRaXhDb2xvcnwqfVxuICAgICAqL1xuICAgIHZhbHVlOiBmdW5jdGlvbiBhcGkoZm4pIHtcbiAgICAgIGZvciAodmFyIF9sZW4gPSBhcmd1bWVudHMubGVuZ3RoLCBhcmdzID0gQXJyYXkoX2xlbiA+IDEgPyBfbGVuIC0gMSA6IDApLCBfa2V5ID0gMTsgX2tleSA8IF9sZW47IF9rZXkrKykge1xuICAgICAgICBhcmdzW19rZXkgLSAxXSA9IGFyZ3VtZW50c1tfa2V5XTtcbiAgICAgIH1cblxuICAgICAgaWYgKGFyZ3VtZW50cy5sZW5ndGggPT09IDApIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuX2NvbG9yO1xuICAgICAgfVxuXG4gICAgICB2YXIgcmVzdWx0ID0gdGhpcy5fY29sb3JbZm5dLmFwcGx5KHRoaXMuX2NvbG9yLCBhcmdzKTtcblxuICAgICAgaWYgKCEocmVzdWx0IGluc3RhbmNlb2YgX2NvbG9yMi5kZWZhdWx0KSkge1xuICAgICAgICAvLyByZXR1cm4gcmVzdWx0IG9mIHRoZSBtZXRob2QgY2FsbFxuICAgICAgICByZXR1cm4gcmVzdWx0O1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gbmV3IENvbG9ySXRlbShyZXN1bHQsIHRoaXMuZm9ybWF0KTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBSZXR1cm5zIHRoZSBvcmlnaW5hbCBDb2xvckl0ZW0gY29uc3RydWN0b3IgZGF0YSxcbiAgICAgKiBwbHVzIGEgJ3ZhbGlkJyBmbGFnIHRvIGtub3cgaWYgaXQncyB2YWxpZCBvciBub3QuXG4gICAgICpcbiAgICAgKiBAcmV0dXJucyB7e2NvbG9yOiAqLCBmb3JtYXQ6IFN0cmluZywgdmFsaWQ6IGJvb2xlYW59fVxuICAgICAqL1xuXG4gIH0sIHtcbiAgICBrZXk6ICdvcmlnaW5hbCcsXG4gICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgICByZXR1cm4gdGhpcy5fb3JpZ2luYWw7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogQHBhcmFtIHtDb2xvckl0ZW18SFNWQUNvbG9yfFFpeENvbG9yfFN0cmluZ3wqfG51bGx9IGNvbG9yIENvbG9yIGRhdGFcbiAgICAgKiBAcGFyYW0ge1N0cmluZ3xudWxsfSBmb3JtYXQgQ29sb3IgbW9kZWwgdG8gY29udmVydCB0byBieSBkZWZhdWx0LiBTdXBwb3J0ZWQ6ICdyZ2InLCAnaHNsJywgJ2hleCcuXG4gICAgICovXG5cbiAgfV0sIFt7XG4gICAga2V5OiAnSFNWQUNvbG9yJyxcblxuXG4gICAgLyoqXG4gICAgICogUmV0dXJucyB0aGUgSFNWQUNvbG9yIGNsYXNzXG4gICAgICpcbiAgICAgKiBAc3RhdGljXG4gICAgICogQGV4YW1wbGUgbGV0IGNvbG9yRGF0YSA9IG5ldyBDb2xvckl0ZW0uSFNWQUNvbG9yKDM2MCwgMTAwLCAxMDAsIDEpO1xuICAgICAqIEByZXR1cm5zIHtIU1ZBQ29sb3J9XG4gICAgICovXG4gICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgICByZXR1cm4gSFNWQUNvbG9yO1xuICAgIH1cbiAgfV0pO1xuXG4gIGZ1bmN0aW9uIENvbG9ySXRlbSgpIHtcbiAgICB2YXIgY29sb3IgPSBhcmd1bWVudHMubGVuZ3RoID4gMCAmJiBhcmd1bWVudHNbMF0gIT09IHVuZGVmaW5lZCA/IGFyZ3VtZW50c1swXSA6IG51bGw7XG4gICAgdmFyIGZvcm1hdCA9IGFyZ3VtZW50cy5sZW5ndGggPiAxICYmIGFyZ3VtZW50c1sxXSAhPT0gdW5kZWZpbmVkID8gYXJndW1lbnRzWzFdIDogbnVsbDtcblxuICAgIF9jbGFzc0NhbGxDaGVjayh0aGlzLCBDb2xvckl0ZW0pO1xuXG4gICAgdGhpcy5yZXBsYWNlKGNvbG9yLCBmb3JtYXQpO1xuICB9XG5cbiAgLyoqXG4gICAqIFJlcGxhY2VzIHRoZSBpbnRlcm5hbCBRaXhDb2xvciBvYmplY3Qgd2l0aCBhIG5ldyBvbmUuXG4gICAqIFRoaXMgYWxzbyByZXBsYWNlcyB0aGUgaW50ZXJuYWwgb3JpZ2luYWwgY29sb3IgZGF0YS5cbiAgICpcbiAgICogQHBhcmFtIHtDb2xvckl0ZW18SFNWQUNvbG9yfFFpeENvbG9yfFN0cmluZ3wqfG51bGx9IGNvbG9yIENvbG9yIGRhdGEgdG8gYmUgcGFyc2VkIChpZiBuZWVkZWQpXG4gICAqIEBwYXJhbSB7U3RyaW5nfG51bGx9IGZvcm1hdCBDb2xvciBtb2RlbCB0byBjb252ZXJ0IHRvIGJ5IGRlZmF1bHQuIFN1cHBvcnRlZDogJ3JnYicsICdoc2wnLCAnaGV4Jy5cbiAgICogQGV4YW1wbGUgY29sb3IucmVwbGFjZSgncmdiKDI1NSwwLDApJywgJ2hzbCcpO1xuICAgKiBAZXhhbXBsZSBjb2xvci5yZXBsYWNlKGhzdmFDb2xvckRhdGEpO1xuICAgKi9cblxuXG4gIF9jcmVhdGVDbGFzcyhDb2xvckl0ZW0sIFt7XG4gICAga2V5OiAncmVwbGFjZScsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHJlcGxhY2UoY29sb3IpIHtcbiAgICAgIHZhciBmb3JtYXQgPSBhcmd1bWVudHMubGVuZ3RoID4gMSAmJiBhcmd1bWVudHNbMV0gIT09IHVuZGVmaW5lZCA/IGFyZ3VtZW50c1sxXSA6IG51bGw7XG5cbiAgICAgIGZvcm1hdCA9IENvbG9ySXRlbS5zYW5pdGl6ZUZvcm1hdChmb3JtYXQpO1xuXG4gICAgICAvKipcbiAgICAgICAqIEB0eXBlIHt7Y29sb3I6ICosIGZvcm1hdDogU3RyaW5nfX1cbiAgICAgICAqIEBwcml2YXRlXG4gICAgICAgKi9cbiAgICAgIHRoaXMuX29yaWdpbmFsID0ge1xuICAgICAgICBjb2xvcjogY29sb3IsXG4gICAgICAgIGZvcm1hdDogZm9ybWF0LFxuICAgICAgICB2YWxpZDogdHJ1ZVxuICAgICAgfTtcbiAgICAgIC8qKlxuICAgICAgICogQHR5cGUge1FpeENvbG9yfVxuICAgICAgICogQHByaXZhdGVcbiAgICAgICAqL1xuICAgICAgdGhpcy5fY29sb3IgPSBDb2xvckl0ZW0ucGFyc2UoY29sb3IpO1xuXG4gICAgICBpZiAodGhpcy5fY29sb3IgPT09IG51bGwpIHtcbiAgICAgICAgdGhpcy5fY29sb3IgPSAoMCwgX2NvbG9yMi5kZWZhdWx0KSgpO1xuICAgICAgICB0aGlzLl9vcmlnaW5hbC52YWxpZCA9IGZhbHNlO1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG5cbiAgICAgIC8qKlxuICAgICAgICogQHR5cGUgeyp8c3RyaW5nfVxuICAgICAgICogQHByaXZhdGVcbiAgICAgICAqL1xuICAgICAgdGhpcy5fZm9ybWF0ID0gZm9ybWF0ID8gZm9ybWF0IDogQ29sb3JJdGVtLmlzSGV4KGNvbG9yKSA/ICdoZXgnIDogdGhpcy5fY29sb3IubW9kZWw7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogUGFyc2VzIHRoZSBjb2xvciByZXR1cm5pbmcgYSBRaXggQ29sb3Igb2JqZWN0IG9yIG51bGwgaWYgY2Fubm90IGJlXG4gICAgICogcGFyc2VkLlxuICAgICAqXG4gICAgICogQHBhcmFtIHtDb2xvckl0ZW18SFNWQUNvbG9yfFFpeENvbG9yfFN0cmluZ3wqfG51bGx9IGNvbG9yIENvbG9yIGRhdGFcbiAgICAgKiBAZXhhbXBsZSBsZXQgcUNvbG9yID0gQ29sb3JJdGVtLnBhcnNlKCdyZ2IoMjU1LDAsMCknKTtcbiAgICAgKiBAc3RhdGljXG4gICAgICogQHJldHVybnMge1FpeENvbG9yfG51bGx9XG4gICAgICovXG5cbiAgfSwge1xuICAgIGtleTogJ2lzVmFsaWQnLFxuXG5cbiAgICAvKipcbiAgICAgKiBSZXR1cm5zIHRydWUgaWYgdGhlIGNvbG9yIGlzIHZhbGlkLCBmYWxzZSBpZiBub3QuXG4gICAgICpcbiAgICAgKiBAcmV0dXJucyB7Ym9vbGVhbn1cbiAgICAgKi9cbiAgICB2YWx1ZTogZnVuY3Rpb24gaXNWYWxpZCgpIHtcbiAgICAgIHJldHVybiB0aGlzLl9vcmlnaW5hbC52YWxpZCA9PT0gdHJ1ZTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBIdWUgdmFsdWUgZnJvbSAwIHRvIDM2MFxuICAgICAqXG4gICAgICogQHJldHVybnMge2ludH1cbiAgICAgKi9cblxuICB9LCB7XG4gICAga2V5OiAnc2V0SHVlUmF0aW8nLFxuXG5cbiAgICAvKipcbiAgICAgKiBTZXRzIHRoZSBodWUgcmF0aW8sIHdoZXJlIDEuMCBpcyAwLCAwLjUgaXMgMTgwIGFuZCAwLjAgaXMgMzYwLlxuICAgICAqXG4gICAgICogQGlnbm9yZVxuICAgICAqIEBwYXJhbSB7bnVtYmVyfSBoIFJhdGlvIGZyb20gMS4wIHRvIDAuMFxuICAgICAqL1xuICAgIHZhbHVlOiBmdW5jdGlvbiBzZXRIdWVSYXRpbyhoKSB7XG4gICAgICB0aGlzLmh1ZSA9ICgxIC0gaCkgKiAzNjA7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogU2V0cyB0aGUgc2F0dXJhdGlvbiB2YWx1ZVxuICAgICAqXG4gICAgICogQHBhcmFtIHtpbnR9IHZhbHVlIEludGVnZXIgZnJvbSAwIHRvIDEwMFxuICAgICAqL1xuXG4gIH0sIHtcbiAgICBrZXk6ICdzZXRTYXR1cmF0aW9uUmF0aW8nLFxuXG5cbiAgICAvKipcbiAgICAgKiBTZXRzIHRoZSBzYXR1cmF0aW9uIHJhdGlvLCB3aGVyZSAxLjAgaXMgMTAwIGFuZCAwLjAgaXMgMC5cbiAgICAgKlxuICAgICAqIEBpZ25vcmVcbiAgICAgKiBAcGFyYW0ge251bWJlcn0gcyBSYXRpbyBmcm9tIDAuMCB0byAxLjBcbiAgICAgKi9cbiAgICB2YWx1ZTogZnVuY3Rpb24gc2V0U2F0dXJhdGlvblJhdGlvKHMpIHtcbiAgICAgIHRoaXMuc2F0dXJhdGlvbiA9IHMgKiAxMDA7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogU2V0cyB0aGUgJ3ZhbHVlJyBjaGFubmVsIHZhbHVlXG4gICAgICpcbiAgICAgKiBAcGFyYW0ge2ludH0gdmFsdWUgSW50ZWdlciBmcm9tIDAgdG8gMTAwXG4gICAgICovXG5cbiAgfSwge1xuICAgIGtleTogJ3NldFZhbHVlUmF0aW8nLFxuXG5cbiAgICAvKipcbiAgICAgKiBTZXRzIHRoZSB2YWx1ZSByYXRpbywgd2hlcmUgMS4wIGlzIDAgYW5kIDAuMCBpcyAxMDAuXG4gICAgICpcbiAgICAgKiBAaWdub3JlXG4gICAgICogQHBhcmFtIHtudW1iZXJ9IHYgUmF0aW8gZnJvbSAxLjAgdG8gMC4wXG4gICAgICovXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHNldFZhbHVlUmF0aW8odikge1xuICAgICAgdGhpcy52YWx1ZSA9ICgxIC0gdikgKiAxMDA7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogU2V0cyB0aGUgYWxwaGEgdmFsdWUuIEl0IHdpbGwgYmUgcm91bmRlZCB0byAyIGRlY2ltYWxzLlxuICAgICAqXG4gICAgICogQHBhcmFtIHtpbnR9IHZhbHVlIEZsb2F0IGZyb20gMC4wIHRvIDEuMFxuICAgICAqL1xuXG4gIH0sIHtcbiAgICBrZXk6ICdzZXRBbHBoYVJhdGlvJyxcblxuXG4gICAgLyoqXG4gICAgICogU2V0cyB0aGUgYWxwaGEgcmF0aW8sIHdoZXJlIDEuMCBpcyAwLjAgYW5kIDAuMCBpcyAxLjAuXG4gICAgICpcbiAgICAgKiBAaWdub3JlXG4gICAgICogQHBhcmFtIHtudW1iZXJ9IGEgUmF0aW8gZnJvbSAxLjAgdG8gMC4wXG4gICAgICovXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHNldEFscGhhUmF0aW8oYSkge1xuICAgICAgdGhpcy5hbHBoYSA9IDEgLSBhO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIFNldHMgdGhlIGRlZmF1bHQgY29sb3IgZm9ybWF0XG4gICAgICpcbiAgICAgKiBAcGFyYW0ge1N0cmluZ30gdmFsdWUgU3VwcG9ydGVkOiAncmdiJywgJ2hzbCcsICdoZXgnXG4gICAgICovXG5cbiAgfSwge1xuICAgIGtleTogJ2lzRGVzYXR1cmF0ZWQnLFxuXG5cbiAgICAvKipcbiAgICAgKiBSZXR1cm5zIHRydWUgaWYgdGhlIHNhdHVyYXRpb24gdmFsdWUgaXMgemVybywgZmFsc2Ugb3RoZXJ3aXNlXG4gICAgICpcbiAgICAgKiBAcmV0dXJucyB7Ym9vbGVhbn1cbiAgICAgKi9cbiAgICB2YWx1ZTogZnVuY3Rpb24gaXNEZXNhdHVyYXRlZCgpIHtcbiAgICAgIHJldHVybiB0aGlzLnNhdHVyYXRpb24gPT09IDA7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogUmV0dXJucyB0cnVlIGlmIHRoZSBhbHBoYSB2YWx1ZSBpcyB6ZXJvLCBmYWxzZSBvdGhlcndpc2VcbiAgICAgKlxuICAgICAqIEByZXR1cm5zIHtib29sZWFufVxuICAgICAqL1xuXG4gIH0sIHtcbiAgICBrZXk6ICdpc1RyYW5zcGFyZW50JyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gaXNUcmFuc3BhcmVudCgpIHtcbiAgICAgIHJldHVybiB0aGlzLmFscGhhID09PSAwO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIFJldHVybnMgdHJ1ZSBpZiB0aGUgYWxwaGEgdmFsdWUgaXMgbnVtZXJpYyBhbmQgbGVzcyB0aGFuIDEsIGZhbHNlIG90aGVyd2lzZVxuICAgICAqXG4gICAgICogQHJldHVybnMge2Jvb2xlYW59XG4gICAgICovXG5cbiAgfSwge1xuICAgIGtleTogJ2hhc1RyYW5zcGFyZW5jeScsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGhhc1RyYW5zcGFyZW5jeSgpIHtcbiAgICAgIHJldHVybiB0aGlzLmhhc0FscGhhKCkgJiYgdGhpcy5hbHBoYSA8IDE7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogUmV0dXJucyB0cnVlIGlmIHRoZSBhbHBoYSB2YWx1ZSBpcyBudW1lcmljLCBmYWxzZSBvdGhlcndpc2VcbiAgICAgKlxuICAgICAqIEByZXR1cm5zIHtib29sZWFufVxuICAgICAqL1xuXG4gIH0sIHtcbiAgICBrZXk6ICdoYXNBbHBoYScsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGhhc0FscGhhKCkge1xuICAgICAgcmV0dXJuICFpc05hTih0aGlzLmFscGhhKTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBSZXR1cm5zIGEgbmV3IEhTVkFDb2xvciBvYmplY3QsIGJhc2VkIG9uIHRoZSBjdXJyZW50IGNvbG9yXG4gICAgICpcbiAgICAgKiBAcmV0dXJucyB7SFNWQUNvbG9yfVxuICAgICAqL1xuXG4gIH0sIHtcbiAgICBrZXk6ICd0b09iamVjdCcsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHRvT2JqZWN0KCkge1xuICAgICAgcmV0dXJuIG5ldyBIU1ZBQ29sb3IodGhpcy5odWUsIHRoaXMuc2F0dXJhdGlvbiwgdGhpcy52YWx1ZSwgdGhpcy5hbHBoYSk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogQWxpYXMgb2YgdG9PYmplY3QoKVxuICAgICAqXG4gICAgICogQHJldHVybnMge0hTVkFDb2xvcn1cbiAgICAgKi9cblxuICB9LCB7XG4gICAga2V5OiAndG9Ic3ZhJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gdG9Ic3ZhKCkge1xuICAgICAgcmV0dXJuIHRoaXMudG9PYmplY3QoKTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBSZXR1cm5zIGEgbmV3IEhTVkFDb2xvciBvYmplY3Qgd2l0aCB0aGUgcmF0aW8gdmFsdWVzIChmcm9tIDAuMCB0byAxLjApLFxuICAgICAqIGJhc2VkIG9uIHRoZSBjdXJyZW50IGNvbG9yLlxuICAgICAqXG4gICAgICogQGlnbm9yZVxuICAgICAqIEByZXR1cm5zIHtIU1ZBQ29sb3J9XG4gICAgICovXG5cbiAgfSwge1xuICAgIGtleTogJ3RvSHN2YVJhdGlvJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gdG9Ic3ZhUmF0aW8oKSB7XG4gICAgICByZXR1cm4gbmV3IEhTVkFDb2xvcih0aGlzLmh1ZSAvIDM2MCwgdGhpcy5zYXR1cmF0aW9uIC8gMTAwLCB0aGlzLnZhbHVlIC8gMTAwLCB0aGlzLmFscGhhKTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBDb252ZXJ0cyB0aGUgY3VycmVudCBjb2xvciB0byBpdHMgc3RyaW5nIHJlcHJlc2VudGF0aW9uLFxuICAgICAqIHVzaW5nIHRoZSBpbnRlcm5hbCBmb3JtYXQgb2YgdGhpcyBpbnN0YW5jZS5cbiAgICAgKlxuICAgICAqIEByZXR1cm5zIHtTdHJpbmd9XG4gICAgICovXG5cbiAgfSwge1xuICAgIGtleTogJ3RvU3RyaW5nJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gdG9TdHJpbmcoKSB7XG4gICAgICByZXR1cm4gdGhpcy5zdHJpbmcoKTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBDb252ZXJ0cyB0aGUgY3VycmVudCBjb2xvciB0byBpdHMgc3RyaW5nIHJlcHJlc2VudGF0aW9uLFxuICAgICAqIHVzaW5nIHRoZSBnaXZlbiBmb3JtYXQuXG4gICAgICpcbiAgICAgKiBAcGFyYW0ge1N0cmluZ3xudWxsfSBmb3JtYXQgRm9ybWF0IHRvIGNvbnZlcnQgdG8uIElmIGVtcHR5IG9yIG51bGwsIHRoZSBpbnRlcm5hbCBmb3JtYXQgd2lsbCBiZSB1c2VkLlxuICAgICAqIEByZXR1cm5zIHtTdHJpbmd9XG4gICAgICovXG5cbiAgfSwge1xuICAgIGtleTogJ3N0cmluZycsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHN0cmluZygpIHtcbiAgICAgIHZhciBmb3JtYXQgPSBhcmd1bWVudHMubGVuZ3RoID4gMCAmJiBhcmd1bWVudHNbMF0gIT09IHVuZGVmaW5lZCA/IGFyZ3VtZW50c1swXSA6IG51bGw7XG5cbiAgICAgIGZvcm1hdCA9IENvbG9ySXRlbS5zYW5pdGl6ZUZvcm1hdChmb3JtYXQgPyBmb3JtYXQgOiB0aGlzLmZvcm1hdCk7XG5cbiAgICAgIGlmICghZm9ybWF0KSB7XG4gICAgICAgIHJldHVybiB0aGlzLl9jb2xvci5yb3VuZCgpLnN0cmluZygpO1xuICAgICAgfVxuXG4gICAgICBpZiAodGhpcy5fY29sb3JbZm9ybWF0XSA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICAgIHRocm93IG5ldyBFcnJvcignVW5zdXBwb3J0ZWQgY29sb3IgZm9ybWF0OiBcXCcnICsgZm9ybWF0ICsgJ1xcJycpO1xuICAgICAgfVxuXG4gICAgICB2YXIgc3RyID0gdGhpcy5fY29sb3JbZm9ybWF0XSgpO1xuXG4gICAgICByZXR1cm4gc3RyLnJvdW5kID8gc3RyLnJvdW5kKCkuc3RyaW5nKCkgOiBzdHI7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogUmV0dXJucyB0cnVlIGlmIHRoZSBnaXZlbiBjb2xvciB2YWx1ZXMgZXF1YWxzIHRoaXMgb25lLCBmYWxzZSBvdGhlcndpc2UuXG4gICAgICogVGhlIGZvcm1hdCBpcyBub3QgY29tcGFyZWQuXG4gICAgICogSWYgYW55IG9mIHRoZSBjb2xvcnMgaXMgaW52YWxpZCwgdGhlIHJlc3VsdCB3aWxsIGJlIGZhbHNlLlxuICAgICAqXG4gICAgICogQHBhcmFtIHtDb2xvckl0ZW18SFNWQUNvbG9yfFFpeENvbG9yfFN0cmluZ3wqfG51bGx9IGNvbG9yIENvbG9yIGRhdGFcbiAgICAgKlxuICAgICAqIEByZXR1cm5zIHtib29sZWFufVxuICAgICAqL1xuXG4gIH0sIHtcbiAgICBrZXk6ICdlcXVhbHMnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBlcXVhbHMoY29sb3IpIHtcbiAgICAgIGNvbG9yID0gY29sb3IgaW5zdGFuY2VvZiBDb2xvckl0ZW0gPyBjb2xvciA6IG5ldyBDb2xvckl0ZW0oY29sb3IpO1xuXG4gICAgICBpZiAoIWNvbG9yLmlzVmFsaWQoKSB8fCAhdGhpcy5pc1ZhbGlkKCkpIHtcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gdGhpcy5odWUgPT09IGNvbG9yLmh1ZSAmJiB0aGlzLnNhdHVyYXRpb24gPT09IGNvbG9yLnNhdHVyYXRpb24gJiYgdGhpcy52YWx1ZSA9PT0gY29sb3IudmFsdWUgJiYgdGhpcy5hbHBoYSA9PT0gY29sb3IuYWxwaGE7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogQ3JlYXRlcyBhIGNvcHkgb2YgdGhpcyBpbnN0YW5jZVxuICAgICAqXG4gICAgICogQHJldHVybnMge0NvbG9ySXRlbX1cbiAgICAgKi9cblxuICB9LCB7XG4gICAga2V5OiAnZ2V0Q2xvbmUnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBnZXRDbG9uZSgpIHtcbiAgICAgIHJldHVybiBuZXcgQ29sb3JJdGVtKHRoaXMuX2NvbG9yLCB0aGlzLmZvcm1hdCk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogQ3JlYXRlcyBhIGNvcHkgb2YgdGhpcyBpbnN0YW5jZSwgb25seSBjb3B5aW5nIHRoZSBodWUgdmFsdWUsXG4gICAgICogYW5kIHNldHRpbmcgdGhlIG90aGVycyB0byBpdHMgbWF4IHZhbHVlLlxuICAgICAqXG4gICAgICogQHJldHVybnMge0NvbG9ySXRlbX1cbiAgICAgKi9cblxuICB9LCB7XG4gICAga2V5OiAnZ2V0Q2xvbmVIdWVPbmx5JyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gZ2V0Q2xvbmVIdWVPbmx5KCkge1xuICAgICAgcmV0dXJuIG5ldyBDb2xvckl0ZW0oW3RoaXMuaHVlLCAxMDAsIDEwMCwgMV0sIHRoaXMuZm9ybWF0KTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBDcmVhdGVzIGEgY29weSBvZiB0aGlzIGluc3RhbmNlIHNldHRpbmcgdGhlIGFscGhhIHRvIHRoZSBtYXguXG4gICAgICpcbiAgICAgKiBAcmV0dXJucyB7Q29sb3JJdGVtfVxuICAgICAqL1xuXG4gIH0sIHtcbiAgICBrZXk6ICdnZXRDbG9uZU9wYXF1ZScsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGdldENsb25lT3BhcXVlKCkge1xuICAgICAgcmV0dXJuIG5ldyBDb2xvckl0ZW0odGhpcy5fY29sb3IuYWxwaGEoMSksIHRoaXMuZm9ybWF0KTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBDb252ZXJ0cyB0aGUgY29sb3IgdG8gYSBSR0Igc3RyaW5nXG4gICAgICpcbiAgICAgKiBAcmV0dXJucyB7U3RyaW5nfVxuICAgICAqL1xuXG4gIH0sIHtcbiAgICBrZXk6ICd0b1JnYlN0cmluZycsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHRvUmdiU3RyaW5nKCkge1xuICAgICAgcmV0dXJuIHRoaXMuc3RyaW5nKCdyZ2InKTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBDb252ZXJ0cyB0aGUgY29sb3IgdG8gYSBIZXhhZGVjaW1hbCBzdHJpbmdcbiAgICAgKlxuICAgICAqIEByZXR1cm5zIHtTdHJpbmd9XG4gICAgICovXG5cbiAgfSwge1xuICAgIGtleTogJ3RvSGV4U3RyaW5nJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gdG9IZXhTdHJpbmcoKSB7XG4gICAgICByZXR1cm4gdGhpcy5zdHJpbmcoJ2hleCcpO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIENvbnZlcnRzIHRoZSBjb2xvciB0byBhIEhTTCBzdHJpbmdcbiAgICAgKlxuICAgICAqIEByZXR1cm5zIHtTdHJpbmd9XG4gICAgICovXG5cbiAgfSwge1xuICAgIGtleTogJ3RvSHNsU3RyaW5nJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gdG9Ic2xTdHJpbmcoKSB7XG4gICAgICByZXR1cm4gdGhpcy5zdHJpbmcoJ2hzbCcpO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIFJldHVybnMgdHJ1ZSBpZiB0aGUgY29sb3IgaXMgZGFyaywgZmFsc2Ugb3RoZXJ3aGlzZS5cbiAgICAgKiBUaGlzIGlzIHVzZWZ1bCB0byBkZWNpZGUgYSB0ZXh0IGNvbG9yLlxuICAgICAqXG4gICAgICogQHJldHVybnMge2Jvb2xlYW59XG4gICAgICovXG5cbiAgfSwge1xuICAgIGtleTogJ2lzRGFyaycsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGlzRGFyaygpIHtcbiAgICAgIHJldHVybiB0aGlzLl9jb2xvci5pc0RhcmsoKTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBSZXR1cm5zIHRydWUgaWYgdGhlIGNvbG9yIGlzIGxpZ2h0LCBmYWxzZSBvdGhlcndoaXNlLlxuICAgICAqIFRoaXMgaXMgdXNlZnVsIHRvIGRlY2lkZSBhIHRleHQgY29sb3IuXG4gICAgICpcbiAgICAgKiBAcmV0dXJucyB7Ym9vbGVhbn1cbiAgICAgKi9cblxuICB9LCB7XG4gICAga2V5OiAnaXNMaWdodCcsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGlzTGlnaHQoKSB7XG4gICAgICByZXR1cm4gdGhpcy5fY29sb3IuaXNMaWdodCgpO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEdlbmVyYXRlcyBhIGxpc3Qgb2YgY29sb3JzIHVzaW5nIHRoZSBnaXZlbiBodWUtYmFzZWQgZm9ybXVsYSBvciB0aGUgZ2l2ZW4gYXJyYXkgb2YgaHVlIHZhbHVlcy5cbiAgICAgKiBIdWUgZm9ybXVsYXMgY2FuIGJlIGV4dGVuZGVkIHVzaW5nIENvbG9ySXRlbS5jb2xvckZvcm11bGFzIHN0YXRpYyBwcm9wZXJ0eS5cbiAgICAgKlxuICAgICAqIEBwYXJhbSB7U3RyaW5nfE51bWJlcltdfSBmb3JtdWxhIEV4YW1wbGVzOiAnY29tcGxlbWVudGFyeScsICd0cmlhZCcsICd0ZXRyYWQnLCAnc3BsaXRjb21wbGVtZW50JywgWzE4MCwgMjcwXVxuICAgICAqIEBleGFtcGxlIGxldCBjb2xvcnMgPSBjb2xvci5nZW5lcmF0ZSgndHJpYWQnKTtcbiAgICAgKiBAZXhhbXBsZSBsZXQgY29sb3JzID0gY29sb3IuZ2VuZXJhdGUoWzQ1LCA4MCwgMTEyLCAyMDBdKTtcbiAgICAgKiBAcmV0dXJucyB7Q29sb3JJdGVtW119XG4gICAgICovXG5cbiAgfSwge1xuICAgIGtleTogJ2dlbmVyYXRlJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gZ2VuZXJhdGUoZm9ybXVsYSkge1xuICAgICAgdmFyIGh1ZXMgPSBbXTtcblxuICAgICAgaWYgKEFycmF5LmlzQXJyYXkoZm9ybXVsYSkpIHtcbiAgICAgICAgaHVlcyA9IGZvcm11bGE7XG4gICAgICB9IGVsc2UgaWYgKCFDb2xvckl0ZW0uY29sb3JGb3JtdWxhcy5oYXNPd25Qcm9wZXJ0eShmb3JtdWxhKSkge1xuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ05vIGNvbG9yIGZvcm11bGEgZm91bmQgd2l0aCB0aGUgbmFtZSBcXCcnICsgZm9ybXVsYSArICdcXCcuJyk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBodWVzID0gQ29sb3JJdGVtLmNvbG9yRm9ybXVsYXNbZm9ybXVsYV07XG4gICAgICB9XG5cbiAgICAgIHZhciBjb2xvcnMgPSBbXSxcbiAgICAgICAgICBtYWluQ29sb3IgPSB0aGlzLl9jb2xvcixcbiAgICAgICAgICBmb3JtYXQgPSB0aGlzLmZvcm1hdDtcblxuICAgICAgaHVlcy5mb3JFYWNoKGZ1bmN0aW9uIChodWUpIHtcbiAgICAgICAgdmFyIGxldmVscyA9IFtodWUgPyAobWFpbkNvbG9yLmh1ZSgpICsgaHVlKSAlIDM2MCA6IG1haW5Db2xvci5odWUoKSwgbWFpbkNvbG9yLnNhdHVyYXRpb252KCksIG1haW5Db2xvci52YWx1ZSgpLCBtYWluQ29sb3IuYWxwaGEoKV07XG5cbiAgICAgICAgY29sb3JzLnB1c2gobmV3IENvbG9ySXRlbShsZXZlbHMsIGZvcm1hdCkpO1xuICAgICAgfSk7XG5cbiAgICAgIHJldHVybiBjb2xvcnM7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnaHVlJyxcbiAgICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICAgIHJldHVybiB0aGlzLl9jb2xvci5odWUoKTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBTYXR1cmF0aW9uIHZhbHVlIGZyb20gMCB0byAxMDBcbiAgICAgKlxuICAgICAqIEByZXR1cm5zIHtpbnR9XG4gICAgICovXG4gICAgLFxuXG5cbiAgICAvKipcbiAgICAgKiBTZXRzIHRoZSBodWUgdmFsdWVcbiAgICAgKlxuICAgICAqIEBwYXJhbSB7aW50fSB2YWx1ZSBJbnRlZ2VyIGZyb20gMCB0byAzNjBcbiAgICAgKi9cbiAgICBzZXQ6IGZ1bmN0aW9uIHNldCh2YWx1ZSkge1xuICAgICAgdGhpcy5fY29sb3IgPSB0aGlzLl9jb2xvci5odWUodmFsdWUpO1xuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ3NhdHVyYXRpb24nLFxuICAgIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgICAgcmV0dXJuIHRoaXMuX2NvbG9yLnNhdHVyYXRpb252KCk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogVmFsdWUgY2hhbm5lbCB2YWx1ZSBmcm9tIDAgdG8gMTAwXG4gICAgICpcbiAgICAgKiBAcmV0dXJucyB7aW50fVxuICAgICAqL1xuICAgICxcbiAgICBzZXQ6IGZ1bmN0aW9uIHNldCh2YWx1ZSkge1xuICAgICAgdGhpcy5fY29sb3IgPSB0aGlzLl9jb2xvci5zYXR1cmF0aW9udih2YWx1ZSk7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAndmFsdWUnLFxuICAgIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgICAgcmV0dXJuIHRoaXMuX2NvbG9yLnZhbHVlKCk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogQWxwaGEgdmFsdWUgZnJvbSAwLjAgdG8gMS4wXG4gICAgICpcbiAgICAgKiBAcmV0dXJucyB7bnVtYmVyfVxuICAgICAqL1xuICAgICxcbiAgICBzZXQ6IGZ1bmN0aW9uIHNldCh2YWx1ZSkge1xuICAgICAgdGhpcy5fY29sb3IgPSB0aGlzLl9jb2xvci52YWx1ZSh2YWx1ZSk7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnYWxwaGEnLFxuICAgIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgICAgdmFyIGEgPSB0aGlzLl9jb2xvci5hbHBoYSgpO1xuXG4gICAgICByZXR1cm4gaXNOYU4oYSkgPyAxIDogYTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBEZWZhdWx0IGNvbG9yIGZvcm1hdCB0byBjb252ZXJ0IHRvIHdoZW4gY2FsbGluZyB0b1N0cmluZygpIG9yIHN0cmluZygpXG4gICAgICpcbiAgICAgKiBAcmV0dXJucyB7U3RyaW5nfSAncmdiJywgJ2hzbCcsICdoZXgnIG9yICcnXG4gICAgICovXG4gICAgLFxuICAgIHNldDogZnVuY3Rpb24gc2V0KHZhbHVlKSB7XG4gICAgICAvLyAyIGRlY2ltYWxzIG1heFxuICAgICAgdGhpcy5fY29sb3IgPSB0aGlzLl9jb2xvci5hbHBoYShNYXRoLnJvdW5kKHZhbHVlICogMTAwKSAvIDEwMCk7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnZm9ybWF0JyxcbiAgICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICAgIHJldHVybiB0aGlzLl9mb3JtYXQgPyB0aGlzLl9mb3JtYXQgOiB0aGlzLl9jb2xvci5tb2RlbDtcbiAgICB9LFxuICAgIHNldDogZnVuY3Rpb24gc2V0KHZhbHVlKSB7XG4gICAgICB0aGlzLl9mb3JtYXQgPSBDb2xvckl0ZW0uc2FuaXRpemVGb3JtYXQodmFsdWUpO1xuICAgIH1cbiAgfV0sIFt7XG4gICAga2V5OiAncGFyc2UnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBwYXJzZShjb2xvcikge1xuICAgICAgaWYgKGNvbG9yIGluc3RhbmNlb2YgX2NvbG9yMi5kZWZhdWx0KSB7XG4gICAgICAgIHJldHVybiBjb2xvcjtcbiAgICAgIH1cblxuICAgICAgaWYgKGNvbG9yIGluc3RhbmNlb2YgQ29sb3JJdGVtKSB7XG4gICAgICAgIHJldHVybiBjb2xvci5fY29sb3I7XG4gICAgICB9XG5cbiAgICAgIHZhciBmb3JtYXQgPSBudWxsO1xuXG4gICAgICBpZiAoY29sb3IgaW5zdGFuY2VvZiBIU1ZBQ29sb3IpIHtcbiAgICAgICAgY29sb3IgPSBbY29sb3IuaCwgY29sb3IucywgY29sb3IudiwgaXNOYU4oY29sb3IuYSkgPyAxIDogY29sb3IuYV07XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBjb2xvciA9IENvbG9ySXRlbS5zYW5pdGl6ZVN0cmluZyhjb2xvcik7XG4gICAgICB9XG5cbiAgICAgIGlmIChjb2xvciA9PT0gbnVsbCkge1xuICAgICAgICByZXR1cm4gbnVsbDtcbiAgICAgIH1cblxuICAgICAgaWYgKEFycmF5LmlzQXJyYXkoY29sb3IpKSB7XG4gICAgICAgIGZvcm1hdCA9ICdoc3YnO1xuICAgICAgfVxuXG4gICAgICB0cnkge1xuICAgICAgICByZXR1cm4gKDAsIF9jb2xvcjIuZGVmYXVsdCkoY29sb3IsIGZvcm1hdCk7XG4gICAgICB9IGNhdGNoIChlKSB7XG4gICAgICAgIHJldHVybiBudWxsO1xuICAgICAgfVxuICAgIH1cblxuICAgIC8qKlxuICAgICAqIFNhbml0aXplcyBhIGNvbG9yIHN0cmluZywgYWRkaW5nIG1pc3NpbmcgaGFzaCB0byBoZXhhZGVjaW1hbCBjb2xvcnNcbiAgICAgKiBhbmQgY29udmVydGluZyAndHJhbnNwYXJlbnQnIHRvIGEgY29sb3IgY29kZS5cbiAgICAgKlxuICAgICAqIEBwYXJhbSB7U3RyaW5nfCp9IHN0ciBDb2xvciBzdHJpbmdcbiAgICAgKiBAZXhhbXBsZSBsZXQgY29sb3JTdHIgPSBDb2xvckl0ZW0uc2FuaXRpemVTdHJpbmcoJ2ZmYWEwMCcpO1xuICAgICAqIEBzdGF0aWNcbiAgICAgKiBAcmV0dXJucyB7U3RyaW5nfCp9XG4gICAgICovXG5cbiAgfSwge1xuICAgIGtleTogJ3Nhbml0aXplU3RyaW5nJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gc2FuaXRpemVTdHJpbmcoc3RyKSB7XG4gICAgICBpZiAoISh0eXBlb2Ygc3RyID09PSAnc3RyaW5nJyB8fCBzdHIgaW5zdGFuY2VvZiBTdHJpbmcpKSB7XG4gICAgICAgIHJldHVybiBzdHI7XG4gICAgICB9XG5cbiAgICAgIGlmIChzdHIubWF0Y2goL15bMC05YS1mXXsyLH0kL2kpKSB7XG4gICAgICAgIHJldHVybiAnIycgKyBzdHI7XG4gICAgICB9XG5cbiAgICAgIGlmIChzdHIudG9Mb3dlckNhc2UoKSA9PT0gJ3RyYW5zcGFyZW50Jykge1xuICAgICAgICByZXR1cm4gJyNGRkZGRkYwMCc7XG4gICAgICB9XG5cbiAgICAgIHJldHVybiBzdHI7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogRGV0ZWN0cyBpZiBhIHZhbHVlIGlzIGEgc3RyaW5nIGFuZCBhIGNvbG9yIGluIGhleGFkZWNpbWFsIGZvcm1hdCAoaW4gYW55IHZhcmlhbnQpLlxuICAgICAqXG4gICAgICogQHBhcmFtIHtTdHJpbmd9IHN0clxuICAgICAqIEBleGFtcGxlIENvbG9ySXRlbS5pc0hleCgncmdiYSgwLDAsMCknKTsgLy8gZmFsc2VcbiAgICAgKiBAZXhhbXBsZSBDb2xvckl0ZW0uaXNIZXgoJ2ZmYWEwMCcpOyAvLyB0cnVlXG4gICAgICogQGV4YW1wbGUgQ29sb3JJdGVtLmlzSGV4KCcjZmZhYTAwJyk7IC8vIHRydWVcbiAgICAgKiBAc3RhdGljXG4gICAgICogQHJldHVybnMge2Jvb2xlYW59XG4gICAgICovXG5cbiAgfSwge1xuICAgIGtleTogJ2lzSGV4JyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gaXNIZXgoc3RyKSB7XG4gICAgICBpZiAoISh0eXBlb2Ygc3RyID09PSAnc3RyaW5nJyB8fCBzdHIgaW5zdGFuY2VvZiBTdHJpbmcpKSB7XG4gICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuICEhc3RyLm1hdGNoKC9eIz9bMC05YS1mXXsyLH0kL2kpO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIFNhbml0aXplcyBhIGNvbG9yIGZvcm1hdCB0byBvbmUgc3VwcG9ydGVkIGJ5IHdlYiBicm93c2Vycy5cbiAgICAgKiBSZXR1cm5zIGFuIGVtcHR5IHN0cmluZyBvZiB0aGUgZm9ybWF0IGNhbid0IGJlIHJlY29nbmlzZWQuXG4gICAgICpcbiAgICAgKiBAcGFyYW0ge1N0cmluZ3wqfSBmb3JtYXRcbiAgICAgKiBAZXhhbXBsZSBDb2xvckl0ZW0uc2FuaXRpemVGb3JtYXQoJ3JnYmEnKTsgLy8gJ3JnYidcbiAgICAgKiBAZXhhbXBsZSBDb2xvckl0ZW0uaXNIZXgoJ2hleDgnKTsgLy8gJ2hleCdcbiAgICAgKiBAZXhhbXBsZSBDb2xvckl0ZW0uaXNIZXgoJ2ludmFsaWQnKTsgLy8gJydcbiAgICAgKiBAc3RhdGljXG4gICAgICogQHJldHVybnMge1N0cmluZ30gJ3JnYicsICdoc2wnLCAnaGV4JyBvciAnJy5cbiAgICAgKi9cblxuICB9LCB7XG4gICAga2V5OiAnc2FuaXRpemVGb3JtYXQnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBzYW5pdGl6ZUZvcm1hdChmb3JtYXQpIHtcbiAgICAgIHN3aXRjaCAoZm9ybWF0KSB7XG4gICAgICAgIGNhc2UgJ2hleCc6XG4gICAgICAgIGNhc2UgJ2hleDMnOlxuICAgICAgICBjYXNlICdoZXg0JzpcbiAgICAgICAgY2FzZSAnaGV4Nic6XG4gICAgICAgIGNhc2UgJ2hleDgnOlxuICAgICAgICAgIHJldHVybiAnaGV4JztcbiAgICAgICAgY2FzZSAncmdiJzpcbiAgICAgICAgY2FzZSAncmdiYSc6XG4gICAgICAgIGNhc2UgJ2tleXdvcmQnOlxuICAgICAgICBjYXNlICduYW1lJzpcbiAgICAgICAgICByZXR1cm4gJ3JnYic7XG4gICAgICAgIGNhc2UgJ2hzbCc6XG4gICAgICAgIGNhc2UgJ2hzbGEnOlxuICAgICAgICBjYXNlICdoc3YnOlxuICAgICAgICBjYXNlICdoc3ZhJzpcbiAgICAgICAgY2FzZSAnaHdiJzogLy8gSFdCIHRoaXMgaXMgc3VwcG9ydGVkIGJ5IFFpeCBDb2xvciwgYnV0IG5vdCBieSBicm93c2Vyc1xuICAgICAgICBjYXNlICdod2JhJzpcbiAgICAgICAgICByZXR1cm4gJ2hzbCc7XG4gICAgICAgIGRlZmF1bHQ6XG4gICAgICAgICAgcmV0dXJuICcnO1xuICAgICAgfVxuICAgIH1cbiAgfV0pO1xuXG4gIHJldHVybiBDb2xvckl0ZW07XG59KCk7XG5cbi8qKlxuICogTGlzdCBvZiBodWUtYmFzZWQgY29sb3IgZm9ybXVsYXMgdXNlZCBieSBDb2xvckl0ZW0ucHJvdG90eXBlLmdlbmVyYXRlKClcbiAqXG4gKiBAc3RhdGljXG4gKiBAdHlwZSB7e2NvbXBsZW1lbnRhcnk6IG51bWJlcltdLCB0cmlhZDogbnVtYmVyW10sIHRldHJhZDogbnVtYmVyW10sIHNwbGl0Y29tcGxlbWVudDogbnVtYmVyW119fVxuICovXG5cblxuQ29sb3JJdGVtLmNvbG9yRm9ybXVsYXMgPSB7XG4gIGNvbXBsZW1lbnRhcnk6IFsxODBdLFxuICB0cmlhZDogWzAsIDEyMCwgMjQwXSxcbiAgdGV0cmFkOiBbMCwgOTAsIDE4MCwgMjcwXSxcbiAgc3BsaXRjb21wbGVtZW50OiBbMCwgNzIsIDIxNl1cbn07XG5cbmV4cG9ydHMuZGVmYXVsdCA9IENvbG9ySXRlbTtcbmV4cG9ydHMuSFNWQUNvbG9yID0gSFNWQUNvbG9yO1xuZXhwb3J0cy5Db2xvckl0ZW0gPSBDb2xvckl0ZW07XG5cbi8qKiovIH0pLFxuLyogMyAqL1xuLyoqKi8gKGZ1bmN0aW9uKG1vZHVsZSwgZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXykge1xuXG5cInVzZSBzdHJpY3RcIjtcblxuLyoqXG4gKiBAbW9kdWxlXG4gKi9cblxuLy8gYWRqdXN0IHRoZXNlIHZhbHVlcyBhY2NvcmRpbmdseSB0byB0aGUgc2FzcyB2YXJzXG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG52YXIgc2Fzc1ZhcnMgPSB7XG4gICdiYXJfc2l6ZV9zaG9ydCc6IDE2LFxuICAnYmFzZV9tYXJnaW4nOiA2LFxuICAnY29sdW1ucyc6IDZcbn07XG5cbnZhciBzbGlkZXJTaXplID0gc2Fzc1ZhcnMuYmFyX3NpemVfc2hvcnQgKiBzYXNzVmFycy5jb2x1bW5zICsgc2Fzc1ZhcnMuYmFzZV9tYXJnaW4gKiAoc2Fzc1ZhcnMuY29sdW1ucyAtIDEpO1xuXG4vKipcbiAqIENvbG9ycGlja2VyIGRlZmF1bHQgb3B0aW9uc1xuICovXG5leHBvcnRzLmRlZmF1bHQgPSB7XG4gIC8qKlxuICAgKiBDdXN0b20gY2xhc3MgdG8gYmUgYWRkZWQgdG8gdGhlIGAuY29sb3JwaWNrZXItZWxlbWVudGAgZWxlbWVudFxuICAgKlxuICAgKiBAdHlwZSB7U3RyaW5nfG51bGx9XG4gICAqIEBkZWZhdWx0IG51bGxcbiAgICovXG4gIGN1c3RvbUNsYXNzOiBudWxsLFxuICAvKipcbiAgICogU2V0cyBhIGluaXRpYWwgY29sb3IsIGlnbm9yaW5nIHRoZSBvbmUgZnJvbSB0aGUgZWxlbWVudC9pbnB1dCB2YWx1ZSBvciB0aGUgZGF0YS1jb2xvciBhdHRyaWJ1dGUuXG4gICAqXG4gICAqIEB0eXBlIHsoU3RyaW5nfENvbG9ySXRlbXxib29sZWFuKX1cbiAgICogQGRlZmF1bHQgZmFsc2VcbiAgICovXG4gIGNvbG9yOiBmYWxzZSxcbiAgLyoqXG4gICAqIEZhbGxiYWNrIGNvbG9yIHRvIHVzZSB3aGVuIHRoZSBnaXZlbiBjb2xvciBpcyBpbnZhbGlkLlxuICAgKiBJZiBmYWxzZSwgdGhlIGxhdGVzdCB2YWxpZCBjb2xvciB3aWxsIGJlIHVzZWQgYXMgYSBmYWxsYmFjay5cbiAgICpcbiAgICogQHR5cGUge1N0cmluZ3xDb2xvckl0ZW18Ym9vbGVhbn1cbiAgICogQGRlZmF1bHQgZmFsc2VcbiAgICovXG4gIGZhbGxiYWNrQ29sb3I6IGZhbHNlLFxuICAvKipcbiAgICogRm9yY2VzIGFuIHNwZWNpZmljIGNvbG9yIGZvcm1hdC4gSWYgJ2F1dG8nLCBpdCB3aWxsIGJlIGF1dG9tYXRpY2FsbHkgZGV0ZWN0ZWQgdGhlIGZpcnN0IHRpbWUgb25seSxcbiAgICogYnV0IGlmIG51bGwgaXQgd2lsbCBiZSBhbHdheXMgcmVjYWxjdWxhdGVkLlxuICAgKlxuICAgKiBOb3RlIHRoYXQgdGhlIGVuZGluZyAnYScgb2YgdGhlIGZvcm1hdCBtZWFuaW5nIFwiYWxwaGFcIiBoYXMgY3VycmVudGx5IG5vIGVmZmVjdCwgbWVhbmluZyB0aGF0IHJnYiBpcyB0aGUgc2FtZSBhc1xuICAgKiByZ2JhIGV4Y2VwdGluZyBpZiB0aGUgYWxwaGEgY2hhbm5lbCBpcyBkaXNhYmxlZCAoc2VlIHVzZUFscGhhKS5cbiAgICpcbiAgICogQHR5cGUgeygncmdiJ3wnaGV4J3wnaHNsJ3wnYXV0byd8bnVsbCl9XG4gICAqIEBkZWZhdWx0ICdhdXRvJ1xuICAgKi9cbiAgZm9ybWF0OiAnYXV0bycsXG4gIC8qKlxuICAgKiBIb3Jpem9udGFsIG1vZGUgbGF5b3V0LlxuICAgKlxuICAgKiBJZiB0cnVlLCB0aGUgaHVlIGFuZCBhbHBoYSBjaGFubmVsIGJhcnMgd2lsbCBiZSByZW5kZXJlZCBob3Jpem9udGFsbHksIGFib3ZlIHRoZSBzYXR1cmF0aW9uIHNlbGVjdG9yLlxuICAgKlxuICAgKiBAdHlwZSB7Ym9vbGVhbn1cbiAgICogQGRlZmF1bHQgZmFsc2VcbiAgICovXG4gIGhvcml6b250YWw6IGZhbHNlLFxuICAvKipcbiAgICogRm9yY2VzIHRvIHNob3cgdGhlIGNvbG9ycGlja2VyIGFzIGFuIGlubGluZSBlbGVtZW50LlxuICAgKlxuICAgKiBOb3RlIHRoYXQgaWYgdGhlcmUgaXMgbm8gY29udGFpbmVyIHNwZWNpZmllZCwgdGhlIGlubGluZSBlbGVtZW50XG4gICAqIHdpbGwgYmUgYWRkZWQgdG8gdGhlIGJvZHksIHNvIHlvdSBtYXkgd2FudCB0byBzZXQgdGhlIGNvbnRhaW5lciBvcHRpb24uXG4gICAqXG4gICAqIEB0eXBlIHtib29sZWFufVxuICAgKiBAZGVmYXVsdCBmYWxzZVxuICAgKi9cbiAgaW5saW5lOiBmYWxzZSxcbiAgLyoqXG4gICAqIENvbnRhaW5lciB3aGVyZSB0aGUgY29sb3JwaWNrZXIgaXMgYXBwZW5kZWQgdG8gaW4gdGhlIERPTS5cbiAgICpcbiAgICogSWYgaXMgYSBzdHJpbmcgKENTUyBzZWxlY3RvciksIHRoZSBjb2xvcnBpY2tlciB3aWxsIGJlIHBsYWNlZCBpbnNpZGUgdGhpcyBjb250YWluZXIuXG4gICAqIElmIHRydWUsIHRoZSBgLmNvbG9ycGlja2VyLWVsZW1lbnRgIGVsZW1lbnQgaXRzZWxmIHdpbGwgYmUgdXNlZCBhcyB0aGUgY29udGFpbmVyLlxuICAgKiBJZiBmYWxzZSwgdGhlIGRvY3VtZW50IGJvZHkgaXMgdXNlZCBhcyB0aGUgY29udGFpbmVyLCB1bmxlc3MgaXQgaXMgYSBwb3BvdmVyIChpbiB0aGlzIGNhc2UgaXQgaXMgYXBwZW5kZWQgdG8gdGhlXG4gICAqIHBvcG92ZXIgYm9keSBpbnN0ZWFkKS5cbiAgICpcbiAgICogQHR5cGUge1N0cmluZ3xib29sZWFufVxuICAgKiBAZGVmYXVsdCBmYWxzZVxuICAgKi9cbiAgY29udGFpbmVyOiBmYWxzZSxcbiAgLyoqXG4gICAqIEJvb3RzdHJhcCBQb3BvdmVyIG9wdGlvbnMuXG4gICAqIFRoZSB0cmlnZ2VyLCBjb250ZW50IGFuZCBodG1sIG9wdGlvbnMgYXJlIGFsd2F5cyBpZ25vcmVkLlxuICAgKlxuICAgKiBAdHlwZSB7Ym9vbGVhbn1cbiAgICogQGRlZmF1bHQgT2JqZWN0XG4gICAqL1xuICBwb3BvdmVyOiB7XG4gICAgYW5pbWF0aW9uOiB0cnVlLFxuICAgIHBsYWNlbWVudDogJ2JvdHRvbScsXG4gICAgZmFsbGJhY2tQbGFjZW1lbnQ6ICdmbGlwJ1xuICB9LFxuICAvKipcbiAgICogSWYgdHJ1ZSwgbG9hZHMgdGhlICdkZWJ1Z2dlcicgZXh0ZW5zaW9uIGF1dG9tYXRpY2FsbHksIHdoaWNoIGxvZ3MgdGhlIGV2ZW50cyBpbiB0aGUgY29uc29sZVxuICAgKiBAdHlwZSB7Ym9vbGVhbn1cbiAgICogQGRlZmF1bHQgZmFsc2VcbiAgICovXG4gIGRlYnVnOiBmYWxzZSxcbiAgLyoqXG4gICAqIENoaWxkIENTUyBzZWxlY3RvciBmb3IgdGhlIGNvbG9ycGlja2VyIGlucHV0LlxuICAgKlxuICAgKiBAdHlwZSB7U3RyaW5nfVxuICAgKiBAZGVmYXVsdCAnaW5wdXQnXG4gICAqL1xuICBpbnB1dDogJ2lucHV0JyxcbiAgLyoqXG4gICAqIENoaWxkIENTUyBzZWxlY3RvciBmb3IgdGhlIGNvbG9ycGlja2VyIGFkZG9uLlxuICAgKiBJZiBpdCBleGlzdHMsIHRoZSBjaGlsZCA8aT4gZWxlbWVudCBiYWNrZ3JvdW5kIHdpbGwgYmUgY2hhbmdlZCBvbiBjb2xvciBjaGFuZ2UuXG4gICAqXG4gICAqIEB0eXBlIHtTdHJpbmd9XG4gICAqIEBkZWZhdWx0ICcuY29sb3JwaWNrZXItdHJpZ2dlciwgLmNvbG9ycGlja2VyLWlucHV0LWFkZG9uJ1xuICAgKi9cbiAgYWRkb246ICcuY29sb3JwaWNrZXItaW5wdXQtYWRkb24nLFxuICAvKipcbiAgICogSWYgdHJ1ZSwgdGhlIGlucHV0IGNvbnRlbnQgd2lsbCBiZSByZXBsYWNlZCBhbHdheXMgd2l0aCBhIHZhbGlkIGNvbG9yLFxuICAgKiBpZiBmYWxzZSwgdGhlIGludmFsaWQgY29sb3Igd2lsbCBiZSBsZWZ0IGluIHRoZSBpbnB1dCxcbiAgICogICB3aGlsZSB0aGUgaW50ZXJuYWwgY29sb3Igb2JqZWN0IHdpbGwgc3RpbGwgcmVzb2x2ZSBpbnRvIGEgdmFsaWQgb25lLlxuICAgKlxuICAgKiBAdHlwZSB7Ym9vbGVhbn1cbiAgICogQGRlZmF1bHQgdHJ1ZVxuICAgKi9cbiAgYXV0b0lucHV0RmFsbGJhY2s6IHRydWUsXG4gIC8qKlxuICAgKiBJZiB0cnVlIGEgaGFzaCB3aWxsIGJlIHByZXBlbmRlZCB0byBoZXhhZGVjaW1hbCBjb2xvcnMuXG4gICAqIElmIGZhbHNlLCB0aGUgaGFzaCB3aWxsIGJlIHJlbW92ZWQuXG4gICAqIFRoaXMgb25seSBhZmZlY3RzIHRoZSBpbnB1dCB2YWx1ZXMgaW4gaGV4YWRlY2ltYWwgZm9ybWF0LlxuICAgKlxuICAgKiBAdHlwZSB7Ym9vbGVhbn1cbiAgICogQGRlZmF1bHQgdHJ1ZVxuICAgKi9cbiAgdXNlSGFzaFByZWZpeDogdHJ1ZSxcbiAgLyoqXG4gICAqIElmIHRydWUsIHRoZSBhbHBoYSBjaGFubmVsIGJhciB3aWxsIGJlIGRpc3BsYXllZCBubyBtYXR0ZXIgd2hhdC5cbiAgICpcbiAgICogSWYgZmFsc2UsIGl0IHdpbGwgYmUgYWx3YXlzIGhpZGRlbiBhbmQgYWxwaGEgY2hhbm5lbCB3aWxsIGJlIGRpc2FibGVkIGFsc28gcHJvZ3JhbW1hdGljYWxseSwgbWVhbmluZyB0aGF0XG4gICAqIHRoZSBzZWxlY3RlZCBvciB0eXBlZCBjb2xvciB3aWxsIGJlIGFsd2F5cyBvcGFxdWUuXG4gICAqXG4gICAqIElmIG51bGwsIHRoZSBhbHBoYSBjaGFubmVsIHdpbGwgYmUgYXV0b21hdGljYWxseSBkaXNhYmxlZC9lbmFibGVkIGRlcGVuZGluZyBpZiB0aGUgaW5pdGlhbCBjb2xvciBmb3JtYXQgc3VwcG9ydHNcbiAgICogYWxwaGEgb3Igbm90LlxuICAgKlxuICAgKiBAdHlwZSB7Ym9vbGVhbn1cbiAgICogQGRlZmF1bHQgdHJ1ZVxuICAgKi9cbiAgdXNlQWxwaGE6IHRydWUsXG4gIC8qKlxuICAgKiBDb2xvcnBpY2tlciB3aWRnZXQgdGVtcGxhdGVcbiAgICogQHR5cGUge1N0cmluZ31cbiAgICogQGV4YW1wbGVcbiAgICogPCEtLSBUaGlzIGlzIHRoZSBkZWZhdWx0IHRlbXBsYXRlOiAtLT5cbiAgICogPGRpdiBjbGFzcz1cImNvbG9ycGlja2VyXCI+XG4gICAqICAgPGRpdiBjbGFzcz1cImNvbG9ycGlja2VyLXNhdHVyYXRpb25cIj48aSBjbGFzcz1cImNvbG9ycGlja2VyLWd1aWRlXCI+PC9pPjwvZGl2PlxuICAgKiAgIDxkaXYgY2xhc3M9XCJjb2xvcnBpY2tlci1odWVcIj48aSBjbGFzcz1cImNvbG9ycGlja2VyLWd1aWRlXCI+PC9pPjwvZGl2PlxuICAgKiAgIDxkaXYgY2xhc3M9XCJjb2xvcnBpY2tlci1hbHBoYVwiPlxuICAgKiAgICAgPGRpdiBjbGFzcz1cImNvbG9ycGlja2VyLWFscGhhLWNvbG9yXCI+PC9kaXY+XG4gICAqICAgICA8aSBjbGFzcz1cImNvbG9ycGlja2VyLWd1aWRlXCI+PC9pPlxuICAgKiAgIDwvZGl2PlxuICAgKiA8L2Rpdj5cbiAgICovXG4gIHRlbXBsYXRlOiAnPGRpdiBjbGFzcz1cImNvbG9ycGlja2VyXCI+XFxuICAgICAgPGRpdiBjbGFzcz1cImNvbG9ycGlja2VyLXNhdHVyYXRpb25cIj48aSBjbGFzcz1cImNvbG9ycGlja2VyLWd1aWRlXCI+PC9pPjwvZGl2PlxcbiAgICAgIDxkaXYgY2xhc3M9XCJjb2xvcnBpY2tlci1odWVcIj48aSBjbGFzcz1cImNvbG9ycGlja2VyLWd1aWRlXCI+PC9pPjwvZGl2PlxcbiAgICAgIDxkaXYgY2xhc3M9XCJjb2xvcnBpY2tlci1hbHBoYVwiPlxcbiAgICAgICAgPGRpdiBjbGFzcz1cImNvbG9ycGlja2VyLWFscGhhLWNvbG9yXCI+PC9kaXY+XFxuICAgICAgICA8aSBjbGFzcz1cImNvbG9ycGlja2VyLWd1aWRlXCI+PC9pPlxcbiAgICAgIDwvZGl2PlxcbiAgICA8L2Rpdj4nLFxuICAvKipcbiAgICpcbiAgICogQXNzb2NpYXRpdmUgb2JqZWN0IHdpdGggdGhlIGV4dGVuc2lvbiBjbGFzcyBuYW1lIGFuZCBpdHMgY29uZmlnLlxuICAgKiBDb2xvcnBpY2tlciBjb21lcyB3aXRoIG1hbnkgYnVuZGxlZCBleHRlbnNpb25zOiBkZWJ1Z2dlciwgcGFsZXR0ZSwgcHJldmlldyBhbmQgc3dhdGNoZXMgKGEgc3VwZXJzZXQgb2YgcGFsZXR0ZSkuXG4gICAqXG4gICAqIEB0eXBlIHtPYmplY3RbXX1cbiAgICogQGV4YW1wbGVcbiAgICogICBleHRlbnNpb25zOiBbXG4gICAqICAgICB7XG4gICAqICAgICAgIG5hbWU6ICdzd2F0Y2hlcydcbiAgICogICAgICAgb3B0aW9uczoge1xuICAgKiAgICAgICAgIGNvbG9yczoge1xuICAgKiAgICAgICAgICAgJ3ByaW1hcnknOiAnIzMzN2FiNycsXG4gICAqICAgICAgICAgICAnc3VjY2Vzcyc6ICcjNWNiODVjJyxcbiAgICogICAgICAgICAgICdpbmZvJzogJyM1YmMwZGUnLFxuICAgKiAgICAgICAgICAgJ3dhcm5pbmcnOiAnI2YwYWQ0ZScsXG4gICAqICAgICAgICAgICAnZGFuZ2VyJzogJyNkOTUzNGYnXG4gICAqICAgICAgICAgfSxcbiAgICogICAgICAgICBuYW1lc0FzVmFsdWVzOiB0cnVlXG4gICAqICAgICAgIH1cbiAgICogICAgIH1cbiAgICogICBdXG4gICAqL1xuICBleHRlbnNpb25zOiBbe1xuICAgIG5hbWU6ICdwcmV2aWV3JyxcbiAgICBvcHRpb25zOiB7XG4gICAgICBzaG93VGV4dDogdHJ1ZVxuICAgIH1cbiAgfV0sXG4gIC8qKlxuICAgKiBWZXJ0aWNhbCBzbGlkZXJzIGNvbmZpZ3VyYXRpb25cbiAgICogQHR5cGUge09iamVjdH1cbiAgICovXG4gIHNsaWRlcnM6IHtcbiAgICBzYXR1cmF0aW9uOiB7XG4gICAgICBzZWxlY3RvcjogJy5jb2xvcnBpY2tlci1zYXR1cmF0aW9uJyxcbiAgICAgIG1heExlZnQ6IHNsaWRlclNpemUsXG4gICAgICBtYXhUb3A6IHNsaWRlclNpemUsXG4gICAgICBjYWxsTGVmdDogJ3NldFNhdHVyYXRpb25SYXRpbycsXG4gICAgICBjYWxsVG9wOiAnc2V0VmFsdWVSYXRpbydcbiAgICB9LFxuICAgIGh1ZToge1xuICAgICAgc2VsZWN0b3I6ICcuY29sb3JwaWNrZXItaHVlJyxcbiAgICAgIG1heExlZnQ6IDAsXG4gICAgICBtYXhUb3A6IHNsaWRlclNpemUsXG4gICAgICBjYWxsTGVmdDogZmFsc2UsXG4gICAgICBjYWxsVG9wOiAnc2V0SHVlUmF0aW8nXG4gICAgfSxcbiAgICBhbHBoYToge1xuICAgICAgc2VsZWN0b3I6ICcuY29sb3JwaWNrZXItYWxwaGEnLFxuICAgICAgY2hpbGRTZWxlY3RvcjogJy5jb2xvcnBpY2tlci1hbHBoYS1jb2xvcicsXG4gICAgICBtYXhMZWZ0OiAwLFxuICAgICAgbWF4VG9wOiBzbGlkZXJTaXplLFxuICAgICAgY2FsbExlZnQ6IGZhbHNlLFxuICAgICAgY2FsbFRvcDogJ3NldEFscGhhUmF0aW8nXG4gICAgfVxuICB9LFxuICAvKipcbiAgICogSG9yaXpvbnRhbCBzbGlkZXJzIGNvbmZpZ3VyYXRpb25cbiAgICogQHR5cGUge09iamVjdH1cbiAgICovXG4gIHNsaWRlcnNIb3J6OiB7XG4gICAgc2F0dXJhdGlvbjoge1xuICAgICAgc2VsZWN0b3I6ICcuY29sb3JwaWNrZXItc2F0dXJhdGlvbicsXG4gICAgICBtYXhMZWZ0OiBzbGlkZXJTaXplLFxuICAgICAgbWF4VG9wOiBzbGlkZXJTaXplLFxuICAgICAgY2FsbExlZnQ6ICdzZXRTYXR1cmF0aW9uUmF0aW8nLFxuICAgICAgY2FsbFRvcDogJ3NldFZhbHVlUmF0aW8nXG4gICAgfSxcbiAgICBodWU6IHtcbiAgICAgIHNlbGVjdG9yOiAnLmNvbG9ycGlja2VyLWh1ZScsXG4gICAgICBtYXhMZWZ0OiBzbGlkZXJTaXplLFxuICAgICAgbWF4VG9wOiAwLFxuICAgICAgY2FsbExlZnQ6ICdzZXRIdWVSYXRpbycsXG4gICAgICBjYWxsVG9wOiBmYWxzZVxuICAgIH0sXG4gICAgYWxwaGE6IHtcbiAgICAgIHNlbGVjdG9yOiAnLmNvbG9ycGlja2VyLWFscGhhJyxcbiAgICAgIGNoaWxkU2VsZWN0b3I6ICcuY29sb3JwaWNrZXItYWxwaGEtY29sb3InLFxuICAgICAgbWF4TGVmdDogc2xpZGVyU2l6ZSxcbiAgICAgIG1heFRvcDogMCxcbiAgICAgIGNhbGxMZWZ0OiAnc2V0QWxwaGFSYXRpbycsXG4gICAgICBjYWxsVG9wOiBmYWxzZVxuICAgIH1cbiAgfVxufTtcbm1vZHVsZS5leHBvcnRzID0gZXhwb3J0cy5kZWZhdWx0O1xuXG4vKioqLyB9KSxcbi8qIDQgKi9cbi8qKiovIChmdW5jdGlvbihtb2R1bGUsIGV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pIHtcblxuXCJ1c2Ugc3RyaWN0XCI7XG5cblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF90eXBlb2YgPSB0eXBlb2YgU3ltYm9sID09PSBcImZ1bmN0aW9uXCIgJiYgdHlwZW9mIFN5bWJvbC5pdGVyYXRvciA9PT0gXCJzeW1ib2xcIiA/IGZ1bmN0aW9uIChvYmopIHsgcmV0dXJuIHR5cGVvZiBvYmo7IH0gOiBmdW5jdGlvbiAob2JqKSB7IHJldHVybiBvYmogJiYgdHlwZW9mIFN5bWJvbCA9PT0gXCJmdW5jdGlvblwiICYmIG9iai5jb25zdHJ1Y3RvciA9PT0gU3ltYm9sICYmIG9iaiAhPT0gU3ltYm9sLnByb3RvdHlwZSA/IFwic3ltYm9sXCIgOiB0eXBlb2Ygb2JqOyB9O1xuXG52YXIgX2NyZWF0ZUNsYXNzID0gZnVuY3Rpb24gKCkgeyBmdW5jdGlvbiBkZWZpbmVQcm9wZXJ0aWVzKHRhcmdldCwgcHJvcHMpIHsgZm9yICh2YXIgaSA9IDA7IGkgPCBwcm9wcy5sZW5ndGg7IGkrKykgeyB2YXIgZGVzY3JpcHRvciA9IHByb3BzW2ldOyBkZXNjcmlwdG9yLmVudW1lcmFibGUgPSBkZXNjcmlwdG9yLmVudW1lcmFibGUgfHwgZmFsc2U7IGRlc2NyaXB0b3IuY29uZmlndXJhYmxlID0gdHJ1ZTsgaWYgKFwidmFsdWVcIiBpbiBkZXNjcmlwdG9yKSBkZXNjcmlwdG9yLndyaXRhYmxlID0gdHJ1ZTsgT2JqZWN0LmRlZmluZVByb3BlcnR5KHRhcmdldCwgZGVzY3JpcHRvci5rZXksIGRlc2NyaXB0b3IpOyB9IH0gcmV0dXJuIGZ1bmN0aW9uIChDb25zdHJ1Y3RvciwgcHJvdG9Qcm9wcywgc3RhdGljUHJvcHMpIHsgaWYgKHByb3RvUHJvcHMpIGRlZmluZVByb3BlcnRpZXMoQ29uc3RydWN0b3IucHJvdG90eXBlLCBwcm90b1Byb3BzKTsgaWYgKHN0YXRpY1Byb3BzKSBkZWZpbmVQcm9wZXJ0aWVzKENvbnN0cnVjdG9yLCBzdGF0aWNQcm9wcyk7IHJldHVybiBDb25zdHJ1Y3RvcjsgfTsgfSgpO1xuXG52YXIgX0V4dGVuc2lvbjIgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKDEpO1xuXG52YXIgX0V4dGVuc2lvbjMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9FeHRlbnNpb24yKTtcblxudmFyIF9qcXVlcnkgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKDApO1xuXG52YXIgX2pxdWVyeTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9qcXVlcnkpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG5mdW5jdGlvbiBfY2xhc3NDYWxsQ2hlY2soaW5zdGFuY2UsIENvbnN0cnVjdG9yKSB7IGlmICghKGluc3RhbmNlIGluc3RhbmNlb2YgQ29uc3RydWN0b3IpKSB7IHRocm93IG5ldyBUeXBlRXJyb3IoXCJDYW5ub3QgY2FsbCBhIGNsYXNzIGFzIGEgZnVuY3Rpb25cIik7IH0gfVxuXG5mdW5jdGlvbiBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybihzZWxmLCBjYWxsKSB7IGlmICghc2VsZikgeyB0aHJvdyBuZXcgUmVmZXJlbmNlRXJyb3IoXCJ0aGlzIGhhc24ndCBiZWVuIGluaXRpYWxpc2VkIC0gc3VwZXIoKSBoYXNuJ3QgYmVlbiBjYWxsZWRcIik7IH0gcmV0dXJuIGNhbGwgJiYgKHR5cGVvZiBjYWxsID09PSBcIm9iamVjdFwiIHx8IHR5cGVvZiBjYWxsID09PSBcImZ1bmN0aW9uXCIpID8gY2FsbCA6IHNlbGY7IH1cblxuZnVuY3Rpb24gX2luaGVyaXRzKHN1YkNsYXNzLCBzdXBlckNsYXNzKSB7IGlmICh0eXBlb2Ygc3VwZXJDbGFzcyAhPT0gXCJmdW5jdGlvblwiICYmIHN1cGVyQ2xhc3MgIT09IG51bGwpIHsgdGhyb3cgbmV3IFR5cGVFcnJvcihcIlN1cGVyIGV4cHJlc3Npb24gbXVzdCBlaXRoZXIgYmUgbnVsbCBvciBhIGZ1bmN0aW9uLCBub3QgXCIgKyB0eXBlb2Ygc3VwZXJDbGFzcyk7IH0gc3ViQ2xhc3MucHJvdG90eXBlID0gT2JqZWN0LmNyZWF0ZShzdXBlckNsYXNzICYmIHN1cGVyQ2xhc3MucHJvdG90eXBlLCB7IGNvbnN0cnVjdG9yOiB7IHZhbHVlOiBzdWJDbGFzcywgZW51bWVyYWJsZTogZmFsc2UsIHdyaXRhYmxlOiB0cnVlLCBjb25maWd1cmFibGU6IHRydWUgfSB9KTsgaWYgKHN1cGVyQ2xhc3MpIE9iamVjdC5zZXRQcm90b3R5cGVPZiA/IE9iamVjdC5zZXRQcm90b3R5cGVPZihzdWJDbGFzcywgc3VwZXJDbGFzcykgOiBzdWJDbGFzcy5fX3Byb3RvX18gPSBzdXBlckNsYXNzOyB9XG5cbnZhciBkZWZhdWx0cyA9IHtcbiAgLyoqXG4gICAqIEtleS12YWx1ZSBwYWlycyBkZWZpbmluZyBhIGNvbG9yIGFsaWFzIGFuZCBpdHMgQ1NTIGNvbG9yIHJlcHJlc2VudGF0aW9uLlxuICAgKlxuICAgKiBUaGV5IGNhbiBhbHNvIGJlIGp1c3QgYW4gYXJyYXkgb2YgdmFsdWVzLiBJbiB0aGF0IGNhc2UsIG5vIHNwZWNpYWwgbmFtZXMgYXJlIHVzZWQsIG9ubHkgdGhlIHJlYWwgY29sb3JzLlxuICAgKlxuICAgKiBAdHlwZSB7T2JqZWN0fEFycmF5fVxuICAgKiBAZGVmYXVsdCBudWxsXG4gICAqIEBleGFtcGxlXG4gICAqICB7XG4gICAqICAgJ2JsYWNrJzogJyMwMDAwMDAnLFxuICAgKiAgICd3aGl0ZSc6ICcjZmZmZmZmJyxcbiAgICogICAncmVkJzogJyNGRjAwMDAnLFxuICAgKiAgICdkZWZhdWx0JzogJyM3Nzc3NzcnLFxuICAgKiAgICdwcmltYXJ5JzogJyMzMzdhYjcnLFxuICAgKiAgICdzdWNjZXNzJzogJyM1Y2I4NWMnLFxuICAgKiAgICdpbmZvJzogJyM1YmMwZGUnLFxuICAgKiAgICd3YXJuaW5nJzogJyNmMGFkNGUnLFxuICAgKiAgICdkYW5nZXInOiAnI2Q5NTM0ZidcbiAgICogIH1cbiAgICpcbiAgICogQGV4YW1wbGUgWycjZjBhZDRlJywgJyMzMzdhYjcnLCAnIzVjYjg1YyddXG4gICAqL1xuICBjb2xvcnM6IG51bGwsXG4gIC8qKlxuICAgKiBJZiB0cnVlLCB3aGVuIGEgY29sb3Igc3dhdGNoIGlzIHNlbGVjdGVkIHRoZSBuYW1lIChhbGlhcykgd2lsbCBiZSB1c2VkIGFzIGlucHV0IHZhbHVlLFxuICAgKiBvdGhlcndpc2UgdGhlIHN3YXRjaCByZWFsIGNvbG9yIHZhbHVlIHdpbGwgYmUgdXNlZC5cbiAgICpcbiAgICogQHR5cGUge2Jvb2xlYW59XG4gICAqIEBkZWZhdWx0IHRydWVcbiAgICovXG4gIG5hbWVzQXNWYWx1ZXM6IHRydWVcbn07XG5cbi8qKlxuICogUGFsZXR0ZSBleHRlbnNpb25cbiAqIEBpZ25vcmVcbiAqL1xuXG52YXIgUGFsZXR0ZSA9IGZ1bmN0aW9uIChfRXh0ZW5zaW9uKSB7XG4gIF9pbmhlcml0cyhQYWxldHRlLCBfRXh0ZW5zaW9uKTtcblxuICBfY3JlYXRlQ2xhc3MoUGFsZXR0ZSwgW3tcbiAgICBrZXk6ICdjb2xvcnMnLFxuXG5cbiAgICAvKipcbiAgICAgKiBAcmV0dXJucyB7T2JqZWN0fEFycmF5fVxuICAgICAqL1xuICAgIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgICAgcmV0dXJuIHRoaXMub3B0aW9ucy5jb2xvcnM7XG4gICAgfVxuICB9XSk7XG5cbiAgZnVuY3Rpb24gUGFsZXR0ZShjb2xvcnBpY2tlcikge1xuICAgIHZhciBvcHRpb25zID0gYXJndW1lbnRzLmxlbmd0aCA+IDEgJiYgYXJndW1lbnRzWzFdICE9PSB1bmRlZmluZWQgPyBhcmd1bWVudHNbMV0gOiB7fTtcblxuICAgIF9jbGFzc0NhbGxDaGVjayh0aGlzLCBQYWxldHRlKTtcblxuICAgIHZhciBfdGhpcyA9IF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuKHRoaXMsIChQYWxldHRlLl9fcHJvdG9fXyB8fCBPYmplY3QuZ2V0UHJvdG90eXBlT2YoUGFsZXR0ZSkpLmNhbGwodGhpcywgY29sb3JwaWNrZXIsIF9qcXVlcnkyLmRlZmF1bHQuZXh0ZW5kKHRydWUsIHt9LCBkZWZhdWx0cywgb3B0aW9ucykpKTtcblxuICAgIGlmICghQXJyYXkuaXNBcnJheShfdGhpcy5vcHRpb25zLmNvbG9ycykgJiYgX3R5cGVvZihfdGhpcy5vcHRpb25zLmNvbG9ycykgIT09ICdvYmplY3QnKSB7XG4gICAgICBfdGhpcy5vcHRpb25zLmNvbG9ycyA9IG51bGw7XG4gICAgfVxuICAgIHJldHVybiBfdGhpcztcbiAgfVxuXG4gIC8qKlxuICAgKiBAcmV0dXJucyB7aW50fVxuICAgKi9cblxuXG4gIF9jcmVhdGVDbGFzcyhQYWxldHRlLCBbe1xuICAgIGtleTogJ2dldExlbmd0aCcsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGdldExlbmd0aCgpIHtcbiAgICAgIGlmICghdGhpcy5vcHRpb25zLmNvbG9ycykge1xuICAgICAgICByZXR1cm4gMDtcbiAgICAgIH1cblxuICAgICAgaWYgKEFycmF5LmlzQXJyYXkodGhpcy5vcHRpb25zLmNvbG9ycykpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMub3B0aW9ucy5jb2xvcnMubGVuZ3RoO1xuICAgICAgfVxuXG4gICAgICBpZiAoX3R5cGVvZih0aGlzLm9wdGlvbnMuY29sb3JzKSA9PT0gJ29iamVjdCcpIHtcbiAgICAgICAgcmV0dXJuIE9iamVjdC5rZXlzKHRoaXMub3B0aW9ucy5jb2xvcnMpLmxlbmd0aDtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIDA7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAncmVzb2x2ZUNvbG9yJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gcmVzb2x2ZUNvbG9yKGNvbG9yKSB7XG4gICAgICB2YXIgcmVhbENvbG9yID0gYXJndW1lbnRzLmxlbmd0aCA+IDEgJiYgYXJndW1lbnRzWzFdICE9PSB1bmRlZmluZWQgPyBhcmd1bWVudHNbMV0gOiB0cnVlO1xuXG4gICAgICBpZiAodGhpcy5nZXRMZW5ndGgoKSA8PSAwKSB7XG4gICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgIH1cblxuICAgICAgLy8gQXJyYXkgb2YgY29sb3JzXG4gICAgICBpZiAoQXJyYXkuaXNBcnJheSh0aGlzLm9wdGlvbnMuY29sb3JzKSkge1xuICAgICAgICBpZiAodGhpcy5vcHRpb25zLmNvbG9ycy5pbmRleE9mKGNvbG9yKSA+PSAwKSB7XG4gICAgICAgICAgcmV0dXJuIGNvbG9yO1xuICAgICAgICB9XG4gICAgICAgIGlmICh0aGlzLm9wdGlvbnMuY29sb3JzLmluZGV4T2YoY29sb3IudG9VcHBlckNhc2UoKSkgPj0gMCkge1xuICAgICAgICAgIHJldHVybiBjb2xvci50b1VwcGVyQ2FzZSgpO1xuICAgICAgICB9XG4gICAgICAgIGlmICh0aGlzLm9wdGlvbnMuY29sb3JzLmluZGV4T2YoY29sb3IudG9Mb3dlckNhc2UoKSkgPj0gMCkge1xuICAgICAgICAgIHJldHVybiBjb2xvci50b0xvd2VyQ2FzZSgpO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgIH1cblxuICAgICAgaWYgKF90eXBlb2YodGhpcy5vcHRpb25zLmNvbG9ycykgIT09ICdvYmplY3QnKSB7XG4gICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgIH1cblxuICAgICAgLy8gTWFwIG9mIG9iamVjdHNcbiAgICAgIGlmICghdGhpcy5vcHRpb25zLm5hbWVzQXNWYWx1ZXMgfHwgcmVhbENvbG9yKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmdldFZhbHVlKGNvbG9yLCBmYWxzZSk7XG4gICAgICB9XG4gICAgICByZXR1cm4gdGhpcy5nZXROYW1lKGNvbG9yLCB0aGlzLmdldE5hbWUoJyMnICsgY29sb3IpKTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBHaXZlbiBhIGNvbG9yIHZhbHVlLCByZXR1cm5zIHRoZSBjb3JyZXNwb25kaW5nIGNvbG9yIG5hbWUgb3IgZGVmYXVsdFZhbHVlLlxuICAgICAqXG4gICAgICogQHBhcmFtIHtTdHJpbmd9IHZhbHVlXG4gICAgICogQHBhcmFtIHsqfSBkZWZhdWx0VmFsdWVcbiAgICAgKiBAcmV0dXJucyB7Kn1cbiAgICAgKi9cblxuICB9LCB7XG4gICAga2V5OiAnZ2V0TmFtZScsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGdldE5hbWUodmFsdWUpIHtcbiAgICAgIHZhciBkZWZhdWx0VmFsdWUgPSBhcmd1bWVudHMubGVuZ3RoID4gMSAmJiBhcmd1bWVudHNbMV0gIT09IHVuZGVmaW5lZCA/IGFyZ3VtZW50c1sxXSA6IGZhbHNlO1xuXG4gICAgICBpZiAoISh0eXBlb2YgdmFsdWUgPT09ICdzdHJpbmcnKSB8fCAhdGhpcy5vcHRpb25zLmNvbG9ycykge1xuICAgICAgICByZXR1cm4gZGVmYXVsdFZhbHVlO1xuICAgICAgfVxuICAgICAgZm9yICh2YXIgbmFtZSBpbiB0aGlzLm9wdGlvbnMuY29sb3JzKSB7XG4gICAgICAgIGlmICghdGhpcy5vcHRpb25zLmNvbG9ycy5oYXNPd25Qcm9wZXJ0eShuYW1lKSkge1xuICAgICAgICAgIGNvbnRpbnVlO1xuICAgICAgICB9XG4gICAgICAgIGlmICh0aGlzLm9wdGlvbnMuY29sb3JzW25hbWVdLnRvTG93ZXJDYXNlKCkgPT09IHZhbHVlLnRvTG93ZXJDYXNlKCkpIHtcbiAgICAgICAgICByZXR1cm4gbmFtZTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgICAgcmV0dXJuIGRlZmF1bHRWYWx1ZTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBHaXZlbiBhIGNvbG9yIG5hbWUsIHJldHVybnMgdGhlIGNvcnJlc3BvbmRpbmcgY29sb3IgdmFsdWUgb3IgZGVmYXVsdFZhbHVlLlxuICAgICAqXG4gICAgICogQHBhcmFtIHtTdHJpbmd9IG5hbWVcbiAgICAgKiBAcGFyYW0geyp9IGRlZmF1bHRWYWx1ZVxuICAgICAqIEByZXR1cm5zIHsqfVxuICAgICAqL1xuXG4gIH0sIHtcbiAgICBrZXk6ICdnZXRWYWx1ZScsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGdldFZhbHVlKG5hbWUpIHtcbiAgICAgIHZhciBkZWZhdWx0VmFsdWUgPSBhcmd1bWVudHMubGVuZ3RoID4gMSAmJiBhcmd1bWVudHNbMV0gIT09IHVuZGVmaW5lZCA/IGFyZ3VtZW50c1sxXSA6IGZhbHNlO1xuXG4gICAgICBpZiAoISh0eXBlb2YgbmFtZSA9PT0gJ3N0cmluZycpIHx8ICF0aGlzLm9wdGlvbnMuY29sb3JzKSB7XG4gICAgICAgIHJldHVybiBkZWZhdWx0VmFsdWU7XG4gICAgICB9XG4gICAgICBpZiAodGhpcy5vcHRpb25zLmNvbG9ycy5oYXNPd25Qcm9wZXJ0eShuYW1lKSkge1xuICAgICAgICByZXR1cm4gdGhpcy5vcHRpb25zLmNvbG9yc1tuYW1lXTtcbiAgICAgIH1cbiAgICAgIHJldHVybiBkZWZhdWx0VmFsdWU7XG4gICAgfVxuICB9XSk7XG5cbiAgcmV0dXJuIFBhbGV0dGU7XG59KF9FeHRlbnNpb24zLmRlZmF1bHQpO1xuXG5leHBvcnRzLmRlZmF1bHQgPSBQYWxldHRlO1xubW9kdWxlLmV4cG9ydHMgPSBleHBvcnRzLmRlZmF1bHQ7XG5cbi8qKiovIH0pLFxuLyogNSAqL1xuLyoqKi8gKGZ1bmN0aW9uKG1vZHVsZSwgZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXykge1xuXG5cInVzZSBzdHJpY3RcIjtcblxyXG5cclxubW9kdWxlLmV4cG9ydHMgPSB7XHJcblx0XCJhbGljZWJsdWVcIjogWzI0MCwgMjQ4LCAyNTVdLFxyXG5cdFwiYW50aXF1ZXdoaXRlXCI6IFsyNTAsIDIzNSwgMjE1XSxcclxuXHRcImFxdWFcIjogWzAsIDI1NSwgMjU1XSxcclxuXHRcImFxdWFtYXJpbmVcIjogWzEyNywgMjU1LCAyMTJdLFxyXG5cdFwiYXp1cmVcIjogWzI0MCwgMjU1LCAyNTVdLFxyXG5cdFwiYmVpZ2VcIjogWzI0NSwgMjQ1LCAyMjBdLFxyXG5cdFwiYmlzcXVlXCI6IFsyNTUsIDIyOCwgMTk2XSxcclxuXHRcImJsYWNrXCI6IFswLCAwLCAwXSxcclxuXHRcImJsYW5jaGVkYWxtb25kXCI6IFsyNTUsIDIzNSwgMjA1XSxcclxuXHRcImJsdWVcIjogWzAsIDAsIDI1NV0sXHJcblx0XCJibHVldmlvbGV0XCI6IFsxMzgsIDQzLCAyMjZdLFxyXG5cdFwiYnJvd25cIjogWzE2NSwgNDIsIDQyXSxcclxuXHRcImJ1cmx5d29vZFwiOiBbMjIyLCAxODQsIDEzNV0sXHJcblx0XCJjYWRldGJsdWVcIjogWzk1LCAxNTgsIDE2MF0sXHJcblx0XCJjaGFydHJldXNlXCI6IFsxMjcsIDI1NSwgMF0sXHJcblx0XCJjaG9jb2xhdGVcIjogWzIxMCwgMTA1LCAzMF0sXHJcblx0XCJjb3JhbFwiOiBbMjU1LCAxMjcsIDgwXSxcclxuXHRcImNvcm5mbG93ZXJibHVlXCI6IFsxMDAsIDE0OSwgMjM3XSxcclxuXHRcImNvcm5zaWxrXCI6IFsyNTUsIDI0OCwgMjIwXSxcclxuXHRcImNyaW1zb25cIjogWzIyMCwgMjAsIDYwXSxcclxuXHRcImN5YW5cIjogWzAsIDI1NSwgMjU1XSxcclxuXHRcImRhcmtibHVlXCI6IFswLCAwLCAxMzldLFxyXG5cdFwiZGFya2N5YW5cIjogWzAsIDEzOSwgMTM5XSxcclxuXHRcImRhcmtnb2xkZW5yb2RcIjogWzE4NCwgMTM0LCAxMV0sXHJcblx0XCJkYXJrZ3JheVwiOiBbMTY5LCAxNjksIDE2OV0sXHJcblx0XCJkYXJrZ3JlZW5cIjogWzAsIDEwMCwgMF0sXHJcblx0XCJkYXJrZ3JleVwiOiBbMTY5LCAxNjksIDE2OV0sXHJcblx0XCJkYXJra2hha2lcIjogWzE4OSwgMTgzLCAxMDddLFxyXG5cdFwiZGFya21hZ2VudGFcIjogWzEzOSwgMCwgMTM5XSxcclxuXHRcImRhcmtvbGl2ZWdyZWVuXCI6IFs4NSwgMTA3LCA0N10sXHJcblx0XCJkYXJrb3JhbmdlXCI6IFsyNTUsIDE0MCwgMF0sXHJcblx0XCJkYXJrb3JjaGlkXCI6IFsxNTMsIDUwLCAyMDRdLFxyXG5cdFwiZGFya3JlZFwiOiBbMTM5LCAwLCAwXSxcclxuXHRcImRhcmtzYWxtb25cIjogWzIzMywgMTUwLCAxMjJdLFxyXG5cdFwiZGFya3NlYWdyZWVuXCI6IFsxNDMsIDE4OCwgMTQzXSxcclxuXHRcImRhcmtzbGF0ZWJsdWVcIjogWzcyLCA2MSwgMTM5XSxcclxuXHRcImRhcmtzbGF0ZWdyYXlcIjogWzQ3LCA3OSwgNzldLFxyXG5cdFwiZGFya3NsYXRlZ3JleVwiOiBbNDcsIDc5LCA3OV0sXHJcblx0XCJkYXJrdHVycXVvaXNlXCI6IFswLCAyMDYsIDIwOV0sXHJcblx0XCJkYXJrdmlvbGV0XCI6IFsxNDgsIDAsIDIxMV0sXHJcblx0XCJkZWVwcGlua1wiOiBbMjU1LCAyMCwgMTQ3XSxcclxuXHRcImRlZXBza3libHVlXCI6IFswLCAxOTEsIDI1NV0sXHJcblx0XCJkaW1ncmF5XCI6IFsxMDUsIDEwNSwgMTA1XSxcclxuXHRcImRpbWdyZXlcIjogWzEwNSwgMTA1LCAxMDVdLFxyXG5cdFwiZG9kZ2VyYmx1ZVwiOiBbMzAsIDE0NCwgMjU1XSxcclxuXHRcImZpcmVicmlja1wiOiBbMTc4LCAzNCwgMzRdLFxyXG5cdFwiZmxvcmFsd2hpdGVcIjogWzI1NSwgMjUwLCAyNDBdLFxyXG5cdFwiZm9yZXN0Z3JlZW5cIjogWzM0LCAxMzksIDM0XSxcclxuXHRcImZ1Y2hzaWFcIjogWzI1NSwgMCwgMjU1XSxcclxuXHRcImdhaW5zYm9yb1wiOiBbMjIwLCAyMjAsIDIyMF0sXHJcblx0XCJnaG9zdHdoaXRlXCI6IFsyNDgsIDI0OCwgMjU1XSxcclxuXHRcImdvbGRcIjogWzI1NSwgMjE1LCAwXSxcclxuXHRcImdvbGRlbnJvZFwiOiBbMjE4LCAxNjUsIDMyXSxcclxuXHRcImdyYXlcIjogWzEyOCwgMTI4LCAxMjhdLFxyXG5cdFwiZ3JlZW5cIjogWzAsIDEyOCwgMF0sXHJcblx0XCJncmVlbnllbGxvd1wiOiBbMTczLCAyNTUsIDQ3XSxcclxuXHRcImdyZXlcIjogWzEyOCwgMTI4LCAxMjhdLFxyXG5cdFwiaG9uZXlkZXdcIjogWzI0MCwgMjU1LCAyNDBdLFxyXG5cdFwiaG90cGlua1wiOiBbMjU1LCAxMDUsIDE4MF0sXHJcblx0XCJpbmRpYW5yZWRcIjogWzIwNSwgOTIsIDkyXSxcclxuXHRcImluZGlnb1wiOiBbNzUsIDAsIDEzMF0sXHJcblx0XCJpdm9yeVwiOiBbMjU1LCAyNTUsIDI0MF0sXHJcblx0XCJraGFraVwiOiBbMjQwLCAyMzAsIDE0MF0sXHJcblx0XCJsYXZlbmRlclwiOiBbMjMwLCAyMzAsIDI1MF0sXHJcblx0XCJsYXZlbmRlcmJsdXNoXCI6IFsyNTUsIDI0MCwgMjQ1XSxcclxuXHRcImxhd25ncmVlblwiOiBbMTI0LCAyNTIsIDBdLFxyXG5cdFwibGVtb25jaGlmZm9uXCI6IFsyNTUsIDI1MCwgMjA1XSxcclxuXHRcImxpZ2h0Ymx1ZVwiOiBbMTczLCAyMTYsIDIzMF0sXHJcblx0XCJsaWdodGNvcmFsXCI6IFsyNDAsIDEyOCwgMTI4XSxcclxuXHRcImxpZ2h0Y3lhblwiOiBbMjI0LCAyNTUsIDI1NV0sXHJcblx0XCJsaWdodGdvbGRlbnJvZHllbGxvd1wiOiBbMjUwLCAyNTAsIDIxMF0sXHJcblx0XCJsaWdodGdyYXlcIjogWzIxMSwgMjExLCAyMTFdLFxyXG5cdFwibGlnaHRncmVlblwiOiBbMTQ0LCAyMzgsIDE0NF0sXHJcblx0XCJsaWdodGdyZXlcIjogWzIxMSwgMjExLCAyMTFdLFxyXG5cdFwibGlnaHRwaW5rXCI6IFsyNTUsIDE4MiwgMTkzXSxcclxuXHRcImxpZ2h0c2FsbW9uXCI6IFsyNTUsIDE2MCwgMTIyXSxcclxuXHRcImxpZ2h0c2VhZ3JlZW5cIjogWzMyLCAxNzgsIDE3MF0sXHJcblx0XCJsaWdodHNreWJsdWVcIjogWzEzNSwgMjA2LCAyNTBdLFxyXG5cdFwibGlnaHRzbGF0ZWdyYXlcIjogWzExOSwgMTM2LCAxNTNdLFxyXG5cdFwibGlnaHRzbGF0ZWdyZXlcIjogWzExOSwgMTM2LCAxNTNdLFxyXG5cdFwibGlnaHRzdGVlbGJsdWVcIjogWzE3NiwgMTk2LCAyMjJdLFxyXG5cdFwibGlnaHR5ZWxsb3dcIjogWzI1NSwgMjU1LCAyMjRdLFxyXG5cdFwibGltZVwiOiBbMCwgMjU1LCAwXSxcclxuXHRcImxpbWVncmVlblwiOiBbNTAsIDIwNSwgNTBdLFxyXG5cdFwibGluZW5cIjogWzI1MCwgMjQwLCAyMzBdLFxyXG5cdFwibWFnZW50YVwiOiBbMjU1LCAwLCAyNTVdLFxyXG5cdFwibWFyb29uXCI6IFsxMjgsIDAsIDBdLFxyXG5cdFwibWVkaXVtYXF1YW1hcmluZVwiOiBbMTAyLCAyMDUsIDE3MF0sXHJcblx0XCJtZWRpdW1ibHVlXCI6IFswLCAwLCAyMDVdLFxyXG5cdFwibWVkaXVtb3JjaGlkXCI6IFsxODYsIDg1LCAyMTFdLFxyXG5cdFwibWVkaXVtcHVycGxlXCI6IFsxNDcsIDExMiwgMjE5XSxcclxuXHRcIm1lZGl1bXNlYWdyZWVuXCI6IFs2MCwgMTc5LCAxMTNdLFxyXG5cdFwibWVkaXVtc2xhdGVibHVlXCI6IFsxMjMsIDEwNCwgMjM4XSxcclxuXHRcIm1lZGl1bXNwcmluZ2dyZWVuXCI6IFswLCAyNTAsIDE1NF0sXHJcblx0XCJtZWRpdW10dXJxdW9pc2VcIjogWzcyLCAyMDksIDIwNF0sXHJcblx0XCJtZWRpdW12aW9sZXRyZWRcIjogWzE5OSwgMjEsIDEzM10sXHJcblx0XCJtaWRuaWdodGJsdWVcIjogWzI1LCAyNSwgMTEyXSxcclxuXHRcIm1pbnRjcmVhbVwiOiBbMjQ1LCAyNTUsIDI1MF0sXHJcblx0XCJtaXN0eXJvc2VcIjogWzI1NSwgMjI4LCAyMjVdLFxyXG5cdFwibW9jY2FzaW5cIjogWzI1NSwgMjI4LCAxODFdLFxyXG5cdFwibmF2YWpvd2hpdGVcIjogWzI1NSwgMjIyLCAxNzNdLFxyXG5cdFwibmF2eVwiOiBbMCwgMCwgMTI4XSxcclxuXHRcIm9sZGxhY2VcIjogWzI1MywgMjQ1LCAyMzBdLFxyXG5cdFwib2xpdmVcIjogWzEyOCwgMTI4LCAwXSxcclxuXHRcIm9saXZlZHJhYlwiOiBbMTA3LCAxNDIsIDM1XSxcclxuXHRcIm9yYW5nZVwiOiBbMjU1LCAxNjUsIDBdLFxyXG5cdFwib3JhbmdlcmVkXCI6IFsyNTUsIDY5LCAwXSxcclxuXHRcIm9yY2hpZFwiOiBbMjE4LCAxMTIsIDIxNF0sXHJcblx0XCJwYWxlZ29sZGVucm9kXCI6IFsyMzgsIDIzMiwgMTcwXSxcclxuXHRcInBhbGVncmVlblwiOiBbMTUyLCAyNTEsIDE1Ml0sXHJcblx0XCJwYWxldHVycXVvaXNlXCI6IFsxNzUsIDIzOCwgMjM4XSxcclxuXHRcInBhbGV2aW9sZXRyZWRcIjogWzIxOSwgMTEyLCAxNDddLFxyXG5cdFwicGFwYXlhd2hpcFwiOiBbMjU1LCAyMzksIDIxM10sXHJcblx0XCJwZWFjaHB1ZmZcIjogWzI1NSwgMjE4LCAxODVdLFxyXG5cdFwicGVydVwiOiBbMjA1LCAxMzMsIDYzXSxcclxuXHRcInBpbmtcIjogWzI1NSwgMTkyLCAyMDNdLFxyXG5cdFwicGx1bVwiOiBbMjIxLCAxNjAsIDIyMV0sXHJcblx0XCJwb3dkZXJibHVlXCI6IFsxNzYsIDIyNCwgMjMwXSxcclxuXHRcInB1cnBsZVwiOiBbMTI4LCAwLCAxMjhdLFxyXG5cdFwicmViZWNjYXB1cnBsZVwiOiBbMTAyLCA1MSwgMTUzXSxcclxuXHRcInJlZFwiOiBbMjU1LCAwLCAwXSxcclxuXHRcInJvc3licm93blwiOiBbMTg4LCAxNDMsIDE0M10sXHJcblx0XCJyb3lhbGJsdWVcIjogWzY1LCAxMDUsIDIyNV0sXHJcblx0XCJzYWRkbGVicm93blwiOiBbMTM5LCA2OSwgMTldLFxyXG5cdFwic2FsbW9uXCI6IFsyNTAsIDEyOCwgMTE0XSxcclxuXHRcInNhbmR5YnJvd25cIjogWzI0NCwgMTY0LCA5Nl0sXHJcblx0XCJzZWFncmVlblwiOiBbNDYsIDEzOSwgODddLFxyXG5cdFwic2Vhc2hlbGxcIjogWzI1NSwgMjQ1LCAyMzhdLFxyXG5cdFwic2llbm5hXCI6IFsxNjAsIDgyLCA0NV0sXHJcblx0XCJzaWx2ZXJcIjogWzE5MiwgMTkyLCAxOTJdLFxyXG5cdFwic2t5Ymx1ZVwiOiBbMTM1LCAyMDYsIDIzNV0sXHJcblx0XCJzbGF0ZWJsdWVcIjogWzEwNiwgOTAsIDIwNV0sXHJcblx0XCJzbGF0ZWdyYXlcIjogWzExMiwgMTI4LCAxNDRdLFxyXG5cdFwic2xhdGVncmV5XCI6IFsxMTIsIDEyOCwgMTQ0XSxcclxuXHRcInNub3dcIjogWzI1NSwgMjUwLCAyNTBdLFxyXG5cdFwic3ByaW5nZ3JlZW5cIjogWzAsIDI1NSwgMTI3XSxcclxuXHRcInN0ZWVsYmx1ZVwiOiBbNzAsIDEzMCwgMTgwXSxcclxuXHRcInRhblwiOiBbMjEwLCAxODAsIDE0MF0sXHJcblx0XCJ0ZWFsXCI6IFswLCAxMjgsIDEyOF0sXHJcblx0XCJ0aGlzdGxlXCI6IFsyMTYsIDE5MSwgMjE2XSxcclxuXHRcInRvbWF0b1wiOiBbMjU1LCA5OSwgNzFdLFxyXG5cdFwidHVycXVvaXNlXCI6IFs2NCwgMjI0LCAyMDhdLFxyXG5cdFwidmlvbGV0XCI6IFsyMzgsIDEzMCwgMjM4XSxcclxuXHRcIndoZWF0XCI6IFsyNDUsIDIyMiwgMTc5XSxcclxuXHRcIndoaXRlXCI6IFsyNTUsIDI1NSwgMjU1XSxcclxuXHRcIndoaXRlc21va2VcIjogWzI0NSwgMjQ1LCAyNDVdLFxyXG5cdFwieWVsbG93XCI6IFsyNTUsIDI1NSwgMF0sXHJcblx0XCJ5ZWxsb3dncmVlblwiOiBbMTU0LCAyMDUsIDUwXVxyXG59O1xyXG5cblxuLyoqKi8gfSksXG4vKiA2ICovXG4vKioqLyAoZnVuY3Rpb24obW9kdWxlLCBleHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKSB7XG5cbi8qIE1JVCBsaWNlbnNlICovXG52YXIgY3NzS2V5d29yZHMgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKDUpO1xuXG4vLyBOT1RFOiBjb252ZXJzaW9ucyBzaG91bGQgb25seSByZXR1cm4gcHJpbWl0aXZlIHZhbHVlcyAoaS5lLiBhcnJheXMsIG9yXG4vLyAgICAgICB2YWx1ZXMgdGhhdCBnaXZlIGNvcnJlY3QgYHR5cGVvZmAgcmVzdWx0cykuXG4vLyAgICAgICBkbyBub3QgdXNlIGJveCB2YWx1ZXMgdHlwZXMgKGkuZS4gTnVtYmVyKCksIFN0cmluZygpLCBldGMuKVxuXG52YXIgcmV2ZXJzZUtleXdvcmRzID0ge307XG5mb3IgKHZhciBrZXkgaW4gY3NzS2V5d29yZHMpIHtcblx0aWYgKGNzc0tleXdvcmRzLmhhc093blByb3BlcnR5KGtleSkpIHtcblx0XHRyZXZlcnNlS2V5d29yZHNbY3NzS2V5d29yZHNba2V5XV0gPSBrZXk7XG5cdH1cbn1cblxudmFyIGNvbnZlcnQgPSBtb2R1bGUuZXhwb3J0cyA9IHtcblx0cmdiOiB7Y2hhbm5lbHM6IDMsIGxhYmVsczogJ3JnYid9LFxuXHRoc2w6IHtjaGFubmVsczogMywgbGFiZWxzOiAnaHNsJ30sXG5cdGhzdjoge2NoYW5uZWxzOiAzLCBsYWJlbHM6ICdoc3YnfSxcblx0aHdiOiB7Y2hhbm5lbHM6IDMsIGxhYmVsczogJ2h3Yid9LFxuXHRjbXlrOiB7Y2hhbm5lbHM6IDQsIGxhYmVsczogJ2NteWsnfSxcblx0eHl6OiB7Y2hhbm5lbHM6IDMsIGxhYmVsczogJ3h5eid9LFxuXHRsYWI6IHtjaGFubmVsczogMywgbGFiZWxzOiAnbGFiJ30sXG5cdGxjaDoge2NoYW5uZWxzOiAzLCBsYWJlbHM6ICdsY2gnfSxcblx0aGV4OiB7Y2hhbm5lbHM6IDEsIGxhYmVsczogWydoZXgnXX0sXG5cdGtleXdvcmQ6IHtjaGFubmVsczogMSwgbGFiZWxzOiBbJ2tleXdvcmQnXX0sXG5cdGFuc2kxNjoge2NoYW5uZWxzOiAxLCBsYWJlbHM6IFsnYW5zaTE2J119LFxuXHRhbnNpMjU2OiB7Y2hhbm5lbHM6IDEsIGxhYmVsczogWydhbnNpMjU2J119LFxuXHRoY2c6IHtjaGFubmVsczogMywgbGFiZWxzOiBbJ2gnLCAnYycsICdnJ119LFxuXHRhcHBsZToge2NoYW5uZWxzOiAzLCBsYWJlbHM6IFsncjE2JywgJ2cxNicsICdiMTYnXX0sXG5cdGdyYXk6IHtjaGFubmVsczogMSwgbGFiZWxzOiBbJ2dyYXknXX1cbn07XG5cbi8vIGhpZGUgLmNoYW5uZWxzIGFuZCAubGFiZWxzIHByb3BlcnRpZXNcbmZvciAodmFyIG1vZGVsIGluIGNvbnZlcnQpIHtcblx0aWYgKGNvbnZlcnQuaGFzT3duUHJvcGVydHkobW9kZWwpKSB7XG5cdFx0aWYgKCEoJ2NoYW5uZWxzJyBpbiBjb252ZXJ0W21vZGVsXSkpIHtcblx0XHRcdHRocm93IG5ldyBFcnJvcignbWlzc2luZyBjaGFubmVscyBwcm9wZXJ0eTogJyArIG1vZGVsKTtcblx0XHR9XG5cblx0XHRpZiAoISgnbGFiZWxzJyBpbiBjb252ZXJ0W21vZGVsXSkpIHtcblx0XHRcdHRocm93IG5ldyBFcnJvcignbWlzc2luZyBjaGFubmVsIGxhYmVscyBwcm9wZXJ0eTogJyArIG1vZGVsKTtcblx0XHR9XG5cblx0XHRpZiAoY29udmVydFttb2RlbF0ubGFiZWxzLmxlbmd0aCAhPT0gY29udmVydFttb2RlbF0uY2hhbm5lbHMpIHtcblx0XHRcdHRocm93IG5ldyBFcnJvcignY2hhbm5lbCBhbmQgbGFiZWwgY291bnRzIG1pc21hdGNoOiAnICsgbW9kZWwpO1xuXHRcdH1cblxuXHRcdHZhciBjaGFubmVscyA9IGNvbnZlcnRbbW9kZWxdLmNoYW5uZWxzO1xuXHRcdHZhciBsYWJlbHMgPSBjb252ZXJ0W21vZGVsXS5sYWJlbHM7XG5cdFx0ZGVsZXRlIGNvbnZlcnRbbW9kZWxdLmNoYW5uZWxzO1xuXHRcdGRlbGV0ZSBjb252ZXJ0W21vZGVsXS5sYWJlbHM7XG5cdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGNvbnZlcnRbbW9kZWxdLCAnY2hhbm5lbHMnLCB7dmFsdWU6IGNoYW5uZWxzfSk7XG5cdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGNvbnZlcnRbbW9kZWxdLCAnbGFiZWxzJywge3ZhbHVlOiBsYWJlbHN9KTtcblx0fVxufVxuXG5jb252ZXJ0LnJnYi5oc2wgPSBmdW5jdGlvbiAocmdiKSB7XG5cdHZhciByID0gcmdiWzBdIC8gMjU1O1xuXHR2YXIgZyA9IHJnYlsxXSAvIDI1NTtcblx0dmFyIGIgPSByZ2JbMl0gLyAyNTU7XG5cdHZhciBtaW4gPSBNYXRoLm1pbihyLCBnLCBiKTtcblx0dmFyIG1heCA9IE1hdGgubWF4KHIsIGcsIGIpO1xuXHR2YXIgZGVsdGEgPSBtYXggLSBtaW47XG5cdHZhciBoO1xuXHR2YXIgcztcblx0dmFyIGw7XG5cblx0aWYgKG1heCA9PT0gbWluKSB7XG5cdFx0aCA9IDA7XG5cdH0gZWxzZSBpZiAociA9PT0gbWF4KSB7XG5cdFx0aCA9IChnIC0gYikgLyBkZWx0YTtcblx0fSBlbHNlIGlmIChnID09PSBtYXgpIHtcblx0XHRoID0gMiArIChiIC0gcikgLyBkZWx0YTtcblx0fSBlbHNlIGlmIChiID09PSBtYXgpIHtcblx0XHRoID0gNCArIChyIC0gZykgLyBkZWx0YTtcblx0fVxuXG5cdGggPSBNYXRoLm1pbihoICogNjAsIDM2MCk7XG5cblx0aWYgKGggPCAwKSB7XG5cdFx0aCArPSAzNjA7XG5cdH1cblxuXHRsID0gKG1pbiArIG1heCkgLyAyO1xuXG5cdGlmIChtYXggPT09IG1pbikge1xuXHRcdHMgPSAwO1xuXHR9IGVsc2UgaWYgKGwgPD0gMC41KSB7XG5cdFx0cyA9IGRlbHRhIC8gKG1heCArIG1pbik7XG5cdH0gZWxzZSB7XG5cdFx0cyA9IGRlbHRhIC8gKDIgLSBtYXggLSBtaW4pO1xuXHR9XG5cblx0cmV0dXJuIFtoLCBzICogMTAwLCBsICogMTAwXTtcbn07XG5cbmNvbnZlcnQucmdiLmhzdiA9IGZ1bmN0aW9uIChyZ2IpIHtcblx0dmFyIHJkaWY7XG5cdHZhciBnZGlmO1xuXHR2YXIgYmRpZjtcblx0dmFyIGg7XG5cdHZhciBzO1xuXG5cdHZhciByID0gcmdiWzBdIC8gMjU1O1xuXHR2YXIgZyA9IHJnYlsxXSAvIDI1NTtcblx0dmFyIGIgPSByZ2JbMl0gLyAyNTU7XG5cdHZhciB2ID0gTWF0aC5tYXgociwgZywgYik7XG5cdHZhciBkaWZmID0gdiAtIE1hdGgubWluKHIsIGcsIGIpO1xuXHR2YXIgZGlmZmMgPSBmdW5jdGlvbiAoYykge1xuXHRcdHJldHVybiAodiAtIGMpIC8gNiAvIGRpZmYgKyAxIC8gMjtcblx0fTtcblxuXHRpZiAoZGlmZiA9PT0gMCkge1xuXHRcdGggPSBzID0gMDtcblx0fSBlbHNlIHtcblx0XHRzID0gZGlmZiAvIHY7XG5cdFx0cmRpZiA9IGRpZmZjKHIpO1xuXHRcdGdkaWYgPSBkaWZmYyhnKTtcblx0XHRiZGlmID0gZGlmZmMoYik7XG5cblx0XHRpZiAociA9PT0gdikge1xuXHRcdFx0aCA9IGJkaWYgLSBnZGlmO1xuXHRcdH0gZWxzZSBpZiAoZyA9PT0gdikge1xuXHRcdFx0aCA9ICgxIC8gMykgKyByZGlmIC0gYmRpZjtcblx0XHR9IGVsc2UgaWYgKGIgPT09IHYpIHtcblx0XHRcdGggPSAoMiAvIDMpICsgZ2RpZiAtIHJkaWY7XG5cdFx0fVxuXHRcdGlmIChoIDwgMCkge1xuXHRcdFx0aCArPSAxO1xuXHRcdH0gZWxzZSBpZiAoaCA+IDEpIHtcblx0XHRcdGggLT0gMTtcblx0XHR9XG5cdH1cblxuXHRyZXR1cm4gW1xuXHRcdGggKiAzNjAsXG5cdFx0cyAqIDEwMCxcblx0XHR2ICogMTAwXG5cdF07XG59O1xuXG5jb252ZXJ0LnJnYi5od2IgPSBmdW5jdGlvbiAocmdiKSB7XG5cdHZhciByID0gcmdiWzBdO1xuXHR2YXIgZyA9IHJnYlsxXTtcblx0dmFyIGIgPSByZ2JbMl07XG5cdHZhciBoID0gY29udmVydC5yZ2IuaHNsKHJnYilbMF07XG5cdHZhciB3ID0gMSAvIDI1NSAqIE1hdGgubWluKHIsIE1hdGgubWluKGcsIGIpKTtcblxuXHRiID0gMSAtIDEgLyAyNTUgKiBNYXRoLm1heChyLCBNYXRoLm1heChnLCBiKSk7XG5cblx0cmV0dXJuIFtoLCB3ICogMTAwLCBiICogMTAwXTtcbn07XG5cbmNvbnZlcnQucmdiLmNteWsgPSBmdW5jdGlvbiAocmdiKSB7XG5cdHZhciByID0gcmdiWzBdIC8gMjU1O1xuXHR2YXIgZyA9IHJnYlsxXSAvIDI1NTtcblx0dmFyIGIgPSByZ2JbMl0gLyAyNTU7XG5cdHZhciBjO1xuXHR2YXIgbTtcblx0dmFyIHk7XG5cdHZhciBrO1xuXG5cdGsgPSBNYXRoLm1pbigxIC0gciwgMSAtIGcsIDEgLSBiKTtcblx0YyA9ICgxIC0gciAtIGspIC8gKDEgLSBrKSB8fCAwO1xuXHRtID0gKDEgLSBnIC0gaykgLyAoMSAtIGspIHx8IDA7XG5cdHkgPSAoMSAtIGIgLSBrKSAvICgxIC0gaykgfHwgMDtcblxuXHRyZXR1cm4gW2MgKiAxMDAsIG0gKiAxMDAsIHkgKiAxMDAsIGsgKiAxMDBdO1xufTtcblxuLyoqXG4gKiBTZWUgaHR0cHM6Ly9lbi5tLndpa2lwZWRpYS5vcmcvd2lraS9FdWNsaWRlYW5fZGlzdGFuY2UjU3F1YXJlZF9FdWNsaWRlYW5fZGlzdGFuY2VcbiAqICovXG5mdW5jdGlvbiBjb21wYXJhdGl2ZURpc3RhbmNlKHgsIHkpIHtcblx0cmV0dXJuIChcblx0XHRNYXRoLnBvdyh4WzBdIC0geVswXSwgMikgK1xuXHRcdE1hdGgucG93KHhbMV0gLSB5WzFdLCAyKSArXG5cdFx0TWF0aC5wb3coeFsyXSAtIHlbMl0sIDIpXG5cdCk7XG59XG5cbmNvbnZlcnQucmdiLmtleXdvcmQgPSBmdW5jdGlvbiAocmdiKSB7XG5cdHZhciByZXZlcnNlZCA9IHJldmVyc2VLZXl3b3Jkc1tyZ2JdO1xuXHRpZiAocmV2ZXJzZWQpIHtcblx0XHRyZXR1cm4gcmV2ZXJzZWQ7XG5cdH1cblxuXHR2YXIgY3VycmVudENsb3Nlc3REaXN0YW5jZSA9IEluZmluaXR5O1xuXHR2YXIgY3VycmVudENsb3Nlc3RLZXl3b3JkO1xuXG5cdGZvciAodmFyIGtleXdvcmQgaW4gY3NzS2V5d29yZHMpIHtcblx0XHRpZiAoY3NzS2V5d29yZHMuaGFzT3duUHJvcGVydHkoa2V5d29yZCkpIHtcblx0XHRcdHZhciB2YWx1ZSA9IGNzc0tleXdvcmRzW2tleXdvcmRdO1xuXG5cdFx0XHQvLyBDb21wdXRlIGNvbXBhcmF0aXZlIGRpc3RhbmNlXG5cdFx0XHR2YXIgZGlzdGFuY2UgPSBjb21wYXJhdGl2ZURpc3RhbmNlKHJnYiwgdmFsdWUpO1xuXG5cdFx0XHQvLyBDaGVjayBpZiBpdHMgbGVzcywgaWYgc28gc2V0IGFzIGNsb3Nlc3Rcblx0XHRcdGlmIChkaXN0YW5jZSA8IGN1cnJlbnRDbG9zZXN0RGlzdGFuY2UpIHtcblx0XHRcdFx0Y3VycmVudENsb3Nlc3REaXN0YW5jZSA9IGRpc3RhbmNlO1xuXHRcdFx0XHRjdXJyZW50Q2xvc2VzdEtleXdvcmQgPSBrZXl3b3JkO1xuXHRcdFx0fVxuXHRcdH1cblx0fVxuXG5cdHJldHVybiBjdXJyZW50Q2xvc2VzdEtleXdvcmQ7XG59O1xuXG5jb252ZXJ0LmtleXdvcmQucmdiID0gZnVuY3Rpb24gKGtleXdvcmQpIHtcblx0cmV0dXJuIGNzc0tleXdvcmRzW2tleXdvcmRdO1xufTtcblxuY29udmVydC5yZ2IueHl6ID0gZnVuY3Rpb24gKHJnYikge1xuXHR2YXIgciA9IHJnYlswXSAvIDI1NTtcblx0dmFyIGcgPSByZ2JbMV0gLyAyNTU7XG5cdHZhciBiID0gcmdiWzJdIC8gMjU1O1xuXG5cdC8vIGFzc3VtZSBzUkdCXG5cdHIgPSByID4gMC4wNDA0NSA/IE1hdGgucG93KCgociArIDAuMDU1KSAvIDEuMDU1KSwgMi40KSA6IChyIC8gMTIuOTIpO1xuXHRnID0gZyA+IDAuMDQwNDUgPyBNYXRoLnBvdygoKGcgKyAwLjA1NSkgLyAxLjA1NSksIDIuNCkgOiAoZyAvIDEyLjkyKTtcblx0YiA9IGIgPiAwLjA0MDQ1ID8gTWF0aC5wb3coKChiICsgMC4wNTUpIC8gMS4wNTUpLCAyLjQpIDogKGIgLyAxMi45Mik7XG5cblx0dmFyIHggPSAociAqIDAuNDEyNCkgKyAoZyAqIDAuMzU3NikgKyAoYiAqIDAuMTgwNSk7XG5cdHZhciB5ID0gKHIgKiAwLjIxMjYpICsgKGcgKiAwLjcxNTIpICsgKGIgKiAwLjA3MjIpO1xuXHR2YXIgeiA9IChyICogMC4wMTkzKSArIChnICogMC4xMTkyKSArIChiICogMC45NTA1KTtcblxuXHRyZXR1cm4gW3ggKiAxMDAsIHkgKiAxMDAsIHogKiAxMDBdO1xufTtcblxuY29udmVydC5yZ2IubGFiID0gZnVuY3Rpb24gKHJnYikge1xuXHR2YXIgeHl6ID0gY29udmVydC5yZ2IueHl6KHJnYik7XG5cdHZhciB4ID0geHl6WzBdO1xuXHR2YXIgeSA9IHh5elsxXTtcblx0dmFyIHogPSB4eXpbMl07XG5cdHZhciBsO1xuXHR2YXIgYTtcblx0dmFyIGI7XG5cblx0eCAvPSA5NS4wNDc7XG5cdHkgLz0gMTAwO1xuXHR6IC89IDEwOC44ODM7XG5cblx0eCA9IHggPiAwLjAwODg1NiA/IE1hdGgucG93KHgsIDEgLyAzKSA6ICg3Ljc4NyAqIHgpICsgKDE2IC8gMTE2KTtcblx0eSA9IHkgPiAwLjAwODg1NiA/IE1hdGgucG93KHksIDEgLyAzKSA6ICg3Ljc4NyAqIHkpICsgKDE2IC8gMTE2KTtcblx0eiA9IHogPiAwLjAwODg1NiA/IE1hdGgucG93KHosIDEgLyAzKSA6ICg3Ljc4NyAqIHopICsgKDE2IC8gMTE2KTtcblxuXHRsID0gKDExNiAqIHkpIC0gMTY7XG5cdGEgPSA1MDAgKiAoeCAtIHkpO1xuXHRiID0gMjAwICogKHkgLSB6KTtcblxuXHRyZXR1cm4gW2wsIGEsIGJdO1xufTtcblxuY29udmVydC5oc2wucmdiID0gZnVuY3Rpb24gKGhzbCkge1xuXHR2YXIgaCA9IGhzbFswXSAvIDM2MDtcblx0dmFyIHMgPSBoc2xbMV0gLyAxMDA7XG5cdHZhciBsID0gaHNsWzJdIC8gMTAwO1xuXHR2YXIgdDE7XG5cdHZhciB0Mjtcblx0dmFyIHQzO1xuXHR2YXIgcmdiO1xuXHR2YXIgdmFsO1xuXG5cdGlmIChzID09PSAwKSB7XG5cdFx0dmFsID0gbCAqIDI1NTtcblx0XHRyZXR1cm4gW3ZhbCwgdmFsLCB2YWxdO1xuXHR9XG5cblx0aWYgKGwgPCAwLjUpIHtcblx0XHR0MiA9IGwgKiAoMSArIHMpO1xuXHR9IGVsc2Uge1xuXHRcdHQyID0gbCArIHMgLSBsICogcztcblx0fVxuXG5cdHQxID0gMiAqIGwgLSB0MjtcblxuXHRyZ2IgPSBbMCwgMCwgMF07XG5cdGZvciAodmFyIGkgPSAwOyBpIDwgMzsgaSsrKSB7XG5cdFx0dDMgPSBoICsgMSAvIDMgKiAtKGkgLSAxKTtcblx0XHRpZiAodDMgPCAwKSB7XG5cdFx0XHR0MysrO1xuXHRcdH1cblx0XHRpZiAodDMgPiAxKSB7XG5cdFx0XHR0My0tO1xuXHRcdH1cblxuXHRcdGlmICg2ICogdDMgPCAxKSB7XG5cdFx0XHR2YWwgPSB0MSArICh0MiAtIHQxKSAqIDYgKiB0Mztcblx0XHR9IGVsc2UgaWYgKDIgKiB0MyA8IDEpIHtcblx0XHRcdHZhbCA9IHQyO1xuXHRcdH0gZWxzZSBpZiAoMyAqIHQzIDwgMikge1xuXHRcdFx0dmFsID0gdDEgKyAodDIgLSB0MSkgKiAoMiAvIDMgLSB0MykgKiA2O1xuXHRcdH0gZWxzZSB7XG5cdFx0XHR2YWwgPSB0MTtcblx0XHR9XG5cblx0XHRyZ2JbaV0gPSB2YWwgKiAyNTU7XG5cdH1cblxuXHRyZXR1cm4gcmdiO1xufTtcblxuY29udmVydC5oc2wuaHN2ID0gZnVuY3Rpb24gKGhzbCkge1xuXHR2YXIgaCA9IGhzbFswXTtcblx0dmFyIHMgPSBoc2xbMV0gLyAxMDA7XG5cdHZhciBsID0gaHNsWzJdIC8gMTAwO1xuXHR2YXIgc21pbiA9IHM7XG5cdHZhciBsbWluID0gTWF0aC5tYXgobCwgMC4wMSk7XG5cdHZhciBzdjtcblx0dmFyIHY7XG5cblx0bCAqPSAyO1xuXHRzICo9IChsIDw9IDEpID8gbCA6IDIgLSBsO1xuXHRzbWluICo9IGxtaW4gPD0gMSA/IGxtaW4gOiAyIC0gbG1pbjtcblx0diA9IChsICsgcykgLyAyO1xuXHRzdiA9IGwgPT09IDAgPyAoMiAqIHNtaW4pIC8gKGxtaW4gKyBzbWluKSA6ICgyICogcykgLyAobCArIHMpO1xuXG5cdHJldHVybiBbaCwgc3YgKiAxMDAsIHYgKiAxMDBdO1xufTtcblxuY29udmVydC5oc3YucmdiID0gZnVuY3Rpb24gKGhzdikge1xuXHR2YXIgaCA9IGhzdlswXSAvIDYwO1xuXHR2YXIgcyA9IGhzdlsxXSAvIDEwMDtcblx0dmFyIHYgPSBoc3ZbMl0gLyAxMDA7XG5cdHZhciBoaSA9IE1hdGguZmxvb3IoaCkgJSA2O1xuXG5cdHZhciBmID0gaCAtIE1hdGguZmxvb3IoaCk7XG5cdHZhciBwID0gMjU1ICogdiAqICgxIC0gcyk7XG5cdHZhciBxID0gMjU1ICogdiAqICgxIC0gKHMgKiBmKSk7XG5cdHZhciB0ID0gMjU1ICogdiAqICgxIC0gKHMgKiAoMSAtIGYpKSk7XG5cdHYgKj0gMjU1O1xuXG5cdHN3aXRjaCAoaGkpIHtcblx0XHRjYXNlIDA6XG5cdFx0XHRyZXR1cm4gW3YsIHQsIHBdO1xuXHRcdGNhc2UgMTpcblx0XHRcdHJldHVybiBbcSwgdiwgcF07XG5cdFx0Y2FzZSAyOlxuXHRcdFx0cmV0dXJuIFtwLCB2LCB0XTtcblx0XHRjYXNlIDM6XG5cdFx0XHRyZXR1cm4gW3AsIHEsIHZdO1xuXHRcdGNhc2UgNDpcblx0XHRcdHJldHVybiBbdCwgcCwgdl07XG5cdFx0Y2FzZSA1OlxuXHRcdFx0cmV0dXJuIFt2LCBwLCBxXTtcblx0fVxufTtcblxuY29udmVydC5oc3YuaHNsID0gZnVuY3Rpb24gKGhzdikge1xuXHR2YXIgaCA9IGhzdlswXTtcblx0dmFyIHMgPSBoc3ZbMV0gLyAxMDA7XG5cdHZhciB2ID0gaHN2WzJdIC8gMTAwO1xuXHR2YXIgdm1pbiA9IE1hdGgubWF4KHYsIDAuMDEpO1xuXHR2YXIgbG1pbjtcblx0dmFyIHNsO1xuXHR2YXIgbDtcblxuXHRsID0gKDIgLSBzKSAqIHY7XG5cdGxtaW4gPSAoMiAtIHMpICogdm1pbjtcblx0c2wgPSBzICogdm1pbjtcblx0c2wgLz0gKGxtaW4gPD0gMSkgPyBsbWluIDogMiAtIGxtaW47XG5cdHNsID0gc2wgfHwgMDtcblx0bCAvPSAyO1xuXG5cdHJldHVybiBbaCwgc2wgKiAxMDAsIGwgKiAxMDBdO1xufTtcblxuLy8gaHR0cDovL2Rldi53My5vcmcvY3Nzd2cvY3NzLWNvbG9yLyNod2ItdG8tcmdiXG5jb252ZXJ0Lmh3Yi5yZ2IgPSBmdW5jdGlvbiAoaHdiKSB7XG5cdHZhciBoID0gaHdiWzBdIC8gMzYwO1xuXHR2YXIgd2ggPSBod2JbMV0gLyAxMDA7XG5cdHZhciBibCA9IGh3YlsyXSAvIDEwMDtcblx0dmFyIHJhdGlvID0gd2ggKyBibDtcblx0dmFyIGk7XG5cdHZhciB2O1xuXHR2YXIgZjtcblx0dmFyIG47XG5cblx0Ly8gd2ggKyBibCBjYW50IGJlID4gMVxuXHRpZiAocmF0aW8gPiAxKSB7XG5cdFx0d2ggLz0gcmF0aW87XG5cdFx0YmwgLz0gcmF0aW87XG5cdH1cblxuXHRpID0gTWF0aC5mbG9vcig2ICogaCk7XG5cdHYgPSAxIC0gYmw7XG5cdGYgPSA2ICogaCAtIGk7XG5cblx0aWYgKChpICYgMHgwMSkgIT09IDApIHtcblx0XHRmID0gMSAtIGY7XG5cdH1cblxuXHRuID0gd2ggKyBmICogKHYgLSB3aCk7IC8vIGxpbmVhciBpbnRlcnBvbGF0aW9uXG5cblx0dmFyIHI7XG5cdHZhciBnO1xuXHR2YXIgYjtcblx0c3dpdGNoIChpKSB7XG5cdFx0ZGVmYXVsdDpcblx0XHRjYXNlIDY6XG5cdFx0Y2FzZSAwOiByID0gdjsgZyA9IG47IGIgPSB3aDsgYnJlYWs7XG5cdFx0Y2FzZSAxOiByID0gbjsgZyA9IHY7IGIgPSB3aDsgYnJlYWs7XG5cdFx0Y2FzZSAyOiByID0gd2g7IGcgPSB2OyBiID0gbjsgYnJlYWs7XG5cdFx0Y2FzZSAzOiByID0gd2g7IGcgPSBuOyBiID0gdjsgYnJlYWs7XG5cdFx0Y2FzZSA0OiByID0gbjsgZyA9IHdoOyBiID0gdjsgYnJlYWs7XG5cdFx0Y2FzZSA1OiByID0gdjsgZyA9IHdoOyBiID0gbjsgYnJlYWs7XG5cdH1cblxuXHRyZXR1cm4gW3IgKiAyNTUsIGcgKiAyNTUsIGIgKiAyNTVdO1xufTtcblxuY29udmVydC5jbXlrLnJnYiA9IGZ1bmN0aW9uIChjbXlrKSB7XG5cdHZhciBjID0gY215a1swXSAvIDEwMDtcblx0dmFyIG0gPSBjbXlrWzFdIC8gMTAwO1xuXHR2YXIgeSA9IGNteWtbMl0gLyAxMDA7XG5cdHZhciBrID0gY215a1szXSAvIDEwMDtcblx0dmFyIHI7XG5cdHZhciBnO1xuXHR2YXIgYjtcblxuXHRyID0gMSAtIE1hdGgubWluKDEsIGMgKiAoMSAtIGspICsgayk7XG5cdGcgPSAxIC0gTWF0aC5taW4oMSwgbSAqICgxIC0gaykgKyBrKTtcblx0YiA9IDEgLSBNYXRoLm1pbigxLCB5ICogKDEgLSBrKSArIGspO1xuXG5cdHJldHVybiBbciAqIDI1NSwgZyAqIDI1NSwgYiAqIDI1NV07XG59O1xuXG5jb252ZXJ0Lnh5ei5yZ2IgPSBmdW5jdGlvbiAoeHl6KSB7XG5cdHZhciB4ID0geHl6WzBdIC8gMTAwO1xuXHR2YXIgeSA9IHh5elsxXSAvIDEwMDtcblx0dmFyIHogPSB4eXpbMl0gLyAxMDA7XG5cdHZhciByO1xuXHR2YXIgZztcblx0dmFyIGI7XG5cblx0ciA9ICh4ICogMy4yNDA2KSArICh5ICogLTEuNTM3MikgKyAoeiAqIC0wLjQ5ODYpO1xuXHRnID0gKHggKiAtMC45Njg5KSArICh5ICogMS44NzU4KSArICh6ICogMC4wNDE1KTtcblx0YiA9ICh4ICogMC4wNTU3KSArICh5ICogLTAuMjA0MCkgKyAoeiAqIDEuMDU3MCk7XG5cblx0Ly8gYXNzdW1lIHNSR0Jcblx0ciA9IHIgPiAwLjAwMzEzMDhcblx0XHQ/ICgoMS4wNTUgKiBNYXRoLnBvdyhyLCAxLjAgLyAyLjQpKSAtIDAuMDU1KVxuXHRcdDogciAqIDEyLjkyO1xuXG5cdGcgPSBnID4gMC4wMDMxMzA4XG5cdFx0PyAoKDEuMDU1ICogTWF0aC5wb3coZywgMS4wIC8gMi40KSkgLSAwLjA1NSlcblx0XHQ6IGcgKiAxMi45MjtcblxuXHRiID0gYiA+IDAuMDAzMTMwOFxuXHRcdD8gKCgxLjA1NSAqIE1hdGgucG93KGIsIDEuMCAvIDIuNCkpIC0gMC4wNTUpXG5cdFx0OiBiICogMTIuOTI7XG5cblx0ciA9IE1hdGgubWluKE1hdGgubWF4KDAsIHIpLCAxKTtcblx0ZyA9IE1hdGgubWluKE1hdGgubWF4KDAsIGcpLCAxKTtcblx0YiA9IE1hdGgubWluKE1hdGgubWF4KDAsIGIpLCAxKTtcblxuXHRyZXR1cm4gW3IgKiAyNTUsIGcgKiAyNTUsIGIgKiAyNTVdO1xufTtcblxuY29udmVydC54eXoubGFiID0gZnVuY3Rpb24gKHh5eikge1xuXHR2YXIgeCA9IHh5elswXTtcblx0dmFyIHkgPSB4eXpbMV07XG5cdHZhciB6ID0geHl6WzJdO1xuXHR2YXIgbDtcblx0dmFyIGE7XG5cdHZhciBiO1xuXG5cdHggLz0gOTUuMDQ3O1xuXHR5IC89IDEwMDtcblx0eiAvPSAxMDguODgzO1xuXG5cdHggPSB4ID4gMC4wMDg4NTYgPyBNYXRoLnBvdyh4LCAxIC8gMykgOiAoNy43ODcgKiB4KSArICgxNiAvIDExNik7XG5cdHkgPSB5ID4gMC4wMDg4NTYgPyBNYXRoLnBvdyh5LCAxIC8gMykgOiAoNy43ODcgKiB5KSArICgxNiAvIDExNik7XG5cdHogPSB6ID4gMC4wMDg4NTYgPyBNYXRoLnBvdyh6LCAxIC8gMykgOiAoNy43ODcgKiB6KSArICgxNiAvIDExNik7XG5cblx0bCA9ICgxMTYgKiB5KSAtIDE2O1xuXHRhID0gNTAwICogKHggLSB5KTtcblx0YiA9IDIwMCAqICh5IC0geik7XG5cblx0cmV0dXJuIFtsLCBhLCBiXTtcbn07XG5cbmNvbnZlcnQubGFiLnh5eiA9IGZ1bmN0aW9uIChsYWIpIHtcblx0dmFyIGwgPSBsYWJbMF07XG5cdHZhciBhID0gbGFiWzFdO1xuXHR2YXIgYiA9IGxhYlsyXTtcblx0dmFyIHg7XG5cdHZhciB5O1xuXHR2YXIgejtcblxuXHR5ID0gKGwgKyAxNikgLyAxMTY7XG5cdHggPSBhIC8gNTAwICsgeTtcblx0eiA9IHkgLSBiIC8gMjAwO1xuXG5cdHZhciB5MiA9IE1hdGgucG93KHksIDMpO1xuXHR2YXIgeDIgPSBNYXRoLnBvdyh4LCAzKTtcblx0dmFyIHoyID0gTWF0aC5wb3coeiwgMyk7XG5cdHkgPSB5MiA+IDAuMDA4ODU2ID8geTIgOiAoeSAtIDE2IC8gMTE2KSAvIDcuNzg3O1xuXHR4ID0geDIgPiAwLjAwODg1NiA/IHgyIDogKHggLSAxNiAvIDExNikgLyA3Ljc4Nztcblx0eiA9IHoyID4gMC4wMDg4NTYgPyB6MiA6ICh6IC0gMTYgLyAxMTYpIC8gNy43ODc7XG5cblx0eCAqPSA5NS4wNDc7XG5cdHkgKj0gMTAwO1xuXHR6ICo9IDEwOC44ODM7XG5cblx0cmV0dXJuIFt4LCB5LCB6XTtcbn07XG5cbmNvbnZlcnQubGFiLmxjaCA9IGZ1bmN0aW9uIChsYWIpIHtcblx0dmFyIGwgPSBsYWJbMF07XG5cdHZhciBhID0gbGFiWzFdO1xuXHR2YXIgYiA9IGxhYlsyXTtcblx0dmFyIGhyO1xuXHR2YXIgaDtcblx0dmFyIGM7XG5cblx0aHIgPSBNYXRoLmF0YW4yKGIsIGEpO1xuXHRoID0gaHIgKiAzNjAgLyAyIC8gTWF0aC5QSTtcblxuXHRpZiAoaCA8IDApIHtcblx0XHRoICs9IDM2MDtcblx0fVxuXG5cdGMgPSBNYXRoLnNxcnQoYSAqIGEgKyBiICogYik7XG5cblx0cmV0dXJuIFtsLCBjLCBoXTtcbn07XG5cbmNvbnZlcnQubGNoLmxhYiA9IGZ1bmN0aW9uIChsY2gpIHtcblx0dmFyIGwgPSBsY2hbMF07XG5cdHZhciBjID0gbGNoWzFdO1xuXHR2YXIgaCA9IGxjaFsyXTtcblx0dmFyIGE7XG5cdHZhciBiO1xuXHR2YXIgaHI7XG5cblx0aHIgPSBoIC8gMzYwICogMiAqIE1hdGguUEk7XG5cdGEgPSBjICogTWF0aC5jb3MoaHIpO1xuXHRiID0gYyAqIE1hdGguc2luKGhyKTtcblxuXHRyZXR1cm4gW2wsIGEsIGJdO1xufTtcblxuY29udmVydC5yZ2IuYW5zaTE2ID0gZnVuY3Rpb24gKGFyZ3MpIHtcblx0dmFyIHIgPSBhcmdzWzBdO1xuXHR2YXIgZyA9IGFyZ3NbMV07XG5cdHZhciBiID0gYXJnc1syXTtcblx0dmFyIHZhbHVlID0gMSBpbiBhcmd1bWVudHMgPyBhcmd1bWVudHNbMV0gOiBjb252ZXJ0LnJnYi5oc3YoYXJncylbMl07IC8vIGhzdiAtPiBhbnNpMTYgb3B0aW1pemF0aW9uXG5cblx0dmFsdWUgPSBNYXRoLnJvdW5kKHZhbHVlIC8gNTApO1xuXG5cdGlmICh2YWx1ZSA9PT0gMCkge1xuXHRcdHJldHVybiAzMDtcblx0fVxuXG5cdHZhciBhbnNpID0gMzBcblx0XHQrICgoTWF0aC5yb3VuZChiIC8gMjU1KSA8PCAyKVxuXHRcdHwgKE1hdGgucm91bmQoZyAvIDI1NSkgPDwgMSlcblx0XHR8IE1hdGgucm91bmQociAvIDI1NSkpO1xuXG5cdGlmICh2YWx1ZSA9PT0gMikge1xuXHRcdGFuc2kgKz0gNjA7XG5cdH1cblxuXHRyZXR1cm4gYW5zaTtcbn07XG5cbmNvbnZlcnQuaHN2LmFuc2kxNiA9IGZ1bmN0aW9uIChhcmdzKSB7XG5cdC8vIG9wdGltaXphdGlvbiBoZXJlOyB3ZSBhbHJlYWR5IGtub3cgdGhlIHZhbHVlIGFuZCBkb24ndCBuZWVkIHRvIGdldFxuXHQvLyBpdCBjb252ZXJ0ZWQgZm9yIHVzLlxuXHRyZXR1cm4gY29udmVydC5yZ2IuYW5zaTE2KGNvbnZlcnQuaHN2LnJnYihhcmdzKSwgYXJnc1syXSk7XG59O1xuXG5jb252ZXJ0LnJnYi5hbnNpMjU2ID0gZnVuY3Rpb24gKGFyZ3MpIHtcblx0dmFyIHIgPSBhcmdzWzBdO1xuXHR2YXIgZyA9IGFyZ3NbMV07XG5cdHZhciBiID0gYXJnc1syXTtcblxuXHQvLyB3ZSB1c2UgdGhlIGV4dGVuZGVkIGdyZXlzY2FsZSBwYWxldHRlIGhlcmUsIHdpdGggdGhlIGV4Y2VwdGlvbiBvZlxuXHQvLyBibGFjayBhbmQgd2hpdGUuIG5vcm1hbCBwYWxldHRlIG9ubHkgaGFzIDQgZ3JleXNjYWxlIHNoYWRlcy5cblx0aWYgKHIgPT09IGcgJiYgZyA9PT0gYikge1xuXHRcdGlmIChyIDwgOCkge1xuXHRcdFx0cmV0dXJuIDE2O1xuXHRcdH1cblxuXHRcdGlmIChyID4gMjQ4KSB7XG5cdFx0XHRyZXR1cm4gMjMxO1xuXHRcdH1cblxuXHRcdHJldHVybiBNYXRoLnJvdW5kKCgociAtIDgpIC8gMjQ3KSAqIDI0KSArIDIzMjtcblx0fVxuXG5cdHZhciBhbnNpID0gMTZcblx0XHQrICgzNiAqIE1hdGgucm91bmQociAvIDI1NSAqIDUpKVxuXHRcdCsgKDYgKiBNYXRoLnJvdW5kKGcgLyAyNTUgKiA1KSlcblx0XHQrIE1hdGgucm91bmQoYiAvIDI1NSAqIDUpO1xuXG5cdHJldHVybiBhbnNpO1xufTtcblxuY29udmVydC5hbnNpMTYucmdiID0gZnVuY3Rpb24gKGFyZ3MpIHtcblx0dmFyIGNvbG9yID0gYXJncyAlIDEwO1xuXG5cdC8vIGhhbmRsZSBncmV5c2NhbGVcblx0aWYgKGNvbG9yID09PSAwIHx8IGNvbG9yID09PSA3KSB7XG5cdFx0aWYgKGFyZ3MgPiA1MCkge1xuXHRcdFx0Y29sb3IgKz0gMy41O1xuXHRcdH1cblxuXHRcdGNvbG9yID0gY29sb3IgLyAxMC41ICogMjU1O1xuXG5cdFx0cmV0dXJuIFtjb2xvciwgY29sb3IsIGNvbG9yXTtcblx0fVxuXG5cdHZhciBtdWx0ID0gKH5+KGFyZ3MgPiA1MCkgKyAxKSAqIDAuNTtcblx0dmFyIHIgPSAoKGNvbG9yICYgMSkgKiBtdWx0KSAqIDI1NTtcblx0dmFyIGcgPSAoKChjb2xvciA+PiAxKSAmIDEpICogbXVsdCkgKiAyNTU7XG5cdHZhciBiID0gKCgoY29sb3IgPj4gMikgJiAxKSAqIG11bHQpICogMjU1O1xuXG5cdHJldHVybiBbciwgZywgYl07XG59O1xuXG5jb252ZXJ0LmFuc2kyNTYucmdiID0gZnVuY3Rpb24gKGFyZ3MpIHtcblx0Ly8gaGFuZGxlIGdyZXlzY2FsZVxuXHRpZiAoYXJncyA+PSAyMzIpIHtcblx0XHR2YXIgYyA9IChhcmdzIC0gMjMyKSAqIDEwICsgODtcblx0XHRyZXR1cm4gW2MsIGMsIGNdO1xuXHR9XG5cblx0YXJncyAtPSAxNjtcblxuXHR2YXIgcmVtO1xuXHR2YXIgciA9IE1hdGguZmxvb3IoYXJncyAvIDM2KSAvIDUgKiAyNTU7XG5cdHZhciBnID0gTWF0aC5mbG9vcigocmVtID0gYXJncyAlIDM2KSAvIDYpIC8gNSAqIDI1NTtcblx0dmFyIGIgPSAocmVtICUgNikgLyA1ICogMjU1O1xuXG5cdHJldHVybiBbciwgZywgYl07XG59O1xuXG5jb252ZXJ0LnJnYi5oZXggPSBmdW5jdGlvbiAoYXJncykge1xuXHR2YXIgaW50ZWdlciA9ICgoTWF0aC5yb3VuZChhcmdzWzBdKSAmIDB4RkYpIDw8IDE2KVxuXHRcdCsgKChNYXRoLnJvdW5kKGFyZ3NbMV0pICYgMHhGRikgPDwgOClcblx0XHQrIChNYXRoLnJvdW5kKGFyZ3NbMl0pICYgMHhGRik7XG5cblx0dmFyIHN0cmluZyA9IGludGVnZXIudG9TdHJpbmcoMTYpLnRvVXBwZXJDYXNlKCk7XG5cdHJldHVybiAnMDAwMDAwJy5zdWJzdHJpbmcoc3RyaW5nLmxlbmd0aCkgKyBzdHJpbmc7XG59O1xuXG5jb252ZXJ0LmhleC5yZ2IgPSBmdW5jdGlvbiAoYXJncykge1xuXHR2YXIgbWF0Y2ggPSBhcmdzLnRvU3RyaW5nKDE2KS5tYXRjaCgvW2EtZjAtOV17Nn18W2EtZjAtOV17M30vaSk7XG5cdGlmICghbWF0Y2gpIHtcblx0XHRyZXR1cm4gWzAsIDAsIDBdO1xuXHR9XG5cblx0dmFyIGNvbG9yU3RyaW5nID0gbWF0Y2hbMF07XG5cblx0aWYgKG1hdGNoWzBdLmxlbmd0aCA9PT0gMykge1xuXHRcdGNvbG9yU3RyaW5nID0gY29sb3JTdHJpbmcuc3BsaXQoJycpLm1hcChmdW5jdGlvbiAoY2hhcikge1xuXHRcdFx0cmV0dXJuIGNoYXIgKyBjaGFyO1xuXHRcdH0pLmpvaW4oJycpO1xuXHR9XG5cblx0dmFyIGludGVnZXIgPSBwYXJzZUludChjb2xvclN0cmluZywgMTYpO1xuXHR2YXIgciA9IChpbnRlZ2VyID4+IDE2KSAmIDB4RkY7XG5cdHZhciBnID0gKGludGVnZXIgPj4gOCkgJiAweEZGO1xuXHR2YXIgYiA9IGludGVnZXIgJiAweEZGO1xuXG5cdHJldHVybiBbciwgZywgYl07XG59O1xuXG5jb252ZXJ0LnJnYi5oY2cgPSBmdW5jdGlvbiAocmdiKSB7XG5cdHZhciByID0gcmdiWzBdIC8gMjU1O1xuXHR2YXIgZyA9IHJnYlsxXSAvIDI1NTtcblx0dmFyIGIgPSByZ2JbMl0gLyAyNTU7XG5cdHZhciBtYXggPSBNYXRoLm1heChNYXRoLm1heChyLCBnKSwgYik7XG5cdHZhciBtaW4gPSBNYXRoLm1pbihNYXRoLm1pbihyLCBnKSwgYik7XG5cdHZhciBjaHJvbWEgPSAobWF4IC0gbWluKTtcblx0dmFyIGdyYXlzY2FsZTtcblx0dmFyIGh1ZTtcblxuXHRpZiAoY2hyb21hIDwgMSkge1xuXHRcdGdyYXlzY2FsZSA9IG1pbiAvICgxIC0gY2hyb21hKTtcblx0fSBlbHNlIHtcblx0XHRncmF5c2NhbGUgPSAwO1xuXHR9XG5cblx0aWYgKGNocm9tYSA8PSAwKSB7XG5cdFx0aHVlID0gMDtcblx0fSBlbHNlXG5cdGlmIChtYXggPT09IHIpIHtcblx0XHRodWUgPSAoKGcgLSBiKSAvIGNocm9tYSkgJSA2O1xuXHR9IGVsc2Vcblx0aWYgKG1heCA9PT0gZykge1xuXHRcdGh1ZSA9IDIgKyAoYiAtIHIpIC8gY2hyb21hO1xuXHR9IGVsc2Uge1xuXHRcdGh1ZSA9IDQgKyAociAtIGcpIC8gY2hyb21hICsgNDtcblx0fVxuXG5cdGh1ZSAvPSA2O1xuXHRodWUgJT0gMTtcblxuXHRyZXR1cm4gW2h1ZSAqIDM2MCwgY2hyb21hICogMTAwLCBncmF5c2NhbGUgKiAxMDBdO1xufTtcblxuY29udmVydC5oc2wuaGNnID0gZnVuY3Rpb24gKGhzbCkge1xuXHR2YXIgcyA9IGhzbFsxXSAvIDEwMDtcblx0dmFyIGwgPSBoc2xbMl0gLyAxMDA7XG5cdHZhciBjID0gMTtcblx0dmFyIGYgPSAwO1xuXG5cdGlmIChsIDwgMC41KSB7XG5cdFx0YyA9IDIuMCAqIHMgKiBsO1xuXHR9IGVsc2Uge1xuXHRcdGMgPSAyLjAgKiBzICogKDEuMCAtIGwpO1xuXHR9XG5cblx0aWYgKGMgPCAxLjApIHtcblx0XHRmID0gKGwgLSAwLjUgKiBjKSAvICgxLjAgLSBjKTtcblx0fVxuXG5cdHJldHVybiBbaHNsWzBdLCBjICogMTAwLCBmICogMTAwXTtcbn07XG5cbmNvbnZlcnQuaHN2LmhjZyA9IGZ1bmN0aW9uIChoc3YpIHtcblx0dmFyIHMgPSBoc3ZbMV0gLyAxMDA7XG5cdHZhciB2ID0gaHN2WzJdIC8gMTAwO1xuXG5cdHZhciBjID0gcyAqIHY7XG5cdHZhciBmID0gMDtcblxuXHRpZiAoYyA8IDEuMCkge1xuXHRcdGYgPSAodiAtIGMpIC8gKDEgLSBjKTtcblx0fVxuXG5cdHJldHVybiBbaHN2WzBdLCBjICogMTAwLCBmICogMTAwXTtcbn07XG5cbmNvbnZlcnQuaGNnLnJnYiA9IGZ1bmN0aW9uIChoY2cpIHtcblx0dmFyIGggPSBoY2dbMF0gLyAzNjA7XG5cdHZhciBjID0gaGNnWzFdIC8gMTAwO1xuXHR2YXIgZyA9IGhjZ1syXSAvIDEwMDtcblxuXHRpZiAoYyA9PT0gMC4wKSB7XG5cdFx0cmV0dXJuIFtnICogMjU1LCBnICogMjU1LCBnICogMjU1XTtcblx0fVxuXG5cdHZhciBwdXJlID0gWzAsIDAsIDBdO1xuXHR2YXIgaGkgPSAoaCAlIDEpICogNjtcblx0dmFyIHYgPSBoaSAlIDE7XG5cdHZhciB3ID0gMSAtIHY7XG5cdHZhciBtZyA9IDA7XG5cblx0c3dpdGNoIChNYXRoLmZsb29yKGhpKSkge1xuXHRcdGNhc2UgMDpcblx0XHRcdHB1cmVbMF0gPSAxOyBwdXJlWzFdID0gdjsgcHVyZVsyXSA9IDA7IGJyZWFrO1xuXHRcdGNhc2UgMTpcblx0XHRcdHB1cmVbMF0gPSB3OyBwdXJlWzFdID0gMTsgcHVyZVsyXSA9IDA7IGJyZWFrO1xuXHRcdGNhc2UgMjpcblx0XHRcdHB1cmVbMF0gPSAwOyBwdXJlWzFdID0gMTsgcHVyZVsyXSA9IHY7IGJyZWFrO1xuXHRcdGNhc2UgMzpcblx0XHRcdHB1cmVbMF0gPSAwOyBwdXJlWzFdID0gdzsgcHVyZVsyXSA9IDE7IGJyZWFrO1xuXHRcdGNhc2UgNDpcblx0XHRcdHB1cmVbMF0gPSB2OyBwdXJlWzFdID0gMDsgcHVyZVsyXSA9IDE7IGJyZWFrO1xuXHRcdGRlZmF1bHQ6XG5cdFx0XHRwdXJlWzBdID0gMTsgcHVyZVsxXSA9IDA7IHB1cmVbMl0gPSB3O1xuXHR9XG5cblx0bWcgPSAoMS4wIC0gYykgKiBnO1xuXG5cdHJldHVybiBbXG5cdFx0KGMgKiBwdXJlWzBdICsgbWcpICogMjU1LFxuXHRcdChjICogcHVyZVsxXSArIG1nKSAqIDI1NSxcblx0XHQoYyAqIHB1cmVbMl0gKyBtZykgKiAyNTVcblx0XTtcbn07XG5cbmNvbnZlcnQuaGNnLmhzdiA9IGZ1bmN0aW9uIChoY2cpIHtcblx0dmFyIGMgPSBoY2dbMV0gLyAxMDA7XG5cdHZhciBnID0gaGNnWzJdIC8gMTAwO1xuXG5cdHZhciB2ID0gYyArIGcgKiAoMS4wIC0gYyk7XG5cdHZhciBmID0gMDtcblxuXHRpZiAodiA+IDAuMCkge1xuXHRcdGYgPSBjIC8gdjtcblx0fVxuXG5cdHJldHVybiBbaGNnWzBdLCBmICogMTAwLCB2ICogMTAwXTtcbn07XG5cbmNvbnZlcnQuaGNnLmhzbCA9IGZ1bmN0aW9uIChoY2cpIHtcblx0dmFyIGMgPSBoY2dbMV0gLyAxMDA7XG5cdHZhciBnID0gaGNnWzJdIC8gMTAwO1xuXG5cdHZhciBsID0gZyAqICgxLjAgLSBjKSArIDAuNSAqIGM7XG5cdHZhciBzID0gMDtcblxuXHRpZiAobCA+IDAuMCAmJiBsIDwgMC41KSB7XG5cdFx0cyA9IGMgLyAoMiAqIGwpO1xuXHR9IGVsc2Vcblx0aWYgKGwgPj0gMC41ICYmIGwgPCAxLjApIHtcblx0XHRzID0gYyAvICgyICogKDEgLSBsKSk7XG5cdH1cblxuXHRyZXR1cm4gW2hjZ1swXSwgcyAqIDEwMCwgbCAqIDEwMF07XG59O1xuXG5jb252ZXJ0LmhjZy5od2IgPSBmdW5jdGlvbiAoaGNnKSB7XG5cdHZhciBjID0gaGNnWzFdIC8gMTAwO1xuXHR2YXIgZyA9IGhjZ1syXSAvIDEwMDtcblx0dmFyIHYgPSBjICsgZyAqICgxLjAgLSBjKTtcblx0cmV0dXJuIFtoY2dbMF0sICh2IC0gYykgKiAxMDAsICgxIC0gdikgKiAxMDBdO1xufTtcblxuY29udmVydC5od2IuaGNnID0gZnVuY3Rpb24gKGh3Yikge1xuXHR2YXIgdyA9IGh3YlsxXSAvIDEwMDtcblx0dmFyIGIgPSBod2JbMl0gLyAxMDA7XG5cdHZhciB2ID0gMSAtIGI7XG5cdHZhciBjID0gdiAtIHc7XG5cdHZhciBnID0gMDtcblxuXHRpZiAoYyA8IDEpIHtcblx0XHRnID0gKHYgLSBjKSAvICgxIC0gYyk7XG5cdH1cblxuXHRyZXR1cm4gW2h3YlswXSwgYyAqIDEwMCwgZyAqIDEwMF07XG59O1xuXG5jb252ZXJ0LmFwcGxlLnJnYiA9IGZ1bmN0aW9uIChhcHBsZSkge1xuXHRyZXR1cm4gWyhhcHBsZVswXSAvIDY1NTM1KSAqIDI1NSwgKGFwcGxlWzFdIC8gNjU1MzUpICogMjU1LCAoYXBwbGVbMl0gLyA2NTUzNSkgKiAyNTVdO1xufTtcblxuY29udmVydC5yZ2IuYXBwbGUgPSBmdW5jdGlvbiAocmdiKSB7XG5cdHJldHVybiBbKHJnYlswXSAvIDI1NSkgKiA2NTUzNSwgKHJnYlsxXSAvIDI1NSkgKiA2NTUzNSwgKHJnYlsyXSAvIDI1NSkgKiA2NTUzNV07XG59O1xuXG5jb252ZXJ0LmdyYXkucmdiID0gZnVuY3Rpb24gKGFyZ3MpIHtcblx0cmV0dXJuIFthcmdzWzBdIC8gMTAwICogMjU1LCBhcmdzWzBdIC8gMTAwICogMjU1LCBhcmdzWzBdIC8gMTAwICogMjU1XTtcbn07XG5cbmNvbnZlcnQuZ3JheS5oc2wgPSBjb252ZXJ0LmdyYXkuaHN2ID0gZnVuY3Rpb24gKGFyZ3MpIHtcblx0cmV0dXJuIFswLCAwLCBhcmdzWzBdXTtcbn07XG5cbmNvbnZlcnQuZ3JheS5od2IgPSBmdW5jdGlvbiAoZ3JheSkge1xuXHRyZXR1cm4gWzAsIDEwMCwgZ3JheVswXV07XG59O1xuXG5jb252ZXJ0LmdyYXkuY215ayA9IGZ1bmN0aW9uIChncmF5KSB7XG5cdHJldHVybiBbMCwgMCwgMCwgZ3JheVswXV07XG59O1xuXG5jb252ZXJ0LmdyYXkubGFiID0gZnVuY3Rpb24gKGdyYXkpIHtcblx0cmV0dXJuIFtncmF5WzBdLCAwLCAwXTtcbn07XG5cbmNvbnZlcnQuZ3JheS5oZXggPSBmdW5jdGlvbiAoZ3JheSkge1xuXHR2YXIgdmFsID0gTWF0aC5yb3VuZChncmF5WzBdIC8gMTAwICogMjU1KSAmIDB4RkY7XG5cdHZhciBpbnRlZ2VyID0gKHZhbCA8PCAxNikgKyAodmFsIDw8IDgpICsgdmFsO1xuXG5cdHZhciBzdHJpbmcgPSBpbnRlZ2VyLnRvU3RyaW5nKDE2KS50b1VwcGVyQ2FzZSgpO1xuXHRyZXR1cm4gJzAwMDAwMCcuc3Vic3RyaW5nKHN0cmluZy5sZW5ndGgpICsgc3RyaW5nO1xufTtcblxuY29udmVydC5yZ2IuZ3JheSA9IGZ1bmN0aW9uIChyZ2IpIHtcblx0dmFyIHZhbCA9IChyZ2JbMF0gKyByZ2JbMV0gKyByZ2JbMl0pIC8gMztcblx0cmV0dXJuIFt2YWwgLyAyNTUgKiAxMDBdO1xufTtcblxuXG4vKioqLyB9KSxcbi8qIDcgKi9cbi8qKiovIChmdW5jdGlvbihtb2R1bGUsIGV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pIHtcblxuXCJ1c2Ugc3RyaWN0XCI7XG5cblxudmFyIF90eXBlb2YgPSB0eXBlb2YgU3ltYm9sID09PSBcImZ1bmN0aW9uXCIgJiYgdHlwZW9mIFN5bWJvbC5pdGVyYXRvciA9PT0gXCJzeW1ib2xcIiA/IGZ1bmN0aW9uIChvYmopIHsgcmV0dXJuIHR5cGVvZiBvYmo7IH0gOiBmdW5jdGlvbiAob2JqKSB7IHJldHVybiBvYmogJiYgdHlwZW9mIFN5bWJvbCA9PT0gXCJmdW5jdGlvblwiICYmIG9iai5jb25zdHJ1Y3RvciA9PT0gU3ltYm9sICYmIG9iaiAhPT0gU3ltYm9sLnByb3RvdHlwZSA/IFwic3ltYm9sXCIgOiB0eXBlb2Ygb2JqOyB9O1xuXG52YXIgX0NvbG9ycGlja2VyID0gX193ZWJwYWNrX3JlcXVpcmVfXyg4KTtcblxudmFyIF9Db2xvcnBpY2tlcjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9Db2xvcnBpY2tlcik7XG5cbnZhciBfanF1ZXJ5ID0gX193ZWJwYWNrX3JlcXVpcmVfXygwKTtcblxudmFyIF9qcXVlcnkyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfanF1ZXJ5KTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIHBsdWdpbiA9ICdjb2xvcnBpY2tlcic7XG5cbl9qcXVlcnkyLmRlZmF1bHRbcGx1Z2luXSA9IF9Db2xvcnBpY2tlcjIuZGVmYXVsdDtcblxuLy8gQ29sb3JwaWNrZXIgalF1ZXJ5IFBsdWdpbiBBUElcbl9qcXVlcnkyLmRlZmF1bHQuZm5bcGx1Z2luXSA9IGZ1bmN0aW9uIChvcHRpb24pIHtcbiAgdmFyIGZuQXJncyA9IEFycmF5LnByb3RvdHlwZS5zbGljZS5jYWxsKGFyZ3VtZW50cywgMSksXG4gICAgICBpc1NpbmdsZUVsZW1lbnQgPSB0aGlzLmxlbmd0aCA9PT0gMSxcbiAgICAgIHJldHVyblZhbHVlID0gbnVsbDtcblxuICB2YXIgJGVsZW1lbnRzID0gdGhpcy5lYWNoKGZ1bmN0aW9uICgpIHtcbiAgICB2YXIgJHRoaXMgPSAoMCwgX2pxdWVyeTIuZGVmYXVsdCkodGhpcyksXG4gICAgICAgIGluc3QgPSAkdGhpcy5kYXRhKHBsdWdpbiksXG4gICAgICAgIG9wdGlvbnMgPSAodHlwZW9mIG9wdGlvbiA9PT0gJ3VuZGVmaW5lZCcgPyAndW5kZWZpbmVkJyA6IF90eXBlb2Yob3B0aW9uKSkgPT09ICdvYmplY3QnID8gb3B0aW9uIDoge307XG5cbiAgICAvLyBDcmVhdGUgaW5zdGFuY2UgaWYgZG9lcyBub3QgZXhpc3RcbiAgICBpZiAoIWluc3QpIHtcbiAgICAgIGluc3QgPSBuZXcgX0NvbG9ycGlja2VyMi5kZWZhdWx0KHRoaXMsIG9wdGlvbnMpO1xuICAgICAgJHRoaXMuZGF0YShwbHVnaW4sIGluc3QpO1xuICAgIH1cblxuICAgIGlmICghaXNTaW5nbGVFbGVtZW50KSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgcmV0dXJuVmFsdWUgPSAkdGhpcztcblxuICAgIGlmICh0eXBlb2Ygb3B0aW9uID09PSAnc3RyaW5nJykge1xuICAgICAgaWYgKG9wdGlvbiA9PT0gJ2NvbG9ycGlja2VyJykge1xuICAgICAgICAvLyBSZXR1cm4gY29sb3JwaWNrZXIgaW5zdGFuY2U6IGUuZy4gLmNvbG9ycGlja2VyKCdjb2xvcnBpY2tlcicpXG4gICAgICAgIHJldHVyblZhbHVlID0gaW5zdDtcbiAgICAgIH0gZWxzZSBpZiAoX2pxdWVyeTIuZGVmYXVsdC5pc0Z1bmN0aW9uKGluc3Rbb3B0aW9uXSkpIHtcbiAgICAgICAgLy8gUmV0dXJuIG1ldGhvZCBjYWxsIHJldHVybiB2YWx1ZTogZS5nLiAuY29sb3JwaWNrZXIoJ2lzRW5hYmxlZCcpXG4gICAgICAgIHJldHVyblZhbHVlID0gaW5zdFtvcHRpb25dLmFwcGx5KGluc3QsIGZuQXJncyk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICAvLyBSZXR1cm4gcHJvcGVydHkgdmFsdWU6IGUuZy4gLmNvbG9ycGlja2VyKCdlbGVtZW50JylcbiAgICAgICAgcmV0dXJuVmFsdWUgPSBpbnN0W29wdGlvbl07XG4gICAgICB9XG4gICAgfVxuICB9KTtcblxuICByZXR1cm4gaXNTaW5nbGVFbGVtZW50ID8gcmV0dXJuVmFsdWUgOiAkZWxlbWVudHM7XG59O1xuXG5fanF1ZXJ5Mi5kZWZhdWx0LmZuW3BsdWdpbl0uY29uc3RydWN0b3IgPSBfQ29sb3JwaWNrZXIyLmRlZmF1bHQ7XG5cbi8qKiovIH0pLFxuLyogOCAqL1xuLyoqKi8gKGZ1bmN0aW9uKG1vZHVsZSwgZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXykge1xuXG5cInVzZSBzdHJpY3RcIjtcblxuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX2NyZWF0ZUNsYXNzID0gZnVuY3Rpb24gKCkgeyBmdW5jdGlvbiBkZWZpbmVQcm9wZXJ0aWVzKHRhcmdldCwgcHJvcHMpIHsgZm9yICh2YXIgaSA9IDA7IGkgPCBwcm9wcy5sZW5ndGg7IGkrKykgeyB2YXIgZGVzY3JpcHRvciA9IHByb3BzW2ldOyBkZXNjcmlwdG9yLmVudW1lcmFibGUgPSBkZXNjcmlwdG9yLmVudW1lcmFibGUgfHwgZmFsc2U7IGRlc2NyaXB0b3IuY29uZmlndXJhYmxlID0gdHJ1ZTsgaWYgKFwidmFsdWVcIiBpbiBkZXNjcmlwdG9yKSBkZXNjcmlwdG9yLndyaXRhYmxlID0gdHJ1ZTsgT2JqZWN0LmRlZmluZVByb3BlcnR5KHRhcmdldCwgZGVzY3JpcHRvci5rZXksIGRlc2NyaXB0b3IpOyB9IH0gcmV0dXJuIGZ1bmN0aW9uIChDb25zdHJ1Y3RvciwgcHJvdG9Qcm9wcywgc3RhdGljUHJvcHMpIHsgaWYgKHByb3RvUHJvcHMpIGRlZmluZVByb3BlcnRpZXMoQ29uc3RydWN0b3IucHJvdG90eXBlLCBwcm90b1Byb3BzKTsgaWYgKHN0YXRpY1Byb3BzKSBkZWZpbmVQcm9wZXJ0aWVzKENvbnN0cnVjdG9yLCBzdGF0aWNQcm9wcyk7IHJldHVybiBDb25zdHJ1Y3RvcjsgfTsgfSgpO1xuXG52YXIgX0V4dGVuc2lvbiA9IF9fd2VicGFja19yZXF1aXJlX18oMSk7XG5cbnZhciBfRXh0ZW5zaW9uMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX0V4dGVuc2lvbik7XG5cbnZhciBfb3B0aW9ucyA9IF9fd2VicGFja19yZXF1aXJlX18oMyk7XG5cbnZhciBfb3B0aW9uczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9vcHRpb25zKTtcblxudmFyIF9leHRlbnNpb25zID0gX193ZWJwYWNrX3JlcXVpcmVfXyg5KTtcblxudmFyIF9leHRlbnNpb25zMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2V4dGVuc2lvbnMpO1xuXG52YXIgX2pxdWVyeSA9IF9fd2VicGFja19yZXF1aXJlX18oMCk7XG5cbnZhciBfanF1ZXJ5MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2pxdWVyeSk7XG5cbnZhciBfU2xpZGVySGFuZGxlciA9IF9fd2VicGFja19yZXF1aXJlX18oMTMpO1xuXG52YXIgX1NsaWRlckhhbmRsZXIyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfU2xpZGVySGFuZGxlcik7XG5cbnZhciBfUG9wdXBIYW5kbGVyID0gX193ZWJwYWNrX3JlcXVpcmVfXygxNCk7XG5cbnZhciBfUG9wdXBIYW5kbGVyMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1BvcHVwSGFuZGxlcik7XG5cbnZhciBfSW5wdXRIYW5kbGVyID0gX193ZWJwYWNrX3JlcXVpcmVfXygxNSk7XG5cbnZhciBfSW5wdXRIYW5kbGVyMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX0lucHV0SGFuZGxlcik7XG5cbnZhciBfQ29sb3JIYW5kbGVyID0gX193ZWJwYWNrX3JlcXVpcmVfXygyMik7XG5cbnZhciBfQ29sb3JIYW5kbGVyMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX0NvbG9ySGFuZGxlcik7XG5cbnZhciBfUGlja2VySGFuZGxlciA9IF9fd2VicGFja19yZXF1aXJlX18oMjMpO1xuXG52YXIgX1BpY2tlckhhbmRsZXIyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfUGlja2VySGFuZGxlcik7XG5cbnZhciBfQWRkb25IYW5kbGVyID0gX193ZWJwYWNrX3JlcXVpcmVfXygyNCk7XG5cbnZhciBfQWRkb25IYW5kbGVyMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX0FkZG9uSGFuZGxlcik7XG5cbnZhciBfQ29sb3JJdGVtID0gX193ZWJwYWNrX3JlcXVpcmVfXygyKTtcblxudmFyIF9Db2xvckl0ZW0yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfQ29sb3JJdGVtKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxuZnVuY3Rpb24gX2NsYXNzQ2FsbENoZWNrKGluc3RhbmNlLCBDb25zdHJ1Y3RvcikgeyBpZiAoIShpbnN0YW5jZSBpbnN0YW5jZW9mIENvbnN0cnVjdG9yKSkgeyB0aHJvdyBuZXcgVHlwZUVycm9yKFwiQ2Fubm90IGNhbGwgYSBjbGFzcyBhcyBhIGZ1bmN0aW9uXCIpOyB9IH1cblxudmFyIGNvbG9yUGlja2VySWRDb3VudGVyID0gMDtcblxudmFyIHJvb3QgPSB0eXBlb2Ygc2VsZiAhPT0gJ3VuZGVmaW5lZCcgPyBzZWxmIDogdW5kZWZpbmVkOyAvLyB3aW5kb3dcblxuLyoqXG4gKiBDb2xvcnBpY2tlciB3aWRnZXQgY2xhc3NcbiAqL1xuXG52YXIgQ29sb3JwaWNrZXIgPSBmdW5jdGlvbiAoKSB7XG4gIF9jcmVhdGVDbGFzcyhDb2xvcnBpY2tlciwgW3tcbiAgICBrZXk6ICdjb2xvcicsXG5cblxuICAgIC8qKlxuICAgICAqIEludGVybmFsIGNvbG9yIG9iamVjdFxuICAgICAqXG4gICAgICogQHR5cGUge0NvbG9yfG51bGx9XG4gICAgICovXG4gICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgICByZXR1cm4gdGhpcy5jb2xvckhhbmRsZXIuY29sb3I7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogSW50ZXJuYWwgY29sb3IgZm9ybWF0XG4gICAgICpcbiAgICAgKiBAdHlwZSB7U3RyaW5nfG51bGx9XG4gICAgICovXG5cbiAgfSwge1xuICAgIGtleTogJ2Zvcm1hdCcsXG4gICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgICByZXR1cm4gdGhpcy5jb2xvckhhbmRsZXIuZm9ybWF0O1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEdldHRlciBvZiB0aGUgcGlja2VyIGVsZW1lbnRcbiAgICAgKlxuICAgICAqIEByZXR1cm5zIHtqUXVlcnl8SFRNTEVsZW1lbnR9XG4gICAgICovXG5cbiAgfSwge1xuICAgIGtleTogJ3BpY2tlcicsXG4gICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgICByZXR1cm4gdGhpcy5waWNrZXJIYW5kbGVyLnBpY2tlcjtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBAZmlyZXMgQ29sb3JwaWNrZXIjY29sb3JwaWNrZXJDcmVhdGVcbiAgICAgKiBAcGFyYW0ge09iamVjdHxTdHJpbmd9IGVsZW1lbnRcbiAgICAgKiBAcGFyYW0ge09iamVjdH0gb3B0aW9uc1xuICAgICAqIEBjb25zdHJ1Y3RvclxuICAgICAqL1xuXG4gIH1dLCBbe1xuICAgIGtleTogJ0NvbG9yJyxcblxuICAgIC8qKlxuICAgICAqIENvbG9yIGNsYXNzXG4gICAgICpcbiAgICAgKiBAc3RhdGljXG4gICAgICogQHR5cGUge0NvbG9yfVxuICAgICAqL1xuICAgIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgICAgcmV0dXJuIF9Db2xvckl0ZW0yLmRlZmF1bHQ7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogRXh0ZW5zaW9uIGNsYXNzXG4gICAgICpcbiAgICAgKiBAc3RhdGljXG4gICAgICogQHR5cGUge0V4dGVuc2lvbn1cbiAgICAgKi9cblxuICB9LCB7XG4gICAga2V5OiAnRXh0ZW5zaW9uJyxcbiAgICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICAgIHJldHVybiBfRXh0ZW5zaW9uMi5kZWZhdWx0O1xuICAgIH1cbiAgfV0pO1xuXG4gIGZ1bmN0aW9uIENvbG9ycGlja2VyKGVsZW1lbnQsIG9wdGlvbnMpIHtcbiAgICBfY2xhc3NDYWxsQ2hlY2sodGhpcywgQ29sb3JwaWNrZXIpO1xuXG4gICAgY29sb3JQaWNrZXJJZENvdW50ZXIgKz0gMTtcbiAgICAvKipcbiAgICAgKiBUaGUgY29sb3JwaWNrZXIgaW5zdGFuY2UgbnVtYmVyXG4gICAgICogQHR5cGUge251bWJlcn1cbiAgICAgKi9cbiAgICB0aGlzLmlkID0gY29sb3JQaWNrZXJJZENvdW50ZXI7XG5cbiAgICAvKipcbiAgICAgKiBMYXRlc3QgY29sb3JwaWNrZXIgZXZlbnRcbiAgICAgKlxuICAgICAqIEB0eXBlIHt7bmFtZTogU3RyaW5nLCBlOiAqfX1cbiAgICAgKi9cbiAgICB0aGlzLmxhc3RFdmVudCA9IHtcbiAgICAgIGFsaWFzOiBudWxsLFxuICAgICAgZTogbnVsbFxuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBUaGUgZWxlbWVudCB0aGF0IHRoZSBjb2xvcnBpY2tlciBpcyBib3VuZCB0b1xuICAgICAqXG4gICAgICogQHR5cGUgeyp8alF1ZXJ5fVxuICAgICAqL1xuICAgIHRoaXMuZWxlbWVudCA9ICgwLCBfanF1ZXJ5Mi5kZWZhdWx0KShlbGVtZW50KS5hZGRDbGFzcygnY29sb3JwaWNrZXItZWxlbWVudCcpLmF0dHIoJ2RhdGEtY29sb3JwaWNrZXItaWQnLCB0aGlzLmlkKTtcblxuICAgIC8qKlxuICAgICAqIEB0eXBlIHtkZWZhdWx0c31cbiAgICAgKi9cbiAgICB0aGlzLm9wdGlvbnMgPSBfanF1ZXJ5Mi5kZWZhdWx0LmV4dGVuZCh0cnVlLCB7fSwgX29wdGlvbnMyLmRlZmF1bHQsIG9wdGlvbnMsIHRoaXMuZWxlbWVudC5kYXRhKCkpO1xuXG4gICAgLyoqXG4gICAgICogQHR5cGUge2Jvb2xlYW59XG4gICAgICogQHByaXZhdGVcbiAgICAgKi9cbiAgICB0aGlzLmRpc2FibGVkID0gZmFsc2U7XG5cbiAgICAvKipcbiAgICAgKiBFeHRlbnNpb25zIGFkZGVkIHRvIHRoaXMgaW5zdGFuY2VcbiAgICAgKlxuICAgICAqIEB0eXBlIHtFeHRlbnNpb25bXX1cbiAgICAgKi9cbiAgICB0aGlzLmV4dGVuc2lvbnMgPSBbXTtcblxuICAgIC8qKlxuICAgICAqIFRoZSBlbGVtZW50IHdoZXJlIHRoZVxuICAgICAqIEB0eXBlIHsqfGpRdWVyeX1cbiAgICAgKi9cbiAgICB0aGlzLmNvbnRhaW5lciA9IHRoaXMub3B0aW9ucy5jb250YWluZXIgPT09IHRydWUgfHwgdGhpcy5vcHRpb25zLmNvbnRhaW5lciAhPT0gdHJ1ZSAmJiB0aGlzLm9wdGlvbnMuaW5saW5lID09PSB0cnVlID8gdGhpcy5lbGVtZW50IDogdGhpcy5vcHRpb25zLmNvbnRhaW5lcjtcblxuICAgIHRoaXMuY29udGFpbmVyID0gdGhpcy5jb250YWluZXIgIT09IGZhbHNlID8gKDAsIF9qcXVlcnkyLmRlZmF1bHQpKHRoaXMuY29udGFpbmVyKSA6IGZhbHNlO1xuXG4gICAgLyoqXG4gICAgICogQHR5cGUge0lucHV0SGFuZGxlcn1cbiAgICAgKi9cbiAgICB0aGlzLmlucHV0SGFuZGxlciA9IG5ldyBfSW5wdXRIYW5kbGVyMi5kZWZhdWx0KHRoaXMpO1xuICAgIC8qKlxuICAgICAqIEB0eXBlIHtDb2xvckhhbmRsZXJ9XG4gICAgICovXG4gICAgdGhpcy5jb2xvckhhbmRsZXIgPSBuZXcgX0NvbG9ySGFuZGxlcjIuZGVmYXVsdCh0aGlzKTtcbiAgICAvKipcbiAgICAgKiBAdHlwZSB7U2xpZGVySGFuZGxlcn1cbiAgICAgKi9cbiAgICB0aGlzLnNsaWRlckhhbmRsZXIgPSBuZXcgX1NsaWRlckhhbmRsZXIyLmRlZmF1bHQodGhpcyk7XG4gICAgLyoqXG4gICAgICogQHR5cGUge1BvcHVwSGFuZGxlcn1cbiAgICAgKi9cbiAgICB0aGlzLnBvcHVwSGFuZGxlciA9IG5ldyBfUG9wdXBIYW5kbGVyMi5kZWZhdWx0KHRoaXMsIHJvb3QpO1xuICAgIC8qKlxuICAgICAqIEB0eXBlIHtQaWNrZXJIYW5kbGVyfVxuICAgICAqL1xuICAgIHRoaXMucGlja2VySGFuZGxlciA9IG5ldyBfUGlja2VySGFuZGxlcjIuZGVmYXVsdCh0aGlzKTtcbiAgICAvKipcbiAgICAgKiBAdHlwZSB7QWRkb25IYW5kbGVyfVxuICAgICAqL1xuICAgIHRoaXMuYWRkb25IYW5kbGVyID0gbmV3IF9BZGRvbkhhbmRsZXIyLmRlZmF1bHQodGhpcyk7XG5cbiAgICB0aGlzLmluaXQoKTtcblxuICAgIC8vIEVtaXQgYSBjcmVhdGUgZXZlbnRcbiAgICAoMCwgX2pxdWVyeTIuZGVmYXVsdCkoX2pxdWVyeTIuZGVmYXVsdC5wcm94eShmdW5jdGlvbiAoKSB7XG4gICAgICAvKipcbiAgICAgICAqIChDb2xvcnBpY2tlcikgV2hlbiB0aGUgQ29sb3JwaWNrZXIgaW5zdGFuY2UgaGFzIGJlZW4gY3JlYXRlZCBhbmQgdGhlIERPTSBpcyByZWFkeS5cbiAgICAgICAqXG4gICAgICAgKiBAZXZlbnQgQ29sb3JwaWNrZXIjY29sb3JwaWNrZXJDcmVhdGVcbiAgICAgICAqL1xuICAgICAgdGhpcy50cmlnZ2VyKCdjb2xvcnBpY2tlckNyZWF0ZScpO1xuICAgIH0sIHRoaXMpKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBJbml0aWFsaXplcyB0aGUgcGx1Z2luXG4gICAqIEBwcml2YXRlXG4gICAqL1xuXG5cbiAgX2NyZWF0ZUNsYXNzKENvbG9ycGlja2VyLCBbe1xuICAgIGtleTogJ2luaXQnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBpbml0KCkge1xuICAgICAgLy8gSW5pdCBhZGRvblxuICAgICAgdGhpcy5hZGRvbkhhbmRsZXIuYmluZCgpO1xuXG4gICAgICAvLyBJbml0IGlucHV0XG4gICAgICB0aGlzLmlucHV0SGFuZGxlci5iaW5kKCk7XG5cbiAgICAgIC8vIEluaXQgZXh0ZW5zaW9ucyAoYmVmb3JlIGluaXRpYWxpemluZyB0aGUgY29sb3IpXG4gICAgICB0aGlzLmluaXRFeHRlbnNpb25zKCk7XG5cbiAgICAgIC8vIEluaXQgY29sb3JcbiAgICAgIHRoaXMuY29sb3JIYW5kbGVyLmJpbmQoKTtcblxuICAgICAgLy8gSW5pdCBwaWNrZXJcbiAgICAgIHRoaXMucGlja2VySGFuZGxlci5iaW5kKCk7XG5cbiAgICAgIC8vIEluaXQgc2xpZGVycyBhbmQgcG9wdXBcbiAgICAgIHRoaXMuc2xpZGVySGFuZGxlci5iaW5kKCk7XG4gICAgICB0aGlzLnBvcHVwSGFuZGxlci5iaW5kKCk7XG5cbiAgICAgIC8vIEluamVjdCBpbnRvIHRoZSBET00gKHRoaXMgbWF5IG1ha2UgaXQgdmlzaWJsZSlcbiAgICAgIHRoaXMucGlja2VySGFuZGxlci5hdHRhY2goKTtcblxuICAgICAgLy8gVXBkYXRlIGFsbCBjb21wb25lbnRzXG4gICAgICB0aGlzLnVwZGF0ZSgpO1xuXG4gICAgICBpZiAodGhpcy5pbnB1dEhhbmRsZXIuaXNEaXNhYmxlZCgpKSB7XG4gICAgICAgIHRoaXMuZGlzYWJsZSgpO1xuICAgICAgfVxuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEluaXRpYWxpemVzIHRoZSBwbHVnaW4gZXh0ZW5zaW9uc1xuICAgICAqIEBwcml2YXRlXG4gICAgICovXG5cbiAgfSwge1xuICAgIGtleTogJ2luaXRFeHRlbnNpb25zJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gaW5pdEV4dGVuc2lvbnMoKSB7XG4gICAgICB2YXIgX3RoaXMgPSB0aGlzO1xuXG4gICAgICBpZiAoIUFycmF5LmlzQXJyYXkodGhpcy5vcHRpb25zLmV4dGVuc2lvbnMpKSB7XG4gICAgICAgIHRoaXMub3B0aW9ucy5leHRlbnNpb25zID0gW107XG4gICAgICB9XG5cbiAgICAgIGlmICh0aGlzLm9wdGlvbnMuZGVidWcpIHtcbiAgICAgICAgdGhpcy5vcHRpb25zLmV4dGVuc2lvbnMucHVzaCh7IG5hbWU6ICdkZWJ1Z2dlcicgfSk7XG4gICAgICB9XG5cbiAgICAgIC8vIFJlZ2lzdGVyIGFuZCBpbnN0YW50aWF0ZSBleHRlbnNpb25zXG4gICAgICB0aGlzLm9wdGlvbnMuZXh0ZW5zaW9ucy5mb3JFYWNoKGZ1bmN0aW9uIChleHQpIHtcbiAgICAgICAgX3RoaXMucmVnaXN0ZXJFeHRlbnNpb24oQ29sb3JwaWNrZXIuZXh0ZW5zaW9uc1tleHQubmFtZS50b0xvd2VyQ2FzZSgpXSwgZXh0Lm9wdGlvbnMgfHwge30pO1xuICAgICAgfSk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogQ3JlYXRlcyBhbmQgcmVnaXN0ZXJzIHRoZSBnaXZlbiBleHRlbnNpb25cbiAgICAgKlxuICAgICAqIEBwYXJhbSB7RXh0ZW5zaW9ufSBFeHRlbnNpb25DbGFzcyBUaGUgZXh0ZW5zaW9uIGNsYXNzIHRvIGluc3RhbnRpYXRlXG4gICAgICogQHBhcmFtIHtPYmplY3R9IFtjb25maWddIEV4dGVuc2lvbiBjb25maWd1cmF0aW9uXG4gICAgICogQHJldHVybnMge0V4dGVuc2lvbn1cbiAgICAgKi9cblxuICB9LCB7XG4gICAga2V5OiAncmVnaXN0ZXJFeHRlbnNpb24nLFxuICAgIHZhbHVlOiBmdW5jdGlvbiByZWdpc3RlckV4dGVuc2lvbihFeHRlbnNpb25DbGFzcykge1xuICAgICAgdmFyIGNvbmZpZyA9IGFyZ3VtZW50cy5sZW5ndGggPiAxICYmIGFyZ3VtZW50c1sxXSAhPT0gdW5kZWZpbmVkID8gYXJndW1lbnRzWzFdIDoge307XG5cbiAgICAgIHZhciBleHQgPSBuZXcgRXh0ZW5zaW9uQ2xhc3ModGhpcywgY29uZmlnKTtcblxuICAgICAgdGhpcy5leHRlbnNpb25zLnB1c2goZXh0KTtcbiAgICAgIHJldHVybiBleHQ7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogRGVzdHJveXMgdGhlIGN1cnJlbnQgaW5zdGFuY2VcbiAgICAgKlxuICAgICAqIEBmaXJlcyBDb2xvcnBpY2tlciNjb2xvcnBpY2tlckRlc3Ryb3lcbiAgICAgKi9cblxuICB9LCB7XG4gICAga2V5OiAnZGVzdHJveScsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGRlc3Ryb3koKSB7XG4gICAgICB2YXIgY29sb3IgPSB0aGlzLmNvbG9yO1xuXG4gICAgICB0aGlzLnNsaWRlckhhbmRsZXIudW5iaW5kKCk7XG4gICAgICB0aGlzLmlucHV0SGFuZGxlci51bmJpbmQoKTtcbiAgICAgIHRoaXMucG9wdXBIYW5kbGVyLnVuYmluZCgpO1xuICAgICAgdGhpcy5jb2xvckhhbmRsZXIudW5iaW5kKCk7XG4gICAgICB0aGlzLmFkZG9uSGFuZGxlci51bmJpbmQoKTtcbiAgICAgIHRoaXMucGlja2VySGFuZGxlci51bmJpbmQoKTtcblxuICAgICAgdGhpcy5lbGVtZW50LnJlbW92ZUNsYXNzKCdjb2xvcnBpY2tlci1lbGVtZW50JykucmVtb3ZlRGF0YSgnY29sb3JwaWNrZXInLCAnY29sb3InKS5vZmYoJy5jb2xvcnBpY2tlcicpO1xuXG4gICAgICAvKipcbiAgICAgICAqIChDb2xvcnBpY2tlcikgV2hlbiB0aGUgaW5zdGFuY2UgaXMgZGVzdHJveWVkIHdpdGggYWxsIGV2ZW50cyB1bmJvdW5kLlxuICAgICAgICpcbiAgICAgICAqIEBldmVudCBDb2xvcnBpY2tlciNjb2xvcnBpY2tlckRlc3Ryb3lcbiAgICAgICAqL1xuICAgICAgdGhpcy50cmlnZ2VyKCdjb2xvcnBpY2tlckRlc3Ryb3knLCBjb2xvcik7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogU2hvd3MgdGhlIGNvbG9ycGlja2VyIHdpZGdldCBpZiBoaWRkZW4uXG4gICAgICogSWYgdGhlIGNvbG9ycGlja2VyIGlzIGRpc2FibGVkIHRoaXMgY2FsbCB3aWxsIGJlIGlnbm9yZWQuXG4gICAgICpcbiAgICAgKiBAZmlyZXMgQ29sb3JwaWNrZXIjY29sb3JwaWNrZXJTaG93XG4gICAgICogQHBhcmFtIHtFdmVudH0gW2VdXG4gICAgICovXG5cbiAgfSwge1xuICAgIGtleTogJ3Nob3cnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBzaG93KGUpIHtcbiAgICAgIHRoaXMucG9wdXBIYW5kbGVyLnNob3coZSk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogSGlkZXMgdGhlIGNvbG9ycGlja2VyIHdpZGdldC5cbiAgICAgKlxuICAgICAqIEBmaXJlcyBDb2xvcnBpY2tlciNjb2xvcnBpY2tlckhpZGVcbiAgICAgKiBAcGFyYW0ge0V2ZW50fSBbZV1cbiAgICAgKi9cblxuICB9LCB7XG4gICAga2V5OiAnaGlkZScsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGhpZGUoZSkge1xuICAgICAgdGhpcy5wb3B1cEhhbmRsZXIuaGlkZShlKTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBUb2dnbGVzIHRoZSBjb2xvcnBpY2tlciBiZXR3ZWVuIHZpc2libGUgYW5kIGhpZGRlbi5cbiAgICAgKlxuICAgICAqIEBmaXJlcyBDb2xvcnBpY2tlciNjb2xvcnBpY2tlclNob3dcbiAgICAgKiBAZmlyZXMgQ29sb3JwaWNrZXIjY29sb3JwaWNrZXJIaWRlXG4gICAgICogQHBhcmFtIHtFdmVudH0gW2VdXG4gICAgICovXG5cbiAgfSwge1xuICAgIGtleTogJ3RvZ2dsZScsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHRvZ2dsZShlKSB7XG4gICAgICB0aGlzLnBvcHVwSGFuZGxlci50b2dnbGUoZSk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogUmV0dXJucyB0aGUgY3VycmVudCBjb2xvciB2YWx1ZSBhcyBzdHJpbmdcbiAgICAgKlxuICAgICAqIEBwYXJhbSB7U3RyaW5nfCp9IFtkZWZhdWx0VmFsdWVdXG4gICAgICogQHJldHVybnMge1N0cmluZ3wqfVxuICAgICAqL1xuXG4gIH0sIHtcbiAgICBrZXk6ICdnZXRWYWx1ZScsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGdldFZhbHVlKCkge1xuICAgICAgdmFyIGRlZmF1bHRWYWx1ZSA9IGFyZ3VtZW50cy5sZW5ndGggPiAwICYmIGFyZ3VtZW50c1swXSAhPT0gdW5kZWZpbmVkID8gYXJndW1lbnRzWzBdIDogbnVsbDtcblxuICAgICAgdmFyIHZhbCA9IHRoaXMuY29sb3JIYW5kbGVyLmNvbG9yO1xuXG4gICAgICB2YWwgPSB2YWwgaW5zdGFuY2VvZiBfQ29sb3JJdGVtMi5kZWZhdWx0ID8gdmFsIDogZGVmYXVsdFZhbHVlO1xuXG4gICAgICBpZiAodmFsIGluc3RhbmNlb2YgX0NvbG9ySXRlbTIuZGVmYXVsdCkge1xuICAgICAgICByZXR1cm4gdmFsLnN0cmluZyh0aGlzLmZvcm1hdCk7XG4gICAgICB9XG5cbiAgICAgIHJldHVybiB2YWw7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogU2V0cyB0aGUgY29sb3IgbWFudWFsbHlcbiAgICAgKlxuICAgICAqIEBmaXJlcyBDb2xvcnBpY2tlciNjb2xvcnBpY2tlckNoYW5nZVxuICAgICAqIEBwYXJhbSB7U3RyaW5nfENvbG9yfSB2YWxcbiAgICAgKi9cblxuICB9LCB7XG4gICAga2V5OiAnc2V0VmFsdWUnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBzZXRWYWx1ZSh2YWwpIHtcbiAgICAgIGlmICh0aGlzLmlzRGlzYWJsZWQoKSkge1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG4gICAgICB2YXIgY2ggPSB0aGlzLmNvbG9ySGFuZGxlcjtcblxuICAgICAgaWYgKGNoLmhhc0NvbG9yKCkgJiYgISF2YWwgJiYgY2guY29sb3IuZXF1YWxzKHZhbCkgfHwgIWNoLmhhc0NvbG9yKCkgJiYgIXZhbCkge1xuICAgICAgICAvLyBzYW1lIGNvbG9yIG9yIHN0aWxsIGVtcHR5XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cblxuICAgICAgY2guY29sb3IgPSB2YWwgPyBjaC5jcmVhdGVDb2xvcih2YWwsIHRoaXMub3B0aW9ucy5hdXRvSW5wdXRGYWxsYmFjaykgOiBudWxsO1xuXG4gICAgICAvKipcbiAgICAgICAqIChDb2xvcnBpY2tlcikgV2hlbiB0aGUgY29sb3IgaXMgc2V0IHByb2dyYW1tYXRpY2FsbHkgd2l0aCBzZXRWYWx1ZSgpLlxuICAgICAgICpcbiAgICAgICAqIEBldmVudCBDb2xvcnBpY2tlciNjb2xvcnBpY2tlckNoYW5nZVxuICAgICAgICovXG4gICAgICB0aGlzLnRyaWdnZXIoJ2NvbG9ycGlja2VyQ2hhbmdlJywgY2guY29sb3IsIHZhbCk7XG5cbiAgICAgIC8vIGZvcmNlIHVwZGF0ZSBpZiBjb2xvciBoYXMgY2hhbmdlZCB0byBlbXB0eVxuICAgICAgdGhpcy51cGRhdGUoKTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBVcGRhdGVzIHRoZSBVSSBhbmQgdGhlIGlucHV0IGNvbG9yIGFjY29yZGluZyB0byB0aGUgaW50ZXJuYWwgY29sb3IuXG4gICAgICpcbiAgICAgKiBAZmlyZXMgQ29sb3JwaWNrZXIjY29sb3JwaWNrZXJVcGRhdGVcbiAgICAgKi9cblxuICB9LCB7XG4gICAga2V5OiAndXBkYXRlJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gdXBkYXRlKCkge1xuICAgICAgaWYgKHRoaXMuY29sb3JIYW5kbGVyLmhhc0NvbG9yKCkpIHtcbiAgICAgICAgdGhpcy5pbnB1dEhhbmRsZXIudXBkYXRlKCk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICB0aGlzLmNvbG9ySGFuZGxlci5hc3N1cmVDb2xvcigpO1xuICAgICAgfVxuXG4gICAgICB0aGlzLmFkZG9uSGFuZGxlci51cGRhdGUoKTtcbiAgICAgIHRoaXMucGlja2VySGFuZGxlci51cGRhdGUoKTtcblxuICAgICAgLyoqXG4gICAgICAgKiAoQ29sb3JwaWNrZXIpIEZpcmVkIHdoZW4gdGhlIHdpZGdldCBpcyB1cGRhdGVkLlxuICAgICAgICpcbiAgICAgICAqIEBldmVudCBDb2xvcnBpY2tlciNjb2xvcnBpY2tlclVwZGF0ZVxuICAgICAgICovXG4gICAgICB0aGlzLnRyaWdnZXIoJ2NvbG9ycGlja2VyVXBkYXRlJyk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogRW5hYmxlcyB0aGUgd2lkZ2V0IGFuZCB0aGUgaW5wdXQgaWYgYW55XG4gICAgICpcbiAgICAgKiBAZmlyZXMgQ29sb3JwaWNrZXIjY29sb3JwaWNrZXJFbmFibGVcbiAgICAgKiBAcmV0dXJucyB7Ym9vbGVhbn1cbiAgICAgKi9cblxuICB9LCB7XG4gICAga2V5OiAnZW5hYmxlJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gZW5hYmxlKCkge1xuICAgICAgdGhpcy5pbnB1dEhhbmRsZXIuZW5hYmxlKCk7XG4gICAgICB0aGlzLmRpc2FibGVkID0gZmFsc2U7XG4gICAgICB0aGlzLnBpY2tlci5yZW1vdmVDbGFzcygnY29sb3JwaWNrZXItZGlzYWJsZWQnKTtcblxuICAgICAgLyoqXG4gICAgICAgKiAoQ29sb3JwaWNrZXIpIFdoZW4gdGhlIHdpZGdldCBoYXMgYmVlbiBlbmFibGVkLlxuICAgICAgICpcbiAgICAgICAqIEBldmVudCBDb2xvcnBpY2tlciNjb2xvcnBpY2tlckVuYWJsZVxuICAgICAgICovXG4gICAgICB0aGlzLnRyaWdnZXIoJ2NvbG9ycGlja2VyRW5hYmxlJyk7XG4gICAgICByZXR1cm4gdHJ1ZTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBEaXNhYmxlcyB0aGUgd2lkZ2V0IGFuZCB0aGUgaW5wdXQgaWYgYW55XG4gICAgICpcbiAgICAgKiBAZmlyZXMgQ29sb3JwaWNrZXIjY29sb3JwaWNrZXJEaXNhYmxlXG4gICAgICogQHJldHVybnMge2Jvb2xlYW59XG4gICAgICovXG5cbiAgfSwge1xuICAgIGtleTogJ2Rpc2FibGUnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBkaXNhYmxlKCkge1xuICAgICAgdGhpcy5pbnB1dEhhbmRsZXIuZGlzYWJsZSgpO1xuICAgICAgdGhpcy5kaXNhYmxlZCA9IHRydWU7XG4gICAgICB0aGlzLnBpY2tlci5hZGRDbGFzcygnY29sb3JwaWNrZXItZGlzYWJsZWQnKTtcblxuICAgICAgLyoqXG4gICAgICAgKiAoQ29sb3JwaWNrZXIpIFdoZW4gdGhlIHdpZGdldCBoYXMgYmVlbiBkaXNhYmxlZC5cbiAgICAgICAqXG4gICAgICAgKiBAZXZlbnQgQ29sb3JwaWNrZXIjY29sb3JwaWNrZXJEaXNhYmxlXG4gICAgICAgKi9cbiAgICAgIHRoaXMudHJpZ2dlcignY29sb3JwaWNrZXJEaXNhYmxlJyk7XG4gICAgICByZXR1cm4gdHJ1ZTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBSZXR1cm5zIHRydWUgaWYgdGhpcyBpbnN0YW5jZSBpcyBlbmFibGVkXG4gICAgICogQHJldHVybnMge2Jvb2xlYW59XG4gICAgICovXG5cbiAgfSwge1xuICAgIGtleTogJ2lzRW5hYmxlZCcsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGlzRW5hYmxlZCgpIHtcbiAgICAgIHJldHVybiAhdGhpcy5pc0Rpc2FibGVkKCk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogUmV0dXJucyB0cnVlIGlmIHRoaXMgaW5zdGFuY2UgaXMgZGlzYWJsZWRcbiAgICAgKiBAcmV0dXJucyB7Ym9vbGVhbn1cbiAgICAgKi9cblxuICB9LCB7XG4gICAga2V5OiAnaXNEaXNhYmxlZCcsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGlzRGlzYWJsZWQoKSB7XG4gICAgICByZXR1cm4gdGhpcy5kaXNhYmxlZCA9PT0gdHJ1ZTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBUcmlnZ2VycyBhIENvbG9ycGlja2VyIGV2ZW50LlxuICAgICAqXG4gICAgICogQHBhcmFtIGV2ZW50TmFtZVxuICAgICAqIEBwYXJhbSBjb2xvclxuICAgICAqIEBwYXJhbSB2YWx1ZVxuICAgICAqL1xuXG4gIH0sIHtcbiAgICBrZXk6ICd0cmlnZ2VyJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gdHJpZ2dlcihldmVudE5hbWUpIHtcbiAgICAgIHZhciBjb2xvciA9IGFyZ3VtZW50cy5sZW5ndGggPiAxICYmIGFyZ3VtZW50c1sxXSAhPT0gdW5kZWZpbmVkID8gYXJndW1lbnRzWzFdIDogbnVsbDtcbiAgICAgIHZhciB2YWx1ZSA9IGFyZ3VtZW50cy5sZW5ndGggPiAyICYmIGFyZ3VtZW50c1syXSAhPT0gdW5kZWZpbmVkID8gYXJndW1lbnRzWzJdIDogbnVsbDtcblxuICAgICAgdGhpcy5lbGVtZW50LnRyaWdnZXIoe1xuICAgICAgICB0eXBlOiBldmVudE5hbWUsXG4gICAgICAgIGNvbG9ycGlja2VyOiB0aGlzLFxuICAgICAgICBjb2xvcjogY29sb3IgPyBjb2xvciA6IHRoaXMuY29sb3IsXG4gICAgICAgIHZhbHVlOiB2YWx1ZSA/IHZhbHVlIDogdGhpcy5nZXRWYWx1ZSgpXG4gICAgICB9KTtcbiAgICB9XG4gIH1dKTtcblxuICByZXR1cm4gQ29sb3JwaWNrZXI7XG59KCk7XG5cbi8qKlxuICogQ29sb3JwaWNrZXIgZXh0ZW5zaW9uIGNsYXNzZXMsIGluZGV4ZWQgYnkgZXh0ZW5zaW9uIG5hbWVcbiAqXG4gKiBAc3RhdGljXG4gKiBAdHlwZSB7T2JqZWN0fSBhIG1hcCBiZXR3ZWVuIHRoZSBleHRlbnNpb24gbmFtZSBhbmQgaXRzIGNsYXNzXG4gKi9cblxuXG5Db2xvcnBpY2tlci5leHRlbnNpb25zID0gX2V4dGVuc2lvbnMyLmRlZmF1bHQ7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IENvbG9ycGlja2VyO1xubW9kdWxlLmV4cG9ydHMgPSBleHBvcnRzLmRlZmF1bHQ7XG5cbi8qKiovIH0pLFxuLyogOSAqL1xuLyoqKi8gKGZ1bmN0aW9uKG1vZHVsZSwgZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXykge1xuXG5cInVzZSBzdHJpY3RcIjtcblxuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuZXhwb3J0cy5QYWxldHRlID0gZXhwb3J0cy5Td2F0Y2hlcyA9IGV4cG9ydHMuUHJldmlldyA9IGV4cG9ydHMuRGVidWdnZXIgPSB1bmRlZmluZWQ7XG5cbnZhciBfRGVidWdnZXIgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKDEwKTtcblxudmFyIF9EZWJ1Z2dlcjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9EZWJ1Z2dlcik7XG5cbnZhciBfUHJldmlldyA9IF9fd2VicGFja19yZXF1aXJlX18oMTEpO1xuXG52YXIgX1ByZXZpZXcyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfUHJldmlldyk7XG5cbnZhciBfU3dhdGNoZXMgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKDEyKTtcblxudmFyIF9Td2F0Y2hlczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9Td2F0Y2hlcyk7XG5cbnZhciBfUGFsZXR0ZSA9IF9fd2VicGFja19yZXF1aXJlX18oNCk7XG5cbnZhciBfUGFsZXR0ZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9QYWxldHRlKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxuZXhwb3J0cy5EZWJ1Z2dlciA9IF9EZWJ1Z2dlcjIuZGVmYXVsdDtcbmV4cG9ydHMuUHJldmlldyA9IF9QcmV2aWV3Mi5kZWZhdWx0O1xuZXhwb3J0cy5Td2F0Y2hlcyA9IF9Td2F0Y2hlczIuZGVmYXVsdDtcbmV4cG9ydHMuUGFsZXR0ZSA9IF9QYWxldHRlMi5kZWZhdWx0O1xuZXhwb3J0cy5kZWZhdWx0ID0ge1xuICAnZGVidWdnZXInOiBfRGVidWdnZXIyLmRlZmF1bHQsXG4gICdwcmV2aWV3JzogX1ByZXZpZXcyLmRlZmF1bHQsXG4gICdzd2F0Y2hlcyc6IF9Td2F0Y2hlczIuZGVmYXVsdCxcbiAgJ3BhbGV0dGUnOiBfUGFsZXR0ZTIuZGVmYXVsdFxufTtcblxuLyoqKi8gfSksXG4vKiAxMCAqL1xuLyoqKi8gKGZ1bmN0aW9uKG1vZHVsZSwgZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXykge1xuXG5cInVzZSBzdHJpY3RcIjtcblxuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX2NyZWF0ZUNsYXNzID0gZnVuY3Rpb24gKCkgeyBmdW5jdGlvbiBkZWZpbmVQcm9wZXJ0aWVzKHRhcmdldCwgcHJvcHMpIHsgZm9yICh2YXIgaSA9IDA7IGkgPCBwcm9wcy5sZW5ndGg7IGkrKykgeyB2YXIgZGVzY3JpcHRvciA9IHByb3BzW2ldOyBkZXNjcmlwdG9yLmVudW1lcmFibGUgPSBkZXNjcmlwdG9yLmVudW1lcmFibGUgfHwgZmFsc2U7IGRlc2NyaXB0b3IuY29uZmlndXJhYmxlID0gdHJ1ZTsgaWYgKFwidmFsdWVcIiBpbiBkZXNjcmlwdG9yKSBkZXNjcmlwdG9yLndyaXRhYmxlID0gdHJ1ZTsgT2JqZWN0LmRlZmluZVByb3BlcnR5KHRhcmdldCwgZGVzY3JpcHRvci5rZXksIGRlc2NyaXB0b3IpOyB9IH0gcmV0dXJuIGZ1bmN0aW9uIChDb25zdHJ1Y3RvciwgcHJvdG9Qcm9wcywgc3RhdGljUHJvcHMpIHsgaWYgKHByb3RvUHJvcHMpIGRlZmluZVByb3BlcnRpZXMoQ29uc3RydWN0b3IucHJvdG90eXBlLCBwcm90b1Byb3BzKTsgaWYgKHN0YXRpY1Byb3BzKSBkZWZpbmVQcm9wZXJ0aWVzKENvbnN0cnVjdG9yLCBzdGF0aWNQcm9wcyk7IHJldHVybiBDb25zdHJ1Y3RvcjsgfTsgfSgpO1xuXG52YXIgX2dldCA9IGZ1bmN0aW9uIGdldChvYmplY3QsIHByb3BlcnR5LCByZWNlaXZlcikgeyBpZiAob2JqZWN0ID09PSBudWxsKSBvYmplY3QgPSBGdW5jdGlvbi5wcm90b3R5cGU7IHZhciBkZXNjID0gT2JqZWN0LmdldE93blByb3BlcnR5RGVzY3JpcHRvcihvYmplY3QsIHByb3BlcnR5KTsgaWYgKGRlc2MgPT09IHVuZGVmaW5lZCkgeyB2YXIgcGFyZW50ID0gT2JqZWN0LmdldFByb3RvdHlwZU9mKG9iamVjdCk7IGlmIChwYXJlbnQgPT09IG51bGwpIHsgcmV0dXJuIHVuZGVmaW5lZDsgfSBlbHNlIHsgcmV0dXJuIGdldChwYXJlbnQsIHByb3BlcnR5LCByZWNlaXZlcik7IH0gfSBlbHNlIGlmIChcInZhbHVlXCIgaW4gZGVzYykgeyByZXR1cm4gZGVzYy52YWx1ZTsgfSBlbHNlIHsgdmFyIGdldHRlciA9IGRlc2MuZ2V0OyBpZiAoZ2V0dGVyID09PSB1bmRlZmluZWQpIHsgcmV0dXJuIHVuZGVmaW5lZDsgfSByZXR1cm4gZ2V0dGVyLmNhbGwocmVjZWl2ZXIpOyB9IH07XG5cbnZhciBfRXh0ZW5zaW9uMiA9IF9fd2VicGFja19yZXF1aXJlX18oMSk7XG5cbnZhciBfRXh0ZW5zaW9uMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX0V4dGVuc2lvbjIpO1xuXG52YXIgX2pxdWVyeSA9IF9fd2VicGFja19yZXF1aXJlX18oMCk7XG5cbnZhciBfanF1ZXJ5MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2pxdWVyeSk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbmZ1bmN0aW9uIF9jbGFzc0NhbGxDaGVjayhpbnN0YW5jZSwgQ29uc3RydWN0b3IpIHsgaWYgKCEoaW5zdGFuY2UgaW5zdGFuY2VvZiBDb25zdHJ1Y3RvcikpIHsgdGhyb3cgbmV3IFR5cGVFcnJvcihcIkNhbm5vdCBjYWxsIGEgY2xhc3MgYXMgYSBmdW5jdGlvblwiKTsgfSB9XG5cbmZ1bmN0aW9uIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuKHNlbGYsIGNhbGwpIHsgaWYgKCFzZWxmKSB7IHRocm93IG5ldyBSZWZlcmVuY2VFcnJvcihcInRoaXMgaGFzbid0IGJlZW4gaW5pdGlhbGlzZWQgLSBzdXBlcigpIGhhc24ndCBiZWVuIGNhbGxlZFwiKTsgfSByZXR1cm4gY2FsbCAmJiAodHlwZW9mIGNhbGwgPT09IFwib2JqZWN0XCIgfHwgdHlwZW9mIGNhbGwgPT09IFwiZnVuY3Rpb25cIikgPyBjYWxsIDogc2VsZjsgfVxuXG5mdW5jdGlvbiBfaW5oZXJpdHMoc3ViQ2xhc3MsIHN1cGVyQ2xhc3MpIHsgaWYgKHR5cGVvZiBzdXBlckNsYXNzICE9PSBcImZ1bmN0aW9uXCIgJiYgc3VwZXJDbGFzcyAhPT0gbnVsbCkgeyB0aHJvdyBuZXcgVHlwZUVycm9yKFwiU3VwZXIgZXhwcmVzc2lvbiBtdXN0IGVpdGhlciBiZSBudWxsIG9yIGEgZnVuY3Rpb24sIG5vdCBcIiArIHR5cGVvZiBzdXBlckNsYXNzKTsgfSBzdWJDbGFzcy5wcm90b3R5cGUgPSBPYmplY3QuY3JlYXRlKHN1cGVyQ2xhc3MgJiYgc3VwZXJDbGFzcy5wcm90b3R5cGUsIHsgY29uc3RydWN0b3I6IHsgdmFsdWU6IHN1YkNsYXNzLCBlbnVtZXJhYmxlOiBmYWxzZSwgd3JpdGFibGU6IHRydWUsIGNvbmZpZ3VyYWJsZTogdHJ1ZSB9IH0pOyBpZiAoc3VwZXJDbGFzcykgT2JqZWN0LnNldFByb3RvdHlwZU9mID8gT2JqZWN0LnNldFByb3RvdHlwZU9mKHN1YkNsYXNzLCBzdXBlckNsYXNzKSA6IHN1YkNsYXNzLl9fcHJvdG9fXyA9IHN1cGVyQ2xhc3M7IH1cblxuLyoqXG4gKiBEZWJ1Z2dlciBleHRlbnNpb24gY2xhc3NcbiAqIEBhbGlhcyBEZWJ1Z2dlckV4dGVuc2lvblxuICogQGlnbm9yZVxuICovXG52YXIgRGVidWdnZXIgPSBmdW5jdGlvbiAoX0V4dGVuc2lvbikge1xuICBfaW5oZXJpdHMoRGVidWdnZXIsIF9FeHRlbnNpb24pO1xuXG4gIGZ1bmN0aW9uIERlYnVnZ2VyKGNvbG9ycGlja2VyKSB7XG4gICAgdmFyIG9wdGlvbnMgPSBhcmd1bWVudHMubGVuZ3RoID4gMSAmJiBhcmd1bWVudHNbMV0gIT09IHVuZGVmaW5lZCA/IGFyZ3VtZW50c1sxXSA6IHt9O1xuXG4gICAgX2NsYXNzQ2FsbENoZWNrKHRoaXMsIERlYnVnZ2VyKTtcblxuICAgIC8qKlxuICAgICAqIEB0eXBlIHtudW1iZXJ9XG4gICAgICovXG4gICAgdmFyIF90aGlzID0gX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4odGhpcywgKERlYnVnZ2VyLl9fcHJvdG9fXyB8fCBPYmplY3QuZ2V0UHJvdG90eXBlT2YoRGVidWdnZXIpKS5jYWxsKHRoaXMsIGNvbG9ycGlja2VyLCBvcHRpb25zKSk7XG5cbiAgICBfdGhpcy5ldmVudENvdW50ZXIgPSAwO1xuICAgIGlmIChfdGhpcy5jb2xvcnBpY2tlci5pbnB1dEhhbmRsZXIuaGFzSW5wdXQoKSkge1xuICAgICAgX3RoaXMuY29sb3JwaWNrZXIuaW5wdXRIYW5kbGVyLmlucHV0Lm9uKCdjaGFuZ2UuY29sb3JwaWNrZXItZXh0JywgX2pxdWVyeTIuZGVmYXVsdC5wcm94eShfdGhpcy5vbkNoYW5nZUlucHV0LCBfdGhpcykpO1xuICAgIH1cbiAgICByZXR1cm4gX3RoaXM7XG4gIH1cblxuICAvKipcbiAgICogQGZpcmVzIERlYnVnZ2VyRXh0ZW5zaW9uI2NvbG9ycGlja2VyRGVidWdcbiAgICogQHBhcmFtIHtzdHJpbmd9IGV2ZW50TmFtZVxuICAgKiBAcGFyYW0geyp9IGFyZ3NcbiAgICovXG5cblxuICBfY3JlYXRlQ2xhc3MoRGVidWdnZXIsIFt7XG4gICAga2V5OiAnbG9nJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gbG9nKGV2ZW50TmFtZSkge1xuICAgICAgdmFyIF9jb25zb2xlO1xuXG4gICAgICBmb3IgKHZhciBfbGVuID0gYXJndW1lbnRzLmxlbmd0aCwgYXJncyA9IEFycmF5KF9sZW4gPiAxID8gX2xlbiAtIDEgOiAwKSwgX2tleSA9IDE7IF9rZXkgPCBfbGVuOyBfa2V5KyspIHtcbiAgICAgICAgYXJnc1tfa2V5IC0gMV0gPSBhcmd1bWVudHNbX2tleV07XG4gICAgICB9XG5cbiAgICAgIHRoaXMuZXZlbnRDb3VudGVyICs9IDE7XG5cbiAgICAgIHZhciBsb2dNZXNzYWdlID0gJyMnICsgdGhpcy5ldmVudENvdW50ZXIgKyAnOiBDb2xvcnBpY2tlciMnICsgdGhpcy5jb2xvcnBpY2tlci5pZCArICcgWycgKyBldmVudE5hbWUgKyAnXSc7XG5cbiAgICAgIChfY29uc29sZSA9IGNvbnNvbGUpLmRlYnVnLmFwcGx5KF9jb25zb2xlLCBbbG9nTWVzc2FnZV0uY29uY2F0KGFyZ3MpKTtcblxuICAgICAgLyoqXG4gICAgICAgKiBXaGVuZXZlciB0aGUgZGVidWdnZXIgbG9ncyBhbiBldmVudCwgdGhpcyBvdGhlciBldmVudCBpcyBlbWl0dGVkLlxuICAgICAgICpcbiAgICAgICAqIEBldmVudCBEZWJ1Z2dlckV4dGVuc2lvbiNjb2xvcnBpY2tlckRlYnVnXG4gICAgICAgKiBAdHlwZSB7b2JqZWN0fSBUaGUgZXZlbnQgb2JqZWN0XG4gICAgICAgKiBAcHJvcGVydHkge0NvbG9ycGlja2VyfSBjb2xvcnBpY2tlciBUaGUgQ29sb3JwaWNrZXIgaW5zdGFuY2VcbiAgICAgICAqIEBwcm9wZXJ0eSB7Q29sb3JJdGVtfSBjb2xvciBUaGUgY29sb3IgaW5zdGFuY2VcbiAgICAgICAqIEBwcm9wZXJ0eSB7e2RlYnVnZ2VyOiBEZWJ1Z2dlckV4dGVuc2lvbiwgZXZlbnROYW1lOiBTdHJpbmcsIGxvZ0FyZ3M6IEFycmF5LCBsb2dNZXNzYWdlOiBTdHJpbmd9fSBkZWJ1Z1xuICAgICAgICogIFRoZSBkZWJ1ZyBpbmZvXG4gICAgICAgKi9cbiAgICAgIHRoaXMuY29sb3JwaWNrZXIuZWxlbWVudC50cmlnZ2VyKHtcbiAgICAgICAgdHlwZTogJ2NvbG9ycGlja2VyRGVidWcnLFxuICAgICAgICBjb2xvcnBpY2tlcjogdGhpcy5jb2xvcnBpY2tlcixcbiAgICAgICAgY29sb3I6IHRoaXMuY29sb3IsXG4gICAgICAgIHZhbHVlOiBudWxsLFxuICAgICAgICBkZWJ1Zzoge1xuICAgICAgICAgIGRlYnVnZ2VyOiB0aGlzLFxuICAgICAgICAgIGV2ZW50TmFtZTogZXZlbnROYW1lLFxuICAgICAgICAgIGxvZ0FyZ3M6IGFyZ3MsXG4gICAgICAgICAgbG9nTWVzc2FnZTogbG9nTWVzc2FnZVxuICAgICAgICB9XG4gICAgICB9KTtcbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdyZXNvbHZlQ29sb3InLFxuICAgIHZhbHVlOiBmdW5jdGlvbiByZXNvbHZlQ29sb3IoY29sb3IpIHtcbiAgICAgIHZhciByZWFsQ29sb3IgPSBhcmd1bWVudHMubGVuZ3RoID4gMSAmJiBhcmd1bWVudHNbMV0gIT09IHVuZGVmaW5lZCA/IGFyZ3VtZW50c1sxXSA6IHRydWU7XG5cbiAgICAgIHRoaXMubG9nKCdyZXNvbHZlQ29sb3IoKScsIGNvbG9yLCByZWFsQ29sb3IpO1xuICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ29uQ3JlYXRlJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gb25DcmVhdGUoZXZlbnQpIHtcbiAgICAgIHRoaXMubG9nKCdjb2xvcnBpY2tlckNyZWF0ZScpO1xuICAgICAgcmV0dXJuIF9nZXQoRGVidWdnZXIucHJvdG90eXBlLl9fcHJvdG9fXyB8fCBPYmplY3QuZ2V0UHJvdG90eXBlT2YoRGVidWdnZXIucHJvdG90eXBlKSwgJ29uQ3JlYXRlJywgdGhpcykuY2FsbCh0aGlzLCBldmVudCk7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnb25EZXN0cm95JyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gb25EZXN0cm95KGV2ZW50KSB7XG4gICAgICB0aGlzLmxvZygnY29sb3JwaWNrZXJEZXN0cm95Jyk7XG4gICAgICB0aGlzLmV2ZW50Q291bnRlciA9IDA7XG5cbiAgICAgIGlmICh0aGlzLmNvbG9ycGlja2VyLmlucHV0SGFuZGxlci5oYXNJbnB1dCgpKSB7XG4gICAgICAgIHRoaXMuY29sb3JwaWNrZXIuaW5wdXRIYW5kbGVyLmlucHV0Lm9mZignLmNvbG9ycGlja2VyLWV4dCcpO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gX2dldChEZWJ1Z2dlci5wcm90b3R5cGUuX19wcm90b19fIHx8IE9iamVjdC5nZXRQcm90b3R5cGVPZihEZWJ1Z2dlci5wcm90b3R5cGUpLCAnb25EZXN0cm95JywgdGhpcykuY2FsbCh0aGlzLCBldmVudCk7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnb25VcGRhdGUnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBvblVwZGF0ZShldmVudCkge1xuICAgICAgdGhpcy5sb2coJ2NvbG9ycGlja2VyVXBkYXRlJyk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogQGxpc3RlbnMgQ29sb3JwaWNrZXIjY2hhbmdlXG4gICAgICogQHBhcmFtIHtFdmVudH0gZXZlbnRcbiAgICAgKi9cblxuICB9LCB7XG4gICAga2V5OiAnb25DaGFuZ2VJbnB1dCcsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIG9uQ2hhbmdlSW5wdXQoZXZlbnQpIHtcbiAgICAgIHRoaXMubG9nKCdpbnB1dDpjaGFuZ2UuY29sb3JwaWNrZXInLCBldmVudC52YWx1ZSwgZXZlbnQuY29sb3IpO1xuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ29uQ2hhbmdlJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gb25DaGFuZ2UoZXZlbnQpIHtcbiAgICAgIHRoaXMubG9nKCdjb2xvcnBpY2tlckNoYW5nZScsIGV2ZW50LnZhbHVlLCBldmVudC5jb2xvcik7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnb25JbnZhbGlkJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gb25JbnZhbGlkKGV2ZW50KSB7XG4gICAgICB0aGlzLmxvZygnY29sb3JwaWNrZXJJbnZhbGlkJywgZXZlbnQudmFsdWUsIGV2ZW50LmNvbG9yKTtcbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdvbkhpZGUnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBvbkhpZGUoZXZlbnQpIHtcbiAgICAgIHRoaXMubG9nKCdjb2xvcnBpY2tlckhpZGUnKTtcbiAgICAgIHRoaXMuZXZlbnRDb3VudGVyID0gMDtcbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdvblNob3cnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBvblNob3coZXZlbnQpIHtcbiAgICAgIHRoaXMubG9nKCdjb2xvcnBpY2tlclNob3cnKTtcbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdvbkRpc2FibGUnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBvbkRpc2FibGUoZXZlbnQpIHtcbiAgICAgIHRoaXMubG9nKCdjb2xvcnBpY2tlckRpc2FibGUnKTtcbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdvbkVuYWJsZScsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIG9uRW5hYmxlKGV2ZW50KSB7XG4gICAgICB0aGlzLmxvZygnY29sb3JwaWNrZXJFbmFibGUnKTtcbiAgICB9XG4gIH1dKTtcblxuICByZXR1cm4gRGVidWdnZXI7XG59KF9FeHRlbnNpb24zLmRlZmF1bHQpO1xuXG5leHBvcnRzLmRlZmF1bHQgPSBEZWJ1Z2dlcjtcbm1vZHVsZS5leHBvcnRzID0gZXhwb3J0cy5kZWZhdWx0O1xuXG4vKioqLyB9KSxcbi8qIDExICovXG4vKioqLyAoZnVuY3Rpb24obW9kdWxlLCBleHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKSB7XG5cblwidXNlIHN0cmljdFwiO1xuXG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfY3JlYXRlQ2xhc3MgPSBmdW5jdGlvbiAoKSB7IGZ1bmN0aW9uIGRlZmluZVByb3BlcnRpZXModGFyZ2V0LCBwcm9wcykgeyBmb3IgKHZhciBpID0gMDsgaSA8IHByb3BzLmxlbmd0aDsgaSsrKSB7IHZhciBkZXNjcmlwdG9yID0gcHJvcHNbaV07IGRlc2NyaXB0b3IuZW51bWVyYWJsZSA9IGRlc2NyaXB0b3IuZW51bWVyYWJsZSB8fCBmYWxzZTsgZGVzY3JpcHRvci5jb25maWd1cmFibGUgPSB0cnVlOyBpZiAoXCJ2YWx1ZVwiIGluIGRlc2NyaXB0b3IpIGRlc2NyaXB0b3Iud3JpdGFibGUgPSB0cnVlOyBPYmplY3QuZGVmaW5lUHJvcGVydHkodGFyZ2V0LCBkZXNjcmlwdG9yLmtleSwgZGVzY3JpcHRvcik7IH0gfSByZXR1cm4gZnVuY3Rpb24gKENvbnN0cnVjdG9yLCBwcm90b1Byb3BzLCBzdGF0aWNQcm9wcykgeyBpZiAocHJvdG9Qcm9wcykgZGVmaW5lUHJvcGVydGllcyhDb25zdHJ1Y3Rvci5wcm90b3R5cGUsIHByb3RvUHJvcHMpOyBpZiAoc3RhdGljUHJvcHMpIGRlZmluZVByb3BlcnRpZXMoQ29uc3RydWN0b3IsIHN0YXRpY1Byb3BzKTsgcmV0dXJuIENvbnN0cnVjdG9yOyB9OyB9KCk7XG5cbnZhciBfZ2V0ID0gZnVuY3Rpb24gZ2V0KG9iamVjdCwgcHJvcGVydHksIHJlY2VpdmVyKSB7IGlmIChvYmplY3QgPT09IG51bGwpIG9iamVjdCA9IEZ1bmN0aW9uLnByb3RvdHlwZTsgdmFyIGRlc2MgPSBPYmplY3QuZ2V0T3duUHJvcGVydHlEZXNjcmlwdG9yKG9iamVjdCwgcHJvcGVydHkpOyBpZiAoZGVzYyA9PT0gdW5kZWZpbmVkKSB7IHZhciBwYXJlbnQgPSBPYmplY3QuZ2V0UHJvdG90eXBlT2Yob2JqZWN0KTsgaWYgKHBhcmVudCA9PT0gbnVsbCkgeyByZXR1cm4gdW5kZWZpbmVkOyB9IGVsc2UgeyByZXR1cm4gZ2V0KHBhcmVudCwgcHJvcGVydHksIHJlY2VpdmVyKTsgfSB9IGVsc2UgaWYgKFwidmFsdWVcIiBpbiBkZXNjKSB7IHJldHVybiBkZXNjLnZhbHVlOyB9IGVsc2UgeyB2YXIgZ2V0dGVyID0gZGVzYy5nZXQ7IGlmIChnZXR0ZXIgPT09IHVuZGVmaW5lZCkgeyByZXR1cm4gdW5kZWZpbmVkOyB9IHJldHVybiBnZXR0ZXIuY2FsbChyZWNlaXZlcik7IH0gfTtcblxudmFyIF9FeHRlbnNpb24yID0gX193ZWJwYWNrX3JlcXVpcmVfXygxKTtcblxudmFyIF9FeHRlbnNpb24zID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfRXh0ZW5zaW9uMik7XG5cbnZhciBfanF1ZXJ5ID0gX193ZWJwYWNrX3JlcXVpcmVfXygwKTtcblxudmFyIF9qcXVlcnkyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfanF1ZXJ5KTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxuZnVuY3Rpb24gX2NsYXNzQ2FsbENoZWNrKGluc3RhbmNlLCBDb25zdHJ1Y3RvcikgeyBpZiAoIShpbnN0YW5jZSBpbnN0YW5jZW9mIENvbnN0cnVjdG9yKSkgeyB0aHJvdyBuZXcgVHlwZUVycm9yKFwiQ2Fubm90IGNhbGwgYSBjbGFzcyBhcyBhIGZ1bmN0aW9uXCIpOyB9IH1cblxuZnVuY3Rpb24gX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4oc2VsZiwgY2FsbCkgeyBpZiAoIXNlbGYpIHsgdGhyb3cgbmV3IFJlZmVyZW5jZUVycm9yKFwidGhpcyBoYXNuJ3QgYmVlbiBpbml0aWFsaXNlZCAtIHN1cGVyKCkgaGFzbid0IGJlZW4gY2FsbGVkXCIpOyB9IHJldHVybiBjYWxsICYmICh0eXBlb2YgY2FsbCA9PT0gXCJvYmplY3RcIiB8fCB0eXBlb2YgY2FsbCA9PT0gXCJmdW5jdGlvblwiKSA/IGNhbGwgOiBzZWxmOyB9XG5cbmZ1bmN0aW9uIF9pbmhlcml0cyhzdWJDbGFzcywgc3VwZXJDbGFzcykgeyBpZiAodHlwZW9mIHN1cGVyQ2xhc3MgIT09IFwiZnVuY3Rpb25cIiAmJiBzdXBlckNsYXNzICE9PSBudWxsKSB7IHRocm93IG5ldyBUeXBlRXJyb3IoXCJTdXBlciBleHByZXNzaW9uIG11c3QgZWl0aGVyIGJlIG51bGwgb3IgYSBmdW5jdGlvbiwgbm90IFwiICsgdHlwZW9mIHN1cGVyQ2xhc3MpOyB9IHN1YkNsYXNzLnByb3RvdHlwZSA9IE9iamVjdC5jcmVhdGUoc3VwZXJDbGFzcyAmJiBzdXBlckNsYXNzLnByb3RvdHlwZSwgeyBjb25zdHJ1Y3RvcjogeyB2YWx1ZTogc3ViQ2xhc3MsIGVudW1lcmFibGU6IGZhbHNlLCB3cml0YWJsZTogdHJ1ZSwgY29uZmlndXJhYmxlOiB0cnVlIH0gfSk7IGlmIChzdXBlckNsYXNzKSBPYmplY3Quc2V0UHJvdG90eXBlT2YgPyBPYmplY3Quc2V0UHJvdG90eXBlT2Yoc3ViQ2xhc3MsIHN1cGVyQ2xhc3MpIDogc3ViQ2xhc3MuX19wcm90b19fID0gc3VwZXJDbGFzczsgfVxuXG4vKipcbiAqIENvbG9yIHByZXZpZXcgZXh0ZW5zaW9uXG4gKiBAaWdub3JlXG4gKi9cbnZhciBQcmV2aWV3ID0gZnVuY3Rpb24gKF9FeHRlbnNpb24pIHtcbiAgX2luaGVyaXRzKFByZXZpZXcsIF9FeHRlbnNpb24pO1xuXG4gIGZ1bmN0aW9uIFByZXZpZXcoY29sb3JwaWNrZXIpIHtcbiAgICB2YXIgb3B0aW9ucyA9IGFyZ3VtZW50cy5sZW5ndGggPiAxICYmIGFyZ3VtZW50c1sxXSAhPT0gdW5kZWZpbmVkID8gYXJndW1lbnRzWzFdIDoge307XG5cbiAgICBfY2xhc3NDYWxsQ2hlY2sodGhpcywgUHJldmlldyk7XG5cbiAgICB2YXIgX3RoaXMgPSBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybih0aGlzLCAoUHJldmlldy5fX3Byb3RvX18gfHwgT2JqZWN0LmdldFByb3RvdHlwZU9mKFByZXZpZXcpKS5jYWxsKHRoaXMsIGNvbG9ycGlja2VyLCBfanF1ZXJ5Mi5kZWZhdWx0LmV4dGVuZCh0cnVlLCB7fSwge1xuICAgICAgdGVtcGxhdGU6ICc8ZGl2IGNsYXNzPVwiY29sb3JwaWNrZXItYmFyIGNvbG9ycGlja2VyLXByZXZpZXdcIj48ZGl2IC8+PC9kaXY+JyxcbiAgICAgIHNob3dUZXh0OiB0cnVlLFxuICAgICAgZm9ybWF0OiBjb2xvcnBpY2tlci5mb3JtYXRcbiAgICB9LCBvcHRpb25zKSkpO1xuXG4gICAgX3RoaXMuZWxlbWVudCA9ICgwLCBfanF1ZXJ5Mi5kZWZhdWx0KShfdGhpcy5vcHRpb25zLnRlbXBsYXRlKTtcbiAgICBfdGhpcy5lbGVtZW50SW5uZXIgPSBfdGhpcy5lbGVtZW50LmZpbmQoJ2RpdicpO1xuICAgIHJldHVybiBfdGhpcztcbiAgfVxuXG4gIF9jcmVhdGVDbGFzcyhQcmV2aWV3LCBbe1xuICAgIGtleTogJ29uQ3JlYXRlJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gb25DcmVhdGUoZXZlbnQpIHtcbiAgICAgIF9nZXQoUHJldmlldy5wcm90b3R5cGUuX19wcm90b19fIHx8IE9iamVjdC5nZXRQcm90b3R5cGVPZihQcmV2aWV3LnByb3RvdHlwZSksICdvbkNyZWF0ZScsIHRoaXMpLmNhbGwodGhpcywgZXZlbnQpO1xuICAgICAgdGhpcy5jb2xvcnBpY2tlci5waWNrZXIuYXBwZW5kKHRoaXMuZWxlbWVudCk7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnb25VcGRhdGUnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBvblVwZGF0ZShldmVudCkge1xuICAgICAgX2dldChQcmV2aWV3LnByb3RvdHlwZS5fX3Byb3RvX18gfHwgT2JqZWN0LmdldFByb3RvdHlwZU9mKFByZXZpZXcucHJvdG90eXBlKSwgJ29uVXBkYXRlJywgdGhpcykuY2FsbCh0aGlzLCBldmVudCk7XG5cbiAgICAgIGlmICghZXZlbnQuY29sb3IpIHtcbiAgICAgICAgdGhpcy5lbGVtZW50SW5uZXIuY3NzKCdiYWNrZ3JvdW5kQ29sb3InLCBudWxsKS5jc3MoJ2NvbG9yJywgbnVsbCkuaHRtbCgnJyk7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cblxuICAgICAgdGhpcy5lbGVtZW50SW5uZXIuY3NzKCdiYWNrZ3JvdW5kQ29sb3InLCBldmVudC5jb2xvci50b1JnYlN0cmluZygpKTtcblxuICAgICAgaWYgKHRoaXMub3B0aW9ucy5zaG93VGV4dCkge1xuICAgICAgICB0aGlzLmVsZW1lbnRJbm5lci5odG1sKGV2ZW50LmNvbG9yLnN0cmluZyh0aGlzLm9wdGlvbnMuZm9ybWF0IHx8IHRoaXMuY29sb3JwaWNrZXIuZm9ybWF0KSk7XG5cbiAgICAgICAgaWYgKGV2ZW50LmNvbG9yLmlzRGFyaygpICYmIGV2ZW50LmNvbG9yLmFscGhhID4gMC41KSB7XG4gICAgICAgICAgdGhpcy5lbGVtZW50SW5uZXIuY3NzKCdjb2xvcicsICd3aGl0ZScpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIHRoaXMuZWxlbWVudElubmVyLmNzcygnY29sb3InLCAnYmxhY2snKTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgfV0pO1xuXG4gIHJldHVybiBQcmV2aWV3O1xufShfRXh0ZW5zaW9uMy5kZWZhdWx0KTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gUHJldmlldztcbm1vZHVsZS5leHBvcnRzID0gZXhwb3J0cy5kZWZhdWx0O1xuXG4vKioqLyB9KSxcbi8qIDEyICovXG4vKioqLyAoZnVuY3Rpb24obW9kdWxlLCBleHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKSB7XG5cblwidXNlIHN0cmljdFwiO1xuXG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfY3JlYXRlQ2xhc3MgPSBmdW5jdGlvbiAoKSB7IGZ1bmN0aW9uIGRlZmluZVByb3BlcnRpZXModGFyZ2V0LCBwcm9wcykgeyBmb3IgKHZhciBpID0gMDsgaSA8IHByb3BzLmxlbmd0aDsgaSsrKSB7IHZhciBkZXNjcmlwdG9yID0gcHJvcHNbaV07IGRlc2NyaXB0b3IuZW51bWVyYWJsZSA9IGRlc2NyaXB0b3IuZW51bWVyYWJsZSB8fCBmYWxzZTsgZGVzY3JpcHRvci5jb25maWd1cmFibGUgPSB0cnVlOyBpZiAoXCJ2YWx1ZVwiIGluIGRlc2NyaXB0b3IpIGRlc2NyaXB0b3Iud3JpdGFibGUgPSB0cnVlOyBPYmplY3QuZGVmaW5lUHJvcGVydHkodGFyZ2V0LCBkZXNjcmlwdG9yLmtleSwgZGVzY3JpcHRvcik7IH0gfSByZXR1cm4gZnVuY3Rpb24gKENvbnN0cnVjdG9yLCBwcm90b1Byb3BzLCBzdGF0aWNQcm9wcykgeyBpZiAocHJvdG9Qcm9wcykgZGVmaW5lUHJvcGVydGllcyhDb25zdHJ1Y3Rvci5wcm90b3R5cGUsIHByb3RvUHJvcHMpOyBpZiAoc3RhdGljUHJvcHMpIGRlZmluZVByb3BlcnRpZXMoQ29uc3RydWN0b3IsIHN0YXRpY1Byb3BzKTsgcmV0dXJuIENvbnN0cnVjdG9yOyB9OyB9KCk7XG5cbnZhciBfZ2V0ID0gZnVuY3Rpb24gZ2V0KG9iamVjdCwgcHJvcGVydHksIHJlY2VpdmVyKSB7IGlmIChvYmplY3QgPT09IG51bGwpIG9iamVjdCA9IEZ1bmN0aW9uLnByb3RvdHlwZTsgdmFyIGRlc2MgPSBPYmplY3QuZ2V0T3duUHJvcGVydHlEZXNjcmlwdG9yKG9iamVjdCwgcHJvcGVydHkpOyBpZiAoZGVzYyA9PT0gdW5kZWZpbmVkKSB7IHZhciBwYXJlbnQgPSBPYmplY3QuZ2V0UHJvdG90eXBlT2Yob2JqZWN0KTsgaWYgKHBhcmVudCA9PT0gbnVsbCkgeyByZXR1cm4gdW5kZWZpbmVkOyB9IGVsc2UgeyByZXR1cm4gZ2V0KHBhcmVudCwgcHJvcGVydHksIHJlY2VpdmVyKTsgfSB9IGVsc2UgaWYgKFwidmFsdWVcIiBpbiBkZXNjKSB7IHJldHVybiBkZXNjLnZhbHVlOyB9IGVsc2UgeyB2YXIgZ2V0dGVyID0gZGVzYy5nZXQ7IGlmIChnZXR0ZXIgPT09IHVuZGVmaW5lZCkgeyByZXR1cm4gdW5kZWZpbmVkOyB9IHJldHVybiBnZXR0ZXIuY2FsbChyZWNlaXZlcik7IH0gfTtcblxudmFyIF9QYWxldHRlMiA9IF9fd2VicGFja19yZXF1aXJlX18oNCk7XG5cbnZhciBfUGFsZXR0ZTMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9QYWxldHRlMik7XG5cbnZhciBfanF1ZXJ5ID0gX193ZWJwYWNrX3JlcXVpcmVfXygwKTtcblxudmFyIF9qcXVlcnkyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfanF1ZXJ5KTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxuZnVuY3Rpb24gX2NsYXNzQ2FsbENoZWNrKGluc3RhbmNlLCBDb25zdHJ1Y3RvcikgeyBpZiAoIShpbnN0YW5jZSBpbnN0YW5jZW9mIENvbnN0cnVjdG9yKSkgeyB0aHJvdyBuZXcgVHlwZUVycm9yKFwiQ2Fubm90IGNhbGwgYSBjbGFzcyBhcyBhIGZ1bmN0aW9uXCIpOyB9IH1cblxuZnVuY3Rpb24gX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4oc2VsZiwgY2FsbCkgeyBpZiAoIXNlbGYpIHsgdGhyb3cgbmV3IFJlZmVyZW5jZUVycm9yKFwidGhpcyBoYXNuJ3QgYmVlbiBpbml0aWFsaXNlZCAtIHN1cGVyKCkgaGFzbid0IGJlZW4gY2FsbGVkXCIpOyB9IHJldHVybiBjYWxsICYmICh0eXBlb2YgY2FsbCA9PT0gXCJvYmplY3RcIiB8fCB0eXBlb2YgY2FsbCA9PT0gXCJmdW5jdGlvblwiKSA/IGNhbGwgOiBzZWxmOyB9XG5cbmZ1bmN0aW9uIF9pbmhlcml0cyhzdWJDbGFzcywgc3VwZXJDbGFzcykgeyBpZiAodHlwZW9mIHN1cGVyQ2xhc3MgIT09IFwiZnVuY3Rpb25cIiAmJiBzdXBlckNsYXNzICE9PSBudWxsKSB7IHRocm93IG5ldyBUeXBlRXJyb3IoXCJTdXBlciBleHByZXNzaW9uIG11c3QgZWl0aGVyIGJlIG51bGwgb3IgYSBmdW5jdGlvbiwgbm90IFwiICsgdHlwZW9mIHN1cGVyQ2xhc3MpOyB9IHN1YkNsYXNzLnByb3RvdHlwZSA9IE9iamVjdC5jcmVhdGUoc3VwZXJDbGFzcyAmJiBzdXBlckNsYXNzLnByb3RvdHlwZSwgeyBjb25zdHJ1Y3RvcjogeyB2YWx1ZTogc3ViQ2xhc3MsIGVudW1lcmFibGU6IGZhbHNlLCB3cml0YWJsZTogdHJ1ZSwgY29uZmlndXJhYmxlOiB0cnVlIH0gfSk7IGlmIChzdXBlckNsYXNzKSBPYmplY3Quc2V0UHJvdG90eXBlT2YgPyBPYmplY3Quc2V0UHJvdG90eXBlT2Yoc3ViQ2xhc3MsIHN1cGVyQ2xhc3MpIDogc3ViQ2xhc3MuX19wcm90b19fID0gc3VwZXJDbGFzczsgfVxuXG52YXIgZGVmYXVsdHMgPSB7XG4gIGJhclRlbXBsYXRlOiAnPGRpdiBjbGFzcz1cImNvbG9ycGlja2VyLWJhciBjb2xvcnBpY2tlci1zd2F0Y2hlc1wiPlxcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbG9ycGlja2VyLXN3YXRjaGVzLS1pbm5lclwiPjwvZGl2PlxcbiAgICAgICAgICAgICAgICA8L2Rpdj4nLFxuICBzd2F0Y2hUZW1wbGF0ZTogJzxpIGNsYXNzPVwiY29sb3JwaWNrZXItc3dhdGNoXCI+PGkgY2xhc3M9XCJjb2xvcnBpY2tlci1zd2F0Y2gtLWlubmVyXCI+PC9pPjwvaT4nXG59O1xuXG4vKipcbiAqIENvbG9yIHN3YXRjaGVzIGV4dGVuc2lvblxuICogQGlnbm9yZVxuICovXG5cbnZhciBTd2F0Y2hlcyA9IGZ1bmN0aW9uIChfUGFsZXR0ZSkge1xuICBfaW5oZXJpdHMoU3dhdGNoZXMsIF9QYWxldHRlKTtcblxuICBmdW5jdGlvbiBTd2F0Y2hlcyhjb2xvcnBpY2tlcikge1xuICAgIHZhciBvcHRpb25zID0gYXJndW1lbnRzLmxlbmd0aCA+IDEgJiYgYXJndW1lbnRzWzFdICE9PSB1bmRlZmluZWQgPyBhcmd1bWVudHNbMV0gOiB7fTtcblxuICAgIF9jbGFzc0NhbGxDaGVjayh0aGlzLCBTd2F0Y2hlcyk7XG5cbiAgICB2YXIgX3RoaXMgPSBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybih0aGlzLCAoU3dhdGNoZXMuX19wcm90b19fIHx8IE9iamVjdC5nZXRQcm90b3R5cGVPZihTd2F0Y2hlcykpLmNhbGwodGhpcywgY29sb3JwaWNrZXIsIF9qcXVlcnkyLmRlZmF1bHQuZXh0ZW5kKHRydWUsIHt9LCBkZWZhdWx0cywgb3B0aW9ucykpKTtcblxuICAgIF90aGlzLmVsZW1lbnQgPSBudWxsO1xuICAgIHJldHVybiBfdGhpcztcbiAgfVxuXG4gIF9jcmVhdGVDbGFzcyhTd2F0Y2hlcywgW3tcbiAgICBrZXk6ICdpc0VuYWJsZWQnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBpc0VuYWJsZWQoKSB7XG4gICAgICByZXR1cm4gdGhpcy5nZXRMZW5ndGgoKSA+IDA7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnb25DcmVhdGUnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBvbkNyZWF0ZShldmVudCkge1xuICAgICAgX2dldChTd2F0Y2hlcy5wcm90b3R5cGUuX19wcm90b19fIHx8IE9iamVjdC5nZXRQcm90b3R5cGVPZihTd2F0Y2hlcy5wcm90b3R5cGUpLCAnb25DcmVhdGUnLCB0aGlzKS5jYWxsKHRoaXMsIGV2ZW50KTtcblxuICAgICAgaWYgKCF0aGlzLmlzRW5hYmxlZCgpKSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cblxuICAgICAgdGhpcy5lbGVtZW50ID0gKDAsIF9qcXVlcnkyLmRlZmF1bHQpKHRoaXMub3B0aW9ucy5iYXJUZW1wbGF0ZSk7XG4gICAgICB0aGlzLmxvYWQoKTtcbiAgICAgIHRoaXMuY29sb3JwaWNrZXIucGlja2VyLmFwcGVuZCh0aGlzLmVsZW1lbnQpO1xuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ2xvYWQnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBsb2FkKCkge1xuICAgICAgdmFyIF90aGlzMiA9IHRoaXM7XG5cbiAgICAgIHZhciBjb2xvcnBpY2tlciA9IHRoaXMuY29sb3JwaWNrZXIsXG4gICAgICAgICAgc3dhdGNoQ29udGFpbmVyID0gdGhpcy5lbGVtZW50LmZpbmQoJy5jb2xvcnBpY2tlci1zd2F0Y2hlcy0taW5uZXInKSxcbiAgICAgICAgICBpc0FsaWFzZWQgPSB0aGlzLm9wdGlvbnMubmFtZXNBc1ZhbHVlcyA9PT0gdHJ1ZSAmJiAhQXJyYXkuaXNBcnJheSh0aGlzLmNvbG9ycyk7XG5cbiAgICAgIHN3YXRjaENvbnRhaW5lci5lbXB0eSgpO1xuXG4gICAgICBfanF1ZXJ5Mi5kZWZhdWx0LmVhY2godGhpcy5jb2xvcnMsIGZ1bmN0aW9uIChuYW1lLCB2YWx1ZSkge1xuICAgICAgICB2YXIgJHN3YXRjaCA9ICgwLCBfanF1ZXJ5Mi5kZWZhdWx0KShfdGhpczIub3B0aW9ucy5zd2F0Y2hUZW1wbGF0ZSkuYXR0cignZGF0YS1uYW1lJywgbmFtZSkuYXR0cignZGF0YS12YWx1ZScsIHZhbHVlKS5hdHRyKCd0aXRsZScsIGlzQWxpYXNlZCA/IG5hbWUgKyAnOiAnICsgdmFsdWUgOiB2YWx1ZSkub24oJ21vdXNlZG93bi5jb2xvcnBpY2tlciB0b3VjaHN0YXJ0LmNvbG9ycGlja2VyJywgZnVuY3Rpb24gKGUpIHtcbiAgICAgICAgICB2YXIgJHN3ID0gKDAsIF9qcXVlcnkyLmRlZmF1bHQpKHRoaXMpO1xuXG4gICAgICAgICAgLy8gZS5wcmV2ZW50RGVmYXVsdCgpO1xuXG4gICAgICAgICAgY29sb3JwaWNrZXIuc2V0VmFsdWUoaXNBbGlhc2VkID8gJHN3LmF0dHIoJ2RhdGEtbmFtZScpIDogJHN3LmF0dHIoJ2RhdGEtdmFsdWUnKSk7XG4gICAgICAgIH0pO1xuXG4gICAgICAgICRzd2F0Y2guZmluZCgnLmNvbG9ycGlja2VyLXN3YXRjaC0taW5uZXInKS5jc3MoJ2JhY2tncm91bmQtY29sb3InLCB2YWx1ZSk7XG5cbiAgICAgICAgc3dhdGNoQ29udGFpbmVyLmFwcGVuZCgkc3dhdGNoKTtcbiAgICAgIH0pO1xuXG4gICAgICBzd2F0Y2hDb250YWluZXIuYXBwZW5kKCgwLCBfanF1ZXJ5Mi5kZWZhdWx0KSgnPGkgY2xhc3M9XCJjb2xvcnBpY2tlci1jbGVhclwiPjwvaT4nKSk7XG4gICAgfVxuICB9XSk7XG5cbiAgcmV0dXJuIFN3YXRjaGVzO1xufShfUGFsZXR0ZTMuZGVmYXVsdCk7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IFN3YXRjaGVzO1xubW9kdWxlLmV4cG9ydHMgPSBleHBvcnRzLmRlZmF1bHQ7XG5cbi8qKiovIH0pLFxuLyogMTMgKi9cbi8qKiovIChmdW5jdGlvbihtb2R1bGUsIGV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pIHtcblxuXCJ1c2Ugc3RyaWN0XCI7XG5cblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9jcmVhdGVDbGFzcyA9IGZ1bmN0aW9uICgpIHsgZnVuY3Rpb24gZGVmaW5lUHJvcGVydGllcyh0YXJnZXQsIHByb3BzKSB7IGZvciAodmFyIGkgPSAwOyBpIDwgcHJvcHMubGVuZ3RoOyBpKyspIHsgdmFyIGRlc2NyaXB0b3IgPSBwcm9wc1tpXTsgZGVzY3JpcHRvci5lbnVtZXJhYmxlID0gZGVzY3JpcHRvci5lbnVtZXJhYmxlIHx8IGZhbHNlOyBkZXNjcmlwdG9yLmNvbmZpZ3VyYWJsZSA9IHRydWU7IGlmIChcInZhbHVlXCIgaW4gZGVzY3JpcHRvcikgZGVzY3JpcHRvci53cml0YWJsZSA9IHRydWU7IE9iamVjdC5kZWZpbmVQcm9wZXJ0eSh0YXJnZXQsIGRlc2NyaXB0b3Iua2V5LCBkZXNjcmlwdG9yKTsgfSB9IHJldHVybiBmdW5jdGlvbiAoQ29uc3RydWN0b3IsIHByb3RvUHJvcHMsIHN0YXRpY1Byb3BzKSB7IGlmIChwcm90b1Byb3BzKSBkZWZpbmVQcm9wZXJ0aWVzKENvbnN0cnVjdG9yLnByb3RvdHlwZSwgcHJvdG9Qcm9wcyk7IGlmIChzdGF0aWNQcm9wcykgZGVmaW5lUHJvcGVydGllcyhDb25zdHJ1Y3Rvciwgc3RhdGljUHJvcHMpOyByZXR1cm4gQ29uc3RydWN0b3I7IH07IH0oKTtcblxudmFyIF9qcXVlcnkgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKDApO1xuXG52YXIgX2pxdWVyeTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9qcXVlcnkpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG5mdW5jdGlvbiBfY2xhc3NDYWxsQ2hlY2soaW5zdGFuY2UsIENvbnN0cnVjdG9yKSB7IGlmICghKGluc3RhbmNlIGluc3RhbmNlb2YgQ29uc3RydWN0b3IpKSB7IHRocm93IG5ldyBUeXBlRXJyb3IoXCJDYW5ub3QgY2FsbCBhIGNsYXNzIGFzIGEgZnVuY3Rpb25cIik7IH0gfVxuXG4vKipcbiAqIENsYXNzIHRoYXQgaGFuZGxlcyBhbGwgY29uZmlndXJlZCBzbGlkZXJzIG9uIG1vdXNlIG9yIHRvdWNoIGV2ZW50cy5cbiAqIEBpZ25vcmVcbiAqL1xudmFyIFNsaWRlckhhbmRsZXIgPSBmdW5jdGlvbiAoKSB7XG4gIC8qKlxuICAgKiBAcGFyYW0ge0NvbG9ycGlja2VyfSBjb2xvcnBpY2tlclxuICAgKi9cbiAgZnVuY3Rpb24gU2xpZGVySGFuZGxlcihjb2xvcnBpY2tlcikge1xuICAgIF9jbGFzc0NhbGxDaGVjayh0aGlzLCBTbGlkZXJIYW5kbGVyKTtcblxuICAgIC8qKlxuICAgICAqIEB0eXBlIHtDb2xvcnBpY2tlcn1cbiAgICAgKi9cbiAgICB0aGlzLmNvbG9ycGlja2VyID0gY29sb3JwaWNrZXI7XG4gICAgLyoqXG4gICAgICogQHR5cGUgeyp8U3RyaW5nfVxuICAgICAqIEBwcml2YXRlXG4gICAgICovXG4gICAgdGhpcy5jdXJyZW50U2xpZGVyID0gbnVsbDtcbiAgICAvKipcbiAgICAgKiBAdHlwZSB7e2xlZnQ6IG51bWJlciwgdG9wOiBudW1iZXJ9fVxuICAgICAqIEBwcml2YXRlXG4gICAgICovXG4gICAgdGhpcy5tb3VzZVBvaW50ZXIgPSB7XG4gICAgICBsZWZ0OiAwLFxuICAgICAgdG9wOiAwXG4gICAgfTtcblxuICAgIC8qKlxuICAgICAqIEB0eXBlIHtGdW5jdGlvbn1cbiAgICAgKi9cbiAgICB0aGlzLm9uTW92ZSA9IF9qcXVlcnkyLmRlZmF1bHQucHJveHkodGhpcy5kZWZhdWx0T25Nb3ZlLCB0aGlzKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBUaGlzIGZ1bmN0aW9uIGlzIGNhbGxlZCBldmVyeSB0aW1lIGEgc2xpZGVyIGd1aWRlIGlzIG1vdmVkXG4gICAqIFRoZSBzY29wZSBvZiBcInRoaXNcIiBpcyB0aGUgU2xpZGVySGFuZGxlciBvYmplY3QuXG4gICAqXG4gICAqIEBwYXJhbSB7aW50fSB0b3BcbiAgICogQHBhcmFtIHtpbnR9IGxlZnRcbiAgICovXG5cblxuICBfY3JlYXRlQ2xhc3MoU2xpZGVySGFuZGxlciwgW3tcbiAgICBrZXk6ICdkZWZhdWx0T25Nb3ZlJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gZGVmYXVsdE9uTW92ZSh0b3AsIGxlZnQpIHtcbiAgICAgIGlmICghdGhpcy5jdXJyZW50U2xpZGVyKSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cblxuICAgICAgdmFyIHNsaWRlciA9IHRoaXMuY3VycmVudFNsaWRlcixcbiAgICAgICAgICBjcCA9IHRoaXMuY29sb3JwaWNrZXIsXG4gICAgICAgICAgY2ggPSBjcC5jb2xvckhhbmRsZXI7XG5cbiAgICAgIC8vIENyZWF0ZSBhIGNvbG9yIG9iamVjdFxuICAgICAgdmFyIGNvbG9yID0gIWNoLmhhc0NvbG9yKCkgPyBjaC5nZXRGYWxsYmFja0NvbG9yKCkgOiBjaC5jb2xvci5nZXRDbG9uZSgpO1xuXG4gICAgICAvLyBBZGp1c3QgdGhlIGd1aWRlIHBvc2l0aW9uXG4gICAgICBzbGlkZXIuZ3VpZGVTdHlsZS5sZWZ0ID0gbGVmdCArICdweCc7XG4gICAgICBzbGlkZXIuZ3VpZGVTdHlsZS50b3AgPSB0b3AgKyAncHgnO1xuXG4gICAgICAvLyBBZGp1c3QgdGhlIGNvbG9yXG4gICAgICBpZiAoc2xpZGVyLmNhbGxMZWZ0KSB7XG4gICAgICAgIGNvbG9yW3NsaWRlci5jYWxsTGVmdF0obGVmdCAvIHNsaWRlci5tYXhMZWZ0KTtcbiAgICAgIH1cbiAgICAgIGlmIChzbGlkZXIuY2FsbFRvcCkge1xuICAgICAgICBjb2xvcltzbGlkZXIuY2FsbFRvcF0odG9wIC8gc2xpZGVyLm1heFRvcCk7XG4gICAgICB9XG5cbiAgICAgIC8vIFNldCB0aGUgbmV3IGNvbG9yXG4gICAgICBjcC5zZXRWYWx1ZShjb2xvcik7XG4gICAgICBjcC5wb3B1cEhhbmRsZXIuZm9jdXMoKTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBCaW5kcyB0aGUgY29sb3JwaWNrZXIgc2xpZGVycyB0byB0aGUgbW91c2UvdG91Y2ggZXZlbnRzXG4gICAgICovXG5cbiAgfSwge1xuICAgIGtleTogJ2JpbmQnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBiaW5kKCkge1xuICAgICAgdmFyIHNsaWRlcnMgPSB0aGlzLmNvbG9ycGlja2VyLm9wdGlvbnMuaG9yaXpvbnRhbCA/IHRoaXMuY29sb3JwaWNrZXIub3B0aW9ucy5zbGlkZXJzSG9yeiA6IHRoaXMuY29sb3JwaWNrZXIub3B0aW9ucy5zbGlkZXJzO1xuXG4gICAgICB2YXIgc2xpZGVyQ2xhc3NlcyA9IFtdO1xuXG4gICAgICBmb3IgKHZhciBzbGlkZXJOYW1lIGluIHNsaWRlcnMpIHtcbiAgICAgICAgaWYgKCFzbGlkZXJzLmhhc093blByb3BlcnR5KHNsaWRlck5hbWUpKSB7XG4gICAgICAgICAgY29udGludWU7XG4gICAgICAgIH1cblxuICAgICAgICBzbGlkZXJDbGFzc2VzLnB1c2goc2xpZGVyc1tzbGlkZXJOYW1lXS5zZWxlY3Rvcik7XG4gICAgICB9XG5cbiAgICAgIHRoaXMuY29sb3JwaWNrZXIucGlja2VyLmZpbmQoc2xpZGVyQ2xhc3Nlcy5qb2luKCcsICcpKS5vbignbW91c2Vkb3duLmNvbG9ycGlja2VyIHRvdWNoc3RhcnQuY29sb3JwaWNrZXInLCBfanF1ZXJ5Mi5kZWZhdWx0LnByb3h5KHRoaXMucHJlc3NlZCwgdGhpcykpO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIFVuYmluZHMgYW55IGV2ZW50IGJvdW5kIGJ5IHRoaXMgaGFuZGxlclxuICAgICAqL1xuXG4gIH0sIHtcbiAgICBrZXk6ICd1bmJpbmQnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiB1bmJpbmQoKSB7XG4gICAgICAoMCwgX2pxdWVyeTIuZGVmYXVsdCkodGhpcy5jb2xvcnBpY2tlci5waWNrZXIpLm9mZih7XG4gICAgICAgICdtb3VzZW1vdmUuY29sb3JwaWNrZXInOiBfanF1ZXJ5Mi5kZWZhdWx0LnByb3h5KHRoaXMubW92ZWQsIHRoaXMpLFxuICAgICAgICAndG91Y2htb3ZlLmNvbG9ycGlja2VyJzogX2pxdWVyeTIuZGVmYXVsdC5wcm94eSh0aGlzLm1vdmVkLCB0aGlzKSxcbiAgICAgICAgJ21vdXNldXAuY29sb3JwaWNrZXInOiBfanF1ZXJ5Mi5kZWZhdWx0LnByb3h5KHRoaXMucmVsZWFzZWQsIHRoaXMpLFxuICAgICAgICAndG91Y2hlbmQuY29sb3JwaWNrZXInOiBfanF1ZXJ5Mi5kZWZhdWx0LnByb3h5KHRoaXMucmVsZWFzZWQsIHRoaXMpXG4gICAgICB9KTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBGdW5jdGlvbiB0cmlnZ2VyZWQgd2hlbiBjbGlja2luZyBpbiBvbmUgb2YgdGhlIGNvbG9yIGFkanVzdG1lbnQgYmFyc1xuICAgICAqXG4gICAgICogQHByaXZhdGVcbiAgICAgKiBAZmlyZXMgQ29sb3JwaWNrZXIjbW91c2Vtb3ZlXG4gICAgICogQHBhcmFtIHtFdmVudH0gZVxuICAgICAqL1xuXG4gIH0sIHtcbiAgICBrZXk6ICdwcmVzc2VkJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gcHJlc3NlZChlKSB7XG4gICAgICBpZiAodGhpcy5jb2xvcnBpY2tlci5pc0Rpc2FibGVkKCkpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuICAgICAgdGhpcy5jb2xvcnBpY2tlci5sYXN0RXZlbnQuYWxpYXMgPSAncHJlc3NlZCc7XG4gICAgICB0aGlzLmNvbG9ycGlja2VyLmxhc3RFdmVudC5lID0gZTtcblxuICAgICAgaWYgKCFlLnBhZ2VYICYmICFlLnBhZ2VZICYmIGUub3JpZ2luYWxFdmVudCAmJiBlLm9yaWdpbmFsRXZlbnQudG91Y2hlcykge1xuICAgICAgICBlLnBhZ2VYID0gZS5vcmlnaW5hbEV2ZW50LnRvdWNoZXNbMF0ucGFnZVg7XG4gICAgICAgIGUucGFnZVkgPSBlLm9yaWdpbmFsRXZlbnQudG91Y2hlc1swXS5wYWdlWTtcbiAgICAgIH1cbiAgICAgIC8vIGUuc3RvcFByb3BhZ2F0aW9uKCk7XG4gICAgICAvLyBlLnByZXZlbnREZWZhdWx0KCk7XG5cbiAgICAgIHZhciB0YXJnZXQgPSAoMCwgX2pxdWVyeTIuZGVmYXVsdCkoZS50YXJnZXQpO1xuXG4gICAgICAvLyBkZXRlY3QgdGhlIHNsaWRlciBhbmQgc2V0IHRoZSBsaW1pdHMgYW5kIGNhbGxiYWNrc1xuICAgICAgdmFyIHpvbmUgPSB0YXJnZXQuY2xvc2VzdCgnZGl2Jyk7XG5cbiAgICAgIHZhciBzbGlkZXJzID0gdGhpcy5jb2xvcnBpY2tlci5vcHRpb25zLmhvcml6b250YWwgPyB0aGlzLmNvbG9ycGlja2VyLm9wdGlvbnMuc2xpZGVyc0hvcnogOiB0aGlzLmNvbG9ycGlja2VyLm9wdGlvbnMuc2xpZGVycztcblxuICAgICAgaWYgKHpvbmUuaXMoJy5jb2xvcnBpY2tlcicpKSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cblxuICAgICAgdGhpcy5jdXJyZW50U2xpZGVyID0gbnVsbDtcblxuICAgICAgZm9yICh2YXIgc2xpZGVyTmFtZSBpbiBzbGlkZXJzKSB7XG4gICAgICAgIGlmICghc2xpZGVycy5oYXNPd25Qcm9wZXJ0eShzbGlkZXJOYW1lKSkge1xuICAgICAgICAgIGNvbnRpbnVlO1xuICAgICAgICB9XG5cbiAgICAgICAgdmFyIHNsaWRlciA9IHNsaWRlcnNbc2xpZGVyTmFtZV07XG5cbiAgICAgICAgaWYgKHpvbmUuaXMoc2xpZGVyLnNlbGVjdG9yKSkge1xuICAgICAgICAgIHRoaXMuY3VycmVudFNsaWRlciA9IF9qcXVlcnkyLmRlZmF1bHQuZXh0ZW5kKHt9LCBzbGlkZXIsIHsgbmFtZTogc2xpZGVyTmFtZSB9KTtcbiAgICAgICAgICBicmVhaztcbiAgICAgICAgfSBlbHNlIGlmIChzbGlkZXIuY2hpbGRTZWxlY3RvciAhPT0gdW5kZWZpbmVkICYmIHpvbmUuaXMoc2xpZGVyLmNoaWxkU2VsZWN0b3IpKSB7XG4gICAgICAgICAgdGhpcy5jdXJyZW50U2xpZGVyID0gX2pxdWVyeTIuZGVmYXVsdC5leHRlbmQoe30sIHNsaWRlciwgeyBuYW1lOiBzbGlkZXJOYW1lIH0pO1xuICAgICAgICAgIHpvbmUgPSB6b25lLnBhcmVudCgpOyAvLyB6b25lLnBhcmVudHMoc2xpZGVyLnNlbGVjdG9yKS5maXJzdCgpID9cbiAgICAgICAgICBicmVhaztcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICB2YXIgZ3VpZGUgPSB6b25lLmZpbmQoJy5jb2xvcnBpY2tlci1ndWlkZScpLmdldCgwKTtcblxuICAgICAgaWYgKHRoaXMuY3VycmVudFNsaWRlciA9PT0gbnVsbCB8fCBndWlkZSA9PT0gbnVsbCkge1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG5cbiAgICAgIHZhciBvZmZzZXQgPSB6b25lLm9mZnNldCgpO1xuXG4gICAgICAvLyByZWZlcmVuY2UgdG8gZ3VpZGUncyBzdHlsZVxuICAgICAgdGhpcy5jdXJyZW50U2xpZGVyLmd1aWRlU3R5bGUgPSBndWlkZS5zdHlsZTtcbiAgICAgIHRoaXMuY3VycmVudFNsaWRlci5sZWZ0ID0gZS5wYWdlWCAtIG9mZnNldC5sZWZ0O1xuICAgICAgdGhpcy5jdXJyZW50U2xpZGVyLnRvcCA9IGUucGFnZVkgLSBvZmZzZXQudG9wO1xuICAgICAgdGhpcy5tb3VzZVBvaW50ZXIgPSB7XG4gICAgICAgIGxlZnQ6IGUucGFnZVgsXG4gICAgICAgIHRvcDogZS5wYWdlWVxuICAgICAgfTtcblxuICAgICAgLy8gVE9ETzogZml4IG1vdmluZyBvdXRzaWRlIHRoZSBwaWNrZXIgbWFrZXMgdGhlIGd1aWRlcyB0byBrZWVwIG1vdmluZy4gVGhlIGV2ZW50IG5lZWRzIHRvIGJlIGJvdW5kIHRvIHRoZSB3aW5kb3cuXG4gICAgICAvKipcbiAgICAgICAqICh3aW5kb3cuZG9jdW1lbnQpIFRyaWdnZXJlZCBvbiBtb3VzZWRvd24gZm9yIHRoZSBkb2N1bWVudCBvYmplY3QsXG4gICAgICAgKiBzbyB0aGUgY29sb3IgYWRqdXN0bWVudCBndWlkZSBpcyBtb3ZlZCB0byB0aGUgY2xpY2tlZCBwb3NpdGlvbi5cbiAgICAgICAqXG4gICAgICAgKiBAZXZlbnQgQ29sb3JwaWNrZXIjbW91c2Vtb3ZlXG4gICAgICAgKi9cbiAgICAgICgwLCBfanF1ZXJ5Mi5kZWZhdWx0KSh0aGlzLmNvbG9ycGlja2VyLnBpY2tlcikub24oe1xuICAgICAgICAnbW91c2Vtb3ZlLmNvbG9ycGlja2VyJzogX2pxdWVyeTIuZGVmYXVsdC5wcm94eSh0aGlzLm1vdmVkLCB0aGlzKSxcbiAgICAgICAgJ3RvdWNobW92ZS5jb2xvcnBpY2tlcic6IF9qcXVlcnkyLmRlZmF1bHQucHJveHkodGhpcy5tb3ZlZCwgdGhpcyksXG4gICAgICAgICdtb3VzZXVwLmNvbG9ycGlja2VyJzogX2pxdWVyeTIuZGVmYXVsdC5wcm94eSh0aGlzLnJlbGVhc2VkLCB0aGlzKSxcbiAgICAgICAgJ3RvdWNoZW5kLmNvbG9ycGlja2VyJzogX2pxdWVyeTIuZGVmYXVsdC5wcm94eSh0aGlzLnJlbGVhc2VkLCB0aGlzKVxuICAgICAgfSkudHJpZ2dlcignbW91c2Vtb3ZlJyk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogRnVuY3Rpb24gdHJpZ2dlcmVkIHdoZW4gZHJhZ2dpbmcgYSBndWlkZSBpbnNpZGUgb25lIG9mIHRoZSBjb2xvciBhZGp1c3RtZW50IGJhcnMuXG4gICAgICpcbiAgICAgKiBAcHJpdmF0ZVxuICAgICAqIEBwYXJhbSB7RXZlbnR9IGVcbiAgICAgKi9cblxuICB9LCB7XG4gICAga2V5OiAnbW92ZWQnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBtb3ZlZChlKSB7XG4gICAgICB0aGlzLmNvbG9ycGlja2VyLmxhc3RFdmVudC5hbGlhcyA9ICdtb3ZlZCc7XG4gICAgICB0aGlzLmNvbG9ycGlja2VyLmxhc3RFdmVudC5lID0gZTtcblxuICAgICAgaWYgKCFlLnBhZ2VYICYmICFlLnBhZ2VZICYmIGUub3JpZ2luYWxFdmVudCAmJiBlLm9yaWdpbmFsRXZlbnQudG91Y2hlcykge1xuICAgICAgICBlLnBhZ2VYID0gZS5vcmlnaW5hbEV2ZW50LnRvdWNoZXNbMF0ucGFnZVg7XG4gICAgICAgIGUucGFnZVkgPSBlLm9yaWdpbmFsRXZlbnQudG91Y2hlc1swXS5wYWdlWTtcbiAgICAgIH1cblxuICAgICAgLy8gZS5zdG9wUHJvcGFnYXRpb24oKTtcbiAgICAgIGUucHJldmVudERlZmF1bHQoKTsgLy8gcHJldmVudHMgc2Nyb2xsaW5nIG9uIG1vYmlsZVxuXG4gICAgICB2YXIgbGVmdCA9IE1hdGgubWF4KDAsIE1hdGgubWluKHRoaXMuY3VycmVudFNsaWRlci5tYXhMZWZ0LCB0aGlzLmN1cnJlbnRTbGlkZXIubGVmdCArICgoZS5wYWdlWCB8fCB0aGlzLm1vdXNlUG9pbnRlci5sZWZ0KSAtIHRoaXMubW91c2VQb2ludGVyLmxlZnQpKSk7XG5cbiAgICAgIHZhciB0b3AgPSBNYXRoLm1heCgwLCBNYXRoLm1pbih0aGlzLmN1cnJlbnRTbGlkZXIubWF4VG9wLCB0aGlzLmN1cnJlbnRTbGlkZXIudG9wICsgKChlLnBhZ2VZIHx8IHRoaXMubW91c2VQb2ludGVyLnRvcCkgLSB0aGlzLm1vdXNlUG9pbnRlci50b3ApKSk7XG5cbiAgICAgIHRoaXMub25Nb3ZlKHRvcCwgbGVmdCk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogRnVuY3Rpb24gdHJpZ2dlcmVkIHdoZW4gcmVsZWFzaW5nIHRoZSBjbGljayBpbiBvbmUgb2YgdGhlIGNvbG9yIGFkanVzdG1lbnQgYmFycy5cbiAgICAgKlxuICAgICAqIEBwcml2YXRlXG4gICAgICogQHBhcmFtIHtFdmVudH0gZVxuICAgICAqL1xuXG4gIH0sIHtcbiAgICBrZXk6ICdyZWxlYXNlZCcsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHJlbGVhc2VkKGUpIHtcbiAgICAgIHRoaXMuY29sb3JwaWNrZXIubGFzdEV2ZW50LmFsaWFzID0gJ3JlbGVhc2VkJztcbiAgICAgIHRoaXMuY29sb3JwaWNrZXIubGFzdEV2ZW50LmUgPSBlO1xuXG4gICAgICAvLyBlLnN0b3BQcm9wYWdhdGlvbigpO1xuICAgICAgLy8gZS5wcmV2ZW50RGVmYXVsdCgpO1xuXG4gICAgICAoMCwgX2pxdWVyeTIuZGVmYXVsdCkodGhpcy5jb2xvcnBpY2tlci5waWNrZXIpLm9mZih7XG4gICAgICAgICdtb3VzZW1vdmUuY29sb3JwaWNrZXInOiB0aGlzLm1vdmVkLFxuICAgICAgICAndG91Y2htb3ZlLmNvbG9ycGlja2VyJzogdGhpcy5tb3ZlZCxcbiAgICAgICAgJ21vdXNldXAuY29sb3JwaWNrZXInOiB0aGlzLnJlbGVhc2VkLFxuICAgICAgICAndG91Y2hlbmQuY29sb3JwaWNrZXInOiB0aGlzLnJlbGVhc2VkXG4gICAgICB9KTtcbiAgICB9XG4gIH1dKTtcblxuICByZXR1cm4gU2xpZGVySGFuZGxlcjtcbn0oKTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gU2xpZGVySGFuZGxlcjtcbm1vZHVsZS5leHBvcnRzID0gZXhwb3J0cy5kZWZhdWx0O1xuXG4vKioqLyB9KSxcbi8qIDE0ICovXG4vKioqLyAoZnVuY3Rpb24obW9kdWxlLCBleHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKSB7XG5cblwidXNlIHN0cmljdFwiO1xuXG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfY3JlYXRlQ2xhc3MgPSBmdW5jdGlvbiAoKSB7IGZ1bmN0aW9uIGRlZmluZVByb3BlcnRpZXModGFyZ2V0LCBwcm9wcykgeyBmb3IgKHZhciBpID0gMDsgaSA8IHByb3BzLmxlbmd0aDsgaSsrKSB7IHZhciBkZXNjcmlwdG9yID0gcHJvcHNbaV07IGRlc2NyaXB0b3IuZW51bWVyYWJsZSA9IGRlc2NyaXB0b3IuZW51bWVyYWJsZSB8fCBmYWxzZTsgZGVzY3JpcHRvci5jb25maWd1cmFibGUgPSB0cnVlOyBpZiAoXCJ2YWx1ZVwiIGluIGRlc2NyaXB0b3IpIGRlc2NyaXB0b3Iud3JpdGFibGUgPSB0cnVlOyBPYmplY3QuZGVmaW5lUHJvcGVydHkodGFyZ2V0LCBkZXNjcmlwdG9yLmtleSwgZGVzY3JpcHRvcik7IH0gfSByZXR1cm4gZnVuY3Rpb24gKENvbnN0cnVjdG9yLCBwcm90b1Byb3BzLCBzdGF0aWNQcm9wcykgeyBpZiAocHJvdG9Qcm9wcykgZGVmaW5lUHJvcGVydGllcyhDb25zdHJ1Y3Rvci5wcm90b3R5cGUsIHByb3RvUHJvcHMpOyBpZiAoc3RhdGljUHJvcHMpIGRlZmluZVByb3BlcnRpZXMoQ29uc3RydWN0b3IsIHN0YXRpY1Byb3BzKTsgcmV0dXJuIENvbnN0cnVjdG9yOyB9OyB9KCk7XG5cbnZhciBfanF1ZXJ5ID0gX193ZWJwYWNrX3JlcXVpcmVfXygwKTtcblxudmFyIF9qcXVlcnkyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfanF1ZXJ5KTtcblxudmFyIF9vcHRpb25zID0gX193ZWJwYWNrX3JlcXVpcmVfXygzKTtcblxudmFyIF9vcHRpb25zMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX29wdGlvbnMpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG5mdW5jdGlvbiBfY2xhc3NDYWxsQ2hlY2soaW5zdGFuY2UsIENvbnN0cnVjdG9yKSB7IGlmICghKGluc3RhbmNlIGluc3RhbmNlb2YgQ29uc3RydWN0b3IpKSB7IHRocm93IG5ldyBUeXBlRXJyb3IoXCJDYW5ub3QgY2FsbCBhIGNsYXNzIGFzIGEgZnVuY3Rpb25cIik7IH0gfVxuXG4vKipcbiAqIEhhbmRsZXMgZXZlcnl0aGluZyByZWxhdGVkIHRvIHRoZSBVSSBvZiB0aGUgY29sb3JwaWNrZXIgcG9wdXA6IHNob3csIGhpZGUsIHBvc2l0aW9uLC4uLlxuICogQGlnbm9yZVxuICovXG52YXIgUG9wdXBIYW5kbGVyID0gZnVuY3Rpb24gKCkge1xuICAvKipcbiAgICogQHBhcmFtIHtDb2xvcnBpY2tlcn0gY29sb3JwaWNrZXJcbiAgICogQHBhcmFtIHtXaW5kb3d9IHJvb3RcbiAgICovXG4gIGZ1bmN0aW9uIFBvcHVwSGFuZGxlcihjb2xvcnBpY2tlciwgcm9vdCkge1xuICAgIF9jbGFzc0NhbGxDaGVjayh0aGlzLCBQb3B1cEhhbmRsZXIpO1xuXG4gICAgLyoqXG4gICAgICogQHR5cGUge1dpbmRvd31cbiAgICAgKi9cbiAgICB0aGlzLnJvb3QgPSByb290O1xuICAgIC8qKlxuICAgICAqIEB0eXBlIHtDb2xvcnBpY2tlcn1cbiAgICAgKi9cbiAgICB0aGlzLmNvbG9ycGlja2VyID0gY29sb3JwaWNrZXI7XG4gICAgLyoqXG4gICAgICogQHR5cGUge2pRdWVyeX1cbiAgICAgKi9cbiAgICB0aGlzLnBvcG92ZXJUYXJnZXQgPSBudWxsO1xuICAgIC8qKlxuICAgICAqIEB0eXBlIHtqUXVlcnl9XG4gICAgICovXG4gICAgdGhpcy5wb3BvdmVyVGlwID0gbnVsbDtcblxuICAgIC8qKlxuICAgICAqIElmIHRydWUsIHRoZSBsYXRlc3QgY2xpY2sgd2FzIGluc2lkZSB0aGUgcG9wb3ZlclxuICAgICAqIEB0eXBlIHtib29sZWFufVxuICAgICAqL1xuICAgIHRoaXMuY2xpY2tpbmcgPSBmYWxzZTtcbiAgICAvKipcbiAgICAgKiBAdHlwZSB7Ym9vbGVhbn1cbiAgICAgKi9cbiAgICB0aGlzLmhpZGRpbmcgPSBmYWxzZTtcbiAgICAvKipcbiAgICAgKiBAdHlwZSB7Ym9vbGVhbn1cbiAgICAgKi9cbiAgICB0aGlzLnNob3dpbmcgPSBmYWxzZTtcbiAgfVxuXG4gIC8qKlxuICAgKiBAcHJpdmF0ZVxuICAgKiBAcmV0dXJucyB7alF1ZXJ5fGZhbHNlfVxuICAgKi9cblxuXG4gIF9jcmVhdGVDbGFzcyhQb3B1cEhhbmRsZXIsIFt7XG4gICAga2V5OiAnYmluZCcsXG5cblxuICAgIC8qKlxuICAgICAqIEJpbmRzIHRoZSBkaWZmZXJlbnQgY29sb3JwaWNrZXIgZWxlbWVudHMgdG8gdGhlIGZvY3VzL21vdXNlL3RvdWNoIGV2ZW50cyBzbyBpdCByZWFjdHMgaW4gb3JkZXIgdG8gc2hvdyBvclxuICAgICAqIGhpZGUgdGhlIGNvbG9ycGlja2VyIHBvcHVwIGFjY29yZGluZ2x5LiBJdCBhbHNvIGFkZHMgdGhlIHByb3BlciBjbGFzc2VzLlxuICAgICAqL1xuICAgIHZhbHVlOiBmdW5jdGlvbiBiaW5kKCkge1xuICAgICAgdmFyIGNwID0gdGhpcy5jb2xvcnBpY2tlcjtcblxuICAgICAgaWYgKGNwLm9wdGlvbnMuaW5saW5lKSB7XG4gICAgICAgIGNwLnBpY2tlci5hZGRDbGFzcygnY29sb3JwaWNrZXItaW5saW5lIGNvbG9ycGlja2VyLXZpc2libGUnKTtcbiAgICAgICAgcmV0dXJuOyAvLyBubyBuZWVkIHRvIGJpbmQgc2hvdy9oaWRlIGV2ZW50cyBmb3IgaW5saW5lIGVsZW1lbnRzXG4gICAgICB9XG5cbiAgICAgIGNwLnBpY2tlci5hZGRDbGFzcygnY29sb3JwaWNrZXItcG9wdXAgY29sb3JwaWNrZXItaGlkZGVuJyk7XG5cbiAgICAgIC8vIHRoZXJlIGlzIG5vIGlucHV0IG9yIGFkZG9uXG4gICAgICBpZiAoIXRoaXMuaGFzSW5wdXQgJiYgIXRoaXMuaGFzQWRkb24pIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICAvLyBjcmVhdGUgQm9vdHN0cmFwIDQgcG9wb3ZlclxuICAgICAgaWYgKGNwLm9wdGlvbnMucG9wb3Zlcikge1xuICAgICAgICB0aGlzLmNyZWF0ZVBvcG92ZXIoKTtcbiAgICAgIH1cblxuICAgICAgLy8gYmluZCBhZGRvbiBzaG93L2hpZGUgZXZlbnRzXG4gICAgICBpZiAodGhpcy5oYXNBZGRvbikge1xuICAgICAgICAvLyBlbmFibGUgZm9jdXMgb24gYWRkb25zXG4gICAgICAgIGlmICghdGhpcy5hZGRvbi5hdHRyKCd0YWJpbmRleCcpKSB7XG4gICAgICAgICAgdGhpcy5hZGRvbi5hdHRyKCd0YWJpbmRleCcsIDApO1xuICAgICAgICB9XG5cbiAgICAgICAgdGhpcy5hZGRvbi5vbih7XG4gICAgICAgICAgJ21vdXNlZG93bi5jb2xvcnBpY2tlciB0b3VjaHN0YXJ0LmNvbG9ycGlja2VyJzogX2pxdWVyeTIuZGVmYXVsdC5wcm94eSh0aGlzLnRvZ2dsZSwgdGhpcylcbiAgICAgICAgfSk7XG5cbiAgICAgICAgdGhpcy5hZGRvbi5vbih7XG4gICAgICAgICAgJ2ZvY3VzLmNvbG9ycGlja2VyJzogX2pxdWVyeTIuZGVmYXVsdC5wcm94eSh0aGlzLnNob3csIHRoaXMpXG4gICAgICAgIH0pO1xuXG4gICAgICAgIHRoaXMuYWRkb24ub24oe1xuICAgICAgICAgICdmb2N1c291dC5jb2xvcnBpY2tlcic6IF9qcXVlcnkyLmRlZmF1bHQucHJveHkodGhpcy5oaWRlLCB0aGlzKVxuICAgICAgICB9KTtcbiAgICAgIH1cblxuICAgICAgLy8gYmluZCBpbnB1dCBzaG93L2hpZGUgZXZlbnRzXG4gICAgICBpZiAodGhpcy5oYXNJbnB1dCAmJiAhdGhpcy5oYXNBZGRvbikge1xuICAgICAgICB0aGlzLmlucHV0Lm9uKHtcbiAgICAgICAgICAnbW91c2Vkb3duLmNvbG9ycGlja2VyIHRvdWNoc3RhcnQuY29sb3JwaWNrZXInOiBfanF1ZXJ5Mi5kZWZhdWx0LnByb3h5KHRoaXMuc2hvdywgdGhpcyksXG4gICAgICAgICAgJ2ZvY3VzLmNvbG9ycGlja2VyJzogX2pxdWVyeTIuZGVmYXVsdC5wcm94eSh0aGlzLnNob3csIHRoaXMpXG4gICAgICAgIH0pO1xuXG4gICAgICAgIHRoaXMuaW5wdXQub24oe1xuICAgICAgICAgICdmb2N1c291dC5jb2xvcnBpY2tlcic6IF9qcXVlcnkyLmRlZmF1bHQucHJveHkodGhpcy5oaWRlLCB0aGlzKVxuICAgICAgICB9KTtcbiAgICAgIH1cblxuICAgICAgLy8gcmVwb3NpdGlvbiBwb3B1cCBvbiB3aW5kb3cgcmVzaXplXG4gICAgICAoMCwgX2pxdWVyeTIuZGVmYXVsdCkodGhpcy5yb290KS5vbigncmVzaXplLmNvbG9ycGlja2VyJywgX2pxdWVyeTIuZGVmYXVsdC5wcm94eSh0aGlzLnJlcG9zaXRpb24sIHRoaXMpKTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBVbmJpbmRzIGFueSBldmVudCBib3VuZCBieSB0aGlzIGhhbmRsZXJcbiAgICAgKi9cblxuICB9LCB7XG4gICAga2V5OiAndW5iaW5kJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gdW5iaW5kKCkge1xuICAgICAgaWYgKHRoaXMuaGFzSW5wdXQpIHtcbiAgICAgICAgdGhpcy5pbnB1dC5vZmYoe1xuICAgICAgICAgICdtb3VzZWRvd24uY29sb3JwaWNrZXIgdG91Y2hzdGFydC5jb2xvcnBpY2tlcic6IF9qcXVlcnkyLmRlZmF1bHQucHJveHkodGhpcy5zaG93LCB0aGlzKSxcbiAgICAgICAgICAnZm9jdXMuY29sb3JwaWNrZXInOiBfanF1ZXJ5Mi5kZWZhdWx0LnByb3h5KHRoaXMuc2hvdywgdGhpcylcbiAgICAgICAgfSk7XG4gICAgICAgIHRoaXMuaW5wdXQub2ZmKHtcbiAgICAgICAgICAnZm9jdXNvdXQuY29sb3JwaWNrZXInOiBfanF1ZXJ5Mi5kZWZhdWx0LnByb3h5KHRoaXMuaGlkZSwgdGhpcylcbiAgICAgICAgfSk7XG4gICAgICB9XG5cbiAgICAgIGlmICh0aGlzLmhhc0FkZG9uKSB7XG4gICAgICAgIHRoaXMuYWRkb24ub2ZmKHtcbiAgICAgICAgICAnbW91c2Vkb3duLmNvbG9ycGlja2VyIHRvdWNoc3RhcnQuY29sb3JwaWNrZXInOiBfanF1ZXJ5Mi5kZWZhdWx0LnByb3h5KHRoaXMudG9nZ2xlLCB0aGlzKVxuICAgICAgICB9KTtcbiAgICAgICAgdGhpcy5hZGRvbi5vZmYoe1xuICAgICAgICAgICdmb2N1cy5jb2xvcnBpY2tlcic6IF9qcXVlcnkyLmRlZmF1bHQucHJveHkodGhpcy5zaG93LCB0aGlzKVxuICAgICAgICB9KTtcbiAgICAgICAgdGhpcy5hZGRvbi5vZmYoe1xuICAgICAgICAgICdmb2N1c291dC5jb2xvcnBpY2tlcic6IF9qcXVlcnkyLmRlZmF1bHQucHJveHkodGhpcy5oaWRlLCB0aGlzKVxuICAgICAgICB9KTtcbiAgICAgIH1cblxuICAgICAgaWYgKHRoaXMucG9wb3ZlclRhcmdldCkge1xuICAgICAgICB0aGlzLnBvcG92ZXJUYXJnZXQucG9wb3ZlcignZGlzcG9zZScpO1xuICAgICAgfVxuXG4gICAgICAoMCwgX2pxdWVyeTIuZGVmYXVsdCkodGhpcy5yb290KS5vZmYoJ3Jlc2l6ZS5jb2xvcnBpY2tlcicsIF9qcXVlcnkyLmRlZmF1bHQucHJveHkodGhpcy5yZXBvc2l0aW9uLCB0aGlzKSk7XG4gICAgICAoMCwgX2pxdWVyeTIuZGVmYXVsdCkodGhpcy5yb290LmRvY3VtZW50KS5vZmYoJ21vdXNlZG93bi5jb2xvcnBpY2tlciB0b3VjaHN0YXJ0LmNvbG9ycGlja2VyJywgX2pxdWVyeTIuZGVmYXVsdC5wcm94eSh0aGlzLmhpZGUsIHRoaXMpKTtcbiAgICAgICgwLCBfanF1ZXJ5Mi5kZWZhdWx0KSh0aGlzLnJvb3QuZG9jdW1lbnQpLm9mZignbW91c2Vkb3duLmNvbG9ycGlja2VyIHRvdWNoc3RhcnQuY29sb3JwaWNrZXInLCBfanF1ZXJ5Mi5kZWZhdWx0LnByb3h5KHRoaXMub25DbGlja2luZ0luc2lkZSwgdGhpcykpO1xuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ2lzQ2xpY2tpbmdJbnNpZGUnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBpc0NsaWNraW5nSW5zaWRlKGUpIHtcbiAgICAgIGlmICghZSkge1xuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICB9XG5cbiAgICAgIHJldHVybiB0aGlzLmlzT3JJc0luc2lkZSh0aGlzLnBvcG92ZXJUaXAsIGUuY3VycmVudFRhcmdldCkgfHwgdGhpcy5pc09ySXNJbnNpZGUodGhpcy5wb3BvdmVyVGlwLCBlLnRhcmdldCkgfHwgdGhpcy5pc09ySXNJbnNpZGUodGhpcy5jb2xvcnBpY2tlci5waWNrZXIsIGUuY3VycmVudFRhcmdldCkgfHwgdGhpcy5pc09ySXNJbnNpZGUodGhpcy5jb2xvcnBpY2tlci5waWNrZXIsIGUudGFyZ2V0KTtcbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdpc09ySXNJbnNpZGUnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBpc09ySXNJbnNpZGUoY29udGFpbmVyLCBlbGVtZW50KSB7XG4gICAgICBpZiAoIWNvbnRhaW5lciB8fCAhZWxlbWVudCkge1xuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICB9XG5cbiAgICAgIGVsZW1lbnQgPSAoMCwgX2pxdWVyeTIuZGVmYXVsdCkoZWxlbWVudCk7XG5cbiAgICAgIHJldHVybiBlbGVtZW50LmlzKGNvbnRhaW5lcikgfHwgY29udGFpbmVyLmZpbmQoZWxlbWVudCkubGVuZ3RoID4gMDtcbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdvbkNsaWNraW5nSW5zaWRlJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gb25DbGlja2luZ0luc2lkZShlKSB7XG4gICAgICB0aGlzLmNsaWNraW5nID0gdGhpcy5pc0NsaWNraW5nSW5zaWRlKGUpO1xuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ2NyZWF0ZVBvcG92ZXInLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBjcmVhdGVQb3BvdmVyKCkge1xuICAgICAgdmFyIGNwID0gdGhpcy5jb2xvcnBpY2tlcjtcblxuICAgICAgdGhpcy5wb3BvdmVyVGFyZ2V0ID0gdGhpcy5oYXNBZGRvbiA/IHRoaXMuYWRkb24gOiB0aGlzLmlucHV0O1xuXG4gICAgICBjcC5waWNrZXIuYWRkQ2xhc3MoJ2NvbG9ycGlja2VyLWJzLXBvcG92ZXItY29udGVudCcpO1xuXG4gICAgICB0aGlzLnBvcG92ZXJUYXJnZXQucG9wb3ZlcihfanF1ZXJ5Mi5kZWZhdWx0LmV4dGVuZCh0cnVlLCB7fSwgX29wdGlvbnMyLmRlZmF1bHQucG9wb3ZlciwgY3Aub3B0aW9ucy5wb3BvdmVyLCB7IHRyaWdnZXI6ICdtYW51YWwnLCBjb250ZW50OiBjcC5waWNrZXIsIGh0bWw6IHRydWUgfSkpO1xuXG4gICAgICB0aGlzLnBvcG92ZXJUaXAgPSAoMCwgX2pxdWVyeTIuZGVmYXVsdCkodGhpcy5wb3BvdmVyVGFyZ2V0LnBvcG92ZXIoJ2dldFRpcEVsZW1lbnQnKS5kYXRhKCdicy5wb3BvdmVyJykudGlwKTtcbiAgICAgIHRoaXMucG9wb3ZlclRpcC5hZGRDbGFzcygnY29sb3JwaWNrZXItYnMtcG9wb3ZlcicpO1xuXG4gICAgICB0aGlzLnBvcG92ZXJUYXJnZXQub24oJ3Nob3duLmJzLnBvcG92ZXInLCBfanF1ZXJ5Mi5kZWZhdWx0LnByb3h5KHRoaXMuZmlyZVNob3csIHRoaXMpKTtcbiAgICAgIHRoaXMucG9wb3ZlclRhcmdldC5vbignaGlkZGVuLmJzLnBvcG92ZXInLCBfanF1ZXJ5Mi5kZWZhdWx0LnByb3h5KHRoaXMuZmlyZUhpZGUsIHRoaXMpKTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBJZiB0aGUgd2lkZ2V0IGlzIG5vdCBpbnNpZGUgYSBjb250YWluZXIgb3IgaW5saW5lLCByZWFycmFuZ2VzIGl0cyBwb3NpdGlvbiByZWxhdGl2ZSB0byBpdHMgZWxlbWVudCBvZmZzZXQuXG4gICAgICpcbiAgICAgKiBAcGFyYW0ge0V2ZW50fSBbZV1cbiAgICAgKiBAcHJpdmF0ZVxuICAgICAqL1xuXG4gIH0sIHtcbiAgICBrZXk6ICdyZXBvc2l0aW9uJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gcmVwb3NpdGlvbihlKSB7XG4gICAgICBpZiAodGhpcy5wb3BvdmVyVGFyZ2V0ICYmIHRoaXMuaXNWaXNpYmxlKCkpIHtcbiAgICAgICAgdGhpcy5wb3BvdmVyVGFyZ2V0LnBvcG92ZXIoJ3VwZGF0ZScpO1xuICAgICAgfVxuICAgIH1cblxuICAgIC8qKlxuICAgICAqIFRvZ2dsZXMgdGhlIGNvbG9ycGlja2VyIGJldHdlZW4gdmlzaWJsZSBvciBoaWRkZW5cbiAgICAgKlxuICAgICAqIEBmaXJlcyBDb2xvcnBpY2tlciNjb2xvcnBpY2tlclNob3dcbiAgICAgKiBAZmlyZXMgQ29sb3JwaWNrZXIjY29sb3JwaWNrZXJIaWRlXG4gICAgICogQHBhcmFtIHtFdmVudH0gW2VdXG4gICAgICovXG5cbiAgfSwge1xuICAgIGtleTogJ3RvZ2dsZScsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHRvZ2dsZShlKSB7XG4gICAgICBpZiAodGhpcy5pc1Zpc2libGUoKSkge1xuICAgICAgICB0aGlzLmhpZGUoZSk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICB0aGlzLnNob3coZSk7XG4gICAgICB9XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogU2hvd3MgdGhlIGNvbG9ycGlja2VyIHdpZGdldCBpZiBoaWRkZW4uXG4gICAgICpcbiAgICAgKiBAZmlyZXMgQ29sb3JwaWNrZXIjY29sb3JwaWNrZXJTaG93XG4gICAgICogQHBhcmFtIHtFdmVudH0gW2VdXG4gICAgICovXG5cbiAgfSwge1xuICAgIGtleTogJ3Nob3cnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBzaG93KGUpIHtcbiAgICAgIGlmICh0aGlzLmlzVmlzaWJsZSgpIHx8IHRoaXMuc2hvd2luZyB8fCB0aGlzLmhpZGRpbmcpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICB0aGlzLnNob3dpbmcgPSB0cnVlO1xuICAgICAgdGhpcy5oaWRkaW5nID0gZmFsc2U7XG4gICAgICB0aGlzLmNsaWNraW5nID0gZmFsc2U7XG5cbiAgICAgIHZhciBjcCA9IHRoaXMuY29sb3JwaWNrZXI7XG5cbiAgICAgIGNwLmxhc3RFdmVudC5hbGlhcyA9ICdzaG93JztcbiAgICAgIGNwLmxhc3RFdmVudC5lID0gZTtcblxuICAgICAgLy8gUHJldmVudCBzaG93aW5nIGJyb3dzZXIgbmF0aXZlIEhUTUw1IGNvbG9ycGlja2VyXG4gICAgICBpZiAoZSAmJiAoIXRoaXMuaGFzSW5wdXQgfHwgdGhpcy5pbnB1dC5hdHRyKCd0eXBlJykgPT09ICdjb2xvcicpICYmIGUgJiYgZS5wcmV2ZW50RGVmYXVsdCkge1xuICAgICAgICBlLnN0b3BQcm9wYWdhdGlvbigpO1xuICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgICB9XG5cbiAgICAgIC8vIElmIGl0J3MgYSBwb3BvdmVyLCBhZGQgZXZlbnQgdG8gdGhlIGRvY3VtZW50IHRvIGhpZGUgdGhlIHBpY2tlciB3aGVuIGNsaWNraW5nIG91dHNpZGUgb2YgaXRcbiAgICAgIGlmICh0aGlzLmlzUG9wb3Zlcikge1xuICAgICAgICAoMCwgX2pxdWVyeTIuZGVmYXVsdCkodGhpcy5yb290KS5vbigncmVzaXplLmNvbG9ycGlja2VyJywgX2pxdWVyeTIuZGVmYXVsdC5wcm94eSh0aGlzLnJlcG9zaXRpb24sIHRoaXMpKTtcbiAgICAgIH1cblxuICAgICAgLy8gYWRkIHZpc2libGUgY2xhc3MgYmVmb3JlIHBvcG92ZXIgaXMgc2hvd25cbiAgICAgIGNwLnBpY2tlci5hZGRDbGFzcygnY29sb3JwaWNrZXItdmlzaWJsZScpLnJlbW92ZUNsYXNzKCdjb2xvcnBpY2tlci1oaWRkZW4nKTtcblxuICAgICAgaWYgKHRoaXMucG9wb3ZlclRhcmdldCkge1xuICAgICAgICB0aGlzLnBvcG92ZXJUYXJnZXQucG9wb3Zlcignc2hvdycpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdGhpcy5maXJlU2hvdygpO1xuICAgICAgfVxuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ2ZpcmVTaG93JyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gZmlyZVNob3coKSB7XG4gICAgICB0aGlzLmhpZGRpbmcgPSBmYWxzZTtcbiAgICAgIHRoaXMuc2hvd2luZyA9IGZhbHNlO1xuXG4gICAgICBpZiAodGhpcy5pc1BvcG92ZXIpIHtcbiAgICAgICAgLy8gQWRkIGV2ZW50IHRvIGhpZGUgb24gb3V0c2lkZSBjbGlja1xuICAgICAgICAoMCwgX2pxdWVyeTIuZGVmYXVsdCkodGhpcy5yb290LmRvY3VtZW50KS5vbignbW91c2Vkb3duLmNvbG9ycGlja2VyIHRvdWNoc3RhcnQuY29sb3JwaWNrZXInLCBfanF1ZXJ5Mi5kZWZhdWx0LnByb3h5KHRoaXMuaGlkZSwgdGhpcykpO1xuICAgICAgICAoMCwgX2pxdWVyeTIuZGVmYXVsdCkodGhpcy5yb290LmRvY3VtZW50KS5vbignbW91c2Vkb3duLmNvbG9ycGlja2VyIHRvdWNoc3RhcnQuY29sb3JwaWNrZXInLCBfanF1ZXJ5Mi5kZWZhdWx0LnByb3h5KHRoaXMub25DbGlja2luZ0luc2lkZSwgdGhpcykpO1xuICAgICAgfVxuXG4gICAgICAvKipcbiAgICAgICAqIChDb2xvcnBpY2tlcikgV2hlbiBzaG93KCkgaXMgY2FsbGVkIGFuZCB0aGUgd2lkZ2V0IGNhbiBiZSBzaG93bi5cbiAgICAgICAqXG4gICAgICAgKiBAZXZlbnQgQ29sb3JwaWNrZXIjY29sb3JwaWNrZXJTaG93XG4gICAgICAgKi9cbiAgICAgIHRoaXMuY29sb3JwaWNrZXIudHJpZ2dlcignY29sb3JwaWNrZXJTaG93Jyk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogSGlkZXMgdGhlIGNvbG9ycGlja2VyIHdpZGdldC5cbiAgICAgKiBIaWRlIGlzIHByZXZlbnRlZCB3aGVuIGl0IGlzIHRyaWdnZXJlZCBieSBhbiBldmVudCB3aG9zZSB0YXJnZXQgZWxlbWVudCBoYXMgYmVlbiBjbGlja2VkL3RvdWNoZWQuXG4gICAgICpcbiAgICAgKiBAZmlyZXMgQ29sb3JwaWNrZXIjY29sb3JwaWNrZXJIaWRlXG4gICAgICogQHBhcmFtIHtFdmVudH0gW2VdXG4gICAgICovXG5cbiAgfSwge1xuICAgIGtleTogJ2hpZGUnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBoaWRlKGUpIHtcbiAgICAgIGlmICh0aGlzLmlzSGlkZGVuKCkgfHwgdGhpcy5zaG93aW5nIHx8IHRoaXMuaGlkZGluZykge1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG5cbiAgICAgIHZhciBjcCA9IHRoaXMuY29sb3JwaWNrZXIsXG4gICAgICAgICAgY2xpY2tpbmcgPSB0aGlzLmNsaWNraW5nIHx8IHRoaXMuaXNDbGlja2luZ0luc2lkZShlKTtcblxuICAgICAgdGhpcy5oaWRkaW5nID0gdHJ1ZTtcbiAgICAgIHRoaXMuc2hvd2luZyA9IGZhbHNlO1xuICAgICAgdGhpcy5jbGlja2luZyA9IGZhbHNlO1xuXG4gICAgICBjcC5sYXN0RXZlbnQuYWxpYXMgPSAnaGlkZSc7XG4gICAgICBjcC5sYXN0RXZlbnQuZSA9IGU7XG5cbiAgICAgIC8vIFRPRE86IGZpeCBoYXZpbmcgdG8gY2xpY2sgdHdpY2Ugb3V0c2lkZSB3aGVuIGxvc2luZyBmb2N1cyBhbmQgbGFzdCAyIGNsaWNrcyB3aGVyZSBpbnNpZGUgdGhlIGNvbG9ycGlja2VyXG5cbiAgICAgIC8vIFByZXZlbnQgaGlkZSBpZiB0cmlnZ2VyZWQgYnkgYW4gZXZlbnQgYW5kIGFuIGVsZW1lbnQgaW5zaWRlIHRoZSBjb2xvcnBpY2tlciBoYXMgYmVlbiBjbGlja2VkL3RvdWNoZWRcbiAgICAgIGlmIChjbGlja2luZykge1xuICAgICAgICB0aGlzLmhpZGRpbmcgPSBmYWxzZTtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICBpZiAodGhpcy5wb3BvdmVyVGFyZ2V0KSB7XG4gICAgICAgIHRoaXMucG9wb3ZlclRhcmdldC5wb3BvdmVyKCdoaWRlJyk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICB0aGlzLmZpcmVIaWRlKCk7XG4gICAgICB9XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnZmlyZUhpZGUnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBmaXJlSGlkZSgpIHtcbiAgICAgIHRoaXMuaGlkZGluZyA9IGZhbHNlO1xuICAgICAgdGhpcy5zaG93aW5nID0gZmFsc2U7XG5cbiAgICAgIHZhciBjcCA9IHRoaXMuY29sb3JwaWNrZXI7XG5cbiAgICAgIC8vIGFkZCBoaWRkZW4gY2xhc3MgYWZ0ZXIgcG9wb3ZlciBpcyBoaWRkZW5cbiAgICAgIGNwLnBpY2tlci5hZGRDbGFzcygnY29sb3JwaWNrZXItaGlkZGVuJykucmVtb3ZlQ2xhc3MoJ2NvbG9ycGlja2VyLXZpc2libGUnKTtcblxuICAgICAgLy8gVW5iaW5kIHdpbmRvdyBhbmQgZG9jdW1lbnQgZXZlbnRzLCBzaW5jZSB0aGVyZSBpcyBubyBuZWVkIHRvIGtlZXAgdGhlbSB3aGlsZSB0aGUgcG9wdXAgaXMgaGlkZGVuXG4gICAgICAoMCwgX2pxdWVyeTIuZGVmYXVsdCkodGhpcy5yb290KS5vZmYoJ3Jlc2l6ZS5jb2xvcnBpY2tlcicsIF9qcXVlcnkyLmRlZmF1bHQucHJveHkodGhpcy5yZXBvc2l0aW9uLCB0aGlzKSk7XG4gICAgICAoMCwgX2pxdWVyeTIuZGVmYXVsdCkodGhpcy5yb290LmRvY3VtZW50KS5vZmYoJ21vdXNlZG93bi5jb2xvcnBpY2tlciB0b3VjaHN0YXJ0LmNvbG9ycGlja2VyJywgX2pxdWVyeTIuZGVmYXVsdC5wcm94eSh0aGlzLmhpZGUsIHRoaXMpKTtcbiAgICAgICgwLCBfanF1ZXJ5Mi5kZWZhdWx0KSh0aGlzLnJvb3QuZG9jdW1lbnQpLm9mZignbW91c2Vkb3duLmNvbG9ycGlja2VyIHRvdWNoc3RhcnQuY29sb3JwaWNrZXInLCBfanF1ZXJ5Mi5kZWZhdWx0LnByb3h5KHRoaXMub25DbGlja2luZ0luc2lkZSwgdGhpcykpO1xuXG4gICAgICAvKipcbiAgICAgICAqIChDb2xvcnBpY2tlcikgV2hlbiBoaWRlKCkgaXMgY2FsbGVkIGFuZCB0aGUgd2lkZ2V0IGNhbiBiZSBoaWRkZW4uXG4gICAgICAgKlxuICAgICAgICogQGV2ZW50IENvbG9ycGlja2VyI2NvbG9ycGlja2VySGlkZVxuICAgICAgICovXG4gICAgICBjcC50cmlnZ2VyKCdjb2xvcnBpY2tlckhpZGUnKTtcbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdmb2N1cycsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGZvY3VzKCkge1xuICAgICAgaWYgKHRoaXMuaGFzQWRkb24pIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuYWRkb24uZm9jdXMoKTtcbiAgICAgIH1cbiAgICAgIGlmICh0aGlzLmhhc0lucHV0KSB7XG4gICAgICAgIHJldHVybiB0aGlzLmlucHV0LmZvY3VzKCk7XG4gICAgICB9XG4gICAgICByZXR1cm4gZmFsc2U7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogUmV0dXJucyB0cnVlIGlmIHRoZSBjb2xvcnBpY2tlciBlbGVtZW50IGhhcyB0aGUgY29sb3JwaWNrZXItdmlzaWJsZSBjbGFzcyBhbmQgbm90IHRoZSBjb2xvcnBpY2tlci1oaWRkZW4gb25lLlxuICAgICAqIEZhbHNlIG90aGVyd2lzZS5cbiAgICAgKlxuICAgICAqIEByZXR1cm5zIHtib29sZWFufVxuICAgICAqL1xuXG4gIH0sIHtcbiAgICBrZXk6ICdpc1Zpc2libGUnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBpc1Zpc2libGUoKSB7XG4gICAgICByZXR1cm4gdGhpcy5jb2xvcnBpY2tlci5waWNrZXIuaGFzQ2xhc3MoJ2NvbG9ycGlja2VyLXZpc2libGUnKSAmJiAhdGhpcy5jb2xvcnBpY2tlci5waWNrZXIuaGFzQ2xhc3MoJ2NvbG9ycGlja2VyLWhpZGRlbicpO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIFJldHVybnMgdHJ1ZSBpZiB0aGUgY29sb3JwaWNrZXIgZWxlbWVudCBoYXMgdGhlIGNvbG9ycGlja2VyLWhpZGRlbiBjbGFzcyBhbmQgbm90IHRoZSBjb2xvcnBpY2tlci12aXNpYmxlIG9uZS5cbiAgICAgKiBGYWxzZSBvdGhlcndpc2UuXG4gICAgICpcbiAgICAgKiBAcmV0dXJucyB7Ym9vbGVhbn1cbiAgICAgKi9cblxuICB9LCB7XG4gICAga2V5OiAnaXNIaWRkZW4nLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBpc0hpZGRlbigpIHtcbiAgICAgIHJldHVybiB0aGlzLmNvbG9ycGlja2VyLnBpY2tlci5oYXNDbGFzcygnY29sb3JwaWNrZXItaGlkZGVuJykgJiYgIXRoaXMuY29sb3JwaWNrZXIucGlja2VyLmhhc0NsYXNzKCdjb2xvcnBpY2tlci12aXNpYmxlJyk7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnaW5wdXQnLFxuICAgIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgICAgcmV0dXJuIHRoaXMuY29sb3JwaWNrZXIuaW5wdXRIYW5kbGVyLmlucHV0O1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEBwcml2YXRlXG4gICAgICogQHJldHVybnMge2Jvb2xlYW59XG4gICAgICovXG5cbiAgfSwge1xuICAgIGtleTogJ2hhc0lucHV0JyxcbiAgICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICAgIHJldHVybiB0aGlzLmNvbG9ycGlja2VyLmlucHV0SGFuZGxlci5oYXNJbnB1dCgpO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEBwcml2YXRlXG4gICAgICogQHJldHVybnMge2pRdWVyeXxmYWxzZX1cbiAgICAgKi9cblxuICB9LCB7XG4gICAga2V5OiAnYWRkb24nLFxuICAgIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgICAgcmV0dXJuIHRoaXMuY29sb3JwaWNrZXIuYWRkb25IYW5kbGVyLmFkZG9uO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEBwcml2YXRlXG4gICAgICogQHJldHVybnMge2Jvb2xlYW59XG4gICAgICovXG5cbiAgfSwge1xuICAgIGtleTogJ2hhc0FkZG9uJyxcbiAgICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICAgIHJldHVybiB0aGlzLmNvbG9ycGlja2VyLmFkZG9uSGFuZGxlci5oYXNBZGRvbigpO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEBwcml2YXRlXG4gICAgICogQHJldHVybnMge2Jvb2xlYW59XG4gICAgICovXG5cbiAgfSwge1xuICAgIGtleTogJ2lzUG9wb3ZlcicsXG4gICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgICByZXR1cm4gIXRoaXMuY29sb3JwaWNrZXIub3B0aW9ucy5pbmxpbmUgJiYgISF0aGlzLnBvcG92ZXJUaXA7XG4gICAgfVxuICB9XSk7XG5cbiAgcmV0dXJuIFBvcHVwSGFuZGxlcjtcbn0oKTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gUG9wdXBIYW5kbGVyO1xubW9kdWxlLmV4cG9ydHMgPSBleHBvcnRzLmRlZmF1bHQ7XG5cbi8qKiovIH0pLFxuLyogMTUgKi9cbi8qKiovIChmdW5jdGlvbihtb2R1bGUsIGV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pIHtcblxuXCJ1c2Ugc3RyaWN0XCI7XG5cblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9jcmVhdGVDbGFzcyA9IGZ1bmN0aW9uICgpIHsgZnVuY3Rpb24gZGVmaW5lUHJvcGVydGllcyh0YXJnZXQsIHByb3BzKSB7IGZvciAodmFyIGkgPSAwOyBpIDwgcHJvcHMubGVuZ3RoOyBpKyspIHsgdmFyIGRlc2NyaXB0b3IgPSBwcm9wc1tpXTsgZGVzY3JpcHRvci5lbnVtZXJhYmxlID0gZGVzY3JpcHRvci5lbnVtZXJhYmxlIHx8IGZhbHNlOyBkZXNjcmlwdG9yLmNvbmZpZ3VyYWJsZSA9IHRydWU7IGlmIChcInZhbHVlXCIgaW4gZGVzY3JpcHRvcikgZGVzY3JpcHRvci53cml0YWJsZSA9IHRydWU7IE9iamVjdC5kZWZpbmVQcm9wZXJ0eSh0YXJnZXQsIGRlc2NyaXB0b3Iua2V5LCBkZXNjcmlwdG9yKTsgfSB9IHJldHVybiBmdW5jdGlvbiAoQ29uc3RydWN0b3IsIHByb3RvUHJvcHMsIHN0YXRpY1Byb3BzKSB7IGlmIChwcm90b1Byb3BzKSBkZWZpbmVQcm9wZXJ0aWVzKENvbnN0cnVjdG9yLnByb3RvdHlwZSwgcHJvdG9Qcm9wcyk7IGlmIChzdGF0aWNQcm9wcykgZGVmaW5lUHJvcGVydGllcyhDb25zdHJ1Y3Rvciwgc3RhdGljUHJvcHMpOyByZXR1cm4gQ29uc3RydWN0b3I7IH07IH0oKTtcblxudmFyIF9qcXVlcnkgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKDApO1xuXG52YXIgX2pxdWVyeTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9qcXVlcnkpO1xuXG52YXIgX0NvbG9ySXRlbSA9IF9fd2VicGFja19yZXF1aXJlX18oMik7XG5cbnZhciBfQ29sb3JJdGVtMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX0NvbG9ySXRlbSk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbmZ1bmN0aW9uIF9jbGFzc0NhbGxDaGVjayhpbnN0YW5jZSwgQ29uc3RydWN0b3IpIHsgaWYgKCEoaW5zdGFuY2UgaW5zdGFuY2VvZiBDb25zdHJ1Y3RvcikpIHsgdGhyb3cgbmV3IFR5cGVFcnJvcihcIkNhbm5vdCBjYWxsIGEgY2xhc3MgYXMgYSBmdW5jdGlvblwiKTsgfSB9XG5cbi8qKlxuICogSGFuZGxlcyBldmVyeXRoaW5nIHJlbGF0ZWQgdG8gdGhlIGNvbG9ycGlja2VyIGlucHV0XG4gKiBAaWdub3JlXG4gKi9cbnZhciBJbnB1dEhhbmRsZXIgPSBmdW5jdGlvbiAoKSB7XG4gIC8qKlxuICAgKiBAcGFyYW0ge0NvbG9ycGlja2VyfSBjb2xvcnBpY2tlclxuICAgKi9cbiAgZnVuY3Rpb24gSW5wdXRIYW5kbGVyKGNvbG9ycGlja2VyKSB7XG4gICAgX2NsYXNzQ2FsbENoZWNrKHRoaXMsIElucHV0SGFuZGxlcik7XG5cbiAgICAvKipcbiAgICAgKiBAdHlwZSB7Q29sb3JwaWNrZXJ9XG4gICAgICovXG4gICAgdGhpcy5jb2xvcnBpY2tlciA9IGNvbG9ycGlja2VyO1xuICAgIC8qKlxuICAgICAqIEB0eXBlIHtqUXVlcnl8ZmFsc2V9XG4gICAgICovXG4gICAgdGhpcy5pbnB1dCA9IHRoaXMuY29sb3JwaWNrZXIuZWxlbWVudC5pcygnaW5wdXQnKSA/IHRoaXMuY29sb3JwaWNrZXIuZWxlbWVudCA6IHRoaXMuY29sb3JwaWNrZXIub3B0aW9ucy5pbnB1dCA/IHRoaXMuY29sb3JwaWNrZXIuZWxlbWVudC5maW5kKHRoaXMuY29sb3JwaWNrZXIub3B0aW9ucy5pbnB1dCkgOiBmYWxzZTtcblxuICAgIGlmICh0aGlzLmlucHV0ICYmIHRoaXMuaW5wdXQubGVuZ3RoID09PSAwKSB7XG4gICAgICB0aGlzLmlucHV0ID0gZmFsc2U7XG4gICAgfVxuXG4gICAgdGhpcy5faW5pdFZhbHVlKCk7XG4gIH1cblxuICBfY3JlYXRlQ2xhc3MoSW5wdXRIYW5kbGVyLCBbe1xuICAgIGtleTogJ2JpbmQnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBiaW5kKCkge1xuICAgICAgaWYgKCF0aGlzLmhhc0lucHV0KCkpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuICAgICAgdGhpcy5pbnB1dC5vbih7XG4gICAgICAgICdrZXl1cC5jb2xvcnBpY2tlcic6IF9qcXVlcnkyLmRlZmF1bHQucHJveHkodGhpcy5vbmtleXVwLCB0aGlzKVxuICAgICAgfSk7XG4gICAgICB0aGlzLmlucHV0Lm9uKHtcbiAgICAgICAgJ2NoYW5nZS5jb2xvcnBpY2tlcic6IF9qcXVlcnkyLmRlZmF1bHQucHJveHkodGhpcy5vbmNoYW5nZSwgdGhpcylcbiAgICAgIH0pO1xuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ3VuYmluZCcsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHVuYmluZCgpIHtcbiAgICAgIGlmICghdGhpcy5oYXNJbnB1dCgpKSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cbiAgICAgIHRoaXMuaW5wdXQub2ZmKCcuY29sb3JwaWNrZXInKTtcbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdfaW5pdFZhbHVlJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gX2luaXRWYWx1ZSgpIHtcbiAgICAgIGlmICghdGhpcy5oYXNJbnB1dCgpKSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cblxuICAgICAgdmFyIHZhbCA9ICcnO1xuXG4gICAgICBbXG4gICAgICAvLyBjYW5kaWRhdGVzOlxuICAgICAgdGhpcy5pbnB1dC52YWwoKSwgdGhpcy5pbnB1dC5kYXRhKCdjb2xvcicpLCB0aGlzLmlucHV0LmF0dHIoJ2RhdGEtY29sb3InKV0ubWFwKGZ1bmN0aW9uIChpdGVtKSB7XG4gICAgICAgIGlmIChpdGVtICYmIHZhbCA9PT0gJycpIHtcbiAgICAgICAgICB2YWwgPSBpdGVtO1xuICAgICAgICB9XG4gICAgICB9KTtcblxuICAgICAgaWYgKHZhbCBpbnN0YW5jZW9mIF9Db2xvckl0ZW0yLmRlZmF1bHQpIHtcbiAgICAgICAgdmFsID0gdGhpcy5nZXRGb3JtYXR0ZWRDb2xvcih2YWwuc3RyaW5nKHRoaXMuY29sb3JwaWNrZXIuZm9ybWF0KSk7XG4gICAgICB9IGVsc2UgaWYgKCEodHlwZW9mIHZhbCA9PT0gJ3N0cmluZycgfHwgdmFsIGluc3RhbmNlb2YgU3RyaW5nKSkge1xuICAgICAgICB2YWwgPSAnJztcbiAgICAgIH1cblxuICAgICAgdGhpcy5pbnB1dC5wcm9wKCd2YWx1ZScsIHZhbCk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogUmV0dXJucyB0aGUgY29sb3Igc3RyaW5nIGZyb20gdGhlIGlucHV0IHZhbHVlLlxuICAgICAqIElmIHRoZXJlIGlzIG5vIGlucHV0IHRoZSByZXR1cm4gdmFsdWUgaXMgZmFsc2UuXG4gICAgICpcbiAgICAgKiBAcmV0dXJucyB7U3RyaW5nfGJvb2xlYW59XG4gICAgICovXG5cbiAgfSwge1xuICAgIGtleTogJ2dldFZhbHVlJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gZ2V0VmFsdWUoKSB7XG4gICAgICBpZiAoIXRoaXMuaGFzSW5wdXQoKSkge1xuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICB9XG5cbiAgICAgIHJldHVybiB0aGlzLmlucHV0LnZhbCgpO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIElmIHRoZSBpbnB1dCBlbGVtZW50IGlzIHByZXNlbnQsIGl0IHVwZGF0ZXMgdGhlIHZhbHVlIHdpdGggdGhlIGN1cnJlbnQgY29sb3Igb2JqZWN0IGNvbG9yIHN0cmluZy5cbiAgICAgKiBJZiB0aGUgdmFsdWUgaXMgY2hhbmdlZCwgdGhpcyBtZXRob2QgZmlyZXMgYSBcImNoYW5nZVwiIGV2ZW50IG9uIHRoZSBpbnB1dCBlbGVtZW50LlxuICAgICAqXG4gICAgICogQHBhcmFtIHtTdHJpbmd9IHZhbFxuICAgICAqXG4gICAgICogQGZpcmVzIENvbG9ycGlja2VyI2NoYW5nZVxuICAgICAqL1xuXG4gIH0sIHtcbiAgICBrZXk6ICdzZXRWYWx1ZScsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHNldFZhbHVlKHZhbCkge1xuICAgICAgaWYgKCF0aGlzLmhhc0lucHV0KCkpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICB2YXIgaW5wdXRWYWwgPSB0aGlzLmlucHV0LnByb3AoJ3ZhbHVlJyk7XG5cbiAgICAgIHZhbCA9IHZhbCA/IHZhbCA6ICcnO1xuXG4gICAgICBpZiAodmFsID09PSAoaW5wdXRWYWwgPyBpbnB1dFZhbCA6ICcnKSkge1xuICAgICAgICAvLyBObyBuZWVkIHRvIHNldCB2YWx1ZSBvciB0cmlnZ2VyIGFueSBldmVudCBpZiBub3RoaW5nIGNoYW5nZWRcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICB0aGlzLmlucHV0LnByb3AoJ3ZhbHVlJywgdmFsKTtcblxuICAgICAgLyoqXG4gICAgICAgKiAoSW5wdXQpIFRyaWdnZXJlZCBvbiB0aGUgaW5wdXQgZWxlbWVudCB3aGVuIGEgbmV3IGNvbG9yIGlzIHNlbGVjdGVkLlxuICAgICAgICpcbiAgICAgICAqIEBldmVudCBDb2xvcnBpY2tlciNjaGFuZ2VcbiAgICAgICAqL1xuICAgICAgdGhpcy5pbnB1dC50cmlnZ2VyKHtcbiAgICAgICAgdHlwZTogJ2NoYW5nZScsXG4gICAgICAgIGNvbG9ycGlja2VyOiB0aGlzLmNvbG9ycGlja2VyLFxuICAgICAgICBjb2xvcjogdGhpcy5jb2xvcnBpY2tlci5jb2xvcixcbiAgICAgICAgdmFsdWU6IHZhbFxuICAgICAgfSk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogUmV0dXJucyB0aGUgZm9ybWF0dGVkIGNvbG9yIHN0cmluZywgd2l0aCB0aGUgZm9ybWF0dGluZyBvcHRpb25zIGFwcGxpZWRcbiAgICAgKiAoZS5nLiB1c2VIYXNoUHJlZml4KVxuICAgICAqXG4gICAgICogQHBhcmFtIHtTdHJpbmd8bnVsbH0gdmFsXG4gICAgICpcbiAgICAgKiBAcmV0dXJucyB7U3RyaW5nfVxuICAgICAqL1xuXG4gIH0sIHtcbiAgICBrZXk6ICdnZXRGb3JtYXR0ZWRDb2xvcicsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGdldEZvcm1hdHRlZENvbG9yKCkge1xuICAgICAgdmFyIHZhbCA9IGFyZ3VtZW50cy5sZW5ndGggPiAwICYmIGFyZ3VtZW50c1swXSAhPT0gdW5kZWZpbmVkID8gYXJndW1lbnRzWzBdIDogbnVsbDtcblxuICAgICAgdmFsID0gdmFsID8gdmFsIDogdGhpcy5jb2xvcnBpY2tlci5jb2xvckhhbmRsZXIuZ2V0Q29sb3JTdHJpbmcoKTtcblxuICAgICAgaWYgKCF2YWwpIHtcbiAgICAgICAgcmV0dXJuICcnO1xuICAgICAgfVxuXG4gICAgICB2YWwgPSB0aGlzLmNvbG9ycGlja2VyLmNvbG9ySGFuZGxlci5yZXNvbHZlQ29sb3JEZWxlZ2F0ZSh2YWwsIGZhbHNlKTtcblxuICAgICAgaWYgKHRoaXMuY29sb3JwaWNrZXIub3B0aW9ucy51c2VIYXNoUHJlZml4ID09PSBmYWxzZSkge1xuICAgICAgICB2YWwgPSB2YWwucmVwbGFjZSgvXiMvZywgJycpO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gdmFsO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIFJldHVybnMgdHJ1ZSBpZiB0aGUgd2lkZ2V0IGhhcyBhbiBhc3NvY2lhdGVkIGlucHV0IGVsZW1lbnQsIGZhbHNlIG90aGVyd2lzZVxuICAgICAqIEByZXR1cm5zIHtib29sZWFufVxuICAgICAqL1xuXG4gIH0sIHtcbiAgICBrZXk6ICdoYXNJbnB1dCcsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGhhc0lucHV0KCkge1xuICAgICAgcmV0dXJuIHRoaXMuaW5wdXQgIT09IGZhbHNlO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIFJldHVybnMgdHJ1ZSBpZiB0aGUgaW5wdXQgZXhpc3RzIGFuZCBpcyBkaXNhYmxlZFxuICAgICAqIEByZXR1cm5zIHtib29sZWFufVxuICAgICAqL1xuXG4gIH0sIHtcbiAgICBrZXk6ICdpc0VuYWJsZWQnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBpc0VuYWJsZWQoKSB7XG4gICAgICByZXR1cm4gdGhpcy5oYXNJbnB1dCgpICYmICF0aGlzLmlzRGlzYWJsZWQoKTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBSZXR1cm5zIHRydWUgaWYgdGhlIGlucHV0IGV4aXN0cyBhbmQgaXMgZGlzYWJsZWRcbiAgICAgKiBAcmV0dXJucyB7Ym9vbGVhbn1cbiAgICAgKi9cblxuICB9LCB7XG4gICAga2V5OiAnaXNEaXNhYmxlZCcsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGlzRGlzYWJsZWQoKSB7XG4gICAgICByZXR1cm4gdGhpcy5oYXNJbnB1dCgpICYmIHRoaXMuaW5wdXQucHJvcCgnZGlzYWJsZWQnKSA9PT0gdHJ1ZTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBEaXNhYmxlcyB0aGUgaW5wdXQgaWYgYW55XG4gICAgICpcbiAgICAgKiBAZmlyZXMgQ29sb3JwaWNrZXIjY29sb3JwaWNrZXJEaXNhYmxlXG4gICAgICogQHJldHVybnMge2Jvb2xlYW59XG4gICAgICovXG5cbiAgfSwge1xuICAgIGtleTogJ2Rpc2FibGUnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBkaXNhYmxlKCkge1xuICAgICAgaWYgKHRoaXMuaGFzSW5wdXQoKSkge1xuICAgICAgICB0aGlzLmlucHV0LnByb3AoJ2Rpc2FibGVkJywgdHJ1ZSk7XG4gICAgICB9XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogRW5hYmxlcyB0aGUgaW5wdXQgaWYgYW55XG4gICAgICpcbiAgICAgKiBAZmlyZXMgQ29sb3JwaWNrZXIjY29sb3JwaWNrZXJFbmFibGVcbiAgICAgKiBAcmV0dXJucyB7Ym9vbGVhbn1cbiAgICAgKi9cblxuICB9LCB7XG4gICAga2V5OiAnZW5hYmxlJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gZW5hYmxlKCkge1xuICAgICAgaWYgKHRoaXMuaGFzSW5wdXQoKSkge1xuICAgICAgICB0aGlzLmlucHV0LnByb3AoJ2Rpc2FibGVkJywgZmFsc2UpO1xuICAgICAgfVxuICAgIH1cblxuICAgIC8qKlxuICAgICAqIENhbGxzIHNldFZhbHVlIHdpdGggdGhlIGN1cnJlbnQgaW50ZXJuYWwgY29sb3IgdmFsdWVcbiAgICAgKlxuICAgICAqIEBmaXJlcyBDb2xvcnBpY2tlciNjaGFuZ2VcbiAgICAgKi9cblxuICB9LCB7XG4gICAga2V5OiAndXBkYXRlJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gdXBkYXRlKCkge1xuICAgICAgaWYgKCF0aGlzLmhhc0lucHV0KCkpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICBpZiAodGhpcy5jb2xvcnBpY2tlci5vcHRpb25zLmF1dG9JbnB1dEZhbGxiYWNrID09PSBmYWxzZSAmJiB0aGlzLmNvbG9ycGlja2VyLmNvbG9ySGFuZGxlci5pc0ludmFsaWRDb2xvcigpKSB7XG4gICAgICAgIC8vIHByZXZlbnQgdXBkYXRlIGlmIGNvbG9yIGlzIGludmFsaWQsIGF1dG9JbnB1dEZhbGxiYWNrIGlzIGRpc2FibGVkIGFuZCB0aGUgbGFzdCBldmVudCBpcyBrZXl1cC5cbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICB0aGlzLnNldFZhbHVlKHRoaXMuZ2V0Rm9ybWF0dGVkQ29sb3IoKSk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogRnVuY3Rpb24gdHJpZ2dlcmVkIHdoZW4gdGhlIGlucHV0IGhhcyBjaGFuZ2VkLCBzbyB0aGUgY29sb3JwaWNrZXIgZ2V0cyB1cGRhdGVkLlxuICAgICAqXG4gICAgICogQHByaXZhdGVcbiAgICAgKiBAcGFyYW0ge0V2ZW50fSBlXG4gICAgICogQHJldHVybnMge2Jvb2xlYW59XG4gICAgICovXG5cbiAgfSwge1xuICAgIGtleTogJ29uY2hhbmdlJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gb25jaGFuZ2UoZSkge1xuICAgICAgdGhpcy5jb2xvcnBpY2tlci5sYXN0RXZlbnQuYWxpYXMgPSAnaW5wdXQuY2hhbmdlJztcbiAgICAgIHRoaXMuY29sb3JwaWNrZXIubGFzdEV2ZW50LmUgPSBlO1xuXG4gICAgICB2YXIgdmFsID0gdGhpcy5nZXRWYWx1ZSgpO1xuXG4gICAgICBpZiAodmFsICE9PSBlLnZhbHVlKSB7XG4gICAgICAgIHRoaXMuY29sb3JwaWNrZXIuc2V0VmFsdWUodmFsKTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBGdW5jdGlvbiB0cmlnZ2VyZWQgYWZ0ZXIgYSBrZXlib2FyZCBrZXkgaGFzIGJlZW4gcmVsZWFzZWQuXG4gICAgICpcbiAgICAgKiBAcHJpdmF0ZVxuICAgICAqIEBwYXJhbSB7RXZlbnR9IGVcbiAgICAgKiBAcmV0dXJucyB7Ym9vbGVhbn1cbiAgICAgKi9cblxuICB9LCB7XG4gICAga2V5OiAnb25rZXl1cCcsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIG9ua2V5dXAoZSkge1xuICAgICAgdGhpcy5jb2xvcnBpY2tlci5sYXN0RXZlbnQuYWxpYXMgPSAnaW5wdXQua2V5dXAnO1xuICAgICAgdGhpcy5jb2xvcnBpY2tlci5sYXN0RXZlbnQuZSA9IGU7XG5cbiAgICAgIHZhciB2YWwgPSB0aGlzLmdldFZhbHVlKCk7XG5cbiAgICAgIGlmICh2YWwgIT09IGUudmFsdWUpIHtcbiAgICAgICAgdGhpcy5jb2xvcnBpY2tlci5zZXRWYWx1ZSh2YWwpO1xuICAgICAgfVxuICAgIH1cbiAgfV0pO1xuXG4gIHJldHVybiBJbnB1dEhhbmRsZXI7XG59KCk7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IElucHV0SGFuZGxlcjtcbm1vZHVsZS5leHBvcnRzID0gZXhwb3J0cy5kZWZhdWx0O1xuXG4vKioqLyB9KSxcbi8qIDE2ICovXG4vKioqLyAoZnVuY3Rpb24obW9kdWxlLCBleHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKSB7XG5cblwidXNlIHN0cmljdFwiO1xuXG5cbnZhciBjb2xvclN0cmluZyA9IF9fd2VicGFja19yZXF1aXJlX18oMTcpO1xudmFyIGNvbnZlcnQgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKDIwKTtcblxudmFyIF9zbGljZSA9IFtdLnNsaWNlO1xuXG52YXIgc2tpcHBlZE1vZGVscyA9IFtcblx0Ly8gdG8gYmUgaG9uZXN0LCBJIGRvbid0IHJlYWxseSBmZWVsIGxpa2Uga2V5d29yZCBiZWxvbmdzIGluIGNvbG9yIGNvbnZlcnQsIGJ1dCBlaC5cblx0J2tleXdvcmQnLFxuXG5cdC8vIGdyYXkgY29uZmxpY3RzIHdpdGggc29tZSBtZXRob2QgbmFtZXMsIGFuZCBoYXMgaXRzIG93biBtZXRob2QgZGVmaW5lZC5cblx0J2dyYXknLFxuXG5cdC8vIHNob3VsZG4ndCByZWFsbHkgYmUgaW4gY29sb3ItY29udmVydCBlaXRoZXIuLi5cblx0J2hleCdcbl07XG5cbnZhciBoYXNoZWRNb2RlbEtleXMgPSB7fTtcbk9iamVjdC5rZXlzKGNvbnZlcnQpLmZvckVhY2goZnVuY3Rpb24gKG1vZGVsKSB7XG5cdGhhc2hlZE1vZGVsS2V5c1tfc2xpY2UuY2FsbChjb252ZXJ0W21vZGVsXS5sYWJlbHMpLnNvcnQoKS5qb2luKCcnKV0gPSBtb2RlbDtcbn0pO1xuXG52YXIgbGltaXRlcnMgPSB7fTtcblxuZnVuY3Rpb24gQ29sb3Iob2JqLCBtb2RlbCkge1xuXHRpZiAoISh0aGlzIGluc3RhbmNlb2YgQ29sb3IpKSB7XG5cdFx0cmV0dXJuIG5ldyBDb2xvcihvYmosIG1vZGVsKTtcblx0fVxuXG5cdGlmIChtb2RlbCAmJiBtb2RlbCBpbiBza2lwcGVkTW9kZWxzKSB7XG5cdFx0bW9kZWwgPSBudWxsO1xuXHR9XG5cblx0aWYgKG1vZGVsICYmICEobW9kZWwgaW4gY29udmVydCkpIHtcblx0XHR0aHJvdyBuZXcgRXJyb3IoJ1Vua25vd24gbW9kZWw6ICcgKyBtb2RlbCk7XG5cdH1cblxuXHR2YXIgaTtcblx0dmFyIGNoYW5uZWxzO1xuXG5cdGlmIChvYmogPT0gbnVsbCkgeyAvLyBlc2xpbnQtZGlzYWJsZS1saW5lIG5vLWVxLW51bGwsZXFlcWVxXG5cdFx0dGhpcy5tb2RlbCA9ICdyZ2InO1xuXHRcdHRoaXMuY29sb3IgPSBbMCwgMCwgMF07XG5cdFx0dGhpcy52YWxwaGEgPSAxO1xuXHR9IGVsc2UgaWYgKG9iaiBpbnN0YW5jZW9mIENvbG9yKSB7XG5cdFx0dGhpcy5tb2RlbCA9IG9iai5tb2RlbDtcblx0XHR0aGlzLmNvbG9yID0gb2JqLmNvbG9yLnNsaWNlKCk7XG5cdFx0dGhpcy52YWxwaGEgPSBvYmoudmFscGhhO1xuXHR9IGVsc2UgaWYgKHR5cGVvZiBvYmogPT09ICdzdHJpbmcnKSB7XG5cdFx0dmFyIHJlc3VsdCA9IGNvbG9yU3RyaW5nLmdldChvYmopO1xuXHRcdGlmIChyZXN1bHQgPT09IG51bGwpIHtcblx0XHRcdHRocm93IG5ldyBFcnJvcignVW5hYmxlIHRvIHBhcnNlIGNvbG9yIGZyb20gc3RyaW5nOiAnICsgb2JqKTtcblx0XHR9XG5cblx0XHR0aGlzLm1vZGVsID0gcmVzdWx0Lm1vZGVsO1xuXHRcdGNoYW5uZWxzID0gY29udmVydFt0aGlzLm1vZGVsXS5jaGFubmVscztcblx0XHR0aGlzLmNvbG9yID0gcmVzdWx0LnZhbHVlLnNsaWNlKDAsIGNoYW5uZWxzKTtcblx0XHR0aGlzLnZhbHBoYSA9IHR5cGVvZiByZXN1bHQudmFsdWVbY2hhbm5lbHNdID09PSAnbnVtYmVyJyA/IHJlc3VsdC52YWx1ZVtjaGFubmVsc10gOiAxO1xuXHR9IGVsc2UgaWYgKG9iai5sZW5ndGgpIHtcblx0XHR0aGlzLm1vZGVsID0gbW9kZWwgfHwgJ3JnYic7XG5cdFx0Y2hhbm5lbHMgPSBjb252ZXJ0W3RoaXMubW9kZWxdLmNoYW5uZWxzO1xuXHRcdHZhciBuZXdBcnIgPSBfc2xpY2UuY2FsbChvYmosIDAsIGNoYW5uZWxzKTtcblx0XHR0aGlzLmNvbG9yID0gemVyb0FycmF5KG5ld0FyciwgY2hhbm5lbHMpO1xuXHRcdHRoaXMudmFscGhhID0gdHlwZW9mIG9ialtjaGFubmVsc10gPT09ICdudW1iZXInID8gb2JqW2NoYW5uZWxzXSA6IDE7XG5cdH0gZWxzZSBpZiAodHlwZW9mIG9iaiA9PT0gJ251bWJlcicpIHtcblx0XHQvLyB0aGlzIGlzIGFsd2F5cyBSR0IgLSBjYW4gYmUgY29udmVydGVkIGxhdGVyIG9uLlxuXHRcdG9iaiAmPSAweEZGRkZGRjtcblx0XHR0aGlzLm1vZGVsID0gJ3JnYic7XG5cdFx0dGhpcy5jb2xvciA9IFtcblx0XHRcdChvYmogPj4gMTYpICYgMHhGRixcblx0XHRcdChvYmogPj4gOCkgJiAweEZGLFxuXHRcdFx0b2JqICYgMHhGRlxuXHRcdF07XG5cdFx0dGhpcy52YWxwaGEgPSAxO1xuXHR9IGVsc2Uge1xuXHRcdHRoaXMudmFscGhhID0gMTtcblxuXHRcdHZhciBrZXlzID0gT2JqZWN0LmtleXMob2JqKTtcblx0XHRpZiAoJ2FscGhhJyBpbiBvYmopIHtcblx0XHRcdGtleXMuc3BsaWNlKGtleXMuaW5kZXhPZignYWxwaGEnKSwgMSk7XG5cdFx0XHR0aGlzLnZhbHBoYSA9IHR5cGVvZiBvYmouYWxwaGEgPT09ICdudW1iZXInID8gb2JqLmFscGhhIDogMDtcblx0XHR9XG5cblx0XHR2YXIgaGFzaGVkS2V5cyA9IGtleXMuc29ydCgpLmpvaW4oJycpO1xuXHRcdGlmICghKGhhc2hlZEtleXMgaW4gaGFzaGVkTW9kZWxLZXlzKSkge1xuXHRcdFx0dGhyb3cgbmV3IEVycm9yKCdVbmFibGUgdG8gcGFyc2UgY29sb3IgZnJvbSBvYmplY3Q6ICcgKyBKU09OLnN0cmluZ2lmeShvYmopKTtcblx0XHR9XG5cblx0XHR0aGlzLm1vZGVsID0gaGFzaGVkTW9kZWxLZXlzW2hhc2hlZEtleXNdO1xuXG5cdFx0dmFyIGxhYmVscyA9IGNvbnZlcnRbdGhpcy5tb2RlbF0ubGFiZWxzO1xuXHRcdHZhciBjb2xvciA9IFtdO1xuXHRcdGZvciAoaSA9IDA7IGkgPCBsYWJlbHMubGVuZ3RoOyBpKyspIHtcblx0XHRcdGNvbG9yLnB1c2gob2JqW2xhYmVsc1tpXV0pO1xuXHRcdH1cblxuXHRcdHRoaXMuY29sb3IgPSB6ZXJvQXJyYXkoY29sb3IpO1xuXHR9XG5cblx0Ly8gcGVyZm9ybSBsaW1pdGF0aW9ucyAoY2xhbXBpbmcsIGV0Yy4pXG5cdGlmIChsaW1pdGVyc1t0aGlzLm1vZGVsXSkge1xuXHRcdGNoYW5uZWxzID0gY29udmVydFt0aGlzLm1vZGVsXS5jaGFubmVscztcblx0XHRmb3IgKGkgPSAwOyBpIDwgY2hhbm5lbHM7IGkrKykge1xuXHRcdFx0dmFyIGxpbWl0ID0gbGltaXRlcnNbdGhpcy5tb2RlbF1baV07XG5cdFx0XHRpZiAobGltaXQpIHtcblx0XHRcdFx0dGhpcy5jb2xvcltpXSA9IGxpbWl0KHRoaXMuY29sb3JbaV0pO1xuXHRcdFx0fVxuXHRcdH1cblx0fVxuXG5cdHRoaXMudmFscGhhID0gTWF0aC5tYXgoMCwgTWF0aC5taW4oMSwgdGhpcy52YWxwaGEpKTtcblxuXHRpZiAoT2JqZWN0LmZyZWV6ZSkge1xuXHRcdE9iamVjdC5mcmVlemUodGhpcyk7XG5cdH1cbn1cblxuQ29sb3IucHJvdG90eXBlID0ge1xuXHR0b1N0cmluZzogZnVuY3Rpb24gKCkge1xuXHRcdHJldHVybiB0aGlzLnN0cmluZygpO1xuXHR9LFxuXG5cdHRvSlNPTjogZnVuY3Rpb24gKCkge1xuXHRcdHJldHVybiB0aGlzW3RoaXMubW9kZWxdKCk7XG5cdH0sXG5cblx0c3RyaW5nOiBmdW5jdGlvbiAocGxhY2VzKSB7XG5cdFx0dmFyIHNlbGYgPSB0aGlzLm1vZGVsIGluIGNvbG9yU3RyaW5nLnRvID8gdGhpcyA6IHRoaXMucmdiKCk7XG5cdFx0c2VsZiA9IHNlbGYucm91bmQodHlwZW9mIHBsYWNlcyA9PT0gJ251bWJlcicgPyBwbGFjZXMgOiAxKTtcblx0XHR2YXIgYXJncyA9IHNlbGYudmFscGhhID09PSAxID8gc2VsZi5jb2xvciA6IHNlbGYuY29sb3IuY29uY2F0KHRoaXMudmFscGhhKTtcblx0XHRyZXR1cm4gY29sb3JTdHJpbmcudG9bc2VsZi5tb2RlbF0oYXJncyk7XG5cdH0sXG5cblx0cGVyY2VudFN0cmluZzogZnVuY3Rpb24gKHBsYWNlcykge1xuXHRcdHZhciBzZWxmID0gdGhpcy5yZ2IoKS5yb3VuZCh0eXBlb2YgcGxhY2VzID09PSAnbnVtYmVyJyA/IHBsYWNlcyA6IDEpO1xuXHRcdHZhciBhcmdzID0gc2VsZi52YWxwaGEgPT09IDEgPyBzZWxmLmNvbG9yIDogc2VsZi5jb2xvci5jb25jYXQodGhpcy52YWxwaGEpO1xuXHRcdHJldHVybiBjb2xvclN0cmluZy50by5yZ2IucGVyY2VudChhcmdzKTtcblx0fSxcblxuXHRhcnJheTogZnVuY3Rpb24gKCkge1xuXHRcdHJldHVybiB0aGlzLnZhbHBoYSA9PT0gMSA/IHRoaXMuY29sb3Iuc2xpY2UoKSA6IHRoaXMuY29sb3IuY29uY2F0KHRoaXMudmFscGhhKTtcblx0fSxcblxuXHRvYmplY3Q6IGZ1bmN0aW9uICgpIHtcblx0XHR2YXIgcmVzdWx0ID0ge307XG5cdFx0dmFyIGNoYW5uZWxzID0gY29udmVydFt0aGlzLm1vZGVsXS5jaGFubmVscztcblx0XHR2YXIgbGFiZWxzID0gY29udmVydFt0aGlzLm1vZGVsXS5sYWJlbHM7XG5cblx0XHRmb3IgKHZhciBpID0gMDsgaSA8IGNoYW5uZWxzOyBpKyspIHtcblx0XHRcdHJlc3VsdFtsYWJlbHNbaV1dID0gdGhpcy5jb2xvcltpXTtcblx0XHR9XG5cblx0XHRpZiAodGhpcy52YWxwaGEgIT09IDEpIHtcblx0XHRcdHJlc3VsdC5hbHBoYSA9IHRoaXMudmFscGhhO1xuXHRcdH1cblxuXHRcdHJldHVybiByZXN1bHQ7XG5cdH0sXG5cblx0dW5pdEFycmF5OiBmdW5jdGlvbiAoKSB7XG5cdFx0dmFyIHJnYiA9IHRoaXMucmdiKCkuY29sb3I7XG5cdFx0cmdiWzBdIC89IDI1NTtcblx0XHRyZ2JbMV0gLz0gMjU1O1xuXHRcdHJnYlsyXSAvPSAyNTU7XG5cblx0XHRpZiAodGhpcy52YWxwaGEgIT09IDEpIHtcblx0XHRcdHJnYi5wdXNoKHRoaXMudmFscGhhKTtcblx0XHR9XG5cblx0XHRyZXR1cm4gcmdiO1xuXHR9LFxuXG5cdHVuaXRPYmplY3Q6IGZ1bmN0aW9uICgpIHtcblx0XHR2YXIgcmdiID0gdGhpcy5yZ2IoKS5vYmplY3QoKTtcblx0XHRyZ2IuciAvPSAyNTU7XG5cdFx0cmdiLmcgLz0gMjU1O1xuXHRcdHJnYi5iIC89IDI1NTtcblxuXHRcdGlmICh0aGlzLnZhbHBoYSAhPT0gMSkge1xuXHRcdFx0cmdiLmFscGhhID0gdGhpcy52YWxwaGE7XG5cdFx0fVxuXG5cdFx0cmV0dXJuIHJnYjtcblx0fSxcblxuXHRyb3VuZDogZnVuY3Rpb24gKHBsYWNlcykge1xuXHRcdHBsYWNlcyA9IE1hdGgubWF4KHBsYWNlcyB8fCAwLCAwKTtcblx0XHRyZXR1cm4gbmV3IENvbG9yKHRoaXMuY29sb3IubWFwKHJvdW5kVG9QbGFjZShwbGFjZXMpKS5jb25jYXQodGhpcy52YWxwaGEpLCB0aGlzLm1vZGVsKTtcblx0fSxcblxuXHRhbHBoYTogZnVuY3Rpb24gKHZhbCkge1xuXHRcdGlmIChhcmd1bWVudHMubGVuZ3RoKSB7XG5cdFx0XHRyZXR1cm4gbmV3IENvbG9yKHRoaXMuY29sb3IuY29uY2F0KE1hdGgubWF4KDAsIE1hdGgubWluKDEsIHZhbCkpKSwgdGhpcy5tb2RlbCk7XG5cdFx0fVxuXG5cdFx0cmV0dXJuIHRoaXMudmFscGhhO1xuXHR9LFxuXG5cdC8vIHJnYlxuXHRyZWQ6IGdldHNldCgncmdiJywgMCwgbWF4Zm4oMjU1KSksXG5cdGdyZWVuOiBnZXRzZXQoJ3JnYicsIDEsIG1heGZuKDI1NSkpLFxuXHRibHVlOiBnZXRzZXQoJ3JnYicsIDIsIG1heGZuKDI1NSkpLFxuXG5cdGh1ZTogZ2V0c2V0KFsnaHNsJywgJ2hzdicsICdoc2wnLCAnaHdiJywgJ2hjZyddLCAwLCBmdW5jdGlvbiAodmFsKSB7IHJldHVybiAoKHZhbCAlIDM2MCkgKyAzNjApICUgMzYwOyB9KSwgLy8gZXNsaW50LWRpc2FibGUtbGluZSBicmFjZS1zdHlsZVxuXG5cdHNhdHVyYXRpb25sOiBnZXRzZXQoJ2hzbCcsIDEsIG1heGZuKDEwMCkpLFxuXHRsaWdodG5lc3M6IGdldHNldCgnaHNsJywgMiwgbWF4Zm4oMTAwKSksXG5cblx0c2F0dXJhdGlvbnY6IGdldHNldCgnaHN2JywgMSwgbWF4Zm4oMTAwKSksXG5cdHZhbHVlOiBnZXRzZXQoJ2hzdicsIDIsIG1heGZuKDEwMCkpLFxuXG5cdGNocm9tYTogZ2V0c2V0KCdoY2cnLCAxLCBtYXhmbigxMDApKSxcblx0Z3JheTogZ2V0c2V0KCdoY2cnLCAyLCBtYXhmbigxMDApKSxcblxuXHR3aGl0ZTogZ2V0c2V0KCdod2InLCAxLCBtYXhmbigxMDApKSxcblx0d2JsYWNrOiBnZXRzZXQoJ2h3YicsIDIsIG1heGZuKDEwMCkpLFxuXG5cdGN5YW46IGdldHNldCgnY215aycsIDAsIG1heGZuKDEwMCkpLFxuXHRtYWdlbnRhOiBnZXRzZXQoJ2NteWsnLCAxLCBtYXhmbigxMDApKSxcblx0eWVsbG93OiBnZXRzZXQoJ2NteWsnLCAyLCBtYXhmbigxMDApKSxcblx0YmxhY2s6IGdldHNldCgnY215aycsIDMsIG1heGZuKDEwMCkpLFxuXG5cdHg6IGdldHNldCgneHl6JywgMCwgbWF4Zm4oMTAwKSksXG5cdHk6IGdldHNldCgneHl6JywgMSwgbWF4Zm4oMTAwKSksXG5cdHo6IGdldHNldCgneHl6JywgMiwgbWF4Zm4oMTAwKSksXG5cblx0bDogZ2V0c2V0KCdsYWInLCAwLCBtYXhmbigxMDApKSxcblx0YTogZ2V0c2V0KCdsYWInLCAxKSxcblx0YjogZ2V0c2V0KCdsYWInLCAyKSxcblxuXHRrZXl3b3JkOiBmdW5jdGlvbiAodmFsKSB7XG5cdFx0aWYgKGFyZ3VtZW50cy5sZW5ndGgpIHtcblx0XHRcdHJldHVybiBuZXcgQ29sb3IodmFsKTtcblx0XHR9XG5cblx0XHRyZXR1cm4gY29udmVydFt0aGlzLm1vZGVsXS5rZXl3b3JkKHRoaXMuY29sb3IpO1xuXHR9LFxuXG5cdGhleDogZnVuY3Rpb24gKHZhbCkge1xuXHRcdGlmIChhcmd1bWVudHMubGVuZ3RoKSB7XG5cdFx0XHRyZXR1cm4gbmV3IENvbG9yKHZhbCk7XG5cdFx0fVxuXG5cdFx0cmV0dXJuIGNvbG9yU3RyaW5nLnRvLmhleCh0aGlzLnJnYigpLnJvdW5kKCkuY29sb3IpO1xuXHR9LFxuXG5cdHJnYk51bWJlcjogZnVuY3Rpb24gKCkge1xuXHRcdHZhciByZ2IgPSB0aGlzLnJnYigpLmNvbG9yO1xuXHRcdHJldHVybiAoKHJnYlswXSAmIDB4RkYpIDw8IDE2KSB8ICgocmdiWzFdICYgMHhGRikgPDwgOCkgfCAocmdiWzJdICYgMHhGRik7XG5cdH0sXG5cblx0bHVtaW5vc2l0eTogZnVuY3Rpb24gKCkge1xuXHRcdC8vIGh0dHA6Ly93d3cudzMub3JnL1RSL1dDQUcyMC8jcmVsYXRpdmVsdW1pbmFuY2VkZWZcblx0XHR2YXIgcmdiID0gdGhpcy5yZ2IoKS5jb2xvcjtcblxuXHRcdHZhciBsdW0gPSBbXTtcblx0XHRmb3IgKHZhciBpID0gMDsgaSA8IHJnYi5sZW5ndGg7IGkrKykge1xuXHRcdFx0dmFyIGNoYW4gPSByZ2JbaV0gLyAyNTU7XG5cdFx0XHRsdW1baV0gPSAoY2hhbiA8PSAwLjAzOTI4KSA/IGNoYW4gLyAxMi45MiA6IE1hdGgucG93KCgoY2hhbiArIDAuMDU1KSAvIDEuMDU1KSwgMi40KTtcblx0XHR9XG5cblx0XHRyZXR1cm4gMC4yMTI2ICogbHVtWzBdICsgMC43MTUyICogbHVtWzFdICsgMC4wNzIyICogbHVtWzJdO1xuXHR9LFxuXG5cdGNvbnRyYXN0OiBmdW5jdGlvbiAoY29sb3IyKSB7XG5cdFx0Ly8gaHR0cDovL3d3dy53My5vcmcvVFIvV0NBRzIwLyNjb250cmFzdC1yYXRpb2RlZlxuXHRcdHZhciBsdW0xID0gdGhpcy5sdW1pbm9zaXR5KCk7XG5cdFx0dmFyIGx1bTIgPSBjb2xvcjIubHVtaW5vc2l0eSgpO1xuXG5cdFx0aWYgKGx1bTEgPiBsdW0yKSB7XG5cdFx0XHRyZXR1cm4gKGx1bTEgKyAwLjA1KSAvIChsdW0yICsgMC4wNSk7XG5cdFx0fVxuXG5cdFx0cmV0dXJuIChsdW0yICsgMC4wNSkgLyAobHVtMSArIDAuMDUpO1xuXHR9LFxuXG5cdGxldmVsOiBmdW5jdGlvbiAoY29sb3IyKSB7XG5cdFx0dmFyIGNvbnRyYXN0UmF0aW8gPSB0aGlzLmNvbnRyYXN0KGNvbG9yMik7XG5cdFx0aWYgKGNvbnRyYXN0UmF0aW8gPj0gNy4xKSB7XG5cdFx0XHRyZXR1cm4gJ0FBQSc7XG5cdFx0fVxuXG5cdFx0cmV0dXJuIChjb250cmFzdFJhdGlvID49IDQuNSkgPyAnQUEnIDogJyc7XG5cdH0sXG5cblx0aXNEYXJrOiBmdW5jdGlvbiAoKSB7XG5cdFx0Ly8gWUlRIGVxdWF0aW9uIGZyb20gaHR0cDovLzI0d2F5cy5vcmcvMjAxMC9jYWxjdWxhdGluZy1jb2xvci1jb250cmFzdFxuXHRcdHZhciByZ2IgPSB0aGlzLnJnYigpLmNvbG9yO1xuXHRcdHZhciB5aXEgPSAocmdiWzBdICogMjk5ICsgcmdiWzFdICogNTg3ICsgcmdiWzJdICogMTE0KSAvIDEwMDA7XG5cdFx0cmV0dXJuIHlpcSA8IDEyODtcblx0fSxcblxuXHRpc0xpZ2h0OiBmdW5jdGlvbiAoKSB7XG5cdFx0cmV0dXJuICF0aGlzLmlzRGFyaygpO1xuXHR9LFxuXG5cdG5lZ2F0ZTogZnVuY3Rpb24gKCkge1xuXHRcdHZhciByZ2IgPSB0aGlzLnJnYigpO1xuXHRcdGZvciAodmFyIGkgPSAwOyBpIDwgMzsgaSsrKSB7XG5cdFx0XHRyZ2IuY29sb3JbaV0gPSAyNTUgLSByZ2IuY29sb3JbaV07XG5cdFx0fVxuXHRcdHJldHVybiByZ2I7XG5cdH0sXG5cblx0bGlnaHRlbjogZnVuY3Rpb24gKHJhdGlvKSB7XG5cdFx0dmFyIGhzbCA9IHRoaXMuaHNsKCk7XG5cdFx0aHNsLmNvbG9yWzJdICs9IGhzbC5jb2xvclsyXSAqIHJhdGlvO1xuXHRcdHJldHVybiBoc2w7XG5cdH0sXG5cblx0ZGFya2VuOiBmdW5jdGlvbiAocmF0aW8pIHtcblx0XHR2YXIgaHNsID0gdGhpcy5oc2woKTtcblx0XHRoc2wuY29sb3JbMl0gLT0gaHNsLmNvbG9yWzJdICogcmF0aW87XG5cdFx0cmV0dXJuIGhzbDtcblx0fSxcblxuXHRzYXR1cmF0ZTogZnVuY3Rpb24gKHJhdGlvKSB7XG5cdFx0dmFyIGhzbCA9IHRoaXMuaHNsKCk7XG5cdFx0aHNsLmNvbG9yWzFdICs9IGhzbC5jb2xvclsxXSAqIHJhdGlvO1xuXHRcdHJldHVybiBoc2w7XG5cdH0sXG5cblx0ZGVzYXR1cmF0ZTogZnVuY3Rpb24gKHJhdGlvKSB7XG5cdFx0dmFyIGhzbCA9IHRoaXMuaHNsKCk7XG5cdFx0aHNsLmNvbG9yWzFdIC09IGhzbC5jb2xvclsxXSAqIHJhdGlvO1xuXHRcdHJldHVybiBoc2w7XG5cdH0sXG5cblx0d2hpdGVuOiBmdW5jdGlvbiAocmF0aW8pIHtcblx0XHR2YXIgaHdiID0gdGhpcy5od2IoKTtcblx0XHRod2IuY29sb3JbMV0gKz0gaHdiLmNvbG9yWzFdICogcmF0aW87XG5cdFx0cmV0dXJuIGh3Yjtcblx0fSxcblxuXHRibGFja2VuOiBmdW5jdGlvbiAocmF0aW8pIHtcblx0XHR2YXIgaHdiID0gdGhpcy5od2IoKTtcblx0XHRod2IuY29sb3JbMl0gKz0gaHdiLmNvbG9yWzJdICogcmF0aW87XG5cdFx0cmV0dXJuIGh3Yjtcblx0fSxcblxuXHRncmF5c2NhbGU6IGZ1bmN0aW9uICgpIHtcblx0XHQvLyBodHRwOi8vZW4ud2lraXBlZGlhLm9yZy93aWtpL0dyYXlzY2FsZSNDb252ZXJ0aW5nX2NvbG9yX3RvX2dyYXlzY2FsZVxuXHRcdHZhciByZ2IgPSB0aGlzLnJnYigpLmNvbG9yO1xuXHRcdHZhciB2YWwgPSByZ2JbMF0gKiAwLjMgKyByZ2JbMV0gKiAwLjU5ICsgcmdiWzJdICogMC4xMTtcblx0XHRyZXR1cm4gQ29sb3IucmdiKHZhbCwgdmFsLCB2YWwpO1xuXHR9LFxuXG5cdGZhZGU6IGZ1bmN0aW9uIChyYXRpbykge1xuXHRcdHJldHVybiB0aGlzLmFscGhhKHRoaXMudmFscGhhIC0gKHRoaXMudmFscGhhICogcmF0aW8pKTtcblx0fSxcblxuXHRvcGFxdWVyOiBmdW5jdGlvbiAocmF0aW8pIHtcblx0XHRyZXR1cm4gdGhpcy5hbHBoYSh0aGlzLnZhbHBoYSArICh0aGlzLnZhbHBoYSAqIHJhdGlvKSk7XG5cdH0sXG5cblx0cm90YXRlOiBmdW5jdGlvbiAoZGVncmVlcykge1xuXHRcdHZhciBoc2wgPSB0aGlzLmhzbCgpO1xuXHRcdHZhciBodWUgPSBoc2wuY29sb3JbMF07XG5cdFx0aHVlID0gKGh1ZSArIGRlZ3JlZXMpICUgMzYwO1xuXHRcdGh1ZSA9IGh1ZSA8IDAgPyAzNjAgKyBodWUgOiBodWU7XG5cdFx0aHNsLmNvbG9yWzBdID0gaHVlO1xuXHRcdHJldHVybiBoc2w7XG5cdH0sXG5cblx0bWl4OiBmdW5jdGlvbiAobWl4aW5Db2xvciwgd2VpZ2h0KSB7XG5cdFx0Ly8gcG9ydGVkIGZyb20gc2FzcyBpbXBsZW1lbnRhdGlvbiBpbiBDXG5cdFx0Ly8gaHR0cHM6Ly9naXRodWIuY29tL3Nhc3MvbGlic2Fzcy9ibG9iLzBlNmI0YTI4NTAwOTIzNTZhYTNlY2UwN2M2YjI0OWYwMjIxY2FjZWQvZnVuY3Rpb25zLmNwcCNMMjA5XG5cdFx0aWYgKCFtaXhpbkNvbG9yIHx8ICFtaXhpbkNvbG9yLnJnYikge1xuXHRcdFx0dGhyb3cgbmV3IEVycm9yKCdBcmd1bWVudCB0byBcIm1peFwiIHdhcyBub3QgYSBDb2xvciBpbnN0YW5jZSwgYnV0IHJhdGhlciBhbiBpbnN0YW5jZSBvZiAnICsgdHlwZW9mIG1peGluQ29sb3IpO1xuXHRcdH1cblx0XHR2YXIgY29sb3IxID0gbWl4aW5Db2xvci5yZ2IoKTtcblx0XHR2YXIgY29sb3IyID0gdGhpcy5yZ2IoKTtcblx0XHR2YXIgcCA9IHdlaWdodCA9PT0gdW5kZWZpbmVkID8gMC41IDogd2VpZ2h0O1xuXG5cdFx0dmFyIHcgPSAyICogcCAtIDE7XG5cdFx0dmFyIGEgPSBjb2xvcjEuYWxwaGEoKSAtIGNvbG9yMi5hbHBoYSgpO1xuXG5cdFx0dmFyIHcxID0gKCgodyAqIGEgPT09IC0xKSA/IHcgOiAodyArIGEpIC8gKDEgKyB3ICogYSkpICsgMSkgLyAyLjA7XG5cdFx0dmFyIHcyID0gMSAtIHcxO1xuXG5cdFx0cmV0dXJuIENvbG9yLnJnYihcblx0XHRcdFx0dzEgKiBjb2xvcjEucmVkKCkgKyB3MiAqIGNvbG9yMi5yZWQoKSxcblx0XHRcdFx0dzEgKiBjb2xvcjEuZ3JlZW4oKSArIHcyICogY29sb3IyLmdyZWVuKCksXG5cdFx0XHRcdHcxICogY29sb3IxLmJsdWUoKSArIHcyICogY29sb3IyLmJsdWUoKSxcblx0XHRcdFx0Y29sb3IxLmFscGhhKCkgKiBwICsgY29sb3IyLmFscGhhKCkgKiAoMSAtIHApKTtcblx0fVxufTtcblxuLy8gbW9kZWwgY29udmVyc2lvbiBtZXRob2RzIGFuZCBzdGF0aWMgY29uc3RydWN0b3JzXG5PYmplY3Qua2V5cyhjb252ZXJ0KS5mb3JFYWNoKGZ1bmN0aW9uIChtb2RlbCkge1xuXHRpZiAoc2tpcHBlZE1vZGVscy5pbmRleE9mKG1vZGVsKSAhPT0gLTEpIHtcblx0XHRyZXR1cm47XG5cdH1cblxuXHR2YXIgY2hhbm5lbHMgPSBjb252ZXJ0W21vZGVsXS5jaGFubmVscztcblxuXHQvLyBjb252ZXJzaW9uIG1ldGhvZHNcblx0Q29sb3IucHJvdG90eXBlW21vZGVsXSA9IGZ1bmN0aW9uICgpIHtcblx0XHRpZiAodGhpcy5tb2RlbCA9PT0gbW9kZWwpIHtcblx0XHRcdHJldHVybiBuZXcgQ29sb3IodGhpcyk7XG5cdFx0fVxuXG5cdFx0aWYgKGFyZ3VtZW50cy5sZW5ndGgpIHtcblx0XHRcdHJldHVybiBuZXcgQ29sb3IoYXJndW1lbnRzLCBtb2RlbCk7XG5cdFx0fVxuXG5cdFx0dmFyIG5ld0FscGhhID0gdHlwZW9mIGFyZ3VtZW50c1tjaGFubmVsc10gPT09ICdudW1iZXInID8gY2hhbm5lbHMgOiB0aGlzLnZhbHBoYTtcblx0XHRyZXR1cm4gbmV3IENvbG9yKGFzc2VydEFycmF5KGNvbnZlcnRbdGhpcy5tb2RlbF1bbW9kZWxdLnJhdyh0aGlzLmNvbG9yKSkuY29uY2F0KG5ld0FscGhhKSwgbW9kZWwpO1xuXHR9O1xuXG5cdC8vICdzdGF0aWMnIGNvbnN0cnVjdGlvbiBtZXRob2RzXG5cdENvbG9yW21vZGVsXSA9IGZ1bmN0aW9uIChjb2xvcikge1xuXHRcdGlmICh0eXBlb2YgY29sb3IgPT09ICdudW1iZXInKSB7XG5cdFx0XHRjb2xvciA9IHplcm9BcnJheShfc2xpY2UuY2FsbChhcmd1bWVudHMpLCBjaGFubmVscyk7XG5cdFx0fVxuXHRcdHJldHVybiBuZXcgQ29sb3IoY29sb3IsIG1vZGVsKTtcblx0fTtcbn0pO1xuXG5mdW5jdGlvbiByb3VuZFRvKG51bSwgcGxhY2VzKSB7XG5cdHJldHVybiBOdW1iZXIobnVtLnRvRml4ZWQocGxhY2VzKSk7XG59XG5cbmZ1bmN0aW9uIHJvdW5kVG9QbGFjZShwbGFjZXMpIHtcblx0cmV0dXJuIGZ1bmN0aW9uIChudW0pIHtcblx0XHRyZXR1cm4gcm91bmRUbyhudW0sIHBsYWNlcyk7XG5cdH07XG59XG5cbmZ1bmN0aW9uIGdldHNldChtb2RlbCwgY2hhbm5lbCwgbW9kaWZpZXIpIHtcblx0bW9kZWwgPSBBcnJheS5pc0FycmF5KG1vZGVsKSA/IG1vZGVsIDogW21vZGVsXTtcblxuXHRtb2RlbC5mb3JFYWNoKGZ1bmN0aW9uIChtKSB7XG5cdFx0KGxpbWl0ZXJzW21dIHx8IChsaW1pdGVyc1ttXSA9IFtdKSlbY2hhbm5lbF0gPSBtb2RpZmllcjtcblx0fSk7XG5cblx0bW9kZWwgPSBtb2RlbFswXTtcblxuXHRyZXR1cm4gZnVuY3Rpb24gKHZhbCkge1xuXHRcdHZhciByZXN1bHQ7XG5cblx0XHRpZiAoYXJndW1lbnRzLmxlbmd0aCkge1xuXHRcdFx0aWYgKG1vZGlmaWVyKSB7XG5cdFx0XHRcdHZhbCA9IG1vZGlmaWVyKHZhbCk7XG5cdFx0XHR9XG5cblx0XHRcdHJlc3VsdCA9IHRoaXNbbW9kZWxdKCk7XG5cdFx0XHRyZXN1bHQuY29sb3JbY2hhbm5lbF0gPSB2YWw7XG5cdFx0XHRyZXR1cm4gcmVzdWx0O1xuXHRcdH1cblxuXHRcdHJlc3VsdCA9IHRoaXNbbW9kZWxdKCkuY29sb3JbY2hhbm5lbF07XG5cdFx0aWYgKG1vZGlmaWVyKSB7XG5cdFx0XHRyZXN1bHQgPSBtb2RpZmllcihyZXN1bHQpO1xuXHRcdH1cblxuXHRcdHJldHVybiByZXN1bHQ7XG5cdH07XG59XG5cbmZ1bmN0aW9uIG1heGZuKG1heCkge1xuXHRyZXR1cm4gZnVuY3Rpb24gKHYpIHtcblx0XHRyZXR1cm4gTWF0aC5tYXgoMCwgTWF0aC5taW4obWF4LCB2KSk7XG5cdH07XG59XG5cbmZ1bmN0aW9uIGFzc2VydEFycmF5KHZhbCkge1xuXHRyZXR1cm4gQXJyYXkuaXNBcnJheSh2YWwpID8gdmFsIDogW3ZhbF07XG59XG5cbmZ1bmN0aW9uIHplcm9BcnJheShhcnIsIGxlbmd0aCkge1xuXHRmb3IgKHZhciBpID0gMDsgaSA8IGxlbmd0aDsgaSsrKSB7XG5cdFx0aWYgKHR5cGVvZiBhcnJbaV0gIT09ICdudW1iZXInKSB7XG5cdFx0XHRhcnJbaV0gPSAwO1xuXHRcdH1cblx0fVxuXG5cdHJldHVybiBhcnI7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gQ29sb3I7XG5cblxuLyoqKi8gfSksXG4vKiAxNyAqL1xuLyoqKi8gKGZ1bmN0aW9uKG1vZHVsZSwgZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXykge1xuXG4vKiBNSVQgbGljZW5zZSAqL1xudmFyIGNvbG9yTmFtZXMgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKDUpO1xudmFyIHN3aXp6bGUgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKDE4KTtcblxudmFyIHJldmVyc2VOYW1lcyA9IHt9O1xuXG4vLyBjcmVhdGUgYSBsaXN0IG9mIHJldmVyc2UgY29sb3IgbmFtZXNcbmZvciAodmFyIG5hbWUgaW4gY29sb3JOYW1lcykge1xuXHRpZiAoY29sb3JOYW1lcy5oYXNPd25Qcm9wZXJ0eShuYW1lKSkge1xuXHRcdHJldmVyc2VOYW1lc1tjb2xvck5hbWVzW25hbWVdXSA9IG5hbWU7XG5cdH1cbn1cblxudmFyIGNzID0gbW9kdWxlLmV4cG9ydHMgPSB7XG5cdHRvOiB7fSxcblx0Z2V0OiB7fVxufTtcblxuY3MuZ2V0ID0gZnVuY3Rpb24gKHN0cmluZykge1xuXHR2YXIgcHJlZml4ID0gc3RyaW5nLnN1YnN0cmluZygwLCAzKS50b0xvd2VyQ2FzZSgpO1xuXHR2YXIgdmFsO1xuXHR2YXIgbW9kZWw7XG5cdHN3aXRjaCAocHJlZml4KSB7XG5cdFx0Y2FzZSAnaHNsJzpcblx0XHRcdHZhbCA9IGNzLmdldC5oc2woc3RyaW5nKTtcblx0XHRcdG1vZGVsID0gJ2hzbCc7XG5cdFx0XHRicmVhaztcblx0XHRjYXNlICdod2InOlxuXHRcdFx0dmFsID0gY3MuZ2V0Lmh3YihzdHJpbmcpO1xuXHRcdFx0bW9kZWwgPSAnaHdiJztcblx0XHRcdGJyZWFrO1xuXHRcdGRlZmF1bHQ6XG5cdFx0XHR2YWwgPSBjcy5nZXQucmdiKHN0cmluZyk7XG5cdFx0XHRtb2RlbCA9ICdyZ2InO1xuXHRcdFx0YnJlYWs7XG5cdH1cblxuXHRpZiAoIXZhbCkge1xuXHRcdHJldHVybiBudWxsO1xuXHR9XG5cblx0cmV0dXJuIHttb2RlbDogbW9kZWwsIHZhbHVlOiB2YWx9O1xufTtcblxuY3MuZ2V0LnJnYiA9IGZ1bmN0aW9uIChzdHJpbmcpIHtcblx0aWYgKCFzdHJpbmcpIHtcblx0XHRyZXR1cm4gbnVsbDtcblx0fVxuXG5cdHZhciBhYmJyID0gL14jKFthLWYwLTldezMsNH0pJC9pO1xuXHR2YXIgaGV4ID0gL14jKFthLWYwLTldezZ9KShbYS1mMC05XXsyfSk/JC9pO1xuXHR2YXIgcmdiYSA9IC9ecmdiYT9cXChcXHMqKFsrLV0/XFxkKylcXHMqLFxccyooWystXT9cXGQrKVxccyosXFxzKihbKy1dP1xcZCspXFxzKig/OixcXHMqKFsrLV0/W1xcZFxcLl0rKVxccyopP1xcKSQvO1xuXHR2YXIgcGVyID0gL15yZ2JhP1xcKFxccyooWystXT9bXFxkXFwuXSspXFwlXFxzKixcXHMqKFsrLV0/W1xcZFxcLl0rKVxcJVxccyosXFxzKihbKy1dP1tcXGRcXC5dKylcXCVcXHMqKD86LFxccyooWystXT9bXFxkXFwuXSspXFxzKik/XFwpJC87XG5cdHZhciBrZXl3b3JkID0gLyhcXEQrKS87XG5cblx0dmFyIHJnYiA9IFswLCAwLCAwLCAxXTtcblx0dmFyIG1hdGNoO1xuXHR2YXIgaTtcblx0dmFyIGhleEFscGhhO1xuXG5cdGlmIChtYXRjaCA9IHN0cmluZy5tYXRjaChoZXgpKSB7XG5cdFx0aGV4QWxwaGEgPSBtYXRjaFsyXTtcblx0XHRtYXRjaCA9IG1hdGNoWzFdO1xuXG5cdFx0Zm9yIChpID0gMDsgaSA8IDM7IGkrKykge1xuXHRcdFx0Ly8gaHR0cHM6Ly9qc3BlcmYuY29tL3NsaWNlLXZzLXN1YnN0ci12cy1zdWJzdHJpbmctbWV0aG9kcy1sb25nLXN0cmluZy8xOVxuXHRcdFx0dmFyIGkyID0gaSAqIDI7XG5cdFx0XHRyZ2JbaV0gPSBwYXJzZUludChtYXRjaC5zbGljZShpMiwgaTIgKyAyKSwgMTYpO1xuXHRcdH1cblxuXHRcdGlmIChoZXhBbHBoYSkge1xuXHRcdFx0cmdiWzNdID0gTWF0aC5yb3VuZCgocGFyc2VJbnQoaGV4QWxwaGEsIDE2KSAvIDI1NSkgKiAxMDApIC8gMTAwO1xuXHRcdH1cblx0fSBlbHNlIGlmIChtYXRjaCA9IHN0cmluZy5tYXRjaChhYmJyKSkge1xuXHRcdG1hdGNoID0gbWF0Y2hbMV07XG5cdFx0aGV4QWxwaGEgPSBtYXRjaFszXTtcblxuXHRcdGZvciAoaSA9IDA7IGkgPCAzOyBpKyspIHtcblx0XHRcdHJnYltpXSA9IHBhcnNlSW50KG1hdGNoW2ldICsgbWF0Y2hbaV0sIDE2KTtcblx0XHR9XG5cblx0XHRpZiAoaGV4QWxwaGEpIHtcblx0XHRcdHJnYlszXSA9IE1hdGgucm91bmQoKHBhcnNlSW50KGhleEFscGhhICsgaGV4QWxwaGEsIDE2KSAvIDI1NSkgKiAxMDApIC8gMTAwO1xuXHRcdH1cblx0fSBlbHNlIGlmIChtYXRjaCA9IHN0cmluZy5tYXRjaChyZ2JhKSkge1xuXHRcdGZvciAoaSA9IDA7IGkgPCAzOyBpKyspIHtcblx0XHRcdHJnYltpXSA9IHBhcnNlSW50KG1hdGNoW2kgKyAxXSwgMCk7XG5cdFx0fVxuXG5cdFx0aWYgKG1hdGNoWzRdKSB7XG5cdFx0XHRyZ2JbM10gPSBwYXJzZUZsb2F0KG1hdGNoWzRdKTtcblx0XHR9XG5cdH0gZWxzZSBpZiAobWF0Y2ggPSBzdHJpbmcubWF0Y2gocGVyKSkge1xuXHRcdGZvciAoaSA9IDA7IGkgPCAzOyBpKyspIHtcblx0XHRcdHJnYltpXSA9IE1hdGgucm91bmQocGFyc2VGbG9hdChtYXRjaFtpICsgMV0pICogMi41NSk7XG5cdFx0fVxuXG5cdFx0aWYgKG1hdGNoWzRdKSB7XG5cdFx0XHRyZ2JbM10gPSBwYXJzZUZsb2F0KG1hdGNoWzRdKTtcblx0XHR9XG5cdH0gZWxzZSBpZiAobWF0Y2ggPSBzdHJpbmcubWF0Y2goa2V5d29yZCkpIHtcblx0XHRpZiAobWF0Y2hbMV0gPT09ICd0cmFuc3BhcmVudCcpIHtcblx0XHRcdHJldHVybiBbMCwgMCwgMCwgMF07XG5cdFx0fVxuXG5cdFx0cmdiID0gY29sb3JOYW1lc1ttYXRjaFsxXV07XG5cblx0XHRpZiAoIXJnYikge1xuXHRcdFx0cmV0dXJuIG51bGw7XG5cdFx0fVxuXG5cdFx0cmdiWzNdID0gMTtcblxuXHRcdHJldHVybiByZ2I7XG5cdH0gZWxzZSB7XG5cdFx0cmV0dXJuIG51bGw7XG5cdH1cblxuXHRmb3IgKGkgPSAwOyBpIDwgMzsgaSsrKSB7XG5cdFx0cmdiW2ldID0gY2xhbXAocmdiW2ldLCAwLCAyNTUpO1xuXHR9XG5cdHJnYlszXSA9IGNsYW1wKHJnYlszXSwgMCwgMSk7XG5cblx0cmV0dXJuIHJnYjtcbn07XG5cbmNzLmdldC5oc2wgPSBmdW5jdGlvbiAoc3RyaW5nKSB7XG5cdGlmICghc3RyaW5nKSB7XG5cdFx0cmV0dXJuIG51bGw7XG5cdH1cblxuXHR2YXIgaHNsID0gL15oc2xhP1xcKFxccyooWystXT8oPzpcXGQqXFwuKT9cXGQrKSg/OmRlZyk/XFxzKixcXHMqKFsrLV0/W1xcZFxcLl0rKSVcXHMqLFxccyooWystXT9bXFxkXFwuXSspJVxccyooPzosXFxzKihbKy1dP1tcXGRcXC5dKylcXHMqKT9cXCkkLztcblx0dmFyIG1hdGNoID0gc3RyaW5nLm1hdGNoKGhzbCk7XG5cblx0aWYgKG1hdGNoKSB7XG5cdFx0dmFyIGFscGhhID0gcGFyc2VGbG9hdChtYXRjaFs0XSk7XG5cdFx0dmFyIGggPSAocGFyc2VGbG9hdChtYXRjaFsxXSkgKyAzNjApICUgMzYwO1xuXHRcdHZhciBzID0gY2xhbXAocGFyc2VGbG9hdChtYXRjaFsyXSksIDAsIDEwMCk7XG5cdFx0dmFyIGwgPSBjbGFtcChwYXJzZUZsb2F0KG1hdGNoWzNdKSwgMCwgMTAwKTtcblx0XHR2YXIgYSA9IGNsYW1wKGlzTmFOKGFscGhhKSA/IDEgOiBhbHBoYSwgMCwgMSk7XG5cblx0XHRyZXR1cm4gW2gsIHMsIGwsIGFdO1xuXHR9XG5cblx0cmV0dXJuIG51bGw7XG59O1xuXG5jcy5nZXQuaHdiID0gZnVuY3Rpb24gKHN0cmluZykge1xuXHRpZiAoIXN0cmluZykge1xuXHRcdHJldHVybiBudWxsO1xuXHR9XG5cblx0dmFyIGh3YiA9IC9eaHdiXFwoXFxzKihbKy1dP1xcZCpbXFwuXT9cXGQrKSg/OmRlZyk/XFxzKixcXHMqKFsrLV0/W1xcZFxcLl0rKSVcXHMqLFxccyooWystXT9bXFxkXFwuXSspJVxccyooPzosXFxzKihbKy1dP1tcXGRcXC5dKylcXHMqKT9cXCkkLztcblx0dmFyIG1hdGNoID0gc3RyaW5nLm1hdGNoKGh3Yik7XG5cblx0aWYgKG1hdGNoKSB7XG5cdFx0dmFyIGFscGhhID0gcGFyc2VGbG9hdChtYXRjaFs0XSk7XG5cdFx0dmFyIGggPSAoKHBhcnNlRmxvYXQobWF0Y2hbMV0pICUgMzYwKSArIDM2MCkgJSAzNjA7XG5cdFx0dmFyIHcgPSBjbGFtcChwYXJzZUZsb2F0KG1hdGNoWzJdKSwgMCwgMTAwKTtcblx0XHR2YXIgYiA9IGNsYW1wKHBhcnNlRmxvYXQobWF0Y2hbM10pLCAwLCAxMDApO1xuXHRcdHZhciBhID0gY2xhbXAoaXNOYU4oYWxwaGEpID8gMSA6IGFscGhhLCAwLCAxKTtcblx0XHRyZXR1cm4gW2gsIHcsIGIsIGFdO1xuXHR9XG5cblx0cmV0dXJuIG51bGw7XG59O1xuXG5jcy50by5oZXggPSBmdW5jdGlvbiAoKSB7XG5cdHZhciByZ2JhID0gc3dpenpsZShhcmd1bWVudHMpO1xuXG5cdHJldHVybiAoXG5cdFx0JyMnICtcblx0XHRoZXhEb3VibGUocmdiYVswXSkgK1xuXHRcdGhleERvdWJsZShyZ2JhWzFdKSArXG5cdFx0aGV4RG91YmxlKHJnYmFbMl0pICtcblx0XHQocmdiYVszXSA8IDFcblx0XHRcdD8gKGhleERvdWJsZShNYXRoLnJvdW5kKHJnYmFbM10gKiAyNTUpKSlcblx0XHRcdDogJycpXG5cdCk7XG59O1xuXG5jcy50by5yZ2IgPSBmdW5jdGlvbiAoKSB7XG5cdHZhciByZ2JhID0gc3dpenpsZShhcmd1bWVudHMpO1xuXG5cdHJldHVybiByZ2JhLmxlbmd0aCA8IDQgfHwgcmdiYVszXSA9PT0gMVxuXHRcdD8gJ3JnYignICsgTWF0aC5yb3VuZChyZ2JhWzBdKSArICcsICcgKyBNYXRoLnJvdW5kKHJnYmFbMV0pICsgJywgJyArIE1hdGgucm91bmQocmdiYVsyXSkgKyAnKSdcblx0XHQ6ICdyZ2JhKCcgKyBNYXRoLnJvdW5kKHJnYmFbMF0pICsgJywgJyArIE1hdGgucm91bmQocmdiYVsxXSkgKyAnLCAnICsgTWF0aC5yb3VuZChyZ2JhWzJdKSArICcsICcgKyByZ2JhWzNdICsgJyknO1xufTtcblxuY3MudG8ucmdiLnBlcmNlbnQgPSBmdW5jdGlvbiAoKSB7XG5cdHZhciByZ2JhID0gc3dpenpsZShhcmd1bWVudHMpO1xuXG5cdHZhciByID0gTWF0aC5yb3VuZChyZ2JhWzBdIC8gMjU1ICogMTAwKTtcblx0dmFyIGcgPSBNYXRoLnJvdW5kKHJnYmFbMV0gLyAyNTUgKiAxMDApO1xuXHR2YXIgYiA9IE1hdGgucm91bmQocmdiYVsyXSAvIDI1NSAqIDEwMCk7XG5cblx0cmV0dXJuIHJnYmEubGVuZ3RoIDwgNCB8fCByZ2JhWzNdID09PSAxXG5cdFx0PyAncmdiKCcgKyByICsgJyUsICcgKyBnICsgJyUsICcgKyBiICsgJyUpJ1xuXHRcdDogJ3JnYmEoJyArIHIgKyAnJSwgJyArIGcgKyAnJSwgJyArIGIgKyAnJSwgJyArIHJnYmFbM10gKyAnKSc7XG59O1xuXG5jcy50by5oc2wgPSBmdW5jdGlvbiAoKSB7XG5cdHZhciBoc2xhID0gc3dpenpsZShhcmd1bWVudHMpO1xuXHRyZXR1cm4gaHNsYS5sZW5ndGggPCA0IHx8IGhzbGFbM10gPT09IDFcblx0XHQ/ICdoc2woJyArIGhzbGFbMF0gKyAnLCAnICsgaHNsYVsxXSArICclLCAnICsgaHNsYVsyXSArICclKSdcblx0XHQ6ICdoc2xhKCcgKyBoc2xhWzBdICsgJywgJyArIGhzbGFbMV0gKyAnJSwgJyArIGhzbGFbMl0gKyAnJSwgJyArIGhzbGFbM10gKyAnKSc7XG59O1xuXG4vLyBod2IgaXMgYSBiaXQgZGlmZmVyZW50IHRoYW4gcmdiKGEpICYgaHNsKGEpIHNpbmNlIHRoZXJlIGlzIG5vIGFscGhhIHNwZWNpZmljIHN5bnRheFxuLy8gKGh3YiBoYXZlIGFscGhhIG9wdGlvbmFsICYgMSBpcyBkZWZhdWx0IHZhbHVlKVxuY3MudG8uaHdiID0gZnVuY3Rpb24gKCkge1xuXHR2YXIgaHdiYSA9IHN3aXp6bGUoYXJndW1lbnRzKTtcblxuXHR2YXIgYSA9ICcnO1xuXHRpZiAoaHdiYS5sZW5ndGggPj0gNCAmJiBod2JhWzNdICE9PSAxKSB7XG5cdFx0YSA9ICcsICcgKyBod2JhWzNdO1xuXHR9XG5cblx0cmV0dXJuICdod2IoJyArIGh3YmFbMF0gKyAnLCAnICsgaHdiYVsxXSArICclLCAnICsgaHdiYVsyXSArICclJyArIGEgKyAnKSc7XG59O1xuXG5jcy50by5rZXl3b3JkID0gZnVuY3Rpb24gKHJnYikge1xuXHRyZXR1cm4gcmV2ZXJzZU5hbWVzW3JnYi5zbGljZSgwLCAzKV07XG59O1xuXG4vLyBoZWxwZXJzXG5mdW5jdGlvbiBjbGFtcChudW0sIG1pbiwgbWF4KSB7XG5cdHJldHVybiBNYXRoLm1pbihNYXRoLm1heChtaW4sIG51bSksIG1heCk7XG59XG5cbmZ1bmN0aW9uIGhleERvdWJsZShudW0pIHtcblx0dmFyIHN0ciA9IG51bS50b1N0cmluZygxNikudG9VcHBlckNhc2UoKTtcblx0cmV0dXJuIChzdHIubGVuZ3RoIDwgMikgPyAnMCcgKyBzdHIgOiBzdHI7XG59XG5cblxuLyoqKi8gfSksXG4vKiAxOCAqL1xuLyoqKi8gKGZ1bmN0aW9uKG1vZHVsZSwgZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXykge1xuXG5cInVzZSBzdHJpY3RcIjtcblxuXG52YXIgaXNBcnJheWlzaCA9IF9fd2VicGFja19yZXF1aXJlX18oMTkpO1xuXG52YXIgY29uY2F0ID0gQXJyYXkucHJvdG90eXBlLmNvbmNhdDtcbnZhciBzbGljZSA9IEFycmF5LnByb3RvdHlwZS5zbGljZTtcblxudmFyIHN3aXp6bGUgPSBtb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIHN3aXp6bGUoYXJncykge1xuXHR2YXIgcmVzdWx0cyA9IFtdO1xuXG5cdGZvciAodmFyIGkgPSAwLCBsZW4gPSBhcmdzLmxlbmd0aDsgaSA8IGxlbjsgaSsrKSB7XG5cdFx0dmFyIGFyZyA9IGFyZ3NbaV07XG5cblx0XHRpZiAoaXNBcnJheWlzaChhcmcpKSB7XG5cdFx0XHQvLyBodHRwOi8vanNwZXJmLmNvbS9qYXZhc2NyaXB0LWFycmF5LWNvbmNhdC12cy1wdXNoLzk4XG5cdFx0XHRyZXN1bHRzID0gY29uY2F0LmNhbGwocmVzdWx0cywgc2xpY2UuY2FsbChhcmcpKTtcblx0XHR9IGVsc2Uge1xuXHRcdFx0cmVzdWx0cy5wdXNoKGFyZyk7XG5cdFx0fVxuXHR9XG5cblx0cmV0dXJuIHJlc3VsdHM7XG59O1xuXG5zd2l6emxlLndyYXAgPSBmdW5jdGlvbiAoZm4pIHtcblx0cmV0dXJuIGZ1bmN0aW9uICgpIHtcblx0XHRyZXR1cm4gZm4oc3dpenpsZShhcmd1bWVudHMpKTtcblx0fTtcbn07XG5cblxuLyoqKi8gfSksXG4vKiAxOSAqL1xuLyoqKi8gKGZ1bmN0aW9uKG1vZHVsZSwgZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXykge1xuXG5cInVzZSBzdHJpY3RcIjtcblxuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIGlzQXJyYXlpc2gob2JqKSB7XG5cdGlmICghb2JqKSB7XG5cdFx0cmV0dXJuIGZhbHNlO1xuXHR9XG5cblx0cmV0dXJuIG9iaiBpbnN0YW5jZW9mIEFycmF5IHx8IEFycmF5LmlzQXJyYXkob2JqKSB8fFxuXHRcdChvYmoubGVuZ3RoID49IDAgJiYgb2JqLnNwbGljZSBpbnN0YW5jZW9mIEZ1bmN0aW9uKTtcbn07XG5cblxuLyoqKi8gfSksXG4vKiAyMCAqL1xuLyoqKi8gKGZ1bmN0aW9uKG1vZHVsZSwgZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXykge1xuXG52YXIgY29udmVyc2lvbnMgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKDYpO1xudmFyIHJvdXRlID0gX193ZWJwYWNrX3JlcXVpcmVfXygyMSk7XG5cbnZhciBjb252ZXJ0ID0ge307XG5cbnZhciBtb2RlbHMgPSBPYmplY3Qua2V5cyhjb252ZXJzaW9ucyk7XG5cbmZ1bmN0aW9uIHdyYXBSYXcoZm4pIHtcblx0dmFyIHdyYXBwZWRGbiA9IGZ1bmN0aW9uIChhcmdzKSB7XG5cdFx0aWYgKGFyZ3MgPT09IHVuZGVmaW5lZCB8fCBhcmdzID09PSBudWxsKSB7XG5cdFx0XHRyZXR1cm4gYXJncztcblx0XHR9XG5cblx0XHRpZiAoYXJndW1lbnRzLmxlbmd0aCA+IDEpIHtcblx0XHRcdGFyZ3MgPSBBcnJheS5wcm90b3R5cGUuc2xpY2UuY2FsbChhcmd1bWVudHMpO1xuXHRcdH1cblxuXHRcdHJldHVybiBmbihhcmdzKTtcblx0fTtcblxuXHQvLyBwcmVzZXJ2ZSAuY29udmVyc2lvbiBwcm9wZXJ0eSBpZiB0aGVyZSBpcyBvbmVcblx0aWYgKCdjb252ZXJzaW9uJyBpbiBmbikge1xuXHRcdHdyYXBwZWRGbi5jb252ZXJzaW9uID0gZm4uY29udmVyc2lvbjtcblx0fVxuXG5cdHJldHVybiB3cmFwcGVkRm47XG59XG5cbmZ1bmN0aW9uIHdyYXBSb3VuZGVkKGZuKSB7XG5cdHZhciB3cmFwcGVkRm4gPSBmdW5jdGlvbiAoYXJncykge1xuXHRcdGlmIChhcmdzID09PSB1bmRlZmluZWQgfHwgYXJncyA9PT0gbnVsbCkge1xuXHRcdFx0cmV0dXJuIGFyZ3M7XG5cdFx0fVxuXG5cdFx0aWYgKGFyZ3VtZW50cy5sZW5ndGggPiAxKSB7XG5cdFx0XHRhcmdzID0gQXJyYXkucHJvdG90eXBlLnNsaWNlLmNhbGwoYXJndW1lbnRzKTtcblx0XHR9XG5cblx0XHR2YXIgcmVzdWx0ID0gZm4oYXJncyk7XG5cblx0XHQvLyB3ZSdyZSBhc3N1bWluZyB0aGUgcmVzdWx0IGlzIGFuIGFycmF5IGhlcmUuXG5cdFx0Ly8gc2VlIG5vdGljZSBpbiBjb252ZXJzaW9ucy5qczsgZG9uJ3QgdXNlIGJveCB0eXBlc1xuXHRcdC8vIGluIGNvbnZlcnNpb24gZnVuY3Rpb25zLlxuXHRcdGlmICh0eXBlb2YgcmVzdWx0ID09PSAnb2JqZWN0Jykge1xuXHRcdFx0Zm9yICh2YXIgbGVuID0gcmVzdWx0Lmxlbmd0aCwgaSA9IDA7IGkgPCBsZW47IGkrKykge1xuXHRcdFx0XHRyZXN1bHRbaV0gPSBNYXRoLnJvdW5kKHJlc3VsdFtpXSk7XG5cdFx0XHR9XG5cdFx0fVxuXG5cdFx0cmV0dXJuIHJlc3VsdDtcblx0fTtcblxuXHQvLyBwcmVzZXJ2ZSAuY29udmVyc2lvbiBwcm9wZXJ0eSBpZiB0aGVyZSBpcyBvbmVcblx0aWYgKCdjb252ZXJzaW9uJyBpbiBmbikge1xuXHRcdHdyYXBwZWRGbi5jb252ZXJzaW9uID0gZm4uY29udmVyc2lvbjtcblx0fVxuXG5cdHJldHVybiB3cmFwcGVkRm47XG59XG5cbm1vZGVscy5mb3JFYWNoKGZ1bmN0aW9uIChmcm9tTW9kZWwpIHtcblx0Y29udmVydFtmcm9tTW9kZWxdID0ge307XG5cblx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGNvbnZlcnRbZnJvbU1vZGVsXSwgJ2NoYW5uZWxzJywge3ZhbHVlOiBjb252ZXJzaW9uc1tmcm9tTW9kZWxdLmNoYW5uZWxzfSk7XG5cdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShjb252ZXJ0W2Zyb21Nb2RlbF0sICdsYWJlbHMnLCB7dmFsdWU6IGNvbnZlcnNpb25zW2Zyb21Nb2RlbF0ubGFiZWxzfSk7XG5cblx0dmFyIHJvdXRlcyA9IHJvdXRlKGZyb21Nb2RlbCk7XG5cdHZhciByb3V0ZU1vZGVscyA9IE9iamVjdC5rZXlzKHJvdXRlcyk7XG5cblx0cm91dGVNb2RlbHMuZm9yRWFjaChmdW5jdGlvbiAodG9Nb2RlbCkge1xuXHRcdHZhciBmbiA9IHJvdXRlc1t0b01vZGVsXTtcblxuXHRcdGNvbnZlcnRbZnJvbU1vZGVsXVt0b01vZGVsXSA9IHdyYXBSb3VuZGVkKGZuKTtcblx0XHRjb252ZXJ0W2Zyb21Nb2RlbF1bdG9Nb2RlbF0ucmF3ID0gd3JhcFJhdyhmbik7XG5cdH0pO1xufSk7XG5cbm1vZHVsZS5leHBvcnRzID0gY29udmVydDtcblxuXG4vKioqLyB9KSxcbi8qIDIxICovXG4vKioqLyAoZnVuY3Rpb24obW9kdWxlLCBleHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKSB7XG5cbnZhciBjb252ZXJzaW9ucyA9IF9fd2VicGFja19yZXF1aXJlX18oNik7XG5cbi8qXG5cdHRoaXMgZnVuY3Rpb24gcm91dGVzIGEgbW9kZWwgdG8gYWxsIG90aGVyIG1vZGVscy5cblxuXHRhbGwgZnVuY3Rpb25zIHRoYXQgYXJlIHJvdXRlZCBoYXZlIGEgcHJvcGVydHkgYC5jb252ZXJzaW9uYCBhdHRhY2hlZFxuXHR0byB0aGUgcmV0dXJuZWQgc3ludGhldGljIGZ1bmN0aW9uLiBUaGlzIHByb3BlcnR5IGlzIGFuIGFycmF5XG5cdG9mIHN0cmluZ3MsIGVhY2ggd2l0aCB0aGUgc3RlcHMgaW4gYmV0d2VlbiB0aGUgJ2Zyb20nIGFuZCAndG8nXG5cdGNvbG9yIG1vZGVscyAoaW5jbHVzaXZlKS5cblxuXHRjb252ZXJzaW9ucyB0aGF0IGFyZSBub3QgcG9zc2libGUgc2ltcGx5IGFyZSBub3QgaW5jbHVkZWQuXG4qL1xuXG5mdW5jdGlvbiBidWlsZEdyYXBoKCkge1xuXHR2YXIgZ3JhcGggPSB7fTtcblx0Ly8gaHR0cHM6Ly9qc3BlcmYuY29tL29iamVjdC1rZXlzLXZzLWZvci1pbi13aXRoLWNsb3N1cmUvM1xuXHR2YXIgbW9kZWxzID0gT2JqZWN0LmtleXMoY29udmVyc2lvbnMpO1xuXG5cdGZvciAodmFyIGxlbiA9IG1vZGVscy5sZW5ndGgsIGkgPSAwOyBpIDwgbGVuOyBpKyspIHtcblx0XHRncmFwaFttb2RlbHNbaV1dID0ge1xuXHRcdFx0Ly8gaHR0cDovL2pzcGVyZi5jb20vMS12cy1pbmZpbml0eVxuXHRcdFx0Ly8gbWljcm8tb3B0LCBidXQgdGhpcyBpcyBzaW1wbGUuXG5cdFx0XHRkaXN0YW5jZTogLTEsXG5cdFx0XHRwYXJlbnQ6IG51bGxcblx0XHR9O1xuXHR9XG5cblx0cmV0dXJuIGdyYXBoO1xufVxuXG4vLyBodHRwczovL2VuLndpa2lwZWRpYS5vcmcvd2lraS9CcmVhZHRoLWZpcnN0X3NlYXJjaFxuZnVuY3Rpb24gZGVyaXZlQkZTKGZyb21Nb2RlbCkge1xuXHR2YXIgZ3JhcGggPSBidWlsZEdyYXBoKCk7XG5cdHZhciBxdWV1ZSA9IFtmcm9tTW9kZWxdOyAvLyB1bnNoaWZ0IC0+IHF1ZXVlIC0+IHBvcFxuXG5cdGdyYXBoW2Zyb21Nb2RlbF0uZGlzdGFuY2UgPSAwO1xuXG5cdHdoaWxlIChxdWV1ZS5sZW5ndGgpIHtcblx0XHR2YXIgY3VycmVudCA9IHF1ZXVlLnBvcCgpO1xuXHRcdHZhciBhZGphY2VudHMgPSBPYmplY3Qua2V5cyhjb252ZXJzaW9uc1tjdXJyZW50XSk7XG5cblx0XHRmb3IgKHZhciBsZW4gPSBhZGphY2VudHMubGVuZ3RoLCBpID0gMDsgaSA8IGxlbjsgaSsrKSB7XG5cdFx0XHR2YXIgYWRqYWNlbnQgPSBhZGphY2VudHNbaV07XG5cdFx0XHR2YXIgbm9kZSA9IGdyYXBoW2FkamFjZW50XTtcblxuXHRcdFx0aWYgKG5vZGUuZGlzdGFuY2UgPT09IC0xKSB7XG5cdFx0XHRcdG5vZGUuZGlzdGFuY2UgPSBncmFwaFtjdXJyZW50XS5kaXN0YW5jZSArIDE7XG5cdFx0XHRcdG5vZGUucGFyZW50ID0gY3VycmVudDtcblx0XHRcdFx0cXVldWUudW5zaGlmdChhZGphY2VudCk7XG5cdFx0XHR9XG5cdFx0fVxuXHR9XG5cblx0cmV0dXJuIGdyYXBoO1xufVxuXG5mdW5jdGlvbiBsaW5rKGZyb20sIHRvKSB7XG5cdHJldHVybiBmdW5jdGlvbiAoYXJncykge1xuXHRcdHJldHVybiB0byhmcm9tKGFyZ3MpKTtcblx0fTtcbn1cblxuZnVuY3Rpb24gd3JhcENvbnZlcnNpb24odG9Nb2RlbCwgZ3JhcGgpIHtcblx0dmFyIHBhdGggPSBbZ3JhcGhbdG9Nb2RlbF0ucGFyZW50LCB0b01vZGVsXTtcblx0dmFyIGZuID0gY29udmVyc2lvbnNbZ3JhcGhbdG9Nb2RlbF0ucGFyZW50XVt0b01vZGVsXTtcblxuXHR2YXIgY3VyID0gZ3JhcGhbdG9Nb2RlbF0ucGFyZW50O1xuXHR3aGlsZSAoZ3JhcGhbY3VyXS5wYXJlbnQpIHtcblx0XHRwYXRoLnVuc2hpZnQoZ3JhcGhbY3VyXS5wYXJlbnQpO1xuXHRcdGZuID0gbGluayhjb252ZXJzaW9uc1tncmFwaFtjdXJdLnBhcmVudF1bY3VyXSwgZm4pO1xuXHRcdGN1ciA9IGdyYXBoW2N1cl0ucGFyZW50O1xuXHR9XG5cblx0Zm4uY29udmVyc2lvbiA9IHBhdGg7XG5cdHJldHVybiBmbjtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAoZnJvbU1vZGVsKSB7XG5cdHZhciBncmFwaCA9IGRlcml2ZUJGUyhmcm9tTW9kZWwpO1xuXHR2YXIgY29udmVyc2lvbiA9IHt9O1xuXG5cdHZhciBtb2RlbHMgPSBPYmplY3Qua2V5cyhncmFwaCk7XG5cdGZvciAodmFyIGxlbiA9IG1vZGVscy5sZW5ndGgsIGkgPSAwOyBpIDwgbGVuOyBpKyspIHtcblx0XHR2YXIgdG9Nb2RlbCA9IG1vZGVsc1tpXTtcblx0XHR2YXIgbm9kZSA9IGdyYXBoW3RvTW9kZWxdO1xuXG5cdFx0aWYgKG5vZGUucGFyZW50ID09PSBudWxsKSB7XG5cdFx0XHQvLyBubyBwb3NzaWJsZSBjb252ZXJzaW9uLCBvciB0aGlzIG5vZGUgaXMgdGhlIHNvdXJjZSBtb2RlbC5cblx0XHRcdGNvbnRpbnVlO1xuXHRcdH1cblxuXHRcdGNvbnZlcnNpb25bdG9Nb2RlbF0gPSB3cmFwQ29udmVyc2lvbih0b01vZGVsLCBncmFwaCk7XG5cdH1cblxuXHRyZXR1cm4gY29udmVyc2lvbjtcbn07XG5cblxuXG4vKioqLyB9KSxcbi8qIDIyICovXG4vKioqLyAoZnVuY3Rpb24obW9kdWxlLCBleHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKSB7XG5cblwidXNlIHN0cmljdFwiO1xuXG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfY3JlYXRlQ2xhc3MgPSBmdW5jdGlvbiAoKSB7IGZ1bmN0aW9uIGRlZmluZVByb3BlcnRpZXModGFyZ2V0LCBwcm9wcykgeyBmb3IgKHZhciBpID0gMDsgaSA8IHByb3BzLmxlbmd0aDsgaSsrKSB7IHZhciBkZXNjcmlwdG9yID0gcHJvcHNbaV07IGRlc2NyaXB0b3IuZW51bWVyYWJsZSA9IGRlc2NyaXB0b3IuZW51bWVyYWJsZSB8fCBmYWxzZTsgZGVzY3JpcHRvci5jb25maWd1cmFibGUgPSB0cnVlOyBpZiAoXCJ2YWx1ZVwiIGluIGRlc2NyaXB0b3IpIGRlc2NyaXB0b3Iud3JpdGFibGUgPSB0cnVlOyBPYmplY3QuZGVmaW5lUHJvcGVydHkodGFyZ2V0LCBkZXNjcmlwdG9yLmtleSwgZGVzY3JpcHRvcik7IH0gfSByZXR1cm4gZnVuY3Rpb24gKENvbnN0cnVjdG9yLCBwcm90b1Byb3BzLCBzdGF0aWNQcm9wcykgeyBpZiAocHJvdG9Qcm9wcykgZGVmaW5lUHJvcGVydGllcyhDb25zdHJ1Y3Rvci5wcm90b3R5cGUsIHByb3RvUHJvcHMpOyBpZiAoc3RhdGljUHJvcHMpIGRlZmluZVByb3BlcnRpZXMoQ29uc3RydWN0b3IsIHN0YXRpY1Byb3BzKTsgcmV0dXJuIENvbnN0cnVjdG9yOyB9OyB9KCk7XG5cbnZhciBfanF1ZXJ5ID0gX193ZWJwYWNrX3JlcXVpcmVfXygwKTtcblxudmFyIF9qcXVlcnkyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfanF1ZXJ5KTtcblxudmFyIF9Db2xvckl0ZW0gPSBfX3dlYnBhY2tfcmVxdWlyZV9fKDIpO1xuXG52YXIgX0NvbG9ySXRlbTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9Db2xvckl0ZW0pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG5mdW5jdGlvbiBfY2xhc3NDYWxsQ2hlY2soaW5zdGFuY2UsIENvbnN0cnVjdG9yKSB7IGlmICghKGluc3RhbmNlIGluc3RhbmNlb2YgQ29uc3RydWN0b3IpKSB7IHRocm93IG5ldyBUeXBlRXJyb3IoXCJDYW5ub3QgY2FsbCBhIGNsYXNzIGFzIGEgZnVuY3Rpb25cIik7IH0gfVxuXG4vKipcbiAqIEhhbmRsZXMgZXZlcnl0aGluZyByZWxhdGVkIHRvIHRoZSBjb2xvcnBpY2tlciBjb2xvclxuICogQGlnbm9yZVxuICovXG52YXIgQ29sb3JIYW5kbGVyID0gZnVuY3Rpb24gKCkge1xuICAvKipcbiAgICogQHBhcmFtIHtDb2xvcnBpY2tlcn0gY29sb3JwaWNrZXJcbiAgICovXG4gIGZ1bmN0aW9uIENvbG9ySGFuZGxlcihjb2xvcnBpY2tlcikge1xuICAgIF9jbGFzc0NhbGxDaGVjayh0aGlzLCBDb2xvckhhbmRsZXIpO1xuXG4gICAgLyoqXG4gICAgICogQHR5cGUge0NvbG9ycGlja2VyfVxuICAgICAqL1xuICAgIHRoaXMuY29sb3JwaWNrZXIgPSBjb2xvcnBpY2tlcjtcbiAgfVxuXG4gIC8qKlxuICAgKiBAcmV0dXJucyB7KnxTdHJpbmd8Q29sb3JJdGVtfVxuICAgKi9cblxuXG4gIF9jcmVhdGVDbGFzcyhDb2xvckhhbmRsZXIsIFt7XG4gICAga2V5OiAnYmluZCcsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGJpbmQoKSB7XG4gICAgICAvLyBpZiB0aGUgY29sb3Igb3B0aW9uIGlzIHNldFxuICAgICAgaWYgKHRoaXMuY29sb3JwaWNrZXIub3B0aW9ucy5jb2xvcikge1xuICAgICAgICB0aGlzLmNvbG9yID0gdGhpcy5jcmVhdGVDb2xvcih0aGlzLmNvbG9ycGlja2VyLm9wdGlvbnMuY29sb3IpO1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG5cbiAgICAgIC8vIGlmIGVsZW1lbnRbY29sb3JdIGlzIGVtcHR5IGFuZCB0aGUgaW5wdXQgaGFzIGEgdmFsdWVcbiAgICAgIGlmICghdGhpcy5jb2xvciAmJiAhIXRoaXMuY29sb3JwaWNrZXIuaW5wdXRIYW5kbGVyLmdldFZhbHVlKCkpIHtcbiAgICAgICAgdGhpcy5jb2xvciA9IHRoaXMuY3JlYXRlQ29sb3IodGhpcy5jb2xvcnBpY2tlci5pbnB1dEhhbmRsZXIuZ2V0VmFsdWUoKSwgdGhpcy5jb2xvcnBpY2tlci5vcHRpb25zLmF1dG9JbnB1dEZhbGxiYWNrKTtcbiAgICAgIH1cbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICd1bmJpbmQnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiB1bmJpbmQoKSB7XG4gICAgICB0aGlzLmNvbG9ycGlja2VyLmVsZW1lbnQucmVtb3ZlRGF0YSgnY29sb3InKTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBSZXR1cm5zIHRoZSBjb2xvciBzdHJpbmcgZnJvbSB0aGUgaW5wdXQgdmFsdWUgb3IgdGhlICdkYXRhLWNvbG9yJyBhdHRyaWJ1dGUgb2YgdGhlIGlucHV0IG9yIGVsZW1lbnQuXG4gICAgICogSWYgZW1wdHksIGl0IHJldHVybnMgdGhlIGRlZmF1bHRWYWx1ZSBwYXJhbWV0ZXIuXG4gICAgICpcbiAgICAgKiBAcmV0dXJucyB7U3RyaW5nfCp9XG4gICAgICovXG5cbiAgfSwge1xuICAgIGtleTogJ2dldENvbG9yU3RyaW5nJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gZ2V0Q29sb3JTdHJpbmcoKSB7XG4gICAgICBpZiAoIXRoaXMuaGFzQ29sb3IoKSkge1xuICAgICAgICByZXR1cm4gJyc7XG4gICAgICB9XG5cbiAgICAgIHJldHVybiB0aGlzLmNvbG9yLnN0cmluZyh0aGlzLmZvcm1hdCk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogU2V0cyB0aGUgY29sb3IgdmFsdWVcbiAgICAgKlxuICAgICAqIEBwYXJhbSB7U3RyaW5nfENvbG9ySXRlbX0gdmFsXG4gICAgICovXG5cbiAgfSwge1xuICAgIGtleTogJ3NldENvbG9yU3RyaW5nJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gc2V0Q29sb3JTdHJpbmcodmFsKSB7XG4gICAgICB2YXIgY29sb3IgPSB2YWwgPyB0aGlzLmNyZWF0ZUNvbG9yKHZhbCkgOiBudWxsO1xuXG4gICAgICB0aGlzLmNvbG9yID0gY29sb3IgPyBjb2xvciA6IG51bGw7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogQ3JlYXRlcyBhIG5ldyBjb2xvciB1c2luZyB0aGUgd2lkZ2V0IGluc3RhbmNlIG9wdGlvbnMgKGZhbGxiYWNrQ29sb3IsIGZvcm1hdCkuXG4gICAgICpcbiAgICAgKiBAZmlyZXMgQ29sb3JwaWNrZXIjY29sb3JwaWNrZXJJbnZhbGlkXG4gICAgICogQHBhcmFtIHsqfSB2YWxcbiAgICAgKiBAcGFyYW0ge2Jvb2xlYW59IGZhbGxiYWNrT25JbnZhbGlkXG4gICAgICogQHJldHVybnMge0NvbG9ySXRlbX1cbiAgICAgKi9cblxuICB9LCB7XG4gICAga2V5OiAnY3JlYXRlQ29sb3InLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBjcmVhdGVDb2xvcih2YWwpIHtcbiAgICAgIHZhciBmYWxsYmFja09uSW52YWxpZCA9IGFyZ3VtZW50cy5sZW5ndGggPiAxICYmIGFyZ3VtZW50c1sxXSAhPT0gdW5kZWZpbmVkID8gYXJndW1lbnRzWzFdIDogdHJ1ZTtcblxuICAgICAgdmFyIGNvbG9yID0gbmV3IF9Db2xvckl0ZW0yLmRlZmF1bHQodGhpcy5yZXNvbHZlQ29sb3JEZWxlZ2F0ZSh2YWwpLCB0aGlzLmZvcm1hdCk7XG5cbiAgICAgIGlmICghY29sb3IuaXNWYWxpZCgpKSB7XG4gICAgICAgIGlmIChmYWxsYmFja09uSW52YWxpZCkge1xuICAgICAgICAgIGNvbG9yID0gdGhpcy5nZXRGYWxsYmFja0NvbG9yKCk7XG4gICAgICAgIH1cblxuICAgICAgICAvKipcbiAgICAgICAgICogKENvbG9ycGlja2VyKSBGaXJlZCB3aGVuIHRoZSBjb2xvciBpcyBpbnZhbGlkIGFuZCB0aGUgZmFsbGJhY2sgY29sb3IgaXMgZ29pbmcgdG8gYmUgdXNlZC5cbiAgICAgICAgICpcbiAgICAgICAgICogQGV2ZW50IENvbG9ycGlja2VyI2NvbG9ycGlja2VySW52YWxpZFxuICAgICAgICAgKi9cbiAgICAgICAgdGhpcy5jb2xvcnBpY2tlci50cmlnZ2VyKCdjb2xvcnBpY2tlckludmFsaWQnLCBjb2xvciwgdmFsKTtcbiAgICAgIH1cblxuICAgICAgaWYgKCF0aGlzLmlzQWxwaGFFbmFibGVkKCkpIHtcbiAgICAgICAgLy8gQWxwaGEgaXMgZGlzYWJsZWRcbiAgICAgICAgY29sb3IuYWxwaGEgPSAxO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gY29sb3I7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnZ2V0RmFsbGJhY2tDb2xvcicsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGdldEZhbGxiYWNrQ29sb3IoKSB7XG4gICAgICBpZiAodGhpcy5mYWxsYmFjayAmJiB0aGlzLmZhbGxiYWNrID09PSB0aGlzLmNvbG9yKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmNvbG9yO1xuICAgICAgfVxuXG4gICAgICB2YXIgZmFsbGJhY2sgPSB0aGlzLnJlc29sdmVDb2xvckRlbGVnYXRlKHRoaXMuZmFsbGJhY2spO1xuXG4gICAgICB2YXIgY29sb3IgPSBuZXcgX0NvbG9ySXRlbTIuZGVmYXVsdChmYWxsYmFjaywgdGhpcy5mb3JtYXQpO1xuXG4gICAgICBpZiAoIWNvbG9yLmlzVmFsaWQoKSkge1xuICAgICAgICBjb25zb2xlLndhcm4oJ1RoZSBmYWxsYmFjayBjb2xvciBpcyBpbnZhbGlkLiBGYWxsaW5nIGJhY2sgdG8gdGhlIHByZXZpb3VzIGNvbG9yIG9yIGJsYWNrIGlmIGFueS4nKTtcbiAgICAgICAgcmV0dXJuIHRoaXMuY29sb3IgPyB0aGlzLmNvbG9yIDogbmV3IF9Db2xvckl0ZW0yLmRlZmF1bHQoJyMwMDAwMDAnLCB0aGlzLmZvcm1hdCk7XG4gICAgICB9XG5cbiAgICAgIHJldHVybiBjb2xvcjtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBAcmV0dXJucyB7Q29sb3JJdGVtfVxuICAgICAqL1xuXG4gIH0sIHtcbiAgICBrZXk6ICdhc3N1cmVDb2xvcicsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGFzc3VyZUNvbG9yKCkge1xuICAgICAgaWYgKCF0aGlzLmhhc0NvbG9yKCkpIHtcbiAgICAgICAgdGhpcy5jb2xvciA9IHRoaXMuZ2V0RmFsbGJhY2tDb2xvcigpO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gdGhpcy5jb2xvcjtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBEZWxlZ2F0ZXMgdGhlIGNvbG9yIHJlc29sdXRpb24gdG8gdGhlIGNvbG9ycGlja2VyIGV4dGVuc2lvbnMuXG4gICAgICpcbiAgICAgKiBAcGFyYW0ge1N0cmluZ3wqfSBjb2xvclxuICAgICAqIEBwYXJhbSB7Ym9vbGVhbn0gcmVhbENvbG9yIGlmIHRydWUsIHRoZSBjb2xvciBzaG91bGQgcmVzb2x2ZSBpbnRvIGEgcmVhbCAobm90IG5hbWVkKSBjb2xvciBjb2RlXG4gICAgICogQHJldHVybnMge0NvbG9ySXRlbXxTdHJpbmd8KnxudWxsfVxuICAgICAqL1xuXG4gIH0sIHtcbiAgICBrZXk6ICdyZXNvbHZlQ29sb3JEZWxlZ2F0ZScsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHJlc29sdmVDb2xvckRlbGVnYXRlKGNvbG9yKSB7XG4gICAgICB2YXIgcmVhbENvbG9yID0gYXJndW1lbnRzLmxlbmd0aCA+IDEgJiYgYXJndW1lbnRzWzFdICE9PSB1bmRlZmluZWQgPyBhcmd1bWVudHNbMV0gOiB0cnVlO1xuXG4gICAgICB2YXIgZXh0UmVzb2x2ZWRDb2xvciA9IGZhbHNlO1xuXG4gICAgICBfanF1ZXJ5Mi5kZWZhdWx0LmVhY2godGhpcy5jb2xvcnBpY2tlci5leHRlbnNpb25zLCBmdW5jdGlvbiAobmFtZSwgZXh0KSB7XG4gICAgICAgIGlmIChleHRSZXNvbHZlZENvbG9yICE9PSBmYWxzZSkge1xuICAgICAgICAgIC8vIHNraXAgaWYgcmVzb2x2ZWRcbiAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cbiAgICAgICAgZXh0UmVzb2x2ZWRDb2xvciA9IGV4dC5yZXNvbHZlQ29sb3IoY29sb3IsIHJlYWxDb2xvcik7XG4gICAgICB9KTtcblxuICAgICAgcmV0dXJuIGV4dFJlc29sdmVkQ29sb3IgPyBleHRSZXNvbHZlZENvbG9yIDogY29sb3I7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogQ2hlY2tzIGlmIHRoZXJlIGlzIGEgY29sb3Igb2JqZWN0LCB0aGF0IGl0IGlzIHZhbGlkIGFuZCBpdCBpcyBub3QgYSBmYWxsYmFja1xuICAgICAqIEByZXR1cm5zIHtib29sZWFufVxuICAgICAqL1xuXG4gIH0sIHtcbiAgICBrZXk6ICdpc0ludmFsaWRDb2xvcicsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGlzSW52YWxpZENvbG9yKCkge1xuICAgICAgcmV0dXJuICF0aGlzLmhhc0NvbG9yKCkgfHwgIXRoaXMuY29sb3IuaXNWYWxpZCgpO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIFJldHVybnMgdHJ1ZSBpZiB0aGUgdXNlQWxwaGEgb3B0aW9uIGlzIGV4YWN0bHkgdHJ1ZSwgZmFsc2Ugb3RoZXJ3aXNlXG4gICAgICogQHJldHVybnMge2Jvb2xlYW59XG4gICAgICovXG5cbiAgfSwge1xuICAgIGtleTogJ2lzQWxwaGFFbmFibGVkJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gaXNBbHBoYUVuYWJsZWQoKSB7XG4gICAgICByZXR1cm4gdGhpcy5jb2xvcnBpY2tlci5vcHRpb25zLnVzZUFscGhhICE9PSBmYWxzZTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBSZXR1cm5zIHRydWUgaWYgdGhlIGN1cnJlbnQgY29sb3Igb2JqZWN0IGlzIGFuIGluc3RhbmNlIG9mIENvbG9yLCBmYWxzZSBvdGhlcndpc2UuXG4gICAgICogQHJldHVybnMge2Jvb2xlYW59XG4gICAgICovXG5cbiAgfSwge1xuICAgIGtleTogJ2hhc0NvbG9yJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gaGFzQ29sb3IoKSB7XG4gICAgICByZXR1cm4gdGhpcy5jb2xvciBpbnN0YW5jZW9mIF9Db2xvckl0ZW0yLmRlZmF1bHQ7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnZmFsbGJhY2snLFxuICAgIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgICAgcmV0dXJuIHRoaXMuY29sb3JwaWNrZXIub3B0aW9ucy5mYWxsYmFja0NvbG9yID8gdGhpcy5jb2xvcnBpY2tlci5vcHRpb25zLmZhbGxiYWNrQ29sb3IgOiB0aGlzLmhhc0NvbG9yKCkgPyB0aGlzLmNvbG9yIDogbnVsbDtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBAcmV0dXJucyB7U3RyaW5nfG51bGx9XG4gICAgICovXG5cbiAgfSwge1xuICAgIGtleTogJ2Zvcm1hdCcsXG4gICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgICBpZiAodGhpcy5jb2xvcnBpY2tlci5vcHRpb25zLmZvcm1hdCkge1xuICAgICAgICByZXR1cm4gdGhpcy5jb2xvcnBpY2tlci5vcHRpb25zLmZvcm1hdDtcbiAgICAgIH1cblxuICAgICAgaWYgKHRoaXMuaGFzQ29sb3IoKSAmJiB0aGlzLmNvbG9yLmhhc1RyYW5zcGFyZW5jeSgpICYmIHRoaXMuY29sb3IuZm9ybWF0Lm1hdGNoKC9eaGV4LykpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuaXNBbHBoYUVuYWJsZWQoKSA/ICdyZ2JhJyA6ICdoZXgnO1xuICAgICAgfVxuXG4gICAgICBpZiAodGhpcy5oYXNDb2xvcigpKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmNvbG9yLmZvcm1hdDtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuICdyZ2InO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEludGVybmFsIGNvbG9yIGdldHRlclxuICAgICAqXG4gICAgICogQHR5cGUge0NvbG9ySXRlbXxudWxsfVxuICAgICAqL1xuXG4gIH0sIHtcbiAgICBrZXk6ICdjb2xvcicsXG4gICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgICByZXR1cm4gdGhpcy5jb2xvcnBpY2tlci5lbGVtZW50LmRhdGEoJ2NvbG9yJyk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogSW50ZXJuYWwgY29sb3Igc2V0dGVyXG4gICAgICpcbiAgICAgKiBAaWdub3JlXG4gICAgICogQHBhcmFtIHtDb2xvckl0ZW18bnVsbH0gdmFsdWVcbiAgICAgKi9cbiAgICAsXG4gICAgc2V0OiBmdW5jdGlvbiBzZXQodmFsdWUpIHtcbiAgICAgIHRoaXMuY29sb3JwaWNrZXIuZWxlbWVudC5kYXRhKCdjb2xvcicsIHZhbHVlKTtcblxuICAgICAgaWYgKHZhbHVlIGluc3RhbmNlb2YgX0NvbG9ySXRlbTIuZGVmYXVsdCAmJiB0aGlzLmNvbG9ycGlja2VyLm9wdGlvbnMuZm9ybWF0ID09PSAnYXV0bycpIHtcbiAgICAgICAgLy8gSWYgZm9ybWF0IGlzICdhdXRvJywgdXNlIHRoZSBmaXJzdCBwYXJzZWQgb25lIGZyb20gbm93IG9uXG4gICAgICAgIHRoaXMuY29sb3JwaWNrZXIub3B0aW9ucy5mb3JtYXQgPSB0aGlzLmNvbG9yLmZvcm1hdDtcbiAgICAgIH1cbiAgICB9XG4gIH1dKTtcblxuICByZXR1cm4gQ29sb3JIYW5kbGVyO1xufSgpO1xuXG5leHBvcnRzLmRlZmF1bHQgPSBDb2xvckhhbmRsZXI7XG5tb2R1bGUuZXhwb3J0cyA9IGV4cG9ydHMuZGVmYXVsdDtcblxuLyoqKi8gfSksXG4vKiAyMyAqL1xuLyoqKi8gKGZ1bmN0aW9uKG1vZHVsZSwgZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXykge1xuXG5cInVzZSBzdHJpY3RcIjtcblxuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX2NyZWF0ZUNsYXNzID0gZnVuY3Rpb24gKCkgeyBmdW5jdGlvbiBkZWZpbmVQcm9wZXJ0aWVzKHRhcmdldCwgcHJvcHMpIHsgZm9yICh2YXIgaSA9IDA7IGkgPCBwcm9wcy5sZW5ndGg7IGkrKykgeyB2YXIgZGVzY3JpcHRvciA9IHByb3BzW2ldOyBkZXNjcmlwdG9yLmVudW1lcmFibGUgPSBkZXNjcmlwdG9yLmVudW1lcmFibGUgfHwgZmFsc2U7IGRlc2NyaXB0b3IuY29uZmlndXJhYmxlID0gdHJ1ZTsgaWYgKFwidmFsdWVcIiBpbiBkZXNjcmlwdG9yKSBkZXNjcmlwdG9yLndyaXRhYmxlID0gdHJ1ZTsgT2JqZWN0LmRlZmluZVByb3BlcnR5KHRhcmdldCwgZGVzY3JpcHRvci5rZXksIGRlc2NyaXB0b3IpOyB9IH0gcmV0dXJuIGZ1bmN0aW9uIChDb25zdHJ1Y3RvciwgcHJvdG9Qcm9wcywgc3RhdGljUHJvcHMpIHsgaWYgKHByb3RvUHJvcHMpIGRlZmluZVByb3BlcnRpZXMoQ29uc3RydWN0b3IucHJvdG90eXBlLCBwcm90b1Byb3BzKTsgaWYgKHN0YXRpY1Byb3BzKSBkZWZpbmVQcm9wZXJ0aWVzKENvbnN0cnVjdG9yLCBzdGF0aWNQcm9wcyk7IHJldHVybiBDb25zdHJ1Y3RvcjsgfTsgfSgpO1xuXG52YXIgX2pxdWVyeSA9IF9fd2VicGFja19yZXF1aXJlX18oMCk7XG5cbnZhciBfanF1ZXJ5MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2pxdWVyeSk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbmZ1bmN0aW9uIF9jbGFzc0NhbGxDaGVjayhpbnN0YW5jZSwgQ29uc3RydWN0b3IpIHsgaWYgKCEoaW5zdGFuY2UgaW5zdGFuY2VvZiBDb25zdHJ1Y3RvcikpIHsgdGhyb3cgbmV3IFR5cGVFcnJvcihcIkNhbm5vdCBjYWxsIGEgY2xhc3MgYXMgYSBmdW5jdGlvblwiKTsgfSB9XG5cbi8qKlxuICogSGFuZGxlcyBldmVyeXRoaW5nIHJlbGF0ZWQgdG8gdGhlIGNvbG9ycGlja2VyIFVJXG4gKiBAaWdub3JlXG4gKi9cbnZhciBQaWNrZXJIYW5kbGVyID0gZnVuY3Rpb24gKCkge1xuICAvKipcbiAgICogQHBhcmFtIHtDb2xvcnBpY2tlcn0gY29sb3JwaWNrZXJcbiAgICovXG4gIGZ1bmN0aW9uIFBpY2tlckhhbmRsZXIoY29sb3JwaWNrZXIpIHtcbiAgICBfY2xhc3NDYWxsQ2hlY2sodGhpcywgUGlja2VySGFuZGxlcik7XG5cbiAgICAvKipcbiAgICAgKiBAdHlwZSB7Q29sb3JwaWNrZXJ9XG4gICAgICovXG4gICAgdGhpcy5jb2xvcnBpY2tlciA9IGNvbG9ycGlja2VyO1xuICAgIC8qKlxuICAgICAqIEB0eXBlIHtqUXVlcnl9XG4gICAgICovXG4gICAgdGhpcy5waWNrZXIgPSBudWxsO1xuICB9XG5cbiAgX2NyZWF0ZUNsYXNzKFBpY2tlckhhbmRsZXIsIFt7XG4gICAga2V5OiAnYmluZCcsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGJpbmQoKSB7XG4gICAgICAvKipcbiAgICAgICAqIEB0eXBlIHtqUXVlcnl8SFRNTEVsZW1lbnR9XG4gICAgICAgKi9cbiAgICAgIHZhciBwaWNrZXIgPSB0aGlzLnBpY2tlciA9ICgwLCBfanF1ZXJ5Mi5kZWZhdWx0KSh0aGlzLm9wdGlvbnMudGVtcGxhdGUpO1xuXG4gICAgICBpZiAodGhpcy5vcHRpb25zLmN1c3RvbUNsYXNzKSB7XG4gICAgICAgIHBpY2tlci5hZGRDbGFzcyh0aGlzLm9wdGlvbnMuY3VzdG9tQ2xhc3MpO1xuICAgICAgfVxuXG4gICAgICBpZiAodGhpcy5vcHRpb25zLmhvcml6b250YWwpIHtcbiAgICAgICAgcGlja2VyLmFkZENsYXNzKCdjb2xvcnBpY2tlci1ob3Jpem9udGFsJyk7XG4gICAgICB9XG5cbiAgICAgIGlmICh0aGlzLl9zdXBwb3J0c0FscGhhQmFyKCkpIHtcbiAgICAgICAgdGhpcy5vcHRpb25zLnVzZUFscGhhID0gdHJ1ZTtcbiAgICAgICAgcGlja2VyLmFkZENsYXNzKCdjb2xvcnBpY2tlci13aXRoLWFscGhhJyk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICB0aGlzLm9wdGlvbnMudXNlQWxwaGEgPSBmYWxzZTtcbiAgICAgIH1cbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdhdHRhY2gnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBhdHRhY2goKSB7XG4gICAgICAvLyBJbmplY3QgdGhlIGNvbG9ycGlja2VyIGVsZW1lbnQgaW50byB0aGUgRE9NXG4gICAgICB2YXIgcGlja2VyUGFyZW50ID0gdGhpcy5jb2xvcnBpY2tlci5jb250YWluZXIgPyB0aGlzLmNvbG9ycGlja2VyLmNvbnRhaW5lciA6IG51bGw7XG5cbiAgICAgIGlmIChwaWNrZXJQYXJlbnQpIHtcbiAgICAgICAgdGhpcy5waWNrZXIuYXBwZW5kVG8ocGlja2VyUGFyZW50KTtcbiAgICAgIH1cbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICd1bmJpbmQnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiB1bmJpbmQoKSB7XG4gICAgICB0aGlzLnBpY2tlci5yZW1vdmUoKTtcbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdfc3VwcG9ydHNBbHBoYUJhcicsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIF9zdXBwb3J0c0FscGhhQmFyKCkge1xuICAgICAgcmV0dXJuICh0aGlzLm9wdGlvbnMudXNlQWxwaGEgfHwgdGhpcy5jb2xvcnBpY2tlci5jb2xvckhhbmRsZXIuaGFzQ29sb3IoKSAmJiB0aGlzLmNvbG9yLmhhc1RyYW5zcGFyZW5jeSgpKSAmJiB0aGlzLm9wdGlvbnMudXNlQWxwaGEgIT09IGZhbHNlICYmICghdGhpcy5vcHRpb25zLmZvcm1hdCB8fCB0aGlzLm9wdGlvbnMuZm9ybWF0ICYmICF0aGlzLm9wdGlvbnMuZm9ybWF0Lm1hdGNoKC9eaGV4KFszNl0pPyQvaSkpO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIENoYW5nZXMgdGhlIGNvbG9yIGFkanVzdG1lbnQgYmFycyB1c2luZyB0aGUgY3VycmVudCBjb2xvciBvYmplY3QgaW5mb3JtYXRpb24uXG4gICAgICovXG5cbiAgfSwge1xuICAgIGtleTogJ3VwZGF0ZScsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHVwZGF0ZSgpIHtcbiAgICAgIGlmICghdGhpcy5jb2xvcnBpY2tlci5jb2xvckhhbmRsZXIuaGFzQ29sb3IoKSkge1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG5cbiAgICAgIHZhciB2ZXJ0aWNhbCA9IHRoaXMub3B0aW9ucy5ob3Jpem9udGFsICE9PSB0cnVlLFxuICAgICAgICAgIHNsaWRlciA9IHZlcnRpY2FsID8gdGhpcy5vcHRpb25zLnNsaWRlcnMgOiB0aGlzLm9wdGlvbnMuc2xpZGVyc0hvcno7XG5cbiAgICAgIHZhciBzYXR1cmF0aW9uR3VpZGUgPSB0aGlzLnBpY2tlci5maW5kKCcuY29sb3JwaWNrZXItc2F0dXJhdGlvbiAuY29sb3JwaWNrZXItZ3VpZGUnKSxcbiAgICAgICAgICBodWVHdWlkZSA9IHRoaXMucGlja2VyLmZpbmQoJy5jb2xvcnBpY2tlci1odWUgLmNvbG9ycGlja2VyLWd1aWRlJyksXG4gICAgICAgICAgYWxwaGFHdWlkZSA9IHRoaXMucGlja2VyLmZpbmQoJy5jb2xvcnBpY2tlci1hbHBoYSAuY29sb3JwaWNrZXItZ3VpZGUnKTtcblxuICAgICAgdmFyIGhzdmEgPSB0aGlzLmNvbG9yLnRvSHN2YVJhdGlvKCk7XG5cbiAgICAgIC8vIFNldCBndWlkZXMgcG9zaXRpb25cbiAgICAgIGlmIChodWVHdWlkZS5sZW5ndGgpIHtcbiAgICAgICAgaHVlR3VpZGUuY3NzKHZlcnRpY2FsID8gJ3RvcCcgOiAnbGVmdCcsICh2ZXJ0aWNhbCA/IHNsaWRlci5odWUubWF4VG9wIDogc2xpZGVyLmh1ZS5tYXhMZWZ0KSAqICgxIC0gaHN2YS5oKSk7XG4gICAgICB9XG4gICAgICBpZiAoYWxwaGFHdWlkZS5sZW5ndGgpIHtcbiAgICAgICAgYWxwaGFHdWlkZS5jc3ModmVydGljYWwgPyAndG9wJyA6ICdsZWZ0JywgKHZlcnRpY2FsID8gc2xpZGVyLmFscGhhLm1heFRvcCA6IHNsaWRlci5hbHBoYS5tYXhMZWZ0KSAqICgxIC0gaHN2YS5hKSk7XG4gICAgICB9XG4gICAgICBpZiAoc2F0dXJhdGlvbkd1aWRlLmxlbmd0aCkge1xuICAgICAgICBzYXR1cmF0aW9uR3VpZGUuY3NzKHtcbiAgICAgICAgICAndG9wJzogc2xpZGVyLnNhdHVyYXRpb24ubWF4VG9wIC0gaHN2YS52ICogc2xpZGVyLnNhdHVyYXRpb24ubWF4VG9wLFxuICAgICAgICAgICdsZWZ0JzogaHN2YS5zICogc2xpZGVyLnNhdHVyYXRpb24ubWF4TGVmdFxuICAgICAgICB9KTtcbiAgICAgIH1cblxuICAgICAgLy8gU2V0IHNhdHVyYXRpb24gaHVlIGJhY2tncm91bmRcbiAgICAgIHRoaXMucGlja2VyLmZpbmQoJy5jb2xvcnBpY2tlci1zYXR1cmF0aW9uJykuY3NzKCdiYWNrZ3JvdW5kQ29sb3InLCB0aGlzLmNvbG9yLmdldENsb25lSHVlT25seSgpLnRvSGV4U3RyaW5nKCkpOyAvLyB3ZSBvbmx5IG5lZWQgaHVlXG5cbiAgICAgIC8vIFNldCBhbHBoYSBjb2xvciBncmFkaWVudFxuICAgICAgdmFyIGhleENvbG9yID0gdGhpcy5jb2xvci50b0hleFN0cmluZygpO1xuXG4gICAgICB2YXIgYWxwaGFCZyA9ICcnO1xuXG4gICAgICBpZiAodGhpcy5vcHRpb25zLmhvcml6b250YWwpIHtcbiAgICAgICAgYWxwaGFCZyA9ICdsaW5lYXItZ3JhZGllbnQodG8gcmlnaHQsICcgKyBoZXhDb2xvciArICcgMCUsIHRyYW5zcGFyZW50IDEwMCUpJztcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGFscGhhQmcgPSAnbGluZWFyLWdyYWRpZW50KHRvIGJvdHRvbSwgJyArIGhleENvbG9yICsgJyAwJSwgdHJhbnNwYXJlbnQgMTAwJSknO1xuICAgICAgfVxuXG4gICAgICB0aGlzLnBpY2tlci5maW5kKCcuY29sb3JwaWNrZXItYWxwaGEtY29sb3InKS5jc3MoJ2JhY2tncm91bmQnLCBhbHBoYUJnKTtcbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdvcHRpb25zJyxcbiAgICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICAgIHJldHVybiB0aGlzLmNvbG9ycGlja2VyLm9wdGlvbnM7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnY29sb3InLFxuICAgIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgICAgcmV0dXJuIHRoaXMuY29sb3JwaWNrZXIuY29sb3JIYW5kbGVyLmNvbG9yO1xuICAgIH1cbiAgfV0pO1xuXG4gIHJldHVybiBQaWNrZXJIYW5kbGVyO1xufSgpO1xuXG5leHBvcnRzLmRlZmF1bHQgPSBQaWNrZXJIYW5kbGVyO1xubW9kdWxlLmV4cG9ydHMgPSBleHBvcnRzLmRlZmF1bHQ7XG5cbi8qKiovIH0pLFxuLyogMjQgKi9cbi8qKiovIChmdW5jdGlvbihtb2R1bGUsIGV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pIHtcblxuXCJ1c2Ugc3RyaWN0XCI7XG5cblxuLyoqXG4gKiBIYW5kbGVzIGV2ZXJ5dGhpbmcgcmVsYXRlZCB0byB0aGUgY29sb3JwaWNrZXIgYWRkb25cbiAqIEBpZ25vcmVcbiAqL1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX2NyZWF0ZUNsYXNzID0gZnVuY3Rpb24gKCkgeyBmdW5jdGlvbiBkZWZpbmVQcm9wZXJ0aWVzKHRhcmdldCwgcHJvcHMpIHsgZm9yICh2YXIgaSA9IDA7IGkgPCBwcm9wcy5sZW5ndGg7IGkrKykgeyB2YXIgZGVzY3JpcHRvciA9IHByb3BzW2ldOyBkZXNjcmlwdG9yLmVudW1lcmFibGUgPSBkZXNjcmlwdG9yLmVudW1lcmFibGUgfHwgZmFsc2U7IGRlc2NyaXB0b3IuY29uZmlndXJhYmxlID0gdHJ1ZTsgaWYgKFwidmFsdWVcIiBpbiBkZXNjcmlwdG9yKSBkZXNjcmlwdG9yLndyaXRhYmxlID0gdHJ1ZTsgT2JqZWN0LmRlZmluZVByb3BlcnR5KHRhcmdldCwgZGVzY3JpcHRvci5rZXksIGRlc2NyaXB0b3IpOyB9IH0gcmV0dXJuIGZ1bmN0aW9uIChDb25zdHJ1Y3RvciwgcHJvdG9Qcm9wcywgc3RhdGljUHJvcHMpIHsgaWYgKHByb3RvUHJvcHMpIGRlZmluZVByb3BlcnRpZXMoQ29uc3RydWN0b3IucHJvdG90eXBlLCBwcm90b1Byb3BzKTsgaWYgKHN0YXRpY1Byb3BzKSBkZWZpbmVQcm9wZXJ0aWVzKENvbnN0cnVjdG9yLCBzdGF0aWNQcm9wcyk7IHJldHVybiBDb25zdHJ1Y3RvcjsgfTsgfSgpO1xuXG5mdW5jdGlvbiBfY2xhc3NDYWxsQ2hlY2soaW5zdGFuY2UsIENvbnN0cnVjdG9yKSB7IGlmICghKGluc3RhbmNlIGluc3RhbmNlb2YgQ29uc3RydWN0b3IpKSB7IHRocm93IG5ldyBUeXBlRXJyb3IoXCJDYW5ub3QgY2FsbCBhIGNsYXNzIGFzIGEgZnVuY3Rpb25cIik7IH0gfVxuXG52YXIgQWRkb25IYW5kbGVyID0gZnVuY3Rpb24gKCkge1xuICAvKipcbiAgICogQHBhcmFtIHtDb2xvcnBpY2tlcn0gY29sb3JwaWNrZXJcbiAgICovXG4gIGZ1bmN0aW9uIEFkZG9uSGFuZGxlcihjb2xvcnBpY2tlcikge1xuICAgIF9jbGFzc0NhbGxDaGVjayh0aGlzLCBBZGRvbkhhbmRsZXIpO1xuXG4gICAgLyoqXG4gICAgICogQHR5cGUge0NvbG9ycGlja2VyfVxuICAgICAqL1xuICAgIHRoaXMuY29sb3JwaWNrZXIgPSBjb2xvcnBpY2tlcjtcbiAgICAvKipcbiAgICAgKiBAdHlwZSB7alF1ZXJ5fVxuICAgICAqL1xuICAgIHRoaXMuYWRkb24gPSBudWxsO1xuICB9XG5cbiAgX2NyZWF0ZUNsYXNzKEFkZG9uSGFuZGxlciwgW3tcbiAgICBrZXk6ICdoYXNBZGRvbicsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGhhc0FkZG9uKCkge1xuICAgICAgcmV0dXJuICEhdGhpcy5hZGRvbjtcbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdiaW5kJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gYmluZCgpIHtcbiAgICAgIC8qKlxuICAgICAgICogQHR5cGUgeyp8alF1ZXJ5fVxuICAgICAgICovXG4gICAgICB0aGlzLmFkZG9uID0gdGhpcy5jb2xvcnBpY2tlci5vcHRpb25zLmFkZG9uID8gdGhpcy5jb2xvcnBpY2tlci5lbGVtZW50LmZpbmQodGhpcy5jb2xvcnBpY2tlci5vcHRpb25zLmFkZG9uKSA6IG51bGw7XG5cbiAgICAgIGlmICh0aGlzLmFkZG9uICYmIHRoaXMuYWRkb24ubGVuZ3RoID09PSAwKSB7XG4gICAgICAgIC8vIG5vdCBmb3VuZFxuICAgICAgICB0aGlzLmFkZG9uID0gbnVsbDtcbiAgICAgIH1cbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICd1bmJpbmQnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiB1bmJpbmQoKSB7XG4gICAgICBpZiAodGhpcy5oYXNBZGRvbigpKSB7XG4gICAgICAgIHRoaXMuYWRkb24ub2ZmKCcuY29sb3JwaWNrZXInKTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBJZiB0aGUgYWRkb24gZWxlbWVudCBpcyBwcmVzZW50LCBpdHMgYmFja2dyb3VuZCBjb2xvciBpcyB1cGRhdGVkXG4gICAgICovXG5cbiAgfSwge1xuICAgIGtleTogJ3VwZGF0ZScsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHVwZGF0ZSgpIHtcbiAgICAgIGlmICghdGhpcy5jb2xvcnBpY2tlci5jb2xvckhhbmRsZXIuaGFzQ29sb3IoKSB8fCAhdGhpcy5oYXNBZGRvbigpKSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cblxuICAgICAgdmFyIGNvbG9yU3RyID0gdGhpcy5jb2xvcnBpY2tlci5jb2xvckhhbmRsZXIuZ2V0Q29sb3JTdHJpbmcoKTtcblxuICAgICAgdmFyIHN0eWxlcyA9IHsgJ2JhY2tncm91bmQnOiBjb2xvclN0ciB9O1xuXG4gICAgICB2YXIgaWNuID0gdGhpcy5hZGRvbi5maW5kKCdpJykuZXEoMCk7XG5cbiAgICAgIGlmIChpY24ubGVuZ3RoID4gMCkge1xuICAgICAgICBpY24uY3NzKHN0eWxlcyk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICB0aGlzLmFkZG9uLmNzcyhzdHlsZXMpO1xuICAgICAgfVxuICAgIH1cbiAgfV0pO1xuXG4gIHJldHVybiBBZGRvbkhhbmRsZXI7XG59KCk7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IEFkZG9uSGFuZGxlcjtcbm1vZHVsZS5leHBvcnRzID0gZXhwb3J0cy5kZWZhdWx0O1xuXG4vKioqLyB9KVxuLyoqKioqKi8gXSk7XG59KTtcbi8vIyBzb3VyY2VNYXBwaW5nVVJMPWJvb3RzdHJhcC1jb2xvcnBpY2tlci5qcy5tYXBcbn0uY2FsbCh3aW5kb3csIGZhbHNlLCBmYWxzZSkpO1xuIiwibW9kdWxlLmV4cG9ydHMgPSB3aW5kb3dbXCJqUXVlcnlcIl07IiwiLy8gVGhlIG1vZHVsZSBjYWNoZVxudmFyIF9fd2VicGFja19tb2R1bGVfY2FjaGVfXyA9IHt9O1xuXG4vLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcblx0dmFyIGNhY2hlZE1vZHVsZSA9IF9fd2VicGFja19tb2R1bGVfY2FjaGVfX1ttb2R1bGVJZF07XG5cdGlmIChjYWNoZWRNb2R1bGUgIT09IHVuZGVmaW5lZCkge1xuXHRcdHJldHVybiBjYWNoZWRNb2R1bGUuZXhwb3J0cztcblx0fVxuXHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuXHR2YXIgbW9kdWxlID0gX193ZWJwYWNrX21vZHVsZV9jYWNoZV9fW21vZHVsZUlkXSA9IHtcblx0XHQvLyBubyBtb2R1bGUuaWQgbmVlZGVkXG5cdFx0Ly8gbm8gbW9kdWxlLmxvYWRlZCBuZWVkZWRcblx0XHRleHBvcnRzOiB7fVxuXHR9O1xuXG5cdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuXHRfX3dlYnBhY2tfbW9kdWxlc19fW21vZHVsZUlkXShtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuXHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuXHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG59XG5cbiIsIi8qKlxuICogQ29weXJpZ2h0IHNpbmNlIDIwMDcgUHJlc3RhU2hvcCBTQSBhbmQgQ29udHJpYnV0b3JzXG4gKiBQcmVzdGFTaG9wIGlzIGFuIEludGVybmF0aW9uYWwgUmVnaXN0ZXJlZCBUcmFkZW1hcmsgJiBQcm9wZXJ0eSBvZiBQcmVzdGFTaG9wIFNBXG4gKlxuICogTk9USUNFIE9GIExJQ0VOU0VcbiAqXG4gKiBUaGlzIHNvdXJjZSBmaWxlIGlzIHN1YmplY3QgdG8gdGhlIE9wZW4gU29mdHdhcmUgTGljZW5zZSAoT1NMIDMuMClcbiAqIHRoYXQgaXMgYnVuZGxlZCB3aXRoIHRoaXMgcGFja2FnZSBpbiB0aGUgZmlsZSBMSUNFTlNFLm1kLlxuICogSXQgaXMgYWxzbyBhdmFpbGFibGUgdGhyb3VnaCB0aGUgd29ybGQtd2lkZS13ZWIgYXQgdGhpcyBVUkw6XG4gKiBodHRwczovL29wZW5zb3VyY2Uub3JnL2xpY2Vuc2VzL09TTC0zLjBcbiAqIElmIHlvdSBkaWQgbm90IHJlY2VpdmUgYSBjb3B5IG9mIHRoZSBsaWNlbnNlIGFuZCBhcmUgdW5hYmxlIHRvXG4gKiBvYnRhaW4gaXQgdGhyb3VnaCB0aGUgd29ybGQtd2lkZS13ZWIsIHBsZWFzZSBzZW5kIGFuIGVtYWlsXG4gKiB0byBsaWNlbnNlQHByZXN0YXNob3AuY29tIHNvIHdlIGNhbiBzZW5kIHlvdSBhIGNvcHkgaW1tZWRpYXRlbHkuXG4gKlxuICogRElTQ0xBSU1FUlxuICpcbiAqIERvIG5vdCBlZGl0IG9yIGFkZCB0byB0aGlzIGZpbGUgaWYgeW91IHdpc2ggdG8gdXBncmFkZSBQcmVzdGFTaG9wIHRvIG5ld2VyXG4gKiB2ZXJzaW9ucyBpbiB0aGUgZnV0dXJlLiBJZiB5b3Ugd2lzaCB0byBjdXN0b21pemUgUHJlc3RhU2hvcCBmb3IgeW91clxuICogbmVlZHMgcGxlYXNlIHJlZmVyIHRvIGh0dHBzOi8vZGV2ZG9jcy5wcmVzdGFzaG9wLmNvbS8gZm9yIG1vcmUgaW5mb3JtYXRpb24uXG4gKlxuICogQGF1dGhvciAgICBQcmVzdGFTaG9wIFNBIGFuZCBDb250cmlidXRvcnMgPGNvbnRhY3RAcHJlc3Rhc2hvcC5jb20+XG4gKiBAY29weXJpZ2h0IFNpbmNlIDIwMDcgUHJlc3RhU2hvcCBTQSBhbmQgQ29udHJpYnV0b3JzXG4gKiBAbGljZW5zZSAgIGh0dHBzOi8vb3BlbnNvdXJjZS5vcmcvbGljZW5zZXMvT1NMLTMuMCBPcGVuIFNvZnR3YXJlIExpY2Vuc2UgKE9TTCAzLjApXG4gKi9cblxuaW1wb3J0IGluaXRDb2xvclBpY2tlcnMgZnJvbSAnQGFwcC91dGlscy9jb2xvcnBpY2tlcic7XG5pbXBvcnQgVHJhbnNsYXRhYmxlSW5wdXQgZnJvbSAnQGNvbXBvbmVudHMvdHJhbnNsYXRhYmxlLWlucHV0JztcblxuY29uc3QgeyR9ID0gd2luZG93O1xuXG4kKCgpID0+IHtcbiAgaW5pdENvbG9yUGlja2VycygpO1xuICBuZXcgVHJhbnNsYXRhYmxlSW5wdXQoKTtcbn0pO1xuIl0sInNvdXJjZVJvb3QiOiIifQ==