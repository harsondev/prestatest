/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./js/app/utils/colorpicker.js":
/*!*************************************!*\
  !*** ./js/app/utils/colorpicker.js ***!
  \*************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

"use strict";


Object.defineProperty(exports, "__esModule", ({
  value: true
}));

__webpack_require__(/*! bootstrap-colorpicker */ "./node_modules/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.js");

var _window = window,
    $ = _window.$;

/**
 * Enable all colorpickers.
 */
/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 */

var init = function initDatePickers() {
  $('.colorpicker input[type="text"]').each(function (i, picker) {
    $(picker).colorpicker();
  });
};

exports.default = init;

/***/ }),

/***/ "./js/components/event-emitter.js":
/*!****************************************!*\
  !*** ./js/components/event-emitter.js ***!
  \****************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

"use strict";


Object.defineProperty(exports, "__esModule", ({
  value: true
}));
exports.EventEmitter = undefined;

var _events = __webpack_require__(/*! events */ "./node_modules/events/events.js");

var _events2 = _interopRequireDefault(_events);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * We instanciate one EventEmitter (restricted via a const) so that every components
 * register/dispatch on the same one and can communicate with each other.
 */
var EventEmitter = exports.EventEmitter = new _events2.default(); /**
                                                                   * Copyright since 2007 PrestaShop SA and Contributors
                                                                   * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
                                                                   *
                                                                   * NOTICE OF LICENSE
                                                                   *
                                                                   * This source file is subject to the Open Software License (OSL 3.0)
                                                                   * that is bundled with this package in the file LICENSE.md.
                                                                   * It is also available through the world-wide-web at this URL:
                                                                   * https://opensource.org/licenses/OSL-3.0
                                                                   * If you did not receive a copy of the license and are unable to
                                                                   * obtain it through the world-wide-web, please send an email
                                                                   * to license@prestashop.com so we can send you a copy immediately.
                                                                   *
                                                                   * DISCLAIMER
                                                                   *
                                                                   * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
                                                                   * versions in the future. If you wish to customize PrestaShop for your
                                                                   * needs please refer to https://devdocs.prestashop.com/ for more information.
                                                                   *
                                                                   * @author    PrestaShop SA and Contributors <contact@prestashop.com>
                                                                   * @copyright Since 2007 PrestaShop SA and Contributors
                                                                   * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
                                                                   */

exports.default = EventEmitter;

/***/ }),

/***/ "./js/components/form/translatable-choice.js":
/*!***************************************************!*\
  !*** ./js/components/form/translatable-choice.js ***!
  \***************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

"use strict";


Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 */

var _window = window,
    $ = _window.$;

/**
 * Component responsible for filtering select values by language selected.
 */

var TranslatableChoice = function () {
  function TranslatableChoice() {
    var _this = this;

    (0, _classCallCheck3.default)(this, TranslatableChoice);

    // registers the event which displays the popover
    $(document).on('change', 'select.translatable_choice_language', function (event) {
      _this.filterSelect(event);
    });

    $('select.translatable_choice_language').trigger('change');
  }

  (0, _createClass3.default)(TranslatableChoice, [{
    key: 'filterSelect',
    value: function filterSelect(event) {
      var $element = $(event.currentTarget);
      var $formGroup = $element.closest('.form-group');
      var language = $element.find('option:selected').val();

      // show all the languages selects
      $formGroup.find('select.translatable_choice[data-language="' + language + '"]').parent().show();

      var $selects = $formGroup.find('select.translatable_choice');

      // Hide all the selects not corresponding to the language selected
      $selects.not('select.translatable_choice[data-language="' + language + '"]').each(function (index, item) {
        $(item).parent().hide();
      });

      // Bind choice selection to fill the hidden input
      this.bindValueSelection($selects);
    }
  }, {
    key: 'bindValueSelection',
    value: function bindValueSelection($selects) {
      $selects.each(function (index, element) {
        $(element).on('change', function (event) {
          var $select = $(event.currentTarget);
          var selectId = $select.attr('id');
          $('#' + selectId + '_value').val($select.find('option:selected').val());
        });
      });
    }
  }]);
  return TranslatableChoice;
}();

exports.default = TranslatableChoice;

/***/ }),

/***/ "./js/components/translatable-input.js":
/*!*********************************************!*\
  !*** ./js/components/translatable-input.js ***!
  \*********************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

"use strict";


Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _eventEmitter = __webpack_require__(/*! ./event-emitter */ "./js/components/event-emitter.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _window = window,
    $ = _window.$;

/**
 * This class is used to automatically toggle translated inputs (displayed with one
 * input and a language selector using the TranslatableType Symfony form type).
 * Also compatible with TranslatableField changes.
 */
/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 */

var TranslatableInput = function () {
  function TranslatableInput(options) {
    var _this = this;

    (0, _classCallCheck3.default)(this, TranslatableInput);

    var opts = options || {};

    this.localeItemSelector = opts.localeItemSelector || '.js-locale-item';
    this.localeButtonSelector = opts.localeButtonSelector || '.js-locale-btn';
    this.localeInputSelector = opts.localeInputSelector || '.js-locale-input';
    this.selectedLocale = $(this.localeItemSelector).data('locale');

    $('body').on('click', this.localeItemSelector, this.toggleLanguage.bind(this));
    _eventEmitter.EventEmitter.on('languageSelected', this.toggleInputs.bind(this));

    return {
      localeItemSelector: this.localeItemSelector,
      localeButtonSelector: this.localeButtonSelector,
      localeInputSelector: this.localeInputSelector,

      /**
       * @param {jQuery} form
       */
      refreshFormInputs: function refreshFormInputs(form) {
        _this.refreshInputs(form);
      },

      /**
       * @returns {string|undefined}
       */
      getSelectedLocale: function getSelectedLocale() {
        return _this.selectedLocale;
      }
    };
  }

  /**
   * @param {jQuery} form
   *
   * @private
   */


  (0, _createClass3.default)(TranslatableInput, [{
    key: 'refreshInputs',
    value: function refreshInputs(form) {
      if (!this.selectedLocale) {
        return;
      }

      _eventEmitter.EventEmitter.emit('languageSelected', {
        selectedLocale: this.selectedLocale,
        form: form
      });
    }

    /**
     * Dispatch event on language selection to update inputs and other components which depend on the locale.
     *
     * @param event
     *
     * @private
     */

  }, {
    key: 'toggleLanguage',
    value: function toggleLanguage(event) {
      var localeItem = $(event.target);
      var form = localeItem.closest('form');
      this.selectedLocale = localeItem.data('locale');
      this.refreshInputs(form);
    }

    /**
     * Toggle all translatable inputs in form in which locale was changed
     *
     * @param {Event} event
     *
     * @private
     */

  }, {
    key: 'toggleInputs',
    value: function toggleInputs(event) {
      var form = event.form;

      this.selectedLocale = event.selectedLocale;
      var localeButton = form.find(this.localeButtonSelector);
      var changeLanguageUrl = localeButton.data('change-language-url');

      localeButton.text(this.selectedLocale);
      form.find(this.localeInputSelector).addClass('d-none');
      form.find(this.localeInputSelector + '.js-locale-' + this.selectedLocale).removeClass('d-none');

      if (changeLanguageUrl) {
        this.saveSelectedLanguage(changeLanguageUrl, this.selectedLocale);
      }
    }

    /**
     * Save language choice for employee forms.
     *
     * @param {String} changeLanguageUrl
     * @param {String} selectedLocale
     *
     * @private
     */

  }, {
    key: 'saveSelectedLanguage',
    value: function saveSelectedLanguage(changeLanguageUrl, selectedLocale) {
      $.post({
        url: changeLanguageUrl,
        data: {
          language_iso_code: selectedLocale
        }
      });
    }
  }]);
  return TranslatableInput;
}();

exports.default = TranslatableInput;

/***/ }),

/***/ "./js/pages/order-states/form-map.js":
/*!*******************************************!*\
  !*** ./js/pages/order-states/form-map.js ***!
  \*******************************************/
/***/ ((__unused_webpack_module, exports) => {

"use strict";


Object.defineProperty(exports, "__esModule", ({
  value: true
}));
/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 */

/**
 * Defines all selectors that are used in currency add/edit form.
 */
exports.default = {
  sendEmailSelector: '#order_state_send_email',
  mailTemplateSelector: '.order_state_template_select',
  mailTemplatePreview: '#order_state_template_preview'
};

/***/ }),

/***/ "./node_modules/babel-runtime/core-js/object/define-property.js":
/*!**********************************************************************!*\
  !*** ./node_modules/babel-runtime/core-js/object/define-property.js ***!
  \**********************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

module.exports = { "default": __webpack_require__(/*! core-js/library/fn/object/define-property */ "./node_modules/core-js/library/fn/object/define-property.js"), __esModule: true };

/***/ }),

/***/ "./node_modules/babel-runtime/helpers/classCallCheck.js":
/*!**************************************************************!*\
  !*** ./node_modules/babel-runtime/helpers/classCallCheck.js ***!
  \**************************************************************/
/***/ ((__unused_webpack_module, exports) => {

"use strict";


exports.__esModule = true;

exports.default = function (instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
};

/***/ }),

/***/ "./node_modules/babel-runtime/helpers/createClass.js":
/*!***********************************************************!*\
  !*** ./node_modules/babel-runtime/helpers/createClass.js ***!
  \***********************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

"use strict";


exports.__esModule = true;

var _defineProperty = __webpack_require__(/*! ../core-js/object/define-property */ "./node_modules/babel-runtime/core-js/object/define-property.js");

var _defineProperty2 = _interopRequireDefault(_defineProperty);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      (0, _defineProperty2.default)(target, descriptor.key, descriptor);
    }
  }

  return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);
    if (staticProps) defineProperties(Constructor, staticProps);
    return Constructor;
  };
}();

/***/ }),

/***/ "./node_modules/core-js/library/fn/object/define-property.js":
/*!*******************************************************************!*\
  !*** ./node_modules/core-js/library/fn/object/define-property.js ***!
  \*******************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

__webpack_require__(/*! ../../modules/es6.object.define-property */ "./node_modules/core-js/library/modules/es6.object.define-property.js");
var $Object = __webpack_require__(/*! ../../modules/_core */ "./node_modules/core-js/library/modules/_core.js").Object;
module.exports = function defineProperty(it, key, desc) {
  return $Object.defineProperty(it, key, desc);
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_a-function.js":
/*!*************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_a-function.js ***!
  \*************************************************************/
/***/ ((module) => {

module.exports = function (it) {
  if (typeof it != 'function') throw TypeError(it + ' is not a function!');
  return it;
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_an-object.js":
/*!************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_an-object.js ***!
  \************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var isObject = __webpack_require__(/*! ./_is-object */ "./node_modules/core-js/library/modules/_is-object.js");
module.exports = function (it) {
  if (!isObject(it)) throw TypeError(it + ' is not an object!');
  return it;
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_core.js":
/*!*******************************************************!*\
  !*** ./node_modules/core-js/library/modules/_core.js ***!
  \*******************************************************/
/***/ ((module) => {

var core = module.exports = { version: '2.6.11' };
if (typeof __e == 'number') __e = core; // eslint-disable-line no-undef


/***/ }),

/***/ "./node_modules/core-js/library/modules/_ctx.js":
/*!******************************************************!*\
  !*** ./node_modules/core-js/library/modules/_ctx.js ***!
  \******************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// optional / simple context binding
var aFunction = __webpack_require__(/*! ./_a-function */ "./node_modules/core-js/library/modules/_a-function.js");
module.exports = function (fn, that, length) {
  aFunction(fn);
  if (that === undefined) return fn;
  switch (length) {
    case 1: return function (a) {
      return fn.call(that, a);
    };
    case 2: return function (a, b) {
      return fn.call(that, a, b);
    };
    case 3: return function (a, b, c) {
      return fn.call(that, a, b, c);
    };
  }
  return function (/* ...args */) {
    return fn.apply(that, arguments);
  };
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_descriptors.js":
/*!**************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_descriptors.js ***!
  \**************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// Thank's IE8 for his funny defineProperty
module.exports = !__webpack_require__(/*! ./_fails */ "./node_modules/core-js/library/modules/_fails.js")(function () {
  return Object.defineProperty({}, 'a', { get: function () { return 7; } }).a != 7;
});


/***/ }),

/***/ "./node_modules/core-js/library/modules/_dom-create.js":
/*!*************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_dom-create.js ***!
  \*************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var isObject = __webpack_require__(/*! ./_is-object */ "./node_modules/core-js/library/modules/_is-object.js");
var document = __webpack_require__(/*! ./_global */ "./node_modules/core-js/library/modules/_global.js").document;
// typeof document.createElement is 'object' in old IE
var is = isObject(document) && isObject(document.createElement);
module.exports = function (it) {
  return is ? document.createElement(it) : {};
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_export.js":
/*!*********************************************************!*\
  !*** ./node_modules/core-js/library/modules/_export.js ***!
  \*********************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var global = __webpack_require__(/*! ./_global */ "./node_modules/core-js/library/modules/_global.js");
var core = __webpack_require__(/*! ./_core */ "./node_modules/core-js/library/modules/_core.js");
var ctx = __webpack_require__(/*! ./_ctx */ "./node_modules/core-js/library/modules/_ctx.js");
var hide = __webpack_require__(/*! ./_hide */ "./node_modules/core-js/library/modules/_hide.js");
var has = __webpack_require__(/*! ./_has */ "./node_modules/core-js/library/modules/_has.js");
var PROTOTYPE = 'prototype';

var $export = function (type, name, source) {
  var IS_FORCED = type & $export.F;
  var IS_GLOBAL = type & $export.G;
  var IS_STATIC = type & $export.S;
  var IS_PROTO = type & $export.P;
  var IS_BIND = type & $export.B;
  var IS_WRAP = type & $export.W;
  var exports = IS_GLOBAL ? core : core[name] || (core[name] = {});
  var expProto = exports[PROTOTYPE];
  var target = IS_GLOBAL ? global : IS_STATIC ? global[name] : (global[name] || {})[PROTOTYPE];
  var key, own, out;
  if (IS_GLOBAL) source = name;
  for (key in source) {
    // contains in native
    own = !IS_FORCED && target && target[key] !== undefined;
    if (own && has(exports, key)) continue;
    // export native or passed
    out = own ? target[key] : source[key];
    // prevent global pollution for namespaces
    exports[key] = IS_GLOBAL && typeof target[key] != 'function' ? source[key]
    // bind timers to global for call from export context
    : IS_BIND && own ? ctx(out, global)
    // wrap global constructors for prevent change them in library
    : IS_WRAP && target[key] == out ? (function (C) {
      var F = function (a, b, c) {
        if (this instanceof C) {
          switch (arguments.length) {
            case 0: return new C();
            case 1: return new C(a);
            case 2: return new C(a, b);
          } return new C(a, b, c);
        } return C.apply(this, arguments);
      };
      F[PROTOTYPE] = C[PROTOTYPE];
      return F;
    // make static versions for prototype methods
    })(out) : IS_PROTO && typeof out == 'function' ? ctx(Function.call, out) : out;
    // export proto methods to core.%CONSTRUCTOR%.methods.%NAME%
    if (IS_PROTO) {
      (exports.virtual || (exports.virtual = {}))[key] = out;
      // export proto methods to core.%CONSTRUCTOR%.prototype.%NAME%
      if (type & $export.R && expProto && !expProto[key]) hide(expProto, key, out);
    }
  }
};
// type bitmap
$export.F = 1;   // forced
$export.G = 2;   // global
$export.S = 4;   // static
$export.P = 8;   // proto
$export.B = 16;  // bind
$export.W = 32;  // wrap
$export.U = 64;  // safe
$export.R = 128; // real proto method for `library`
module.exports = $export;


/***/ }),

/***/ "./node_modules/core-js/library/modules/_fails.js":
/*!********************************************************!*\
  !*** ./node_modules/core-js/library/modules/_fails.js ***!
  \********************************************************/
/***/ ((module) => {

module.exports = function (exec) {
  try {
    return !!exec();
  } catch (e) {
    return true;
  }
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_global.js":
/*!*********************************************************!*\
  !*** ./node_modules/core-js/library/modules/_global.js ***!
  \*********************************************************/
/***/ ((module) => {

// https://github.com/zloirock/core-js/issues/86#issuecomment-115759028
var global = module.exports = typeof window != 'undefined' && window.Math == Math
  ? window : typeof self != 'undefined' && self.Math == Math ? self
  // eslint-disable-next-line no-new-func
  : Function('return this')();
if (typeof __g == 'number') __g = global; // eslint-disable-line no-undef


/***/ }),

/***/ "./node_modules/core-js/library/modules/_has.js":
/*!******************************************************!*\
  !*** ./node_modules/core-js/library/modules/_has.js ***!
  \******************************************************/
/***/ ((module) => {

var hasOwnProperty = {}.hasOwnProperty;
module.exports = function (it, key) {
  return hasOwnProperty.call(it, key);
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_hide.js":
/*!*******************************************************!*\
  !*** ./node_modules/core-js/library/modules/_hide.js ***!
  \*******************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var dP = __webpack_require__(/*! ./_object-dp */ "./node_modules/core-js/library/modules/_object-dp.js");
var createDesc = __webpack_require__(/*! ./_property-desc */ "./node_modules/core-js/library/modules/_property-desc.js");
module.exports = __webpack_require__(/*! ./_descriptors */ "./node_modules/core-js/library/modules/_descriptors.js") ? function (object, key, value) {
  return dP.f(object, key, createDesc(1, value));
} : function (object, key, value) {
  object[key] = value;
  return object;
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_ie8-dom-define.js":
/*!*****************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_ie8-dom-define.js ***!
  \*****************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

module.exports = !__webpack_require__(/*! ./_descriptors */ "./node_modules/core-js/library/modules/_descriptors.js") && !__webpack_require__(/*! ./_fails */ "./node_modules/core-js/library/modules/_fails.js")(function () {
  return Object.defineProperty(__webpack_require__(/*! ./_dom-create */ "./node_modules/core-js/library/modules/_dom-create.js")('div'), 'a', { get: function () { return 7; } }).a != 7;
});


/***/ }),

/***/ "./node_modules/core-js/library/modules/_is-object.js":
/*!************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_is-object.js ***!
  \************************************************************/
/***/ ((module) => {

module.exports = function (it) {
  return typeof it === 'object' ? it !== null : typeof it === 'function';
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_object-dp.js":
/*!************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_object-dp.js ***!
  \************************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

var anObject = __webpack_require__(/*! ./_an-object */ "./node_modules/core-js/library/modules/_an-object.js");
var IE8_DOM_DEFINE = __webpack_require__(/*! ./_ie8-dom-define */ "./node_modules/core-js/library/modules/_ie8-dom-define.js");
var toPrimitive = __webpack_require__(/*! ./_to-primitive */ "./node_modules/core-js/library/modules/_to-primitive.js");
var dP = Object.defineProperty;

exports.f = __webpack_require__(/*! ./_descriptors */ "./node_modules/core-js/library/modules/_descriptors.js") ? Object.defineProperty : function defineProperty(O, P, Attributes) {
  anObject(O);
  P = toPrimitive(P, true);
  anObject(Attributes);
  if (IE8_DOM_DEFINE) try {
    return dP(O, P, Attributes);
  } catch (e) { /* empty */ }
  if ('get' in Attributes || 'set' in Attributes) throw TypeError('Accessors not supported!');
  if ('value' in Attributes) O[P] = Attributes.value;
  return O;
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_property-desc.js":
/*!****************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_property-desc.js ***!
  \****************************************************************/
/***/ ((module) => {

module.exports = function (bitmap, value) {
  return {
    enumerable: !(bitmap & 1),
    configurable: !(bitmap & 2),
    writable: !(bitmap & 4),
    value: value
  };
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_to-primitive.js":
/*!***************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_to-primitive.js ***!
  \***************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// 7.1.1 ToPrimitive(input [, PreferredType])
var isObject = __webpack_require__(/*! ./_is-object */ "./node_modules/core-js/library/modules/_is-object.js");
// instead of the ES6 spec version, we didn't implement @@toPrimitive case
// and the second argument - flag - preferred type is a string
module.exports = function (it, S) {
  if (!isObject(it)) return it;
  var fn, val;
  if (S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;
  if (typeof (fn = it.valueOf) == 'function' && !isObject(val = fn.call(it))) return val;
  if (!S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;
  throw TypeError("Can't convert object to primitive value");
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/es6.object.define-property.js":
/*!****************************************************************************!*\
  !*** ./node_modules/core-js/library/modules/es6.object.define-property.js ***!
  \****************************************************************************/
/***/ ((__unused_webpack_module, __unused_webpack_exports, __webpack_require__) => {

var $export = __webpack_require__(/*! ./_export */ "./node_modules/core-js/library/modules/_export.js");
// 19.1.2.4 / 15.2.3.6 Object.defineProperty(O, P, Attributes)
$export($export.S + $export.F * !__webpack_require__(/*! ./_descriptors */ "./node_modules/core-js/library/modules/_descriptors.js"), 'Object', { defineProperty: __webpack_require__(/*! ./_object-dp */ "./node_modules/core-js/library/modules/_object-dp.js").f });


/***/ }),

/***/ "./node_modules/events/events.js":
/*!***************************************!*\
  !*** ./node_modules/events/events.js ***!
  \***************************************/
/***/ ((module) => {

"use strict";
// Copyright Joyent, Inc. and other Node contributors.
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.



var R = typeof Reflect === 'object' ? Reflect : null
var ReflectApply = R && typeof R.apply === 'function'
  ? R.apply
  : function ReflectApply(target, receiver, args) {
    return Function.prototype.apply.call(target, receiver, args);
  }

var ReflectOwnKeys
if (R && typeof R.ownKeys === 'function') {
  ReflectOwnKeys = R.ownKeys
} else if (Object.getOwnPropertySymbols) {
  ReflectOwnKeys = function ReflectOwnKeys(target) {
    return Object.getOwnPropertyNames(target)
      .concat(Object.getOwnPropertySymbols(target));
  };
} else {
  ReflectOwnKeys = function ReflectOwnKeys(target) {
    return Object.getOwnPropertyNames(target);
  };
}

function ProcessEmitWarning(warning) {
  if (console && console.warn) console.warn(warning);
}

var NumberIsNaN = Number.isNaN || function NumberIsNaN(value) {
  return value !== value;
}

function EventEmitter() {
  EventEmitter.init.call(this);
}
module.exports = EventEmitter;

// Backwards-compat with node 0.10.x
EventEmitter.EventEmitter = EventEmitter;

EventEmitter.prototype._events = undefined;
EventEmitter.prototype._eventsCount = 0;
EventEmitter.prototype._maxListeners = undefined;

// By default EventEmitters will print a warning if more than 10 listeners are
// added to it. This is a useful default which helps finding memory leaks.
var defaultMaxListeners = 10;

function checkListener(listener) {
  if (typeof listener !== 'function') {
    throw new TypeError('The "listener" argument must be of type Function. Received type ' + typeof listener);
  }
}

Object.defineProperty(EventEmitter, 'defaultMaxListeners', {
  enumerable: true,
  get: function() {
    return defaultMaxListeners;
  },
  set: function(arg) {
    if (typeof arg !== 'number' || arg < 0 || NumberIsNaN(arg)) {
      throw new RangeError('The value of "defaultMaxListeners" is out of range. It must be a non-negative number. Received ' + arg + '.');
    }
    defaultMaxListeners = arg;
  }
});

EventEmitter.init = function() {

  if (this._events === undefined ||
      this._events === Object.getPrototypeOf(this)._events) {
    this._events = Object.create(null);
    this._eventsCount = 0;
  }

  this._maxListeners = this._maxListeners || undefined;
};

// Obviously not all Emitters should be limited to 10. This function allows
// that to be increased. Set to zero for unlimited.
EventEmitter.prototype.setMaxListeners = function setMaxListeners(n) {
  if (typeof n !== 'number' || n < 0 || NumberIsNaN(n)) {
    throw new RangeError('The value of "n" is out of range. It must be a non-negative number. Received ' + n + '.');
  }
  this._maxListeners = n;
  return this;
};

function _getMaxListeners(that) {
  if (that._maxListeners === undefined)
    return EventEmitter.defaultMaxListeners;
  return that._maxListeners;
}

EventEmitter.prototype.getMaxListeners = function getMaxListeners() {
  return _getMaxListeners(this);
};

EventEmitter.prototype.emit = function emit(type) {
  var args = [];
  for (var i = 1; i < arguments.length; i++) args.push(arguments[i]);
  var doError = (type === 'error');

  var events = this._events;
  if (events !== undefined)
    doError = (doError && events.error === undefined);
  else if (!doError)
    return false;

  // If there is no 'error' event listener then throw.
  if (doError) {
    var er;
    if (args.length > 0)
      er = args[0];
    if (er instanceof Error) {
      // Note: The comments on the `throw` lines are intentional, they show
      // up in Node's output if this results in an unhandled exception.
      throw er; // Unhandled 'error' event
    }
    // At least give some kind of context to the user
    var err = new Error('Unhandled error.' + (er ? ' (' + er.message + ')' : ''));
    err.context = er;
    throw err; // Unhandled 'error' event
  }

  var handler = events[type];

  if (handler === undefined)
    return false;

  if (typeof handler === 'function') {
    ReflectApply(handler, this, args);
  } else {
    var len = handler.length;
    var listeners = arrayClone(handler, len);
    for (var i = 0; i < len; ++i)
      ReflectApply(listeners[i], this, args);
  }

  return true;
};

function _addListener(target, type, listener, prepend) {
  var m;
  var events;
  var existing;

  checkListener(listener);

  events = target._events;
  if (events === undefined) {
    events = target._events = Object.create(null);
    target._eventsCount = 0;
  } else {
    // To avoid recursion in the case that type === "newListener"! Before
    // adding it to the listeners, first emit "newListener".
    if (events.newListener !== undefined) {
      target.emit('newListener', type,
                  listener.listener ? listener.listener : listener);

      // Re-assign `events` because a newListener handler could have caused the
      // this._events to be assigned to a new object
      events = target._events;
    }
    existing = events[type];
  }

  if (existing === undefined) {
    // Optimize the case of one listener. Don't need the extra array object.
    existing = events[type] = listener;
    ++target._eventsCount;
  } else {
    if (typeof existing === 'function') {
      // Adding the second element, need to change to array.
      existing = events[type] =
        prepend ? [listener, existing] : [existing, listener];
      // If we've already got an array, just append.
    } else if (prepend) {
      existing.unshift(listener);
    } else {
      existing.push(listener);
    }

    // Check for listener leak
    m = _getMaxListeners(target);
    if (m > 0 && existing.length > m && !existing.warned) {
      existing.warned = true;
      // No error code for this since it is a Warning
      // eslint-disable-next-line no-restricted-syntax
      var w = new Error('Possible EventEmitter memory leak detected. ' +
                          existing.length + ' ' + String(type) + ' listeners ' +
                          'added. Use emitter.setMaxListeners() to ' +
                          'increase limit');
      w.name = 'MaxListenersExceededWarning';
      w.emitter = target;
      w.type = type;
      w.count = existing.length;
      ProcessEmitWarning(w);
    }
  }

  return target;
}

EventEmitter.prototype.addListener = function addListener(type, listener) {
  return _addListener(this, type, listener, false);
};

EventEmitter.prototype.on = EventEmitter.prototype.addListener;

EventEmitter.prototype.prependListener =
    function prependListener(type, listener) {
      return _addListener(this, type, listener, true);
    };

function onceWrapper() {
  if (!this.fired) {
    this.target.removeListener(this.type, this.wrapFn);
    this.fired = true;
    if (arguments.length === 0)
      return this.listener.call(this.target);
    return this.listener.apply(this.target, arguments);
  }
}

function _onceWrap(target, type, listener) {
  var state = { fired: false, wrapFn: undefined, target: target, type: type, listener: listener };
  var wrapped = onceWrapper.bind(state);
  wrapped.listener = listener;
  state.wrapFn = wrapped;
  return wrapped;
}

EventEmitter.prototype.once = function once(type, listener) {
  checkListener(listener);
  this.on(type, _onceWrap(this, type, listener));
  return this;
};

EventEmitter.prototype.prependOnceListener =
    function prependOnceListener(type, listener) {
      checkListener(listener);
      this.prependListener(type, _onceWrap(this, type, listener));
      return this;
    };

// Emits a 'removeListener' event if and only if the listener was removed.
EventEmitter.prototype.removeListener =
    function removeListener(type, listener) {
      var list, events, position, i, originalListener;

      checkListener(listener);

      events = this._events;
      if (events === undefined)
        return this;

      list = events[type];
      if (list === undefined)
        return this;

      if (list === listener || list.listener === listener) {
        if (--this._eventsCount === 0)
          this._events = Object.create(null);
        else {
          delete events[type];
          if (events.removeListener)
            this.emit('removeListener', type, list.listener || listener);
        }
      } else if (typeof list !== 'function') {
        position = -1;

        for (i = list.length - 1; i >= 0; i--) {
          if (list[i] === listener || list[i].listener === listener) {
            originalListener = list[i].listener;
            position = i;
            break;
          }
        }

        if (position < 0)
          return this;

        if (position === 0)
          list.shift();
        else {
          spliceOne(list, position);
        }

        if (list.length === 1)
          events[type] = list[0];

        if (events.removeListener !== undefined)
          this.emit('removeListener', type, originalListener || listener);
      }

      return this;
    };

EventEmitter.prototype.off = EventEmitter.prototype.removeListener;

EventEmitter.prototype.removeAllListeners =
    function removeAllListeners(type) {
      var listeners, events, i;

      events = this._events;
      if (events === undefined)
        return this;

      // not listening for removeListener, no need to emit
      if (events.removeListener === undefined) {
        if (arguments.length === 0) {
          this._events = Object.create(null);
          this._eventsCount = 0;
        } else if (events[type] !== undefined) {
          if (--this._eventsCount === 0)
            this._events = Object.create(null);
          else
            delete events[type];
        }
        return this;
      }

      // emit removeListener for all listeners on all events
      if (arguments.length === 0) {
        var keys = Object.keys(events);
        var key;
        for (i = 0; i < keys.length; ++i) {
          key = keys[i];
          if (key === 'removeListener') continue;
          this.removeAllListeners(key);
        }
        this.removeAllListeners('removeListener');
        this._events = Object.create(null);
        this._eventsCount = 0;
        return this;
      }

      listeners = events[type];

      if (typeof listeners === 'function') {
        this.removeListener(type, listeners);
      } else if (listeners !== undefined) {
        // LIFO order
        for (i = listeners.length - 1; i >= 0; i--) {
          this.removeListener(type, listeners[i]);
        }
      }

      return this;
    };

function _listeners(target, type, unwrap) {
  var events = target._events;

  if (events === undefined)
    return [];

  var evlistener = events[type];
  if (evlistener === undefined)
    return [];

  if (typeof evlistener === 'function')
    return unwrap ? [evlistener.listener || evlistener] : [evlistener];

  return unwrap ?
    unwrapListeners(evlistener) : arrayClone(evlistener, evlistener.length);
}

EventEmitter.prototype.listeners = function listeners(type) {
  return _listeners(this, type, true);
};

EventEmitter.prototype.rawListeners = function rawListeners(type) {
  return _listeners(this, type, false);
};

EventEmitter.listenerCount = function(emitter, type) {
  if (typeof emitter.listenerCount === 'function') {
    return emitter.listenerCount(type);
  } else {
    return listenerCount.call(emitter, type);
  }
};

EventEmitter.prototype.listenerCount = listenerCount;
function listenerCount(type) {
  var events = this._events;

  if (events !== undefined) {
    var evlistener = events[type];

    if (typeof evlistener === 'function') {
      return 1;
    } else if (evlistener !== undefined) {
      return evlistener.length;
    }
  }

  return 0;
}

EventEmitter.prototype.eventNames = function eventNames() {
  return this._eventsCount > 0 ? ReflectOwnKeys(this._events) : [];
};

function arrayClone(arr, n) {
  var copy = new Array(n);
  for (var i = 0; i < n; ++i)
    copy[i] = arr[i];
  return copy;
}

function spliceOne(list, index) {
  for (; index + 1 < list.length; index++)
    list[index] = list[index + 1];
  list.pop();
}

function unwrapListeners(arr) {
  var ret = new Array(arr.length);
  for (var i = 0; i < ret.length; ++i) {
    ret[i] = arr[i].listener || arr[i];
  }
  return ret;
}


/***/ }),

/***/ "./node_modules/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.js":
/*!*****************************************************************************!*\
  !*** ./node_modules/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.js ***!
  \*****************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

/*** IMPORTS FROM imports-loader ***/

(function(define, exports) {
/*!
 * Bootstrap Colorpicker - Bootstrap Colorpicker is a modular color picker plugin for Bootstrap 4.
 * @package bootstrap-colorpicker
 * @version v3.2.0
 * @license MIT
 * @link https://itsjavi.com/bootstrap-colorpicker/
 * @link https://github.com/itsjavi/bootstrap-colorpicker.git
 */
(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && "object" === 'object')
		module.exports = factory(__webpack_require__(/*! jquery */ "jquery"));
	else if(typeof define === 'function' && define.amd)
		define("bootstrap-colorpicker", ["jquery"], factory);
	else if(typeof exports === 'object')
		exports["bootstrap-colorpicker"] = factory(__webpack_require__(/*! jquery */ "jquery"));
	else
		root["bootstrap-colorpicker"] = factory(root["jQuery"]);
})(window, function(__WEBPACK_EXTERNAL_MODULE__0__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __nested_webpack_require_1036__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __nested_webpack_require_1036__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__nested_webpack_require_1036__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__nested_webpack_require_1036__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__nested_webpack_require_1036__.d = function(exports, name, getter) {
/******/ 		if(!__nested_webpack_require_1036__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__nested_webpack_require_1036__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__nested_webpack_require_1036__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __nested_webpack_require_1036__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__nested_webpack_require_1036__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __nested_webpack_require_1036__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__nested_webpack_require_1036__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__nested_webpack_require_1036__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__nested_webpack_require_1036__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__nested_webpack_require_1036__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __nested_webpack_require_1036__(__nested_webpack_require_1036__.s = 7);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE__0__;

/***/ }),
/* 1 */
/***/ (function(module, exports, __nested_webpack_require_4604__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _jquery = __nested_webpack_require_4604__(0);

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * Colorpicker extension class.
 */
var Extension = function () {
  /**
   * @param {Colorpicker} colorpicker
   * @param {Object} options
   */
  function Extension(colorpicker) {
    var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

    _classCallCheck(this, Extension);

    /**
     * The colorpicker instance
     * @type {Colorpicker}
     */
    this.colorpicker = colorpicker;
    /**
     * Extension options
     *
     * @type {Object}
     */
    this.options = options;

    if (!(this.colorpicker.element && this.colorpicker.element.length)) {
      throw new Error('Extension: this.colorpicker.element is not valid');
    }

    this.colorpicker.element.on('colorpickerCreate.colorpicker-ext', _jquery2.default.proxy(this.onCreate, this));
    this.colorpicker.element.on('colorpickerDestroy.colorpicker-ext', _jquery2.default.proxy(this.onDestroy, this));
    this.colorpicker.element.on('colorpickerUpdate.colorpicker-ext', _jquery2.default.proxy(this.onUpdate, this));
    this.colorpicker.element.on('colorpickerChange.colorpicker-ext', _jquery2.default.proxy(this.onChange, this));
    this.colorpicker.element.on('colorpickerInvalid.colorpicker-ext', _jquery2.default.proxy(this.onInvalid, this));
    this.colorpicker.element.on('colorpickerShow.colorpicker-ext', _jquery2.default.proxy(this.onShow, this));
    this.colorpicker.element.on('colorpickerHide.colorpicker-ext', _jquery2.default.proxy(this.onHide, this));
    this.colorpicker.element.on('colorpickerEnable.colorpicker-ext', _jquery2.default.proxy(this.onEnable, this));
    this.colorpicker.element.on('colorpickerDisable.colorpicker-ext', _jquery2.default.proxy(this.onDisable, this));
  }

  /**
   * Function called every time a new color needs to be created.
   * Return false to skip this resolver and continue with other extensions' ones
   * or return anything else to consider the color resolved.
   *
   * @param {ColorItem|String|*} color
   * @param {boolean} realColor if true, the color should resolve into a real (not named) color code
   * @return {ColorItem|String|*}
   */


  _createClass(Extension, [{
    key: 'resolveColor',
    value: function resolveColor(color) {
      var realColor = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

      return false;
    }

    /**
     * Method called after the colorpicker is created
     *
     * @listens Colorpicker#colorpickerCreate
     * @param {Event} event
     */

  }, {
    key: 'onCreate',
    value: function onCreate(event) {}
    // to be extended


    /**
     * Method called after the colorpicker is destroyed
     *
     * @listens Colorpicker#colorpickerDestroy
     * @param {Event} event
     */

  }, {
    key: 'onDestroy',
    value: function onDestroy(event) {
      this.colorpicker.element.off('.colorpicker-ext');
    }

    /**
     * Method called after the colorpicker is updated
     *
     * @listens Colorpicker#colorpickerUpdate
     * @param {Event} event
     */

  }, {
    key: 'onUpdate',
    value: function onUpdate(event) {}
    // to be extended


    /**
     * Method called after the colorpicker color is changed
     *
     * @listens Colorpicker#colorpickerChange
     * @param {Event} event
     */

  }, {
    key: 'onChange',
    value: function onChange(event) {}
    // to be extended


    /**
     * Method called when the colorpicker color is invalid
     *
     * @listens Colorpicker#colorpickerInvalid
     * @param {Event} event
     */

  }, {
    key: 'onInvalid',
    value: function onInvalid(event) {}
    // to be extended


    /**
     * Method called after the colorpicker is hidden
     *
     * @listens Colorpicker#colorpickerHide
     * @param {Event} event
     */

  }, {
    key: 'onHide',
    value: function onHide(event) {}
    // to be extended


    /**
     * Method called after the colorpicker is shown
     *
     * @listens Colorpicker#colorpickerShow
     * @param {Event} event
     */

  }, {
    key: 'onShow',
    value: function onShow(event) {}
    // to be extended


    /**
     * Method called after the colorpicker is disabled
     *
     * @listens Colorpicker#colorpickerDisable
     * @param {Event} event
     */

  }, {
    key: 'onDisable',
    value: function onDisable(event) {}
    // to be extended


    /**
     * Method called after the colorpicker is enabled
     *
     * @listens Colorpicker#colorpickerEnable
     * @param {Event} event
     */

  }, {
    key: 'onEnable',
    value: function onEnable(event) {
      // to be extended
    }
  }]);

  return Extension;
}();

exports.default = Extension;
module.exports = exports.default;

/***/ }),
/* 2 */
/***/ (function(module, exports, __nested_webpack_require_10342__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ColorItem = exports.HSVAColor = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }(); /**
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      * Color manipulation class, specific for Bootstrap Colorpicker
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      */


var _color = __nested_webpack_require_10342__(16);

var _color2 = _interopRequireDefault(_color);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * HSVA color data class, containing the hue, saturation, value and alpha
 * information.
 */
var HSVAColor = function () {
  /**
   * @param {number|int} h
   * @param {number|int} s
   * @param {number|int} v
   * @param {number|int} a
   */
  function HSVAColor(h, s, v, a) {
    _classCallCheck(this, HSVAColor);

    this.h = isNaN(h) ? 0 : h;
    this.s = isNaN(s) ? 0 : s;
    this.v = isNaN(v) ? 0 : v;
    this.a = isNaN(h) ? 1 : a;
  }

  _createClass(HSVAColor, [{
    key: 'toString',
    value: function toString() {
      return this.h + ', ' + this.s + '%, ' + this.v + '%, ' + this.a;
    }
  }]);

  return HSVAColor;
}();

/**
 * HSVA color manipulation
 */


var ColorItem = function () {
  _createClass(ColorItem, [{
    key: 'api',


    /**
     * Applies a method of the QixColor API and returns a new Color object or
     * the return value of the method call.
     *
     * If no argument is provided, the internal QixColor object is returned.
     *
     * @param {String} fn QixColor function name
     * @param args QixColor function arguments
     * @example let darkerColor = color.api('darken', 0.25);
     * @example let luminosity = color.api('luminosity');
     * @example color = color.api('negate');
     * @example let qColor = color.api().negate();
     * @returns {ColorItem|QixColor|*}
     */
    value: function api(fn) {
      for (var _len = arguments.length, args = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
        args[_key - 1] = arguments[_key];
      }

      if (arguments.length === 0) {
        return this._color;
      }

      var result = this._color[fn].apply(this._color, args);

      if (!(result instanceof _color2.default)) {
        // return result of the method call
        return result;
      }

      return new ColorItem(result, this.format);
    }

    /**
     * Returns the original ColorItem constructor data,
     * plus a 'valid' flag to know if it's valid or not.
     *
     * @returns {{color: *, format: String, valid: boolean}}
     */

  }, {
    key: 'original',
    get: function get() {
      return this._original;
    }

    /**
     * @param {ColorItem|HSVAColor|QixColor|String|*|null} color Color data
     * @param {String|null} format Color model to convert to by default. Supported: 'rgb', 'hsl', 'hex'.
     */

  }], [{
    key: 'HSVAColor',


    /**
     * Returns the HSVAColor class
     *
     * @static
     * @example let colorData = new ColorItem.HSVAColor(360, 100, 100, 1);
     * @returns {HSVAColor}
     */
    get: function get() {
      return HSVAColor;
    }
  }]);

  function ColorItem() {
    var color = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
    var format = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;

    _classCallCheck(this, ColorItem);

    this.replace(color, format);
  }

  /**
   * Replaces the internal QixColor object with a new one.
   * This also replaces the internal original color data.
   *
   * @param {ColorItem|HSVAColor|QixColor|String|*|null} color Color data to be parsed (if needed)
   * @param {String|null} format Color model to convert to by default. Supported: 'rgb', 'hsl', 'hex'.
   * @example color.replace('rgb(255,0,0)', 'hsl');
   * @example color.replace(hsvaColorData);
   */


  _createClass(ColorItem, [{
    key: 'replace',
    value: function replace(color) {
      var format = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;

      format = ColorItem.sanitizeFormat(format);

      /**
       * @type {{color: *, format: String}}
       * @private
       */
      this._original = {
        color: color,
        format: format,
        valid: true
      };
      /**
       * @type {QixColor}
       * @private
       */
      this._color = ColorItem.parse(color);

      if (this._color === null) {
        this._color = (0, _color2.default)();
        this._original.valid = false;
        return;
      }

      /**
       * @type {*|string}
       * @private
       */
      this._format = format ? format : ColorItem.isHex(color) ? 'hex' : this._color.model;
    }

    /**
     * Parses the color returning a Qix Color object or null if cannot be
     * parsed.
     *
     * @param {ColorItem|HSVAColor|QixColor|String|*|null} color Color data
     * @example let qColor = ColorItem.parse('rgb(255,0,0)');
     * @static
     * @returns {QixColor|null}
     */

  }, {
    key: 'isValid',


    /**
     * Returns true if the color is valid, false if not.
     *
     * @returns {boolean}
     */
    value: function isValid() {
      return this._original.valid === true;
    }

    /**
     * Hue value from 0 to 360
     *
     * @returns {int}
     */

  }, {
    key: 'setHueRatio',


    /**
     * Sets the hue ratio, where 1.0 is 0, 0.5 is 180 and 0.0 is 360.
     *
     * @ignore
     * @param {number} h Ratio from 1.0 to 0.0
     */
    value: function setHueRatio(h) {
      this.hue = (1 - h) * 360;
    }

    /**
     * Sets the saturation value
     *
     * @param {int} value Integer from 0 to 100
     */

  }, {
    key: 'setSaturationRatio',


    /**
     * Sets the saturation ratio, where 1.0 is 100 and 0.0 is 0.
     *
     * @ignore
     * @param {number} s Ratio from 0.0 to 1.0
     */
    value: function setSaturationRatio(s) {
      this.saturation = s * 100;
    }

    /**
     * Sets the 'value' channel value
     *
     * @param {int} value Integer from 0 to 100
     */

  }, {
    key: 'setValueRatio',


    /**
     * Sets the value ratio, where 1.0 is 0 and 0.0 is 100.
     *
     * @ignore
     * @param {number} v Ratio from 1.0 to 0.0
     */
    value: function setValueRatio(v) {
      this.value = (1 - v) * 100;
    }

    /**
     * Sets the alpha value. It will be rounded to 2 decimals.
     *
     * @param {int} value Float from 0.0 to 1.0
     */

  }, {
    key: 'setAlphaRatio',


    /**
     * Sets the alpha ratio, where 1.0 is 0.0 and 0.0 is 1.0.
     *
     * @ignore
     * @param {number} a Ratio from 1.0 to 0.0
     */
    value: function setAlphaRatio(a) {
      this.alpha = 1 - a;
    }

    /**
     * Sets the default color format
     *
     * @param {String} value Supported: 'rgb', 'hsl', 'hex'
     */

  }, {
    key: 'isDesaturated',


    /**
     * Returns true if the saturation value is zero, false otherwise
     *
     * @returns {boolean}
     */
    value: function isDesaturated() {
      return this.saturation === 0;
    }

    /**
     * Returns true if the alpha value is zero, false otherwise
     *
     * @returns {boolean}
     */

  }, {
    key: 'isTransparent',
    value: function isTransparent() {
      return this.alpha === 0;
    }

    /**
     * Returns true if the alpha value is numeric and less than 1, false otherwise
     *
     * @returns {boolean}
     */

  }, {
    key: 'hasTransparency',
    value: function hasTransparency() {
      return this.hasAlpha() && this.alpha < 1;
    }

    /**
     * Returns true if the alpha value is numeric, false otherwise
     *
     * @returns {boolean}
     */

  }, {
    key: 'hasAlpha',
    value: function hasAlpha() {
      return !isNaN(this.alpha);
    }

    /**
     * Returns a new HSVAColor object, based on the current color
     *
     * @returns {HSVAColor}
     */

  }, {
    key: 'toObject',
    value: function toObject() {
      return new HSVAColor(this.hue, this.saturation, this.value, this.alpha);
    }

    /**
     * Alias of toObject()
     *
     * @returns {HSVAColor}
     */

  }, {
    key: 'toHsva',
    value: function toHsva() {
      return this.toObject();
    }

    /**
     * Returns a new HSVAColor object with the ratio values (from 0.0 to 1.0),
     * based on the current color.
     *
     * @ignore
     * @returns {HSVAColor}
     */

  }, {
    key: 'toHsvaRatio',
    value: function toHsvaRatio() {
      return new HSVAColor(this.hue / 360, this.saturation / 100, this.value / 100, this.alpha);
    }

    /**
     * Converts the current color to its string representation,
     * using the internal format of this instance.
     *
     * @returns {String}
     */

  }, {
    key: 'toString',
    value: function toString() {
      return this.string();
    }

    /**
     * Converts the current color to its string representation,
     * using the given format.
     *
     * @param {String|null} format Format to convert to. If empty or null, the internal format will be used.
     * @returns {String}
     */

  }, {
    key: 'string',
    value: function string() {
      var format = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;

      format = ColorItem.sanitizeFormat(format ? format : this.format);

      if (!format) {
        return this._color.round().string();
      }

      if (this._color[format] === undefined) {
        throw new Error('Unsupported color format: \'' + format + '\'');
      }

      var str = this._color[format]();

      return str.round ? str.round().string() : str;
    }

    /**
     * Returns true if the given color values equals this one, false otherwise.
     * The format is not compared.
     * If any of the colors is invalid, the result will be false.
     *
     * @param {ColorItem|HSVAColor|QixColor|String|*|null} color Color data
     *
     * @returns {boolean}
     */

  }, {
    key: 'equals',
    value: function equals(color) {
      color = color instanceof ColorItem ? color : new ColorItem(color);

      if (!color.isValid() || !this.isValid()) {
        return false;
      }

      return this.hue === color.hue && this.saturation === color.saturation && this.value === color.value && this.alpha === color.alpha;
    }

    /**
     * Creates a copy of this instance
     *
     * @returns {ColorItem}
     */

  }, {
    key: 'getClone',
    value: function getClone() {
      return new ColorItem(this._color, this.format);
    }

    /**
     * Creates a copy of this instance, only copying the hue value,
     * and setting the others to its max value.
     *
     * @returns {ColorItem}
     */

  }, {
    key: 'getCloneHueOnly',
    value: function getCloneHueOnly() {
      return new ColorItem([this.hue, 100, 100, 1], this.format);
    }

    /**
     * Creates a copy of this instance setting the alpha to the max.
     *
     * @returns {ColorItem}
     */

  }, {
    key: 'getCloneOpaque',
    value: function getCloneOpaque() {
      return new ColorItem(this._color.alpha(1), this.format);
    }

    /**
     * Converts the color to a RGB string
     *
     * @returns {String}
     */

  }, {
    key: 'toRgbString',
    value: function toRgbString() {
      return this.string('rgb');
    }

    /**
     * Converts the color to a Hexadecimal string
     *
     * @returns {String}
     */

  }, {
    key: 'toHexString',
    value: function toHexString() {
      return this.string('hex');
    }

    /**
     * Converts the color to a HSL string
     *
     * @returns {String}
     */

  }, {
    key: 'toHslString',
    value: function toHslString() {
      return this.string('hsl');
    }

    /**
     * Returns true if the color is dark, false otherwhise.
     * This is useful to decide a text color.
     *
     * @returns {boolean}
     */

  }, {
    key: 'isDark',
    value: function isDark() {
      return this._color.isDark();
    }

    /**
     * Returns true if the color is light, false otherwhise.
     * This is useful to decide a text color.
     *
     * @returns {boolean}
     */

  }, {
    key: 'isLight',
    value: function isLight() {
      return this._color.isLight();
    }

    /**
     * Generates a list of colors using the given hue-based formula or the given array of hue values.
     * Hue formulas can be extended using ColorItem.colorFormulas static property.
     *
     * @param {String|Number[]} formula Examples: 'complementary', 'triad', 'tetrad', 'splitcomplement', [180, 270]
     * @example let colors = color.generate('triad');
     * @example let colors = color.generate([45, 80, 112, 200]);
     * @returns {ColorItem[]}
     */

  }, {
    key: 'generate',
    value: function generate(formula) {
      var hues = [];

      if (Array.isArray(formula)) {
        hues = formula;
      } else if (!ColorItem.colorFormulas.hasOwnProperty(formula)) {
        throw new Error('No color formula found with the name \'' + formula + '\'.');
      } else {
        hues = ColorItem.colorFormulas[formula];
      }

      var colors = [],
          mainColor = this._color,
          format = this.format;

      hues.forEach(function (hue) {
        var levels = [hue ? (mainColor.hue() + hue) % 360 : mainColor.hue(), mainColor.saturationv(), mainColor.value(), mainColor.alpha()];

        colors.push(new ColorItem(levels, format));
      });

      return colors;
    }
  }, {
    key: 'hue',
    get: function get() {
      return this._color.hue();
    }

    /**
     * Saturation value from 0 to 100
     *
     * @returns {int}
     */
    ,


    /**
     * Sets the hue value
     *
     * @param {int} value Integer from 0 to 360
     */
    set: function set(value) {
      this._color = this._color.hue(value);
    }
  }, {
    key: 'saturation',
    get: function get() {
      return this._color.saturationv();
    }

    /**
     * Value channel value from 0 to 100
     *
     * @returns {int}
     */
    ,
    set: function set(value) {
      this._color = this._color.saturationv(value);
    }
  }, {
    key: 'value',
    get: function get() {
      return this._color.value();
    }

    /**
     * Alpha value from 0.0 to 1.0
     *
     * @returns {number}
     */
    ,
    set: function set(value) {
      this._color = this._color.value(value);
    }
  }, {
    key: 'alpha',
    get: function get() {
      var a = this._color.alpha();

      return isNaN(a) ? 1 : a;
    }

    /**
     * Default color format to convert to when calling toString() or string()
     *
     * @returns {String} 'rgb', 'hsl', 'hex' or ''
     */
    ,
    set: function set(value) {
      // 2 decimals max
      this._color = this._color.alpha(Math.round(value * 100) / 100);
    }
  }, {
    key: 'format',
    get: function get() {
      return this._format ? this._format : this._color.model;
    },
    set: function set(value) {
      this._format = ColorItem.sanitizeFormat(value);
    }
  }], [{
    key: 'parse',
    value: function parse(color) {
      if (color instanceof _color2.default) {
        return color;
      }

      if (color instanceof ColorItem) {
        return color._color;
      }

      var format = null;

      if (color instanceof HSVAColor) {
        color = [color.h, color.s, color.v, isNaN(color.a) ? 1 : color.a];
      } else {
        color = ColorItem.sanitizeString(color);
      }

      if (color === null) {
        return null;
      }

      if (Array.isArray(color)) {
        format = 'hsv';
      }

      try {
        return (0, _color2.default)(color, format);
      } catch (e) {
        return null;
      }
    }

    /**
     * Sanitizes a color string, adding missing hash to hexadecimal colors
     * and converting 'transparent' to a color code.
     *
     * @param {String|*} str Color string
     * @example let colorStr = ColorItem.sanitizeString('ffaa00');
     * @static
     * @returns {String|*}
     */

  }, {
    key: 'sanitizeString',
    value: function sanitizeString(str) {
      if (!(typeof str === 'string' || str instanceof String)) {
        return str;
      }

      if (str.match(/^[0-9a-f]{2,}$/i)) {
        return '#' + str;
      }

      if (str.toLowerCase() === 'transparent') {
        return '#FFFFFF00';
      }

      return str;
    }

    /**
     * Detects if a value is a string and a color in hexadecimal format (in any variant).
     *
     * @param {String} str
     * @example ColorItem.isHex('rgba(0,0,0)'); // false
     * @example ColorItem.isHex('ffaa00'); // true
     * @example ColorItem.isHex('#ffaa00'); // true
     * @static
     * @returns {boolean}
     */

  }, {
    key: 'isHex',
    value: function isHex(str) {
      if (!(typeof str === 'string' || str instanceof String)) {
        return false;
      }

      return !!str.match(/^#?[0-9a-f]{2,}$/i);
    }

    /**
     * Sanitizes a color format to one supported by web browsers.
     * Returns an empty string of the format can't be recognised.
     *
     * @param {String|*} format
     * @example ColorItem.sanitizeFormat('rgba'); // 'rgb'
     * @example ColorItem.isHex('hex8'); // 'hex'
     * @example ColorItem.isHex('invalid'); // ''
     * @static
     * @returns {String} 'rgb', 'hsl', 'hex' or ''.
     */

  }, {
    key: 'sanitizeFormat',
    value: function sanitizeFormat(format) {
      switch (format) {
        case 'hex':
        case 'hex3':
        case 'hex4':
        case 'hex6':
        case 'hex8':
          return 'hex';
        case 'rgb':
        case 'rgba':
        case 'keyword':
        case 'name':
          return 'rgb';
        case 'hsl':
        case 'hsla':
        case 'hsv':
        case 'hsva':
        case 'hwb': // HWB this is supported by Qix Color, but not by browsers
        case 'hwba':
          return 'hsl';
        default:
          return '';
      }
    }
  }]);

  return ColorItem;
}();

/**
 * List of hue-based color formulas used by ColorItem.prototype.generate()
 *
 * @static
 * @type {{complementary: number[], triad: number[], tetrad: number[], splitcomplement: number[]}}
 */


ColorItem.colorFormulas = {
  complementary: [180],
  triad: [0, 120, 240],
  tetrad: [0, 90, 180, 270],
  splitcomplement: [0, 72, 216]
};

exports.default = ColorItem;
exports.HSVAColor = HSVAColor;
exports.ColorItem = ColorItem;

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/**
 * @module
 */

// adjust these values accordingly to the sass vars

Object.defineProperty(exports, "__esModule", {
  value: true
});
var sassVars = {
  'bar_size_short': 16,
  'base_margin': 6,
  'columns': 6
};

var sliderSize = sassVars.bar_size_short * sassVars.columns + sassVars.base_margin * (sassVars.columns - 1);

/**
 * Colorpicker default options
 */
exports.default = {
  /**
   * Custom class to be added to the `.colorpicker-element` element
   *
   * @type {String|null}
   * @default null
   */
  customClass: null,
  /**
   * Sets a initial color, ignoring the one from the element/input value or the data-color attribute.
   *
   * @type {(String|ColorItem|boolean)}
   * @default false
   */
  color: false,
  /**
   * Fallback color to use when the given color is invalid.
   * If false, the latest valid color will be used as a fallback.
   *
   * @type {String|ColorItem|boolean}
   * @default false
   */
  fallbackColor: false,
  /**
   * Forces an specific color format. If 'auto', it will be automatically detected the first time only,
   * but if null it will be always recalculated.
   *
   * Note that the ending 'a' of the format meaning "alpha" has currently no effect, meaning that rgb is the same as
   * rgba excepting if the alpha channel is disabled (see useAlpha).
   *
   * @type {('rgb'|'hex'|'hsl'|'auto'|null)}
   * @default 'auto'
   */
  format: 'auto',
  /**
   * Horizontal mode layout.
   *
   * If true, the hue and alpha channel bars will be rendered horizontally, above the saturation selector.
   *
   * @type {boolean}
   * @default false
   */
  horizontal: false,
  /**
   * Forces to show the colorpicker as an inline element.
   *
   * Note that if there is no container specified, the inline element
   * will be added to the body, so you may want to set the container option.
   *
   * @type {boolean}
   * @default false
   */
  inline: false,
  /**
   * Container where the colorpicker is appended to in the DOM.
   *
   * If is a string (CSS selector), the colorpicker will be placed inside this container.
   * If true, the `.colorpicker-element` element itself will be used as the container.
   * If false, the document body is used as the container, unless it is a popover (in this case it is appended to the
   * popover body instead).
   *
   * @type {String|boolean}
   * @default false
   */
  container: false,
  /**
   * Bootstrap Popover options.
   * The trigger, content and html options are always ignored.
   *
   * @type {boolean}
   * @default Object
   */
  popover: {
    animation: true,
    placement: 'bottom',
    fallbackPlacement: 'flip'
  },
  /**
   * If true, loads the 'debugger' extension automatically, which logs the events in the console
   * @type {boolean}
   * @default false
   */
  debug: false,
  /**
   * Child CSS selector for the colorpicker input.
   *
   * @type {String}
   * @default 'input'
   */
  input: 'input',
  /**
   * Child CSS selector for the colorpicker addon.
   * If it exists, the child <i> element background will be changed on color change.
   *
   * @type {String}
   * @default '.colorpicker-trigger, .colorpicker-input-addon'
   */
  addon: '.colorpicker-input-addon',
  /**
   * If true, the input content will be replaced always with a valid color,
   * if false, the invalid color will be left in the input,
   *   while the internal color object will still resolve into a valid one.
   *
   * @type {boolean}
   * @default true
   */
  autoInputFallback: true,
  /**
   * If true a hash will be prepended to hexadecimal colors.
   * If false, the hash will be removed.
   * This only affects the input values in hexadecimal format.
   *
   * @type {boolean}
   * @default true
   */
  useHashPrefix: true,
  /**
   * If true, the alpha channel bar will be displayed no matter what.
   *
   * If false, it will be always hidden and alpha channel will be disabled also programmatically, meaning that
   * the selected or typed color will be always opaque.
   *
   * If null, the alpha channel will be automatically disabled/enabled depending if the initial color format supports
   * alpha or not.
   *
   * @type {boolean}
   * @default true
   */
  useAlpha: true,
  /**
   * Colorpicker widget template
   * @type {String}
   * @example
   * <!-- This is the default template: -->
   * <div class="colorpicker">
   *   <div class="colorpicker-saturation"><i class="colorpicker-guide"></i></div>
   *   <div class="colorpicker-hue"><i class="colorpicker-guide"></i></div>
   *   <div class="colorpicker-alpha">
   *     <div class="colorpicker-alpha-color"></div>
   *     <i class="colorpicker-guide"></i>
   *   </div>
   * </div>
   */
  template: '<div class="colorpicker">\n      <div class="colorpicker-saturation"><i class="colorpicker-guide"></i></div>\n      <div class="colorpicker-hue"><i class="colorpicker-guide"></i></div>\n      <div class="colorpicker-alpha">\n        <div class="colorpicker-alpha-color"></div>\n        <i class="colorpicker-guide"></i>\n      </div>\n    </div>',
  /**
   *
   * Associative object with the extension class name and its config.
   * Colorpicker comes with many bundled extensions: debugger, palette, preview and swatches (a superset of palette).
   *
   * @type {Object[]}
   * @example
   *   extensions: [
   *     {
   *       name: 'swatches'
   *       options: {
   *         colors: {
   *           'primary': '#337ab7',
   *           'success': '#5cb85c',
   *           'info': '#5bc0de',
   *           'warning': '#f0ad4e',
   *           'danger': '#d9534f'
   *         },
   *         namesAsValues: true
   *       }
   *     }
   *   ]
   */
  extensions: [{
    name: 'preview',
    options: {
      showText: true
    }
  }],
  /**
   * Vertical sliders configuration
   * @type {Object}
   */
  sliders: {
    saturation: {
      selector: '.colorpicker-saturation',
      maxLeft: sliderSize,
      maxTop: sliderSize,
      callLeft: 'setSaturationRatio',
      callTop: 'setValueRatio'
    },
    hue: {
      selector: '.colorpicker-hue',
      maxLeft: 0,
      maxTop: sliderSize,
      callLeft: false,
      callTop: 'setHueRatio'
    },
    alpha: {
      selector: '.colorpicker-alpha',
      childSelector: '.colorpicker-alpha-color',
      maxLeft: 0,
      maxTop: sliderSize,
      callLeft: false,
      callTop: 'setAlphaRatio'
    }
  },
  /**
   * Horizontal sliders configuration
   * @type {Object}
   */
  slidersHorz: {
    saturation: {
      selector: '.colorpicker-saturation',
      maxLeft: sliderSize,
      maxTop: sliderSize,
      callLeft: 'setSaturationRatio',
      callTop: 'setValueRatio'
    },
    hue: {
      selector: '.colorpicker-hue',
      maxLeft: sliderSize,
      maxTop: 0,
      callLeft: 'setHueRatio',
      callTop: false
    },
    alpha: {
      selector: '.colorpicker-alpha',
      childSelector: '.colorpicker-alpha-color',
      maxLeft: sliderSize,
      maxTop: 0,
      callLeft: 'setAlphaRatio',
      callTop: false
    }
  }
};
module.exports = exports.default;

/***/ }),
/* 4 */
/***/ (function(module, exports, __nested_webpack_require_37509__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Extension2 = __nested_webpack_require_37509__(1);

var _Extension3 = _interopRequireDefault(_Extension2);

var _jquery = __nested_webpack_require_37509__(0);

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var defaults = {
  /**
   * Key-value pairs defining a color alias and its CSS color representation.
   *
   * They can also be just an array of values. In that case, no special names are used, only the real colors.
   *
   * @type {Object|Array}
   * @default null
   * @example
   *  {
   *   'black': '#000000',
   *   'white': '#ffffff',
   *   'red': '#FF0000',
   *   'default': '#777777',
   *   'primary': '#337ab7',
   *   'success': '#5cb85c',
   *   'info': '#5bc0de',
   *   'warning': '#f0ad4e',
   *   'danger': '#d9534f'
   *  }
   *
   * @example ['#f0ad4e', '#337ab7', '#5cb85c']
   */
  colors: null,
  /**
   * If true, when a color swatch is selected the name (alias) will be used as input value,
   * otherwise the swatch real color value will be used.
   *
   * @type {boolean}
   * @default true
   */
  namesAsValues: true
};

/**
 * Palette extension
 * @ignore
 */

var Palette = function (_Extension) {
  _inherits(Palette, _Extension);

  _createClass(Palette, [{
    key: 'colors',


    /**
     * @returns {Object|Array}
     */
    get: function get() {
      return this.options.colors;
    }
  }]);

  function Palette(colorpicker) {
    var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

    _classCallCheck(this, Palette);

    var _this = _possibleConstructorReturn(this, (Palette.__proto__ || Object.getPrototypeOf(Palette)).call(this, colorpicker, _jquery2.default.extend(true, {}, defaults, options)));

    if (!Array.isArray(_this.options.colors) && _typeof(_this.options.colors) !== 'object') {
      _this.options.colors = null;
    }
    return _this;
  }

  /**
   * @returns {int}
   */


  _createClass(Palette, [{
    key: 'getLength',
    value: function getLength() {
      if (!this.options.colors) {
        return 0;
      }

      if (Array.isArray(this.options.colors)) {
        return this.options.colors.length;
      }

      if (_typeof(this.options.colors) === 'object') {
        return Object.keys(this.options.colors).length;
      }

      return 0;
    }
  }, {
    key: 'resolveColor',
    value: function resolveColor(color) {
      var realColor = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

      if (this.getLength() <= 0) {
        return false;
      }

      // Array of colors
      if (Array.isArray(this.options.colors)) {
        if (this.options.colors.indexOf(color) >= 0) {
          return color;
        }
        if (this.options.colors.indexOf(color.toUpperCase()) >= 0) {
          return color.toUpperCase();
        }
        if (this.options.colors.indexOf(color.toLowerCase()) >= 0) {
          return color.toLowerCase();
        }
        return false;
      }

      if (_typeof(this.options.colors) !== 'object') {
        return false;
      }

      // Map of objects
      if (!this.options.namesAsValues || realColor) {
        return this.getValue(color, false);
      }
      return this.getName(color, this.getName('#' + color));
    }

    /**
     * Given a color value, returns the corresponding color name or defaultValue.
     *
     * @param {String} value
     * @param {*} defaultValue
     * @returns {*}
     */

  }, {
    key: 'getName',
    value: function getName(value) {
      var defaultValue = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

      if (!(typeof value === 'string') || !this.options.colors) {
        return defaultValue;
      }
      for (var name in this.options.colors) {
        if (!this.options.colors.hasOwnProperty(name)) {
          continue;
        }
        if (this.options.colors[name].toLowerCase() === value.toLowerCase()) {
          return name;
        }
      }
      return defaultValue;
    }

    /**
     * Given a color name, returns the corresponding color value or defaultValue.
     *
     * @param {String} name
     * @param {*} defaultValue
     * @returns {*}
     */

  }, {
    key: 'getValue',
    value: function getValue(name) {
      var defaultValue = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

      if (!(typeof name === 'string') || !this.options.colors) {
        return defaultValue;
      }
      if (this.options.colors.hasOwnProperty(name)) {
        return this.options.colors[name];
      }
      return defaultValue;
    }
  }]);

  return Palette;
}(_Extension3.default);

exports.default = Palette;
module.exports = exports.default;

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = {
	"aliceblue": [240, 248, 255],
	"antiquewhite": [250, 235, 215],
	"aqua": [0, 255, 255],
	"aquamarine": [127, 255, 212],
	"azure": [240, 255, 255],
	"beige": [245, 245, 220],
	"bisque": [255, 228, 196],
	"black": [0, 0, 0],
	"blanchedalmond": [255, 235, 205],
	"blue": [0, 0, 255],
	"blueviolet": [138, 43, 226],
	"brown": [165, 42, 42],
	"burlywood": [222, 184, 135],
	"cadetblue": [95, 158, 160],
	"chartreuse": [127, 255, 0],
	"chocolate": [210, 105, 30],
	"coral": [255, 127, 80],
	"cornflowerblue": [100, 149, 237],
	"cornsilk": [255, 248, 220],
	"crimson": [220, 20, 60],
	"cyan": [0, 255, 255],
	"darkblue": [0, 0, 139],
	"darkcyan": [0, 139, 139],
	"darkgoldenrod": [184, 134, 11],
	"darkgray": [169, 169, 169],
	"darkgreen": [0, 100, 0],
	"darkgrey": [169, 169, 169],
	"darkkhaki": [189, 183, 107],
	"darkmagenta": [139, 0, 139],
	"darkolivegreen": [85, 107, 47],
	"darkorange": [255, 140, 0],
	"darkorchid": [153, 50, 204],
	"darkred": [139, 0, 0],
	"darksalmon": [233, 150, 122],
	"darkseagreen": [143, 188, 143],
	"darkslateblue": [72, 61, 139],
	"darkslategray": [47, 79, 79],
	"darkslategrey": [47, 79, 79],
	"darkturquoise": [0, 206, 209],
	"darkviolet": [148, 0, 211],
	"deeppink": [255, 20, 147],
	"deepskyblue": [0, 191, 255],
	"dimgray": [105, 105, 105],
	"dimgrey": [105, 105, 105],
	"dodgerblue": [30, 144, 255],
	"firebrick": [178, 34, 34],
	"floralwhite": [255, 250, 240],
	"forestgreen": [34, 139, 34],
	"fuchsia": [255, 0, 255],
	"gainsboro": [220, 220, 220],
	"ghostwhite": [248, 248, 255],
	"gold": [255, 215, 0],
	"goldenrod": [218, 165, 32],
	"gray": [128, 128, 128],
	"green": [0, 128, 0],
	"greenyellow": [173, 255, 47],
	"grey": [128, 128, 128],
	"honeydew": [240, 255, 240],
	"hotpink": [255, 105, 180],
	"indianred": [205, 92, 92],
	"indigo": [75, 0, 130],
	"ivory": [255, 255, 240],
	"khaki": [240, 230, 140],
	"lavender": [230, 230, 250],
	"lavenderblush": [255, 240, 245],
	"lawngreen": [124, 252, 0],
	"lemonchiffon": [255, 250, 205],
	"lightblue": [173, 216, 230],
	"lightcoral": [240, 128, 128],
	"lightcyan": [224, 255, 255],
	"lightgoldenrodyellow": [250, 250, 210],
	"lightgray": [211, 211, 211],
	"lightgreen": [144, 238, 144],
	"lightgrey": [211, 211, 211],
	"lightpink": [255, 182, 193],
	"lightsalmon": [255, 160, 122],
	"lightseagreen": [32, 178, 170],
	"lightskyblue": [135, 206, 250],
	"lightslategray": [119, 136, 153],
	"lightslategrey": [119, 136, 153],
	"lightsteelblue": [176, 196, 222],
	"lightyellow": [255, 255, 224],
	"lime": [0, 255, 0],
	"limegreen": [50, 205, 50],
	"linen": [250, 240, 230],
	"magenta": [255, 0, 255],
	"maroon": [128, 0, 0],
	"mediumaquamarine": [102, 205, 170],
	"mediumblue": [0, 0, 205],
	"mediumorchid": [186, 85, 211],
	"mediumpurple": [147, 112, 219],
	"mediumseagreen": [60, 179, 113],
	"mediumslateblue": [123, 104, 238],
	"mediumspringgreen": [0, 250, 154],
	"mediumturquoise": [72, 209, 204],
	"mediumvioletred": [199, 21, 133],
	"midnightblue": [25, 25, 112],
	"mintcream": [245, 255, 250],
	"mistyrose": [255, 228, 225],
	"moccasin": [255, 228, 181],
	"navajowhite": [255, 222, 173],
	"navy": [0, 0, 128],
	"oldlace": [253, 245, 230],
	"olive": [128, 128, 0],
	"olivedrab": [107, 142, 35],
	"orange": [255, 165, 0],
	"orangered": [255, 69, 0],
	"orchid": [218, 112, 214],
	"palegoldenrod": [238, 232, 170],
	"palegreen": [152, 251, 152],
	"paleturquoise": [175, 238, 238],
	"palevioletred": [219, 112, 147],
	"papayawhip": [255, 239, 213],
	"peachpuff": [255, 218, 185],
	"peru": [205, 133, 63],
	"pink": [255, 192, 203],
	"plum": [221, 160, 221],
	"powderblue": [176, 224, 230],
	"purple": [128, 0, 128],
	"rebeccapurple": [102, 51, 153],
	"red": [255, 0, 0],
	"rosybrown": [188, 143, 143],
	"royalblue": [65, 105, 225],
	"saddlebrown": [139, 69, 19],
	"salmon": [250, 128, 114],
	"sandybrown": [244, 164, 96],
	"seagreen": [46, 139, 87],
	"seashell": [255, 245, 238],
	"sienna": [160, 82, 45],
	"silver": [192, 192, 192],
	"skyblue": [135, 206, 235],
	"slateblue": [106, 90, 205],
	"slategray": [112, 128, 144],
	"slategrey": [112, 128, 144],
	"snow": [255, 250, 250],
	"springgreen": [0, 255, 127],
	"steelblue": [70, 130, 180],
	"tan": [210, 180, 140],
	"teal": [0, 128, 128],
	"thistle": [216, 191, 216],
	"tomato": [255, 99, 71],
	"turquoise": [64, 224, 208],
	"violet": [238, 130, 238],
	"wheat": [245, 222, 179],
	"white": [255, 255, 255],
	"whitesmoke": [245, 245, 245],
	"yellow": [255, 255, 0],
	"yellowgreen": [154, 205, 50]
};


/***/ }),
/* 6 */
/***/ (function(module, exports, __nested_webpack_require_48826__) {

/* MIT license */
var cssKeywords = __nested_webpack_require_48826__(5);

// NOTE: conversions should only return primitive values (i.e. arrays, or
//       values that give correct `typeof` results).
//       do not use box values types (i.e. Number(), String(), etc.)

var reverseKeywords = {};
for (var key in cssKeywords) {
	if (cssKeywords.hasOwnProperty(key)) {
		reverseKeywords[cssKeywords[key]] = key;
	}
}

var convert = module.exports = {
	rgb: {channels: 3, labels: 'rgb'},
	hsl: {channels: 3, labels: 'hsl'},
	hsv: {channels: 3, labels: 'hsv'},
	hwb: {channels: 3, labels: 'hwb'},
	cmyk: {channels: 4, labels: 'cmyk'},
	xyz: {channels: 3, labels: 'xyz'},
	lab: {channels: 3, labels: 'lab'},
	lch: {channels: 3, labels: 'lch'},
	hex: {channels: 1, labels: ['hex']},
	keyword: {channels: 1, labels: ['keyword']},
	ansi16: {channels: 1, labels: ['ansi16']},
	ansi256: {channels: 1, labels: ['ansi256']},
	hcg: {channels: 3, labels: ['h', 'c', 'g']},
	apple: {channels: 3, labels: ['r16', 'g16', 'b16']},
	gray: {channels: 1, labels: ['gray']}
};

// hide .channels and .labels properties
for (var model in convert) {
	if (convert.hasOwnProperty(model)) {
		if (!('channels' in convert[model])) {
			throw new Error('missing channels property: ' + model);
		}

		if (!('labels' in convert[model])) {
			throw new Error('missing channel labels property: ' + model);
		}

		if (convert[model].labels.length !== convert[model].channels) {
			throw new Error('channel and label counts mismatch: ' + model);
		}

		var channels = convert[model].channels;
		var labels = convert[model].labels;
		delete convert[model].channels;
		delete convert[model].labels;
		Object.defineProperty(convert[model], 'channels', {value: channels});
		Object.defineProperty(convert[model], 'labels', {value: labels});
	}
}

convert.rgb.hsl = function (rgb) {
	var r = rgb[0] / 255;
	var g = rgb[1] / 255;
	var b = rgb[2] / 255;
	var min = Math.min(r, g, b);
	var max = Math.max(r, g, b);
	var delta = max - min;
	var h;
	var s;
	var l;

	if (max === min) {
		h = 0;
	} else if (r === max) {
		h = (g - b) / delta;
	} else if (g === max) {
		h = 2 + (b - r) / delta;
	} else if (b === max) {
		h = 4 + (r - g) / delta;
	}

	h = Math.min(h * 60, 360);

	if (h < 0) {
		h += 360;
	}

	l = (min + max) / 2;

	if (max === min) {
		s = 0;
	} else if (l <= 0.5) {
		s = delta / (max + min);
	} else {
		s = delta / (2 - max - min);
	}

	return [h, s * 100, l * 100];
};

convert.rgb.hsv = function (rgb) {
	var rdif;
	var gdif;
	var bdif;
	var h;
	var s;

	var r = rgb[0] / 255;
	var g = rgb[1] / 255;
	var b = rgb[2] / 255;
	var v = Math.max(r, g, b);
	var diff = v - Math.min(r, g, b);
	var diffc = function (c) {
		return (v - c) / 6 / diff + 1 / 2;
	};

	if (diff === 0) {
		h = s = 0;
	} else {
		s = diff / v;
		rdif = diffc(r);
		gdif = diffc(g);
		bdif = diffc(b);

		if (r === v) {
			h = bdif - gdif;
		} else if (g === v) {
			h = (1 / 3) + rdif - bdif;
		} else if (b === v) {
			h = (2 / 3) + gdif - rdif;
		}
		if (h < 0) {
			h += 1;
		} else if (h > 1) {
			h -= 1;
		}
	}

	return [
		h * 360,
		s * 100,
		v * 100
	];
};

convert.rgb.hwb = function (rgb) {
	var r = rgb[0];
	var g = rgb[1];
	var b = rgb[2];
	var h = convert.rgb.hsl(rgb)[0];
	var w = 1 / 255 * Math.min(r, Math.min(g, b));

	b = 1 - 1 / 255 * Math.max(r, Math.max(g, b));

	return [h, w * 100, b * 100];
};

convert.rgb.cmyk = function (rgb) {
	var r = rgb[0] / 255;
	var g = rgb[1] / 255;
	var b = rgb[2] / 255;
	var c;
	var m;
	var y;
	var k;

	k = Math.min(1 - r, 1 - g, 1 - b);
	c = (1 - r - k) / (1 - k) || 0;
	m = (1 - g - k) / (1 - k) || 0;
	y = (1 - b - k) / (1 - k) || 0;

	return [c * 100, m * 100, y * 100, k * 100];
};

/**
 * See https://en.m.wikipedia.org/wiki/Euclidean_distance#Squared_Euclidean_distance
 * */
function comparativeDistance(x, y) {
	return (
		Math.pow(x[0] - y[0], 2) +
		Math.pow(x[1] - y[1], 2) +
		Math.pow(x[2] - y[2], 2)
	);
}

convert.rgb.keyword = function (rgb) {
	var reversed = reverseKeywords[rgb];
	if (reversed) {
		return reversed;
	}

	var currentClosestDistance = Infinity;
	var currentClosestKeyword;

	for (var keyword in cssKeywords) {
		if (cssKeywords.hasOwnProperty(keyword)) {
			var value = cssKeywords[keyword];

			// Compute comparative distance
			var distance = comparativeDistance(rgb, value);

			// Check if its less, if so set as closest
			if (distance < currentClosestDistance) {
				currentClosestDistance = distance;
				currentClosestKeyword = keyword;
			}
		}
	}

	return currentClosestKeyword;
};

convert.keyword.rgb = function (keyword) {
	return cssKeywords[keyword];
};

convert.rgb.xyz = function (rgb) {
	var r = rgb[0] / 255;
	var g = rgb[1] / 255;
	var b = rgb[2] / 255;

	// assume sRGB
	r = r > 0.04045 ? Math.pow(((r + 0.055) / 1.055), 2.4) : (r / 12.92);
	g = g > 0.04045 ? Math.pow(((g + 0.055) / 1.055), 2.4) : (g / 12.92);
	b = b > 0.04045 ? Math.pow(((b + 0.055) / 1.055), 2.4) : (b / 12.92);

	var x = (r * 0.4124) + (g * 0.3576) + (b * 0.1805);
	var y = (r * 0.2126) + (g * 0.7152) + (b * 0.0722);
	var z = (r * 0.0193) + (g * 0.1192) + (b * 0.9505);

	return [x * 100, y * 100, z * 100];
};

convert.rgb.lab = function (rgb) {
	var xyz = convert.rgb.xyz(rgb);
	var x = xyz[0];
	var y = xyz[1];
	var z = xyz[2];
	var l;
	var a;
	var b;

	x /= 95.047;
	y /= 100;
	z /= 108.883;

	x = x > 0.008856 ? Math.pow(x, 1 / 3) : (7.787 * x) + (16 / 116);
	y = y > 0.008856 ? Math.pow(y, 1 / 3) : (7.787 * y) + (16 / 116);
	z = z > 0.008856 ? Math.pow(z, 1 / 3) : (7.787 * z) + (16 / 116);

	l = (116 * y) - 16;
	a = 500 * (x - y);
	b = 200 * (y - z);

	return [l, a, b];
};

convert.hsl.rgb = function (hsl) {
	var h = hsl[0] / 360;
	var s = hsl[1] / 100;
	var l = hsl[2] / 100;
	var t1;
	var t2;
	var t3;
	var rgb;
	var val;

	if (s === 0) {
		val = l * 255;
		return [val, val, val];
	}

	if (l < 0.5) {
		t2 = l * (1 + s);
	} else {
		t2 = l + s - l * s;
	}

	t1 = 2 * l - t2;

	rgb = [0, 0, 0];
	for (var i = 0; i < 3; i++) {
		t3 = h + 1 / 3 * -(i - 1);
		if (t3 < 0) {
			t3++;
		}
		if (t3 > 1) {
			t3--;
		}

		if (6 * t3 < 1) {
			val = t1 + (t2 - t1) * 6 * t3;
		} else if (2 * t3 < 1) {
			val = t2;
		} else if (3 * t3 < 2) {
			val = t1 + (t2 - t1) * (2 / 3 - t3) * 6;
		} else {
			val = t1;
		}

		rgb[i] = val * 255;
	}

	return rgb;
};

convert.hsl.hsv = function (hsl) {
	var h = hsl[0];
	var s = hsl[1] / 100;
	var l = hsl[2] / 100;
	var smin = s;
	var lmin = Math.max(l, 0.01);
	var sv;
	var v;

	l *= 2;
	s *= (l <= 1) ? l : 2 - l;
	smin *= lmin <= 1 ? lmin : 2 - lmin;
	v = (l + s) / 2;
	sv = l === 0 ? (2 * smin) / (lmin + smin) : (2 * s) / (l + s);

	return [h, sv * 100, v * 100];
};

convert.hsv.rgb = function (hsv) {
	var h = hsv[0] / 60;
	var s = hsv[1] / 100;
	var v = hsv[2] / 100;
	var hi = Math.floor(h) % 6;

	var f = h - Math.floor(h);
	var p = 255 * v * (1 - s);
	var q = 255 * v * (1 - (s * f));
	var t = 255 * v * (1 - (s * (1 - f)));
	v *= 255;

	switch (hi) {
		case 0:
			return [v, t, p];
		case 1:
			return [q, v, p];
		case 2:
			return [p, v, t];
		case 3:
			return [p, q, v];
		case 4:
			return [t, p, v];
		case 5:
			return [v, p, q];
	}
};

convert.hsv.hsl = function (hsv) {
	var h = hsv[0];
	var s = hsv[1] / 100;
	var v = hsv[2] / 100;
	var vmin = Math.max(v, 0.01);
	var lmin;
	var sl;
	var l;

	l = (2 - s) * v;
	lmin = (2 - s) * vmin;
	sl = s * vmin;
	sl /= (lmin <= 1) ? lmin : 2 - lmin;
	sl = sl || 0;
	l /= 2;

	return [h, sl * 100, l * 100];
};

// http://dev.w3.org/csswg/css-color/#hwb-to-rgb
convert.hwb.rgb = function (hwb) {
	var h = hwb[0] / 360;
	var wh = hwb[1] / 100;
	var bl = hwb[2] / 100;
	var ratio = wh + bl;
	var i;
	var v;
	var f;
	var n;

	// wh + bl cant be > 1
	if (ratio > 1) {
		wh /= ratio;
		bl /= ratio;
	}

	i = Math.floor(6 * h);
	v = 1 - bl;
	f = 6 * h - i;

	if ((i & 0x01) !== 0) {
		f = 1 - f;
	}

	n = wh + f * (v - wh); // linear interpolation

	var r;
	var g;
	var b;
	switch (i) {
		default:
		case 6:
		case 0: r = v; g = n; b = wh; break;
		case 1: r = n; g = v; b = wh; break;
		case 2: r = wh; g = v; b = n; break;
		case 3: r = wh; g = n; b = v; break;
		case 4: r = n; g = wh; b = v; break;
		case 5: r = v; g = wh; b = n; break;
	}

	return [r * 255, g * 255, b * 255];
};

convert.cmyk.rgb = function (cmyk) {
	var c = cmyk[0] / 100;
	var m = cmyk[1] / 100;
	var y = cmyk[2] / 100;
	var k = cmyk[3] / 100;
	var r;
	var g;
	var b;

	r = 1 - Math.min(1, c * (1 - k) + k);
	g = 1 - Math.min(1, m * (1 - k) + k);
	b = 1 - Math.min(1, y * (1 - k) + k);

	return [r * 255, g * 255, b * 255];
};

convert.xyz.rgb = function (xyz) {
	var x = xyz[0] / 100;
	var y = xyz[1] / 100;
	var z = xyz[2] / 100;
	var r;
	var g;
	var b;

	r = (x * 3.2406) + (y * -1.5372) + (z * -0.4986);
	g = (x * -0.9689) + (y * 1.8758) + (z * 0.0415);
	b = (x * 0.0557) + (y * -0.2040) + (z * 1.0570);

	// assume sRGB
	r = r > 0.0031308
		? ((1.055 * Math.pow(r, 1.0 / 2.4)) - 0.055)
		: r * 12.92;

	g = g > 0.0031308
		? ((1.055 * Math.pow(g, 1.0 / 2.4)) - 0.055)
		: g * 12.92;

	b = b > 0.0031308
		? ((1.055 * Math.pow(b, 1.0 / 2.4)) - 0.055)
		: b * 12.92;

	r = Math.min(Math.max(0, r), 1);
	g = Math.min(Math.max(0, g), 1);
	b = Math.min(Math.max(0, b), 1);

	return [r * 255, g * 255, b * 255];
};

convert.xyz.lab = function (xyz) {
	var x = xyz[0];
	var y = xyz[1];
	var z = xyz[2];
	var l;
	var a;
	var b;

	x /= 95.047;
	y /= 100;
	z /= 108.883;

	x = x > 0.008856 ? Math.pow(x, 1 / 3) : (7.787 * x) + (16 / 116);
	y = y > 0.008856 ? Math.pow(y, 1 / 3) : (7.787 * y) + (16 / 116);
	z = z > 0.008856 ? Math.pow(z, 1 / 3) : (7.787 * z) + (16 / 116);

	l = (116 * y) - 16;
	a = 500 * (x - y);
	b = 200 * (y - z);

	return [l, a, b];
};

convert.lab.xyz = function (lab) {
	var l = lab[0];
	var a = lab[1];
	var b = lab[2];
	var x;
	var y;
	var z;

	y = (l + 16) / 116;
	x = a / 500 + y;
	z = y - b / 200;

	var y2 = Math.pow(y, 3);
	var x2 = Math.pow(x, 3);
	var z2 = Math.pow(z, 3);
	y = y2 > 0.008856 ? y2 : (y - 16 / 116) / 7.787;
	x = x2 > 0.008856 ? x2 : (x - 16 / 116) / 7.787;
	z = z2 > 0.008856 ? z2 : (z - 16 / 116) / 7.787;

	x *= 95.047;
	y *= 100;
	z *= 108.883;

	return [x, y, z];
};

convert.lab.lch = function (lab) {
	var l = lab[0];
	var a = lab[1];
	var b = lab[2];
	var hr;
	var h;
	var c;

	hr = Math.atan2(b, a);
	h = hr * 360 / 2 / Math.PI;

	if (h < 0) {
		h += 360;
	}

	c = Math.sqrt(a * a + b * b);

	return [l, c, h];
};

convert.lch.lab = function (lch) {
	var l = lch[0];
	var c = lch[1];
	var h = lch[2];
	var a;
	var b;
	var hr;

	hr = h / 360 * 2 * Math.PI;
	a = c * Math.cos(hr);
	b = c * Math.sin(hr);

	return [l, a, b];
};

convert.rgb.ansi16 = function (args) {
	var r = args[0];
	var g = args[1];
	var b = args[2];
	var value = 1 in arguments ? arguments[1] : convert.rgb.hsv(args)[2]; // hsv -> ansi16 optimization

	value = Math.round(value / 50);

	if (value === 0) {
		return 30;
	}

	var ansi = 30
		+ ((Math.round(b / 255) << 2)
		| (Math.round(g / 255) << 1)
		| Math.round(r / 255));

	if (value === 2) {
		ansi += 60;
	}

	return ansi;
};

convert.hsv.ansi16 = function (args) {
	// optimization here; we already know the value and don't need to get
	// it converted for us.
	return convert.rgb.ansi16(convert.hsv.rgb(args), args[2]);
};

convert.rgb.ansi256 = function (args) {
	var r = args[0];
	var g = args[1];
	var b = args[2];

	// we use the extended greyscale palette here, with the exception of
	// black and white. normal palette only has 4 greyscale shades.
	if (r === g && g === b) {
		if (r < 8) {
			return 16;
		}

		if (r > 248) {
			return 231;
		}

		return Math.round(((r - 8) / 247) * 24) + 232;
	}

	var ansi = 16
		+ (36 * Math.round(r / 255 * 5))
		+ (6 * Math.round(g / 255 * 5))
		+ Math.round(b / 255 * 5);

	return ansi;
};

convert.ansi16.rgb = function (args) {
	var color = args % 10;

	// handle greyscale
	if (color === 0 || color === 7) {
		if (args > 50) {
			color += 3.5;
		}

		color = color / 10.5 * 255;

		return [color, color, color];
	}

	var mult = (~~(args > 50) + 1) * 0.5;
	var r = ((color & 1) * mult) * 255;
	var g = (((color >> 1) & 1) * mult) * 255;
	var b = (((color >> 2) & 1) * mult) * 255;

	return [r, g, b];
};

convert.ansi256.rgb = function (args) {
	// handle greyscale
	if (args >= 232) {
		var c = (args - 232) * 10 + 8;
		return [c, c, c];
	}

	args -= 16;

	var rem;
	var r = Math.floor(args / 36) / 5 * 255;
	var g = Math.floor((rem = args % 36) / 6) / 5 * 255;
	var b = (rem % 6) / 5 * 255;

	return [r, g, b];
};

convert.rgb.hex = function (args) {
	var integer = ((Math.round(args[0]) & 0xFF) << 16)
		+ ((Math.round(args[1]) & 0xFF) << 8)
		+ (Math.round(args[2]) & 0xFF);

	var string = integer.toString(16).toUpperCase();
	return '000000'.substring(string.length) + string;
};

convert.hex.rgb = function (args) {
	var match = args.toString(16).match(/[a-f0-9]{6}|[a-f0-9]{3}/i);
	if (!match) {
		return [0, 0, 0];
	}

	var colorString = match[0];

	if (match[0].length === 3) {
		colorString = colorString.split('').map(function (char) {
			return char + char;
		}).join('');
	}

	var integer = parseInt(colorString, 16);
	var r = (integer >> 16) & 0xFF;
	var g = (integer >> 8) & 0xFF;
	var b = integer & 0xFF;

	return [r, g, b];
};

convert.rgb.hcg = function (rgb) {
	var r = rgb[0] / 255;
	var g = rgb[1] / 255;
	var b = rgb[2] / 255;
	var max = Math.max(Math.max(r, g), b);
	var min = Math.min(Math.min(r, g), b);
	var chroma = (max - min);
	var grayscale;
	var hue;

	if (chroma < 1) {
		grayscale = min / (1 - chroma);
	} else {
		grayscale = 0;
	}

	if (chroma <= 0) {
		hue = 0;
	} else
	if (max === r) {
		hue = ((g - b) / chroma) % 6;
	} else
	if (max === g) {
		hue = 2 + (b - r) / chroma;
	} else {
		hue = 4 + (r - g) / chroma + 4;
	}

	hue /= 6;
	hue %= 1;

	return [hue * 360, chroma * 100, grayscale * 100];
};

convert.hsl.hcg = function (hsl) {
	var s = hsl[1] / 100;
	var l = hsl[2] / 100;
	var c = 1;
	var f = 0;

	if (l < 0.5) {
		c = 2.0 * s * l;
	} else {
		c = 2.0 * s * (1.0 - l);
	}

	if (c < 1.0) {
		f = (l - 0.5 * c) / (1.0 - c);
	}

	return [hsl[0], c * 100, f * 100];
};

convert.hsv.hcg = function (hsv) {
	var s = hsv[1] / 100;
	var v = hsv[2] / 100;

	var c = s * v;
	var f = 0;

	if (c < 1.0) {
		f = (v - c) / (1 - c);
	}

	return [hsv[0], c * 100, f * 100];
};

convert.hcg.rgb = function (hcg) {
	var h = hcg[0] / 360;
	var c = hcg[1] / 100;
	var g = hcg[2] / 100;

	if (c === 0.0) {
		return [g * 255, g * 255, g * 255];
	}

	var pure = [0, 0, 0];
	var hi = (h % 1) * 6;
	var v = hi % 1;
	var w = 1 - v;
	var mg = 0;

	switch (Math.floor(hi)) {
		case 0:
			pure[0] = 1; pure[1] = v; pure[2] = 0; break;
		case 1:
			pure[0] = w; pure[1] = 1; pure[2] = 0; break;
		case 2:
			pure[0] = 0; pure[1] = 1; pure[2] = v; break;
		case 3:
			pure[0] = 0; pure[1] = w; pure[2] = 1; break;
		case 4:
			pure[0] = v; pure[1] = 0; pure[2] = 1; break;
		default:
			pure[0] = 1; pure[1] = 0; pure[2] = w;
	}

	mg = (1.0 - c) * g;

	return [
		(c * pure[0] + mg) * 255,
		(c * pure[1] + mg) * 255,
		(c * pure[2] + mg) * 255
	];
};

convert.hcg.hsv = function (hcg) {
	var c = hcg[1] / 100;
	var g = hcg[2] / 100;

	var v = c + g * (1.0 - c);
	var f = 0;

	if (v > 0.0) {
		f = c / v;
	}

	return [hcg[0], f * 100, v * 100];
};

convert.hcg.hsl = function (hcg) {
	var c = hcg[1] / 100;
	var g = hcg[2] / 100;

	var l = g * (1.0 - c) + 0.5 * c;
	var s = 0;

	if (l > 0.0 && l < 0.5) {
		s = c / (2 * l);
	} else
	if (l >= 0.5 && l < 1.0) {
		s = c / (2 * (1 - l));
	}

	return [hcg[0], s * 100, l * 100];
};

convert.hcg.hwb = function (hcg) {
	var c = hcg[1] / 100;
	var g = hcg[2] / 100;
	var v = c + g * (1.0 - c);
	return [hcg[0], (v - c) * 100, (1 - v) * 100];
};

convert.hwb.hcg = function (hwb) {
	var w = hwb[1] / 100;
	var b = hwb[2] / 100;
	var v = 1 - b;
	var c = v - w;
	var g = 0;

	if (c < 1) {
		g = (v - c) / (1 - c);
	}

	return [hwb[0], c * 100, g * 100];
};

convert.apple.rgb = function (apple) {
	return [(apple[0] / 65535) * 255, (apple[1] / 65535) * 255, (apple[2] / 65535) * 255];
};

convert.rgb.apple = function (rgb) {
	return [(rgb[0] / 255) * 65535, (rgb[1] / 255) * 65535, (rgb[2] / 255) * 65535];
};

convert.gray.rgb = function (args) {
	return [args[0] / 100 * 255, args[0] / 100 * 255, args[0] / 100 * 255];
};

convert.gray.hsl = convert.gray.hsv = function (args) {
	return [0, 0, args[0]];
};

convert.gray.hwb = function (gray) {
	return [0, 100, gray[0]];
};

convert.gray.cmyk = function (gray) {
	return [0, 0, 0, gray[0]];
};

convert.gray.lab = function (gray) {
	return [gray[0], 0, 0];
};

convert.gray.hex = function (gray) {
	var val = Math.round(gray[0] / 100 * 255) & 0xFF;
	var integer = (val << 16) + (val << 8) + val;

	var string = integer.toString(16).toUpperCase();
	return '000000'.substring(string.length) + string;
};

convert.rgb.gray = function (rgb) {
	var val = (rgb[0] + rgb[1] + rgb[2]) / 3;
	return [val / 255 * 100];
};


/***/ }),
/* 7 */
/***/ (function(module, exports, __nested_webpack_require_65754__) {

"use strict";


var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _Colorpicker = __nested_webpack_require_65754__(8);

var _Colorpicker2 = _interopRequireDefault(_Colorpicker);

var _jquery = __nested_webpack_require_65754__(0);

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var plugin = 'colorpicker';

_jquery2.default[plugin] = _Colorpicker2.default;

// Colorpicker jQuery Plugin API
_jquery2.default.fn[plugin] = function (option) {
  var fnArgs = Array.prototype.slice.call(arguments, 1),
      isSingleElement = this.length === 1,
      returnValue = null;

  var $elements = this.each(function () {
    var $this = (0, _jquery2.default)(this),
        inst = $this.data(plugin),
        options = (typeof option === 'undefined' ? 'undefined' : _typeof(option)) === 'object' ? option : {};

    // Create instance if does not exist
    if (!inst) {
      inst = new _Colorpicker2.default(this, options);
      $this.data(plugin, inst);
    }

    if (!isSingleElement) {
      return;
    }

    returnValue = $this;

    if (typeof option === 'string') {
      if (option === 'colorpicker') {
        // Return colorpicker instance: e.g. .colorpicker('colorpicker')
        returnValue = inst;
      } else if (_jquery2.default.isFunction(inst[option])) {
        // Return method call return value: e.g. .colorpicker('isEnabled')
        returnValue = inst[option].apply(inst, fnArgs);
      } else {
        // Return property value: e.g. .colorpicker('element')
        returnValue = inst[option];
      }
    }
  });

  return isSingleElement ? returnValue : $elements;
};

_jquery2.default.fn[plugin].constructor = _Colorpicker2.default;

/***/ }),
/* 8 */
/***/ (function(module, exports, __nested_webpack_require_67781__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Extension = __nested_webpack_require_67781__(1);

var _Extension2 = _interopRequireDefault(_Extension);

var _options = __nested_webpack_require_67781__(3);

var _options2 = _interopRequireDefault(_options);

var _extensions = __nested_webpack_require_67781__(9);

var _extensions2 = _interopRequireDefault(_extensions);

var _jquery = __nested_webpack_require_67781__(0);

var _jquery2 = _interopRequireDefault(_jquery);

var _SliderHandler = __nested_webpack_require_67781__(13);

var _SliderHandler2 = _interopRequireDefault(_SliderHandler);

var _PopupHandler = __nested_webpack_require_67781__(14);

var _PopupHandler2 = _interopRequireDefault(_PopupHandler);

var _InputHandler = __nested_webpack_require_67781__(15);

var _InputHandler2 = _interopRequireDefault(_InputHandler);

var _ColorHandler = __nested_webpack_require_67781__(22);

var _ColorHandler2 = _interopRequireDefault(_ColorHandler);

var _PickerHandler = __nested_webpack_require_67781__(23);

var _PickerHandler2 = _interopRequireDefault(_PickerHandler);

var _AddonHandler = __nested_webpack_require_67781__(24);

var _AddonHandler2 = _interopRequireDefault(_AddonHandler);

var _ColorItem = __nested_webpack_require_67781__(2);

var _ColorItem2 = _interopRequireDefault(_ColorItem);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var colorPickerIdCounter = 0;

var root = typeof self !== 'undefined' ? self : undefined; // window

/**
 * Colorpicker widget class
 */

var Colorpicker = function () {
  _createClass(Colorpicker, [{
    key: 'color',


    /**
     * Internal color object
     *
     * @type {Color|null}
     */
    get: function get() {
      return this.colorHandler.color;
    }

    /**
     * Internal color format
     *
     * @type {String|null}
     */

  }, {
    key: 'format',
    get: function get() {
      return this.colorHandler.format;
    }

    /**
     * Getter of the picker element
     *
     * @returns {jQuery|HTMLElement}
     */

  }, {
    key: 'picker',
    get: function get() {
      return this.pickerHandler.picker;
    }

    /**
     * @fires Colorpicker#colorpickerCreate
     * @param {Object|String} element
     * @param {Object} options
     * @constructor
     */

  }], [{
    key: 'Color',

    /**
     * Color class
     *
     * @static
     * @type {Color}
     */
    get: function get() {
      return _ColorItem2.default;
    }

    /**
     * Extension class
     *
     * @static
     * @type {Extension}
     */

  }, {
    key: 'Extension',
    get: function get() {
      return _Extension2.default;
    }
  }]);

  function Colorpicker(element, options) {
    _classCallCheck(this, Colorpicker);

    colorPickerIdCounter += 1;
    /**
     * The colorpicker instance number
     * @type {number}
     */
    this.id = colorPickerIdCounter;

    /**
     * Latest colorpicker event
     *
     * @type {{name: String, e: *}}
     */
    this.lastEvent = {
      alias: null,
      e: null
    };

    /**
     * The element that the colorpicker is bound to
     *
     * @type {*|jQuery}
     */
    this.element = (0, _jquery2.default)(element).addClass('colorpicker-element').attr('data-colorpicker-id', this.id);

    /**
     * @type {defaults}
     */
    this.options = _jquery2.default.extend(true, {}, _options2.default, options, this.element.data());

    /**
     * @type {boolean}
     * @private
     */
    this.disabled = false;

    /**
     * Extensions added to this instance
     *
     * @type {Extension[]}
     */
    this.extensions = [];

    /**
     * The element where the
     * @type {*|jQuery}
     */
    this.container = this.options.container === true || this.options.container !== true && this.options.inline === true ? this.element : this.options.container;

    this.container = this.container !== false ? (0, _jquery2.default)(this.container) : false;

    /**
     * @type {InputHandler}
     */
    this.inputHandler = new _InputHandler2.default(this);
    /**
     * @type {ColorHandler}
     */
    this.colorHandler = new _ColorHandler2.default(this);
    /**
     * @type {SliderHandler}
     */
    this.sliderHandler = new _SliderHandler2.default(this);
    /**
     * @type {PopupHandler}
     */
    this.popupHandler = new _PopupHandler2.default(this, root);
    /**
     * @type {PickerHandler}
     */
    this.pickerHandler = new _PickerHandler2.default(this);
    /**
     * @type {AddonHandler}
     */
    this.addonHandler = new _AddonHandler2.default(this);

    this.init();

    // Emit a create event
    (0, _jquery2.default)(_jquery2.default.proxy(function () {
      /**
       * (Colorpicker) When the Colorpicker instance has been created and the DOM is ready.
       *
       * @event Colorpicker#colorpickerCreate
       */
      this.trigger('colorpickerCreate');
    }, this));
  }

  /**
   * Initializes the plugin
   * @private
   */


  _createClass(Colorpicker, [{
    key: 'init',
    value: function init() {
      // Init addon
      this.addonHandler.bind();

      // Init input
      this.inputHandler.bind();

      // Init extensions (before initializing the color)
      this.initExtensions();

      // Init color
      this.colorHandler.bind();

      // Init picker
      this.pickerHandler.bind();

      // Init sliders and popup
      this.sliderHandler.bind();
      this.popupHandler.bind();

      // Inject into the DOM (this may make it visible)
      this.pickerHandler.attach();

      // Update all components
      this.update();

      if (this.inputHandler.isDisabled()) {
        this.disable();
      }
    }

    /**
     * Initializes the plugin extensions
     * @private
     */

  }, {
    key: 'initExtensions',
    value: function initExtensions() {
      var _this = this;

      if (!Array.isArray(this.options.extensions)) {
        this.options.extensions = [];
      }

      if (this.options.debug) {
        this.options.extensions.push({ name: 'debugger' });
      }

      // Register and instantiate extensions
      this.options.extensions.forEach(function (ext) {
        _this.registerExtension(Colorpicker.extensions[ext.name.toLowerCase()], ext.options || {});
      });
    }

    /**
     * Creates and registers the given extension
     *
     * @param {Extension} ExtensionClass The extension class to instantiate
     * @param {Object} [config] Extension configuration
     * @returns {Extension}
     */

  }, {
    key: 'registerExtension',
    value: function registerExtension(ExtensionClass) {
      var config = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

      var ext = new ExtensionClass(this, config);

      this.extensions.push(ext);
      return ext;
    }

    /**
     * Destroys the current instance
     *
     * @fires Colorpicker#colorpickerDestroy
     */

  }, {
    key: 'destroy',
    value: function destroy() {
      var color = this.color;

      this.sliderHandler.unbind();
      this.inputHandler.unbind();
      this.popupHandler.unbind();
      this.colorHandler.unbind();
      this.addonHandler.unbind();
      this.pickerHandler.unbind();

      this.element.removeClass('colorpicker-element').removeData('colorpicker', 'color').off('.colorpicker');

      /**
       * (Colorpicker) When the instance is destroyed with all events unbound.
       *
       * @event Colorpicker#colorpickerDestroy
       */
      this.trigger('colorpickerDestroy', color);
    }

    /**
     * Shows the colorpicker widget if hidden.
     * If the colorpicker is disabled this call will be ignored.
     *
     * @fires Colorpicker#colorpickerShow
     * @param {Event} [e]
     */

  }, {
    key: 'show',
    value: function show(e) {
      this.popupHandler.show(e);
    }

    /**
     * Hides the colorpicker widget.
     *
     * @fires Colorpicker#colorpickerHide
     * @param {Event} [e]
     */

  }, {
    key: 'hide',
    value: function hide(e) {
      this.popupHandler.hide(e);
    }

    /**
     * Toggles the colorpicker between visible and hidden.
     *
     * @fires Colorpicker#colorpickerShow
     * @fires Colorpicker#colorpickerHide
     * @param {Event} [e]
     */

  }, {
    key: 'toggle',
    value: function toggle(e) {
      this.popupHandler.toggle(e);
    }

    /**
     * Returns the current color value as string
     *
     * @param {String|*} [defaultValue]
     * @returns {String|*}
     */

  }, {
    key: 'getValue',
    value: function getValue() {
      var defaultValue = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;

      var val = this.colorHandler.color;

      val = val instanceof _ColorItem2.default ? val : defaultValue;

      if (val instanceof _ColorItem2.default) {
        return val.string(this.format);
      }

      return val;
    }

    /**
     * Sets the color manually
     *
     * @fires Colorpicker#colorpickerChange
     * @param {String|Color} val
     */

  }, {
    key: 'setValue',
    value: function setValue(val) {
      if (this.isDisabled()) {
        return;
      }
      var ch = this.colorHandler;

      if (ch.hasColor() && !!val && ch.color.equals(val) || !ch.hasColor() && !val) {
        // same color or still empty
        return;
      }

      ch.color = val ? ch.createColor(val, this.options.autoInputFallback) : null;

      /**
       * (Colorpicker) When the color is set programmatically with setValue().
       *
       * @event Colorpicker#colorpickerChange
       */
      this.trigger('colorpickerChange', ch.color, val);

      // force update if color has changed to empty
      this.update();
    }

    /**
     * Updates the UI and the input color according to the internal color.
     *
     * @fires Colorpicker#colorpickerUpdate
     */

  }, {
    key: 'update',
    value: function update() {
      if (this.colorHandler.hasColor()) {
        this.inputHandler.update();
      } else {
        this.colorHandler.assureColor();
      }

      this.addonHandler.update();
      this.pickerHandler.update();

      /**
       * (Colorpicker) Fired when the widget is updated.
       *
       * @event Colorpicker#colorpickerUpdate
       */
      this.trigger('colorpickerUpdate');
    }

    /**
     * Enables the widget and the input if any
     *
     * @fires Colorpicker#colorpickerEnable
     * @returns {boolean}
     */

  }, {
    key: 'enable',
    value: function enable() {
      this.inputHandler.enable();
      this.disabled = false;
      this.picker.removeClass('colorpicker-disabled');

      /**
       * (Colorpicker) When the widget has been enabled.
       *
       * @event Colorpicker#colorpickerEnable
       */
      this.trigger('colorpickerEnable');
      return true;
    }

    /**
     * Disables the widget and the input if any
     *
     * @fires Colorpicker#colorpickerDisable
     * @returns {boolean}
     */

  }, {
    key: 'disable',
    value: function disable() {
      this.inputHandler.disable();
      this.disabled = true;
      this.picker.addClass('colorpicker-disabled');

      /**
       * (Colorpicker) When the widget has been disabled.
       *
       * @event Colorpicker#colorpickerDisable
       */
      this.trigger('colorpickerDisable');
      return true;
    }

    /**
     * Returns true if this instance is enabled
     * @returns {boolean}
     */

  }, {
    key: 'isEnabled',
    value: function isEnabled() {
      return !this.isDisabled();
    }

    /**
     * Returns true if this instance is disabled
     * @returns {boolean}
     */

  }, {
    key: 'isDisabled',
    value: function isDisabled() {
      return this.disabled === true;
    }

    /**
     * Triggers a Colorpicker event.
     *
     * @param eventName
     * @param color
     * @param value
     */

  }, {
    key: 'trigger',
    value: function trigger(eventName) {
      var color = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
      var value = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;

      this.element.trigger({
        type: eventName,
        colorpicker: this,
        color: color ? color : this.color,
        value: value ? value : this.getValue()
      });
    }
  }]);

  return Colorpicker;
}();

/**
 * Colorpicker extension classes, indexed by extension name
 *
 * @static
 * @type {Object} a map between the extension name and its class
 */


Colorpicker.extensions = _extensions2.default;

exports.default = Colorpicker;
module.exports = exports.default;

/***/ }),
/* 9 */
/***/ (function(module, exports, __nested_webpack_require_81019__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Palette = exports.Swatches = exports.Preview = exports.Debugger = undefined;

var _Debugger = __nested_webpack_require_81019__(10);

var _Debugger2 = _interopRequireDefault(_Debugger);

var _Preview = __nested_webpack_require_81019__(11);

var _Preview2 = _interopRequireDefault(_Preview);

var _Swatches = __nested_webpack_require_81019__(12);

var _Swatches2 = _interopRequireDefault(_Swatches);

var _Palette = __nested_webpack_require_81019__(4);

var _Palette2 = _interopRequireDefault(_Palette);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.Debugger = _Debugger2.default;
exports.Preview = _Preview2.default;
exports.Swatches = _Swatches2.default;
exports.Palette = _Palette2.default;
exports.default = {
  'debugger': _Debugger2.default,
  'preview': _Preview2.default,
  'swatches': _Swatches2.default,
  'palette': _Palette2.default
};

/***/ }),
/* 10 */
/***/ (function(module, exports, __nested_webpack_require_82039__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _Extension2 = __nested_webpack_require_82039__(1);

var _Extension3 = _interopRequireDefault(_Extension2);

var _jquery = __nested_webpack_require_82039__(0);

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/**
 * Debugger extension class
 * @alias DebuggerExtension
 * @ignore
 */
var Debugger = function (_Extension) {
  _inherits(Debugger, _Extension);

  function Debugger(colorpicker) {
    var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

    _classCallCheck(this, Debugger);

    /**
     * @type {number}
     */
    var _this = _possibleConstructorReturn(this, (Debugger.__proto__ || Object.getPrototypeOf(Debugger)).call(this, colorpicker, options));

    _this.eventCounter = 0;
    if (_this.colorpicker.inputHandler.hasInput()) {
      _this.colorpicker.inputHandler.input.on('change.colorpicker-ext', _jquery2.default.proxy(_this.onChangeInput, _this));
    }
    return _this;
  }

  /**
   * @fires DebuggerExtension#colorpickerDebug
   * @param {string} eventName
   * @param {*} args
   */


  _createClass(Debugger, [{
    key: 'log',
    value: function log(eventName) {
      var _console;

      for (var _len = arguments.length, args = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
        args[_key - 1] = arguments[_key];
      }

      this.eventCounter += 1;

      var logMessage = '#' + this.eventCounter + ': Colorpicker#' + this.colorpicker.id + ' [' + eventName + ']';

      (_console = console).debug.apply(_console, [logMessage].concat(args));

      /**
       * Whenever the debugger logs an event, this other event is emitted.
       *
       * @event DebuggerExtension#colorpickerDebug
       * @type {object} The event object
       * @property {Colorpicker} colorpicker The Colorpicker instance
       * @property {ColorItem} color The color instance
       * @property {{debugger: DebuggerExtension, eventName: String, logArgs: Array, logMessage: String}} debug
       *  The debug info
       */
      this.colorpicker.element.trigger({
        type: 'colorpickerDebug',
        colorpicker: this.colorpicker,
        color: this.color,
        value: null,
        debug: {
          debugger: this,
          eventName: eventName,
          logArgs: args,
          logMessage: logMessage
        }
      });
    }
  }, {
    key: 'resolveColor',
    value: function resolveColor(color) {
      var realColor = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

      this.log('resolveColor()', color, realColor);
      return false;
    }
  }, {
    key: 'onCreate',
    value: function onCreate(event) {
      this.log('colorpickerCreate');
      return _get(Debugger.prototype.__proto__ || Object.getPrototypeOf(Debugger.prototype), 'onCreate', this).call(this, event);
    }
  }, {
    key: 'onDestroy',
    value: function onDestroy(event) {
      this.log('colorpickerDestroy');
      this.eventCounter = 0;

      if (this.colorpicker.inputHandler.hasInput()) {
        this.colorpicker.inputHandler.input.off('.colorpicker-ext');
      }

      return _get(Debugger.prototype.__proto__ || Object.getPrototypeOf(Debugger.prototype), 'onDestroy', this).call(this, event);
    }
  }, {
    key: 'onUpdate',
    value: function onUpdate(event) {
      this.log('colorpickerUpdate');
    }

    /**
     * @listens Colorpicker#change
     * @param {Event} event
     */

  }, {
    key: 'onChangeInput',
    value: function onChangeInput(event) {
      this.log('input:change.colorpicker', event.value, event.color);
    }
  }, {
    key: 'onChange',
    value: function onChange(event) {
      this.log('colorpickerChange', event.value, event.color);
    }
  }, {
    key: 'onInvalid',
    value: function onInvalid(event) {
      this.log('colorpickerInvalid', event.value, event.color);
    }
  }, {
    key: 'onHide',
    value: function onHide(event) {
      this.log('colorpickerHide');
      this.eventCounter = 0;
    }
  }, {
    key: 'onShow',
    value: function onShow(event) {
      this.log('colorpickerShow');
    }
  }, {
    key: 'onDisable',
    value: function onDisable(event) {
      this.log('colorpickerDisable');
    }
  }, {
    key: 'onEnable',
    value: function onEnable(event) {
      this.log('colorpickerEnable');
    }
  }]);

  return Debugger;
}(_Extension3.default);

exports.default = Debugger;
module.exports = exports.default;

/***/ }),
/* 11 */
/***/ (function(module, exports, __nested_webpack_require_88622__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _Extension2 = __nested_webpack_require_88622__(1);

var _Extension3 = _interopRequireDefault(_Extension2);

var _jquery = __nested_webpack_require_88622__(0);

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/**
 * Color preview extension
 * @ignore
 */
var Preview = function (_Extension) {
  _inherits(Preview, _Extension);

  function Preview(colorpicker) {
    var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

    _classCallCheck(this, Preview);

    var _this = _possibleConstructorReturn(this, (Preview.__proto__ || Object.getPrototypeOf(Preview)).call(this, colorpicker, _jquery2.default.extend(true, {}, {
      template: '<div class="colorpicker-bar colorpicker-preview"><div /></div>',
      showText: true,
      format: colorpicker.format
    }, options)));

    _this.element = (0, _jquery2.default)(_this.options.template);
    _this.elementInner = _this.element.find('div');
    return _this;
  }

  _createClass(Preview, [{
    key: 'onCreate',
    value: function onCreate(event) {
      _get(Preview.prototype.__proto__ || Object.getPrototypeOf(Preview.prototype), 'onCreate', this).call(this, event);
      this.colorpicker.picker.append(this.element);
    }
  }, {
    key: 'onUpdate',
    value: function onUpdate(event) {
      _get(Preview.prototype.__proto__ || Object.getPrototypeOf(Preview.prototype), 'onUpdate', this).call(this, event);

      if (!event.color) {
        this.elementInner.css('backgroundColor', null).css('color', null).html('');
        return;
      }

      this.elementInner.css('backgroundColor', event.color.toRgbString());

      if (this.options.showText) {
        this.elementInner.html(event.color.string(this.options.format || this.colorpicker.format));

        if (event.color.isDark() && event.color.alpha > 0.5) {
          this.elementInner.css('color', 'white');
        } else {
          this.elementInner.css('color', 'black');
        }
      }
    }
  }]);

  return Preview;
}(_Extension3.default);

exports.default = Preview;
module.exports = exports.default;

/***/ }),
/* 12 */
/***/ (function(module, exports, __nested_webpack_require_92878__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _Palette2 = __nested_webpack_require_92878__(4);

var _Palette3 = _interopRequireDefault(_Palette2);

var _jquery = __nested_webpack_require_92878__(0);

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var defaults = {
  barTemplate: '<div class="colorpicker-bar colorpicker-swatches">\n                    <div class="colorpicker-swatches--inner"></div>\n                </div>',
  swatchTemplate: '<i class="colorpicker-swatch"><i class="colorpicker-swatch--inner"></i></i>'
};

/**
 * Color swatches extension
 * @ignore
 */

var Swatches = function (_Palette) {
  _inherits(Swatches, _Palette);

  function Swatches(colorpicker) {
    var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

    _classCallCheck(this, Swatches);

    var _this = _possibleConstructorReturn(this, (Swatches.__proto__ || Object.getPrototypeOf(Swatches)).call(this, colorpicker, _jquery2.default.extend(true, {}, defaults, options)));

    _this.element = null;
    return _this;
  }

  _createClass(Swatches, [{
    key: 'isEnabled',
    value: function isEnabled() {
      return this.getLength() > 0;
    }
  }, {
    key: 'onCreate',
    value: function onCreate(event) {
      _get(Swatches.prototype.__proto__ || Object.getPrototypeOf(Swatches.prototype), 'onCreate', this).call(this, event);

      if (!this.isEnabled()) {
        return;
      }

      this.element = (0, _jquery2.default)(this.options.barTemplate);
      this.load();
      this.colorpicker.picker.append(this.element);
    }
  }, {
    key: 'load',
    value: function load() {
      var _this2 = this;

      var colorpicker = this.colorpicker,
          swatchContainer = this.element.find('.colorpicker-swatches--inner'),
          isAliased = this.options.namesAsValues === true && !Array.isArray(this.colors);

      swatchContainer.empty();

      _jquery2.default.each(this.colors, function (name, value) {
        var $swatch = (0, _jquery2.default)(_this2.options.swatchTemplate).attr('data-name', name).attr('data-value', value).attr('title', isAliased ? name + ': ' + value : value).on('mousedown.colorpicker touchstart.colorpicker', function (e) {
          var $sw = (0, _jquery2.default)(this);

          // e.preventDefault();

          colorpicker.setValue(isAliased ? $sw.attr('data-name') : $sw.attr('data-value'));
        });

        $swatch.find('.colorpicker-swatch--inner').css('background-color', value);

        swatchContainer.append($swatch);
      });

      swatchContainer.append((0, _jquery2.default)('<i class="colorpicker-clear"></i>'));
    }
  }]);

  return Swatches;
}(_Palette3.default);

exports.default = Swatches;
module.exports = exports.default;

/***/ }),
/* 13 */
/***/ (function(module, exports, __nested_webpack_require_97750__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _jquery = __nested_webpack_require_97750__(0);

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * Class that handles all configured sliders on mouse or touch events.
 * @ignore
 */
var SliderHandler = function () {
  /**
   * @param {Colorpicker} colorpicker
   */
  function SliderHandler(colorpicker) {
    _classCallCheck(this, SliderHandler);

    /**
     * @type {Colorpicker}
     */
    this.colorpicker = colorpicker;
    /**
     * @type {*|String}
     * @private
     */
    this.currentSlider = null;
    /**
     * @type {{left: number, top: number}}
     * @private
     */
    this.mousePointer = {
      left: 0,
      top: 0
    };

    /**
     * @type {Function}
     */
    this.onMove = _jquery2.default.proxy(this.defaultOnMove, this);
  }

  /**
   * This function is called every time a slider guide is moved
   * The scope of "this" is the SliderHandler object.
   *
   * @param {int} top
   * @param {int} left
   */


  _createClass(SliderHandler, [{
    key: 'defaultOnMove',
    value: function defaultOnMove(top, left) {
      if (!this.currentSlider) {
        return;
      }

      var slider = this.currentSlider,
          cp = this.colorpicker,
          ch = cp.colorHandler;

      // Create a color object
      var color = !ch.hasColor() ? ch.getFallbackColor() : ch.color.getClone();

      // Adjust the guide position
      slider.guideStyle.left = left + 'px';
      slider.guideStyle.top = top + 'px';

      // Adjust the color
      if (slider.callLeft) {
        color[slider.callLeft](left / slider.maxLeft);
      }
      if (slider.callTop) {
        color[slider.callTop](top / slider.maxTop);
      }

      // Set the new color
      cp.setValue(color);
      cp.popupHandler.focus();
    }

    /**
     * Binds the colorpicker sliders to the mouse/touch events
     */

  }, {
    key: 'bind',
    value: function bind() {
      var sliders = this.colorpicker.options.horizontal ? this.colorpicker.options.slidersHorz : this.colorpicker.options.sliders;

      var sliderClasses = [];

      for (var sliderName in sliders) {
        if (!sliders.hasOwnProperty(sliderName)) {
          continue;
        }

        sliderClasses.push(sliders[sliderName].selector);
      }

      this.colorpicker.picker.find(sliderClasses.join(', ')).on('mousedown.colorpicker touchstart.colorpicker', _jquery2.default.proxy(this.pressed, this));
    }

    /**
     * Unbinds any event bound by this handler
     */

  }, {
    key: 'unbind',
    value: function unbind() {
      (0, _jquery2.default)(this.colorpicker.picker).off({
        'mousemove.colorpicker': _jquery2.default.proxy(this.moved, this),
        'touchmove.colorpicker': _jquery2.default.proxy(this.moved, this),
        'mouseup.colorpicker': _jquery2.default.proxy(this.released, this),
        'touchend.colorpicker': _jquery2.default.proxy(this.released, this)
      });
    }

    /**
     * Function triggered when clicking in one of the color adjustment bars
     *
     * @private
     * @fires Colorpicker#mousemove
     * @param {Event} e
     */

  }, {
    key: 'pressed',
    value: function pressed(e) {
      if (this.colorpicker.isDisabled()) {
        return;
      }
      this.colorpicker.lastEvent.alias = 'pressed';
      this.colorpicker.lastEvent.e = e;

      if (!e.pageX && !e.pageY && e.originalEvent && e.originalEvent.touches) {
        e.pageX = e.originalEvent.touches[0].pageX;
        e.pageY = e.originalEvent.touches[0].pageY;
      }
      // e.stopPropagation();
      // e.preventDefault();

      var target = (0, _jquery2.default)(e.target);

      // detect the slider and set the limits and callbacks
      var zone = target.closest('div');

      var sliders = this.colorpicker.options.horizontal ? this.colorpicker.options.slidersHorz : this.colorpicker.options.sliders;

      if (zone.is('.colorpicker')) {
        return;
      }

      this.currentSlider = null;

      for (var sliderName in sliders) {
        if (!sliders.hasOwnProperty(sliderName)) {
          continue;
        }

        var slider = sliders[sliderName];

        if (zone.is(slider.selector)) {
          this.currentSlider = _jquery2.default.extend({}, slider, { name: sliderName });
          break;
        } else if (slider.childSelector !== undefined && zone.is(slider.childSelector)) {
          this.currentSlider = _jquery2.default.extend({}, slider, { name: sliderName });
          zone = zone.parent(); // zone.parents(slider.selector).first() ?
          break;
        }
      }

      var guide = zone.find('.colorpicker-guide').get(0);

      if (this.currentSlider === null || guide === null) {
        return;
      }

      var offset = zone.offset();

      // reference to guide's style
      this.currentSlider.guideStyle = guide.style;
      this.currentSlider.left = e.pageX - offset.left;
      this.currentSlider.top = e.pageY - offset.top;
      this.mousePointer = {
        left: e.pageX,
        top: e.pageY
      };

      // TODO: fix moving outside the picker makes the guides to keep moving. The event needs to be bound to the window.
      /**
       * (window.document) Triggered on mousedown for the document object,
       * so the color adjustment guide is moved to the clicked position.
       *
       * @event Colorpicker#mousemove
       */
      (0, _jquery2.default)(this.colorpicker.picker).on({
        'mousemove.colorpicker': _jquery2.default.proxy(this.moved, this),
        'touchmove.colorpicker': _jquery2.default.proxy(this.moved, this),
        'mouseup.colorpicker': _jquery2.default.proxy(this.released, this),
        'touchend.colorpicker': _jquery2.default.proxy(this.released, this)
      }).trigger('mousemove');
    }

    /**
     * Function triggered when dragging a guide inside one of the color adjustment bars.
     *
     * @private
     * @param {Event} e
     */

  }, {
    key: 'moved',
    value: function moved(e) {
      this.colorpicker.lastEvent.alias = 'moved';
      this.colorpicker.lastEvent.e = e;

      if (!e.pageX && !e.pageY && e.originalEvent && e.originalEvent.touches) {
        e.pageX = e.originalEvent.touches[0].pageX;
        e.pageY = e.originalEvent.touches[0].pageY;
      }

      // e.stopPropagation();
      e.preventDefault(); // prevents scrolling on mobile

      var left = Math.max(0, Math.min(this.currentSlider.maxLeft, this.currentSlider.left + ((e.pageX || this.mousePointer.left) - this.mousePointer.left)));

      var top = Math.max(0, Math.min(this.currentSlider.maxTop, this.currentSlider.top + ((e.pageY || this.mousePointer.top) - this.mousePointer.top)));

      this.onMove(top, left);
    }

    /**
     * Function triggered when releasing the click in one of the color adjustment bars.
     *
     * @private
     * @param {Event} e
     */

  }, {
    key: 'released',
    value: function released(e) {
      this.colorpicker.lastEvent.alias = 'released';
      this.colorpicker.lastEvent.e = e;

      // e.stopPropagation();
      // e.preventDefault();

      (0, _jquery2.default)(this.colorpicker.picker).off({
        'mousemove.colorpicker': this.moved,
        'touchmove.colorpicker': this.moved,
        'mouseup.colorpicker': this.released,
        'touchend.colorpicker': this.released
      });
    }
  }]);

  return SliderHandler;
}();

exports.default = SliderHandler;
module.exports = exports.default;

/***/ }),
/* 14 */
/***/ (function(module, exports, __nested_webpack_require_106103__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _jquery = __nested_webpack_require_106103__(0);

var _jquery2 = _interopRequireDefault(_jquery);

var _options = __nested_webpack_require_106103__(3);

var _options2 = _interopRequireDefault(_options);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * Handles everything related to the UI of the colorpicker popup: show, hide, position,...
 * @ignore
 */
var PopupHandler = function () {
  /**
   * @param {Colorpicker} colorpicker
   * @param {Window} root
   */
  function PopupHandler(colorpicker, root) {
    _classCallCheck(this, PopupHandler);

    /**
     * @type {Window}
     */
    this.root = root;
    /**
     * @type {Colorpicker}
     */
    this.colorpicker = colorpicker;
    /**
     * @type {jQuery}
     */
    this.popoverTarget = null;
    /**
     * @type {jQuery}
     */
    this.popoverTip = null;

    /**
     * If true, the latest click was inside the popover
     * @type {boolean}
     */
    this.clicking = false;
    /**
     * @type {boolean}
     */
    this.hidding = false;
    /**
     * @type {boolean}
     */
    this.showing = false;
  }

  /**
   * @private
   * @returns {jQuery|false}
   */


  _createClass(PopupHandler, [{
    key: 'bind',


    /**
     * Binds the different colorpicker elements to the focus/mouse/touch events so it reacts in order to show or
     * hide the colorpicker popup accordingly. It also adds the proper classes.
     */
    value: function bind() {
      var cp = this.colorpicker;

      if (cp.options.inline) {
        cp.picker.addClass('colorpicker-inline colorpicker-visible');
        return; // no need to bind show/hide events for inline elements
      }

      cp.picker.addClass('colorpicker-popup colorpicker-hidden');

      // there is no input or addon
      if (!this.hasInput && !this.hasAddon) {
        return;
      }

      // create Bootstrap 4 popover
      if (cp.options.popover) {
        this.createPopover();
      }

      // bind addon show/hide events
      if (this.hasAddon) {
        // enable focus on addons
        if (!this.addon.attr('tabindex')) {
          this.addon.attr('tabindex', 0);
        }

        this.addon.on({
          'mousedown.colorpicker touchstart.colorpicker': _jquery2.default.proxy(this.toggle, this)
        });

        this.addon.on({
          'focus.colorpicker': _jquery2.default.proxy(this.show, this)
        });

        this.addon.on({
          'focusout.colorpicker': _jquery2.default.proxy(this.hide, this)
        });
      }

      // bind input show/hide events
      if (this.hasInput && !this.hasAddon) {
        this.input.on({
          'mousedown.colorpicker touchstart.colorpicker': _jquery2.default.proxy(this.show, this),
          'focus.colorpicker': _jquery2.default.proxy(this.show, this)
        });

        this.input.on({
          'focusout.colorpicker': _jquery2.default.proxy(this.hide, this)
        });
      }

      // reposition popup on window resize
      (0, _jquery2.default)(this.root).on('resize.colorpicker', _jquery2.default.proxy(this.reposition, this));
    }

    /**
     * Unbinds any event bound by this handler
     */

  }, {
    key: 'unbind',
    value: function unbind() {
      if (this.hasInput) {
        this.input.off({
          'mousedown.colorpicker touchstart.colorpicker': _jquery2.default.proxy(this.show, this),
          'focus.colorpicker': _jquery2.default.proxy(this.show, this)
        });
        this.input.off({
          'focusout.colorpicker': _jquery2.default.proxy(this.hide, this)
        });
      }

      if (this.hasAddon) {
        this.addon.off({
          'mousedown.colorpicker touchstart.colorpicker': _jquery2.default.proxy(this.toggle, this)
        });
        this.addon.off({
          'focus.colorpicker': _jquery2.default.proxy(this.show, this)
        });
        this.addon.off({
          'focusout.colorpicker': _jquery2.default.proxy(this.hide, this)
        });
      }

      if (this.popoverTarget) {
        this.popoverTarget.popover('dispose');
      }

      (0, _jquery2.default)(this.root).off('resize.colorpicker', _jquery2.default.proxy(this.reposition, this));
      (0, _jquery2.default)(this.root.document).off('mousedown.colorpicker touchstart.colorpicker', _jquery2.default.proxy(this.hide, this));
      (0, _jquery2.default)(this.root.document).off('mousedown.colorpicker touchstart.colorpicker', _jquery2.default.proxy(this.onClickingInside, this));
    }
  }, {
    key: 'isClickingInside',
    value: function isClickingInside(e) {
      if (!e) {
        return false;
      }

      return this.isOrIsInside(this.popoverTip, e.currentTarget) || this.isOrIsInside(this.popoverTip, e.target) || this.isOrIsInside(this.colorpicker.picker, e.currentTarget) || this.isOrIsInside(this.colorpicker.picker, e.target);
    }
  }, {
    key: 'isOrIsInside',
    value: function isOrIsInside(container, element) {
      if (!container || !element) {
        return false;
      }

      element = (0, _jquery2.default)(element);

      return element.is(container) || container.find(element).length > 0;
    }
  }, {
    key: 'onClickingInside',
    value: function onClickingInside(e) {
      this.clicking = this.isClickingInside(e);
    }
  }, {
    key: 'createPopover',
    value: function createPopover() {
      var cp = this.colorpicker;

      this.popoverTarget = this.hasAddon ? this.addon : this.input;

      cp.picker.addClass('colorpicker-bs-popover-content');

      this.popoverTarget.popover(_jquery2.default.extend(true, {}, _options2.default.popover, cp.options.popover, { trigger: 'manual', content: cp.picker, html: true }));

      this.popoverTip = (0, _jquery2.default)(this.popoverTarget.popover('getTipElement').data('bs.popover').tip);
      this.popoverTip.addClass('colorpicker-bs-popover');

      this.popoverTarget.on('shown.bs.popover', _jquery2.default.proxy(this.fireShow, this));
      this.popoverTarget.on('hidden.bs.popover', _jquery2.default.proxy(this.fireHide, this));
    }

    /**
     * If the widget is not inside a container or inline, rearranges its position relative to its element offset.
     *
     * @param {Event} [e]
     * @private
     */

  }, {
    key: 'reposition',
    value: function reposition(e) {
      if (this.popoverTarget && this.isVisible()) {
        this.popoverTarget.popover('update');
      }
    }

    /**
     * Toggles the colorpicker between visible or hidden
     *
     * @fires Colorpicker#colorpickerShow
     * @fires Colorpicker#colorpickerHide
     * @param {Event} [e]
     */

  }, {
    key: 'toggle',
    value: function toggle(e) {
      if (this.isVisible()) {
        this.hide(e);
      } else {
        this.show(e);
      }
    }

    /**
     * Shows the colorpicker widget if hidden.
     *
     * @fires Colorpicker#colorpickerShow
     * @param {Event} [e]
     */

  }, {
    key: 'show',
    value: function show(e) {
      if (this.isVisible() || this.showing || this.hidding) {
        return;
      }

      this.showing = true;
      this.hidding = false;
      this.clicking = false;

      var cp = this.colorpicker;

      cp.lastEvent.alias = 'show';
      cp.lastEvent.e = e;

      // Prevent showing browser native HTML5 colorpicker
      if (e && (!this.hasInput || this.input.attr('type') === 'color') && e && e.preventDefault) {
        e.stopPropagation();
        e.preventDefault();
      }

      // If it's a popover, add event to the document to hide the picker when clicking outside of it
      if (this.isPopover) {
        (0, _jquery2.default)(this.root).on('resize.colorpicker', _jquery2.default.proxy(this.reposition, this));
      }

      // add visible class before popover is shown
      cp.picker.addClass('colorpicker-visible').removeClass('colorpicker-hidden');

      if (this.popoverTarget) {
        this.popoverTarget.popover('show');
      } else {
        this.fireShow();
      }
    }
  }, {
    key: 'fireShow',
    value: function fireShow() {
      this.hidding = false;
      this.showing = false;

      if (this.isPopover) {
        // Add event to hide on outside click
        (0, _jquery2.default)(this.root.document).on('mousedown.colorpicker touchstart.colorpicker', _jquery2.default.proxy(this.hide, this));
        (0, _jquery2.default)(this.root.document).on('mousedown.colorpicker touchstart.colorpicker', _jquery2.default.proxy(this.onClickingInside, this));
      }

      /**
       * (Colorpicker) When show() is called and the widget can be shown.
       *
       * @event Colorpicker#colorpickerShow
       */
      this.colorpicker.trigger('colorpickerShow');
    }

    /**
     * Hides the colorpicker widget.
     * Hide is prevented when it is triggered by an event whose target element has been clicked/touched.
     *
     * @fires Colorpicker#colorpickerHide
     * @param {Event} [e]
     */

  }, {
    key: 'hide',
    value: function hide(e) {
      if (this.isHidden() || this.showing || this.hidding) {
        return;
      }

      var cp = this.colorpicker,
          clicking = this.clicking || this.isClickingInside(e);

      this.hidding = true;
      this.showing = false;
      this.clicking = false;

      cp.lastEvent.alias = 'hide';
      cp.lastEvent.e = e;

      // TODO: fix having to click twice outside when losing focus and last 2 clicks where inside the colorpicker

      // Prevent hide if triggered by an event and an element inside the colorpicker has been clicked/touched
      if (clicking) {
        this.hidding = false;
        return;
      }

      if (this.popoverTarget) {
        this.popoverTarget.popover('hide');
      } else {
        this.fireHide();
      }
    }
  }, {
    key: 'fireHide',
    value: function fireHide() {
      this.hidding = false;
      this.showing = false;

      var cp = this.colorpicker;

      // add hidden class after popover is hidden
      cp.picker.addClass('colorpicker-hidden').removeClass('colorpicker-visible');

      // Unbind window and document events, since there is no need to keep them while the popup is hidden
      (0, _jquery2.default)(this.root).off('resize.colorpicker', _jquery2.default.proxy(this.reposition, this));
      (0, _jquery2.default)(this.root.document).off('mousedown.colorpicker touchstart.colorpicker', _jquery2.default.proxy(this.hide, this));
      (0, _jquery2.default)(this.root.document).off('mousedown.colorpicker touchstart.colorpicker', _jquery2.default.proxy(this.onClickingInside, this));

      /**
       * (Colorpicker) When hide() is called and the widget can be hidden.
       *
       * @event Colorpicker#colorpickerHide
       */
      cp.trigger('colorpickerHide');
    }
  }, {
    key: 'focus',
    value: function focus() {
      if (this.hasAddon) {
        return this.addon.focus();
      }
      if (this.hasInput) {
        return this.input.focus();
      }
      return false;
    }

    /**
     * Returns true if the colorpicker element has the colorpicker-visible class and not the colorpicker-hidden one.
     * False otherwise.
     *
     * @returns {boolean}
     */

  }, {
    key: 'isVisible',
    value: function isVisible() {
      return this.colorpicker.picker.hasClass('colorpicker-visible') && !this.colorpicker.picker.hasClass('colorpicker-hidden');
    }

    /**
     * Returns true if the colorpicker element has the colorpicker-hidden class and not the colorpicker-visible one.
     * False otherwise.
     *
     * @returns {boolean}
     */

  }, {
    key: 'isHidden',
    value: function isHidden() {
      return this.colorpicker.picker.hasClass('colorpicker-hidden') && !this.colorpicker.picker.hasClass('colorpicker-visible');
    }
  }, {
    key: 'input',
    get: function get() {
      return this.colorpicker.inputHandler.input;
    }

    /**
     * @private
     * @returns {boolean}
     */

  }, {
    key: 'hasInput',
    get: function get() {
      return this.colorpicker.inputHandler.hasInput();
    }

    /**
     * @private
     * @returns {jQuery|false}
     */

  }, {
    key: 'addon',
    get: function get() {
      return this.colorpicker.addonHandler.addon;
    }

    /**
     * @private
     * @returns {boolean}
     */

  }, {
    key: 'hasAddon',
    get: function get() {
      return this.colorpicker.addonHandler.hasAddon();
    }

    /**
     * @private
     * @returns {boolean}
     */

  }, {
    key: 'isPopover',
    get: function get() {
      return !this.colorpicker.options.inline && !!this.popoverTip;
    }
  }]);

  return PopupHandler;
}();

exports.default = PopupHandler;
module.exports = exports.default;

/***/ }),
/* 15 */
/***/ (function(module, exports, __nested_webpack_require_119576__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _jquery = __nested_webpack_require_119576__(0);

var _jquery2 = _interopRequireDefault(_jquery);

var _ColorItem = __nested_webpack_require_119576__(2);

var _ColorItem2 = _interopRequireDefault(_ColorItem);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * Handles everything related to the colorpicker input
 * @ignore
 */
var InputHandler = function () {
  /**
   * @param {Colorpicker} colorpicker
   */
  function InputHandler(colorpicker) {
    _classCallCheck(this, InputHandler);

    /**
     * @type {Colorpicker}
     */
    this.colorpicker = colorpicker;
    /**
     * @type {jQuery|false}
     */
    this.input = this.colorpicker.element.is('input') ? this.colorpicker.element : this.colorpicker.options.input ? this.colorpicker.element.find(this.colorpicker.options.input) : false;

    if (this.input && this.input.length === 0) {
      this.input = false;
    }

    this._initValue();
  }

  _createClass(InputHandler, [{
    key: 'bind',
    value: function bind() {
      if (!this.hasInput()) {
        return;
      }
      this.input.on({
        'keyup.colorpicker': _jquery2.default.proxy(this.onkeyup, this)
      });
      this.input.on({
        'change.colorpicker': _jquery2.default.proxy(this.onchange, this)
      });
    }
  }, {
    key: 'unbind',
    value: function unbind() {
      if (!this.hasInput()) {
        return;
      }
      this.input.off('.colorpicker');
    }
  }, {
    key: '_initValue',
    value: function _initValue() {
      if (!this.hasInput()) {
        return;
      }

      var val = '';

      [
      // candidates:
      this.input.val(), this.input.data('color'), this.input.attr('data-color')].map(function (item) {
        if (item && val === '') {
          val = item;
        }
      });

      if (val instanceof _ColorItem2.default) {
        val = this.getFormattedColor(val.string(this.colorpicker.format));
      } else if (!(typeof val === 'string' || val instanceof String)) {
        val = '';
      }

      this.input.prop('value', val);
    }

    /**
     * Returns the color string from the input value.
     * If there is no input the return value is false.
     *
     * @returns {String|boolean}
     */

  }, {
    key: 'getValue',
    value: function getValue() {
      if (!this.hasInput()) {
        return false;
      }

      return this.input.val();
    }

    /**
     * If the input element is present, it updates the value with the current color object color string.
     * If the value is changed, this method fires a "change" event on the input element.
     *
     * @param {String} val
     *
     * @fires Colorpicker#change
     */

  }, {
    key: 'setValue',
    value: function setValue(val) {
      if (!this.hasInput()) {
        return;
      }

      var inputVal = this.input.prop('value');

      val = val ? val : '';

      if (val === (inputVal ? inputVal : '')) {
        // No need to set value or trigger any event if nothing changed
        return;
      }

      this.input.prop('value', val);

      /**
       * (Input) Triggered on the input element when a new color is selected.
       *
       * @event Colorpicker#change
       */
      this.input.trigger({
        type: 'change',
        colorpicker: this.colorpicker,
        color: this.colorpicker.color,
        value: val
      });
    }

    /**
     * Returns the formatted color string, with the formatting options applied
     * (e.g. useHashPrefix)
     *
     * @param {String|null} val
     *
     * @returns {String}
     */

  }, {
    key: 'getFormattedColor',
    value: function getFormattedColor() {
      var val = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;

      val = val ? val : this.colorpicker.colorHandler.getColorString();

      if (!val) {
        return '';
      }

      val = this.colorpicker.colorHandler.resolveColorDelegate(val, false);

      if (this.colorpicker.options.useHashPrefix === false) {
        val = val.replace(/^#/g, '');
      }

      return val;
    }

    /**
     * Returns true if the widget has an associated input element, false otherwise
     * @returns {boolean}
     */

  }, {
    key: 'hasInput',
    value: function hasInput() {
      return this.input !== false;
    }

    /**
     * Returns true if the input exists and is disabled
     * @returns {boolean}
     */

  }, {
    key: 'isEnabled',
    value: function isEnabled() {
      return this.hasInput() && !this.isDisabled();
    }

    /**
     * Returns true if the input exists and is disabled
     * @returns {boolean}
     */

  }, {
    key: 'isDisabled',
    value: function isDisabled() {
      return this.hasInput() && this.input.prop('disabled') === true;
    }

    /**
     * Disables the input if any
     *
     * @fires Colorpicker#colorpickerDisable
     * @returns {boolean}
     */

  }, {
    key: 'disable',
    value: function disable() {
      if (this.hasInput()) {
        this.input.prop('disabled', true);
      }
    }

    /**
     * Enables the input if any
     *
     * @fires Colorpicker#colorpickerEnable
     * @returns {boolean}
     */

  }, {
    key: 'enable',
    value: function enable() {
      if (this.hasInput()) {
        this.input.prop('disabled', false);
      }
    }

    /**
     * Calls setValue with the current internal color value
     *
     * @fires Colorpicker#change
     */

  }, {
    key: 'update',
    value: function update() {
      if (!this.hasInput()) {
        return;
      }

      if (this.colorpicker.options.autoInputFallback === false && this.colorpicker.colorHandler.isInvalidColor()) {
        // prevent update if color is invalid, autoInputFallback is disabled and the last event is keyup.
        return;
      }

      this.setValue(this.getFormattedColor());
    }

    /**
     * Function triggered when the input has changed, so the colorpicker gets updated.
     *
     * @private
     * @param {Event} e
     * @returns {boolean}
     */

  }, {
    key: 'onchange',
    value: function onchange(e) {
      this.colorpicker.lastEvent.alias = 'input.change';
      this.colorpicker.lastEvent.e = e;

      var val = this.getValue();

      if (val !== e.value) {
        this.colorpicker.setValue(val);
      }
    }

    /**
     * Function triggered after a keyboard key has been released.
     *
     * @private
     * @param {Event} e
     * @returns {boolean}
     */

  }, {
    key: 'onkeyup',
    value: function onkeyup(e) {
      this.colorpicker.lastEvent.alias = 'input.keyup';
      this.colorpicker.lastEvent.e = e;

      var val = this.getValue();

      if (val !== e.value) {
        this.colorpicker.setValue(val);
      }
    }
  }]);

  return InputHandler;
}();

exports.default = InputHandler;
module.exports = exports.default;

/***/ }),
/* 16 */
/***/ (function(module, exports, __nested_webpack_require_127262__) {

"use strict";


var colorString = __nested_webpack_require_127262__(17);
var convert = __nested_webpack_require_127262__(20);

var _slice = [].slice;

var skippedModels = [
	// to be honest, I don't really feel like keyword belongs in color convert, but eh.
	'keyword',

	// gray conflicts with some method names, and has its own method defined.
	'gray',

	// shouldn't really be in color-convert either...
	'hex'
];

var hashedModelKeys = {};
Object.keys(convert).forEach(function (model) {
	hashedModelKeys[_slice.call(convert[model].labels).sort().join('')] = model;
});

var limiters = {};

function Color(obj, model) {
	if (!(this instanceof Color)) {
		return new Color(obj, model);
	}

	if (model && model in skippedModels) {
		model = null;
	}

	if (model && !(model in convert)) {
		throw new Error('Unknown model: ' + model);
	}

	var i;
	var channels;

	if (obj == null) { // eslint-disable-line no-eq-null,eqeqeq
		this.model = 'rgb';
		this.color = [0, 0, 0];
		this.valpha = 1;
	} else if (obj instanceof Color) {
		this.model = obj.model;
		this.color = obj.color.slice();
		this.valpha = obj.valpha;
	} else if (typeof obj === 'string') {
		var result = colorString.get(obj);
		if (result === null) {
			throw new Error('Unable to parse color from string: ' + obj);
		}

		this.model = result.model;
		channels = convert[this.model].channels;
		this.color = result.value.slice(0, channels);
		this.valpha = typeof result.value[channels] === 'number' ? result.value[channels] : 1;
	} else if (obj.length) {
		this.model = model || 'rgb';
		channels = convert[this.model].channels;
		var newArr = _slice.call(obj, 0, channels);
		this.color = zeroArray(newArr, channels);
		this.valpha = typeof obj[channels] === 'number' ? obj[channels] : 1;
	} else if (typeof obj === 'number') {
		// this is always RGB - can be converted later on.
		obj &= 0xFFFFFF;
		this.model = 'rgb';
		this.color = [
			(obj >> 16) & 0xFF,
			(obj >> 8) & 0xFF,
			obj & 0xFF
		];
		this.valpha = 1;
	} else {
		this.valpha = 1;

		var keys = Object.keys(obj);
		if ('alpha' in obj) {
			keys.splice(keys.indexOf('alpha'), 1);
			this.valpha = typeof obj.alpha === 'number' ? obj.alpha : 0;
		}

		var hashedKeys = keys.sort().join('');
		if (!(hashedKeys in hashedModelKeys)) {
			throw new Error('Unable to parse color from object: ' + JSON.stringify(obj));
		}

		this.model = hashedModelKeys[hashedKeys];

		var labels = convert[this.model].labels;
		var color = [];
		for (i = 0; i < labels.length; i++) {
			color.push(obj[labels[i]]);
		}

		this.color = zeroArray(color);
	}

	// perform limitations (clamping, etc.)
	if (limiters[this.model]) {
		channels = convert[this.model].channels;
		for (i = 0; i < channels; i++) {
			var limit = limiters[this.model][i];
			if (limit) {
				this.color[i] = limit(this.color[i]);
			}
		}
	}

	this.valpha = Math.max(0, Math.min(1, this.valpha));

	if (Object.freeze) {
		Object.freeze(this);
	}
}

Color.prototype = {
	toString: function () {
		return this.string();
	},

	toJSON: function () {
		return this[this.model]();
	},

	string: function (places) {
		var self = this.model in colorString.to ? this : this.rgb();
		self = self.round(typeof places === 'number' ? places : 1);
		var args = self.valpha === 1 ? self.color : self.color.concat(this.valpha);
		return colorString.to[self.model](args);
	},

	percentString: function (places) {
		var self = this.rgb().round(typeof places === 'number' ? places : 1);
		var args = self.valpha === 1 ? self.color : self.color.concat(this.valpha);
		return colorString.to.rgb.percent(args);
	},

	array: function () {
		return this.valpha === 1 ? this.color.slice() : this.color.concat(this.valpha);
	},

	object: function () {
		var result = {};
		var channels = convert[this.model].channels;
		var labels = convert[this.model].labels;

		for (var i = 0; i < channels; i++) {
			result[labels[i]] = this.color[i];
		}

		if (this.valpha !== 1) {
			result.alpha = this.valpha;
		}

		return result;
	},

	unitArray: function () {
		var rgb = this.rgb().color;
		rgb[0] /= 255;
		rgb[1] /= 255;
		rgb[2] /= 255;

		if (this.valpha !== 1) {
			rgb.push(this.valpha);
		}

		return rgb;
	},

	unitObject: function () {
		var rgb = this.rgb().object();
		rgb.r /= 255;
		rgb.g /= 255;
		rgb.b /= 255;

		if (this.valpha !== 1) {
			rgb.alpha = this.valpha;
		}

		return rgb;
	},

	round: function (places) {
		places = Math.max(places || 0, 0);
		return new Color(this.color.map(roundToPlace(places)).concat(this.valpha), this.model);
	},

	alpha: function (val) {
		if (arguments.length) {
			return new Color(this.color.concat(Math.max(0, Math.min(1, val))), this.model);
		}

		return this.valpha;
	},

	// rgb
	red: getset('rgb', 0, maxfn(255)),
	green: getset('rgb', 1, maxfn(255)),
	blue: getset('rgb', 2, maxfn(255)),

	hue: getset(['hsl', 'hsv', 'hsl', 'hwb', 'hcg'], 0, function (val) { return ((val % 360) + 360) % 360; }), // eslint-disable-line brace-style

	saturationl: getset('hsl', 1, maxfn(100)),
	lightness: getset('hsl', 2, maxfn(100)),

	saturationv: getset('hsv', 1, maxfn(100)),
	value: getset('hsv', 2, maxfn(100)),

	chroma: getset('hcg', 1, maxfn(100)),
	gray: getset('hcg', 2, maxfn(100)),

	white: getset('hwb', 1, maxfn(100)),
	wblack: getset('hwb', 2, maxfn(100)),

	cyan: getset('cmyk', 0, maxfn(100)),
	magenta: getset('cmyk', 1, maxfn(100)),
	yellow: getset('cmyk', 2, maxfn(100)),
	black: getset('cmyk', 3, maxfn(100)),

	x: getset('xyz', 0, maxfn(100)),
	y: getset('xyz', 1, maxfn(100)),
	z: getset('xyz', 2, maxfn(100)),

	l: getset('lab', 0, maxfn(100)),
	a: getset('lab', 1),
	b: getset('lab', 2),

	keyword: function (val) {
		if (arguments.length) {
			return new Color(val);
		}

		return convert[this.model].keyword(this.color);
	},

	hex: function (val) {
		if (arguments.length) {
			return new Color(val);
		}

		return colorString.to.hex(this.rgb().round().color);
	},

	rgbNumber: function () {
		var rgb = this.rgb().color;
		return ((rgb[0] & 0xFF) << 16) | ((rgb[1] & 0xFF) << 8) | (rgb[2] & 0xFF);
	},

	luminosity: function () {
		// http://www.w3.org/TR/WCAG20/#relativeluminancedef
		var rgb = this.rgb().color;

		var lum = [];
		for (var i = 0; i < rgb.length; i++) {
			var chan = rgb[i] / 255;
			lum[i] = (chan <= 0.03928) ? chan / 12.92 : Math.pow(((chan + 0.055) / 1.055), 2.4);
		}

		return 0.2126 * lum[0] + 0.7152 * lum[1] + 0.0722 * lum[2];
	},

	contrast: function (color2) {
		// http://www.w3.org/TR/WCAG20/#contrast-ratiodef
		var lum1 = this.luminosity();
		var lum2 = color2.luminosity();

		if (lum1 > lum2) {
			return (lum1 + 0.05) / (lum2 + 0.05);
		}

		return (lum2 + 0.05) / (lum1 + 0.05);
	},

	level: function (color2) {
		var contrastRatio = this.contrast(color2);
		if (contrastRatio >= 7.1) {
			return 'AAA';
		}

		return (contrastRatio >= 4.5) ? 'AA' : '';
	},

	isDark: function () {
		// YIQ equation from http://24ways.org/2010/calculating-color-contrast
		var rgb = this.rgb().color;
		var yiq = (rgb[0] * 299 + rgb[1] * 587 + rgb[2] * 114) / 1000;
		return yiq < 128;
	},

	isLight: function () {
		return !this.isDark();
	},

	negate: function () {
		var rgb = this.rgb();
		for (var i = 0; i < 3; i++) {
			rgb.color[i] = 255 - rgb.color[i];
		}
		return rgb;
	},

	lighten: function (ratio) {
		var hsl = this.hsl();
		hsl.color[2] += hsl.color[2] * ratio;
		return hsl;
	},

	darken: function (ratio) {
		var hsl = this.hsl();
		hsl.color[2] -= hsl.color[2] * ratio;
		return hsl;
	},

	saturate: function (ratio) {
		var hsl = this.hsl();
		hsl.color[1] += hsl.color[1] * ratio;
		return hsl;
	},

	desaturate: function (ratio) {
		var hsl = this.hsl();
		hsl.color[1] -= hsl.color[1] * ratio;
		return hsl;
	},

	whiten: function (ratio) {
		var hwb = this.hwb();
		hwb.color[1] += hwb.color[1] * ratio;
		return hwb;
	},

	blacken: function (ratio) {
		var hwb = this.hwb();
		hwb.color[2] += hwb.color[2] * ratio;
		return hwb;
	},

	grayscale: function () {
		// http://en.wikipedia.org/wiki/Grayscale#Converting_color_to_grayscale
		var rgb = this.rgb().color;
		var val = rgb[0] * 0.3 + rgb[1] * 0.59 + rgb[2] * 0.11;
		return Color.rgb(val, val, val);
	},

	fade: function (ratio) {
		return this.alpha(this.valpha - (this.valpha * ratio));
	},

	opaquer: function (ratio) {
		return this.alpha(this.valpha + (this.valpha * ratio));
	},

	rotate: function (degrees) {
		var hsl = this.hsl();
		var hue = hsl.color[0];
		hue = (hue + degrees) % 360;
		hue = hue < 0 ? 360 + hue : hue;
		hsl.color[0] = hue;
		return hsl;
	},

	mix: function (mixinColor, weight) {
		// ported from sass implementation in C
		// https://github.com/sass/libsass/blob/0e6b4a2850092356aa3ece07c6b249f0221caced/functions.cpp#L209
		if (!mixinColor || !mixinColor.rgb) {
			throw new Error('Argument to "mix" was not a Color instance, but rather an instance of ' + typeof mixinColor);
		}
		var color1 = mixinColor.rgb();
		var color2 = this.rgb();
		var p = weight === undefined ? 0.5 : weight;

		var w = 2 * p - 1;
		var a = color1.alpha() - color2.alpha();

		var w1 = (((w * a === -1) ? w : (w + a) / (1 + w * a)) + 1) / 2.0;
		var w2 = 1 - w1;

		return Color.rgb(
				w1 * color1.red() + w2 * color2.red(),
				w1 * color1.green() + w2 * color2.green(),
				w1 * color1.blue() + w2 * color2.blue(),
				color1.alpha() * p + color2.alpha() * (1 - p));
	}
};

// model conversion methods and static constructors
Object.keys(convert).forEach(function (model) {
	if (skippedModels.indexOf(model) !== -1) {
		return;
	}

	var channels = convert[model].channels;

	// conversion methods
	Color.prototype[model] = function () {
		if (this.model === model) {
			return new Color(this);
		}

		if (arguments.length) {
			return new Color(arguments, model);
		}

		var newAlpha = typeof arguments[channels] === 'number' ? channels : this.valpha;
		return new Color(assertArray(convert[this.model][model].raw(this.color)).concat(newAlpha), model);
	};

	// 'static' construction methods
	Color[model] = function (color) {
		if (typeof color === 'number') {
			color = zeroArray(_slice.call(arguments), channels);
		}
		return new Color(color, model);
	};
});

function roundTo(num, places) {
	return Number(num.toFixed(places));
}

function roundToPlace(places) {
	return function (num) {
		return roundTo(num, places);
	};
}

function getset(model, channel, modifier) {
	model = Array.isArray(model) ? model : [model];

	model.forEach(function (m) {
		(limiters[m] || (limiters[m] = []))[channel] = modifier;
	});

	model = model[0];

	return function (val) {
		var result;

		if (arguments.length) {
			if (modifier) {
				val = modifier(val);
			}

			result = this[model]();
			result.color[channel] = val;
			return result;
		}

		result = this[model]().color[channel];
		if (modifier) {
			result = modifier(result);
		}

		return result;
	};
}

function maxfn(max) {
	return function (v) {
		return Math.max(0, Math.min(max, v));
	};
}

function assertArray(val) {
	return Array.isArray(val) ? val : [val];
}

function zeroArray(arr, length) {
	for (var i = 0; i < length; i++) {
		if (typeof arr[i] !== 'number') {
			arr[i] = 0;
		}
	}

	return arr;
}

module.exports = Color;


/***/ }),
/* 17 */
/***/ (function(module, exports, __nested_webpack_require_138459__) {

/* MIT license */
var colorNames = __nested_webpack_require_138459__(5);
var swizzle = __nested_webpack_require_138459__(18);

var reverseNames = {};

// create a list of reverse color names
for (var name in colorNames) {
	if (colorNames.hasOwnProperty(name)) {
		reverseNames[colorNames[name]] = name;
	}
}

var cs = module.exports = {
	to: {},
	get: {}
};

cs.get = function (string) {
	var prefix = string.substring(0, 3).toLowerCase();
	var val;
	var model;
	switch (prefix) {
		case 'hsl':
			val = cs.get.hsl(string);
			model = 'hsl';
			break;
		case 'hwb':
			val = cs.get.hwb(string);
			model = 'hwb';
			break;
		default:
			val = cs.get.rgb(string);
			model = 'rgb';
			break;
	}

	if (!val) {
		return null;
	}

	return {model: model, value: val};
};

cs.get.rgb = function (string) {
	if (!string) {
		return null;
	}

	var abbr = /^#([a-f0-9]{3,4})$/i;
	var hex = /^#([a-f0-9]{6})([a-f0-9]{2})?$/i;
	var rgba = /^rgba?\(\s*([+-]?\d+)\s*,\s*([+-]?\d+)\s*,\s*([+-]?\d+)\s*(?:,\s*([+-]?[\d\.]+)\s*)?\)$/;
	var per = /^rgba?\(\s*([+-]?[\d\.]+)\%\s*,\s*([+-]?[\d\.]+)\%\s*,\s*([+-]?[\d\.]+)\%\s*(?:,\s*([+-]?[\d\.]+)\s*)?\)$/;
	var keyword = /(\D+)/;

	var rgb = [0, 0, 0, 1];
	var match;
	var i;
	var hexAlpha;

	if (match = string.match(hex)) {
		hexAlpha = match[2];
		match = match[1];

		for (i = 0; i < 3; i++) {
			// https://jsperf.com/slice-vs-substr-vs-substring-methods-long-string/19
			var i2 = i * 2;
			rgb[i] = parseInt(match.slice(i2, i2 + 2), 16);
		}

		if (hexAlpha) {
			rgb[3] = Math.round((parseInt(hexAlpha, 16) / 255) * 100) / 100;
		}
	} else if (match = string.match(abbr)) {
		match = match[1];
		hexAlpha = match[3];

		for (i = 0; i < 3; i++) {
			rgb[i] = parseInt(match[i] + match[i], 16);
		}

		if (hexAlpha) {
			rgb[3] = Math.round((parseInt(hexAlpha + hexAlpha, 16) / 255) * 100) / 100;
		}
	} else if (match = string.match(rgba)) {
		for (i = 0; i < 3; i++) {
			rgb[i] = parseInt(match[i + 1], 0);
		}

		if (match[4]) {
			rgb[3] = parseFloat(match[4]);
		}
	} else if (match = string.match(per)) {
		for (i = 0; i < 3; i++) {
			rgb[i] = Math.round(parseFloat(match[i + 1]) * 2.55);
		}

		if (match[4]) {
			rgb[3] = parseFloat(match[4]);
		}
	} else if (match = string.match(keyword)) {
		if (match[1] === 'transparent') {
			return [0, 0, 0, 0];
		}

		rgb = colorNames[match[1]];

		if (!rgb) {
			return null;
		}

		rgb[3] = 1;

		return rgb;
	} else {
		return null;
	}

	for (i = 0; i < 3; i++) {
		rgb[i] = clamp(rgb[i], 0, 255);
	}
	rgb[3] = clamp(rgb[3], 0, 1);

	return rgb;
};

cs.get.hsl = function (string) {
	if (!string) {
		return null;
	}

	var hsl = /^hsla?\(\s*([+-]?(?:\d*\.)?\d+)(?:deg)?\s*,\s*([+-]?[\d\.]+)%\s*,\s*([+-]?[\d\.]+)%\s*(?:,\s*([+-]?[\d\.]+)\s*)?\)$/;
	var match = string.match(hsl);

	if (match) {
		var alpha = parseFloat(match[4]);
		var h = (parseFloat(match[1]) + 360) % 360;
		var s = clamp(parseFloat(match[2]), 0, 100);
		var l = clamp(parseFloat(match[3]), 0, 100);
		var a = clamp(isNaN(alpha) ? 1 : alpha, 0, 1);

		return [h, s, l, a];
	}

	return null;
};

cs.get.hwb = function (string) {
	if (!string) {
		return null;
	}

	var hwb = /^hwb\(\s*([+-]?\d*[\.]?\d+)(?:deg)?\s*,\s*([+-]?[\d\.]+)%\s*,\s*([+-]?[\d\.]+)%\s*(?:,\s*([+-]?[\d\.]+)\s*)?\)$/;
	var match = string.match(hwb);

	if (match) {
		var alpha = parseFloat(match[4]);
		var h = ((parseFloat(match[1]) % 360) + 360) % 360;
		var w = clamp(parseFloat(match[2]), 0, 100);
		var b = clamp(parseFloat(match[3]), 0, 100);
		var a = clamp(isNaN(alpha) ? 1 : alpha, 0, 1);
		return [h, w, b, a];
	}

	return null;
};

cs.to.hex = function () {
	var rgba = swizzle(arguments);

	return (
		'#' +
		hexDouble(rgba[0]) +
		hexDouble(rgba[1]) +
		hexDouble(rgba[2]) +
		(rgba[3] < 1
			? (hexDouble(Math.round(rgba[3] * 255)))
			: '')
	);
};

cs.to.rgb = function () {
	var rgba = swizzle(arguments);

	return rgba.length < 4 || rgba[3] === 1
		? 'rgb(' + Math.round(rgba[0]) + ', ' + Math.round(rgba[1]) + ', ' + Math.round(rgba[2]) + ')'
		: 'rgba(' + Math.round(rgba[0]) + ', ' + Math.round(rgba[1]) + ', ' + Math.round(rgba[2]) + ', ' + rgba[3] + ')';
};

cs.to.rgb.percent = function () {
	var rgba = swizzle(arguments);

	var r = Math.round(rgba[0] / 255 * 100);
	var g = Math.round(rgba[1] / 255 * 100);
	var b = Math.round(rgba[2] / 255 * 100);

	return rgba.length < 4 || rgba[3] === 1
		? 'rgb(' + r + '%, ' + g + '%, ' + b + '%)'
		: 'rgba(' + r + '%, ' + g + '%, ' + b + '%, ' + rgba[3] + ')';
};

cs.to.hsl = function () {
	var hsla = swizzle(arguments);
	return hsla.length < 4 || hsla[3] === 1
		? 'hsl(' + hsla[0] + ', ' + hsla[1] + '%, ' + hsla[2] + '%)'
		: 'hsla(' + hsla[0] + ', ' + hsla[1] + '%, ' + hsla[2] + '%, ' + hsla[3] + ')';
};

// hwb is a bit different than rgb(a) & hsl(a) since there is no alpha specific syntax
// (hwb have alpha optional & 1 is default value)
cs.to.hwb = function () {
	var hwba = swizzle(arguments);

	var a = '';
	if (hwba.length >= 4 && hwba[3] !== 1) {
		a = ', ' + hwba[3];
	}

	return 'hwb(' + hwba[0] + ', ' + hwba[1] + '%, ' + hwba[2] + '%' + a + ')';
};

cs.to.keyword = function (rgb) {
	return reverseNames[rgb.slice(0, 3)];
};

// helpers
function clamp(num, min, max) {
	return Math.min(Math.max(min, num), max);
}

function hexDouble(num) {
	var str = num.toString(16).toUpperCase();
	return (str.length < 2) ? '0' + str : str;
}


/***/ }),
/* 18 */
/***/ (function(module, exports, __nested_webpack_require_143863__) {

"use strict";


var isArrayish = __nested_webpack_require_143863__(19);

var concat = Array.prototype.concat;
var slice = Array.prototype.slice;

var swizzle = module.exports = function swizzle(args) {
	var results = [];

	for (var i = 0, len = args.length; i < len; i++) {
		var arg = args[i];

		if (isArrayish(arg)) {
			// http://jsperf.com/javascript-array-concat-vs-push/98
			results = concat.call(results, slice.call(arg));
		} else {
			results.push(arg);
		}
	}

	return results;
};

swizzle.wrap = function (fn) {
	return function () {
		return fn(swizzle(arguments));
	};
};


/***/ }),
/* 19 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function isArrayish(obj) {
	if (!obj) {
		return false;
	}

	return obj instanceof Array || Array.isArray(obj) ||
		(obj.length >= 0 && obj.splice instanceof Function);
};


/***/ }),
/* 20 */
/***/ (function(module, exports, __nested_webpack_require_144797__) {

var conversions = __nested_webpack_require_144797__(6);
var route = __nested_webpack_require_144797__(21);

var convert = {};

var models = Object.keys(conversions);

function wrapRaw(fn) {
	var wrappedFn = function (args) {
		if (args === undefined || args === null) {
			return args;
		}

		if (arguments.length > 1) {
			args = Array.prototype.slice.call(arguments);
		}

		return fn(args);
	};

	// preserve .conversion property if there is one
	if ('conversion' in fn) {
		wrappedFn.conversion = fn.conversion;
	}

	return wrappedFn;
}

function wrapRounded(fn) {
	var wrappedFn = function (args) {
		if (args === undefined || args === null) {
			return args;
		}

		if (arguments.length > 1) {
			args = Array.prototype.slice.call(arguments);
		}

		var result = fn(args);

		// we're assuming the result is an array here.
		// see notice in conversions.js; don't use box types
		// in conversion functions.
		if (typeof result === 'object') {
			for (var len = result.length, i = 0; i < len; i++) {
				result[i] = Math.round(result[i]);
			}
		}

		return result;
	};

	// preserve .conversion property if there is one
	if ('conversion' in fn) {
		wrappedFn.conversion = fn.conversion;
	}

	return wrappedFn;
}

models.forEach(function (fromModel) {
	convert[fromModel] = {};

	Object.defineProperty(convert[fromModel], 'channels', {value: conversions[fromModel].channels});
	Object.defineProperty(convert[fromModel], 'labels', {value: conversions[fromModel].labels});

	var routes = route(fromModel);
	var routeModels = Object.keys(routes);

	routeModels.forEach(function (toModel) {
		var fn = routes[toModel];

		convert[fromModel][toModel] = wrapRounded(fn);
		convert[fromModel][toModel].raw = wrapRaw(fn);
	});
});

module.exports = convert;


/***/ }),
/* 21 */
/***/ (function(module, exports, __nested_webpack_require_146603__) {

var conversions = __nested_webpack_require_146603__(6);

/*
	this function routes a model to all other models.

	all functions that are routed have a property `.conversion` attached
	to the returned synthetic function. This property is an array
	of strings, each with the steps in between the 'from' and 'to'
	color models (inclusive).

	conversions that are not possible simply are not included.
*/

function buildGraph() {
	var graph = {};
	// https://jsperf.com/object-keys-vs-for-in-with-closure/3
	var models = Object.keys(conversions);

	for (var len = models.length, i = 0; i < len; i++) {
		graph[models[i]] = {
			// http://jsperf.com/1-vs-infinity
			// micro-opt, but this is simple.
			distance: -1,
			parent: null
		};
	}

	return graph;
}

// https://en.wikipedia.org/wiki/Breadth-first_search
function deriveBFS(fromModel) {
	var graph = buildGraph();
	var queue = [fromModel]; // unshift -> queue -> pop

	graph[fromModel].distance = 0;

	while (queue.length) {
		var current = queue.pop();
		var adjacents = Object.keys(conversions[current]);

		for (var len = adjacents.length, i = 0; i < len; i++) {
			var adjacent = adjacents[i];
			var node = graph[adjacent];

			if (node.distance === -1) {
				node.distance = graph[current].distance + 1;
				node.parent = current;
				queue.unshift(adjacent);
			}
		}
	}

	return graph;
}

function link(from, to) {
	return function (args) {
		return to(from(args));
	};
}

function wrapConversion(toModel, graph) {
	var path = [graph[toModel].parent, toModel];
	var fn = conversions[graph[toModel].parent][toModel];

	var cur = graph[toModel].parent;
	while (graph[cur].parent) {
		path.unshift(graph[cur].parent);
		fn = link(conversions[graph[cur].parent][cur], fn);
		cur = graph[cur].parent;
	}

	fn.conversion = path;
	return fn;
}

module.exports = function (fromModel) {
	var graph = deriveBFS(fromModel);
	var conversion = {};

	var models = Object.keys(graph);
	for (var len = models.length, i = 0; i < len; i++) {
		var toModel = models[i];
		var node = graph[toModel];

		if (node.parent === null) {
			// no possible conversion, or this node is the source model.
			continue;
		}

		conversion[toModel] = wrapConversion(toModel, graph);
	}

	return conversion;
};



/***/ }),
/* 22 */
/***/ (function(module, exports, __nested_webpack_require_148906__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _jquery = __nested_webpack_require_148906__(0);

var _jquery2 = _interopRequireDefault(_jquery);

var _ColorItem = __nested_webpack_require_148906__(2);

var _ColorItem2 = _interopRequireDefault(_ColorItem);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * Handles everything related to the colorpicker color
 * @ignore
 */
var ColorHandler = function () {
  /**
   * @param {Colorpicker} colorpicker
   */
  function ColorHandler(colorpicker) {
    _classCallCheck(this, ColorHandler);

    /**
     * @type {Colorpicker}
     */
    this.colorpicker = colorpicker;
  }

  /**
   * @returns {*|String|ColorItem}
   */


  _createClass(ColorHandler, [{
    key: 'bind',
    value: function bind() {
      // if the color option is set
      if (this.colorpicker.options.color) {
        this.color = this.createColor(this.colorpicker.options.color);
        return;
      }

      // if element[color] is empty and the input has a value
      if (!this.color && !!this.colorpicker.inputHandler.getValue()) {
        this.color = this.createColor(this.colorpicker.inputHandler.getValue(), this.colorpicker.options.autoInputFallback);
      }
    }
  }, {
    key: 'unbind',
    value: function unbind() {
      this.colorpicker.element.removeData('color');
    }

    /**
     * Returns the color string from the input value or the 'data-color' attribute of the input or element.
     * If empty, it returns the defaultValue parameter.
     *
     * @returns {String|*}
     */

  }, {
    key: 'getColorString',
    value: function getColorString() {
      if (!this.hasColor()) {
        return '';
      }

      return this.color.string(this.format);
    }

    /**
     * Sets the color value
     *
     * @param {String|ColorItem} val
     */

  }, {
    key: 'setColorString',
    value: function setColorString(val) {
      var color = val ? this.createColor(val) : null;

      this.color = color ? color : null;
    }

    /**
     * Creates a new color using the widget instance options (fallbackColor, format).
     *
     * @fires Colorpicker#colorpickerInvalid
     * @param {*} val
     * @param {boolean} fallbackOnInvalid
     * @returns {ColorItem}
     */

  }, {
    key: 'createColor',
    value: function createColor(val) {
      var fallbackOnInvalid = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

      var color = new _ColorItem2.default(this.resolveColorDelegate(val), this.format);

      if (!color.isValid()) {
        if (fallbackOnInvalid) {
          color = this.getFallbackColor();
        }

        /**
         * (Colorpicker) Fired when the color is invalid and the fallback color is going to be used.
         *
         * @event Colorpicker#colorpickerInvalid
         */
        this.colorpicker.trigger('colorpickerInvalid', color, val);
      }

      if (!this.isAlphaEnabled()) {
        // Alpha is disabled
        color.alpha = 1;
      }

      return color;
    }
  }, {
    key: 'getFallbackColor',
    value: function getFallbackColor() {
      if (this.fallback && this.fallback === this.color) {
        return this.color;
      }

      var fallback = this.resolveColorDelegate(this.fallback);

      var color = new _ColorItem2.default(fallback, this.format);

      if (!color.isValid()) {
        console.warn('The fallback color is invalid. Falling back to the previous color or black if any.');
        return this.color ? this.color : new _ColorItem2.default('#000000', this.format);
      }

      return color;
    }

    /**
     * @returns {ColorItem}
     */

  }, {
    key: 'assureColor',
    value: function assureColor() {
      if (!this.hasColor()) {
        this.color = this.getFallbackColor();
      }

      return this.color;
    }

    /**
     * Delegates the color resolution to the colorpicker extensions.
     *
     * @param {String|*} color
     * @param {boolean} realColor if true, the color should resolve into a real (not named) color code
     * @returns {ColorItem|String|*|null}
     */

  }, {
    key: 'resolveColorDelegate',
    value: function resolveColorDelegate(color) {
      var realColor = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

      var extResolvedColor = false;

      _jquery2.default.each(this.colorpicker.extensions, function (name, ext) {
        if (extResolvedColor !== false) {
          // skip if resolved
          return;
        }
        extResolvedColor = ext.resolveColor(color, realColor);
      });

      return extResolvedColor ? extResolvedColor : color;
    }

    /**
     * Checks if there is a color object, that it is valid and it is not a fallback
     * @returns {boolean}
     */

  }, {
    key: 'isInvalidColor',
    value: function isInvalidColor() {
      return !this.hasColor() || !this.color.isValid();
    }

    /**
     * Returns true if the useAlpha option is exactly true, false otherwise
     * @returns {boolean}
     */

  }, {
    key: 'isAlphaEnabled',
    value: function isAlphaEnabled() {
      return this.colorpicker.options.useAlpha !== false;
    }

    /**
     * Returns true if the current color object is an instance of Color, false otherwise.
     * @returns {boolean}
     */

  }, {
    key: 'hasColor',
    value: function hasColor() {
      return this.color instanceof _ColorItem2.default;
    }
  }, {
    key: 'fallback',
    get: function get() {
      return this.colorpicker.options.fallbackColor ? this.colorpicker.options.fallbackColor : this.hasColor() ? this.color : null;
    }

    /**
     * @returns {String|null}
     */

  }, {
    key: 'format',
    get: function get() {
      if (this.colorpicker.options.format) {
        return this.colorpicker.options.format;
      }

      if (this.hasColor() && this.color.hasTransparency() && this.color.format.match(/^hex/)) {
        return this.isAlphaEnabled() ? 'rgba' : 'hex';
      }

      if (this.hasColor()) {
        return this.color.format;
      }

      return 'rgb';
    }

    /**
     * Internal color getter
     *
     * @type {ColorItem|null}
     */

  }, {
    key: 'color',
    get: function get() {
      return this.colorpicker.element.data('color');
    }

    /**
     * Internal color setter
     *
     * @ignore
     * @param {ColorItem|null} value
     */
    ,
    set: function set(value) {
      this.colorpicker.element.data('color', value);

      if (value instanceof _ColorItem2.default && this.colorpicker.options.format === 'auto') {
        // If format is 'auto', use the first parsed one from now on
        this.colorpicker.options.format = this.color.format;
      }
    }
  }]);

  return ColorHandler;
}();

exports.default = ColorHandler;
module.exports = exports.default;

/***/ }),
/* 23 */
/***/ (function(module, exports, __nested_webpack_require_156531__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _jquery = __nested_webpack_require_156531__(0);

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * Handles everything related to the colorpicker UI
 * @ignore
 */
var PickerHandler = function () {
  /**
   * @param {Colorpicker} colorpicker
   */
  function PickerHandler(colorpicker) {
    _classCallCheck(this, PickerHandler);

    /**
     * @type {Colorpicker}
     */
    this.colorpicker = colorpicker;
    /**
     * @type {jQuery}
     */
    this.picker = null;
  }

  _createClass(PickerHandler, [{
    key: 'bind',
    value: function bind() {
      /**
       * @type {jQuery|HTMLElement}
       */
      var picker = this.picker = (0, _jquery2.default)(this.options.template);

      if (this.options.customClass) {
        picker.addClass(this.options.customClass);
      }

      if (this.options.horizontal) {
        picker.addClass('colorpicker-horizontal');
      }

      if (this._supportsAlphaBar()) {
        this.options.useAlpha = true;
        picker.addClass('colorpicker-with-alpha');
      } else {
        this.options.useAlpha = false;
      }
    }
  }, {
    key: 'attach',
    value: function attach() {
      // Inject the colorpicker element into the DOM
      var pickerParent = this.colorpicker.container ? this.colorpicker.container : null;

      if (pickerParent) {
        this.picker.appendTo(pickerParent);
      }
    }
  }, {
    key: 'unbind',
    value: function unbind() {
      this.picker.remove();
    }
  }, {
    key: '_supportsAlphaBar',
    value: function _supportsAlphaBar() {
      return (this.options.useAlpha || this.colorpicker.colorHandler.hasColor() && this.color.hasTransparency()) && this.options.useAlpha !== false && (!this.options.format || this.options.format && !this.options.format.match(/^hex([36])?$/i));
    }

    /**
     * Changes the color adjustment bars using the current color object information.
     */

  }, {
    key: 'update',
    value: function update() {
      if (!this.colorpicker.colorHandler.hasColor()) {
        return;
      }

      var vertical = this.options.horizontal !== true,
          slider = vertical ? this.options.sliders : this.options.slidersHorz;

      var saturationGuide = this.picker.find('.colorpicker-saturation .colorpicker-guide'),
          hueGuide = this.picker.find('.colorpicker-hue .colorpicker-guide'),
          alphaGuide = this.picker.find('.colorpicker-alpha .colorpicker-guide');

      var hsva = this.color.toHsvaRatio();

      // Set guides position
      if (hueGuide.length) {
        hueGuide.css(vertical ? 'top' : 'left', (vertical ? slider.hue.maxTop : slider.hue.maxLeft) * (1 - hsva.h));
      }
      if (alphaGuide.length) {
        alphaGuide.css(vertical ? 'top' : 'left', (vertical ? slider.alpha.maxTop : slider.alpha.maxLeft) * (1 - hsva.a));
      }
      if (saturationGuide.length) {
        saturationGuide.css({
          'top': slider.saturation.maxTop - hsva.v * slider.saturation.maxTop,
          'left': hsva.s * slider.saturation.maxLeft
        });
      }

      // Set saturation hue background
      this.picker.find('.colorpicker-saturation').css('backgroundColor', this.color.getCloneHueOnly().toHexString()); // we only need hue

      // Set alpha color gradient
      var hexColor = this.color.toHexString();

      var alphaBg = '';

      if (this.options.horizontal) {
        alphaBg = 'linear-gradient(to right, ' + hexColor + ' 0%, transparent 100%)';
      } else {
        alphaBg = 'linear-gradient(to bottom, ' + hexColor + ' 0%, transparent 100%)';
      }

      this.picker.find('.colorpicker-alpha-color').css('background', alphaBg);
    }
  }, {
    key: 'options',
    get: function get() {
      return this.colorpicker.options;
    }
  }, {
    key: 'color',
    get: function get() {
      return this.colorpicker.colorHandler.color;
    }
  }]);

  return PickerHandler;
}();

exports.default = PickerHandler;
module.exports = exports.default;

/***/ }),
/* 24 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * Handles everything related to the colorpicker addon
 * @ignore
 */

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var AddonHandler = function () {
  /**
   * @param {Colorpicker} colorpicker
   */
  function AddonHandler(colorpicker) {
    _classCallCheck(this, AddonHandler);

    /**
     * @type {Colorpicker}
     */
    this.colorpicker = colorpicker;
    /**
     * @type {jQuery}
     */
    this.addon = null;
  }

  _createClass(AddonHandler, [{
    key: 'hasAddon',
    value: function hasAddon() {
      return !!this.addon;
    }
  }, {
    key: 'bind',
    value: function bind() {
      /**
       * @type {*|jQuery}
       */
      this.addon = this.colorpicker.options.addon ? this.colorpicker.element.find(this.colorpicker.options.addon) : null;

      if (this.addon && this.addon.length === 0) {
        // not found
        this.addon = null;
      }
    }
  }, {
    key: 'unbind',
    value: function unbind() {
      if (this.hasAddon()) {
        this.addon.off('.colorpicker');
      }
    }

    /**
     * If the addon element is present, its background color is updated
     */

  }, {
    key: 'update',
    value: function update() {
      if (!this.colorpicker.colorHandler.hasColor() || !this.hasAddon()) {
        return;
      }

      var colorStr = this.colorpicker.colorHandler.getColorString();

      var styles = { 'background': colorStr };

      var icn = this.addon.find('i').eq(0);

      if (icn.length > 0) {
        icn.css(styles);
      } else {
        this.addon.css(styles);
      }
    }
  }]);

  return AddonHandler;
}();

exports.default = AddonHandler;
module.exports = exports.default;

/***/ })
/******/ ]);
});
//# sourceMappingURL=bootstrap-colorpicker.js.map
}.call(window, false, false));


/***/ }),

/***/ "jquery":
/*!*************************!*\
  !*** external "jQuery" ***!
  \*************************/
/***/ ((module) => {

"use strict";
module.exports = window["jQuery"];

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be in strict mode.
(() => {
"use strict";
/*!***************************************!*\
  !*** ./js/pages/order-states/form.js ***!
  \***************************************/


var _colorpicker = __webpack_require__(/*! @app/utils/colorpicker */ "./js/app/utils/colorpicker.js");

var _colorpicker2 = _interopRequireDefault(_colorpicker);

var _translatableChoice = __webpack_require__(/*! @components/form/translatable-choice */ "./js/components/form/translatable-choice.js");

var _translatableChoice2 = _interopRequireDefault(_translatableChoice);

var _translatableInput = __webpack_require__(/*! @components/translatable-input */ "./js/components/translatable-input.js");

var _translatableInput2 = _interopRequireDefault(_translatableInput);

var _formMap = __webpack_require__(/*! @pages/order-states/form-map */ "./js/pages/order-states/form-map.js");

var _formMap2 = _interopRequireDefault(_formMap);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 */

var _window = window,
    $ = _window.$;


$(function () {
  (0, _colorpicker2.default)();
  new _translatableInput2.default();
  new _translatableChoice2.default();

  var templatePreviewWindow = null;
  function viewTemplates($uri) {
    if (templatePreviewWindow != null && !templatePreviewWindow.closed) {
      templatePreviewWindow.close();
    }
    templatePreviewWindow = window.open($uri, 'tpl_viewing', 'toolbar=0,' + 'location=0,' + 'directories=0,' + 'statfr=no,' + 'menubar=0,' + 'scrollbars=yes,' + 'resizable=yes,' + 'width=520,' + 'height=400,' + 'top=50,' + 'left=300');
    templatePreviewWindow.focus();
  }

  $(document).ready(function () {
    if (!$(_formMap2.default.sendEmailSelector).is(':checked')) {
      $(_formMap2.default.mailTemplateSelector).hide();
    }
    $(document).on('change', _formMap2.default.sendEmailSelector, function () {
      $(_formMap2.default.mailTemplateSelector).slideToggle();
    });

    $(document).on('click', _formMap2.default.mailTemplatePreview, function (event) {
      var $element = $(event.currentTarget);
      var $select = $element.closest('.form-group').find('select.translatable_choice:visible');
      var $uri = $select.find('option:selected').attr('data-preview');

      viewTemplates($uri);
    });
  });
});
})();

window.order_states_form = __webpack_exports__;
/******/ })()
;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9bbmFtZV0vLi9qcy9hcHAvdXRpbHMvY29sb3JwaWNrZXIuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vanMvY29tcG9uZW50cy9ldmVudC1lbWl0dGVyLmpzIiwid2VicGFjazovL1tuYW1lXS8uL2pzL2NvbXBvbmVudHMvZm9ybS90cmFuc2xhdGFibGUtY2hvaWNlLmpzIiwid2VicGFjazovL1tuYW1lXS8uL2pzL2NvbXBvbmVudHMvdHJhbnNsYXRhYmxlLWlucHV0LmpzIiwid2VicGFjazovL1tuYW1lXS8uL2pzL3BhZ2VzL29yZGVyLXN0YXRlcy9mb3JtLW1hcC5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvYmFiZWwtcnVudGltZS9jb3JlLWpzL29iamVjdC9kZWZpbmUtcHJvcGVydHkuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jbGFzc0NhbGxDaGVjay5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvYmFiZWwtcnVudGltZS9oZWxwZXJzL2NyZWF0ZUNsYXNzLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvZm4vb2JqZWN0L2RlZmluZS1wcm9wZXJ0eS5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX2EtZnVuY3Rpb24uanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19hbi1vYmplY3QuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19jb3JlLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fY3R4LmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fZGVzY3JpcHRvcnMuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19kb20tY3JlYXRlLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fZXhwb3J0LmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fZmFpbHMuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19nbG9iYWwuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19oYXMuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19oaWRlLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9faWU4LWRvbS1kZWZpbmUuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19pcy1vYmplY3QuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19vYmplY3QtZHAuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19wcm9wZXJ0eS1kZXNjLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fdG8tcHJpbWl0aXZlLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9lczYub2JqZWN0LmRlZmluZS1wcm9wZXJ0eS5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvZXZlbnRzL2V2ZW50cy5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvYm9vdHN0cmFwLWNvbG9ycGlja2VyL2Rpc3QvanMvYm9vdHN0cmFwLWNvbG9ycGlja2VyLmpzIiwid2VicGFjazovL1tuYW1lXS9leHRlcm5hbCBcImpRdWVyeVwiIiwid2VicGFjazovL1tuYW1lXS93ZWJwYWNrL2Jvb3RzdHJhcCIsIndlYnBhY2s6Ly9bbmFtZV0vLi9qcy9wYWdlcy9vcmRlci1zdGF0ZXMvZm9ybS5qcyJdLCJuYW1lcyI6WyJ3aW5kb3ciLCIkIiwiaW5pdCIsImluaXREYXRlUGlja2VycyIsImVhY2giLCJpIiwicGlja2VyIiwiY29sb3JwaWNrZXIiLCJFdmVudEVtaXR0ZXIiLCJFdmVudEVtaXR0ZXJDbGFzcyIsIlRyYW5zbGF0YWJsZUNob2ljZSIsImRvY3VtZW50Iiwib24iLCJldmVudCIsImZpbHRlclNlbGVjdCIsInRyaWdnZXIiLCIkZWxlbWVudCIsImN1cnJlbnRUYXJnZXQiLCIkZm9ybUdyb3VwIiwiY2xvc2VzdCIsImxhbmd1YWdlIiwiZmluZCIsInZhbCIsInBhcmVudCIsInNob3ciLCIkc2VsZWN0cyIsIm5vdCIsImluZGV4IiwiaXRlbSIsImhpZGUiLCJiaW5kVmFsdWVTZWxlY3Rpb24iLCJlbGVtZW50IiwiJHNlbGVjdCIsInNlbGVjdElkIiwiYXR0ciIsIlRyYW5zbGF0YWJsZUlucHV0Iiwib3B0aW9ucyIsIm9wdHMiLCJsb2NhbGVJdGVtU2VsZWN0b3IiLCJsb2NhbGVCdXR0b25TZWxlY3RvciIsImxvY2FsZUlucHV0U2VsZWN0b3IiLCJzZWxlY3RlZExvY2FsZSIsImRhdGEiLCJ0b2dnbGVMYW5ndWFnZSIsImJpbmQiLCJ0b2dnbGVJbnB1dHMiLCJyZWZyZXNoRm9ybUlucHV0cyIsImZvcm0iLCJyZWZyZXNoSW5wdXRzIiwiZ2V0U2VsZWN0ZWRMb2NhbGUiLCJlbWl0IiwibG9jYWxlSXRlbSIsInRhcmdldCIsImxvY2FsZUJ1dHRvbiIsImNoYW5nZUxhbmd1YWdlVXJsIiwidGV4dCIsImFkZENsYXNzIiwicmVtb3ZlQ2xhc3MiLCJzYXZlU2VsZWN0ZWRMYW5ndWFnZSIsInBvc3QiLCJ1cmwiLCJsYW5ndWFnZV9pc29fY29kZSIsInNlbmRFbWFpbFNlbGVjdG9yIiwibWFpbFRlbXBsYXRlU2VsZWN0b3IiLCJtYWlsVGVtcGxhdGVQcmV2aWV3IiwidGVtcGxhdGVQcmV2aWV3V2luZG93Iiwidmlld1RlbXBsYXRlcyIsIiR1cmkiLCJjbG9zZWQiLCJjbG9zZSIsIm9wZW4iLCJmb2N1cyIsInJlYWR5IiwiRm9ybU1hcCIsImlzIiwic2xpZGVUb2dnbGUiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7QUF3QkE7O2NBRVlBLE07SUFBTEMsQyxXQUFBQSxDOztBQUVQOzs7QUE1QkE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUErQkEsSUFBTUMsT0FBTyxTQUFTQyxlQUFULEdBQTJCO0FBQ3RDRixJQUFFLGlDQUFGLEVBQXFDRyxJQUFyQyxDQUEwQyxVQUFDQyxDQUFELEVBQUlDLE1BQUosRUFBZTtBQUN2REwsTUFBRUssTUFBRixFQUFVQyxXQUFWO0FBQ0QsR0FGRDtBQUdELENBSkQ7O2tCQU1lTCxJOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNaZjs7Ozs7O0FBRUE7Ozs7QUFJTyxJQUFNTSxlQUFlQSx1QkFBQSxJQUFJQyxnQkFBSixFQUFyQixDLENBL0JQOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O2tCQWlDZUQsWTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDakNmOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O2NBeUJZUixNO0lBQUxDLEMsV0FBQUEsQzs7QUFFUDs7OztJQUdxQlMsa0I7QUFDbkIsZ0NBQWM7QUFBQTs7QUFBQTs7QUFDWjtBQUNBVCxNQUFFVSxRQUFGLEVBQVlDLEVBQVosQ0FBZSxRQUFmLEVBQXlCLHFDQUF6QixFQUFnRSxVQUFDQyxLQUFELEVBQVc7QUFDekUsWUFBS0MsWUFBTCxDQUFrQkQsS0FBbEI7QUFDRCxLQUZEOztBQUlBWixNQUFFLHFDQUFGLEVBQXlDYyxPQUF6QyxDQUFpRCxRQUFqRDtBQUNEOzs7O2lDQUVZRixLLEVBQU87QUFDbEIsVUFBTUcsV0FBV2YsRUFBRVksTUFBTUksYUFBUixDQUFqQjtBQUNBLFVBQU1DLGFBQWFGLFNBQVNHLE9BQVQsQ0FBaUIsYUFBakIsQ0FBbkI7QUFDQSxVQUFNQyxXQUFXSixTQUFTSyxJQUFULENBQWMsaUJBQWQsRUFBaUNDLEdBQWpDLEVBQWpCOztBQUVBO0FBQ0FKLGlCQUFXRyxJQUFYLGdEQUE2REQsUUFBN0QsU0FBMkVHLE1BQTNFLEdBQW9GQyxJQUFwRjs7QUFFQSxVQUFNQyxXQUFXUCxXQUFXRyxJQUFYLENBQWdCLDRCQUFoQixDQUFqQjs7QUFFQTtBQUNBSSxlQUFTQyxHQUFULGdEQUEwRE4sUUFBMUQsU0FBd0VoQixJQUF4RSxDQUE2RSxVQUFDdUIsS0FBRCxFQUFRQyxJQUFSLEVBQWlCO0FBQzVGM0IsVUFBRTJCLElBQUYsRUFBUUwsTUFBUixHQUFpQk0sSUFBakI7QUFDRCxPQUZEOztBQUlBO0FBQ0EsV0FBS0Msa0JBQUwsQ0FBd0JMLFFBQXhCO0FBQ0Q7Ozt1Q0FFa0JBLFEsRUFBVTtBQUMzQkEsZUFBU3JCLElBQVQsQ0FBYyxVQUFDdUIsS0FBRCxFQUFRSSxPQUFSLEVBQW9CO0FBQ2hDOUIsVUFBRThCLE9BQUYsRUFBV25CLEVBQVgsQ0FBYyxRQUFkLEVBQXdCLFVBQUNDLEtBQUQsRUFBVztBQUNqQyxjQUFNbUIsVUFBVS9CLEVBQUVZLE1BQU1JLGFBQVIsQ0FBaEI7QUFDQSxjQUFNZ0IsV0FBV0QsUUFBUUUsSUFBUixDQUFhLElBQWIsQ0FBakI7QUFDQWpDLGtCQUFNZ0MsUUFBTixhQUF3QlgsR0FBeEIsQ0FBNEJVLFFBQVFYLElBQVIsQ0FBYSxpQkFBYixFQUFnQ0MsR0FBaEMsRUFBNUI7QUFDRCxTQUpEO0FBS0QsT0FORDtBQU9EOzs7OztrQkFyQ2tCWixrQjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ0xyQjs7OztjQUVZVixNO0lBQUxDLEMsV0FBQUEsQzs7QUFFUDs7Ozs7QUE3QkE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7SUFrQ01rQyxpQjtBQUNKLDZCQUFZQyxPQUFaLEVBQXFCO0FBQUE7O0FBQUE7O0FBQ25CLFFBQU1DLE9BQU9ELFdBQVcsRUFBeEI7O0FBRUEsU0FBS0Usa0JBQUwsR0FBMEJELEtBQUtDLGtCQUFMLElBQTJCLGlCQUFyRDtBQUNBLFNBQUtDLG9CQUFMLEdBQTRCRixLQUFLRSxvQkFBTCxJQUE2QixnQkFBekQ7QUFDQSxTQUFLQyxtQkFBTCxHQUEyQkgsS0FBS0csbUJBQUwsSUFBNEIsa0JBQXZEO0FBQ0EsU0FBS0MsY0FBTCxHQUFzQnhDLEVBQUUsS0FBS3FDLGtCQUFQLEVBQTJCSSxJQUEzQixDQUFnQyxRQUFoQyxDQUF0Qjs7QUFFQXpDLE1BQUUsTUFBRixFQUFVVyxFQUFWLENBQ0UsT0FERixFQUVFLEtBQUswQixrQkFGUCxFQUdFLEtBQUtLLGNBQUwsQ0FBb0JDLElBQXBCLENBQXlCLElBQXpCLENBSEY7QUFLQXBDLCtCQUFhSSxFQUFiLENBQWdCLGtCQUFoQixFQUFvQyxLQUFLaUMsWUFBTCxDQUFrQkQsSUFBbEIsQ0FBdUIsSUFBdkIsQ0FBcEM7O0FBRUEsV0FBTztBQUNMTiwwQkFBb0IsS0FBS0Esa0JBRHBCO0FBRUxDLDRCQUFzQixLQUFLQSxvQkFGdEI7QUFHTEMsMkJBQXFCLEtBQUtBLG1CQUhyQjs7QUFLTDs7O0FBR0FNLHlCQUFtQiwyQkFBQ0MsSUFBRCxFQUFVO0FBQUUsY0FBS0MsYUFBTCxDQUFtQkQsSUFBbkI7QUFBMkIsT0FSckQ7O0FBVUw7OztBQUdBRSx5QkFBbUI7QUFBQSxlQUFNLE1BQUtSLGNBQVg7QUFBQTtBQWJkLEtBQVA7QUFlRDs7QUFFRDs7Ozs7Ozs7O2tDQUtjTSxJLEVBQU07QUFDbEIsVUFBSSxDQUFDLEtBQUtOLGNBQVYsRUFBMEI7QUFDeEI7QUFDRDs7QUFFRGpDLGlDQUFhMEMsSUFBYixDQUFrQixrQkFBbEIsRUFBc0M7QUFDcENULHdCQUFnQixLQUFLQSxjQURlO0FBRXBDTTtBQUZvQyxPQUF0QztBQUlEOztBQUVEOzs7Ozs7Ozs7O21DQU9lbEMsSyxFQUFPO0FBQ3BCLFVBQU1zQyxhQUFhbEQsRUFBRVksTUFBTXVDLE1BQVIsQ0FBbkI7QUFDQSxVQUFNTCxPQUFPSSxXQUFXaEMsT0FBWCxDQUFtQixNQUFuQixDQUFiO0FBQ0EsV0FBS3NCLGNBQUwsR0FBc0JVLFdBQVdULElBQVgsQ0FBZ0IsUUFBaEIsQ0FBdEI7QUFDQSxXQUFLTSxhQUFMLENBQW1CRCxJQUFuQjtBQUNEOztBQUVEOzs7Ozs7Ozs7O2lDQU9hbEMsSyxFQUFPO0FBQUEsVUFDWGtDLElBRFcsR0FDSGxDLEtBREcsQ0FDWGtDLElBRFc7O0FBRWxCLFdBQUtOLGNBQUwsR0FBc0I1QixNQUFNNEIsY0FBNUI7QUFDQSxVQUFNWSxlQUFlTixLQUFLMUIsSUFBTCxDQUFVLEtBQUtrQixvQkFBZixDQUFyQjtBQUNBLFVBQU1lLG9CQUFvQkQsYUFBYVgsSUFBYixDQUFrQixxQkFBbEIsQ0FBMUI7O0FBRUFXLG1CQUFhRSxJQUFiLENBQWtCLEtBQUtkLGNBQXZCO0FBQ0FNLFdBQUsxQixJQUFMLENBQVUsS0FBS21CLG1CQUFmLEVBQW9DZ0IsUUFBcEMsQ0FBNkMsUUFBN0M7QUFDQVQsV0FDRzFCLElBREgsQ0FDVyxLQUFLbUIsbUJBRGhCLG1CQUNpRCxLQUFLQyxjQUR0RCxFQUVHZ0IsV0FGSCxDQUVlLFFBRmY7O0FBSUEsVUFBSUgsaUJBQUosRUFBdUI7QUFDckIsYUFBS0ksb0JBQUwsQ0FBMEJKLGlCQUExQixFQUE2QyxLQUFLYixjQUFsRDtBQUNEO0FBQ0Y7O0FBRUQ7Ozs7Ozs7Ozs7O3lDQVFxQmEsaUIsRUFBbUJiLGMsRUFBZ0I7QUFDdER4QyxRQUFFMEQsSUFBRixDQUFPO0FBQ0xDLGFBQUtOLGlCQURBO0FBRUxaLGNBQU07QUFDSm1CLDZCQUFtQnBCO0FBRGY7QUFGRCxPQUFQO0FBTUQ7Ozs7O2tCQUdZTixpQjs7Ozs7Ozs7Ozs7Ozs7OztBQzNJZjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQXlCQTs7O2tCQUdlO0FBQ2IyQixxQkFBbUIseUJBRE47QUFFYkMsd0JBQXNCLDhCQUZUO0FBR2JDLHVCQUFxQjtBQUhSLEM7Ozs7Ozs7Ozs7QUM1QmYsa0JBQWtCLFlBQVksbUJBQU8sQ0FBQyw4R0FBMkMsc0I7Ozs7Ozs7Ozs7O0FDQXBFOztBQUViLGtCQUFrQjs7QUFFbEIsZUFBZTtBQUNmO0FBQ0E7QUFDQTtBQUNBLEU7Ozs7Ozs7Ozs7O0FDUmE7O0FBRWIsa0JBQWtCOztBQUVsQixzQkFBc0IsbUJBQU8sQ0FBQyx5R0FBbUM7O0FBRWpFOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixlQUFlO0FBQ2Y7QUFDQSxtQkFBbUIsa0JBQWtCO0FBQ3JDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDLEc7Ozs7Ozs7Ozs7QUMxQkQsbUJBQU8sQ0FBQyxzSEFBMEM7QUFDbEQsY0FBYyx3R0FBcUM7QUFDbkQ7QUFDQTtBQUNBOzs7Ozs7Ozs7OztBQ0pBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7OztBQ0hBLGVBQWUsbUJBQU8sQ0FBQywwRUFBYztBQUNyQztBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNKQSw2QkFBNkI7QUFDN0IsdUNBQXVDOzs7Ozs7Ozs7OztBQ0R2QztBQUNBLGdCQUFnQixtQkFBTyxDQUFDLDRFQUFlO0FBQ3ZDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNuQkE7QUFDQSxrQkFBa0IsbUJBQU8sQ0FBQyxrRUFBVTtBQUNwQyxpQ0FBaUMsUUFBUSxtQkFBbUIsVUFBVSxFQUFFLEVBQUU7QUFDMUUsQ0FBQzs7Ozs7Ozs7Ozs7QUNIRCxlQUFlLG1CQUFPLENBQUMsMEVBQWM7QUFDckMsZUFBZSxrR0FBNkI7QUFDNUM7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNOQSxhQUFhLG1CQUFPLENBQUMsb0VBQVc7QUFDaEMsV0FBVyxtQkFBTyxDQUFDLGdFQUFTO0FBQzVCLFVBQVUsbUJBQU8sQ0FBQyw4REFBUTtBQUMxQixXQUFXLG1CQUFPLENBQUMsZ0VBQVM7QUFDNUIsVUFBVSxtQkFBTyxDQUFDLDhEQUFRO0FBQzFCOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUVBQWlFO0FBQ2pFO0FBQ0Esa0ZBQWtGO0FBQ2xGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXO0FBQ1gsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSwrQ0FBK0M7QUFDL0M7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsY0FBYztBQUNkLGNBQWM7QUFDZCxjQUFjO0FBQ2QsY0FBYztBQUNkLGVBQWU7QUFDZixlQUFlO0FBQ2YsZUFBZTtBQUNmLGdCQUFnQjtBQUNoQjs7Ozs7Ozs7Ozs7QUM3REE7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNOQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EseUNBQXlDOzs7Ozs7Ozs7OztBQ0x6Qyx1QkFBdUI7QUFDdkI7QUFDQTtBQUNBOzs7Ozs7Ozs7OztBQ0hBLFNBQVMsbUJBQU8sQ0FBQywwRUFBYztBQUMvQixpQkFBaUIsbUJBQU8sQ0FBQyxrRkFBa0I7QUFDM0MsaUJBQWlCLG1CQUFPLENBQUMsOEVBQWdCO0FBQ3pDO0FBQ0EsQ0FBQztBQUNEO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNQQSxrQkFBa0IsbUJBQU8sQ0FBQyw4RUFBZ0IsTUFBTSxtQkFBTyxDQUFDLGtFQUFVO0FBQ2xFLCtCQUErQixtQkFBTyxDQUFDLDRFQUFlLGdCQUFnQixtQkFBbUIsVUFBVSxFQUFFLEVBQUU7QUFDdkcsQ0FBQzs7Ozs7Ozs7Ozs7QUNGRDtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7O0FDRkEsZUFBZSxtQkFBTyxDQUFDLDBFQUFjO0FBQ3JDLHFCQUFxQixtQkFBTyxDQUFDLG9GQUFtQjtBQUNoRCxrQkFBa0IsbUJBQU8sQ0FBQyxnRkFBaUI7QUFDM0M7O0FBRUEsU0FBUyxHQUFHLG1CQUFPLENBQUMsOEVBQWdCO0FBQ3BDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHLFlBQVk7QUFDZjtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNmQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7OztBQ1BBO0FBQ0EsZUFBZSxtQkFBTyxDQUFDLDBFQUFjO0FBQ3JDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7OztBQ1hBLGNBQWMsbUJBQU8sQ0FBQyxvRUFBVztBQUNqQztBQUNBLGlDQUFpQyxtQkFBTyxDQUFDLDhFQUFnQixjQUFjLGlCQUFpQixpR0FBeUIsRUFBRTs7Ozs7Ozs7Ozs7O0FDRm5IO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRWE7O0FBRWI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLENBQUM7QUFDRDtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUM7QUFDRDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsaUJBQWlCLHNCQUFzQjtBQUN2Qzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlO0FBQ2Y7QUFDQTtBQUNBO0FBQ0E7QUFDQSxjQUFjO0FBQ2Q7O0FBRUE7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQSxtQkFBbUIsU0FBUztBQUM1QjtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBLEtBQUs7QUFDTDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0EsZUFBZTtBQUNmO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQOztBQUVBLGlDQUFpQyxRQUFRO0FBQ3pDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtQkFBbUIsaUJBQWlCO0FBQ3BDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0Esc0NBQXNDLFFBQVE7QUFDOUM7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLGlCQUFpQixPQUFPO0FBQ3hCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLFFBQVEseUJBQXlCO0FBQ2pDO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsaUJBQWlCLGdCQUFnQjtBQUNqQztBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUM3YkE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBbUMsUUFBYTtBQUNoRCwyQkFBMkIsbUJBQU8sQ0FBQyxzQkFBUTtBQUMzQztBQUNBO0FBQ0E7QUFDQSw2Q0FBNkMsbUJBQU8sQ0FBQyxzQkFBUTtBQUM3RDtBQUNBO0FBQ0EsQ0FBQztBQUNELG9DQUFvQztBQUNwQztBQUNBO0FBQ0E7QUFDQTtBQUNBLG1CQUFtQiwrQkFBbUI7QUFDdEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwwRUFBMEUsK0JBQW1CO0FBQzdGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsVUFBVSwrQkFBbUI7QUFDN0I7QUFDQTtBQUNBLFVBQVUsK0JBQW1CO0FBQzdCO0FBQ0E7QUFDQSxVQUFVLCtCQUFtQjtBQUM3QixlQUFlLCtCQUFtQjtBQUNsQyxrREFBa0QsZ0NBQWdDO0FBQ2xGO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsVUFBVSwrQkFBbUI7QUFDN0I7QUFDQSxnRUFBZ0Usa0JBQWtCO0FBQ2xGO0FBQ0EseURBQXlELGNBQWM7QUFDdkU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxVQUFVLCtCQUFtQjtBQUM3QixnQ0FBZ0MsK0JBQW1CO0FBQ25EO0FBQ0E7QUFDQTtBQUNBLFdBQVcsK0JBQW1CO0FBQzlCLGlEQUFpRCxpQ0FBaUM7QUFDbEYsMEVBQTBFLCtCQUFtQiwyQkFBMkIsbUJBQW1CLEVBQUU7QUFDN0k7QUFDQTtBQUNBO0FBQ0E7QUFDQSxVQUFVLCtCQUFtQjtBQUM3QjtBQUNBLG1DQUFtQywwQkFBMEIsRUFBRTtBQUMvRCx5Q0FBeUMsZUFBZTtBQUN4RCxXQUFXLCtCQUFtQjtBQUM5QjtBQUNBO0FBQ0E7QUFDQTtBQUNBLFVBQVUsK0JBQW1CLGlDQUFpQywrREFBK0Q7QUFDN0g7QUFDQTtBQUNBLFVBQVUsK0JBQW1CO0FBQzdCO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQiwrQkFBbUIsQ0FBQywrQkFBbUI7QUFDeEQsVUFBVTtBQUNWO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBLE9BQU87QUFDUDtBQUNBLGlDQUFpQywrQkFBbUI7O0FBRXBEOzs7QUFHQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRCxnQ0FBZ0MsMkNBQTJDLGdCQUFnQixrQkFBa0IsT0FBTywyQkFBMkIsd0RBQXdELGdDQUFnQyx1REFBdUQsMkRBQTJELEVBQUUsRUFBRSx5REFBeUQscUVBQXFFLDZEQUE2RCxvQkFBb0IsR0FBRyxFQUFFOztBQUVqakIsY0FBYywrQkFBbUI7O0FBRWpDOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixpREFBaUQsMENBQTBDLDBEQUEwRCxFQUFFOztBQUV2SjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYSxZQUFZO0FBQ3pCLGFBQWEsT0FBTztBQUNwQjtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBLGNBQWM7QUFDZDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsY0FBYztBQUNkO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLG1CQUFtQjtBQUNoQyxhQUFhLFFBQVE7QUFDckIsY0FBYztBQUNkOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZSxNQUFNO0FBQ3JCOztBQUVBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZSxNQUFNO0FBQ3JCOztBQUVBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWUsTUFBTTtBQUNyQjs7QUFFQSxHQUFHO0FBQ0g7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWUsTUFBTTtBQUNyQjs7QUFFQSxHQUFHO0FBQ0g7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWUsTUFBTTtBQUNyQjs7QUFFQSxHQUFHO0FBQ0g7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWUsTUFBTTtBQUNyQjs7QUFFQSxHQUFHO0FBQ0g7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWUsTUFBTTtBQUNyQjs7QUFFQSxHQUFHO0FBQ0g7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWUsTUFBTTtBQUNyQjs7QUFFQSxHQUFHO0FBQ0g7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWUsTUFBTTtBQUNyQjs7QUFFQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHOztBQUVIO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBOztBQUVBLE9BQU87QUFDUDtBQUNBLGlDQUFpQyxnQ0FBbUI7O0FBRXBEOzs7QUFHQTtBQUNBO0FBQ0EsQ0FBQztBQUNEOztBQUVBLGdDQUFnQywyQ0FBMkMsZ0JBQWdCLGtCQUFrQixPQUFPLDJCQUEyQix3REFBd0QsZ0NBQWdDLHVEQUF1RCwyREFBMkQsRUFBRSxFQUFFLHlEQUF5RCxxRUFBcUUsNkRBQTZELG9CQUFvQixHQUFHLEVBQUUsR0FBRztBQUNwakI7QUFDQTs7O0FBR0EsYUFBYSxnQ0FBbUI7O0FBRWhDOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixpREFBaUQsMENBQTBDLDBEQUEwRCxFQUFFOztBQUV2SjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLFdBQVc7QUFDeEIsYUFBYSxXQUFXO0FBQ3hCLGFBQWEsV0FBVztBQUN4QixhQUFhLFdBQVc7QUFDeEI7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7O0FBRUg7QUFDQSxDQUFDOztBQUVEO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlLE9BQU87QUFDdEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjtBQUNBO0FBQ0Esd0ZBQXdGLGFBQWE7QUFDckc7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGtCQUFrQjtBQUNsQjs7QUFFQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxlQUFlLDJDQUEyQztBQUMxRCxlQUFlLFlBQVk7QUFDM0I7O0FBRUEsR0FBRztBQUNIOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRzs7QUFFSDtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWEsMkNBQTJDO0FBQ3hELGFBQWEsWUFBWTtBQUN6QjtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0EsaUJBQWlCO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxnQkFBZ0I7QUFDaEI7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxnQkFBZ0I7QUFDaEI7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlLDJDQUEyQztBQUMxRDtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCOztBQUVBLEdBQUc7QUFDSDs7O0FBR0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjs7QUFFQSxHQUFHO0FBQ0g7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZSxPQUFPO0FBQ3RCO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGVBQWUsSUFBSTtBQUNuQjs7QUFFQSxHQUFHO0FBQ0g7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZSxPQUFPO0FBQ3RCO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGVBQWUsSUFBSTtBQUNuQjs7QUFFQSxHQUFHO0FBQ0g7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZSxPQUFPO0FBQ3RCO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGVBQWUsSUFBSTtBQUNuQjs7QUFFQSxHQUFHO0FBQ0g7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZSxPQUFPO0FBQ3RCO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGVBQWUsT0FBTztBQUN0Qjs7QUFFQSxHQUFHO0FBQ0g7OztBQUdBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7O0FBRUEsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjs7QUFFQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCOztBQUVBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7O0FBRUEsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjs7QUFFQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjs7QUFFQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7O0FBRUEsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZSxZQUFZO0FBQzNCLGlCQUFpQjtBQUNqQjs7QUFFQSxHQUFHO0FBQ0g7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZSwyQ0FBMkM7QUFDMUQ7QUFDQSxpQkFBaUI7QUFDakI7O0FBRUEsR0FBRztBQUNIO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7O0FBRUEsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCOztBQUVBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7O0FBRUEsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjs7QUFFQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCOztBQUVBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7O0FBRUEsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCOztBQUVBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjs7QUFFQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlLGdCQUFnQjtBQUMvQjtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCOztBQUVBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBLE9BQU87QUFDUDtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0EsT0FBTzs7QUFFUDtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQSxlQUFlLElBQUk7QUFDbkI7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCLE9BQU87QUFDeEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlLFNBQVM7QUFDeEI7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjs7QUFFQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSwrQkFBK0IsR0FBRztBQUNsQztBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGVBQWUsT0FBTztBQUN0QiwrQ0FBK0M7QUFDL0MsMENBQTBDO0FBQzFDLDJDQUEyQztBQUMzQztBQUNBLGlCQUFpQjtBQUNqQjs7QUFFQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxzQ0FBc0MsR0FBRztBQUN6Qzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWUsU0FBUztBQUN4QixpREFBaUQ7QUFDakQsd0NBQXdDO0FBQ3hDLDJDQUEyQztBQUMzQztBQUNBLGlCQUFpQixPQUFPO0FBQ3hCOztBQUVBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7O0FBRUg7QUFDQSxDQUFDOztBQUVEO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVztBQUNYOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBLE9BQU87QUFDUDtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQztBQUNEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxZQUFZO0FBQ1o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsWUFBWTtBQUNaO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsWUFBWTtBQUNaO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsWUFBWTtBQUNaO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxZQUFZO0FBQ1o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsWUFBWTtBQUNaO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxZQUFZO0FBQ1o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxZQUFZO0FBQ1o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQSxZQUFZO0FBQ1o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsWUFBWTtBQUNaO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsWUFBWTtBQUNaO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxZQUFZO0FBQ1o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFlBQVk7QUFDWjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxZQUFZO0FBQ1o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFlBQVk7QUFDWjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsWUFBWTtBQUNaO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxjQUFjO0FBQ2Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBLFlBQVk7QUFDWjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQSxZQUFZO0FBQ1o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxPQUFPO0FBQ1A7QUFDQSxpQ0FBaUMsZ0NBQW1COztBQUVwRDs7O0FBR0E7QUFDQTtBQUNBLENBQUM7O0FBRUQsb0dBQW9HLG1CQUFtQixFQUFFLG1CQUFtQiw4SEFBOEg7O0FBRTFRLGdDQUFnQywyQ0FBMkMsZ0JBQWdCLGtCQUFrQixPQUFPLDJCQUEyQix3REFBd0QsZ0NBQWdDLHVEQUF1RCwyREFBMkQsRUFBRSxFQUFFLHlEQUF5RCxxRUFBcUUsNkRBQTZELG9CQUFvQixHQUFHLEVBQUU7O0FBRWpqQixrQkFBa0IsZ0NBQW1COztBQUVyQzs7QUFFQSxjQUFjLGdDQUFtQjs7QUFFakM7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGLGlEQUFpRCwwQ0FBMEMsMERBQTBELEVBQUU7O0FBRXZKLGlEQUFpRCxhQUFhLHVGQUF1RixFQUFFLHVGQUF1Rjs7QUFFOU8sMENBQTBDLCtEQUErRCxxR0FBcUcsRUFBRSx5RUFBeUUsZUFBZSx5RUFBeUUsRUFBRSxFQUFFLHVIQUF1SDs7QUFFNWU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsWUFBWTtBQUNaO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFlBQVk7QUFDWjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7OztBQUdBO0FBQ0EsaUJBQWlCO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRzs7QUFFSDtBQUNBOztBQUVBOztBQUVBLCtKQUErSjs7QUFFL0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLGVBQWU7QUFDZjs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsZUFBZSxPQUFPO0FBQ3RCLGVBQWUsRUFBRTtBQUNqQixpQkFBaUI7QUFDakI7O0FBRUEsR0FBRztBQUNIO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxlQUFlLE9BQU87QUFDdEIsZUFBZSxFQUFFO0FBQ2pCLGlCQUFpQjtBQUNqQjs7QUFFQSxHQUFHO0FBQ0g7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHOztBQUVIO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBOztBQUVBLE9BQU87QUFDUDtBQUNBOztBQUVBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBLE9BQU87QUFDUDtBQUNBLGlDQUFpQyxnQ0FBbUI7O0FBRXBEO0FBQ0Esa0JBQWtCLGdDQUFtQjs7QUFFckM7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLE9BQU8sMkJBQTJCO0FBQ2xDLE9BQU8sMkJBQTJCO0FBQ2xDLE9BQU8sMkJBQTJCO0FBQ2xDLE9BQU8sMkJBQTJCO0FBQ2xDLFFBQVEsNEJBQTRCO0FBQ3BDLE9BQU8sMkJBQTJCO0FBQ2xDLE9BQU8sMkJBQTJCO0FBQ2xDLE9BQU8sMkJBQTJCO0FBQ2xDLE9BQU8sNkJBQTZCO0FBQ3BDLFdBQVcsaUNBQWlDO0FBQzVDLFVBQVUsZ0NBQWdDO0FBQzFDLFdBQVcsaUNBQWlDO0FBQzVDLE9BQU8scUNBQXFDO0FBQzVDLFNBQVMsMkNBQTJDO0FBQ3BELFFBQVE7QUFDUjs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFEQUFxRCxnQkFBZ0I7QUFDckUsbURBQW1ELGNBQWM7QUFDakU7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsRUFBRTtBQUNGO0FBQ0EsRUFBRTtBQUNGO0FBQ0EsRUFBRTtBQUNGO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQSxFQUFFO0FBQ0Y7QUFDQSxFQUFFO0FBQ0Y7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxFQUFFO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsRUFBRTtBQUNGO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQSxnQkFBZ0IsT0FBTztBQUN2QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUEsdUJBQXVCOztBQUV2QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxnQkFBZ0IsT0FBTyxRQUFRO0FBQy9CLGdCQUFnQixPQUFPLFFBQVE7QUFDL0IsaUJBQWlCLE9BQU8sT0FBTztBQUMvQixpQkFBaUIsT0FBTyxPQUFPO0FBQy9CLGdCQUFnQixRQUFRLE9BQU87QUFDL0IsZ0JBQWdCLFFBQVEsT0FBTztBQUMvQjs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxzRUFBc0U7O0FBRXRFOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBLHNCQUFzQjtBQUN0QjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSwrQ0FBK0MsRUFBRSxVQUFVLEVBQUU7QUFDN0Q7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsRUFBRTtBQUNGO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEVBQUU7QUFDRjtBQUNBO0FBQ0EsRUFBRTtBQUNGO0FBQ0E7QUFDQSxFQUFFO0FBQ0Y7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsRUFBRTtBQUNGO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLGVBQWUsYUFBYSxhQUFhO0FBQ3pDO0FBQ0EsZUFBZSxhQUFhLGFBQWE7QUFDekM7QUFDQSxlQUFlLGFBQWEsYUFBYTtBQUN6QztBQUNBLGVBQWUsYUFBYSxhQUFhO0FBQ3pDO0FBQ0EsZUFBZSxhQUFhLGFBQWE7QUFDekM7QUFDQSxlQUFlLGFBQWE7QUFDNUI7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxFQUFFO0FBQ0Y7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0EsT0FBTztBQUNQO0FBQ0EsaUNBQWlDLGdDQUFtQjs7QUFFcEQ7OztBQUdBLG9HQUFvRyxtQkFBbUIsRUFBRSxtQkFBbUIsOEhBQThIOztBQUUxUSxtQkFBbUIsZ0NBQW1COztBQUV0Qzs7QUFFQSxjQUFjLGdDQUFtQjs7QUFFakM7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7O0FBRUg7QUFDQTs7QUFFQTs7QUFFQSxPQUFPO0FBQ1A7QUFDQSxpQ0FBaUMsZ0NBQW1COztBQUVwRDs7O0FBR0E7QUFDQTtBQUNBLENBQUM7O0FBRUQsZ0NBQWdDLDJDQUEyQyxnQkFBZ0Isa0JBQWtCLE9BQU8sMkJBQTJCLHdEQUF3RCxnQ0FBZ0MsdURBQXVELDJEQUEyRCxFQUFFLEVBQUUseURBQXlELHFFQUFxRSw2REFBNkQsb0JBQW9CLEdBQUcsRUFBRTs7QUFFampCLGlCQUFpQixnQ0FBbUI7O0FBRXBDOztBQUVBLGVBQWUsZ0NBQW1COztBQUVsQzs7QUFFQSxrQkFBa0IsZ0NBQW1COztBQUVyQzs7QUFFQSxjQUFjLGdDQUFtQjs7QUFFakM7O0FBRUEscUJBQXFCLGdDQUFtQjs7QUFFeEM7O0FBRUEsb0JBQW9CLGdDQUFtQjs7QUFFdkM7O0FBRUEsb0JBQW9CLGdDQUFtQjs7QUFFdkM7O0FBRUEsb0JBQW9CLGdDQUFtQjs7QUFFdkM7O0FBRUEscUJBQXFCLGdDQUFtQjs7QUFFeEM7O0FBRUEsb0JBQW9CLGdDQUFtQjs7QUFFdkM7O0FBRUEsaUJBQWlCLGdDQUFtQjs7QUFFcEM7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGLGlEQUFpRCwwQ0FBMEMsMERBQTBELEVBQUU7O0FBRXZKOztBQUVBLDBEQUEwRDs7QUFFMUQ7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0EsY0FBYztBQUNkO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGNBQWM7QUFDZDs7QUFFQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCOztBQUVBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsZUFBZSxjQUFjO0FBQzdCLGVBQWUsT0FBTztBQUN0QjtBQUNBOztBQUVBLEdBQUc7QUFDSDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGNBQWM7QUFDZDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGNBQWM7QUFDZDs7QUFFQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHOztBQUVIO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsY0FBYztBQUNkO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsZUFBZTtBQUNmO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsY0FBYztBQUNkO0FBQ0E7O0FBRUE7QUFDQSxjQUFjO0FBQ2Q7QUFDQSxtREFBbUQ7O0FBRW5EO0FBQ0EsY0FBYztBQUNkO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxjQUFjO0FBQ2Q7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsY0FBYztBQUNkO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQSxjQUFjO0FBQ2Q7QUFDQTtBQUNBO0FBQ0EsY0FBYztBQUNkO0FBQ0E7QUFDQTtBQUNBLGNBQWM7QUFDZDtBQUNBO0FBQ0E7QUFDQSxjQUFjO0FBQ2Q7QUFDQTtBQUNBO0FBQ0EsY0FBYztBQUNkO0FBQ0E7QUFDQTtBQUNBLGNBQWM7QUFDZDtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0Esc0NBQXNDLG1CQUFtQjtBQUN6RDs7QUFFQTtBQUNBO0FBQ0EsaUdBQWlHO0FBQ2pHLE9BQU87QUFDUDs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxlQUFlLFVBQVU7QUFDekIsZUFBZSxPQUFPO0FBQ3RCLGlCQUFpQjtBQUNqQjs7QUFFQSxHQUFHO0FBQ0g7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlLE1BQU07QUFDckI7O0FBRUEsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZSxNQUFNO0FBQ3JCOztBQUVBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZSxNQUFNO0FBQ3JCOztBQUVBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxlQUFlLFNBQVM7QUFDeEIsaUJBQWlCO0FBQ2pCOztBQUVBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlLGFBQWE7QUFDNUI7O0FBRUEsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjs7QUFFQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCOztBQUVBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjs7QUFFQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjs7QUFFQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQSxHQUFHOztBQUVIO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7QUFDQTtBQUNBLFVBQVUsT0FBTztBQUNqQjs7O0FBR0E7O0FBRUE7QUFDQTs7QUFFQSxPQUFPO0FBQ1A7QUFDQSxpQ0FBaUMsZ0NBQW1COztBQUVwRDs7O0FBR0E7QUFDQTtBQUNBLENBQUM7QUFDRDs7QUFFQSxnQkFBZ0IsZ0NBQW1COztBQUVuQzs7QUFFQSxlQUFlLGdDQUFtQjs7QUFFbEM7O0FBRUEsZ0JBQWdCLGdDQUFtQjs7QUFFbkM7O0FBRUEsZUFBZSxnQ0FBbUI7O0FBRWxDOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxPQUFPO0FBQ1A7QUFDQSxpQ0FBaUMsZ0NBQW1COztBQUVwRDs7O0FBR0E7QUFDQTtBQUNBLENBQUM7O0FBRUQsZ0NBQWdDLDJDQUEyQyxnQkFBZ0Isa0JBQWtCLE9BQU8sMkJBQTJCLHdEQUF3RCxnQ0FBZ0MsdURBQXVELDJEQUEyRCxFQUFFLEVBQUUseURBQXlELHFFQUFxRSw2REFBNkQsb0JBQW9CLEdBQUcsRUFBRTs7QUFFampCLHFEQUFxRCxrREFBa0QsOERBQThELDBCQUEwQiw0Q0FBNEMsdUJBQXVCLGtCQUFrQixFQUFFLE9BQU8sd0NBQXdDLEVBQUUsRUFBRSw0QkFBNEIsbUJBQW1CLEVBQUUsT0FBTyx1QkFBdUIsNEJBQTRCLGtCQUFrQixFQUFFLDhCQUE4QixFQUFFOztBQUV4ZSxrQkFBa0IsZ0NBQW1COztBQUVyQzs7QUFFQSxjQUFjLGdDQUFtQjs7QUFFakM7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGLGlEQUFpRCwwQ0FBMEMsMERBQTBELEVBQUU7O0FBRXZKLGlEQUFpRCxhQUFhLHVGQUF1RixFQUFFLHVGQUF1Rjs7QUFFOU8sMENBQTBDLCtEQUErRCxxR0FBcUcsRUFBRSx5RUFBeUUsZUFBZSx5RUFBeUUsRUFBRSxFQUFFLHVIQUF1SDs7QUFFNWU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQTtBQUNBLGNBQWM7QUFDZDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsYUFBYSxPQUFPO0FBQ3BCLGFBQWEsRUFBRTtBQUNmOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSx3RkFBd0YsYUFBYTtBQUNyRztBQUNBOztBQUVBOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZ0JBQWdCLE9BQU87QUFDdkIsb0JBQW9CLFlBQVk7QUFDaEMsb0JBQW9CLFVBQVU7QUFDOUIscUJBQXFCLG9GQUFvRjtBQUN6RztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLGVBQWUsTUFBTTtBQUNyQjs7QUFFQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7O0FBRUg7QUFDQSxDQUFDOztBQUVEO0FBQ0E7O0FBRUEsT0FBTztBQUNQO0FBQ0EsaUNBQWlDLGdDQUFtQjs7QUFFcEQ7OztBQUdBO0FBQ0E7QUFDQSxDQUFDOztBQUVELGdDQUFnQywyQ0FBMkMsZ0JBQWdCLGtCQUFrQixPQUFPLDJCQUEyQix3REFBd0QsZ0NBQWdDLHVEQUF1RCwyREFBMkQsRUFBRSxFQUFFLHlEQUF5RCxxRUFBcUUsNkRBQTZELG9CQUFvQixHQUFHLEVBQUU7O0FBRWpqQixxREFBcUQsa0RBQWtELDhEQUE4RCwwQkFBMEIsNENBQTRDLHVCQUF1QixrQkFBa0IsRUFBRSxPQUFPLHdDQUF3QyxFQUFFLEVBQUUsNEJBQTRCLG1CQUFtQixFQUFFLE9BQU8sdUJBQXVCLDRCQUE0QixrQkFBa0IsRUFBRSw4QkFBOEIsRUFBRTs7QUFFeGUsa0JBQWtCLGdDQUFtQjs7QUFFckM7O0FBRUEsY0FBYyxnQ0FBbUI7O0FBRWpDOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixpREFBaUQsMENBQTBDLDBEQUEwRCxFQUFFOztBQUV2SixpREFBaUQsYUFBYSx1RkFBdUYsRUFBRSx1RkFBdUY7O0FBRTlPLDBDQUEwQywrREFBK0QscUdBQXFHLEVBQUUseUVBQXlFLGVBQWUseUVBQXlFLEVBQUUsRUFBRSx1SEFBdUg7O0FBRTVlO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBLCtKQUErSjtBQUMvSjtBQUNBO0FBQ0E7QUFDQSxLQUFLOztBQUVMO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRzs7QUFFSDtBQUNBLENBQUM7O0FBRUQ7QUFDQTs7QUFFQSxPQUFPO0FBQ1A7QUFDQSxpQ0FBaUMsZ0NBQW1COztBQUVwRDs7O0FBR0E7QUFDQTtBQUNBLENBQUM7O0FBRUQsZ0NBQWdDLDJDQUEyQyxnQkFBZ0Isa0JBQWtCLE9BQU8sMkJBQTJCLHdEQUF3RCxnQ0FBZ0MsdURBQXVELDJEQUEyRCxFQUFFLEVBQUUseURBQXlELHFFQUFxRSw2REFBNkQsb0JBQW9CLEdBQUcsRUFBRTs7QUFFampCLHFEQUFxRCxrREFBa0QsOERBQThELDBCQUEwQiw0Q0FBNEMsdUJBQXVCLGtCQUFrQixFQUFFLE9BQU8sd0NBQXdDLEVBQUUsRUFBRSw0QkFBNEIsbUJBQW1CLEVBQUUsT0FBTyx1QkFBdUIsNEJBQTRCLGtCQUFrQixFQUFFLDhCQUE4QixFQUFFOztBQUV4ZSxnQkFBZ0IsZ0NBQW1COztBQUVuQzs7QUFFQSxjQUFjLGdDQUFtQjs7QUFFakM7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGLGlEQUFpRCwwQ0FBMEMsMERBQTBELEVBQUU7O0FBRXZKLGlEQUFpRCxhQUFhLHVGQUF1RixFQUFFLHVGQUF1Rjs7QUFFOU8sMENBQTBDLCtEQUErRCxxR0FBcUcsRUFBRSx5RUFBeUUsZUFBZSx5RUFBeUUsRUFBRSxFQUFFLHVIQUF1SDs7QUFFNWU7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBLGlLQUFpSzs7QUFFaks7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBLFNBQVM7O0FBRVQ7O0FBRUE7QUFDQSxPQUFPOztBQUVQO0FBQ0E7QUFDQSxHQUFHOztBQUVIO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBOztBQUVBLE9BQU87QUFDUDtBQUNBLGlDQUFpQyxnQ0FBbUI7O0FBRXBEOzs7QUFHQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRCxnQ0FBZ0MsMkNBQTJDLGdCQUFnQixrQkFBa0IsT0FBTywyQkFBMkIsd0RBQXdELGdDQUFnQyx1REFBdUQsMkRBQTJELEVBQUUsRUFBRSx5REFBeUQscUVBQXFFLDZEQUE2RCxvQkFBb0IsR0FBRyxFQUFFOztBQUVqakIsY0FBYyxnQ0FBbUI7O0FBRWpDOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixpREFBaUQsMENBQTBDLDBEQUEwRCxFQUFFOztBQUV2SjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLFlBQVk7QUFDekI7QUFDQTtBQUNBOztBQUVBO0FBQ0EsY0FBYztBQUNkO0FBQ0E7QUFDQTtBQUNBLGNBQWM7QUFDZDtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWU7QUFDZjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxjQUFjO0FBQ2Q7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYSxJQUFJO0FBQ2pCLGFBQWEsSUFBSTtBQUNqQjs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUEsR0FBRztBQUNIO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZSxNQUFNO0FBQ3JCOztBQUVBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQSx5REFBeUQsV0FBVyxtQkFBbUI7QUFDdkY7QUFDQSxTQUFTO0FBQ1QseURBQXlELFdBQVcsbUJBQW1CO0FBQ3ZGLCtCQUErQjtBQUMvQjtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZSxNQUFNO0FBQ3JCOztBQUVBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLHlCQUF5Qjs7QUFFekI7O0FBRUE7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWUsTUFBTTtBQUNyQjs7QUFFQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0EsR0FBRzs7QUFFSDtBQUNBLENBQUM7O0FBRUQ7QUFDQTs7QUFFQSxPQUFPO0FBQ1A7QUFDQSxpQ0FBaUMsaUNBQW1COztBQUVwRDs7O0FBR0E7QUFDQTtBQUNBLENBQUM7O0FBRUQsZ0NBQWdDLDJDQUEyQyxnQkFBZ0Isa0JBQWtCLE9BQU8sMkJBQTJCLHdEQUF3RCxnQ0FBZ0MsdURBQXVELDJEQUEyRCxFQUFFLEVBQUUseURBQXlELHFFQUFxRSw2REFBNkQsb0JBQW9CLEdBQUcsRUFBRTs7QUFFampCLGNBQWMsaUNBQW1COztBQUVqQzs7QUFFQSxlQUFlLGlDQUFtQjs7QUFFbEM7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGLGlEQUFpRCwwQ0FBMEMsMERBQTBELEVBQUU7O0FBRXZKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWEsWUFBWTtBQUN6QixhQUFhLE9BQU87QUFDcEI7QUFDQTtBQUNBOztBQUVBO0FBQ0EsY0FBYztBQUNkO0FBQ0E7QUFDQTtBQUNBLGNBQWM7QUFDZDtBQUNBO0FBQ0E7QUFDQSxjQUFjO0FBQ2Q7QUFDQTtBQUNBO0FBQ0EsY0FBYztBQUNkO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLGNBQWM7QUFDZDtBQUNBO0FBQ0E7QUFDQSxjQUFjO0FBQ2Q7QUFDQTtBQUNBO0FBQ0EsY0FBYztBQUNkO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsZUFBZTtBQUNmOzs7QUFHQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLGVBQWU7QUFDZjs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLFNBQVM7O0FBRVQ7QUFDQTtBQUNBLFNBQVM7O0FBRVQ7QUFDQTtBQUNBLFNBQVM7QUFDVDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUzs7QUFFVDtBQUNBO0FBQ0EsU0FBUztBQUNUOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUEsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0EsU0FBUztBQUNUOztBQUVBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBOztBQUVBOztBQUVBOztBQUVBLGlFQUFpRSxrREFBa0Qsb0RBQW9EOztBQUV2SztBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxlQUFlLE1BQU07QUFDckI7QUFDQTs7QUFFQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlLE1BQU07QUFDckI7O0FBRUEsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWUsTUFBTTtBQUNyQjs7QUFFQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZSxNQUFNO0FBQ3JCOztBQUVBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCOztBQUVBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjs7QUFFQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjs7QUFFQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjs7QUFFQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjs7QUFFQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjs7QUFFQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHOztBQUVIO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBOztBQUVBLE9BQU87QUFDUDtBQUNBLGlDQUFpQyxpQ0FBbUI7O0FBRXBEOzs7QUFHQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRCxnQ0FBZ0MsMkNBQTJDLGdCQUFnQixrQkFBa0IsT0FBTywyQkFBMkIsd0RBQXdELGdDQUFnQyx1REFBdUQsMkRBQTJELEVBQUUsRUFBRSx5REFBeUQscUVBQXFFLDZEQUE2RCxvQkFBb0IsR0FBRyxFQUFFOztBQUVqakIsY0FBYyxpQ0FBbUI7O0FBRWpDOztBQUVBLGlCQUFpQixpQ0FBbUI7O0FBRXBDOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixpREFBaUQsMENBQTBDLDBEQUEwRCxFQUFFOztBQUV2SjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLFlBQVk7QUFDekI7QUFDQTtBQUNBOztBQUVBO0FBQ0EsY0FBYztBQUNkO0FBQ0E7QUFDQTtBQUNBLGNBQWM7QUFDZDtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87O0FBRVA7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7O0FBRUEsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWUsT0FBTztBQUN0QjtBQUNBO0FBQ0E7O0FBRUEsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlLFlBQVk7QUFDM0I7QUFDQSxpQkFBaUI7QUFDakI7O0FBRUEsR0FBRztBQUNIO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjs7QUFFQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjs7QUFFQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjs7QUFFQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7O0FBRUEsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjs7QUFFQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWUsTUFBTTtBQUNyQixpQkFBaUI7QUFDakI7O0FBRUEsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZSxNQUFNO0FBQ3JCLGlCQUFpQjtBQUNqQjs7QUFFQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHOztBQUVIO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBOztBQUVBLE9BQU87QUFDUDtBQUNBLGlDQUFpQyxpQ0FBbUI7O0FBRXBEOzs7QUFHQSxrQkFBa0IsaUNBQW1CO0FBQ3JDLGNBQWMsaUNBQW1COztBQUVqQzs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUEsbUJBQW1CO0FBQ25CO0FBQ0E7QUFDQTtBQUNBLEVBQUU7QUFDRjtBQUNBO0FBQ0E7QUFDQSxFQUFFO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxFQUFFO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEVBQUU7QUFDRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxFQUFFO0FBQ0Y7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0EsYUFBYSxtQkFBbUI7QUFDaEM7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGFBQWEsY0FBYztBQUMzQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsRUFBRTs7QUFFRjtBQUNBO0FBQ0EsRUFBRTs7QUFFRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsRUFBRTs7QUFFRjtBQUNBO0FBQ0E7QUFDQTtBQUNBLEVBQUU7O0FBRUY7QUFDQTtBQUNBLEVBQUU7O0FBRUY7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsaUJBQWlCLGNBQWM7QUFDL0I7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxFQUFFOztBQUVGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0EsRUFBRTs7QUFFRjtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLEVBQUU7O0FBRUY7QUFDQTtBQUNBO0FBQ0EsRUFBRTs7QUFFRjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLEVBQUU7O0FBRUY7QUFDQTtBQUNBO0FBQ0E7O0FBRUEscUVBQXFFLGtDQUFrQyxFQUFFOztBQUV6RztBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxFQUFFOztBQUVGO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0EsRUFBRTs7QUFFRjtBQUNBO0FBQ0E7QUFDQSxFQUFFOztBQUVGO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLGlCQUFpQixnQkFBZ0I7QUFDakM7QUFDQTtBQUNBOztBQUVBO0FBQ0EsRUFBRTs7QUFFRjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxFQUFFOztBQUVGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxFQUFFOztBQUVGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxFQUFFOztBQUVGO0FBQ0E7QUFDQSxFQUFFOztBQUVGO0FBQ0E7QUFDQSxpQkFBaUIsT0FBTztBQUN4QjtBQUNBO0FBQ0E7QUFDQSxFQUFFOztBQUVGO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsRUFBRTs7QUFFRjtBQUNBO0FBQ0E7QUFDQTtBQUNBLEVBQUU7O0FBRUY7QUFDQTtBQUNBO0FBQ0E7QUFDQSxFQUFFOztBQUVGO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsRUFBRTs7QUFFRjtBQUNBO0FBQ0E7QUFDQTtBQUNBLEVBQUU7O0FBRUY7QUFDQTtBQUNBO0FBQ0E7QUFDQSxFQUFFOztBQUVGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxFQUFFOztBQUVGO0FBQ0E7QUFDQSxFQUFFOztBQUVGO0FBQ0E7QUFDQSxFQUFFOztBQUVGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsRUFBRTs7QUFFRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxFQUFFOztBQUVGOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0EsZ0JBQWdCLFlBQVk7QUFDNUI7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTs7O0FBR0EsT0FBTztBQUNQO0FBQ0EsaUNBQWlDLGlDQUFtQjs7QUFFcEQ7QUFDQSxpQkFBaUIsaUNBQW1CO0FBQ3BDLGNBQWMsaUNBQW1COztBQUVqQzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxPQUFPO0FBQ1A7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBLFNBQVM7QUFDVDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSx5QkFBeUIsSUFBSTtBQUM3Qix3QkFBd0IsRUFBRSxXQUFXLEVBQUU7QUFDdkM7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQSxhQUFhLE9BQU87QUFDcEI7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsRUFBRTtBQUNGO0FBQ0E7O0FBRUEsYUFBYSxPQUFPO0FBQ3BCO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsRUFBRTtBQUNGLGFBQWEsT0FBTztBQUNwQjtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEVBQUU7QUFDRixhQUFhLE9BQU87QUFDcEI7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxFQUFFO0FBQ0Y7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBLEVBQUU7QUFDRjtBQUNBOztBQUVBLFlBQVksT0FBTztBQUNuQjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0EsT0FBTztBQUNQO0FBQ0EsaUNBQWlDLGlDQUFtQjs7QUFFcEQ7OztBQUdBLGlCQUFpQixpQ0FBbUI7O0FBRXBDO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSxtQ0FBbUMsU0FBUztBQUM1Qzs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0EsT0FBTztBQUNQO0FBQ0E7O0FBRUE7OztBQUdBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7O0FBR0EsT0FBTztBQUNQO0FBQ0EsaUNBQWlDLGlDQUFtQjs7QUFFcEQsa0JBQWtCLGlDQUFtQjtBQUNyQyxZQUFZLGlDQUFtQjs7QUFFL0I7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0Esa0NBQWtDO0FBQ2xDO0FBQ0E7QUFDQSx1Q0FBdUMsU0FBUztBQUNoRDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUEsd0RBQXdELHVDQUF1QztBQUMvRixzREFBc0QscUNBQXFDOztBQUUzRjtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEVBQUU7QUFDRixDQUFDOztBQUVEOzs7QUFHQSxPQUFPO0FBQ1A7QUFDQSxpQ0FBaUMsaUNBQW1COztBQUVwRCxrQkFBa0IsaUNBQW1COztBQUVyQztBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUEscUNBQXFDLFNBQVM7QUFDOUM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSx5QkFBeUI7O0FBRXpCOztBQUVBO0FBQ0E7QUFDQTs7QUFFQSx5Q0FBeUMsU0FBUztBQUNsRDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxxQ0FBcUMsU0FBUztBQUM5QztBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7OztBQUlBLE9BQU87QUFDUDtBQUNBLGlDQUFpQyxpQ0FBbUI7O0FBRXBEOzs7QUFHQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRCxnQ0FBZ0MsMkNBQTJDLGdCQUFnQixrQkFBa0IsT0FBTywyQkFBMkIsd0RBQXdELGdDQUFnQyx1REFBdUQsMkRBQTJELEVBQUUsRUFBRSx5REFBeUQscUVBQXFFLDZEQUE2RCxvQkFBb0IsR0FBRyxFQUFFOztBQUVqakIsY0FBYyxpQ0FBbUI7O0FBRWpDOztBQUVBLGlCQUFpQixpQ0FBbUI7O0FBRXBDOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixpREFBaUQsMENBQTBDLDBEQUEwRCxFQUFFOztBQUV2SjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLFlBQVk7QUFDekI7QUFDQTtBQUNBOztBQUVBO0FBQ0EsY0FBYztBQUNkO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLGVBQWU7QUFDZjs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCOztBQUVBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsZUFBZSxpQkFBaUI7QUFDaEM7O0FBRUEsR0FBRztBQUNIO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZSxFQUFFO0FBQ2pCLGVBQWUsUUFBUTtBQUN2QixpQkFBaUI7QUFDakI7O0FBRUEsR0FBRztBQUNIO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBLGlCQUFpQjtBQUNqQjs7QUFFQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGVBQWUsU0FBUztBQUN4QixlQUFlLFFBQVE7QUFDdkIsaUJBQWlCO0FBQ2pCOztBQUVBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTzs7QUFFUDtBQUNBOztBQUVBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7O0FBRUEsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7O0FBRUEsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7O0FBRUEsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0EsaUJBQWlCO0FBQ2pCOztBQUVBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxjQUFjO0FBQ2Q7O0FBRUEsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZSxlQUFlO0FBQzlCO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHOztBQUVIO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBOztBQUVBLE9BQU87QUFDUDtBQUNBLGlDQUFpQyxpQ0FBbUI7O0FBRXBEOzs7QUFHQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRCxnQ0FBZ0MsMkNBQTJDLGdCQUFnQixrQkFBa0IsT0FBTywyQkFBMkIsd0RBQXdELGdDQUFnQyx1REFBdUQsMkRBQTJELEVBQUUsRUFBRSx5REFBeUQscUVBQXFFLDZEQUE2RCxvQkFBb0IsR0FBRyxFQUFFOztBQUVqakIsY0FBYyxpQ0FBbUI7O0FBRWpDOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixpREFBaUQsMENBQTBDLDBEQUEwRCxFQUFFOztBQUV2SjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLFlBQVk7QUFDekI7QUFDQTtBQUNBOztBQUVBO0FBQ0EsY0FBYztBQUNkO0FBQ0E7QUFDQTtBQUNBLGNBQWM7QUFDZDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxnQkFBZ0I7QUFDaEI7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7O0FBRUE7QUFDQSxxSEFBcUg7O0FBRXJIO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBOztBQUVBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHOztBQUVIO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBOztBQUVBLE9BQU87QUFDUDtBQUNBOztBQUVBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRCxnQ0FBZ0MsMkNBQTJDLGdCQUFnQixrQkFBa0IsT0FBTywyQkFBMkIsd0RBQXdELGdDQUFnQyx1REFBdUQsMkRBQTJELEVBQUUsRUFBRSx5REFBeUQscUVBQXFFLDZEQUE2RCxvQkFBb0IsR0FBRyxFQUFFOztBQUVqakIsaURBQWlELDBDQUEwQywwREFBMEQsRUFBRTs7QUFFdko7QUFDQTtBQUNBLGFBQWEsWUFBWTtBQUN6QjtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxjQUFjO0FBQ2Q7QUFDQTtBQUNBO0FBQ0EsY0FBYztBQUNkO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBLGdCQUFnQjtBQUNoQjtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQSxvQkFBb0I7O0FBRXBCOztBQUVBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0EsR0FBRzs7QUFFSDtBQUNBLENBQUM7O0FBRUQ7QUFDQTs7QUFFQSxPQUFPO0FBQ1A7QUFDQSxDQUFDO0FBQ0Q7QUFDQSxDQUFDOzs7Ozs7Ozs7Ozs7QUMvbU1ELGtDOzs7Ozs7VUNBQTtVQUNBOztVQUVBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBOztVQUVBO1VBQ0E7O1VBRUE7VUFDQTtVQUNBOzs7Ozs7Ozs7Ozs7QUNHQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7Ozs7O0FBNUJBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O2NBOEJZaEUsTTtJQUFMQyxDLFdBQUFBLEM7OztBQUVQQSxFQUFFLFlBQU07QUFDTjtBQUNBLE1BQUlrQywyQkFBSjtBQUNBLE1BQUl6Qiw0QkFBSjs7QUFFQSxNQUFJdUQsd0JBQXdCLElBQTVCO0FBQ0EsV0FBU0MsYUFBVCxDQUF1QkMsSUFBdkIsRUFBNkI7QUFDM0IsUUFBSUYseUJBQXlCLElBQXpCLElBQWlDLENBQUNBLHNCQUFzQkcsTUFBNUQsRUFBb0U7QUFDbEVILDRCQUFzQkksS0FBdEI7QUFDRDtBQUNESiw0QkFBd0JqRSxPQUFPc0UsSUFBUCxDQUN0QkgsSUFEc0IsRUFFdEIsYUFGc0IsRUFHdEIsZUFDRSxhQURGLEdBRUUsZ0JBRkYsR0FHRSxZQUhGLEdBSUUsWUFKRixHQUtFLGlCQUxGLEdBTUUsZ0JBTkYsR0FPRSxZQVBGLEdBUUUsYUFSRixHQVNFLFNBVEYsR0FVRSxVQWJvQixDQUF4QjtBQWVBRiwwQkFBc0JNLEtBQXRCO0FBQ0Q7O0FBRUR0RSxJQUFFVSxRQUFGLEVBQVk2RCxLQUFaLENBQWtCLFlBQU07QUFDdEIsUUFBSSxDQUFDdkUsRUFBRXdFLGtCQUFRWCxpQkFBVixFQUE2QlksRUFBN0IsQ0FBZ0MsVUFBaEMsQ0FBTCxFQUFrRDtBQUNoRHpFLFFBQUV3RSxrQkFBUVYsb0JBQVYsRUFBZ0NsQyxJQUFoQztBQUNEO0FBQ0Q1QixNQUFFVSxRQUFGLEVBQVlDLEVBQVosQ0FBZSxRQUFmLEVBQXlCNkQsa0JBQVFYLGlCQUFqQyxFQUFvRCxZQUFNO0FBQ3hEN0QsUUFBRXdFLGtCQUFRVixvQkFBVixFQUFnQ1ksV0FBaEM7QUFDRCxLQUZEOztBQUlBMUUsTUFBRVUsUUFBRixFQUFZQyxFQUFaLENBQWUsT0FBZixFQUF3QjZELGtCQUFRVCxtQkFBaEMsRUFBcUQsVUFBQ25ELEtBQUQsRUFBVztBQUM5RCxVQUFNRyxXQUFXZixFQUFFWSxNQUFNSSxhQUFSLENBQWpCO0FBQ0EsVUFBTWUsVUFBVWhCLFNBQVNHLE9BQVQsQ0FBaUIsYUFBakIsRUFBZ0NFLElBQWhDLENBQXFDLG9DQUFyQyxDQUFoQjtBQUNBLFVBQU04QyxPQUFPbkMsUUFBUVgsSUFBUixDQUFhLGlCQUFiLEVBQWdDYSxJQUFoQyxDQUFxQyxjQUFyQyxDQUFiOztBQUVBZ0Msb0JBQWNDLElBQWQ7QUFDRCxLQU5EO0FBT0QsR0FmRDtBQWdCRCxDQTVDRCxFIiwiZmlsZSI6Im9yZGVyX3N0YXRlc19mb3JtLmJ1bmRsZS5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQ29weXJpZ2h0IHNpbmNlIDIwMDcgUHJlc3RhU2hvcCBTQSBhbmQgQ29udHJpYnV0b3JzXG4gKiBQcmVzdGFTaG9wIGlzIGFuIEludGVybmF0aW9uYWwgUmVnaXN0ZXJlZCBUcmFkZW1hcmsgJiBQcm9wZXJ0eSBvZiBQcmVzdGFTaG9wIFNBXG4gKlxuICogTk9USUNFIE9GIExJQ0VOU0VcbiAqXG4gKiBUaGlzIHNvdXJjZSBmaWxlIGlzIHN1YmplY3QgdG8gdGhlIE9wZW4gU29mdHdhcmUgTGljZW5zZSAoT1NMIDMuMClcbiAqIHRoYXQgaXMgYnVuZGxlZCB3aXRoIHRoaXMgcGFja2FnZSBpbiB0aGUgZmlsZSBMSUNFTlNFLm1kLlxuICogSXQgaXMgYWxzbyBhdmFpbGFibGUgdGhyb3VnaCB0aGUgd29ybGQtd2lkZS13ZWIgYXQgdGhpcyBVUkw6XG4gKiBodHRwczovL29wZW5zb3VyY2Uub3JnL2xpY2Vuc2VzL09TTC0zLjBcbiAqIElmIHlvdSBkaWQgbm90IHJlY2VpdmUgYSBjb3B5IG9mIHRoZSBsaWNlbnNlIGFuZCBhcmUgdW5hYmxlIHRvXG4gKiBvYnRhaW4gaXQgdGhyb3VnaCB0aGUgd29ybGQtd2lkZS13ZWIsIHBsZWFzZSBzZW5kIGFuIGVtYWlsXG4gKiB0byBsaWNlbnNlQHByZXN0YXNob3AuY29tIHNvIHdlIGNhbiBzZW5kIHlvdSBhIGNvcHkgaW1tZWRpYXRlbHkuXG4gKlxuICogRElTQ0xBSU1FUlxuICpcbiAqIERvIG5vdCBlZGl0IG9yIGFkZCB0byB0aGlzIGZpbGUgaWYgeW91IHdpc2ggdG8gdXBncmFkZSBQcmVzdGFTaG9wIHRvIG5ld2VyXG4gKiB2ZXJzaW9ucyBpbiB0aGUgZnV0dXJlLiBJZiB5b3Ugd2lzaCB0byBjdXN0b21pemUgUHJlc3RhU2hvcCBmb3IgeW91clxuICogbmVlZHMgcGxlYXNlIHJlZmVyIHRvIGh0dHBzOi8vZGV2ZG9jcy5wcmVzdGFzaG9wLmNvbS8gZm9yIG1vcmUgaW5mb3JtYXRpb24uXG4gKlxuICogQGF1dGhvciAgICBQcmVzdGFTaG9wIFNBIGFuZCBDb250cmlidXRvcnMgPGNvbnRhY3RAcHJlc3Rhc2hvcC5jb20+XG4gKiBAY29weXJpZ2h0IFNpbmNlIDIwMDcgUHJlc3RhU2hvcCBTQSBhbmQgQ29udHJpYnV0b3JzXG4gKiBAbGljZW5zZSAgIGh0dHBzOi8vb3BlbnNvdXJjZS5vcmcvbGljZW5zZXMvT1NMLTMuMCBPcGVuIFNvZnR3YXJlIExpY2Vuc2UgKE9TTCAzLjApXG4gKi9cbmltcG9ydCAnYm9vdHN0cmFwLWNvbG9ycGlja2VyJztcblxuY29uc3QgeyR9ID0gd2luZG93O1xuXG4vKipcbiAqIEVuYWJsZSBhbGwgY29sb3JwaWNrZXJzLlxuICovXG5jb25zdCBpbml0ID0gZnVuY3Rpb24gaW5pdERhdGVQaWNrZXJzKCkge1xuICAkKCcuY29sb3JwaWNrZXIgaW5wdXRbdHlwZT1cInRleHRcIl0nKS5lYWNoKChpLCBwaWNrZXIpID0+IHtcbiAgICAkKHBpY2tlcikuY29sb3JwaWNrZXIoKTtcbiAgfSk7XG59O1xuXG5leHBvcnQgZGVmYXVsdCBpbml0O1xuIiwiLyoqXG4gKiBDb3B5cmlnaHQgc2luY2UgMjAwNyBQcmVzdGFTaG9wIFNBIGFuZCBDb250cmlidXRvcnNcbiAqIFByZXN0YVNob3AgaXMgYW4gSW50ZXJuYXRpb25hbCBSZWdpc3RlcmVkIFRyYWRlbWFyayAmIFByb3BlcnR5IG9mIFByZXN0YVNob3AgU0FcbiAqXG4gKiBOT1RJQ0UgT0YgTElDRU5TRVxuICpcbiAqIFRoaXMgc291cmNlIGZpbGUgaXMgc3ViamVjdCB0byB0aGUgT3BlbiBTb2Z0d2FyZSBMaWNlbnNlIChPU0wgMy4wKVxuICogdGhhdCBpcyBidW5kbGVkIHdpdGggdGhpcyBwYWNrYWdlIGluIHRoZSBmaWxlIExJQ0VOU0UubWQuXG4gKiBJdCBpcyBhbHNvIGF2YWlsYWJsZSB0aHJvdWdoIHRoZSB3b3JsZC13aWRlLXdlYiBhdCB0aGlzIFVSTDpcbiAqIGh0dHBzOi8vb3BlbnNvdXJjZS5vcmcvbGljZW5zZXMvT1NMLTMuMFxuICogSWYgeW91IGRpZCBub3QgcmVjZWl2ZSBhIGNvcHkgb2YgdGhlIGxpY2Vuc2UgYW5kIGFyZSB1bmFibGUgdG9cbiAqIG9idGFpbiBpdCB0aHJvdWdoIHRoZSB3b3JsZC13aWRlLXdlYiwgcGxlYXNlIHNlbmQgYW4gZW1haWxcbiAqIHRvIGxpY2Vuc2VAcHJlc3Rhc2hvcC5jb20gc28gd2UgY2FuIHNlbmQgeW91IGEgY29weSBpbW1lZGlhdGVseS5cbiAqXG4gKiBESVNDTEFJTUVSXG4gKlxuICogRG8gbm90IGVkaXQgb3IgYWRkIHRvIHRoaXMgZmlsZSBpZiB5b3Ugd2lzaCB0byB1cGdyYWRlIFByZXN0YVNob3AgdG8gbmV3ZXJcbiAqIHZlcnNpb25zIGluIHRoZSBmdXR1cmUuIElmIHlvdSB3aXNoIHRvIGN1c3RvbWl6ZSBQcmVzdGFTaG9wIGZvciB5b3VyXG4gKiBuZWVkcyBwbGVhc2UgcmVmZXIgdG8gaHR0cHM6Ly9kZXZkb2NzLnByZXN0YXNob3AuY29tLyBmb3IgbW9yZSBpbmZvcm1hdGlvbi5cbiAqXG4gKiBAYXV0aG9yICAgIFByZXN0YVNob3AgU0EgYW5kIENvbnRyaWJ1dG9ycyA8Y29udGFjdEBwcmVzdGFzaG9wLmNvbT5cbiAqIEBjb3B5cmlnaHQgU2luY2UgMjAwNyBQcmVzdGFTaG9wIFNBIGFuZCBDb250cmlidXRvcnNcbiAqIEBsaWNlbnNlICAgaHR0cHM6Ly9vcGVuc291cmNlLm9yZy9saWNlbnNlcy9PU0wtMy4wIE9wZW4gU29mdHdhcmUgTGljZW5zZSAoT1NMIDMuMClcbiAqL1xuXG5pbXBvcnQgRXZlbnRFbWl0dGVyQ2xhc3MgZnJvbSAnZXZlbnRzJztcblxuLyoqXG4gKiBXZSBpbnN0YW5jaWF0ZSBvbmUgRXZlbnRFbWl0dGVyIChyZXN0cmljdGVkIHZpYSBhIGNvbnN0KSBzbyB0aGF0IGV2ZXJ5IGNvbXBvbmVudHNcbiAqIHJlZ2lzdGVyL2Rpc3BhdGNoIG9uIHRoZSBzYW1lIG9uZSBhbmQgY2FuIGNvbW11bmljYXRlIHdpdGggZWFjaCBvdGhlci5cbiAqL1xuZXhwb3J0IGNvbnN0IEV2ZW50RW1pdHRlciA9IG5ldyBFdmVudEVtaXR0ZXJDbGFzcygpO1xuXG5leHBvcnQgZGVmYXVsdCBFdmVudEVtaXR0ZXI7XG4iLCIvKipcbiAqIENvcHlyaWdodCBzaW5jZSAyMDA3IFByZXN0YVNob3AgU0EgYW5kIENvbnRyaWJ1dG9yc1xuICogUHJlc3RhU2hvcCBpcyBhbiBJbnRlcm5hdGlvbmFsIFJlZ2lzdGVyZWQgVHJhZGVtYXJrICYgUHJvcGVydHkgb2YgUHJlc3RhU2hvcCBTQVxuICpcbiAqIE5PVElDRSBPRiBMSUNFTlNFXG4gKlxuICogVGhpcyBzb3VyY2UgZmlsZSBpcyBzdWJqZWN0IHRvIHRoZSBPcGVuIFNvZnR3YXJlIExpY2Vuc2UgKE9TTCAzLjApXG4gKiB0aGF0IGlzIGJ1bmRsZWQgd2l0aCB0aGlzIHBhY2thZ2UgaW4gdGhlIGZpbGUgTElDRU5TRS5tZC5cbiAqIEl0IGlzIGFsc28gYXZhaWxhYmxlIHRocm91Z2ggdGhlIHdvcmxkLXdpZGUtd2ViIGF0IHRoaXMgVVJMOlxuICogaHR0cHM6Ly9vcGVuc291cmNlLm9yZy9saWNlbnNlcy9PU0wtMy4wXG4gKiBJZiB5b3UgZGlkIG5vdCByZWNlaXZlIGEgY29weSBvZiB0aGUgbGljZW5zZSBhbmQgYXJlIHVuYWJsZSB0b1xuICogb2J0YWluIGl0IHRocm91Z2ggdGhlIHdvcmxkLXdpZGUtd2ViLCBwbGVhc2Ugc2VuZCBhbiBlbWFpbFxuICogdG8gbGljZW5zZUBwcmVzdGFzaG9wLmNvbSBzbyB3ZSBjYW4gc2VuZCB5b3UgYSBjb3B5IGltbWVkaWF0ZWx5LlxuICpcbiAqIERJU0NMQUlNRVJcbiAqXG4gKiBEbyBub3QgZWRpdCBvciBhZGQgdG8gdGhpcyBmaWxlIGlmIHlvdSB3aXNoIHRvIHVwZ3JhZGUgUHJlc3RhU2hvcCB0byBuZXdlclxuICogdmVyc2lvbnMgaW4gdGhlIGZ1dHVyZS4gSWYgeW91IHdpc2ggdG8gY3VzdG9taXplIFByZXN0YVNob3AgZm9yIHlvdXJcbiAqIG5lZWRzIHBsZWFzZSByZWZlciB0byBodHRwczovL2RldmRvY3MucHJlc3Rhc2hvcC5jb20vIGZvciBtb3JlIGluZm9ybWF0aW9uLlxuICpcbiAqIEBhdXRob3IgICAgUHJlc3RhU2hvcCBTQSBhbmQgQ29udHJpYnV0b3JzIDxjb250YWN0QHByZXN0YXNob3AuY29tPlxuICogQGNvcHlyaWdodCBTaW5jZSAyMDA3IFByZXN0YVNob3AgU0EgYW5kIENvbnRyaWJ1dG9yc1xuICogQGxpY2Vuc2UgICBodHRwczovL29wZW5zb3VyY2Uub3JnL2xpY2Vuc2VzL09TTC0zLjAgT3BlbiBTb2Z0d2FyZSBMaWNlbnNlIChPU0wgMy4wKVxuICovXG5cbmNvbnN0IHskfSA9IHdpbmRvdztcblxuLyoqXG4gKiBDb21wb25lbnQgcmVzcG9uc2libGUgZm9yIGZpbHRlcmluZyBzZWxlY3QgdmFsdWVzIGJ5IGxhbmd1YWdlIHNlbGVjdGVkLlxuICovXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBUcmFuc2xhdGFibGVDaG9pY2Uge1xuICBjb25zdHJ1Y3RvcigpIHtcbiAgICAvLyByZWdpc3RlcnMgdGhlIGV2ZW50IHdoaWNoIGRpc3BsYXlzIHRoZSBwb3BvdmVyXG4gICAgJChkb2N1bWVudCkub24oJ2NoYW5nZScsICdzZWxlY3QudHJhbnNsYXRhYmxlX2Nob2ljZV9sYW5ndWFnZScsIChldmVudCkgPT4ge1xuICAgICAgdGhpcy5maWx0ZXJTZWxlY3QoZXZlbnQpO1xuICAgIH0pO1xuXG4gICAgJCgnc2VsZWN0LnRyYW5zbGF0YWJsZV9jaG9pY2VfbGFuZ3VhZ2UnKS50cmlnZ2VyKCdjaGFuZ2UnKTtcbiAgfVxuXG4gIGZpbHRlclNlbGVjdChldmVudCkge1xuICAgIGNvbnN0ICRlbGVtZW50ID0gJChldmVudC5jdXJyZW50VGFyZ2V0KTtcbiAgICBjb25zdCAkZm9ybUdyb3VwID0gJGVsZW1lbnQuY2xvc2VzdCgnLmZvcm0tZ3JvdXAnKTtcbiAgICBjb25zdCBsYW5ndWFnZSA9ICRlbGVtZW50LmZpbmQoJ29wdGlvbjpzZWxlY3RlZCcpLnZhbCgpO1xuXG4gICAgLy8gc2hvdyBhbGwgdGhlIGxhbmd1YWdlcyBzZWxlY3RzXG4gICAgJGZvcm1Hcm91cC5maW5kKGBzZWxlY3QudHJhbnNsYXRhYmxlX2Nob2ljZVtkYXRhLWxhbmd1YWdlPVwiJHtsYW5ndWFnZX1cIl1gKS5wYXJlbnQoKS5zaG93KCk7XG5cbiAgICBjb25zdCAkc2VsZWN0cyA9ICRmb3JtR3JvdXAuZmluZCgnc2VsZWN0LnRyYW5zbGF0YWJsZV9jaG9pY2UnKTtcblxuICAgIC8vIEhpZGUgYWxsIHRoZSBzZWxlY3RzIG5vdCBjb3JyZXNwb25kaW5nIHRvIHRoZSBsYW5ndWFnZSBzZWxlY3RlZFxuICAgICRzZWxlY3RzLm5vdChgc2VsZWN0LnRyYW5zbGF0YWJsZV9jaG9pY2VbZGF0YS1sYW5ndWFnZT1cIiR7bGFuZ3VhZ2V9XCJdYCkuZWFjaCgoaW5kZXgsIGl0ZW0pID0+IHtcbiAgICAgICQoaXRlbSkucGFyZW50KCkuaGlkZSgpO1xuICAgIH0pO1xuXG4gICAgLy8gQmluZCBjaG9pY2Ugc2VsZWN0aW9uIHRvIGZpbGwgdGhlIGhpZGRlbiBpbnB1dFxuICAgIHRoaXMuYmluZFZhbHVlU2VsZWN0aW9uKCRzZWxlY3RzKTtcbiAgfVxuXG4gIGJpbmRWYWx1ZVNlbGVjdGlvbigkc2VsZWN0cykge1xuICAgICRzZWxlY3RzLmVhY2goKGluZGV4LCBlbGVtZW50KSA9PiB7XG4gICAgICAkKGVsZW1lbnQpLm9uKCdjaGFuZ2UnLCAoZXZlbnQpID0+IHtcbiAgICAgICAgY29uc3QgJHNlbGVjdCA9ICQoZXZlbnQuY3VycmVudFRhcmdldCk7XG4gICAgICAgIGNvbnN0IHNlbGVjdElkID0gJHNlbGVjdC5hdHRyKCdpZCcpO1xuICAgICAgICAkKGAjJHtzZWxlY3RJZH1fdmFsdWVgKS52YWwoJHNlbGVjdC5maW5kKCdvcHRpb246c2VsZWN0ZWQnKS52YWwoKSk7XG4gICAgICB9KTtcbiAgICB9KTtcbiAgfVxufVxuIiwiLyoqXG4gKiBDb3B5cmlnaHQgc2luY2UgMjAwNyBQcmVzdGFTaG9wIFNBIGFuZCBDb250cmlidXRvcnNcbiAqIFByZXN0YVNob3AgaXMgYW4gSW50ZXJuYXRpb25hbCBSZWdpc3RlcmVkIFRyYWRlbWFyayAmIFByb3BlcnR5IG9mIFByZXN0YVNob3AgU0FcbiAqXG4gKiBOT1RJQ0UgT0YgTElDRU5TRVxuICpcbiAqIFRoaXMgc291cmNlIGZpbGUgaXMgc3ViamVjdCB0byB0aGUgT3BlbiBTb2Z0d2FyZSBMaWNlbnNlIChPU0wgMy4wKVxuICogdGhhdCBpcyBidW5kbGVkIHdpdGggdGhpcyBwYWNrYWdlIGluIHRoZSBmaWxlIExJQ0VOU0UubWQuXG4gKiBJdCBpcyBhbHNvIGF2YWlsYWJsZSB0aHJvdWdoIHRoZSB3b3JsZC13aWRlLXdlYiBhdCB0aGlzIFVSTDpcbiAqIGh0dHBzOi8vb3BlbnNvdXJjZS5vcmcvbGljZW5zZXMvT1NMLTMuMFxuICogSWYgeW91IGRpZCBub3QgcmVjZWl2ZSBhIGNvcHkgb2YgdGhlIGxpY2Vuc2UgYW5kIGFyZSB1bmFibGUgdG9cbiAqIG9idGFpbiBpdCB0aHJvdWdoIHRoZSB3b3JsZC13aWRlLXdlYiwgcGxlYXNlIHNlbmQgYW4gZW1haWxcbiAqIHRvIGxpY2Vuc2VAcHJlc3Rhc2hvcC5jb20gc28gd2UgY2FuIHNlbmQgeW91IGEgY29weSBpbW1lZGlhdGVseS5cbiAqXG4gKiBESVNDTEFJTUVSXG4gKlxuICogRG8gbm90IGVkaXQgb3IgYWRkIHRvIHRoaXMgZmlsZSBpZiB5b3Ugd2lzaCB0byB1cGdyYWRlIFByZXN0YVNob3AgdG8gbmV3ZXJcbiAqIHZlcnNpb25zIGluIHRoZSBmdXR1cmUuIElmIHlvdSB3aXNoIHRvIGN1c3RvbWl6ZSBQcmVzdGFTaG9wIGZvciB5b3VyXG4gKiBuZWVkcyBwbGVhc2UgcmVmZXIgdG8gaHR0cHM6Ly9kZXZkb2NzLnByZXN0YXNob3AuY29tLyBmb3IgbW9yZSBpbmZvcm1hdGlvbi5cbiAqXG4gKiBAYXV0aG9yICAgIFByZXN0YVNob3AgU0EgYW5kIENvbnRyaWJ1dG9ycyA8Y29udGFjdEBwcmVzdGFzaG9wLmNvbT5cbiAqIEBjb3B5cmlnaHQgU2luY2UgMjAwNyBQcmVzdGFTaG9wIFNBIGFuZCBDb250cmlidXRvcnNcbiAqIEBsaWNlbnNlICAgaHR0cHM6Ly9vcGVuc291cmNlLm9yZy9saWNlbnNlcy9PU0wtMy4wIE9wZW4gU29mdHdhcmUgTGljZW5zZSAoT1NMIDMuMClcbiAqL1xuXG5pbXBvcnQge0V2ZW50RW1pdHRlcn0gZnJvbSAnLi9ldmVudC1lbWl0dGVyJztcblxuY29uc3QgeyR9ID0gd2luZG93O1xuXG4vKipcbiAqIFRoaXMgY2xhc3MgaXMgdXNlZCB0byBhdXRvbWF0aWNhbGx5IHRvZ2dsZSB0cmFuc2xhdGVkIGlucHV0cyAoZGlzcGxheWVkIHdpdGggb25lXG4gKiBpbnB1dCBhbmQgYSBsYW5ndWFnZSBzZWxlY3RvciB1c2luZyB0aGUgVHJhbnNsYXRhYmxlVHlwZSBTeW1mb255IGZvcm0gdHlwZSkuXG4gKiBBbHNvIGNvbXBhdGlibGUgd2l0aCBUcmFuc2xhdGFibGVGaWVsZCBjaGFuZ2VzLlxuICovXG5jbGFzcyBUcmFuc2xhdGFibGVJbnB1dCB7XG4gIGNvbnN0cnVjdG9yKG9wdGlvbnMpIHtcbiAgICBjb25zdCBvcHRzID0gb3B0aW9ucyB8fCB7fTtcblxuICAgIHRoaXMubG9jYWxlSXRlbVNlbGVjdG9yID0gb3B0cy5sb2NhbGVJdGVtU2VsZWN0b3IgfHwgJy5qcy1sb2NhbGUtaXRlbSc7XG4gICAgdGhpcy5sb2NhbGVCdXR0b25TZWxlY3RvciA9IG9wdHMubG9jYWxlQnV0dG9uU2VsZWN0b3IgfHwgJy5qcy1sb2NhbGUtYnRuJztcbiAgICB0aGlzLmxvY2FsZUlucHV0U2VsZWN0b3IgPSBvcHRzLmxvY2FsZUlucHV0U2VsZWN0b3IgfHwgJy5qcy1sb2NhbGUtaW5wdXQnO1xuICAgIHRoaXMuc2VsZWN0ZWRMb2NhbGUgPSAkKHRoaXMubG9jYWxlSXRlbVNlbGVjdG9yKS5kYXRhKCdsb2NhbGUnKTtcblxuICAgICQoJ2JvZHknKS5vbihcbiAgICAgICdjbGljaycsXG4gICAgICB0aGlzLmxvY2FsZUl0ZW1TZWxlY3RvcixcbiAgICAgIHRoaXMudG9nZ2xlTGFuZ3VhZ2UuYmluZCh0aGlzKSxcbiAgICApO1xuICAgIEV2ZW50RW1pdHRlci5vbignbGFuZ3VhZ2VTZWxlY3RlZCcsIHRoaXMudG9nZ2xlSW5wdXRzLmJpbmQodGhpcykpO1xuXG4gICAgcmV0dXJuIHtcbiAgICAgIGxvY2FsZUl0ZW1TZWxlY3RvcjogdGhpcy5sb2NhbGVJdGVtU2VsZWN0b3IsXG4gICAgICBsb2NhbGVCdXR0b25TZWxlY3RvcjogdGhpcy5sb2NhbGVCdXR0b25TZWxlY3RvcixcbiAgICAgIGxvY2FsZUlucHV0U2VsZWN0b3I6IHRoaXMubG9jYWxlSW5wdXRTZWxlY3RvcixcblxuICAgICAgLyoqXG4gICAgICAgKiBAcGFyYW0ge2pRdWVyeX0gZm9ybVxuICAgICAgICovXG4gICAgICByZWZyZXNoRm9ybUlucHV0czogKGZvcm0pID0+IHsgdGhpcy5yZWZyZXNoSW5wdXRzKGZvcm0pOyB9LFxuXG4gICAgICAvKipcbiAgICAgICAqIEByZXR1cm5zIHtzdHJpbmd8dW5kZWZpbmVkfVxuICAgICAgICovXG4gICAgICBnZXRTZWxlY3RlZExvY2FsZTogKCkgPT4gdGhpcy5zZWxlY3RlZExvY2FsZSxcbiAgICB9O1xuICB9XG5cbiAgLyoqXG4gICAqIEBwYXJhbSB7alF1ZXJ5fSBmb3JtXG4gICAqXG4gICAqIEBwcml2YXRlXG4gICAqL1xuICByZWZyZXNoSW5wdXRzKGZvcm0pIHtcbiAgICBpZiAoIXRoaXMuc2VsZWN0ZWRMb2NhbGUpIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICBFdmVudEVtaXR0ZXIuZW1pdCgnbGFuZ3VhZ2VTZWxlY3RlZCcsIHtcbiAgICAgIHNlbGVjdGVkTG9jYWxlOiB0aGlzLnNlbGVjdGVkTG9jYWxlLFxuICAgICAgZm9ybSxcbiAgICB9KTtcbiAgfVxuXG4gIC8qKlxuICAgKiBEaXNwYXRjaCBldmVudCBvbiBsYW5ndWFnZSBzZWxlY3Rpb24gdG8gdXBkYXRlIGlucHV0cyBhbmQgb3RoZXIgY29tcG9uZW50cyB3aGljaCBkZXBlbmQgb24gdGhlIGxvY2FsZS5cbiAgICpcbiAgICogQHBhcmFtIGV2ZW50XG4gICAqXG4gICAqIEBwcml2YXRlXG4gICAqL1xuICB0b2dnbGVMYW5ndWFnZShldmVudCkge1xuICAgIGNvbnN0IGxvY2FsZUl0ZW0gPSAkKGV2ZW50LnRhcmdldCk7XG4gICAgY29uc3QgZm9ybSA9IGxvY2FsZUl0ZW0uY2xvc2VzdCgnZm9ybScpO1xuICAgIHRoaXMuc2VsZWN0ZWRMb2NhbGUgPSBsb2NhbGVJdGVtLmRhdGEoJ2xvY2FsZScpO1xuICAgIHRoaXMucmVmcmVzaElucHV0cyhmb3JtKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBUb2dnbGUgYWxsIHRyYW5zbGF0YWJsZSBpbnB1dHMgaW4gZm9ybSBpbiB3aGljaCBsb2NhbGUgd2FzIGNoYW5nZWRcbiAgICpcbiAgICogQHBhcmFtIHtFdmVudH0gZXZlbnRcbiAgICpcbiAgICogQHByaXZhdGVcbiAgICovXG4gIHRvZ2dsZUlucHV0cyhldmVudCkge1xuICAgIGNvbnN0IHtmb3JtfSA9IGV2ZW50O1xuICAgIHRoaXMuc2VsZWN0ZWRMb2NhbGUgPSBldmVudC5zZWxlY3RlZExvY2FsZTtcbiAgICBjb25zdCBsb2NhbGVCdXR0b24gPSBmb3JtLmZpbmQodGhpcy5sb2NhbGVCdXR0b25TZWxlY3Rvcik7XG4gICAgY29uc3QgY2hhbmdlTGFuZ3VhZ2VVcmwgPSBsb2NhbGVCdXR0b24uZGF0YSgnY2hhbmdlLWxhbmd1YWdlLXVybCcpO1xuXG4gICAgbG9jYWxlQnV0dG9uLnRleHQodGhpcy5zZWxlY3RlZExvY2FsZSk7XG4gICAgZm9ybS5maW5kKHRoaXMubG9jYWxlSW5wdXRTZWxlY3RvcikuYWRkQ2xhc3MoJ2Qtbm9uZScpO1xuICAgIGZvcm1cbiAgICAgIC5maW5kKGAke3RoaXMubG9jYWxlSW5wdXRTZWxlY3Rvcn0uanMtbG9jYWxlLSR7dGhpcy5zZWxlY3RlZExvY2FsZX1gKVxuICAgICAgLnJlbW92ZUNsYXNzKCdkLW5vbmUnKTtcblxuICAgIGlmIChjaGFuZ2VMYW5ndWFnZVVybCkge1xuICAgICAgdGhpcy5zYXZlU2VsZWN0ZWRMYW5ndWFnZShjaGFuZ2VMYW5ndWFnZVVybCwgdGhpcy5zZWxlY3RlZExvY2FsZSk7XG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIFNhdmUgbGFuZ3VhZ2UgY2hvaWNlIGZvciBlbXBsb3llZSBmb3Jtcy5cbiAgICpcbiAgICogQHBhcmFtIHtTdHJpbmd9IGNoYW5nZUxhbmd1YWdlVXJsXG4gICAqIEBwYXJhbSB7U3RyaW5nfSBzZWxlY3RlZExvY2FsZVxuICAgKlxuICAgKiBAcHJpdmF0ZVxuICAgKi9cbiAgc2F2ZVNlbGVjdGVkTGFuZ3VhZ2UoY2hhbmdlTGFuZ3VhZ2VVcmwsIHNlbGVjdGVkTG9jYWxlKSB7XG4gICAgJC5wb3N0KHtcbiAgICAgIHVybDogY2hhbmdlTGFuZ3VhZ2VVcmwsXG4gICAgICBkYXRhOiB7XG4gICAgICAgIGxhbmd1YWdlX2lzb19jb2RlOiBzZWxlY3RlZExvY2FsZSxcbiAgICAgIH0sXG4gICAgfSk7XG4gIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgVHJhbnNsYXRhYmxlSW5wdXQ7XG4iLCIvKipcbiAqIENvcHlyaWdodCBzaW5jZSAyMDA3IFByZXN0YVNob3AgU0EgYW5kIENvbnRyaWJ1dG9yc1xuICogUHJlc3RhU2hvcCBpcyBhbiBJbnRlcm5hdGlvbmFsIFJlZ2lzdGVyZWQgVHJhZGVtYXJrICYgUHJvcGVydHkgb2YgUHJlc3RhU2hvcCBTQVxuICpcbiAqIE5PVElDRSBPRiBMSUNFTlNFXG4gKlxuICogVGhpcyBzb3VyY2UgZmlsZSBpcyBzdWJqZWN0IHRvIHRoZSBPcGVuIFNvZnR3YXJlIExpY2Vuc2UgKE9TTCAzLjApXG4gKiB0aGF0IGlzIGJ1bmRsZWQgd2l0aCB0aGlzIHBhY2thZ2UgaW4gdGhlIGZpbGUgTElDRU5TRS5tZC5cbiAqIEl0IGlzIGFsc28gYXZhaWxhYmxlIHRocm91Z2ggdGhlIHdvcmxkLXdpZGUtd2ViIGF0IHRoaXMgVVJMOlxuICogaHR0cHM6Ly9vcGVuc291cmNlLm9yZy9saWNlbnNlcy9PU0wtMy4wXG4gKiBJZiB5b3UgZGlkIG5vdCByZWNlaXZlIGEgY29weSBvZiB0aGUgbGljZW5zZSBhbmQgYXJlIHVuYWJsZSB0b1xuICogb2J0YWluIGl0IHRocm91Z2ggdGhlIHdvcmxkLXdpZGUtd2ViLCBwbGVhc2Ugc2VuZCBhbiBlbWFpbFxuICogdG8gbGljZW5zZUBwcmVzdGFzaG9wLmNvbSBzbyB3ZSBjYW4gc2VuZCB5b3UgYSBjb3B5IGltbWVkaWF0ZWx5LlxuICpcbiAqIERJU0NMQUlNRVJcbiAqXG4gKiBEbyBub3QgZWRpdCBvciBhZGQgdG8gdGhpcyBmaWxlIGlmIHlvdSB3aXNoIHRvIHVwZ3JhZGUgUHJlc3RhU2hvcCB0byBuZXdlclxuICogdmVyc2lvbnMgaW4gdGhlIGZ1dHVyZS4gSWYgeW91IHdpc2ggdG8gY3VzdG9taXplIFByZXN0YVNob3AgZm9yIHlvdXJcbiAqIG5lZWRzIHBsZWFzZSByZWZlciB0byBodHRwczovL2RldmRvY3MucHJlc3Rhc2hvcC5jb20vIGZvciBtb3JlIGluZm9ybWF0aW9uLlxuICpcbiAqIEBhdXRob3IgICAgUHJlc3RhU2hvcCBTQSBhbmQgQ29udHJpYnV0b3JzIDxjb250YWN0QHByZXN0YXNob3AuY29tPlxuICogQGNvcHlyaWdodCBTaW5jZSAyMDA3IFByZXN0YVNob3AgU0EgYW5kIENvbnRyaWJ1dG9yc1xuICogQGxpY2Vuc2UgICBodHRwczovL29wZW5zb3VyY2Uub3JnL2xpY2Vuc2VzL09TTC0zLjAgT3BlbiBTb2Z0d2FyZSBMaWNlbnNlIChPU0wgMy4wKVxuICovXG5cbi8qKlxuICogRGVmaW5lcyBhbGwgc2VsZWN0b3JzIHRoYXQgYXJlIHVzZWQgaW4gY3VycmVuY3kgYWRkL2VkaXQgZm9ybS5cbiAqL1xuZXhwb3J0IGRlZmF1bHQge1xuICBzZW5kRW1haWxTZWxlY3RvcjogJyNvcmRlcl9zdGF0ZV9zZW5kX2VtYWlsJyxcbiAgbWFpbFRlbXBsYXRlU2VsZWN0b3I6ICcub3JkZXJfc3RhdGVfdGVtcGxhdGVfc2VsZWN0JyxcbiAgbWFpbFRlbXBsYXRlUHJldmlldzogJyNvcmRlcl9zdGF0ZV90ZW1wbGF0ZV9wcmV2aWV3Jyxcbn07XG4iLCJtb2R1bGUuZXhwb3J0cyA9IHsgXCJkZWZhdWx0XCI6IHJlcXVpcmUoXCJjb3JlLWpzL2xpYnJhcnkvZm4vb2JqZWN0L2RlZmluZS1wcm9wZXJ0eVwiKSwgX19lc01vZHVsZTogdHJ1ZSB9OyIsIlwidXNlIHN0cmljdFwiO1xuXG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xuXG5leHBvcnRzLmRlZmF1bHQgPSBmdW5jdGlvbiAoaW5zdGFuY2UsIENvbnN0cnVjdG9yKSB7XG4gIGlmICghKGluc3RhbmNlIGluc3RhbmNlb2YgQ29uc3RydWN0b3IpKSB7XG4gICAgdGhyb3cgbmV3IFR5cGVFcnJvcihcIkNhbm5vdCBjYWxsIGEgY2xhc3MgYXMgYSBmdW5jdGlvblwiKTtcbiAgfVxufTsiLCJcInVzZSBzdHJpY3RcIjtcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcblxudmFyIF9kZWZpbmVQcm9wZXJ0eSA9IHJlcXVpcmUoXCIuLi9jb3JlLWpzL29iamVjdC9kZWZpbmUtcHJvcGVydHlcIik7XG5cbnZhciBfZGVmaW5lUHJvcGVydHkyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZGVmaW5lUHJvcGVydHkpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG5leHBvcnRzLmRlZmF1bHQgPSBmdW5jdGlvbiAoKSB7XG4gIGZ1bmN0aW9uIGRlZmluZVByb3BlcnRpZXModGFyZ2V0LCBwcm9wcykge1xuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgcHJvcHMubGVuZ3RoOyBpKyspIHtcbiAgICAgIHZhciBkZXNjcmlwdG9yID0gcHJvcHNbaV07XG4gICAgICBkZXNjcmlwdG9yLmVudW1lcmFibGUgPSBkZXNjcmlwdG9yLmVudW1lcmFibGUgfHwgZmFsc2U7XG4gICAgICBkZXNjcmlwdG9yLmNvbmZpZ3VyYWJsZSA9IHRydWU7XG4gICAgICBpZiAoXCJ2YWx1ZVwiIGluIGRlc2NyaXB0b3IpIGRlc2NyaXB0b3Iud3JpdGFibGUgPSB0cnVlO1xuICAgICAgKDAsIF9kZWZpbmVQcm9wZXJ0eTIuZGVmYXVsdCkodGFyZ2V0LCBkZXNjcmlwdG9yLmtleSwgZGVzY3JpcHRvcik7XG4gICAgfVxuICB9XG5cbiAgcmV0dXJuIGZ1bmN0aW9uIChDb25zdHJ1Y3RvciwgcHJvdG9Qcm9wcywgc3RhdGljUHJvcHMpIHtcbiAgICBpZiAocHJvdG9Qcm9wcykgZGVmaW5lUHJvcGVydGllcyhDb25zdHJ1Y3Rvci5wcm90b3R5cGUsIHByb3RvUHJvcHMpO1xuICAgIGlmIChzdGF0aWNQcm9wcykgZGVmaW5lUHJvcGVydGllcyhDb25zdHJ1Y3Rvciwgc3RhdGljUHJvcHMpO1xuICAgIHJldHVybiBDb25zdHJ1Y3RvcjtcbiAgfTtcbn0oKTsiLCJyZXF1aXJlKCcuLi8uLi9tb2R1bGVzL2VzNi5vYmplY3QuZGVmaW5lLXByb3BlcnR5Jyk7XG52YXIgJE9iamVjdCA9IHJlcXVpcmUoJy4uLy4uL21vZHVsZXMvX2NvcmUnKS5PYmplY3Q7XG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIGRlZmluZVByb3BlcnR5KGl0LCBrZXksIGRlc2MpIHtcbiAgcmV0dXJuICRPYmplY3QuZGVmaW5lUHJvcGVydHkoaXQsIGtleSwgZGVzYyk7XG59O1xuIiwibW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAoaXQpIHtcbiAgaWYgKHR5cGVvZiBpdCAhPSAnZnVuY3Rpb24nKSB0aHJvdyBUeXBlRXJyb3IoaXQgKyAnIGlzIG5vdCBhIGZ1bmN0aW9uIScpO1xuICByZXR1cm4gaXQ7XG59O1xuIiwidmFyIGlzT2JqZWN0ID0gcmVxdWlyZSgnLi9faXMtb2JqZWN0Jyk7XG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIChpdCkge1xuICBpZiAoIWlzT2JqZWN0KGl0KSkgdGhyb3cgVHlwZUVycm9yKGl0ICsgJyBpcyBub3QgYW4gb2JqZWN0IScpO1xuICByZXR1cm4gaXQ7XG59O1xuIiwidmFyIGNvcmUgPSBtb2R1bGUuZXhwb3J0cyA9IHsgdmVyc2lvbjogJzIuNi4xMScgfTtcbmlmICh0eXBlb2YgX19lID09ICdudW1iZXInKSBfX2UgPSBjb3JlOyAvLyBlc2xpbnQtZGlzYWJsZS1saW5lIG5vLXVuZGVmXG4iLCIvLyBvcHRpb25hbCAvIHNpbXBsZSBjb250ZXh0IGJpbmRpbmdcbnZhciBhRnVuY3Rpb24gPSByZXF1aXJlKCcuL19hLWZ1bmN0aW9uJyk7XG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIChmbiwgdGhhdCwgbGVuZ3RoKSB7XG4gIGFGdW5jdGlvbihmbik7XG4gIGlmICh0aGF0ID09PSB1bmRlZmluZWQpIHJldHVybiBmbjtcbiAgc3dpdGNoIChsZW5ndGgpIHtcbiAgICBjYXNlIDE6IHJldHVybiBmdW5jdGlvbiAoYSkge1xuICAgICAgcmV0dXJuIGZuLmNhbGwodGhhdCwgYSk7XG4gICAgfTtcbiAgICBjYXNlIDI6IHJldHVybiBmdW5jdGlvbiAoYSwgYikge1xuICAgICAgcmV0dXJuIGZuLmNhbGwodGhhdCwgYSwgYik7XG4gICAgfTtcbiAgICBjYXNlIDM6IHJldHVybiBmdW5jdGlvbiAoYSwgYiwgYykge1xuICAgICAgcmV0dXJuIGZuLmNhbGwodGhhdCwgYSwgYiwgYyk7XG4gICAgfTtcbiAgfVxuICByZXR1cm4gZnVuY3Rpb24gKC8qIC4uLmFyZ3MgKi8pIHtcbiAgICByZXR1cm4gZm4uYXBwbHkodGhhdCwgYXJndW1lbnRzKTtcbiAgfTtcbn07XG4iLCIvLyBUaGFuaydzIElFOCBmb3IgaGlzIGZ1bm55IGRlZmluZVByb3BlcnR5XG5tb2R1bGUuZXhwb3J0cyA9ICFyZXF1aXJlKCcuL19mYWlscycpKGZ1bmN0aW9uICgpIHtcbiAgcmV0dXJuIE9iamVjdC5kZWZpbmVQcm9wZXJ0eSh7fSwgJ2EnLCB7IGdldDogZnVuY3Rpb24gKCkgeyByZXR1cm4gNzsgfSB9KS5hICE9IDc7XG59KTtcbiIsInZhciBpc09iamVjdCA9IHJlcXVpcmUoJy4vX2lzLW9iamVjdCcpO1xudmFyIGRvY3VtZW50ID0gcmVxdWlyZSgnLi9fZ2xvYmFsJykuZG9jdW1lbnQ7XG4vLyB0eXBlb2YgZG9jdW1lbnQuY3JlYXRlRWxlbWVudCBpcyAnb2JqZWN0JyBpbiBvbGQgSUVcbnZhciBpcyA9IGlzT2JqZWN0KGRvY3VtZW50KSAmJiBpc09iamVjdChkb2N1bWVudC5jcmVhdGVFbGVtZW50KTtcbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKGl0KSB7XG4gIHJldHVybiBpcyA/IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoaXQpIDoge307XG59O1xuIiwidmFyIGdsb2JhbCA9IHJlcXVpcmUoJy4vX2dsb2JhbCcpO1xudmFyIGNvcmUgPSByZXF1aXJlKCcuL19jb3JlJyk7XG52YXIgY3R4ID0gcmVxdWlyZSgnLi9fY3R4Jyk7XG52YXIgaGlkZSA9IHJlcXVpcmUoJy4vX2hpZGUnKTtcbnZhciBoYXMgPSByZXF1aXJlKCcuL19oYXMnKTtcbnZhciBQUk9UT1RZUEUgPSAncHJvdG90eXBlJztcblxudmFyICRleHBvcnQgPSBmdW5jdGlvbiAodHlwZSwgbmFtZSwgc291cmNlKSB7XG4gIHZhciBJU19GT1JDRUQgPSB0eXBlICYgJGV4cG9ydC5GO1xuICB2YXIgSVNfR0xPQkFMID0gdHlwZSAmICRleHBvcnQuRztcbiAgdmFyIElTX1NUQVRJQyA9IHR5cGUgJiAkZXhwb3J0LlM7XG4gIHZhciBJU19QUk9UTyA9IHR5cGUgJiAkZXhwb3J0LlA7XG4gIHZhciBJU19CSU5EID0gdHlwZSAmICRleHBvcnQuQjtcbiAgdmFyIElTX1dSQVAgPSB0eXBlICYgJGV4cG9ydC5XO1xuICB2YXIgZXhwb3J0cyA9IElTX0dMT0JBTCA/IGNvcmUgOiBjb3JlW25hbWVdIHx8IChjb3JlW25hbWVdID0ge30pO1xuICB2YXIgZXhwUHJvdG8gPSBleHBvcnRzW1BST1RPVFlQRV07XG4gIHZhciB0YXJnZXQgPSBJU19HTE9CQUwgPyBnbG9iYWwgOiBJU19TVEFUSUMgPyBnbG9iYWxbbmFtZV0gOiAoZ2xvYmFsW25hbWVdIHx8IHt9KVtQUk9UT1RZUEVdO1xuICB2YXIga2V5LCBvd24sIG91dDtcbiAgaWYgKElTX0dMT0JBTCkgc291cmNlID0gbmFtZTtcbiAgZm9yIChrZXkgaW4gc291cmNlKSB7XG4gICAgLy8gY29udGFpbnMgaW4gbmF0aXZlXG4gICAgb3duID0gIUlTX0ZPUkNFRCAmJiB0YXJnZXQgJiYgdGFyZ2V0W2tleV0gIT09IHVuZGVmaW5lZDtcbiAgICBpZiAob3duICYmIGhhcyhleHBvcnRzLCBrZXkpKSBjb250aW51ZTtcbiAgICAvLyBleHBvcnQgbmF0aXZlIG9yIHBhc3NlZFxuICAgIG91dCA9IG93biA/IHRhcmdldFtrZXldIDogc291cmNlW2tleV07XG4gICAgLy8gcHJldmVudCBnbG9iYWwgcG9sbHV0aW9uIGZvciBuYW1lc3BhY2VzXG4gICAgZXhwb3J0c1trZXldID0gSVNfR0xPQkFMICYmIHR5cGVvZiB0YXJnZXRba2V5XSAhPSAnZnVuY3Rpb24nID8gc291cmNlW2tleV1cbiAgICAvLyBiaW5kIHRpbWVycyB0byBnbG9iYWwgZm9yIGNhbGwgZnJvbSBleHBvcnQgY29udGV4dFxuICAgIDogSVNfQklORCAmJiBvd24gPyBjdHgob3V0LCBnbG9iYWwpXG4gICAgLy8gd3JhcCBnbG9iYWwgY29uc3RydWN0b3JzIGZvciBwcmV2ZW50IGNoYW5nZSB0aGVtIGluIGxpYnJhcnlcbiAgICA6IElTX1dSQVAgJiYgdGFyZ2V0W2tleV0gPT0gb3V0ID8gKGZ1bmN0aW9uIChDKSB7XG4gICAgICB2YXIgRiA9IGZ1bmN0aW9uIChhLCBiLCBjKSB7XG4gICAgICAgIGlmICh0aGlzIGluc3RhbmNlb2YgQykge1xuICAgICAgICAgIHN3aXRjaCAoYXJndW1lbnRzLmxlbmd0aCkge1xuICAgICAgICAgICAgY2FzZSAwOiByZXR1cm4gbmV3IEMoKTtcbiAgICAgICAgICAgIGNhc2UgMTogcmV0dXJuIG5ldyBDKGEpO1xuICAgICAgICAgICAgY2FzZSAyOiByZXR1cm4gbmV3IEMoYSwgYik7XG4gICAgICAgICAgfSByZXR1cm4gbmV3IEMoYSwgYiwgYyk7XG4gICAgICAgIH0gcmV0dXJuIEMuYXBwbHkodGhpcywgYXJndW1lbnRzKTtcbiAgICAgIH07XG4gICAgICBGW1BST1RPVFlQRV0gPSBDW1BST1RPVFlQRV07XG4gICAgICByZXR1cm4gRjtcbiAgICAvLyBtYWtlIHN0YXRpYyB2ZXJzaW9ucyBmb3IgcHJvdG90eXBlIG1ldGhvZHNcbiAgICB9KShvdXQpIDogSVNfUFJPVE8gJiYgdHlwZW9mIG91dCA9PSAnZnVuY3Rpb24nID8gY3R4KEZ1bmN0aW9uLmNhbGwsIG91dCkgOiBvdXQ7XG4gICAgLy8gZXhwb3J0IHByb3RvIG1ldGhvZHMgdG8gY29yZS4lQ09OU1RSVUNUT1IlLm1ldGhvZHMuJU5BTUUlXG4gICAgaWYgKElTX1BST1RPKSB7XG4gICAgICAoZXhwb3J0cy52aXJ0dWFsIHx8IChleHBvcnRzLnZpcnR1YWwgPSB7fSkpW2tleV0gPSBvdXQ7XG4gICAgICAvLyBleHBvcnQgcHJvdG8gbWV0aG9kcyB0byBjb3JlLiVDT05TVFJVQ1RPUiUucHJvdG90eXBlLiVOQU1FJVxuICAgICAgaWYgKHR5cGUgJiAkZXhwb3J0LlIgJiYgZXhwUHJvdG8gJiYgIWV4cFByb3RvW2tleV0pIGhpZGUoZXhwUHJvdG8sIGtleSwgb3V0KTtcbiAgICB9XG4gIH1cbn07XG4vLyB0eXBlIGJpdG1hcFxuJGV4cG9ydC5GID0gMTsgICAvLyBmb3JjZWRcbiRleHBvcnQuRyA9IDI7ICAgLy8gZ2xvYmFsXG4kZXhwb3J0LlMgPSA0OyAgIC8vIHN0YXRpY1xuJGV4cG9ydC5QID0gODsgICAvLyBwcm90b1xuJGV4cG9ydC5CID0gMTY7ICAvLyBiaW5kXG4kZXhwb3J0LlcgPSAzMjsgIC8vIHdyYXBcbiRleHBvcnQuVSA9IDY0OyAgLy8gc2FmZVxuJGV4cG9ydC5SID0gMTI4OyAvLyByZWFsIHByb3RvIG1ldGhvZCBmb3IgYGxpYnJhcnlgXG5tb2R1bGUuZXhwb3J0cyA9ICRleHBvcnQ7XG4iLCJtb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIChleGVjKSB7XG4gIHRyeSB7XG4gICAgcmV0dXJuICEhZXhlYygpO1xuICB9IGNhdGNoIChlKSB7XG4gICAgcmV0dXJuIHRydWU7XG4gIH1cbn07XG4iLCIvLyBodHRwczovL2dpdGh1Yi5jb20vemxvaXJvY2svY29yZS1qcy9pc3N1ZXMvODYjaXNzdWVjb21tZW50LTExNTc1OTAyOFxudmFyIGdsb2JhbCA9IG1vZHVsZS5leHBvcnRzID0gdHlwZW9mIHdpbmRvdyAhPSAndW5kZWZpbmVkJyAmJiB3aW5kb3cuTWF0aCA9PSBNYXRoXG4gID8gd2luZG93IDogdHlwZW9mIHNlbGYgIT0gJ3VuZGVmaW5lZCcgJiYgc2VsZi5NYXRoID09IE1hdGggPyBzZWxmXG4gIC8vIGVzbGludC1kaXNhYmxlLW5leHQtbGluZSBuby1uZXctZnVuY1xuICA6IEZ1bmN0aW9uKCdyZXR1cm4gdGhpcycpKCk7XG5pZiAodHlwZW9mIF9fZyA9PSAnbnVtYmVyJykgX19nID0gZ2xvYmFsOyAvLyBlc2xpbnQtZGlzYWJsZS1saW5lIG5vLXVuZGVmXG4iLCJ2YXIgaGFzT3duUHJvcGVydHkgPSB7fS5oYXNPd25Qcm9wZXJ0eTtcbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKGl0LCBrZXkpIHtcbiAgcmV0dXJuIGhhc093blByb3BlcnR5LmNhbGwoaXQsIGtleSk7XG59O1xuIiwidmFyIGRQID0gcmVxdWlyZSgnLi9fb2JqZWN0LWRwJyk7XG52YXIgY3JlYXRlRGVzYyA9IHJlcXVpcmUoJy4vX3Byb3BlcnR5LWRlc2MnKTtcbm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZSgnLi9fZGVzY3JpcHRvcnMnKSA/IGZ1bmN0aW9uIChvYmplY3QsIGtleSwgdmFsdWUpIHtcbiAgcmV0dXJuIGRQLmYob2JqZWN0LCBrZXksIGNyZWF0ZURlc2MoMSwgdmFsdWUpKTtcbn0gOiBmdW5jdGlvbiAob2JqZWN0LCBrZXksIHZhbHVlKSB7XG4gIG9iamVjdFtrZXldID0gdmFsdWU7XG4gIHJldHVybiBvYmplY3Q7XG59O1xuIiwibW9kdWxlLmV4cG9ydHMgPSAhcmVxdWlyZSgnLi9fZGVzY3JpcHRvcnMnKSAmJiAhcmVxdWlyZSgnLi9fZmFpbHMnKShmdW5jdGlvbiAoKSB7XG4gIHJldHVybiBPYmplY3QuZGVmaW5lUHJvcGVydHkocmVxdWlyZSgnLi9fZG9tLWNyZWF0ZScpKCdkaXYnKSwgJ2EnLCB7IGdldDogZnVuY3Rpb24gKCkgeyByZXR1cm4gNzsgfSB9KS5hICE9IDc7XG59KTtcbiIsIm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKGl0KSB7XG4gIHJldHVybiB0eXBlb2YgaXQgPT09ICdvYmplY3QnID8gaXQgIT09IG51bGwgOiB0eXBlb2YgaXQgPT09ICdmdW5jdGlvbic7XG59O1xuIiwidmFyIGFuT2JqZWN0ID0gcmVxdWlyZSgnLi9fYW4tb2JqZWN0Jyk7XG52YXIgSUU4X0RPTV9ERUZJTkUgPSByZXF1aXJlKCcuL19pZTgtZG9tLWRlZmluZScpO1xudmFyIHRvUHJpbWl0aXZlID0gcmVxdWlyZSgnLi9fdG8tcHJpbWl0aXZlJyk7XG52YXIgZFAgPSBPYmplY3QuZGVmaW5lUHJvcGVydHk7XG5cbmV4cG9ydHMuZiA9IHJlcXVpcmUoJy4vX2Rlc2NyaXB0b3JzJykgPyBPYmplY3QuZGVmaW5lUHJvcGVydHkgOiBmdW5jdGlvbiBkZWZpbmVQcm9wZXJ0eShPLCBQLCBBdHRyaWJ1dGVzKSB7XG4gIGFuT2JqZWN0KE8pO1xuICBQID0gdG9QcmltaXRpdmUoUCwgdHJ1ZSk7XG4gIGFuT2JqZWN0KEF0dHJpYnV0ZXMpO1xuICBpZiAoSUU4X0RPTV9ERUZJTkUpIHRyeSB7XG4gICAgcmV0dXJuIGRQKE8sIFAsIEF0dHJpYnV0ZXMpO1xuICB9IGNhdGNoIChlKSB7IC8qIGVtcHR5ICovIH1cbiAgaWYgKCdnZXQnIGluIEF0dHJpYnV0ZXMgfHwgJ3NldCcgaW4gQXR0cmlidXRlcykgdGhyb3cgVHlwZUVycm9yKCdBY2Nlc3NvcnMgbm90IHN1cHBvcnRlZCEnKTtcbiAgaWYgKCd2YWx1ZScgaW4gQXR0cmlidXRlcykgT1tQXSA9IEF0dHJpYnV0ZXMudmFsdWU7XG4gIHJldHVybiBPO1xufTtcbiIsIm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKGJpdG1hcCwgdmFsdWUpIHtcbiAgcmV0dXJuIHtcbiAgICBlbnVtZXJhYmxlOiAhKGJpdG1hcCAmIDEpLFxuICAgIGNvbmZpZ3VyYWJsZTogIShiaXRtYXAgJiAyKSxcbiAgICB3cml0YWJsZTogIShiaXRtYXAgJiA0KSxcbiAgICB2YWx1ZTogdmFsdWVcbiAgfTtcbn07XG4iLCIvLyA3LjEuMSBUb1ByaW1pdGl2ZShpbnB1dCBbLCBQcmVmZXJyZWRUeXBlXSlcbnZhciBpc09iamVjdCA9IHJlcXVpcmUoJy4vX2lzLW9iamVjdCcpO1xuLy8gaW5zdGVhZCBvZiB0aGUgRVM2IHNwZWMgdmVyc2lvbiwgd2UgZGlkbid0IGltcGxlbWVudCBAQHRvUHJpbWl0aXZlIGNhc2Vcbi8vIGFuZCB0aGUgc2Vjb25kIGFyZ3VtZW50IC0gZmxhZyAtIHByZWZlcnJlZCB0eXBlIGlzIGEgc3RyaW5nXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIChpdCwgUykge1xuICBpZiAoIWlzT2JqZWN0KGl0KSkgcmV0dXJuIGl0O1xuICB2YXIgZm4sIHZhbDtcbiAgaWYgKFMgJiYgdHlwZW9mIChmbiA9IGl0LnRvU3RyaW5nKSA9PSAnZnVuY3Rpb24nICYmICFpc09iamVjdCh2YWwgPSBmbi5jYWxsKGl0KSkpIHJldHVybiB2YWw7XG4gIGlmICh0eXBlb2YgKGZuID0gaXQudmFsdWVPZikgPT0gJ2Z1bmN0aW9uJyAmJiAhaXNPYmplY3QodmFsID0gZm4uY2FsbChpdCkpKSByZXR1cm4gdmFsO1xuICBpZiAoIVMgJiYgdHlwZW9mIChmbiA9IGl0LnRvU3RyaW5nKSA9PSAnZnVuY3Rpb24nICYmICFpc09iamVjdCh2YWwgPSBmbi5jYWxsKGl0KSkpIHJldHVybiB2YWw7XG4gIHRocm93IFR5cGVFcnJvcihcIkNhbid0IGNvbnZlcnQgb2JqZWN0IHRvIHByaW1pdGl2ZSB2YWx1ZVwiKTtcbn07XG4iLCJ2YXIgJGV4cG9ydCA9IHJlcXVpcmUoJy4vX2V4cG9ydCcpO1xuLy8gMTkuMS4yLjQgLyAxNS4yLjMuNiBPYmplY3QuZGVmaW5lUHJvcGVydHkoTywgUCwgQXR0cmlidXRlcylcbiRleHBvcnQoJGV4cG9ydC5TICsgJGV4cG9ydC5GICogIXJlcXVpcmUoJy4vX2Rlc2NyaXB0b3JzJyksICdPYmplY3QnLCB7IGRlZmluZVByb3BlcnR5OiByZXF1aXJlKCcuL19vYmplY3QtZHAnKS5mIH0pO1xuIiwiLy8gQ29weXJpZ2h0IEpveWVudCwgSW5jLiBhbmQgb3RoZXIgTm9kZSBjb250cmlidXRvcnMuXG4vL1xuLy8gUGVybWlzc2lvbiBpcyBoZXJlYnkgZ3JhbnRlZCwgZnJlZSBvZiBjaGFyZ2UsIHRvIGFueSBwZXJzb24gb2J0YWluaW5nIGFcbi8vIGNvcHkgb2YgdGhpcyBzb2Z0d2FyZSBhbmQgYXNzb2NpYXRlZCBkb2N1bWVudGF0aW9uIGZpbGVzICh0aGVcbi8vIFwiU29mdHdhcmVcIiksIHRvIGRlYWwgaW4gdGhlIFNvZnR3YXJlIHdpdGhvdXQgcmVzdHJpY3Rpb24sIGluY2x1ZGluZ1xuLy8gd2l0aG91dCBsaW1pdGF0aW9uIHRoZSByaWdodHMgdG8gdXNlLCBjb3B5LCBtb2RpZnksIG1lcmdlLCBwdWJsaXNoLFxuLy8gZGlzdHJpYnV0ZSwgc3VibGljZW5zZSwgYW5kL29yIHNlbGwgY29waWVzIG9mIHRoZSBTb2Z0d2FyZSwgYW5kIHRvIHBlcm1pdFxuLy8gcGVyc29ucyB0byB3aG9tIHRoZSBTb2Z0d2FyZSBpcyBmdXJuaXNoZWQgdG8gZG8gc28sIHN1YmplY3QgdG8gdGhlXG4vLyBmb2xsb3dpbmcgY29uZGl0aW9uczpcbi8vXG4vLyBUaGUgYWJvdmUgY29weXJpZ2h0IG5vdGljZSBhbmQgdGhpcyBwZXJtaXNzaW9uIG5vdGljZSBzaGFsbCBiZSBpbmNsdWRlZFxuLy8gaW4gYWxsIGNvcGllcyBvciBzdWJzdGFudGlhbCBwb3J0aW9ucyBvZiB0aGUgU29mdHdhcmUuXG4vL1xuLy8gVEhFIFNPRlRXQVJFIElTIFBST1ZJREVEIFwiQVMgSVNcIiwgV0lUSE9VVCBXQVJSQU5UWSBPRiBBTlkgS0lORCwgRVhQUkVTU1xuLy8gT1IgSU1QTElFRCwgSU5DTFVESU5HIEJVVCBOT1QgTElNSVRFRCBUTyBUSEUgV0FSUkFOVElFUyBPRlxuLy8gTUVSQ0hBTlRBQklMSVRZLCBGSVRORVNTIEZPUiBBIFBBUlRJQ1VMQVIgUFVSUE9TRSBBTkQgTk9OSU5GUklOR0VNRU5ULiBJTlxuLy8gTk8gRVZFTlQgU0hBTEwgVEhFIEFVVEhPUlMgT1IgQ09QWVJJR0hUIEhPTERFUlMgQkUgTElBQkxFIEZPUiBBTlkgQ0xBSU0sXG4vLyBEQU1BR0VTIE9SIE9USEVSIExJQUJJTElUWSwgV0hFVEhFUiBJTiBBTiBBQ1RJT04gT0YgQ09OVFJBQ1QsIFRPUlQgT1Jcbi8vIE9USEVSV0lTRSwgQVJJU0lORyBGUk9NLCBPVVQgT0YgT1IgSU4gQ09OTkVDVElPTiBXSVRIIFRIRSBTT0ZUV0FSRSBPUiBUSEVcbi8vIFVTRSBPUiBPVEhFUiBERUFMSU5HUyBJTiBUSEUgU09GVFdBUkUuXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIFIgPSB0eXBlb2YgUmVmbGVjdCA9PT0gJ29iamVjdCcgPyBSZWZsZWN0IDogbnVsbFxudmFyIFJlZmxlY3RBcHBseSA9IFIgJiYgdHlwZW9mIFIuYXBwbHkgPT09ICdmdW5jdGlvbidcbiAgPyBSLmFwcGx5XG4gIDogZnVuY3Rpb24gUmVmbGVjdEFwcGx5KHRhcmdldCwgcmVjZWl2ZXIsIGFyZ3MpIHtcbiAgICByZXR1cm4gRnVuY3Rpb24ucHJvdG90eXBlLmFwcGx5LmNhbGwodGFyZ2V0LCByZWNlaXZlciwgYXJncyk7XG4gIH1cblxudmFyIFJlZmxlY3RPd25LZXlzXG5pZiAoUiAmJiB0eXBlb2YgUi5vd25LZXlzID09PSAnZnVuY3Rpb24nKSB7XG4gIFJlZmxlY3RPd25LZXlzID0gUi5vd25LZXlzXG59IGVsc2UgaWYgKE9iamVjdC5nZXRPd25Qcm9wZXJ0eVN5bWJvbHMpIHtcbiAgUmVmbGVjdE93bktleXMgPSBmdW5jdGlvbiBSZWZsZWN0T3duS2V5cyh0YXJnZXQpIHtcbiAgICByZXR1cm4gT2JqZWN0LmdldE93blByb3BlcnR5TmFtZXModGFyZ2V0KVxuICAgICAgLmNvbmNhdChPYmplY3QuZ2V0T3duUHJvcGVydHlTeW1ib2xzKHRhcmdldCkpO1xuICB9O1xufSBlbHNlIHtcbiAgUmVmbGVjdE93bktleXMgPSBmdW5jdGlvbiBSZWZsZWN0T3duS2V5cyh0YXJnZXQpIHtcbiAgICByZXR1cm4gT2JqZWN0LmdldE93blByb3BlcnR5TmFtZXModGFyZ2V0KTtcbiAgfTtcbn1cblxuZnVuY3Rpb24gUHJvY2Vzc0VtaXRXYXJuaW5nKHdhcm5pbmcpIHtcbiAgaWYgKGNvbnNvbGUgJiYgY29uc29sZS53YXJuKSBjb25zb2xlLndhcm4od2FybmluZyk7XG59XG5cbnZhciBOdW1iZXJJc05hTiA9IE51bWJlci5pc05hTiB8fCBmdW5jdGlvbiBOdW1iZXJJc05hTih2YWx1ZSkge1xuICByZXR1cm4gdmFsdWUgIT09IHZhbHVlO1xufVxuXG5mdW5jdGlvbiBFdmVudEVtaXR0ZXIoKSB7XG4gIEV2ZW50RW1pdHRlci5pbml0LmNhbGwodGhpcyk7XG59XG5tb2R1bGUuZXhwb3J0cyA9IEV2ZW50RW1pdHRlcjtcblxuLy8gQmFja3dhcmRzLWNvbXBhdCB3aXRoIG5vZGUgMC4xMC54XG5FdmVudEVtaXR0ZXIuRXZlbnRFbWl0dGVyID0gRXZlbnRFbWl0dGVyO1xuXG5FdmVudEVtaXR0ZXIucHJvdG90eXBlLl9ldmVudHMgPSB1bmRlZmluZWQ7XG5FdmVudEVtaXR0ZXIucHJvdG90eXBlLl9ldmVudHNDb3VudCA9IDA7XG5FdmVudEVtaXR0ZXIucHJvdG90eXBlLl9tYXhMaXN0ZW5lcnMgPSB1bmRlZmluZWQ7XG5cbi8vIEJ5IGRlZmF1bHQgRXZlbnRFbWl0dGVycyB3aWxsIHByaW50IGEgd2FybmluZyBpZiBtb3JlIHRoYW4gMTAgbGlzdGVuZXJzIGFyZVxuLy8gYWRkZWQgdG8gaXQuIFRoaXMgaXMgYSB1c2VmdWwgZGVmYXVsdCB3aGljaCBoZWxwcyBmaW5kaW5nIG1lbW9yeSBsZWFrcy5cbnZhciBkZWZhdWx0TWF4TGlzdGVuZXJzID0gMTA7XG5cbmZ1bmN0aW9uIGNoZWNrTGlzdGVuZXIobGlzdGVuZXIpIHtcbiAgaWYgKHR5cGVvZiBsaXN0ZW5lciAhPT0gJ2Z1bmN0aW9uJykge1xuICAgIHRocm93IG5ldyBUeXBlRXJyb3IoJ1RoZSBcImxpc3RlbmVyXCIgYXJndW1lbnQgbXVzdCBiZSBvZiB0eXBlIEZ1bmN0aW9uLiBSZWNlaXZlZCB0eXBlICcgKyB0eXBlb2YgbGlzdGVuZXIpO1xuICB9XG59XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShFdmVudEVtaXR0ZXIsICdkZWZhdWx0TWF4TGlzdGVuZXJzJywge1xuICBlbnVtZXJhYmxlOiB0cnVlLFxuICBnZXQ6IGZ1bmN0aW9uKCkge1xuICAgIHJldHVybiBkZWZhdWx0TWF4TGlzdGVuZXJzO1xuICB9LFxuICBzZXQ6IGZ1bmN0aW9uKGFyZykge1xuICAgIGlmICh0eXBlb2YgYXJnICE9PSAnbnVtYmVyJyB8fCBhcmcgPCAwIHx8IE51bWJlcklzTmFOKGFyZykpIHtcbiAgICAgIHRocm93IG5ldyBSYW5nZUVycm9yKCdUaGUgdmFsdWUgb2YgXCJkZWZhdWx0TWF4TGlzdGVuZXJzXCIgaXMgb3V0IG9mIHJhbmdlLiBJdCBtdXN0IGJlIGEgbm9uLW5lZ2F0aXZlIG51bWJlci4gUmVjZWl2ZWQgJyArIGFyZyArICcuJyk7XG4gICAgfVxuICAgIGRlZmF1bHRNYXhMaXN0ZW5lcnMgPSBhcmc7XG4gIH1cbn0pO1xuXG5FdmVudEVtaXR0ZXIuaW5pdCA9IGZ1bmN0aW9uKCkge1xuXG4gIGlmICh0aGlzLl9ldmVudHMgPT09IHVuZGVmaW5lZCB8fFxuICAgICAgdGhpcy5fZXZlbnRzID09PSBPYmplY3QuZ2V0UHJvdG90eXBlT2YodGhpcykuX2V2ZW50cykge1xuICAgIHRoaXMuX2V2ZW50cyA9IE9iamVjdC5jcmVhdGUobnVsbCk7XG4gICAgdGhpcy5fZXZlbnRzQ291bnQgPSAwO1xuICB9XG5cbiAgdGhpcy5fbWF4TGlzdGVuZXJzID0gdGhpcy5fbWF4TGlzdGVuZXJzIHx8IHVuZGVmaW5lZDtcbn07XG5cbi8vIE9idmlvdXNseSBub3QgYWxsIEVtaXR0ZXJzIHNob3VsZCBiZSBsaW1pdGVkIHRvIDEwLiBUaGlzIGZ1bmN0aW9uIGFsbG93c1xuLy8gdGhhdCB0byBiZSBpbmNyZWFzZWQuIFNldCB0byB6ZXJvIGZvciB1bmxpbWl0ZWQuXG5FdmVudEVtaXR0ZXIucHJvdG90eXBlLnNldE1heExpc3RlbmVycyA9IGZ1bmN0aW9uIHNldE1heExpc3RlbmVycyhuKSB7XG4gIGlmICh0eXBlb2YgbiAhPT0gJ251bWJlcicgfHwgbiA8IDAgfHwgTnVtYmVySXNOYU4obikpIHtcbiAgICB0aHJvdyBuZXcgUmFuZ2VFcnJvcignVGhlIHZhbHVlIG9mIFwiblwiIGlzIG91dCBvZiByYW5nZS4gSXQgbXVzdCBiZSBhIG5vbi1uZWdhdGl2ZSBudW1iZXIuIFJlY2VpdmVkICcgKyBuICsgJy4nKTtcbiAgfVxuICB0aGlzLl9tYXhMaXN0ZW5lcnMgPSBuO1xuICByZXR1cm4gdGhpcztcbn07XG5cbmZ1bmN0aW9uIF9nZXRNYXhMaXN0ZW5lcnModGhhdCkge1xuICBpZiAodGhhdC5fbWF4TGlzdGVuZXJzID09PSB1bmRlZmluZWQpXG4gICAgcmV0dXJuIEV2ZW50RW1pdHRlci5kZWZhdWx0TWF4TGlzdGVuZXJzO1xuICByZXR1cm4gdGhhdC5fbWF4TGlzdGVuZXJzO1xufVxuXG5FdmVudEVtaXR0ZXIucHJvdG90eXBlLmdldE1heExpc3RlbmVycyA9IGZ1bmN0aW9uIGdldE1heExpc3RlbmVycygpIHtcbiAgcmV0dXJuIF9nZXRNYXhMaXN0ZW5lcnModGhpcyk7XG59O1xuXG5FdmVudEVtaXR0ZXIucHJvdG90eXBlLmVtaXQgPSBmdW5jdGlvbiBlbWl0KHR5cGUpIHtcbiAgdmFyIGFyZ3MgPSBbXTtcbiAgZm9yICh2YXIgaSA9IDE7IGkgPCBhcmd1bWVudHMubGVuZ3RoOyBpKyspIGFyZ3MucHVzaChhcmd1bWVudHNbaV0pO1xuICB2YXIgZG9FcnJvciA9ICh0eXBlID09PSAnZXJyb3InKTtcblxuICB2YXIgZXZlbnRzID0gdGhpcy5fZXZlbnRzO1xuICBpZiAoZXZlbnRzICE9PSB1bmRlZmluZWQpXG4gICAgZG9FcnJvciA9IChkb0Vycm9yICYmIGV2ZW50cy5lcnJvciA9PT0gdW5kZWZpbmVkKTtcbiAgZWxzZSBpZiAoIWRvRXJyb3IpXG4gICAgcmV0dXJuIGZhbHNlO1xuXG4gIC8vIElmIHRoZXJlIGlzIG5vICdlcnJvcicgZXZlbnQgbGlzdGVuZXIgdGhlbiB0aHJvdy5cbiAgaWYgKGRvRXJyb3IpIHtcbiAgICB2YXIgZXI7XG4gICAgaWYgKGFyZ3MubGVuZ3RoID4gMClcbiAgICAgIGVyID0gYXJnc1swXTtcbiAgICBpZiAoZXIgaW5zdGFuY2VvZiBFcnJvcikge1xuICAgICAgLy8gTm90ZTogVGhlIGNvbW1lbnRzIG9uIHRoZSBgdGhyb3dgIGxpbmVzIGFyZSBpbnRlbnRpb25hbCwgdGhleSBzaG93XG4gICAgICAvLyB1cCBpbiBOb2RlJ3Mgb3V0cHV0IGlmIHRoaXMgcmVzdWx0cyBpbiBhbiB1bmhhbmRsZWQgZXhjZXB0aW9uLlxuICAgICAgdGhyb3cgZXI7IC8vIFVuaGFuZGxlZCAnZXJyb3InIGV2ZW50XG4gICAgfVxuICAgIC8vIEF0IGxlYXN0IGdpdmUgc29tZSBraW5kIG9mIGNvbnRleHQgdG8gdGhlIHVzZXJcbiAgICB2YXIgZXJyID0gbmV3IEVycm9yKCdVbmhhbmRsZWQgZXJyb3IuJyArIChlciA/ICcgKCcgKyBlci5tZXNzYWdlICsgJyknIDogJycpKTtcbiAgICBlcnIuY29udGV4dCA9IGVyO1xuICAgIHRocm93IGVycjsgLy8gVW5oYW5kbGVkICdlcnJvcicgZXZlbnRcbiAgfVxuXG4gIHZhciBoYW5kbGVyID0gZXZlbnRzW3R5cGVdO1xuXG4gIGlmIChoYW5kbGVyID09PSB1bmRlZmluZWQpXG4gICAgcmV0dXJuIGZhbHNlO1xuXG4gIGlmICh0eXBlb2YgaGFuZGxlciA9PT0gJ2Z1bmN0aW9uJykge1xuICAgIFJlZmxlY3RBcHBseShoYW5kbGVyLCB0aGlzLCBhcmdzKTtcbiAgfSBlbHNlIHtcbiAgICB2YXIgbGVuID0gaGFuZGxlci5sZW5ndGg7XG4gICAgdmFyIGxpc3RlbmVycyA9IGFycmF5Q2xvbmUoaGFuZGxlciwgbGVuKTtcbiAgICBmb3IgKHZhciBpID0gMDsgaSA8IGxlbjsgKytpKVxuICAgICAgUmVmbGVjdEFwcGx5KGxpc3RlbmVyc1tpXSwgdGhpcywgYXJncyk7XG4gIH1cblxuICByZXR1cm4gdHJ1ZTtcbn07XG5cbmZ1bmN0aW9uIF9hZGRMaXN0ZW5lcih0YXJnZXQsIHR5cGUsIGxpc3RlbmVyLCBwcmVwZW5kKSB7XG4gIHZhciBtO1xuICB2YXIgZXZlbnRzO1xuICB2YXIgZXhpc3Rpbmc7XG5cbiAgY2hlY2tMaXN0ZW5lcihsaXN0ZW5lcik7XG5cbiAgZXZlbnRzID0gdGFyZ2V0Ll9ldmVudHM7XG4gIGlmIChldmVudHMgPT09IHVuZGVmaW5lZCkge1xuICAgIGV2ZW50cyA9IHRhcmdldC5fZXZlbnRzID0gT2JqZWN0LmNyZWF0ZShudWxsKTtcbiAgICB0YXJnZXQuX2V2ZW50c0NvdW50ID0gMDtcbiAgfSBlbHNlIHtcbiAgICAvLyBUbyBhdm9pZCByZWN1cnNpb24gaW4gdGhlIGNhc2UgdGhhdCB0eXBlID09PSBcIm5ld0xpc3RlbmVyXCIhIEJlZm9yZVxuICAgIC8vIGFkZGluZyBpdCB0byB0aGUgbGlzdGVuZXJzLCBmaXJzdCBlbWl0IFwibmV3TGlzdGVuZXJcIi5cbiAgICBpZiAoZXZlbnRzLm5ld0xpc3RlbmVyICE9PSB1bmRlZmluZWQpIHtcbiAgICAgIHRhcmdldC5lbWl0KCduZXdMaXN0ZW5lcicsIHR5cGUsXG4gICAgICAgICAgICAgICAgICBsaXN0ZW5lci5saXN0ZW5lciA/IGxpc3RlbmVyLmxpc3RlbmVyIDogbGlzdGVuZXIpO1xuXG4gICAgICAvLyBSZS1hc3NpZ24gYGV2ZW50c2AgYmVjYXVzZSBhIG5ld0xpc3RlbmVyIGhhbmRsZXIgY291bGQgaGF2ZSBjYXVzZWQgdGhlXG4gICAgICAvLyB0aGlzLl9ldmVudHMgdG8gYmUgYXNzaWduZWQgdG8gYSBuZXcgb2JqZWN0XG4gICAgICBldmVudHMgPSB0YXJnZXQuX2V2ZW50cztcbiAgICB9XG4gICAgZXhpc3RpbmcgPSBldmVudHNbdHlwZV07XG4gIH1cblxuICBpZiAoZXhpc3RpbmcgPT09IHVuZGVmaW5lZCkge1xuICAgIC8vIE9wdGltaXplIHRoZSBjYXNlIG9mIG9uZSBsaXN0ZW5lci4gRG9uJ3QgbmVlZCB0aGUgZXh0cmEgYXJyYXkgb2JqZWN0LlxuICAgIGV4aXN0aW5nID0gZXZlbnRzW3R5cGVdID0gbGlzdGVuZXI7XG4gICAgKyt0YXJnZXQuX2V2ZW50c0NvdW50O1xuICB9IGVsc2Uge1xuICAgIGlmICh0eXBlb2YgZXhpc3RpbmcgPT09ICdmdW5jdGlvbicpIHtcbiAgICAgIC8vIEFkZGluZyB0aGUgc2Vjb25kIGVsZW1lbnQsIG5lZWQgdG8gY2hhbmdlIHRvIGFycmF5LlxuICAgICAgZXhpc3RpbmcgPSBldmVudHNbdHlwZV0gPVxuICAgICAgICBwcmVwZW5kID8gW2xpc3RlbmVyLCBleGlzdGluZ10gOiBbZXhpc3RpbmcsIGxpc3RlbmVyXTtcbiAgICAgIC8vIElmIHdlJ3ZlIGFscmVhZHkgZ290IGFuIGFycmF5LCBqdXN0IGFwcGVuZC5cbiAgICB9IGVsc2UgaWYgKHByZXBlbmQpIHtcbiAgICAgIGV4aXN0aW5nLnVuc2hpZnQobGlzdGVuZXIpO1xuICAgIH0gZWxzZSB7XG4gICAgICBleGlzdGluZy5wdXNoKGxpc3RlbmVyKTtcbiAgICB9XG5cbiAgICAvLyBDaGVjayBmb3IgbGlzdGVuZXIgbGVha1xuICAgIG0gPSBfZ2V0TWF4TGlzdGVuZXJzKHRhcmdldCk7XG4gICAgaWYgKG0gPiAwICYmIGV4aXN0aW5nLmxlbmd0aCA+IG0gJiYgIWV4aXN0aW5nLndhcm5lZCkge1xuICAgICAgZXhpc3Rpbmcud2FybmVkID0gdHJ1ZTtcbiAgICAgIC8vIE5vIGVycm9yIGNvZGUgZm9yIHRoaXMgc2luY2UgaXQgaXMgYSBXYXJuaW5nXG4gICAgICAvLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmUgbm8tcmVzdHJpY3RlZC1zeW50YXhcbiAgICAgIHZhciB3ID0gbmV3IEVycm9yKCdQb3NzaWJsZSBFdmVudEVtaXR0ZXIgbWVtb3J5IGxlYWsgZGV0ZWN0ZWQuICcgK1xuICAgICAgICAgICAgICAgICAgICAgICAgICBleGlzdGluZy5sZW5ndGggKyAnICcgKyBTdHJpbmcodHlwZSkgKyAnIGxpc3RlbmVycyAnICtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgJ2FkZGVkLiBVc2UgZW1pdHRlci5zZXRNYXhMaXN0ZW5lcnMoKSB0byAnICtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgJ2luY3JlYXNlIGxpbWl0Jyk7XG4gICAgICB3Lm5hbWUgPSAnTWF4TGlzdGVuZXJzRXhjZWVkZWRXYXJuaW5nJztcbiAgICAgIHcuZW1pdHRlciA9IHRhcmdldDtcbiAgICAgIHcudHlwZSA9IHR5cGU7XG4gICAgICB3LmNvdW50ID0gZXhpc3RpbmcubGVuZ3RoO1xuICAgICAgUHJvY2Vzc0VtaXRXYXJuaW5nKHcpO1xuICAgIH1cbiAgfVxuXG4gIHJldHVybiB0YXJnZXQ7XG59XG5cbkV2ZW50RW1pdHRlci5wcm90b3R5cGUuYWRkTGlzdGVuZXIgPSBmdW5jdGlvbiBhZGRMaXN0ZW5lcih0eXBlLCBsaXN0ZW5lcikge1xuICByZXR1cm4gX2FkZExpc3RlbmVyKHRoaXMsIHR5cGUsIGxpc3RlbmVyLCBmYWxzZSk7XG59O1xuXG5FdmVudEVtaXR0ZXIucHJvdG90eXBlLm9uID0gRXZlbnRFbWl0dGVyLnByb3RvdHlwZS5hZGRMaXN0ZW5lcjtcblxuRXZlbnRFbWl0dGVyLnByb3RvdHlwZS5wcmVwZW5kTGlzdGVuZXIgPVxuICAgIGZ1bmN0aW9uIHByZXBlbmRMaXN0ZW5lcih0eXBlLCBsaXN0ZW5lcikge1xuICAgICAgcmV0dXJuIF9hZGRMaXN0ZW5lcih0aGlzLCB0eXBlLCBsaXN0ZW5lciwgdHJ1ZSk7XG4gICAgfTtcblxuZnVuY3Rpb24gb25jZVdyYXBwZXIoKSB7XG4gIGlmICghdGhpcy5maXJlZCkge1xuICAgIHRoaXMudGFyZ2V0LnJlbW92ZUxpc3RlbmVyKHRoaXMudHlwZSwgdGhpcy53cmFwRm4pO1xuICAgIHRoaXMuZmlyZWQgPSB0cnVlO1xuICAgIGlmIChhcmd1bWVudHMubGVuZ3RoID09PSAwKVxuICAgICAgcmV0dXJuIHRoaXMubGlzdGVuZXIuY2FsbCh0aGlzLnRhcmdldCk7XG4gICAgcmV0dXJuIHRoaXMubGlzdGVuZXIuYXBwbHkodGhpcy50YXJnZXQsIGFyZ3VtZW50cyk7XG4gIH1cbn1cblxuZnVuY3Rpb24gX29uY2VXcmFwKHRhcmdldCwgdHlwZSwgbGlzdGVuZXIpIHtcbiAgdmFyIHN0YXRlID0geyBmaXJlZDogZmFsc2UsIHdyYXBGbjogdW5kZWZpbmVkLCB0YXJnZXQ6IHRhcmdldCwgdHlwZTogdHlwZSwgbGlzdGVuZXI6IGxpc3RlbmVyIH07XG4gIHZhciB3cmFwcGVkID0gb25jZVdyYXBwZXIuYmluZChzdGF0ZSk7XG4gIHdyYXBwZWQubGlzdGVuZXIgPSBsaXN0ZW5lcjtcbiAgc3RhdGUud3JhcEZuID0gd3JhcHBlZDtcbiAgcmV0dXJuIHdyYXBwZWQ7XG59XG5cbkV2ZW50RW1pdHRlci5wcm90b3R5cGUub25jZSA9IGZ1bmN0aW9uIG9uY2UodHlwZSwgbGlzdGVuZXIpIHtcbiAgY2hlY2tMaXN0ZW5lcihsaXN0ZW5lcik7XG4gIHRoaXMub24odHlwZSwgX29uY2VXcmFwKHRoaXMsIHR5cGUsIGxpc3RlbmVyKSk7XG4gIHJldHVybiB0aGlzO1xufTtcblxuRXZlbnRFbWl0dGVyLnByb3RvdHlwZS5wcmVwZW5kT25jZUxpc3RlbmVyID1cbiAgICBmdW5jdGlvbiBwcmVwZW5kT25jZUxpc3RlbmVyKHR5cGUsIGxpc3RlbmVyKSB7XG4gICAgICBjaGVja0xpc3RlbmVyKGxpc3RlbmVyKTtcbiAgICAgIHRoaXMucHJlcGVuZExpc3RlbmVyKHR5cGUsIF9vbmNlV3JhcCh0aGlzLCB0eXBlLCBsaXN0ZW5lcikpO1xuICAgICAgcmV0dXJuIHRoaXM7XG4gICAgfTtcblxuLy8gRW1pdHMgYSAncmVtb3ZlTGlzdGVuZXInIGV2ZW50IGlmIGFuZCBvbmx5IGlmIHRoZSBsaXN0ZW5lciB3YXMgcmVtb3ZlZC5cbkV2ZW50RW1pdHRlci5wcm90b3R5cGUucmVtb3ZlTGlzdGVuZXIgPVxuICAgIGZ1bmN0aW9uIHJlbW92ZUxpc3RlbmVyKHR5cGUsIGxpc3RlbmVyKSB7XG4gICAgICB2YXIgbGlzdCwgZXZlbnRzLCBwb3NpdGlvbiwgaSwgb3JpZ2luYWxMaXN0ZW5lcjtcblxuICAgICAgY2hlY2tMaXN0ZW5lcihsaXN0ZW5lcik7XG5cbiAgICAgIGV2ZW50cyA9IHRoaXMuX2V2ZW50cztcbiAgICAgIGlmIChldmVudHMgPT09IHVuZGVmaW5lZClcbiAgICAgICAgcmV0dXJuIHRoaXM7XG5cbiAgICAgIGxpc3QgPSBldmVudHNbdHlwZV07XG4gICAgICBpZiAobGlzdCA9PT0gdW5kZWZpbmVkKVxuICAgICAgICByZXR1cm4gdGhpcztcblxuICAgICAgaWYgKGxpc3QgPT09IGxpc3RlbmVyIHx8IGxpc3QubGlzdGVuZXIgPT09IGxpc3RlbmVyKSB7XG4gICAgICAgIGlmICgtLXRoaXMuX2V2ZW50c0NvdW50ID09PSAwKVxuICAgICAgICAgIHRoaXMuX2V2ZW50cyA9IE9iamVjdC5jcmVhdGUobnVsbCk7XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgIGRlbGV0ZSBldmVudHNbdHlwZV07XG4gICAgICAgICAgaWYgKGV2ZW50cy5yZW1vdmVMaXN0ZW5lcilcbiAgICAgICAgICAgIHRoaXMuZW1pdCgncmVtb3ZlTGlzdGVuZXInLCB0eXBlLCBsaXN0Lmxpc3RlbmVyIHx8IGxpc3RlbmVyKTtcbiAgICAgICAgfVxuICAgICAgfSBlbHNlIGlmICh0eXBlb2YgbGlzdCAhPT0gJ2Z1bmN0aW9uJykge1xuICAgICAgICBwb3NpdGlvbiA9IC0xO1xuXG4gICAgICAgIGZvciAoaSA9IGxpc3QubGVuZ3RoIC0gMTsgaSA+PSAwOyBpLS0pIHtcbiAgICAgICAgICBpZiAobGlzdFtpXSA9PT0gbGlzdGVuZXIgfHwgbGlzdFtpXS5saXN0ZW5lciA9PT0gbGlzdGVuZXIpIHtcbiAgICAgICAgICAgIG9yaWdpbmFsTGlzdGVuZXIgPSBsaXN0W2ldLmxpc3RlbmVyO1xuICAgICAgICAgICAgcG9zaXRpb24gPSBpO1xuICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgaWYgKHBvc2l0aW9uIDwgMClcbiAgICAgICAgICByZXR1cm4gdGhpcztcblxuICAgICAgICBpZiAocG9zaXRpb24gPT09IDApXG4gICAgICAgICAgbGlzdC5zaGlmdCgpO1xuICAgICAgICBlbHNlIHtcbiAgICAgICAgICBzcGxpY2VPbmUobGlzdCwgcG9zaXRpb24pO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKGxpc3QubGVuZ3RoID09PSAxKVxuICAgICAgICAgIGV2ZW50c1t0eXBlXSA9IGxpc3RbMF07XG5cbiAgICAgICAgaWYgKGV2ZW50cy5yZW1vdmVMaXN0ZW5lciAhPT0gdW5kZWZpbmVkKVxuICAgICAgICAgIHRoaXMuZW1pdCgncmVtb3ZlTGlzdGVuZXInLCB0eXBlLCBvcmlnaW5hbExpc3RlbmVyIHx8IGxpc3RlbmVyKTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIHRoaXM7XG4gICAgfTtcblxuRXZlbnRFbWl0dGVyLnByb3RvdHlwZS5vZmYgPSBFdmVudEVtaXR0ZXIucHJvdG90eXBlLnJlbW92ZUxpc3RlbmVyO1xuXG5FdmVudEVtaXR0ZXIucHJvdG90eXBlLnJlbW92ZUFsbExpc3RlbmVycyA9XG4gICAgZnVuY3Rpb24gcmVtb3ZlQWxsTGlzdGVuZXJzKHR5cGUpIHtcbiAgICAgIHZhciBsaXN0ZW5lcnMsIGV2ZW50cywgaTtcblxuICAgICAgZXZlbnRzID0gdGhpcy5fZXZlbnRzO1xuICAgICAgaWYgKGV2ZW50cyA9PT0gdW5kZWZpbmVkKVxuICAgICAgICByZXR1cm4gdGhpcztcblxuICAgICAgLy8gbm90IGxpc3RlbmluZyBmb3IgcmVtb3ZlTGlzdGVuZXIsIG5vIG5lZWQgdG8gZW1pdFxuICAgICAgaWYgKGV2ZW50cy5yZW1vdmVMaXN0ZW5lciA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICAgIGlmIChhcmd1bWVudHMubGVuZ3RoID09PSAwKSB7XG4gICAgICAgICAgdGhpcy5fZXZlbnRzID0gT2JqZWN0LmNyZWF0ZShudWxsKTtcbiAgICAgICAgICB0aGlzLl9ldmVudHNDb3VudCA9IDA7XG4gICAgICAgIH0gZWxzZSBpZiAoZXZlbnRzW3R5cGVdICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICBpZiAoLS10aGlzLl9ldmVudHNDb3VudCA9PT0gMClcbiAgICAgICAgICAgIHRoaXMuX2V2ZW50cyA9IE9iamVjdC5jcmVhdGUobnVsbCk7XG4gICAgICAgICAgZWxzZVxuICAgICAgICAgICAgZGVsZXRlIGV2ZW50c1t0eXBlXTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gdGhpcztcbiAgICAgIH1cblxuICAgICAgLy8gZW1pdCByZW1vdmVMaXN0ZW5lciBmb3IgYWxsIGxpc3RlbmVycyBvbiBhbGwgZXZlbnRzXG4gICAgICBpZiAoYXJndW1lbnRzLmxlbmd0aCA9PT0gMCkge1xuICAgICAgICB2YXIga2V5cyA9IE9iamVjdC5rZXlzKGV2ZW50cyk7XG4gICAgICAgIHZhciBrZXk7XG4gICAgICAgIGZvciAoaSA9IDA7IGkgPCBrZXlzLmxlbmd0aDsgKytpKSB7XG4gICAgICAgICAga2V5ID0ga2V5c1tpXTtcbiAgICAgICAgICBpZiAoa2V5ID09PSAncmVtb3ZlTGlzdGVuZXInKSBjb250aW51ZTtcbiAgICAgICAgICB0aGlzLnJlbW92ZUFsbExpc3RlbmVycyhrZXkpO1xuICAgICAgICB9XG4gICAgICAgIHRoaXMucmVtb3ZlQWxsTGlzdGVuZXJzKCdyZW1vdmVMaXN0ZW5lcicpO1xuICAgICAgICB0aGlzLl9ldmVudHMgPSBPYmplY3QuY3JlYXRlKG51bGwpO1xuICAgICAgICB0aGlzLl9ldmVudHNDb3VudCA9IDA7XG4gICAgICAgIHJldHVybiB0aGlzO1xuICAgICAgfVxuXG4gICAgICBsaXN0ZW5lcnMgPSBldmVudHNbdHlwZV07XG5cbiAgICAgIGlmICh0eXBlb2YgbGlzdGVuZXJzID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICAgIHRoaXMucmVtb3ZlTGlzdGVuZXIodHlwZSwgbGlzdGVuZXJzKTtcbiAgICAgIH0gZWxzZSBpZiAobGlzdGVuZXJzICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgLy8gTElGTyBvcmRlclxuICAgICAgICBmb3IgKGkgPSBsaXN0ZW5lcnMubGVuZ3RoIC0gMTsgaSA+PSAwOyBpLS0pIHtcbiAgICAgICAgICB0aGlzLnJlbW92ZUxpc3RlbmVyKHR5cGUsIGxpc3RlbmVyc1tpXSk7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgcmV0dXJuIHRoaXM7XG4gICAgfTtcblxuZnVuY3Rpb24gX2xpc3RlbmVycyh0YXJnZXQsIHR5cGUsIHVud3JhcCkge1xuICB2YXIgZXZlbnRzID0gdGFyZ2V0Ll9ldmVudHM7XG5cbiAgaWYgKGV2ZW50cyA9PT0gdW5kZWZpbmVkKVxuICAgIHJldHVybiBbXTtcblxuICB2YXIgZXZsaXN0ZW5lciA9IGV2ZW50c1t0eXBlXTtcbiAgaWYgKGV2bGlzdGVuZXIgPT09IHVuZGVmaW5lZClcbiAgICByZXR1cm4gW107XG5cbiAgaWYgKHR5cGVvZiBldmxpc3RlbmVyID09PSAnZnVuY3Rpb24nKVxuICAgIHJldHVybiB1bndyYXAgPyBbZXZsaXN0ZW5lci5saXN0ZW5lciB8fCBldmxpc3RlbmVyXSA6IFtldmxpc3RlbmVyXTtcblxuICByZXR1cm4gdW53cmFwID9cbiAgICB1bndyYXBMaXN0ZW5lcnMoZXZsaXN0ZW5lcikgOiBhcnJheUNsb25lKGV2bGlzdGVuZXIsIGV2bGlzdGVuZXIubGVuZ3RoKTtcbn1cblxuRXZlbnRFbWl0dGVyLnByb3RvdHlwZS5saXN0ZW5lcnMgPSBmdW5jdGlvbiBsaXN0ZW5lcnModHlwZSkge1xuICByZXR1cm4gX2xpc3RlbmVycyh0aGlzLCB0eXBlLCB0cnVlKTtcbn07XG5cbkV2ZW50RW1pdHRlci5wcm90b3R5cGUucmF3TGlzdGVuZXJzID0gZnVuY3Rpb24gcmF3TGlzdGVuZXJzKHR5cGUpIHtcbiAgcmV0dXJuIF9saXN0ZW5lcnModGhpcywgdHlwZSwgZmFsc2UpO1xufTtcblxuRXZlbnRFbWl0dGVyLmxpc3RlbmVyQ291bnQgPSBmdW5jdGlvbihlbWl0dGVyLCB0eXBlKSB7XG4gIGlmICh0eXBlb2YgZW1pdHRlci5saXN0ZW5lckNvdW50ID09PSAnZnVuY3Rpb24nKSB7XG4gICAgcmV0dXJuIGVtaXR0ZXIubGlzdGVuZXJDb3VudCh0eXBlKTtcbiAgfSBlbHNlIHtcbiAgICByZXR1cm4gbGlzdGVuZXJDb3VudC5jYWxsKGVtaXR0ZXIsIHR5cGUpO1xuICB9XG59O1xuXG5FdmVudEVtaXR0ZXIucHJvdG90eXBlLmxpc3RlbmVyQ291bnQgPSBsaXN0ZW5lckNvdW50O1xuZnVuY3Rpb24gbGlzdGVuZXJDb3VudCh0eXBlKSB7XG4gIHZhciBldmVudHMgPSB0aGlzLl9ldmVudHM7XG5cbiAgaWYgKGV2ZW50cyAhPT0gdW5kZWZpbmVkKSB7XG4gICAgdmFyIGV2bGlzdGVuZXIgPSBldmVudHNbdHlwZV07XG5cbiAgICBpZiAodHlwZW9mIGV2bGlzdGVuZXIgPT09ICdmdW5jdGlvbicpIHtcbiAgICAgIHJldHVybiAxO1xuICAgIH0gZWxzZSBpZiAoZXZsaXN0ZW5lciAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICByZXR1cm4gZXZsaXN0ZW5lci5sZW5ndGg7XG4gICAgfVxuICB9XG5cbiAgcmV0dXJuIDA7XG59XG5cbkV2ZW50RW1pdHRlci5wcm90b3R5cGUuZXZlbnROYW1lcyA9IGZ1bmN0aW9uIGV2ZW50TmFtZXMoKSB7XG4gIHJldHVybiB0aGlzLl9ldmVudHNDb3VudCA+IDAgPyBSZWZsZWN0T3duS2V5cyh0aGlzLl9ldmVudHMpIDogW107XG59O1xuXG5mdW5jdGlvbiBhcnJheUNsb25lKGFyciwgbikge1xuICB2YXIgY29weSA9IG5ldyBBcnJheShuKTtcbiAgZm9yICh2YXIgaSA9IDA7IGkgPCBuOyArK2kpXG4gICAgY29weVtpXSA9IGFycltpXTtcbiAgcmV0dXJuIGNvcHk7XG59XG5cbmZ1bmN0aW9uIHNwbGljZU9uZShsaXN0LCBpbmRleCkge1xuICBmb3IgKDsgaW5kZXggKyAxIDwgbGlzdC5sZW5ndGg7IGluZGV4KyspXG4gICAgbGlzdFtpbmRleF0gPSBsaXN0W2luZGV4ICsgMV07XG4gIGxpc3QucG9wKCk7XG59XG5cbmZ1bmN0aW9uIHVud3JhcExpc3RlbmVycyhhcnIpIHtcbiAgdmFyIHJldCA9IG5ldyBBcnJheShhcnIubGVuZ3RoKTtcbiAgZm9yICh2YXIgaSA9IDA7IGkgPCByZXQubGVuZ3RoOyArK2kpIHtcbiAgICByZXRbaV0gPSBhcnJbaV0ubGlzdGVuZXIgfHwgYXJyW2ldO1xuICB9XG4gIHJldHVybiByZXQ7XG59XG4iLCIvKioqIElNUE9SVFMgRlJPTSBpbXBvcnRzLWxvYWRlciAqKiovXG5cbihmdW5jdGlvbihkZWZpbmUsIGV4cG9ydHMpIHtcbi8qIVxuICogQm9vdHN0cmFwIENvbG9ycGlja2VyIC0gQm9vdHN0cmFwIENvbG9ycGlja2VyIGlzIGEgbW9kdWxhciBjb2xvciBwaWNrZXIgcGx1Z2luIGZvciBCb290c3RyYXAgNC5cbiAqIEBwYWNrYWdlIGJvb3RzdHJhcC1jb2xvcnBpY2tlclxuICogQHZlcnNpb24gdjMuMi4wXG4gKiBAbGljZW5zZSBNSVRcbiAqIEBsaW5rIGh0dHBzOi8vaXRzamF2aS5jb20vYm9vdHN0cmFwLWNvbG9ycGlja2VyL1xuICogQGxpbmsgaHR0cHM6Ly9naXRodWIuY29tL2l0c2phdmkvYm9vdHN0cmFwLWNvbG9ycGlja2VyLmdpdFxuICovXG4oZnVuY3Rpb24gd2VicGFja1VuaXZlcnNhbE1vZHVsZURlZmluaXRpb24ocm9vdCwgZmFjdG9yeSkge1xuXHRpZih0eXBlb2YgZXhwb3J0cyA9PT0gJ29iamVjdCcgJiYgdHlwZW9mIG1vZHVsZSA9PT0gJ29iamVjdCcpXG5cdFx0bW9kdWxlLmV4cG9ydHMgPSBmYWN0b3J5KHJlcXVpcmUoXCJqcXVlcnlcIikpO1xuXHRlbHNlIGlmKHR5cGVvZiBkZWZpbmUgPT09ICdmdW5jdGlvbicgJiYgZGVmaW5lLmFtZClcblx0XHRkZWZpbmUoXCJib290c3RyYXAtY29sb3JwaWNrZXJcIiwgW1wianF1ZXJ5XCJdLCBmYWN0b3J5KTtcblx0ZWxzZSBpZih0eXBlb2YgZXhwb3J0cyA9PT0gJ29iamVjdCcpXG5cdFx0ZXhwb3J0c1tcImJvb3RzdHJhcC1jb2xvcnBpY2tlclwiXSA9IGZhY3RvcnkocmVxdWlyZShcImpxdWVyeVwiKSk7XG5cdGVsc2Vcblx0XHRyb290W1wiYm9vdHN0cmFwLWNvbG9ycGlja2VyXCJdID0gZmFjdG9yeShyb290W1wialF1ZXJ5XCJdKTtcbn0pKHdpbmRvdywgZnVuY3Rpb24oX19XRUJQQUNLX0VYVEVSTkFMX01PRFVMRV9fMF9fKSB7XG5yZXR1cm4gLyoqKioqKi8gKGZ1bmN0aW9uKG1vZHVsZXMpIHsgLy8gd2VicGFja0Jvb3RzdHJhcFxuLyoqKioqKi8gXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4vKioqKioqLyBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG4vKioqKioqL1xuLyoqKioqKi8gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuLyoqKioqKi8gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG4vKioqKioqL1xuLyoqKioqKi8gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuLyoqKioqKi8gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4vKioqKioqLyBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0cztcbi8qKioqKiovIFx0XHR9XG4vKioqKioqLyBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbi8qKioqKiovIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4vKioqKioqLyBcdFx0XHRpOiBtb2R1bGVJZCxcbi8qKioqKiovIFx0XHRcdGw6IGZhbHNlLFxuLyoqKioqKi8gXHRcdFx0ZXhwb3J0czoge31cbi8qKioqKiovIFx0XHR9O1xuLyoqKioqKi9cbi8qKioqKiovIFx0XHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cbi8qKioqKiovIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcbi8qKioqKiovXG4vKioqKioqLyBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuLyoqKioqKi8gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcbi8qKioqKiovXG4vKioqKioqLyBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbi8qKioqKiovIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4vKioqKioqLyBcdH1cbi8qKioqKiovXG4vKioqKioqL1xuLyoqKioqKi8gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuLyoqKioqKi8gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuLyoqKioqKi9cbi8qKioqKiovIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbi8qKioqKiovIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcbi8qKioqKiovXG4vKioqKioqLyBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuLyoqKioqKi8gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbi8qKioqKiovIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4vKioqKioqLyBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwgeyBlbnVtZXJhYmxlOiB0cnVlLCBnZXQ6IGdldHRlciB9KTtcbi8qKioqKiovIFx0XHR9XG4vKioqKioqLyBcdH07XG4vKioqKioqL1xuLyoqKioqKi8gXHQvLyBkZWZpbmUgX19lc01vZHVsZSBvbiBleHBvcnRzXG4vKioqKioqLyBcdF9fd2VicGFja19yZXF1aXJlX18uciA9IGZ1bmN0aW9uKGV4cG9ydHMpIHtcbi8qKioqKiovIFx0XHRpZih0eXBlb2YgU3ltYm9sICE9PSAndW5kZWZpbmVkJyAmJiBTeW1ib2wudG9TdHJpbmdUYWcpIHtcbi8qKioqKiovIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBTeW1ib2wudG9TdHJpbmdUYWcsIHsgdmFsdWU6ICdNb2R1bGUnIH0pO1xuLyoqKioqKi8gXHRcdH1cbi8qKioqKiovIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ19fZXNNb2R1bGUnLCB7IHZhbHVlOiB0cnVlIH0pO1xuLyoqKioqKi8gXHR9O1xuLyoqKioqKi9cbi8qKioqKiovIFx0Ly8gY3JlYXRlIGEgZmFrZSBuYW1lc3BhY2Ugb2JqZWN0XG4vKioqKioqLyBcdC8vIG1vZGUgJiAxOiB2YWx1ZSBpcyBhIG1vZHVsZSBpZCwgcmVxdWlyZSBpdFxuLyoqKioqKi8gXHQvLyBtb2RlICYgMjogbWVyZ2UgYWxsIHByb3BlcnRpZXMgb2YgdmFsdWUgaW50byB0aGUgbnNcbi8qKioqKiovIFx0Ly8gbW9kZSAmIDQ6IHJldHVybiB2YWx1ZSB3aGVuIGFscmVhZHkgbnMgb2JqZWN0XG4vKioqKioqLyBcdC8vIG1vZGUgJiA4fDE6IGJlaGF2ZSBsaWtlIHJlcXVpcmVcbi8qKioqKiovIFx0X193ZWJwYWNrX3JlcXVpcmVfXy50ID0gZnVuY3Rpb24odmFsdWUsIG1vZGUpIHtcbi8qKioqKiovIFx0XHRpZihtb2RlICYgMSkgdmFsdWUgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKHZhbHVlKTtcbi8qKioqKiovIFx0XHRpZihtb2RlICYgOCkgcmV0dXJuIHZhbHVlO1xuLyoqKioqKi8gXHRcdGlmKChtb2RlICYgNCkgJiYgdHlwZW9mIHZhbHVlID09PSAnb2JqZWN0JyAmJiB2YWx1ZSAmJiB2YWx1ZS5fX2VzTW9kdWxlKSByZXR1cm4gdmFsdWU7XG4vKioqKioqLyBcdFx0dmFyIG5zID0gT2JqZWN0LmNyZWF0ZShudWxsKTtcbi8qKioqKiovIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIobnMpO1xuLyoqKioqKi8gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShucywgJ2RlZmF1bHQnLCB7IGVudW1lcmFibGU6IHRydWUsIHZhbHVlOiB2YWx1ZSB9KTtcbi8qKioqKiovIFx0XHRpZihtb2RlICYgMiAmJiB0eXBlb2YgdmFsdWUgIT0gJ3N0cmluZycpIGZvcih2YXIga2V5IGluIHZhbHVlKSBfX3dlYnBhY2tfcmVxdWlyZV9fLmQobnMsIGtleSwgZnVuY3Rpb24oa2V5KSB7IHJldHVybiB2YWx1ZVtrZXldOyB9LmJpbmQobnVsbCwga2V5KSk7XG4vKioqKioqLyBcdFx0cmV0dXJuIG5zO1xuLyoqKioqKi8gXHR9O1xuLyoqKioqKi9cbi8qKioqKiovIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbi8qKioqKiovIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4vKioqKioqLyBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4vKioqKioqLyBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuLyoqKioqKi8gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbi8qKioqKiovIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4vKioqKioqLyBcdFx0cmV0dXJuIGdldHRlcjtcbi8qKioqKiovIFx0fTtcbi8qKioqKiovXG4vKioqKioqLyBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuLyoqKioqKi8gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG4vKioqKioqL1xuLyoqKioqKi8gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuLyoqKioqKi8gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuLyoqKioqKi9cbi8qKioqKiovXG4vKioqKioqLyBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuLyoqKioqKi8gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSA3KTtcbi8qKioqKiovIH0pXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuLyoqKioqKi8gKFtcbi8qIDAgKi9cbi8qKiovIChmdW5jdGlvbihtb2R1bGUsIGV4cG9ydHMpIHtcblxubW9kdWxlLmV4cG9ydHMgPSBfX1dFQlBBQ0tfRVhURVJOQUxfTU9EVUxFX18wX187XG5cbi8qKiovIH0pLFxuLyogMSAqL1xuLyoqKi8gKGZ1bmN0aW9uKG1vZHVsZSwgZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXykge1xuXG5cInVzZSBzdHJpY3RcIjtcblxuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX2NyZWF0ZUNsYXNzID0gZnVuY3Rpb24gKCkgeyBmdW5jdGlvbiBkZWZpbmVQcm9wZXJ0aWVzKHRhcmdldCwgcHJvcHMpIHsgZm9yICh2YXIgaSA9IDA7IGkgPCBwcm9wcy5sZW5ndGg7IGkrKykgeyB2YXIgZGVzY3JpcHRvciA9IHByb3BzW2ldOyBkZXNjcmlwdG9yLmVudW1lcmFibGUgPSBkZXNjcmlwdG9yLmVudW1lcmFibGUgfHwgZmFsc2U7IGRlc2NyaXB0b3IuY29uZmlndXJhYmxlID0gdHJ1ZTsgaWYgKFwidmFsdWVcIiBpbiBkZXNjcmlwdG9yKSBkZXNjcmlwdG9yLndyaXRhYmxlID0gdHJ1ZTsgT2JqZWN0LmRlZmluZVByb3BlcnR5KHRhcmdldCwgZGVzY3JpcHRvci5rZXksIGRlc2NyaXB0b3IpOyB9IH0gcmV0dXJuIGZ1bmN0aW9uIChDb25zdHJ1Y3RvciwgcHJvdG9Qcm9wcywgc3RhdGljUHJvcHMpIHsgaWYgKHByb3RvUHJvcHMpIGRlZmluZVByb3BlcnRpZXMoQ29uc3RydWN0b3IucHJvdG90eXBlLCBwcm90b1Byb3BzKTsgaWYgKHN0YXRpY1Byb3BzKSBkZWZpbmVQcm9wZXJ0aWVzKENvbnN0cnVjdG9yLCBzdGF0aWNQcm9wcyk7IHJldHVybiBDb25zdHJ1Y3RvcjsgfTsgfSgpO1xuXG52YXIgX2pxdWVyeSA9IF9fd2VicGFja19yZXF1aXJlX18oMCk7XG5cbnZhciBfanF1ZXJ5MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2pxdWVyeSk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbmZ1bmN0aW9uIF9jbGFzc0NhbGxDaGVjayhpbnN0YW5jZSwgQ29uc3RydWN0b3IpIHsgaWYgKCEoaW5zdGFuY2UgaW5zdGFuY2VvZiBDb25zdHJ1Y3RvcikpIHsgdGhyb3cgbmV3IFR5cGVFcnJvcihcIkNhbm5vdCBjYWxsIGEgY2xhc3MgYXMgYSBmdW5jdGlvblwiKTsgfSB9XG5cbi8qKlxuICogQ29sb3JwaWNrZXIgZXh0ZW5zaW9uIGNsYXNzLlxuICovXG52YXIgRXh0ZW5zaW9uID0gZnVuY3Rpb24gKCkge1xuICAvKipcbiAgICogQHBhcmFtIHtDb2xvcnBpY2tlcn0gY29sb3JwaWNrZXJcbiAgICogQHBhcmFtIHtPYmplY3R9IG9wdGlvbnNcbiAgICovXG4gIGZ1bmN0aW9uIEV4dGVuc2lvbihjb2xvcnBpY2tlcikge1xuICAgIHZhciBvcHRpb25zID0gYXJndW1lbnRzLmxlbmd0aCA+IDEgJiYgYXJndW1lbnRzWzFdICE9PSB1bmRlZmluZWQgPyBhcmd1bWVudHNbMV0gOiB7fTtcblxuICAgIF9jbGFzc0NhbGxDaGVjayh0aGlzLCBFeHRlbnNpb24pO1xuXG4gICAgLyoqXG4gICAgICogVGhlIGNvbG9ycGlja2VyIGluc3RhbmNlXG4gICAgICogQHR5cGUge0NvbG9ycGlja2VyfVxuICAgICAqL1xuICAgIHRoaXMuY29sb3JwaWNrZXIgPSBjb2xvcnBpY2tlcjtcbiAgICAvKipcbiAgICAgKiBFeHRlbnNpb24gb3B0aW9uc1xuICAgICAqXG4gICAgICogQHR5cGUge09iamVjdH1cbiAgICAgKi9cbiAgICB0aGlzLm9wdGlvbnMgPSBvcHRpb25zO1xuXG4gICAgaWYgKCEodGhpcy5jb2xvcnBpY2tlci5lbGVtZW50ICYmIHRoaXMuY29sb3JwaWNrZXIuZWxlbWVudC5sZW5ndGgpKSB7XG4gICAgICB0aHJvdyBuZXcgRXJyb3IoJ0V4dGVuc2lvbjogdGhpcy5jb2xvcnBpY2tlci5lbGVtZW50IGlzIG5vdCB2YWxpZCcpO1xuICAgIH1cblxuICAgIHRoaXMuY29sb3JwaWNrZXIuZWxlbWVudC5vbignY29sb3JwaWNrZXJDcmVhdGUuY29sb3JwaWNrZXItZXh0JywgX2pxdWVyeTIuZGVmYXVsdC5wcm94eSh0aGlzLm9uQ3JlYXRlLCB0aGlzKSk7XG4gICAgdGhpcy5jb2xvcnBpY2tlci5lbGVtZW50Lm9uKCdjb2xvcnBpY2tlckRlc3Ryb3kuY29sb3JwaWNrZXItZXh0JywgX2pxdWVyeTIuZGVmYXVsdC5wcm94eSh0aGlzLm9uRGVzdHJveSwgdGhpcykpO1xuICAgIHRoaXMuY29sb3JwaWNrZXIuZWxlbWVudC5vbignY29sb3JwaWNrZXJVcGRhdGUuY29sb3JwaWNrZXItZXh0JywgX2pxdWVyeTIuZGVmYXVsdC5wcm94eSh0aGlzLm9uVXBkYXRlLCB0aGlzKSk7XG4gICAgdGhpcy5jb2xvcnBpY2tlci5lbGVtZW50Lm9uKCdjb2xvcnBpY2tlckNoYW5nZS5jb2xvcnBpY2tlci1leHQnLCBfanF1ZXJ5Mi5kZWZhdWx0LnByb3h5KHRoaXMub25DaGFuZ2UsIHRoaXMpKTtcbiAgICB0aGlzLmNvbG9ycGlja2VyLmVsZW1lbnQub24oJ2NvbG9ycGlja2VySW52YWxpZC5jb2xvcnBpY2tlci1leHQnLCBfanF1ZXJ5Mi5kZWZhdWx0LnByb3h5KHRoaXMub25JbnZhbGlkLCB0aGlzKSk7XG4gICAgdGhpcy5jb2xvcnBpY2tlci5lbGVtZW50Lm9uKCdjb2xvcnBpY2tlclNob3cuY29sb3JwaWNrZXItZXh0JywgX2pxdWVyeTIuZGVmYXVsdC5wcm94eSh0aGlzLm9uU2hvdywgdGhpcykpO1xuICAgIHRoaXMuY29sb3JwaWNrZXIuZWxlbWVudC5vbignY29sb3JwaWNrZXJIaWRlLmNvbG9ycGlja2VyLWV4dCcsIF9qcXVlcnkyLmRlZmF1bHQucHJveHkodGhpcy5vbkhpZGUsIHRoaXMpKTtcbiAgICB0aGlzLmNvbG9ycGlja2VyLmVsZW1lbnQub24oJ2NvbG9ycGlja2VyRW5hYmxlLmNvbG9ycGlja2VyLWV4dCcsIF9qcXVlcnkyLmRlZmF1bHQucHJveHkodGhpcy5vbkVuYWJsZSwgdGhpcykpO1xuICAgIHRoaXMuY29sb3JwaWNrZXIuZWxlbWVudC5vbignY29sb3JwaWNrZXJEaXNhYmxlLmNvbG9ycGlja2VyLWV4dCcsIF9qcXVlcnkyLmRlZmF1bHQucHJveHkodGhpcy5vbkRpc2FibGUsIHRoaXMpKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBGdW5jdGlvbiBjYWxsZWQgZXZlcnkgdGltZSBhIG5ldyBjb2xvciBuZWVkcyB0byBiZSBjcmVhdGVkLlxuICAgKiBSZXR1cm4gZmFsc2UgdG8gc2tpcCB0aGlzIHJlc29sdmVyIGFuZCBjb250aW51ZSB3aXRoIG90aGVyIGV4dGVuc2lvbnMnIG9uZXNcbiAgICogb3IgcmV0dXJuIGFueXRoaW5nIGVsc2UgdG8gY29uc2lkZXIgdGhlIGNvbG9yIHJlc29sdmVkLlxuICAgKlxuICAgKiBAcGFyYW0ge0NvbG9ySXRlbXxTdHJpbmd8Kn0gY29sb3JcbiAgICogQHBhcmFtIHtib29sZWFufSByZWFsQ29sb3IgaWYgdHJ1ZSwgdGhlIGNvbG9yIHNob3VsZCByZXNvbHZlIGludG8gYSByZWFsIChub3QgbmFtZWQpIGNvbG9yIGNvZGVcbiAgICogQHJldHVybiB7Q29sb3JJdGVtfFN0cmluZ3wqfVxuICAgKi9cblxuXG4gIF9jcmVhdGVDbGFzcyhFeHRlbnNpb24sIFt7XG4gICAga2V5OiAncmVzb2x2ZUNvbG9yJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gcmVzb2x2ZUNvbG9yKGNvbG9yKSB7XG4gICAgICB2YXIgcmVhbENvbG9yID0gYXJndW1lbnRzLmxlbmd0aCA+IDEgJiYgYXJndW1lbnRzWzFdICE9PSB1bmRlZmluZWQgPyBhcmd1bWVudHNbMV0gOiB0cnVlO1xuXG4gICAgICByZXR1cm4gZmFsc2U7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogTWV0aG9kIGNhbGxlZCBhZnRlciB0aGUgY29sb3JwaWNrZXIgaXMgY3JlYXRlZFxuICAgICAqXG4gICAgICogQGxpc3RlbnMgQ29sb3JwaWNrZXIjY29sb3JwaWNrZXJDcmVhdGVcbiAgICAgKiBAcGFyYW0ge0V2ZW50fSBldmVudFxuICAgICAqL1xuXG4gIH0sIHtcbiAgICBrZXk6ICdvbkNyZWF0ZScsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIG9uQ3JlYXRlKGV2ZW50KSB7fVxuICAgIC8vIHRvIGJlIGV4dGVuZGVkXG5cblxuICAgIC8qKlxuICAgICAqIE1ldGhvZCBjYWxsZWQgYWZ0ZXIgdGhlIGNvbG9ycGlja2VyIGlzIGRlc3Ryb3llZFxuICAgICAqXG4gICAgICogQGxpc3RlbnMgQ29sb3JwaWNrZXIjY29sb3JwaWNrZXJEZXN0cm95XG4gICAgICogQHBhcmFtIHtFdmVudH0gZXZlbnRcbiAgICAgKi9cblxuICB9LCB7XG4gICAga2V5OiAnb25EZXN0cm95JyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gb25EZXN0cm95KGV2ZW50KSB7XG4gICAgICB0aGlzLmNvbG9ycGlja2VyLmVsZW1lbnQub2ZmKCcuY29sb3JwaWNrZXItZXh0Jyk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogTWV0aG9kIGNhbGxlZCBhZnRlciB0aGUgY29sb3JwaWNrZXIgaXMgdXBkYXRlZFxuICAgICAqXG4gICAgICogQGxpc3RlbnMgQ29sb3JwaWNrZXIjY29sb3JwaWNrZXJVcGRhdGVcbiAgICAgKiBAcGFyYW0ge0V2ZW50fSBldmVudFxuICAgICAqL1xuXG4gIH0sIHtcbiAgICBrZXk6ICdvblVwZGF0ZScsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIG9uVXBkYXRlKGV2ZW50KSB7fVxuICAgIC8vIHRvIGJlIGV4dGVuZGVkXG5cblxuICAgIC8qKlxuICAgICAqIE1ldGhvZCBjYWxsZWQgYWZ0ZXIgdGhlIGNvbG9ycGlja2VyIGNvbG9yIGlzIGNoYW5nZWRcbiAgICAgKlxuICAgICAqIEBsaXN0ZW5zIENvbG9ycGlja2VyI2NvbG9ycGlja2VyQ2hhbmdlXG4gICAgICogQHBhcmFtIHtFdmVudH0gZXZlbnRcbiAgICAgKi9cblxuICB9LCB7XG4gICAga2V5OiAnb25DaGFuZ2UnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBvbkNoYW5nZShldmVudCkge31cbiAgICAvLyB0byBiZSBleHRlbmRlZFxuXG5cbiAgICAvKipcbiAgICAgKiBNZXRob2QgY2FsbGVkIHdoZW4gdGhlIGNvbG9ycGlja2VyIGNvbG9yIGlzIGludmFsaWRcbiAgICAgKlxuICAgICAqIEBsaXN0ZW5zIENvbG9ycGlja2VyI2NvbG9ycGlja2VySW52YWxpZFxuICAgICAqIEBwYXJhbSB7RXZlbnR9IGV2ZW50XG4gICAgICovXG5cbiAgfSwge1xuICAgIGtleTogJ29uSW52YWxpZCcsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIG9uSW52YWxpZChldmVudCkge31cbiAgICAvLyB0byBiZSBleHRlbmRlZFxuXG5cbiAgICAvKipcbiAgICAgKiBNZXRob2QgY2FsbGVkIGFmdGVyIHRoZSBjb2xvcnBpY2tlciBpcyBoaWRkZW5cbiAgICAgKlxuICAgICAqIEBsaXN0ZW5zIENvbG9ycGlja2VyI2NvbG9ycGlja2VySGlkZVxuICAgICAqIEBwYXJhbSB7RXZlbnR9IGV2ZW50XG4gICAgICovXG5cbiAgfSwge1xuICAgIGtleTogJ29uSGlkZScsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIG9uSGlkZShldmVudCkge31cbiAgICAvLyB0byBiZSBleHRlbmRlZFxuXG5cbiAgICAvKipcbiAgICAgKiBNZXRob2QgY2FsbGVkIGFmdGVyIHRoZSBjb2xvcnBpY2tlciBpcyBzaG93blxuICAgICAqXG4gICAgICogQGxpc3RlbnMgQ29sb3JwaWNrZXIjY29sb3JwaWNrZXJTaG93XG4gICAgICogQHBhcmFtIHtFdmVudH0gZXZlbnRcbiAgICAgKi9cblxuICB9LCB7XG4gICAga2V5OiAnb25TaG93JyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gb25TaG93KGV2ZW50KSB7fVxuICAgIC8vIHRvIGJlIGV4dGVuZGVkXG5cblxuICAgIC8qKlxuICAgICAqIE1ldGhvZCBjYWxsZWQgYWZ0ZXIgdGhlIGNvbG9ycGlja2VyIGlzIGRpc2FibGVkXG4gICAgICpcbiAgICAgKiBAbGlzdGVucyBDb2xvcnBpY2tlciNjb2xvcnBpY2tlckRpc2FibGVcbiAgICAgKiBAcGFyYW0ge0V2ZW50fSBldmVudFxuICAgICAqL1xuXG4gIH0sIHtcbiAgICBrZXk6ICdvbkRpc2FibGUnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBvbkRpc2FibGUoZXZlbnQpIHt9XG4gICAgLy8gdG8gYmUgZXh0ZW5kZWRcblxuXG4gICAgLyoqXG4gICAgICogTWV0aG9kIGNhbGxlZCBhZnRlciB0aGUgY29sb3JwaWNrZXIgaXMgZW5hYmxlZFxuICAgICAqXG4gICAgICogQGxpc3RlbnMgQ29sb3JwaWNrZXIjY29sb3JwaWNrZXJFbmFibGVcbiAgICAgKiBAcGFyYW0ge0V2ZW50fSBldmVudFxuICAgICAqL1xuXG4gIH0sIHtcbiAgICBrZXk6ICdvbkVuYWJsZScsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIG9uRW5hYmxlKGV2ZW50KSB7XG4gICAgICAvLyB0byBiZSBleHRlbmRlZFxuICAgIH1cbiAgfV0pO1xuXG4gIHJldHVybiBFeHRlbnNpb247XG59KCk7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IEV4dGVuc2lvbjtcbm1vZHVsZS5leHBvcnRzID0gZXhwb3J0cy5kZWZhdWx0O1xuXG4vKioqLyB9KSxcbi8qIDIgKi9cbi8qKiovIChmdW5jdGlvbihtb2R1bGUsIGV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pIHtcblxuXCJ1c2Ugc3RyaWN0XCI7XG5cblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcbmV4cG9ydHMuQ29sb3JJdGVtID0gZXhwb3J0cy5IU1ZBQ29sb3IgPSB1bmRlZmluZWQ7XG5cbnZhciBfY3JlYXRlQ2xhc3MgPSBmdW5jdGlvbiAoKSB7IGZ1bmN0aW9uIGRlZmluZVByb3BlcnRpZXModGFyZ2V0LCBwcm9wcykgeyBmb3IgKHZhciBpID0gMDsgaSA8IHByb3BzLmxlbmd0aDsgaSsrKSB7IHZhciBkZXNjcmlwdG9yID0gcHJvcHNbaV07IGRlc2NyaXB0b3IuZW51bWVyYWJsZSA9IGRlc2NyaXB0b3IuZW51bWVyYWJsZSB8fCBmYWxzZTsgZGVzY3JpcHRvci5jb25maWd1cmFibGUgPSB0cnVlOyBpZiAoXCJ2YWx1ZVwiIGluIGRlc2NyaXB0b3IpIGRlc2NyaXB0b3Iud3JpdGFibGUgPSB0cnVlOyBPYmplY3QuZGVmaW5lUHJvcGVydHkodGFyZ2V0LCBkZXNjcmlwdG9yLmtleSwgZGVzY3JpcHRvcik7IH0gfSByZXR1cm4gZnVuY3Rpb24gKENvbnN0cnVjdG9yLCBwcm90b1Byb3BzLCBzdGF0aWNQcm9wcykgeyBpZiAocHJvdG9Qcm9wcykgZGVmaW5lUHJvcGVydGllcyhDb25zdHJ1Y3Rvci5wcm90b3R5cGUsIHByb3RvUHJvcHMpOyBpZiAoc3RhdGljUHJvcHMpIGRlZmluZVByb3BlcnRpZXMoQ29uc3RydWN0b3IsIHN0YXRpY1Byb3BzKTsgcmV0dXJuIENvbnN0cnVjdG9yOyB9OyB9KCk7IC8qKlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAqIENvbG9yIG1hbmlwdWxhdGlvbiBjbGFzcywgc3BlY2lmaWMgZm9yIEJvb3RzdHJhcCBDb2xvcnBpY2tlclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAqL1xuXG5cbnZhciBfY29sb3IgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKDE2KTtcblxudmFyIF9jb2xvcjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jb2xvcik7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbmZ1bmN0aW9uIF9jbGFzc0NhbGxDaGVjayhpbnN0YW5jZSwgQ29uc3RydWN0b3IpIHsgaWYgKCEoaW5zdGFuY2UgaW5zdGFuY2VvZiBDb25zdHJ1Y3RvcikpIHsgdGhyb3cgbmV3IFR5cGVFcnJvcihcIkNhbm5vdCBjYWxsIGEgY2xhc3MgYXMgYSBmdW5jdGlvblwiKTsgfSB9XG5cbi8qKlxuICogSFNWQSBjb2xvciBkYXRhIGNsYXNzLCBjb250YWluaW5nIHRoZSBodWUsIHNhdHVyYXRpb24sIHZhbHVlIGFuZCBhbHBoYVxuICogaW5mb3JtYXRpb24uXG4gKi9cbnZhciBIU1ZBQ29sb3IgPSBmdW5jdGlvbiAoKSB7XG4gIC8qKlxuICAgKiBAcGFyYW0ge251bWJlcnxpbnR9IGhcbiAgICogQHBhcmFtIHtudW1iZXJ8aW50fSBzXG4gICAqIEBwYXJhbSB7bnVtYmVyfGludH0gdlxuICAgKiBAcGFyYW0ge251bWJlcnxpbnR9IGFcbiAgICovXG4gIGZ1bmN0aW9uIEhTVkFDb2xvcihoLCBzLCB2LCBhKSB7XG4gICAgX2NsYXNzQ2FsbENoZWNrKHRoaXMsIEhTVkFDb2xvcik7XG5cbiAgICB0aGlzLmggPSBpc05hTihoKSA/IDAgOiBoO1xuICAgIHRoaXMucyA9IGlzTmFOKHMpID8gMCA6IHM7XG4gICAgdGhpcy52ID0gaXNOYU4odikgPyAwIDogdjtcbiAgICB0aGlzLmEgPSBpc05hTihoKSA/IDEgOiBhO1xuICB9XG5cbiAgX2NyZWF0ZUNsYXNzKEhTVkFDb2xvciwgW3tcbiAgICBrZXk6ICd0b1N0cmluZycsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHRvU3RyaW5nKCkge1xuICAgICAgcmV0dXJuIHRoaXMuaCArICcsICcgKyB0aGlzLnMgKyAnJSwgJyArIHRoaXMudiArICclLCAnICsgdGhpcy5hO1xuICAgIH1cbiAgfV0pO1xuXG4gIHJldHVybiBIU1ZBQ29sb3I7XG59KCk7XG5cbi8qKlxuICogSFNWQSBjb2xvciBtYW5pcHVsYXRpb25cbiAqL1xuXG5cbnZhciBDb2xvckl0ZW0gPSBmdW5jdGlvbiAoKSB7XG4gIF9jcmVhdGVDbGFzcyhDb2xvckl0ZW0sIFt7XG4gICAga2V5OiAnYXBpJyxcblxuXG4gICAgLyoqXG4gICAgICogQXBwbGllcyBhIG1ldGhvZCBvZiB0aGUgUWl4Q29sb3IgQVBJIGFuZCByZXR1cm5zIGEgbmV3IENvbG9yIG9iamVjdCBvclxuICAgICAqIHRoZSByZXR1cm4gdmFsdWUgb2YgdGhlIG1ldGhvZCBjYWxsLlxuICAgICAqXG4gICAgICogSWYgbm8gYXJndW1lbnQgaXMgcHJvdmlkZWQsIHRoZSBpbnRlcm5hbCBRaXhDb2xvciBvYmplY3QgaXMgcmV0dXJuZWQuXG4gICAgICpcbiAgICAgKiBAcGFyYW0ge1N0cmluZ30gZm4gUWl4Q29sb3IgZnVuY3Rpb24gbmFtZVxuICAgICAqIEBwYXJhbSBhcmdzIFFpeENvbG9yIGZ1bmN0aW9uIGFyZ3VtZW50c1xuICAgICAqIEBleGFtcGxlIGxldCBkYXJrZXJDb2xvciA9IGNvbG9yLmFwaSgnZGFya2VuJywgMC4yNSk7XG4gICAgICogQGV4YW1wbGUgbGV0IGx1bWlub3NpdHkgPSBjb2xvci5hcGkoJ2x1bWlub3NpdHknKTtcbiAgICAgKiBAZXhhbXBsZSBjb2xvciA9IGNvbG9yLmFwaSgnbmVnYXRlJyk7XG4gICAgICogQGV4YW1wbGUgbGV0IHFDb2xvciA9IGNvbG9yLmFwaSgpLm5lZ2F0ZSgpO1xuICAgICAqIEByZXR1cm5zIHtDb2xvckl0ZW18UWl4Q29sb3J8Kn1cbiAgICAgKi9cbiAgICB2YWx1ZTogZnVuY3Rpb24gYXBpKGZuKSB7XG4gICAgICBmb3IgKHZhciBfbGVuID0gYXJndW1lbnRzLmxlbmd0aCwgYXJncyA9IEFycmF5KF9sZW4gPiAxID8gX2xlbiAtIDEgOiAwKSwgX2tleSA9IDE7IF9rZXkgPCBfbGVuOyBfa2V5KyspIHtcbiAgICAgICAgYXJnc1tfa2V5IC0gMV0gPSBhcmd1bWVudHNbX2tleV07XG4gICAgICB9XG5cbiAgICAgIGlmIChhcmd1bWVudHMubGVuZ3RoID09PSAwKSB7XG4gICAgICAgIHJldHVybiB0aGlzLl9jb2xvcjtcbiAgICAgIH1cblxuICAgICAgdmFyIHJlc3VsdCA9IHRoaXMuX2NvbG9yW2ZuXS5hcHBseSh0aGlzLl9jb2xvciwgYXJncyk7XG5cbiAgICAgIGlmICghKHJlc3VsdCBpbnN0YW5jZW9mIF9jb2xvcjIuZGVmYXVsdCkpIHtcbiAgICAgICAgLy8gcmV0dXJuIHJlc3VsdCBvZiB0aGUgbWV0aG9kIGNhbGxcbiAgICAgICAgcmV0dXJuIHJlc3VsdDtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIG5ldyBDb2xvckl0ZW0ocmVzdWx0LCB0aGlzLmZvcm1hdCk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogUmV0dXJucyB0aGUgb3JpZ2luYWwgQ29sb3JJdGVtIGNvbnN0cnVjdG9yIGRhdGEsXG4gICAgICogcGx1cyBhICd2YWxpZCcgZmxhZyB0byBrbm93IGlmIGl0J3MgdmFsaWQgb3Igbm90LlxuICAgICAqXG4gICAgICogQHJldHVybnMge3tjb2xvcjogKiwgZm9ybWF0OiBTdHJpbmcsIHZhbGlkOiBib29sZWFufX1cbiAgICAgKi9cblxuICB9LCB7XG4gICAga2V5OiAnb3JpZ2luYWwnLFxuICAgIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgICAgcmV0dXJuIHRoaXMuX29yaWdpbmFsO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEBwYXJhbSB7Q29sb3JJdGVtfEhTVkFDb2xvcnxRaXhDb2xvcnxTdHJpbmd8KnxudWxsfSBjb2xvciBDb2xvciBkYXRhXG4gICAgICogQHBhcmFtIHtTdHJpbmd8bnVsbH0gZm9ybWF0IENvbG9yIG1vZGVsIHRvIGNvbnZlcnQgdG8gYnkgZGVmYXVsdC4gU3VwcG9ydGVkOiAncmdiJywgJ2hzbCcsICdoZXgnLlxuICAgICAqL1xuXG4gIH1dLCBbe1xuICAgIGtleTogJ0hTVkFDb2xvcicsXG5cblxuICAgIC8qKlxuICAgICAqIFJldHVybnMgdGhlIEhTVkFDb2xvciBjbGFzc1xuICAgICAqXG4gICAgICogQHN0YXRpY1xuICAgICAqIEBleGFtcGxlIGxldCBjb2xvckRhdGEgPSBuZXcgQ29sb3JJdGVtLkhTVkFDb2xvcigzNjAsIDEwMCwgMTAwLCAxKTtcbiAgICAgKiBAcmV0dXJucyB7SFNWQUNvbG9yfVxuICAgICAqL1xuICAgIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgICAgcmV0dXJuIEhTVkFDb2xvcjtcbiAgICB9XG4gIH1dKTtcblxuICBmdW5jdGlvbiBDb2xvckl0ZW0oKSB7XG4gICAgdmFyIGNvbG9yID0gYXJndW1lbnRzLmxlbmd0aCA+IDAgJiYgYXJndW1lbnRzWzBdICE9PSB1bmRlZmluZWQgPyBhcmd1bWVudHNbMF0gOiBudWxsO1xuICAgIHZhciBmb3JtYXQgPSBhcmd1bWVudHMubGVuZ3RoID4gMSAmJiBhcmd1bWVudHNbMV0gIT09IHVuZGVmaW5lZCA/IGFyZ3VtZW50c1sxXSA6IG51bGw7XG5cbiAgICBfY2xhc3NDYWxsQ2hlY2sodGhpcywgQ29sb3JJdGVtKTtcblxuICAgIHRoaXMucmVwbGFjZShjb2xvciwgZm9ybWF0KTtcbiAgfVxuXG4gIC8qKlxuICAgKiBSZXBsYWNlcyB0aGUgaW50ZXJuYWwgUWl4Q29sb3Igb2JqZWN0IHdpdGggYSBuZXcgb25lLlxuICAgKiBUaGlzIGFsc28gcmVwbGFjZXMgdGhlIGludGVybmFsIG9yaWdpbmFsIGNvbG9yIGRhdGEuXG4gICAqXG4gICAqIEBwYXJhbSB7Q29sb3JJdGVtfEhTVkFDb2xvcnxRaXhDb2xvcnxTdHJpbmd8KnxudWxsfSBjb2xvciBDb2xvciBkYXRhIHRvIGJlIHBhcnNlZCAoaWYgbmVlZGVkKVxuICAgKiBAcGFyYW0ge1N0cmluZ3xudWxsfSBmb3JtYXQgQ29sb3IgbW9kZWwgdG8gY29udmVydCB0byBieSBkZWZhdWx0LiBTdXBwb3J0ZWQ6ICdyZ2InLCAnaHNsJywgJ2hleCcuXG4gICAqIEBleGFtcGxlIGNvbG9yLnJlcGxhY2UoJ3JnYigyNTUsMCwwKScsICdoc2wnKTtcbiAgICogQGV4YW1wbGUgY29sb3IucmVwbGFjZShoc3ZhQ29sb3JEYXRhKTtcbiAgICovXG5cblxuICBfY3JlYXRlQ2xhc3MoQ29sb3JJdGVtLCBbe1xuICAgIGtleTogJ3JlcGxhY2UnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiByZXBsYWNlKGNvbG9yKSB7XG4gICAgICB2YXIgZm9ybWF0ID0gYXJndW1lbnRzLmxlbmd0aCA+IDEgJiYgYXJndW1lbnRzWzFdICE9PSB1bmRlZmluZWQgPyBhcmd1bWVudHNbMV0gOiBudWxsO1xuXG4gICAgICBmb3JtYXQgPSBDb2xvckl0ZW0uc2FuaXRpemVGb3JtYXQoZm9ybWF0KTtcblxuICAgICAgLyoqXG4gICAgICAgKiBAdHlwZSB7e2NvbG9yOiAqLCBmb3JtYXQ6IFN0cmluZ319XG4gICAgICAgKiBAcHJpdmF0ZVxuICAgICAgICovXG4gICAgICB0aGlzLl9vcmlnaW5hbCA9IHtcbiAgICAgICAgY29sb3I6IGNvbG9yLFxuICAgICAgICBmb3JtYXQ6IGZvcm1hdCxcbiAgICAgICAgdmFsaWQ6IHRydWVcbiAgICAgIH07XG4gICAgICAvKipcbiAgICAgICAqIEB0eXBlIHtRaXhDb2xvcn1cbiAgICAgICAqIEBwcml2YXRlXG4gICAgICAgKi9cbiAgICAgIHRoaXMuX2NvbG9yID0gQ29sb3JJdGVtLnBhcnNlKGNvbG9yKTtcblxuICAgICAgaWYgKHRoaXMuX2NvbG9yID09PSBudWxsKSB7XG4gICAgICAgIHRoaXMuX2NvbG9yID0gKDAsIF9jb2xvcjIuZGVmYXVsdCkoKTtcbiAgICAgICAgdGhpcy5fb3JpZ2luYWwudmFsaWQgPSBmYWxzZTtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICAvKipcbiAgICAgICAqIEB0eXBlIHsqfHN0cmluZ31cbiAgICAgICAqIEBwcml2YXRlXG4gICAgICAgKi9cbiAgICAgIHRoaXMuX2Zvcm1hdCA9IGZvcm1hdCA/IGZvcm1hdCA6IENvbG9ySXRlbS5pc0hleChjb2xvcikgPyAnaGV4JyA6IHRoaXMuX2NvbG9yLm1vZGVsO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIFBhcnNlcyB0aGUgY29sb3IgcmV0dXJuaW5nIGEgUWl4IENvbG9yIG9iamVjdCBvciBudWxsIGlmIGNhbm5vdCBiZVxuICAgICAqIHBhcnNlZC5cbiAgICAgKlxuICAgICAqIEBwYXJhbSB7Q29sb3JJdGVtfEhTVkFDb2xvcnxRaXhDb2xvcnxTdHJpbmd8KnxudWxsfSBjb2xvciBDb2xvciBkYXRhXG4gICAgICogQGV4YW1wbGUgbGV0IHFDb2xvciA9IENvbG9ySXRlbS5wYXJzZSgncmdiKDI1NSwwLDApJyk7XG4gICAgICogQHN0YXRpY1xuICAgICAqIEByZXR1cm5zIHtRaXhDb2xvcnxudWxsfVxuICAgICAqL1xuXG4gIH0sIHtcbiAgICBrZXk6ICdpc1ZhbGlkJyxcblxuXG4gICAgLyoqXG4gICAgICogUmV0dXJucyB0cnVlIGlmIHRoZSBjb2xvciBpcyB2YWxpZCwgZmFsc2UgaWYgbm90LlxuICAgICAqXG4gICAgICogQHJldHVybnMge2Jvb2xlYW59XG4gICAgICovXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGlzVmFsaWQoKSB7XG4gICAgICByZXR1cm4gdGhpcy5fb3JpZ2luYWwudmFsaWQgPT09IHRydWU7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogSHVlIHZhbHVlIGZyb20gMCB0byAzNjBcbiAgICAgKlxuICAgICAqIEByZXR1cm5zIHtpbnR9XG4gICAgICovXG5cbiAgfSwge1xuICAgIGtleTogJ3NldEh1ZVJhdGlvJyxcblxuXG4gICAgLyoqXG4gICAgICogU2V0cyB0aGUgaHVlIHJhdGlvLCB3aGVyZSAxLjAgaXMgMCwgMC41IGlzIDE4MCBhbmQgMC4wIGlzIDM2MC5cbiAgICAgKlxuICAgICAqIEBpZ25vcmVcbiAgICAgKiBAcGFyYW0ge251bWJlcn0gaCBSYXRpbyBmcm9tIDEuMCB0byAwLjBcbiAgICAgKi9cbiAgICB2YWx1ZTogZnVuY3Rpb24gc2V0SHVlUmF0aW8oaCkge1xuICAgICAgdGhpcy5odWUgPSAoMSAtIGgpICogMzYwO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIFNldHMgdGhlIHNhdHVyYXRpb24gdmFsdWVcbiAgICAgKlxuICAgICAqIEBwYXJhbSB7aW50fSB2YWx1ZSBJbnRlZ2VyIGZyb20gMCB0byAxMDBcbiAgICAgKi9cblxuICB9LCB7XG4gICAga2V5OiAnc2V0U2F0dXJhdGlvblJhdGlvJyxcblxuXG4gICAgLyoqXG4gICAgICogU2V0cyB0aGUgc2F0dXJhdGlvbiByYXRpbywgd2hlcmUgMS4wIGlzIDEwMCBhbmQgMC4wIGlzIDAuXG4gICAgICpcbiAgICAgKiBAaWdub3JlXG4gICAgICogQHBhcmFtIHtudW1iZXJ9IHMgUmF0aW8gZnJvbSAwLjAgdG8gMS4wXG4gICAgICovXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHNldFNhdHVyYXRpb25SYXRpbyhzKSB7XG4gICAgICB0aGlzLnNhdHVyYXRpb24gPSBzICogMTAwO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIFNldHMgdGhlICd2YWx1ZScgY2hhbm5lbCB2YWx1ZVxuICAgICAqXG4gICAgICogQHBhcmFtIHtpbnR9IHZhbHVlIEludGVnZXIgZnJvbSAwIHRvIDEwMFxuICAgICAqL1xuXG4gIH0sIHtcbiAgICBrZXk6ICdzZXRWYWx1ZVJhdGlvJyxcblxuXG4gICAgLyoqXG4gICAgICogU2V0cyB0aGUgdmFsdWUgcmF0aW8sIHdoZXJlIDEuMCBpcyAwIGFuZCAwLjAgaXMgMTAwLlxuICAgICAqXG4gICAgICogQGlnbm9yZVxuICAgICAqIEBwYXJhbSB7bnVtYmVyfSB2IFJhdGlvIGZyb20gMS4wIHRvIDAuMFxuICAgICAqL1xuICAgIHZhbHVlOiBmdW5jdGlvbiBzZXRWYWx1ZVJhdGlvKHYpIHtcbiAgICAgIHRoaXMudmFsdWUgPSAoMSAtIHYpICogMTAwO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIFNldHMgdGhlIGFscGhhIHZhbHVlLiBJdCB3aWxsIGJlIHJvdW5kZWQgdG8gMiBkZWNpbWFscy5cbiAgICAgKlxuICAgICAqIEBwYXJhbSB7aW50fSB2YWx1ZSBGbG9hdCBmcm9tIDAuMCB0byAxLjBcbiAgICAgKi9cblxuICB9LCB7XG4gICAga2V5OiAnc2V0QWxwaGFSYXRpbycsXG5cblxuICAgIC8qKlxuICAgICAqIFNldHMgdGhlIGFscGhhIHJhdGlvLCB3aGVyZSAxLjAgaXMgMC4wIGFuZCAwLjAgaXMgMS4wLlxuICAgICAqXG4gICAgICogQGlnbm9yZVxuICAgICAqIEBwYXJhbSB7bnVtYmVyfSBhIFJhdGlvIGZyb20gMS4wIHRvIDAuMFxuICAgICAqL1xuICAgIHZhbHVlOiBmdW5jdGlvbiBzZXRBbHBoYVJhdGlvKGEpIHtcbiAgICAgIHRoaXMuYWxwaGEgPSAxIC0gYTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBTZXRzIHRoZSBkZWZhdWx0IGNvbG9yIGZvcm1hdFxuICAgICAqXG4gICAgICogQHBhcmFtIHtTdHJpbmd9IHZhbHVlIFN1cHBvcnRlZDogJ3JnYicsICdoc2wnLCAnaGV4J1xuICAgICAqL1xuXG4gIH0sIHtcbiAgICBrZXk6ICdpc0Rlc2F0dXJhdGVkJyxcblxuXG4gICAgLyoqXG4gICAgICogUmV0dXJucyB0cnVlIGlmIHRoZSBzYXR1cmF0aW9uIHZhbHVlIGlzIHplcm8sIGZhbHNlIG90aGVyd2lzZVxuICAgICAqXG4gICAgICogQHJldHVybnMge2Jvb2xlYW59XG4gICAgICovXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGlzRGVzYXR1cmF0ZWQoKSB7XG4gICAgICByZXR1cm4gdGhpcy5zYXR1cmF0aW9uID09PSAwO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIFJldHVybnMgdHJ1ZSBpZiB0aGUgYWxwaGEgdmFsdWUgaXMgemVybywgZmFsc2Ugb3RoZXJ3aXNlXG4gICAgICpcbiAgICAgKiBAcmV0dXJucyB7Ym9vbGVhbn1cbiAgICAgKi9cblxuICB9LCB7XG4gICAga2V5OiAnaXNUcmFuc3BhcmVudCcsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGlzVHJhbnNwYXJlbnQoKSB7XG4gICAgICByZXR1cm4gdGhpcy5hbHBoYSA9PT0gMDtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBSZXR1cm5zIHRydWUgaWYgdGhlIGFscGhhIHZhbHVlIGlzIG51bWVyaWMgYW5kIGxlc3MgdGhhbiAxLCBmYWxzZSBvdGhlcndpc2VcbiAgICAgKlxuICAgICAqIEByZXR1cm5zIHtib29sZWFufVxuICAgICAqL1xuXG4gIH0sIHtcbiAgICBrZXk6ICdoYXNUcmFuc3BhcmVuY3knLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBoYXNUcmFuc3BhcmVuY3koKSB7XG4gICAgICByZXR1cm4gdGhpcy5oYXNBbHBoYSgpICYmIHRoaXMuYWxwaGEgPCAxO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIFJldHVybnMgdHJ1ZSBpZiB0aGUgYWxwaGEgdmFsdWUgaXMgbnVtZXJpYywgZmFsc2Ugb3RoZXJ3aXNlXG4gICAgICpcbiAgICAgKiBAcmV0dXJucyB7Ym9vbGVhbn1cbiAgICAgKi9cblxuICB9LCB7XG4gICAga2V5OiAnaGFzQWxwaGEnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBoYXNBbHBoYSgpIHtcbiAgICAgIHJldHVybiAhaXNOYU4odGhpcy5hbHBoYSk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogUmV0dXJucyBhIG5ldyBIU1ZBQ29sb3Igb2JqZWN0LCBiYXNlZCBvbiB0aGUgY3VycmVudCBjb2xvclxuICAgICAqXG4gICAgICogQHJldHVybnMge0hTVkFDb2xvcn1cbiAgICAgKi9cblxuICB9LCB7XG4gICAga2V5OiAndG9PYmplY3QnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiB0b09iamVjdCgpIHtcbiAgICAgIHJldHVybiBuZXcgSFNWQUNvbG9yKHRoaXMuaHVlLCB0aGlzLnNhdHVyYXRpb24sIHRoaXMudmFsdWUsIHRoaXMuYWxwaGEpO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEFsaWFzIG9mIHRvT2JqZWN0KClcbiAgICAgKlxuICAgICAqIEByZXR1cm5zIHtIU1ZBQ29sb3J9XG4gICAgICovXG5cbiAgfSwge1xuICAgIGtleTogJ3RvSHN2YScsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHRvSHN2YSgpIHtcbiAgICAgIHJldHVybiB0aGlzLnRvT2JqZWN0KCk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogUmV0dXJucyBhIG5ldyBIU1ZBQ29sb3Igb2JqZWN0IHdpdGggdGhlIHJhdGlvIHZhbHVlcyAoZnJvbSAwLjAgdG8gMS4wKSxcbiAgICAgKiBiYXNlZCBvbiB0aGUgY3VycmVudCBjb2xvci5cbiAgICAgKlxuICAgICAqIEBpZ25vcmVcbiAgICAgKiBAcmV0dXJucyB7SFNWQUNvbG9yfVxuICAgICAqL1xuXG4gIH0sIHtcbiAgICBrZXk6ICd0b0hzdmFSYXRpbycsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHRvSHN2YVJhdGlvKCkge1xuICAgICAgcmV0dXJuIG5ldyBIU1ZBQ29sb3IodGhpcy5odWUgLyAzNjAsIHRoaXMuc2F0dXJhdGlvbiAvIDEwMCwgdGhpcy52YWx1ZSAvIDEwMCwgdGhpcy5hbHBoYSk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogQ29udmVydHMgdGhlIGN1cnJlbnQgY29sb3IgdG8gaXRzIHN0cmluZyByZXByZXNlbnRhdGlvbixcbiAgICAgKiB1c2luZyB0aGUgaW50ZXJuYWwgZm9ybWF0IG9mIHRoaXMgaW5zdGFuY2UuXG4gICAgICpcbiAgICAgKiBAcmV0dXJucyB7U3RyaW5nfVxuICAgICAqL1xuXG4gIH0sIHtcbiAgICBrZXk6ICd0b1N0cmluZycsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHRvU3RyaW5nKCkge1xuICAgICAgcmV0dXJuIHRoaXMuc3RyaW5nKCk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogQ29udmVydHMgdGhlIGN1cnJlbnQgY29sb3IgdG8gaXRzIHN0cmluZyByZXByZXNlbnRhdGlvbixcbiAgICAgKiB1c2luZyB0aGUgZ2l2ZW4gZm9ybWF0LlxuICAgICAqXG4gICAgICogQHBhcmFtIHtTdHJpbmd8bnVsbH0gZm9ybWF0IEZvcm1hdCB0byBjb252ZXJ0IHRvLiBJZiBlbXB0eSBvciBudWxsLCB0aGUgaW50ZXJuYWwgZm9ybWF0IHdpbGwgYmUgdXNlZC5cbiAgICAgKiBAcmV0dXJucyB7U3RyaW5nfVxuICAgICAqL1xuXG4gIH0sIHtcbiAgICBrZXk6ICdzdHJpbmcnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBzdHJpbmcoKSB7XG4gICAgICB2YXIgZm9ybWF0ID0gYXJndW1lbnRzLmxlbmd0aCA+IDAgJiYgYXJndW1lbnRzWzBdICE9PSB1bmRlZmluZWQgPyBhcmd1bWVudHNbMF0gOiBudWxsO1xuXG4gICAgICBmb3JtYXQgPSBDb2xvckl0ZW0uc2FuaXRpemVGb3JtYXQoZm9ybWF0ID8gZm9ybWF0IDogdGhpcy5mb3JtYXQpO1xuXG4gICAgICBpZiAoIWZvcm1hdCkge1xuICAgICAgICByZXR1cm4gdGhpcy5fY29sb3Iucm91bmQoKS5zdHJpbmcoKTtcbiAgICAgIH1cblxuICAgICAgaWYgKHRoaXMuX2NvbG9yW2Zvcm1hdF0gPT09IHVuZGVmaW5lZCkge1xuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ1Vuc3VwcG9ydGVkIGNvbG9yIGZvcm1hdDogXFwnJyArIGZvcm1hdCArICdcXCcnKTtcbiAgICAgIH1cblxuICAgICAgdmFyIHN0ciA9IHRoaXMuX2NvbG9yW2Zvcm1hdF0oKTtcblxuICAgICAgcmV0dXJuIHN0ci5yb3VuZCA/IHN0ci5yb3VuZCgpLnN0cmluZygpIDogc3RyO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIFJldHVybnMgdHJ1ZSBpZiB0aGUgZ2l2ZW4gY29sb3IgdmFsdWVzIGVxdWFscyB0aGlzIG9uZSwgZmFsc2Ugb3RoZXJ3aXNlLlxuICAgICAqIFRoZSBmb3JtYXQgaXMgbm90IGNvbXBhcmVkLlxuICAgICAqIElmIGFueSBvZiB0aGUgY29sb3JzIGlzIGludmFsaWQsIHRoZSByZXN1bHQgd2lsbCBiZSBmYWxzZS5cbiAgICAgKlxuICAgICAqIEBwYXJhbSB7Q29sb3JJdGVtfEhTVkFDb2xvcnxRaXhDb2xvcnxTdHJpbmd8KnxudWxsfSBjb2xvciBDb2xvciBkYXRhXG4gICAgICpcbiAgICAgKiBAcmV0dXJucyB7Ym9vbGVhbn1cbiAgICAgKi9cblxuICB9LCB7XG4gICAga2V5OiAnZXF1YWxzJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gZXF1YWxzKGNvbG9yKSB7XG4gICAgICBjb2xvciA9IGNvbG9yIGluc3RhbmNlb2YgQ29sb3JJdGVtID8gY29sb3IgOiBuZXcgQ29sb3JJdGVtKGNvbG9yKTtcblxuICAgICAgaWYgKCFjb2xvci5pc1ZhbGlkKCkgfHwgIXRoaXMuaXNWYWxpZCgpKSB7XG4gICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIHRoaXMuaHVlID09PSBjb2xvci5odWUgJiYgdGhpcy5zYXR1cmF0aW9uID09PSBjb2xvci5zYXR1cmF0aW9uICYmIHRoaXMudmFsdWUgPT09IGNvbG9yLnZhbHVlICYmIHRoaXMuYWxwaGEgPT09IGNvbG9yLmFscGhhO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIENyZWF0ZXMgYSBjb3B5IG9mIHRoaXMgaW5zdGFuY2VcbiAgICAgKlxuICAgICAqIEByZXR1cm5zIHtDb2xvckl0ZW19XG4gICAgICovXG5cbiAgfSwge1xuICAgIGtleTogJ2dldENsb25lJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gZ2V0Q2xvbmUoKSB7XG4gICAgICByZXR1cm4gbmV3IENvbG9ySXRlbSh0aGlzLl9jb2xvciwgdGhpcy5mb3JtYXQpO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIENyZWF0ZXMgYSBjb3B5IG9mIHRoaXMgaW5zdGFuY2UsIG9ubHkgY29weWluZyB0aGUgaHVlIHZhbHVlLFxuICAgICAqIGFuZCBzZXR0aW5nIHRoZSBvdGhlcnMgdG8gaXRzIG1heCB2YWx1ZS5cbiAgICAgKlxuICAgICAqIEByZXR1cm5zIHtDb2xvckl0ZW19XG4gICAgICovXG5cbiAgfSwge1xuICAgIGtleTogJ2dldENsb25lSHVlT25seScsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGdldENsb25lSHVlT25seSgpIHtcbiAgICAgIHJldHVybiBuZXcgQ29sb3JJdGVtKFt0aGlzLmh1ZSwgMTAwLCAxMDAsIDFdLCB0aGlzLmZvcm1hdCk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogQ3JlYXRlcyBhIGNvcHkgb2YgdGhpcyBpbnN0YW5jZSBzZXR0aW5nIHRoZSBhbHBoYSB0byB0aGUgbWF4LlxuICAgICAqXG4gICAgICogQHJldHVybnMge0NvbG9ySXRlbX1cbiAgICAgKi9cblxuICB9LCB7XG4gICAga2V5OiAnZ2V0Q2xvbmVPcGFxdWUnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBnZXRDbG9uZU9wYXF1ZSgpIHtcbiAgICAgIHJldHVybiBuZXcgQ29sb3JJdGVtKHRoaXMuX2NvbG9yLmFscGhhKDEpLCB0aGlzLmZvcm1hdCk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogQ29udmVydHMgdGhlIGNvbG9yIHRvIGEgUkdCIHN0cmluZ1xuICAgICAqXG4gICAgICogQHJldHVybnMge1N0cmluZ31cbiAgICAgKi9cblxuICB9LCB7XG4gICAga2V5OiAndG9SZ2JTdHJpbmcnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiB0b1JnYlN0cmluZygpIHtcbiAgICAgIHJldHVybiB0aGlzLnN0cmluZygncmdiJyk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogQ29udmVydHMgdGhlIGNvbG9yIHRvIGEgSGV4YWRlY2ltYWwgc3RyaW5nXG4gICAgICpcbiAgICAgKiBAcmV0dXJucyB7U3RyaW5nfVxuICAgICAqL1xuXG4gIH0sIHtcbiAgICBrZXk6ICd0b0hleFN0cmluZycsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHRvSGV4U3RyaW5nKCkge1xuICAgICAgcmV0dXJuIHRoaXMuc3RyaW5nKCdoZXgnKTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBDb252ZXJ0cyB0aGUgY29sb3IgdG8gYSBIU0wgc3RyaW5nXG4gICAgICpcbiAgICAgKiBAcmV0dXJucyB7U3RyaW5nfVxuICAgICAqL1xuXG4gIH0sIHtcbiAgICBrZXk6ICd0b0hzbFN0cmluZycsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHRvSHNsU3RyaW5nKCkge1xuICAgICAgcmV0dXJuIHRoaXMuc3RyaW5nKCdoc2wnKTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBSZXR1cm5zIHRydWUgaWYgdGhlIGNvbG9yIGlzIGRhcmssIGZhbHNlIG90aGVyd2hpc2UuXG4gICAgICogVGhpcyBpcyB1c2VmdWwgdG8gZGVjaWRlIGEgdGV4dCBjb2xvci5cbiAgICAgKlxuICAgICAqIEByZXR1cm5zIHtib29sZWFufVxuICAgICAqL1xuXG4gIH0sIHtcbiAgICBrZXk6ICdpc0RhcmsnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBpc0RhcmsoKSB7XG4gICAgICByZXR1cm4gdGhpcy5fY29sb3IuaXNEYXJrKCk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogUmV0dXJucyB0cnVlIGlmIHRoZSBjb2xvciBpcyBsaWdodCwgZmFsc2Ugb3RoZXJ3aGlzZS5cbiAgICAgKiBUaGlzIGlzIHVzZWZ1bCB0byBkZWNpZGUgYSB0ZXh0IGNvbG9yLlxuICAgICAqXG4gICAgICogQHJldHVybnMge2Jvb2xlYW59XG4gICAgICovXG5cbiAgfSwge1xuICAgIGtleTogJ2lzTGlnaHQnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBpc0xpZ2h0KCkge1xuICAgICAgcmV0dXJuIHRoaXMuX2NvbG9yLmlzTGlnaHQoKTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBHZW5lcmF0ZXMgYSBsaXN0IG9mIGNvbG9ycyB1c2luZyB0aGUgZ2l2ZW4gaHVlLWJhc2VkIGZvcm11bGEgb3IgdGhlIGdpdmVuIGFycmF5IG9mIGh1ZSB2YWx1ZXMuXG4gICAgICogSHVlIGZvcm11bGFzIGNhbiBiZSBleHRlbmRlZCB1c2luZyBDb2xvckl0ZW0uY29sb3JGb3JtdWxhcyBzdGF0aWMgcHJvcGVydHkuXG4gICAgICpcbiAgICAgKiBAcGFyYW0ge1N0cmluZ3xOdW1iZXJbXX0gZm9ybXVsYSBFeGFtcGxlczogJ2NvbXBsZW1lbnRhcnknLCAndHJpYWQnLCAndGV0cmFkJywgJ3NwbGl0Y29tcGxlbWVudCcsIFsxODAsIDI3MF1cbiAgICAgKiBAZXhhbXBsZSBsZXQgY29sb3JzID0gY29sb3IuZ2VuZXJhdGUoJ3RyaWFkJyk7XG4gICAgICogQGV4YW1wbGUgbGV0IGNvbG9ycyA9IGNvbG9yLmdlbmVyYXRlKFs0NSwgODAsIDExMiwgMjAwXSk7XG4gICAgICogQHJldHVybnMge0NvbG9ySXRlbVtdfVxuICAgICAqL1xuXG4gIH0sIHtcbiAgICBrZXk6ICdnZW5lcmF0ZScsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGdlbmVyYXRlKGZvcm11bGEpIHtcbiAgICAgIHZhciBodWVzID0gW107XG5cbiAgICAgIGlmIChBcnJheS5pc0FycmF5KGZvcm11bGEpKSB7XG4gICAgICAgIGh1ZXMgPSBmb3JtdWxhO1xuICAgICAgfSBlbHNlIGlmICghQ29sb3JJdGVtLmNvbG9yRm9ybXVsYXMuaGFzT3duUHJvcGVydHkoZm9ybXVsYSkpIHtcbiAgICAgICAgdGhyb3cgbmV3IEVycm9yKCdObyBjb2xvciBmb3JtdWxhIGZvdW5kIHdpdGggdGhlIG5hbWUgXFwnJyArIGZvcm11bGEgKyAnXFwnLicpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgaHVlcyA9IENvbG9ySXRlbS5jb2xvckZvcm11bGFzW2Zvcm11bGFdO1xuICAgICAgfVxuXG4gICAgICB2YXIgY29sb3JzID0gW10sXG4gICAgICAgICAgbWFpbkNvbG9yID0gdGhpcy5fY29sb3IsXG4gICAgICAgICAgZm9ybWF0ID0gdGhpcy5mb3JtYXQ7XG5cbiAgICAgIGh1ZXMuZm9yRWFjaChmdW5jdGlvbiAoaHVlKSB7XG4gICAgICAgIHZhciBsZXZlbHMgPSBbaHVlID8gKG1haW5Db2xvci5odWUoKSArIGh1ZSkgJSAzNjAgOiBtYWluQ29sb3IuaHVlKCksIG1haW5Db2xvci5zYXR1cmF0aW9udigpLCBtYWluQ29sb3IudmFsdWUoKSwgbWFpbkNvbG9yLmFscGhhKCldO1xuXG4gICAgICAgIGNvbG9ycy5wdXNoKG5ldyBDb2xvckl0ZW0obGV2ZWxzLCBmb3JtYXQpKTtcbiAgICAgIH0pO1xuXG4gICAgICByZXR1cm4gY29sb3JzO1xuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ2h1ZScsXG4gICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgICByZXR1cm4gdGhpcy5fY29sb3IuaHVlKCk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogU2F0dXJhdGlvbiB2YWx1ZSBmcm9tIDAgdG8gMTAwXG4gICAgICpcbiAgICAgKiBAcmV0dXJucyB7aW50fVxuICAgICAqL1xuICAgICxcblxuXG4gICAgLyoqXG4gICAgICogU2V0cyB0aGUgaHVlIHZhbHVlXG4gICAgICpcbiAgICAgKiBAcGFyYW0ge2ludH0gdmFsdWUgSW50ZWdlciBmcm9tIDAgdG8gMzYwXG4gICAgICovXG4gICAgc2V0OiBmdW5jdGlvbiBzZXQodmFsdWUpIHtcbiAgICAgIHRoaXMuX2NvbG9yID0gdGhpcy5fY29sb3IuaHVlKHZhbHVlKTtcbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdzYXR1cmF0aW9uJyxcbiAgICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICAgIHJldHVybiB0aGlzLl9jb2xvci5zYXR1cmF0aW9udigpO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIFZhbHVlIGNoYW5uZWwgdmFsdWUgZnJvbSAwIHRvIDEwMFxuICAgICAqXG4gICAgICogQHJldHVybnMge2ludH1cbiAgICAgKi9cbiAgICAsXG4gICAgc2V0OiBmdW5jdGlvbiBzZXQodmFsdWUpIHtcbiAgICAgIHRoaXMuX2NvbG9yID0gdGhpcy5fY29sb3Iuc2F0dXJhdGlvbnYodmFsdWUpO1xuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ3ZhbHVlJyxcbiAgICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICAgIHJldHVybiB0aGlzLl9jb2xvci52YWx1ZSgpO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEFscGhhIHZhbHVlIGZyb20gMC4wIHRvIDEuMFxuICAgICAqXG4gICAgICogQHJldHVybnMge251bWJlcn1cbiAgICAgKi9cbiAgICAsXG4gICAgc2V0OiBmdW5jdGlvbiBzZXQodmFsdWUpIHtcbiAgICAgIHRoaXMuX2NvbG9yID0gdGhpcy5fY29sb3IudmFsdWUodmFsdWUpO1xuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ2FscGhhJyxcbiAgICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICAgIHZhciBhID0gdGhpcy5fY29sb3IuYWxwaGEoKTtcblxuICAgICAgcmV0dXJuIGlzTmFOKGEpID8gMSA6IGE7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogRGVmYXVsdCBjb2xvciBmb3JtYXQgdG8gY29udmVydCB0byB3aGVuIGNhbGxpbmcgdG9TdHJpbmcoKSBvciBzdHJpbmcoKVxuICAgICAqXG4gICAgICogQHJldHVybnMge1N0cmluZ30gJ3JnYicsICdoc2wnLCAnaGV4JyBvciAnJ1xuICAgICAqL1xuICAgICxcbiAgICBzZXQ6IGZ1bmN0aW9uIHNldCh2YWx1ZSkge1xuICAgICAgLy8gMiBkZWNpbWFscyBtYXhcbiAgICAgIHRoaXMuX2NvbG9yID0gdGhpcy5fY29sb3IuYWxwaGEoTWF0aC5yb3VuZCh2YWx1ZSAqIDEwMCkgLyAxMDApO1xuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ2Zvcm1hdCcsXG4gICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgICByZXR1cm4gdGhpcy5fZm9ybWF0ID8gdGhpcy5fZm9ybWF0IDogdGhpcy5fY29sb3IubW9kZWw7XG4gICAgfSxcbiAgICBzZXQ6IGZ1bmN0aW9uIHNldCh2YWx1ZSkge1xuICAgICAgdGhpcy5fZm9ybWF0ID0gQ29sb3JJdGVtLnNhbml0aXplRm9ybWF0KHZhbHVlKTtcbiAgICB9XG4gIH1dLCBbe1xuICAgIGtleTogJ3BhcnNlJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gcGFyc2UoY29sb3IpIHtcbiAgICAgIGlmIChjb2xvciBpbnN0YW5jZW9mIF9jb2xvcjIuZGVmYXVsdCkge1xuICAgICAgICByZXR1cm4gY29sb3I7XG4gICAgICB9XG5cbiAgICAgIGlmIChjb2xvciBpbnN0YW5jZW9mIENvbG9ySXRlbSkge1xuICAgICAgICByZXR1cm4gY29sb3IuX2NvbG9yO1xuICAgICAgfVxuXG4gICAgICB2YXIgZm9ybWF0ID0gbnVsbDtcblxuICAgICAgaWYgKGNvbG9yIGluc3RhbmNlb2YgSFNWQUNvbG9yKSB7XG4gICAgICAgIGNvbG9yID0gW2NvbG9yLmgsIGNvbG9yLnMsIGNvbG9yLnYsIGlzTmFOKGNvbG9yLmEpID8gMSA6IGNvbG9yLmFdO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgY29sb3IgPSBDb2xvckl0ZW0uc2FuaXRpemVTdHJpbmcoY29sb3IpO1xuICAgICAgfVxuXG4gICAgICBpZiAoY29sb3IgPT09IG51bGwpIHtcbiAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgICB9XG5cbiAgICAgIGlmIChBcnJheS5pc0FycmF5KGNvbG9yKSkge1xuICAgICAgICBmb3JtYXQgPSAnaHN2JztcbiAgICAgIH1cblxuICAgICAgdHJ5IHtcbiAgICAgICAgcmV0dXJuICgwLCBfY29sb3IyLmRlZmF1bHQpKGNvbG9yLCBmb3JtYXQpO1xuICAgICAgfSBjYXRjaCAoZSkge1xuICAgICAgICByZXR1cm4gbnVsbDtcbiAgICAgIH1cbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBTYW5pdGl6ZXMgYSBjb2xvciBzdHJpbmcsIGFkZGluZyBtaXNzaW5nIGhhc2ggdG8gaGV4YWRlY2ltYWwgY29sb3JzXG4gICAgICogYW5kIGNvbnZlcnRpbmcgJ3RyYW5zcGFyZW50JyB0byBhIGNvbG9yIGNvZGUuXG4gICAgICpcbiAgICAgKiBAcGFyYW0ge1N0cmluZ3wqfSBzdHIgQ29sb3Igc3RyaW5nXG4gICAgICogQGV4YW1wbGUgbGV0IGNvbG9yU3RyID0gQ29sb3JJdGVtLnNhbml0aXplU3RyaW5nKCdmZmFhMDAnKTtcbiAgICAgKiBAc3RhdGljXG4gICAgICogQHJldHVybnMge1N0cmluZ3wqfVxuICAgICAqL1xuXG4gIH0sIHtcbiAgICBrZXk6ICdzYW5pdGl6ZVN0cmluZycsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHNhbml0aXplU3RyaW5nKHN0cikge1xuICAgICAgaWYgKCEodHlwZW9mIHN0ciA9PT0gJ3N0cmluZycgfHwgc3RyIGluc3RhbmNlb2YgU3RyaW5nKSkge1xuICAgICAgICByZXR1cm4gc3RyO1xuICAgICAgfVxuXG4gICAgICBpZiAoc3RyLm1hdGNoKC9eWzAtOWEtZl17Mix9JC9pKSkge1xuICAgICAgICByZXR1cm4gJyMnICsgc3RyO1xuICAgICAgfVxuXG4gICAgICBpZiAoc3RyLnRvTG93ZXJDYXNlKCkgPT09ICd0cmFuc3BhcmVudCcpIHtcbiAgICAgICAgcmV0dXJuICcjRkZGRkZGMDAnO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gc3RyO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIERldGVjdHMgaWYgYSB2YWx1ZSBpcyBhIHN0cmluZyBhbmQgYSBjb2xvciBpbiBoZXhhZGVjaW1hbCBmb3JtYXQgKGluIGFueSB2YXJpYW50KS5cbiAgICAgKlxuICAgICAqIEBwYXJhbSB7U3RyaW5nfSBzdHJcbiAgICAgKiBAZXhhbXBsZSBDb2xvckl0ZW0uaXNIZXgoJ3JnYmEoMCwwLDApJyk7IC8vIGZhbHNlXG4gICAgICogQGV4YW1wbGUgQ29sb3JJdGVtLmlzSGV4KCdmZmFhMDAnKTsgLy8gdHJ1ZVxuICAgICAqIEBleGFtcGxlIENvbG9ySXRlbS5pc0hleCgnI2ZmYWEwMCcpOyAvLyB0cnVlXG4gICAgICogQHN0YXRpY1xuICAgICAqIEByZXR1cm5zIHtib29sZWFufVxuICAgICAqL1xuXG4gIH0sIHtcbiAgICBrZXk6ICdpc0hleCcsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGlzSGV4KHN0cikge1xuICAgICAgaWYgKCEodHlwZW9mIHN0ciA9PT0gJ3N0cmluZycgfHwgc3RyIGluc3RhbmNlb2YgU3RyaW5nKSkge1xuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICB9XG5cbiAgICAgIHJldHVybiAhIXN0ci5tYXRjaCgvXiM/WzAtOWEtZl17Mix9JC9pKTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBTYW5pdGl6ZXMgYSBjb2xvciBmb3JtYXQgdG8gb25lIHN1cHBvcnRlZCBieSB3ZWIgYnJvd3NlcnMuXG4gICAgICogUmV0dXJucyBhbiBlbXB0eSBzdHJpbmcgb2YgdGhlIGZvcm1hdCBjYW4ndCBiZSByZWNvZ25pc2VkLlxuICAgICAqXG4gICAgICogQHBhcmFtIHtTdHJpbmd8Kn0gZm9ybWF0XG4gICAgICogQGV4YW1wbGUgQ29sb3JJdGVtLnNhbml0aXplRm9ybWF0KCdyZ2JhJyk7IC8vICdyZ2InXG4gICAgICogQGV4YW1wbGUgQ29sb3JJdGVtLmlzSGV4KCdoZXg4Jyk7IC8vICdoZXgnXG4gICAgICogQGV4YW1wbGUgQ29sb3JJdGVtLmlzSGV4KCdpbnZhbGlkJyk7IC8vICcnXG4gICAgICogQHN0YXRpY1xuICAgICAqIEByZXR1cm5zIHtTdHJpbmd9ICdyZ2InLCAnaHNsJywgJ2hleCcgb3IgJycuXG4gICAgICovXG5cbiAgfSwge1xuICAgIGtleTogJ3Nhbml0aXplRm9ybWF0JyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gc2FuaXRpemVGb3JtYXQoZm9ybWF0KSB7XG4gICAgICBzd2l0Y2ggKGZvcm1hdCkge1xuICAgICAgICBjYXNlICdoZXgnOlxuICAgICAgICBjYXNlICdoZXgzJzpcbiAgICAgICAgY2FzZSAnaGV4NCc6XG4gICAgICAgIGNhc2UgJ2hleDYnOlxuICAgICAgICBjYXNlICdoZXg4JzpcbiAgICAgICAgICByZXR1cm4gJ2hleCc7XG4gICAgICAgIGNhc2UgJ3JnYic6XG4gICAgICAgIGNhc2UgJ3JnYmEnOlxuICAgICAgICBjYXNlICdrZXl3b3JkJzpcbiAgICAgICAgY2FzZSAnbmFtZSc6XG4gICAgICAgICAgcmV0dXJuICdyZ2InO1xuICAgICAgICBjYXNlICdoc2wnOlxuICAgICAgICBjYXNlICdoc2xhJzpcbiAgICAgICAgY2FzZSAnaHN2JzpcbiAgICAgICAgY2FzZSAnaHN2YSc6XG4gICAgICAgIGNhc2UgJ2h3Yic6IC8vIEhXQiB0aGlzIGlzIHN1cHBvcnRlZCBieSBRaXggQ29sb3IsIGJ1dCBub3QgYnkgYnJvd3NlcnNcbiAgICAgICAgY2FzZSAnaHdiYSc6XG4gICAgICAgICAgcmV0dXJuICdoc2wnO1xuICAgICAgICBkZWZhdWx0OlxuICAgICAgICAgIHJldHVybiAnJztcbiAgICAgIH1cbiAgICB9XG4gIH1dKTtcblxuICByZXR1cm4gQ29sb3JJdGVtO1xufSgpO1xuXG4vKipcbiAqIExpc3Qgb2YgaHVlLWJhc2VkIGNvbG9yIGZvcm11bGFzIHVzZWQgYnkgQ29sb3JJdGVtLnByb3RvdHlwZS5nZW5lcmF0ZSgpXG4gKlxuICogQHN0YXRpY1xuICogQHR5cGUge3tjb21wbGVtZW50YXJ5OiBudW1iZXJbXSwgdHJpYWQ6IG51bWJlcltdLCB0ZXRyYWQ6IG51bWJlcltdLCBzcGxpdGNvbXBsZW1lbnQ6IG51bWJlcltdfX1cbiAqL1xuXG5cbkNvbG9ySXRlbS5jb2xvckZvcm11bGFzID0ge1xuICBjb21wbGVtZW50YXJ5OiBbMTgwXSxcbiAgdHJpYWQ6IFswLCAxMjAsIDI0MF0sXG4gIHRldHJhZDogWzAsIDkwLCAxODAsIDI3MF0sXG4gIHNwbGl0Y29tcGxlbWVudDogWzAsIDcyLCAyMTZdXG59O1xuXG5leHBvcnRzLmRlZmF1bHQgPSBDb2xvckl0ZW07XG5leHBvcnRzLkhTVkFDb2xvciA9IEhTVkFDb2xvcjtcbmV4cG9ydHMuQ29sb3JJdGVtID0gQ29sb3JJdGVtO1xuXG4vKioqLyB9KSxcbi8qIDMgKi9cbi8qKiovIChmdW5jdGlvbihtb2R1bGUsIGV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pIHtcblxuXCJ1c2Ugc3RyaWN0XCI7XG5cbi8qKlxuICogQG1vZHVsZVxuICovXG5cbi8vIGFkanVzdCB0aGVzZSB2YWx1ZXMgYWNjb3JkaW5nbHkgdG8gdGhlIHNhc3MgdmFyc1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xudmFyIHNhc3NWYXJzID0ge1xuICAnYmFyX3NpemVfc2hvcnQnOiAxNixcbiAgJ2Jhc2VfbWFyZ2luJzogNixcbiAgJ2NvbHVtbnMnOiA2XG59O1xuXG52YXIgc2xpZGVyU2l6ZSA9IHNhc3NWYXJzLmJhcl9zaXplX3Nob3J0ICogc2Fzc1ZhcnMuY29sdW1ucyArIHNhc3NWYXJzLmJhc2VfbWFyZ2luICogKHNhc3NWYXJzLmNvbHVtbnMgLSAxKTtcblxuLyoqXG4gKiBDb2xvcnBpY2tlciBkZWZhdWx0IG9wdGlvbnNcbiAqL1xuZXhwb3J0cy5kZWZhdWx0ID0ge1xuICAvKipcbiAgICogQ3VzdG9tIGNsYXNzIHRvIGJlIGFkZGVkIHRvIHRoZSBgLmNvbG9ycGlja2VyLWVsZW1lbnRgIGVsZW1lbnRcbiAgICpcbiAgICogQHR5cGUge1N0cmluZ3xudWxsfVxuICAgKiBAZGVmYXVsdCBudWxsXG4gICAqL1xuICBjdXN0b21DbGFzczogbnVsbCxcbiAgLyoqXG4gICAqIFNldHMgYSBpbml0aWFsIGNvbG9yLCBpZ25vcmluZyB0aGUgb25lIGZyb20gdGhlIGVsZW1lbnQvaW5wdXQgdmFsdWUgb3IgdGhlIGRhdGEtY29sb3IgYXR0cmlidXRlLlxuICAgKlxuICAgKiBAdHlwZSB7KFN0cmluZ3xDb2xvckl0ZW18Ym9vbGVhbil9XG4gICAqIEBkZWZhdWx0IGZhbHNlXG4gICAqL1xuICBjb2xvcjogZmFsc2UsXG4gIC8qKlxuICAgKiBGYWxsYmFjayBjb2xvciB0byB1c2Ugd2hlbiB0aGUgZ2l2ZW4gY29sb3IgaXMgaW52YWxpZC5cbiAgICogSWYgZmFsc2UsIHRoZSBsYXRlc3QgdmFsaWQgY29sb3Igd2lsbCBiZSB1c2VkIGFzIGEgZmFsbGJhY2suXG4gICAqXG4gICAqIEB0eXBlIHtTdHJpbmd8Q29sb3JJdGVtfGJvb2xlYW59XG4gICAqIEBkZWZhdWx0IGZhbHNlXG4gICAqL1xuICBmYWxsYmFja0NvbG9yOiBmYWxzZSxcbiAgLyoqXG4gICAqIEZvcmNlcyBhbiBzcGVjaWZpYyBjb2xvciBmb3JtYXQuIElmICdhdXRvJywgaXQgd2lsbCBiZSBhdXRvbWF0aWNhbGx5IGRldGVjdGVkIHRoZSBmaXJzdCB0aW1lIG9ubHksXG4gICAqIGJ1dCBpZiBudWxsIGl0IHdpbGwgYmUgYWx3YXlzIHJlY2FsY3VsYXRlZC5cbiAgICpcbiAgICogTm90ZSB0aGF0IHRoZSBlbmRpbmcgJ2EnIG9mIHRoZSBmb3JtYXQgbWVhbmluZyBcImFscGhhXCIgaGFzIGN1cnJlbnRseSBubyBlZmZlY3QsIG1lYW5pbmcgdGhhdCByZ2IgaXMgdGhlIHNhbWUgYXNcbiAgICogcmdiYSBleGNlcHRpbmcgaWYgdGhlIGFscGhhIGNoYW5uZWwgaXMgZGlzYWJsZWQgKHNlZSB1c2VBbHBoYSkuXG4gICAqXG4gICAqIEB0eXBlIHsoJ3JnYid8J2hleCd8J2hzbCd8J2F1dG8nfG51bGwpfVxuICAgKiBAZGVmYXVsdCAnYXV0bydcbiAgICovXG4gIGZvcm1hdDogJ2F1dG8nLFxuICAvKipcbiAgICogSG9yaXpvbnRhbCBtb2RlIGxheW91dC5cbiAgICpcbiAgICogSWYgdHJ1ZSwgdGhlIGh1ZSBhbmQgYWxwaGEgY2hhbm5lbCBiYXJzIHdpbGwgYmUgcmVuZGVyZWQgaG9yaXpvbnRhbGx5LCBhYm92ZSB0aGUgc2F0dXJhdGlvbiBzZWxlY3Rvci5cbiAgICpcbiAgICogQHR5cGUge2Jvb2xlYW59XG4gICAqIEBkZWZhdWx0IGZhbHNlXG4gICAqL1xuICBob3Jpem9udGFsOiBmYWxzZSxcbiAgLyoqXG4gICAqIEZvcmNlcyB0byBzaG93IHRoZSBjb2xvcnBpY2tlciBhcyBhbiBpbmxpbmUgZWxlbWVudC5cbiAgICpcbiAgICogTm90ZSB0aGF0IGlmIHRoZXJlIGlzIG5vIGNvbnRhaW5lciBzcGVjaWZpZWQsIHRoZSBpbmxpbmUgZWxlbWVudFxuICAgKiB3aWxsIGJlIGFkZGVkIHRvIHRoZSBib2R5LCBzbyB5b3UgbWF5IHdhbnQgdG8gc2V0IHRoZSBjb250YWluZXIgb3B0aW9uLlxuICAgKlxuICAgKiBAdHlwZSB7Ym9vbGVhbn1cbiAgICogQGRlZmF1bHQgZmFsc2VcbiAgICovXG4gIGlubGluZTogZmFsc2UsXG4gIC8qKlxuICAgKiBDb250YWluZXIgd2hlcmUgdGhlIGNvbG9ycGlja2VyIGlzIGFwcGVuZGVkIHRvIGluIHRoZSBET00uXG4gICAqXG4gICAqIElmIGlzIGEgc3RyaW5nIChDU1Mgc2VsZWN0b3IpLCB0aGUgY29sb3JwaWNrZXIgd2lsbCBiZSBwbGFjZWQgaW5zaWRlIHRoaXMgY29udGFpbmVyLlxuICAgKiBJZiB0cnVlLCB0aGUgYC5jb2xvcnBpY2tlci1lbGVtZW50YCBlbGVtZW50IGl0c2VsZiB3aWxsIGJlIHVzZWQgYXMgdGhlIGNvbnRhaW5lci5cbiAgICogSWYgZmFsc2UsIHRoZSBkb2N1bWVudCBib2R5IGlzIHVzZWQgYXMgdGhlIGNvbnRhaW5lciwgdW5sZXNzIGl0IGlzIGEgcG9wb3ZlciAoaW4gdGhpcyBjYXNlIGl0IGlzIGFwcGVuZGVkIHRvIHRoZVxuICAgKiBwb3BvdmVyIGJvZHkgaW5zdGVhZCkuXG4gICAqXG4gICAqIEB0eXBlIHtTdHJpbmd8Ym9vbGVhbn1cbiAgICogQGRlZmF1bHQgZmFsc2VcbiAgICovXG4gIGNvbnRhaW5lcjogZmFsc2UsXG4gIC8qKlxuICAgKiBCb290c3RyYXAgUG9wb3ZlciBvcHRpb25zLlxuICAgKiBUaGUgdHJpZ2dlciwgY29udGVudCBhbmQgaHRtbCBvcHRpb25zIGFyZSBhbHdheXMgaWdub3JlZC5cbiAgICpcbiAgICogQHR5cGUge2Jvb2xlYW59XG4gICAqIEBkZWZhdWx0IE9iamVjdFxuICAgKi9cbiAgcG9wb3Zlcjoge1xuICAgIGFuaW1hdGlvbjogdHJ1ZSxcbiAgICBwbGFjZW1lbnQ6ICdib3R0b20nLFxuICAgIGZhbGxiYWNrUGxhY2VtZW50OiAnZmxpcCdcbiAgfSxcbiAgLyoqXG4gICAqIElmIHRydWUsIGxvYWRzIHRoZSAnZGVidWdnZXInIGV4dGVuc2lvbiBhdXRvbWF0aWNhbGx5LCB3aGljaCBsb2dzIHRoZSBldmVudHMgaW4gdGhlIGNvbnNvbGVcbiAgICogQHR5cGUge2Jvb2xlYW59XG4gICAqIEBkZWZhdWx0IGZhbHNlXG4gICAqL1xuICBkZWJ1ZzogZmFsc2UsXG4gIC8qKlxuICAgKiBDaGlsZCBDU1Mgc2VsZWN0b3IgZm9yIHRoZSBjb2xvcnBpY2tlciBpbnB1dC5cbiAgICpcbiAgICogQHR5cGUge1N0cmluZ31cbiAgICogQGRlZmF1bHQgJ2lucHV0J1xuICAgKi9cbiAgaW5wdXQ6ICdpbnB1dCcsXG4gIC8qKlxuICAgKiBDaGlsZCBDU1Mgc2VsZWN0b3IgZm9yIHRoZSBjb2xvcnBpY2tlciBhZGRvbi5cbiAgICogSWYgaXQgZXhpc3RzLCB0aGUgY2hpbGQgPGk+IGVsZW1lbnQgYmFja2dyb3VuZCB3aWxsIGJlIGNoYW5nZWQgb24gY29sb3IgY2hhbmdlLlxuICAgKlxuICAgKiBAdHlwZSB7U3RyaW5nfVxuICAgKiBAZGVmYXVsdCAnLmNvbG9ycGlja2VyLXRyaWdnZXIsIC5jb2xvcnBpY2tlci1pbnB1dC1hZGRvbidcbiAgICovXG4gIGFkZG9uOiAnLmNvbG9ycGlja2VyLWlucHV0LWFkZG9uJyxcbiAgLyoqXG4gICAqIElmIHRydWUsIHRoZSBpbnB1dCBjb250ZW50IHdpbGwgYmUgcmVwbGFjZWQgYWx3YXlzIHdpdGggYSB2YWxpZCBjb2xvcixcbiAgICogaWYgZmFsc2UsIHRoZSBpbnZhbGlkIGNvbG9yIHdpbGwgYmUgbGVmdCBpbiB0aGUgaW5wdXQsXG4gICAqICAgd2hpbGUgdGhlIGludGVybmFsIGNvbG9yIG9iamVjdCB3aWxsIHN0aWxsIHJlc29sdmUgaW50byBhIHZhbGlkIG9uZS5cbiAgICpcbiAgICogQHR5cGUge2Jvb2xlYW59XG4gICAqIEBkZWZhdWx0IHRydWVcbiAgICovXG4gIGF1dG9JbnB1dEZhbGxiYWNrOiB0cnVlLFxuICAvKipcbiAgICogSWYgdHJ1ZSBhIGhhc2ggd2lsbCBiZSBwcmVwZW5kZWQgdG8gaGV4YWRlY2ltYWwgY29sb3JzLlxuICAgKiBJZiBmYWxzZSwgdGhlIGhhc2ggd2lsbCBiZSByZW1vdmVkLlxuICAgKiBUaGlzIG9ubHkgYWZmZWN0cyB0aGUgaW5wdXQgdmFsdWVzIGluIGhleGFkZWNpbWFsIGZvcm1hdC5cbiAgICpcbiAgICogQHR5cGUge2Jvb2xlYW59XG4gICAqIEBkZWZhdWx0IHRydWVcbiAgICovXG4gIHVzZUhhc2hQcmVmaXg6IHRydWUsXG4gIC8qKlxuICAgKiBJZiB0cnVlLCB0aGUgYWxwaGEgY2hhbm5lbCBiYXIgd2lsbCBiZSBkaXNwbGF5ZWQgbm8gbWF0dGVyIHdoYXQuXG4gICAqXG4gICAqIElmIGZhbHNlLCBpdCB3aWxsIGJlIGFsd2F5cyBoaWRkZW4gYW5kIGFscGhhIGNoYW5uZWwgd2lsbCBiZSBkaXNhYmxlZCBhbHNvIHByb2dyYW1tYXRpY2FsbHksIG1lYW5pbmcgdGhhdFxuICAgKiB0aGUgc2VsZWN0ZWQgb3IgdHlwZWQgY29sb3Igd2lsbCBiZSBhbHdheXMgb3BhcXVlLlxuICAgKlxuICAgKiBJZiBudWxsLCB0aGUgYWxwaGEgY2hhbm5lbCB3aWxsIGJlIGF1dG9tYXRpY2FsbHkgZGlzYWJsZWQvZW5hYmxlZCBkZXBlbmRpbmcgaWYgdGhlIGluaXRpYWwgY29sb3IgZm9ybWF0IHN1cHBvcnRzXG4gICAqIGFscGhhIG9yIG5vdC5cbiAgICpcbiAgICogQHR5cGUge2Jvb2xlYW59XG4gICAqIEBkZWZhdWx0IHRydWVcbiAgICovXG4gIHVzZUFscGhhOiB0cnVlLFxuICAvKipcbiAgICogQ29sb3JwaWNrZXIgd2lkZ2V0IHRlbXBsYXRlXG4gICAqIEB0eXBlIHtTdHJpbmd9XG4gICAqIEBleGFtcGxlXG4gICAqIDwhLS0gVGhpcyBpcyB0aGUgZGVmYXVsdCB0ZW1wbGF0ZTogLS0+XG4gICAqIDxkaXYgY2xhc3M9XCJjb2xvcnBpY2tlclwiPlxuICAgKiAgIDxkaXYgY2xhc3M9XCJjb2xvcnBpY2tlci1zYXR1cmF0aW9uXCI+PGkgY2xhc3M9XCJjb2xvcnBpY2tlci1ndWlkZVwiPjwvaT48L2Rpdj5cbiAgICogICA8ZGl2IGNsYXNzPVwiY29sb3JwaWNrZXItaHVlXCI+PGkgY2xhc3M9XCJjb2xvcnBpY2tlci1ndWlkZVwiPjwvaT48L2Rpdj5cbiAgICogICA8ZGl2IGNsYXNzPVwiY29sb3JwaWNrZXItYWxwaGFcIj5cbiAgICogICAgIDxkaXYgY2xhc3M9XCJjb2xvcnBpY2tlci1hbHBoYS1jb2xvclwiPjwvZGl2PlxuICAgKiAgICAgPGkgY2xhc3M9XCJjb2xvcnBpY2tlci1ndWlkZVwiPjwvaT5cbiAgICogICA8L2Rpdj5cbiAgICogPC9kaXY+XG4gICAqL1xuICB0ZW1wbGF0ZTogJzxkaXYgY2xhc3M9XCJjb2xvcnBpY2tlclwiPlxcbiAgICAgIDxkaXYgY2xhc3M9XCJjb2xvcnBpY2tlci1zYXR1cmF0aW9uXCI+PGkgY2xhc3M9XCJjb2xvcnBpY2tlci1ndWlkZVwiPjwvaT48L2Rpdj5cXG4gICAgICA8ZGl2IGNsYXNzPVwiY29sb3JwaWNrZXItaHVlXCI+PGkgY2xhc3M9XCJjb2xvcnBpY2tlci1ndWlkZVwiPjwvaT48L2Rpdj5cXG4gICAgICA8ZGl2IGNsYXNzPVwiY29sb3JwaWNrZXItYWxwaGFcIj5cXG4gICAgICAgIDxkaXYgY2xhc3M9XCJjb2xvcnBpY2tlci1hbHBoYS1jb2xvclwiPjwvZGl2PlxcbiAgICAgICAgPGkgY2xhc3M9XCJjb2xvcnBpY2tlci1ndWlkZVwiPjwvaT5cXG4gICAgICA8L2Rpdj5cXG4gICAgPC9kaXY+JyxcbiAgLyoqXG4gICAqXG4gICAqIEFzc29jaWF0aXZlIG9iamVjdCB3aXRoIHRoZSBleHRlbnNpb24gY2xhc3MgbmFtZSBhbmQgaXRzIGNvbmZpZy5cbiAgICogQ29sb3JwaWNrZXIgY29tZXMgd2l0aCBtYW55IGJ1bmRsZWQgZXh0ZW5zaW9uczogZGVidWdnZXIsIHBhbGV0dGUsIHByZXZpZXcgYW5kIHN3YXRjaGVzIChhIHN1cGVyc2V0IG9mIHBhbGV0dGUpLlxuICAgKlxuICAgKiBAdHlwZSB7T2JqZWN0W119XG4gICAqIEBleGFtcGxlXG4gICAqICAgZXh0ZW5zaW9uczogW1xuICAgKiAgICAge1xuICAgKiAgICAgICBuYW1lOiAnc3dhdGNoZXMnXG4gICAqICAgICAgIG9wdGlvbnM6IHtcbiAgICogICAgICAgICBjb2xvcnM6IHtcbiAgICogICAgICAgICAgICdwcmltYXJ5JzogJyMzMzdhYjcnLFxuICAgKiAgICAgICAgICAgJ3N1Y2Nlc3MnOiAnIzVjYjg1YycsXG4gICAqICAgICAgICAgICAnaW5mbyc6ICcjNWJjMGRlJyxcbiAgICogICAgICAgICAgICd3YXJuaW5nJzogJyNmMGFkNGUnLFxuICAgKiAgICAgICAgICAgJ2Rhbmdlcic6ICcjZDk1MzRmJ1xuICAgKiAgICAgICAgIH0sXG4gICAqICAgICAgICAgbmFtZXNBc1ZhbHVlczogdHJ1ZVxuICAgKiAgICAgICB9XG4gICAqICAgICB9XG4gICAqICAgXVxuICAgKi9cbiAgZXh0ZW5zaW9uczogW3tcbiAgICBuYW1lOiAncHJldmlldycsXG4gICAgb3B0aW9uczoge1xuICAgICAgc2hvd1RleHQ6IHRydWVcbiAgICB9XG4gIH1dLFxuICAvKipcbiAgICogVmVydGljYWwgc2xpZGVycyBjb25maWd1cmF0aW9uXG4gICAqIEB0eXBlIHtPYmplY3R9XG4gICAqL1xuICBzbGlkZXJzOiB7XG4gICAgc2F0dXJhdGlvbjoge1xuICAgICAgc2VsZWN0b3I6ICcuY29sb3JwaWNrZXItc2F0dXJhdGlvbicsXG4gICAgICBtYXhMZWZ0OiBzbGlkZXJTaXplLFxuICAgICAgbWF4VG9wOiBzbGlkZXJTaXplLFxuICAgICAgY2FsbExlZnQ6ICdzZXRTYXR1cmF0aW9uUmF0aW8nLFxuICAgICAgY2FsbFRvcDogJ3NldFZhbHVlUmF0aW8nXG4gICAgfSxcbiAgICBodWU6IHtcbiAgICAgIHNlbGVjdG9yOiAnLmNvbG9ycGlja2VyLWh1ZScsXG4gICAgICBtYXhMZWZ0OiAwLFxuICAgICAgbWF4VG9wOiBzbGlkZXJTaXplLFxuICAgICAgY2FsbExlZnQ6IGZhbHNlLFxuICAgICAgY2FsbFRvcDogJ3NldEh1ZVJhdGlvJ1xuICAgIH0sXG4gICAgYWxwaGE6IHtcbiAgICAgIHNlbGVjdG9yOiAnLmNvbG9ycGlja2VyLWFscGhhJyxcbiAgICAgIGNoaWxkU2VsZWN0b3I6ICcuY29sb3JwaWNrZXItYWxwaGEtY29sb3InLFxuICAgICAgbWF4TGVmdDogMCxcbiAgICAgIG1heFRvcDogc2xpZGVyU2l6ZSxcbiAgICAgIGNhbGxMZWZ0OiBmYWxzZSxcbiAgICAgIGNhbGxUb3A6ICdzZXRBbHBoYVJhdGlvJ1xuICAgIH1cbiAgfSxcbiAgLyoqXG4gICAqIEhvcml6b250YWwgc2xpZGVycyBjb25maWd1cmF0aW9uXG4gICAqIEB0eXBlIHtPYmplY3R9XG4gICAqL1xuICBzbGlkZXJzSG9yejoge1xuICAgIHNhdHVyYXRpb246IHtcbiAgICAgIHNlbGVjdG9yOiAnLmNvbG9ycGlja2VyLXNhdHVyYXRpb24nLFxuICAgICAgbWF4TGVmdDogc2xpZGVyU2l6ZSxcbiAgICAgIG1heFRvcDogc2xpZGVyU2l6ZSxcbiAgICAgIGNhbGxMZWZ0OiAnc2V0U2F0dXJhdGlvblJhdGlvJyxcbiAgICAgIGNhbGxUb3A6ICdzZXRWYWx1ZVJhdGlvJ1xuICAgIH0sXG4gICAgaHVlOiB7XG4gICAgICBzZWxlY3RvcjogJy5jb2xvcnBpY2tlci1odWUnLFxuICAgICAgbWF4TGVmdDogc2xpZGVyU2l6ZSxcbiAgICAgIG1heFRvcDogMCxcbiAgICAgIGNhbGxMZWZ0OiAnc2V0SHVlUmF0aW8nLFxuICAgICAgY2FsbFRvcDogZmFsc2VcbiAgICB9LFxuICAgIGFscGhhOiB7XG4gICAgICBzZWxlY3RvcjogJy5jb2xvcnBpY2tlci1hbHBoYScsXG4gICAgICBjaGlsZFNlbGVjdG9yOiAnLmNvbG9ycGlja2VyLWFscGhhLWNvbG9yJyxcbiAgICAgIG1heExlZnQ6IHNsaWRlclNpemUsXG4gICAgICBtYXhUb3A6IDAsXG4gICAgICBjYWxsTGVmdDogJ3NldEFscGhhUmF0aW8nLFxuICAgICAgY2FsbFRvcDogZmFsc2VcbiAgICB9XG4gIH1cbn07XG5tb2R1bGUuZXhwb3J0cyA9IGV4cG9ydHMuZGVmYXVsdDtcblxuLyoqKi8gfSksXG4vKiA0ICovXG4vKioqLyAoZnVuY3Rpb24obW9kdWxlLCBleHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKSB7XG5cblwidXNlIHN0cmljdFwiO1xuXG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfdHlwZW9mID0gdHlwZW9mIFN5bWJvbCA9PT0gXCJmdW5jdGlvblwiICYmIHR5cGVvZiBTeW1ib2wuaXRlcmF0b3IgPT09IFwic3ltYm9sXCIgPyBmdW5jdGlvbiAob2JqKSB7IHJldHVybiB0eXBlb2Ygb2JqOyB9IDogZnVuY3Rpb24gKG9iaikgeyByZXR1cm4gb2JqICYmIHR5cGVvZiBTeW1ib2wgPT09IFwiZnVuY3Rpb25cIiAmJiBvYmouY29uc3RydWN0b3IgPT09IFN5bWJvbCAmJiBvYmogIT09IFN5bWJvbC5wcm90b3R5cGUgPyBcInN5bWJvbFwiIDogdHlwZW9mIG9iajsgfTtcblxudmFyIF9jcmVhdGVDbGFzcyA9IGZ1bmN0aW9uICgpIHsgZnVuY3Rpb24gZGVmaW5lUHJvcGVydGllcyh0YXJnZXQsIHByb3BzKSB7IGZvciAodmFyIGkgPSAwOyBpIDwgcHJvcHMubGVuZ3RoOyBpKyspIHsgdmFyIGRlc2NyaXB0b3IgPSBwcm9wc1tpXTsgZGVzY3JpcHRvci5lbnVtZXJhYmxlID0gZGVzY3JpcHRvci5lbnVtZXJhYmxlIHx8IGZhbHNlOyBkZXNjcmlwdG9yLmNvbmZpZ3VyYWJsZSA9IHRydWU7IGlmIChcInZhbHVlXCIgaW4gZGVzY3JpcHRvcikgZGVzY3JpcHRvci53cml0YWJsZSA9IHRydWU7IE9iamVjdC5kZWZpbmVQcm9wZXJ0eSh0YXJnZXQsIGRlc2NyaXB0b3Iua2V5LCBkZXNjcmlwdG9yKTsgfSB9IHJldHVybiBmdW5jdGlvbiAoQ29uc3RydWN0b3IsIHByb3RvUHJvcHMsIHN0YXRpY1Byb3BzKSB7IGlmIChwcm90b1Byb3BzKSBkZWZpbmVQcm9wZXJ0aWVzKENvbnN0cnVjdG9yLnByb3RvdHlwZSwgcHJvdG9Qcm9wcyk7IGlmIChzdGF0aWNQcm9wcykgZGVmaW5lUHJvcGVydGllcyhDb25zdHJ1Y3Rvciwgc3RhdGljUHJvcHMpOyByZXR1cm4gQ29uc3RydWN0b3I7IH07IH0oKTtcblxudmFyIF9FeHRlbnNpb24yID0gX193ZWJwYWNrX3JlcXVpcmVfXygxKTtcblxudmFyIF9FeHRlbnNpb24zID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfRXh0ZW5zaW9uMik7XG5cbnZhciBfanF1ZXJ5ID0gX193ZWJwYWNrX3JlcXVpcmVfXygwKTtcblxudmFyIF9qcXVlcnkyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfanF1ZXJ5KTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxuZnVuY3Rpb24gX2NsYXNzQ2FsbENoZWNrKGluc3RhbmNlLCBDb25zdHJ1Y3RvcikgeyBpZiAoIShpbnN0YW5jZSBpbnN0YW5jZW9mIENvbnN0cnVjdG9yKSkgeyB0aHJvdyBuZXcgVHlwZUVycm9yKFwiQ2Fubm90IGNhbGwgYSBjbGFzcyBhcyBhIGZ1bmN0aW9uXCIpOyB9IH1cblxuZnVuY3Rpb24gX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4oc2VsZiwgY2FsbCkgeyBpZiAoIXNlbGYpIHsgdGhyb3cgbmV3IFJlZmVyZW5jZUVycm9yKFwidGhpcyBoYXNuJ3QgYmVlbiBpbml0aWFsaXNlZCAtIHN1cGVyKCkgaGFzbid0IGJlZW4gY2FsbGVkXCIpOyB9IHJldHVybiBjYWxsICYmICh0eXBlb2YgY2FsbCA9PT0gXCJvYmplY3RcIiB8fCB0eXBlb2YgY2FsbCA9PT0gXCJmdW5jdGlvblwiKSA/IGNhbGwgOiBzZWxmOyB9XG5cbmZ1bmN0aW9uIF9pbmhlcml0cyhzdWJDbGFzcywgc3VwZXJDbGFzcykgeyBpZiAodHlwZW9mIHN1cGVyQ2xhc3MgIT09IFwiZnVuY3Rpb25cIiAmJiBzdXBlckNsYXNzICE9PSBudWxsKSB7IHRocm93IG5ldyBUeXBlRXJyb3IoXCJTdXBlciBleHByZXNzaW9uIG11c3QgZWl0aGVyIGJlIG51bGwgb3IgYSBmdW5jdGlvbiwgbm90IFwiICsgdHlwZW9mIHN1cGVyQ2xhc3MpOyB9IHN1YkNsYXNzLnByb3RvdHlwZSA9IE9iamVjdC5jcmVhdGUoc3VwZXJDbGFzcyAmJiBzdXBlckNsYXNzLnByb3RvdHlwZSwgeyBjb25zdHJ1Y3RvcjogeyB2YWx1ZTogc3ViQ2xhc3MsIGVudW1lcmFibGU6IGZhbHNlLCB3cml0YWJsZTogdHJ1ZSwgY29uZmlndXJhYmxlOiB0cnVlIH0gfSk7IGlmIChzdXBlckNsYXNzKSBPYmplY3Quc2V0UHJvdG90eXBlT2YgPyBPYmplY3Quc2V0UHJvdG90eXBlT2Yoc3ViQ2xhc3MsIHN1cGVyQ2xhc3MpIDogc3ViQ2xhc3MuX19wcm90b19fID0gc3VwZXJDbGFzczsgfVxuXG52YXIgZGVmYXVsdHMgPSB7XG4gIC8qKlxuICAgKiBLZXktdmFsdWUgcGFpcnMgZGVmaW5pbmcgYSBjb2xvciBhbGlhcyBhbmQgaXRzIENTUyBjb2xvciByZXByZXNlbnRhdGlvbi5cbiAgICpcbiAgICogVGhleSBjYW4gYWxzbyBiZSBqdXN0IGFuIGFycmF5IG9mIHZhbHVlcy4gSW4gdGhhdCBjYXNlLCBubyBzcGVjaWFsIG5hbWVzIGFyZSB1c2VkLCBvbmx5IHRoZSByZWFsIGNvbG9ycy5cbiAgICpcbiAgICogQHR5cGUge09iamVjdHxBcnJheX1cbiAgICogQGRlZmF1bHQgbnVsbFxuICAgKiBAZXhhbXBsZVxuICAgKiAge1xuICAgKiAgICdibGFjayc6ICcjMDAwMDAwJyxcbiAgICogICAnd2hpdGUnOiAnI2ZmZmZmZicsXG4gICAqICAgJ3JlZCc6ICcjRkYwMDAwJyxcbiAgICogICAnZGVmYXVsdCc6ICcjNzc3Nzc3JyxcbiAgICogICAncHJpbWFyeSc6ICcjMzM3YWI3JyxcbiAgICogICAnc3VjY2Vzcyc6ICcjNWNiODVjJyxcbiAgICogICAnaW5mbyc6ICcjNWJjMGRlJyxcbiAgICogICAnd2FybmluZyc6ICcjZjBhZDRlJyxcbiAgICogICAnZGFuZ2VyJzogJyNkOTUzNGYnXG4gICAqICB9XG4gICAqXG4gICAqIEBleGFtcGxlIFsnI2YwYWQ0ZScsICcjMzM3YWI3JywgJyM1Y2I4NWMnXVxuICAgKi9cbiAgY29sb3JzOiBudWxsLFxuICAvKipcbiAgICogSWYgdHJ1ZSwgd2hlbiBhIGNvbG9yIHN3YXRjaCBpcyBzZWxlY3RlZCB0aGUgbmFtZSAoYWxpYXMpIHdpbGwgYmUgdXNlZCBhcyBpbnB1dCB2YWx1ZSxcbiAgICogb3RoZXJ3aXNlIHRoZSBzd2F0Y2ggcmVhbCBjb2xvciB2YWx1ZSB3aWxsIGJlIHVzZWQuXG4gICAqXG4gICAqIEB0eXBlIHtib29sZWFufVxuICAgKiBAZGVmYXVsdCB0cnVlXG4gICAqL1xuICBuYW1lc0FzVmFsdWVzOiB0cnVlXG59O1xuXG4vKipcbiAqIFBhbGV0dGUgZXh0ZW5zaW9uXG4gKiBAaWdub3JlXG4gKi9cblxudmFyIFBhbGV0dGUgPSBmdW5jdGlvbiAoX0V4dGVuc2lvbikge1xuICBfaW5oZXJpdHMoUGFsZXR0ZSwgX0V4dGVuc2lvbik7XG5cbiAgX2NyZWF0ZUNsYXNzKFBhbGV0dGUsIFt7XG4gICAga2V5OiAnY29sb3JzJyxcblxuXG4gICAgLyoqXG4gICAgICogQHJldHVybnMge09iamVjdHxBcnJheX1cbiAgICAgKi9cbiAgICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICAgIHJldHVybiB0aGlzLm9wdGlvbnMuY29sb3JzO1xuICAgIH1cbiAgfV0pO1xuXG4gIGZ1bmN0aW9uIFBhbGV0dGUoY29sb3JwaWNrZXIpIHtcbiAgICB2YXIgb3B0aW9ucyA9IGFyZ3VtZW50cy5sZW5ndGggPiAxICYmIGFyZ3VtZW50c1sxXSAhPT0gdW5kZWZpbmVkID8gYXJndW1lbnRzWzFdIDoge307XG5cbiAgICBfY2xhc3NDYWxsQ2hlY2sodGhpcywgUGFsZXR0ZSk7XG5cbiAgICB2YXIgX3RoaXMgPSBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybih0aGlzLCAoUGFsZXR0ZS5fX3Byb3RvX18gfHwgT2JqZWN0LmdldFByb3RvdHlwZU9mKFBhbGV0dGUpKS5jYWxsKHRoaXMsIGNvbG9ycGlja2VyLCBfanF1ZXJ5Mi5kZWZhdWx0LmV4dGVuZCh0cnVlLCB7fSwgZGVmYXVsdHMsIG9wdGlvbnMpKSk7XG5cbiAgICBpZiAoIUFycmF5LmlzQXJyYXkoX3RoaXMub3B0aW9ucy5jb2xvcnMpICYmIF90eXBlb2YoX3RoaXMub3B0aW9ucy5jb2xvcnMpICE9PSAnb2JqZWN0Jykge1xuICAgICAgX3RoaXMub3B0aW9ucy5jb2xvcnMgPSBudWxsO1xuICAgIH1cbiAgICByZXR1cm4gX3RoaXM7XG4gIH1cblxuICAvKipcbiAgICogQHJldHVybnMge2ludH1cbiAgICovXG5cblxuICBfY3JlYXRlQ2xhc3MoUGFsZXR0ZSwgW3tcbiAgICBrZXk6ICdnZXRMZW5ndGgnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBnZXRMZW5ndGgoKSB7XG4gICAgICBpZiAoIXRoaXMub3B0aW9ucy5jb2xvcnMpIHtcbiAgICAgICAgcmV0dXJuIDA7XG4gICAgICB9XG5cbiAgICAgIGlmIChBcnJheS5pc0FycmF5KHRoaXMub3B0aW9ucy5jb2xvcnMpKSB7XG4gICAgICAgIHJldHVybiB0aGlzLm9wdGlvbnMuY29sb3JzLmxlbmd0aDtcbiAgICAgIH1cblxuICAgICAgaWYgKF90eXBlb2YodGhpcy5vcHRpb25zLmNvbG9ycykgPT09ICdvYmplY3QnKSB7XG4gICAgICAgIHJldHVybiBPYmplY3Qua2V5cyh0aGlzLm9wdGlvbnMuY29sb3JzKS5sZW5ndGg7XG4gICAgICB9XG5cbiAgICAgIHJldHVybiAwO1xuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ3Jlc29sdmVDb2xvcicsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHJlc29sdmVDb2xvcihjb2xvcikge1xuICAgICAgdmFyIHJlYWxDb2xvciA9IGFyZ3VtZW50cy5sZW5ndGggPiAxICYmIGFyZ3VtZW50c1sxXSAhPT0gdW5kZWZpbmVkID8gYXJndW1lbnRzWzFdIDogdHJ1ZTtcblxuICAgICAgaWYgKHRoaXMuZ2V0TGVuZ3RoKCkgPD0gMCkge1xuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICB9XG5cbiAgICAgIC8vIEFycmF5IG9mIGNvbG9yc1xuICAgICAgaWYgKEFycmF5LmlzQXJyYXkodGhpcy5vcHRpb25zLmNvbG9ycykpIHtcbiAgICAgICAgaWYgKHRoaXMub3B0aW9ucy5jb2xvcnMuaW5kZXhPZihjb2xvcikgPj0gMCkge1xuICAgICAgICAgIHJldHVybiBjb2xvcjtcbiAgICAgICAgfVxuICAgICAgICBpZiAodGhpcy5vcHRpb25zLmNvbG9ycy5pbmRleE9mKGNvbG9yLnRvVXBwZXJDYXNlKCkpID49IDApIHtcbiAgICAgICAgICByZXR1cm4gY29sb3IudG9VcHBlckNhc2UoKTtcbiAgICAgICAgfVxuICAgICAgICBpZiAodGhpcy5vcHRpb25zLmNvbG9ycy5pbmRleE9mKGNvbG9yLnRvTG93ZXJDYXNlKCkpID49IDApIHtcbiAgICAgICAgICByZXR1cm4gY29sb3IudG9Mb3dlckNhc2UoKTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICB9XG5cbiAgICAgIGlmIChfdHlwZW9mKHRoaXMub3B0aW9ucy5jb2xvcnMpICE9PSAnb2JqZWN0Jykge1xuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICB9XG5cbiAgICAgIC8vIE1hcCBvZiBvYmplY3RzXG4gICAgICBpZiAoIXRoaXMub3B0aW9ucy5uYW1lc0FzVmFsdWVzIHx8IHJlYWxDb2xvcikge1xuICAgICAgICByZXR1cm4gdGhpcy5nZXRWYWx1ZShjb2xvciwgZmFsc2UpO1xuICAgICAgfVxuICAgICAgcmV0dXJuIHRoaXMuZ2V0TmFtZShjb2xvciwgdGhpcy5nZXROYW1lKCcjJyArIGNvbG9yKSk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogR2l2ZW4gYSBjb2xvciB2YWx1ZSwgcmV0dXJucyB0aGUgY29ycmVzcG9uZGluZyBjb2xvciBuYW1lIG9yIGRlZmF1bHRWYWx1ZS5cbiAgICAgKlxuICAgICAqIEBwYXJhbSB7U3RyaW5nfSB2YWx1ZVxuICAgICAqIEBwYXJhbSB7Kn0gZGVmYXVsdFZhbHVlXG4gICAgICogQHJldHVybnMgeyp9XG4gICAgICovXG5cbiAgfSwge1xuICAgIGtleTogJ2dldE5hbWUnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBnZXROYW1lKHZhbHVlKSB7XG4gICAgICB2YXIgZGVmYXVsdFZhbHVlID0gYXJndW1lbnRzLmxlbmd0aCA+IDEgJiYgYXJndW1lbnRzWzFdICE9PSB1bmRlZmluZWQgPyBhcmd1bWVudHNbMV0gOiBmYWxzZTtcblxuICAgICAgaWYgKCEodHlwZW9mIHZhbHVlID09PSAnc3RyaW5nJykgfHwgIXRoaXMub3B0aW9ucy5jb2xvcnMpIHtcbiAgICAgICAgcmV0dXJuIGRlZmF1bHRWYWx1ZTtcbiAgICAgIH1cbiAgICAgIGZvciAodmFyIG5hbWUgaW4gdGhpcy5vcHRpb25zLmNvbG9ycykge1xuICAgICAgICBpZiAoIXRoaXMub3B0aW9ucy5jb2xvcnMuaGFzT3duUHJvcGVydHkobmFtZSkpIHtcbiAgICAgICAgICBjb250aW51ZTtcbiAgICAgICAgfVxuICAgICAgICBpZiAodGhpcy5vcHRpb25zLmNvbG9yc1tuYW1lXS50b0xvd2VyQ2FzZSgpID09PSB2YWx1ZS50b0xvd2VyQ2FzZSgpKSB7XG4gICAgICAgICAgcmV0dXJuIG5hbWU7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICAgIHJldHVybiBkZWZhdWx0VmFsdWU7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogR2l2ZW4gYSBjb2xvciBuYW1lLCByZXR1cm5zIHRoZSBjb3JyZXNwb25kaW5nIGNvbG9yIHZhbHVlIG9yIGRlZmF1bHRWYWx1ZS5cbiAgICAgKlxuICAgICAqIEBwYXJhbSB7U3RyaW5nfSBuYW1lXG4gICAgICogQHBhcmFtIHsqfSBkZWZhdWx0VmFsdWVcbiAgICAgKiBAcmV0dXJucyB7Kn1cbiAgICAgKi9cblxuICB9LCB7XG4gICAga2V5OiAnZ2V0VmFsdWUnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBnZXRWYWx1ZShuYW1lKSB7XG4gICAgICB2YXIgZGVmYXVsdFZhbHVlID0gYXJndW1lbnRzLmxlbmd0aCA+IDEgJiYgYXJndW1lbnRzWzFdICE9PSB1bmRlZmluZWQgPyBhcmd1bWVudHNbMV0gOiBmYWxzZTtcblxuICAgICAgaWYgKCEodHlwZW9mIG5hbWUgPT09ICdzdHJpbmcnKSB8fCAhdGhpcy5vcHRpb25zLmNvbG9ycykge1xuICAgICAgICByZXR1cm4gZGVmYXVsdFZhbHVlO1xuICAgICAgfVxuICAgICAgaWYgKHRoaXMub3B0aW9ucy5jb2xvcnMuaGFzT3duUHJvcGVydHkobmFtZSkpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMub3B0aW9ucy5jb2xvcnNbbmFtZV07XG4gICAgICB9XG4gICAgICByZXR1cm4gZGVmYXVsdFZhbHVlO1xuICAgIH1cbiAgfV0pO1xuXG4gIHJldHVybiBQYWxldHRlO1xufShfRXh0ZW5zaW9uMy5kZWZhdWx0KTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gUGFsZXR0ZTtcbm1vZHVsZS5leHBvcnRzID0gZXhwb3J0cy5kZWZhdWx0O1xuXG4vKioqLyB9KSxcbi8qIDUgKi9cbi8qKiovIChmdW5jdGlvbihtb2R1bGUsIGV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pIHtcblxuXCJ1c2Ugc3RyaWN0XCI7XG5cclxuXHJcbm1vZHVsZS5leHBvcnRzID0ge1xyXG5cdFwiYWxpY2VibHVlXCI6IFsyNDAsIDI0OCwgMjU1XSxcclxuXHRcImFudGlxdWV3aGl0ZVwiOiBbMjUwLCAyMzUsIDIxNV0sXHJcblx0XCJhcXVhXCI6IFswLCAyNTUsIDI1NV0sXHJcblx0XCJhcXVhbWFyaW5lXCI6IFsxMjcsIDI1NSwgMjEyXSxcclxuXHRcImF6dXJlXCI6IFsyNDAsIDI1NSwgMjU1XSxcclxuXHRcImJlaWdlXCI6IFsyNDUsIDI0NSwgMjIwXSxcclxuXHRcImJpc3F1ZVwiOiBbMjU1LCAyMjgsIDE5Nl0sXHJcblx0XCJibGFja1wiOiBbMCwgMCwgMF0sXHJcblx0XCJibGFuY2hlZGFsbW9uZFwiOiBbMjU1LCAyMzUsIDIwNV0sXHJcblx0XCJibHVlXCI6IFswLCAwLCAyNTVdLFxyXG5cdFwiYmx1ZXZpb2xldFwiOiBbMTM4LCA0MywgMjI2XSxcclxuXHRcImJyb3duXCI6IFsxNjUsIDQyLCA0Ml0sXHJcblx0XCJidXJseXdvb2RcIjogWzIyMiwgMTg0LCAxMzVdLFxyXG5cdFwiY2FkZXRibHVlXCI6IFs5NSwgMTU4LCAxNjBdLFxyXG5cdFwiY2hhcnRyZXVzZVwiOiBbMTI3LCAyNTUsIDBdLFxyXG5cdFwiY2hvY29sYXRlXCI6IFsyMTAsIDEwNSwgMzBdLFxyXG5cdFwiY29yYWxcIjogWzI1NSwgMTI3LCA4MF0sXHJcblx0XCJjb3JuZmxvd2VyYmx1ZVwiOiBbMTAwLCAxNDksIDIzN10sXHJcblx0XCJjb3Juc2lsa1wiOiBbMjU1LCAyNDgsIDIyMF0sXHJcblx0XCJjcmltc29uXCI6IFsyMjAsIDIwLCA2MF0sXHJcblx0XCJjeWFuXCI6IFswLCAyNTUsIDI1NV0sXHJcblx0XCJkYXJrYmx1ZVwiOiBbMCwgMCwgMTM5XSxcclxuXHRcImRhcmtjeWFuXCI6IFswLCAxMzksIDEzOV0sXHJcblx0XCJkYXJrZ29sZGVucm9kXCI6IFsxODQsIDEzNCwgMTFdLFxyXG5cdFwiZGFya2dyYXlcIjogWzE2OSwgMTY5LCAxNjldLFxyXG5cdFwiZGFya2dyZWVuXCI6IFswLCAxMDAsIDBdLFxyXG5cdFwiZGFya2dyZXlcIjogWzE2OSwgMTY5LCAxNjldLFxyXG5cdFwiZGFya2toYWtpXCI6IFsxODksIDE4MywgMTA3XSxcclxuXHRcImRhcmttYWdlbnRhXCI6IFsxMzksIDAsIDEzOV0sXHJcblx0XCJkYXJrb2xpdmVncmVlblwiOiBbODUsIDEwNywgNDddLFxyXG5cdFwiZGFya29yYW5nZVwiOiBbMjU1LCAxNDAsIDBdLFxyXG5cdFwiZGFya29yY2hpZFwiOiBbMTUzLCA1MCwgMjA0XSxcclxuXHRcImRhcmtyZWRcIjogWzEzOSwgMCwgMF0sXHJcblx0XCJkYXJrc2FsbW9uXCI6IFsyMzMsIDE1MCwgMTIyXSxcclxuXHRcImRhcmtzZWFncmVlblwiOiBbMTQzLCAxODgsIDE0M10sXHJcblx0XCJkYXJrc2xhdGVibHVlXCI6IFs3MiwgNjEsIDEzOV0sXHJcblx0XCJkYXJrc2xhdGVncmF5XCI6IFs0NywgNzksIDc5XSxcclxuXHRcImRhcmtzbGF0ZWdyZXlcIjogWzQ3LCA3OSwgNzldLFxyXG5cdFwiZGFya3R1cnF1b2lzZVwiOiBbMCwgMjA2LCAyMDldLFxyXG5cdFwiZGFya3Zpb2xldFwiOiBbMTQ4LCAwLCAyMTFdLFxyXG5cdFwiZGVlcHBpbmtcIjogWzI1NSwgMjAsIDE0N10sXHJcblx0XCJkZWVwc2t5Ymx1ZVwiOiBbMCwgMTkxLCAyNTVdLFxyXG5cdFwiZGltZ3JheVwiOiBbMTA1LCAxMDUsIDEwNV0sXHJcblx0XCJkaW1ncmV5XCI6IFsxMDUsIDEwNSwgMTA1XSxcclxuXHRcImRvZGdlcmJsdWVcIjogWzMwLCAxNDQsIDI1NV0sXHJcblx0XCJmaXJlYnJpY2tcIjogWzE3OCwgMzQsIDM0XSxcclxuXHRcImZsb3JhbHdoaXRlXCI6IFsyNTUsIDI1MCwgMjQwXSxcclxuXHRcImZvcmVzdGdyZWVuXCI6IFszNCwgMTM5LCAzNF0sXHJcblx0XCJmdWNoc2lhXCI6IFsyNTUsIDAsIDI1NV0sXHJcblx0XCJnYWluc2Jvcm9cIjogWzIyMCwgMjIwLCAyMjBdLFxyXG5cdFwiZ2hvc3R3aGl0ZVwiOiBbMjQ4LCAyNDgsIDI1NV0sXHJcblx0XCJnb2xkXCI6IFsyNTUsIDIxNSwgMF0sXHJcblx0XCJnb2xkZW5yb2RcIjogWzIxOCwgMTY1LCAzMl0sXHJcblx0XCJncmF5XCI6IFsxMjgsIDEyOCwgMTI4XSxcclxuXHRcImdyZWVuXCI6IFswLCAxMjgsIDBdLFxyXG5cdFwiZ3JlZW55ZWxsb3dcIjogWzE3MywgMjU1LCA0N10sXHJcblx0XCJncmV5XCI6IFsxMjgsIDEyOCwgMTI4XSxcclxuXHRcImhvbmV5ZGV3XCI6IFsyNDAsIDI1NSwgMjQwXSxcclxuXHRcImhvdHBpbmtcIjogWzI1NSwgMTA1LCAxODBdLFxyXG5cdFwiaW5kaWFucmVkXCI6IFsyMDUsIDkyLCA5Ml0sXHJcblx0XCJpbmRpZ29cIjogWzc1LCAwLCAxMzBdLFxyXG5cdFwiaXZvcnlcIjogWzI1NSwgMjU1LCAyNDBdLFxyXG5cdFwia2hha2lcIjogWzI0MCwgMjMwLCAxNDBdLFxyXG5cdFwibGF2ZW5kZXJcIjogWzIzMCwgMjMwLCAyNTBdLFxyXG5cdFwibGF2ZW5kZXJibHVzaFwiOiBbMjU1LCAyNDAsIDI0NV0sXHJcblx0XCJsYXduZ3JlZW5cIjogWzEyNCwgMjUyLCAwXSxcclxuXHRcImxlbW9uY2hpZmZvblwiOiBbMjU1LCAyNTAsIDIwNV0sXHJcblx0XCJsaWdodGJsdWVcIjogWzE3MywgMjE2LCAyMzBdLFxyXG5cdFwibGlnaHRjb3JhbFwiOiBbMjQwLCAxMjgsIDEyOF0sXHJcblx0XCJsaWdodGN5YW5cIjogWzIyNCwgMjU1LCAyNTVdLFxyXG5cdFwibGlnaHRnb2xkZW5yb2R5ZWxsb3dcIjogWzI1MCwgMjUwLCAyMTBdLFxyXG5cdFwibGlnaHRncmF5XCI6IFsyMTEsIDIxMSwgMjExXSxcclxuXHRcImxpZ2h0Z3JlZW5cIjogWzE0NCwgMjM4LCAxNDRdLFxyXG5cdFwibGlnaHRncmV5XCI6IFsyMTEsIDIxMSwgMjExXSxcclxuXHRcImxpZ2h0cGlua1wiOiBbMjU1LCAxODIsIDE5M10sXHJcblx0XCJsaWdodHNhbG1vblwiOiBbMjU1LCAxNjAsIDEyMl0sXHJcblx0XCJsaWdodHNlYWdyZWVuXCI6IFszMiwgMTc4LCAxNzBdLFxyXG5cdFwibGlnaHRza3libHVlXCI6IFsxMzUsIDIwNiwgMjUwXSxcclxuXHRcImxpZ2h0c2xhdGVncmF5XCI6IFsxMTksIDEzNiwgMTUzXSxcclxuXHRcImxpZ2h0c2xhdGVncmV5XCI6IFsxMTksIDEzNiwgMTUzXSxcclxuXHRcImxpZ2h0c3RlZWxibHVlXCI6IFsxNzYsIDE5NiwgMjIyXSxcclxuXHRcImxpZ2h0eWVsbG93XCI6IFsyNTUsIDI1NSwgMjI0XSxcclxuXHRcImxpbWVcIjogWzAsIDI1NSwgMF0sXHJcblx0XCJsaW1lZ3JlZW5cIjogWzUwLCAyMDUsIDUwXSxcclxuXHRcImxpbmVuXCI6IFsyNTAsIDI0MCwgMjMwXSxcclxuXHRcIm1hZ2VudGFcIjogWzI1NSwgMCwgMjU1XSxcclxuXHRcIm1hcm9vblwiOiBbMTI4LCAwLCAwXSxcclxuXHRcIm1lZGl1bWFxdWFtYXJpbmVcIjogWzEwMiwgMjA1LCAxNzBdLFxyXG5cdFwibWVkaXVtYmx1ZVwiOiBbMCwgMCwgMjA1XSxcclxuXHRcIm1lZGl1bW9yY2hpZFwiOiBbMTg2LCA4NSwgMjExXSxcclxuXHRcIm1lZGl1bXB1cnBsZVwiOiBbMTQ3LCAxMTIsIDIxOV0sXHJcblx0XCJtZWRpdW1zZWFncmVlblwiOiBbNjAsIDE3OSwgMTEzXSxcclxuXHRcIm1lZGl1bXNsYXRlYmx1ZVwiOiBbMTIzLCAxMDQsIDIzOF0sXHJcblx0XCJtZWRpdW1zcHJpbmdncmVlblwiOiBbMCwgMjUwLCAxNTRdLFxyXG5cdFwibWVkaXVtdHVycXVvaXNlXCI6IFs3MiwgMjA5LCAyMDRdLFxyXG5cdFwibWVkaXVtdmlvbGV0cmVkXCI6IFsxOTksIDIxLCAxMzNdLFxyXG5cdFwibWlkbmlnaHRibHVlXCI6IFsyNSwgMjUsIDExMl0sXHJcblx0XCJtaW50Y3JlYW1cIjogWzI0NSwgMjU1LCAyNTBdLFxyXG5cdFwibWlzdHlyb3NlXCI6IFsyNTUsIDIyOCwgMjI1XSxcclxuXHRcIm1vY2Nhc2luXCI6IFsyNTUsIDIyOCwgMTgxXSxcclxuXHRcIm5hdmFqb3doaXRlXCI6IFsyNTUsIDIyMiwgMTczXSxcclxuXHRcIm5hdnlcIjogWzAsIDAsIDEyOF0sXHJcblx0XCJvbGRsYWNlXCI6IFsyNTMsIDI0NSwgMjMwXSxcclxuXHRcIm9saXZlXCI6IFsxMjgsIDEyOCwgMF0sXHJcblx0XCJvbGl2ZWRyYWJcIjogWzEwNywgMTQyLCAzNV0sXHJcblx0XCJvcmFuZ2VcIjogWzI1NSwgMTY1LCAwXSxcclxuXHRcIm9yYW5nZXJlZFwiOiBbMjU1LCA2OSwgMF0sXHJcblx0XCJvcmNoaWRcIjogWzIxOCwgMTEyLCAyMTRdLFxyXG5cdFwicGFsZWdvbGRlbnJvZFwiOiBbMjM4LCAyMzIsIDE3MF0sXHJcblx0XCJwYWxlZ3JlZW5cIjogWzE1MiwgMjUxLCAxNTJdLFxyXG5cdFwicGFsZXR1cnF1b2lzZVwiOiBbMTc1LCAyMzgsIDIzOF0sXHJcblx0XCJwYWxldmlvbGV0cmVkXCI6IFsyMTksIDExMiwgMTQ3XSxcclxuXHRcInBhcGF5YXdoaXBcIjogWzI1NSwgMjM5LCAyMTNdLFxyXG5cdFwicGVhY2hwdWZmXCI6IFsyNTUsIDIxOCwgMTg1XSxcclxuXHRcInBlcnVcIjogWzIwNSwgMTMzLCA2M10sXHJcblx0XCJwaW5rXCI6IFsyNTUsIDE5MiwgMjAzXSxcclxuXHRcInBsdW1cIjogWzIyMSwgMTYwLCAyMjFdLFxyXG5cdFwicG93ZGVyYmx1ZVwiOiBbMTc2LCAyMjQsIDIzMF0sXHJcblx0XCJwdXJwbGVcIjogWzEyOCwgMCwgMTI4XSxcclxuXHRcInJlYmVjY2FwdXJwbGVcIjogWzEwMiwgNTEsIDE1M10sXHJcblx0XCJyZWRcIjogWzI1NSwgMCwgMF0sXHJcblx0XCJyb3N5YnJvd25cIjogWzE4OCwgMTQzLCAxNDNdLFxyXG5cdFwicm95YWxibHVlXCI6IFs2NSwgMTA1LCAyMjVdLFxyXG5cdFwic2FkZGxlYnJvd25cIjogWzEzOSwgNjksIDE5XSxcclxuXHRcInNhbG1vblwiOiBbMjUwLCAxMjgsIDExNF0sXHJcblx0XCJzYW5keWJyb3duXCI6IFsyNDQsIDE2NCwgOTZdLFxyXG5cdFwic2VhZ3JlZW5cIjogWzQ2LCAxMzksIDg3XSxcclxuXHRcInNlYXNoZWxsXCI6IFsyNTUsIDI0NSwgMjM4XSxcclxuXHRcInNpZW5uYVwiOiBbMTYwLCA4MiwgNDVdLFxyXG5cdFwic2lsdmVyXCI6IFsxOTIsIDE5MiwgMTkyXSxcclxuXHRcInNreWJsdWVcIjogWzEzNSwgMjA2LCAyMzVdLFxyXG5cdFwic2xhdGVibHVlXCI6IFsxMDYsIDkwLCAyMDVdLFxyXG5cdFwic2xhdGVncmF5XCI6IFsxMTIsIDEyOCwgMTQ0XSxcclxuXHRcInNsYXRlZ3JleVwiOiBbMTEyLCAxMjgsIDE0NF0sXHJcblx0XCJzbm93XCI6IFsyNTUsIDI1MCwgMjUwXSxcclxuXHRcInNwcmluZ2dyZWVuXCI6IFswLCAyNTUsIDEyN10sXHJcblx0XCJzdGVlbGJsdWVcIjogWzcwLCAxMzAsIDE4MF0sXHJcblx0XCJ0YW5cIjogWzIxMCwgMTgwLCAxNDBdLFxyXG5cdFwidGVhbFwiOiBbMCwgMTI4LCAxMjhdLFxyXG5cdFwidGhpc3RsZVwiOiBbMjE2LCAxOTEsIDIxNl0sXHJcblx0XCJ0b21hdG9cIjogWzI1NSwgOTksIDcxXSxcclxuXHRcInR1cnF1b2lzZVwiOiBbNjQsIDIyNCwgMjA4XSxcclxuXHRcInZpb2xldFwiOiBbMjM4LCAxMzAsIDIzOF0sXHJcblx0XCJ3aGVhdFwiOiBbMjQ1LCAyMjIsIDE3OV0sXHJcblx0XCJ3aGl0ZVwiOiBbMjU1LCAyNTUsIDI1NV0sXHJcblx0XCJ3aGl0ZXNtb2tlXCI6IFsyNDUsIDI0NSwgMjQ1XSxcclxuXHRcInllbGxvd1wiOiBbMjU1LCAyNTUsIDBdLFxyXG5cdFwieWVsbG93Z3JlZW5cIjogWzE1NCwgMjA1LCA1MF1cclxufTtcclxuXG5cbi8qKiovIH0pLFxuLyogNiAqL1xuLyoqKi8gKGZ1bmN0aW9uKG1vZHVsZSwgZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXykge1xuXG4vKiBNSVQgbGljZW5zZSAqL1xudmFyIGNzc0tleXdvcmRzID0gX193ZWJwYWNrX3JlcXVpcmVfXyg1KTtcblxuLy8gTk9URTogY29udmVyc2lvbnMgc2hvdWxkIG9ubHkgcmV0dXJuIHByaW1pdGl2ZSB2YWx1ZXMgKGkuZS4gYXJyYXlzLCBvclxuLy8gICAgICAgdmFsdWVzIHRoYXQgZ2l2ZSBjb3JyZWN0IGB0eXBlb2ZgIHJlc3VsdHMpLlxuLy8gICAgICAgZG8gbm90IHVzZSBib3ggdmFsdWVzIHR5cGVzIChpLmUuIE51bWJlcigpLCBTdHJpbmcoKSwgZXRjLilcblxudmFyIHJldmVyc2VLZXl3b3JkcyA9IHt9O1xuZm9yICh2YXIga2V5IGluIGNzc0tleXdvcmRzKSB7XG5cdGlmIChjc3NLZXl3b3Jkcy5oYXNPd25Qcm9wZXJ0eShrZXkpKSB7XG5cdFx0cmV2ZXJzZUtleXdvcmRzW2Nzc0tleXdvcmRzW2tleV1dID0ga2V5O1xuXHR9XG59XG5cbnZhciBjb252ZXJ0ID0gbW9kdWxlLmV4cG9ydHMgPSB7XG5cdHJnYjoge2NoYW5uZWxzOiAzLCBsYWJlbHM6ICdyZ2InfSxcblx0aHNsOiB7Y2hhbm5lbHM6IDMsIGxhYmVsczogJ2hzbCd9LFxuXHRoc3Y6IHtjaGFubmVsczogMywgbGFiZWxzOiAnaHN2J30sXG5cdGh3Yjoge2NoYW5uZWxzOiAzLCBsYWJlbHM6ICdod2InfSxcblx0Y215azoge2NoYW5uZWxzOiA0LCBsYWJlbHM6ICdjbXlrJ30sXG5cdHh5ejoge2NoYW5uZWxzOiAzLCBsYWJlbHM6ICd4eXonfSxcblx0bGFiOiB7Y2hhbm5lbHM6IDMsIGxhYmVsczogJ2xhYid9LFxuXHRsY2g6IHtjaGFubmVsczogMywgbGFiZWxzOiAnbGNoJ30sXG5cdGhleDoge2NoYW5uZWxzOiAxLCBsYWJlbHM6IFsnaGV4J119LFxuXHRrZXl3b3JkOiB7Y2hhbm5lbHM6IDEsIGxhYmVsczogWydrZXl3b3JkJ119LFxuXHRhbnNpMTY6IHtjaGFubmVsczogMSwgbGFiZWxzOiBbJ2Fuc2kxNiddfSxcblx0YW5zaTI1Njoge2NoYW5uZWxzOiAxLCBsYWJlbHM6IFsnYW5zaTI1NiddfSxcblx0aGNnOiB7Y2hhbm5lbHM6IDMsIGxhYmVsczogWydoJywgJ2MnLCAnZyddfSxcblx0YXBwbGU6IHtjaGFubmVsczogMywgbGFiZWxzOiBbJ3IxNicsICdnMTYnLCAnYjE2J119LFxuXHRncmF5OiB7Y2hhbm5lbHM6IDEsIGxhYmVsczogWydncmF5J119XG59O1xuXG4vLyBoaWRlIC5jaGFubmVscyBhbmQgLmxhYmVscyBwcm9wZXJ0aWVzXG5mb3IgKHZhciBtb2RlbCBpbiBjb252ZXJ0KSB7XG5cdGlmIChjb252ZXJ0Lmhhc093blByb3BlcnR5KG1vZGVsKSkge1xuXHRcdGlmICghKCdjaGFubmVscycgaW4gY29udmVydFttb2RlbF0pKSB7XG5cdFx0XHR0aHJvdyBuZXcgRXJyb3IoJ21pc3NpbmcgY2hhbm5lbHMgcHJvcGVydHk6ICcgKyBtb2RlbCk7XG5cdFx0fVxuXG5cdFx0aWYgKCEoJ2xhYmVscycgaW4gY29udmVydFttb2RlbF0pKSB7XG5cdFx0XHR0aHJvdyBuZXcgRXJyb3IoJ21pc3NpbmcgY2hhbm5lbCBsYWJlbHMgcHJvcGVydHk6ICcgKyBtb2RlbCk7XG5cdFx0fVxuXG5cdFx0aWYgKGNvbnZlcnRbbW9kZWxdLmxhYmVscy5sZW5ndGggIT09IGNvbnZlcnRbbW9kZWxdLmNoYW5uZWxzKSB7XG5cdFx0XHR0aHJvdyBuZXcgRXJyb3IoJ2NoYW5uZWwgYW5kIGxhYmVsIGNvdW50cyBtaXNtYXRjaDogJyArIG1vZGVsKTtcblx0XHR9XG5cblx0XHR2YXIgY2hhbm5lbHMgPSBjb252ZXJ0W21vZGVsXS5jaGFubmVscztcblx0XHR2YXIgbGFiZWxzID0gY29udmVydFttb2RlbF0ubGFiZWxzO1xuXHRcdGRlbGV0ZSBjb252ZXJ0W21vZGVsXS5jaGFubmVscztcblx0XHRkZWxldGUgY29udmVydFttb2RlbF0ubGFiZWxzO1xuXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShjb252ZXJ0W21vZGVsXSwgJ2NoYW5uZWxzJywge3ZhbHVlOiBjaGFubmVsc30pO1xuXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShjb252ZXJ0W21vZGVsXSwgJ2xhYmVscycsIHt2YWx1ZTogbGFiZWxzfSk7XG5cdH1cbn1cblxuY29udmVydC5yZ2IuaHNsID0gZnVuY3Rpb24gKHJnYikge1xuXHR2YXIgciA9IHJnYlswXSAvIDI1NTtcblx0dmFyIGcgPSByZ2JbMV0gLyAyNTU7XG5cdHZhciBiID0gcmdiWzJdIC8gMjU1O1xuXHR2YXIgbWluID0gTWF0aC5taW4ociwgZywgYik7XG5cdHZhciBtYXggPSBNYXRoLm1heChyLCBnLCBiKTtcblx0dmFyIGRlbHRhID0gbWF4IC0gbWluO1xuXHR2YXIgaDtcblx0dmFyIHM7XG5cdHZhciBsO1xuXG5cdGlmIChtYXggPT09IG1pbikge1xuXHRcdGggPSAwO1xuXHR9IGVsc2UgaWYgKHIgPT09IG1heCkge1xuXHRcdGggPSAoZyAtIGIpIC8gZGVsdGE7XG5cdH0gZWxzZSBpZiAoZyA9PT0gbWF4KSB7XG5cdFx0aCA9IDIgKyAoYiAtIHIpIC8gZGVsdGE7XG5cdH0gZWxzZSBpZiAoYiA9PT0gbWF4KSB7XG5cdFx0aCA9IDQgKyAociAtIGcpIC8gZGVsdGE7XG5cdH1cblxuXHRoID0gTWF0aC5taW4oaCAqIDYwLCAzNjApO1xuXG5cdGlmIChoIDwgMCkge1xuXHRcdGggKz0gMzYwO1xuXHR9XG5cblx0bCA9IChtaW4gKyBtYXgpIC8gMjtcblxuXHRpZiAobWF4ID09PSBtaW4pIHtcblx0XHRzID0gMDtcblx0fSBlbHNlIGlmIChsIDw9IDAuNSkge1xuXHRcdHMgPSBkZWx0YSAvIChtYXggKyBtaW4pO1xuXHR9IGVsc2Uge1xuXHRcdHMgPSBkZWx0YSAvICgyIC0gbWF4IC0gbWluKTtcblx0fVxuXG5cdHJldHVybiBbaCwgcyAqIDEwMCwgbCAqIDEwMF07XG59O1xuXG5jb252ZXJ0LnJnYi5oc3YgPSBmdW5jdGlvbiAocmdiKSB7XG5cdHZhciByZGlmO1xuXHR2YXIgZ2RpZjtcblx0dmFyIGJkaWY7XG5cdHZhciBoO1xuXHR2YXIgcztcblxuXHR2YXIgciA9IHJnYlswXSAvIDI1NTtcblx0dmFyIGcgPSByZ2JbMV0gLyAyNTU7XG5cdHZhciBiID0gcmdiWzJdIC8gMjU1O1xuXHR2YXIgdiA9IE1hdGgubWF4KHIsIGcsIGIpO1xuXHR2YXIgZGlmZiA9IHYgLSBNYXRoLm1pbihyLCBnLCBiKTtcblx0dmFyIGRpZmZjID0gZnVuY3Rpb24gKGMpIHtcblx0XHRyZXR1cm4gKHYgLSBjKSAvIDYgLyBkaWZmICsgMSAvIDI7XG5cdH07XG5cblx0aWYgKGRpZmYgPT09IDApIHtcblx0XHRoID0gcyA9IDA7XG5cdH0gZWxzZSB7XG5cdFx0cyA9IGRpZmYgLyB2O1xuXHRcdHJkaWYgPSBkaWZmYyhyKTtcblx0XHRnZGlmID0gZGlmZmMoZyk7XG5cdFx0YmRpZiA9IGRpZmZjKGIpO1xuXG5cdFx0aWYgKHIgPT09IHYpIHtcblx0XHRcdGggPSBiZGlmIC0gZ2RpZjtcblx0XHR9IGVsc2UgaWYgKGcgPT09IHYpIHtcblx0XHRcdGggPSAoMSAvIDMpICsgcmRpZiAtIGJkaWY7XG5cdFx0fSBlbHNlIGlmIChiID09PSB2KSB7XG5cdFx0XHRoID0gKDIgLyAzKSArIGdkaWYgLSByZGlmO1xuXHRcdH1cblx0XHRpZiAoaCA8IDApIHtcblx0XHRcdGggKz0gMTtcblx0XHR9IGVsc2UgaWYgKGggPiAxKSB7XG5cdFx0XHRoIC09IDE7XG5cdFx0fVxuXHR9XG5cblx0cmV0dXJuIFtcblx0XHRoICogMzYwLFxuXHRcdHMgKiAxMDAsXG5cdFx0diAqIDEwMFxuXHRdO1xufTtcblxuY29udmVydC5yZ2IuaHdiID0gZnVuY3Rpb24gKHJnYikge1xuXHR2YXIgciA9IHJnYlswXTtcblx0dmFyIGcgPSByZ2JbMV07XG5cdHZhciBiID0gcmdiWzJdO1xuXHR2YXIgaCA9IGNvbnZlcnQucmdiLmhzbChyZ2IpWzBdO1xuXHR2YXIgdyA9IDEgLyAyNTUgKiBNYXRoLm1pbihyLCBNYXRoLm1pbihnLCBiKSk7XG5cblx0YiA9IDEgLSAxIC8gMjU1ICogTWF0aC5tYXgociwgTWF0aC5tYXgoZywgYikpO1xuXG5cdHJldHVybiBbaCwgdyAqIDEwMCwgYiAqIDEwMF07XG59O1xuXG5jb252ZXJ0LnJnYi5jbXlrID0gZnVuY3Rpb24gKHJnYikge1xuXHR2YXIgciA9IHJnYlswXSAvIDI1NTtcblx0dmFyIGcgPSByZ2JbMV0gLyAyNTU7XG5cdHZhciBiID0gcmdiWzJdIC8gMjU1O1xuXHR2YXIgYztcblx0dmFyIG07XG5cdHZhciB5O1xuXHR2YXIgaztcblxuXHRrID0gTWF0aC5taW4oMSAtIHIsIDEgLSBnLCAxIC0gYik7XG5cdGMgPSAoMSAtIHIgLSBrKSAvICgxIC0gaykgfHwgMDtcblx0bSA9ICgxIC0gZyAtIGspIC8gKDEgLSBrKSB8fCAwO1xuXHR5ID0gKDEgLSBiIC0gaykgLyAoMSAtIGspIHx8IDA7XG5cblx0cmV0dXJuIFtjICogMTAwLCBtICogMTAwLCB5ICogMTAwLCBrICogMTAwXTtcbn07XG5cbi8qKlxuICogU2VlIGh0dHBzOi8vZW4ubS53aWtpcGVkaWEub3JnL3dpa2kvRXVjbGlkZWFuX2Rpc3RhbmNlI1NxdWFyZWRfRXVjbGlkZWFuX2Rpc3RhbmNlXG4gKiAqL1xuZnVuY3Rpb24gY29tcGFyYXRpdmVEaXN0YW5jZSh4LCB5KSB7XG5cdHJldHVybiAoXG5cdFx0TWF0aC5wb3coeFswXSAtIHlbMF0sIDIpICtcblx0XHRNYXRoLnBvdyh4WzFdIC0geVsxXSwgMikgK1xuXHRcdE1hdGgucG93KHhbMl0gLSB5WzJdLCAyKVxuXHQpO1xufVxuXG5jb252ZXJ0LnJnYi5rZXl3b3JkID0gZnVuY3Rpb24gKHJnYikge1xuXHR2YXIgcmV2ZXJzZWQgPSByZXZlcnNlS2V5d29yZHNbcmdiXTtcblx0aWYgKHJldmVyc2VkKSB7XG5cdFx0cmV0dXJuIHJldmVyc2VkO1xuXHR9XG5cblx0dmFyIGN1cnJlbnRDbG9zZXN0RGlzdGFuY2UgPSBJbmZpbml0eTtcblx0dmFyIGN1cnJlbnRDbG9zZXN0S2V5d29yZDtcblxuXHRmb3IgKHZhciBrZXl3b3JkIGluIGNzc0tleXdvcmRzKSB7XG5cdFx0aWYgKGNzc0tleXdvcmRzLmhhc093blByb3BlcnR5KGtleXdvcmQpKSB7XG5cdFx0XHR2YXIgdmFsdWUgPSBjc3NLZXl3b3Jkc1trZXl3b3JkXTtcblxuXHRcdFx0Ly8gQ29tcHV0ZSBjb21wYXJhdGl2ZSBkaXN0YW5jZVxuXHRcdFx0dmFyIGRpc3RhbmNlID0gY29tcGFyYXRpdmVEaXN0YW5jZShyZ2IsIHZhbHVlKTtcblxuXHRcdFx0Ly8gQ2hlY2sgaWYgaXRzIGxlc3MsIGlmIHNvIHNldCBhcyBjbG9zZXN0XG5cdFx0XHRpZiAoZGlzdGFuY2UgPCBjdXJyZW50Q2xvc2VzdERpc3RhbmNlKSB7XG5cdFx0XHRcdGN1cnJlbnRDbG9zZXN0RGlzdGFuY2UgPSBkaXN0YW5jZTtcblx0XHRcdFx0Y3VycmVudENsb3Nlc3RLZXl3b3JkID0ga2V5d29yZDtcblx0XHRcdH1cblx0XHR9XG5cdH1cblxuXHRyZXR1cm4gY3VycmVudENsb3Nlc3RLZXl3b3JkO1xufTtcblxuY29udmVydC5rZXl3b3JkLnJnYiA9IGZ1bmN0aW9uIChrZXl3b3JkKSB7XG5cdHJldHVybiBjc3NLZXl3b3Jkc1trZXl3b3JkXTtcbn07XG5cbmNvbnZlcnQucmdiLnh5eiA9IGZ1bmN0aW9uIChyZ2IpIHtcblx0dmFyIHIgPSByZ2JbMF0gLyAyNTU7XG5cdHZhciBnID0gcmdiWzFdIC8gMjU1O1xuXHR2YXIgYiA9IHJnYlsyXSAvIDI1NTtcblxuXHQvLyBhc3N1bWUgc1JHQlxuXHRyID0gciA+IDAuMDQwNDUgPyBNYXRoLnBvdygoKHIgKyAwLjA1NSkgLyAxLjA1NSksIDIuNCkgOiAociAvIDEyLjkyKTtcblx0ZyA9IGcgPiAwLjA0MDQ1ID8gTWF0aC5wb3coKChnICsgMC4wNTUpIC8gMS4wNTUpLCAyLjQpIDogKGcgLyAxMi45Mik7XG5cdGIgPSBiID4gMC4wNDA0NSA/IE1hdGgucG93KCgoYiArIDAuMDU1KSAvIDEuMDU1KSwgMi40KSA6IChiIC8gMTIuOTIpO1xuXG5cdHZhciB4ID0gKHIgKiAwLjQxMjQpICsgKGcgKiAwLjM1NzYpICsgKGIgKiAwLjE4MDUpO1xuXHR2YXIgeSA9IChyICogMC4yMTI2KSArIChnICogMC43MTUyKSArIChiICogMC4wNzIyKTtcblx0dmFyIHogPSAociAqIDAuMDE5MykgKyAoZyAqIDAuMTE5MikgKyAoYiAqIDAuOTUwNSk7XG5cblx0cmV0dXJuIFt4ICogMTAwLCB5ICogMTAwLCB6ICogMTAwXTtcbn07XG5cbmNvbnZlcnQucmdiLmxhYiA9IGZ1bmN0aW9uIChyZ2IpIHtcblx0dmFyIHh5eiA9IGNvbnZlcnQucmdiLnh5eihyZ2IpO1xuXHR2YXIgeCA9IHh5elswXTtcblx0dmFyIHkgPSB4eXpbMV07XG5cdHZhciB6ID0geHl6WzJdO1xuXHR2YXIgbDtcblx0dmFyIGE7XG5cdHZhciBiO1xuXG5cdHggLz0gOTUuMDQ3O1xuXHR5IC89IDEwMDtcblx0eiAvPSAxMDguODgzO1xuXG5cdHggPSB4ID4gMC4wMDg4NTYgPyBNYXRoLnBvdyh4LCAxIC8gMykgOiAoNy43ODcgKiB4KSArICgxNiAvIDExNik7XG5cdHkgPSB5ID4gMC4wMDg4NTYgPyBNYXRoLnBvdyh5LCAxIC8gMykgOiAoNy43ODcgKiB5KSArICgxNiAvIDExNik7XG5cdHogPSB6ID4gMC4wMDg4NTYgPyBNYXRoLnBvdyh6LCAxIC8gMykgOiAoNy43ODcgKiB6KSArICgxNiAvIDExNik7XG5cblx0bCA9ICgxMTYgKiB5KSAtIDE2O1xuXHRhID0gNTAwICogKHggLSB5KTtcblx0YiA9IDIwMCAqICh5IC0geik7XG5cblx0cmV0dXJuIFtsLCBhLCBiXTtcbn07XG5cbmNvbnZlcnQuaHNsLnJnYiA9IGZ1bmN0aW9uIChoc2wpIHtcblx0dmFyIGggPSBoc2xbMF0gLyAzNjA7XG5cdHZhciBzID0gaHNsWzFdIC8gMTAwO1xuXHR2YXIgbCA9IGhzbFsyXSAvIDEwMDtcblx0dmFyIHQxO1xuXHR2YXIgdDI7XG5cdHZhciB0Mztcblx0dmFyIHJnYjtcblx0dmFyIHZhbDtcblxuXHRpZiAocyA9PT0gMCkge1xuXHRcdHZhbCA9IGwgKiAyNTU7XG5cdFx0cmV0dXJuIFt2YWwsIHZhbCwgdmFsXTtcblx0fVxuXG5cdGlmIChsIDwgMC41KSB7XG5cdFx0dDIgPSBsICogKDEgKyBzKTtcblx0fSBlbHNlIHtcblx0XHR0MiA9IGwgKyBzIC0gbCAqIHM7XG5cdH1cblxuXHR0MSA9IDIgKiBsIC0gdDI7XG5cblx0cmdiID0gWzAsIDAsIDBdO1xuXHRmb3IgKHZhciBpID0gMDsgaSA8IDM7IGkrKykge1xuXHRcdHQzID0gaCArIDEgLyAzICogLShpIC0gMSk7XG5cdFx0aWYgKHQzIDwgMCkge1xuXHRcdFx0dDMrKztcblx0XHR9XG5cdFx0aWYgKHQzID4gMSkge1xuXHRcdFx0dDMtLTtcblx0XHR9XG5cblx0XHRpZiAoNiAqIHQzIDwgMSkge1xuXHRcdFx0dmFsID0gdDEgKyAodDIgLSB0MSkgKiA2ICogdDM7XG5cdFx0fSBlbHNlIGlmICgyICogdDMgPCAxKSB7XG5cdFx0XHR2YWwgPSB0Mjtcblx0XHR9IGVsc2UgaWYgKDMgKiB0MyA8IDIpIHtcblx0XHRcdHZhbCA9IHQxICsgKHQyIC0gdDEpICogKDIgLyAzIC0gdDMpICogNjtcblx0XHR9IGVsc2Uge1xuXHRcdFx0dmFsID0gdDE7XG5cdFx0fVxuXG5cdFx0cmdiW2ldID0gdmFsICogMjU1O1xuXHR9XG5cblx0cmV0dXJuIHJnYjtcbn07XG5cbmNvbnZlcnQuaHNsLmhzdiA9IGZ1bmN0aW9uIChoc2wpIHtcblx0dmFyIGggPSBoc2xbMF07XG5cdHZhciBzID0gaHNsWzFdIC8gMTAwO1xuXHR2YXIgbCA9IGhzbFsyXSAvIDEwMDtcblx0dmFyIHNtaW4gPSBzO1xuXHR2YXIgbG1pbiA9IE1hdGgubWF4KGwsIDAuMDEpO1xuXHR2YXIgc3Y7XG5cdHZhciB2O1xuXG5cdGwgKj0gMjtcblx0cyAqPSAobCA8PSAxKSA/IGwgOiAyIC0gbDtcblx0c21pbiAqPSBsbWluIDw9IDEgPyBsbWluIDogMiAtIGxtaW47XG5cdHYgPSAobCArIHMpIC8gMjtcblx0c3YgPSBsID09PSAwID8gKDIgKiBzbWluKSAvIChsbWluICsgc21pbikgOiAoMiAqIHMpIC8gKGwgKyBzKTtcblxuXHRyZXR1cm4gW2gsIHN2ICogMTAwLCB2ICogMTAwXTtcbn07XG5cbmNvbnZlcnQuaHN2LnJnYiA9IGZ1bmN0aW9uIChoc3YpIHtcblx0dmFyIGggPSBoc3ZbMF0gLyA2MDtcblx0dmFyIHMgPSBoc3ZbMV0gLyAxMDA7XG5cdHZhciB2ID0gaHN2WzJdIC8gMTAwO1xuXHR2YXIgaGkgPSBNYXRoLmZsb29yKGgpICUgNjtcblxuXHR2YXIgZiA9IGggLSBNYXRoLmZsb29yKGgpO1xuXHR2YXIgcCA9IDI1NSAqIHYgKiAoMSAtIHMpO1xuXHR2YXIgcSA9IDI1NSAqIHYgKiAoMSAtIChzICogZikpO1xuXHR2YXIgdCA9IDI1NSAqIHYgKiAoMSAtIChzICogKDEgLSBmKSkpO1xuXHR2ICo9IDI1NTtcblxuXHRzd2l0Y2ggKGhpKSB7XG5cdFx0Y2FzZSAwOlxuXHRcdFx0cmV0dXJuIFt2LCB0LCBwXTtcblx0XHRjYXNlIDE6XG5cdFx0XHRyZXR1cm4gW3EsIHYsIHBdO1xuXHRcdGNhc2UgMjpcblx0XHRcdHJldHVybiBbcCwgdiwgdF07XG5cdFx0Y2FzZSAzOlxuXHRcdFx0cmV0dXJuIFtwLCBxLCB2XTtcblx0XHRjYXNlIDQ6XG5cdFx0XHRyZXR1cm4gW3QsIHAsIHZdO1xuXHRcdGNhc2UgNTpcblx0XHRcdHJldHVybiBbdiwgcCwgcV07XG5cdH1cbn07XG5cbmNvbnZlcnQuaHN2LmhzbCA9IGZ1bmN0aW9uIChoc3YpIHtcblx0dmFyIGggPSBoc3ZbMF07XG5cdHZhciBzID0gaHN2WzFdIC8gMTAwO1xuXHR2YXIgdiA9IGhzdlsyXSAvIDEwMDtcblx0dmFyIHZtaW4gPSBNYXRoLm1heCh2LCAwLjAxKTtcblx0dmFyIGxtaW47XG5cdHZhciBzbDtcblx0dmFyIGw7XG5cblx0bCA9ICgyIC0gcykgKiB2O1xuXHRsbWluID0gKDIgLSBzKSAqIHZtaW47XG5cdHNsID0gcyAqIHZtaW47XG5cdHNsIC89IChsbWluIDw9IDEpID8gbG1pbiA6IDIgLSBsbWluO1xuXHRzbCA9IHNsIHx8IDA7XG5cdGwgLz0gMjtcblxuXHRyZXR1cm4gW2gsIHNsICogMTAwLCBsICogMTAwXTtcbn07XG5cbi8vIGh0dHA6Ly9kZXYudzMub3JnL2Nzc3dnL2Nzcy1jb2xvci8jaHdiLXRvLXJnYlxuY29udmVydC5od2IucmdiID0gZnVuY3Rpb24gKGh3Yikge1xuXHR2YXIgaCA9IGh3YlswXSAvIDM2MDtcblx0dmFyIHdoID0gaHdiWzFdIC8gMTAwO1xuXHR2YXIgYmwgPSBod2JbMl0gLyAxMDA7XG5cdHZhciByYXRpbyA9IHdoICsgYmw7XG5cdHZhciBpO1xuXHR2YXIgdjtcblx0dmFyIGY7XG5cdHZhciBuO1xuXG5cdC8vIHdoICsgYmwgY2FudCBiZSA+IDFcblx0aWYgKHJhdGlvID4gMSkge1xuXHRcdHdoIC89IHJhdGlvO1xuXHRcdGJsIC89IHJhdGlvO1xuXHR9XG5cblx0aSA9IE1hdGguZmxvb3IoNiAqIGgpO1xuXHR2ID0gMSAtIGJsO1xuXHRmID0gNiAqIGggLSBpO1xuXG5cdGlmICgoaSAmIDB4MDEpICE9PSAwKSB7XG5cdFx0ZiA9IDEgLSBmO1xuXHR9XG5cblx0biA9IHdoICsgZiAqICh2IC0gd2gpOyAvLyBsaW5lYXIgaW50ZXJwb2xhdGlvblxuXG5cdHZhciByO1xuXHR2YXIgZztcblx0dmFyIGI7XG5cdHN3aXRjaCAoaSkge1xuXHRcdGRlZmF1bHQ6XG5cdFx0Y2FzZSA2OlxuXHRcdGNhc2UgMDogciA9IHY7IGcgPSBuOyBiID0gd2g7IGJyZWFrO1xuXHRcdGNhc2UgMTogciA9IG47IGcgPSB2OyBiID0gd2g7IGJyZWFrO1xuXHRcdGNhc2UgMjogciA9IHdoOyBnID0gdjsgYiA9IG47IGJyZWFrO1xuXHRcdGNhc2UgMzogciA9IHdoOyBnID0gbjsgYiA9IHY7IGJyZWFrO1xuXHRcdGNhc2UgNDogciA9IG47IGcgPSB3aDsgYiA9IHY7IGJyZWFrO1xuXHRcdGNhc2UgNTogciA9IHY7IGcgPSB3aDsgYiA9IG47IGJyZWFrO1xuXHR9XG5cblx0cmV0dXJuIFtyICogMjU1LCBnICogMjU1LCBiICogMjU1XTtcbn07XG5cbmNvbnZlcnQuY215ay5yZ2IgPSBmdW5jdGlvbiAoY215aykge1xuXHR2YXIgYyA9IGNteWtbMF0gLyAxMDA7XG5cdHZhciBtID0gY215a1sxXSAvIDEwMDtcblx0dmFyIHkgPSBjbXlrWzJdIC8gMTAwO1xuXHR2YXIgayA9IGNteWtbM10gLyAxMDA7XG5cdHZhciByO1xuXHR2YXIgZztcblx0dmFyIGI7XG5cblx0ciA9IDEgLSBNYXRoLm1pbigxLCBjICogKDEgLSBrKSArIGspO1xuXHRnID0gMSAtIE1hdGgubWluKDEsIG0gKiAoMSAtIGspICsgayk7XG5cdGIgPSAxIC0gTWF0aC5taW4oMSwgeSAqICgxIC0gaykgKyBrKTtcblxuXHRyZXR1cm4gW3IgKiAyNTUsIGcgKiAyNTUsIGIgKiAyNTVdO1xufTtcblxuY29udmVydC54eXoucmdiID0gZnVuY3Rpb24gKHh5eikge1xuXHR2YXIgeCA9IHh5elswXSAvIDEwMDtcblx0dmFyIHkgPSB4eXpbMV0gLyAxMDA7XG5cdHZhciB6ID0geHl6WzJdIC8gMTAwO1xuXHR2YXIgcjtcblx0dmFyIGc7XG5cdHZhciBiO1xuXG5cdHIgPSAoeCAqIDMuMjQwNikgKyAoeSAqIC0xLjUzNzIpICsgKHogKiAtMC40OTg2KTtcblx0ZyA9ICh4ICogLTAuOTY4OSkgKyAoeSAqIDEuODc1OCkgKyAoeiAqIDAuMDQxNSk7XG5cdGIgPSAoeCAqIDAuMDU1NykgKyAoeSAqIC0wLjIwNDApICsgKHogKiAxLjA1NzApO1xuXG5cdC8vIGFzc3VtZSBzUkdCXG5cdHIgPSByID4gMC4wMDMxMzA4XG5cdFx0PyAoKDEuMDU1ICogTWF0aC5wb3cociwgMS4wIC8gMi40KSkgLSAwLjA1NSlcblx0XHQ6IHIgKiAxMi45MjtcblxuXHRnID0gZyA+IDAuMDAzMTMwOFxuXHRcdD8gKCgxLjA1NSAqIE1hdGgucG93KGcsIDEuMCAvIDIuNCkpIC0gMC4wNTUpXG5cdFx0OiBnICogMTIuOTI7XG5cblx0YiA9IGIgPiAwLjAwMzEzMDhcblx0XHQ/ICgoMS4wNTUgKiBNYXRoLnBvdyhiLCAxLjAgLyAyLjQpKSAtIDAuMDU1KVxuXHRcdDogYiAqIDEyLjkyO1xuXG5cdHIgPSBNYXRoLm1pbihNYXRoLm1heCgwLCByKSwgMSk7XG5cdGcgPSBNYXRoLm1pbihNYXRoLm1heCgwLCBnKSwgMSk7XG5cdGIgPSBNYXRoLm1pbihNYXRoLm1heCgwLCBiKSwgMSk7XG5cblx0cmV0dXJuIFtyICogMjU1LCBnICogMjU1LCBiICogMjU1XTtcbn07XG5cbmNvbnZlcnQueHl6LmxhYiA9IGZ1bmN0aW9uICh4eXopIHtcblx0dmFyIHggPSB4eXpbMF07XG5cdHZhciB5ID0geHl6WzFdO1xuXHR2YXIgeiA9IHh5elsyXTtcblx0dmFyIGw7XG5cdHZhciBhO1xuXHR2YXIgYjtcblxuXHR4IC89IDk1LjA0Nztcblx0eSAvPSAxMDA7XG5cdHogLz0gMTA4Ljg4MztcblxuXHR4ID0geCA+IDAuMDA4ODU2ID8gTWF0aC5wb3coeCwgMSAvIDMpIDogKDcuNzg3ICogeCkgKyAoMTYgLyAxMTYpO1xuXHR5ID0geSA+IDAuMDA4ODU2ID8gTWF0aC5wb3coeSwgMSAvIDMpIDogKDcuNzg3ICogeSkgKyAoMTYgLyAxMTYpO1xuXHR6ID0geiA+IDAuMDA4ODU2ID8gTWF0aC5wb3coeiwgMSAvIDMpIDogKDcuNzg3ICogeikgKyAoMTYgLyAxMTYpO1xuXG5cdGwgPSAoMTE2ICogeSkgLSAxNjtcblx0YSA9IDUwMCAqICh4IC0geSk7XG5cdGIgPSAyMDAgKiAoeSAtIHopO1xuXG5cdHJldHVybiBbbCwgYSwgYl07XG59O1xuXG5jb252ZXJ0LmxhYi54eXogPSBmdW5jdGlvbiAobGFiKSB7XG5cdHZhciBsID0gbGFiWzBdO1xuXHR2YXIgYSA9IGxhYlsxXTtcblx0dmFyIGIgPSBsYWJbMl07XG5cdHZhciB4O1xuXHR2YXIgeTtcblx0dmFyIHo7XG5cblx0eSA9IChsICsgMTYpIC8gMTE2O1xuXHR4ID0gYSAvIDUwMCArIHk7XG5cdHogPSB5IC0gYiAvIDIwMDtcblxuXHR2YXIgeTIgPSBNYXRoLnBvdyh5LCAzKTtcblx0dmFyIHgyID0gTWF0aC5wb3coeCwgMyk7XG5cdHZhciB6MiA9IE1hdGgucG93KHosIDMpO1xuXHR5ID0geTIgPiAwLjAwODg1NiA/IHkyIDogKHkgLSAxNiAvIDExNikgLyA3Ljc4Nztcblx0eCA9IHgyID4gMC4wMDg4NTYgPyB4MiA6ICh4IC0gMTYgLyAxMTYpIC8gNy43ODc7XG5cdHogPSB6MiA+IDAuMDA4ODU2ID8gejIgOiAoeiAtIDE2IC8gMTE2KSAvIDcuNzg3O1xuXG5cdHggKj0gOTUuMDQ3O1xuXHR5ICo9IDEwMDtcblx0eiAqPSAxMDguODgzO1xuXG5cdHJldHVybiBbeCwgeSwgel07XG59O1xuXG5jb252ZXJ0LmxhYi5sY2ggPSBmdW5jdGlvbiAobGFiKSB7XG5cdHZhciBsID0gbGFiWzBdO1xuXHR2YXIgYSA9IGxhYlsxXTtcblx0dmFyIGIgPSBsYWJbMl07XG5cdHZhciBocjtcblx0dmFyIGg7XG5cdHZhciBjO1xuXG5cdGhyID0gTWF0aC5hdGFuMihiLCBhKTtcblx0aCA9IGhyICogMzYwIC8gMiAvIE1hdGguUEk7XG5cblx0aWYgKGggPCAwKSB7XG5cdFx0aCArPSAzNjA7XG5cdH1cblxuXHRjID0gTWF0aC5zcXJ0KGEgKiBhICsgYiAqIGIpO1xuXG5cdHJldHVybiBbbCwgYywgaF07XG59O1xuXG5jb252ZXJ0LmxjaC5sYWIgPSBmdW5jdGlvbiAobGNoKSB7XG5cdHZhciBsID0gbGNoWzBdO1xuXHR2YXIgYyA9IGxjaFsxXTtcblx0dmFyIGggPSBsY2hbMl07XG5cdHZhciBhO1xuXHR2YXIgYjtcblx0dmFyIGhyO1xuXG5cdGhyID0gaCAvIDM2MCAqIDIgKiBNYXRoLlBJO1xuXHRhID0gYyAqIE1hdGguY29zKGhyKTtcblx0YiA9IGMgKiBNYXRoLnNpbihocik7XG5cblx0cmV0dXJuIFtsLCBhLCBiXTtcbn07XG5cbmNvbnZlcnQucmdiLmFuc2kxNiA9IGZ1bmN0aW9uIChhcmdzKSB7XG5cdHZhciByID0gYXJnc1swXTtcblx0dmFyIGcgPSBhcmdzWzFdO1xuXHR2YXIgYiA9IGFyZ3NbMl07XG5cdHZhciB2YWx1ZSA9IDEgaW4gYXJndW1lbnRzID8gYXJndW1lbnRzWzFdIDogY29udmVydC5yZ2IuaHN2KGFyZ3MpWzJdOyAvLyBoc3YgLT4gYW5zaTE2IG9wdGltaXphdGlvblxuXG5cdHZhbHVlID0gTWF0aC5yb3VuZCh2YWx1ZSAvIDUwKTtcblxuXHRpZiAodmFsdWUgPT09IDApIHtcblx0XHRyZXR1cm4gMzA7XG5cdH1cblxuXHR2YXIgYW5zaSA9IDMwXG5cdFx0KyAoKE1hdGgucm91bmQoYiAvIDI1NSkgPDwgMilcblx0XHR8IChNYXRoLnJvdW5kKGcgLyAyNTUpIDw8IDEpXG5cdFx0fCBNYXRoLnJvdW5kKHIgLyAyNTUpKTtcblxuXHRpZiAodmFsdWUgPT09IDIpIHtcblx0XHRhbnNpICs9IDYwO1xuXHR9XG5cblx0cmV0dXJuIGFuc2k7XG59O1xuXG5jb252ZXJ0Lmhzdi5hbnNpMTYgPSBmdW5jdGlvbiAoYXJncykge1xuXHQvLyBvcHRpbWl6YXRpb24gaGVyZTsgd2UgYWxyZWFkeSBrbm93IHRoZSB2YWx1ZSBhbmQgZG9uJ3QgbmVlZCB0byBnZXRcblx0Ly8gaXQgY29udmVydGVkIGZvciB1cy5cblx0cmV0dXJuIGNvbnZlcnQucmdiLmFuc2kxNihjb252ZXJ0Lmhzdi5yZ2IoYXJncyksIGFyZ3NbMl0pO1xufTtcblxuY29udmVydC5yZ2IuYW5zaTI1NiA9IGZ1bmN0aW9uIChhcmdzKSB7XG5cdHZhciByID0gYXJnc1swXTtcblx0dmFyIGcgPSBhcmdzWzFdO1xuXHR2YXIgYiA9IGFyZ3NbMl07XG5cblx0Ly8gd2UgdXNlIHRoZSBleHRlbmRlZCBncmV5c2NhbGUgcGFsZXR0ZSBoZXJlLCB3aXRoIHRoZSBleGNlcHRpb24gb2Zcblx0Ly8gYmxhY2sgYW5kIHdoaXRlLiBub3JtYWwgcGFsZXR0ZSBvbmx5IGhhcyA0IGdyZXlzY2FsZSBzaGFkZXMuXG5cdGlmIChyID09PSBnICYmIGcgPT09IGIpIHtcblx0XHRpZiAociA8IDgpIHtcblx0XHRcdHJldHVybiAxNjtcblx0XHR9XG5cblx0XHRpZiAociA+IDI0OCkge1xuXHRcdFx0cmV0dXJuIDIzMTtcblx0XHR9XG5cblx0XHRyZXR1cm4gTWF0aC5yb3VuZCgoKHIgLSA4KSAvIDI0NykgKiAyNCkgKyAyMzI7XG5cdH1cblxuXHR2YXIgYW5zaSA9IDE2XG5cdFx0KyAoMzYgKiBNYXRoLnJvdW5kKHIgLyAyNTUgKiA1KSlcblx0XHQrICg2ICogTWF0aC5yb3VuZChnIC8gMjU1ICogNSkpXG5cdFx0KyBNYXRoLnJvdW5kKGIgLyAyNTUgKiA1KTtcblxuXHRyZXR1cm4gYW5zaTtcbn07XG5cbmNvbnZlcnQuYW5zaTE2LnJnYiA9IGZ1bmN0aW9uIChhcmdzKSB7XG5cdHZhciBjb2xvciA9IGFyZ3MgJSAxMDtcblxuXHQvLyBoYW5kbGUgZ3JleXNjYWxlXG5cdGlmIChjb2xvciA9PT0gMCB8fCBjb2xvciA9PT0gNykge1xuXHRcdGlmIChhcmdzID4gNTApIHtcblx0XHRcdGNvbG9yICs9IDMuNTtcblx0XHR9XG5cblx0XHRjb2xvciA9IGNvbG9yIC8gMTAuNSAqIDI1NTtcblxuXHRcdHJldHVybiBbY29sb3IsIGNvbG9yLCBjb2xvcl07XG5cdH1cblxuXHR2YXIgbXVsdCA9ICh+fihhcmdzID4gNTApICsgMSkgKiAwLjU7XG5cdHZhciByID0gKChjb2xvciAmIDEpICogbXVsdCkgKiAyNTU7XG5cdHZhciBnID0gKCgoY29sb3IgPj4gMSkgJiAxKSAqIG11bHQpICogMjU1O1xuXHR2YXIgYiA9ICgoKGNvbG9yID4+IDIpICYgMSkgKiBtdWx0KSAqIDI1NTtcblxuXHRyZXR1cm4gW3IsIGcsIGJdO1xufTtcblxuY29udmVydC5hbnNpMjU2LnJnYiA9IGZ1bmN0aW9uIChhcmdzKSB7XG5cdC8vIGhhbmRsZSBncmV5c2NhbGVcblx0aWYgKGFyZ3MgPj0gMjMyKSB7XG5cdFx0dmFyIGMgPSAoYXJncyAtIDIzMikgKiAxMCArIDg7XG5cdFx0cmV0dXJuIFtjLCBjLCBjXTtcblx0fVxuXG5cdGFyZ3MgLT0gMTY7XG5cblx0dmFyIHJlbTtcblx0dmFyIHIgPSBNYXRoLmZsb29yKGFyZ3MgLyAzNikgLyA1ICogMjU1O1xuXHR2YXIgZyA9IE1hdGguZmxvb3IoKHJlbSA9IGFyZ3MgJSAzNikgLyA2KSAvIDUgKiAyNTU7XG5cdHZhciBiID0gKHJlbSAlIDYpIC8gNSAqIDI1NTtcblxuXHRyZXR1cm4gW3IsIGcsIGJdO1xufTtcblxuY29udmVydC5yZ2IuaGV4ID0gZnVuY3Rpb24gKGFyZ3MpIHtcblx0dmFyIGludGVnZXIgPSAoKE1hdGgucm91bmQoYXJnc1swXSkgJiAweEZGKSA8PCAxNilcblx0XHQrICgoTWF0aC5yb3VuZChhcmdzWzFdKSAmIDB4RkYpIDw8IDgpXG5cdFx0KyAoTWF0aC5yb3VuZChhcmdzWzJdKSAmIDB4RkYpO1xuXG5cdHZhciBzdHJpbmcgPSBpbnRlZ2VyLnRvU3RyaW5nKDE2KS50b1VwcGVyQ2FzZSgpO1xuXHRyZXR1cm4gJzAwMDAwMCcuc3Vic3RyaW5nKHN0cmluZy5sZW5ndGgpICsgc3RyaW5nO1xufTtcblxuY29udmVydC5oZXgucmdiID0gZnVuY3Rpb24gKGFyZ3MpIHtcblx0dmFyIG1hdGNoID0gYXJncy50b1N0cmluZygxNikubWF0Y2goL1thLWYwLTldezZ9fFthLWYwLTldezN9L2kpO1xuXHRpZiAoIW1hdGNoKSB7XG5cdFx0cmV0dXJuIFswLCAwLCAwXTtcblx0fVxuXG5cdHZhciBjb2xvclN0cmluZyA9IG1hdGNoWzBdO1xuXG5cdGlmIChtYXRjaFswXS5sZW5ndGggPT09IDMpIHtcblx0XHRjb2xvclN0cmluZyA9IGNvbG9yU3RyaW5nLnNwbGl0KCcnKS5tYXAoZnVuY3Rpb24gKGNoYXIpIHtcblx0XHRcdHJldHVybiBjaGFyICsgY2hhcjtcblx0XHR9KS5qb2luKCcnKTtcblx0fVxuXG5cdHZhciBpbnRlZ2VyID0gcGFyc2VJbnQoY29sb3JTdHJpbmcsIDE2KTtcblx0dmFyIHIgPSAoaW50ZWdlciA+PiAxNikgJiAweEZGO1xuXHR2YXIgZyA9IChpbnRlZ2VyID4+IDgpICYgMHhGRjtcblx0dmFyIGIgPSBpbnRlZ2VyICYgMHhGRjtcblxuXHRyZXR1cm4gW3IsIGcsIGJdO1xufTtcblxuY29udmVydC5yZ2IuaGNnID0gZnVuY3Rpb24gKHJnYikge1xuXHR2YXIgciA9IHJnYlswXSAvIDI1NTtcblx0dmFyIGcgPSByZ2JbMV0gLyAyNTU7XG5cdHZhciBiID0gcmdiWzJdIC8gMjU1O1xuXHR2YXIgbWF4ID0gTWF0aC5tYXgoTWF0aC5tYXgociwgZyksIGIpO1xuXHR2YXIgbWluID0gTWF0aC5taW4oTWF0aC5taW4ociwgZyksIGIpO1xuXHR2YXIgY2hyb21hID0gKG1heCAtIG1pbik7XG5cdHZhciBncmF5c2NhbGU7XG5cdHZhciBodWU7XG5cblx0aWYgKGNocm9tYSA8IDEpIHtcblx0XHRncmF5c2NhbGUgPSBtaW4gLyAoMSAtIGNocm9tYSk7XG5cdH0gZWxzZSB7XG5cdFx0Z3JheXNjYWxlID0gMDtcblx0fVxuXG5cdGlmIChjaHJvbWEgPD0gMCkge1xuXHRcdGh1ZSA9IDA7XG5cdH0gZWxzZVxuXHRpZiAobWF4ID09PSByKSB7XG5cdFx0aHVlID0gKChnIC0gYikgLyBjaHJvbWEpICUgNjtcblx0fSBlbHNlXG5cdGlmIChtYXggPT09IGcpIHtcblx0XHRodWUgPSAyICsgKGIgLSByKSAvIGNocm9tYTtcblx0fSBlbHNlIHtcblx0XHRodWUgPSA0ICsgKHIgLSBnKSAvIGNocm9tYSArIDQ7XG5cdH1cblxuXHRodWUgLz0gNjtcblx0aHVlICU9IDE7XG5cblx0cmV0dXJuIFtodWUgKiAzNjAsIGNocm9tYSAqIDEwMCwgZ3JheXNjYWxlICogMTAwXTtcbn07XG5cbmNvbnZlcnQuaHNsLmhjZyA9IGZ1bmN0aW9uIChoc2wpIHtcblx0dmFyIHMgPSBoc2xbMV0gLyAxMDA7XG5cdHZhciBsID0gaHNsWzJdIC8gMTAwO1xuXHR2YXIgYyA9IDE7XG5cdHZhciBmID0gMDtcblxuXHRpZiAobCA8IDAuNSkge1xuXHRcdGMgPSAyLjAgKiBzICogbDtcblx0fSBlbHNlIHtcblx0XHRjID0gMi4wICogcyAqICgxLjAgLSBsKTtcblx0fVxuXG5cdGlmIChjIDwgMS4wKSB7XG5cdFx0ZiA9IChsIC0gMC41ICogYykgLyAoMS4wIC0gYyk7XG5cdH1cblxuXHRyZXR1cm4gW2hzbFswXSwgYyAqIDEwMCwgZiAqIDEwMF07XG59O1xuXG5jb252ZXJ0Lmhzdi5oY2cgPSBmdW5jdGlvbiAoaHN2KSB7XG5cdHZhciBzID0gaHN2WzFdIC8gMTAwO1xuXHR2YXIgdiA9IGhzdlsyXSAvIDEwMDtcblxuXHR2YXIgYyA9IHMgKiB2O1xuXHR2YXIgZiA9IDA7XG5cblx0aWYgKGMgPCAxLjApIHtcblx0XHRmID0gKHYgLSBjKSAvICgxIC0gYyk7XG5cdH1cblxuXHRyZXR1cm4gW2hzdlswXSwgYyAqIDEwMCwgZiAqIDEwMF07XG59O1xuXG5jb252ZXJ0LmhjZy5yZ2IgPSBmdW5jdGlvbiAoaGNnKSB7XG5cdHZhciBoID0gaGNnWzBdIC8gMzYwO1xuXHR2YXIgYyA9IGhjZ1sxXSAvIDEwMDtcblx0dmFyIGcgPSBoY2dbMl0gLyAxMDA7XG5cblx0aWYgKGMgPT09IDAuMCkge1xuXHRcdHJldHVybiBbZyAqIDI1NSwgZyAqIDI1NSwgZyAqIDI1NV07XG5cdH1cblxuXHR2YXIgcHVyZSA9IFswLCAwLCAwXTtcblx0dmFyIGhpID0gKGggJSAxKSAqIDY7XG5cdHZhciB2ID0gaGkgJSAxO1xuXHR2YXIgdyA9IDEgLSB2O1xuXHR2YXIgbWcgPSAwO1xuXG5cdHN3aXRjaCAoTWF0aC5mbG9vcihoaSkpIHtcblx0XHRjYXNlIDA6XG5cdFx0XHRwdXJlWzBdID0gMTsgcHVyZVsxXSA9IHY7IHB1cmVbMl0gPSAwOyBicmVhaztcblx0XHRjYXNlIDE6XG5cdFx0XHRwdXJlWzBdID0gdzsgcHVyZVsxXSA9IDE7IHB1cmVbMl0gPSAwOyBicmVhaztcblx0XHRjYXNlIDI6XG5cdFx0XHRwdXJlWzBdID0gMDsgcHVyZVsxXSA9IDE7IHB1cmVbMl0gPSB2OyBicmVhaztcblx0XHRjYXNlIDM6XG5cdFx0XHRwdXJlWzBdID0gMDsgcHVyZVsxXSA9IHc7IHB1cmVbMl0gPSAxOyBicmVhaztcblx0XHRjYXNlIDQ6XG5cdFx0XHRwdXJlWzBdID0gdjsgcHVyZVsxXSA9IDA7IHB1cmVbMl0gPSAxOyBicmVhaztcblx0XHRkZWZhdWx0OlxuXHRcdFx0cHVyZVswXSA9IDE7IHB1cmVbMV0gPSAwOyBwdXJlWzJdID0gdztcblx0fVxuXG5cdG1nID0gKDEuMCAtIGMpICogZztcblxuXHRyZXR1cm4gW1xuXHRcdChjICogcHVyZVswXSArIG1nKSAqIDI1NSxcblx0XHQoYyAqIHB1cmVbMV0gKyBtZykgKiAyNTUsXG5cdFx0KGMgKiBwdXJlWzJdICsgbWcpICogMjU1XG5cdF07XG59O1xuXG5jb252ZXJ0LmhjZy5oc3YgPSBmdW5jdGlvbiAoaGNnKSB7XG5cdHZhciBjID0gaGNnWzFdIC8gMTAwO1xuXHR2YXIgZyA9IGhjZ1syXSAvIDEwMDtcblxuXHR2YXIgdiA9IGMgKyBnICogKDEuMCAtIGMpO1xuXHR2YXIgZiA9IDA7XG5cblx0aWYgKHYgPiAwLjApIHtcblx0XHRmID0gYyAvIHY7XG5cdH1cblxuXHRyZXR1cm4gW2hjZ1swXSwgZiAqIDEwMCwgdiAqIDEwMF07XG59O1xuXG5jb252ZXJ0LmhjZy5oc2wgPSBmdW5jdGlvbiAoaGNnKSB7XG5cdHZhciBjID0gaGNnWzFdIC8gMTAwO1xuXHR2YXIgZyA9IGhjZ1syXSAvIDEwMDtcblxuXHR2YXIgbCA9IGcgKiAoMS4wIC0gYykgKyAwLjUgKiBjO1xuXHR2YXIgcyA9IDA7XG5cblx0aWYgKGwgPiAwLjAgJiYgbCA8IDAuNSkge1xuXHRcdHMgPSBjIC8gKDIgKiBsKTtcblx0fSBlbHNlXG5cdGlmIChsID49IDAuNSAmJiBsIDwgMS4wKSB7XG5cdFx0cyA9IGMgLyAoMiAqICgxIC0gbCkpO1xuXHR9XG5cblx0cmV0dXJuIFtoY2dbMF0sIHMgKiAxMDAsIGwgKiAxMDBdO1xufTtcblxuY29udmVydC5oY2cuaHdiID0gZnVuY3Rpb24gKGhjZykge1xuXHR2YXIgYyA9IGhjZ1sxXSAvIDEwMDtcblx0dmFyIGcgPSBoY2dbMl0gLyAxMDA7XG5cdHZhciB2ID0gYyArIGcgKiAoMS4wIC0gYyk7XG5cdHJldHVybiBbaGNnWzBdLCAodiAtIGMpICogMTAwLCAoMSAtIHYpICogMTAwXTtcbn07XG5cbmNvbnZlcnQuaHdiLmhjZyA9IGZ1bmN0aW9uIChod2IpIHtcblx0dmFyIHcgPSBod2JbMV0gLyAxMDA7XG5cdHZhciBiID0gaHdiWzJdIC8gMTAwO1xuXHR2YXIgdiA9IDEgLSBiO1xuXHR2YXIgYyA9IHYgLSB3O1xuXHR2YXIgZyA9IDA7XG5cblx0aWYgKGMgPCAxKSB7XG5cdFx0ZyA9ICh2IC0gYykgLyAoMSAtIGMpO1xuXHR9XG5cblx0cmV0dXJuIFtod2JbMF0sIGMgKiAxMDAsIGcgKiAxMDBdO1xufTtcblxuY29udmVydC5hcHBsZS5yZ2IgPSBmdW5jdGlvbiAoYXBwbGUpIHtcblx0cmV0dXJuIFsoYXBwbGVbMF0gLyA2NTUzNSkgKiAyNTUsIChhcHBsZVsxXSAvIDY1NTM1KSAqIDI1NSwgKGFwcGxlWzJdIC8gNjU1MzUpICogMjU1XTtcbn07XG5cbmNvbnZlcnQucmdiLmFwcGxlID0gZnVuY3Rpb24gKHJnYikge1xuXHRyZXR1cm4gWyhyZ2JbMF0gLyAyNTUpICogNjU1MzUsIChyZ2JbMV0gLyAyNTUpICogNjU1MzUsIChyZ2JbMl0gLyAyNTUpICogNjU1MzVdO1xufTtcblxuY29udmVydC5ncmF5LnJnYiA9IGZ1bmN0aW9uIChhcmdzKSB7XG5cdHJldHVybiBbYXJnc1swXSAvIDEwMCAqIDI1NSwgYXJnc1swXSAvIDEwMCAqIDI1NSwgYXJnc1swXSAvIDEwMCAqIDI1NV07XG59O1xuXG5jb252ZXJ0LmdyYXkuaHNsID0gY29udmVydC5ncmF5LmhzdiA9IGZ1bmN0aW9uIChhcmdzKSB7XG5cdHJldHVybiBbMCwgMCwgYXJnc1swXV07XG59O1xuXG5jb252ZXJ0LmdyYXkuaHdiID0gZnVuY3Rpb24gKGdyYXkpIHtcblx0cmV0dXJuIFswLCAxMDAsIGdyYXlbMF1dO1xufTtcblxuY29udmVydC5ncmF5LmNteWsgPSBmdW5jdGlvbiAoZ3JheSkge1xuXHRyZXR1cm4gWzAsIDAsIDAsIGdyYXlbMF1dO1xufTtcblxuY29udmVydC5ncmF5LmxhYiA9IGZ1bmN0aW9uIChncmF5KSB7XG5cdHJldHVybiBbZ3JheVswXSwgMCwgMF07XG59O1xuXG5jb252ZXJ0LmdyYXkuaGV4ID0gZnVuY3Rpb24gKGdyYXkpIHtcblx0dmFyIHZhbCA9IE1hdGgucm91bmQoZ3JheVswXSAvIDEwMCAqIDI1NSkgJiAweEZGO1xuXHR2YXIgaW50ZWdlciA9ICh2YWwgPDwgMTYpICsgKHZhbCA8PCA4KSArIHZhbDtcblxuXHR2YXIgc3RyaW5nID0gaW50ZWdlci50b1N0cmluZygxNikudG9VcHBlckNhc2UoKTtcblx0cmV0dXJuICcwMDAwMDAnLnN1YnN0cmluZyhzdHJpbmcubGVuZ3RoKSArIHN0cmluZztcbn07XG5cbmNvbnZlcnQucmdiLmdyYXkgPSBmdW5jdGlvbiAocmdiKSB7XG5cdHZhciB2YWwgPSAocmdiWzBdICsgcmdiWzFdICsgcmdiWzJdKSAvIDM7XG5cdHJldHVybiBbdmFsIC8gMjU1ICogMTAwXTtcbn07XG5cblxuLyoqKi8gfSksXG4vKiA3ICovXG4vKioqLyAoZnVuY3Rpb24obW9kdWxlLCBleHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKSB7XG5cblwidXNlIHN0cmljdFwiO1xuXG5cbnZhciBfdHlwZW9mID0gdHlwZW9mIFN5bWJvbCA9PT0gXCJmdW5jdGlvblwiICYmIHR5cGVvZiBTeW1ib2wuaXRlcmF0b3IgPT09IFwic3ltYm9sXCIgPyBmdW5jdGlvbiAob2JqKSB7IHJldHVybiB0eXBlb2Ygb2JqOyB9IDogZnVuY3Rpb24gKG9iaikgeyByZXR1cm4gb2JqICYmIHR5cGVvZiBTeW1ib2wgPT09IFwiZnVuY3Rpb25cIiAmJiBvYmouY29uc3RydWN0b3IgPT09IFN5bWJvbCAmJiBvYmogIT09IFN5bWJvbC5wcm90b3R5cGUgPyBcInN5bWJvbFwiIDogdHlwZW9mIG9iajsgfTtcblxudmFyIF9Db2xvcnBpY2tlciA9IF9fd2VicGFja19yZXF1aXJlX18oOCk7XG5cbnZhciBfQ29sb3JwaWNrZXIyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfQ29sb3JwaWNrZXIpO1xuXG52YXIgX2pxdWVyeSA9IF9fd2VicGFja19yZXF1aXJlX18oMCk7XG5cbnZhciBfanF1ZXJ5MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2pxdWVyeSk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBwbHVnaW4gPSAnY29sb3JwaWNrZXInO1xuXG5fanF1ZXJ5Mi5kZWZhdWx0W3BsdWdpbl0gPSBfQ29sb3JwaWNrZXIyLmRlZmF1bHQ7XG5cbi8vIENvbG9ycGlja2VyIGpRdWVyeSBQbHVnaW4gQVBJXG5fanF1ZXJ5Mi5kZWZhdWx0LmZuW3BsdWdpbl0gPSBmdW5jdGlvbiAob3B0aW9uKSB7XG4gIHZhciBmbkFyZ3MgPSBBcnJheS5wcm90b3R5cGUuc2xpY2UuY2FsbChhcmd1bWVudHMsIDEpLFxuICAgICAgaXNTaW5nbGVFbGVtZW50ID0gdGhpcy5sZW5ndGggPT09IDEsXG4gICAgICByZXR1cm5WYWx1ZSA9IG51bGw7XG5cbiAgdmFyICRlbGVtZW50cyA9IHRoaXMuZWFjaChmdW5jdGlvbiAoKSB7XG4gICAgdmFyICR0aGlzID0gKDAsIF9qcXVlcnkyLmRlZmF1bHQpKHRoaXMpLFxuICAgICAgICBpbnN0ID0gJHRoaXMuZGF0YShwbHVnaW4pLFxuICAgICAgICBvcHRpb25zID0gKHR5cGVvZiBvcHRpb24gPT09ICd1bmRlZmluZWQnID8gJ3VuZGVmaW5lZCcgOiBfdHlwZW9mKG9wdGlvbikpID09PSAnb2JqZWN0JyA/IG9wdGlvbiA6IHt9O1xuXG4gICAgLy8gQ3JlYXRlIGluc3RhbmNlIGlmIGRvZXMgbm90IGV4aXN0XG4gICAgaWYgKCFpbnN0KSB7XG4gICAgICBpbnN0ID0gbmV3IF9Db2xvcnBpY2tlcjIuZGVmYXVsdCh0aGlzLCBvcHRpb25zKTtcbiAgICAgICR0aGlzLmRhdGEocGx1Z2luLCBpbnN0KTtcbiAgICB9XG5cbiAgICBpZiAoIWlzU2luZ2xlRWxlbWVudCkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIHJldHVyblZhbHVlID0gJHRoaXM7XG5cbiAgICBpZiAodHlwZW9mIG9wdGlvbiA9PT0gJ3N0cmluZycpIHtcbiAgICAgIGlmIChvcHRpb24gPT09ICdjb2xvcnBpY2tlcicpIHtcbiAgICAgICAgLy8gUmV0dXJuIGNvbG9ycGlja2VyIGluc3RhbmNlOiBlLmcuIC5jb2xvcnBpY2tlcignY29sb3JwaWNrZXInKVxuICAgICAgICByZXR1cm5WYWx1ZSA9IGluc3Q7XG4gICAgICB9IGVsc2UgaWYgKF9qcXVlcnkyLmRlZmF1bHQuaXNGdW5jdGlvbihpbnN0W29wdGlvbl0pKSB7XG4gICAgICAgIC8vIFJldHVybiBtZXRob2QgY2FsbCByZXR1cm4gdmFsdWU6IGUuZy4gLmNvbG9ycGlja2VyKCdpc0VuYWJsZWQnKVxuICAgICAgICByZXR1cm5WYWx1ZSA9IGluc3Rbb3B0aW9uXS5hcHBseShpbnN0LCBmbkFyZ3MpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgLy8gUmV0dXJuIHByb3BlcnR5IHZhbHVlOiBlLmcuIC5jb2xvcnBpY2tlcignZWxlbWVudCcpXG4gICAgICAgIHJldHVyblZhbHVlID0gaW5zdFtvcHRpb25dO1xuICAgICAgfVxuICAgIH1cbiAgfSk7XG5cbiAgcmV0dXJuIGlzU2luZ2xlRWxlbWVudCA/IHJldHVyblZhbHVlIDogJGVsZW1lbnRzO1xufTtcblxuX2pxdWVyeTIuZGVmYXVsdC5mbltwbHVnaW5dLmNvbnN0cnVjdG9yID0gX0NvbG9ycGlja2VyMi5kZWZhdWx0O1xuXG4vKioqLyB9KSxcbi8qIDggKi9cbi8qKiovIChmdW5jdGlvbihtb2R1bGUsIGV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pIHtcblxuXCJ1c2Ugc3RyaWN0XCI7XG5cblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9jcmVhdGVDbGFzcyA9IGZ1bmN0aW9uICgpIHsgZnVuY3Rpb24gZGVmaW5lUHJvcGVydGllcyh0YXJnZXQsIHByb3BzKSB7IGZvciAodmFyIGkgPSAwOyBpIDwgcHJvcHMubGVuZ3RoOyBpKyspIHsgdmFyIGRlc2NyaXB0b3IgPSBwcm9wc1tpXTsgZGVzY3JpcHRvci5lbnVtZXJhYmxlID0gZGVzY3JpcHRvci5lbnVtZXJhYmxlIHx8IGZhbHNlOyBkZXNjcmlwdG9yLmNvbmZpZ3VyYWJsZSA9IHRydWU7IGlmIChcInZhbHVlXCIgaW4gZGVzY3JpcHRvcikgZGVzY3JpcHRvci53cml0YWJsZSA9IHRydWU7IE9iamVjdC5kZWZpbmVQcm9wZXJ0eSh0YXJnZXQsIGRlc2NyaXB0b3Iua2V5LCBkZXNjcmlwdG9yKTsgfSB9IHJldHVybiBmdW5jdGlvbiAoQ29uc3RydWN0b3IsIHByb3RvUHJvcHMsIHN0YXRpY1Byb3BzKSB7IGlmIChwcm90b1Byb3BzKSBkZWZpbmVQcm9wZXJ0aWVzKENvbnN0cnVjdG9yLnByb3RvdHlwZSwgcHJvdG9Qcm9wcyk7IGlmIChzdGF0aWNQcm9wcykgZGVmaW5lUHJvcGVydGllcyhDb25zdHJ1Y3Rvciwgc3RhdGljUHJvcHMpOyByZXR1cm4gQ29uc3RydWN0b3I7IH07IH0oKTtcblxudmFyIF9FeHRlbnNpb24gPSBfX3dlYnBhY2tfcmVxdWlyZV9fKDEpO1xuXG52YXIgX0V4dGVuc2lvbjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9FeHRlbnNpb24pO1xuXG52YXIgX29wdGlvbnMgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKDMpO1xuXG52YXIgX29wdGlvbnMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfb3B0aW9ucyk7XG5cbnZhciBfZXh0ZW5zaW9ucyA9IF9fd2VicGFja19yZXF1aXJlX18oOSk7XG5cbnZhciBfZXh0ZW5zaW9uczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9leHRlbnNpb25zKTtcblxudmFyIF9qcXVlcnkgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKDApO1xuXG52YXIgX2pxdWVyeTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9qcXVlcnkpO1xuXG52YXIgX1NsaWRlckhhbmRsZXIgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKDEzKTtcblxudmFyIF9TbGlkZXJIYW5kbGVyMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1NsaWRlckhhbmRsZXIpO1xuXG52YXIgX1BvcHVwSGFuZGxlciA9IF9fd2VicGFja19yZXF1aXJlX18oMTQpO1xuXG52YXIgX1BvcHVwSGFuZGxlcjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9Qb3B1cEhhbmRsZXIpO1xuXG52YXIgX0lucHV0SGFuZGxlciA9IF9fd2VicGFja19yZXF1aXJlX18oMTUpO1xuXG52YXIgX0lucHV0SGFuZGxlcjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9JbnB1dEhhbmRsZXIpO1xuXG52YXIgX0NvbG9ySGFuZGxlciA9IF9fd2VicGFja19yZXF1aXJlX18oMjIpO1xuXG52YXIgX0NvbG9ySGFuZGxlcjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9Db2xvckhhbmRsZXIpO1xuXG52YXIgX1BpY2tlckhhbmRsZXIgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKDIzKTtcblxudmFyIF9QaWNrZXJIYW5kbGVyMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1BpY2tlckhhbmRsZXIpO1xuXG52YXIgX0FkZG9uSGFuZGxlciA9IF9fd2VicGFja19yZXF1aXJlX18oMjQpO1xuXG52YXIgX0FkZG9uSGFuZGxlcjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9BZGRvbkhhbmRsZXIpO1xuXG52YXIgX0NvbG9ySXRlbSA9IF9fd2VicGFja19yZXF1aXJlX18oMik7XG5cbnZhciBfQ29sb3JJdGVtMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX0NvbG9ySXRlbSk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbmZ1bmN0aW9uIF9jbGFzc0NhbGxDaGVjayhpbnN0YW5jZSwgQ29uc3RydWN0b3IpIHsgaWYgKCEoaW5zdGFuY2UgaW5zdGFuY2VvZiBDb25zdHJ1Y3RvcikpIHsgdGhyb3cgbmV3IFR5cGVFcnJvcihcIkNhbm5vdCBjYWxsIGEgY2xhc3MgYXMgYSBmdW5jdGlvblwiKTsgfSB9XG5cbnZhciBjb2xvclBpY2tlcklkQ291bnRlciA9IDA7XG5cbnZhciByb290ID0gdHlwZW9mIHNlbGYgIT09ICd1bmRlZmluZWQnID8gc2VsZiA6IHVuZGVmaW5lZDsgLy8gd2luZG93XG5cbi8qKlxuICogQ29sb3JwaWNrZXIgd2lkZ2V0IGNsYXNzXG4gKi9cblxudmFyIENvbG9ycGlja2VyID0gZnVuY3Rpb24gKCkge1xuICBfY3JlYXRlQ2xhc3MoQ29sb3JwaWNrZXIsIFt7XG4gICAga2V5OiAnY29sb3InLFxuXG5cbiAgICAvKipcbiAgICAgKiBJbnRlcm5hbCBjb2xvciBvYmplY3RcbiAgICAgKlxuICAgICAqIEB0eXBlIHtDb2xvcnxudWxsfVxuICAgICAqL1xuICAgIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgICAgcmV0dXJuIHRoaXMuY29sb3JIYW5kbGVyLmNvbG9yO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEludGVybmFsIGNvbG9yIGZvcm1hdFxuICAgICAqXG4gICAgICogQHR5cGUge1N0cmluZ3xudWxsfVxuICAgICAqL1xuXG4gIH0sIHtcbiAgICBrZXk6ICdmb3JtYXQnLFxuICAgIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgICAgcmV0dXJuIHRoaXMuY29sb3JIYW5kbGVyLmZvcm1hdDtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBHZXR0ZXIgb2YgdGhlIHBpY2tlciBlbGVtZW50XG4gICAgICpcbiAgICAgKiBAcmV0dXJucyB7alF1ZXJ5fEhUTUxFbGVtZW50fVxuICAgICAqL1xuXG4gIH0sIHtcbiAgICBrZXk6ICdwaWNrZXInLFxuICAgIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgICAgcmV0dXJuIHRoaXMucGlja2VySGFuZGxlci5waWNrZXI7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogQGZpcmVzIENvbG9ycGlja2VyI2NvbG9ycGlja2VyQ3JlYXRlXG4gICAgICogQHBhcmFtIHtPYmplY3R8U3RyaW5nfSBlbGVtZW50XG4gICAgICogQHBhcmFtIHtPYmplY3R9IG9wdGlvbnNcbiAgICAgKiBAY29uc3RydWN0b3JcbiAgICAgKi9cblxuICB9XSwgW3tcbiAgICBrZXk6ICdDb2xvcicsXG5cbiAgICAvKipcbiAgICAgKiBDb2xvciBjbGFzc1xuICAgICAqXG4gICAgICogQHN0YXRpY1xuICAgICAqIEB0eXBlIHtDb2xvcn1cbiAgICAgKi9cbiAgICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICAgIHJldHVybiBfQ29sb3JJdGVtMi5kZWZhdWx0O1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEV4dGVuc2lvbiBjbGFzc1xuICAgICAqXG4gICAgICogQHN0YXRpY1xuICAgICAqIEB0eXBlIHtFeHRlbnNpb259XG4gICAgICovXG5cbiAgfSwge1xuICAgIGtleTogJ0V4dGVuc2lvbicsXG4gICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgICByZXR1cm4gX0V4dGVuc2lvbjIuZGVmYXVsdDtcbiAgICB9XG4gIH1dKTtcblxuICBmdW5jdGlvbiBDb2xvcnBpY2tlcihlbGVtZW50LCBvcHRpb25zKSB7XG4gICAgX2NsYXNzQ2FsbENoZWNrKHRoaXMsIENvbG9ycGlja2VyKTtcblxuICAgIGNvbG9yUGlja2VySWRDb3VudGVyICs9IDE7XG4gICAgLyoqXG4gICAgICogVGhlIGNvbG9ycGlja2VyIGluc3RhbmNlIG51bWJlclxuICAgICAqIEB0eXBlIHtudW1iZXJ9XG4gICAgICovXG4gICAgdGhpcy5pZCA9IGNvbG9yUGlja2VySWRDb3VudGVyO1xuXG4gICAgLyoqXG4gICAgICogTGF0ZXN0IGNvbG9ycGlja2VyIGV2ZW50XG4gICAgICpcbiAgICAgKiBAdHlwZSB7e25hbWU6IFN0cmluZywgZTogKn19XG4gICAgICovXG4gICAgdGhpcy5sYXN0RXZlbnQgPSB7XG4gICAgICBhbGlhczogbnVsbCxcbiAgICAgIGU6IG51bGxcbiAgICB9O1xuXG4gICAgLyoqXG4gICAgICogVGhlIGVsZW1lbnQgdGhhdCB0aGUgY29sb3JwaWNrZXIgaXMgYm91bmQgdG9cbiAgICAgKlxuICAgICAqIEB0eXBlIHsqfGpRdWVyeX1cbiAgICAgKi9cbiAgICB0aGlzLmVsZW1lbnQgPSAoMCwgX2pxdWVyeTIuZGVmYXVsdCkoZWxlbWVudCkuYWRkQ2xhc3MoJ2NvbG9ycGlja2VyLWVsZW1lbnQnKS5hdHRyKCdkYXRhLWNvbG9ycGlja2VyLWlkJywgdGhpcy5pZCk7XG5cbiAgICAvKipcbiAgICAgKiBAdHlwZSB7ZGVmYXVsdHN9XG4gICAgICovXG4gICAgdGhpcy5vcHRpb25zID0gX2pxdWVyeTIuZGVmYXVsdC5leHRlbmQodHJ1ZSwge30sIF9vcHRpb25zMi5kZWZhdWx0LCBvcHRpb25zLCB0aGlzLmVsZW1lbnQuZGF0YSgpKTtcblxuICAgIC8qKlxuICAgICAqIEB0eXBlIHtib29sZWFufVxuICAgICAqIEBwcml2YXRlXG4gICAgICovXG4gICAgdGhpcy5kaXNhYmxlZCA9IGZhbHNlO1xuXG4gICAgLyoqXG4gICAgICogRXh0ZW5zaW9ucyBhZGRlZCB0byB0aGlzIGluc3RhbmNlXG4gICAgICpcbiAgICAgKiBAdHlwZSB7RXh0ZW5zaW9uW119XG4gICAgICovXG4gICAgdGhpcy5leHRlbnNpb25zID0gW107XG5cbiAgICAvKipcbiAgICAgKiBUaGUgZWxlbWVudCB3aGVyZSB0aGVcbiAgICAgKiBAdHlwZSB7KnxqUXVlcnl9XG4gICAgICovXG4gICAgdGhpcy5jb250YWluZXIgPSB0aGlzLm9wdGlvbnMuY29udGFpbmVyID09PSB0cnVlIHx8IHRoaXMub3B0aW9ucy5jb250YWluZXIgIT09IHRydWUgJiYgdGhpcy5vcHRpb25zLmlubGluZSA9PT0gdHJ1ZSA/IHRoaXMuZWxlbWVudCA6IHRoaXMub3B0aW9ucy5jb250YWluZXI7XG5cbiAgICB0aGlzLmNvbnRhaW5lciA9IHRoaXMuY29udGFpbmVyICE9PSBmYWxzZSA/ICgwLCBfanF1ZXJ5Mi5kZWZhdWx0KSh0aGlzLmNvbnRhaW5lcikgOiBmYWxzZTtcblxuICAgIC8qKlxuICAgICAqIEB0eXBlIHtJbnB1dEhhbmRsZXJ9XG4gICAgICovXG4gICAgdGhpcy5pbnB1dEhhbmRsZXIgPSBuZXcgX0lucHV0SGFuZGxlcjIuZGVmYXVsdCh0aGlzKTtcbiAgICAvKipcbiAgICAgKiBAdHlwZSB7Q29sb3JIYW5kbGVyfVxuICAgICAqL1xuICAgIHRoaXMuY29sb3JIYW5kbGVyID0gbmV3IF9Db2xvckhhbmRsZXIyLmRlZmF1bHQodGhpcyk7XG4gICAgLyoqXG4gICAgICogQHR5cGUge1NsaWRlckhhbmRsZXJ9XG4gICAgICovXG4gICAgdGhpcy5zbGlkZXJIYW5kbGVyID0gbmV3IF9TbGlkZXJIYW5kbGVyMi5kZWZhdWx0KHRoaXMpO1xuICAgIC8qKlxuICAgICAqIEB0eXBlIHtQb3B1cEhhbmRsZXJ9XG4gICAgICovXG4gICAgdGhpcy5wb3B1cEhhbmRsZXIgPSBuZXcgX1BvcHVwSGFuZGxlcjIuZGVmYXVsdCh0aGlzLCByb290KTtcbiAgICAvKipcbiAgICAgKiBAdHlwZSB7UGlja2VySGFuZGxlcn1cbiAgICAgKi9cbiAgICB0aGlzLnBpY2tlckhhbmRsZXIgPSBuZXcgX1BpY2tlckhhbmRsZXIyLmRlZmF1bHQodGhpcyk7XG4gICAgLyoqXG4gICAgICogQHR5cGUge0FkZG9uSGFuZGxlcn1cbiAgICAgKi9cbiAgICB0aGlzLmFkZG9uSGFuZGxlciA9IG5ldyBfQWRkb25IYW5kbGVyMi5kZWZhdWx0KHRoaXMpO1xuXG4gICAgdGhpcy5pbml0KCk7XG5cbiAgICAvLyBFbWl0IGEgY3JlYXRlIGV2ZW50XG4gICAgKDAsIF9qcXVlcnkyLmRlZmF1bHQpKF9qcXVlcnkyLmRlZmF1bHQucHJveHkoZnVuY3Rpb24gKCkge1xuICAgICAgLyoqXG4gICAgICAgKiAoQ29sb3JwaWNrZXIpIFdoZW4gdGhlIENvbG9ycGlja2VyIGluc3RhbmNlIGhhcyBiZWVuIGNyZWF0ZWQgYW5kIHRoZSBET00gaXMgcmVhZHkuXG4gICAgICAgKlxuICAgICAgICogQGV2ZW50IENvbG9ycGlja2VyI2NvbG9ycGlja2VyQ3JlYXRlXG4gICAgICAgKi9cbiAgICAgIHRoaXMudHJpZ2dlcignY29sb3JwaWNrZXJDcmVhdGUnKTtcbiAgICB9LCB0aGlzKSk7XG4gIH1cblxuICAvKipcbiAgICogSW5pdGlhbGl6ZXMgdGhlIHBsdWdpblxuICAgKiBAcHJpdmF0ZVxuICAgKi9cblxuXG4gIF9jcmVhdGVDbGFzcyhDb2xvcnBpY2tlciwgW3tcbiAgICBrZXk6ICdpbml0JyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gaW5pdCgpIHtcbiAgICAgIC8vIEluaXQgYWRkb25cbiAgICAgIHRoaXMuYWRkb25IYW5kbGVyLmJpbmQoKTtcblxuICAgICAgLy8gSW5pdCBpbnB1dFxuICAgICAgdGhpcy5pbnB1dEhhbmRsZXIuYmluZCgpO1xuXG4gICAgICAvLyBJbml0IGV4dGVuc2lvbnMgKGJlZm9yZSBpbml0aWFsaXppbmcgdGhlIGNvbG9yKVxuICAgICAgdGhpcy5pbml0RXh0ZW5zaW9ucygpO1xuXG4gICAgICAvLyBJbml0IGNvbG9yXG4gICAgICB0aGlzLmNvbG9ySGFuZGxlci5iaW5kKCk7XG5cbiAgICAgIC8vIEluaXQgcGlja2VyXG4gICAgICB0aGlzLnBpY2tlckhhbmRsZXIuYmluZCgpO1xuXG4gICAgICAvLyBJbml0IHNsaWRlcnMgYW5kIHBvcHVwXG4gICAgICB0aGlzLnNsaWRlckhhbmRsZXIuYmluZCgpO1xuICAgICAgdGhpcy5wb3B1cEhhbmRsZXIuYmluZCgpO1xuXG4gICAgICAvLyBJbmplY3QgaW50byB0aGUgRE9NICh0aGlzIG1heSBtYWtlIGl0IHZpc2libGUpXG4gICAgICB0aGlzLnBpY2tlckhhbmRsZXIuYXR0YWNoKCk7XG5cbiAgICAgIC8vIFVwZGF0ZSBhbGwgY29tcG9uZW50c1xuICAgICAgdGhpcy51cGRhdGUoKTtcblxuICAgICAgaWYgKHRoaXMuaW5wdXRIYW5kbGVyLmlzRGlzYWJsZWQoKSkge1xuICAgICAgICB0aGlzLmRpc2FibGUoKTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBJbml0aWFsaXplcyB0aGUgcGx1Z2luIGV4dGVuc2lvbnNcbiAgICAgKiBAcHJpdmF0ZVxuICAgICAqL1xuXG4gIH0sIHtcbiAgICBrZXk6ICdpbml0RXh0ZW5zaW9ucycsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGluaXRFeHRlbnNpb25zKCkge1xuICAgICAgdmFyIF90aGlzID0gdGhpcztcblxuICAgICAgaWYgKCFBcnJheS5pc0FycmF5KHRoaXMub3B0aW9ucy5leHRlbnNpb25zKSkge1xuICAgICAgICB0aGlzLm9wdGlvbnMuZXh0ZW5zaW9ucyA9IFtdO1xuICAgICAgfVxuXG4gICAgICBpZiAodGhpcy5vcHRpb25zLmRlYnVnKSB7XG4gICAgICAgIHRoaXMub3B0aW9ucy5leHRlbnNpb25zLnB1c2goeyBuYW1lOiAnZGVidWdnZXInIH0pO1xuICAgICAgfVxuXG4gICAgICAvLyBSZWdpc3RlciBhbmQgaW5zdGFudGlhdGUgZXh0ZW5zaW9uc1xuICAgICAgdGhpcy5vcHRpb25zLmV4dGVuc2lvbnMuZm9yRWFjaChmdW5jdGlvbiAoZXh0KSB7XG4gICAgICAgIF90aGlzLnJlZ2lzdGVyRXh0ZW5zaW9uKENvbG9ycGlja2VyLmV4dGVuc2lvbnNbZXh0Lm5hbWUudG9Mb3dlckNhc2UoKV0sIGV4dC5vcHRpb25zIHx8IHt9KTtcbiAgICAgIH0pO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIENyZWF0ZXMgYW5kIHJlZ2lzdGVycyB0aGUgZ2l2ZW4gZXh0ZW5zaW9uXG4gICAgICpcbiAgICAgKiBAcGFyYW0ge0V4dGVuc2lvbn0gRXh0ZW5zaW9uQ2xhc3MgVGhlIGV4dGVuc2lvbiBjbGFzcyB0byBpbnN0YW50aWF0ZVxuICAgICAqIEBwYXJhbSB7T2JqZWN0fSBbY29uZmlnXSBFeHRlbnNpb24gY29uZmlndXJhdGlvblxuICAgICAqIEByZXR1cm5zIHtFeHRlbnNpb259XG4gICAgICovXG5cbiAgfSwge1xuICAgIGtleTogJ3JlZ2lzdGVyRXh0ZW5zaW9uJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gcmVnaXN0ZXJFeHRlbnNpb24oRXh0ZW5zaW9uQ2xhc3MpIHtcbiAgICAgIHZhciBjb25maWcgPSBhcmd1bWVudHMubGVuZ3RoID4gMSAmJiBhcmd1bWVudHNbMV0gIT09IHVuZGVmaW5lZCA/IGFyZ3VtZW50c1sxXSA6IHt9O1xuXG4gICAgICB2YXIgZXh0ID0gbmV3IEV4dGVuc2lvbkNsYXNzKHRoaXMsIGNvbmZpZyk7XG5cbiAgICAgIHRoaXMuZXh0ZW5zaW9ucy5wdXNoKGV4dCk7XG4gICAgICByZXR1cm4gZXh0O1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIERlc3Ryb3lzIHRoZSBjdXJyZW50IGluc3RhbmNlXG4gICAgICpcbiAgICAgKiBAZmlyZXMgQ29sb3JwaWNrZXIjY29sb3JwaWNrZXJEZXN0cm95XG4gICAgICovXG5cbiAgfSwge1xuICAgIGtleTogJ2Rlc3Ryb3knLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBkZXN0cm95KCkge1xuICAgICAgdmFyIGNvbG9yID0gdGhpcy5jb2xvcjtcblxuICAgICAgdGhpcy5zbGlkZXJIYW5kbGVyLnVuYmluZCgpO1xuICAgICAgdGhpcy5pbnB1dEhhbmRsZXIudW5iaW5kKCk7XG4gICAgICB0aGlzLnBvcHVwSGFuZGxlci51bmJpbmQoKTtcbiAgICAgIHRoaXMuY29sb3JIYW5kbGVyLnVuYmluZCgpO1xuICAgICAgdGhpcy5hZGRvbkhhbmRsZXIudW5iaW5kKCk7XG4gICAgICB0aGlzLnBpY2tlckhhbmRsZXIudW5iaW5kKCk7XG5cbiAgICAgIHRoaXMuZWxlbWVudC5yZW1vdmVDbGFzcygnY29sb3JwaWNrZXItZWxlbWVudCcpLnJlbW92ZURhdGEoJ2NvbG9ycGlja2VyJywgJ2NvbG9yJykub2ZmKCcuY29sb3JwaWNrZXInKTtcblxuICAgICAgLyoqXG4gICAgICAgKiAoQ29sb3JwaWNrZXIpIFdoZW4gdGhlIGluc3RhbmNlIGlzIGRlc3Ryb3llZCB3aXRoIGFsbCBldmVudHMgdW5ib3VuZC5cbiAgICAgICAqXG4gICAgICAgKiBAZXZlbnQgQ29sb3JwaWNrZXIjY29sb3JwaWNrZXJEZXN0cm95XG4gICAgICAgKi9cbiAgICAgIHRoaXMudHJpZ2dlcignY29sb3JwaWNrZXJEZXN0cm95JywgY29sb3IpO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIFNob3dzIHRoZSBjb2xvcnBpY2tlciB3aWRnZXQgaWYgaGlkZGVuLlxuICAgICAqIElmIHRoZSBjb2xvcnBpY2tlciBpcyBkaXNhYmxlZCB0aGlzIGNhbGwgd2lsbCBiZSBpZ25vcmVkLlxuICAgICAqXG4gICAgICogQGZpcmVzIENvbG9ycGlja2VyI2NvbG9ycGlja2VyU2hvd1xuICAgICAqIEBwYXJhbSB7RXZlbnR9IFtlXVxuICAgICAqL1xuXG4gIH0sIHtcbiAgICBrZXk6ICdzaG93JyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gc2hvdyhlKSB7XG4gICAgICB0aGlzLnBvcHVwSGFuZGxlci5zaG93KGUpO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEhpZGVzIHRoZSBjb2xvcnBpY2tlciB3aWRnZXQuXG4gICAgICpcbiAgICAgKiBAZmlyZXMgQ29sb3JwaWNrZXIjY29sb3JwaWNrZXJIaWRlXG4gICAgICogQHBhcmFtIHtFdmVudH0gW2VdXG4gICAgICovXG5cbiAgfSwge1xuICAgIGtleTogJ2hpZGUnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBoaWRlKGUpIHtcbiAgICAgIHRoaXMucG9wdXBIYW5kbGVyLmhpZGUoZSk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogVG9nZ2xlcyB0aGUgY29sb3JwaWNrZXIgYmV0d2VlbiB2aXNpYmxlIGFuZCBoaWRkZW4uXG4gICAgICpcbiAgICAgKiBAZmlyZXMgQ29sb3JwaWNrZXIjY29sb3JwaWNrZXJTaG93XG4gICAgICogQGZpcmVzIENvbG9ycGlja2VyI2NvbG9ycGlja2VySGlkZVxuICAgICAqIEBwYXJhbSB7RXZlbnR9IFtlXVxuICAgICAqL1xuXG4gIH0sIHtcbiAgICBrZXk6ICd0b2dnbGUnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiB0b2dnbGUoZSkge1xuICAgICAgdGhpcy5wb3B1cEhhbmRsZXIudG9nZ2xlKGUpO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIFJldHVybnMgdGhlIGN1cnJlbnQgY29sb3IgdmFsdWUgYXMgc3RyaW5nXG4gICAgICpcbiAgICAgKiBAcGFyYW0ge1N0cmluZ3wqfSBbZGVmYXVsdFZhbHVlXVxuICAgICAqIEByZXR1cm5zIHtTdHJpbmd8Kn1cbiAgICAgKi9cblxuICB9LCB7XG4gICAga2V5OiAnZ2V0VmFsdWUnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBnZXRWYWx1ZSgpIHtcbiAgICAgIHZhciBkZWZhdWx0VmFsdWUgPSBhcmd1bWVudHMubGVuZ3RoID4gMCAmJiBhcmd1bWVudHNbMF0gIT09IHVuZGVmaW5lZCA/IGFyZ3VtZW50c1swXSA6IG51bGw7XG5cbiAgICAgIHZhciB2YWwgPSB0aGlzLmNvbG9ySGFuZGxlci5jb2xvcjtcblxuICAgICAgdmFsID0gdmFsIGluc3RhbmNlb2YgX0NvbG9ySXRlbTIuZGVmYXVsdCA/IHZhbCA6IGRlZmF1bHRWYWx1ZTtcblxuICAgICAgaWYgKHZhbCBpbnN0YW5jZW9mIF9Db2xvckl0ZW0yLmRlZmF1bHQpIHtcbiAgICAgICAgcmV0dXJuIHZhbC5zdHJpbmcodGhpcy5mb3JtYXQpO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gdmFsO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIFNldHMgdGhlIGNvbG9yIG1hbnVhbGx5XG4gICAgICpcbiAgICAgKiBAZmlyZXMgQ29sb3JwaWNrZXIjY29sb3JwaWNrZXJDaGFuZ2VcbiAgICAgKiBAcGFyYW0ge1N0cmluZ3xDb2xvcn0gdmFsXG4gICAgICovXG5cbiAgfSwge1xuICAgIGtleTogJ3NldFZhbHVlJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gc2V0VmFsdWUodmFsKSB7XG4gICAgICBpZiAodGhpcy5pc0Rpc2FibGVkKCkpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuICAgICAgdmFyIGNoID0gdGhpcy5jb2xvckhhbmRsZXI7XG5cbiAgICAgIGlmIChjaC5oYXNDb2xvcigpICYmICEhdmFsICYmIGNoLmNvbG9yLmVxdWFscyh2YWwpIHx8ICFjaC5oYXNDb2xvcigpICYmICF2YWwpIHtcbiAgICAgICAgLy8gc2FtZSBjb2xvciBvciBzdGlsbCBlbXB0eVxuICAgICAgICByZXR1cm47XG4gICAgICB9XG5cbiAgICAgIGNoLmNvbG9yID0gdmFsID8gY2guY3JlYXRlQ29sb3IodmFsLCB0aGlzLm9wdGlvbnMuYXV0b0lucHV0RmFsbGJhY2spIDogbnVsbDtcblxuICAgICAgLyoqXG4gICAgICAgKiAoQ29sb3JwaWNrZXIpIFdoZW4gdGhlIGNvbG9yIGlzIHNldCBwcm9ncmFtbWF0aWNhbGx5IHdpdGggc2V0VmFsdWUoKS5cbiAgICAgICAqXG4gICAgICAgKiBAZXZlbnQgQ29sb3JwaWNrZXIjY29sb3JwaWNrZXJDaGFuZ2VcbiAgICAgICAqL1xuICAgICAgdGhpcy50cmlnZ2VyKCdjb2xvcnBpY2tlckNoYW5nZScsIGNoLmNvbG9yLCB2YWwpO1xuXG4gICAgICAvLyBmb3JjZSB1cGRhdGUgaWYgY29sb3IgaGFzIGNoYW5nZWQgdG8gZW1wdHlcbiAgICAgIHRoaXMudXBkYXRlKCk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogVXBkYXRlcyB0aGUgVUkgYW5kIHRoZSBpbnB1dCBjb2xvciBhY2NvcmRpbmcgdG8gdGhlIGludGVybmFsIGNvbG9yLlxuICAgICAqXG4gICAgICogQGZpcmVzIENvbG9ycGlja2VyI2NvbG9ycGlja2VyVXBkYXRlXG4gICAgICovXG5cbiAgfSwge1xuICAgIGtleTogJ3VwZGF0ZScsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHVwZGF0ZSgpIHtcbiAgICAgIGlmICh0aGlzLmNvbG9ySGFuZGxlci5oYXNDb2xvcigpKSB7XG4gICAgICAgIHRoaXMuaW5wdXRIYW5kbGVyLnVwZGF0ZSgpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdGhpcy5jb2xvckhhbmRsZXIuYXNzdXJlQ29sb3IoKTtcbiAgICAgIH1cblxuICAgICAgdGhpcy5hZGRvbkhhbmRsZXIudXBkYXRlKCk7XG4gICAgICB0aGlzLnBpY2tlckhhbmRsZXIudXBkYXRlKCk7XG5cbiAgICAgIC8qKlxuICAgICAgICogKENvbG9ycGlja2VyKSBGaXJlZCB3aGVuIHRoZSB3aWRnZXQgaXMgdXBkYXRlZC5cbiAgICAgICAqXG4gICAgICAgKiBAZXZlbnQgQ29sb3JwaWNrZXIjY29sb3JwaWNrZXJVcGRhdGVcbiAgICAgICAqL1xuICAgICAgdGhpcy50cmlnZ2VyKCdjb2xvcnBpY2tlclVwZGF0ZScpO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEVuYWJsZXMgdGhlIHdpZGdldCBhbmQgdGhlIGlucHV0IGlmIGFueVxuICAgICAqXG4gICAgICogQGZpcmVzIENvbG9ycGlja2VyI2NvbG9ycGlja2VyRW5hYmxlXG4gICAgICogQHJldHVybnMge2Jvb2xlYW59XG4gICAgICovXG5cbiAgfSwge1xuICAgIGtleTogJ2VuYWJsZScsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGVuYWJsZSgpIHtcbiAgICAgIHRoaXMuaW5wdXRIYW5kbGVyLmVuYWJsZSgpO1xuICAgICAgdGhpcy5kaXNhYmxlZCA9IGZhbHNlO1xuICAgICAgdGhpcy5waWNrZXIucmVtb3ZlQ2xhc3MoJ2NvbG9ycGlja2VyLWRpc2FibGVkJyk7XG5cbiAgICAgIC8qKlxuICAgICAgICogKENvbG9ycGlja2VyKSBXaGVuIHRoZSB3aWRnZXQgaGFzIGJlZW4gZW5hYmxlZC5cbiAgICAgICAqXG4gICAgICAgKiBAZXZlbnQgQ29sb3JwaWNrZXIjY29sb3JwaWNrZXJFbmFibGVcbiAgICAgICAqL1xuICAgICAgdGhpcy50cmlnZ2VyKCdjb2xvcnBpY2tlckVuYWJsZScpO1xuICAgICAgcmV0dXJuIHRydWU7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogRGlzYWJsZXMgdGhlIHdpZGdldCBhbmQgdGhlIGlucHV0IGlmIGFueVxuICAgICAqXG4gICAgICogQGZpcmVzIENvbG9ycGlja2VyI2NvbG9ycGlja2VyRGlzYWJsZVxuICAgICAqIEByZXR1cm5zIHtib29sZWFufVxuICAgICAqL1xuXG4gIH0sIHtcbiAgICBrZXk6ICdkaXNhYmxlJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gZGlzYWJsZSgpIHtcbiAgICAgIHRoaXMuaW5wdXRIYW5kbGVyLmRpc2FibGUoKTtcbiAgICAgIHRoaXMuZGlzYWJsZWQgPSB0cnVlO1xuICAgICAgdGhpcy5waWNrZXIuYWRkQ2xhc3MoJ2NvbG9ycGlja2VyLWRpc2FibGVkJyk7XG5cbiAgICAgIC8qKlxuICAgICAgICogKENvbG9ycGlja2VyKSBXaGVuIHRoZSB3aWRnZXQgaGFzIGJlZW4gZGlzYWJsZWQuXG4gICAgICAgKlxuICAgICAgICogQGV2ZW50IENvbG9ycGlja2VyI2NvbG9ycGlja2VyRGlzYWJsZVxuICAgICAgICovXG4gICAgICB0aGlzLnRyaWdnZXIoJ2NvbG9ycGlja2VyRGlzYWJsZScpO1xuICAgICAgcmV0dXJuIHRydWU7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogUmV0dXJucyB0cnVlIGlmIHRoaXMgaW5zdGFuY2UgaXMgZW5hYmxlZFxuICAgICAqIEByZXR1cm5zIHtib29sZWFufVxuICAgICAqL1xuXG4gIH0sIHtcbiAgICBrZXk6ICdpc0VuYWJsZWQnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBpc0VuYWJsZWQoKSB7XG4gICAgICByZXR1cm4gIXRoaXMuaXNEaXNhYmxlZCgpO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIFJldHVybnMgdHJ1ZSBpZiB0aGlzIGluc3RhbmNlIGlzIGRpc2FibGVkXG4gICAgICogQHJldHVybnMge2Jvb2xlYW59XG4gICAgICovXG5cbiAgfSwge1xuICAgIGtleTogJ2lzRGlzYWJsZWQnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBpc0Rpc2FibGVkKCkge1xuICAgICAgcmV0dXJuIHRoaXMuZGlzYWJsZWQgPT09IHRydWU7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogVHJpZ2dlcnMgYSBDb2xvcnBpY2tlciBldmVudC5cbiAgICAgKlxuICAgICAqIEBwYXJhbSBldmVudE5hbWVcbiAgICAgKiBAcGFyYW0gY29sb3JcbiAgICAgKiBAcGFyYW0gdmFsdWVcbiAgICAgKi9cblxuICB9LCB7XG4gICAga2V5OiAndHJpZ2dlcicsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHRyaWdnZXIoZXZlbnROYW1lKSB7XG4gICAgICB2YXIgY29sb3IgPSBhcmd1bWVudHMubGVuZ3RoID4gMSAmJiBhcmd1bWVudHNbMV0gIT09IHVuZGVmaW5lZCA/IGFyZ3VtZW50c1sxXSA6IG51bGw7XG4gICAgICB2YXIgdmFsdWUgPSBhcmd1bWVudHMubGVuZ3RoID4gMiAmJiBhcmd1bWVudHNbMl0gIT09IHVuZGVmaW5lZCA/IGFyZ3VtZW50c1syXSA6IG51bGw7XG5cbiAgICAgIHRoaXMuZWxlbWVudC50cmlnZ2VyKHtcbiAgICAgICAgdHlwZTogZXZlbnROYW1lLFxuICAgICAgICBjb2xvcnBpY2tlcjogdGhpcyxcbiAgICAgICAgY29sb3I6IGNvbG9yID8gY29sb3IgOiB0aGlzLmNvbG9yLFxuICAgICAgICB2YWx1ZTogdmFsdWUgPyB2YWx1ZSA6IHRoaXMuZ2V0VmFsdWUoKVxuICAgICAgfSk7XG4gICAgfVxuICB9XSk7XG5cbiAgcmV0dXJuIENvbG9ycGlja2VyO1xufSgpO1xuXG4vKipcbiAqIENvbG9ycGlja2VyIGV4dGVuc2lvbiBjbGFzc2VzLCBpbmRleGVkIGJ5IGV4dGVuc2lvbiBuYW1lXG4gKlxuICogQHN0YXRpY1xuICogQHR5cGUge09iamVjdH0gYSBtYXAgYmV0d2VlbiB0aGUgZXh0ZW5zaW9uIG5hbWUgYW5kIGl0cyBjbGFzc1xuICovXG5cblxuQ29sb3JwaWNrZXIuZXh0ZW5zaW9ucyA9IF9leHRlbnNpb25zMi5kZWZhdWx0O1xuXG5leHBvcnRzLmRlZmF1bHQgPSBDb2xvcnBpY2tlcjtcbm1vZHVsZS5leHBvcnRzID0gZXhwb3J0cy5kZWZhdWx0O1xuXG4vKioqLyB9KSxcbi8qIDkgKi9cbi8qKiovIChmdW5jdGlvbihtb2R1bGUsIGV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pIHtcblxuXCJ1c2Ugc3RyaWN0XCI7XG5cblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcbmV4cG9ydHMuUGFsZXR0ZSA9IGV4cG9ydHMuU3dhdGNoZXMgPSBleHBvcnRzLlByZXZpZXcgPSBleHBvcnRzLkRlYnVnZ2VyID0gdW5kZWZpbmVkO1xuXG52YXIgX0RlYnVnZ2VyID0gX193ZWJwYWNrX3JlcXVpcmVfXygxMCk7XG5cbnZhciBfRGVidWdnZXIyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfRGVidWdnZXIpO1xuXG52YXIgX1ByZXZpZXcgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKDExKTtcblxudmFyIF9QcmV2aWV3MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1ByZXZpZXcpO1xuXG52YXIgX1N3YXRjaGVzID0gX193ZWJwYWNrX3JlcXVpcmVfXygxMik7XG5cbnZhciBfU3dhdGNoZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfU3dhdGNoZXMpO1xuXG52YXIgX1BhbGV0dGUgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKDQpO1xuXG52YXIgX1BhbGV0dGUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfUGFsZXR0ZSk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbmV4cG9ydHMuRGVidWdnZXIgPSBfRGVidWdnZXIyLmRlZmF1bHQ7XG5leHBvcnRzLlByZXZpZXcgPSBfUHJldmlldzIuZGVmYXVsdDtcbmV4cG9ydHMuU3dhdGNoZXMgPSBfU3dhdGNoZXMyLmRlZmF1bHQ7XG5leHBvcnRzLlBhbGV0dGUgPSBfUGFsZXR0ZTIuZGVmYXVsdDtcbmV4cG9ydHMuZGVmYXVsdCA9IHtcbiAgJ2RlYnVnZ2VyJzogX0RlYnVnZ2VyMi5kZWZhdWx0LFxuICAncHJldmlldyc6IF9QcmV2aWV3Mi5kZWZhdWx0LFxuICAnc3dhdGNoZXMnOiBfU3dhdGNoZXMyLmRlZmF1bHQsXG4gICdwYWxldHRlJzogX1BhbGV0dGUyLmRlZmF1bHRcbn07XG5cbi8qKiovIH0pLFxuLyogMTAgKi9cbi8qKiovIChmdW5jdGlvbihtb2R1bGUsIGV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pIHtcblxuXCJ1c2Ugc3RyaWN0XCI7XG5cblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9jcmVhdGVDbGFzcyA9IGZ1bmN0aW9uICgpIHsgZnVuY3Rpb24gZGVmaW5lUHJvcGVydGllcyh0YXJnZXQsIHByb3BzKSB7IGZvciAodmFyIGkgPSAwOyBpIDwgcHJvcHMubGVuZ3RoOyBpKyspIHsgdmFyIGRlc2NyaXB0b3IgPSBwcm9wc1tpXTsgZGVzY3JpcHRvci5lbnVtZXJhYmxlID0gZGVzY3JpcHRvci5lbnVtZXJhYmxlIHx8IGZhbHNlOyBkZXNjcmlwdG9yLmNvbmZpZ3VyYWJsZSA9IHRydWU7IGlmIChcInZhbHVlXCIgaW4gZGVzY3JpcHRvcikgZGVzY3JpcHRvci53cml0YWJsZSA9IHRydWU7IE9iamVjdC5kZWZpbmVQcm9wZXJ0eSh0YXJnZXQsIGRlc2NyaXB0b3Iua2V5LCBkZXNjcmlwdG9yKTsgfSB9IHJldHVybiBmdW5jdGlvbiAoQ29uc3RydWN0b3IsIHByb3RvUHJvcHMsIHN0YXRpY1Byb3BzKSB7IGlmIChwcm90b1Byb3BzKSBkZWZpbmVQcm9wZXJ0aWVzKENvbnN0cnVjdG9yLnByb3RvdHlwZSwgcHJvdG9Qcm9wcyk7IGlmIChzdGF0aWNQcm9wcykgZGVmaW5lUHJvcGVydGllcyhDb25zdHJ1Y3Rvciwgc3RhdGljUHJvcHMpOyByZXR1cm4gQ29uc3RydWN0b3I7IH07IH0oKTtcblxudmFyIF9nZXQgPSBmdW5jdGlvbiBnZXQob2JqZWN0LCBwcm9wZXJ0eSwgcmVjZWl2ZXIpIHsgaWYgKG9iamVjdCA9PT0gbnVsbCkgb2JqZWN0ID0gRnVuY3Rpb24ucHJvdG90eXBlOyB2YXIgZGVzYyA9IE9iamVjdC5nZXRPd25Qcm9wZXJ0eURlc2NyaXB0b3Iob2JqZWN0LCBwcm9wZXJ0eSk7IGlmIChkZXNjID09PSB1bmRlZmluZWQpIHsgdmFyIHBhcmVudCA9IE9iamVjdC5nZXRQcm90b3R5cGVPZihvYmplY3QpOyBpZiAocGFyZW50ID09PSBudWxsKSB7IHJldHVybiB1bmRlZmluZWQ7IH0gZWxzZSB7IHJldHVybiBnZXQocGFyZW50LCBwcm9wZXJ0eSwgcmVjZWl2ZXIpOyB9IH0gZWxzZSBpZiAoXCJ2YWx1ZVwiIGluIGRlc2MpIHsgcmV0dXJuIGRlc2MudmFsdWU7IH0gZWxzZSB7IHZhciBnZXR0ZXIgPSBkZXNjLmdldDsgaWYgKGdldHRlciA9PT0gdW5kZWZpbmVkKSB7IHJldHVybiB1bmRlZmluZWQ7IH0gcmV0dXJuIGdldHRlci5jYWxsKHJlY2VpdmVyKTsgfSB9O1xuXG52YXIgX0V4dGVuc2lvbjIgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKDEpO1xuXG52YXIgX0V4dGVuc2lvbjMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9FeHRlbnNpb24yKTtcblxudmFyIF9qcXVlcnkgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKDApO1xuXG52YXIgX2pxdWVyeTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9qcXVlcnkpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG5mdW5jdGlvbiBfY2xhc3NDYWxsQ2hlY2soaW5zdGFuY2UsIENvbnN0cnVjdG9yKSB7IGlmICghKGluc3RhbmNlIGluc3RhbmNlb2YgQ29uc3RydWN0b3IpKSB7IHRocm93IG5ldyBUeXBlRXJyb3IoXCJDYW5ub3QgY2FsbCBhIGNsYXNzIGFzIGEgZnVuY3Rpb25cIik7IH0gfVxuXG5mdW5jdGlvbiBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybihzZWxmLCBjYWxsKSB7IGlmICghc2VsZikgeyB0aHJvdyBuZXcgUmVmZXJlbmNlRXJyb3IoXCJ0aGlzIGhhc24ndCBiZWVuIGluaXRpYWxpc2VkIC0gc3VwZXIoKSBoYXNuJ3QgYmVlbiBjYWxsZWRcIik7IH0gcmV0dXJuIGNhbGwgJiYgKHR5cGVvZiBjYWxsID09PSBcIm9iamVjdFwiIHx8IHR5cGVvZiBjYWxsID09PSBcImZ1bmN0aW9uXCIpID8gY2FsbCA6IHNlbGY7IH1cblxuZnVuY3Rpb24gX2luaGVyaXRzKHN1YkNsYXNzLCBzdXBlckNsYXNzKSB7IGlmICh0eXBlb2Ygc3VwZXJDbGFzcyAhPT0gXCJmdW5jdGlvblwiICYmIHN1cGVyQ2xhc3MgIT09IG51bGwpIHsgdGhyb3cgbmV3IFR5cGVFcnJvcihcIlN1cGVyIGV4cHJlc3Npb24gbXVzdCBlaXRoZXIgYmUgbnVsbCBvciBhIGZ1bmN0aW9uLCBub3QgXCIgKyB0eXBlb2Ygc3VwZXJDbGFzcyk7IH0gc3ViQ2xhc3MucHJvdG90eXBlID0gT2JqZWN0LmNyZWF0ZShzdXBlckNsYXNzICYmIHN1cGVyQ2xhc3MucHJvdG90eXBlLCB7IGNvbnN0cnVjdG9yOiB7IHZhbHVlOiBzdWJDbGFzcywgZW51bWVyYWJsZTogZmFsc2UsIHdyaXRhYmxlOiB0cnVlLCBjb25maWd1cmFibGU6IHRydWUgfSB9KTsgaWYgKHN1cGVyQ2xhc3MpIE9iamVjdC5zZXRQcm90b3R5cGVPZiA/IE9iamVjdC5zZXRQcm90b3R5cGVPZihzdWJDbGFzcywgc3VwZXJDbGFzcykgOiBzdWJDbGFzcy5fX3Byb3RvX18gPSBzdXBlckNsYXNzOyB9XG5cbi8qKlxuICogRGVidWdnZXIgZXh0ZW5zaW9uIGNsYXNzXG4gKiBAYWxpYXMgRGVidWdnZXJFeHRlbnNpb25cbiAqIEBpZ25vcmVcbiAqL1xudmFyIERlYnVnZ2VyID0gZnVuY3Rpb24gKF9FeHRlbnNpb24pIHtcbiAgX2luaGVyaXRzKERlYnVnZ2VyLCBfRXh0ZW5zaW9uKTtcblxuICBmdW5jdGlvbiBEZWJ1Z2dlcihjb2xvcnBpY2tlcikge1xuICAgIHZhciBvcHRpb25zID0gYXJndW1lbnRzLmxlbmd0aCA+IDEgJiYgYXJndW1lbnRzWzFdICE9PSB1bmRlZmluZWQgPyBhcmd1bWVudHNbMV0gOiB7fTtcblxuICAgIF9jbGFzc0NhbGxDaGVjayh0aGlzLCBEZWJ1Z2dlcik7XG5cbiAgICAvKipcbiAgICAgKiBAdHlwZSB7bnVtYmVyfVxuICAgICAqL1xuICAgIHZhciBfdGhpcyA9IF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuKHRoaXMsIChEZWJ1Z2dlci5fX3Byb3RvX18gfHwgT2JqZWN0LmdldFByb3RvdHlwZU9mKERlYnVnZ2VyKSkuY2FsbCh0aGlzLCBjb2xvcnBpY2tlciwgb3B0aW9ucykpO1xuXG4gICAgX3RoaXMuZXZlbnRDb3VudGVyID0gMDtcbiAgICBpZiAoX3RoaXMuY29sb3JwaWNrZXIuaW5wdXRIYW5kbGVyLmhhc0lucHV0KCkpIHtcbiAgICAgIF90aGlzLmNvbG9ycGlja2VyLmlucHV0SGFuZGxlci5pbnB1dC5vbignY2hhbmdlLmNvbG9ycGlja2VyLWV4dCcsIF9qcXVlcnkyLmRlZmF1bHQucHJveHkoX3RoaXMub25DaGFuZ2VJbnB1dCwgX3RoaXMpKTtcbiAgICB9XG4gICAgcmV0dXJuIF90aGlzO1xuICB9XG5cbiAgLyoqXG4gICAqIEBmaXJlcyBEZWJ1Z2dlckV4dGVuc2lvbiNjb2xvcnBpY2tlckRlYnVnXG4gICAqIEBwYXJhbSB7c3RyaW5nfSBldmVudE5hbWVcbiAgICogQHBhcmFtIHsqfSBhcmdzXG4gICAqL1xuXG5cbiAgX2NyZWF0ZUNsYXNzKERlYnVnZ2VyLCBbe1xuICAgIGtleTogJ2xvZycsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGxvZyhldmVudE5hbWUpIHtcbiAgICAgIHZhciBfY29uc29sZTtcblxuICAgICAgZm9yICh2YXIgX2xlbiA9IGFyZ3VtZW50cy5sZW5ndGgsIGFyZ3MgPSBBcnJheShfbGVuID4gMSA/IF9sZW4gLSAxIDogMCksIF9rZXkgPSAxOyBfa2V5IDwgX2xlbjsgX2tleSsrKSB7XG4gICAgICAgIGFyZ3NbX2tleSAtIDFdID0gYXJndW1lbnRzW19rZXldO1xuICAgICAgfVxuXG4gICAgICB0aGlzLmV2ZW50Q291bnRlciArPSAxO1xuXG4gICAgICB2YXIgbG9nTWVzc2FnZSA9ICcjJyArIHRoaXMuZXZlbnRDb3VudGVyICsgJzogQ29sb3JwaWNrZXIjJyArIHRoaXMuY29sb3JwaWNrZXIuaWQgKyAnIFsnICsgZXZlbnROYW1lICsgJ10nO1xuXG4gICAgICAoX2NvbnNvbGUgPSBjb25zb2xlKS5kZWJ1Zy5hcHBseShfY29uc29sZSwgW2xvZ01lc3NhZ2VdLmNvbmNhdChhcmdzKSk7XG5cbiAgICAgIC8qKlxuICAgICAgICogV2hlbmV2ZXIgdGhlIGRlYnVnZ2VyIGxvZ3MgYW4gZXZlbnQsIHRoaXMgb3RoZXIgZXZlbnQgaXMgZW1pdHRlZC5cbiAgICAgICAqXG4gICAgICAgKiBAZXZlbnQgRGVidWdnZXJFeHRlbnNpb24jY29sb3JwaWNrZXJEZWJ1Z1xuICAgICAgICogQHR5cGUge29iamVjdH0gVGhlIGV2ZW50IG9iamVjdFxuICAgICAgICogQHByb3BlcnR5IHtDb2xvcnBpY2tlcn0gY29sb3JwaWNrZXIgVGhlIENvbG9ycGlja2VyIGluc3RhbmNlXG4gICAgICAgKiBAcHJvcGVydHkge0NvbG9ySXRlbX0gY29sb3IgVGhlIGNvbG9yIGluc3RhbmNlXG4gICAgICAgKiBAcHJvcGVydHkge3tkZWJ1Z2dlcjogRGVidWdnZXJFeHRlbnNpb24sIGV2ZW50TmFtZTogU3RyaW5nLCBsb2dBcmdzOiBBcnJheSwgbG9nTWVzc2FnZTogU3RyaW5nfX0gZGVidWdcbiAgICAgICAqICBUaGUgZGVidWcgaW5mb1xuICAgICAgICovXG4gICAgICB0aGlzLmNvbG9ycGlja2VyLmVsZW1lbnQudHJpZ2dlcih7XG4gICAgICAgIHR5cGU6ICdjb2xvcnBpY2tlckRlYnVnJyxcbiAgICAgICAgY29sb3JwaWNrZXI6IHRoaXMuY29sb3JwaWNrZXIsXG4gICAgICAgIGNvbG9yOiB0aGlzLmNvbG9yLFxuICAgICAgICB2YWx1ZTogbnVsbCxcbiAgICAgICAgZGVidWc6IHtcbiAgICAgICAgICBkZWJ1Z2dlcjogdGhpcyxcbiAgICAgICAgICBldmVudE5hbWU6IGV2ZW50TmFtZSxcbiAgICAgICAgICBsb2dBcmdzOiBhcmdzLFxuICAgICAgICAgIGxvZ01lc3NhZ2U6IGxvZ01lc3NhZ2VcbiAgICAgICAgfVxuICAgICAgfSk7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAncmVzb2x2ZUNvbG9yJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gcmVzb2x2ZUNvbG9yKGNvbG9yKSB7XG4gICAgICB2YXIgcmVhbENvbG9yID0gYXJndW1lbnRzLmxlbmd0aCA+IDEgJiYgYXJndW1lbnRzWzFdICE9PSB1bmRlZmluZWQgPyBhcmd1bWVudHNbMV0gOiB0cnVlO1xuXG4gICAgICB0aGlzLmxvZygncmVzb2x2ZUNvbG9yKCknLCBjb2xvciwgcmVhbENvbG9yKTtcbiAgICAgIHJldHVybiBmYWxzZTtcbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdvbkNyZWF0ZScsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIG9uQ3JlYXRlKGV2ZW50KSB7XG4gICAgICB0aGlzLmxvZygnY29sb3JwaWNrZXJDcmVhdGUnKTtcbiAgICAgIHJldHVybiBfZ2V0KERlYnVnZ2VyLnByb3RvdHlwZS5fX3Byb3RvX18gfHwgT2JqZWN0LmdldFByb3RvdHlwZU9mKERlYnVnZ2VyLnByb3RvdHlwZSksICdvbkNyZWF0ZScsIHRoaXMpLmNhbGwodGhpcywgZXZlbnQpO1xuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ29uRGVzdHJveScsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIG9uRGVzdHJveShldmVudCkge1xuICAgICAgdGhpcy5sb2coJ2NvbG9ycGlja2VyRGVzdHJveScpO1xuICAgICAgdGhpcy5ldmVudENvdW50ZXIgPSAwO1xuXG4gICAgICBpZiAodGhpcy5jb2xvcnBpY2tlci5pbnB1dEhhbmRsZXIuaGFzSW5wdXQoKSkge1xuICAgICAgICB0aGlzLmNvbG9ycGlja2VyLmlucHV0SGFuZGxlci5pbnB1dC5vZmYoJy5jb2xvcnBpY2tlci1leHQnKTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIF9nZXQoRGVidWdnZXIucHJvdG90eXBlLl9fcHJvdG9fXyB8fCBPYmplY3QuZ2V0UHJvdG90eXBlT2YoRGVidWdnZXIucHJvdG90eXBlKSwgJ29uRGVzdHJveScsIHRoaXMpLmNhbGwodGhpcywgZXZlbnQpO1xuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ29uVXBkYXRlJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gb25VcGRhdGUoZXZlbnQpIHtcbiAgICAgIHRoaXMubG9nKCdjb2xvcnBpY2tlclVwZGF0ZScpO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEBsaXN0ZW5zIENvbG9ycGlja2VyI2NoYW5nZVxuICAgICAqIEBwYXJhbSB7RXZlbnR9IGV2ZW50XG4gICAgICovXG5cbiAgfSwge1xuICAgIGtleTogJ29uQ2hhbmdlSW5wdXQnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBvbkNoYW5nZUlucHV0KGV2ZW50KSB7XG4gICAgICB0aGlzLmxvZygnaW5wdXQ6Y2hhbmdlLmNvbG9ycGlja2VyJywgZXZlbnQudmFsdWUsIGV2ZW50LmNvbG9yKTtcbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdvbkNoYW5nZScsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIG9uQ2hhbmdlKGV2ZW50KSB7XG4gICAgICB0aGlzLmxvZygnY29sb3JwaWNrZXJDaGFuZ2UnLCBldmVudC52YWx1ZSwgZXZlbnQuY29sb3IpO1xuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ29uSW52YWxpZCcsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIG9uSW52YWxpZChldmVudCkge1xuICAgICAgdGhpcy5sb2coJ2NvbG9ycGlja2VySW52YWxpZCcsIGV2ZW50LnZhbHVlLCBldmVudC5jb2xvcik7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnb25IaWRlJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gb25IaWRlKGV2ZW50KSB7XG4gICAgICB0aGlzLmxvZygnY29sb3JwaWNrZXJIaWRlJyk7XG4gICAgICB0aGlzLmV2ZW50Q291bnRlciA9IDA7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnb25TaG93JyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gb25TaG93KGV2ZW50KSB7XG4gICAgICB0aGlzLmxvZygnY29sb3JwaWNrZXJTaG93Jyk7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnb25EaXNhYmxlJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gb25EaXNhYmxlKGV2ZW50KSB7XG4gICAgICB0aGlzLmxvZygnY29sb3JwaWNrZXJEaXNhYmxlJyk7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnb25FbmFibGUnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBvbkVuYWJsZShldmVudCkge1xuICAgICAgdGhpcy5sb2coJ2NvbG9ycGlja2VyRW5hYmxlJyk7XG4gICAgfVxuICB9XSk7XG5cbiAgcmV0dXJuIERlYnVnZ2VyO1xufShfRXh0ZW5zaW9uMy5kZWZhdWx0KTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gRGVidWdnZXI7XG5tb2R1bGUuZXhwb3J0cyA9IGV4cG9ydHMuZGVmYXVsdDtcblxuLyoqKi8gfSksXG4vKiAxMSAqL1xuLyoqKi8gKGZ1bmN0aW9uKG1vZHVsZSwgZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXykge1xuXG5cInVzZSBzdHJpY3RcIjtcblxuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX2NyZWF0ZUNsYXNzID0gZnVuY3Rpb24gKCkgeyBmdW5jdGlvbiBkZWZpbmVQcm9wZXJ0aWVzKHRhcmdldCwgcHJvcHMpIHsgZm9yICh2YXIgaSA9IDA7IGkgPCBwcm9wcy5sZW5ndGg7IGkrKykgeyB2YXIgZGVzY3JpcHRvciA9IHByb3BzW2ldOyBkZXNjcmlwdG9yLmVudW1lcmFibGUgPSBkZXNjcmlwdG9yLmVudW1lcmFibGUgfHwgZmFsc2U7IGRlc2NyaXB0b3IuY29uZmlndXJhYmxlID0gdHJ1ZTsgaWYgKFwidmFsdWVcIiBpbiBkZXNjcmlwdG9yKSBkZXNjcmlwdG9yLndyaXRhYmxlID0gdHJ1ZTsgT2JqZWN0LmRlZmluZVByb3BlcnR5KHRhcmdldCwgZGVzY3JpcHRvci5rZXksIGRlc2NyaXB0b3IpOyB9IH0gcmV0dXJuIGZ1bmN0aW9uIChDb25zdHJ1Y3RvciwgcHJvdG9Qcm9wcywgc3RhdGljUHJvcHMpIHsgaWYgKHByb3RvUHJvcHMpIGRlZmluZVByb3BlcnRpZXMoQ29uc3RydWN0b3IucHJvdG90eXBlLCBwcm90b1Byb3BzKTsgaWYgKHN0YXRpY1Byb3BzKSBkZWZpbmVQcm9wZXJ0aWVzKENvbnN0cnVjdG9yLCBzdGF0aWNQcm9wcyk7IHJldHVybiBDb25zdHJ1Y3RvcjsgfTsgfSgpO1xuXG52YXIgX2dldCA9IGZ1bmN0aW9uIGdldChvYmplY3QsIHByb3BlcnR5LCByZWNlaXZlcikgeyBpZiAob2JqZWN0ID09PSBudWxsKSBvYmplY3QgPSBGdW5jdGlvbi5wcm90b3R5cGU7IHZhciBkZXNjID0gT2JqZWN0LmdldE93blByb3BlcnR5RGVzY3JpcHRvcihvYmplY3QsIHByb3BlcnR5KTsgaWYgKGRlc2MgPT09IHVuZGVmaW5lZCkgeyB2YXIgcGFyZW50ID0gT2JqZWN0LmdldFByb3RvdHlwZU9mKG9iamVjdCk7IGlmIChwYXJlbnQgPT09IG51bGwpIHsgcmV0dXJuIHVuZGVmaW5lZDsgfSBlbHNlIHsgcmV0dXJuIGdldChwYXJlbnQsIHByb3BlcnR5LCByZWNlaXZlcik7IH0gfSBlbHNlIGlmIChcInZhbHVlXCIgaW4gZGVzYykgeyByZXR1cm4gZGVzYy52YWx1ZTsgfSBlbHNlIHsgdmFyIGdldHRlciA9IGRlc2MuZ2V0OyBpZiAoZ2V0dGVyID09PSB1bmRlZmluZWQpIHsgcmV0dXJuIHVuZGVmaW5lZDsgfSByZXR1cm4gZ2V0dGVyLmNhbGwocmVjZWl2ZXIpOyB9IH07XG5cbnZhciBfRXh0ZW5zaW9uMiA9IF9fd2VicGFja19yZXF1aXJlX18oMSk7XG5cbnZhciBfRXh0ZW5zaW9uMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX0V4dGVuc2lvbjIpO1xuXG52YXIgX2pxdWVyeSA9IF9fd2VicGFja19yZXF1aXJlX18oMCk7XG5cbnZhciBfanF1ZXJ5MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2pxdWVyeSk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbmZ1bmN0aW9uIF9jbGFzc0NhbGxDaGVjayhpbnN0YW5jZSwgQ29uc3RydWN0b3IpIHsgaWYgKCEoaW5zdGFuY2UgaW5zdGFuY2VvZiBDb25zdHJ1Y3RvcikpIHsgdGhyb3cgbmV3IFR5cGVFcnJvcihcIkNhbm5vdCBjYWxsIGEgY2xhc3MgYXMgYSBmdW5jdGlvblwiKTsgfSB9XG5cbmZ1bmN0aW9uIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuKHNlbGYsIGNhbGwpIHsgaWYgKCFzZWxmKSB7IHRocm93IG5ldyBSZWZlcmVuY2VFcnJvcihcInRoaXMgaGFzbid0IGJlZW4gaW5pdGlhbGlzZWQgLSBzdXBlcigpIGhhc24ndCBiZWVuIGNhbGxlZFwiKTsgfSByZXR1cm4gY2FsbCAmJiAodHlwZW9mIGNhbGwgPT09IFwib2JqZWN0XCIgfHwgdHlwZW9mIGNhbGwgPT09IFwiZnVuY3Rpb25cIikgPyBjYWxsIDogc2VsZjsgfVxuXG5mdW5jdGlvbiBfaW5oZXJpdHMoc3ViQ2xhc3MsIHN1cGVyQ2xhc3MpIHsgaWYgKHR5cGVvZiBzdXBlckNsYXNzICE9PSBcImZ1bmN0aW9uXCIgJiYgc3VwZXJDbGFzcyAhPT0gbnVsbCkgeyB0aHJvdyBuZXcgVHlwZUVycm9yKFwiU3VwZXIgZXhwcmVzc2lvbiBtdXN0IGVpdGhlciBiZSBudWxsIG9yIGEgZnVuY3Rpb24sIG5vdCBcIiArIHR5cGVvZiBzdXBlckNsYXNzKTsgfSBzdWJDbGFzcy5wcm90b3R5cGUgPSBPYmplY3QuY3JlYXRlKHN1cGVyQ2xhc3MgJiYgc3VwZXJDbGFzcy5wcm90b3R5cGUsIHsgY29uc3RydWN0b3I6IHsgdmFsdWU6IHN1YkNsYXNzLCBlbnVtZXJhYmxlOiBmYWxzZSwgd3JpdGFibGU6IHRydWUsIGNvbmZpZ3VyYWJsZTogdHJ1ZSB9IH0pOyBpZiAoc3VwZXJDbGFzcykgT2JqZWN0LnNldFByb3RvdHlwZU9mID8gT2JqZWN0LnNldFByb3RvdHlwZU9mKHN1YkNsYXNzLCBzdXBlckNsYXNzKSA6IHN1YkNsYXNzLl9fcHJvdG9fXyA9IHN1cGVyQ2xhc3M7IH1cblxuLyoqXG4gKiBDb2xvciBwcmV2aWV3IGV4dGVuc2lvblxuICogQGlnbm9yZVxuICovXG52YXIgUHJldmlldyA9IGZ1bmN0aW9uIChfRXh0ZW5zaW9uKSB7XG4gIF9pbmhlcml0cyhQcmV2aWV3LCBfRXh0ZW5zaW9uKTtcblxuICBmdW5jdGlvbiBQcmV2aWV3KGNvbG9ycGlja2VyKSB7XG4gICAgdmFyIG9wdGlvbnMgPSBhcmd1bWVudHMubGVuZ3RoID4gMSAmJiBhcmd1bWVudHNbMV0gIT09IHVuZGVmaW5lZCA/IGFyZ3VtZW50c1sxXSA6IHt9O1xuXG4gICAgX2NsYXNzQ2FsbENoZWNrKHRoaXMsIFByZXZpZXcpO1xuXG4gICAgdmFyIF90aGlzID0gX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4odGhpcywgKFByZXZpZXcuX19wcm90b19fIHx8IE9iamVjdC5nZXRQcm90b3R5cGVPZihQcmV2aWV3KSkuY2FsbCh0aGlzLCBjb2xvcnBpY2tlciwgX2pxdWVyeTIuZGVmYXVsdC5leHRlbmQodHJ1ZSwge30sIHtcbiAgICAgIHRlbXBsYXRlOiAnPGRpdiBjbGFzcz1cImNvbG9ycGlja2VyLWJhciBjb2xvcnBpY2tlci1wcmV2aWV3XCI+PGRpdiAvPjwvZGl2PicsXG4gICAgICBzaG93VGV4dDogdHJ1ZSxcbiAgICAgIGZvcm1hdDogY29sb3JwaWNrZXIuZm9ybWF0XG4gICAgfSwgb3B0aW9ucykpKTtcblxuICAgIF90aGlzLmVsZW1lbnQgPSAoMCwgX2pxdWVyeTIuZGVmYXVsdCkoX3RoaXMub3B0aW9ucy50ZW1wbGF0ZSk7XG4gICAgX3RoaXMuZWxlbWVudElubmVyID0gX3RoaXMuZWxlbWVudC5maW5kKCdkaXYnKTtcbiAgICByZXR1cm4gX3RoaXM7XG4gIH1cblxuICBfY3JlYXRlQ2xhc3MoUHJldmlldywgW3tcbiAgICBrZXk6ICdvbkNyZWF0ZScsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIG9uQ3JlYXRlKGV2ZW50KSB7XG4gICAgICBfZ2V0KFByZXZpZXcucHJvdG90eXBlLl9fcHJvdG9fXyB8fCBPYmplY3QuZ2V0UHJvdG90eXBlT2YoUHJldmlldy5wcm90b3R5cGUpLCAnb25DcmVhdGUnLCB0aGlzKS5jYWxsKHRoaXMsIGV2ZW50KTtcbiAgICAgIHRoaXMuY29sb3JwaWNrZXIucGlja2VyLmFwcGVuZCh0aGlzLmVsZW1lbnQpO1xuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ29uVXBkYXRlJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gb25VcGRhdGUoZXZlbnQpIHtcbiAgICAgIF9nZXQoUHJldmlldy5wcm90b3R5cGUuX19wcm90b19fIHx8IE9iamVjdC5nZXRQcm90b3R5cGVPZihQcmV2aWV3LnByb3RvdHlwZSksICdvblVwZGF0ZScsIHRoaXMpLmNhbGwodGhpcywgZXZlbnQpO1xuXG4gICAgICBpZiAoIWV2ZW50LmNvbG9yKSB7XG4gICAgICAgIHRoaXMuZWxlbWVudElubmVyLmNzcygnYmFja2dyb3VuZENvbG9yJywgbnVsbCkuY3NzKCdjb2xvcicsIG51bGwpLmh0bWwoJycpO1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG5cbiAgICAgIHRoaXMuZWxlbWVudElubmVyLmNzcygnYmFja2dyb3VuZENvbG9yJywgZXZlbnQuY29sb3IudG9SZ2JTdHJpbmcoKSk7XG5cbiAgICAgIGlmICh0aGlzLm9wdGlvbnMuc2hvd1RleHQpIHtcbiAgICAgICAgdGhpcy5lbGVtZW50SW5uZXIuaHRtbChldmVudC5jb2xvci5zdHJpbmcodGhpcy5vcHRpb25zLmZvcm1hdCB8fCB0aGlzLmNvbG9ycGlja2VyLmZvcm1hdCkpO1xuXG4gICAgICAgIGlmIChldmVudC5jb2xvci5pc0RhcmsoKSAmJiBldmVudC5jb2xvci5hbHBoYSA+IDAuNSkge1xuICAgICAgICAgIHRoaXMuZWxlbWVudElubmVyLmNzcygnY29sb3InLCAnd2hpdGUnKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICB0aGlzLmVsZW1lbnRJbm5lci5jc3MoJ2NvbG9yJywgJ2JsYWNrJyk7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH1dKTtcblxuICByZXR1cm4gUHJldmlldztcbn0oX0V4dGVuc2lvbjMuZGVmYXVsdCk7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IFByZXZpZXc7XG5tb2R1bGUuZXhwb3J0cyA9IGV4cG9ydHMuZGVmYXVsdDtcblxuLyoqKi8gfSksXG4vKiAxMiAqL1xuLyoqKi8gKGZ1bmN0aW9uKG1vZHVsZSwgZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXykge1xuXG5cInVzZSBzdHJpY3RcIjtcblxuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX2NyZWF0ZUNsYXNzID0gZnVuY3Rpb24gKCkgeyBmdW5jdGlvbiBkZWZpbmVQcm9wZXJ0aWVzKHRhcmdldCwgcHJvcHMpIHsgZm9yICh2YXIgaSA9IDA7IGkgPCBwcm9wcy5sZW5ndGg7IGkrKykgeyB2YXIgZGVzY3JpcHRvciA9IHByb3BzW2ldOyBkZXNjcmlwdG9yLmVudW1lcmFibGUgPSBkZXNjcmlwdG9yLmVudW1lcmFibGUgfHwgZmFsc2U7IGRlc2NyaXB0b3IuY29uZmlndXJhYmxlID0gdHJ1ZTsgaWYgKFwidmFsdWVcIiBpbiBkZXNjcmlwdG9yKSBkZXNjcmlwdG9yLndyaXRhYmxlID0gdHJ1ZTsgT2JqZWN0LmRlZmluZVByb3BlcnR5KHRhcmdldCwgZGVzY3JpcHRvci5rZXksIGRlc2NyaXB0b3IpOyB9IH0gcmV0dXJuIGZ1bmN0aW9uIChDb25zdHJ1Y3RvciwgcHJvdG9Qcm9wcywgc3RhdGljUHJvcHMpIHsgaWYgKHByb3RvUHJvcHMpIGRlZmluZVByb3BlcnRpZXMoQ29uc3RydWN0b3IucHJvdG90eXBlLCBwcm90b1Byb3BzKTsgaWYgKHN0YXRpY1Byb3BzKSBkZWZpbmVQcm9wZXJ0aWVzKENvbnN0cnVjdG9yLCBzdGF0aWNQcm9wcyk7IHJldHVybiBDb25zdHJ1Y3RvcjsgfTsgfSgpO1xuXG52YXIgX2dldCA9IGZ1bmN0aW9uIGdldChvYmplY3QsIHByb3BlcnR5LCByZWNlaXZlcikgeyBpZiAob2JqZWN0ID09PSBudWxsKSBvYmplY3QgPSBGdW5jdGlvbi5wcm90b3R5cGU7IHZhciBkZXNjID0gT2JqZWN0LmdldE93blByb3BlcnR5RGVzY3JpcHRvcihvYmplY3QsIHByb3BlcnR5KTsgaWYgKGRlc2MgPT09IHVuZGVmaW5lZCkgeyB2YXIgcGFyZW50ID0gT2JqZWN0LmdldFByb3RvdHlwZU9mKG9iamVjdCk7IGlmIChwYXJlbnQgPT09IG51bGwpIHsgcmV0dXJuIHVuZGVmaW5lZDsgfSBlbHNlIHsgcmV0dXJuIGdldChwYXJlbnQsIHByb3BlcnR5LCByZWNlaXZlcik7IH0gfSBlbHNlIGlmIChcInZhbHVlXCIgaW4gZGVzYykgeyByZXR1cm4gZGVzYy52YWx1ZTsgfSBlbHNlIHsgdmFyIGdldHRlciA9IGRlc2MuZ2V0OyBpZiAoZ2V0dGVyID09PSB1bmRlZmluZWQpIHsgcmV0dXJuIHVuZGVmaW5lZDsgfSByZXR1cm4gZ2V0dGVyLmNhbGwocmVjZWl2ZXIpOyB9IH07XG5cbnZhciBfUGFsZXR0ZTIgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKDQpO1xuXG52YXIgX1BhbGV0dGUzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfUGFsZXR0ZTIpO1xuXG52YXIgX2pxdWVyeSA9IF9fd2VicGFja19yZXF1aXJlX18oMCk7XG5cbnZhciBfanF1ZXJ5MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2pxdWVyeSk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbmZ1bmN0aW9uIF9jbGFzc0NhbGxDaGVjayhpbnN0YW5jZSwgQ29uc3RydWN0b3IpIHsgaWYgKCEoaW5zdGFuY2UgaW5zdGFuY2VvZiBDb25zdHJ1Y3RvcikpIHsgdGhyb3cgbmV3IFR5cGVFcnJvcihcIkNhbm5vdCBjYWxsIGEgY2xhc3MgYXMgYSBmdW5jdGlvblwiKTsgfSB9XG5cbmZ1bmN0aW9uIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuKHNlbGYsIGNhbGwpIHsgaWYgKCFzZWxmKSB7IHRocm93IG5ldyBSZWZlcmVuY2VFcnJvcihcInRoaXMgaGFzbid0IGJlZW4gaW5pdGlhbGlzZWQgLSBzdXBlcigpIGhhc24ndCBiZWVuIGNhbGxlZFwiKTsgfSByZXR1cm4gY2FsbCAmJiAodHlwZW9mIGNhbGwgPT09IFwib2JqZWN0XCIgfHwgdHlwZW9mIGNhbGwgPT09IFwiZnVuY3Rpb25cIikgPyBjYWxsIDogc2VsZjsgfVxuXG5mdW5jdGlvbiBfaW5oZXJpdHMoc3ViQ2xhc3MsIHN1cGVyQ2xhc3MpIHsgaWYgKHR5cGVvZiBzdXBlckNsYXNzICE9PSBcImZ1bmN0aW9uXCIgJiYgc3VwZXJDbGFzcyAhPT0gbnVsbCkgeyB0aHJvdyBuZXcgVHlwZUVycm9yKFwiU3VwZXIgZXhwcmVzc2lvbiBtdXN0IGVpdGhlciBiZSBudWxsIG9yIGEgZnVuY3Rpb24sIG5vdCBcIiArIHR5cGVvZiBzdXBlckNsYXNzKTsgfSBzdWJDbGFzcy5wcm90b3R5cGUgPSBPYmplY3QuY3JlYXRlKHN1cGVyQ2xhc3MgJiYgc3VwZXJDbGFzcy5wcm90b3R5cGUsIHsgY29uc3RydWN0b3I6IHsgdmFsdWU6IHN1YkNsYXNzLCBlbnVtZXJhYmxlOiBmYWxzZSwgd3JpdGFibGU6IHRydWUsIGNvbmZpZ3VyYWJsZTogdHJ1ZSB9IH0pOyBpZiAoc3VwZXJDbGFzcykgT2JqZWN0LnNldFByb3RvdHlwZU9mID8gT2JqZWN0LnNldFByb3RvdHlwZU9mKHN1YkNsYXNzLCBzdXBlckNsYXNzKSA6IHN1YkNsYXNzLl9fcHJvdG9fXyA9IHN1cGVyQ2xhc3M7IH1cblxudmFyIGRlZmF1bHRzID0ge1xuICBiYXJUZW1wbGF0ZTogJzxkaXYgY2xhc3M9XCJjb2xvcnBpY2tlci1iYXIgY29sb3JwaWNrZXItc3dhdGNoZXNcIj5cXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2xvcnBpY2tlci1zd2F0Y2hlcy0taW5uZXJcIj48L2Rpdj5cXG4gICAgICAgICAgICAgICAgPC9kaXY+JyxcbiAgc3dhdGNoVGVtcGxhdGU6ICc8aSBjbGFzcz1cImNvbG9ycGlja2VyLXN3YXRjaFwiPjxpIGNsYXNzPVwiY29sb3JwaWNrZXItc3dhdGNoLS1pbm5lclwiPjwvaT48L2k+J1xufTtcblxuLyoqXG4gKiBDb2xvciBzd2F0Y2hlcyBleHRlbnNpb25cbiAqIEBpZ25vcmVcbiAqL1xuXG52YXIgU3dhdGNoZXMgPSBmdW5jdGlvbiAoX1BhbGV0dGUpIHtcbiAgX2luaGVyaXRzKFN3YXRjaGVzLCBfUGFsZXR0ZSk7XG5cbiAgZnVuY3Rpb24gU3dhdGNoZXMoY29sb3JwaWNrZXIpIHtcbiAgICB2YXIgb3B0aW9ucyA9IGFyZ3VtZW50cy5sZW5ndGggPiAxICYmIGFyZ3VtZW50c1sxXSAhPT0gdW5kZWZpbmVkID8gYXJndW1lbnRzWzFdIDoge307XG5cbiAgICBfY2xhc3NDYWxsQ2hlY2sodGhpcywgU3dhdGNoZXMpO1xuXG4gICAgdmFyIF90aGlzID0gX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4odGhpcywgKFN3YXRjaGVzLl9fcHJvdG9fXyB8fCBPYmplY3QuZ2V0UHJvdG90eXBlT2YoU3dhdGNoZXMpKS5jYWxsKHRoaXMsIGNvbG9ycGlja2VyLCBfanF1ZXJ5Mi5kZWZhdWx0LmV4dGVuZCh0cnVlLCB7fSwgZGVmYXVsdHMsIG9wdGlvbnMpKSk7XG5cbiAgICBfdGhpcy5lbGVtZW50ID0gbnVsbDtcbiAgICByZXR1cm4gX3RoaXM7XG4gIH1cblxuICBfY3JlYXRlQ2xhc3MoU3dhdGNoZXMsIFt7XG4gICAga2V5OiAnaXNFbmFibGVkJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gaXNFbmFibGVkKCkge1xuICAgICAgcmV0dXJuIHRoaXMuZ2V0TGVuZ3RoKCkgPiAwO1xuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ29uQ3JlYXRlJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gb25DcmVhdGUoZXZlbnQpIHtcbiAgICAgIF9nZXQoU3dhdGNoZXMucHJvdG90eXBlLl9fcHJvdG9fXyB8fCBPYmplY3QuZ2V0UHJvdG90eXBlT2YoU3dhdGNoZXMucHJvdG90eXBlKSwgJ29uQ3JlYXRlJywgdGhpcykuY2FsbCh0aGlzLCBldmVudCk7XG5cbiAgICAgIGlmICghdGhpcy5pc0VuYWJsZWQoKSkge1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG5cbiAgICAgIHRoaXMuZWxlbWVudCA9ICgwLCBfanF1ZXJ5Mi5kZWZhdWx0KSh0aGlzLm9wdGlvbnMuYmFyVGVtcGxhdGUpO1xuICAgICAgdGhpcy5sb2FkKCk7XG4gICAgICB0aGlzLmNvbG9ycGlja2VyLnBpY2tlci5hcHBlbmQodGhpcy5lbGVtZW50KTtcbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdsb2FkJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gbG9hZCgpIHtcbiAgICAgIHZhciBfdGhpczIgPSB0aGlzO1xuXG4gICAgICB2YXIgY29sb3JwaWNrZXIgPSB0aGlzLmNvbG9ycGlja2VyLFxuICAgICAgICAgIHN3YXRjaENvbnRhaW5lciA9IHRoaXMuZWxlbWVudC5maW5kKCcuY29sb3JwaWNrZXItc3dhdGNoZXMtLWlubmVyJyksXG4gICAgICAgICAgaXNBbGlhc2VkID0gdGhpcy5vcHRpb25zLm5hbWVzQXNWYWx1ZXMgPT09IHRydWUgJiYgIUFycmF5LmlzQXJyYXkodGhpcy5jb2xvcnMpO1xuXG4gICAgICBzd2F0Y2hDb250YWluZXIuZW1wdHkoKTtcblxuICAgICAgX2pxdWVyeTIuZGVmYXVsdC5lYWNoKHRoaXMuY29sb3JzLCBmdW5jdGlvbiAobmFtZSwgdmFsdWUpIHtcbiAgICAgICAgdmFyICRzd2F0Y2ggPSAoMCwgX2pxdWVyeTIuZGVmYXVsdCkoX3RoaXMyLm9wdGlvbnMuc3dhdGNoVGVtcGxhdGUpLmF0dHIoJ2RhdGEtbmFtZScsIG5hbWUpLmF0dHIoJ2RhdGEtdmFsdWUnLCB2YWx1ZSkuYXR0cigndGl0bGUnLCBpc0FsaWFzZWQgPyBuYW1lICsgJzogJyArIHZhbHVlIDogdmFsdWUpLm9uKCdtb3VzZWRvd24uY29sb3JwaWNrZXIgdG91Y2hzdGFydC5jb2xvcnBpY2tlcicsIGZ1bmN0aW9uIChlKSB7XG4gICAgICAgICAgdmFyICRzdyA9ICgwLCBfanF1ZXJ5Mi5kZWZhdWx0KSh0aGlzKTtcblxuICAgICAgICAgIC8vIGUucHJldmVudERlZmF1bHQoKTtcblxuICAgICAgICAgIGNvbG9ycGlja2VyLnNldFZhbHVlKGlzQWxpYXNlZCA/ICRzdy5hdHRyKCdkYXRhLW5hbWUnKSA6ICRzdy5hdHRyKCdkYXRhLXZhbHVlJykpO1xuICAgICAgICB9KTtcblxuICAgICAgICAkc3dhdGNoLmZpbmQoJy5jb2xvcnBpY2tlci1zd2F0Y2gtLWlubmVyJykuY3NzKCdiYWNrZ3JvdW5kLWNvbG9yJywgdmFsdWUpO1xuXG4gICAgICAgIHN3YXRjaENvbnRhaW5lci5hcHBlbmQoJHN3YXRjaCk7XG4gICAgICB9KTtcblxuICAgICAgc3dhdGNoQ29udGFpbmVyLmFwcGVuZCgoMCwgX2pxdWVyeTIuZGVmYXVsdCkoJzxpIGNsYXNzPVwiY29sb3JwaWNrZXItY2xlYXJcIj48L2k+JykpO1xuICAgIH1cbiAgfV0pO1xuXG4gIHJldHVybiBTd2F0Y2hlcztcbn0oX1BhbGV0dGUzLmRlZmF1bHQpO1xuXG5leHBvcnRzLmRlZmF1bHQgPSBTd2F0Y2hlcztcbm1vZHVsZS5leHBvcnRzID0gZXhwb3J0cy5kZWZhdWx0O1xuXG4vKioqLyB9KSxcbi8qIDEzICovXG4vKioqLyAoZnVuY3Rpb24obW9kdWxlLCBleHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKSB7XG5cblwidXNlIHN0cmljdFwiO1xuXG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfY3JlYXRlQ2xhc3MgPSBmdW5jdGlvbiAoKSB7IGZ1bmN0aW9uIGRlZmluZVByb3BlcnRpZXModGFyZ2V0LCBwcm9wcykgeyBmb3IgKHZhciBpID0gMDsgaSA8IHByb3BzLmxlbmd0aDsgaSsrKSB7IHZhciBkZXNjcmlwdG9yID0gcHJvcHNbaV07IGRlc2NyaXB0b3IuZW51bWVyYWJsZSA9IGRlc2NyaXB0b3IuZW51bWVyYWJsZSB8fCBmYWxzZTsgZGVzY3JpcHRvci5jb25maWd1cmFibGUgPSB0cnVlOyBpZiAoXCJ2YWx1ZVwiIGluIGRlc2NyaXB0b3IpIGRlc2NyaXB0b3Iud3JpdGFibGUgPSB0cnVlOyBPYmplY3QuZGVmaW5lUHJvcGVydHkodGFyZ2V0LCBkZXNjcmlwdG9yLmtleSwgZGVzY3JpcHRvcik7IH0gfSByZXR1cm4gZnVuY3Rpb24gKENvbnN0cnVjdG9yLCBwcm90b1Byb3BzLCBzdGF0aWNQcm9wcykgeyBpZiAocHJvdG9Qcm9wcykgZGVmaW5lUHJvcGVydGllcyhDb25zdHJ1Y3Rvci5wcm90b3R5cGUsIHByb3RvUHJvcHMpOyBpZiAoc3RhdGljUHJvcHMpIGRlZmluZVByb3BlcnRpZXMoQ29uc3RydWN0b3IsIHN0YXRpY1Byb3BzKTsgcmV0dXJuIENvbnN0cnVjdG9yOyB9OyB9KCk7XG5cbnZhciBfanF1ZXJ5ID0gX193ZWJwYWNrX3JlcXVpcmVfXygwKTtcblxudmFyIF9qcXVlcnkyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfanF1ZXJ5KTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxuZnVuY3Rpb24gX2NsYXNzQ2FsbENoZWNrKGluc3RhbmNlLCBDb25zdHJ1Y3RvcikgeyBpZiAoIShpbnN0YW5jZSBpbnN0YW5jZW9mIENvbnN0cnVjdG9yKSkgeyB0aHJvdyBuZXcgVHlwZUVycm9yKFwiQ2Fubm90IGNhbGwgYSBjbGFzcyBhcyBhIGZ1bmN0aW9uXCIpOyB9IH1cblxuLyoqXG4gKiBDbGFzcyB0aGF0IGhhbmRsZXMgYWxsIGNvbmZpZ3VyZWQgc2xpZGVycyBvbiBtb3VzZSBvciB0b3VjaCBldmVudHMuXG4gKiBAaWdub3JlXG4gKi9cbnZhciBTbGlkZXJIYW5kbGVyID0gZnVuY3Rpb24gKCkge1xuICAvKipcbiAgICogQHBhcmFtIHtDb2xvcnBpY2tlcn0gY29sb3JwaWNrZXJcbiAgICovXG4gIGZ1bmN0aW9uIFNsaWRlckhhbmRsZXIoY29sb3JwaWNrZXIpIHtcbiAgICBfY2xhc3NDYWxsQ2hlY2sodGhpcywgU2xpZGVySGFuZGxlcik7XG5cbiAgICAvKipcbiAgICAgKiBAdHlwZSB7Q29sb3JwaWNrZXJ9XG4gICAgICovXG4gICAgdGhpcy5jb2xvcnBpY2tlciA9IGNvbG9ycGlja2VyO1xuICAgIC8qKlxuICAgICAqIEB0eXBlIHsqfFN0cmluZ31cbiAgICAgKiBAcHJpdmF0ZVxuICAgICAqL1xuICAgIHRoaXMuY3VycmVudFNsaWRlciA9IG51bGw7XG4gICAgLyoqXG4gICAgICogQHR5cGUge3tsZWZ0OiBudW1iZXIsIHRvcDogbnVtYmVyfX1cbiAgICAgKiBAcHJpdmF0ZVxuICAgICAqL1xuICAgIHRoaXMubW91c2VQb2ludGVyID0ge1xuICAgICAgbGVmdDogMCxcbiAgICAgIHRvcDogMFxuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBAdHlwZSB7RnVuY3Rpb259XG4gICAgICovXG4gICAgdGhpcy5vbk1vdmUgPSBfanF1ZXJ5Mi5kZWZhdWx0LnByb3h5KHRoaXMuZGVmYXVsdE9uTW92ZSwgdGhpcyk7XG4gIH1cblxuICAvKipcbiAgICogVGhpcyBmdW5jdGlvbiBpcyBjYWxsZWQgZXZlcnkgdGltZSBhIHNsaWRlciBndWlkZSBpcyBtb3ZlZFxuICAgKiBUaGUgc2NvcGUgb2YgXCJ0aGlzXCIgaXMgdGhlIFNsaWRlckhhbmRsZXIgb2JqZWN0LlxuICAgKlxuICAgKiBAcGFyYW0ge2ludH0gdG9wXG4gICAqIEBwYXJhbSB7aW50fSBsZWZ0XG4gICAqL1xuXG5cbiAgX2NyZWF0ZUNsYXNzKFNsaWRlckhhbmRsZXIsIFt7XG4gICAga2V5OiAnZGVmYXVsdE9uTW92ZScsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGRlZmF1bHRPbk1vdmUodG9wLCBsZWZ0KSB7XG4gICAgICBpZiAoIXRoaXMuY3VycmVudFNsaWRlcikge1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG5cbiAgICAgIHZhciBzbGlkZXIgPSB0aGlzLmN1cnJlbnRTbGlkZXIsXG4gICAgICAgICAgY3AgPSB0aGlzLmNvbG9ycGlja2VyLFxuICAgICAgICAgIGNoID0gY3AuY29sb3JIYW5kbGVyO1xuXG4gICAgICAvLyBDcmVhdGUgYSBjb2xvciBvYmplY3RcbiAgICAgIHZhciBjb2xvciA9ICFjaC5oYXNDb2xvcigpID8gY2guZ2V0RmFsbGJhY2tDb2xvcigpIDogY2guY29sb3IuZ2V0Q2xvbmUoKTtcblxuICAgICAgLy8gQWRqdXN0IHRoZSBndWlkZSBwb3NpdGlvblxuICAgICAgc2xpZGVyLmd1aWRlU3R5bGUubGVmdCA9IGxlZnQgKyAncHgnO1xuICAgICAgc2xpZGVyLmd1aWRlU3R5bGUudG9wID0gdG9wICsgJ3B4JztcblxuICAgICAgLy8gQWRqdXN0IHRoZSBjb2xvclxuICAgICAgaWYgKHNsaWRlci5jYWxsTGVmdCkge1xuICAgICAgICBjb2xvcltzbGlkZXIuY2FsbExlZnRdKGxlZnQgLyBzbGlkZXIubWF4TGVmdCk7XG4gICAgICB9XG4gICAgICBpZiAoc2xpZGVyLmNhbGxUb3ApIHtcbiAgICAgICAgY29sb3Jbc2xpZGVyLmNhbGxUb3BdKHRvcCAvIHNsaWRlci5tYXhUb3ApO1xuICAgICAgfVxuXG4gICAgICAvLyBTZXQgdGhlIG5ldyBjb2xvclxuICAgICAgY3Auc2V0VmFsdWUoY29sb3IpO1xuICAgICAgY3AucG9wdXBIYW5kbGVyLmZvY3VzKCk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogQmluZHMgdGhlIGNvbG9ycGlja2VyIHNsaWRlcnMgdG8gdGhlIG1vdXNlL3RvdWNoIGV2ZW50c1xuICAgICAqL1xuXG4gIH0sIHtcbiAgICBrZXk6ICdiaW5kJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gYmluZCgpIHtcbiAgICAgIHZhciBzbGlkZXJzID0gdGhpcy5jb2xvcnBpY2tlci5vcHRpb25zLmhvcml6b250YWwgPyB0aGlzLmNvbG9ycGlja2VyLm9wdGlvbnMuc2xpZGVyc0hvcnogOiB0aGlzLmNvbG9ycGlja2VyLm9wdGlvbnMuc2xpZGVycztcblxuICAgICAgdmFyIHNsaWRlckNsYXNzZXMgPSBbXTtcblxuICAgICAgZm9yICh2YXIgc2xpZGVyTmFtZSBpbiBzbGlkZXJzKSB7XG4gICAgICAgIGlmICghc2xpZGVycy5oYXNPd25Qcm9wZXJ0eShzbGlkZXJOYW1lKSkge1xuICAgICAgICAgIGNvbnRpbnVlO1xuICAgICAgICB9XG5cbiAgICAgICAgc2xpZGVyQ2xhc3Nlcy5wdXNoKHNsaWRlcnNbc2xpZGVyTmFtZV0uc2VsZWN0b3IpO1xuICAgICAgfVxuXG4gICAgICB0aGlzLmNvbG9ycGlja2VyLnBpY2tlci5maW5kKHNsaWRlckNsYXNzZXMuam9pbignLCAnKSkub24oJ21vdXNlZG93bi5jb2xvcnBpY2tlciB0b3VjaHN0YXJ0LmNvbG9ycGlja2VyJywgX2pxdWVyeTIuZGVmYXVsdC5wcm94eSh0aGlzLnByZXNzZWQsIHRoaXMpKTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBVbmJpbmRzIGFueSBldmVudCBib3VuZCBieSB0aGlzIGhhbmRsZXJcbiAgICAgKi9cblxuICB9LCB7XG4gICAga2V5OiAndW5iaW5kJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gdW5iaW5kKCkge1xuICAgICAgKDAsIF9qcXVlcnkyLmRlZmF1bHQpKHRoaXMuY29sb3JwaWNrZXIucGlja2VyKS5vZmYoe1xuICAgICAgICAnbW91c2Vtb3ZlLmNvbG9ycGlja2VyJzogX2pxdWVyeTIuZGVmYXVsdC5wcm94eSh0aGlzLm1vdmVkLCB0aGlzKSxcbiAgICAgICAgJ3RvdWNobW92ZS5jb2xvcnBpY2tlcic6IF9qcXVlcnkyLmRlZmF1bHQucHJveHkodGhpcy5tb3ZlZCwgdGhpcyksXG4gICAgICAgICdtb3VzZXVwLmNvbG9ycGlja2VyJzogX2pxdWVyeTIuZGVmYXVsdC5wcm94eSh0aGlzLnJlbGVhc2VkLCB0aGlzKSxcbiAgICAgICAgJ3RvdWNoZW5kLmNvbG9ycGlja2VyJzogX2pxdWVyeTIuZGVmYXVsdC5wcm94eSh0aGlzLnJlbGVhc2VkLCB0aGlzKVxuICAgICAgfSk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogRnVuY3Rpb24gdHJpZ2dlcmVkIHdoZW4gY2xpY2tpbmcgaW4gb25lIG9mIHRoZSBjb2xvciBhZGp1c3RtZW50IGJhcnNcbiAgICAgKlxuICAgICAqIEBwcml2YXRlXG4gICAgICogQGZpcmVzIENvbG9ycGlja2VyI21vdXNlbW92ZVxuICAgICAqIEBwYXJhbSB7RXZlbnR9IGVcbiAgICAgKi9cblxuICB9LCB7XG4gICAga2V5OiAncHJlc3NlZCcsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHByZXNzZWQoZSkge1xuICAgICAgaWYgKHRoaXMuY29sb3JwaWNrZXIuaXNEaXNhYmxlZCgpKSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cbiAgICAgIHRoaXMuY29sb3JwaWNrZXIubGFzdEV2ZW50LmFsaWFzID0gJ3ByZXNzZWQnO1xuICAgICAgdGhpcy5jb2xvcnBpY2tlci5sYXN0RXZlbnQuZSA9IGU7XG5cbiAgICAgIGlmICghZS5wYWdlWCAmJiAhZS5wYWdlWSAmJiBlLm9yaWdpbmFsRXZlbnQgJiYgZS5vcmlnaW5hbEV2ZW50LnRvdWNoZXMpIHtcbiAgICAgICAgZS5wYWdlWCA9IGUub3JpZ2luYWxFdmVudC50b3VjaGVzWzBdLnBhZ2VYO1xuICAgICAgICBlLnBhZ2VZID0gZS5vcmlnaW5hbEV2ZW50LnRvdWNoZXNbMF0ucGFnZVk7XG4gICAgICB9XG4gICAgICAvLyBlLnN0b3BQcm9wYWdhdGlvbigpO1xuICAgICAgLy8gZS5wcmV2ZW50RGVmYXVsdCgpO1xuXG4gICAgICB2YXIgdGFyZ2V0ID0gKDAsIF9qcXVlcnkyLmRlZmF1bHQpKGUudGFyZ2V0KTtcblxuICAgICAgLy8gZGV0ZWN0IHRoZSBzbGlkZXIgYW5kIHNldCB0aGUgbGltaXRzIGFuZCBjYWxsYmFja3NcbiAgICAgIHZhciB6b25lID0gdGFyZ2V0LmNsb3Nlc3QoJ2RpdicpO1xuXG4gICAgICB2YXIgc2xpZGVycyA9IHRoaXMuY29sb3JwaWNrZXIub3B0aW9ucy5ob3Jpem9udGFsID8gdGhpcy5jb2xvcnBpY2tlci5vcHRpb25zLnNsaWRlcnNIb3J6IDogdGhpcy5jb2xvcnBpY2tlci5vcHRpb25zLnNsaWRlcnM7XG5cbiAgICAgIGlmICh6b25lLmlzKCcuY29sb3JwaWNrZXInKSkge1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG5cbiAgICAgIHRoaXMuY3VycmVudFNsaWRlciA9IG51bGw7XG5cbiAgICAgIGZvciAodmFyIHNsaWRlck5hbWUgaW4gc2xpZGVycykge1xuICAgICAgICBpZiAoIXNsaWRlcnMuaGFzT3duUHJvcGVydHkoc2xpZGVyTmFtZSkpIHtcbiAgICAgICAgICBjb250aW51ZTtcbiAgICAgICAgfVxuXG4gICAgICAgIHZhciBzbGlkZXIgPSBzbGlkZXJzW3NsaWRlck5hbWVdO1xuXG4gICAgICAgIGlmICh6b25lLmlzKHNsaWRlci5zZWxlY3RvcikpIHtcbiAgICAgICAgICB0aGlzLmN1cnJlbnRTbGlkZXIgPSBfanF1ZXJ5Mi5kZWZhdWx0LmV4dGVuZCh7fSwgc2xpZGVyLCB7IG5hbWU6IHNsaWRlck5hbWUgfSk7XG4gICAgICAgICAgYnJlYWs7XG4gICAgICAgIH0gZWxzZSBpZiAoc2xpZGVyLmNoaWxkU2VsZWN0b3IgIT09IHVuZGVmaW5lZCAmJiB6b25lLmlzKHNsaWRlci5jaGlsZFNlbGVjdG9yKSkge1xuICAgICAgICAgIHRoaXMuY3VycmVudFNsaWRlciA9IF9qcXVlcnkyLmRlZmF1bHQuZXh0ZW5kKHt9LCBzbGlkZXIsIHsgbmFtZTogc2xpZGVyTmFtZSB9KTtcbiAgICAgICAgICB6b25lID0gem9uZS5wYXJlbnQoKTsgLy8gem9uZS5wYXJlbnRzKHNsaWRlci5zZWxlY3RvcikuZmlyc3QoKSA/XG4gICAgICAgICAgYnJlYWs7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgdmFyIGd1aWRlID0gem9uZS5maW5kKCcuY29sb3JwaWNrZXItZ3VpZGUnKS5nZXQoMCk7XG5cbiAgICAgIGlmICh0aGlzLmN1cnJlbnRTbGlkZXIgPT09IG51bGwgfHwgZ3VpZGUgPT09IG51bGwpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICB2YXIgb2Zmc2V0ID0gem9uZS5vZmZzZXQoKTtcblxuICAgICAgLy8gcmVmZXJlbmNlIHRvIGd1aWRlJ3Mgc3R5bGVcbiAgICAgIHRoaXMuY3VycmVudFNsaWRlci5ndWlkZVN0eWxlID0gZ3VpZGUuc3R5bGU7XG4gICAgICB0aGlzLmN1cnJlbnRTbGlkZXIubGVmdCA9IGUucGFnZVggLSBvZmZzZXQubGVmdDtcbiAgICAgIHRoaXMuY3VycmVudFNsaWRlci50b3AgPSBlLnBhZ2VZIC0gb2Zmc2V0LnRvcDtcbiAgICAgIHRoaXMubW91c2VQb2ludGVyID0ge1xuICAgICAgICBsZWZ0OiBlLnBhZ2VYLFxuICAgICAgICB0b3A6IGUucGFnZVlcbiAgICAgIH07XG5cbiAgICAgIC8vIFRPRE86IGZpeCBtb3Zpbmcgb3V0c2lkZSB0aGUgcGlja2VyIG1ha2VzIHRoZSBndWlkZXMgdG8ga2VlcCBtb3ZpbmcuIFRoZSBldmVudCBuZWVkcyB0byBiZSBib3VuZCB0byB0aGUgd2luZG93LlxuICAgICAgLyoqXG4gICAgICAgKiAod2luZG93LmRvY3VtZW50KSBUcmlnZ2VyZWQgb24gbW91c2Vkb3duIGZvciB0aGUgZG9jdW1lbnQgb2JqZWN0LFxuICAgICAgICogc28gdGhlIGNvbG9yIGFkanVzdG1lbnQgZ3VpZGUgaXMgbW92ZWQgdG8gdGhlIGNsaWNrZWQgcG9zaXRpb24uXG4gICAgICAgKlxuICAgICAgICogQGV2ZW50IENvbG9ycGlja2VyI21vdXNlbW92ZVxuICAgICAgICovXG4gICAgICAoMCwgX2pxdWVyeTIuZGVmYXVsdCkodGhpcy5jb2xvcnBpY2tlci5waWNrZXIpLm9uKHtcbiAgICAgICAgJ21vdXNlbW92ZS5jb2xvcnBpY2tlcic6IF9qcXVlcnkyLmRlZmF1bHQucHJveHkodGhpcy5tb3ZlZCwgdGhpcyksXG4gICAgICAgICd0b3VjaG1vdmUuY29sb3JwaWNrZXInOiBfanF1ZXJ5Mi5kZWZhdWx0LnByb3h5KHRoaXMubW92ZWQsIHRoaXMpLFxuICAgICAgICAnbW91c2V1cC5jb2xvcnBpY2tlcic6IF9qcXVlcnkyLmRlZmF1bHQucHJveHkodGhpcy5yZWxlYXNlZCwgdGhpcyksXG4gICAgICAgICd0b3VjaGVuZC5jb2xvcnBpY2tlcic6IF9qcXVlcnkyLmRlZmF1bHQucHJveHkodGhpcy5yZWxlYXNlZCwgdGhpcylcbiAgICAgIH0pLnRyaWdnZXIoJ21vdXNlbW92ZScpO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEZ1bmN0aW9uIHRyaWdnZXJlZCB3aGVuIGRyYWdnaW5nIGEgZ3VpZGUgaW5zaWRlIG9uZSBvZiB0aGUgY29sb3IgYWRqdXN0bWVudCBiYXJzLlxuICAgICAqXG4gICAgICogQHByaXZhdGVcbiAgICAgKiBAcGFyYW0ge0V2ZW50fSBlXG4gICAgICovXG5cbiAgfSwge1xuICAgIGtleTogJ21vdmVkJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gbW92ZWQoZSkge1xuICAgICAgdGhpcy5jb2xvcnBpY2tlci5sYXN0RXZlbnQuYWxpYXMgPSAnbW92ZWQnO1xuICAgICAgdGhpcy5jb2xvcnBpY2tlci5sYXN0RXZlbnQuZSA9IGU7XG5cbiAgICAgIGlmICghZS5wYWdlWCAmJiAhZS5wYWdlWSAmJiBlLm9yaWdpbmFsRXZlbnQgJiYgZS5vcmlnaW5hbEV2ZW50LnRvdWNoZXMpIHtcbiAgICAgICAgZS5wYWdlWCA9IGUub3JpZ2luYWxFdmVudC50b3VjaGVzWzBdLnBhZ2VYO1xuICAgICAgICBlLnBhZ2VZID0gZS5vcmlnaW5hbEV2ZW50LnRvdWNoZXNbMF0ucGFnZVk7XG4gICAgICB9XG5cbiAgICAgIC8vIGUuc3RvcFByb3BhZ2F0aW9uKCk7XG4gICAgICBlLnByZXZlbnREZWZhdWx0KCk7IC8vIHByZXZlbnRzIHNjcm9sbGluZyBvbiBtb2JpbGVcblxuICAgICAgdmFyIGxlZnQgPSBNYXRoLm1heCgwLCBNYXRoLm1pbih0aGlzLmN1cnJlbnRTbGlkZXIubWF4TGVmdCwgdGhpcy5jdXJyZW50U2xpZGVyLmxlZnQgKyAoKGUucGFnZVggfHwgdGhpcy5tb3VzZVBvaW50ZXIubGVmdCkgLSB0aGlzLm1vdXNlUG9pbnRlci5sZWZ0KSkpO1xuXG4gICAgICB2YXIgdG9wID0gTWF0aC5tYXgoMCwgTWF0aC5taW4odGhpcy5jdXJyZW50U2xpZGVyLm1heFRvcCwgdGhpcy5jdXJyZW50U2xpZGVyLnRvcCArICgoZS5wYWdlWSB8fCB0aGlzLm1vdXNlUG9pbnRlci50b3ApIC0gdGhpcy5tb3VzZVBvaW50ZXIudG9wKSkpO1xuXG4gICAgICB0aGlzLm9uTW92ZSh0b3AsIGxlZnQpO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEZ1bmN0aW9uIHRyaWdnZXJlZCB3aGVuIHJlbGVhc2luZyB0aGUgY2xpY2sgaW4gb25lIG9mIHRoZSBjb2xvciBhZGp1c3RtZW50IGJhcnMuXG4gICAgICpcbiAgICAgKiBAcHJpdmF0ZVxuICAgICAqIEBwYXJhbSB7RXZlbnR9IGVcbiAgICAgKi9cblxuICB9LCB7XG4gICAga2V5OiAncmVsZWFzZWQnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiByZWxlYXNlZChlKSB7XG4gICAgICB0aGlzLmNvbG9ycGlja2VyLmxhc3RFdmVudC5hbGlhcyA9ICdyZWxlYXNlZCc7XG4gICAgICB0aGlzLmNvbG9ycGlja2VyLmxhc3RFdmVudC5lID0gZTtcblxuICAgICAgLy8gZS5zdG9wUHJvcGFnYXRpb24oKTtcbiAgICAgIC8vIGUucHJldmVudERlZmF1bHQoKTtcblxuICAgICAgKDAsIF9qcXVlcnkyLmRlZmF1bHQpKHRoaXMuY29sb3JwaWNrZXIucGlja2VyKS5vZmYoe1xuICAgICAgICAnbW91c2Vtb3ZlLmNvbG9ycGlja2VyJzogdGhpcy5tb3ZlZCxcbiAgICAgICAgJ3RvdWNobW92ZS5jb2xvcnBpY2tlcic6IHRoaXMubW92ZWQsXG4gICAgICAgICdtb3VzZXVwLmNvbG9ycGlja2VyJzogdGhpcy5yZWxlYXNlZCxcbiAgICAgICAgJ3RvdWNoZW5kLmNvbG9ycGlja2VyJzogdGhpcy5yZWxlYXNlZFxuICAgICAgfSk7XG4gICAgfVxuICB9XSk7XG5cbiAgcmV0dXJuIFNsaWRlckhhbmRsZXI7XG59KCk7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IFNsaWRlckhhbmRsZXI7XG5tb2R1bGUuZXhwb3J0cyA9IGV4cG9ydHMuZGVmYXVsdDtcblxuLyoqKi8gfSksXG4vKiAxNCAqL1xuLyoqKi8gKGZ1bmN0aW9uKG1vZHVsZSwgZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXykge1xuXG5cInVzZSBzdHJpY3RcIjtcblxuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX2NyZWF0ZUNsYXNzID0gZnVuY3Rpb24gKCkgeyBmdW5jdGlvbiBkZWZpbmVQcm9wZXJ0aWVzKHRhcmdldCwgcHJvcHMpIHsgZm9yICh2YXIgaSA9IDA7IGkgPCBwcm9wcy5sZW5ndGg7IGkrKykgeyB2YXIgZGVzY3JpcHRvciA9IHByb3BzW2ldOyBkZXNjcmlwdG9yLmVudW1lcmFibGUgPSBkZXNjcmlwdG9yLmVudW1lcmFibGUgfHwgZmFsc2U7IGRlc2NyaXB0b3IuY29uZmlndXJhYmxlID0gdHJ1ZTsgaWYgKFwidmFsdWVcIiBpbiBkZXNjcmlwdG9yKSBkZXNjcmlwdG9yLndyaXRhYmxlID0gdHJ1ZTsgT2JqZWN0LmRlZmluZVByb3BlcnR5KHRhcmdldCwgZGVzY3JpcHRvci5rZXksIGRlc2NyaXB0b3IpOyB9IH0gcmV0dXJuIGZ1bmN0aW9uIChDb25zdHJ1Y3RvciwgcHJvdG9Qcm9wcywgc3RhdGljUHJvcHMpIHsgaWYgKHByb3RvUHJvcHMpIGRlZmluZVByb3BlcnRpZXMoQ29uc3RydWN0b3IucHJvdG90eXBlLCBwcm90b1Byb3BzKTsgaWYgKHN0YXRpY1Byb3BzKSBkZWZpbmVQcm9wZXJ0aWVzKENvbnN0cnVjdG9yLCBzdGF0aWNQcm9wcyk7IHJldHVybiBDb25zdHJ1Y3RvcjsgfTsgfSgpO1xuXG52YXIgX2pxdWVyeSA9IF9fd2VicGFja19yZXF1aXJlX18oMCk7XG5cbnZhciBfanF1ZXJ5MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2pxdWVyeSk7XG5cbnZhciBfb3B0aW9ucyA9IF9fd2VicGFja19yZXF1aXJlX18oMyk7XG5cbnZhciBfb3B0aW9uczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9vcHRpb25zKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxuZnVuY3Rpb24gX2NsYXNzQ2FsbENoZWNrKGluc3RhbmNlLCBDb25zdHJ1Y3RvcikgeyBpZiAoIShpbnN0YW5jZSBpbnN0YW5jZW9mIENvbnN0cnVjdG9yKSkgeyB0aHJvdyBuZXcgVHlwZUVycm9yKFwiQ2Fubm90IGNhbGwgYSBjbGFzcyBhcyBhIGZ1bmN0aW9uXCIpOyB9IH1cblxuLyoqXG4gKiBIYW5kbGVzIGV2ZXJ5dGhpbmcgcmVsYXRlZCB0byB0aGUgVUkgb2YgdGhlIGNvbG9ycGlja2VyIHBvcHVwOiBzaG93LCBoaWRlLCBwb3NpdGlvbiwuLi5cbiAqIEBpZ25vcmVcbiAqL1xudmFyIFBvcHVwSGFuZGxlciA9IGZ1bmN0aW9uICgpIHtcbiAgLyoqXG4gICAqIEBwYXJhbSB7Q29sb3JwaWNrZXJ9IGNvbG9ycGlja2VyXG4gICAqIEBwYXJhbSB7V2luZG93fSByb290XG4gICAqL1xuICBmdW5jdGlvbiBQb3B1cEhhbmRsZXIoY29sb3JwaWNrZXIsIHJvb3QpIHtcbiAgICBfY2xhc3NDYWxsQ2hlY2sodGhpcywgUG9wdXBIYW5kbGVyKTtcblxuICAgIC8qKlxuICAgICAqIEB0eXBlIHtXaW5kb3d9XG4gICAgICovXG4gICAgdGhpcy5yb290ID0gcm9vdDtcbiAgICAvKipcbiAgICAgKiBAdHlwZSB7Q29sb3JwaWNrZXJ9XG4gICAgICovXG4gICAgdGhpcy5jb2xvcnBpY2tlciA9IGNvbG9ycGlja2VyO1xuICAgIC8qKlxuICAgICAqIEB0eXBlIHtqUXVlcnl9XG4gICAgICovXG4gICAgdGhpcy5wb3BvdmVyVGFyZ2V0ID0gbnVsbDtcbiAgICAvKipcbiAgICAgKiBAdHlwZSB7alF1ZXJ5fVxuICAgICAqL1xuICAgIHRoaXMucG9wb3ZlclRpcCA9IG51bGw7XG5cbiAgICAvKipcbiAgICAgKiBJZiB0cnVlLCB0aGUgbGF0ZXN0IGNsaWNrIHdhcyBpbnNpZGUgdGhlIHBvcG92ZXJcbiAgICAgKiBAdHlwZSB7Ym9vbGVhbn1cbiAgICAgKi9cbiAgICB0aGlzLmNsaWNraW5nID0gZmFsc2U7XG4gICAgLyoqXG4gICAgICogQHR5cGUge2Jvb2xlYW59XG4gICAgICovXG4gICAgdGhpcy5oaWRkaW5nID0gZmFsc2U7XG4gICAgLyoqXG4gICAgICogQHR5cGUge2Jvb2xlYW59XG4gICAgICovXG4gICAgdGhpcy5zaG93aW5nID0gZmFsc2U7XG4gIH1cblxuICAvKipcbiAgICogQHByaXZhdGVcbiAgICogQHJldHVybnMge2pRdWVyeXxmYWxzZX1cbiAgICovXG5cblxuICBfY3JlYXRlQ2xhc3MoUG9wdXBIYW5kbGVyLCBbe1xuICAgIGtleTogJ2JpbmQnLFxuXG5cbiAgICAvKipcbiAgICAgKiBCaW5kcyB0aGUgZGlmZmVyZW50IGNvbG9ycGlja2VyIGVsZW1lbnRzIHRvIHRoZSBmb2N1cy9tb3VzZS90b3VjaCBldmVudHMgc28gaXQgcmVhY3RzIGluIG9yZGVyIHRvIHNob3cgb3JcbiAgICAgKiBoaWRlIHRoZSBjb2xvcnBpY2tlciBwb3B1cCBhY2NvcmRpbmdseS4gSXQgYWxzbyBhZGRzIHRoZSBwcm9wZXIgY2xhc3Nlcy5cbiAgICAgKi9cbiAgICB2YWx1ZTogZnVuY3Rpb24gYmluZCgpIHtcbiAgICAgIHZhciBjcCA9IHRoaXMuY29sb3JwaWNrZXI7XG5cbiAgICAgIGlmIChjcC5vcHRpb25zLmlubGluZSkge1xuICAgICAgICBjcC5waWNrZXIuYWRkQ2xhc3MoJ2NvbG9ycGlja2VyLWlubGluZSBjb2xvcnBpY2tlci12aXNpYmxlJyk7XG4gICAgICAgIHJldHVybjsgLy8gbm8gbmVlZCB0byBiaW5kIHNob3cvaGlkZSBldmVudHMgZm9yIGlubGluZSBlbGVtZW50c1xuICAgICAgfVxuXG4gICAgICBjcC5waWNrZXIuYWRkQ2xhc3MoJ2NvbG9ycGlja2VyLXBvcHVwIGNvbG9ycGlja2VyLWhpZGRlbicpO1xuXG4gICAgICAvLyB0aGVyZSBpcyBubyBpbnB1dCBvciBhZGRvblxuICAgICAgaWYgKCF0aGlzLmhhc0lucHV0ICYmICF0aGlzLmhhc0FkZG9uKSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cblxuICAgICAgLy8gY3JlYXRlIEJvb3RzdHJhcCA0IHBvcG92ZXJcbiAgICAgIGlmIChjcC5vcHRpb25zLnBvcG92ZXIpIHtcbiAgICAgICAgdGhpcy5jcmVhdGVQb3BvdmVyKCk7XG4gICAgICB9XG5cbiAgICAgIC8vIGJpbmQgYWRkb24gc2hvdy9oaWRlIGV2ZW50c1xuICAgICAgaWYgKHRoaXMuaGFzQWRkb24pIHtcbiAgICAgICAgLy8gZW5hYmxlIGZvY3VzIG9uIGFkZG9uc1xuICAgICAgICBpZiAoIXRoaXMuYWRkb24uYXR0cigndGFiaW5kZXgnKSkge1xuICAgICAgICAgIHRoaXMuYWRkb24uYXR0cigndGFiaW5kZXgnLCAwKTtcbiAgICAgICAgfVxuXG4gICAgICAgIHRoaXMuYWRkb24ub24oe1xuICAgICAgICAgICdtb3VzZWRvd24uY29sb3JwaWNrZXIgdG91Y2hzdGFydC5jb2xvcnBpY2tlcic6IF9qcXVlcnkyLmRlZmF1bHQucHJveHkodGhpcy50b2dnbGUsIHRoaXMpXG4gICAgICAgIH0pO1xuXG4gICAgICAgIHRoaXMuYWRkb24ub24oe1xuICAgICAgICAgICdmb2N1cy5jb2xvcnBpY2tlcic6IF9qcXVlcnkyLmRlZmF1bHQucHJveHkodGhpcy5zaG93LCB0aGlzKVxuICAgICAgICB9KTtcblxuICAgICAgICB0aGlzLmFkZG9uLm9uKHtcbiAgICAgICAgICAnZm9jdXNvdXQuY29sb3JwaWNrZXInOiBfanF1ZXJ5Mi5kZWZhdWx0LnByb3h5KHRoaXMuaGlkZSwgdGhpcylcbiAgICAgICAgfSk7XG4gICAgICB9XG5cbiAgICAgIC8vIGJpbmQgaW5wdXQgc2hvdy9oaWRlIGV2ZW50c1xuICAgICAgaWYgKHRoaXMuaGFzSW5wdXQgJiYgIXRoaXMuaGFzQWRkb24pIHtcbiAgICAgICAgdGhpcy5pbnB1dC5vbih7XG4gICAgICAgICAgJ21vdXNlZG93bi5jb2xvcnBpY2tlciB0b3VjaHN0YXJ0LmNvbG9ycGlja2VyJzogX2pxdWVyeTIuZGVmYXVsdC5wcm94eSh0aGlzLnNob3csIHRoaXMpLFxuICAgICAgICAgICdmb2N1cy5jb2xvcnBpY2tlcic6IF9qcXVlcnkyLmRlZmF1bHQucHJveHkodGhpcy5zaG93LCB0aGlzKVxuICAgICAgICB9KTtcblxuICAgICAgICB0aGlzLmlucHV0Lm9uKHtcbiAgICAgICAgICAnZm9jdXNvdXQuY29sb3JwaWNrZXInOiBfanF1ZXJ5Mi5kZWZhdWx0LnByb3h5KHRoaXMuaGlkZSwgdGhpcylcbiAgICAgICAgfSk7XG4gICAgICB9XG5cbiAgICAgIC8vIHJlcG9zaXRpb24gcG9wdXAgb24gd2luZG93IHJlc2l6ZVxuICAgICAgKDAsIF9qcXVlcnkyLmRlZmF1bHQpKHRoaXMucm9vdCkub24oJ3Jlc2l6ZS5jb2xvcnBpY2tlcicsIF9qcXVlcnkyLmRlZmF1bHQucHJveHkodGhpcy5yZXBvc2l0aW9uLCB0aGlzKSk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogVW5iaW5kcyBhbnkgZXZlbnQgYm91bmQgYnkgdGhpcyBoYW5kbGVyXG4gICAgICovXG5cbiAgfSwge1xuICAgIGtleTogJ3VuYmluZCcsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHVuYmluZCgpIHtcbiAgICAgIGlmICh0aGlzLmhhc0lucHV0KSB7XG4gICAgICAgIHRoaXMuaW5wdXQub2ZmKHtcbiAgICAgICAgICAnbW91c2Vkb3duLmNvbG9ycGlja2VyIHRvdWNoc3RhcnQuY29sb3JwaWNrZXInOiBfanF1ZXJ5Mi5kZWZhdWx0LnByb3h5KHRoaXMuc2hvdywgdGhpcyksXG4gICAgICAgICAgJ2ZvY3VzLmNvbG9ycGlja2VyJzogX2pxdWVyeTIuZGVmYXVsdC5wcm94eSh0aGlzLnNob3csIHRoaXMpXG4gICAgICAgIH0pO1xuICAgICAgICB0aGlzLmlucHV0Lm9mZih7XG4gICAgICAgICAgJ2ZvY3Vzb3V0LmNvbG9ycGlja2VyJzogX2pxdWVyeTIuZGVmYXVsdC5wcm94eSh0aGlzLmhpZGUsIHRoaXMpXG4gICAgICAgIH0pO1xuICAgICAgfVxuXG4gICAgICBpZiAodGhpcy5oYXNBZGRvbikge1xuICAgICAgICB0aGlzLmFkZG9uLm9mZih7XG4gICAgICAgICAgJ21vdXNlZG93bi5jb2xvcnBpY2tlciB0b3VjaHN0YXJ0LmNvbG9ycGlja2VyJzogX2pxdWVyeTIuZGVmYXVsdC5wcm94eSh0aGlzLnRvZ2dsZSwgdGhpcylcbiAgICAgICAgfSk7XG4gICAgICAgIHRoaXMuYWRkb24ub2ZmKHtcbiAgICAgICAgICAnZm9jdXMuY29sb3JwaWNrZXInOiBfanF1ZXJ5Mi5kZWZhdWx0LnByb3h5KHRoaXMuc2hvdywgdGhpcylcbiAgICAgICAgfSk7XG4gICAgICAgIHRoaXMuYWRkb24ub2ZmKHtcbiAgICAgICAgICAnZm9jdXNvdXQuY29sb3JwaWNrZXInOiBfanF1ZXJ5Mi5kZWZhdWx0LnByb3h5KHRoaXMuaGlkZSwgdGhpcylcbiAgICAgICAgfSk7XG4gICAgICB9XG5cbiAgICAgIGlmICh0aGlzLnBvcG92ZXJUYXJnZXQpIHtcbiAgICAgICAgdGhpcy5wb3BvdmVyVGFyZ2V0LnBvcG92ZXIoJ2Rpc3Bvc2UnKTtcbiAgICAgIH1cblxuICAgICAgKDAsIF9qcXVlcnkyLmRlZmF1bHQpKHRoaXMucm9vdCkub2ZmKCdyZXNpemUuY29sb3JwaWNrZXInLCBfanF1ZXJ5Mi5kZWZhdWx0LnByb3h5KHRoaXMucmVwb3NpdGlvbiwgdGhpcykpO1xuICAgICAgKDAsIF9qcXVlcnkyLmRlZmF1bHQpKHRoaXMucm9vdC5kb2N1bWVudCkub2ZmKCdtb3VzZWRvd24uY29sb3JwaWNrZXIgdG91Y2hzdGFydC5jb2xvcnBpY2tlcicsIF9qcXVlcnkyLmRlZmF1bHQucHJveHkodGhpcy5oaWRlLCB0aGlzKSk7XG4gICAgICAoMCwgX2pxdWVyeTIuZGVmYXVsdCkodGhpcy5yb290LmRvY3VtZW50KS5vZmYoJ21vdXNlZG93bi5jb2xvcnBpY2tlciB0b3VjaHN0YXJ0LmNvbG9ycGlja2VyJywgX2pxdWVyeTIuZGVmYXVsdC5wcm94eSh0aGlzLm9uQ2xpY2tpbmdJbnNpZGUsIHRoaXMpKTtcbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdpc0NsaWNraW5nSW5zaWRlJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gaXNDbGlja2luZ0luc2lkZShlKSB7XG4gICAgICBpZiAoIWUpIHtcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gdGhpcy5pc09ySXNJbnNpZGUodGhpcy5wb3BvdmVyVGlwLCBlLmN1cnJlbnRUYXJnZXQpIHx8IHRoaXMuaXNPcklzSW5zaWRlKHRoaXMucG9wb3ZlclRpcCwgZS50YXJnZXQpIHx8IHRoaXMuaXNPcklzSW5zaWRlKHRoaXMuY29sb3JwaWNrZXIucGlja2VyLCBlLmN1cnJlbnRUYXJnZXQpIHx8IHRoaXMuaXNPcklzSW5zaWRlKHRoaXMuY29sb3JwaWNrZXIucGlja2VyLCBlLnRhcmdldCk7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnaXNPcklzSW5zaWRlJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gaXNPcklzSW5zaWRlKGNvbnRhaW5lciwgZWxlbWVudCkge1xuICAgICAgaWYgKCFjb250YWluZXIgfHwgIWVsZW1lbnQpIHtcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgfVxuXG4gICAgICBlbGVtZW50ID0gKDAsIF9qcXVlcnkyLmRlZmF1bHQpKGVsZW1lbnQpO1xuXG4gICAgICByZXR1cm4gZWxlbWVudC5pcyhjb250YWluZXIpIHx8IGNvbnRhaW5lci5maW5kKGVsZW1lbnQpLmxlbmd0aCA+IDA7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnb25DbGlja2luZ0luc2lkZScsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIG9uQ2xpY2tpbmdJbnNpZGUoZSkge1xuICAgICAgdGhpcy5jbGlja2luZyA9IHRoaXMuaXNDbGlja2luZ0luc2lkZShlKTtcbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdjcmVhdGVQb3BvdmVyJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gY3JlYXRlUG9wb3ZlcigpIHtcbiAgICAgIHZhciBjcCA9IHRoaXMuY29sb3JwaWNrZXI7XG5cbiAgICAgIHRoaXMucG9wb3ZlclRhcmdldCA9IHRoaXMuaGFzQWRkb24gPyB0aGlzLmFkZG9uIDogdGhpcy5pbnB1dDtcblxuICAgICAgY3AucGlja2VyLmFkZENsYXNzKCdjb2xvcnBpY2tlci1icy1wb3BvdmVyLWNvbnRlbnQnKTtcblxuICAgICAgdGhpcy5wb3BvdmVyVGFyZ2V0LnBvcG92ZXIoX2pxdWVyeTIuZGVmYXVsdC5leHRlbmQodHJ1ZSwge30sIF9vcHRpb25zMi5kZWZhdWx0LnBvcG92ZXIsIGNwLm9wdGlvbnMucG9wb3ZlciwgeyB0cmlnZ2VyOiAnbWFudWFsJywgY29udGVudDogY3AucGlja2VyLCBodG1sOiB0cnVlIH0pKTtcblxuICAgICAgdGhpcy5wb3BvdmVyVGlwID0gKDAsIF9qcXVlcnkyLmRlZmF1bHQpKHRoaXMucG9wb3ZlclRhcmdldC5wb3BvdmVyKCdnZXRUaXBFbGVtZW50JykuZGF0YSgnYnMucG9wb3ZlcicpLnRpcCk7XG4gICAgICB0aGlzLnBvcG92ZXJUaXAuYWRkQ2xhc3MoJ2NvbG9ycGlja2VyLWJzLXBvcG92ZXInKTtcblxuICAgICAgdGhpcy5wb3BvdmVyVGFyZ2V0Lm9uKCdzaG93bi5icy5wb3BvdmVyJywgX2pxdWVyeTIuZGVmYXVsdC5wcm94eSh0aGlzLmZpcmVTaG93LCB0aGlzKSk7XG4gICAgICB0aGlzLnBvcG92ZXJUYXJnZXQub24oJ2hpZGRlbi5icy5wb3BvdmVyJywgX2pxdWVyeTIuZGVmYXVsdC5wcm94eSh0aGlzLmZpcmVIaWRlLCB0aGlzKSk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogSWYgdGhlIHdpZGdldCBpcyBub3QgaW5zaWRlIGEgY29udGFpbmVyIG9yIGlubGluZSwgcmVhcnJhbmdlcyBpdHMgcG9zaXRpb24gcmVsYXRpdmUgdG8gaXRzIGVsZW1lbnQgb2Zmc2V0LlxuICAgICAqXG4gICAgICogQHBhcmFtIHtFdmVudH0gW2VdXG4gICAgICogQHByaXZhdGVcbiAgICAgKi9cblxuICB9LCB7XG4gICAga2V5OiAncmVwb3NpdGlvbicsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHJlcG9zaXRpb24oZSkge1xuICAgICAgaWYgKHRoaXMucG9wb3ZlclRhcmdldCAmJiB0aGlzLmlzVmlzaWJsZSgpKSB7XG4gICAgICAgIHRoaXMucG9wb3ZlclRhcmdldC5wb3BvdmVyKCd1cGRhdGUnKTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBUb2dnbGVzIHRoZSBjb2xvcnBpY2tlciBiZXR3ZWVuIHZpc2libGUgb3IgaGlkZGVuXG4gICAgICpcbiAgICAgKiBAZmlyZXMgQ29sb3JwaWNrZXIjY29sb3JwaWNrZXJTaG93XG4gICAgICogQGZpcmVzIENvbG9ycGlja2VyI2NvbG9ycGlja2VySGlkZVxuICAgICAqIEBwYXJhbSB7RXZlbnR9IFtlXVxuICAgICAqL1xuXG4gIH0sIHtcbiAgICBrZXk6ICd0b2dnbGUnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiB0b2dnbGUoZSkge1xuICAgICAgaWYgKHRoaXMuaXNWaXNpYmxlKCkpIHtcbiAgICAgICAgdGhpcy5oaWRlKGUpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdGhpcy5zaG93KGUpO1xuICAgICAgfVxuICAgIH1cblxuICAgIC8qKlxuICAgICAqIFNob3dzIHRoZSBjb2xvcnBpY2tlciB3aWRnZXQgaWYgaGlkZGVuLlxuICAgICAqXG4gICAgICogQGZpcmVzIENvbG9ycGlja2VyI2NvbG9ycGlja2VyU2hvd1xuICAgICAqIEBwYXJhbSB7RXZlbnR9IFtlXVxuICAgICAqL1xuXG4gIH0sIHtcbiAgICBrZXk6ICdzaG93JyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gc2hvdyhlKSB7XG4gICAgICBpZiAodGhpcy5pc1Zpc2libGUoKSB8fCB0aGlzLnNob3dpbmcgfHwgdGhpcy5oaWRkaW5nKSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cblxuICAgICAgdGhpcy5zaG93aW5nID0gdHJ1ZTtcbiAgICAgIHRoaXMuaGlkZGluZyA9IGZhbHNlO1xuICAgICAgdGhpcy5jbGlja2luZyA9IGZhbHNlO1xuXG4gICAgICB2YXIgY3AgPSB0aGlzLmNvbG9ycGlja2VyO1xuXG4gICAgICBjcC5sYXN0RXZlbnQuYWxpYXMgPSAnc2hvdyc7XG4gICAgICBjcC5sYXN0RXZlbnQuZSA9IGU7XG5cbiAgICAgIC8vIFByZXZlbnQgc2hvd2luZyBicm93c2VyIG5hdGl2ZSBIVE1MNSBjb2xvcnBpY2tlclxuICAgICAgaWYgKGUgJiYgKCF0aGlzLmhhc0lucHV0IHx8IHRoaXMuaW5wdXQuYXR0cigndHlwZScpID09PSAnY29sb3InKSAmJiBlICYmIGUucHJldmVudERlZmF1bHQpIHtcbiAgICAgICAgZS5zdG9wUHJvcGFnYXRpb24oKTtcbiAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgfVxuXG4gICAgICAvLyBJZiBpdCdzIGEgcG9wb3ZlciwgYWRkIGV2ZW50IHRvIHRoZSBkb2N1bWVudCB0byBoaWRlIHRoZSBwaWNrZXIgd2hlbiBjbGlja2luZyBvdXRzaWRlIG9mIGl0XG4gICAgICBpZiAodGhpcy5pc1BvcG92ZXIpIHtcbiAgICAgICAgKDAsIF9qcXVlcnkyLmRlZmF1bHQpKHRoaXMucm9vdCkub24oJ3Jlc2l6ZS5jb2xvcnBpY2tlcicsIF9qcXVlcnkyLmRlZmF1bHQucHJveHkodGhpcy5yZXBvc2l0aW9uLCB0aGlzKSk7XG4gICAgICB9XG5cbiAgICAgIC8vIGFkZCB2aXNpYmxlIGNsYXNzIGJlZm9yZSBwb3BvdmVyIGlzIHNob3duXG4gICAgICBjcC5waWNrZXIuYWRkQ2xhc3MoJ2NvbG9ycGlja2VyLXZpc2libGUnKS5yZW1vdmVDbGFzcygnY29sb3JwaWNrZXItaGlkZGVuJyk7XG5cbiAgICAgIGlmICh0aGlzLnBvcG92ZXJUYXJnZXQpIHtcbiAgICAgICAgdGhpcy5wb3BvdmVyVGFyZ2V0LnBvcG92ZXIoJ3Nob3cnKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHRoaXMuZmlyZVNob3coKTtcbiAgICAgIH1cbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdmaXJlU2hvdycsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGZpcmVTaG93KCkge1xuICAgICAgdGhpcy5oaWRkaW5nID0gZmFsc2U7XG4gICAgICB0aGlzLnNob3dpbmcgPSBmYWxzZTtcblxuICAgICAgaWYgKHRoaXMuaXNQb3BvdmVyKSB7XG4gICAgICAgIC8vIEFkZCBldmVudCB0byBoaWRlIG9uIG91dHNpZGUgY2xpY2tcbiAgICAgICAgKDAsIF9qcXVlcnkyLmRlZmF1bHQpKHRoaXMucm9vdC5kb2N1bWVudCkub24oJ21vdXNlZG93bi5jb2xvcnBpY2tlciB0b3VjaHN0YXJ0LmNvbG9ycGlja2VyJywgX2pxdWVyeTIuZGVmYXVsdC5wcm94eSh0aGlzLmhpZGUsIHRoaXMpKTtcbiAgICAgICAgKDAsIF9qcXVlcnkyLmRlZmF1bHQpKHRoaXMucm9vdC5kb2N1bWVudCkub24oJ21vdXNlZG93bi5jb2xvcnBpY2tlciB0b3VjaHN0YXJ0LmNvbG9ycGlja2VyJywgX2pxdWVyeTIuZGVmYXVsdC5wcm94eSh0aGlzLm9uQ2xpY2tpbmdJbnNpZGUsIHRoaXMpKTtcbiAgICAgIH1cblxuICAgICAgLyoqXG4gICAgICAgKiAoQ29sb3JwaWNrZXIpIFdoZW4gc2hvdygpIGlzIGNhbGxlZCBhbmQgdGhlIHdpZGdldCBjYW4gYmUgc2hvd24uXG4gICAgICAgKlxuICAgICAgICogQGV2ZW50IENvbG9ycGlja2VyI2NvbG9ycGlja2VyU2hvd1xuICAgICAgICovXG4gICAgICB0aGlzLmNvbG9ycGlja2VyLnRyaWdnZXIoJ2NvbG9ycGlja2VyU2hvdycpO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEhpZGVzIHRoZSBjb2xvcnBpY2tlciB3aWRnZXQuXG4gICAgICogSGlkZSBpcyBwcmV2ZW50ZWQgd2hlbiBpdCBpcyB0cmlnZ2VyZWQgYnkgYW4gZXZlbnQgd2hvc2UgdGFyZ2V0IGVsZW1lbnQgaGFzIGJlZW4gY2xpY2tlZC90b3VjaGVkLlxuICAgICAqXG4gICAgICogQGZpcmVzIENvbG9ycGlja2VyI2NvbG9ycGlja2VySGlkZVxuICAgICAqIEBwYXJhbSB7RXZlbnR9IFtlXVxuICAgICAqL1xuXG4gIH0sIHtcbiAgICBrZXk6ICdoaWRlJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gaGlkZShlKSB7XG4gICAgICBpZiAodGhpcy5pc0hpZGRlbigpIHx8IHRoaXMuc2hvd2luZyB8fCB0aGlzLmhpZGRpbmcpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICB2YXIgY3AgPSB0aGlzLmNvbG9ycGlja2VyLFxuICAgICAgICAgIGNsaWNraW5nID0gdGhpcy5jbGlja2luZyB8fCB0aGlzLmlzQ2xpY2tpbmdJbnNpZGUoZSk7XG5cbiAgICAgIHRoaXMuaGlkZGluZyA9IHRydWU7XG4gICAgICB0aGlzLnNob3dpbmcgPSBmYWxzZTtcbiAgICAgIHRoaXMuY2xpY2tpbmcgPSBmYWxzZTtcblxuICAgICAgY3AubGFzdEV2ZW50LmFsaWFzID0gJ2hpZGUnO1xuICAgICAgY3AubGFzdEV2ZW50LmUgPSBlO1xuXG4gICAgICAvLyBUT0RPOiBmaXggaGF2aW5nIHRvIGNsaWNrIHR3aWNlIG91dHNpZGUgd2hlbiBsb3NpbmcgZm9jdXMgYW5kIGxhc3QgMiBjbGlja3Mgd2hlcmUgaW5zaWRlIHRoZSBjb2xvcnBpY2tlclxuXG4gICAgICAvLyBQcmV2ZW50IGhpZGUgaWYgdHJpZ2dlcmVkIGJ5IGFuIGV2ZW50IGFuZCBhbiBlbGVtZW50IGluc2lkZSB0aGUgY29sb3JwaWNrZXIgaGFzIGJlZW4gY2xpY2tlZC90b3VjaGVkXG4gICAgICBpZiAoY2xpY2tpbmcpIHtcbiAgICAgICAgdGhpcy5oaWRkaW5nID0gZmFsc2U7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cblxuICAgICAgaWYgKHRoaXMucG9wb3ZlclRhcmdldCkge1xuICAgICAgICB0aGlzLnBvcG92ZXJUYXJnZXQucG9wb3ZlcignaGlkZScpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdGhpcy5maXJlSGlkZSgpO1xuICAgICAgfVxuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ2ZpcmVIaWRlJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gZmlyZUhpZGUoKSB7XG4gICAgICB0aGlzLmhpZGRpbmcgPSBmYWxzZTtcbiAgICAgIHRoaXMuc2hvd2luZyA9IGZhbHNlO1xuXG4gICAgICB2YXIgY3AgPSB0aGlzLmNvbG9ycGlja2VyO1xuXG4gICAgICAvLyBhZGQgaGlkZGVuIGNsYXNzIGFmdGVyIHBvcG92ZXIgaXMgaGlkZGVuXG4gICAgICBjcC5waWNrZXIuYWRkQ2xhc3MoJ2NvbG9ycGlja2VyLWhpZGRlbicpLnJlbW92ZUNsYXNzKCdjb2xvcnBpY2tlci12aXNpYmxlJyk7XG5cbiAgICAgIC8vIFVuYmluZCB3aW5kb3cgYW5kIGRvY3VtZW50IGV2ZW50cywgc2luY2UgdGhlcmUgaXMgbm8gbmVlZCB0byBrZWVwIHRoZW0gd2hpbGUgdGhlIHBvcHVwIGlzIGhpZGRlblxuICAgICAgKDAsIF9qcXVlcnkyLmRlZmF1bHQpKHRoaXMucm9vdCkub2ZmKCdyZXNpemUuY29sb3JwaWNrZXInLCBfanF1ZXJ5Mi5kZWZhdWx0LnByb3h5KHRoaXMucmVwb3NpdGlvbiwgdGhpcykpO1xuICAgICAgKDAsIF9qcXVlcnkyLmRlZmF1bHQpKHRoaXMucm9vdC5kb2N1bWVudCkub2ZmKCdtb3VzZWRvd24uY29sb3JwaWNrZXIgdG91Y2hzdGFydC5jb2xvcnBpY2tlcicsIF9qcXVlcnkyLmRlZmF1bHQucHJveHkodGhpcy5oaWRlLCB0aGlzKSk7XG4gICAgICAoMCwgX2pxdWVyeTIuZGVmYXVsdCkodGhpcy5yb290LmRvY3VtZW50KS5vZmYoJ21vdXNlZG93bi5jb2xvcnBpY2tlciB0b3VjaHN0YXJ0LmNvbG9ycGlja2VyJywgX2pxdWVyeTIuZGVmYXVsdC5wcm94eSh0aGlzLm9uQ2xpY2tpbmdJbnNpZGUsIHRoaXMpKTtcblxuICAgICAgLyoqXG4gICAgICAgKiAoQ29sb3JwaWNrZXIpIFdoZW4gaGlkZSgpIGlzIGNhbGxlZCBhbmQgdGhlIHdpZGdldCBjYW4gYmUgaGlkZGVuLlxuICAgICAgICpcbiAgICAgICAqIEBldmVudCBDb2xvcnBpY2tlciNjb2xvcnBpY2tlckhpZGVcbiAgICAgICAqL1xuICAgICAgY3AudHJpZ2dlcignY29sb3JwaWNrZXJIaWRlJyk7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnZm9jdXMnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBmb2N1cygpIHtcbiAgICAgIGlmICh0aGlzLmhhc0FkZG9uKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmFkZG9uLmZvY3VzKCk7XG4gICAgICB9XG4gICAgICBpZiAodGhpcy5oYXNJbnB1dCkge1xuICAgICAgICByZXR1cm4gdGhpcy5pbnB1dC5mb2N1cygpO1xuICAgICAgfVxuICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIFJldHVybnMgdHJ1ZSBpZiB0aGUgY29sb3JwaWNrZXIgZWxlbWVudCBoYXMgdGhlIGNvbG9ycGlja2VyLXZpc2libGUgY2xhc3MgYW5kIG5vdCB0aGUgY29sb3JwaWNrZXItaGlkZGVuIG9uZS5cbiAgICAgKiBGYWxzZSBvdGhlcndpc2UuXG4gICAgICpcbiAgICAgKiBAcmV0dXJucyB7Ym9vbGVhbn1cbiAgICAgKi9cblxuICB9LCB7XG4gICAga2V5OiAnaXNWaXNpYmxlJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gaXNWaXNpYmxlKCkge1xuICAgICAgcmV0dXJuIHRoaXMuY29sb3JwaWNrZXIucGlja2VyLmhhc0NsYXNzKCdjb2xvcnBpY2tlci12aXNpYmxlJykgJiYgIXRoaXMuY29sb3JwaWNrZXIucGlja2VyLmhhc0NsYXNzKCdjb2xvcnBpY2tlci1oaWRkZW4nKTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBSZXR1cm5zIHRydWUgaWYgdGhlIGNvbG9ycGlja2VyIGVsZW1lbnQgaGFzIHRoZSBjb2xvcnBpY2tlci1oaWRkZW4gY2xhc3MgYW5kIG5vdCB0aGUgY29sb3JwaWNrZXItdmlzaWJsZSBvbmUuXG4gICAgICogRmFsc2Ugb3RoZXJ3aXNlLlxuICAgICAqXG4gICAgICogQHJldHVybnMge2Jvb2xlYW59XG4gICAgICovXG5cbiAgfSwge1xuICAgIGtleTogJ2lzSGlkZGVuJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gaXNIaWRkZW4oKSB7XG4gICAgICByZXR1cm4gdGhpcy5jb2xvcnBpY2tlci5waWNrZXIuaGFzQ2xhc3MoJ2NvbG9ycGlja2VyLWhpZGRlbicpICYmICF0aGlzLmNvbG9ycGlja2VyLnBpY2tlci5oYXNDbGFzcygnY29sb3JwaWNrZXItdmlzaWJsZScpO1xuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ2lucHV0JyxcbiAgICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICAgIHJldHVybiB0aGlzLmNvbG9ycGlja2VyLmlucHV0SGFuZGxlci5pbnB1dDtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBAcHJpdmF0ZVxuICAgICAqIEByZXR1cm5zIHtib29sZWFufVxuICAgICAqL1xuXG4gIH0sIHtcbiAgICBrZXk6ICdoYXNJbnB1dCcsXG4gICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgICByZXR1cm4gdGhpcy5jb2xvcnBpY2tlci5pbnB1dEhhbmRsZXIuaGFzSW5wdXQoKTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBAcHJpdmF0ZVxuICAgICAqIEByZXR1cm5zIHtqUXVlcnl8ZmFsc2V9XG4gICAgICovXG5cbiAgfSwge1xuICAgIGtleTogJ2FkZG9uJyxcbiAgICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICAgIHJldHVybiB0aGlzLmNvbG9ycGlja2VyLmFkZG9uSGFuZGxlci5hZGRvbjtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBAcHJpdmF0ZVxuICAgICAqIEByZXR1cm5zIHtib29sZWFufVxuICAgICAqL1xuXG4gIH0sIHtcbiAgICBrZXk6ICdoYXNBZGRvbicsXG4gICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgICByZXR1cm4gdGhpcy5jb2xvcnBpY2tlci5hZGRvbkhhbmRsZXIuaGFzQWRkb24oKTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBAcHJpdmF0ZVxuICAgICAqIEByZXR1cm5zIHtib29sZWFufVxuICAgICAqL1xuXG4gIH0sIHtcbiAgICBrZXk6ICdpc1BvcG92ZXInLFxuICAgIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgICAgcmV0dXJuICF0aGlzLmNvbG9ycGlja2VyLm9wdGlvbnMuaW5saW5lICYmICEhdGhpcy5wb3BvdmVyVGlwO1xuICAgIH1cbiAgfV0pO1xuXG4gIHJldHVybiBQb3B1cEhhbmRsZXI7XG59KCk7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IFBvcHVwSGFuZGxlcjtcbm1vZHVsZS5leHBvcnRzID0gZXhwb3J0cy5kZWZhdWx0O1xuXG4vKioqLyB9KSxcbi8qIDE1ICovXG4vKioqLyAoZnVuY3Rpb24obW9kdWxlLCBleHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKSB7XG5cblwidXNlIHN0cmljdFwiO1xuXG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfY3JlYXRlQ2xhc3MgPSBmdW5jdGlvbiAoKSB7IGZ1bmN0aW9uIGRlZmluZVByb3BlcnRpZXModGFyZ2V0LCBwcm9wcykgeyBmb3IgKHZhciBpID0gMDsgaSA8IHByb3BzLmxlbmd0aDsgaSsrKSB7IHZhciBkZXNjcmlwdG9yID0gcHJvcHNbaV07IGRlc2NyaXB0b3IuZW51bWVyYWJsZSA9IGRlc2NyaXB0b3IuZW51bWVyYWJsZSB8fCBmYWxzZTsgZGVzY3JpcHRvci5jb25maWd1cmFibGUgPSB0cnVlOyBpZiAoXCJ2YWx1ZVwiIGluIGRlc2NyaXB0b3IpIGRlc2NyaXB0b3Iud3JpdGFibGUgPSB0cnVlOyBPYmplY3QuZGVmaW5lUHJvcGVydHkodGFyZ2V0LCBkZXNjcmlwdG9yLmtleSwgZGVzY3JpcHRvcik7IH0gfSByZXR1cm4gZnVuY3Rpb24gKENvbnN0cnVjdG9yLCBwcm90b1Byb3BzLCBzdGF0aWNQcm9wcykgeyBpZiAocHJvdG9Qcm9wcykgZGVmaW5lUHJvcGVydGllcyhDb25zdHJ1Y3Rvci5wcm90b3R5cGUsIHByb3RvUHJvcHMpOyBpZiAoc3RhdGljUHJvcHMpIGRlZmluZVByb3BlcnRpZXMoQ29uc3RydWN0b3IsIHN0YXRpY1Byb3BzKTsgcmV0dXJuIENvbnN0cnVjdG9yOyB9OyB9KCk7XG5cbnZhciBfanF1ZXJ5ID0gX193ZWJwYWNrX3JlcXVpcmVfXygwKTtcblxudmFyIF9qcXVlcnkyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfanF1ZXJ5KTtcblxudmFyIF9Db2xvckl0ZW0gPSBfX3dlYnBhY2tfcmVxdWlyZV9fKDIpO1xuXG52YXIgX0NvbG9ySXRlbTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9Db2xvckl0ZW0pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG5mdW5jdGlvbiBfY2xhc3NDYWxsQ2hlY2soaW5zdGFuY2UsIENvbnN0cnVjdG9yKSB7IGlmICghKGluc3RhbmNlIGluc3RhbmNlb2YgQ29uc3RydWN0b3IpKSB7IHRocm93IG5ldyBUeXBlRXJyb3IoXCJDYW5ub3QgY2FsbCBhIGNsYXNzIGFzIGEgZnVuY3Rpb25cIik7IH0gfVxuXG4vKipcbiAqIEhhbmRsZXMgZXZlcnl0aGluZyByZWxhdGVkIHRvIHRoZSBjb2xvcnBpY2tlciBpbnB1dFxuICogQGlnbm9yZVxuICovXG52YXIgSW5wdXRIYW5kbGVyID0gZnVuY3Rpb24gKCkge1xuICAvKipcbiAgICogQHBhcmFtIHtDb2xvcnBpY2tlcn0gY29sb3JwaWNrZXJcbiAgICovXG4gIGZ1bmN0aW9uIElucHV0SGFuZGxlcihjb2xvcnBpY2tlcikge1xuICAgIF9jbGFzc0NhbGxDaGVjayh0aGlzLCBJbnB1dEhhbmRsZXIpO1xuXG4gICAgLyoqXG4gICAgICogQHR5cGUge0NvbG9ycGlja2VyfVxuICAgICAqL1xuICAgIHRoaXMuY29sb3JwaWNrZXIgPSBjb2xvcnBpY2tlcjtcbiAgICAvKipcbiAgICAgKiBAdHlwZSB7alF1ZXJ5fGZhbHNlfVxuICAgICAqL1xuICAgIHRoaXMuaW5wdXQgPSB0aGlzLmNvbG9ycGlja2VyLmVsZW1lbnQuaXMoJ2lucHV0JykgPyB0aGlzLmNvbG9ycGlja2VyLmVsZW1lbnQgOiB0aGlzLmNvbG9ycGlja2VyLm9wdGlvbnMuaW5wdXQgPyB0aGlzLmNvbG9ycGlja2VyLmVsZW1lbnQuZmluZCh0aGlzLmNvbG9ycGlja2VyLm9wdGlvbnMuaW5wdXQpIDogZmFsc2U7XG5cbiAgICBpZiAodGhpcy5pbnB1dCAmJiB0aGlzLmlucHV0Lmxlbmd0aCA9PT0gMCkge1xuICAgICAgdGhpcy5pbnB1dCA9IGZhbHNlO1xuICAgIH1cblxuICAgIHRoaXMuX2luaXRWYWx1ZSgpO1xuICB9XG5cbiAgX2NyZWF0ZUNsYXNzKElucHV0SGFuZGxlciwgW3tcbiAgICBrZXk6ICdiaW5kJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gYmluZCgpIHtcbiAgICAgIGlmICghdGhpcy5oYXNJbnB1dCgpKSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cbiAgICAgIHRoaXMuaW5wdXQub24oe1xuICAgICAgICAna2V5dXAuY29sb3JwaWNrZXInOiBfanF1ZXJ5Mi5kZWZhdWx0LnByb3h5KHRoaXMub25rZXl1cCwgdGhpcylcbiAgICAgIH0pO1xuICAgICAgdGhpcy5pbnB1dC5vbih7XG4gICAgICAgICdjaGFuZ2UuY29sb3JwaWNrZXInOiBfanF1ZXJ5Mi5kZWZhdWx0LnByb3h5KHRoaXMub25jaGFuZ2UsIHRoaXMpXG4gICAgICB9KTtcbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICd1bmJpbmQnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiB1bmJpbmQoKSB7XG4gICAgICBpZiAoIXRoaXMuaGFzSW5wdXQoKSkge1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG4gICAgICB0aGlzLmlucHV0Lm9mZignLmNvbG9ycGlja2VyJyk7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnX2luaXRWYWx1ZScsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIF9pbml0VmFsdWUoKSB7XG4gICAgICBpZiAoIXRoaXMuaGFzSW5wdXQoKSkge1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG5cbiAgICAgIHZhciB2YWwgPSAnJztcblxuICAgICAgW1xuICAgICAgLy8gY2FuZGlkYXRlczpcbiAgICAgIHRoaXMuaW5wdXQudmFsKCksIHRoaXMuaW5wdXQuZGF0YSgnY29sb3InKSwgdGhpcy5pbnB1dC5hdHRyKCdkYXRhLWNvbG9yJyldLm1hcChmdW5jdGlvbiAoaXRlbSkge1xuICAgICAgICBpZiAoaXRlbSAmJiB2YWwgPT09ICcnKSB7XG4gICAgICAgICAgdmFsID0gaXRlbTtcbiAgICAgICAgfVxuICAgICAgfSk7XG5cbiAgICAgIGlmICh2YWwgaW5zdGFuY2VvZiBfQ29sb3JJdGVtMi5kZWZhdWx0KSB7XG4gICAgICAgIHZhbCA9IHRoaXMuZ2V0Rm9ybWF0dGVkQ29sb3IodmFsLnN0cmluZyh0aGlzLmNvbG9ycGlja2VyLmZvcm1hdCkpO1xuICAgICAgfSBlbHNlIGlmICghKHR5cGVvZiB2YWwgPT09ICdzdHJpbmcnIHx8IHZhbCBpbnN0YW5jZW9mIFN0cmluZykpIHtcbiAgICAgICAgdmFsID0gJyc7XG4gICAgICB9XG5cbiAgICAgIHRoaXMuaW5wdXQucHJvcCgndmFsdWUnLCB2YWwpO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIFJldHVybnMgdGhlIGNvbG9yIHN0cmluZyBmcm9tIHRoZSBpbnB1dCB2YWx1ZS5cbiAgICAgKiBJZiB0aGVyZSBpcyBubyBpbnB1dCB0aGUgcmV0dXJuIHZhbHVlIGlzIGZhbHNlLlxuICAgICAqXG4gICAgICogQHJldHVybnMge1N0cmluZ3xib29sZWFufVxuICAgICAqL1xuXG4gIH0sIHtcbiAgICBrZXk6ICdnZXRWYWx1ZScsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGdldFZhbHVlKCkge1xuICAgICAgaWYgKCF0aGlzLmhhc0lucHV0KCkpIHtcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gdGhpcy5pbnB1dC52YWwoKTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBJZiB0aGUgaW5wdXQgZWxlbWVudCBpcyBwcmVzZW50LCBpdCB1cGRhdGVzIHRoZSB2YWx1ZSB3aXRoIHRoZSBjdXJyZW50IGNvbG9yIG9iamVjdCBjb2xvciBzdHJpbmcuXG4gICAgICogSWYgdGhlIHZhbHVlIGlzIGNoYW5nZWQsIHRoaXMgbWV0aG9kIGZpcmVzIGEgXCJjaGFuZ2VcIiBldmVudCBvbiB0aGUgaW5wdXQgZWxlbWVudC5cbiAgICAgKlxuICAgICAqIEBwYXJhbSB7U3RyaW5nfSB2YWxcbiAgICAgKlxuICAgICAqIEBmaXJlcyBDb2xvcnBpY2tlciNjaGFuZ2VcbiAgICAgKi9cblxuICB9LCB7XG4gICAga2V5OiAnc2V0VmFsdWUnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBzZXRWYWx1ZSh2YWwpIHtcbiAgICAgIGlmICghdGhpcy5oYXNJbnB1dCgpKSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cblxuICAgICAgdmFyIGlucHV0VmFsID0gdGhpcy5pbnB1dC5wcm9wKCd2YWx1ZScpO1xuXG4gICAgICB2YWwgPSB2YWwgPyB2YWwgOiAnJztcblxuICAgICAgaWYgKHZhbCA9PT0gKGlucHV0VmFsID8gaW5wdXRWYWwgOiAnJykpIHtcbiAgICAgICAgLy8gTm8gbmVlZCB0byBzZXQgdmFsdWUgb3IgdHJpZ2dlciBhbnkgZXZlbnQgaWYgbm90aGluZyBjaGFuZ2VkXG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cblxuICAgICAgdGhpcy5pbnB1dC5wcm9wKCd2YWx1ZScsIHZhbCk7XG5cbiAgICAgIC8qKlxuICAgICAgICogKElucHV0KSBUcmlnZ2VyZWQgb24gdGhlIGlucHV0IGVsZW1lbnQgd2hlbiBhIG5ldyBjb2xvciBpcyBzZWxlY3RlZC5cbiAgICAgICAqXG4gICAgICAgKiBAZXZlbnQgQ29sb3JwaWNrZXIjY2hhbmdlXG4gICAgICAgKi9cbiAgICAgIHRoaXMuaW5wdXQudHJpZ2dlcih7XG4gICAgICAgIHR5cGU6ICdjaGFuZ2UnLFxuICAgICAgICBjb2xvcnBpY2tlcjogdGhpcy5jb2xvcnBpY2tlcixcbiAgICAgICAgY29sb3I6IHRoaXMuY29sb3JwaWNrZXIuY29sb3IsXG4gICAgICAgIHZhbHVlOiB2YWxcbiAgICAgIH0pO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIFJldHVybnMgdGhlIGZvcm1hdHRlZCBjb2xvciBzdHJpbmcsIHdpdGggdGhlIGZvcm1hdHRpbmcgb3B0aW9ucyBhcHBsaWVkXG4gICAgICogKGUuZy4gdXNlSGFzaFByZWZpeClcbiAgICAgKlxuICAgICAqIEBwYXJhbSB7U3RyaW5nfG51bGx9IHZhbFxuICAgICAqXG4gICAgICogQHJldHVybnMge1N0cmluZ31cbiAgICAgKi9cblxuICB9LCB7XG4gICAga2V5OiAnZ2V0Rm9ybWF0dGVkQ29sb3InLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBnZXRGb3JtYXR0ZWRDb2xvcigpIHtcbiAgICAgIHZhciB2YWwgPSBhcmd1bWVudHMubGVuZ3RoID4gMCAmJiBhcmd1bWVudHNbMF0gIT09IHVuZGVmaW5lZCA/IGFyZ3VtZW50c1swXSA6IG51bGw7XG5cbiAgICAgIHZhbCA9IHZhbCA/IHZhbCA6IHRoaXMuY29sb3JwaWNrZXIuY29sb3JIYW5kbGVyLmdldENvbG9yU3RyaW5nKCk7XG5cbiAgICAgIGlmICghdmFsKSB7XG4gICAgICAgIHJldHVybiAnJztcbiAgICAgIH1cblxuICAgICAgdmFsID0gdGhpcy5jb2xvcnBpY2tlci5jb2xvckhhbmRsZXIucmVzb2x2ZUNvbG9yRGVsZWdhdGUodmFsLCBmYWxzZSk7XG5cbiAgICAgIGlmICh0aGlzLmNvbG9ycGlja2VyLm9wdGlvbnMudXNlSGFzaFByZWZpeCA9PT0gZmFsc2UpIHtcbiAgICAgICAgdmFsID0gdmFsLnJlcGxhY2UoL14jL2csICcnKTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIHZhbDtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBSZXR1cm5zIHRydWUgaWYgdGhlIHdpZGdldCBoYXMgYW4gYXNzb2NpYXRlZCBpbnB1dCBlbGVtZW50LCBmYWxzZSBvdGhlcndpc2VcbiAgICAgKiBAcmV0dXJucyB7Ym9vbGVhbn1cbiAgICAgKi9cblxuICB9LCB7XG4gICAga2V5OiAnaGFzSW5wdXQnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBoYXNJbnB1dCgpIHtcbiAgICAgIHJldHVybiB0aGlzLmlucHV0ICE9PSBmYWxzZTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBSZXR1cm5zIHRydWUgaWYgdGhlIGlucHV0IGV4aXN0cyBhbmQgaXMgZGlzYWJsZWRcbiAgICAgKiBAcmV0dXJucyB7Ym9vbGVhbn1cbiAgICAgKi9cblxuICB9LCB7XG4gICAga2V5OiAnaXNFbmFibGVkJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gaXNFbmFibGVkKCkge1xuICAgICAgcmV0dXJuIHRoaXMuaGFzSW5wdXQoKSAmJiAhdGhpcy5pc0Rpc2FibGVkKCk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogUmV0dXJucyB0cnVlIGlmIHRoZSBpbnB1dCBleGlzdHMgYW5kIGlzIGRpc2FibGVkXG4gICAgICogQHJldHVybnMge2Jvb2xlYW59XG4gICAgICovXG5cbiAgfSwge1xuICAgIGtleTogJ2lzRGlzYWJsZWQnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBpc0Rpc2FibGVkKCkge1xuICAgICAgcmV0dXJuIHRoaXMuaGFzSW5wdXQoKSAmJiB0aGlzLmlucHV0LnByb3AoJ2Rpc2FibGVkJykgPT09IHRydWU7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogRGlzYWJsZXMgdGhlIGlucHV0IGlmIGFueVxuICAgICAqXG4gICAgICogQGZpcmVzIENvbG9ycGlja2VyI2NvbG9ycGlja2VyRGlzYWJsZVxuICAgICAqIEByZXR1cm5zIHtib29sZWFufVxuICAgICAqL1xuXG4gIH0sIHtcbiAgICBrZXk6ICdkaXNhYmxlJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gZGlzYWJsZSgpIHtcbiAgICAgIGlmICh0aGlzLmhhc0lucHV0KCkpIHtcbiAgICAgICAgdGhpcy5pbnB1dC5wcm9wKCdkaXNhYmxlZCcsIHRydWUpO1xuICAgICAgfVxuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEVuYWJsZXMgdGhlIGlucHV0IGlmIGFueVxuICAgICAqXG4gICAgICogQGZpcmVzIENvbG9ycGlja2VyI2NvbG9ycGlja2VyRW5hYmxlXG4gICAgICogQHJldHVybnMge2Jvb2xlYW59XG4gICAgICovXG5cbiAgfSwge1xuICAgIGtleTogJ2VuYWJsZScsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGVuYWJsZSgpIHtcbiAgICAgIGlmICh0aGlzLmhhc0lucHV0KCkpIHtcbiAgICAgICAgdGhpcy5pbnB1dC5wcm9wKCdkaXNhYmxlZCcsIGZhbHNlKTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBDYWxscyBzZXRWYWx1ZSB3aXRoIHRoZSBjdXJyZW50IGludGVybmFsIGNvbG9yIHZhbHVlXG4gICAgICpcbiAgICAgKiBAZmlyZXMgQ29sb3JwaWNrZXIjY2hhbmdlXG4gICAgICovXG5cbiAgfSwge1xuICAgIGtleTogJ3VwZGF0ZScsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHVwZGF0ZSgpIHtcbiAgICAgIGlmICghdGhpcy5oYXNJbnB1dCgpKSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cblxuICAgICAgaWYgKHRoaXMuY29sb3JwaWNrZXIub3B0aW9ucy5hdXRvSW5wdXRGYWxsYmFjayA9PT0gZmFsc2UgJiYgdGhpcy5jb2xvcnBpY2tlci5jb2xvckhhbmRsZXIuaXNJbnZhbGlkQ29sb3IoKSkge1xuICAgICAgICAvLyBwcmV2ZW50IHVwZGF0ZSBpZiBjb2xvciBpcyBpbnZhbGlkLCBhdXRvSW5wdXRGYWxsYmFjayBpcyBkaXNhYmxlZCBhbmQgdGhlIGxhc3QgZXZlbnQgaXMga2V5dXAuXG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cblxuICAgICAgdGhpcy5zZXRWYWx1ZSh0aGlzLmdldEZvcm1hdHRlZENvbG9yKCkpO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEZ1bmN0aW9uIHRyaWdnZXJlZCB3aGVuIHRoZSBpbnB1dCBoYXMgY2hhbmdlZCwgc28gdGhlIGNvbG9ycGlja2VyIGdldHMgdXBkYXRlZC5cbiAgICAgKlxuICAgICAqIEBwcml2YXRlXG4gICAgICogQHBhcmFtIHtFdmVudH0gZVxuICAgICAqIEByZXR1cm5zIHtib29sZWFufVxuICAgICAqL1xuXG4gIH0sIHtcbiAgICBrZXk6ICdvbmNoYW5nZScsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIG9uY2hhbmdlKGUpIHtcbiAgICAgIHRoaXMuY29sb3JwaWNrZXIubGFzdEV2ZW50LmFsaWFzID0gJ2lucHV0LmNoYW5nZSc7XG4gICAgICB0aGlzLmNvbG9ycGlja2VyLmxhc3RFdmVudC5lID0gZTtcblxuICAgICAgdmFyIHZhbCA9IHRoaXMuZ2V0VmFsdWUoKTtcblxuICAgICAgaWYgKHZhbCAhPT0gZS52YWx1ZSkge1xuICAgICAgICB0aGlzLmNvbG9ycGlja2VyLnNldFZhbHVlKHZhbCk7XG4gICAgICB9XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogRnVuY3Rpb24gdHJpZ2dlcmVkIGFmdGVyIGEga2V5Ym9hcmQga2V5IGhhcyBiZWVuIHJlbGVhc2VkLlxuICAgICAqXG4gICAgICogQHByaXZhdGVcbiAgICAgKiBAcGFyYW0ge0V2ZW50fSBlXG4gICAgICogQHJldHVybnMge2Jvb2xlYW59XG4gICAgICovXG5cbiAgfSwge1xuICAgIGtleTogJ29ua2V5dXAnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBvbmtleXVwKGUpIHtcbiAgICAgIHRoaXMuY29sb3JwaWNrZXIubGFzdEV2ZW50LmFsaWFzID0gJ2lucHV0LmtleXVwJztcbiAgICAgIHRoaXMuY29sb3JwaWNrZXIubGFzdEV2ZW50LmUgPSBlO1xuXG4gICAgICB2YXIgdmFsID0gdGhpcy5nZXRWYWx1ZSgpO1xuXG4gICAgICBpZiAodmFsICE9PSBlLnZhbHVlKSB7XG4gICAgICAgIHRoaXMuY29sb3JwaWNrZXIuc2V0VmFsdWUodmFsKTtcbiAgICAgIH1cbiAgICB9XG4gIH1dKTtcblxuICByZXR1cm4gSW5wdXRIYW5kbGVyO1xufSgpO1xuXG5leHBvcnRzLmRlZmF1bHQgPSBJbnB1dEhhbmRsZXI7XG5tb2R1bGUuZXhwb3J0cyA9IGV4cG9ydHMuZGVmYXVsdDtcblxuLyoqKi8gfSksXG4vKiAxNiAqL1xuLyoqKi8gKGZ1bmN0aW9uKG1vZHVsZSwgZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXykge1xuXG5cInVzZSBzdHJpY3RcIjtcblxuXG52YXIgY29sb3JTdHJpbmcgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKDE3KTtcbnZhciBjb252ZXJ0ID0gX193ZWJwYWNrX3JlcXVpcmVfXygyMCk7XG5cbnZhciBfc2xpY2UgPSBbXS5zbGljZTtcblxudmFyIHNraXBwZWRNb2RlbHMgPSBbXG5cdC8vIHRvIGJlIGhvbmVzdCwgSSBkb24ndCByZWFsbHkgZmVlbCBsaWtlIGtleXdvcmQgYmVsb25ncyBpbiBjb2xvciBjb252ZXJ0LCBidXQgZWguXG5cdCdrZXl3b3JkJyxcblxuXHQvLyBncmF5IGNvbmZsaWN0cyB3aXRoIHNvbWUgbWV0aG9kIG5hbWVzLCBhbmQgaGFzIGl0cyBvd24gbWV0aG9kIGRlZmluZWQuXG5cdCdncmF5JyxcblxuXHQvLyBzaG91bGRuJ3QgcmVhbGx5IGJlIGluIGNvbG9yLWNvbnZlcnQgZWl0aGVyLi4uXG5cdCdoZXgnXG5dO1xuXG52YXIgaGFzaGVkTW9kZWxLZXlzID0ge307XG5PYmplY3Qua2V5cyhjb252ZXJ0KS5mb3JFYWNoKGZ1bmN0aW9uIChtb2RlbCkge1xuXHRoYXNoZWRNb2RlbEtleXNbX3NsaWNlLmNhbGwoY29udmVydFttb2RlbF0ubGFiZWxzKS5zb3J0KCkuam9pbignJyldID0gbW9kZWw7XG59KTtcblxudmFyIGxpbWl0ZXJzID0ge307XG5cbmZ1bmN0aW9uIENvbG9yKG9iaiwgbW9kZWwpIHtcblx0aWYgKCEodGhpcyBpbnN0YW5jZW9mIENvbG9yKSkge1xuXHRcdHJldHVybiBuZXcgQ29sb3Iob2JqLCBtb2RlbCk7XG5cdH1cblxuXHRpZiAobW9kZWwgJiYgbW9kZWwgaW4gc2tpcHBlZE1vZGVscykge1xuXHRcdG1vZGVsID0gbnVsbDtcblx0fVxuXG5cdGlmIChtb2RlbCAmJiAhKG1vZGVsIGluIGNvbnZlcnQpKSB7XG5cdFx0dGhyb3cgbmV3IEVycm9yKCdVbmtub3duIG1vZGVsOiAnICsgbW9kZWwpO1xuXHR9XG5cblx0dmFyIGk7XG5cdHZhciBjaGFubmVscztcblxuXHRpZiAob2JqID09IG51bGwpIHsgLy8gZXNsaW50LWRpc2FibGUtbGluZSBuby1lcS1udWxsLGVxZXFlcVxuXHRcdHRoaXMubW9kZWwgPSAncmdiJztcblx0XHR0aGlzLmNvbG9yID0gWzAsIDAsIDBdO1xuXHRcdHRoaXMudmFscGhhID0gMTtcblx0fSBlbHNlIGlmIChvYmogaW5zdGFuY2VvZiBDb2xvcikge1xuXHRcdHRoaXMubW9kZWwgPSBvYmoubW9kZWw7XG5cdFx0dGhpcy5jb2xvciA9IG9iai5jb2xvci5zbGljZSgpO1xuXHRcdHRoaXMudmFscGhhID0gb2JqLnZhbHBoYTtcblx0fSBlbHNlIGlmICh0eXBlb2Ygb2JqID09PSAnc3RyaW5nJykge1xuXHRcdHZhciByZXN1bHQgPSBjb2xvclN0cmluZy5nZXQob2JqKTtcblx0XHRpZiAocmVzdWx0ID09PSBudWxsKSB7XG5cdFx0XHR0aHJvdyBuZXcgRXJyb3IoJ1VuYWJsZSB0byBwYXJzZSBjb2xvciBmcm9tIHN0cmluZzogJyArIG9iaik7XG5cdFx0fVxuXG5cdFx0dGhpcy5tb2RlbCA9IHJlc3VsdC5tb2RlbDtcblx0XHRjaGFubmVscyA9IGNvbnZlcnRbdGhpcy5tb2RlbF0uY2hhbm5lbHM7XG5cdFx0dGhpcy5jb2xvciA9IHJlc3VsdC52YWx1ZS5zbGljZSgwLCBjaGFubmVscyk7XG5cdFx0dGhpcy52YWxwaGEgPSB0eXBlb2YgcmVzdWx0LnZhbHVlW2NoYW5uZWxzXSA9PT0gJ251bWJlcicgPyByZXN1bHQudmFsdWVbY2hhbm5lbHNdIDogMTtcblx0fSBlbHNlIGlmIChvYmoubGVuZ3RoKSB7XG5cdFx0dGhpcy5tb2RlbCA9IG1vZGVsIHx8ICdyZ2InO1xuXHRcdGNoYW5uZWxzID0gY29udmVydFt0aGlzLm1vZGVsXS5jaGFubmVscztcblx0XHR2YXIgbmV3QXJyID0gX3NsaWNlLmNhbGwob2JqLCAwLCBjaGFubmVscyk7XG5cdFx0dGhpcy5jb2xvciA9IHplcm9BcnJheShuZXdBcnIsIGNoYW5uZWxzKTtcblx0XHR0aGlzLnZhbHBoYSA9IHR5cGVvZiBvYmpbY2hhbm5lbHNdID09PSAnbnVtYmVyJyA/IG9ialtjaGFubmVsc10gOiAxO1xuXHR9IGVsc2UgaWYgKHR5cGVvZiBvYmogPT09ICdudW1iZXInKSB7XG5cdFx0Ly8gdGhpcyBpcyBhbHdheXMgUkdCIC0gY2FuIGJlIGNvbnZlcnRlZCBsYXRlciBvbi5cblx0XHRvYmogJj0gMHhGRkZGRkY7XG5cdFx0dGhpcy5tb2RlbCA9ICdyZ2InO1xuXHRcdHRoaXMuY29sb3IgPSBbXG5cdFx0XHQob2JqID4+IDE2KSAmIDB4RkYsXG5cdFx0XHQob2JqID4+IDgpICYgMHhGRixcblx0XHRcdG9iaiAmIDB4RkZcblx0XHRdO1xuXHRcdHRoaXMudmFscGhhID0gMTtcblx0fSBlbHNlIHtcblx0XHR0aGlzLnZhbHBoYSA9IDE7XG5cblx0XHR2YXIga2V5cyA9IE9iamVjdC5rZXlzKG9iaik7XG5cdFx0aWYgKCdhbHBoYScgaW4gb2JqKSB7XG5cdFx0XHRrZXlzLnNwbGljZShrZXlzLmluZGV4T2YoJ2FscGhhJyksIDEpO1xuXHRcdFx0dGhpcy52YWxwaGEgPSB0eXBlb2Ygb2JqLmFscGhhID09PSAnbnVtYmVyJyA/IG9iai5hbHBoYSA6IDA7XG5cdFx0fVxuXG5cdFx0dmFyIGhhc2hlZEtleXMgPSBrZXlzLnNvcnQoKS5qb2luKCcnKTtcblx0XHRpZiAoIShoYXNoZWRLZXlzIGluIGhhc2hlZE1vZGVsS2V5cykpIHtcblx0XHRcdHRocm93IG5ldyBFcnJvcignVW5hYmxlIHRvIHBhcnNlIGNvbG9yIGZyb20gb2JqZWN0OiAnICsgSlNPTi5zdHJpbmdpZnkob2JqKSk7XG5cdFx0fVxuXG5cdFx0dGhpcy5tb2RlbCA9IGhhc2hlZE1vZGVsS2V5c1toYXNoZWRLZXlzXTtcblxuXHRcdHZhciBsYWJlbHMgPSBjb252ZXJ0W3RoaXMubW9kZWxdLmxhYmVscztcblx0XHR2YXIgY29sb3IgPSBbXTtcblx0XHRmb3IgKGkgPSAwOyBpIDwgbGFiZWxzLmxlbmd0aDsgaSsrKSB7XG5cdFx0XHRjb2xvci5wdXNoKG9ialtsYWJlbHNbaV1dKTtcblx0XHR9XG5cblx0XHR0aGlzLmNvbG9yID0gemVyb0FycmF5KGNvbG9yKTtcblx0fVxuXG5cdC8vIHBlcmZvcm0gbGltaXRhdGlvbnMgKGNsYW1waW5nLCBldGMuKVxuXHRpZiAobGltaXRlcnNbdGhpcy5tb2RlbF0pIHtcblx0XHRjaGFubmVscyA9IGNvbnZlcnRbdGhpcy5tb2RlbF0uY2hhbm5lbHM7XG5cdFx0Zm9yIChpID0gMDsgaSA8IGNoYW5uZWxzOyBpKyspIHtcblx0XHRcdHZhciBsaW1pdCA9IGxpbWl0ZXJzW3RoaXMubW9kZWxdW2ldO1xuXHRcdFx0aWYgKGxpbWl0KSB7XG5cdFx0XHRcdHRoaXMuY29sb3JbaV0gPSBsaW1pdCh0aGlzLmNvbG9yW2ldKTtcblx0XHRcdH1cblx0XHR9XG5cdH1cblxuXHR0aGlzLnZhbHBoYSA9IE1hdGgubWF4KDAsIE1hdGgubWluKDEsIHRoaXMudmFscGhhKSk7XG5cblx0aWYgKE9iamVjdC5mcmVlemUpIHtcblx0XHRPYmplY3QuZnJlZXplKHRoaXMpO1xuXHR9XG59XG5cbkNvbG9yLnByb3RvdHlwZSA9IHtcblx0dG9TdHJpbmc6IGZ1bmN0aW9uICgpIHtcblx0XHRyZXR1cm4gdGhpcy5zdHJpbmcoKTtcblx0fSxcblxuXHR0b0pTT046IGZ1bmN0aW9uICgpIHtcblx0XHRyZXR1cm4gdGhpc1t0aGlzLm1vZGVsXSgpO1xuXHR9LFxuXG5cdHN0cmluZzogZnVuY3Rpb24gKHBsYWNlcykge1xuXHRcdHZhciBzZWxmID0gdGhpcy5tb2RlbCBpbiBjb2xvclN0cmluZy50byA/IHRoaXMgOiB0aGlzLnJnYigpO1xuXHRcdHNlbGYgPSBzZWxmLnJvdW5kKHR5cGVvZiBwbGFjZXMgPT09ICdudW1iZXInID8gcGxhY2VzIDogMSk7XG5cdFx0dmFyIGFyZ3MgPSBzZWxmLnZhbHBoYSA9PT0gMSA/IHNlbGYuY29sb3IgOiBzZWxmLmNvbG9yLmNvbmNhdCh0aGlzLnZhbHBoYSk7XG5cdFx0cmV0dXJuIGNvbG9yU3RyaW5nLnRvW3NlbGYubW9kZWxdKGFyZ3MpO1xuXHR9LFxuXG5cdHBlcmNlbnRTdHJpbmc6IGZ1bmN0aW9uIChwbGFjZXMpIHtcblx0XHR2YXIgc2VsZiA9IHRoaXMucmdiKCkucm91bmQodHlwZW9mIHBsYWNlcyA9PT0gJ251bWJlcicgPyBwbGFjZXMgOiAxKTtcblx0XHR2YXIgYXJncyA9IHNlbGYudmFscGhhID09PSAxID8gc2VsZi5jb2xvciA6IHNlbGYuY29sb3IuY29uY2F0KHRoaXMudmFscGhhKTtcblx0XHRyZXR1cm4gY29sb3JTdHJpbmcudG8ucmdiLnBlcmNlbnQoYXJncyk7XG5cdH0sXG5cblx0YXJyYXk6IGZ1bmN0aW9uICgpIHtcblx0XHRyZXR1cm4gdGhpcy52YWxwaGEgPT09IDEgPyB0aGlzLmNvbG9yLnNsaWNlKCkgOiB0aGlzLmNvbG9yLmNvbmNhdCh0aGlzLnZhbHBoYSk7XG5cdH0sXG5cblx0b2JqZWN0OiBmdW5jdGlvbiAoKSB7XG5cdFx0dmFyIHJlc3VsdCA9IHt9O1xuXHRcdHZhciBjaGFubmVscyA9IGNvbnZlcnRbdGhpcy5tb2RlbF0uY2hhbm5lbHM7XG5cdFx0dmFyIGxhYmVscyA9IGNvbnZlcnRbdGhpcy5tb2RlbF0ubGFiZWxzO1xuXG5cdFx0Zm9yICh2YXIgaSA9IDA7IGkgPCBjaGFubmVsczsgaSsrKSB7XG5cdFx0XHRyZXN1bHRbbGFiZWxzW2ldXSA9IHRoaXMuY29sb3JbaV07XG5cdFx0fVxuXG5cdFx0aWYgKHRoaXMudmFscGhhICE9PSAxKSB7XG5cdFx0XHRyZXN1bHQuYWxwaGEgPSB0aGlzLnZhbHBoYTtcblx0XHR9XG5cblx0XHRyZXR1cm4gcmVzdWx0O1xuXHR9LFxuXG5cdHVuaXRBcnJheTogZnVuY3Rpb24gKCkge1xuXHRcdHZhciByZ2IgPSB0aGlzLnJnYigpLmNvbG9yO1xuXHRcdHJnYlswXSAvPSAyNTU7XG5cdFx0cmdiWzFdIC89IDI1NTtcblx0XHRyZ2JbMl0gLz0gMjU1O1xuXG5cdFx0aWYgKHRoaXMudmFscGhhICE9PSAxKSB7XG5cdFx0XHRyZ2IucHVzaCh0aGlzLnZhbHBoYSk7XG5cdFx0fVxuXG5cdFx0cmV0dXJuIHJnYjtcblx0fSxcblxuXHR1bml0T2JqZWN0OiBmdW5jdGlvbiAoKSB7XG5cdFx0dmFyIHJnYiA9IHRoaXMucmdiKCkub2JqZWN0KCk7XG5cdFx0cmdiLnIgLz0gMjU1O1xuXHRcdHJnYi5nIC89IDI1NTtcblx0XHRyZ2IuYiAvPSAyNTU7XG5cblx0XHRpZiAodGhpcy52YWxwaGEgIT09IDEpIHtcblx0XHRcdHJnYi5hbHBoYSA9IHRoaXMudmFscGhhO1xuXHRcdH1cblxuXHRcdHJldHVybiByZ2I7XG5cdH0sXG5cblx0cm91bmQ6IGZ1bmN0aW9uIChwbGFjZXMpIHtcblx0XHRwbGFjZXMgPSBNYXRoLm1heChwbGFjZXMgfHwgMCwgMCk7XG5cdFx0cmV0dXJuIG5ldyBDb2xvcih0aGlzLmNvbG9yLm1hcChyb3VuZFRvUGxhY2UocGxhY2VzKSkuY29uY2F0KHRoaXMudmFscGhhKSwgdGhpcy5tb2RlbCk7XG5cdH0sXG5cblx0YWxwaGE6IGZ1bmN0aW9uICh2YWwpIHtcblx0XHRpZiAoYXJndW1lbnRzLmxlbmd0aCkge1xuXHRcdFx0cmV0dXJuIG5ldyBDb2xvcih0aGlzLmNvbG9yLmNvbmNhdChNYXRoLm1heCgwLCBNYXRoLm1pbigxLCB2YWwpKSksIHRoaXMubW9kZWwpO1xuXHRcdH1cblxuXHRcdHJldHVybiB0aGlzLnZhbHBoYTtcblx0fSxcblxuXHQvLyByZ2Jcblx0cmVkOiBnZXRzZXQoJ3JnYicsIDAsIG1heGZuKDI1NSkpLFxuXHRncmVlbjogZ2V0c2V0KCdyZ2InLCAxLCBtYXhmbigyNTUpKSxcblx0Ymx1ZTogZ2V0c2V0KCdyZ2InLCAyLCBtYXhmbigyNTUpKSxcblxuXHRodWU6IGdldHNldChbJ2hzbCcsICdoc3YnLCAnaHNsJywgJ2h3YicsICdoY2cnXSwgMCwgZnVuY3Rpb24gKHZhbCkgeyByZXR1cm4gKCh2YWwgJSAzNjApICsgMzYwKSAlIDM2MDsgfSksIC8vIGVzbGludC1kaXNhYmxlLWxpbmUgYnJhY2Utc3R5bGVcblxuXHRzYXR1cmF0aW9ubDogZ2V0c2V0KCdoc2wnLCAxLCBtYXhmbigxMDApKSxcblx0bGlnaHRuZXNzOiBnZXRzZXQoJ2hzbCcsIDIsIG1heGZuKDEwMCkpLFxuXG5cdHNhdHVyYXRpb252OiBnZXRzZXQoJ2hzdicsIDEsIG1heGZuKDEwMCkpLFxuXHR2YWx1ZTogZ2V0c2V0KCdoc3YnLCAyLCBtYXhmbigxMDApKSxcblxuXHRjaHJvbWE6IGdldHNldCgnaGNnJywgMSwgbWF4Zm4oMTAwKSksXG5cdGdyYXk6IGdldHNldCgnaGNnJywgMiwgbWF4Zm4oMTAwKSksXG5cblx0d2hpdGU6IGdldHNldCgnaHdiJywgMSwgbWF4Zm4oMTAwKSksXG5cdHdibGFjazogZ2V0c2V0KCdod2InLCAyLCBtYXhmbigxMDApKSxcblxuXHRjeWFuOiBnZXRzZXQoJ2NteWsnLCAwLCBtYXhmbigxMDApKSxcblx0bWFnZW50YTogZ2V0c2V0KCdjbXlrJywgMSwgbWF4Zm4oMTAwKSksXG5cdHllbGxvdzogZ2V0c2V0KCdjbXlrJywgMiwgbWF4Zm4oMTAwKSksXG5cdGJsYWNrOiBnZXRzZXQoJ2NteWsnLCAzLCBtYXhmbigxMDApKSxcblxuXHR4OiBnZXRzZXQoJ3h5eicsIDAsIG1heGZuKDEwMCkpLFxuXHR5OiBnZXRzZXQoJ3h5eicsIDEsIG1heGZuKDEwMCkpLFxuXHR6OiBnZXRzZXQoJ3h5eicsIDIsIG1heGZuKDEwMCkpLFxuXG5cdGw6IGdldHNldCgnbGFiJywgMCwgbWF4Zm4oMTAwKSksXG5cdGE6IGdldHNldCgnbGFiJywgMSksXG5cdGI6IGdldHNldCgnbGFiJywgMiksXG5cblx0a2V5d29yZDogZnVuY3Rpb24gKHZhbCkge1xuXHRcdGlmIChhcmd1bWVudHMubGVuZ3RoKSB7XG5cdFx0XHRyZXR1cm4gbmV3IENvbG9yKHZhbCk7XG5cdFx0fVxuXG5cdFx0cmV0dXJuIGNvbnZlcnRbdGhpcy5tb2RlbF0ua2V5d29yZCh0aGlzLmNvbG9yKTtcblx0fSxcblxuXHRoZXg6IGZ1bmN0aW9uICh2YWwpIHtcblx0XHRpZiAoYXJndW1lbnRzLmxlbmd0aCkge1xuXHRcdFx0cmV0dXJuIG5ldyBDb2xvcih2YWwpO1xuXHRcdH1cblxuXHRcdHJldHVybiBjb2xvclN0cmluZy50by5oZXgodGhpcy5yZ2IoKS5yb3VuZCgpLmNvbG9yKTtcblx0fSxcblxuXHRyZ2JOdW1iZXI6IGZ1bmN0aW9uICgpIHtcblx0XHR2YXIgcmdiID0gdGhpcy5yZ2IoKS5jb2xvcjtcblx0XHRyZXR1cm4gKChyZ2JbMF0gJiAweEZGKSA8PCAxNikgfCAoKHJnYlsxXSAmIDB4RkYpIDw8IDgpIHwgKHJnYlsyXSAmIDB4RkYpO1xuXHR9LFxuXG5cdGx1bWlub3NpdHk6IGZ1bmN0aW9uICgpIHtcblx0XHQvLyBodHRwOi8vd3d3LnczLm9yZy9UUi9XQ0FHMjAvI3JlbGF0aXZlbHVtaW5hbmNlZGVmXG5cdFx0dmFyIHJnYiA9IHRoaXMucmdiKCkuY29sb3I7XG5cblx0XHR2YXIgbHVtID0gW107XG5cdFx0Zm9yICh2YXIgaSA9IDA7IGkgPCByZ2IubGVuZ3RoOyBpKyspIHtcblx0XHRcdHZhciBjaGFuID0gcmdiW2ldIC8gMjU1O1xuXHRcdFx0bHVtW2ldID0gKGNoYW4gPD0gMC4wMzkyOCkgPyBjaGFuIC8gMTIuOTIgOiBNYXRoLnBvdygoKGNoYW4gKyAwLjA1NSkgLyAxLjA1NSksIDIuNCk7XG5cdFx0fVxuXG5cdFx0cmV0dXJuIDAuMjEyNiAqIGx1bVswXSArIDAuNzE1MiAqIGx1bVsxXSArIDAuMDcyMiAqIGx1bVsyXTtcblx0fSxcblxuXHRjb250cmFzdDogZnVuY3Rpb24gKGNvbG9yMikge1xuXHRcdC8vIGh0dHA6Ly93d3cudzMub3JnL1RSL1dDQUcyMC8jY29udHJhc3QtcmF0aW9kZWZcblx0XHR2YXIgbHVtMSA9IHRoaXMubHVtaW5vc2l0eSgpO1xuXHRcdHZhciBsdW0yID0gY29sb3IyLmx1bWlub3NpdHkoKTtcblxuXHRcdGlmIChsdW0xID4gbHVtMikge1xuXHRcdFx0cmV0dXJuIChsdW0xICsgMC4wNSkgLyAobHVtMiArIDAuMDUpO1xuXHRcdH1cblxuXHRcdHJldHVybiAobHVtMiArIDAuMDUpIC8gKGx1bTEgKyAwLjA1KTtcblx0fSxcblxuXHRsZXZlbDogZnVuY3Rpb24gKGNvbG9yMikge1xuXHRcdHZhciBjb250cmFzdFJhdGlvID0gdGhpcy5jb250cmFzdChjb2xvcjIpO1xuXHRcdGlmIChjb250cmFzdFJhdGlvID49IDcuMSkge1xuXHRcdFx0cmV0dXJuICdBQUEnO1xuXHRcdH1cblxuXHRcdHJldHVybiAoY29udHJhc3RSYXRpbyA+PSA0LjUpID8gJ0FBJyA6ICcnO1xuXHR9LFxuXG5cdGlzRGFyazogZnVuY3Rpb24gKCkge1xuXHRcdC8vIFlJUSBlcXVhdGlvbiBmcm9tIGh0dHA6Ly8yNHdheXMub3JnLzIwMTAvY2FsY3VsYXRpbmctY29sb3ItY29udHJhc3Rcblx0XHR2YXIgcmdiID0gdGhpcy5yZ2IoKS5jb2xvcjtcblx0XHR2YXIgeWlxID0gKHJnYlswXSAqIDI5OSArIHJnYlsxXSAqIDU4NyArIHJnYlsyXSAqIDExNCkgLyAxMDAwO1xuXHRcdHJldHVybiB5aXEgPCAxMjg7XG5cdH0sXG5cblx0aXNMaWdodDogZnVuY3Rpb24gKCkge1xuXHRcdHJldHVybiAhdGhpcy5pc0RhcmsoKTtcblx0fSxcblxuXHRuZWdhdGU6IGZ1bmN0aW9uICgpIHtcblx0XHR2YXIgcmdiID0gdGhpcy5yZ2IoKTtcblx0XHRmb3IgKHZhciBpID0gMDsgaSA8IDM7IGkrKykge1xuXHRcdFx0cmdiLmNvbG9yW2ldID0gMjU1IC0gcmdiLmNvbG9yW2ldO1xuXHRcdH1cblx0XHRyZXR1cm4gcmdiO1xuXHR9LFxuXG5cdGxpZ2h0ZW46IGZ1bmN0aW9uIChyYXRpbykge1xuXHRcdHZhciBoc2wgPSB0aGlzLmhzbCgpO1xuXHRcdGhzbC5jb2xvclsyXSArPSBoc2wuY29sb3JbMl0gKiByYXRpbztcblx0XHRyZXR1cm4gaHNsO1xuXHR9LFxuXG5cdGRhcmtlbjogZnVuY3Rpb24gKHJhdGlvKSB7XG5cdFx0dmFyIGhzbCA9IHRoaXMuaHNsKCk7XG5cdFx0aHNsLmNvbG9yWzJdIC09IGhzbC5jb2xvclsyXSAqIHJhdGlvO1xuXHRcdHJldHVybiBoc2w7XG5cdH0sXG5cblx0c2F0dXJhdGU6IGZ1bmN0aW9uIChyYXRpbykge1xuXHRcdHZhciBoc2wgPSB0aGlzLmhzbCgpO1xuXHRcdGhzbC5jb2xvclsxXSArPSBoc2wuY29sb3JbMV0gKiByYXRpbztcblx0XHRyZXR1cm4gaHNsO1xuXHR9LFxuXG5cdGRlc2F0dXJhdGU6IGZ1bmN0aW9uIChyYXRpbykge1xuXHRcdHZhciBoc2wgPSB0aGlzLmhzbCgpO1xuXHRcdGhzbC5jb2xvclsxXSAtPSBoc2wuY29sb3JbMV0gKiByYXRpbztcblx0XHRyZXR1cm4gaHNsO1xuXHR9LFxuXG5cdHdoaXRlbjogZnVuY3Rpb24gKHJhdGlvKSB7XG5cdFx0dmFyIGh3YiA9IHRoaXMuaHdiKCk7XG5cdFx0aHdiLmNvbG9yWzFdICs9IGh3Yi5jb2xvclsxXSAqIHJhdGlvO1xuXHRcdHJldHVybiBod2I7XG5cdH0sXG5cblx0YmxhY2tlbjogZnVuY3Rpb24gKHJhdGlvKSB7XG5cdFx0dmFyIGh3YiA9IHRoaXMuaHdiKCk7XG5cdFx0aHdiLmNvbG9yWzJdICs9IGh3Yi5jb2xvclsyXSAqIHJhdGlvO1xuXHRcdHJldHVybiBod2I7XG5cdH0sXG5cblx0Z3JheXNjYWxlOiBmdW5jdGlvbiAoKSB7XG5cdFx0Ly8gaHR0cDovL2VuLndpa2lwZWRpYS5vcmcvd2lraS9HcmF5c2NhbGUjQ29udmVydGluZ19jb2xvcl90b19ncmF5c2NhbGVcblx0XHR2YXIgcmdiID0gdGhpcy5yZ2IoKS5jb2xvcjtcblx0XHR2YXIgdmFsID0gcmdiWzBdICogMC4zICsgcmdiWzFdICogMC41OSArIHJnYlsyXSAqIDAuMTE7XG5cdFx0cmV0dXJuIENvbG9yLnJnYih2YWwsIHZhbCwgdmFsKTtcblx0fSxcblxuXHRmYWRlOiBmdW5jdGlvbiAocmF0aW8pIHtcblx0XHRyZXR1cm4gdGhpcy5hbHBoYSh0aGlzLnZhbHBoYSAtICh0aGlzLnZhbHBoYSAqIHJhdGlvKSk7XG5cdH0sXG5cblx0b3BhcXVlcjogZnVuY3Rpb24gKHJhdGlvKSB7XG5cdFx0cmV0dXJuIHRoaXMuYWxwaGEodGhpcy52YWxwaGEgKyAodGhpcy52YWxwaGEgKiByYXRpbykpO1xuXHR9LFxuXG5cdHJvdGF0ZTogZnVuY3Rpb24gKGRlZ3JlZXMpIHtcblx0XHR2YXIgaHNsID0gdGhpcy5oc2woKTtcblx0XHR2YXIgaHVlID0gaHNsLmNvbG9yWzBdO1xuXHRcdGh1ZSA9IChodWUgKyBkZWdyZWVzKSAlIDM2MDtcblx0XHRodWUgPSBodWUgPCAwID8gMzYwICsgaHVlIDogaHVlO1xuXHRcdGhzbC5jb2xvclswXSA9IGh1ZTtcblx0XHRyZXR1cm4gaHNsO1xuXHR9LFxuXG5cdG1peDogZnVuY3Rpb24gKG1peGluQ29sb3IsIHdlaWdodCkge1xuXHRcdC8vIHBvcnRlZCBmcm9tIHNhc3MgaW1wbGVtZW50YXRpb24gaW4gQ1xuXHRcdC8vIGh0dHBzOi8vZ2l0aHViLmNvbS9zYXNzL2xpYnNhc3MvYmxvYi8wZTZiNGEyODUwMDkyMzU2YWEzZWNlMDdjNmIyNDlmMDIyMWNhY2VkL2Z1bmN0aW9ucy5jcHAjTDIwOVxuXHRcdGlmICghbWl4aW5Db2xvciB8fCAhbWl4aW5Db2xvci5yZ2IpIHtcblx0XHRcdHRocm93IG5ldyBFcnJvcignQXJndW1lbnQgdG8gXCJtaXhcIiB3YXMgbm90IGEgQ29sb3IgaW5zdGFuY2UsIGJ1dCByYXRoZXIgYW4gaW5zdGFuY2Ugb2YgJyArIHR5cGVvZiBtaXhpbkNvbG9yKTtcblx0XHR9XG5cdFx0dmFyIGNvbG9yMSA9IG1peGluQ29sb3IucmdiKCk7XG5cdFx0dmFyIGNvbG9yMiA9IHRoaXMucmdiKCk7XG5cdFx0dmFyIHAgPSB3ZWlnaHQgPT09IHVuZGVmaW5lZCA/IDAuNSA6IHdlaWdodDtcblxuXHRcdHZhciB3ID0gMiAqIHAgLSAxO1xuXHRcdHZhciBhID0gY29sb3IxLmFscGhhKCkgLSBjb2xvcjIuYWxwaGEoKTtcblxuXHRcdHZhciB3MSA9ICgoKHcgKiBhID09PSAtMSkgPyB3IDogKHcgKyBhKSAvICgxICsgdyAqIGEpKSArIDEpIC8gMi4wO1xuXHRcdHZhciB3MiA9IDEgLSB3MTtcblxuXHRcdHJldHVybiBDb2xvci5yZ2IoXG5cdFx0XHRcdHcxICogY29sb3IxLnJlZCgpICsgdzIgKiBjb2xvcjIucmVkKCksXG5cdFx0XHRcdHcxICogY29sb3IxLmdyZWVuKCkgKyB3MiAqIGNvbG9yMi5ncmVlbigpLFxuXHRcdFx0XHR3MSAqIGNvbG9yMS5ibHVlKCkgKyB3MiAqIGNvbG9yMi5ibHVlKCksXG5cdFx0XHRcdGNvbG9yMS5hbHBoYSgpICogcCArIGNvbG9yMi5hbHBoYSgpICogKDEgLSBwKSk7XG5cdH1cbn07XG5cbi8vIG1vZGVsIGNvbnZlcnNpb24gbWV0aG9kcyBhbmQgc3RhdGljIGNvbnN0cnVjdG9yc1xuT2JqZWN0LmtleXMoY29udmVydCkuZm9yRWFjaChmdW5jdGlvbiAobW9kZWwpIHtcblx0aWYgKHNraXBwZWRNb2RlbHMuaW5kZXhPZihtb2RlbCkgIT09IC0xKSB7XG5cdFx0cmV0dXJuO1xuXHR9XG5cblx0dmFyIGNoYW5uZWxzID0gY29udmVydFttb2RlbF0uY2hhbm5lbHM7XG5cblx0Ly8gY29udmVyc2lvbiBtZXRob2RzXG5cdENvbG9yLnByb3RvdHlwZVttb2RlbF0gPSBmdW5jdGlvbiAoKSB7XG5cdFx0aWYgKHRoaXMubW9kZWwgPT09IG1vZGVsKSB7XG5cdFx0XHRyZXR1cm4gbmV3IENvbG9yKHRoaXMpO1xuXHRcdH1cblxuXHRcdGlmIChhcmd1bWVudHMubGVuZ3RoKSB7XG5cdFx0XHRyZXR1cm4gbmV3IENvbG9yKGFyZ3VtZW50cywgbW9kZWwpO1xuXHRcdH1cblxuXHRcdHZhciBuZXdBbHBoYSA9IHR5cGVvZiBhcmd1bWVudHNbY2hhbm5lbHNdID09PSAnbnVtYmVyJyA/IGNoYW5uZWxzIDogdGhpcy52YWxwaGE7XG5cdFx0cmV0dXJuIG5ldyBDb2xvcihhc3NlcnRBcnJheShjb252ZXJ0W3RoaXMubW9kZWxdW21vZGVsXS5yYXcodGhpcy5jb2xvcikpLmNvbmNhdChuZXdBbHBoYSksIG1vZGVsKTtcblx0fTtcblxuXHQvLyAnc3RhdGljJyBjb25zdHJ1Y3Rpb24gbWV0aG9kc1xuXHRDb2xvclttb2RlbF0gPSBmdW5jdGlvbiAoY29sb3IpIHtcblx0XHRpZiAodHlwZW9mIGNvbG9yID09PSAnbnVtYmVyJykge1xuXHRcdFx0Y29sb3IgPSB6ZXJvQXJyYXkoX3NsaWNlLmNhbGwoYXJndW1lbnRzKSwgY2hhbm5lbHMpO1xuXHRcdH1cblx0XHRyZXR1cm4gbmV3IENvbG9yKGNvbG9yLCBtb2RlbCk7XG5cdH07XG59KTtcblxuZnVuY3Rpb24gcm91bmRUbyhudW0sIHBsYWNlcykge1xuXHRyZXR1cm4gTnVtYmVyKG51bS50b0ZpeGVkKHBsYWNlcykpO1xufVxuXG5mdW5jdGlvbiByb3VuZFRvUGxhY2UocGxhY2VzKSB7XG5cdHJldHVybiBmdW5jdGlvbiAobnVtKSB7XG5cdFx0cmV0dXJuIHJvdW5kVG8obnVtLCBwbGFjZXMpO1xuXHR9O1xufVxuXG5mdW5jdGlvbiBnZXRzZXQobW9kZWwsIGNoYW5uZWwsIG1vZGlmaWVyKSB7XG5cdG1vZGVsID0gQXJyYXkuaXNBcnJheShtb2RlbCkgPyBtb2RlbCA6IFttb2RlbF07XG5cblx0bW9kZWwuZm9yRWFjaChmdW5jdGlvbiAobSkge1xuXHRcdChsaW1pdGVyc1ttXSB8fCAobGltaXRlcnNbbV0gPSBbXSkpW2NoYW5uZWxdID0gbW9kaWZpZXI7XG5cdH0pO1xuXG5cdG1vZGVsID0gbW9kZWxbMF07XG5cblx0cmV0dXJuIGZ1bmN0aW9uICh2YWwpIHtcblx0XHR2YXIgcmVzdWx0O1xuXG5cdFx0aWYgKGFyZ3VtZW50cy5sZW5ndGgpIHtcblx0XHRcdGlmIChtb2RpZmllcikge1xuXHRcdFx0XHR2YWwgPSBtb2RpZmllcih2YWwpO1xuXHRcdFx0fVxuXG5cdFx0XHRyZXN1bHQgPSB0aGlzW21vZGVsXSgpO1xuXHRcdFx0cmVzdWx0LmNvbG9yW2NoYW5uZWxdID0gdmFsO1xuXHRcdFx0cmV0dXJuIHJlc3VsdDtcblx0XHR9XG5cblx0XHRyZXN1bHQgPSB0aGlzW21vZGVsXSgpLmNvbG9yW2NoYW5uZWxdO1xuXHRcdGlmIChtb2RpZmllcikge1xuXHRcdFx0cmVzdWx0ID0gbW9kaWZpZXIocmVzdWx0KTtcblx0XHR9XG5cblx0XHRyZXR1cm4gcmVzdWx0O1xuXHR9O1xufVxuXG5mdW5jdGlvbiBtYXhmbihtYXgpIHtcblx0cmV0dXJuIGZ1bmN0aW9uICh2KSB7XG5cdFx0cmV0dXJuIE1hdGgubWF4KDAsIE1hdGgubWluKG1heCwgdikpO1xuXHR9O1xufVxuXG5mdW5jdGlvbiBhc3NlcnRBcnJheSh2YWwpIHtcblx0cmV0dXJuIEFycmF5LmlzQXJyYXkodmFsKSA/IHZhbCA6IFt2YWxdO1xufVxuXG5mdW5jdGlvbiB6ZXJvQXJyYXkoYXJyLCBsZW5ndGgpIHtcblx0Zm9yICh2YXIgaSA9IDA7IGkgPCBsZW5ndGg7IGkrKykge1xuXHRcdGlmICh0eXBlb2YgYXJyW2ldICE9PSAnbnVtYmVyJykge1xuXHRcdFx0YXJyW2ldID0gMDtcblx0XHR9XG5cdH1cblxuXHRyZXR1cm4gYXJyO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IENvbG9yO1xuXG5cbi8qKiovIH0pLFxuLyogMTcgKi9cbi8qKiovIChmdW5jdGlvbihtb2R1bGUsIGV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pIHtcblxuLyogTUlUIGxpY2Vuc2UgKi9cbnZhciBjb2xvck5hbWVzID0gX193ZWJwYWNrX3JlcXVpcmVfXyg1KTtcbnZhciBzd2l6emxlID0gX193ZWJwYWNrX3JlcXVpcmVfXygxOCk7XG5cbnZhciByZXZlcnNlTmFtZXMgPSB7fTtcblxuLy8gY3JlYXRlIGEgbGlzdCBvZiByZXZlcnNlIGNvbG9yIG5hbWVzXG5mb3IgKHZhciBuYW1lIGluIGNvbG9yTmFtZXMpIHtcblx0aWYgKGNvbG9yTmFtZXMuaGFzT3duUHJvcGVydHkobmFtZSkpIHtcblx0XHRyZXZlcnNlTmFtZXNbY29sb3JOYW1lc1tuYW1lXV0gPSBuYW1lO1xuXHR9XG59XG5cbnZhciBjcyA9IG1vZHVsZS5leHBvcnRzID0ge1xuXHR0bzoge30sXG5cdGdldDoge31cbn07XG5cbmNzLmdldCA9IGZ1bmN0aW9uIChzdHJpbmcpIHtcblx0dmFyIHByZWZpeCA9IHN0cmluZy5zdWJzdHJpbmcoMCwgMykudG9Mb3dlckNhc2UoKTtcblx0dmFyIHZhbDtcblx0dmFyIG1vZGVsO1xuXHRzd2l0Y2ggKHByZWZpeCkge1xuXHRcdGNhc2UgJ2hzbCc6XG5cdFx0XHR2YWwgPSBjcy5nZXQuaHNsKHN0cmluZyk7XG5cdFx0XHRtb2RlbCA9ICdoc2wnO1xuXHRcdFx0YnJlYWs7XG5cdFx0Y2FzZSAnaHdiJzpcblx0XHRcdHZhbCA9IGNzLmdldC5od2Ioc3RyaW5nKTtcblx0XHRcdG1vZGVsID0gJ2h3Yic7XG5cdFx0XHRicmVhaztcblx0XHRkZWZhdWx0OlxuXHRcdFx0dmFsID0gY3MuZ2V0LnJnYihzdHJpbmcpO1xuXHRcdFx0bW9kZWwgPSAncmdiJztcblx0XHRcdGJyZWFrO1xuXHR9XG5cblx0aWYgKCF2YWwpIHtcblx0XHRyZXR1cm4gbnVsbDtcblx0fVxuXG5cdHJldHVybiB7bW9kZWw6IG1vZGVsLCB2YWx1ZTogdmFsfTtcbn07XG5cbmNzLmdldC5yZ2IgPSBmdW5jdGlvbiAoc3RyaW5nKSB7XG5cdGlmICghc3RyaW5nKSB7XG5cdFx0cmV0dXJuIG51bGw7XG5cdH1cblxuXHR2YXIgYWJiciA9IC9eIyhbYS1mMC05XXszLDR9KSQvaTtcblx0dmFyIGhleCA9IC9eIyhbYS1mMC05XXs2fSkoW2EtZjAtOV17Mn0pPyQvaTtcblx0dmFyIHJnYmEgPSAvXnJnYmE/XFwoXFxzKihbKy1dP1xcZCspXFxzKixcXHMqKFsrLV0/XFxkKylcXHMqLFxccyooWystXT9cXGQrKVxccyooPzosXFxzKihbKy1dP1tcXGRcXC5dKylcXHMqKT9cXCkkLztcblx0dmFyIHBlciA9IC9ecmdiYT9cXChcXHMqKFsrLV0/W1xcZFxcLl0rKVxcJVxccyosXFxzKihbKy1dP1tcXGRcXC5dKylcXCVcXHMqLFxccyooWystXT9bXFxkXFwuXSspXFwlXFxzKig/OixcXHMqKFsrLV0/W1xcZFxcLl0rKVxccyopP1xcKSQvO1xuXHR2YXIga2V5d29yZCA9IC8oXFxEKykvO1xuXG5cdHZhciByZ2IgPSBbMCwgMCwgMCwgMV07XG5cdHZhciBtYXRjaDtcblx0dmFyIGk7XG5cdHZhciBoZXhBbHBoYTtcblxuXHRpZiAobWF0Y2ggPSBzdHJpbmcubWF0Y2goaGV4KSkge1xuXHRcdGhleEFscGhhID0gbWF0Y2hbMl07XG5cdFx0bWF0Y2ggPSBtYXRjaFsxXTtcblxuXHRcdGZvciAoaSA9IDA7IGkgPCAzOyBpKyspIHtcblx0XHRcdC8vIGh0dHBzOi8vanNwZXJmLmNvbS9zbGljZS12cy1zdWJzdHItdnMtc3Vic3RyaW5nLW1ldGhvZHMtbG9uZy1zdHJpbmcvMTlcblx0XHRcdHZhciBpMiA9IGkgKiAyO1xuXHRcdFx0cmdiW2ldID0gcGFyc2VJbnQobWF0Y2guc2xpY2UoaTIsIGkyICsgMiksIDE2KTtcblx0XHR9XG5cblx0XHRpZiAoaGV4QWxwaGEpIHtcblx0XHRcdHJnYlszXSA9IE1hdGgucm91bmQoKHBhcnNlSW50KGhleEFscGhhLCAxNikgLyAyNTUpICogMTAwKSAvIDEwMDtcblx0XHR9XG5cdH0gZWxzZSBpZiAobWF0Y2ggPSBzdHJpbmcubWF0Y2goYWJicikpIHtcblx0XHRtYXRjaCA9IG1hdGNoWzFdO1xuXHRcdGhleEFscGhhID0gbWF0Y2hbM107XG5cblx0XHRmb3IgKGkgPSAwOyBpIDwgMzsgaSsrKSB7XG5cdFx0XHRyZ2JbaV0gPSBwYXJzZUludChtYXRjaFtpXSArIG1hdGNoW2ldLCAxNik7XG5cdFx0fVxuXG5cdFx0aWYgKGhleEFscGhhKSB7XG5cdFx0XHRyZ2JbM10gPSBNYXRoLnJvdW5kKChwYXJzZUludChoZXhBbHBoYSArIGhleEFscGhhLCAxNikgLyAyNTUpICogMTAwKSAvIDEwMDtcblx0XHR9XG5cdH0gZWxzZSBpZiAobWF0Y2ggPSBzdHJpbmcubWF0Y2gocmdiYSkpIHtcblx0XHRmb3IgKGkgPSAwOyBpIDwgMzsgaSsrKSB7XG5cdFx0XHRyZ2JbaV0gPSBwYXJzZUludChtYXRjaFtpICsgMV0sIDApO1xuXHRcdH1cblxuXHRcdGlmIChtYXRjaFs0XSkge1xuXHRcdFx0cmdiWzNdID0gcGFyc2VGbG9hdChtYXRjaFs0XSk7XG5cdFx0fVxuXHR9IGVsc2UgaWYgKG1hdGNoID0gc3RyaW5nLm1hdGNoKHBlcikpIHtcblx0XHRmb3IgKGkgPSAwOyBpIDwgMzsgaSsrKSB7XG5cdFx0XHRyZ2JbaV0gPSBNYXRoLnJvdW5kKHBhcnNlRmxvYXQobWF0Y2hbaSArIDFdKSAqIDIuNTUpO1xuXHRcdH1cblxuXHRcdGlmIChtYXRjaFs0XSkge1xuXHRcdFx0cmdiWzNdID0gcGFyc2VGbG9hdChtYXRjaFs0XSk7XG5cdFx0fVxuXHR9IGVsc2UgaWYgKG1hdGNoID0gc3RyaW5nLm1hdGNoKGtleXdvcmQpKSB7XG5cdFx0aWYgKG1hdGNoWzFdID09PSAndHJhbnNwYXJlbnQnKSB7XG5cdFx0XHRyZXR1cm4gWzAsIDAsIDAsIDBdO1xuXHRcdH1cblxuXHRcdHJnYiA9IGNvbG9yTmFtZXNbbWF0Y2hbMV1dO1xuXG5cdFx0aWYgKCFyZ2IpIHtcblx0XHRcdHJldHVybiBudWxsO1xuXHRcdH1cblxuXHRcdHJnYlszXSA9IDE7XG5cblx0XHRyZXR1cm4gcmdiO1xuXHR9IGVsc2Uge1xuXHRcdHJldHVybiBudWxsO1xuXHR9XG5cblx0Zm9yIChpID0gMDsgaSA8IDM7IGkrKykge1xuXHRcdHJnYltpXSA9IGNsYW1wKHJnYltpXSwgMCwgMjU1KTtcblx0fVxuXHRyZ2JbM10gPSBjbGFtcChyZ2JbM10sIDAsIDEpO1xuXG5cdHJldHVybiByZ2I7XG59O1xuXG5jcy5nZXQuaHNsID0gZnVuY3Rpb24gKHN0cmluZykge1xuXHRpZiAoIXN0cmluZykge1xuXHRcdHJldHVybiBudWxsO1xuXHR9XG5cblx0dmFyIGhzbCA9IC9eaHNsYT9cXChcXHMqKFsrLV0/KD86XFxkKlxcLik/XFxkKykoPzpkZWcpP1xccyosXFxzKihbKy1dP1tcXGRcXC5dKyklXFxzKixcXHMqKFsrLV0/W1xcZFxcLl0rKSVcXHMqKD86LFxccyooWystXT9bXFxkXFwuXSspXFxzKik/XFwpJC87XG5cdHZhciBtYXRjaCA9IHN0cmluZy5tYXRjaChoc2wpO1xuXG5cdGlmIChtYXRjaCkge1xuXHRcdHZhciBhbHBoYSA9IHBhcnNlRmxvYXQobWF0Y2hbNF0pO1xuXHRcdHZhciBoID0gKHBhcnNlRmxvYXQobWF0Y2hbMV0pICsgMzYwKSAlIDM2MDtcblx0XHR2YXIgcyA9IGNsYW1wKHBhcnNlRmxvYXQobWF0Y2hbMl0pLCAwLCAxMDApO1xuXHRcdHZhciBsID0gY2xhbXAocGFyc2VGbG9hdChtYXRjaFszXSksIDAsIDEwMCk7XG5cdFx0dmFyIGEgPSBjbGFtcChpc05hTihhbHBoYSkgPyAxIDogYWxwaGEsIDAsIDEpO1xuXG5cdFx0cmV0dXJuIFtoLCBzLCBsLCBhXTtcblx0fVxuXG5cdHJldHVybiBudWxsO1xufTtcblxuY3MuZ2V0Lmh3YiA9IGZ1bmN0aW9uIChzdHJpbmcpIHtcblx0aWYgKCFzdHJpbmcpIHtcblx0XHRyZXR1cm4gbnVsbDtcblx0fVxuXG5cdHZhciBod2IgPSAvXmh3YlxcKFxccyooWystXT9cXGQqW1xcLl0/XFxkKykoPzpkZWcpP1xccyosXFxzKihbKy1dP1tcXGRcXC5dKyklXFxzKixcXHMqKFsrLV0/W1xcZFxcLl0rKSVcXHMqKD86LFxccyooWystXT9bXFxkXFwuXSspXFxzKik/XFwpJC87XG5cdHZhciBtYXRjaCA9IHN0cmluZy5tYXRjaChod2IpO1xuXG5cdGlmIChtYXRjaCkge1xuXHRcdHZhciBhbHBoYSA9IHBhcnNlRmxvYXQobWF0Y2hbNF0pO1xuXHRcdHZhciBoID0gKChwYXJzZUZsb2F0KG1hdGNoWzFdKSAlIDM2MCkgKyAzNjApICUgMzYwO1xuXHRcdHZhciB3ID0gY2xhbXAocGFyc2VGbG9hdChtYXRjaFsyXSksIDAsIDEwMCk7XG5cdFx0dmFyIGIgPSBjbGFtcChwYXJzZUZsb2F0KG1hdGNoWzNdKSwgMCwgMTAwKTtcblx0XHR2YXIgYSA9IGNsYW1wKGlzTmFOKGFscGhhKSA/IDEgOiBhbHBoYSwgMCwgMSk7XG5cdFx0cmV0dXJuIFtoLCB3LCBiLCBhXTtcblx0fVxuXG5cdHJldHVybiBudWxsO1xufTtcblxuY3MudG8uaGV4ID0gZnVuY3Rpb24gKCkge1xuXHR2YXIgcmdiYSA9IHN3aXp6bGUoYXJndW1lbnRzKTtcblxuXHRyZXR1cm4gKFxuXHRcdCcjJyArXG5cdFx0aGV4RG91YmxlKHJnYmFbMF0pICtcblx0XHRoZXhEb3VibGUocmdiYVsxXSkgK1xuXHRcdGhleERvdWJsZShyZ2JhWzJdKSArXG5cdFx0KHJnYmFbM10gPCAxXG5cdFx0XHQ/IChoZXhEb3VibGUoTWF0aC5yb3VuZChyZ2JhWzNdICogMjU1KSkpXG5cdFx0XHQ6ICcnKVxuXHQpO1xufTtcblxuY3MudG8ucmdiID0gZnVuY3Rpb24gKCkge1xuXHR2YXIgcmdiYSA9IHN3aXp6bGUoYXJndW1lbnRzKTtcblxuXHRyZXR1cm4gcmdiYS5sZW5ndGggPCA0IHx8IHJnYmFbM10gPT09IDFcblx0XHQ/ICdyZ2IoJyArIE1hdGgucm91bmQocmdiYVswXSkgKyAnLCAnICsgTWF0aC5yb3VuZChyZ2JhWzFdKSArICcsICcgKyBNYXRoLnJvdW5kKHJnYmFbMl0pICsgJyknXG5cdFx0OiAncmdiYSgnICsgTWF0aC5yb3VuZChyZ2JhWzBdKSArICcsICcgKyBNYXRoLnJvdW5kKHJnYmFbMV0pICsgJywgJyArIE1hdGgucm91bmQocmdiYVsyXSkgKyAnLCAnICsgcmdiYVszXSArICcpJztcbn07XG5cbmNzLnRvLnJnYi5wZXJjZW50ID0gZnVuY3Rpb24gKCkge1xuXHR2YXIgcmdiYSA9IHN3aXp6bGUoYXJndW1lbnRzKTtcblxuXHR2YXIgciA9IE1hdGgucm91bmQocmdiYVswXSAvIDI1NSAqIDEwMCk7XG5cdHZhciBnID0gTWF0aC5yb3VuZChyZ2JhWzFdIC8gMjU1ICogMTAwKTtcblx0dmFyIGIgPSBNYXRoLnJvdW5kKHJnYmFbMl0gLyAyNTUgKiAxMDApO1xuXG5cdHJldHVybiByZ2JhLmxlbmd0aCA8IDQgfHwgcmdiYVszXSA9PT0gMVxuXHRcdD8gJ3JnYignICsgciArICclLCAnICsgZyArICclLCAnICsgYiArICclKSdcblx0XHQ6ICdyZ2JhKCcgKyByICsgJyUsICcgKyBnICsgJyUsICcgKyBiICsgJyUsICcgKyByZ2JhWzNdICsgJyknO1xufTtcblxuY3MudG8uaHNsID0gZnVuY3Rpb24gKCkge1xuXHR2YXIgaHNsYSA9IHN3aXp6bGUoYXJndW1lbnRzKTtcblx0cmV0dXJuIGhzbGEubGVuZ3RoIDwgNCB8fCBoc2xhWzNdID09PSAxXG5cdFx0PyAnaHNsKCcgKyBoc2xhWzBdICsgJywgJyArIGhzbGFbMV0gKyAnJSwgJyArIGhzbGFbMl0gKyAnJSknXG5cdFx0OiAnaHNsYSgnICsgaHNsYVswXSArICcsICcgKyBoc2xhWzFdICsgJyUsICcgKyBoc2xhWzJdICsgJyUsICcgKyBoc2xhWzNdICsgJyknO1xufTtcblxuLy8gaHdiIGlzIGEgYml0IGRpZmZlcmVudCB0aGFuIHJnYihhKSAmIGhzbChhKSBzaW5jZSB0aGVyZSBpcyBubyBhbHBoYSBzcGVjaWZpYyBzeW50YXhcbi8vIChod2IgaGF2ZSBhbHBoYSBvcHRpb25hbCAmIDEgaXMgZGVmYXVsdCB2YWx1ZSlcbmNzLnRvLmh3YiA9IGZ1bmN0aW9uICgpIHtcblx0dmFyIGh3YmEgPSBzd2l6emxlKGFyZ3VtZW50cyk7XG5cblx0dmFyIGEgPSAnJztcblx0aWYgKGh3YmEubGVuZ3RoID49IDQgJiYgaHdiYVszXSAhPT0gMSkge1xuXHRcdGEgPSAnLCAnICsgaHdiYVszXTtcblx0fVxuXG5cdHJldHVybiAnaHdiKCcgKyBod2JhWzBdICsgJywgJyArIGh3YmFbMV0gKyAnJSwgJyArIGh3YmFbMl0gKyAnJScgKyBhICsgJyknO1xufTtcblxuY3MudG8ua2V5d29yZCA9IGZ1bmN0aW9uIChyZ2IpIHtcblx0cmV0dXJuIHJldmVyc2VOYW1lc1tyZ2Iuc2xpY2UoMCwgMyldO1xufTtcblxuLy8gaGVscGVyc1xuZnVuY3Rpb24gY2xhbXAobnVtLCBtaW4sIG1heCkge1xuXHRyZXR1cm4gTWF0aC5taW4oTWF0aC5tYXgobWluLCBudW0pLCBtYXgpO1xufVxuXG5mdW5jdGlvbiBoZXhEb3VibGUobnVtKSB7XG5cdHZhciBzdHIgPSBudW0udG9TdHJpbmcoMTYpLnRvVXBwZXJDYXNlKCk7XG5cdHJldHVybiAoc3RyLmxlbmd0aCA8IDIpID8gJzAnICsgc3RyIDogc3RyO1xufVxuXG5cbi8qKiovIH0pLFxuLyogMTggKi9cbi8qKiovIChmdW5jdGlvbihtb2R1bGUsIGV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pIHtcblxuXCJ1c2Ugc3RyaWN0XCI7XG5cblxudmFyIGlzQXJyYXlpc2ggPSBfX3dlYnBhY2tfcmVxdWlyZV9fKDE5KTtcblxudmFyIGNvbmNhdCA9IEFycmF5LnByb3RvdHlwZS5jb25jYXQ7XG52YXIgc2xpY2UgPSBBcnJheS5wcm90b3R5cGUuc2xpY2U7XG5cbnZhciBzd2l6emxlID0gbW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBzd2l6emxlKGFyZ3MpIHtcblx0dmFyIHJlc3VsdHMgPSBbXTtcblxuXHRmb3IgKHZhciBpID0gMCwgbGVuID0gYXJncy5sZW5ndGg7IGkgPCBsZW47IGkrKykge1xuXHRcdHZhciBhcmcgPSBhcmdzW2ldO1xuXG5cdFx0aWYgKGlzQXJyYXlpc2goYXJnKSkge1xuXHRcdFx0Ly8gaHR0cDovL2pzcGVyZi5jb20vamF2YXNjcmlwdC1hcnJheS1jb25jYXQtdnMtcHVzaC85OFxuXHRcdFx0cmVzdWx0cyA9IGNvbmNhdC5jYWxsKHJlc3VsdHMsIHNsaWNlLmNhbGwoYXJnKSk7XG5cdFx0fSBlbHNlIHtcblx0XHRcdHJlc3VsdHMucHVzaChhcmcpO1xuXHRcdH1cblx0fVxuXG5cdHJldHVybiByZXN1bHRzO1xufTtcblxuc3dpenpsZS53cmFwID0gZnVuY3Rpb24gKGZuKSB7XG5cdHJldHVybiBmdW5jdGlvbiAoKSB7XG5cdFx0cmV0dXJuIGZuKHN3aXp6bGUoYXJndW1lbnRzKSk7XG5cdH07XG59O1xuXG5cbi8qKiovIH0pLFxuLyogMTkgKi9cbi8qKiovIChmdW5jdGlvbihtb2R1bGUsIGV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pIHtcblxuXCJ1c2Ugc3RyaWN0XCI7XG5cblxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBpc0FycmF5aXNoKG9iaikge1xuXHRpZiAoIW9iaikge1xuXHRcdHJldHVybiBmYWxzZTtcblx0fVxuXG5cdHJldHVybiBvYmogaW5zdGFuY2VvZiBBcnJheSB8fCBBcnJheS5pc0FycmF5KG9iaikgfHxcblx0XHQob2JqLmxlbmd0aCA+PSAwICYmIG9iai5zcGxpY2UgaW5zdGFuY2VvZiBGdW5jdGlvbik7XG59O1xuXG5cbi8qKiovIH0pLFxuLyogMjAgKi9cbi8qKiovIChmdW5jdGlvbihtb2R1bGUsIGV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pIHtcblxudmFyIGNvbnZlcnNpb25zID0gX193ZWJwYWNrX3JlcXVpcmVfXyg2KTtcbnZhciByb3V0ZSA9IF9fd2VicGFja19yZXF1aXJlX18oMjEpO1xuXG52YXIgY29udmVydCA9IHt9O1xuXG52YXIgbW9kZWxzID0gT2JqZWN0LmtleXMoY29udmVyc2lvbnMpO1xuXG5mdW5jdGlvbiB3cmFwUmF3KGZuKSB7XG5cdHZhciB3cmFwcGVkRm4gPSBmdW5jdGlvbiAoYXJncykge1xuXHRcdGlmIChhcmdzID09PSB1bmRlZmluZWQgfHwgYXJncyA9PT0gbnVsbCkge1xuXHRcdFx0cmV0dXJuIGFyZ3M7XG5cdFx0fVxuXG5cdFx0aWYgKGFyZ3VtZW50cy5sZW5ndGggPiAxKSB7XG5cdFx0XHRhcmdzID0gQXJyYXkucHJvdG90eXBlLnNsaWNlLmNhbGwoYXJndW1lbnRzKTtcblx0XHR9XG5cblx0XHRyZXR1cm4gZm4oYXJncyk7XG5cdH07XG5cblx0Ly8gcHJlc2VydmUgLmNvbnZlcnNpb24gcHJvcGVydHkgaWYgdGhlcmUgaXMgb25lXG5cdGlmICgnY29udmVyc2lvbicgaW4gZm4pIHtcblx0XHR3cmFwcGVkRm4uY29udmVyc2lvbiA9IGZuLmNvbnZlcnNpb247XG5cdH1cblxuXHRyZXR1cm4gd3JhcHBlZEZuO1xufVxuXG5mdW5jdGlvbiB3cmFwUm91bmRlZChmbikge1xuXHR2YXIgd3JhcHBlZEZuID0gZnVuY3Rpb24gKGFyZ3MpIHtcblx0XHRpZiAoYXJncyA9PT0gdW5kZWZpbmVkIHx8IGFyZ3MgPT09IG51bGwpIHtcblx0XHRcdHJldHVybiBhcmdzO1xuXHRcdH1cblxuXHRcdGlmIChhcmd1bWVudHMubGVuZ3RoID4gMSkge1xuXHRcdFx0YXJncyA9IEFycmF5LnByb3RvdHlwZS5zbGljZS5jYWxsKGFyZ3VtZW50cyk7XG5cdFx0fVxuXG5cdFx0dmFyIHJlc3VsdCA9IGZuKGFyZ3MpO1xuXG5cdFx0Ly8gd2UncmUgYXNzdW1pbmcgdGhlIHJlc3VsdCBpcyBhbiBhcnJheSBoZXJlLlxuXHRcdC8vIHNlZSBub3RpY2UgaW4gY29udmVyc2lvbnMuanM7IGRvbid0IHVzZSBib3ggdHlwZXNcblx0XHQvLyBpbiBjb252ZXJzaW9uIGZ1bmN0aW9ucy5cblx0XHRpZiAodHlwZW9mIHJlc3VsdCA9PT0gJ29iamVjdCcpIHtcblx0XHRcdGZvciAodmFyIGxlbiA9IHJlc3VsdC5sZW5ndGgsIGkgPSAwOyBpIDwgbGVuOyBpKyspIHtcblx0XHRcdFx0cmVzdWx0W2ldID0gTWF0aC5yb3VuZChyZXN1bHRbaV0pO1xuXHRcdFx0fVxuXHRcdH1cblxuXHRcdHJldHVybiByZXN1bHQ7XG5cdH07XG5cblx0Ly8gcHJlc2VydmUgLmNvbnZlcnNpb24gcHJvcGVydHkgaWYgdGhlcmUgaXMgb25lXG5cdGlmICgnY29udmVyc2lvbicgaW4gZm4pIHtcblx0XHR3cmFwcGVkRm4uY29udmVyc2lvbiA9IGZuLmNvbnZlcnNpb247XG5cdH1cblxuXHRyZXR1cm4gd3JhcHBlZEZuO1xufVxuXG5tb2RlbHMuZm9yRWFjaChmdW5jdGlvbiAoZnJvbU1vZGVsKSB7XG5cdGNvbnZlcnRbZnJvbU1vZGVsXSA9IHt9O1xuXG5cdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShjb252ZXJ0W2Zyb21Nb2RlbF0sICdjaGFubmVscycsIHt2YWx1ZTogY29udmVyc2lvbnNbZnJvbU1vZGVsXS5jaGFubmVsc30pO1xuXHRPYmplY3QuZGVmaW5lUHJvcGVydHkoY29udmVydFtmcm9tTW9kZWxdLCAnbGFiZWxzJywge3ZhbHVlOiBjb252ZXJzaW9uc1tmcm9tTW9kZWxdLmxhYmVsc30pO1xuXG5cdHZhciByb3V0ZXMgPSByb3V0ZShmcm9tTW9kZWwpO1xuXHR2YXIgcm91dGVNb2RlbHMgPSBPYmplY3Qua2V5cyhyb3V0ZXMpO1xuXG5cdHJvdXRlTW9kZWxzLmZvckVhY2goZnVuY3Rpb24gKHRvTW9kZWwpIHtcblx0XHR2YXIgZm4gPSByb3V0ZXNbdG9Nb2RlbF07XG5cblx0XHRjb252ZXJ0W2Zyb21Nb2RlbF1bdG9Nb2RlbF0gPSB3cmFwUm91bmRlZChmbik7XG5cdFx0Y29udmVydFtmcm9tTW9kZWxdW3RvTW9kZWxdLnJhdyA9IHdyYXBSYXcoZm4pO1xuXHR9KTtcbn0pO1xuXG5tb2R1bGUuZXhwb3J0cyA9IGNvbnZlcnQ7XG5cblxuLyoqKi8gfSksXG4vKiAyMSAqL1xuLyoqKi8gKGZ1bmN0aW9uKG1vZHVsZSwgZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXykge1xuXG52YXIgY29udmVyc2lvbnMgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKDYpO1xuXG4vKlxuXHR0aGlzIGZ1bmN0aW9uIHJvdXRlcyBhIG1vZGVsIHRvIGFsbCBvdGhlciBtb2RlbHMuXG5cblx0YWxsIGZ1bmN0aW9ucyB0aGF0IGFyZSByb3V0ZWQgaGF2ZSBhIHByb3BlcnR5IGAuY29udmVyc2lvbmAgYXR0YWNoZWRcblx0dG8gdGhlIHJldHVybmVkIHN5bnRoZXRpYyBmdW5jdGlvbi4gVGhpcyBwcm9wZXJ0eSBpcyBhbiBhcnJheVxuXHRvZiBzdHJpbmdzLCBlYWNoIHdpdGggdGhlIHN0ZXBzIGluIGJldHdlZW4gdGhlICdmcm9tJyBhbmQgJ3RvJ1xuXHRjb2xvciBtb2RlbHMgKGluY2x1c2l2ZSkuXG5cblx0Y29udmVyc2lvbnMgdGhhdCBhcmUgbm90IHBvc3NpYmxlIHNpbXBseSBhcmUgbm90IGluY2x1ZGVkLlxuKi9cblxuZnVuY3Rpb24gYnVpbGRHcmFwaCgpIHtcblx0dmFyIGdyYXBoID0ge307XG5cdC8vIGh0dHBzOi8vanNwZXJmLmNvbS9vYmplY3Qta2V5cy12cy1mb3ItaW4td2l0aC1jbG9zdXJlLzNcblx0dmFyIG1vZGVscyA9IE9iamVjdC5rZXlzKGNvbnZlcnNpb25zKTtcblxuXHRmb3IgKHZhciBsZW4gPSBtb2RlbHMubGVuZ3RoLCBpID0gMDsgaSA8IGxlbjsgaSsrKSB7XG5cdFx0Z3JhcGhbbW9kZWxzW2ldXSA9IHtcblx0XHRcdC8vIGh0dHA6Ly9qc3BlcmYuY29tLzEtdnMtaW5maW5pdHlcblx0XHRcdC8vIG1pY3JvLW9wdCwgYnV0IHRoaXMgaXMgc2ltcGxlLlxuXHRcdFx0ZGlzdGFuY2U6IC0xLFxuXHRcdFx0cGFyZW50OiBudWxsXG5cdFx0fTtcblx0fVxuXG5cdHJldHVybiBncmFwaDtcbn1cblxuLy8gaHR0cHM6Ly9lbi53aWtpcGVkaWEub3JnL3dpa2kvQnJlYWR0aC1maXJzdF9zZWFyY2hcbmZ1bmN0aW9uIGRlcml2ZUJGUyhmcm9tTW9kZWwpIHtcblx0dmFyIGdyYXBoID0gYnVpbGRHcmFwaCgpO1xuXHR2YXIgcXVldWUgPSBbZnJvbU1vZGVsXTsgLy8gdW5zaGlmdCAtPiBxdWV1ZSAtPiBwb3BcblxuXHRncmFwaFtmcm9tTW9kZWxdLmRpc3RhbmNlID0gMDtcblxuXHR3aGlsZSAocXVldWUubGVuZ3RoKSB7XG5cdFx0dmFyIGN1cnJlbnQgPSBxdWV1ZS5wb3AoKTtcblx0XHR2YXIgYWRqYWNlbnRzID0gT2JqZWN0LmtleXMoY29udmVyc2lvbnNbY3VycmVudF0pO1xuXG5cdFx0Zm9yICh2YXIgbGVuID0gYWRqYWNlbnRzLmxlbmd0aCwgaSA9IDA7IGkgPCBsZW47IGkrKykge1xuXHRcdFx0dmFyIGFkamFjZW50ID0gYWRqYWNlbnRzW2ldO1xuXHRcdFx0dmFyIG5vZGUgPSBncmFwaFthZGphY2VudF07XG5cblx0XHRcdGlmIChub2RlLmRpc3RhbmNlID09PSAtMSkge1xuXHRcdFx0XHRub2RlLmRpc3RhbmNlID0gZ3JhcGhbY3VycmVudF0uZGlzdGFuY2UgKyAxO1xuXHRcdFx0XHRub2RlLnBhcmVudCA9IGN1cnJlbnQ7XG5cdFx0XHRcdHF1ZXVlLnVuc2hpZnQoYWRqYWNlbnQpO1xuXHRcdFx0fVxuXHRcdH1cblx0fVxuXG5cdHJldHVybiBncmFwaDtcbn1cblxuZnVuY3Rpb24gbGluayhmcm9tLCB0bykge1xuXHRyZXR1cm4gZnVuY3Rpb24gKGFyZ3MpIHtcblx0XHRyZXR1cm4gdG8oZnJvbShhcmdzKSk7XG5cdH07XG59XG5cbmZ1bmN0aW9uIHdyYXBDb252ZXJzaW9uKHRvTW9kZWwsIGdyYXBoKSB7XG5cdHZhciBwYXRoID0gW2dyYXBoW3RvTW9kZWxdLnBhcmVudCwgdG9Nb2RlbF07XG5cdHZhciBmbiA9IGNvbnZlcnNpb25zW2dyYXBoW3RvTW9kZWxdLnBhcmVudF1bdG9Nb2RlbF07XG5cblx0dmFyIGN1ciA9IGdyYXBoW3RvTW9kZWxdLnBhcmVudDtcblx0d2hpbGUgKGdyYXBoW2N1cl0ucGFyZW50KSB7XG5cdFx0cGF0aC51bnNoaWZ0KGdyYXBoW2N1cl0ucGFyZW50KTtcblx0XHRmbiA9IGxpbmsoY29udmVyc2lvbnNbZ3JhcGhbY3VyXS5wYXJlbnRdW2N1cl0sIGZuKTtcblx0XHRjdXIgPSBncmFwaFtjdXJdLnBhcmVudDtcblx0fVxuXG5cdGZuLmNvbnZlcnNpb24gPSBwYXRoO1xuXHRyZXR1cm4gZm47XG59XG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKGZyb21Nb2RlbCkge1xuXHR2YXIgZ3JhcGggPSBkZXJpdmVCRlMoZnJvbU1vZGVsKTtcblx0dmFyIGNvbnZlcnNpb24gPSB7fTtcblxuXHR2YXIgbW9kZWxzID0gT2JqZWN0LmtleXMoZ3JhcGgpO1xuXHRmb3IgKHZhciBsZW4gPSBtb2RlbHMubGVuZ3RoLCBpID0gMDsgaSA8IGxlbjsgaSsrKSB7XG5cdFx0dmFyIHRvTW9kZWwgPSBtb2RlbHNbaV07XG5cdFx0dmFyIG5vZGUgPSBncmFwaFt0b01vZGVsXTtcblxuXHRcdGlmIChub2RlLnBhcmVudCA9PT0gbnVsbCkge1xuXHRcdFx0Ly8gbm8gcG9zc2libGUgY29udmVyc2lvbiwgb3IgdGhpcyBub2RlIGlzIHRoZSBzb3VyY2UgbW9kZWwuXG5cdFx0XHRjb250aW51ZTtcblx0XHR9XG5cblx0XHRjb252ZXJzaW9uW3RvTW9kZWxdID0gd3JhcENvbnZlcnNpb24odG9Nb2RlbCwgZ3JhcGgpO1xuXHR9XG5cblx0cmV0dXJuIGNvbnZlcnNpb247XG59O1xuXG5cblxuLyoqKi8gfSksXG4vKiAyMiAqL1xuLyoqKi8gKGZ1bmN0aW9uKG1vZHVsZSwgZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXykge1xuXG5cInVzZSBzdHJpY3RcIjtcblxuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX2NyZWF0ZUNsYXNzID0gZnVuY3Rpb24gKCkgeyBmdW5jdGlvbiBkZWZpbmVQcm9wZXJ0aWVzKHRhcmdldCwgcHJvcHMpIHsgZm9yICh2YXIgaSA9IDA7IGkgPCBwcm9wcy5sZW5ndGg7IGkrKykgeyB2YXIgZGVzY3JpcHRvciA9IHByb3BzW2ldOyBkZXNjcmlwdG9yLmVudW1lcmFibGUgPSBkZXNjcmlwdG9yLmVudW1lcmFibGUgfHwgZmFsc2U7IGRlc2NyaXB0b3IuY29uZmlndXJhYmxlID0gdHJ1ZTsgaWYgKFwidmFsdWVcIiBpbiBkZXNjcmlwdG9yKSBkZXNjcmlwdG9yLndyaXRhYmxlID0gdHJ1ZTsgT2JqZWN0LmRlZmluZVByb3BlcnR5KHRhcmdldCwgZGVzY3JpcHRvci5rZXksIGRlc2NyaXB0b3IpOyB9IH0gcmV0dXJuIGZ1bmN0aW9uIChDb25zdHJ1Y3RvciwgcHJvdG9Qcm9wcywgc3RhdGljUHJvcHMpIHsgaWYgKHByb3RvUHJvcHMpIGRlZmluZVByb3BlcnRpZXMoQ29uc3RydWN0b3IucHJvdG90eXBlLCBwcm90b1Byb3BzKTsgaWYgKHN0YXRpY1Byb3BzKSBkZWZpbmVQcm9wZXJ0aWVzKENvbnN0cnVjdG9yLCBzdGF0aWNQcm9wcyk7IHJldHVybiBDb25zdHJ1Y3RvcjsgfTsgfSgpO1xuXG52YXIgX2pxdWVyeSA9IF9fd2VicGFja19yZXF1aXJlX18oMCk7XG5cbnZhciBfanF1ZXJ5MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2pxdWVyeSk7XG5cbnZhciBfQ29sb3JJdGVtID0gX193ZWJwYWNrX3JlcXVpcmVfXygyKTtcblxudmFyIF9Db2xvckl0ZW0yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfQ29sb3JJdGVtKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxuZnVuY3Rpb24gX2NsYXNzQ2FsbENoZWNrKGluc3RhbmNlLCBDb25zdHJ1Y3RvcikgeyBpZiAoIShpbnN0YW5jZSBpbnN0YW5jZW9mIENvbnN0cnVjdG9yKSkgeyB0aHJvdyBuZXcgVHlwZUVycm9yKFwiQ2Fubm90IGNhbGwgYSBjbGFzcyBhcyBhIGZ1bmN0aW9uXCIpOyB9IH1cblxuLyoqXG4gKiBIYW5kbGVzIGV2ZXJ5dGhpbmcgcmVsYXRlZCB0byB0aGUgY29sb3JwaWNrZXIgY29sb3JcbiAqIEBpZ25vcmVcbiAqL1xudmFyIENvbG9ySGFuZGxlciA9IGZ1bmN0aW9uICgpIHtcbiAgLyoqXG4gICAqIEBwYXJhbSB7Q29sb3JwaWNrZXJ9IGNvbG9ycGlja2VyXG4gICAqL1xuICBmdW5jdGlvbiBDb2xvckhhbmRsZXIoY29sb3JwaWNrZXIpIHtcbiAgICBfY2xhc3NDYWxsQ2hlY2sodGhpcywgQ29sb3JIYW5kbGVyKTtcblxuICAgIC8qKlxuICAgICAqIEB0eXBlIHtDb2xvcnBpY2tlcn1cbiAgICAgKi9cbiAgICB0aGlzLmNvbG9ycGlja2VyID0gY29sb3JwaWNrZXI7XG4gIH1cblxuICAvKipcbiAgICogQHJldHVybnMgeyp8U3RyaW5nfENvbG9ySXRlbX1cbiAgICovXG5cblxuICBfY3JlYXRlQ2xhc3MoQ29sb3JIYW5kbGVyLCBbe1xuICAgIGtleTogJ2JpbmQnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBiaW5kKCkge1xuICAgICAgLy8gaWYgdGhlIGNvbG9yIG9wdGlvbiBpcyBzZXRcbiAgICAgIGlmICh0aGlzLmNvbG9ycGlja2VyLm9wdGlvbnMuY29sb3IpIHtcbiAgICAgICAgdGhpcy5jb2xvciA9IHRoaXMuY3JlYXRlQ29sb3IodGhpcy5jb2xvcnBpY2tlci5vcHRpb25zLmNvbG9yKTtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICAvLyBpZiBlbGVtZW50W2NvbG9yXSBpcyBlbXB0eSBhbmQgdGhlIGlucHV0IGhhcyBhIHZhbHVlXG4gICAgICBpZiAoIXRoaXMuY29sb3IgJiYgISF0aGlzLmNvbG9ycGlja2VyLmlucHV0SGFuZGxlci5nZXRWYWx1ZSgpKSB7XG4gICAgICAgIHRoaXMuY29sb3IgPSB0aGlzLmNyZWF0ZUNvbG9yKHRoaXMuY29sb3JwaWNrZXIuaW5wdXRIYW5kbGVyLmdldFZhbHVlKCksIHRoaXMuY29sb3JwaWNrZXIub3B0aW9ucy5hdXRvSW5wdXRGYWxsYmFjayk7XG4gICAgICB9XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAndW5iaW5kJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gdW5iaW5kKCkge1xuICAgICAgdGhpcy5jb2xvcnBpY2tlci5lbGVtZW50LnJlbW92ZURhdGEoJ2NvbG9yJyk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogUmV0dXJucyB0aGUgY29sb3Igc3RyaW5nIGZyb20gdGhlIGlucHV0IHZhbHVlIG9yIHRoZSAnZGF0YS1jb2xvcicgYXR0cmlidXRlIG9mIHRoZSBpbnB1dCBvciBlbGVtZW50LlxuICAgICAqIElmIGVtcHR5LCBpdCByZXR1cm5zIHRoZSBkZWZhdWx0VmFsdWUgcGFyYW1ldGVyLlxuICAgICAqXG4gICAgICogQHJldHVybnMge1N0cmluZ3wqfVxuICAgICAqL1xuXG4gIH0sIHtcbiAgICBrZXk6ICdnZXRDb2xvclN0cmluZycsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGdldENvbG9yU3RyaW5nKCkge1xuICAgICAgaWYgKCF0aGlzLmhhc0NvbG9yKCkpIHtcbiAgICAgICAgcmV0dXJuICcnO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gdGhpcy5jb2xvci5zdHJpbmcodGhpcy5mb3JtYXQpO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIFNldHMgdGhlIGNvbG9yIHZhbHVlXG4gICAgICpcbiAgICAgKiBAcGFyYW0ge1N0cmluZ3xDb2xvckl0ZW19IHZhbFxuICAgICAqL1xuXG4gIH0sIHtcbiAgICBrZXk6ICdzZXRDb2xvclN0cmluZycsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHNldENvbG9yU3RyaW5nKHZhbCkge1xuICAgICAgdmFyIGNvbG9yID0gdmFsID8gdGhpcy5jcmVhdGVDb2xvcih2YWwpIDogbnVsbDtcblxuICAgICAgdGhpcy5jb2xvciA9IGNvbG9yID8gY29sb3IgOiBudWxsO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIENyZWF0ZXMgYSBuZXcgY29sb3IgdXNpbmcgdGhlIHdpZGdldCBpbnN0YW5jZSBvcHRpb25zIChmYWxsYmFja0NvbG9yLCBmb3JtYXQpLlxuICAgICAqXG4gICAgICogQGZpcmVzIENvbG9ycGlja2VyI2NvbG9ycGlja2VySW52YWxpZFxuICAgICAqIEBwYXJhbSB7Kn0gdmFsXG4gICAgICogQHBhcmFtIHtib29sZWFufSBmYWxsYmFja09uSW52YWxpZFxuICAgICAqIEByZXR1cm5zIHtDb2xvckl0ZW19XG4gICAgICovXG5cbiAgfSwge1xuICAgIGtleTogJ2NyZWF0ZUNvbG9yJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gY3JlYXRlQ29sb3IodmFsKSB7XG4gICAgICB2YXIgZmFsbGJhY2tPbkludmFsaWQgPSBhcmd1bWVudHMubGVuZ3RoID4gMSAmJiBhcmd1bWVudHNbMV0gIT09IHVuZGVmaW5lZCA/IGFyZ3VtZW50c1sxXSA6IHRydWU7XG5cbiAgICAgIHZhciBjb2xvciA9IG5ldyBfQ29sb3JJdGVtMi5kZWZhdWx0KHRoaXMucmVzb2x2ZUNvbG9yRGVsZWdhdGUodmFsKSwgdGhpcy5mb3JtYXQpO1xuXG4gICAgICBpZiAoIWNvbG9yLmlzVmFsaWQoKSkge1xuICAgICAgICBpZiAoZmFsbGJhY2tPbkludmFsaWQpIHtcbiAgICAgICAgICBjb2xvciA9IHRoaXMuZ2V0RmFsbGJhY2tDb2xvcigpO1xuICAgICAgICB9XG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIChDb2xvcnBpY2tlcikgRmlyZWQgd2hlbiB0aGUgY29sb3IgaXMgaW52YWxpZCBhbmQgdGhlIGZhbGxiYWNrIGNvbG9yIGlzIGdvaW5nIHRvIGJlIHVzZWQuXG4gICAgICAgICAqXG4gICAgICAgICAqIEBldmVudCBDb2xvcnBpY2tlciNjb2xvcnBpY2tlckludmFsaWRcbiAgICAgICAgICovXG4gICAgICAgIHRoaXMuY29sb3JwaWNrZXIudHJpZ2dlcignY29sb3JwaWNrZXJJbnZhbGlkJywgY29sb3IsIHZhbCk7XG4gICAgICB9XG5cbiAgICAgIGlmICghdGhpcy5pc0FscGhhRW5hYmxlZCgpKSB7XG4gICAgICAgIC8vIEFscGhhIGlzIGRpc2FibGVkXG4gICAgICAgIGNvbG9yLmFscGhhID0gMTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIGNvbG9yO1xuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ2dldEZhbGxiYWNrQ29sb3InLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBnZXRGYWxsYmFja0NvbG9yKCkge1xuICAgICAgaWYgKHRoaXMuZmFsbGJhY2sgJiYgdGhpcy5mYWxsYmFjayA9PT0gdGhpcy5jb2xvcikge1xuICAgICAgICByZXR1cm4gdGhpcy5jb2xvcjtcbiAgICAgIH1cblxuICAgICAgdmFyIGZhbGxiYWNrID0gdGhpcy5yZXNvbHZlQ29sb3JEZWxlZ2F0ZSh0aGlzLmZhbGxiYWNrKTtcblxuICAgICAgdmFyIGNvbG9yID0gbmV3IF9Db2xvckl0ZW0yLmRlZmF1bHQoZmFsbGJhY2ssIHRoaXMuZm9ybWF0KTtcblxuICAgICAgaWYgKCFjb2xvci5pc1ZhbGlkKCkpIHtcbiAgICAgICAgY29uc29sZS53YXJuKCdUaGUgZmFsbGJhY2sgY29sb3IgaXMgaW52YWxpZC4gRmFsbGluZyBiYWNrIHRvIHRoZSBwcmV2aW91cyBjb2xvciBvciBibGFjayBpZiBhbnkuJyk7XG4gICAgICAgIHJldHVybiB0aGlzLmNvbG9yID8gdGhpcy5jb2xvciA6IG5ldyBfQ29sb3JJdGVtMi5kZWZhdWx0KCcjMDAwMDAwJywgdGhpcy5mb3JtYXQpO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gY29sb3I7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogQHJldHVybnMge0NvbG9ySXRlbX1cbiAgICAgKi9cblxuICB9LCB7XG4gICAga2V5OiAnYXNzdXJlQ29sb3InLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBhc3N1cmVDb2xvcigpIHtcbiAgICAgIGlmICghdGhpcy5oYXNDb2xvcigpKSB7XG4gICAgICAgIHRoaXMuY29sb3IgPSB0aGlzLmdldEZhbGxiYWNrQ29sb3IoKTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIHRoaXMuY29sb3I7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogRGVsZWdhdGVzIHRoZSBjb2xvciByZXNvbHV0aW9uIHRvIHRoZSBjb2xvcnBpY2tlciBleHRlbnNpb25zLlxuICAgICAqXG4gICAgICogQHBhcmFtIHtTdHJpbmd8Kn0gY29sb3JcbiAgICAgKiBAcGFyYW0ge2Jvb2xlYW59IHJlYWxDb2xvciBpZiB0cnVlLCB0aGUgY29sb3Igc2hvdWxkIHJlc29sdmUgaW50byBhIHJlYWwgKG5vdCBuYW1lZCkgY29sb3IgY29kZVxuICAgICAqIEByZXR1cm5zIHtDb2xvckl0ZW18U3RyaW5nfCp8bnVsbH1cbiAgICAgKi9cblxuICB9LCB7XG4gICAga2V5OiAncmVzb2x2ZUNvbG9yRGVsZWdhdGUnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiByZXNvbHZlQ29sb3JEZWxlZ2F0ZShjb2xvcikge1xuICAgICAgdmFyIHJlYWxDb2xvciA9IGFyZ3VtZW50cy5sZW5ndGggPiAxICYmIGFyZ3VtZW50c1sxXSAhPT0gdW5kZWZpbmVkID8gYXJndW1lbnRzWzFdIDogdHJ1ZTtcblxuICAgICAgdmFyIGV4dFJlc29sdmVkQ29sb3IgPSBmYWxzZTtcblxuICAgICAgX2pxdWVyeTIuZGVmYXVsdC5lYWNoKHRoaXMuY29sb3JwaWNrZXIuZXh0ZW5zaW9ucywgZnVuY3Rpb24gKG5hbWUsIGV4dCkge1xuICAgICAgICBpZiAoZXh0UmVzb2x2ZWRDb2xvciAhPT0gZmFsc2UpIHtcbiAgICAgICAgICAvLyBza2lwIGlmIHJlc29sdmVkXG4gICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG4gICAgICAgIGV4dFJlc29sdmVkQ29sb3IgPSBleHQucmVzb2x2ZUNvbG9yKGNvbG9yLCByZWFsQ29sb3IpO1xuICAgICAgfSk7XG5cbiAgICAgIHJldHVybiBleHRSZXNvbHZlZENvbG9yID8gZXh0UmVzb2x2ZWRDb2xvciA6IGNvbG9yO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIENoZWNrcyBpZiB0aGVyZSBpcyBhIGNvbG9yIG9iamVjdCwgdGhhdCBpdCBpcyB2YWxpZCBhbmQgaXQgaXMgbm90IGEgZmFsbGJhY2tcbiAgICAgKiBAcmV0dXJucyB7Ym9vbGVhbn1cbiAgICAgKi9cblxuICB9LCB7XG4gICAga2V5OiAnaXNJbnZhbGlkQ29sb3InLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBpc0ludmFsaWRDb2xvcigpIHtcbiAgICAgIHJldHVybiAhdGhpcy5oYXNDb2xvcigpIHx8ICF0aGlzLmNvbG9yLmlzVmFsaWQoKTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBSZXR1cm5zIHRydWUgaWYgdGhlIHVzZUFscGhhIG9wdGlvbiBpcyBleGFjdGx5IHRydWUsIGZhbHNlIG90aGVyd2lzZVxuICAgICAqIEByZXR1cm5zIHtib29sZWFufVxuICAgICAqL1xuXG4gIH0sIHtcbiAgICBrZXk6ICdpc0FscGhhRW5hYmxlZCcsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGlzQWxwaGFFbmFibGVkKCkge1xuICAgICAgcmV0dXJuIHRoaXMuY29sb3JwaWNrZXIub3B0aW9ucy51c2VBbHBoYSAhPT0gZmFsc2U7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogUmV0dXJucyB0cnVlIGlmIHRoZSBjdXJyZW50IGNvbG9yIG9iamVjdCBpcyBhbiBpbnN0YW5jZSBvZiBDb2xvciwgZmFsc2Ugb3RoZXJ3aXNlLlxuICAgICAqIEByZXR1cm5zIHtib29sZWFufVxuICAgICAqL1xuXG4gIH0sIHtcbiAgICBrZXk6ICdoYXNDb2xvcicsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGhhc0NvbG9yKCkge1xuICAgICAgcmV0dXJuIHRoaXMuY29sb3IgaW5zdGFuY2VvZiBfQ29sb3JJdGVtMi5kZWZhdWx0O1xuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ2ZhbGxiYWNrJyxcbiAgICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICAgIHJldHVybiB0aGlzLmNvbG9ycGlja2VyLm9wdGlvbnMuZmFsbGJhY2tDb2xvciA/IHRoaXMuY29sb3JwaWNrZXIub3B0aW9ucy5mYWxsYmFja0NvbG9yIDogdGhpcy5oYXNDb2xvcigpID8gdGhpcy5jb2xvciA6IG51bGw7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogQHJldHVybnMge1N0cmluZ3xudWxsfVxuICAgICAqL1xuXG4gIH0sIHtcbiAgICBrZXk6ICdmb3JtYXQnLFxuICAgIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgICAgaWYgKHRoaXMuY29sb3JwaWNrZXIub3B0aW9ucy5mb3JtYXQpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuY29sb3JwaWNrZXIub3B0aW9ucy5mb3JtYXQ7XG4gICAgICB9XG5cbiAgICAgIGlmICh0aGlzLmhhc0NvbG9yKCkgJiYgdGhpcy5jb2xvci5oYXNUcmFuc3BhcmVuY3koKSAmJiB0aGlzLmNvbG9yLmZvcm1hdC5tYXRjaCgvXmhleC8pKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmlzQWxwaGFFbmFibGVkKCkgPyAncmdiYScgOiAnaGV4JztcbiAgICAgIH1cblxuICAgICAgaWYgKHRoaXMuaGFzQ29sb3IoKSkge1xuICAgICAgICByZXR1cm4gdGhpcy5jb2xvci5mb3JtYXQ7XG4gICAgICB9XG5cbiAgICAgIHJldHVybiAncmdiJztcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBJbnRlcm5hbCBjb2xvciBnZXR0ZXJcbiAgICAgKlxuICAgICAqIEB0eXBlIHtDb2xvckl0ZW18bnVsbH1cbiAgICAgKi9cblxuICB9LCB7XG4gICAga2V5OiAnY29sb3InLFxuICAgIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgICAgcmV0dXJuIHRoaXMuY29sb3JwaWNrZXIuZWxlbWVudC5kYXRhKCdjb2xvcicpO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEludGVybmFsIGNvbG9yIHNldHRlclxuICAgICAqXG4gICAgICogQGlnbm9yZVxuICAgICAqIEBwYXJhbSB7Q29sb3JJdGVtfG51bGx9IHZhbHVlXG4gICAgICovXG4gICAgLFxuICAgIHNldDogZnVuY3Rpb24gc2V0KHZhbHVlKSB7XG4gICAgICB0aGlzLmNvbG9ycGlja2VyLmVsZW1lbnQuZGF0YSgnY29sb3InLCB2YWx1ZSk7XG5cbiAgICAgIGlmICh2YWx1ZSBpbnN0YW5jZW9mIF9Db2xvckl0ZW0yLmRlZmF1bHQgJiYgdGhpcy5jb2xvcnBpY2tlci5vcHRpb25zLmZvcm1hdCA9PT0gJ2F1dG8nKSB7XG4gICAgICAgIC8vIElmIGZvcm1hdCBpcyAnYXV0bycsIHVzZSB0aGUgZmlyc3QgcGFyc2VkIG9uZSBmcm9tIG5vdyBvblxuICAgICAgICB0aGlzLmNvbG9ycGlja2VyLm9wdGlvbnMuZm9ybWF0ID0gdGhpcy5jb2xvci5mb3JtYXQ7XG4gICAgICB9XG4gICAgfVxuICB9XSk7XG5cbiAgcmV0dXJuIENvbG9ySGFuZGxlcjtcbn0oKTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gQ29sb3JIYW5kbGVyO1xubW9kdWxlLmV4cG9ydHMgPSBleHBvcnRzLmRlZmF1bHQ7XG5cbi8qKiovIH0pLFxuLyogMjMgKi9cbi8qKiovIChmdW5jdGlvbihtb2R1bGUsIGV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pIHtcblxuXCJ1c2Ugc3RyaWN0XCI7XG5cblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9jcmVhdGVDbGFzcyA9IGZ1bmN0aW9uICgpIHsgZnVuY3Rpb24gZGVmaW5lUHJvcGVydGllcyh0YXJnZXQsIHByb3BzKSB7IGZvciAodmFyIGkgPSAwOyBpIDwgcHJvcHMubGVuZ3RoOyBpKyspIHsgdmFyIGRlc2NyaXB0b3IgPSBwcm9wc1tpXTsgZGVzY3JpcHRvci5lbnVtZXJhYmxlID0gZGVzY3JpcHRvci5lbnVtZXJhYmxlIHx8IGZhbHNlOyBkZXNjcmlwdG9yLmNvbmZpZ3VyYWJsZSA9IHRydWU7IGlmIChcInZhbHVlXCIgaW4gZGVzY3JpcHRvcikgZGVzY3JpcHRvci53cml0YWJsZSA9IHRydWU7IE9iamVjdC5kZWZpbmVQcm9wZXJ0eSh0YXJnZXQsIGRlc2NyaXB0b3Iua2V5LCBkZXNjcmlwdG9yKTsgfSB9IHJldHVybiBmdW5jdGlvbiAoQ29uc3RydWN0b3IsIHByb3RvUHJvcHMsIHN0YXRpY1Byb3BzKSB7IGlmIChwcm90b1Byb3BzKSBkZWZpbmVQcm9wZXJ0aWVzKENvbnN0cnVjdG9yLnByb3RvdHlwZSwgcHJvdG9Qcm9wcyk7IGlmIChzdGF0aWNQcm9wcykgZGVmaW5lUHJvcGVydGllcyhDb25zdHJ1Y3Rvciwgc3RhdGljUHJvcHMpOyByZXR1cm4gQ29uc3RydWN0b3I7IH07IH0oKTtcblxudmFyIF9qcXVlcnkgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKDApO1xuXG52YXIgX2pxdWVyeTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9qcXVlcnkpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG5mdW5jdGlvbiBfY2xhc3NDYWxsQ2hlY2soaW5zdGFuY2UsIENvbnN0cnVjdG9yKSB7IGlmICghKGluc3RhbmNlIGluc3RhbmNlb2YgQ29uc3RydWN0b3IpKSB7IHRocm93IG5ldyBUeXBlRXJyb3IoXCJDYW5ub3QgY2FsbCBhIGNsYXNzIGFzIGEgZnVuY3Rpb25cIik7IH0gfVxuXG4vKipcbiAqIEhhbmRsZXMgZXZlcnl0aGluZyByZWxhdGVkIHRvIHRoZSBjb2xvcnBpY2tlciBVSVxuICogQGlnbm9yZVxuICovXG52YXIgUGlja2VySGFuZGxlciA9IGZ1bmN0aW9uICgpIHtcbiAgLyoqXG4gICAqIEBwYXJhbSB7Q29sb3JwaWNrZXJ9IGNvbG9ycGlja2VyXG4gICAqL1xuICBmdW5jdGlvbiBQaWNrZXJIYW5kbGVyKGNvbG9ycGlja2VyKSB7XG4gICAgX2NsYXNzQ2FsbENoZWNrKHRoaXMsIFBpY2tlckhhbmRsZXIpO1xuXG4gICAgLyoqXG4gICAgICogQHR5cGUge0NvbG9ycGlja2VyfVxuICAgICAqL1xuICAgIHRoaXMuY29sb3JwaWNrZXIgPSBjb2xvcnBpY2tlcjtcbiAgICAvKipcbiAgICAgKiBAdHlwZSB7alF1ZXJ5fVxuICAgICAqL1xuICAgIHRoaXMucGlja2VyID0gbnVsbDtcbiAgfVxuXG4gIF9jcmVhdGVDbGFzcyhQaWNrZXJIYW5kbGVyLCBbe1xuICAgIGtleTogJ2JpbmQnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBiaW5kKCkge1xuICAgICAgLyoqXG4gICAgICAgKiBAdHlwZSB7alF1ZXJ5fEhUTUxFbGVtZW50fVxuICAgICAgICovXG4gICAgICB2YXIgcGlja2VyID0gdGhpcy5waWNrZXIgPSAoMCwgX2pxdWVyeTIuZGVmYXVsdCkodGhpcy5vcHRpb25zLnRlbXBsYXRlKTtcblxuICAgICAgaWYgKHRoaXMub3B0aW9ucy5jdXN0b21DbGFzcykge1xuICAgICAgICBwaWNrZXIuYWRkQ2xhc3ModGhpcy5vcHRpb25zLmN1c3RvbUNsYXNzKTtcbiAgICAgIH1cblxuICAgICAgaWYgKHRoaXMub3B0aW9ucy5ob3Jpem9udGFsKSB7XG4gICAgICAgIHBpY2tlci5hZGRDbGFzcygnY29sb3JwaWNrZXItaG9yaXpvbnRhbCcpO1xuICAgICAgfVxuXG4gICAgICBpZiAodGhpcy5fc3VwcG9ydHNBbHBoYUJhcigpKSB7XG4gICAgICAgIHRoaXMub3B0aW9ucy51c2VBbHBoYSA9IHRydWU7XG4gICAgICAgIHBpY2tlci5hZGRDbGFzcygnY29sb3JwaWNrZXItd2l0aC1hbHBoYScpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdGhpcy5vcHRpb25zLnVzZUFscGhhID0gZmFsc2U7XG4gICAgICB9XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnYXR0YWNoJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gYXR0YWNoKCkge1xuICAgICAgLy8gSW5qZWN0IHRoZSBjb2xvcnBpY2tlciBlbGVtZW50IGludG8gdGhlIERPTVxuICAgICAgdmFyIHBpY2tlclBhcmVudCA9IHRoaXMuY29sb3JwaWNrZXIuY29udGFpbmVyID8gdGhpcy5jb2xvcnBpY2tlci5jb250YWluZXIgOiBudWxsO1xuXG4gICAgICBpZiAocGlja2VyUGFyZW50KSB7XG4gICAgICAgIHRoaXMucGlja2VyLmFwcGVuZFRvKHBpY2tlclBhcmVudCk7XG4gICAgICB9XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAndW5iaW5kJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gdW5iaW5kKCkge1xuICAgICAgdGhpcy5waWNrZXIucmVtb3ZlKCk7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnX3N1cHBvcnRzQWxwaGFCYXInLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBfc3VwcG9ydHNBbHBoYUJhcigpIHtcbiAgICAgIHJldHVybiAodGhpcy5vcHRpb25zLnVzZUFscGhhIHx8IHRoaXMuY29sb3JwaWNrZXIuY29sb3JIYW5kbGVyLmhhc0NvbG9yKCkgJiYgdGhpcy5jb2xvci5oYXNUcmFuc3BhcmVuY3koKSkgJiYgdGhpcy5vcHRpb25zLnVzZUFscGhhICE9PSBmYWxzZSAmJiAoIXRoaXMub3B0aW9ucy5mb3JtYXQgfHwgdGhpcy5vcHRpb25zLmZvcm1hdCAmJiAhdGhpcy5vcHRpb25zLmZvcm1hdC5tYXRjaCgvXmhleChbMzZdKT8kL2kpKTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBDaGFuZ2VzIHRoZSBjb2xvciBhZGp1c3RtZW50IGJhcnMgdXNpbmcgdGhlIGN1cnJlbnQgY29sb3Igb2JqZWN0IGluZm9ybWF0aW9uLlxuICAgICAqL1xuXG4gIH0sIHtcbiAgICBrZXk6ICd1cGRhdGUnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiB1cGRhdGUoKSB7XG4gICAgICBpZiAoIXRoaXMuY29sb3JwaWNrZXIuY29sb3JIYW5kbGVyLmhhc0NvbG9yKCkpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICB2YXIgdmVydGljYWwgPSB0aGlzLm9wdGlvbnMuaG9yaXpvbnRhbCAhPT0gdHJ1ZSxcbiAgICAgICAgICBzbGlkZXIgPSB2ZXJ0aWNhbCA/IHRoaXMub3B0aW9ucy5zbGlkZXJzIDogdGhpcy5vcHRpb25zLnNsaWRlcnNIb3J6O1xuXG4gICAgICB2YXIgc2F0dXJhdGlvbkd1aWRlID0gdGhpcy5waWNrZXIuZmluZCgnLmNvbG9ycGlja2VyLXNhdHVyYXRpb24gLmNvbG9ycGlja2VyLWd1aWRlJyksXG4gICAgICAgICAgaHVlR3VpZGUgPSB0aGlzLnBpY2tlci5maW5kKCcuY29sb3JwaWNrZXItaHVlIC5jb2xvcnBpY2tlci1ndWlkZScpLFxuICAgICAgICAgIGFscGhhR3VpZGUgPSB0aGlzLnBpY2tlci5maW5kKCcuY29sb3JwaWNrZXItYWxwaGEgLmNvbG9ycGlja2VyLWd1aWRlJyk7XG5cbiAgICAgIHZhciBoc3ZhID0gdGhpcy5jb2xvci50b0hzdmFSYXRpbygpO1xuXG4gICAgICAvLyBTZXQgZ3VpZGVzIHBvc2l0aW9uXG4gICAgICBpZiAoaHVlR3VpZGUubGVuZ3RoKSB7XG4gICAgICAgIGh1ZUd1aWRlLmNzcyh2ZXJ0aWNhbCA/ICd0b3AnIDogJ2xlZnQnLCAodmVydGljYWwgPyBzbGlkZXIuaHVlLm1heFRvcCA6IHNsaWRlci5odWUubWF4TGVmdCkgKiAoMSAtIGhzdmEuaCkpO1xuICAgICAgfVxuICAgICAgaWYgKGFscGhhR3VpZGUubGVuZ3RoKSB7XG4gICAgICAgIGFscGhhR3VpZGUuY3NzKHZlcnRpY2FsID8gJ3RvcCcgOiAnbGVmdCcsICh2ZXJ0aWNhbCA/IHNsaWRlci5hbHBoYS5tYXhUb3AgOiBzbGlkZXIuYWxwaGEubWF4TGVmdCkgKiAoMSAtIGhzdmEuYSkpO1xuICAgICAgfVxuICAgICAgaWYgKHNhdHVyYXRpb25HdWlkZS5sZW5ndGgpIHtcbiAgICAgICAgc2F0dXJhdGlvbkd1aWRlLmNzcyh7XG4gICAgICAgICAgJ3RvcCc6IHNsaWRlci5zYXR1cmF0aW9uLm1heFRvcCAtIGhzdmEudiAqIHNsaWRlci5zYXR1cmF0aW9uLm1heFRvcCxcbiAgICAgICAgICAnbGVmdCc6IGhzdmEucyAqIHNsaWRlci5zYXR1cmF0aW9uLm1heExlZnRcbiAgICAgICAgfSk7XG4gICAgICB9XG5cbiAgICAgIC8vIFNldCBzYXR1cmF0aW9uIGh1ZSBiYWNrZ3JvdW5kXG4gICAgICB0aGlzLnBpY2tlci5maW5kKCcuY29sb3JwaWNrZXItc2F0dXJhdGlvbicpLmNzcygnYmFja2dyb3VuZENvbG9yJywgdGhpcy5jb2xvci5nZXRDbG9uZUh1ZU9ubHkoKS50b0hleFN0cmluZygpKTsgLy8gd2Ugb25seSBuZWVkIGh1ZVxuXG4gICAgICAvLyBTZXQgYWxwaGEgY29sb3IgZ3JhZGllbnRcbiAgICAgIHZhciBoZXhDb2xvciA9IHRoaXMuY29sb3IudG9IZXhTdHJpbmcoKTtcblxuICAgICAgdmFyIGFscGhhQmcgPSAnJztcblxuICAgICAgaWYgKHRoaXMub3B0aW9ucy5ob3Jpem9udGFsKSB7XG4gICAgICAgIGFscGhhQmcgPSAnbGluZWFyLWdyYWRpZW50KHRvIHJpZ2h0LCAnICsgaGV4Q29sb3IgKyAnIDAlLCB0cmFuc3BhcmVudCAxMDAlKSc7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBhbHBoYUJnID0gJ2xpbmVhci1ncmFkaWVudCh0byBib3R0b20sICcgKyBoZXhDb2xvciArICcgMCUsIHRyYW5zcGFyZW50IDEwMCUpJztcbiAgICAgIH1cblxuICAgICAgdGhpcy5waWNrZXIuZmluZCgnLmNvbG9ycGlja2VyLWFscGhhLWNvbG9yJykuY3NzKCdiYWNrZ3JvdW5kJywgYWxwaGFCZyk7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnb3B0aW9ucycsXG4gICAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgICByZXR1cm4gdGhpcy5jb2xvcnBpY2tlci5vcHRpb25zO1xuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ2NvbG9yJyxcbiAgICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICAgIHJldHVybiB0aGlzLmNvbG9ycGlja2VyLmNvbG9ySGFuZGxlci5jb2xvcjtcbiAgICB9XG4gIH1dKTtcblxuICByZXR1cm4gUGlja2VySGFuZGxlcjtcbn0oKTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gUGlja2VySGFuZGxlcjtcbm1vZHVsZS5leHBvcnRzID0gZXhwb3J0cy5kZWZhdWx0O1xuXG4vKioqLyB9KSxcbi8qIDI0ICovXG4vKioqLyAoZnVuY3Rpb24obW9kdWxlLCBleHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKSB7XG5cblwidXNlIHN0cmljdFwiO1xuXG5cbi8qKlxuICogSGFuZGxlcyBldmVyeXRoaW5nIHJlbGF0ZWQgdG8gdGhlIGNvbG9ycGlja2VyIGFkZG9uXG4gKiBAaWdub3JlXG4gKi9cblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9jcmVhdGVDbGFzcyA9IGZ1bmN0aW9uICgpIHsgZnVuY3Rpb24gZGVmaW5lUHJvcGVydGllcyh0YXJnZXQsIHByb3BzKSB7IGZvciAodmFyIGkgPSAwOyBpIDwgcHJvcHMubGVuZ3RoOyBpKyspIHsgdmFyIGRlc2NyaXB0b3IgPSBwcm9wc1tpXTsgZGVzY3JpcHRvci5lbnVtZXJhYmxlID0gZGVzY3JpcHRvci5lbnVtZXJhYmxlIHx8IGZhbHNlOyBkZXNjcmlwdG9yLmNvbmZpZ3VyYWJsZSA9IHRydWU7IGlmIChcInZhbHVlXCIgaW4gZGVzY3JpcHRvcikgZGVzY3JpcHRvci53cml0YWJsZSA9IHRydWU7IE9iamVjdC5kZWZpbmVQcm9wZXJ0eSh0YXJnZXQsIGRlc2NyaXB0b3Iua2V5LCBkZXNjcmlwdG9yKTsgfSB9IHJldHVybiBmdW5jdGlvbiAoQ29uc3RydWN0b3IsIHByb3RvUHJvcHMsIHN0YXRpY1Byb3BzKSB7IGlmIChwcm90b1Byb3BzKSBkZWZpbmVQcm9wZXJ0aWVzKENvbnN0cnVjdG9yLnByb3RvdHlwZSwgcHJvdG9Qcm9wcyk7IGlmIChzdGF0aWNQcm9wcykgZGVmaW5lUHJvcGVydGllcyhDb25zdHJ1Y3Rvciwgc3RhdGljUHJvcHMpOyByZXR1cm4gQ29uc3RydWN0b3I7IH07IH0oKTtcblxuZnVuY3Rpb24gX2NsYXNzQ2FsbENoZWNrKGluc3RhbmNlLCBDb25zdHJ1Y3RvcikgeyBpZiAoIShpbnN0YW5jZSBpbnN0YW5jZW9mIENvbnN0cnVjdG9yKSkgeyB0aHJvdyBuZXcgVHlwZUVycm9yKFwiQ2Fubm90IGNhbGwgYSBjbGFzcyBhcyBhIGZ1bmN0aW9uXCIpOyB9IH1cblxudmFyIEFkZG9uSGFuZGxlciA9IGZ1bmN0aW9uICgpIHtcbiAgLyoqXG4gICAqIEBwYXJhbSB7Q29sb3JwaWNrZXJ9IGNvbG9ycGlja2VyXG4gICAqL1xuICBmdW5jdGlvbiBBZGRvbkhhbmRsZXIoY29sb3JwaWNrZXIpIHtcbiAgICBfY2xhc3NDYWxsQ2hlY2sodGhpcywgQWRkb25IYW5kbGVyKTtcblxuICAgIC8qKlxuICAgICAqIEB0eXBlIHtDb2xvcnBpY2tlcn1cbiAgICAgKi9cbiAgICB0aGlzLmNvbG9ycGlja2VyID0gY29sb3JwaWNrZXI7XG4gICAgLyoqXG4gICAgICogQHR5cGUge2pRdWVyeX1cbiAgICAgKi9cbiAgICB0aGlzLmFkZG9uID0gbnVsbDtcbiAgfVxuXG4gIF9jcmVhdGVDbGFzcyhBZGRvbkhhbmRsZXIsIFt7XG4gICAga2V5OiAnaGFzQWRkb24nLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBoYXNBZGRvbigpIHtcbiAgICAgIHJldHVybiAhIXRoaXMuYWRkb247XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnYmluZCcsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGJpbmQoKSB7XG4gICAgICAvKipcbiAgICAgICAqIEB0eXBlIHsqfGpRdWVyeX1cbiAgICAgICAqL1xuICAgICAgdGhpcy5hZGRvbiA9IHRoaXMuY29sb3JwaWNrZXIub3B0aW9ucy5hZGRvbiA/IHRoaXMuY29sb3JwaWNrZXIuZWxlbWVudC5maW5kKHRoaXMuY29sb3JwaWNrZXIub3B0aW9ucy5hZGRvbikgOiBudWxsO1xuXG4gICAgICBpZiAodGhpcy5hZGRvbiAmJiB0aGlzLmFkZG9uLmxlbmd0aCA9PT0gMCkge1xuICAgICAgICAvLyBub3QgZm91bmRcbiAgICAgICAgdGhpcy5hZGRvbiA9IG51bGw7XG4gICAgICB9XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAndW5iaW5kJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gdW5iaW5kKCkge1xuICAgICAgaWYgKHRoaXMuaGFzQWRkb24oKSkge1xuICAgICAgICB0aGlzLmFkZG9uLm9mZignLmNvbG9ycGlja2VyJyk7XG4gICAgICB9XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogSWYgdGhlIGFkZG9uIGVsZW1lbnQgaXMgcHJlc2VudCwgaXRzIGJhY2tncm91bmQgY29sb3IgaXMgdXBkYXRlZFxuICAgICAqL1xuXG4gIH0sIHtcbiAgICBrZXk6ICd1cGRhdGUnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiB1cGRhdGUoKSB7XG4gICAgICBpZiAoIXRoaXMuY29sb3JwaWNrZXIuY29sb3JIYW5kbGVyLmhhc0NvbG9yKCkgfHwgIXRoaXMuaGFzQWRkb24oKSkge1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG5cbiAgICAgIHZhciBjb2xvclN0ciA9IHRoaXMuY29sb3JwaWNrZXIuY29sb3JIYW5kbGVyLmdldENvbG9yU3RyaW5nKCk7XG5cbiAgICAgIHZhciBzdHlsZXMgPSB7ICdiYWNrZ3JvdW5kJzogY29sb3JTdHIgfTtcblxuICAgICAgdmFyIGljbiA9IHRoaXMuYWRkb24uZmluZCgnaScpLmVxKDApO1xuXG4gICAgICBpZiAoaWNuLmxlbmd0aCA+IDApIHtcbiAgICAgICAgaWNuLmNzcyhzdHlsZXMpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdGhpcy5hZGRvbi5jc3Moc3R5bGVzKTtcbiAgICAgIH1cbiAgICB9XG4gIH1dKTtcblxuICByZXR1cm4gQWRkb25IYW5kbGVyO1xufSgpO1xuXG5leHBvcnRzLmRlZmF1bHQgPSBBZGRvbkhhbmRsZXI7XG5tb2R1bGUuZXhwb3J0cyA9IGV4cG9ydHMuZGVmYXVsdDtcblxuLyoqKi8gfSlcbi8qKioqKiovIF0pO1xufSk7XG4vLyMgc291cmNlTWFwcGluZ1VSTD1ib290c3RyYXAtY29sb3JwaWNrZXIuanMubWFwXG59LmNhbGwod2luZG93LCBmYWxzZSwgZmFsc2UpKTtcbiIsIm1vZHVsZS5leHBvcnRzID0gd2luZG93W1wialF1ZXJ5XCJdOyIsIi8vIFRoZSBtb2R1bGUgY2FjaGVcbnZhciBfX3dlYnBhY2tfbW9kdWxlX2NhY2hlX18gPSB7fTtcblxuLy8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbmZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG5cdHZhciBjYWNoZWRNb2R1bGUgPSBfX3dlYnBhY2tfbW9kdWxlX2NhY2hlX19bbW9kdWxlSWRdO1xuXHRpZiAoY2FjaGVkTW9kdWxlICE9PSB1bmRlZmluZWQpIHtcblx0XHRyZXR1cm4gY2FjaGVkTW9kdWxlLmV4cG9ydHM7XG5cdH1cblx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcblx0dmFyIG1vZHVsZSA9IF9fd2VicGFja19tb2R1bGVfY2FjaGVfX1ttb2R1bGVJZF0gPSB7XG5cdFx0Ly8gbm8gbW9kdWxlLmlkIG5lZWRlZFxuXHRcdC8vIG5vIG1vZHVsZS5sb2FkZWQgbmVlZGVkXG5cdFx0ZXhwb3J0czoge31cblx0fTtcblxuXHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cblx0X193ZWJwYWNrX21vZHVsZXNfX1ttb2R1bGVJZF0obW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cblx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcblx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xufVxuXG4iLCIvKipcbiAqIENvcHlyaWdodCBzaW5jZSAyMDA3IFByZXN0YVNob3AgU0EgYW5kIENvbnRyaWJ1dG9yc1xuICogUHJlc3RhU2hvcCBpcyBhbiBJbnRlcm5hdGlvbmFsIFJlZ2lzdGVyZWQgVHJhZGVtYXJrICYgUHJvcGVydHkgb2YgUHJlc3RhU2hvcCBTQVxuICpcbiAqIE5PVElDRSBPRiBMSUNFTlNFXG4gKlxuICogVGhpcyBzb3VyY2UgZmlsZSBpcyBzdWJqZWN0IHRvIHRoZSBPcGVuIFNvZnR3YXJlIExpY2Vuc2UgKE9TTCAzLjApXG4gKiB0aGF0IGlzIGJ1bmRsZWQgd2l0aCB0aGlzIHBhY2thZ2UgaW4gdGhlIGZpbGUgTElDRU5TRS5tZC5cbiAqIEl0IGlzIGFsc28gYXZhaWxhYmxlIHRocm91Z2ggdGhlIHdvcmxkLXdpZGUtd2ViIGF0IHRoaXMgVVJMOlxuICogaHR0cHM6Ly9vcGVuc291cmNlLm9yZy9saWNlbnNlcy9PU0wtMy4wXG4gKiBJZiB5b3UgZGlkIG5vdCByZWNlaXZlIGEgY29weSBvZiB0aGUgbGljZW5zZSBhbmQgYXJlIHVuYWJsZSB0b1xuICogb2J0YWluIGl0IHRocm91Z2ggdGhlIHdvcmxkLXdpZGUtd2ViLCBwbGVhc2Ugc2VuZCBhbiBlbWFpbFxuICogdG8gbGljZW5zZUBwcmVzdGFzaG9wLmNvbSBzbyB3ZSBjYW4gc2VuZCB5b3UgYSBjb3B5IGltbWVkaWF0ZWx5LlxuICpcbiAqIERJU0NMQUlNRVJcbiAqXG4gKiBEbyBub3QgZWRpdCBvciBhZGQgdG8gdGhpcyBmaWxlIGlmIHlvdSB3aXNoIHRvIHVwZ3JhZGUgUHJlc3RhU2hvcCB0byBuZXdlclxuICogdmVyc2lvbnMgaW4gdGhlIGZ1dHVyZS4gSWYgeW91IHdpc2ggdG8gY3VzdG9taXplIFByZXN0YVNob3AgZm9yIHlvdXJcbiAqIG5lZWRzIHBsZWFzZSByZWZlciB0byBodHRwczovL2RldmRvY3MucHJlc3Rhc2hvcC5jb20vIGZvciBtb3JlIGluZm9ybWF0aW9uLlxuICpcbiAqIEBhdXRob3IgICAgUHJlc3RhU2hvcCBTQSBhbmQgQ29udHJpYnV0b3JzIDxjb250YWN0QHByZXN0YXNob3AuY29tPlxuICogQGNvcHlyaWdodCBTaW5jZSAyMDA3IFByZXN0YVNob3AgU0EgYW5kIENvbnRyaWJ1dG9yc1xuICogQGxpY2Vuc2UgICBodHRwczovL29wZW5zb3VyY2Uub3JnL2xpY2Vuc2VzL09TTC0zLjAgT3BlbiBTb2Z0d2FyZSBMaWNlbnNlIChPU0wgMy4wKVxuICovXG5cbmltcG9ydCBpbml0Q29sb3JQaWNrZXJzIGZyb20gJ0BhcHAvdXRpbHMvY29sb3JwaWNrZXInO1xuaW1wb3J0IFRyYW5zbGF0YWJsZUNob2ljZSBmcm9tICdAY29tcG9uZW50cy9mb3JtL3RyYW5zbGF0YWJsZS1jaG9pY2UnO1xuaW1wb3J0IFRyYW5zbGF0YWJsZUlucHV0IGZyb20gJ0Bjb21wb25lbnRzL3RyYW5zbGF0YWJsZS1pbnB1dCc7XG5pbXBvcnQgRm9ybU1hcCBmcm9tICdAcGFnZXMvb3JkZXItc3RhdGVzL2Zvcm0tbWFwJztcblxuY29uc3QgeyR9ID0gd2luZG93O1xuXG4kKCgpID0+IHtcbiAgaW5pdENvbG9yUGlja2VycygpO1xuICBuZXcgVHJhbnNsYXRhYmxlSW5wdXQoKTtcbiAgbmV3IFRyYW5zbGF0YWJsZUNob2ljZSgpO1xuXG4gIGxldCB0ZW1wbGF0ZVByZXZpZXdXaW5kb3cgPSBudWxsO1xuICBmdW5jdGlvbiB2aWV3VGVtcGxhdGVzKCR1cmkpIHtcbiAgICBpZiAodGVtcGxhdGVQcmV2aWV3V2luZG93ICE9IG51bGwgJiYgIXRlbXBsYXRlUHJldmlld1dpbmRvdy5jbG9zZWQpIHtcbiAgICAgIHRlbXBsYXRlUHJldmlld1dpbmRvdy5jbG9zZSgpO1xuICAgIH1cbiAgICB0ZW1wbGF0ZVByZXZpZXdXaW5kb3cgPSB3aW5kb3cub3BlbihcbiAgICAgICR1cmksXG4gICAgICAndHBsX3ZpZXdpbmcnLFxuICAgICAgJ3Rvb2xiYXI9MCwnXG4gICAgICArICdsb2NhdGlvbj0wLCdcbiAgICAgICsgJ2RpcmVjdG9yaWVzPTAsJ1xuICAgICAgKyAnc3RhdGZyPW5vLCdcbiAgICAgICsgJ21lbnViYXI9MCwnXG4gICAgICArICdzY3JvbGxiYXJzPXllcywnXG4gICAgICArICdyZXNpemFibGU9eWVzLCdcbiAgICAgICsgJ3dpZHRoPTUyMCwnXG4gICAgICArICdoZWlnaHQ9NDAwLCdcbiAgICAgICsgJ3RvcD01MCwnXG4gICAgICArICdsZWZ0PTMwMCcsXG4gICAgKTtcbiAgICB0ZW1wbGF0ZVByZXZpZXdXaW5kb3cuZm9jdXMoKTtcbiAgfVxuXG4gICQoZG9jdW1lbnQpLnJlYWR5KCgpID0+IHtcbiAgICBpZiAoISQoRm9ybU1hcC5zZW5kRW1haWxTZWxlY3RvcikuaXMoJzpjaGVja2VkJykpIHtcbiAgICAgICQoRm9ybU1hcC5tYWlsVGVtcGxhdGVTZWxlY3RvcikuaGlkZSgpO1xuICAgIH1cbiAgICAkKGRvY3VtZW50KS5vbignY2hhbmdlJywgRm9ybU1hcC5zZW5kRW1haWxTZWxlY3RvciwgKCkgPT4ge1xuICAgICAgJChGb3JtTWFwLm1haWxUZW1wbGF0ZVNlbGVjdG9yKS5zbGlkZVRvZ2dsZSgpO1xuICAgIH0pO1xuXG4gICAgJChkb2N1bWVudCkub24oJ2NsaWNrJywgRm9ybU1hcC5tYWlsVGVtcGxhdGVQcmV2aWV3LCAoZXZlbnQpID0+IHtcbiAgICAgIGNvbnN0ICRlbGVtZW50ID0gJChldmVudC5jdXJyZW50VGFyZ2V0KTtcbiAgICAgIGNvbnN0ICRzZWxlY3QgPSAkZWxlbWVudC5jbG9zZXN0KCcuZm9ybS1ncm91cCcpLmZpbmQoJ3NlbGVjdC50cmFuc2xhdGFibGVfY2hvaWNlOnZpc2libGUnKTtcbiAgICAgIGNvbnN0ICR1cmkgPSAkc2VsZWN0LmZpbmQoJ29wdGlvbjpzZWxlY3RlZCcpLmF0dHIoJ2RhdGEtcHJldmlldycpO1xuXG4gICAgICB2aWV3VGVtcGxhdGVzKCR1cmkpO1xuICAgIH0pO1xuICB9KTtcbn0pO1xuIl0sInNvdXJjZVJvb3QiOiIifQ==