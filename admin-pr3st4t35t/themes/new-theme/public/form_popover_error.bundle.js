/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
var __webpack_exports__ = {};
/*!**************************************************!*\
  !*** ./js/components/form/form-popover-error.js ***!
  \**************************************************/


/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 */

var _window = window,
    $ = _window.$;

/**
 * Component responsible for displaying form popover errors with modified width which is calculated based on the
 * form group width.
 */

$(function () {
  // loads form popover instance
  $('[data-toggle="form-popover-error"]').popover({
    html: true,
    content: function content() {
      return getErrorContent(this);
    }
  });

  /**
   * Recalculates popover position so it is always aligned horizontally and width is identical
   * to the child elements of the form.
   * @param {Object} event
   */
  var repositionPopover = function repositionPopover(event) {
    var $element = $(event.currentTarget);
    var $formGroup = $element.closest('.form-group');
    var $invalidFeedbackContainer = $formGroup.find('.invalid-feedback-container');
    var $errorPopover = $formGroup.find('.form-popover-error');

    var localeVisibleElementWidth = $invalidFeedbackContainer.width();

    $errorPopover.css('width', localeVisibleElementWidth);

    var horizontalDifference = getHorizontalDifference($invalidFeedbackContainer, $errorPopover);

    $errorPopover.css('left', horizontalDifference + 'px');
  };

  /**
   * gets horizontal difference which helps to align popover horizontally.
   * @param {jQuery} $invalidFeedbackContainer
   * @param {jQuery} $errorPopover
   * @returns {number}
   */
  var getHorizontalDifference = function getHorizontalDifference($invalidFeedbackContainer, $errorPopover) {
    var inputHorizontalPosition = $invalidFeedbackContainer.offset().left;
    var popoverHorizontalPosition = $errorPopover.offset().left;

    return inputHorizontalPosition - popoverHorizontalPosition;
  };

  /**
   * Gets popover error content pre-fetched in html. It used unique selector to identify which one content to render.
   *
   * @param popoverTriggerElement
   * @returns {jQuery}
   */
  var getErrorContent = function getErrorContent(popoverTriggerElement) {
    var popoverTriggerId = $(popoverTriggerElement).data('id');

    return $('.js-popover-error-content[data-id="' + popoverTriggerId + '"]').html();
  };

  // registers the event which displays the popover
  $(document).on('shown.bs.popover', '[data-toggle="form-popover-error"]', function (event) {
    return repositionPopover(event);
  });
});
window.form_popover_error = __webpack_exports__;
/******/ })()
;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9bbmFtZV0vLi9qcy9jb21wb25lbnRzL2Zvcm0vZm9ybS1wb3BvdmVyLWVycm9yLmpzIl0sIm5hbWVzIjpbIndpbmRvdyIsIiQiLCJwb3BvdmVyIiwiaHRtbCIsImNvbnRlbnQiLCJnZXRFcnJvckNvbnRlbnQiLCJyZXBvc2l0aW9uUG9wb3ZlciIsImV2ZW50IiwiJGVsZW1lbnQiLCJjdXJyZW50VGFyZ2V0IiwiJGZvcm1Hcm91cCIsImNsb3Nlc3QiLCIkaW52YWxpZEZlZWRiYWNrQ29udGFpbmVyIiwiZmluZCIsIiRlcnJvclBvcG92ZXIiLCJsb2NhbGVWaXNpYmxlRWxlbWVudFdpZHRoIiwid2lkdGgiLCJjc3MiLCJob3Jpem9udGFsRGlmZmVyZW5jZSIsImdldEhvcml6b250YWxEaWZmZXJlbmNlIiwiaW5wdXRIb3Jpem9udGFsUG9zaXRpb24iLCJvZmZzZXQiLCJsZWZ0IiwicG9wb3Zlckhvcml6b250YWxQb3NpdGlvbiIsInBvcG92ZXJUcmlnZ2VyRWxlbWVudCIsInBvcG92ZXJUcmlnZ2VySWQiLCJkYXRhIiwiZG9jdW1lbnQiLCJvbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFBQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztjQXlCWUEsTTtJQUFMQyxDLFdBQUFBLEM7O0FBRVA7Ozs7O0FBSUFBLEVBQUUsWUFBTTtBQUNOO0FBQ0FBLElBQUUsb0NBQUYsRUFBd0NDLE9BQXhDLENBQWdEO0FBQzlDQyxVQUFNLElBRHdDO0FBRTlDQyxXQUY4QyxxQkFFcEM7QUFDUixhQUFPQyxnQkFBZ0IsSUFBaEIsQ0FBUDtBQUNEO0FBSjZDLEdBQWhEOztBQU9BOzs7OztBQUtBLE1BQU1DLG9CQUFvQixTQUFwQkEsaUJBQW9CLENBQUNDLEtBQUQsRUFBVztBQUNuQyxRQUFNQyxXQUFXUCxFQUFFTSxNQUFNRSxhQUFSLENBQWpCO0FBQ0EsUUFBTUMsYUFBYUYsU0FBU0csT0FBVCxDQUFpQixhQUFqQixDQUFuQjtBQUNBLFFBQU1DLDRCQUE0QkYsV0FBV0csSUFBWCxDQUFnQiw2QkFBaEIsQ0FBbEM7QUFDQSxRQUFNQyxnQkFBZ0JKLFdBQVdHLElBQVgsQ0FBZ0IscUJBQWhCLENBQXRCOztBQUVBLFFBQU1FLDRCQUE0QkgsMEJBQTBCSSxLQUExQixFQUFsQzs7QUFFQUYsa0JBQWNHLEdBQWQsQ0FBa0IsT0FBbEIsRUFBMkJGLHlCQUEzQjs7QUFFQSxRQUFNRyx1QkFBdUJDLHdCQUF3QlAseUJBQXhCLEVBQW1ERSxhQUFuRCxDQUE3Qjs7QUFFQUEsa0JBQWNHLEdBQWQsQ0FBa0IsTUFBbEIsRUFBNkJDLG9CQUE3QjtBQUNELEdBYkQ7O0FBZUE7Ozs7OztBQU1BLE1BQU1DLDBCQUEwQixTQUExQkEsdUJBQTBCLENBQUNQLHlCQUFELEVBQTRCRSxhQUE1QixFQUE4QztBQUM1RSxRQUFNTSwwQkFBMEJSLDBCQUEwQlMsTUFBMUIsR0FBbUNDLElBQW5FO0FBQ0EsUUFBTUMsNEJBQTRCVCxjQUFjTyxNQUFkLEdBQXVCQyxJQUF6RDs7QUFFQSxXQUFPRiwwQkFBMEJHLHlCQUFqQztBQUNELEdBTEQ7O0FBT0E7Ozs7OztBQU1BLE1BQU1sQixrQkFBa0IsU0FBbEJBLGVBQWtCLENBQUNtQixxQkFBRCxFQUEyQjtBQUNqRCxRQUFNQyxtQkFBbUJ4QixFQUFFdUIscUJBQUYsRUFBeUJFLElBQXpCLENBQThCLElBQTlCLENBQXpCOztBQUVBLFdBQU96QiwwQ0FBd0N3QixnQkFBeEMsU0FBOER0QixJQUE5RCxFQUFQO0FBQ0QsR0FKRDs7QUFNQTtBQUNBRixJQUFFMEIsUUFBRixFQUFZQyxFQUFaLENBQWUsa0JBQWYsRUFBbUMsb0NBQW5DLEVBQXlFLFVBQUNyQixLQUFEO0FBQUEsV0FBV0Qsa0JBQWtCQyxLQUFsQixDQUFYO0FBQUEsR0FBekU7QUFDRCxDQXhERCxFIiwiZmlsZSI6ImZvcm1fcG9wb3Zlcl9lcnJvci5idW5kbGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIENvcHlyaWdodCBzaW5jZSAyMDA3IFByZXN0YVNob3AgU0EgYW5kIENvbnRyaWJ1dG9yc1xuICogUHJlc3RhU2hvcCBpcyBhbiBJbnRlcm5hdGlvbmFsIFJlZ2lzdGVyZWQgVHJhZGVtYXJrICYgUHJvcGVydHkgb2YgUHJlc3RhU2hvcCBTQVxuICpcbiAqIE5PVElDRSBPRiBMSUNFTlNFXG4gKlxuICogVGhpcyBzb3VyY2UgZmlsZSBpcyBzdWJqZWN0IHRvIHRoZSBPcGVuIFNvZnR3YXJlIExpY2Vuc2UgKE9TTCAzLjApXG4gKiB0aGF0IGlzIGJ1bmRsZWQgd2l0aCB0aGlzIHBhY2thZ2UgaW4gdGhlIGZpbGUgTElDRU5TRS5tZC5cbiAqIEl0IGlzIGFsc28gYXZhaWxhYmxlIHRocm91Z2ggdGhlIHdvcmxkLXdpZGUtd2ViIGF0IHRoaXMgVVJMOlxuICogaHR0cHM6Ly9vcGVuc291cmNlLm9yZy9saWNlbnNlcy9PU0wtMy4wXG4gKiBJZiB5b3UgZGlkIG5vdCByZWNlaXZlIGEgY29weSBvZiB0aGUgbGljZW5zZSBhbmQgYXJlIHVuYWJsZSB0b1xuICogb2J0YWluIGl0IHRocm91Z2ggdGhlIHdvcmxkLXdpZGUtd2ViLCBwbGVhc2Ugc2VuZCBhbiBlbWFpbFxuICogdG8gbGljZW5zZUBwcmVzdGFzaG9wLmNvbSBzbyB3ZSBjYW4gc2VuZCB5b3UgYSBjb3B5IGltbWVkaWF0ZWx5LlxuICpcbiAqIERJU0NMQUlNRVJcbiAqXG4gKiBEbyBub3QgZWRpdCBvciBhZGQgdG8gdGhpcyBmaWxlIGlmIHlvdSB3aXNoIHRvIHVwZ3JhZGUgUHJlc3RhU2hvcCB0byBuZXdlclxuICogdmVyc2lvbnMgaW4gdGhlIGZ1dHVyZS4gSWYgeW91IHdpc2ggdG8gY3VzdG9taXplIFByZXN0YVNob3AgZm9yIHlvdXJcbiAqIG5lZWRzIHBsZWFzZSByZWZlciB0byBodHRwczovL2RldmRvY3MucHJlc3Rhc2hvcC5jb20vIGZvciBtb3JlIGluZm9ybWF0aW9uLlxuICpcbiAqIEBhdXRob3IgICAgUHJlc3RhU2hvcCBTQSBhbmQgQ29udHJpYnV0b3JzIDxjb250YWN0QHByZXN0YXNob3AuY29tPlxuICogQGNvcHlyaWdodCBTaW5jZSAyMDA3IFByZXN0YVNob3AgU0EgYW5kIENvbnRyaWJ1dG9yc1xuICogQGxpY2Vuc2UgICBodHRwczovL29wZW5zb3VyY2Uub3JnL2xpY2Vuc2VzL09TTC0zLjAgT3BlbiBTb2Z0d2FyZSBMaWNlbnNlIChPU0wgMy4wKVxuICovXG5cbmNvbnN0IHskfSA9IHdpbmRvdztcblxuLyoqXG4gKiBDb21wb25lbnQgcmVzcG9uc2libGUgZm9yIGRpc3BsYXlpbmcgZm9ybSBwb3BvdmVyIGVycm9ycyB3aXRoIG1vZGlmaWVkIHdpZHRoIHdoaWNoIGlzIGNhbGN1bGF0ZWQgYmFzZWQgb24gdGhlXG4gKiBmb3JtIGdyb3VwIHdpZHRoLlxuICovXG4kKCgpID0+IHtcbiAgLy8gbG9hZHMgZm9ybSBwb3BvdmVyIGluc3RhbmNlXG4gICQoJ1tkYXRhLXRvZ2dsZT1cImZvcm0tcG9wb3Zlci1lcnJvclwiXScpLnBvcG92ZXIoe1xuICAgIGh0bWw6IHRydWUsXG4gICAgY29udGVudCgpIHtcbiAgICAgIHJldHVybiBnZXRFcnJvckNvbnRlbnQodGhpcyk7XG4gICAgfSxcbiAgfSk7XG5cbiAgLyoqXG4gICAqIFJlY2FsY3VsYXRlcyBwb3BvdmVyIHBvc2l0aW9uIHNvIGl0IGlzIGFsd2F5cyBhbGlnbmVkIGhvcml6b250YWxseSBhbmQgd2lkdGggaXMgaWRlbnRpY2FsXG4gICAqIHRvIHRoZSBjaGlsZCBlbGVtZW50cyBvZiB0aGUgZm9ybS5cbiAgICogQHBhcmFtIHtPYmplY3R9IGV2ZW50XG4gICAqL1xuICBjb25zdCByZXBvc2l0aW9uUG9wb3ZlciA9IChldmVudCkgPT4ge1xuICAgIGNvbnN0ICRlbGVtZW50ID0gJChldmVudC5jdXJyZW50VGFyZ2V0KTtcbiAgICBjb25zdCAkZm9ybUdyb3VwID0gJGVsZW1lbnQuY2xvc2VzdCgnLmZvcm0tZ3JvdXAnKTtcbiAgICBjb25zdCAkaW52YWxpZEZlZWRiYWNrQ29udGFpbmVyID0gJGZvcm1Hcm91cC5maW5kKCcuaW52YWxpZC1mZWVkYmFjay1jb250YWluZXInKTtcbiAgICBjb25zdCAkZXJyb3JQb3BvdmVyID0gJGZvcm1Hcm91cC5maW5kKCcuZm9ybS1wb3BvdmVyLWVycm9yJyk7XG5cbiAgICBjb25zdCBsb2NhbGVWaXNpYmxlRWxlbWVudFdpZHRoID0gJGludmFsaWRGZWVkYmFja0NvbnRhaW5lci53aWR0aCgpO1xuXG4gICAgJGVycm9yUG9wb3Zlci5jc3MoJ3dpZHRoJywgbG9jYWxlVmlzaWJsZUVsZW1lbnRXaWR0aCk7XG5cbiAgICBjb25zdCBob3Jpem9udGFsRGlmZmVyZW5jZSA9IGdldEhvcml6b250YWxEaWZmZXJlbmNlKCRpbnZhbGlkRmVlZGJhY2tDb250YWluZXIsICRlcnJvclBvcG92ZXIpO1xuXG4gICAgJGVycm9yUG9wb3Zlci5jc3MoJ2xlZnQnLCBgJHtob3Jpem9udGFsRGlmZmVyZW5jZX1weGApO1xuICB9O1xuXG4gIC8qKlxuICAgKiBnZXRzIGhvcml6b250YWwgZGlmZmVyZW5jZSB3aGljaCBoZWxwcyB0byBhbGlnbiBwb3BvdmVyIGhvcml6b250YWxseS5cbiAgICogQHBhcmFtIHtqUXVlcnl9ICRpbnZhbGlkRmVlZGJhY2tDb250YWluZXJcbiAgICogQHBhcmFtIHtqUXVlcnl9ICRlcnJvclBvcG92ZXJcbiAgICogQHJldHVybnMge251bWJlcn1cbiAgICovXG4gIGNvbnN0IGdldEhvcml6b250YWxEaWZmZXJlbmNlID0gKCRpbnZhbGlkRmVlZGJhY2tDb250YWluZXIsICRlcnJvclBvcG92ZXIpID0+IHtcbiAgICBjb25zdCBpbnB1dEhvcml6b250YWxQb3NpdGlvbiA9ICRpbnZhbGlkRmVlZGJhY2tDb250YWluZXIub2Zmc2V0KCkubGVmdDtcbiAgICBjb25zdCBwb3BvdmVySG9yaXpvbnRhbFBvc2l0aW9uID0gJGVycm9yUG9wb3Zlci5vZmZzZXQoKS5sZWZ0O1xuXG4gICAgcmV0dXJuIGlucHV0SG9yaXpvbnRhbFBvc2l0aW9uIC0gcG9wb3Zlckhvcml6b250YWxQb3NpdGlvbjtcbiAgfTtcblxuICAvKipcbiAgICogR2V0cyBwb3BvdmVyIGVycm9yIGNvbnRlbnQgcHJlLWZldGNoZWQgaW4gaHRtbC4gSXQgdXNlZCB1bmlxdWUgc2VsZWN0b3IgdG8gaWRlbnRpZnkgd2hpY2ggb25lIGNvbnRlbnQgdG8gcmVuZGVyLlxuICAgKlxuICAgKiBAcGFyYW0gcG9wb3ZlclRyaWdnZXJFbGVtZW50XG4gICAqIEByZXR1cm5zIHtqUXVlcnl9XG4gICAqL1xuICBjb25zdCBnZXRFcnJvckNvbnRlbnQgPSAocG9wb3ZlclRyaWdnZXJFbGVtZW50KSA9PiB7XG4gICAgY29uc3QgcG9wb3ZlclRyaWdnZXJJZCA9ICQocG9wb3ZlclRyaWdnZXJFbGVtZW50KS5kYXRhKCdpZCcpO1xuXG4gICAgcmV0dXJuICQoYC5qcy1wb3BvdmVyLWVycm9yLWNvbnRlbnRbZGF0YS1pZD1cIiR7cG9wb3ZlclRyaWdnZXJJZH1cIl1gKS5odG1sKCk7XG4gIH07XG5cbiAgLy8gcmVnaXN0ZXJzIHRoZSBldmVudCB3aGljaCBkaXNwbGF5cyB0aGUgcG9wb3ZlclxuICAkKGRvY3VtZW50KS5vbignc2hvd24uYnMucG9wb3ZlcicsICdbZGF0YS10b2dnbGU9XCJmb3JtLXBvcG92ZXItZXJyb3JcIl0nLCAoZXZlbnQpID0+IHJlcG9zaXRpb25Qb3BvdmVyKGV2ZW50KSk7XG59KTtcbiJdLCJzb3VyY2VSb290IjoiIn0=