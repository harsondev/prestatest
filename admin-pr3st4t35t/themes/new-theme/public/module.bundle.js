/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./js/components/modal.js":
/*!********************************!*\
  !*** ./js/components/modal.js ***!
  \********************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

"use strict";


Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var _toConsumableArray2 = __webpack_require__(/*! babel-runtime/helpers/toConsumableArray */ "./node_modules/babel-runtime/helpers/toConsumableArray.js");

var _toConsumableArray3 = _interopRequireDefault(_toConsumableArray2);

exports.default = ConfirmModal;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 */

var _window = window,
    $ = _window.$;

/**
 * ConfirmModal component
 *
 * @param {String} id
 * @param {String} confirmTitle
 * @param {String} confirmMessage
 * @param {String} closeButtonLabel
 * @param {String} confirmButtonLabel
 * @param {String} confirmButtonClass
 * @param {Array} customButtons
 * @param {Boolean} closable
 * @param {Function} confirmCallback
 * @param {Function} cancelCallback
 *
 */

function ConfirmModal(params, confirmCallback, cancelCallback) {
  var _this = this;

  // Construct the modal
  var id = params.id,
      closable = params.closable;

  this.modal = Modal(params);

  // jQuery modal object
  this.$modal = $(this.modal.container);

  this.show = function () {
    _this.$modal.modal();
  };

  this.modal.confirmButton.addEventListener('click', confirmCallback);

  this.$modal.modal({
    backdrop: closable ? true : 'static',
    keyboard: closable !== undefined ? closable : true,
    closable: closable !== undefined ? closable : true,
    show: false
  });

  this.$modal.on('hidden.bs.modal', function () {
    document.querySelector('#' + id).remove();
    if (cancelCallback) {
      cancelCallback();
    }
  });

  document.body.appendChild(this.modal.container);
}

/**
 * Modal component to improve lisibility by constructing the modal outside the main function
 *
 * @param {Object} params
 *
 */
function Modal(_ref) {
  var _modal$footer;

  var _ref$id = _ref.id,
      id = _ref$id === undefined ? 'confirm-modal' : _ref$id,
      confirmTitle = _ref.confirmTitle,
      _ref$confirmMessage = _ref.confirmMessage,
      confirmMessage = _ref$confirmMessage === undefined ? '' : _ref$confirmMessage,
      _ref$closeButtonLabel = _ref.closeButtonLabel,
      closeButtonLabel = _ref$closeButtonLabel === undefined ? 'Close' : _ref$closeButtonLabel,
      _ref$confirmButtonLab = _ref.confirmButtonLabel,
      confirmButtonLabel = _ref$confirmButtonLab === undefined ? 'Accept' : _ref$confirmButtonLab,
      _ref$confirmButtonCla = _ref.confirmButtonClass,
      confirmButtonClass = _ref$confirmButtonCla === undefined ? 'btn-primary' : _ref$confirmButtonCla,
      _ref$customButtons = _ref.customButtons,
      customButtons = _ref$customButtons === undefined ? [] : _ref$customButtons;

  var modal = {};

  // Main modal element
  modal.container = document.createElement('div');
  modal.container.classList.add('modal', 'fade');
  modal.container.id = id;

  // Modal dialog element
  modal.dialog = document.createElement('div');
  modal.dialog.classList.add('modal-dialog');

  // Modal content element
  modal.content = document.createElement('div');
  modal.content.classList.add('modal-content');

  // Modal header element
  modal.header = document.createElement('div');
  modal.header.classList.add('modal-header');

  // Modal title element
  if (confirmTitle) {
    modal.title = document.createElement('h4');
    modal.title.classList.add('modal-title');
    modal.title.innerHTML = confirmTitle;
  }

  // Modal close button icon
  modal.closeIcon = document.createElement('button');
  modal.closeIcon.classList.add('close');
  modal.closeIcon.setAttribute('type', 'button');
  modal.closeIcon.dataset.dismiss = 'modal';
  modal.closeIcon.innerHTML = '×';

  // Modal body element
  modal.body = document.createElement('div');
  modal.body.classList.add('modal-body', 'text-left', 'font-weight-normal');

  // Modal message element
  modal.message = document.createElement('p');
  modal.message.classList.add('confirm-message');
  modal.message.innerHTML = confirmMessage;

  // Modal footer element
  modal.footer = document.createElement('div');
  modal.footer.classList.add('modal-footer');

  // Modal close button element
  modal.closeButton = document.createElement('button');
  modal.closeButton.setAttribute('type', 'button');
  modal.closeButton.classList.add('btn', 'btn-outline-secondary', 'btn-lg');
  modal.closeButton.dataset.dismiss = 'modal';
  modal.closeButton.innerHTML = closeButtonLabel;

  // Modal confirm button element
  modal.confirmButton = document.createElement('button');
  modal.confirmButton.setAttribute('type', 'button');
  modal.confirmButton.classList.add('btn', confirmButtonClass, 'btn-lg', 'btn-confirm-submit');
  modal.confirmButton.dataset.dismiss = 'modal';
  modal.confirmButton.innerHTML = confirmButtonLabel;

  // Constructing the modal
  if (confirmTitle) {
    modal.header.append(modal.title, modal.closeIcon);
  } else {
    modal.header.appendChild(modal.closeIcon);
  }

  modal.body.appendChild(modal.message);
  (_modal$footer = modal.footer).append.apply(_modal$footer, [modal.closeButton].concat((0, _toConsumableArray3.default)(customButtons), [modal.confirmButton]));
  modal.content.append(modal.header, modal.body, modal.footer);
  modal.dialog.appendChild(modal.content);
  modal.container.appendChild(modal.dialog);

  return modal;
}

/***/ }),

/***/ "./js/components/module-card.js":
/*!**************************************!*\
  !*** ./js/components/module-card.js ***!
  \**************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

"use strict";
/* provided dependency */ var jQuery = __webpack_require__(/*! jquery */ "jquery");


Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var _keys = __webpack_require__(/*! babel-runtime/core-js/object/keys */ "./node_modules/babel-runtime/core-js/object/keys.js");

var _keys2 = _interopRequireDefault(_keys);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _modal = __webpack_require__(/*! @components/modal */ "./js/components/modal.js");

var _modal2 = _interopRequireDefault(_modal);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _window = window,
    $ = _window.$; /**
                    * Copyright since 2007 PrestaShop SA and Contributors
                    * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
                    *
                    * NOTICE OF LICENSE
                    *
                    * This source file is subject to the Open Software License (OSL 3.0)
                    * that is bundled with this package in the file LICENSE.md.
                    * It is also available through the world-wide-web at this URL:
                    * https://opensource.org/licenses/OSL-3.0
                    * If you did not receive a copy of the license and are unable to
                    * obtain it through the world-wide-web, please send an email
                    * to license@prestashop.com so we can send you a copy immediately.
                    *
                    * DISCLAIMER
                    *
                    * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
                    * versions in the future. If you wish to customize PrestaShop for your
                    * needs please refer to https://devdocs.prestashop.com/ for more information.
                    *
                    * @author    PrestaShop SA and Contributors <contact@prestashop.com>
                    * @copyright Since 2007 PrestaShop SA and Contributors
                    * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
                    */

var BOEvent = {
  on: function on(eventName, callback, context) {
    document.addEventListener(eventName, function (event) {
      if (typeof context !== 'undefined') {
        callback.call(context, event);
      } else {
        callback(event);
      }
    });
  },
  emitEvent: function emitEvent(eventName, eventType) {
    var event = document.createEvent(eventType);
    // true values stand for: can bubble, and is cancellable
    event.initEvent(eventName, true, true);
    document.dispatchEvent(event);
  }
};

/**
 * Class is responsible for handling Module Card behavior
 *
 * This is a port of admin-dev/themes/default/js/bundle/module/module_card.js
 */

var ModuleCard = function () {
  function ModuleCard() {
    (0, _classCallCheck3.default)(this, ModuleCard);

    /* Selectors for module action links (uninstall, reset, etc...) to add a confirm popin */
    this.moduleActionMenuLinkSelector = 'button.module_action_menu_';
    this.moduleActionMenuInstallLinkSelector = 'button.module_action_menu_install';
    this.moduleActionMenuEnableLinkSelector = 'button.module_action_menu_enable';
    this.moduleActionMenuUninstallLinkSelector = 'button.module_action_menu_uninstall';
    this.moduleActionMenuDisableLinkSelector = 'button.module_action_menu_disable';
    this.moduleActionMenuEnableMobileLinkSelector = 'button.module_action_menu_enable_mobile';
    this.moduleActionMenuDisableMobileLinkSelector = 'button.module_action_menu_disable_mobile';
    this.moduleActionMenuResetLinkSelector = 'button.module_action_menu_reset';
    this.moduleActionMenuUpdateLinkSelector = 'button.module_action_menu_upgrade';
    this.moduleItemListSelector = '.module-item-list';
    this.moduleItemGridSelector = '.module-item-grid';
    this.moduleItemActionsSelector = '.module-actions';

    /* Selectors only for modal buttons */
    this.moduleActionModalDisableLinkSelector = 'a.module_action_modal_disable';
    this.moduleActionModalResetLinkSelector = 'a.module_action_modal_reset';
    this.moduleActionModalUninstallLinkSelector = 'a.module_action_modal_uninstall';
    this.forceDeletionOption = '#force_deletion';

    this.initActionButtons();
  }

  (0, _createClass3.default)(ModuleCard, [{
    key: 'initActionButtons',
    value: function initActionButtons() {
      var self = this;

      $(document).on('click', this.forceDeletionOption, function () {
        var btn = $(self.moduleActionModalUninstallLinkSelector, $('div.module-item-list[data-tech-name=\'' + $(this).attr('data-tech-name') + '\']'));

        if ($(this).prop('checked') === true) {
          btn.attr('data-deletion', 'true');
        } else {
          btn.removeAttr('data-deletion');
        }
      });

      $(document).on('click', this.moduleActionMenuInstallLinkSelector, function () {
        if ($('#modal-prestatrust').length) {
          $('#modal-prestatrust').modal('hide');
        }

        return self.dispatchPreEvent('install', this) && self.confirmAction('install', this) && self.requestToController('install', $(this));
      });

      $(document).on('click', this.moduleActionMenuEnableLinkSelector, function () {
        return self.dispatchPreEvent('enable', this) && self.confirmAction('enable', this) && self.requestToController('enable', $(this));
      });

      $(document).on('click', this.moduleActionMenuUninstallLinkSelector, function () {
        return self.dispatchPreEvent('uninstall', this) && self.confirmAction('uninstall', this) && self.requestToController('uninstall', $(this));
      });

      $(document).on('click', this.moduleActionMenuDisableLinkSelector, function () {
        return self.dispatchPreEvent('disable', this) && self.confirmAction('disable', this) && self.requestToController('disable', $(this));
      });

      $(document).on('click', this.moduleActionMenuEnableMobileLinkSelector, function () {
        return self.dispatchPreEvent('enable_mobile', this) && self.confirmAction('enable_mobile', this) && self.requestToController('enable_mobile', $(this));
      });

      $(document).on('click', this.moduleActionMenuDisableMobileLinkSelector, function () {
        return self.dispatchPreEvent('disable_mobile', this) && self.confirmAction('disable_mobile', this) && self.requestToController('disable_mobile', $(this));
      });

      $(document).on('click', this.moduleActionMenuResetLinkSelector, function () {
        return self.dispatchPreEvent('reset', this) && self.confirmAction('reset', this) && self.requestToController('reset', $(this));
      });

      $(document).on('click', this.moduleActionMenuUpdateLinkSelector, function (event) {
        var _this = this;

        event.preventDefault();
        var modal = $('#' + $(this).data('confirm_modal'));
        var isMaintenanceMode = window.isShopMaintenance;

        if (modal.length !== 1) {
          // Modal body element
          var maintenanceLink = document.createElement('a');
          maintenanceLink.classList.add('btn', 'btn-primary', 'btn-lg');
          maintenanceLink.setAttribute('href', window.moduleURLs.maintenancePage);
          maintenanceLink.innerHTML = window.moduleTranslations.moduleModalUpdateMaintenance;

          var updateConfirmModal = new _modal2.default({
            id: 'confirm-module-update-modal',
            confirmTitle: window.moduleTranslations.singleModuleModalUpdateTitle,
            closeButtonLabel: window.moduleTranslations.moduleModalUpdateCancel,
            confirmButtonLabel: isMaintenanceMode ? window.moduleTranslations.moduleModalUpdateUpgrade : window.moduleTranslations.upgradeAnywayButtonText,
            confirmButtonClass: isMaintenanceMode ? 'btn-primary' : 'btn-secondary',
            confirmMessage: isMaintenanceMode ? '' : window.moduleTranslations.moduleModalUpdateConfirmMessage,
            closable: true,
            customButtons: isMaintenanceMode ? [] : [maintenanceLink]
          }, function () {
            return self.dispatchPreEvent('update', _this) && self.confirmAction('update', _this) && self.requestToController('update', $(_this));
          });

          updateConfirmModal.show();
        } else {
          return self.dispatchPreEvent('update', this) && self.confirmAction('update', this) && self.requestToController('update', $(this));
        }

        return false;
      });

      $(document).on('click', this.moduleActionModalDisableLinkSelector, function () {
        return self.requestToController('disable', $(self.moduleActionMenuDisableLinkSelector, $('div.module-item-list[data-tech-name=\'' + $(this).attr('data-tech-name') + '\']')));
      });

      $(document).on('click', this.moduleActionModalResetLinkSelector, function () {
        return self.requestToController('reset', $(self.moduleActionMenuResetLinkSelector, $('div.module-item-list[data-tech-name=\'' + $(this).attr('data-tech-name') + '\']')));
      });

      $(document).on('click', this.moduleActionModalUninstallLinkSelector, function (e) {
        $(e.target).parents('.modal').on('hidden.bs.modal', function () {
          return self.requestToController('uninstall', $(self.moduleActionMenuUninstallLinkSelector, $('div.module-item-list[data-tech-name=\'' + $(e.target).attr('data-tech-name') + '\']')), $(e.target).attr('data-deletion'));
        });
      });
    }
  }, {
    key: 'getModuleItemSelector',
    value: function getModuleItemSelector() {
      if ($(this.moduleItemListSelector).length) {
        return this.moduleItemListSelector;
      }

      return this.moduleItemGridSelector;
    }
  }, {
    key: 'confirmAction',
    value: function confirmAction(action, element) {
      var modal = $('#' + $(element).data('confirm_modal'));

      if (modal.length !== 1) {
        return true;
      }

      modal.first().modal('show');

      return false; // do not allow a.href to reload the page. The confirm modal dialog will do it async if needed.
    }

    /**
     * Update the content of a modal asking a confirmation for PrestaTrust and open it
     *
     * @param {array} result containing module data
     * @return {void}
     */

  }, {
    key: 'confirmPrestaTrust',
    value: function confirmPrestaTrust(result) {
      var that = this;
      var modal = this.replacePrestaTrustPlaceholders(result);

      modal.find('.pstrust-install').off('click').on('click', function () {
        // Find related form, update it and submit it
        var installButton = $(that.moduleActionMenuInstallLinkSelector, '.module-item[data-tech-name="' + result.module.attributes.name + '"]');

        var form = installButton.parent('form');
        $('<input>').attr({
          type: 'hidden',
          value: '1',
          name: 'actionParams[confirmPrestaTrust]'
        }).appendTo(form);

        installButton.click();
        modal.modal('hide');
      });

      modal.modal();
    }
  }, {
    key: 'replacePrestaTrustPlaceholders',
    value: function replacePrestaTrustPlaceholders(result) {
      var modal = $('#modal-prestatrust');
      var module = result.module.attributes;

      if (result.confirmation_subject !== 'PrestaTrust' || !modal.length) {
        return false;
      }

      var alertClass = module.prestatrust.status ? 'success' : 'warning';

      if (module.prestatrust.check_list.property) {
        modal.find('#pstrust-btn-property-ok').show();
        modal.find('#pstrust-btn-property-nok').hide();
      } else {
        modal.find('#pstrust-btn-property-ok').hide();
        modal.find('#pstrust-btn-property-nok').show();
        modal.find('#pstrust-buy').attr('href', module.url).toggle(module.url !== null);
      }

      modal.find('#pstrust-img').attr({ src: module.img, alt: module.name });
      modal.find('#pstrust-name').text(module.displayName);
      modal.find('#pstrust-author').text(module.author);
      modal.find('#pstrust-label').attr('class', 'text-' + alertClass).text(module.prestatrust.status ? 'OK' : 'KO');
      modal.find('#pstrust-message').attr('class', 'alert alert-' + alertClass);
      modal.find('#pstrust-message > p').text(module.prestatrust.message);

      return modal;
    }
  }, {
    key: 'dispatchPreEvent',
    value: function dispatchPreEvent(action, element) {
      var event = jQuery.Event('module_card_action_event');

      $(element).trigger(event, [action]);
      if (event.isPropagationStopped() !== false || event.isImmediatePropagationStopped() !== false) {
        return false; // if all handlers have not been called, then stop propagation of the click event.
      }

      return event.result !== false; // explicit false must be set from handlers to stop propagation of the click event.
    }
  }, {
    key: 'requestToController',
    value: function requestToController(action, element, forceDeletion, disableCacheClear, callback) {
      var self = this;
      var jqElementObj = element.closest(this.moduleItemActionsSelector);
      var form = element.closest('form');
      var spinnerObj = $('<button class="btn-primary-reverse onclick unbind spinner "></button>');
      var url = '//' + window.location.host + form.attr('action');
      var actionParams = form.serializeArray();

      if (forceDeletion === 'true' || forceDeletion === true) {
        actionParams.push({ name: 'actionParams[deletion]', value: true });
      }
      if (disableCacheClear === 'true' || disableCacheClear === true) {
        actionParams.push({ name: 'actionParams[cacheClearEnabled]', value: 0 });
      }

      $.ajax({
        url: url,
        dataType: 'json',
        method: 'POST',
        data: actionParams,
        beforeSend: function beforeSend() {
          jqElementObj.hide();
          jqElementObj.after(spinnerObj);
        }
      }).done(function (result) {
        if (result === undefined) {
          $.growl.error({
            message: 'No answer received from server',
            fixed: true
          });
          return;
        }

        if (typeof result.status !== 'undefined' && result.status === false) {
          $.growl.error({ message: result.msg, fixed: true });
          return;
        }

        var moduleTechName = (0, _keys2.default)(result)[0];

        if (result[moduleTechName].status === false) {
          if (typeof result[moduleTechName].confirmation_subject !== 'undefined') {
            self.confirmPrestaTrust(result[moduleTechName]);
          }

          $.growl.error({ message: result[moduleTechName].msg, fixed: true });
          return;
        }

        $.growl({
          message: result[moduleTechName].msg,
          duration: 6000
        });

        var alteredSelector = self.getModuleItemSelector().replace('.', '');
        var mainElement = null;

        if (action === 'uninstall') {
          mainElement = jqElementObj.closest('.' + alteredSelector);
          mainElement.remove();

          BOEvent.emitEvent('Module Uninstalled', 'CustomEvent');
        } else if (action === 'disable') {
          mainElement = jqElementObj.closest('.' + alteredSelector);
          mainElement.addClass(alteredSelector + '-isNotActive');
          mainElement.attr('data-active', '0');

          BOEvent.emitEvent('Module Disabled', 'CustomEvent');
        } else if (action === 'enable') {
          mainElement = jqElementObj.closest('.' + alteredSelector);
          mainElement.removeClass(alteredSelector + '-isNotActive');
          mainElement.attr('data-active', '1');

          BOEvent.emitEvent('Module Enabled', 'CustomEvent');
        }

        jqElementObj.replaceWith(result[moduleTechName].action_menu_html);
      }).fail(function () {
        var moduleItem = jqElementObj.closest('module-item-list');
        var techName = moduleItem.data('techName');
        $.growl.error({
          message: 'Could not perform action ' + action + ' for module ' + techName,
          fixed: true
        });
      }).always(function () {
        jqElementObj.fadeIn();
        spinnerObj.remove();
        if (callback) {
          callback();
        }
      });

      return false;
    }
  }]);
  return ModuleCard;
}();

exports.default = ModuleCard;

/***/ }),

/***/ "./js/pages/module/controller.js":
/*!***************************************!*\
  !*** ./js/pages/module/controller.js ***!
  \***************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

"use strict";


Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var _keys = __webpack_require__(/*! babel-runtime/core-js/object/keys */ "./node_modules/babel-runtime/core-js/object/keys.js");

var _keys2 = _interopRequireDefault(_keys);

var _isNan = __webpack_require__(/*! babel-runtime/core-js/number/is-nan */ "./node_modules/babel-runtime/core-js/number/is-nan.js");

var _isNan2 = _interopRequireDefault(_isNan);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _modal = __webpack_require__(/*! @components/modal */ "./js/components/modal.js");

var _modal2 = _interopRequireDefault(_modal);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _window = window,
    $ = _window.$;

/**
 * Module Admin Page Controller.
 * @constructor
 */
/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 */

var AdminModuleController = function () {
  /**
   * Initialize all listeners and bind everything
   * @method init
   * @memberof AdminModule
   */
  function AdminModuleController(moduleCardController) {
    (0, _classCallCheck3.default)(this, AdminModuleController);

    this.moduleCardController = moduleCardController;

    this.DEFAULT_MAX_RECENTLY_USED = 10;
    this.DEFAULT_MAX_PER_CATEGORIES = 6;
    this.DISPLAY_GRID = 'grid';
    this.DISPLAY_LIST = 'list';
    this.CATEGORY_RECENTLY_USED = 'recently-used';

    this.currentCategoryDisplay = {};
    this.currentDisplay = '';
    this.isCategoryGridDisplayed = false;
    this.currentTagsList = [];
    this.currentRefCategory = null;
    this.currentRefStatus = null;
    this.currentSorting = null;
    this.baseAddonsUrl = 'https://addons.prestashop.com/';
    this.pstaggerInput = null;
    this.lastBulkAction = null;
    this.isUploadStarted = false;

    this.recentlyUsedSelector = '#module-recently-used-list .modules-list';

    /**
     * Loaded modules list.
     * Containing the card and list display.
     * @type {Array}
     */
    this.modulesList = [];
    this.addonsCardGrid = null;
    this.addonsCardList = null;

    this.moduleShortList = '.module-short-list';
    // See more & See less selector
    this.seeMoreSelector = '.see-more';
    this.seeLessSelector = '.see-less';

    // Selectors into vars to make it easier to change them while keeping same code logic
    this.moduleItemGridSelector = '.module-item-grid';
    this.moduleItemListSelector = '.module-item-list';
    this.categorySelectorLabelSelector = '.module-category-selector-label';
    this.categorySelector = '.module-category-selector';
    this.categoryItemSelector = '.module-category-menu';
    this.addonsLoginButtonSelector = '#addons_login_btn';
    this.categoryResetBtnSelector = '.module-category-reset';
    this.moduleInstallBtnSelector = 'input.module-install-btn';
    this.moduleSortingDropdownSelector = '.module-sorting-author select';
    this.categoryGridSelector = '#modules-categories-grid';
    this.categoryGridItemSelector = '.module-category-item';
    this.addonItemGridSelector = '.module-addons-item-grid';
    this.addonItemListSelector = '.module-addons-item-list';

    // Upgrade All selectors
    this.upgradeAllSource = '.module_action_menu_upgrade_all';
    this.upgradeContainer = '#modules-list-container-update';
    this.upgradeAllTargets = this.upgradeContainer + ' .module_action_menu_upgrade:visible';

    // Notification selectors
    this.notificationContainer = '#modules-list-container-notification';

    // Bulk action selectors
    this.bulkActionDropDownSelector = '.module-bulk-actions';
    this.bulkItemSelector = '.module-bulk-menu';
    this.bulkActionCheckboxListSelector = '.module-checkbox-bulk-list input';
    this.bulkActionCheckboxGridSelector = '.module-checkbox-bulk-grid input';
    this.checkedBulkActionListSelector = this.bulkActionCheckboxListSelector + ':checked';
    this.checkedBulkActionGridSelector = this.bulkActionCheckboxGridSelector + ':checked';
    this.bulkActionCheckboxSelector = '#module-modal-bulk-checkbox';
    this.bulkConfirmModalSelector = '#module-modal-bulk-confirm';
    this.bulkConfirmModalActionNameSelector = '#module-modal-bulk-confirm-action-name';
    this.bulkConfirmModalListSelector = '#module-modal-bulk-confirm-list';
    this.bulkConfirmModalAckBtnSelector = '#module-modal-confirm-bulk-ack';

    // Placeholders
    this.placeholderGlobalSelector = '.module-placeholders-wrapper';
    this.placeholderFailureGlobalSelector = '.module-placeholders-failure';
    this.placeholderFailureMsgSelector = '.module-placeholders-failure-msg';
    this.placeholderFailureRetryBtnSelector = '#module-placeholders-failure-retry';

    // Module's statuses selectors
    this.statusSelectorLabelSelector = '.module-status-selector-label';
    this.statusItemSelector = '.module-status-menu';
    this.statusResetBtnSelector = '.module-status-reset';

    // Selectors for Module Import and Addons connect
    this.addonsConnectModalBtnSelector = '#page-header-desc-configuration-addons_connect';
    this.addonsLogoutModalBtnSelector = '#page-header-desc-configuration-addons_logout';
    this.addonsImportModalBtnSelector = '#page-header-desc-configuration-add_module';
    this.dropZoneModalSelector = '#module-modal-import';
    this.dropZoneModalFooterSelector = '#module-modal-import .modal-footer';
    this.dropZoneImportZoneSelector = '#importDropzone';
    this.addonsConnectModalSelector = '#module-modal-addons-connect';
    this.addonsLogoutModalSelector = '#module-modal-addons-logout';
    this.addonsConnectForm = '#addons-connect-form';
    this.moduleImportModalCloseBtn = '#module-modal-import-closing-cross';
    this.moduleImportStartSelector = '.module-import-start';
    this.moduleImportProcessingSelector = '.module-import-processing';
    this.moduleImportSuccessSelector = '.module-import-success';
    this.moduleImportSuccessConfigureBtnSelector = '.module-import-success-configure';
    this.moduleImportFailureSelector = '.module-import-failure';
    this.moduleImportFailureRetrySelector = '.module-import-failure-retry';
    this.moduleImportFailureDetailsBtnSelector = '.module-import-failure-details-action';
    this.moduleImportSelectFileManualSelector = '.module-import-start-select-manual';
    this.moduleImportFailureMsgDetailsSelector = '.module-import-failure-details';
    this.moduleImportConfirmSelector = '.module-import-confirm';

    this.initSortingDropdown();
    this.initBOEventRegistering();
    this.initCurrentDisplay();
    this.initSortingDisplaySwitch();
    this.initBulkDropdown();
    this.initSearchBlock();
    this.initCategorySelect();
    this.initCategoriesGrid();
    this.initActionButtons();
    this.initAddonsSearch();
    this.initAddonsConnect();
    this.initAddModuleAction();
    this.initDropzone();
    this.initPageChangeProtection();
    this.initPlaceholderMechanism();
    this.initFilterStatusDropdown();
    this.fetchModulesList();
    this.getNotificationsCount();
    this.initializeSeeMore();
  }

  (0, _createClass3.default)(AdminModuleController, [{
    key: 'initFilterStatusDropdown',
    value: function initFilterStatusDropdown() {
      var self = this;
      var body = $('body');
      body.on('click', self.statusItemSelector, function () {
        // Get data from li DOM input
        self.currentRefStatus = parseInt($(this).data('status-ref'), 10);
        // Change dropdown label to set it to the current status' displayname
        $(self.statusSelectorLabelSelector).text($(this).text());
        $(self.statusResetBtnSelector).show();
        self.updateModuleVisibility();
      });

      body.on('click', self.statusResetBtnSelector, function () {
        $(self.statusSelectorLabelSelector).text($(this).text());
        $(this).hide();
        self.currentRefStatus = null;
        self.updateModuleVisibility();
      });
    }
  }, {
    key: 'initBulkDropdown',
    value: function initBulkDropdown() {
      var self = this;
      var body = $('body');

      body.on('click', self.getBulkCheckboxesSelector(), function () {
        var selector = $(self.bulkActionDropDownSelector);

        if ($(self.getBulkCheckboxesCheckedSelector()).length > 0) {
          selector.closest('.module-top-menu-item').removeClass('disabled');
        } else {
          selector.closest('.module-top-menu-item').addClass('disabled');
        }
      });

      body.on('click', self.bulkItemSelector, function initializeBodyChange() {
        if ($(self.getBulkCheckboxesCheckedSelector()).length === 0) {
          $.growl.warning({
            message: window.translate_javascripts['Bulk Action - One module minimum']
          });
          return;
        }

        self.lastBulkAction = $(this).data('ref');
        var modulesListString = self.buildBulkActionModuleList();
        var actionString = $(this).find(':checked').text().toLowerCase();
        $(self.bulkConfirmModalListSelector).html(modulesListString);
        $(self.bulkConfirmModalActionNameSelector).text(actionString);

        if (self.lastBulkAction === 'bulk-uninstall') {
          $(self.bulkActionCheckboxSelector).show();
        } else {
          $(self.bulkActionCheckboxSelector).hide();
        }

        $(self.bulkConfirmModalSelector).modal('show');
      });

      body.on('click', this.bulkConfirmModalAckBtnSelector, function (event) {
        event.preventDefault();
        event.stopPropagation();
        $(self.bulkConfirmModalSelector).modal('hide');
        self.doBulkAction(self.lastBulkAction);
      });
    }
  }, {
    key: 'initBOEventRegistering',
    value: function initBOEventRegistering() {
      window.BOEvent.on('Module Disabled', this.onModuleDisabled, this);
      window.BOEvent.on('Module Uninstalled', this.updateTotalResults, this);
    }
  }, {
    key: 'onModuleDisabled',
    value: function onModuleDisabled() {
      var self = this;
      self.getModuleItemSelector();

      $('.modules-list').each(function () {
        self.updateTotalResults();
      });
    }
  }, {
    key: 'initPlaceholderMechanism',
    value: function initPlaceholderMechanism() {
      var self = this;

      if ($(self.placeholderGlobalSelector).length) {
        self.ajaxLoadPage();
      }

      // Retry loading mechanism
      $('body').on('click', self.placeholderFailureRetryBtnSelector, function () {
        $(self.placeholderFailureGlobalSelector).fadeOut();
        $(self.placeholderGlobalSelector).fadeIn();
        self.ajaxLoadPage();
      });
    }
  }, {
    key: 'ajaxLoadPage',
    value: function ajaxLoadPage() {
      var self = this;

      $.ajax({
        method: 'GET',
        url: window.moduleURLs.catalogRefresh
      }).done(function (response) {
        if (response.status === true) {
          if (typeof response.domElements === 'undefined') response.domElements = null;
          if (typeof response.msg === 'undefined') response.msg = null;

          var stylesheet = document.styleSheets[0];
          var stylesheetRule = '{display: none}';
          var moduleGlobalSelector = '.modules-list';
          var moduleSortingSelector = '.module-sorting-menu';
          var requiredSelectorCombination = moduleGlobalSelector + ',' + moduleSortingSelector;

          if (stylesheet.insertRule) {
            stylesheet.insertRule(requiredSelectorCombination + stylesheetRule, stylesheet.cssRules.length);
          } else if (stylesheet.addRule) {
            stylesheet.addRule(requiredSelectorCombination, stylesheetRule, -1);
          }

          $(self.placeholderGlobalSelector).fadeOut(800, function () {
            $.each(response.domElements, function (index, element) {
              $(element.selector).append(element.content);
            });
            $(moduleGlobalSelector).fadeIn(800).css('display', 'flex');
            $(moduleSortingSelector).fadeIn(800);
            $('[data-toggle="popover"]').popover();
            self.initCurrentDisplay();
            self.fetchModulesList();
          });
        } else {
          $(self.placeholderGlobalSelector).fadeOut(800, function () {
            $(self.placeholderFailureMsgSelector).text(response.msg);
            $(self.placeholderFailureGlobalSelector).fadeIn(800);
          });
        }
      }).fail(function (response) {
        $(self.placeholderGlobalSelector).fadeOut(800, function () {
          $(self.placeholderFailureMsgSelector).text(response.statusText);
          $(self.placeholderFailureGlobalSelector).fadeIn(800);
        });
      });
    }
  }, {
    key: 'fetchModulesList',
    value: function fetchModulesList() {
      var self = this;
      var container = void 0;
      var $this = void 0;

      self.modulesList = [];
      $('.modules-list').each(function prepareContainer() {
        container = $(this);
        container.find('.module-item').each(function prepareModules() {
          $this = $(this);
          self.modulesList.push({
            domObject: $this,
            id: $this.data('id'),
            name: $this.data('name').toLowerCase(),
            scoring: parseFloat($this.data('scoring')),
            logo: $this.data('logo'),
            author: $this.data('author').toLowerCase(),
            version: $this.data('version'),
            description: $this.data('description').toLowerCase(),
            techName: $this.data('tech-name').toLowerCase(),
            childCategories: $this.data('child-categories'),
            categories: String($this.data('categories')).toLowerCase(),
            type: $this.data('type'),
            price: parseFloat($this.data('price')),
            active: parseInt($this.data('active'), 10),
            access: $this.data('last-access'),
            display: $this.hasClass('module-item-list') ? self.DISPLAY_LIST : self.DISPLAY_GRID,
            container: container
          });

          if (self.isModulesPage()) {
            $this.remove();
          }
        });
      });

      self.addonsCardGrid = $(this.addonItemGridSelector);
      self.addonsCardList = $(this.addonItemListSelector);
      self.updateModuleVisibility();
      $('body').trigger('moduleCatalogLoaded');
    }

    /**
     * Prepare sorting
     *
     */

  }, {
    key: 'updateModuleSorting',
    value: function updateModuleSorting() {
      var self = this;

      if (!self.currentSorting) {
        return;
      }

      // Modules sorting
      var order = 'asc';
      var key = self.currentSorting;
      var splittedKey = key.split('-');

      if (splittedKey.length > 1) {
        key = splittedKey[0];
        if (splittedKey[1] === 'desc') {
          order = 'desc';
        }
      }

      var currentCompare = function currentCompare(a, b) {
        var aData = a[key];
        var bData = b[key];

        if (key === 'access') {
          aData = new Date(aData).getTime();
          bData = new Date(bData).getTime();
          aData = (0, _isNan2.default)(aData) ? 0 : aData;
          bData = (0, _isNan2.default)(bData) ? 0 : bData;
          if (aData === bData) {
            return b.name.localeCompare(a.name);
          }
        }

        if (aData < bData) return -1;
        if (aData > bData) return 1;

        return 0;
      };

      self.modulesList.sort(currentCompare);
      if (order === 'desc') {
        self.modulesList.reverse();
      }
    }
  }, {
    key: 'updateModuleContainerDisplay',
    value: function updateModuleContainerDisplay() {
      var self = this;

      $('.module-short-list').each(function setShortListVisibility() {
        var container = $(this);
        var nbModulesInContainer = container.find('.module-item').length;

        if (self.currentRefCategory && self.currentRefCategory !== String(container.find('.modules-list').data('name')) || self.currentRefStatus !== null && nbModulesInContainer === 0 || nbModulesInContainer === 0 && String(container.find('.modules-list').data('name')) === self.CATEGORY_RECENTLY_USED || self.currentTagsList.length > 0 && nbModulesInContainer === 0) {
          container.hide();
          return;
        }

        container.show();
        container.find(self.seeMoreSelector + ', ' + self.seeLessSelector).toggle(nbModulesInContainer >= self.DEFAULT_MAX_PER_CATEGORIES);
      });
    }
  }, {
    key: 'updateModuleVisibility',
    value: function updateModuleVisibility() {
      var self = this;

      self.updateModuleSorting();

      if (self.isModulesPage()) {
        $(self.recentlyUsedSelector).find('.module-item').remove();
        $('.modules-list').find('.module-item').remove();
      }

      // Modules visibility management
      var isVisible = void 0;
      var currentModule = void 0;
      var moduleCategory = void 0;
      var tagExists = void 0;
      var newValue = void 0;
      var defaultMax = void 0;

      var modulesListLength = self.modulesList.length;
      var counter = {};
      var checkTag = function checkTag(index, value) {
        newValue = value.toLowerCase();
        tagExists |= currentModule.name.indexOf(newValue) !== -1 || currentModule.description.indexOf(newValue) !== -1 || currentModule.author.indexOf(newValue) !== -1 || currentModule.techName.indexOf(newValue) !== -1;
      };

      for (var i = 0; i < modulesListLength; i += 1) {
        currentModule = self.modulesList[i];
        if (currentModule.display === self.currentDisplay) {
          isVisible = true;

          moduleCategory = self.currentRefCategory === self.CATEGORY_RECENTLY_USED ? self.CATEGORY_RECENTLY_USED : currentModule.categories;

          // Check for same category
          if (self.currentRefCategory !== null) {
            isVisible &= moduleCategory === self.currentRefCategory;
          }

          // Check for same status
          if (self.currentRefStatus !== null) {
            isVisible &= currentModule.active === self.currentRefStatus;
          }

          // Check for tag list
          if (self.currentTagsList.length) {
            tagExists = false;
            $.each(self.currentTagsList, checkTag);
            isVisible &= tagExists;
          }

          /**
           * If list display without search we must display only the first 5 modules
           */
          if (self.currentDisplay === self.DISPLAY_LIST && !self.currentTagsList.length) {
            if (self.currentCategoryDisplay[moduleCategory] === undefined) {
              self.currentCategoryDisplay[moduleCategory] = false;
            }

            if (!counter[moduleCategory]) {
              counter[moduleCategory] = 0;
            }

            defaultMax = moduleCategory === self.CATEGORY_RECENTLY_USED ? self.DEFAULT_MAX_RECENTLY_USED : self.DEFAULT_MAX_PER_CATEGORIES;
            if (counter[moduleCategory] >= defaultMax) {
              isVisible &= self.currentCategoryDisplay[moduleCategory];
            }

            counter[moduleCategory] += 1;
          }

          // If visible, display (Thx captain obvious)
          if (isVisible) {
            if (self.currentRefCategory === self.CATEGORY_RECENTLY_USED) {
              $(self.recentlyUsedSelector).append(currentModule.domObject);
            } else {
              currentModule.container.append(currentModule.domObject);
            }
          }
        }
      }

      self.updateModuleContainerDisplay();

      if (self.currentTagsList.length) {
        $('.modules-list').append(this.currentDisplay === self.DISPLAY_GRID ? this.addonsCardGrid : this.addonsCardList);
      }

      self.updateTotalResults();
    }
  }, {
    key: 'initPageChangeProtection',
    value: function initPageChangeProtection() {
      var self = this;

      $(window).on('beforeunload', function () {
        if (self.isUploadStarted === true) {
          return 'It seems some critical operation are running, are you sure you want to change page? ' + 'It might cause some unexepcted behaviors.';
        }

        return undefined;
      });
    }
  }, {
    key: 'buildBulkActionModuleList',
    value: function buildBulkActionModuleList() {
      var checkBoxesSelector = this.getBulkCheckboxesCheckedSelector();
      var moduleItemSelector = this.getModuleItemSelector();
      var alreadyDoneFlag = 0;
      var htmlGenerated = '';
      var currentElement = void 0;

      $(checkBoxesSelector).each(function prepareCheckboxes() {
        if (alreadyDoneFlag === 10) {
          // Break each
          htmlGenerated += '- ...';
          return false;
        }

        currentElement = $(this).closest(moduleItemSelector);
        htmlGenerated += '- ' + currentElement.data('name') + '<br/>';
        alreadyDoneFlag += 1;

        return true;
      });

      return htmlGenerated;
    }
  }, {
    key: 'initAddonsConnect',
    value: function initAddonsConnect() {
      var self = this;

      // Make addons connect modal ready to be clicked
      if ($(self.addonsConnectModalBtnSelector).attr('href') === '#') {
        $(self.addonsConnectModalBtnSelector).attr('data-toggle', 'modal');
        $(self.addonsConnectModalBtnSelector).attr('data-target', self.addonsConnectModalSelector);
      }

      if ($(self.addonsLogoutModalBtnSelector).attr('href') === '#') {
        $(self.addonsLogoutModalBtnSelector).attr('data-toggle', 'modal');
        $(self.addonsLogoutModalBtnSelector).attr('data-target', self.addonsLogoutModalSelector);
      }

      $('body').on('submit', self.addonsConnectForm, function initializeBodySubmit(event) {
        event.preventDefault();
        event.stopPropagation();

        $.ajax({
          method: 'POST',
          url: $(this).attr('action'),
          dataType: 'json',
          data: $(this).serialize(),
          beforeSend: function beforeSend() {
            $(self.addonsLoginButtonSelector).show();
            $('button.btn[type="submit"]', self.addonsConnectForm).hide();
          }
        }).done(function (response) {
          if (response.success === 1) {
            window.location.reload();
          } else {
            $.growl.error({ message: response.message });
            $(self.addonsLoginButtonSelector).hide();
            $('button.btn[type="submit"]', self.addonsConnectForm).fadeIn();
          }
        });
      });
    }
  }, {
    key: 'initAddModuleAction',
    value: function initAddModuleAction() {
      var self = this;
      var addModuleButton = $(self.addonsImportModalBtnSelector);
      addModuleButton.attr('data-toggle', 'modal');
      addModuleButton.attr('data-target', self.dropZoneModalSelector);
    }
  }, {
    key: 'initDropzone',
    value: function initDropzone() {
      var self = this;
      var body = $('body');
      var dropzone = $('.dropzone');

      // Reset modal when click on Retry in case of failure
      body.on('click', this.moduleImportFailureRetrySelector, function () {
        /* eslint-disable max-len */
        $(self.moduleImportSuccessSelector + ',' + self.moduleImportFailureSelector + ',' + self.moduleImportProcessingSelector).fadeOut(function () {
          /**
           * Added timeout for a better render of animation
           * and avoid to have displayed at the same time
           */
          setTimeout(function () {
            $(self.moduleImportStartSelector).fadeIn(function () {
              $(self.moduleImportFailureMsgDetailsSelector).hide();
              $(self.moduleImportSuccessConfigureBtnSelector).hide();
              dropzone.removeAttr('style');
            });
          }, 550);
        });
        /* eslint-enable max-len */
      });

      // Reinit modal on exit, but check if not already processing something
      body.on('hidden.bs.modal', this.dropZoneModalSelector, function () {
        $(self.moduleImportSuccessSelector + ', ' + self.moduleImportFailureSelector).hide();
        $(self.moduleImportStartSelector).show();

        dropzone.removeAttr('style');
        $(self.moduleImportFailureMsgDetailsSelector).hide();
        $(self.moduleImportSuccessConfigureBtnSelector).hide();
        $(self.dropZoneModalFooterSelector).html('');
        $(self.moduleImportConfirmSelector).hide();
      });

      // Change the way Dropzone.js lib handle file input trigger
      body.on('click', '.dropzone:not(' + this.moduleImportSelectFileManualSelector + ', ' + this.moduleImportSuccessConfigureBtnSelector + ')', function (event, manualSelect) {
        // if click comes from .module-import-start-select-manual, stop everything
        if (typeof manualSelect === 'undefined') {
          event.stopPropagation();
          event.preventDefault();
        }
      });

      body.on('click', this.moduleImportSelectFileManualSelector, function (event) {
        event.stopPropagation();
        event.preventDefault();
        /**
         * Trigger click on hidden file input, and pass extra data
         * to .dropzone click handler fro it to notice it comes from here
         */
        $('.dz-hidden-input').trigger('click', ['manual_select']);
      });

      // Handle modal closure
      body.on('click', this.moduleImportModalCloseBtn, function () {
        if (self.isUploadStarted !== true) {
          $(self.dropZoneModalSelector).modal('hide');
        }
      });

      // Fix issue on click configure button
      body.on('click', this.moduleImportSuccessConfigureBtnSelector, function initializeBodyClickOnModuleImport(event) {
        event.stopPropagation();
        event.preventDefault();
        window.location = $(this).attr('href');
      });

      // Open failure message details box
      body.on('click', this.moduleImportFailureDetailsBtnSelector, function () {
        $(self.moduleImportFailureMsgDetailsSelector).slideDown();
      });

      // @see: dropzone.js
      var dropzoneOptions = {
        url: window.moduleURLs.moduleImport,
        acceptedFiles: '.zip, .tar',
        // The name that will be used to transfer the file
        paramName: 'file_uploaded',
        maxFilesize: 50, // can't be greater than 50Mb because it's an addons limitation
        uploadMultiple: false,
        addRemoveLinks: true,
        dictDefaultMessage: '',
        hiddenInputContainer: self.dropZoneImportZoneSelector,
        /**
         * Add unlimited timeout. Otherwise dropzone timeout is 30 seconds
         *  and if a module is long to install, it is not possible to install the module.
         */
        timeout: 0,
        addedfile: function addedfile() {
          self.animateStartUpload();
        },
        processing: function processing() {
          // Leave it empty since we don't require anything while processing upload
        },
        error: function error(file, message) {
          self.displayOnUploadError(message);
        },
        complete: function complete(file) {
          if (file.status !== 'error') {
            var responseObject = $.parseJSON(file.xhr.response);

            if (typeof responseObject.is_configurable === 'undefined') responseObject.is_configurable = null;
            if (typeof responseObject.module_name === 'undefined') responseObject.module_name = null;

            self.displayOnUploadDone(responseObject);
          }
          // State that we have finish the process to unlock some actions
          self.isUploadStarted = false;
        }
      };

      dropzone.dropzone($.extend(dropzoneOptions));
    }
  }, {
    key: 'animateStartUpload',
    value: function animateStartUpload() {
      var self = this;
      var dropzone = $('.dropzone');
      // State that we start module upload
      self.isUploadStarted = true;
      $(self.moduleImportStartSelector).hide(0);
      dropzone.css('border', 'none');
      $(self.moduleImportProcessingSelector).fadeIn();
    }
  }, {
    key: 'animateEndUpload',
    value: function animateEndUpload(callback) {
      var self = this;
      $(self.moduleImportProcessingSelector).finish().fadeOut(callback);
    }

    /**
     * Method to call for upload modal, when the ajax call went well.
     *
     * @param object result containing the server response
     */

  }, {
    key: 'displayOnUploadDone',
    value: function displayOnUploadDone(result) {
      var self = this;
      self.animateEndUpload(function () {
        if (result.status === true) {
          if (result.is_configurable === true) {
            var configureLink = window.moduleURLs.configurationPage.replace(/:number:/, result.module_name);
            $(self.moduleImportSuccessConfigureBtnSelector).attr('href', configureLink);
            $(self.moduleImportSuccessConfigureBtnSelector).show();
          }
          $(self.moduleImportSuccessSelector).fadeIn();
        } else if (typeof result.confirmation_subject !== 'undefined') {
          self.displayPrestaTrustStep(result);
        } else {
          $(self.moduleImportFailureMsgDetailsSelector).html(result.msg);
          $(self.moduleImportFailureSelector).fadeIn();
        }
      });
    }

    /**
     * Method to call for upload modal, when the ajax call went wrong or when the action requested could not
     * succeed for some reason.
     *
     * @param string message explaining the error.
     */

  }, {
    key: 'displayOnUploadError',
    value: function displayOnUploadError(message) {
      var self = this;
      self.animateEndUpload(function () {
        $(self.moduleImportFailureMsgDetailsSelector).html(message);
        $(self.moduleImportFailureSelector).fadeIn();
      });
    }

    /**
     * If PrestaTrust needs to be confirmed, we ask for the confirmation
     * modal content and we display it in the currently displayed one.
     * We also generate the ajax call to trigger once we confirm we want to install
     * the module.
     *
     * @param Previous server response result
     */

  }, {
    key: 'displayPrestaTrustStep',
    value: function displayPrestaTrustStep(result) {
      var self = this;
      var modal = self.moduleCardController.replacePrestaTrustPlaceholders(result);
      var moduleName = result.module.attributes.name;

      $(this.moduleImportConfirmSelector).html(modal.find('.modal-body').html()).fadeIn();
      $(this.dropZoneModalFooterSelector).html(modal.find('.modal-footer').html()).fadeIn();

      $(this.dropZoneModalFooterSelector).find('.pstrust-install').off('click').on('click', function () {
        $(self.moduleImportConfirmSelector).hide();
        $(self.dropZoneModalFooterSelector).html('');
        self.animateStartUpload();

        // Install ajax call
        $.post(result.module.attributes.urls.install, {
          'actionParams[confirmPrestaTrust]': '1'
        }).done(function (data) {
          self.displayOnUploadDone(data[moduleName]);
        }).fail(function (data) {
          self.displayOnUploadError(data[moduleName]);
        }).always(function () {
          self.isUploadStarted = false;
        });
      });
    }
  }, {
    key: 'getBulkCheckboxesSelector',
    value: function getBulkCheckboxesSelector() {
      return this.currentDisplay === this.DISPLAY_GRID ? this.bulkActionCheckboxGridSelector : this.bulkActionCheckboxListSelector;
    }
  }, {
    key: 'getBulkCheckboxesCheckedSelector',
    value: function getBulkCheckboxesCheckedSelector() {
      return this.currentDisplay === this.DISPLAY_GRID ? this.checkedBulkActionGridSelector : this.checkedBulkActionListSelector;
    }
  }, {
    key: 'getModuleItemSelector',
    value: function getModuleItemSelector() {
      return this.currentDisplay === this.DISPLAY_GRID ? this.moduleItemGridSelector : this.moduleItemListSelector;
    }

    /**
     * Get the module notifications count and displays it as a badge on the notification tab
     * @return void
     */

  }, {
    key: 'getNotificationsCount',
    value: function getNotificationsCount() {
      var self = this;
      $.getJSON(window.moduleURLs.notificationsCount, self.updateNotificationsCount).fail(function () {
        console.error('Could not retrieve module notifications count.');
      });
    }
  }, {
    key: 'updateNotificationsCount',
    value: function updateNotificationsCount(badge) {
      var destinationTabs = {
        to_configure: $('#subtab-AdminModulesNotifications'),
        to_update: $('#subtab-AdminModulesUpdates')
      };

      (0, _keys2.default)(destinationTabs).forEach(function (destinationKey) {
        if (destinationTabs[destinationKey].length !== 0) {
          destinationTabs[destinationKey].find('.notification-counter').text(badge[destinationKey]);
        }
      });
    }
  }, {
    key: 'initAddonsSearch',
    value: function initAddonsSearch() {
      var self = this;
      $('body').on('click', self.addonItemGridSelector + ', ' + self.addonItemListSelector, function () {
        var searchQuery = '';

        if (self.currentTagsList.length) {
          searchQuery = encodeURIComponent(self.currentTagsList.join(' '));
        }

        window.open(self.baseAddonsUrl + 'search.php?search_query=' + searchQuery, '_blank');
      });
    }
  }, {
    key: 'initCategoriesGrid',
    value: function initCategoriesGrid() {
      var self = this;

      $('body').on('click', this.categoryGridItemSelector, function initilaizeGridBodyClick(event) {
        event.stopPropagation();
        event.preventDefault();
        var refCategory = $(this).data('category-ref');

        // In case we have some tags we need to reset it !
        if (self.currentTagsList.length) {
          self.pstaggerInput.resetTags(false);
          self.currentTagsList = [];
        }
        var menuCategoryToTrigger = $(self.categoryItemSelector + '[data-category-ref="' + refCategory + '"]');

        if (!menuCategoryToTrigger.length) {
          console.warn('No category with ref (' + refCategory + ') seems to exist!');
          return false;
        }

        // Hide current category grid
        if (self.isCategoryGridDisplayed === true) {
          $(self.categoryGridSelector).fadeOut();
          self.isCategoryGridDisplayed = false;
        }

        // Trigger click on right category
        $(self.categoryItemSelector + '[data-category-ref="' + refCategory + '"]').click();
        return true;
      });
    }
  }, {
    key: 'initCurrentDisplay',
    value: function initCurrentDisplay() {
      this.currentDisplay = this.currentDisplay === '' ? this.DISPLAY_LIST : this.DISPLAY_GRID;
    }
  }, {
    key: 'initSortingDropdown',
    value: function initSortingDropdown() {
      var self = this;

      self.currentSorting = $(this.moduleSortingDropdownSelector).find(':checked').attr('value');
      if (!self.currentSorting) {
        self.currentSorting = 'access-desc';
      }

      $('body').on('change', self.moduleSortingDropdownSelector, function initializeBodySortingChange() {
        self.currentSorting = $(this).find(':checked').attr('value');
        self.updateModuleVisibility();
      });
    }
  }, {
    key: 'doBulkAction',
    value: function doBulkAction(requestedBulkAction) {
      // This object is used to check if requested bulkAction is available and give proper
      // url segment to be called for it
      var forceDeletion = $('#force_bulk_deletion').prop('checked');

      var bulkActionToUrl = {
        'bulk-uninstall': 'uninstall',
        'bulk-disable': 'disable',
        'bulk-enable': 'enable',
        'bulk-disable-mobile': 'disable_mobile',
        'bulk-enable-mobile': 'enable_mobile',
        'bulk-reset': 'reset'
      };

      // Note no grid selector used yet since we do not needed it at dev time
      // Maybe useful to implement this kind of things later if intended to
      // use this functionality elsewhere but "manage my module" section
      if (typeof bulkActionToUrl[requestedBulkAction] === 'undefined') {
        $.growl.error({
          message: window.translate_javascripts['Bulk Action - Request not found'].replace('[1]', requestedBulkAction)
        });
        return false;
      }

      // Loop over all checked bulk checkboxes
      var bulkActionSelectedSelector = this.getBulkCheckboxesCheckedSelector();
      var bulkModuleAction = bulkActionToUrl[requestedBulkAction];

      if ($(bulkActionSelectedSelector).length <= 0) {
        console.warn(window.translate_javascripts['Bulk Action - One module minimum']);
        return false;
      }

      var modulesActions = [];
      var moduleTechName = void 0;
      $(bulkActionSelectedSelector).each(function bulkActionSelector() {
        moduleTechName = $(this).data('tech-name');
        modulesActions.push({
          techName: moduleTechName,
          actionMenuObj: $(this).closest('.module-checkbox-bulk-list').next()
        });
      });

      this.performModulesAction(modulesActions, bulkModuleAction, forceDeletion);

      return true;
    }
  }, {
    key: 'performModulesAction',
    value: function performModulesAction(modulesActions, bulkModuleAction, forceDeletion) {
      var self = this;

      if (typeof self.moduleCardController === 'undefined') {
        return;
      }

      // First let's filter modules that can't perform this action
      var actionMenuLinks = filterAllowedActions(modulesActions);

      if (!actionMenuLinks.length) {
        return;
      }

      var modulesRequestedCountdown = actionMenuLinks.length - 1;
      var spinnerObj = $('<button class="btn-primary-reverse onclick unbind spinner "></button>');

      if (actionMenuLinks.length > 1) {
        // Loop through all the modules except the last one which waits for other
        // requests and then call its request with cache clear enabled
        $.each(actionMenuLinks, function (index, actionMenuLink) {
          if (index >= actionMenuLinks.length - 1) {
            return;
          }
          requestModuleAction(actionMenuLink, true, countdownModulesRequest);
        });
        // Display a spinner for the last module
        var lastMenuLink = actionMenuLinks[actionMenuLinks.length - 1];
        var actionMenuObj = lastMenuLink.closest(self.moduleCardController.moduleItemActionsSelector);
        actionMenuObj.hide();
        actionMenuObj.after(spinnerObj);
      } else {
        requestModuleAction(actionMenuLinks[0]);
      }

      function requestModuleAction(actionMenuLink, disableCacheClear, requestEndCallback) {
        self.moduleCardController.requestToController(bulkModuleAction, actionMenuLink, forceDeletion, disableCacheClear, requestEndCallback);
      }

      function countdownModulesRequest() {
        modulesRequestedCountdown -= 1;
        // Now that all other modules have performed their action WITHOUT cache clear, we
        // can request the last module request WITH cache clear
        if (modulesRequestedCountdown <= 0) {
          if (spinnerObj) {
            spinnerObj.remove();
            spinnerObj = null;
          }

          var _lastMenuLink = actionMenuLinks[actionMenuLinks.length - 1];
          var _actionMenuObj = _lastMenuLink.closest(self.moduleCardController.moduleItemActionsSelector);
          _actionMenuObj.fadeIn();
          requestModuleAction(_lastMenuLink);
        }
      }

      function filterAllowedActions(actions) {
        var menuLinks = [];
        var actionMenuLink = void 0;
        $.each(actions, function (index, moduleData) {
          actionMenuLink = $(self.moduleCardController.moduleActionMenuLinkSelector + bulkModuleAction, moduleData.actionMenuObj);
          if (actionMenuLink.length > 0) {
            menuLinks.push(actionMenuLink);
          } else {
            $.growl.error({
              message: window.translate_javascripts['Bulk Action - Request not available for module'].replace('[1]', bulkModuleAction).replace('[2]', moduleData.techName)
            });
          }
        });

        return menuLinks;
      }
    }
  }, {
    key: 'initActionButtons',
    value: function initActionButtons() {
      var _this = this;

      var self = this;
      $('body').on('click', self.moduleInstallBtnSelector, function initializeActionButtonsClick(event) {
        var $this = $(this);
        var $next = $($this.next());
        event.preventDefault();

        $this.hide();
        $next.show();

        $.ajax({
          url: $this.data('url'),
          dataType: 'json'
        }).done(function () {
          $next.fadeOut();
        });
      });

      // "Upgrade All" button handler
      $('body').on('click', self.upgradeAllSource, function (event) {
        event.preventDefault();
        var isMaintenanceMode = window.isShopMaintenance;

        // Modal body element
        var maintenanceLink = document.createElement('a');
        maintenanceLink.classList.add('btn', 'btn-primary', 'btn-lg');
        maintenanceLink.setAttribute('href', window.moduleURLs.maintenancePage);
        maintenanceLink.innerHTML = window.moduleTranslations.moduleModalUpdateMaintenance;

        var updateAllConfirmModal = new _modal2.default({
          id: 'confirm-module-update-modal',
          confirmTitle: window.moduleTranslations.singleModuleModalUpdateTitle,
          closeButtonLabel: window.moduleTranslations.moduleModalUpdateCancel,
          confirmButtonLabel: isMaintenanceMode ? window.moduleTranslations.moduleModalUpdateUpgrade : window.moduleTranslations.upgradeAnywayButtonText,
          confirmButtonClass: isMaintenanceMode ? 'btn-primary' : 'btn-secondary',
          confirmMessage: isMaintenanceMode ? '' : window.moduleTranslations.moduleModalUpdateConfirmMessage,
          closable: true,
          customButtons: isMaintenanceMode ? [] : [maintenanceLink]
        }, function () {
          if ($(self.upgradeAllTargets).length <= 0) {
            console.warn(window.translate_javascripts['Upgrade All Action - One module minimum']);
            return false;
          }

          var modulesActions = [];
          var moduleTechName = void 0;
          $(self.upgradeAllTargets).each(function bulkActionSelector() {
            var moduleItemList = $(this).closest('.module-item-list');
            moduleTechName = moduleItemList.data('tech-name');
            modulesActions.push({
              techName: moduleTechName,
              actionMenuObj: $('.module-actions', moduleItemList)
            });
          });

          _this.performModulesAction(modulesActions, 'upgrade');

          return true;
        });

        updateAllConfirmModal.show();

        return true;
      });
    }
  }, {
    key: 'initCategorySelect',
    value: function initCategorySelect() {
      var self = this;
      var body = $('body');
      body.on('click', self.categoryItemSelector, function initializeCategorySelectClick() {
        // Get data from li DOM input
        self.currentRefCategory = $(this).data('category-ref');
        self.currentRefCategory = self.currentRefCategory ? String(self.currentRefCategory).toLowerCase() : null;
        // Change dropdown label to set it to the current category's displayname
        $(self.categorySelectorLabelSelector).text($(this).data('category-display-name'));
        $(self.categoryResetBtnSelector).show();
        self.updateModuleVisibility();
      });

      body.on('click', self.categoryResetBtnSelector, function initializeCategoryResetButtonClick() {
        var rawText = $(self.categorySelector).attr('aria-labelledby');
        var upperFirstLetter = rawText.charAt(0).toUpperCase();
        var removedFirstLetter = rawText.slice(1);
        var originalText = upperFirstLetter + removedFirstLetter;

        $(self.categorySelectorLabelSelector).text(originalText);
        $(this).hide();
        self.currentRefCategory = null;
        self.updateModuleVisibility();
      });
    }
  }, {
    key: 'initSearchBlock',
    value: function initSearchBlock() {
      var _this2 = this;

      var self = this;
      self.pstaggerInput = $('#module-search-bar').pstagger({
        onTagsChanged: function onTagsChanged(tagList) {
          self.currentTagsList = tagList;
          self.updateModuleVisibility();
        },
        onResetTags: function onResetTags() {
          self.currentTagsList = [];
          self.updateModuleVisibility();
        },
        inputPlaceholder: window.translate_javascripts['Search - placeholder'],
        closingCross: true,
        context: self
      });

      $('body').on('click', '.module-addons-search-link', function (event) {
        event.preventDefault();
        event.stopPropagation();
        window.open($(_this2).attr('href'), '_blank');
      });
    }

    /**
     * Initialize display switching between List or Grid
     */

  }, {
    key: 'initSortingDisplaySwitch',
    value: function initSortingDisplaySwitch() {
      var self = this;

      $('body').on('click', '.module-sort-switch', function switchSort() {
        var switchTo = $(this).data('switch');
        var isAlreadyDisplayed = $(this).hasClass('active-display');

        if (typeof switchTo !== 'undefined' && isAlreadyDisplayed === false) {
          self.switchSortingDisplayTo(switchTo);
          self.currentDisplay = switchTo;
        }
      });
    }
  }, {
    key: 'switchSortingDisplayTo',
    value: function switchSortingDisplayTo(switchTo) {
      if (switchTo !== this.DISPLAY_GRID && switchTo !== this.DISPLAY_LIST) {
        console.error('Can\'t switch to undefined display property "' + switchTo + '"');
        return;
      }

      $('.module-sort-switch').removeClass('module-sort-active');
      $('#module-sort-' + switchTo).addClass('module-sort-active');
      this.currentDisplay = switchTo;
      this.updateModuleVisibility();
    }
  }, {
    key: 'initializeSeeMore',
    value: function initializeSeeMore() {
      var self = this;

      $(self.moduleShortList + ' ' + self.seeMoreSelector).on('click', function seeMore() {
        self.currentCategoryDisplay[$(this).data('category')] = true;
        $(this).addClass('d-none');
        $(this).closest(self.moduleShortList).find(self.seeLessSelector).removeClass('d-none');
        self.updateModuleVisibility();
      });

      $(self.moduleShortList + ' ' + self.seeLessSelector).on('click', function seeMore() {
        self.currentCategoryDisplay[$(this).data('category')] = false;
        $(this).addClass('d-none');
        $(this).closest(self.moduleShortList).find(self.seeMoreSelector).removeClass('d-none');
        self.updateModuleVisibility();
      });
    }
  }, {
    key: 'updateTotalResults',
    value: function updateTotalResults() {
      var self = this;
      var replaceFirstWordBy = function replaceFirstWordBy(element, value) {
        var explodedText = element.text().split(' ');
        explodedText[0] = value;
        element.text(explodedText.join(' '));
      };

      // If there are some shortlist: each shortlist count the modules on the next container.
      var $shortLists = $('.module-short-list');

      if ($shortLists.length > 0) {
        $shortLists.each(function shortLists() {
          var $this = $(this);
          replaceFirstWordBy($this.find('.module-search-result-wording'), $this.next('.modules-list').find('.module-item').length);
        });

        // If there is no shortlist: the wording directly update from the only module container.
      } else {
        var modulesCount = $('.modules-list').find('.module-item').length;
        replaceFirstWordBy($('.module-search-result-wording'), modulesCount);

        // eslint-disable-next-line
        var selectorToToggle = self.currentDisplay === self.DISPLAY_LIST ? this.addonItemListSelector : this.addonItemGridSelector;
        $(selectorToToggle).toggle(modulesCount !== this.modulesList.length / 2);

        if (modulesCount === 0) {
          $('.module-addons-search-link').attr('href', this.baseAddonsUrl + 'search.php?search_query=' + encodeURIComponent(this.currentTagsList.join(' ')));
        }
      }
    }
  }, {
    key: 'isModulesPage',
    value: function isModulesPage() {
      return $(this.upgradeContainer).length === 0 && $(this.notificationContainer).length === 0;
    }
  }]);
  return AdminModuleController;
}();

exports.default = AdminModuleController;

/***/ }),

/***/ "./js/pages/module/loader.js":
/*!***********************************!*\
  !*** ./js/pages/module/loader.js ***!
  \***********************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

"use strict";


Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 */

var _window = window,
    $ = _window.$;

/**
 * Module Admin Page Loader.
 * @constructor
 */

var ModuleLoader = function () {
  function ModuleLoader() {
    (0, _classCallCheck3.default)(this, ModuleLoader);

    ModuleLoader.handleImport();
    ModuleLoader.handleEvents();
  }

  (0, _createClass3.default)(ModuleLoader, null, [{
    key: 'handleImport',
    value: function handleImport() {
      var moduleImport = $('#module-import');
      moduleImport.click(function () {
        moduleImport.addClass('onclick', 250, validate);
      });

      function validate() {
        setTimeout(function () {
          moduleImport.removeClass('onclick');
          moduleImport.addClass('validate', 450, callback);
        }, 2250);
      }
      function callback() {
        setTimeout(function () {
          moduleImport.removeClass('validate');
        }, 1250);
      }
    }
  }, {
    key: 'handleEvents',
    value: function handleEvents() {
      $('body').on('click', 'a.module-read-more-grid-btn, a.module-read-more-list-btn', function (event) {
        event.preventDefault();
        var modulePoppin = $(event.target).data('target');

        $.get(event.target.href, function (data) {
          $(modulePoppin).html(data);
          $(modulePoppin).modal();
        });
      });
    }
  }]);
  return ModuleLoader;
}();

exports.default = ModuleLoader;

/***/ }),

/***/ "./node_modules/babel-runtime/core-js/array/from.js":
/*!**********************************************************!*\
  !*** ./node_modules/babel-runtime/core-js/array/from.js ***!
  \**********************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

module.exports = { "default": __webpack_require__(/*! core-js/library/fn/array/from */ "./node_modules/core-js/library/fn/array/from.js"), __esModule: true };

/***/ }),

/***/ "./node_modules/babel-runtime/core-js/number/is-nan.js":
/*!*************************************************************!*\
  !*** ./node_modules/babel-runtime/core-js/number/is-nan.js ***!
  \*************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

module.exports = { "default": __webpack_require__(/*! core-js/library/fn/number/is-nan */ "./node_modules/core-js/library/fn/number/is-nan.js"), __esModule: true };

/***/ }),

/***/ "./node_modules/babel-runtime/core-js/object/define-property.js":
/*!**********************************************************************!*\
  !*** ./node_modules/babel-runtime/core-js/object/define-property.js ***!
  \**********************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

module.exports = { "default": __webpack_require__(/*! core-js/library/fn/object/define-property */ "./node_modules/core-js/library/fn/object/define-property.js"), __esModule: true };

/***/ }),

/***/ "./node_modules/babel-runtime/core-js/object/keys.js":
/*!***********************************************************!*\
  !*** ./node_modules/babel-runtime/core-js/object/keys.js ***!
  \***********************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

module.exports = { "default": __webpack_require__(/*! core-js/library/fn/object/keys */ "./node_modules/core-js/library/fn/object/keys.js"), __esModule: true };

/***/ }),

/***/ "./node_modules/babel-runtime/helpers/classCallCheck.js":
/*!**************************************************************!*\
  !*** ./node_modules/babel-runtime/helpers/classCallCheck.js ***!
  \**************************************************************/
/***/ ((__unused_webpack_module, exports) => {

"use strict";


exports.__esModule = true;

exports.default = function (instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
};

/***/ }),

/***/ "./node_modules/babel-runtime/helpers/createClass.js":
/*!***********************************************************!*\
  !*** ./node_modules/babel-runtime/helpers/createClass.js ***!
  \***********************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

"use strict";


exports.__esModule = true;

var _defineProperty = __webpack_require__(/*! ../core-js/object/define-property */ "./node_modules/babel-runtime/core-js/object/define-property.js");

var _defineProperty2 = _interopRequireDefault(_defineProperty);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      (0, _defineProperty2.default)(target, descriptor.key, descriptor);
    }
  }

  return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);
    if (staticProps) defineProperties(Constructor, staticProps);
    return Constructor;
  };
}();

/***/ }),

/***/ "./node_modules/babel-runtime/helpers/toConsumableArray.js":
/*!*****************************************************************!*\
  !*** ./node_modules/babel-runtime/helpers/toConsumableArray.js ***!
  \*****************************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

"use strict";


exports.__esModule = true;

var _from = __webpack_require__(/*! ../core-js/array/from */ "./node_modules/babel-runtime/core-js/array/from.js");

var _from2 = _interopRequireDefault(_from);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (arr) {
  if (Array.isArray(arr)) {
    for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) {
      arr2[i] = arr[i];
    }

    return arr2;
  } else {
    return (0, _from2.default)(arr);
  }
};

/***/ }),

/***/ "./node_modules/core-js/library/fn/array/from.js":
/*!*******************************************************!*\
  !*** ./node_modules/core-js/library/fn/array/from.js ***!
  \*******************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

__webpack_require__(/*! ../../modules/es6.string.iterator */ "./node_modules/core-js/library/modules/es6.string.iterator.js");
__webpack_require__(/*! ../../modules/es6.array.from */ "./node_modules/core-js/library/modules/es6.array.from.js");
module.exports = __webpack_require__(/*! ../../modules/_core */ "./node_modules/core-js/library/modules/_core.js").Array.from;


/***/ }),

/***/ "./node_modules/core-js/library/fn/number/is-nan.js":
/*!**********************************************************!*\
  !*** ./node_modules/core-js/library/fn/number/is-nan.js ***!
  \**********************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

__webpack_require__(/*! ../../modules/es6.number.is-nan */ "./node_modules/core-js/library/modules/es6.number.is-nan.js");
module.exports = __webpack_require__(/*! ../../modules/_core */ "./node_modules/core-js/library/modules/_core.js").Number.isNaN;


/***/ }),

/***/ "./node_modules/core-js/library/fn/object/define-property.js":
/*!*******************************************************************!*\
  !*** ./node_modules/core-js/library/fn/object/define-property.js ***!
  \*******************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

__webpack_require__(/*! ../../modules/es6.object.define-property */ "./node_modules/core-js/library/modules/es6.object.define-property.js");
var $Object = __webpack_require__(/*! ../../modules/_core */ "./node_modules/core-js/library/modules/_core.js").Object;
module.exports = function defineProperty(it, key, desc) {
  return $Object.defineProperty(it, key, desc);
};


/***/ }),

/***/ "./node_modules/core-js/library/fn/object/keys.js":
/*!********************************************************!*\
  !*** ./node_modules/core-js/library/fn/object/keys.js ***!
  \********************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

__webpack_require__(/*! ../../modules/es6.object.keys */ "./node_modules/core-js/library/modules/es6.object.keys.js");
module.exports = __webpack_require__(/*! ../../modules/_core */ "./node_modules/core-js/library/modules/_core.js").Object.keys;


/***/ }),

/***/ "./node_modules/core-js/library/modules/_a-function.js":
/*!*************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_a-function.js ***!
  \*************************************************************/
/***/ ((module) => {

module.exports = function (it) {
  if (typeof it != 'function') throw TypeError(it + ' is not a function!');
  return it;
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_an-object.js":
/*!************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_an-object.js ***!
  \************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var isObject = __webpack_require__(/*! ./_is-object */ "./node_modules/core-js/library/modules/_is-object.js");
module.exports = function (it) {
  if (!isObject(it)) throw TypeError(it + ' is not an object!');
  return it;
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_array-includes.js":
/*!*****************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_array-includes.js ***!
  \*****************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// false -> Array#indexOf
// true  -> Array#includes
var toIObject = __webpack_require__(/*! ./_to-iobject */ "./node_modules/core-js/library/modules/_to-iobject.js");
var toLength = __webpack_require__(/*! ./_to-length */ "./node_modules/core-js/library/modules/_to-length.js");
var toAbsoluteIndex = __webpack_require__(/*! ./_to-absolute-index */ "./node_modules/core-js/library/modules/_to-absolute-index.js");
module.exports = function (IS_INCLUDES) {
  return function ($this, el, fromIndex) {
    var O = toIObject($this);
    var length = toLength(O.length);
    var index = toAbsoluteIndex(fromIndex, length);
    var value;
    // Array#includes uses SameValueZero equality algorithm
    // eslint-disable-next-line no-self-compare
    if (IS_INCLUDES && el != el) while (length > index) {
      value = O[index++];
      // eslint-disable-next-line no-self-compare
      if (value != value) return true;
    // Array#indexOf ignores holes, Array#includes - not
    } else for (;length > index; index++) if (IS_INCLUDES || index in O) {
      if (O[index] === el) return IS_INCLUDES || index || 0;
    } return !IS_INCLUDES && -1;
  };
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_classof.js":
/*!**********************************************************!*\
  !*** ./node_modules/core-js/library/modules/_classof.js ***!
  \**********************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// getting tag from 19.1.3.6 Object.prototype.toString()
var cof = __webpack_require__(/*! ./_cof */ "./node_modules/core-js/library/modules/_cof.js");
var TAG = __webpack_require__(/*! ./_wks */ "./node_modules/core-js/library/modules/_wks.js")('toStringTag');
// ES3 wrong here
var ARG = cof(function () { return arguments; }()) == 'Arguments';

// fallback for IE11 Script Access Denied error
var tryGet = function (it, key) {
  try {
    return it[key];
  } catch (e) { /* empty */ }
};

module.exports = function (it) {
  var O, T, B;
  return it === undefined ? 'Undefined' : it === null ? 'Null'
    // @@toStringTag case
    : typeof (T = tryGet(O = Object(it), TAG)) == 'string' ? T
    // builtinTag case
    : ARG ? cof(O)
    // ES3 arguments fallback
    : (B = cof(O)) == 'Object' && typeof O.callee == 'function' ? 'Arguments' : B;
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_cof.js":
/*!******************************************************!*\
  !*** ./node_modules/core-js/library/modules/_cof.js ***!
  \******************************************************/
/***/ ((module) => {

var toString = {}.toString;

module.exports = function (it) {
  return toString.call(it).slice(8, -1);
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_core.js":
/*!*******************************************************!*\
  !*** ./node_modules/core-js/library/modules/_core.js ***!
  \*******************************************************/
/***/ ((module) => {

var core = module.exports = { version: '2.6.11' };
if (typeof __e == 'number') __e = core; // eslint-disable-line no-undef


/***/ }),

/***/ "./node_modules/core-js/library/modules/_create-property.js":
/*!******************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_create-property.js ***!
  \******************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";

var $defineProperty = __webpack_require__(/*! ./_object-dp */ "./node_modules/core-js/library/modules/_object-dp.js");
var createDesc = __webpack_require__(/*! ./_property-desc */ "./node_modules/core-js/library/modules/_property-desc.js");

module.exports = function (object, index, value) {
  if (index in object) $defineProperty.f(object, index, createDesc(0, value));
  else object[index] = value;
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_ctx.js":
/*!******************************************************!*\
  !*** ./node_modules/core-js/library/modules/_ctx.js ***!
  \******************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// optional / simple context binding
var aFunction = __webpack_require__(/*! ./_a-function */ "./node_modules/core-js/library/modules/_a-function.js");
module.exports = function (fn, that, length) {
  aFunction(fn);
  if (that === undefined) return fn;
  switch (length) {
    case 1: return function (a) {
      return fn.call(that, a);
    };
    case 2: return function (a, b) {
      return fn.call(that, a, b);
    };
    case 3: return function (a, b, c) {
      return fn.call(that, a, b, c);
    };
  }
  return function (/* ...args */) {
    return fn.apply(that, arguments);
  };
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_defined.js":
/*!**********************************************************!*\
  !*** ./node_modules/core-js/library/modules/_defined.js ***!
  \**********************************************************/
/***/ ((module) => {

// 7.2.1 RequireObjectCoercible(argument)
module.exports = function (it) {
  if (it == undefined) throw TypeError("Can't call method on  " + it);
  return it;
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_descriptors.js":
/*!**************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_descriptors.js ***!
  \**************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// Thank's IE8 for his funny defineProperty
module.exports = !__webpack_require__(/*! ./_fails */ "./node_modules/core-js/library/modules/_fails.js")(function () {
  return Object.defineProperty({}, 'a', { get: function () { return 7; } }).a != 7;
});


/***/ }),

/***/ "./node_modules/core-js/library/modules/_dom-create.js":
/*!*************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_dom-create.js ***!
  \*************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var isObject = __webpack_require__(/*! ./_is-object */ "./node_modules/core-js/library/modules/_is-object.js");
var document = __webpack_require__(/*! ./_global */ "./node_modules/core-js/library/modules/_global.js").document;
// typeof document.createElement is 'object' in old IE
var is = isObject(document) && isObject(document.createElement);
module.exports = function (it) {
  return is ? document.createElement(it) : {};
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_enum-bug-keys.js":
/*!****************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_enum-bug-keys.js ***!
  \****************************************************************/
/***/ ((module) => {

// IE 8- don't enum bug keys
module.exports = (
  'constructor,hasOwnProperty,isPrototypeOf,propertyIsEnumerable,toLocaleString,toString,valueOf'
).split(',');


/***/ }),

/***/ "./node_modules/core-js/library/modules/_export.js":
/*!*********************************************************!*\
  !*** ./node_modules/core-js/library/modules/_export.js ***!
  \*********************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var global = __webpack_require__(/*! ./_global */ "./node_modules/core-js/library/modules/_global.js");
var core = __webpack_require__(/*! ./_core */ "./node_modules/core-js/library/modules/_core.js");
var ctx = __webpack_require__(/*! ./_ctx */ "./node_modules/core-js/library/modules/_ctx.js");
var hide = __webpack_require__(/*! ./_hide */ "./node_modules/core-js/library/modules/_hide.js");
var has = __webpack_require__(/*! ./_has */ "./node_modules/core-js/library/modules/_has.js");
var PROTOTYPE = 'prototype';

var $export = function (type, name, source) {
  var IS_FORCED = type & $export.F;
  var IS_GLOBAL = type & $export.G;
  var IS_STATIC = type & $export.S;
  var IS_PROTO = type & $export.P;
  var IS_BIND = type & $export.B;
  var IS_WRAP = type & $export.W;
  var exports = IS_GLOBAL ? core : core[name] || (core[name] = {});
  var expProto = exports[PROTOTYPE];
  var target = IS_GLOBAL ? global : IS_STATIC ? global[name] : (global[name] || {})[PROTOTYPE];
  var key, own, out;
  if (IS_GLOBAL) source = name;
  for (key in source) {
    // contains in native
    own = !IS_FORCED && target && target[key] !== undefined;
    if (own && has(exports, key)) continue;
    // export native or passed
    out = own ? target[key] : source[key];
    // prevent global pollution for namespaces
    exports[key] = IS_GLOBAL && typeof target[key] != 'function' ? source[key]
    // bind timers to global for call from export context
    : IS_BIND && own ? ctx(out, global)
    // wrap global constructors for prevent change them in library
    : IS_WRAP && target[key] == out ? (function (C) {
      var F = function (a, b, c) {
        if (this instanceof C) {
          switch (arguments.length) {
            case 0: return new C();
            case 1: return new C(a);
            case 2: return new C(a, b);
          } return new C(a, b, c);
        } return C.apply(this, arguments);
      };
      F[PROTOTYPE] = C[PROTOTYPE];
      return F;
    // make static versions for prototype methods
    })(out) : IS_PROTO && typeof out == 'function' ? ctx(Function.call, out) : out;
    // export proto methods to core.%CONSTRUCTOR%.methods.%NAME%
    if (IS_PROTO) {
      (exports.virtual || (exports.virtual = {}))[key] = out;
      // export proto methods to core.%CONSTRUCTOR%.prototype.%NAME%
      if (type & $export.R && expProto && !expProto[key]) hide(expProto, key, out);
    }
  }
};
// type bitmap
$export.F = 1;   // forced
$export.G = 2;   // global
$export.S = 4;   // static
$export.P = 8;   // proto
$export.B = 16;  // bind
$export.W = 32;  // wrap
$export.U = 64;  // safe
$export.R = 128; // real proto method for `library`
module.exports = $export;


/***/ }),

/***/ "./node_modules/core-js/library/modules/_fails.js":
/*!********************************************************!*\
  !*** ./node_modules/core-js/library/modules/_fails.js ***!
  \********************************************************/
/***/ ((module) => {

module.exports = function (exec) {
  try {
    return !!exec();
  } catch (e) {
    return true;
  }
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_global.js":
/*!*********************************************************!*\
  !*** ./node_modules/core-js/library/modules/_global.js ***!
  \*********************************************************/
/***/ ((module) => {

// https://github.com/zloirock/core-js/issues/86#issuecomment-115759028
var global = module.exports = typeof window != 'undefined' && window.Math == Math
  ? window : typeof self != 'undefined' && self.Math == Math ? self
  // eslint-disable-next-line no-new-func
  : Function('return this')();
if (typeof __g == 'number') __g = global; // eslint-disable-line no-undef


/***/ }),

/***/ "./node_modules/core-js/library/modules/_has.js":
/*!******************************************************!*\
  !*** ./node_modules/core-js/library/modules/_has.js ***!
  \******************************************************/
/***/ ((module) => {

var hasOwnProperty = {}.hasOwnProperty;
module.exports = function (it, key) {
  return hasOwnProperty.call(it, key);
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_hide.js":
/*!*******************************************************!*\
  !*** ./node_modules/core-js/library/modules/_hide.js ***!
  \*******************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var dP = __webpack_require__(/*! ./_object-dp */ "./node_modules/core-js/library/modules/_object-dp.js");
var createDesc = __webpack_require__(/*! ./_property-desc */ "./node_modules/core-js/library/modules/_property-desc.js");
module.exports = __webpack_require__(/*! ./_descriptors */ "./node_modules/core-js/library/modules/_descriptors.js") ? function (object, key, value) {
  return dP.f(object, key, createDesc(1, value));
} : function (object, key, value) {
  object[key] = value;
  return object;
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_html.js":
/*!*******************************************************!*\
  !*** ./node_modules/core-js/library/modules/_html.js ***!
  \*******************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var document = __webpack_require__(/*! ./_global */ "./node_modules/core-js/library/modules/_global.js").document;
module.exports = document && document.documentElement;


/***/ }),

/***/ "./node_modules/core-js/library/modules/_ie8-dom-define.js":
/*!*****************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_ie8-dom-define.js ***!
  \*****************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

module.exports = !__webpack_require__(/*! ./_descriptors */ "./node_modules/core-js/library/modules/_descriptors.js") && !__webpack_require__(/*! ./_fails */ "./node_modules/core-js/library/modules/_fails.js")(function () {
  return Object.defineProperty(__webpack_require__(/*! ./_dom-create */ "./node_modules/core-js/library/modules/_dom-create.js")('div'), 'a', { get: function () { return 7; } }).a != 7;
});


/***/ }),

/***/ "./node_modules/core-js/library/modules/_iobject.js":
/*!**********************************************************!*\
  !*** ./node_modules/core-js/library/modules/_iobject.js ***!
  \**********************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// fallback for non-array-like ES3 and non-enumerable old V8 strings
var cof = __webpack_require__(/*! ./_cof */ "./node_modules/core-js/library/modules/_cof.js");
// eslint-disable-next-line no-prototype-builtins
module.exports = Object('z').propertyIsEnumerable(0) ? Object : function (it) {
  return cof(it) == 'String' ? it.split('') : Object(it);
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_is-array-iter.js":
/*!****************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_is-array-iter.js ***!
  \****************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// check on default Array iterator
var Iterators = __webpack_require__(/*! ./_iterators */ "./node_modules/core-js/library/modules/_iterators.js");
var ITERATOR = __webpack_require__(/*! ./_wks */ "./node_modules/core-js/library/modules/_wks.js")('iterator');
var ArrayProto = Array.prototype;

module.exports = function (it) {
  return it !== undefined && (Iterators.Array === it || ArrayProto[ITERATOR] === it);
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_is-object.js":
/*!************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_is-object.js ***!
  \************************************************************/
/***/ ((module) => {

module.exports = function (it) {
  return typeof it === 'object' ? it !== null : typeof it === 'function';
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_iter-call.js":
/*!************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_iter-call.js ***!
  \************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// call something on iterator step with safe closing on error
var anObject = __webpack_require__(/*! ./_an-object */ "./node_modules/core-js/library/modules/_an-object.js");
module.exports = function (iterator, fn, value, entries) {
  try {
    return entries ? fn(anObject(value)[0], value[1]) : fn(value);
  // 7.4.6 IteratorClose(iterator, completion)
  } catch (e) {
    var ret = iterator['return'];
    if (ret !== undefined) anObject(ret.call(iterator));
    throw e;
  }
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_iter-create.js":
/*!**************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_iter-create.js ***!
  \**************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";

var create = __webpack_require__(/*! ./_object-create */ "./node_modules/core-js/library/modules/_object-create.js");
var descriptor = __webpack_require__(/*! ./_property-desc */ "./node_modules/core-js/library/modules/_property-desc.js");
var setToStringTag = __webpack_require__(/*! ./_set-to-string-tag */ "./node_modules/core-js/library/modules/_set-to-string-tag.js");
var IteratorPrototype = {};

// 25.1.2.1.1 %IteratorPrototype%[@@iterator]()
__webpack_require__(/*! ./_hide */ "./node_modules/core-js/library/modules/_hide.js")(IteratorPrototype, __webpack_require__(/*! ./_wks */ "./node_modules/core-js/library/modules/_wks.js")('iterator'), function () { return this; });

module.exports = function (Constructor, NAME, next) {
  Constructor.prototype = create(IteratorPrototype, { next: descriptor(1, next) });
  setToStringTag(Constructor, NAME + ' Iterator');
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_iter-define.js":
/*!**************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_iter-define.js ***!
  \**************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";

var LIBRARY = __webpack_require__(/*! ./_library */ "./node_modules/core-js/library/modules/_library.js");
var $export = __webpack_require__(/*! ./_export */ "./node_modules/core-js/library/modules/_export.js");
var redefine = __webpack_require__(/*! ./_redefine */ "./node_modules/core-js/library/modules/_redefine.js");
var hide = __webpack_require__(/*! ./_hide */ "./node_modules/core-js/library/modules/_hide.js");
var Iterators = __webpack_require__(/*! ./_iterators */ "./node_modules/core-js/library/modules/_iterators.js");
var $iterCreate = __webpack_require__(/*! ./_iter-create */ "./node_modules/core-js/library/modules/_iter-create.js");
var setToStringTag = __webpack_require__(/*! ./_set-to-string-tag */ "./node_modules/core-js/library/modules/_set-to-string-tag.js");
var getPrototypeOf = __webpack_require__(/*! ./_object-gpo */ "./node_modules/core-js/library/modules/_object-gpo.js");
var ITERATOR = __webpack_require__(/*! ./_wks */ "./node_modules/core-js/library/modules/_wks.js")('iterator');
var BUGGY = !([].keys && 'next' in [].keys()); // Safari has buggy iterators w/o `next`
var FF_ITERATOR = '@@iterator';
var KEYS = 'keys';
var VALUES = 'values';

var returnThis = function () { return this; };

module.exports = function (Base, NAME, Constructor, next, DEFAULT, IS_SET, FORCED) {
  $iterCreate(Constructor, NAME, next);
  var getMethod = function (kind) {
    if (!BUGGY && kind in proto) return proto[kind];
    switch (kind) {
      case KEYS: return function keys() { return new Constructor(this, kind); };
      case VALUES: return function values() { return new Constructor(this, kind); };
    } return function entries() { return new Constructor(this, kind); };
  };
  var TAG = NAME + ' Iterator';
  var DEF_VALUES = DEFAULT == VALUES;
  var VALUES_BUG = false;
  var proto = Base.prototype;
  var $native = proto[ITERATOR] || proto[FF_ITERATOR] || DEFAULT && proto[DEFAULT];
  var $default = $native || getMethod(DEFAULT);
  var $entries = DEFAULT ? !DEF_VALUES ? $default : getMethod('entries') : undefined;
  var $anyNative = NAME == 'Array' ? proto.entries || $native : $native;
  var methods, key, IteratorPrototype;
  // Fix native
  if ($anyNative) {
    IteratorPrototype = getPrototypeOf($anyNative.call(new Base()));
    if (IteratorPrototype !== Object.prototype && IteratorPrototype.next) {
      // Set @@toStringTag to native iterators
      setToStringTag(IteratorPrototype, TAG, true);
      // fix for some old engines
      if (!LIBRARY && typeof IteratorPrototype[ITERATOR] != 'function') hide(IteratorPrototype, ITERATOR, returnThis);
    }
  }
  // fix Array#{values, @@iterator}.name in V8 / FF
  if (DEF_VALUES && $native && $native.name !== VALUES) {
    VALUES_BUG = true;
    $default = function values() { return $native.call(this); };
  }
  // Define iterator
  if ((!LIBRARY || FORCED) && (BUGGY || VALUES_BUG || !proto[ITERATOR])) {
    hide(proto, ITERATOR, $default);
  }
  // Plug for library
  Iterators[NAME] = $default;
  Iterators[TAG] = returnThis;
  if (DEFAULT) {
    methods = {
      values: DEF_VALUES ? $default : getMethod(VALUES),
      keys: IS_SET ? $default : getMethod(KEYS),
      entries: $entries
    };
    if (FORCED) for (key in methods) {
      if (!(key in proto)) redefine(proto, key, methods[key]);
    } else $export($export.P + $export.F * (BUGGY || VALUES_BUG), NAME, methods);
  }
  return methods;
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_iter-detect.js":
/*!**************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_iter-detect.js ***!
  \**************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var ITERATOR = __webpack_require__(/*! ./_wks */ "./node_modules/core-js/library/modules/_wks.js")('iterator');
var SAFE_CLOSING = false;

try {
  var riter = [7][ITERATOR]();
  riter['return'] = function () { SAFE_CLOSING = true; };
  // eslint-disable-next-line no-throw-literal
  Array.from(riter, function () { throw 2; });
} catch (e) { /* empty */ }

module.exports = function (exec, skipClosing) {
  if (!skipClosing && !SAFE_CLOSING) return false;
  var safe = false;
  try {
    var arr = [7];
    var iter = arr[ITERATOR]();
    iter.next = function () { return { done: safe = true }; };
    arr[ITERATOR] = function () { return iter; };
    exec(arr);
  } catch (e) { /* empty */ }
  return safe;
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_iterators.js":
/*!************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_iterators.js ***!
  \************************************************************/
/***/ ((module) => {

module.exports = {};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_library.js":
/*!**********************************************************!*\
  !*** ./node_modules/core-js/library/modules/_library.js ***!
  \**********************************************************/
/***/ ((module) => {

module.exports = true;


/***/ }),

/***/ "./node_modules/core-js/library/modules/_object-create.js":
/*!****************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_object-create.js ***!
  \****************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// 19.1.2.2 / 15.2.3.5 Object.create(O [, Properties])
var anObject = __webpack_require__(/*! ./_an-object */ "./node_modules/core-js/library/modules/_an-object.js");
var dPs = __webpack_require__(/*! ./_object-dps */ "./node_modules/core-js/library/modules/_object-dps.js");
var enumBugKeys = __webpack_require__(/*! ./_enum-bug-keys */ "./node_modules/core-js/library/modules/_enum-bug-keys.js");
var IE_PROTO = __webpack_require__(/*! ./_shared-key */ "./node_modules/core-js/library/modules/_shared-key.js")('IE_PROTO');
var Empty = function () { /* empty */ };
var PROTOTYPE = 'prototype';

// Create object with fake `null` prototype: use iframe Object with cleared prototype
var createDict = function () {
  // Thrash, waste and sodomy: IE GC bug
  var iframe = __webpack_require__(/*! ./_dom-create */ "./node_modules/core-js/library/modules/_dom-create.js")('iframe');
  var i = enumBugKeys.length;
  var lt = '<';
  var gt = '>';
  var iframeDocument;
  iframe.style.display = 'none';
  __webpack_require__(/*! ./_html */ "./node_modules/core-js/library/modules/_html.js").appendChild(iframe);
  iframe.src = 'javascript:'; // eslint-disable-line no-script-url
  // createDict = iframe.contentWindow.Object;
  // html.removeChild(iframe);
  iframeDocument = iframe.contentWindow.document;
  iframeDocument.open();
  iframeDocument.write(lt + 'script' + gt + 'document.F=Object' + lt + '/script' + gt);
  iframeDocument.close();
  createDict = iframeDocument.F;
  while (i--) delete createDict[PROTOTYPE][enumBugKeys[i]];
  return createDict();
};

module.exports = Object.create || function create(O, Properties) {
  var result;
  if (O !== null) {
    Empty[PROTOTYPE] = anObject(O);
    result = new Empty();
    Empty[PROTOTYPE] = null;
    // add "__proto__" for Object.getPrototypeOf polyfill
    result[IE_PROTO] = O;
  } else result = createDict();
  return Properties === undefined ? result : dPs(result, Properties);
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_object-dp.js":
/*!************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_object-dp.js ***!
  \************************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

var anObject = __webpack_require__(/*! ./_an-object */ "./node_modules/core-js/library/modules/_an-object.js");
var IE8_DOM_DEFINE = __webpack_require__(/*! ./_ie8-dom-define */ "./node_modules/core-js/library/modules/_ie8-dom-define.js");
var toPrimitive = __webpack_require__(/*! ./_to-primitive */ "./node_modules/core-js/library/modules/_to-primitive.js");
var dP = Object.defineProperty;

exports.f = __webpack_require__(/*! ./_descriptors */ "./node_modules/core-js/library/modules/_descriptors.js") ? Object.defineProperty : function defineProperty(O, P, Attributes) {
  anObject(O);
  P = toPrimitive(P, true);
  anObject(Attributes);
  if (IE8_DOM_DEFINE) try {
    return dP(O, P, Attributes);
  } catch (e) { /* empty */ }
  if ('get' in Attributes || 'set' in Attributes) throw TypeError('Accessors not supported!');
  if ('value' in Attributes) O[P] = Attributes.value;
  return O;
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_object-dps.js":
/*!*************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_object-dps.js ***!
  \*************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var dP = __webpack_require__(/*! ./_object-dp */ "./node_modules/core-js/library/modules/_object-dp.js");
var anObject = __webpack_require__(/*! ./_an-object */ "./node_modules/core-js/library/modules/_an-object.js");
var getKeys = __webpack_require__(/*! ./_object-keys */ "./node_modules/core-js/library/modules/_object-keys.js");

module.exports = __webpack_require__(/*! ./_descriptors */ "./node_modules/core-js/library/modules/_descriptors.js") ? Object.defineProperties : function defineProperties(O, Properties) {
  anObject(O);
  var keys = getKeys(Properties);
  var length = keys.length;
  var i = 0;
  var P;
  while (length > i) dP.f(O, P = keys[i++], Properties[P]);
  return O;
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_object-gpo.js":
/*!*************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_object-gpo.js ***!
  \*************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// 19.1.2.9 / 15.2.3.2 Object.getPrototypeOf(O)
var has = __webpack_require__(/*! ./_has */ "./node_modules/core-js/library/modules/_has.js");
var toObject = __webpack_require__(/*! ./_to-object */ "./node_modules/core-js/library/modules/_to-object.js");
var IE_PROTO = __webpack_require__(/*! ./_shared-key */ "./node_modules/core-js/library/modules/_shared-key.js")('IE_PROTO');
var ObjectProto = Object.prototype;

module.exports = Object.getPrototypeOf || function (O) {
  O = toObject(O);
  if (has(O, IE_PROTO)) return O[IE_PROTO];
  if (typeof O.constructor == 'function' && O instanceof O.constructor) {
    return O.constructor.prototype;
  } return O instanceof Object ? ObjectProto : null;
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_object-keys-internal.js":
/*!***********************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_object-keys-internal.js ***!
  \***********************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var has = __webpack_require__(/*! ./_has */ "./node_modules/core-js/library/modules/_has.js");
var toIObject = __webpack_require__(/*! ./_to-iobject */ "./node_modules/core-js/library/modules/_to-iobject.js");
var arrayIndexOf = __webpack_require__(/*! ./_array-includes */ "./node_modules/core-js/library/modules/_array-includes.js")(false);
var IE_PROTO = __webpack_require__(/*! ./_shared-key */ "./node_modules/core-js/library/modules/_shared-key.js")('IE_PROTO');

module.exports = function (object, names) {
  var O = toIObject(object);
  var i = 0;
  var result = [];
  var key;
  for (key in O) if (key != IE_PROTO) has(O, key) && result.push(key);
  // Don't enum bug & hidden keys
  while (names.length > i) if (has(O, key = names[i++])) {
    ~arrayIndexOf(result, key) || result.push(key);
  }
  return result;
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_object-keys.js":
/*!**************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_object-keys.js ***!
  \**************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// 19.1.2.14 / 15.2.3.14 Object.keys(O)
var $keys = __webpack_require__(/*! ./_object-keys-internal */ "./node_modules/core-js/library/modules/_object-keys-internal.js");
var enumBugKeys = __webpack_require__(/*! ./_enum-bug-keys */ "./node_modules/core-js/library/modules/_enum-bug-keys.js");

module.exports = Object.keys || function keys(O) {
  return $keys(O, enumBugKeys);
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_object-sap.js":
/*!*************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_object-sap.js ***!
  \*************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// most Object methods by ES6 should accept primitives
var $export = __webpack_require__(/*! ./_export */ "./node_modules/core-js/library/modules/_export.js");
var core = __webpack_require__(/*! ./_core */ "./node_modules/core-js/library/modules/_core.js");
var fails = __webpack_require__(/*! ./_fails */ "./node_modules/core-js/library/modules/_fails.js");
module.exports = function (KEY, exec) {
  var fn = (core.Object || {})[KEY] || Object[KEY];
  var exp = {};
  exp[KEY] = exec(fn);
  $export($export.S + $export.F * fails(function () { fn(1); }), 'Object', exp);
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_property-desc.js":
/*!****************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_property-desc.js ***!
  \****************************************************************/
/***/ ((module) => {

module.exports = function (bitmap, value) {
  return {
    enumerable: !(bitmap & 1),
    configurable: !(bitmap & 2),
    writable: !(bitmap & 4),
    value: value
  };
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_redefine.js":
/*!***********************************************************!*\
  !*** ./node_modules/core-js/library/modules/_redefine.js ***!
  \***********************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

module.exports = __webpack_require__(/*! ./_hide */ "./node_modules/core-js/library/modules/_hide.js");


/***/ }),

/***/ "./node_modules/core-js/library/modules/_set-to-string-tag.js":
/*!********************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_set-to-string-tag.js ***!
  \********************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var def = __webpack_require__(/*! ./_object-dp */ "./node_modules/core-js/library/modules/_object-dp.js").f;
var has = __webpack_require__(/*! ./_has */ "./node_modules/core-js/library/modules/_has.js");
var TAG = __webpack_require__(/*! ./_wks */ "./node_modules/core-js/library/modules/_wks.js")('toStringTag');

module.exports = function (it, tag, stat) {
  if (it && !has(it = stat ? it : it.prototype, TAG)) def(it, TAG, { configurable: true, value: tag });
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_shared-key.js":
/*!*************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_shared-key.js ***!
  \*************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var shared = __webpack_require__(/*! ./_shared */ "./node_modules/core-js/library/modules/_shared.js")('keys');
var uid = __webpack_require__(/*! ./_uid */ "./node_modules/core-js/library/modules/_uid.js");
module.exports = function (key) {
  return shared[key] || (shared[key] = uid(key));
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_shared.js":
/*!*********************************************************!*\
  !*** ./node_modules/core-js/library/modules/_shared.js ***!
  \*********************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var core = __webpack_require__(/*! ./_core */ "./node_modules/core-js/library/modules/_core.js");
var global = __webpack_require__(/*! ./_global */ "./node_modules/core-js/library/modules/_global.js");
var SHARED = '__core-js_shared__';
var store = global[SHARED] || (global[SHARED] = {});

(module.exports = function (key, value) {
  return store[key] || (store[key] = value !== undefined ? value : {});
})('versions', []).push({
  version: core.version,
  mode: __webpack_require__(/*! ./_library */ "./node_modules/core-js/library/modules/_library.js") ? 'pure' : 'global',
  copyright: '© 2019 Denis Pushkarev (zloirock.ru)'
});


/***/ }),

/***/ "./node_modules/core-js/library/modules/_string-at.js":
/*!************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_string-at.js ***!
  \************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var toInteger = __webpack_require__(/*! ./_to-integer */ "./node_modules/core-js/library/modules/_to-integer.js");
var defined = __webpack_require__(/*! ./_defined */ "./node_modules/core-js/library/modules/_defined.js");
// true  -> String#at
// false -> String#codePointAt
module.exports = function (TO_STRING) {
  return function (that, pos) {
    var s = String(defined(that));
    var i = toInteger(pos);
    var l = s.length;
    var a, b;
    if (i < 0 || i >= l) return TO_STRING ? '' : undefined;
    a = s.charCodeAt(i);
    return a < 0xd800 || a > 0xdbff || i + 1 === l || (b = s.charCodeAt(i + 1)) < 0xdc00 || b > 0xdfff
      ? TO_STRING ? s.charAt(i) : a
      : TO_STRING ? s.slice(i, i + 2) : (a - 0xd800 << 10) + (b - 0xdc00) + 0x10000;
  };
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_to-absolute-index.js":
/*!********************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_to-absolute-index.js ***!
  \********************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var toInteger = __webpack_require__(/*! ./_to-integer */ "./node_modules/core-js/library/modules/_to-integer.js");
var max = Math.max;
var min = Math.min;
module.exports = function (index, length) {
  index = toInteger(index);
  return index < 0 ? max(index + length, 0) : min(index, length);
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_to-integer.js":
/*!*************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_to-integer.js ***!
  \*************************************************************/
/***/ ((module) => {

// 7.1.4 ToInteger
var ceil = Math.ceil;
var floor = Math.floor;
module.exports = function (it) {
  return isNaN(it = +it) ? 0 : (it > 0 ? floor : ceil)(it);
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_to-iobject.js":
/*!*************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_to-iobject.js ***!
  \*************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// to indexed object, toObject with fallback for non-array-like ES3 strings
var IObject = __webpack_require__(/*! ./_iobject */ "./node_modules/core-js/library/modules/_iobject.js");
var defined = __webpack_require__(/*! ./_defined */ "./node_modules/core-js/library/modules/_defined.js");
module.exports = function (it) {
  return IObject(defined(it));
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_to-length.js":
/*!************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_to-length.js ***!
  \************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// 7.1.15 ToLength
var toInteger = __webpack_require__(/*! ./_to-integer */ "./node_modules/core-js/library/modules/_to-integer.js");
var min = Math.min;
module.exports = function (it) {
  return it > 0 ? min(toInteger(it), 0x1fffffffffffff) : 0; // pow(2, 53) - 1 == 9007199254740991
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_to-object.js":
/*!************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_to-object.js ***!
  \************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// 7.1.13 ToObject(argument)
var defined = __webpack_require__(/*! ./_defined */ "./node_modules/core-js/library/modules/_defined.js");
module.exports = function (it) {
  return Object(defined(it));
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_to-primitive.js":
/*!***************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_to-primitive.js ***!
  \***************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// 7.1.1 ToPrimitive(input [, PreferredType])
var isObject = __webpack_require__(/*! ./_is-object */ "./node_modules/core-js/library/modules/_is-object.js");
// instead of the ES6 spec version, we didn't implement @@toPrimitive case
// and the second argument - flag - preferred type is a string
module.exports = function (it, S) {
  if (!isObject(it)) return it;
  var fn, val;
  if (S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;
  if (typeof (fn = it.valueOf) == 'function' && !isObject(val = fn.call(it))) return val;
  if (!S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;
  throw TypeError("Can't convert object to primitive value");
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_uid.js":
/*!******************************************************!*\
  !*** ./node_modules/core-js/library/modules/_uid.js ***!
  \******************************************************/
/***/ ((module) => {

var id = 0;
var px = Math.random();
module.exports = function (key) {
  return 'Symbol('.concat(key === undefined ? '' : key, ')_', (++id + px).toString(36));
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_wks.js":
/*!******************************************************!*\
  !*** ./node_modules/core-js/library/modules/_wks.js ***!
  \******************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var store = __webpack_require__(/*! ./_shared */ "./node_modules/core-js/library/modules/_shared.js")('wks');
var uid = __webpack_require__(/*! ./_uid */ "./node_modules/core-js/library/modules/_uid.js");
var Symbol = __webpack_require__(/*! ./_global */ "./node_modules/core-js/library/modules/_global.js").Symbol;
var USE_SYMBOL = typeof Symbol == 'function';

var $exports = module.exports = function (name) {
  return store[name] || (store[name] =
    USE_SYMBOL && Symbol[name] || (USE_SYMBOL ? Symbol : uid)('Symbol.' + name));
};

$exports.store = store;


/***/ }),

/***/ "./node_modules/core-js/library/modules/core.get-iterator-method.js":
/*!**************************************************************************!*\
  !*** ./node_modules/core-js/library/modules/core.get-iterator-method.js ***!
  \**************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var classof = __webpack_require__(/*! ./_classof */ "./node_modules/core-js/library/modules/_classof.js");
var ITERATOR = __webpack_require__(/*! ./_wks */ "./node_modules/core-js/library/modules/_wks.js")('iterator');
var Iterators = __webpack_require__(/*! ./_iterators */ "./node_modules/core-js/library/modules/_iterators.js");
module.exports = __webpack_require__(/*! ./_core */ "./node_modules/core-js/library/modules/_core.js").getIteratorMethod = function (it) {
  if (it != undefined) return it[ITERATOR]
    || it['@@iterator']
    || Iterators[classof(it)];
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/es6.array.from.js":
/*!****************************************************************!*\
  !*** ./node_modules/core-js/library/modules/es6.array.from.js ***!
  \****************************************************************/
/***/ ((__unused_webpack_module, __unused_webpack_exports, __webpack_require__) => {

"use strict";

var ctx = __webpack_require__(/*! ./_ctx */ "./node_modules/core-js/library/modules/_ctx.js");
var $export = __webpack_require__(/*! ./_export */ "./node_modules/core-js/library/modules/_export.js");
var toObject = __webpack_require__(/*! ./_to-object */ "./node_modules/core-js/library/modules/_to-object.js");
var call = __webpack_require__(/*! ./_iter-call */ "./node_modules/core-js/library/modules/_iter-call.js");
var isArrayIter = __webpack_require__(/*! ./_is-array-iter */ "./node_modules/core-js/library/modules/_is-array-iter.js");
var toLength = __webpack_require__(/*! ./_to-length */ "./node_modules/core-js/library/modules/_to-length.js");
var createProperty = __webpack_require__(/*! ./_create-property */ "./node_modules/core-js/library/modules/_create-property.js");
var getIterFn = __webpack_require__(/*! ./core.get-iterator-method */ "./node_modules/core-js/library/modules/core.get-iterator-method.js");

$export($export.S + $export.F * !__webpack_require__(/*! ./_iter-detect */ "./node_modules/core-js/library/modules/_iter-detect.js")(function (iter) { Array.from(iter); }), 'Array', {
  // 22.1.2.1 Array.from(arrayLike, mapfn = undefined, thisArg = undefined)
  from: function from(arrayLike /* , mapfn = undefined, thisArg = undefined */) {
    var O = toObject(arrayLike);
    var C = typeof this == 'function' ? this : Array;
    var aLen = arguments.length;
    var mapfn = aLen > 1 ? arguments[1] : undefined;
    var mapping = mapfn !== undefined;
    var index = 0;
    var iterFn = getIterFn(O);
    var length, result, step, iterator;
    if (mapping) mapfn = ctx(mapfn, aLen > 2 ? arguments[2] : undefined, 2);
    // if object isn't iterable or it's array with default iterator - use simple case
    if (iterFn != undefined && !(C == Array && isArrayIter(iterFn))) {
      for (iterator = iterFn.call(O), result = new C(); !(step = iterator.next()).done; index++) {
        createProperty(result, index, mapping ? call(iterator, mapfn, [step.value, index], true) : step.value);
      }
    } else {
      length = toLength(O.length);
      for (result = new C(length); length > index; index++) {
        createProperty(result, index, mapping ? mapfn(O[index], index) : O[index]);
      }
    }
    result.length = index;
    return result;
  }
});


/***/ }),

/***/ "./node_modules/core-js/library/modules/es6.number.is-nan.js":
/*!*******************************************************************!*\
  !*** ./node_modules/core-js/library/modules/es6.number.is-nan.js ***!
  \*******************************************************************/
/***/ ((__unused_webpack_module, __unused_webpack_exports, __webpack_require__) => {

// 20.1.2.4 Number.isNaN(number)
var $export = __webpack_require__(/*! ./_export */ "./node_modules/core-js/library/modules/_export.js");

$export($export.S, 'Number', {
  isNaN: function isNaN(number) {
    // eslint-disable-next-line no-self-compare
    return number != number;
  }
});


/***/ }),

/***/ "./node_modules/core-js/library/modules/es6.object.define-property.js":
/*!****************************************************************************!*\
  !*** ./node_modules/core-js/library/modules/es6.object.define-property.js ***!
  \****************************************************************************/
/***/ ((__unused_webpack_module, __unused_webpack_exports, __webpack_require__) => {

var $export = __webpack_require__(/*! ./_export */ "./node_modules/core-js/library/modules/_export.js");
// 19.1.2.4 / 15.2.3.6 Object.defineProperty(O, P, Attributes)
$export($export.S + $export.F * !__webpack_require__(/*! ./_descriptors */ "./node_modules/core-js/library/modules/_descriptors.js"), 'Object', { defineProperty: __webpack_require__(/*! ./_object-dp */ "./node_modules/core-js/library/modules/_object-dp.js").f });


/***/ }),

/***/ "./node_modules/core-js/library/modules/es6.object.keys.js":
/*!*****************************************************************!*\
  !*** ./node_modules/core-js/library/modules/es6.object.keys.js ***!
  \*****************************************************************/
/***/ ((__unused_webpack_module, __unused_webpack_exports, __webpack_require__) => {

// 19.1.2.14 Object.keys(O)
var toObject = __webpack_require__(/*! ./_to-object */ "./node_modules/core-js/library/modules/_to-object.js");
var $keys = __webpack_require__(/*! ./_object-keys */ "./node_modules/core-js/library/modules/_object-keys.js");

__webpack_require__(/*! ./_object-sap */ "./node_modules/core-js/library/modules/_object-sap.js")('keys', function () {
  return function keys(it) {
    return $keys(toObject(it));
  };
});


/***/ }),

/***/ "./node_modules/core-js/library/modules/es6.string.iterator.js":
/*!*********************************************************************!*\
  !*** ./node_modules/core-js/library/modules/es6.string.iterator.js ***!
  \*********************************************************************/
/***/ ((__unused_webpack_module, __unused_webpack_exports, __webpack_require__) => {

"use strict";

var $at = __webpack_require__(/*! ./_string-at */ "./node_modules/core-js/library/modules/_string-at.js")(true);

// 21.1.3.27 String.prototype[@@iterator]()
__webpack_require__(/*! ./_iter-define */ "./node_modules/core-js/library/modules/_iter-define.js")(String, 'String', function (iterated) {
  this._t = String(iterated); // target
  this._i = 0;                // next index
// 21.1.5.2.1 %StringIteratorPrototype%.next()
}, function () {
  var O = this._t;
  var index = this._i;
  var point;
  if (index >= O.length) return { value: undefined, done: true };
  point = $at(O, index);
  this._i += point.length;
  return { value: point, done: false };
});


/***/ }),

/***/ "jquery":
/*!*************************!*\
  !*** external "jQuery" ***!
  \*************************/
/***/ ((module) => {

"use strict";
module.exports = window["jQuery"];

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be in strict mode.
(() => {
"use strict";
/*!**********************************!*\
  !*** ./js/pages/module/index.js ***!
  \**********************************/


var _moduleCard = __webpack_require__(/*! @components/module-card */ "./js/components/module-card.js");

var _moduleCard2 = _interopRequireDefault(_moduleCard);

var _controller = __webpack_require__(/*! @pages/module/controller */ "./js/pages/module/controller.js");

var _controller2 = _interopRequireDefault(_controller);

var _loader = __webpack_require__(/*! @pages/module/loader */ "./js/pages/module/loader.js");

var _loader2 = _interopRequireDefault(_loader);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _window = window,
    $ = _window.$; /**
                    * Copyright since 2007 PrestaShop SA and Contributors
                    * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
                    *
                    * NOTICE OF LICENSE
                    *
                    * This source file is subject to the Open Software License (OSL 3.0)
                    * that is bundled with this package in the file LICENSE.md.
                    * It is also available through the world-wide-web at this URL:
                    * https://opensource.org/licenses/OSL-3.0
                    * If you did not receive a copy of the license and are unable to
                    * obtain it through the world-wide-web, please send an email
                    * to license@prestashop.com so we can send you a copy immediately.
                    *
                    * DISCLAIMER
                    *
                    * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
                    * versions in the future. If you wish to customize PrestaShop for your
                    * needs please refer to https://devdocs.prestashop.com/ for more information.
                    *
                    * @author    PrestaShop SA and Contributors <contact@prestashop.com>
                    * @copyright Since 2007 PrestaShop SA and Contributors
                    * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
                    */

$(function () {
  var moduleCardController = new _moduleCard2.default();
  new _loader2.default();
  new _controller2.default(moduleCardController);
});
})();

window.module = __webpack_exports__;
/******/ })()
;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9bbmFtZV0vLi9qcy9jb21wb25lbnRzL21vZGFsLmpzIiwid2VicGFjazovL1tuYW1lXS8uL2pzL2NvbXBvbmVudHMvbW9kdWxlLWNhcmQuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vanMvcGFnZXMvbW9kdWxlL2NvbnRyb2xsZXIuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vanMvcGFnZXMvbW9kdWxlL2xvYWRlci5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvYmFiZWwtcnVudGltZS9jb3JlLWpzL2FycmF5L2Zyb20uanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2JhYmVsLXJ1bnRpbWUvY29yZS1qcy9udW1iZXIvaXMtbmFuLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9iYWJlbC1ydW50aW1lL2NvcmUtanMvb2JqZWN0L2RlZmluZS1wcm9wZXJ0eS5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvYmFiZWwtcnVudGltZS9jb3JlLWpzL29iamVjdC9rZXlzLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9iYWJlbC1ydW50aW1lL2hlbHBlcnMvY2xhc3NDYWxsQ2hlY2suanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jcmVhdGVDbGFzcy5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvYmFiZWwtcnVudGltZS9oZWxwZXJzL3RvQ29uc3VtYWJsZUFycmF5LmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvZm4vYXJyYXkvZnJvbS5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L2ZuL251bWJlci9pcy1uYW4uanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9mbi9vYmplY3QvZGVmaW5lLXByb3BlcnR5LmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvZm4vb2JqZWN0L2tleXMuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19hLWZ1bmN0aW9uLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fYW4tb2JqZWN0LmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fYXJyYXktaW5jbHVkZXMuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19jbGFzc29mLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fY29mLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fY29yZS5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX2NyZWF0ZS1wcm9wZXJ0eS5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX2N0eC5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX2RlZmluZWQuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19kZXNjcmlwdG9ycy5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX2RvbS1jcmVhdGUuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19lbnVtLWJ1Zy1rZXlzLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fZXhwb3J0LmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fZmFpbHMuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19nbG9iYWwuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19oYXMuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19oaWRlLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9faHRtbC5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX2llOC1kb20tZGVmaW5lLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9faW9iamVjdC5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX2lzLWFycmF5LWl0ZXIuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19pcy1vYmplY3QuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19pdGVyLWNhbGwuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19pdGVyLWNyZWF0ZS5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX2l0ZXItZGVmaW5lLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9faXRlci1kZXRlY3QuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19pdGVyYXRvcnMuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19saWJyYXJ5LmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fb2JqZWN0LWNyZWF0ZS5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX29iamVjdC1kcC5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX29iamVjdC1kcHMuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19vYmplY3QtZ3BvLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fb2JqZWN0LWtleXMtaW50ZXJuYWwuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19vYmplY3Qta2V5cy5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX29iamVjdC1zYXAuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19wcm9wZXJ0eS1kZXNjLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fcmVkZWZpbmUuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19zZXQtdG8tc3RyaW5nLXRhZy5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX3NoYXJlZC1rZXkuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19zaGFyZWQuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19zdHJpbmctYXQuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL190by1hYnNvbHV0ZS1pbmRleC5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX3RvLWludGVnZXIuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL190by1pb2JqZWN0LmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fdG8tbGVuZ3RoLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fdG8tb2JqZWN0LmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fdG8tcHJpbWl0aXZlLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fdWlkLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fd2tzLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9jb3JlLmdldC1pdGVyYXRvci1tZXRob2QuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL2VzNi5hcnJheS5mcm9tLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9lczYubnVtYmVyLmlzLW5hbi5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvZXM2Lm9iamVjdC5kZWZpbmUtcHJvcGVydHkuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL2VzNi5vYmplY3Qua2V5cy5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvZXM2LnN0cmluZy5pdGVyYXRvci5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vZXh0ZXJuYWwgXCJqUXVlcnlcIiIsIndlYnBhY2s6Ly9bbmFtZV0vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vanMvcGFnZXMvbW9kdWxlL2luZGV4LmpzIl0sIm5hbWVzIjpbIkNvbmZpcm1Nb2RhbCIsIndpbmRvdyIsIiQiLCJwYXJhbXMiLCJjb25maXJtQ2FsbGJhY2siLCJjYW5jZWxDYWxsYmFjayIsImlkIiwiY2xvc2FibGUiLCJtb2RhbCIsIk1vZGFsIiwiJG1vZGFsIiwiY29udGFpbmVyIiwic2hvdyIsImNvbmZpcm1CdXR0b24iLCJhZGRFdmVudExpc3RlbmVyIiwiYmFja2Ryb3AiLCJrZXlib2FyZCIsInVuZGVmaW5lZCIsIm9uIiwiZG9jdW1lbnQiLCJxdWVyeVNlbGVjdG9yIiwicmVtb3ZlIiwiYm9keSIsImFwcGVuZENoaWxkIiwiY29uZmlybVRpdGxlIiwiY29uZmlybU1lc3NhZ2UiLCJjbG9zZUJ1dHRvbkxhYmVsIiwiY29uZmlybUJ1dHRvbkxhYmVsIiwiY29uZmlybUJ1dHRvbkNsYXNzIiwiY3VzdG9tQnV0dG9ucyIsImNyZWF0ZUVsZW1lbnQiLCJjbGFzc0xpc3QiLCJhZGQiLCJkaWFsb2ciLCJjb250ZW50IiwiaGVhZGVyIiwidGl0bGUiLCJpbm5lckhUTUwiLCJjbG9zZUljb24iLCJzZXRBdHRyaWJ1dGUiLCJkYXRhc2V0IiwiZGlzbWlzcyIsIm1lc3NhZ2UiLCJmb290ZXIiLCJjbG9zZUJ1dHRvbiIsImFwcGVuZCIsIkJPRXZlbnQiLCJldmVudE5hbWUiLCJjYWxsYmFjayIsImNvbnRleHQiLCJldmVudCIsImNhbGwiLCJlbWl0RXZlbnQiLCJldmVudFR5cGUiLCJjcmVhdGVFdmVudCIsImluaXRFdmVudCIsImRpc3BhdGNoRXZlbnQiLCJNb2R1bGVDYXJkIiwibW9kdWxlQWN0aW9uTWVudUxpbmtTZWxlY3RvciIsIm1vZHVsZUFjdGlvbk1lbnVJbnN0YWxsTGlua1NlbGVjdG9yIiwibW9kdWxlQWN0aW9uTWVudUVuYWJsZUxpbmtTZWxlY3RvciIsIm1vZHVsZUFjdGlvbk1lbnVVbmluc3RhbGxMaW5rU2VsZWN0b3IiLCJtb2R1bGVBY3Rpb25NZW51RGlzYWJsZUxpbmtTZWxlY3RvciIsIm1vZHVsZUFjdGlvbk1lbnVFbmFibGVNb2JpbGVMaW5rU2VsZWN0b3IiLCJtb2R1bGVBY3Rpb25NZW51RGlzYWJsZU1vYmlsZUxpbmtTZWxlY3RvciIsIm1vZHVsZUFjdGlvbk1lbnVSZXNldExpbmtTZWxlY3RvciIsIm1vZHVsZUFjdGlvbk1lbnVVcGRhdGVMaW5rU2VsZWN0b3IiLCJtb2R1bGVJdGVtTGlzdFNlbGVjdG9yIiwibW9kdWxlSXRlbUdyaWRTZWxlY3RvciIsIm1vZHVsZUl0ZW1BY3Rpb25zU2VsZWN0b3IiLCJtb2R1bGVBY3Rpb25Nb2RhbERpc2FibGVMaW5rU2VsZWN0b3IiLCJtb2R1bGVBY3Rpb25Nb2RhbFJlc2V0TGlua1NlbGVjdG9yIiwibW9kdWxlQWN0aW9uTW9kYWxVbmluc3RhbGxMaW5rU2VsZWN0b3IiLCJmb3JjZURlbGV0aW9uT3B0aW9uIiwiaW5pdEFjdGlvbkJ1dHRvbnMiLCJzZWxmIiwiYnRuIiwiYXR0ciIsInByb3AiLCJyZW1vdmVBdHRyIiwibGVuZ3RoIiwiZGlzcGF0Y2hQcmVFdmVudCIsImNvbmZpcm1BY3Rpb24iLCJyZXF1ZXN0VG9Db250cm9sbGVyIiwicHJldmVudERlZmF1bHQiLCJkYXRhIiwiaXNNYWludGVuYW5jZU1vZGUiLCJpc1Nob3BNYWludGVuYW5jZSIsIm1haW50ZW5hbmNlTGluayIsIm1vZHVsZVVSTHMiLCJtYWludGVuYW5jZVBhZ2UiLCJtb2R1bGVUcmFuc2xhdGlvbnMiLCJtb2R1bGVNb2RhbFVwZGF0ZU1haW50ZW5hbmNlIiwidXBkYXRlQ29uZmlybU1vZGFsIiwic2luZ2xlTW9kdWxlTW9kYWxVcGRhdGVUaXRsZSIsIm1vZHVsZU1vZGFsVXBkYXRlQ2FuY2VsIiwibW9kdWxlTW9kYWxVcGRhdGVVcGdyYWRlIiwidXBncmFkZUFueXdheUJ1dHRvblRleHQiLCJtb2R1bGVNb2RhbFVwZGF0ZUNvbmZpcm1NZXNzYWdlIiwiZSIsInRhcmdldCIsInBhcmVudHMiLCJhY3Rpb24iLCJlbGVtZW50IiwiZmlyc3QiLCJyZXN1bHQiLCJ0aGF0IiwicmVwbGFjZVByZXN0YVRydXN0UGxhY2Vob2xkZXJzIiwiZmluZCIsIm9mZiIsImluc3RhbGxCdXR0b24iLCJtb2R1bGUiLCJhdHRyaWJ1dGVzIiwibmFtZSIsImZvcm0iLCJwYXJlbnQiLCJ0eXBlIiwidmFsdWUiLCJhcHBlbmRUbyIsImNsaWNrIiwiY29uZmlybWF0aW9uX3N1YmplY3QiLCJhbGVydENsYXNzIiwicHJlc3RhdHJ1c3QiLCJzdGF0dXMiLCJjaGVja19saXN0IiwicHJvcGVydHkiLCJoaWRlIiwidXJsIiwidG9nZ2xlIiwic3JjIiwiaW1nIiwiYWx0IiwidGV4dCIsImRpc3BsYXlOYW1lIiwiYXV0aG9yIiwialF1ZXJ5IiwiRXZlbnQiLCJ0cmlnZ2VyIiwiaXNQcm9wYWdhdGlvblN0b3BwZWQiLCJpc0ltbWVkaWF0ZVByb3BhZ2F0aW9uU3RvcHBlZCIsImZvcmNlRGVsZXRpb24iLCJkaXNhYmxlQ2FjaGVDbGVhciIsImpxRWxlbWVudE9iaiIsImNsb3Nlc3QiLCJzcGlubmVyT2JqIiwibG9jYXRpb24iLCJob3N0IiwiYWN0aW9uUGFyYW1zIiwic2VyaWFsaXplQXJyYXkiLCJwdXNoIiwiYWpheCIsImRhdGFUeXBlIiwibWV0aG9kIiwiYmVmb3JlU2VuZCIsImFmdGVyIiwiZG9uZSIsImdyb3dsIiwiZXJyb3IiLCJmaXhlZCIsIm1zZyIsIm1vZHVsZVRlY2hOYW1lIiwiY29uZmlybVByZXN0YVRydXN0IiwiZHVyYXRpb24iLCJhbHRlcmVkU2VsZWN0b3IiLCJnZXRNb2R1bGVJdGVtU2VsZWN0b3IiLCJyZXBsYWNlIiwibWFpbkVsZW1lbnQiLCJhZGRDbGFzcyIsInJlbW92ZUNsYXNzIiwicmVwbGFjZVdpdGgiLCJhY3Rpb25fbWVudV9odG1sIiwiZmFpbCIsIm1vZHVsZUl0ZW0iLCJ0ZWNoTmFtZSIsImFsd2F5cyIsImZhZGVJbiIsIkFkbWluTW9kdWxlQ29udHJvbGxlciIsIm1vZHVsZUNhcmRDb250cm9sbGVyIiwiREVGQVVMVF9NQVhfUkVDRU5UTFlfVVNFRCIsIkRFRkFVTFRfTUFYX1BFUl9DQVRFR09SSUVTIiwiRElTUExBWV9HUklEIiwiRElTUExBWV9MSVNUIiwiQ0FURUdPUllfUkVDRU5UTFlfVVNFRCIsImN1cnJlbnRDYXRlZ29yeURpc3BsYXkiLCJjdXJyZW50RGlzcGxheSIsImlzQ2F0ZWdvcnlHcmlkRGlzcGxheWVkIiwiY3VycmVudFRhZ3NMaXN0IiwiY3VycmVudFJlZkNhdGVnb3J5IiwiY3VycmVudFJlZlN0YXR1cyIsImN1cnJlbnRTb3J0aW5nIiwiYmFzZUFkZG9uc1VybCIsInBzdGFnZ2VySW5wdXQiLCJsYXN0QnVsa0FjdGlvbiIsImlzVXBsb2FkU3RhcnRlZCIsInJlY2VudGx5VXNlZFNlbGVjdG9yIiwibW9kdWxlc0xpc3QiLCJhZGRvbnNDYXJkR3JpZCIsImFkZG9uc0NhcmRMaXN0IiwibW9kdWxlU2hvcnRMaXN0Iiwic2VlTW9yZVNlbGVjdG9yIiwic2VlTGVzc1NlbGVjdG9yIiwiY2F0ZWdvcnlTZWxlY3RvckxhYmVsU2VsZWN0b3IiLCJjYXRlZ29yeVNlbGVjdG9yIiwiY2F0ZWdvcnlJdGVtU2VsZWN0b3IiLCJhZGRvbnNMb2dpbkJ1dHRvblNlbGVjdG9yIiwiY2F0ZWdvcnlSZXNldEJ0blNlbGVjdG9yIiwibW9kdWxlSW5zdGFsbEJ0blNlbGVjdG9yIiwibW9kdWxlU29ydGluZ0Ryb3Bkb3duU2VsZWN0b3IiLCJjYXRlZ29yeUdyaWRTZWxlY3RvciIsImNhdGVnb3J5R3JpZEl0ZW1TZWxlY3RvciIsImFkZG9uSXRlbUdyaWRTZWxlY3RvciIsImFkZG9uSXRlbUxpc3RTZWxlY3RvciIsInVwZ3JhZGVBbGxTb3VyY2UiLCJ1cGdyYWRlQ29udGFpbmVyIiwidXBncmFkZUFsbFRhcmdldHMiLCJub3RpZmljYXRpb25Db250YWluZXIiLCJidWxrQWN0aW9uRHJvcERvd25TZWxlY3RvciIsImJ1bGtJdGVtU2VsZWN0b3IiLCJidWxrQWN0aW9uQ2hlY2tib3hMaXN0U2VsZWN0b3IiLCJidWxrQWN0aW9uQ2hlY2tib3hHcmlkU2VsZWN0b3IiLCJjaGVja2VkQnVsa0FjdGlvbkxpc3RTZWxlY3RvciIsImNoZWNrZWRCdWxrQWN0aW9uR3JpZFNlbGVjdG9yIiwiYnVsa0FjdGlvbkNoZWNrYm94U2VsZWN0b3IiLCJidWxrQ29uZmlybU1vZGFsU2VsZWN0b3IiLCJidWxrQ29uZmlybU1vZGFsQWN0aW9uTmFtZVNlbGVjdG9yIiwiYnVsa0NvbmZpcm1Nb2RhbExpc3RTZWxlY3RvciIsImJ1bGtDb25maXJtTW9kYWxBY2tCdG5TZWxlY3RvciIsInBsYWNlaG9sZGVyR2xvYmFsU2VsZWN0b3IiLCJwbGFjZWhvbGRlckZhaWx1cmVHbG9iYWxTZWxlY3RvciIsInBsYWNlaG9sZGVyRmFpbHVyZU1zZ1NlbGVjdG9yIiwicGxhY2Vob2xkZXJGYWlsdXJlUmV0cnlCdG5TZWxlY3RvciIsInN0YXR1c1NlbGVjdG9yTGFiZWxTZWxlY3RvciIsInN0YXR1c0l0ZW1TZWxlY3RvciIsInN0YXR1c1Jlc2V0QnRuU2VsZWN0b3IiLCJhZGRvbnNDb25uZWN0TW9kYWxCdG5TZWxlY3RvciIsImFkZG9uc0xvZ291dE1vZGFsQnRuU2VsZWN0b3IiLCJhZGRvbnNJbXBvcnRNb2RhbEJ0blNlbGVjdG9yIiwiZHJvcFpvbmVNb2RhbFNlbGVjdG9yIiwiZHJvcFpvbmVNb2RhbEZvb3RlclNlbGVjdG9yIiwiZHJvcFpvbmVJbXBvcnRab25lU2VsZWN0b3IiLCJhZGRvbnNDb25uZWN0TW9kYWxTZWxlY3RvciIsImFkZG9uc0xvZ291dE1vZGFsU2VsZWN0b3IiLCJhZGRvbnNDb25uZWN0Rm9ybSIsIm1vZHVsZUltcG9ydE1vZGFsQ2xvc2VCdG4iLCJtb2R1bGVJbXBvcnRTdGFydFNlbGVjdG9yIiwibW9kdWxlSW1wb3J0UHJvY2Vzc2luZ1NlbGVjdG9yIiwibW9kdWxlSW1wb3J0U3VjY2Vzc1NlbGVjdG9yIiwibW9kdWxlSW1wb3J0U3VjY2Vzc0NvbmZpZ3VyZUJ0blNlbGVjdG9yIiwibW9kdWxlSW1wb3J0RmFpbHVyZVNlbGVjdG9yIiwibW9kdWxlSW1wb3J0RmFpbHVyZVJldHJ5U2VsZWN0b3IiLCJtb2R1bGVJbXBvcnRGYWlsdXJlRGV0YWlsc0J0blNlbGVjdG9yIiwibW9kdWxlSW1wb3J0U2VsZWN0RmlsZU1hbnVhbFNlbGVjdG9yIiwibW9kdWxlSW1wb3J0RmFpbHVyZU1zZ0RldGFpbHNTZWxlY3RvciIsIm1vZHVsZUltcG9ydENvbmZpcm1TZWxlY3RvciIsImluaXRTb3J0aW5nRHJvcGRvd24iLCJpbml0Qk9FdmVudFJlZ2lzdGVyaW5nIiwiaW5pdEN1cnJlbnREaXNwbGF5IiwiaW5pdFNvcnRpbmdEaXNwbGF5U3dpdGNoIiwiaW5pdEJ1bGtEcm9wZG93biIsImluaXRTZWFyY2hCbG9jayIsImluaXRDYXRlZ29yeVNlbGVjdCIsImluaXRDYXRlZ29yaWVzR3JpZCIsImluaXRBZGRvbnNTZWFyY2giLCJpbml0QWRkb25zQ29ubmVjdCIsImluaXRBZGRNb2R1bGVBY3Rpb24iLCJpbml0RHJvcHpvbmUiLCJpbml0UGFnZUNoYW5nZVByb3RlY3Rpb24iLCJpbml0UGxhY2Vob2xkZXJNZWNoYW5pc20iLCJpbml0RmlsdGVyU3RhdHVzRHJvcGRvd24iLCJmZXRjaE1vZHVsZXNMaXN0IiwiZ2V0Tm90aWZpY2F0aW9uc0NvdW50IiwiaW5pdGlhbGl6ZVNlZU1vcmUiLCJwYXJzZUludCIsInVwZGF0ZU1vZHVsZVZpc2liaWxpdHkiLCJnZXRCdWxrQ2hlY2tib3hlc1NlbGVjdG9yIiwic2VsZWN0b3IiLCJnZXRCdWxrQ2hlY2tib3hlc0NoZWNrZWRTZWxlY3RvciIsImluaXRpYWxpemVCb2R5Q2hhbmdlIiwid2FybmluZyIsInRyYW5zbGF0ZV9qYXZhc2NyaXB0cyIsIm1vZHVsZXNMaXN0U3RyaW5nIiwiYnVpbGRCdWxrQWN0aW9uTW9kdWxlTGlzdCIsImFjdGlvblN0cmluZyIsInRvTG93ZXJDYXNlIiwiaHRtbCIsInN0b3BQcm9wYWdhdGlvbiIsImRvQnVsa0FjdGlvbiIsIm9uTW9kdWxlRGlzYWJsZWQiLCJ1cGRhdGVUb3RhbFJlc3VsdHMiLCJlYWNoIiwiYWpheExvYWRQYWdlIiwiZmFkZU91dCIsImNhdGFsb2dSZWZyZXNoIiwicmVzcG9uc2UiLCJkb21FbGVtZW50cyIsInN0eWxlc2hlZXQiLCJzdHlsZVNoZWV0cyIsInN0eWxlc2hlZXRSdWxlIiwibW9kdWxlR2xvYmFsU2VsZWN0b3IiLCJtb2R1bGVTb3J0aW5nU2VsZWN0b3IiLCJyZXF1aXJlZFNlbGVjdG9yQ29tYmluYXRpb24iLCJpbnNlcnRSdWxlIiwiY3NzUnVsZXMiLCJhZGRSdWxlIiwiaW5kZXgiLCJjc3MiLCJwb3BvdmVyIiwic3RhdHVzVGV4dCIsIiR0aGlzIiwicHJlcGFyZUNvbnRhaW5lciIsInByZXBhcmVNb2R1bGVzIiwiZG9tT2JqZWN0Iiwic2NvcmluZyIsInBhcnNlRmxvYXQiLCJsb2dvIiwidmVyc2lvbiIsImRlc2NyaXB0aW9uIiwiY2hpbGRDYXRlZ29yaWVzIiwiY2F0ZWdvcmllcyIsIlN0cmluZyIsInByaWNlIiwiYWN0aXZlIiwiYWNjZXNzIiwiZGlzcGxheSIsImhhc0NsYXNzIiwiaXNNb2R1bGVzUGFnZSIsIm9yZGVyIiwia2V5Iiwic3BsaXR0ZWRLZXkiLCJzcGxpdCIsImN1cnJlbnRDb21wYXJlIiwiYSIsImIiLCJhRGF0YSIsImJEYXRhIiwiRGF0ZSIsImdldFRpbWUiLCJsb2NhbGVDb21wYXJlIiwic29ydCIsInJldmVyc2UiLCJzZXRTaG9ydExpc3RWaXNpYmlsaXR5IiwibmJNb2R1bGVzSW5Db250YWluZXIiLCJ1cGRhdGVNb2R1bGVTb3J0aW5nIiwiaXNWaXNpYmxlIiwiY3VycmVudE1vZHVsZSIsIm1vZHVsZUNhdGVnb3J5IiwidGFnRXhpc3RzIiwibmV3VmFsdWUiLCJkZWZhdWx0TWF4IiwibW9kdWxlc0xpc3RMZW5ndGgiLCJjb3VudGVyIiwiY2hlY2tUYWciLCJpbmRleE9mIiwiaSIsInVwZGF0ZU1vZHVsZUNvbnRhaW5lckRpc3BsYXkiLCJjaGVja0JveGVzU2VsZWN0b3IiLCJtb2R1bGVJdGVtU2VsZWN0b3IiLCJhbHJlYWR5RG9uZUZsYWciLCJodG1sR2VuZXJhdGVkIiwiY3VycmVudEVsZW1lbnQiLCJwcmVwYXJlQ2hlY2tib3hlcyIsImluaXRpYWxpemVCb2R5U3VibWl0Iiwic2VyaWFsaXplIiwic3VjY2VzcyIsInJlbG9hZCIsImFkZE1vZHVsZUJ1dHRvbiIsImRyb3B6b25lIiwic2V0VGltZW91dCIsIm1hbnVhbFNlbGVjdCIsImluaXRpYWxpemVCb2R5Q2xpY2tPbk1vZHVsZUltcG9ydCIsInNsaWRlRG93biIsImRyb3B6b25lT3B0aW9ucyIsIm1vZHVsZUltcG9ydCIsImFjY2VwdGVkRmlsZXMiLCJwYXJhbU5hbWUiLCJtYXhGaWxlc2l6ZSIsInVwbG9hZE11bHRpcGxlIiwiYWRkUmVtb3ZlTGlua3MiLCJkaWN0RGVmYXVsdE1lc3NhZ2UiLCJoaWRkZW5JbnB1dENvbnRhaW5lciIsInRpbWVvdXQiLCJhZGRlZGZpbGUiLCJhbmltYXRlU3RhcnRVcGxvYWQiLCJwcm9jZXNzaW5nIiwiZmlsZSIsImRpc3BsYXlPblVwbG9hZEVycm9yIiwiY29tcGxldGUiLCJyZXNwb25zZU9iamVjdCIsInBhcnNlSlNPTiIsInhociIsImlzX2NvbmZpZ3VyYWJsZSIsIm1vZHVsZV9uYW1lIiwiZGlzcGxheU9uVXBsb2FkRG9uZSIsImV4dGVuZCIsImZpbmlzaCIsImFuaW1hdGVFbmRVcGxvYWQiLCJjb25maWd1cmVMaW5rIiwiY29uZmlndXJhdGlvblBhZ2UiLCJkaXNwbGF5UHJlc3RhVHJ1c3RTdGVwIiwibW9kdWxlTmFtZSIsInBvc3QiLCJ1cmxzIiwiaW5zdGFsbCIsImdldEpTT04iLCJub3RpZmljYXRpb25zQ291bnQiLCJ1cGRhdGVOb3RpZmljYXRpb25zQ291bnQiLCJjb25zb2xlIiwiYmFkZ2UiLCJkZXN0aW5hdGlvblRhYnMiLCJ0b19jb25maWd1cmUiLCJ0b191cGRhdGUiLCJmb3JFYWNoIiwiZGVzdGluYXRpb25LZXkiLCJzZWFyY2hRdWVyeSIsImVuY29kZVVSSUNvbXBvbmVudCIsImpvaW4iLCJvcGVuIiwiaW5pdGlsYWl6ZUdyaWRCb2R5Q2xpY2siLCJyZWZDYXRlZ29yeSIsInJlc2V0VGFncyIsIm1lbnVDYXRlZ29yeVRvVHJpZ2dlciIsIndhcm4iLCJpbml0aWFsaXplQm9keVNvcnRpbmdDaGFuZ2UiLCJyZXF1ZXN0ZWRCdWxrQWN0aW9uIiwiYnVsa0FjdGlvblRvVXJsIiwiYnVsa0FjdGlvblNlbGVjdGVkU2VsZWN0b3IiLCJidWxrTW9kdWxlQWN0aW9uIiwibW9kdWxlc0FjdGlvbnMiLCJidWxrQWN0aW9uU2VsZWN0b3IiLCJhY3Rpb25NZW51T2JqIiwibmV4dCIsInBlcmZvcm1Nb2R1bGVzQWN0aW9uIiwiYWN0aW9uTWVudUxpbmtzIiwiZmlsdGVyQWxsb3dlZEFjdGlvbnMiLCJtb2R1bGVzUmVxdWVzdGVkQ291bnRkb3duIiwiYWN0aW9uTWVudUxpbmsiLCJyZXF1ZXN0TW9kdWxlQWN0aW9uIiwiY291bnRkb3duTW9kdWxlc1JlcXVlc3QiLCJsYXN0TWVudUxpbmsiLCJyZXF1ZXN0RW5kQ2FsbGJhY2siLCJhY3Rpb25zIiwibWVudUxpbmtzIiwibW9kdWxlRGF0YSIsImluaXRpYWxpemVBY3Rpb25CdXR0b25zQ2xpY2siLCIkbmV4dCIsInVwZGF0ZUFsbENvbmZpcm1Nb2RhbCIsIm1vZHVsZUl0ZW1MaXN0IiwiaW5pdGlhbGl6ZUNhdGVnb3J5U2VsZWN0Q2xpY2siLCJpbml0aWFsaXplQ2F0ZWdvcnlSZXNldEJ1dHRvbkNsaWNrIiwicmF3VGV4dCIsInVwcGVyRmlyc3RMZXR0ZXIiLCJjaGFyQXQiLCJ0b1VwcGVyQ2FzZSIsInJlbW92ZWRGaXJzdExldHRlciIsInNsaWNlIiwib3JpZ2luYWxUZXh0IiwicHN0YWdnZXIiLCJvblRhZ3NDaGFuZ2VkIiwidGFnTGlzdCIsIm9uUmVzZXRUYWdzIiwiaW5wdXRQbGFjZWhvbGRlciIsImNsb3NpbmdDcm9zcyIsInN3aXRjaFNvcnQiLCJzd2l0Y2hUbyIsImlzQWxyZWFkeURpc3BsYXllZCIsInN3aXRjaFNvcnRpbmdEaXNwbGF5VG8iLCJzZWVNb3JlIiwicmVwbGFjZUZpcnN0V29yZEJ5IiwiZXhwbG9kZWRUZXh0IiwiJHNob3J0TGlzdHMiLCJzaG9ydExpc3RzIiwibW9kdWxlc0NvdW50Iiwic2VsZWN0b3JUb1RvZ2dsZSIsIk1vZHVsZUxvYWRlciIsImhhbmRsZUltcG9ydCIsImhhbmRsZUV2ZW50cyIsInZhbGlkYXRlIiwibW9kdWxlUG9wcGluIiwiZ2V0IiwiaHJlZiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7a0JBMEN3QkEsWTs7OztBQTFDeEI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Y0F5QllDLE07SUFBTEMsQyxXQUFBQSxDOztBQUVQOzs7Ozs7Ozs7Ozs7Ozs7O0FBZWUsU0FBU0YsWUFBVCxDQUFzQkcsTUFBdEIsRUFBOEJDLGVBQTlCLEVBQStDQyxjQUEvQyxFQUErRDtBQUFBOztBQUM1RTtBQUQ0RSxNQUVyRUMsRUFGcUUsR0FFckRILE1BRnFELENBRXJFRyxFQUZxRTtBQUFBLE1BRWpFQyxRQUZpRSxHQUVyREosTUFGcUQsQ0FFakVJLFFBRmlFOztBQUc1RSxPQUFLQyxLQUFMLEdBQWFDLE1BQU1OLE1BQU4sQ0FBYjs7QUFFQTtBQUNBLE9BQUtPLE1BQUwsR0FBY1IsRUFBRSxLQUFLTSxLQUFMLENBQVdHLFNBQWIsQ0FBZDs7QUFFQSxPQUFLQyxJQUFMLEdBQVksWUFBTTtBQUNoQixVQUFLRixNQUFMLENBQVlGLEtBQVo7QUFDRCxHQUZEOztBQUlBLE9BQUtBLEtBQUwsQ0FBV0ssYUFBWCxDQUF5QkMsZ0JBQXpCLENBQTBDLE9BQTFDLEVBQW1EVixlQUFuRDs7QUFFQSxPQUFLTSxNQUFMLENBQVlGLEtBQVosQ0FBa0I7QUFDaEJPLGNBQVVSLFdBQVcsSUFBWCxHQUFrQixRQURaO0FBRWhCUyxjQUFVVCxhQUFhVSxTQUFiLEdBQXlCVixRQUF6QixHQUFvQyxJQUY5QjtBQUdoQkEsY0FBVUEsYUFBYVUsU0FBYixHQUF5QlYsUUFBekIsR0FBb0MsSUFIOUI7QUFJaEJLLFVBQU07QUFKVSxHQUFsQjs7QUFPQSxPQUFLRixNQUFMLENBQVlRLEVBQVosQ0FBZSxpQkFBZixFQUFrQyxZQUFNO0FBQ3RDQyxhQUFTQyxhQUFULE9BQTJCZCxFQUEzQixFQUFpQ2UsTUFBakM7QUFDQSxRQUFJaEIsY0FBSixFQUFvQjtBQUNsQkE7QUFDRDtBQUNGLEdBTEQ7O0FBT0FjLFdBQVNHLElBQVQsQ0FBY0MsV0FBZCxDQUEwQixLQUFLZixLQUFMLENBQVdHLFNBQXJDO0FBQ0Q7O0FBRUQ7Ozs7OztBQU1BLFNBQVNGLEtBQVQsT0FRRztBQUFBOztBQUFBLHFCQVBESCxFQU9DO0FBQUEsTUFQREEsRUFPQywyQkFQSSxlQU9KO0FBQUEsTUFORGtCLFlBTUMsUUFOREEsWUFNQztBQUFBLGlDQUxEQyxjQUtDO0FBQUEsTUFMREEsY0FLQyx1Q0FMZ0IsRUFLaEI7QUFBQSxtQ0FKREMsZ0JBSUM7QUFBQSxNQUpEQSxnQkFJQyx5Q0FKa0IsT0FJbEI7QUFBQSxtQ0FIREMsa0JBR0M7QUFBQSxNQUhEQSxrQkFHQyx5Q0FIb0IsUUFHcEI7QUFBQSxtQ0FGREMsa0JBRUM7QUFBQSxNQUZEQSxrQkFFQyx5Q0FGb0IsYUFFcEI7QUFBQSxnQ0FEREMsYUFDQztBQUFBLE1BRERBLGFBQ0Msc0NBRGUsRUFDZjs7QUFDRCxNQUFNckIsUUFBUSxFQUFkOztBQUVBO0FBQ0FBLFFBQU1HLFNBQU4sR0FBa0JRLFNBQVNXLGFBQVQsQ0FBdUIsS0FBdkIsQ0FBbEI7QUFDQXRCLFFBQU1HLFNBQU4sQ0FBZ0JvQixTQUFoQixDQUEwQkMsR0FBMUIsQ0FBOEIsT0FBOUIsRUFBdUMsTUFBdkM7QUFDQXhCLFFBQU1HLFNBQU4sQ0FBZ0JMLEVBQWhCLEdBQXFCQSxFQUFyQjs7QUFFQTtBQUNBRSxRQUFNeUIsTUFBTixHQUFlZCxTQUFTVyxhQUFULENBQXVCLEtBQXZCLENBQWY7QUFDQXRCLFFBQU15QixNQUFOLENBQWFGLFNBQWIsQ0FBdUJDLEdBQXZCLENBQTJCLGNBQTNCOztBQUVBO0FBQ0F4QixRQUFNMEIsT0FBTixHQUFnQmYsU0FBU1csYUFBVCxDQUF1QixLQUF2QixDQUFoQjtBQUNBdEIsUUFBTTBCLE9BQU4sQ0FBY0gsU0FBZCxDQUF3QkMsR0FBeEIsQ0FBNEIsZUFBNUI7O0FBRUE7QUFDQXhCLFFBQU0yQixNQUFOLEdBQWVoQixTQUFTVyxhQUFULENBQXVCLEtBQXZCLENBQWY7QUFDQXRCLFFBQU0yQixNQUFOLENBQWFKLFNBQWIsQ0FBdUJDLEdBQXZCLENBQTJCLGNBQTNCOztBQUVBO0FBQ0EsTUFBSVIsWUFBSixFQUFrQjtBQUNoQmhCLFVBQU00QixLQUFOLEdBQWNqQixTQUFTVyxhQUFULENBQXVCLElBQXZCLENBQWQ7QUFDQXRCLFVBQU00QixLQUFOLENBQVlMLFNBQVosQ0FBc0JDLEdBQXRCLENBQTBCLGFBQTFCO0FBQ0F4QixVQUFNNEIsS0FBTixDQUFZQyxTQUFaLEdBQXdCYixZQUF4QjtBQUNEOztBQUVEO0FBQ0FoQixRQUFNOEIsU0FBTixHQUFrQm5CLFNBQVNXLGFBQVQsQ0FBdUIsUUFBdkIsQ0FBbEI7QUFDQXRCLFFBQU04QixTQUFOLENBQWdCUCxTQUFoQixDQUEwQkMsR0FBMUIsQ0FBOEIsT0FBOUI7QUFDQXhCLFFBQU04QixTQUFOLENBQWdCQyxZQUFoQixDQUE2QixNQUE3QixFQUFxQyxRQUFyQztBQUNBL0IsUUFBTThCLFNBQU4sQ0FBZ0JFLE9BQWhCLENBQXdCQyxPQUF4QixHQUFrQyxPQUFsQztBQUNBakMsUUFBTThCLFNBQU4sQ0FBZ0JELFNBQWhCLEdBQTRCLEdBQTVCOztBQUVBO0FBQ0E3QixRQUFNYyxJQUFOLEdBQWFILFNBQVNXLGFBQVQsQ0FBdUIsS0FBdkIsQ0FBYjtBQUNBdEIsUUFBTWMsSUFBTixDQUFXUyxTQUFYLENBQXFCQyxHQUFyQixDQUF5QixZQUF6QixFQUF1QyxXQUF2QyxFQUFvRCxvQkFBcEQ7O0FBRUE7QUFDQXhCLFFBQU1rQyxPQUFOLEdBQWdCdkIsU0FBU1csYUFBVCxDQUF1QixHQUF2QixDQUFoQjtBQUNBdEIsUUFBTWtDLE9BQU4sQ0FBY1gsU0FBZCxDQUF3QkMsR0FBeEIsQ0FBNEIsaUJBQTVCO0FBQ0F4QixRQUFNa0MsT0FBTixDQUFjTCxTQUFkLEdBQTBCWixjQUExQjs7QUFFQTtBQUNBakIsUUFBTW1DLE1BQU4sR0FBZXhCLFNBQVNXLGFBQVQsQ0FBdUIsS0FBdkIsQ0FBZjtBQUNBdEIsUUFBTW1DLE1BQU4sQ0FBYVosU0FBYixDQUF1QkMsR0FBdkIsQ0FBMkIsY0FBM0I7O0FBRUE7QUFDQXhCLFFBQU1vQyxXQUFOLEdBQW9CekIsU0FBU1csYUFBVCxDQUF1QixRQUF2QixDQUFwQjtBQUNBdEIsUUFBTW9DLFdBQU4sQ0FBa0JMLFlBQWxCLENBQStCLE1BQS9CLEVBQXVDLFFBQXZDO0FBQ0EvQixRQUFNb0MsV0FBTixDQUFrQmIsU0FBbEIsQ0FBNEJDLEdBQTVCLENBQWdDLEtBQWhDLEVBQXVDLHVCQUF2QyxFQUFnRSxRQUFoRTtBQUNBeEIsUUFBTW9DLFdBQU4sQ0FBa0JKLE9BQWxCLENBQTBCQyxPQUExQixHQUFvQyxPQUFwQztBQUNBakMsUUFBTW9DLFdBQU4sQ0FBa0JQLFNBQWxCLEdBQThCWCxnQkFBOUI7O0FBRUE7QUFDQWxCLFFBQU1LLGFBQU4sR0FBc0JNLFNBQVNXLGFBQVQsQ0FBdUIsUUFBdkIsQ0FBdEI7QUFDQXRCLFFBQU1LLGFBQU4sQ0FBb0IwQixZQUFwQixDQUFpQyxNQUFqQyxFQUF5QyxRQUF6QztBQUNBL0IsUUFBTUssYUFBTixDQUFvQmtCLFNBQXBCLENBQThCQyxHQUE5QixDQUFrQyxLQUFsQyxFQUF5Q0osa0JBQXpDLEVBQTZELFFBQTdELEVBQXVFLG9CQUF2RTtBQUNBcEIsUUFBTUssYUFBTixDQUFvQjJCLE9BQXBCLENBQTRCQyxPQUE1QixHQUFzQyxPQUF0QztBQUNBakMsUUFBTUssYUFBTixDQUFvQndCLFNBQXBCLEdBQWdDVixrQkFBaEM7O0FBRUE7QUFDQSxNQUFJSCxZQUFKLEVBQWtCO0FBQ2hCaEIsVUFBTTJCLE1BQU4sQ0FBYVUsTUFBYixDQUFvQnJDLE1BQU00QixLQUExQixFQUFpQzVCLE1BQU04QixTQUF2QztBQUNELEdBRkQsTUFFTztBQUNMOUIsVUFBTTJCLE1BQU4sQ0FBYVosV0FBYixDQUF5QmYsTUFBTThCLFNBQS9CO0FBQ0Q7O0FBRUQ5QixRQUFNYyxJQUFOLENBQVdDLFdBQVgsQ0FBdUJmLE1BQU1rQyxPQUE3QjtBQUNBLHlCQUFNQyxNQUFOLEVBQWFFLE1BQWIsdUJBQW9CckMsTUFBTW9DLFdBQTFCLDBDQUEwQ2YsYUFBMUMsSUFBeURyQixNQUFNSyxhQUEvRDtBQUNBTCxRQUFNMEIsT0FBTixDQUFjVyxNQUFkLENBQXFCckMsTUFBTTJCLE1BQTNCLEVBQW1DM0IsTUFBTWMsSUFBekMsRUFBK0NkLE1BQU1tQyxNQUFyRDtBQUNBbkMsUUFBTXlCLE1BQU4sQ0FBYVYsV0FBYixDQUF5QmYsTUFBTTBCLE9BQS9CO0FBQ0ExQixRQUFNRyxTQUFOLENBQWdCWSxXQUFoQixDQUE0QmYsTUFBTXlCLE1BQWxDOztBQUVBLFNBQU96QixLQUFQO0FBQ0QsQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDMUlEOzs7Ozs7Y0FFWVAsTTtJQUFMQyxDLFdBQUFBLEMsRUExQlA7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUE0QkEsSUFBTTRDLFVBQVU7QUFDZDVCLElBRGMsY0FDWDZCLFNBRFcsRUFDQUMsUUFEQSxFQUNVQyxPQURWLEVBQ21CO0FBQy9COUIsYUFBU0wsZ0JBQVQsQ0FBMEJpQyxTQUExQixFQUFxQyxVQUFDRyxLQUFELEVBQVc7QUFDOUMsVUFBSSxPQUFPRCxPQUFQLEtBQW1CLFdBQXZCLEVBQW9DO0FBQ2xDRCxpQkFBU0csSUFBVCxDQUFjRixPQUFkLEVBQXVCQyxLQUF2QjtBQUNELE9BRkQsTUFFTztBQUNMRixpQkFBU0UsS0FBVDtBQUNEO0FBQ0YsS0FORDtBQU9ELEdBVGE7QUFXZEUsV0FYYyxxQkFXSkwsU0FYSSxFQVdPTSxTQVhQLEVBV2tCO0FBQzlCLFFBQU1ILFFBQVEvQixTQUFTbUMsV0FBVCxDQUFxQkQsU0FBckIsQ0FBZDtBQUNBO0FBQ0FILFVBQU1LLFNBQU4sQ0FBZ0JSLFNBQWhCLEVBQTJCLElBQTNCLEVBQWlDLElBQWpDO0FBQ0E1QixhQUFTcUMsYUFBVCxDQUF1Qk4sS0FBdkI7QUFDRDtBQWhCYSxDQUFoQjs7QUFtQkE7Ozs7OztJQUtxQk8sVTtBQUNuQix3QkFBYztBQUFBOztBQUNaO0FBQ0EsU0FBS0MsNEJBQUwsR0FBb0MsNEJBQXBDO0FBQ0EsU0FBS0MsbUNBQUwsR0FBMkMsbUNBQTNDO0FBQ0EsU0FBS0Msa0NBQUwsR0FBMEMsa0NBQTFDO0FBQ0EsU0FBS0MscUNBQUwsR0FBNkMscUNBQTdDO0FBQ0EsU0FBS0MsbUNBQUwsR0FBMkMsbUNBQTNDO0FBQ0EsU0FBS0Msd0NBQUwsR0FBZ0QseUNBQWhEO0FBQ0EsU0FBS0MseUNBQUwsR0FBaUQsMENBQWpEO0FBQ0EsU0FBS0MsaUNBQUwsR0FBeUMsaUNBQXpDO0FBQ0EsU0FBS0Msa0NBQUwsR0FBMEMsbUNBQTFDO0FBQ0EsU0FBS0Msc0JBQUwsR0FBOEIsbUJBQTlCO0FBQ0EsU0FBS0Msc0JBQUwsR0FBOEIsbUJBQTlCO0FBQ0EsU0FBS0MseUJBQUwsR0FBaUMsaUJBQWpDOztBQUVBO0FBQ0EsU0FBS0Msb0NBQUwsR0FBNEMsK0JBQTVDO0FBQ0EsU0FBS0Msa0NBQUwsR0FBMEMsNkJBQTFDO0FBQ0EsU0FBS0Msc0NBQUwsR0FBOEMsaUNBQTlDO0FBQ0EsU0FBS0MsbUJBQUwsR0FBMkIsaUJBQTNCOztBQUVBLFNBQUtDLGlCQUFMO0FBQ0Q7Ozs7d0NBRW1CO0FBQ2xCLFVBQU1DLE9BQU8sSUFBYjs7QUFFQXpFLFFBQUVpQixRQUFGLEVBQVlELEVBQVosQ0FBZSxPQUFmLEVBQXdCLEtBQUt1RCxtQkFBN0IsRUFBa0QsWUFBWTtBQUM1RCxZQUFNRyxNQUFNMUUsRUFDVnlFLEtBQUtILHNDQURLLEVBRVZ0RSw2Q0FBMENBLEVBQUUsSUFBRixFQUFRMkUsSUFBUixDQUFhLGdCQUFiLENBQTFDLFNBRlUsQ0FBWjs7QUFLQSxZQUFJM0UsRUFBRSxJQUFGLEVBQVE0RSxJQUFSLENBQWEsU0FBYixNQUE0QixJQUFoQyxFQUFzQztBQUNwQ0YsY0FBSUMsSUFBSixDQUFTLGVBQVQsRUFBMEIsTUFBMUI7QUFDRCxTQUZELE1BRU87QUFDTEQsY0FBSUcsVUFBSixDQUFlLGVBQWY7QUFDRDtBQUNGLE9BWEQ7O0FBYUE3RSxRQUFFaUIsUUFBRixFQUFZRCxFQUFaLENBQWUsT0FBZixFQUF3QixLQUFLeUMsbUNBQTdCLEVBQWtFLFlBQVk7QUFDNUUsWUFBSXpELEVBQUUsb0JBQUYsRUFBd0I4RSxNQUE1QixFQUFvQztBQUNsQzlFLFlBQUUsb0JBQUYsRUFBd0JNLEtBQXhCLENBQThCLE1BQTlCO0FBQ0Q7O0FBRUQsZUFDRW1FLEtBQUtNLGdCQUFMLENBQXNCLFNBQXRCLEVBQWlDLElBQWpDLEtBQ0dOLEtBQUtPLGFBQUwsQ0FBbUIsU0FBbkIsRUFBOEIsSUFBOUIsQ0FESCxJQUVHUCxLQUFLUSxtQkFBTCxDQUF5QixTQUF6QixFQUFvQ2pGLEVBQUUsSUFBRixDQUFwQyxDQUhMO0FBS0QsT0FWRDs7QUFZQUEsUUFBRWlCLFFBQUYsRUFBWUQsRUFBWixDQUFlLE9BQWYsRUFBd0IsS0FBSzBDLGtDQUE3QixFQUFpRSxZQUFZO0FBQzNFLGVBQ0VlLEtBQUtNLGdCQUFMLENBQXNCLFFBQXRCLEVBQWdDLElBQWhDLEtBQ0dOLEtBQUtPLGFBQUwsQ0FBbUIsUUFBbkIsRUFBNkIsSUFBN0IsQ0FESCxJQUVHUCxLQUFLUSxtQkFBTCxDQUF5QixRQUF6QixFQUFtQ2pGLEVBQUUsSUFBRixDQUFuQyxDQUhMO0FBS0QsT0FORDs7QUFRQUEsUUFBRWlCLFFBQUYsRUFBWUQsRUFBWixDQUFlLE9BQWYsRUFBd0IsS0FBSzJDLHFDQUE3QixFQUFvRSxZQUFZO0FBQzlFLGVBQ0VjLEtBQUtNLGdCQUFMLENBQXNCLFdBQXRCLEVBQW1DLElBQW5DLEtBQ0dOLEtBQUtPLGFBQUwsQ0FBbUIsV0FBbkIsRUFBZ0MsSUFBaEMsQ0FESCxJQUVHUCxLQUFLUSxtQkFBTCxDQUF5QixXQUF6QixFQUFzQ2pGLEVBQUUsSUFBRixDQUF0QyxDQUhMO0FBS0QsT0FORDs7QUFRQUEsUUFBRWlCLFFBQUYsRUFBWUQsRUFBWixDQUFlLE9BQWYsRUFBd0IsS0FBSzRDLG1DQUE3QixFQUFrRSxZQUFZO0FBQzVFLGVBQ0VhLEtBQUtNLGdCQUFMLENBQXNCLFNBQXRCLEVBQWlDLElBQWpDLEtBQ0dOLEtBQUtPLGFBQUwsQ0FBbUIsU0FBbkIsRUFBOEIsSUFBOUIsQ0FESCxJQUVHUCxLQUFLUSxtQkFBTCxDQUF5QixTQUF6QixFQUFvQ2pGLEVBQUUsSUFBRixDQUFwQyxDQUhMO0FBS0QsT0FORDs7QUFRQUEsUUFBRWlCLFFBQUYsRUFBWUQsRUFBWixDQUFlLE9BQWYsRUFBd0IsS0FBSzZDLHdDQUE3QixFQUF1RSxZQUFZO0FBQ2pGLGVBQ0VZLEtBQUtNLGdCQUFMLENBQXNCLGVBQXRCLEVBQXVDLElBQXZDLEtBQ0dOLEtBQUtPLGFBQUwsQ0FBbUIsZUFBbkIsRUFBb0MsSUFBcEMsQ0FESCxJQUVHUCxLQUFLUSxtQkFBTCxDQUF5QixlQUF6QixFQUEwQ2pGLEVBQUUsSUFBRixDQUExQyxDQUhMO0FBS0QsT0FORDs7QUFRQUEsUUFBRWlCLFFBQUYsRUFBWUQsRUFBWixDQUFlLE9BQWYsRUFBd0IsS0FBSzhDLHlDQUE3QixFQUF3RSxZQUFZO0FBQ2xGLGVBQ0VXLEtBQUtNLGdCQUFMLENBQXNCLGdCQUF0QixFQUF3QyxJQUF4QyxLQUNHTixLQUFLTyxhQUFMLENBQW1CLGdCQUFuQixFQUFxQyxJQUFyQyxDQURILElBRUdQLEtBQUtRLG1CQUFMLENBQXlCLGdCQUF6QixFQUEyQ2pGLEVBQUUsSUFBRixDQUEzQyxDQUhMO0FBS0QsT0FORDs7QUFRQUEsUUFBRWlCLFFBQUYsRUFBWUQsRUFBWixDQUFlLE9BQWYsRUFBd0IsS0FBSytDLGlDQUE3QixFQUFnRSxZQUFZO0FBQzFFLGVBQ0VVLEtBQUtNLGdCQUFMLENBQXNCLE9BQXRCLEVBQStCLElBQS9CLEtBQ0dOLEtBQUtPLGFBQUwsQ0FBbUIsT0FBbkIsRUFBNEIsSUFBNUIsQ0FESCxJQUVHUCxLQUFLUSxtQkFBTCxDQUF5QixPQUF6QixFQUFrQ2pGLEVBQUUsSUFBRixDQUFsQyxDQUhMO0FBS0QsT0FORDs7QUFRQUEsUUFBRWlCLFFBQUYsRUFBWUQsRUFBWixDQUFlLE9BQWYsRUFBd0IsS0FBS2dELGtDQUE3QixFQUFpRSxVQUFVaEIsS0FBVixFQUFpQjtBQUFBOztBQUNoRkEsY0FBTWtDLGNBQU47QUFDQSxZQUFNNUUsUUFBUU4sUUFBTUEsRUFBRSxJQUFGLEVBQVFtRixJQUFSLENBQWEsZUFBYixDQUFOLENBQWQ7QUFDQSxZQUFNQyxvQkFBb0JyRixPQUFPc0YsaUJBQWpDOztBQUVBLFlBQUkvRSxNQUFNd0UsTUFBTixLQUFpQixDQUFyQixFQUF3QjtBQUN0QjtBQUNBLGNBQU1RLGtCQUFrQnJFLFNBQVNXLGFBQVQsQ0FBdUIsR0FBdkIsQ0FBeEI7QUFDQTBELDBCQUFnQnpELFNBQWhCLENBQTBCQyxHQUExQixDQUE4QixLQUE5QixFQUFxQyxhQUFyQyxFQUFvRCxRQUFwRDtBQUNBd0QsMEJBQWdCakQsWUFBaEIsQ0FBNkIsTUFBN0IsRUFBcUN0QyxPQUFPd0YsVUFBUCxDQUFrQkMsZUFBdkQ7QUFDQUYsMEJBQWdCbkQsU0FBaEIsR0FBNEJwQyxPQUFPMEYsa0JBQVAsQ0FBMEJDLDRCQUF0RDs7QUFFQSxjQUFNQyxxQkFBcUIsSUFBSTdGLGVBQUosQ0FDekI7QUFDRU0sZ0JBQUksNkJBRE47QUFFRWtCLDBCQUFjdkIsT0FBTzBGLGtCQUFQLENBQTBCRyw0QkFGMUM7QUFHRXBFLDhCQUFrQnpCLE9BQU8wRixrQkFBUCxDQUEwQkksdUJBSDlDO0FBSUVwRSxnQ0FBb0IyRCxvQkFDaEJyRixPQUFPMEYsa0JBQVAsQ0FBMEJLLHdCQURWLEdBRWhCL0YsT0FBTzBGLGtCQUFQLENBQTBCTSx1QkFOaEM7QUFPRXJFLGdDQUFvQjBELG9CQUFvQixhQUFwQixHQUFvQyxlQVAxRDtBQVFFN0QsNEJBQWdCNkQsb0JBQW9CLEVBQXBCLEdBQXlCckYsT0FBTzBGLGtCQUFQLENBQTBCTywrQkFSckU7QUFTRTNGLHNCQUFVLElBVFo7QUFVRXNCLDJCQUFleUQsb0JBQW9CLEVBQXBCLEdBQXlCLENBQUNFLGVBQUQ7QUFWMUMsV0FEeUIsRUFjekI7QUFBQSxtQkFBTWIsS0FBS00sZ0JBQUwsQ0FBc0IsUUFBdEIsRUFBZ0MsS0FBaEMsS0FDRE4sS0FBS08sYUFBTCxDQUFtQixRQUFuQixFQUE2QixLQUE3QixDQURDLElBRURQLEtBQUtRLG1CQUFMLENBQXlCLFFBQXpCLEVBQW1DakYsRUFBRSxLQUFGLENBQW5DLENBRkw7QUFBQSxXQWR5QixDQUEzQjs7QUFtQkEyRiw2QkFBbUJqRixJQUFuQjtBQUNELFNBM0JELE1BMkJPO0FBQ0wsaUJBQ0UrRCxLQUFLTSxnQkFBTCxDQUFzQixRQUF0QixFQUFnQyxJQUFoQyxLQUNHTixLQUFLTyxhQUFMLENBQW1CLFFBQW5CLEVBQTZCLElBQTdCLENBREgsSUFFR1AsS0FBS1EsbUJBQUwsQ0FBeUIsUUFBekIsRUFBbUNqRixFQUFFLElBQUYsQ0FBbkMsQ0FITDtBQUtEOztBQUVELGVBQU8sS0FBUDtBQUNELE9BekNEOztBQTJDQUEsUUFBRWlCLFFBQUYsRUFBWUQsRUFBWixDQUFlLE9BQWYsRUFBd0IsS0FBS29ELG9DQUE3QixFQUFtRSxZQUFZO0FBQzdFLGVBQU9LLEtBQUtRLG1CQUFMLENBQ0wsU0FESyxFQUVMakYsRUFDRXlFLEtBQUtiLG1DQURQLEVBRUU1RCw2Q0FBMENBLEVBQUUsSUFBRixFQUFRMkUsSUFBUixDQUFhLGdCQUFiLENBQTFDLFNBRkYsQ0FGSyxDQUFQO0FBT0QsT0FSRDs7QUFVQTNFLFFBQUVpQixRQUFGLEVBQVlELEVBQVosQ0FBZSxPQUFmLEVBQXdCLEtBQUtxRCxrQ0FBN0IsRUFBaUUsWUFBWTtBQUMzRSxlQUFPSSxLQUFLUSxtQkFBTCxDQUNMLE9BREssRUFFTGpGLEVBQ0V5RSxLQUFLVixpQ0FEUCxFQUVFL0QsNkNBQTBDQSxFQUFFLElBQUYsRUFBUTJFLElBQVIsQ0FBYSxnQkFBYixDQUExQyxTQUZGLENBRkssQ0FBUDtBQU9ELE9BUkQ7O0FBVUEzRSxRQUFFaUIsUUFBRixFQUFZRCxFQUFaLENBQWUsT0FBZixFQUF3QixLQUFLc0Qsc0NBQTdCLEVBQXFFLFVBQUMyQixDQUFELEVBQU87QUFDMUVqRyxVQUFFaUcsRUFBRUMsTUFBSixFQUNHQyxPQURILENBQ1csUUFEWCxFQUVHbkYsRUFGSCxDQUVNLGlCQUZOLEVBRXlCO0FBQUEsaUJBQU15RCxLQUFLUSxtQkFBTCxDQUMzQixXQUQyQixFQUUzQmpGLEVBQ0V5RSxLQUFLZCxxQ0FEUCxFQUVFM0QsNkNBQTBDQSxFQUFFaUcsRUFBRUMsTUFBSixFQUFZdkIsSUFBWixDQUFpQixnQkFBakIsQ0FBMUMsU0FGRixDQUYyQixFQU0zQjNFLEVBQUVpRyxFQUFFQyxNQUFKLEVBQVl2QixJQUFaLENBQWlCLGVBQWpCLENBTjJCLENBQU47QUFBQSxTQUZ6QjtBQVdELE9BWkQ7QUFhRDs7OzRDQUV1QjtBQUN0QixVQUFJM0UsRUFBRSxLQUFLaUUsc0JBQVAsRUFBK0JhLE1BQW5DLEVBQTJDO0FBQ3pDLGVBQU8sS0FBS2Isc0JBQVo7QUFDRDs7QUFFRCxhQUFPLEtBQUtDLHNCQUFaO0FBQ0Q7OztrQ0FFYWtDLE0sRUFBUUMsTyxFQUFTO0FBQzdCLFVBQU0vRixRQUFRTixRQUFNQSxFQUFFcUcsT0FBRixFQUFXbEIsSUFBWCxDQUFnQixlQUFoQixDQUFOLENBQWQ7O0FBRUEsVUFBSTdFLE1BQU13RSxNQUFOLEtBQWlCLENBQXJCLEVBQXdCO0FBQ3RCLGVBQU8sSUFBUDtBQUNEOztBQUVEeEUsWUFBTWdHLEtBQU4sR0FBY2hHLEtBQWQsQ0FBb0IsTUFBcEI7O0FBRUEsYUFBTyxLQUFQLENBVDZCLENBU2Y7QUFDZjs7QUFFRDs7Ozs7Ozs7O3VDQU1tQmlHLE0sRUFBUTtBQUN6QixVQUFNQyxPQUFPLElBQWI7QUFDQSxVQUFNbEcsUUFBUSxLQUFLbUcsOEJBQUwsQ0FBb0NGLE1BQXBDLENBQWQ7O0FBRUFqRyxZQUNHb0csSUFESCxDQUNRLGtCQURSLEVBRUdDLEdBRkgsQ0FFTyxPQUZQLEVBR0czRixFQUhILENBR00sT0FITixFQUdlLFlBQU07QUFDakI7QUFDQSxZQUFNNEYsZ0JBQWdCNUcsRUFDcEJ3RyxLQUFLL0MsbUNBRGUsb0NBRVk4QyxPQUFPTSxNQUFQLENBQWNDLFVBQWQsQ0FBeUJDLElBRnJDLFFBQXRCOztBQUtBLFlBQU1DLE9BQU9KLGNBQWNLLE1BQWQsQ0FBcUIsTUFBckIsQ0FBYjtBQUNBakgsVUFBRSxTQUFGLEVBQ0cyRSxJQURILENBQ1E7QUFDSnVDLGdCQUFNLFFBREY7QUFFSkMsaUJBQU8sR0FGSDtBQUdKSixnQkFBTTtBQUhGLFNBRFIsRUFNR0ssUUFOSCxDQU1ZSixJQU5aOztBQVFBSixzQkFBY1MsS0FBZDtBQUNBL0csY0FBTUEsS0FBTixDQUFZLE1BQVo7QUFDRCxPQXJCSDs7QUF1QkFBLFlBQU1BLEtBQU47QUFDRDs7O21EQUU4QmlHLE0sRUFBUTtBQUNyQyxVQUFNakcsUUFBUU4sRUFBRSxvQkFBRixDQUFkO0FBQ0EsVUFBTTZHLFNBQVNOLE9BQU9NLE1BQVAsQ0FBY0MsVUFBN0I7O0FBRUEsVUFBSVAsT0FBT2Usb0JBQVAsS0FBZ0MsYUFBaEMsSUFBaUQsQ0FBQ2hILE1BQU13RSxNQUE1RCxFQUFvRTtBQUNsRSxlQUFPLEtBQVA7QUFDRDs7QUFFRCxVQUFNeUMsYUFBYVYsT0FBT1csV0FBUCxDQUFtQkMsTUFBbkIsR0FBNEIsU0FBNUIsR0FBd0MsU0FBM0Q7O0FBRUEsVUFBSVosT0FBT1csV0FBUCxDQUFtQkUsVUFBbkIsQ0FBOEJDLFFBQWxDLEVBQTRDO0FBQzFDckgsY0FBTW9HLElBQU4sQ0FBVywwQkFBWCxFQUF1Q2hHLElBQXZDO0FBQ0FKLGNBQU1vRyxJQUFOLENBQVcsMkJBQVgsRUFBd0NrQixJQUF4QztBQUNELE9BSEQsTUFHTztBQUNMdEgsY0FBTW9HLElBQU4sQ0FBVywwQkFBWCxFQUF1Q2tCLElBQXZDO0FBQ0F0SCxjQUFNb0csSUFBTixDQUFXLDJCQUFYLEVBQXdDaEcsSUFBeEM7QUFDQUosY0FDR29HLElBREgsQ0FDUSxjQURSLEVBRUcvQixJQUZILENBRVEsTUFGUixFQUVnQmtDLE9BQU9nQixHQUZ2QixFQUdHQyxNQUhILENBR1VqQixPQUFPZ0IsR0FBUCxLQUFlLElBSHpCO0FBSUQ7O0FBRUR2SCxZQUFNb0csSUFBTixDQUFXLGNBQVgsRUFBMkIvQixJQUEzQixDQUFnQyxFQUFDb0QsS0FBS2xCLE9BQU9tQixHQUFiLEVBQWtCQyxLQUFLcEIsT0FBT0UsSUFBOUIsRUFBaEM7QUFDQXpHLFlBQU1vRyxJQUFOLENBQVcsZUFBWCxFQUE0QndCLElBQTVCLENBQWlDckIsT0FBT3NCLFdBQXhDO0FBQ0E3SCxZQUFNb0csSUFBTixDQUFXLGlCQUFYLEVBQThCd0IsSUFBOUIsQ0FBbUNyQixPQUFPdUIsTUFBMUM7QUFDQTlILFlBQ0dvRyxJQURILENBQ1EsZ0JBRFIsRUFFRy9CLElBRkgsQ0FFUSxPQUZSLFlBRXlCNEMsVUFGekIsRUFHR1csSUFISCxDQUdRckIsT0FBT1csV0FBUCxDQUFtQkMsTUFBbkIsR0FBNEIsSUFBNUIsR0FBbUMsSUFIM0M7QUFJQW5ILFlBQU1vRyxJQUFOLENBQVcsa0JBQVgsRUFBK0IvQixJQUEvQixDQUFvQyxPQUFwQyxtQkFBNEQ0QyxVQUE1RDtBQUNBakgsWUFBTW9HLElBQU4sQ0FBVyxzQkFBWCxFQUFtQ3dCLElBQW5DLENBQXdDckIsT0FBT1csV0FBUCxDQUFtQmhGLE9BQTNEOztBQUVBLGFBQU9sQyxLQUFQO0FBQ0Q7OztxQ0FFZ0I4RixNLEVBQVFDLE8sRUFBUztBQUNoQyxVQUFNckQsUUFBUXFGLE1BQU1BLENBQUNDLEtBQVAsQ0FBYSwwQkFBYixDQUFkOztBQUVBdEksUUFBRXFHLE9BQUYsRUFBV2tDLE9BQVgsQ0FBbUJ2RixLQUFuQixFQUEwQixDQUFDb0QsTUFBRCxDQUExQjtBQUNBLFVBQUlwRCxNQUFNd0Ysb0JBQU4sT0FBaUMsS0FBakMsSUFBMEN4RixNQUFNeUYsNkJBQU4sT0FBMEMsS0FBeEYsRUFBK0Y7QUFDN0YsZUFBTyxLQUFQLENBRDZGLENBQy9FO0FBQ2Y7O0FBRUQsYUFBT3pGLE1BQU11RCxNQUFOLEtBQWlCLEtBQXhCLENBUmdDLENBUUQ7QUFDaEM7Ozt3Q0FFbUJILE0sRUFBUUMsTyxFQUFTcUMsYSxFQUFlQyxpQixFQUFtQjdGLFEsRUFBVTtBQUMvRSxVQUFNMkIsT0FBTyxJQUFiO0FBQ0EsVUFBTW1FLGVBQWV2QyxRQUFRd0MsT0FBUixDQUFnQixLQUFLMUUseUJBQXJCLENBQXJCO0FBQ0EsVUFBTTZDLE9BQU9YLFFBQVF3QyxPQUFSLENBQWdCLE1BQWhCLENBQWI7QUFDQSxVQUFNQyxhQUFhOUksRUFBRSx1RUFBRixDQUFuQjtBQUNBLFVBQU02SCxhQUFXOUgsT0FBT2dKLFFBQVAsQ0FBZ0JDLElBQTNCLEdBQWtDaEMsS0FBS3JDLElBQUwsQ0FBVSxRQUFWLENBQXhDO0FBQ0EsVUFBTXNFLGVBQWVqQyxLQUFLa0MsY0FBTCxFQUFyQjs7QUFFQSxVQUFJUixrQkFBa0IsTUFBbEIsSUFBNEJBLGtCQUFrQixJQUFsRCxFQUF3RDtBQUN0RE8scUJBQWFFLElBQWIsQ0FBa0IsRUFBQ3BDLE1BQU0sd0JBQVAsRUFBaUNJLE9BQU8sSUFBeEMsRUFBbEI7QUFDRDtBQUNELFVBQUl3QixzQkFBc0IsTUFBdEIsSUFBZ0NBLHNCQUFzQixJQUExRCxFQUFnRTtBQUM5RE0scUJBQWFFLElBQWIsQ0FBa0IsRUFBQ3BDLE1BQU0saUNBQVAsRUFBMENJLE9BQU8sQ0FBakQsRUFBbEI7QUFDRDs7QUFFRG5ILFFBQUVvSixJQUFGLENBQU87QUFDTHZCLGdCQURLO0FBRUx3QixrQkFBVSxNQUZMO0FBR0xDLGdCQUFRLE1BSEg7QUFJTG5FLGNBQU04RCxZQUpEO0FBS0xNLGtCQUxLLHdCQUtRO0FBQ1hYLHVCQUFhaEIsSUFBYjtBQUNBZ0IsdUJBQWFZLEtBQWIsQ0FBbUJWLFVBQW5CO0FBQ0Q7QUFSSSxPQUFQLEVBVUdXLElBVkgsQ0FVUSxVQUFDbEQsTUFBRCxFQUFZO0FBQ2hCLFlBQUlBLFdBQVd4RixTQUFmLEVBQTBCO0FBQ3hCZixZQUFFMEosS0FBRixDQUFRQyxLQUFSLENBQWM7QUFDWm5ILHFCQUFTLGdDQURHO0FBRVpvSCxtQkFBTztBQUZLLFdBQWQ7QUFJQTtBQUNEOztBQUVELFlBQUksT0FBT3JELE9BQU9rQixNQUFkLEtBQXlCLFdBQXpCLElBQXdDbEIsT0FBT2tCLE1BQVAsS0FBa0IsS0FBOUQsRUFBcUU7QUFDbkV6SCxZQUFFMEosS0FBRixDQUFRQyxLQUFSLENBQWMsRUFBQ25ILFNBQVMrRCxPQUFPc0QsR0FBakIsRUFBc0JELE9BQU8sSUFBN0IsRUFBZDtBQUNBO0FBQ0Q7O0FBRUQsWUFBTUUsaUJBQWlCLG9CQUFZdkQsTUFBWixFQUFvQixDQUFwQixDQUF2Qjs7QUFFQSxZQUFJQSxPQUFPdUQsY0FBUCxFQUF1QnJDLE1BQXZCLEtBQWtDLEtBQXRDLEVBQTZDO0FBQzNDLGNBQUksT0FBT2xCLE9BQU91RCxjQUFQLEVBQXVCeEMsb0JBQTlCLEtBQXVELFdBQTNELEVBQXdFO0FBQ3RFN0MsaUJBQUtzRixrQkFBTCxDQUF3QnhELE9BQU91RCxjQUFQLENBQXhCO0FBQ0Q7O0FBRUQ5SixZQUFFMEosS0FBRixDQUFRQyxLQUFSLENBQWMsRUFBQ25ILFNBQVMrRCxPQUFPdUQsY0FBUCxFQUF1QkQsR0FBakMsRUFBc0NELE9BQU8sSUFBN0MsRUFBZDtBQUNBO0FBQ0Q7O0FBRUQ1SixVQUFFMEosS0FBRixDQUFRO0FBQ05sSCxtQkFBUytELE9BQU91RCxjQUFQLEVBQXVCRCxHQUQxQjtBQUVORyxvQkFBVTtBQUZKLFNBQVI7O0FBS0EsWUFBTUMsa0JBQWtCeEYsS0FBS3lGLHFCQUFMLEdBQTZCQyxPQUE3QixDQUFxQyxHQUFyQyxFQUEwQyxFQUExQyxDQUF4QjtBQUNBLFlBQUlDLGNBQWMsSUFBbEI7O0FBRUEsWUFBSWhFLFdBQVcsV0FBZixFQUE0QjtBQUMxQmdFLHdCQUFjeEIsYUFBYUMsT0FBYixPQUF5Qm9CLGVBQXpCLENBQWQ7QUFDQUcsc0JBQVlqSixNQUFaOztBQUVBeUIsa0JBQVFNLFNBQVIsQ0FBa0Isb0JBQWxCLEVBQXdDLGFBQXhDO0FBQ0QsU0FMRCxNQUtPLElBQUlrRCxXQUFXLFNBQWYsRUFBMEI7QUFDL0JnRSx3QkFBY3hCLGFBQWFDLE9BQWIsT0FBeUJvQixlQUF6QixDQUFkO0FBQ0FHLHNCQUFZQyxRQUFaLENBQXdCSixlQUF4QjtBQUNBRyxzQkFBWXpGLElBQVosQ0FBaUIsYUFBakIsRUFBZ0MsR0FBaEM7O0FBRUEvQixrQkFBUU0sU0FBUixDQUFrQixpQkFBbEIsRUFBcUMsYUFBckM7QUFDRCxTQU5NLE1BTUEsSUFBSWtELFdBQVcsUUFBZixFQUF5QjtBQUM5QmdFLHdCQUFjeEIsYUFBYUMsT0FBYixPQUF5Qm9CLGVBQXpCLENBQWQ7QUFDQUcsc0JBQVlFLFdBQVosQ0FBMkJMLGVBQTNCO0FBQ0FHLHNCQUFZekYsSUFBWixDQUFpQixhQUFqQixFQUFnQyxHQUFoQzs7QUFFQS9CLGtCQUFRTSxTQUFSLENBQWtCLGdCQUFsQixFQUFvQyxhQUFwQztBQUNEOztBQUVEMEYscUJBQWEyQixXQUFiLENBQXlCaEUsT0FBT3VELGNBQVAsRUFBdUJVLGdCQUFoRDtBQUNELE9BL0RILEVBZ0VHQyxJQWhFSCxDQWdFUSxZQUFNO0FBQ1YsWUFBTUMsYUFBYTlCLGFBQWFDLE9BQWIsQ0FBcUIsa0JBQXJCLENBQW5CO0FBQ0EsWUFBTThCLFdBQVdELFdBQVd2RixJQUFYLENBQWdCLFVBQWhCLENBQWpCO0FBQ0FuRixVQUFFMEosS0FBRixDQUFRQyxLQUFSLENBQWM7QUFDWm5ILGlEQUFxQzRELE1BQXJDLG9CQUEwRHVFLFFBRDlDO0FBRVpmLGlCQUFPO0FBRkssU0FBZDtBQUlELE9BdkVILEVBd0VHZ0IsTUF4RUgsQ0F3RVUsWUFBTTtBQUNaaEMscUJBQWFpQyxNQUFiO0FBQ0EvQixtQkFBVzNILE1BQVg7QUFDQSxZQUFJMkIsUUFBSixFQUFjO0FBQ1pBO0FBQ0Q7QUFDRixPQTlFSDs7QUFnRkEsYUFBTyxLQUFQO0FBQ0Q7Ozs7O2tCQXpYa0JTLFU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzNCckI7Ozs7OztjQUVZeEQsTTtJQUFMQyxDLFdBQUFBLEM7O0FBRVA7Ozs7QUE3QkE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7SUFpQ004SyxxQjtBQUNKOzs7OztBQUtBLGlDQUFZQyxvQkFBWixFQUFrQztBQUFBOztBQUNoQyxTQUFLQSxvQkFBTCxHQUE0QkEsb0JBQTVCOztBQUVBLFNBQUtDLHlCQUFMLEdBQWlDLEVBQWpDO0FBQ0EsU0FBS0MsMEJBQUwsR0FBa0MsQ0FBbEM7QUFDQSxTQUFLQyxZQUFMLEdBQW9CLE1BQXBCO0FBQ0EsU0FBS0MsWUFBTCxHQUFvQixNQUFwQjtBQUNBLFNBQUtDLHNCQUFMLEdBQThCLGVBQTlCOztBQUVBLFNBQUtDLHNCQUFMLEdBQThCLEVBQTlCO0FBQ0EsU0FBS0MsY0FBTCxHQUFzQixFQUF0QjtBQUNBLFNBQUtDLHVCQUFMLEdBQStCLEtBQS9CO0FBQ0EsU0FBS0MsZUFBTCxHQUF1QixFQUF2QjtBQUNBLFNBQUtDLGtCQUFMLEdBQTBCLElBQTFCO0FBQ0EsU0FBS0MsZ0JBQUwsR0FBd0IsSUFBeEI7QUFDQSxTQUFLQyxjQUFMLEdBQXNCLElBQXRCO0FBQ0EsU0FBS0MsYUFBTCxHQUFxQixnQ0FBckI7QUFDQSxTQUFLQyxhQUFMLEdBQXFCLElBQXJCO0FBQ0EsU0FBS0MsY0FBTCxHQUFzQixJQUF0QjtBQUNBLFNBQUtDLGVBQUwsR0FBdUIsS0FBdkI7O0FBRUEsU0FBS0Msb0JBQUwsR0FBNEIsMENBQTVCOztBQUVBOzs7OztBQUtBLFNBQUtDLFdBQUwsR0FBbUIsRUFBbkI7QUFDQSxTQUFLQyxjQUFMLEdBQXNCLElBQXRCO0FBQ0EsU0FBS0MsY0FBTCxHQUFzQixJQUF0Qjs7QUFFQSxTQUFLQyxlQUFMLEdBQXVCLG9CQUF2QjtBQUNBO0FBQ0EsU0FBS0MsZUFBTCxHQUF1QixXQUF2QjtBQUNBLFNBQUtDLGVBQUwsR0FBdUIsV0FBdkI7O0FBRUE7QUFDQSxTQUFLcEksc0JBQUwsR0FBOEIsbUJBQTlCO0FBQ0EsU0FBS0Qsc0JBQUwsR0FBOEIsbUJBQTlCO0FBQ0EsU0FBS3NJLDZCQUFMLEdBQXFDLGlDQUFyQztBQUNBLFNBQUtDLGdCQUFMLEdBQXdCLDJCQUF4QjtBQUNBLFNBQUtDLG9CQUFMLEdBQTRCLHVCQUE1QjtBQUNBLFNBQUtDLHlCQUFMLEdBQWlDLG1CQUFqQztBQUNBLFNBQUtDLHdCQUFMLEdBQWdDLHdCQUFoQztBQUNBLFNBQUtDLHdCQUFMLEdBQWdDLDBCQUFoQztBQUNBLFNBQUtDLDZCQUFMLEdBQXFDLCtCQUFyQztBQUNBLFNBQUtDLG9CQUFMLEdBQTRCLDBCQUE1QjtBQUNBLFNBQUtDLHdCQUFMLEdBQWdDLHVCQUFoQztBQUNBLFNBQUtDLHFCQUFMLEdBQTZCLDBCQUE3QjtBQUNBLFNBQUtDLHFCQUFMLEdBQTZCLDBCQUE3Qjs7QUFFQTtBQUNBLFNBQUtDLGdCQUFMLEdBQXdCLGlDQUF4QjtBQUNBLFNBQUtDLGdCQUFMLEdBQXdCLGdDQUF4QjtBQUNBLFNBQUtDLGlCQUFMLEdBQTRCLEtBQUtELGdCQUFqQzs7QUFFQTtBQUNBLFNBQUtFLHFCQUFMLEdBQTZCLHNDQUE3Qjs7QUFFQTtBQUNBLFNBQUtDLDBCQUFMLEdBQWtDLHNCQUFsQztBQUNBLFNBQUtDLGdCQUFMLEdBQXdCLG1CQUF4QjtBQUNBLFNBQUtDLDhCQUFMLEdBQXNDLGtDQUF0QztBQUNBLFNBQUtDLDhCQUFMLEdBQXNDLGtDQUF0QztBQUNBLFNBQUtDLDZCQUFMLEdBQXdDLEtBQUtGLDhCQUE3QztBQUNBLFNBQUtHLDZCQUFMLEdBQXdDLEtBQUtGLDhCQUE3QztBQUNBLFNBQUtHLDBCQUFMLEdBQWtDLDZCQUFsQztBQUNBLFNBQUtDLHdCQUFMLEdBQWdDLDRCQUFoQztBQUNBLFNBQUtDLGtDQUFMLEdBQTBDLHdDQUExQztBQUNBLFNBQUtDLDRCQUFMLEdBQW9DLGlDQUFwQztBQUNBLFNBQUtDLDhCQUFMLEdBQXNDLGdDQUF0Qzs7QUFFQTtBQUNBLFNBQUtDLHlCQUFMLEdBQWlDLDhCQUFqQztBQUNBLFNBQUtDLGdDQUFMLEdBQXdDLDhCQUF4QztBQUNBLFNBQUtDLDZCQUFMLEdBQXFDLGtDQUFyQztBQUNBLFNBQUtDLGtDQUFMLEdBQTBDLG9DQUExQzs7QUFFQTtBQUNBLFNBQUtDLDJCQUFMLEdBQW1DLCtCQUFuQztBQUNBLFNBQUtDLGtCQUFMLEdBQTBCLHFCQUExQjtBQUNBLFNBQUtDLHNCQUFMLEdBQThCLHNCQUE5Qjs7QUFFQTtBQUNBLFNBQUtDLDZCQUFMLEdBQXFDLGdEQUFyQztBQUNBLFNBQUtDLDRCQUFMLEdBQW9DLCtDQUFwQztBQUNBLFNBQUtDLDRCQUFMLEdBQW9DLDRDQUFwQztBQUNBLFNBQUtDLHFCQUFMLEdBQTZCLHNCQUE3QjtBQUNBLFNBQUtDLDJCQUFMLEdBQW1DLG9DQUFuQztBQUNBLFNBQUtDLDBCQUFMLEdBQWtDLGlCQUFsQztBQUNBLFNBQUtDLDBCQUFMLEdBQWtDLDhCQUFsQztBQUNBLFNBQUtDLHlCQUFMLEdBQWlDLDZCQUFqQztBQUNBLFNBQUtDLGlCQUFMLEdBQXlCLHNCQUF6QjtBQUNBLFNBQUtDLHlCQUFMLEdBQWlDLG9DQUFqQztBQUNBLFNBQUtDLHlCQUFMLEdBQWlDLHNCQUFqQztBQUNBLFNBQUtDLDhCQUFMLEdBQXNDLDJCQUF0QztBQUNBLFNBQUtDLDJCQUFMLEdBQW1DLHdCQUFuQztBQUNBLFNBQUtDLHVDQUFMLEdBQStDLGtDQUEvQztBQUNBLFNBQUtDLDJCQUFMLEdBQW1DLHdCQUFuQztBQUNBLFNBQUtDLGdDQUFMLEdBQXdDLDhCQUF4QztBQUNBLFNBQUtDLHFDQUFMLEdBQTZDLHVDQUE3QztBQUNBLFNBQUtDLG9DQUFMLEdBQTRDLG9DQUE1QztBQUNBLFNBQUtDLHFDQUFMLEdBQTZDLGdDQUE3QztBQUNBLFNBQUtDLDJCQUFMLEdBQW1DLHdCQUFuQzs7QUFFQSxTQUFLQyxtQkFBTDtBQUNBLFNBQUtDLHNCQUFMO0FBQ0EsU0FBS0Msa0JBQUw7QUFDQSxTQUFLQyx3QkFBTDtBQUNBLFNBQUtDLGdCQUFMO0FBQ0EsU0FBS0MsZUFBTDtBQUNBLFNBQUtDLGtCQUFMO0FBQ0EsU0FBS0Msa0JBQUw7QUFDQSxTQUFLM0wsaUJBQUw7QUFDQSxTQUFLNEwsZ0JBQUw7QUFDQSxTQUFLQyxpQkFBTDtBQUNBLFNBQUtDLG1CQUFMO0FBQ0EsU0FBS0MsWUFBTDtBQUNBLFNBQUtDLHdCQUFMO0FBQ0EsU0FBS0Msd0JBQUw7QUFDQSxTQUFLQyx3QkFBTDtBQUNBLFNBQUtDLGdCQUFMO0FBQ0EsU0FBS0MscUJBQUw7QUFDQSxTQUFLQyxpQkFBTDtBQUNEOzs7OytDQUUwQjtBQUN6QixVQUFNcE0sT0FBTyxJQUFiO0FBQ0EsVUFBTXJELE9BQU9wQixFQUFFLE1BQUYsQ0FBYjtBQUNBb0IsV0FBS0osRUFBTCxDQUFRLE9BQVIsRUFBaUJ5RCxLQUFLNkosa0JBQXRCLEVBQTBDLFlBQVk7QUFDcEQ7QUFDQTdKLGFBQUtpSCxnQkFBTCxHQUF3Qm9GLFNBQVM5USxFQUFFLElBQUYsRUFBUW1GLElBQVIsQ0FBYSxZQUFiLENBQVQsRUFBcUMsRUFBckMsQ0FBeEI7QUFDQTtBQUNBbkYsVUFBRXlFLEtBQUs0SiwyQkFBUCxFQUFvQ25HLElBQXBDLENBQXlDbEksRUFBRSxJQUFGLEVBQVFrSSxJQUFSLEVBQXpDO0FBQ0FsSSxVQUFFeUUsS0FBSzhKLHNCQUFQLEVBQStCN04sSUFBL0I7QUFDQStELGFBQUtzTSxzQkFBTDtBQUNELE9BUEQ7O0FBU0EzUCxXQUFLSixFQUFMLENBQVEsT0FBUixFQUFpQnlELEtBQUs4SixzQkFBdEIsRUFBOEMsWUFBWTtBQUN4RHZPLFVBQUV5RSxLQUFLNEosMkJBQVAsRUFBb0NuRyxJQUFwQyxDQUF5Q2xJLEVBQUUsSUFBRixFQUFRa0ksSUFBUixFQUF6QztBQUNBbEksVUFBRSxJQUFGLEVBQVE0SCxJQUFSO0FBQ0FuRCxhQUFLaUgsZ0JBQUwsR0FBd0IsSUFBeEI7QUFDQWpILGFBQUtzTSxzQkFBTDtBQUNELE9BTEQ7QUFNRDs7O3VDQUVrQjtBQUNqQixVQUFNdE0sT0FBTyxJQUFiO0FBQ0EsVUFBTXJELE9BQU9wQixFQUFFLE1BQUYsQ0FBYjs7QUFFQW9CLFdBQUtKLEVBQUwsQ0FBUSxPQUFSLEVBQWlCeUQsS0FBS3VNLHlCQUFMLEVBQWpCLEVBQW1ELFlBQU07QUFDdkQsWUFBTUMsV0FBV2pSLEVBQUV5RSxLQUFLNkksMEJBQVAsQ0FBakI7O0FBRUEsWUFBSXROLEVBQUV5RSxLQUFLeU0sZ0NBQUwsRUFBRixFQUEyQ3BNLE1BQTNDLEdBQW9ELENBQXhELEVBQTJEO0FBQ3pEbU0sbUJBQVNwSSxPQUFULENBQWlCLHVCQUFqQixFQUEwQ3lCLFdBQTFDLENBQXNELFVBQXREO0FBQ0QsU0FGRCxNQUVPO0FBQ0wyRyxtQkFBU3BJLE9BQVQsQ0FBaUIsdUJBQWpCLEVBQTBDd0IsUUFBMUMsQ0FBbUQsVUFBbkQ7QUFDRDtBQUNGLE9BUkQ7O0FBVUFqSixXQUFLSixFQUFMLENBQVEsT0FBUixFQUFpQnlELEtBQUs4SSxnQkFBdEIsRUFBd0MsU0FBUzRELG9CQUFULEdBQWdDO0FBQ3RFLFlBQUluUixFQUFFeUUsS0FBS3lNLGdDQUFMLEVBQUYsRUFBMkNwTSxNQUEzQyxLQUFzRCxDQUExRCxFQUE2RDtBQUMzRDlFLFlBQUUwSixLQUFGLENBQVEwSCxPQUFSLENBQWdCO0FBQ2Q1TyxxQkFBU3pDLE9BQU9zUixxQkFBUCxDQUE2QixrQ0FBN0I7QUFESyxXQUFoQjtBQUdBO0FBQ0Q7O0FBRUQ1TSxhQUFLcUgsY0FBTCxHQUFzQjlMLEVBQUUsSUFBRixFQUFRbUYsSUFBUixDQUFhLEtBQWIsQ0FBdEI7QUFDQSxZQUFNbU0sb0JBQW9CN00sS0FBSzhNLHlCQUFMLEVBQTFCO0FBQ0EsWUFBTUMsZUFBZXhSLEVBQUUsSUFBRixFQUNsQjBHLElBRGtCLENBQ2IsVUFEYSxFQUVsQndCLElBRmtCLEdBR2xCdUosV0FIa0IsRUFBckI7QUFJQXpSLFVBQUV5RSxLQUFLc0osNEJBQVAsRUFBcUMyRCxJQUFyQyxDQUEwQ0osaUJBQTFDO0FBQ0F0UixVQUFFeUUsS0FBS3FKLGtDQUFQLEVBQTJDNUYsSUFBM0MsQ0FBZ0RzSixZQUFoRDs7QUFFQSxZQUFJL00sS0FBS3FILGNBQUwsS0FBd0IsZ0JBQTVCLEVBQThDO0FBQzVDOUwsWUFBRXlFLEtBQUttSiwwQkFBUCxFQUFtQ2xOLElBQW5DO0FBQ0QsU0FGRCxNQUVPO0FBQ0xWLFlBQUV5RSxLQUFLbUosMEJBQVAsRUFBbUNoRyxJQUFuQztBQUNEOztBQUVENUgsVUFBRXlFLEtBQUtvSix3QkFBUCxFQUFpQ3ZOLEtBQWpDLENBQXVDLE1BQXZDO0FBQ0QsT0F4QkQ7O0FBMEJBYyxXQUFLSixFQUFMLENBQVEsT0FBUixFQUFpQixLQUFLZ04sOEJBQXRCLEVBQXNELFVBQUNoTCxLQUFELEVBQVc7QUFDL0RBLGNBQU1rQyxjQUFOO0FBQ0FsQyxjQUFNMk8sZUFBTjtBQUNBM1IsVUFBRXlFLEtBQUtvSix3QkFBUCxFQUFpQ3ZOLEtBQWpDLENBQXVDLE1BQXZDO0FBQ0FtRSxhQUFLbU4sWUFBTCxDQUFrQm5OLEtBQUtxSCxjQUF2QjtBQUNELE9BTEQ7QUFNRDs7OzZDQUV3QjtBQUN2Qi9MLGFBQU82QyxPQUFQLENBQWU1QixFQUFmLENBQWtCLGlCQUFsQixFQUFxQyxLQUFLNlEsZ0JBQTFDLEVBQTRELElBQTVEO0FBQ0E5UixhQUFPNkMsT0FBUCxDQUFlNUIsRUFBZixDQUFrQixvQkFBbEIsRUFBd0MsS0FBSzhRLGtCQUE3QyxFQUFpRSxJQUFqRTtBQUNEOzs7dUNBRWtCO0FBQ2pCLFVBQU1yTixPQUFPLElBQWI7QUFDQUEsV0FBS3lGLHFCQUFMOztBQUVBbEssUUFBRSxlQUFGLEVBQW1CK1IsSUFBbkIsQ0FBd0IsWUFBTTtBQUM1QnROLGFBQUtxTixrQkFBTDtBQUNELE9BRkQ7QUFHRDs7OytDQUUwQjtBQUN6QixVQUFNck4sT0FBTyxJQUFiOztBQUVBLFVBQUl6RSxFQUFFeUUsS0FBS3dKLHlCQUFQLEVBQWtDbkosTUFBdEMsRUFBOEM7QUFDNUNMLGFBQUt1TixZQUFMO0FBQ0Q7O0FBRUQ7QUFDQWhTLFFBQUUsTUFBRixFQUFVZ0IsRUFBVixDQUFhLE9BQWIsRUFBc0J5RCxLQUFLMkosa0NBQTNCLEVBQStELFlBQU07QUFDbkVwTyxVQUFFeUUsS0FBS3lKLGdDQUFQLEVBQXlDK0QsT0FBekM7QUFDQWpTLFVBQUV5RSxLQUFLd0oseUJBQVAsRUFBa0NwRCxNQUFsQztBQUNBcEcsYUFBS3VOLFlBQUw7QUFDRCxPQUpEO0FBS0Q7OzttQ0FFYztBQUNiLFVBQU12TixPQUFPLElBQWI7O0FBRUF6RSxRQUFFb0osSUFBRixDQUFPO0FBQ0xFLGdCQUFRLEtBREg7QUFFTHpCLGFBQUs5SCxPQUFPd0YsVUFBUCxDQUFrQjJNO0FBRmxCLE9BQVAsRUFJR3pJLElBSkgsQ0FJUSxVQUFDMEksUUFBRCxFQUFjO0FBQ2xCLFlBQUlBLFNBQVMxSyxNQUFULEtBQW9CLElBQXhCLEVBQThCO0FBQzVCLGNBQUksT0FBTzBLLFNBQVNDLFdBQWhCLEtBQWdDLFdBQXBDLEVBQWlERCxTQUFTQyxXQUFULEdBQXVCLElBQXZCO0FBQ2pELGNBQUksT0FBT0QsU0FBU3RJLEdBQWhCLEtBQXdCLFdBQTVCLEVBQXlDc0ksU0FBU3RJLEdBQVQsR0FBZSxJQUFmOztBQUV6QyxjQUFNd0ksYUFBYXBSLFNBQVNxUixXQUFULENBQXFCLENBQXJCLENBQW5CO0FBQ0EsY0FBTUMsaUJBQWlCLGlCQUF2QjtBQUNBLGNBQU1DLHVCQUF1QixlQUE3QjtBQUNBLGNBQU1DLHdCQUF3QixzQkFBOUI7QUFDQSxjQUFNQyw4QkFBaUNGLG9CQUFqQyxTQUF5REMscUJBQS9EOztBQUVBLGNBQUlKLFdBQVdNLFVBQWYsRUFBMkI7QUFDekJOLHVCQUFXTSxVQUFYLENBQXNCRCw4QkFBOEJILGNBQXBELEVBQW9FRixXQUFXTyxRQUFYLENBQW9COU4sTUFBeEY7QUFDRCxXQUZELE1BRU8sSUFBSXVOLFdBQVdRLE9BQWYsRUFBd0I7QUFDN0JSLHVCQUFXUSxPQUFYLENBQW1CSCwyQkFBbkIsRUFBZ0RILGNBQWhELEVBQWdFLENBQUMsQ0FBakU7QUFDRDs7QUFFRHZTLFlBQUV5RSxLQUFLd0oseUJBQVAsRUFBa0NnRSxPQUFsQyxDQUEwQyxHQUExQyxFQUErQyxZQUFNO0FBQ25EalMsY0FBRStSLElBQUYsQ0FBT0ksU0FBU0MsV0FBaEIsRUFBNkIsVUFBQ1UsS0FBRCxFQUFRek0sT0FBUixFQUFvQjtBQUMvQ3JHLGdCQUFFcUcsUUFBUTRLLFFBQVYsRUFBb0J0TyxNQUFwQixDQUEyQjBELFFBQVFyRSxPQUFuQztBQUNELGFBRkQ7QUFHQWhDLGNBQUV3UyxvQkFBRixFQUNHM0gsTUFESCxDQUNVLEdBRFYsRUFFR2tJLEdBRkgsQ0FFTyxTQUZQLEVBRWtCLE1BRmxCO0FBR0EvUyxjQUFFeVMscUJBQUYsRUFBeUI1SCxNQUF6QixDQUFnQyxHQUFoQztBQUNBN0ssY0FBRSx5QkFBRixFQUE2QmdULE9BQTdCO0FBQ0F2TyxpQkFBS3FMLGtCQUFMO0FBQ0FyTCxpQkFBS2tNLGdCQUFMO0FBQ0QsV0FYRDtBQVlELFNBNUJELE1BNEJPO0FBQ0wzUSxZQUFFeUUsS0FBS3dKLHlCQUFQLEVBQWtDZ0UsT0FBbEMsQ0FBMEMsR0FBMUMsRUFBK0MsWUFBTTtBQUNuRGpTLGNBQUV5RSxLQUFLMEosNkJBQVAsRUFBc0NqRyxJQUF0QyxDQUEyQ2lLLFNBQVN0SSxHQUFwRDtBQUNBN0osY0FBRXlFLEtBQUt5SixnQ0FBUCxFQUF5Q3JELE1BQXpDLENBQWdELEdBQWhEO0FBQ0QsV0FIRDtBQUlEO0FBQ0YsT0F2Q0gsRUF3Q0dKLElBeENILENBd0NRLFVBQUMwSCxRQUFELEVBQWM7QUFDbEJuUyxVQUFFeUUsS0FBS3dKLHlCQUFQLEVBQWtDZ0UsT0FBbEMsQ0FBMEMsR0FBMUMsRUFBK0MsWUFBTTtBQUNuRGpTLFlBQUV5RSxLQUFLMEosNkJBQVAsRUFBc0NqRyxJQUF0QyxDQUEyQ2lLLFNBQVNjLFVBQXBEO0FBQ0FqVCxZQUFFeUUsS0FBS3lKLGdDQUFQLEVBQXlDckQsTUFBekMsQ0FBZ0QsR0FBaEQ7QUFDRCxTQUhEO0FBSUQsT0E3Q0g7QUE4Q0Q7Ozt1Q0FFa0I7QUFDakIsVUFBTXBHLE9BQU8sSUFBYjtBQUNBLFVBQUloRSxrQkFBSjtBQUNBLFVBQUl5UyxjQUFKOztBQUVBek8sV0FBS3dILFdBQUwsR0FBbUIsRUFBbkI7QUFDQWpNLFFBQUUsZUFBRixFQUFtQitSLElBQW5CLENBQXdCLFNBQVNvQixnQkFBVCxHQUE0QjtBQUNsRDFTLG9CQUFZVCxFQUFFLElBQUYsQ0FBWjtBQUNBUyxrQkFBVWlHLElBQVYsQ0FBZSxjQUFmLEVBQStCcUwsSUFBL0IsQ0FBb0MsU0FBU3FCLGNBQVQsR0FBMEI7QUFDNURGLGtCQUFRbFQsRUFBRSxJQUFGLENBQVI7QUFDQXlFLGVBQUt3SCxXQUFMLENBQWlCOUMsSUFBakIsQ0FBc0I7QUFDcEJrSyx1QkFBV0gsS0FEUztBQUVwQjlTLGdCQUFJOFMsTUFBTS9OLElBQU4sQ0FBVyxJQUFYLENBRmdCO0FBR3BCNEIsa0JBQU1tTSxNQUFNL04sSUFBTixDQUFXLE1BQVgsRUFBbUJzTSxXQUFuQixFQUhjO0FBSXBCNkIscUJBQVNDLFdBQVdMLE1BQU0vTixJQUFOLENBQVcsU0FBWCxDQUFYLENBSlc7QUFLcEJxTyxrQkFBTU4sTUFBTS9OLElBQU4sQ0FBVyxNQUFYLENBTGM7QUFNcEJpRCxvQkFBUThLLE1BQU0vTixJQUFOLENBQVcsUUFBWCxFQUFxQnNNLFdBQXJCLEVBTlk7QUFPcEJnQyxxQkFBU1AsTUFBTS9OLElBQU4sQ0FBVyxTQUFYLENBUFc7QUFRcEJ1Tyx5QkFBYVIsTUFBTS9OLElBQU4sQ0FBVyxhQUFYLEVBQTBCc00sV0FBMUIsRUFSTztBQVNwQjlHLHNCQUFVdUksTUFBTS9OLElBQU4sQ0FBVyxXQUFYLEVBQXdCc00sV0FBeEIsRUFUVTtBQVVwQmtDLDZCQUFpQlQsTUFBTS9OLElBQU4sQ0FBVyxrQkFBWCxDQVZHO0FBV3BCeU8sd0JBQVlDLE9BQU9YLE1BQU0vTixJQUFOLENBQVcsWUFBWCxDQUFQLEVBQWlDc00sV0FBakMsRUFYUTtBQVlwQnZLLGtCQUFNZ00sTUFBTS9OLElBQU4sQ0FBVyxNQUFYLENBWmM7QUFhcEIyTyxtQkFBT1AsV0FBV0wsTUFBTS9OLElBQU4sQ0FBVyxPQUFYLENBQVgsQ0FiYTtBQWNwQjRPLG9CQUFRakQsU0FBU29DLE1BQU0vTixJQUFOLENBQVcsUUFBWCxDQUFULEVBQStCLEVBQS9CLENBZFk7QUFlcEI2TyxvQkFBUWQsTUFBTS9OLElBQU4sQ0FBVyxhQUFYLENBZlk7QUFnQnBCOE8scUJBQVNmLE1BQU1nQixRQUFOLENBQWUsa0JBQWYsSUFBcUN6UCxLQUFLMEcsWUFBMUMsR0FBeUQxRyxLQUFLeUcsWUFoQm5EO0FBaUJwQnpLO0FBakJvQixXQUF0Qjs7QUFvQkEsY0FBSWdFLEtBQUswUCxhQUFMLEVBQUosRUFBMEI7QUFDeEJqQixrQkFBTS9SLE1BQU47QUFDRDtBQUNGLFNBekJEO0FBMEJELE9BNUJEOztBQThCQXNELFdBQUt5SCxjQUFMLEdBQXNCbE0sRUFBRSxLQUFLZ04scUJBQVAsQ0FBdEI7QUFDQXZJLFdBQUswSCxjQUFMLEdBQXNCbk0sRUFBRSxLQUFLaU4scUJBQVAsQ0FBdEI7QUFDQXhJLFdBQUtzTSxzQkFBTDtBQUNBL1EsUUFBRSxNQUFGLEVBQVV1SSxPQUFWLENBQWtCLHFCQUFsQjtBQUNEOztBQUVEOzs7Ozs7OzBDQUlzQjtBQUNwQixVQUFNOUQsT0FBTyxJQUFiOztBQUVBLFVBQUksQ0FBQ0EsS0FBS2tILGNBQVYsRUFBMEI7QUFDeEI7QUFDRDs7QUFFRDtBQUNBLFVBQUl5SSxRQUFRLEtBQVo7QUFDQSxVQUFJQyxNQUFNNVAsS0FBS2tILGNBQWY7QUFDQSxVQUFNMkksY0FBY0QsSUFBSUUsS0FBSixDQUFVLEdBQVYsQ0FBcEI7O0FBRUEsVUFBSUQsWUFBWXhQLE1BQVosR0FBcUIsQ0FBekIsRUFBNEI7QUFDMUJ1UCxjQUFNQyxZQUFZLENBQVosQ0FBTjtBQUNBLFlBQUlBLFlBQVksQ0FBWixNQUFtQixNQUF2QixFQUErQjtBQUM3QkYsa0JBQVEsTUFBUjtBQUNEO0FBQ0Y7O0FBRUQsVUFBTUksaUJBQWlCLFNBQWpCQSxjQUFpQixDQUFDQyxDQUFELEVBQUlDLENBQUosRUFBVTtBQUMvQixZQUFJQyxRQUFRRixFQUFFSixHQUFGLENBQVo7QUFDQSxZQUFJTyxRQUFRRixFQUFFTCxHQUFGLENBQVo7O0FBRUEsWUFBSUEsUUFBUSxRQUFaLEVBQXNCO0FBQ3BCTSxrQkFBUSxJQUFJRSxJQUFKLENBQVNGLEtBQVQsRUFBZ0JHLE9BQWhCLEVBQVI7QUFDQUYsa0JBQVEsSUFBSUMsSUFBSixDQUFTRCxLQUFULEVBQWdCRSxPQUFoQixFQUFSO0FBQ0FILGtCQUFRLHFCQUFhQSxLQUFiLElBQXNCLENBQXRCLEdBQTBCQSxLQUFsQztBQUNBQyxrQkFBUSxxQkFBYUEsS0FBYixJQUFzQixDQUF0QixHQUEwQkEsS0FBbEM7QUFDQSxjQUFJRCxVQUFVQyxLQUFkLEVBQXFCO0FBQ25CLG1CQUFPRixFQUFFM04sSUFBRixDQUFPZ08sYUFBUCxDQUFxQk4sRUFBRTFOLElBQXZCLENBQVA7QUFDRDtBQUNGOztBQUVELFlBQUk0TixRQUFRQyxLQUFaLEVBQW1CLE9BQU8sQ0FBQyxDQUFSO0FBQ25CLFlBQUlELFFBQVFDLEtBQVosRUFBbUIsT0FBTyxDQUFQOztBQUVuQixlQUFPLENBQVA7QUFDRCxPQWxCRDs7QUFvQkFuUSxXQUFLd0gsV0FBTCxDQUFpQitJLElBQWpCLENBQXNCUixjQUF0QjtBQUNBLFVBQUlKLFVBQVUsTUFBZCxFQUFzQjtBQUNwQjNQLGFBQUt3SCxXQUFMLENBQWlCZ0osT0FBakI7QUFDRDtBQUNGOzs7bURBRThCO0FBQzdCLFVBQU14USxPQUFPLElBQWI7O0FBRUF6RSxRQUFFLG9CQUFGLEVBQXdCK1IsSUFBeEIsQ0FBNkIsU0FBU21ELHNCQUFULEdBQWtDO0FBQzdELFlBQU16VSxZQUFZVCxFQUFFLElBQUYsQ0FBbEI7QUFDQSxZQUFNbVYsdUJBQXVCMVUsVUFBVWlHLElBQVYsQ0FBZSxjQUFmLEVBQStCNUIsTUFBNUQ7O0FBRUEsWUFDR0wsS0FBS2dILGtCQUFMLElBQTJCaEgsS0FBS2dILGtCQUFMLEtBQTRCb0ksT0FBT3BULFVBQVVpRyxJQUFWLENBQWUsZUFBZixFQUFnQ3ZCLElBQWhDLENBQXFDLE1BQXJDLENBQVAsQ0FBeEQsSUFDSVYsS0FBS2lILGdCQUFMLEtBQTBCLElBQTFCLElBQWtDeUoseUJBQXlCLENBRC9ELElBRUlBLHlCQUF5QixDQUF6QixJQUNDdEIsT0FBT3BULFVBQVVpRyxJQUFWLENBQWUsZUFBZixFQUFnQ3ZCLElBQWhDLENBQXFDLE1BQXJDLENBQVAsTUFBeURWLEtBQUsyRyxzQkFIbkUsSUFJSTNHLEtBQUsrRyxlQUFMLENBQXFCMUcsTUFBckIsR0FBOEIsQ0FBOUIsSUFBbUNxUSx5QkFBeUIsQ0FMbEUsRUFNRTtBQUNBMVUsb0JBQVVtSCxJQUFWO0FBQ0E7QUFDRDs7QUFFRG5ILGtCQUFVQyxJQUFWO0FBQ0FELGtCQUNHaUcsSUFESCxDQUNXakMsS0FBSzRILGVBRGhCLFVBQ29DNUgsS0FBSzZILGVBRHpDLEVBRUd4RSxNQUZILENBRVVxTix3QkFBd0IxUSxLQUFLd0csMEJBRnZDO0FBR0QsT0FuQkQ7QUFvQkQ7Ozs2Q0FFd0I7QUFDdkIsVUFBTXhHLE9BQU8sSUFBYjs7QUFFQUEsV0FBSzJRLG1CQUFMOztBQUVBLFVBQUkzUSxLQUFLMFAsYUFBTCxFQUFKLEVBQTBCO0FBQ3hCblUsVUFBRXlFLEtBQUt1SCxvQkFBUCxFQUNHdEYsSUFESCxDQUNRLGNBRFIsRUFFR3ZGLE1BRkg7QUFHQW5CLFVBQUUsZUFBRixFQUNHMEcsSUFESCxDQUNRLGNBRFIsRUFFR3ZGLE1BRkg7QUFHRDs7QUFFRDtBQUNBLFVBQUlrVSxrQkFBSjtBQUNBLFVBQUlDLHNCQUFKO0FBQ0EsVUFBSUMsdUJBQUo7QUFDQSxVQUFJQyxrQkFBSjtBQUNBLFVBQUlDLGlCQUFKO0FBQ0EsVUFBSUMsbUJBQUo7O0FBRUEsVUFBTUMsb0JBQW9CbFIsS0FBS3dILFdBQUwsQ0FBaUJuSCxNQUEzQztBQUNBLFVBQU04USxVQUFVLEVBQWhCO0FBQ0EsVUFBTUMsV0FBVyxTQUFYQSxRQUFXLENBQUMvQyxLQUFELEVBQVEzTCxLQUFSLEVBQWtCO0FBQ2pDc08sbUJBQVd0TyxNQUFNc0ssV0FBTixFQUFYO0FBQ0ErRCxxQkFDS0YsY0FBY3ZPLElBQWQsQ0FBbUIrTyxPQUFuQixDQUEyQkwsUUFBM0IsTUFBeUMsQ0FBQyxDQUExQyxJQUNBSCxjQUFjNUIsV0FBZCxDQUEwQm9DLE9BQTFCLENBQWtDTCxRQUFsQyxNQUFnRCxDQUFDLENBRGpELElBRUFILGNBQWNsTixNQUFkLENBQXFCME4sT0FBckIsQ0FBNkJMLFFBQTdCLE1BQTJDLENBQUMsQ0FGNUMsSUFHQUgsY0FBYzNLLFFBQWQsQ0FBdUJtTCxPQUF2QixDQUErQkwsUUFBL0IsTUFBNkMsQ0FBQyxDQUpuRDtBQUtELE9BUEQ7O0FBU0EsV0FBSyxJQUFJTSxJQUFJLENBQWIsRUFBZ0JBLElBQUlKLGlCQUFwQixFQUF1Q0ksS0FBSyxDQUE1QyxFQUErQztBQUM3Q1Qsd0JBQWdCN1EsS0FBS3dILFdBQUwsQ0FBaUI4SixDQUFqQixDQUFoQjtBQUNBLFlBQUlULGNBQWNyQixPQUFkLEtBQTBCeFAsS0FBSzZHLGNBQW5DLEVBQW1EO0FBQ2pEK0osc0JBQVksSUFBWjs7QUFFQUUsMkJBQWlCOVEsS0FBS2dILGtCQUFMLEtBQTRCaEgsS0FBSzJHLHNCQUFqQyxHQUNiM0csS0FBSzJHLHNCQURRLEdBRWJrSyxjQUFjMUIsVUFGbEI7O0FBSUE7QUFDQSxjQUFJblAsS0FBS2dILGtCQUFMLEtBQTRCLElBQWhDLEVBQXNDO0FBQ3BDNEoseUJBQWFFLG1CQUFtQjlRLEtBQUtnSCxrQkFBckM7QUFDRDs7QUFFRDtBQUNBLGNBQUloSCxLQUFLaUgsZ0JBQUwsS0FBMEIsSUFBOUIsRUFBb0M7QUFDbEMySix5QkFBYUMsY0FBY3ZCLE1BQWQsS0FBeUJ0UCxLQUFLaUgsZ0JBQTNDO0FBQ0Q7O0FBRUQ7QUFDQSxjQUFJakgsS0FBSytHLGVBQUwsQ0FBcUIxRyxNQUF6QixFQUFpQztBQUMvQjBRLHdCQUFZLEtBQVo7QUFDQXhWLGNBQUUrUixJQUFGLENBQU90TixLQUFLK0csZUFBWixFQUE2QnFLLFFBQTdCO0FBQ0FSLHlCQUFhRyxTQUFiO0FBQ0Q7O0FBRUQ7OztBQUdBLGNBQUkvUSxLQUFLNkcsY0FBTCxLQUF3QjdHLEtBQUswRyxZQUE3QixJQUE2QyxDQUFDMUcsS0FBSytHLGVBQUwsQ0FBcUIxRyxNQUF2RSxFQUErRTtBQUM3RSxnQkFBSUwsS0FBSzRHLHNCQUFMLENBQTRCa0ssY0FBNUIsTUFBZ0R4VSxTQUFwRCxFQUErRDtBQUM3RDBELG1CQUFLNEcsc0JBQUwsQ0FBNEJrSyxjQUE1QixJQUE4QyxLQUE5QztBQUNEOztBQUVELGdCQUFJLENBQUNLLFFBQVFMLGNBQVIsQ0FBTCxFQUE4QjtBQUM1Qkssc0JBQVFMLGNBQVIsSUFBMEIsQ0FBMUI7QUFDRDs7QUFFREcseUJBQWFILG1CQUFtQjlRLEtBQUsyRyxzQkFBeEIsR0FDVDNHLEtBQUt1Ryx5QkFESSxHQUVUdkcsS0FBS3dHLDBCQUZUO0FBR0EsZ0JBQUkySyxRQUFRTCxjQUFSLEtBQTJCRyxVQUEvQixFQUEyQztBQUN6Q0wsMkJBQWE1USxLQUFLNEcsc0JBQUwsQ0FBNEJrSyxjQUE1QixDQUFiO0FBQ0Q7O0FBRURLLG9CQUFRTCxjQUFSLEtBQTJCLENBQTNCO0FBQ0Q7O0FBRUQ7QUFDQSxjQUFJRixTQUFKLEVBQWU7QUFDYixnQkFBSTVRLEtBQUtnSCxrQkFBTCxLQUE0QmhILEtBQUsyRyxzQkFBckMsRUFBNkQ7QUFDM0RwTCxnQkFBRXlFLEtBQUt1SCxvQkFBUCxFQUE2QnJKLE1BQTdCLENBQW9DMlMsY0FBY2pDLFNBQWxEO0FBQ0QsYUFGRCxNQUVPO0FBQ0xpQyw0QkFBYzdVLFNBQWQsQ0FBd0JrQyxNQUF4QixDQUErQjJTLGNBQWNqQyxTQUE3QztBQUNEO0FBQ0Y7QUFDRjtBQUNGOztBQUVENU8sV0FBS3VSLDRCQUFMOztBQUVBLFVBQUl2UixLQUFLK0csZUFBTCxDQUFxQjFHLE1BQXpCLEVBQWlDO0FBQy9COUUsVUFBRSxlQUFGLEVBQW1CMkMsTUFBbkIsQ0FBMEIsS0FBSzJJLGNBQUwsS0FBd0I3RyxLQUFLeUcsWUFBN0IsR0FBNEMsS0FBS2dCLGNBQWpELEdBQWtFLEtBQUtDLGNBQWpHO0FBQ0Q7O0FBRUQxSCxXQUFLcU4sa0JBQUw7QUFDRDs7OytDQUUwQjtBQUN6QixVQUFNck4sT0FBTyxJQUFiOztBQUVBekUsUUFBRUQsTUFBRixFQUFVaUIsRUFBVixDQUFhLGNBQWIsRUFBNkIsWUFBTTtBQUNqQyxZQUFJeUQsS0FBS3NILGVBQUwsS0FBeUIsSUFBN0IsRUFBbUM7QUFDakMsaUJBQ0UseUZBQ0UsMkNBRko7QUFJRDs7QUFFRCxlQUFPaEwsU0FBUDtBQUNELE9BVEQ7QUFVRDs7O2dEQUUyQjtBQUMxQixVQUFNa1YscUJBQXFCLEtBQUsvRSxnQ0FBTCxFQUEzQjtBQUNBLFVBQU1nRixxQkFBcUIsS0FBS2hNLHFCQUFMLEVBQTNCO0FBQ0EsVUFBSWlNLGtCQUFrQixDQUF0QjtBQUNBLFVBQUlDLGdCQUFnQixFQUFwQjtBQUNBLFVBQUlDLHVCQUFKOztBQUVBclcsUUFBRWlXLGtCQUFGLEVBQXNCbEUsSUFBdEIsQ0FBMkIsU0FBU3VFLGlCQUFULEdBQTZCO0FBQ3RELFlBQUlILG9CQUFvQixFQUF4QixFQUE0QjtBQUMxQjtBQUNBQywyQkFBaUIsT0FBakI7QUFDQSxpQkFBTyxLQUFQO0FBQ0Q7O0FBRURDLHlCQUFpQnJXLEVBQUUsSUFBRixFQUFRNkksT0FBUixDQUFnQnFOLGtCQUFoQixDQUFqQjtBQUNBRSxnQ0FBc0JDLGVBQWVsUixJQUFmLENBQW9CLE1BQXBCLENBQXRCO0FBQ0FnUiwyQkFBbUIsQ0FBbkI7O0FBRUEsZUFBTyxJQUFQO0FBQ0QsT0FaRDs7QUFjQSxhQUFPQyxhQUFQO0FBQ0Q7Ozt3Q0FFbUI7QUFDbEIsVUFBTTNSLE9BQU8sSUFBYjs7QUFFQTtBQUNBLFVBQUl6RSxFQUFFeUUsS0FBSytKLDZCQUFQLEVBQXNDN0osSUFBdEMsQ0FBMkMsTUFBM0MsTUFBdUQsR0FBM0QsRUFBZ0U7QUFDOUQzRSxVQUFFeUUsS0FBSytKLDZCQUFQLEVBQXNDN0osSUFBdEMsQ0FBMkMsYUFBM0MsRUFBMEQsT0FBMUQ7QUFDQTNFLFVBQUV5RSxLQUFLK0osNkJBQVAsRUFBc0M3SixJQUF0QyxDQUEyQyxhQUEzQyxFQUEwREYsS0FBS3FLLDBCQUEvRDtBQUNEOztBQUVELFVBQUk5TyxFQUFFeUUsS0FBS2dLLDRCQUFQLEVBQXFDOUosSUFBckMsQ0FBMEMsTUFBMUMsTUFBc0QsR0FBMUQsRUFBK0Q7QUFDN0QzRSxVQUFFeUUsS0FBS2dLLDRCQUFQLEVBQXFDOUosSUFBckMsQ0FBMEMsYUFBMUMsRUFBeUQsT0FBekQ7QUFDQTNFLFVBQUV5RSxLQUFLZ0ssNEJBQVAsRUFBcUM5SixJQUFyQyxDQUEwQyxhQUExQyxFQUF5REYsS0FBS3NLLHlCQUE5RDtBQUNEOztBQUVEL08sUUFBRSxNQUFGLEVBQVVnQixFQUFWLENBQWEsUUFBYixFQUF1QnlELEtBQUt1SyxpQkFBNUIsRUFBK0MsU0FBU3VILG9CQUFULENBQThCdlQsS0FBOUIsRUFBcUM7QUFDbEZBLGNBQU1rQyxjQUFOO0FBQ0FsQyxjQUFNMk8sZUFBTjs7QUFFQTNSLFVBQUVvSixJQUFGLENBQU87QUFDTEUsa0JBQVEsTUFESDtBQUVMekIsZUFBSzdILEVBQUUsSUFBRixFQUFRMkUsSUFBUixDQUFhLFFBQWIsQ0FGQTtBQUdMMEUsb0JBQVUsTUFITDtBQUlMbEUsZ0JBQU1uRixFQUFFLElBQUYsRUFBUXdXLFNBQVIsRUFKRDtBQUtMak4sc0JBQVksc0JBQU07QUFDaEJ2SixjQUFFeUUsS0FBS2lJLHlCQUFQLEVBQWtDaE0sSUFBbEM7QUFDQVYsY0FBRSwyQkFBRixFQUErQnlFLEtBQUt1SyxpQkFBcEMsRUFBdURwSCxJQUF2RDtBQUNEO0FBUkksU0FBUCxFQVNHNkIsSUFUSCxDQVNRLFVBQUMwSSxRQUFELEVBQWM7QUFDcEIsY0FBSUEsU0FBU3NFLE9BQVQsS0FBcUIsQ0FBekIsRUFBNEI7QUFDMUIxVyxtQkFBT2dKLFFBQVAsQ0FBZ0IyTixNQUFoQjtBQUNELFdBRkQsTUFFTztBQUNMMVcsY0FBRTBKLEtBQUYsQ0FBUUMsS0FBUixDQUFjLEVBQUNuSCxTQUFTMlAsU0FBUzNQLE9BQW5CLEVBQWQ7QUFDQXhDLGNBQUV5RSxLQUFLaUkseUJBQVAsRUFBa0M5RSxJQUFsQztBQUNBNUgsY0FBRSwyQkFBRixFQUErQnlFLEtBQUt1SyxpQkFBcEMsRUFBdURuRSxNQUF2RDtBQUNEO0FBQ0YsU0FqQkQ7QUFrQkQsT0F0QkQ7QUF1QkQ7OzswQ0FFcUI7QUFDcEIsVUFBTXBHLE9BQU8sSUFBYjtBQUNBLFVBQU1rUyxrQkFBa0IzVyxFQUFFeUUsS0FBS2lLLDRCQUFQLENBQXhCO0FBQ0FpSSxzQkFBZ0JoUyxJQUFoQixDQUFxQixhQUFyQixFQUFvQyxPQUFwQztBQUNBZ1Msc0JBQWdCaFMsSUFBaEIsQ0FBcUIsYUFBckIsRUFBb0NGLEtBQUtrSyxxQkFBekM7QUFDRDs7O21DQUVjO0FBQ2IsVUFBTWxLLE9BQU8sSUFBYjtBQUNBLFVBQU1yRCxPQUFPcEIsRUFBRSxNQUFGLENBQWI7QUFDQSxVQUFNNFcsV0FBVzVXLEVBQUUsV0FBRixDQUFqQjs7QUFFQTtBQUNBb0IsV0FBS0osRUFBTCxDQUFRLE9BQVIsRUFBaUIsS0FBS3VPLGdDQUF0QixFQUF3RCxZQUFNO0FBQzVEO0FBQ0F2UCxVQUNLeUUsS0FBSzJLLDJCQURWLFNBQ3lDM0ssS0FBSzZLLDJCQUQ5QyxTQUM2RTdLLEtBQUswSyw4QkFEbEYsRUFFRThDLE9BRkYsQ0FFVSxZQUFNO0FBQ2Q7Ozs7QUFJQTRFLHFCQUFXLFlBQU07QUFDZjdXLGNBQUV5RSxLQUFLeUsseUJBQVAsRUFBa0NyRSxNQUFsQyxDQUF5QyxZQUFNO0FBQzdDN0ssZ0JBQUV5RSxLQUFLaUwscUNBQVAsRUFBOEM5SCxJQUE5QztBQUNBNUgsZ0JBQUV5RSxLQUFLNEssdUNBQVAsRUFBZ0R6SCxJQUFoRDtBQUNBZ1AsdUJBQVMvUixVQUFULENBQW9CLE9BQXBCO0FBQ0QsYUFKRDtBQUtELFdBTkQsRUFNRyxHQU5IO0FBT0QsU0FkRDtBQWVBO0FBQ0QsT0FsQkQ7O0FBb0JBO0FBQ0F6RCxXQUFLSixFQUFMLENBQVEsaUJBQVIsRUFBMkIsS0FBSzJOLHFCQUFoQyxFQUF1RCxZQUFNO0FBQzNEM08sVUFBS3lFLEtBQUsySywyQkFBVixVQUEwQzNLLEtBQUs2SywyQkFBL0MsRUFBOEUxSCxJQUE5RTtBQUNBNUgsVUFBRXlFLEtBQUt5Syx5QkFBUCxFQUFrQ3hPLElBQWxDOztBQUVBa1csaUJBQVMvUixVQUFULENBQW9CLE9BQXBCO0FBQ0E3RSxVQUFFeUUsS0FBS2lMLHFDQUFQLEVBQThDOUgsSUFBOUM7QUFDQTVILFVBQUV5RSxLQUFLNEssdUNBQVAsRUFBZ0R6SCxJQUFoRDtBQUNBNUgsVUFBRXlFLEtBQUttSywyQkFBUCxFQUFvQzhDLElBQXBDLENBQXlDLEVBQXpDO0FBQ0ExUixVQUFFeUUsS0FBS2tMLDJCQUFQLEVBQW9DL0gsSUFBcEM7QUFDRCxPQVREOztBQVdBO0FBQ0F4RyxXQUFLSixFQUFMLENBQ0UsT0FERixxQkFFbUIsS0FBS3lPLG9DQUZ4QixVQUVpRSxLQUFLSix1Q0FGdEUsUUFHRSxVQUFDck0sS0FBRCxFQUFROFQsWUFBUixFQUF5QjtBQUN2QjtBQUNBLFlBQUksT0FBT0EsWUFBUCxLQUF3QixXQUE1QixFQUF5QztBQUN2QzlULGdCQUFNMk8sZUFBTjtBQUNBM08sZ0JBQU1rQyxjQUFOO0FBQ0Q7QUFDRixPQVRIOztBQVlBOUQsV0FBS0osRUFBTCxDQUFRLE9BQVIsRUFBaUIsS0FBS3lPLG9DQUF0QixFQUE0RCxVQUFDek0sS0FBRCxFQUFXO0FBQ3JFQSxjQUFNMk8sZUFBTjtBQUNBM08sY0FBTWtDLGNBQU47QUFDQTs7OztBQUlBbEYsVUFBRSxrQkFBRixFQUFzQnVJLE9BQXRCLENBQThCLE9BQTlCLEVBQXVDLENBQUMsZUFBRCxDQUF2QztBQUNELE9BUkQ7O0FBVUE7QUFDQW5ILFdBQUtKLEVBQUwsQ0FBUSxPQUFSLEVBQWlCLEtBQUtpTyx5QkFBdEIsRUFBaUQsWUFBTTtBQUNyRCxZQUFJeEssS0FBS3NILGVBQUwsS0FBeUIsSUFBN0IsRUFBbUM7QUFDakMvTCxZQUFFeUUsS0FBS2tLLHFCQUFQLEVBQThCck8sS0FBOUIsQ0FBb0MsTUFBcEM7QUFDRDtBQUNGLE9BSkQ7O0FBTUE7QUFDQWMsV0FBS0osRUFBTCxDQUFRLE9BQVIsRUFBaUIsS0FBS3FPLHVDQUF0QixFQUErRCxTQUFTMEgsaUNBQVQsQ0FBMkMvVCxLQUEzQyxFQUFrRDtBQUMvR0EsY0FBTTJPLGVBQU47QUFDQTNPLGNBQU1rQyxjQUFOO0FBQ0FuRixlQUFPZ0osUUFBUCxHQUFrQi9JLEVBQUUsSUFBRixFQUFRMkUsSUFBUixDQUFhLE1BQWIsQ0FBbEI7QUFDRCxPQUpEOztBQU1BO0FBQ0F2RCxXQUFLSixFQUFMLENBQVEsT0FBUixFQUFpQixLQUFLd08scUNBQXRCLEVBQTZELFlBQU07QUFDakV4UCxVQUFFeUUsS0FBS2lMLHFDQUFQLEVBQThDc0gsU0FBOUM7QUFDRCxPQUZEOztBQUlBO0FBQ0EsVUFBTUMsa0JBQWtCO0FBQ3RCcFAsYUFBSzlILE9BQU93RixVQUFQLENBQWtCMlIsWUFERDtBQUV0QkMsdUJBQWUsWUFGTztBQUd0QjtBQUNBQyxtQkFBVyxlQUpXO0FBS3RCQyxxQkFBYSxFQUxTLEVBS0w7QUFDakJDLHdCQUFnQixLQU5NO0FBT3RCQyx3QkFBZ0IsSUFQTTtBQVF0QkMsNEJBQW9CLEVBUkU7QUFTdEJDLDhCQUFzQmhULEtBQUtvSywwQkFUTDtBQVV0Qjs7OztBQUlBNkksaUJBQVMsQ0FkYTtBQWV0QkMsbUJBQVcscUJBQU07QUFDZmxULGVBQUttVCxrQkFBTDtBQUNELFNBakJxQjtBQWtCdEJDLG9CQUFZLHNCQUFNO0FBQ2hCO0FBQ0QsU0FwQnFCO0FBcUJ0QmxPLGVBQU8sZUFBQ21PLElBQUQsRUFBT3RWLE9BQVAsRUFBbUI7QUFDeEJpQyxlQUFLc1Qsb0JBQUwsQ0FBMEJ2VixPQUExQjtBQUNELFNBdkJxQjtBQXdCdEJ3VixrQkFBVSxrQkFBQ0YsSUFBRCxFQUFVO0FBQ2xCLGNBQUlBLEtBQUtyUSxNQUFMLEtBQWdCLE9BQXBCLEVBQTZCO0FBQzNCLGdCQUFNd1EsaUJBQWlCalksRUFBRWtZLFNBQUYsQ0FBWUosS0FBS0ssR0FBTCxDQUFTaEcsUUFBckIsQ0FBdkI7O0FBRUEsZ0JBQUksT0FBTzhGLGVBQWVHLGVBQXRCLEtBQTBDLFdBQTlDLEVBQTJESCxlQUFlRyxlQUFmLEdBQWlDLElBQWpDO0FBQzNELGdCQUFJLE9BQU9ILGVBQWVJLFdBQXRCLEtBQXNDLFdBQTFDLEVBQXVESixlQUFlSSxXQUFmLEdBQTZCLElBQTdCOztBQUV2RDVULGlCQUFLNlQsbUJBQUwsQ0FBeUJMLGNBQXpCO0FBQ0Q7QUFDRDtBQUNBeFQsZUFBS3NILGVBQUwsR0FBdUIsS0FBdkI7QUFDRDtBQW5DcUIsT0FBeEI7O0FBc0NBNkssZUFBU0EsUUFBVCxDQUFrQjVXLEVBQUV1WSxNQUFGLENBQVN0QixlQUFULENBQWxCO0FBQ0Q7Ozt5Q0FFb0I7QUFDbkIsVUFBTXhTLE9BQU8sSUFBYjtBQUNBLFVBQU1tUyxXQUFXNVcsRUFBRSxXQUFGLENBQWpCO0FBQ0E7QUFDQXlFLFdBQUtzSCxlQUFMLEdBQXVCLElBQXZCO0FBQ0EvTCxRQUFFeUUsS0FBS3lLLHlCQUFQLEVBQWtDdEgsSUFBbEMsQ0FBdUMsQ0FBdkM7QUFDQWdQLGVBQVM3RCxHQUFULENBQWEsUUFBYixFQUF1QixNQUF2QjtBQUNBL1MsUUFBRXlFLEtBQUswSyw4QkFBUCxFQUF1Q3RFLE1BQXZDO0FBQ0Q7OztxQ0FFZ0IvSCxRLEVBQVU7QUFDekIsVUFBTTJCLE9BQU8sSUFBYjtBQUNBekUsUUFBRXlFLEtBQUswSyw4QkFBUCxFQUNHcUosTUFESCxHQUVHdkcsT0FGSCxDQUVXblAsUUFGWDtBQUdEOztBQUVEOzs7Ozs7Ozt3Q0FLb0J5RCxNLEVBQVE7QUFDMUIsVUFBTTlCLE9BQU8sSUFBYjtBQUNBQSxXQUFLZ1UsZ0JBQUwsQ0FBc0IsWUFBTTtBQUMxQixZQUFJbFMsT0FBT2tCLE1BQVAsS0FBa0IsSUFBdEIsRUFBNEI7QUFDMUIsY0FBSWxCLE9BQU82UixlQUFQLEtBQTJCLElBQS9CLEVBQXFDO0FBQ25DLGdCQUFNTSxnQkFBZ0IzWSxPQUFPd0YsVUFBUCxDQUFrQm9ULGlCQUFsQixDQUFvQ3hPLE9BQXBDLENBQTRDLFVBQTVDLEVBQXdENUQsT0FBTzhSLFdBQS9ELENBQXRCO0FBQ0FyWSxjQUFFeUUsS0FBSzRLLHVDQUFQLEVBQWdEMUssSUFBaEQsQ0FBcUQsTUFBckQsRUFBNkQrVCxhQUE3RDtBQUNBMVksY0FBRXlFLEtBQUs0Syx1Q0FBUCxFQUFnRDNPLElBQWhEO0FBQ0Q7QUFDRFYsWUFBRXlFLEtBQUsySywyQkFBUCxFQUFvQ3ZFLE1BQXBDO0FBQ0QsU0FQRCxNQU9PLElBQUksT0FBT3RFLE9BQU9lLG9CQUFkLEtBQXVDLFdBQTNDLEVBQXdEO0FBQzdEN0MsZUFBS21VLHNCQUFMLENBQTRCclMsTUFBNUI7QUFDRCxTQUZNLE1BRUE7QUFDTHZHLFlBQUV5RSxLQUFLaUwscUNBQVAsRUFBOENnQyxJQUE5QyxDQUFtRG5MLE9BQU9zRCxHQUExRDtBQUNBN0osWUFBRXlFLEtBQUs2SywyQkFBUCxFQUFvQ3pFLE1BQXBDO0FBQ0Q7QUFDRixPQWREO0FBZUQ7O0FBRUQ7Ozs7Ozs7Ozt5Q0FNcUJySSxPLEVBQVM7QUFDNUIsVUFBTWlDLE9BQU8sSUFBYjtBQUNBQSxXQUFLZ1UsZ0JBQUwsQ0FBc0IsWUFBTTtBQUMxQnpZLFVBQUV5RSxLQUFLaUwscUNBQVAsRUFBOENnQyxJQUE5QyxDQUFtRGxQLE9BQW5EO0FBQ0F4QyxVQUFFeUUsS0FBSzZLLDJCQUFQLEVBQW9DekUsTUFBcEM7QUFDRCxPQUhEO0FBSUQ7O0FBRUQ7Ozs7Ozs7Ozs7OzJDQVF1QnRFLE0sRUFBUTtBQUM3QixVQUFNOUIsT0FBTyxJQUFiO0FBQ0EsVUFBTW5FLFFBQVFtRSxLQUFLc0csb0JBQUwsQ0FBMEJ0RSw4QkFBMUIsQ0FBeURGLE1BQXpELENBQWQ7QUFDQSxVQUFNc1MsYUFBYXRTLE9BQU9NLE1BQVAsQ0FBY0MsVUFBZCxDQUF5QkMsSUFBNUM7O0FBRUEvRyxRQUFFLEtBQUsyUCwyQkFBUCxFQUNHK0IsSUFESCxDQUNRcFIsTUFBTW9HLElBQU4sQ0FBVyxhQUFYLEVBQTBCZ0wsSUFBMUIsRUFEUixFQUVHN0csTUFGSDtBQUdBN0ssUUFBRSxLQUFLNE8sMkJBQVAsRUFDRzhDLElBREgsQ0FDUXBSLE1BQU1vRyxJQUFOLENBQVcsZUFBWCxFQUE0QmdMLElBQTVCLEVBRFIsRUFFRzdHLE1BRkg7O0FBSUE3SyxRQUFFLEtBQUs0TywyQkFBUCxFQUNHbEksSUFESCxDQUNRLGtCQURSLEVBRUdDLEdBRkgsQ0FFTyxPQUZQLEVBR0czRixFQUhILENBR00sT0FITixFQUdlLFlBQU07QUFDakJoQixVQUFFeUUsS0FBS2tMLDJCQUFQLEVBQW9DL0gsSUFBcEM7QUFDQTVILFVBQUV5RSxLQUFLbUssMkJBQVAsRUFBb0M4QyxJQUFwQyxDQUF5QyxFQUF6QztBQUNBak4sYUFBS21ULGtCQUFMOztBQUVBO0FBQ0E1WCxVQUFFOFksSUFBRixDQUFPdlMsT0FBT00sTUFBUCxDQUFjQyxVQUFkLENBQXlCaVMsSUFBekIsQ0FBOEJDLE9BQXJDLEVBQThDO0FBQzVDLDhDQUFvQztBQURRLFNBQTlDLEVBR0d2UCxJQUhILENBR1EsVUFBQ3RFLElBQUQsRUFBVTtBQUNkVixlQUFLNlQsbUJBQUwsQ0FBeUJuVCxLQUFLMFQsVUFBTCxDQUF6QjtBQUNELFNBTEgsRUFNR3BPLElBTkgsQ0FNUSxVQUFDdEYsSUFBRCxFQUFVO0FBQ2RWLGVBQUtzVCxvQkFBTCxDQUEwQjVTLEtBQUswVCxVQUFMLENBQTFCO0FBQ0QsU0FSSCxFQVNHak8sTUFUSCxDQVNVLFlBQU07QUFDWm5HLGVBQUtzSCxlQUFMLEdBQXVCLEtBQXZCO0FBQ0QsU0FYSDtBQVlELE9BckJIO0FBc0JEOzs7Z0RBRTJCO0FBQzFCLGFBQU8sS0FBS1QsY0FBTCxLQUF3QixLQUFLSixZQUE3QixHQUNILEtBQUt1Qyw4QkFERixHQUVILEtBQUtELDhCQUZUO0FBR0Q7Ozt1REFFa0M7QUFDakMsYUFBTyxLQUFLbEMsY0FBTCxLQUF3QixLQUFLSixZQUE3QixHQUNILEtBQUt5Qyw2QkFERixHQUVILEtBQUtELDZCQUZUO0FBR0Q7Ozs0Q0FFdUI7QUFDdEIsYUFBTyxLQUFLcEMsY0FBTCxLQUF3QixLQUFLSixZQUE3QixHQUE0QyxLQUFLaEgsc0JBQWpELEdBQTBFLEtBQUtELHNCQUF0RjtBQUNEOztBQUVEOzs7Ozs7OzRDQUl3QjtBQUN0QixVQUFNUSxPQUFPLElBQWI7QUFDQXpFLFFBQUVpWixPQUFGLENBQVVsWixPQUFPd0YsVUFBUCxDQUFrQjJULGtCQUE1QixFQUFnRHpVLEtBQUswVSx3QkFBckQsRUFBK0UxTyxJQUEvRSxDQUFvRixZQUFNO0FBQ3hGMk8sZ0JBQVF6UCxLQUFSLENBQWMsZ0RBQWQ7QUFDRCxPQUZEO0FBR0Q7Ozs2Q0FFd0IwUCxLLEVBQU87QUFDOUIsVUFBTUMsa0JBQWtCO0FBQ3RCQyxzQkFBY3ZaLEVBQUUsbUNBQUYsQ0FEUTtBQUV0QndaLG1CQUFXeFosRUFBRSw2QkFBRjtBQUZXLE9BQXhCOztBQUtBLDBCQUFZc1osZUFBWixFQUE2QkcsT0FBN0IsQ0FBcUMsVUFBQ0MsY0FBRCxFQUFvQjtBQUN2RCxZQUFJSixnQkFBZ0JJLGNBQWhCLEVBQWdDNVUsTUFBaEMsS0FBMkMsQ0FBL0MsRUFBa0Q7QUFDaER3VSwwQkFBZ0JJLGNBQWhCLEVBQWdDaFQsSUFBaEMsQ0FBcUMsdUJBQXJDLEVBQThEd0IsSUFBOUQsQ0FBbUVtUixNQUFNSyxjQUFOLENBQW5FO0FBQ0Q7QUFDRixPQUpEO0FBS0Q7Ozt1Q0FFa0I7QUFDakIsVUFBTWpWLE9BQU8sSUFBYjtBQUNBekUsUUFBRSxNQUFGLEVBQVVnQixFQUFWLENBQWEsT0FBYixFQUF5QnlELEtBQUt1SSxxQkFBOUIsVUFBd0R2SSxLQUFLd0kscUJBQTdELEVBQXNGLFlBQU07QUFDMUYsWUFBSTBNLGNBQWMsRUFBbEI7O0FBRUEsWUFBSWxWLEtBQUsrRyxlQUFMLENBQXFCMUcsTUFBekIsRUFBaUM7QUFDL0I2VSx3QkFBY0MsbUJBQW1CblYsS0FBSytHLGVBQUwsQ0FBcUJxTyxJQUFyQixDQUEwQixHQUExQixDQUFuQixDQUFkO0FBQ0Q7O0FBRUQ5WixlQUFPK1osSUFBUCxDQUFlclYsS0FBS21ILGFBQXBCLGdDQUE0RCtOLFdBQTVELEVBQTJFLFFBQTNFO0FBQ0QsT0FSRDtBQVNEOzs7eUNBRW9CO0FBQ25CLFVBQU1sVixPQUFPLElBQWI7O0FBRUF6RSxRQUFFLE1BQUYsRUFBVWdCLEVBQVYsQ0FBYSxPQUFiLEVBQXNCLEtBQUsrTCx3QkFBM0IsRUFBcUQsU0FBU2dOLHVCQUFULENBQWlDL1csS0FBakMsRUFBd0M7QUFDM0ZBLGNBQU0yTyxlQUFOO0FBQ0EzTyxjQUFNa0MsY0FBTjtBQUNBLFlBQU04VSxjQUFjaGEsRUFBRSxJQUFGLEVBQVFtRixJQUFSLENBQWEsY0FBYixDQUFwQjs7QUFFQTtBQUNBLFlBQUlWLEtBQUsrRyxlQUFMLENBQXFCMUcsTUFBekIsRUFBaUM7QUFDL0JMLGVBQUtvSCxhQUFMLENBQW1Cb08sU0FBbkIsQ0FBNkIsS0FBN0I7QUFDQXhWLGVBQUsrRyxlQUFMLEdBQXVCLEVBQXZCO0FBQ0Q7QUFDRCxZQUFNME8sd0JBQXdCbGEsRUFBS3lFLEtBQUtnSSxvQkFBViw0QkFBcUR1TixXQUFyRCxRQUE5Qjs7QUFFQSxZQUFJLENBQUNFLHNCQUFzQnBWLE1BQTNCLEVBQW1DO0FBQ2pDc1Usa0JBQVFlLElBQVIsNEJBQXNDSCxXQUF0QztBQUNBLGlCQUFPLEtBQVA7QUFDRDs7QUFFRDtBQUNBLFlBQUl2VixLQUFLOEcsdUJBQUwsS0FBaUMsSUFBckMsRUFBMkM7QUFDekN2TCxZQUFFeUUsS0FBS3FJLG9CQUFQLEVBQTZCbUYsT0FBN0I7QUFDQXhOLGVBQUs4Ryx1QkFBTCxHQUErQixLQUEvQjtBQUNEOztBQUVEO0FBQ0F2TCxVQUFLeUUsS0FBS2dJLG9CQUFWLDRCQUFxRHVOLFdBQXJELFNBQXNFM1MsS0FBdEU7QUFDQSxlQUFPLElBQVA7QUFDRCxPQTFCRDtBQTJCRDs7O3lDQUVvQjtBQUNuQixXQUFLaUUsY0FBTCxHQUFzQixLQUFLQSxjQUFMLEtBQXdCLEVBQXhCLEdBQTZCLEtBQUtILFlBQWxDLEdBQWlELEtBQUtELFlBQTVFO0FBQ0Q7OzswQ0FFcUI7QUFDcEIsVUFBTXpHLE9BQU8sSUFBYjs7QUFFQUEsV0FBS2tILGNBQUwsR0FBc0IzTCxFQUFFLEtBQUs2TSw2QkFBUCxFQUNuQm5HLElBRG1CLENBQ2QsVUFEYyxFQUVuQi9CLElBRm1CLENBRWQsT0FGYyxDQUF0QjtBQUdBLFVBQUksQ0FBQ0YsS0FBS2tILGNBQVYsRUFBMEI7QUFDeEJsSCxhQUFLa0gsY0FBTCxHQUFzQixhQUF0QjtBQUNEOztBQUVEM0wsUUFBRSxNQUFGLEVBQVVnQixFQUFWLENBQWEsUUFBYixFQUF1QnlELEtBQUtvSSw2QkFBNUIsRUFBMkQsU0FBU3VOLDJCQUFULEdBQXVDO0FBQ2hHM1YsYUFBS2tILGNBQUwsR0FBc0IzTCxFQUFFLElBQUYsRUFDbkIwRyxJQURtQixDQUNkLFVBRGMsRUFFbkIvQixJQUZtQixDQUVkLE9BRmMsQ0FBdEI7QUFHQUYsYUFBS3NNLHNCQUFMO0FBQ0QsT0FMRDtBQU1EOzs7aUNBRVlzSixtQixFQUFxQjtBQUNoQztBQUNBO0FBQ0EsVUFBTTNSLGdCQUFnQjFJLEVBQUUsc0JBQUYsRUFBMEI0RSxJQUExQixDQUErQixTQUEvQixDQUF0Qjs7QUFFQSxVQUFNMFYsa0JBQWtCO0FBQ3RCLDBCQUFrQixXQURJO0FBRXRCLHdCQUFnQixTQUZNO0FBR3RCLHVCQUFlLFFBSE87QUFJdEIsK0JBQXVCLGdCQUpEO0FBS3RCLDhCQUFzQixlQUxBO0FBTXRCLHNCQUFjO0FBTlEsT0FBeEI7O0FBU0E7QUFDQTtBQUNBO0FBQ0EsVUFBSSxPQUFPQSxnQkFBZ0JELG1CQUFoQixDQUFQLEtBQWdELFdBQXBELEVBQWlFO0FBQy9EcmEsVUFBRTBKLEtBQUYsQ0FBUUMsS0FBUixDQUFjO0FBQ1puSCxtQkFBU3pDLE9BQU9zUixxQkFBUCxDQUE2QixpQ0FBN0IsRUFBZ0VsSCxPQUFoRSxDQUF3RSxLQUF4RSxFQUErRWtRLG1CQUEvRTtBQURHLFNBQWQ7QUFHQSxlQUFPLEtBQVA7QUFDRDs7QUFFRDtBQUNBLFVBQU1FLDZCQUE2QixLQUFLckosZ0NBQUwsRUFBbkM7QUFDQSxVQUFNc0osbUJBQW1CRixnQkFBZ0JELG1CQUFoQixDQUF6Qjs7QUFFQSxVQUFJcmEsRUFBRXVhLDBCQUFGLEVBQThCelYsTUFBOUIsSUFBd0MsQ0FBNUMsRUFBK0M7QUFDN0NzVSxnQkFBUWUsSUFBUixDQUFhcGEsT0FBT3NSLHFCQUFQLENBQTZCLGtDQUE3QixDQUFiO0FBQ0EsZUFBTyxLQUFQO0FBQ0Q7O0FBRUQsVUFBTW9KLGlCQUFpQixFQUF2QjtBQUNBLFVBQUkzUSx1QkFBSjtBQUNBOUosUUFBRXVhLDBCQUFGLEVBQThCeEksSUFBOUIsQ0FBbUMsU0FBUzJJLGtCQUFULEdBQThCO0FBQy9ENVEseUJBQWlCOUosRUFBRSxJQUFGLEVBQVFtRixJQUFSLENBQWEsV0FBYixDQUFqQjtBQUNBc1YsdUJBQWV0UixJQUFmLENBQW9CO0FBQ2xCd0Isb0JBQVViLGNBRFE7QUFFbEI2USx5QkFBZTNhLEVBQUUsSUFBRixFQUNaNkksT0FEWSxDQUNKLDRCQURJLEVBRVorUixJQUZZO0FBRkcsU0FBcEI7QUFNRCxPQVJEOztBQVVBLFdBQUtDLG9CQUFMLENBQTBCSixjQUExQixFQUEwQ0QsZ0JBQTFDLEVBQTREOVIsYUFBNUQ7O0FBRUEsYUFBTyxJQUFQO0FBQ0Q7Ozt5Q0FFb0IrUixjLEVBQWdCRCxnQixFQUFrQjlSLGEsRUFBZTtBQUNwRSxVQUFNakUsT0FBTyxJQUFiOztBQUVBLFVBQUksT0FBT0EsS0FBS3NHLG9CQUFaLEtBQXFDLFdBQXpDLEVBQXNEO0FBQ3BEO0FBQ0Q7O0FBRUQ7QUFDQSxVQUFNK1Asa0JBQWtCQyxxQkFBcUJOLGNBQXJCLENBQXhCOztBQUVBLFVBQUksQ0FBQ0ssZ0JBQWdCaFcsTUFBckIsRUFBNkI7QUFDM0I7QUFDRDs7QUFFRCxVQUFJa1csNEJBQTRCRixnQkFBZ0JoVyxNQUFoQixHQUF5QixDQUF6RDtBQUNBLFVBQUlnRSxhQUFhOUksRUFBRSx1RUFBRixDQUFqQjs7QUFFQSxVQUFJOGEsZ0JBQWdCaFcsTUFBaEIsR0FBeUIsQ0FBN0IsRUFBZ0M7QUFDOUI7QUFDQTtBQUNBOUUsVUFBRStSLElBQUYsQ0FBTytJLGVBQVAsRUFBd0IsVUFBQ2hJLEtBQUQsRUFBUW1JLGNBQVIsRUFBMkI7QUFDakQsY0FBSW5JLFNBQVNnSSxnQkFBZ0JoVyxNQUFoQixHQUF5QixDQUF0QyxFQUF5QztBQUN2QztBQUNEO0FBQ0RvVyw4QkFBb0JELGNBQXBCLEVBQW9DLElBQXBDLEVBQTBDRSx1QkFBMUM7QUFDRCxTQUxEO0FBTUE7QUFDQSxZQUFNQyxlQUFlTixnQkFBZ0JBLGdCQUFnQmhXLE1BQWhCLEdBQXlCLENBQXpDLENBQXJCO0FBQ0EsWUFBTTZWLGdCQUFnQlMsYUFBYXZTLE9BQWIsQ0FBcUJwRSxLQUFLc0csb0JBQUwsQ0FBMEI1Ryx5QkFBL0MsQ0FBdEI7QUFDQXdXLHNCQUFjL1MsSUFBZDtBQUNBK1Msc0JBQWNuUixLQUFkLENBQW9CVixVQUFwQjtBQUNELE9BZEQsTUFjTztBQUNMb1MsNEJBQW9CSixnQkFBZ0IsQ0FBaEIsQ0FBcEI7QUFDRDs7QUFFRCxlQUFTSSxtQkFBVCxDQUE2QkQsY0FBN0IsRUFBNkN0UyxpQkFBN0MsRUFBZ0UwUyxrQkFBaEUsRUFBb0Y7QUFDbEY1VyxhQUFLc0csb0JBQUwsQ0FBMEI5RixtQkFBMUIsQ0FDRXVWLGdCQURGLEVBRUVTLGNBRkYsRUFHRXZTLGFBSEYsRUFJRUMsaUJBSkYsRUFLRTBTLGtCQUxGO0FBT0Q7O0FBRUQsZUFBU0YsdUJBQVQsR0FBbUM7QUFDakNILHFDQUE2QixDQUE3QjtBQUNBO0FBQ0E7QUFDQSxZQUFJQSw2QkFBNkIsQ0FBakMsRUFBb0M7QUFDbEMsY0FBSWxTLFVBQUosRUFBZ0I7QUFDZEEsdUJBQVczSCxNQUFYO0FBQ0EySCx5QkFBYSxJQUFiO0FBQ0Q7O0FBRUQsY0FBTXNTLGdCQUFlTixnQkFBZ0JBLGdCQUFnQmhXLE1BQWhCLEdBQXlCLENBQXpDLENBQXJCO0FBQ0EsY0FBTTZWLGlCQUFnQlMsY0FBYXZTLE9BQWIsQ0FBcUJwRSxLQUFLc0csb0JBQUwsQ0FBMEI1Ryx5QkFBL0MsQ0FBdEI7QUFDQXdXLHlCQUFjOVAsTUFBZDtBQUNBcVEsOEJBQW9CRSxhQUFwQjtBQUNEO0FBQ0Y7O0FBRUQsZUFBU0wsb0JBQVQsQ0FBOEJPLE9BQTlCLEVBQXVDO0FBQ3JDLFlBQU1DLFlBQVksRUFBbEI7QUFDQSxZQUFJTix1QkFBSjtBQUNBamIsVUFBRStSLElBQUYsQ0FBT3VKLE9BQVAsRUFBZ0IsVUFBQ3hJLEtBQUQsRUFBUTBJLFVBQVIsRUFBdUI7QUFDckNQLDJCQUFpQmpiLEVBQ2Z5RSxLQUFLc0csb0JBQUwsQ0FBMEJ2SCw0QkFBMUIsR0FBeURnWCxnQkFEMUMsRUFFZmdCLFdBQVdiLGFBRkksQ0FBakI7QUFJQSxjQUFJTSxlQUFlblcsTUFBZixHQUF3QixDQUE1QixFQUErQjtBQUM3QnlXLHNCQUFVcFMsSUFBVixDQUFlOFIsY0FBZjtBQUNELFdBRkQsTUFFTztBQUNMamIsY0FBRTBKLEtBQUYsQ0FBUUMsS0FBUixDQUFjO0FBQ1puSCx1QkFBU3pDLE9BQU9zUixxQkFBUCxDQUE2QixnREFBN0IsRUFDTmxILE9BRE0sQ0FDRSxLQURGLEVBQ1NxUSxnQkFEVCxFQUVOclEsT0FGTSxDQUVFLEtBRkYsRUFFU3FSLFdBQVc3USxRQUZwQjtBQURHLGFBQWQ7QUFLRDtBQUNGLFNBZEQ7O0FBZ0JBLGVBQU80USxTQUFQO0FBQ0Q7QUFDRjs7O3dDQUVtQjtBQUFBOztBQUNsQixVQUFNOVcsT0FBTyxJQUFiO0FBQ0F6RSxRQUFFLE1BQUYsRUFBVWdCLEVBQVYsQ0FBYSxPQUFiLEVBQXNCeUQsS0FBS21JLHdCQUEzQixFQUFxRCxTQUFTNk8sNEJBQVQsQ0FBc0N6WSxLQUF0QyxFQUE2QztBQUNoRyxZQUFNa1EsUUFBUWxULEVBQUUsSUFBRixDQUFkO0FBQ0EsWUFBTTBiLFFBQVExYixFQUFFa1QsTUFBTTBILElBQU4sRUFBRixDQUFkO0FBQ0E1WCxjQUFNa0MsY0FBTjs7QUFFQWdPLGNBQU10TCxJQUFOO0FBQ0E4VCxjQUFNaGIsSUFBTjs7QUFFQVYsVUFBRW9KLElBQUYsQ0FBTztBQUNMdkIsZUFBS3FMLE1BQU0vTixJQUFOLENBQVcsS0FBWCxDQURBO0FBRUxrRSxvQkFBVTtBQUZMLFNBQVAsRUFHR0ksSUFISCxDQUdRLFlBQU07QUFDWmlTLGdCQUFNekosT0FBTjtBQUNELFNBTEQ7QUFNRCxPQWREOztBQWdCQTtBQUNBalMsUUFBRSxNQUFGLEVBQVVnQixFQUFWLENBQWEsT0FBYixFQUFzQnlELEtBQUt5SSxnQkFBM0IsRUFBNkMsVUFBQ2xLLEtBQUQsRUFBVztBQUN0REEsY0FBTWtDLGNBQU47QUFDQSxZQUFNRSxvQkFBb0JyRixPQUFPc0YsaUJBQWpDOztBQUVBO0FBQ0EsWUFBTUMsa0JBQWtCckUsU0FBU1csYUFBVCxDQUF1QixHQUF2QixDQUF4QjtBQUNBMEQsd0JBQWdCekQsU0FBaEIsQ0FBMEJDLEdBQTFCLENBQThCLEtBQTlCLEVBQXFDLGFBQXJDLEVBQW9ELFFBQXBEO0FBQ0F3RCx3QkFBZ0JqRCxZQUFoQixDQUE2QixNQUE3QixFQUFxQ3RDLE9BQU93RixVQUFQLENBQWtCQyxlQUF2RDtBQUNBRix3QkFBZ0JuRCxTQUFoQixHQUE0QnBDLE9BQU8wRixrQkFBUCxDQUEwQkMsNEJBQXREOztBQUVBLFlBQU1pVyx3QkFBd0IsSUFBSTdiLGVBQUosQ0FDNUI7QUFDRU0sY0FBSSw2QkFETjtBQUVFa0Isd0JBQWN2QixPQUFPMEYsa0JBQVAsQ0FBMEJHLDRCQUYxQztBQUdFcEUsNEJBQWtCekIsT0FBTzBGLGtCQUFQLENBQTBCSSx1QkFIOUM7QUFJRXBFLDhCQUFvQjJELG9CQUNoQnJGLE9BQU8wRixrQkFBUCxDQUEwQkssd0JBRFYsR0FFaEIvRixPQUFPMEYsa0JBQVAsQ0FBMEJNLHVCQU5oQztBQU9FckUsOEJBQW9CMEQsb0JBQW9CLGFBQXBCLEdBQW9DLGVBUDFEO0FBUUU3RCwwQkFBZ0I2RCxvQkFBb0IsRUFBcEIsR0FBeUJyRixPQUFPMEYsa0JBQVAsQ0FBMEJPLCtCQVJyRTtBQVNFM0Ysb0JBQVUsSUFUWjtBQVVFc0IseUJBQWV5RCxvQkFBb0IsRUFBcEIsR0FBeUIsQ0FBQ0UsZUFBRDtBQVYxQyxTQUQ0QixFQWE1QixZQUFNO0FBQ0osY0FBSXRGLEVBQUV5RSxLQUFLMkksaUJBQVAsRUFBMEJ0SSxNQUExQixJQUFvQyxDQUF4QyxFQUEyQztBQUN6Q3NVLG9CQUFRZSxJQUFSLENBQWFwYSxPQUFPc1IscUJBQVAsQ0FBNkIseUNBQTdCLENBQWI7QUFDQSxtQkFBTyxLQUFQO0FBQ0Q7O0FBRUQsY0FBTW9KLGlCQUFpQixFQUF2QjtBQUNBLGNBQUkzUSx1QkFBSjtBQUNBOUosWUFBRXlFLEtBQUsySSxpQkFBUCxFQUEwQjJFLElBQTFCLENBQStCLFNBQVMySSxrQkFBVCxHQUE4QjtBQUMzRCxnQkFBTWtCLGlCQUFpQjViLEVBQUUsSUFBRixFQUFRNkksT0FBUixDQUFnQixtQkFBaEIsQ0FBdkI7QUFDQWlCLDZCQUFpQjhSLGVBQWV6VyxJQUFmLENBQW9CLFdBQXBCLENBQWpCO0FBQ0FzViwyQkFBZXRSLElBQWYsQ0FBb0I7QUFDbEJ3Qix3QkFBVWIsY0FEUTtBQUVsQjZRLDZCQUFlM2EsRUFBRSxpQkFBRixFQUFxQjRiLGNBQXJCO0FBRkcsYUFBcEI7QUFJRCxXQVBEOztBQVNBLGdCQUFLZixvQkFBTCxDQUEwQkosY0FBMUIsRUFBMEMsU0FBMUM7O0FBRUEsaUJBQU8sSUFBUDtBQUNELFNBakMyQixDQUE5Qjs7QUFvQ0FrQiw4QkFBc0JqYixJQUF0Qjs7QUFFQSxlQUFPLElBQVA7QUFDRCxPQWpERDtBQWtERDs7O3lDQUVvQjtBQUNuQixVQUFNK0QsT0FBTyxJQUFiO0FBQ0EsVUFBTXJELE9BQU9wQixFQUFFLE1BQUYsQ0FBYjtBQUNBb0IsV0FBS0osRUFBTCxDQUFRLE9BQVIsRUFBaUJ5RCxLQUFLZ0ksb0JBQXRCLEVBQTRDLFNBQVNvUCw2QkFBVCxHQUF5QztBQUNuRjtBQUNBcFgsYUFBS2dILGtCQUFMLEdBQTBCekwsRUFBRSxJQUFGLEVBQVFtRixJQUFSLENBQWEsY0FBYixDQUExQjtBQUNBVixhQUFLZ0gsa0JBQUwsR0FBMEJoSCxLQUFLZ0gsa0JBQUwsR0FBMEJvSSxPQUFPcFAsS0FBS2dILGtCQUFaLEVBQWdDZ0csV0FBaEMsRUFBMUIsR0FBMEUsSUFBcEc7QUFDQTtBQUNBelIsVUFBRXlFLEtBQUs4SCw2QkFBUCxFQUFzQ3JFLElBQXRDLENBQTJDbEksRUFBRSxJQUFGLEVBQVFtRixJQUFSLENBQWEsdUJBQWIsQ0FBM0M7QUFDQW5GLFVBQUV5RSxLQUFLa0ksd0JBQVAsRUFBaUNqTSxJQUFqQztBQUNBK0QsYUFBS3NNLHNCQUFMO0FBQ0QsT0FSRDs7QUFVQTNQLFdBQUtKLEVBQUwsQ0FBUSxPQUFSLEVBQWlCeUQsS0FBS2tJLHdCQUF0QixFQUFnRCxTQUFTbVAsa0NBQVQsR0FBOEM7QUFDNUYsWUFBTUMsVUFBVS9iLEVBQUV5RSxLQUFLK0gsZ0JBQVAsRUFBeUI3SCxJQUF6QixDQUE4QixpQkFBOUIsQ0FBaEI7QUFDQSxZQUFNcVgsbUJBQW1CRCxRQUFRRSxNQUFSLENBQWUsQ0FBZixFQUFrQkMsV0FBbEIsRUFBekI7QUFDQSxZQUFNQyxxQkFBcUJKLFFBQVFLLEtBQVIsQ0FBYyxDQUFkLENBQTNCO0FBQ0EsWUFBTUMsZUFBZUwsbUJBQW1CRyxrQkFBeEM7O0FBRUFuYyxVQUFFeUUsS0FBSzhILDZCQUFQLEVBQXNDckUsSUFBdEMsQ0FBMkNtVSxZQUEzQztBQUNBcmMsVUFBRSxJQUFGLEVBQVE0SCxJQUFSO0FBQ0FuRCxhQUFLZ0gsa0JBQUwsR0FBMEIsSUFBMUI7QUFDQWhILGFBQUtzTSxzQkFBTDtBQUNELE9BVkQ7QUFXRDs7O3NDQUVpQjtBQUFBOztBQUNoQixVQUFNdE0sT0FBTyxJQUFiO0FBQ0FBLFdBQUtvSCxhQUFMLEdBQXFCN0wsRUFBRSxvQkFBRixFQUF3QnNjLFFBQXhCLENBQWlDO0FBQ3BEQyx1QkFBZSx1QkFBQ0MsT0FBRCxFQUFhO0FBQzFCL1gsZUFBSytHLGVBQUwsR0FBdUJnUixPQUF2QjtBQUNBL1gsZUFBS3NNLHNCQUFMO0FBQ0QsU0FKbUQ7QUFLcEQwTCxxQkFBYSx1QkFBTTtBQUNqQmhZLGVBQUsrRyxlQUFMLEdBQXVCLEVBQXZCO0FBQ0EvRyxlQUFLc00sc0JBQUw7QUFDRCxTQVJtRDtBQVNwRDJMLDBCQUFrQjNjLE9BQU9zUixxQkFBUCxDQUE2QixzQkFBN0IsQ0FUa0M7QUFVcERzTCxzQkFBYyxJQVZzQztBQVdwRDVaLGlCQUFTMEI7QUFYMkMsT0FBakMsQ0FBckI7O0FBY0F6RSxRQUFFLE1BQUYsRUFBVWdCLEVBQVYsQ0FBYSxPQUFiLEVBQXNCLDRCQUF0QixFQUFvRCxVQUFDZ0MsS0FBRCxFQUFXO0FBQzdEQSxjQUFNa0MsY0FBTjtBQUNBbEMsY0FBTTJPLGVBQU47QUFDQTVSLGVBQU8rWixJQUFQLENBQVk5WixFQUFFLE1BQUYsRUFBUTJFLElBQVIsQ0FBYSxNQUFiLENBQVosRUFBa0MsUUFBbEM7QUFDRCxPQUpEO0FBS0Q7O0FBRUQ7Ozs7OzsrQ0FHMkI7QUFDekIsVUFBTUYsT0FBTyxJQUFiOztBQUVBekUsUUFBRSxNQUFGLEVBQVVnQixFQUFWLENBQWEsT0FBYixFQUFzQixxQkFBdEIsRUFBNkMsU0FBUzRiLFVBQVQsR0FBc0I7QUFDakUsWUFBTUMsV0FBVzdjLEVBQUUsSUFBRixFQUFRbUYsSUFBUixDQUFhLFFBQWIsQ0FBakI7QUFDQSxZQUFNMlgscUJBQXFCOWMsRUFBRSxJQUFGLEVBQVFrVSxRQUFSLENBQWlCLGdCQUFqQixDQUEzQjs7QUFFQSxZQUFJLE9BQU8ySSxRQUFQLEtBQW9CLFdBQXBCLElBQW1DQyx1QkFBdUIsS0FBOUQsRUFBcUU7QUFDbkVyWSxlQUFLc1ksc0JBQUwsQ0FBNEJGLFFBQTVCO0FBQ0FwWSxlQUFLNkcsY0FBTCxHQUFzQnVSLFFBQXRCO0FBQ0Q7QUFDRixPQVJEO0FBU0Q7OzsyQ0FFc0JBLFEsRUFBVTtBQUMvQixVQUFJQSxhQUFhLEtBQUszUixZQUFsQixJQUFrQzJSLGFBQWEsS0FBSzFSLFlBQXhELEVBQXNFO0FBQ3BFaU8sZ0JBQVF6UCxLQUFSLG1EQUE2RGtULFFBQTdEO0FBQ0E7QUFDRDs7QUFFRDdjLFFBQUUscUJBQUYsRUFBeUJzSyxXQUF6QixDQUFxQyxvQkFBckM7QUFDQXRLLDBCQUFrQjZjLFFBQWxCLEVBQThCeFMsUUFBOUIsQ0FBdUMsb0JBQXZDO0FBQ0EsV0FBS2lCLGNBQUwsR0FBc0J1UixRQUF0QjtBQUNBLFdBQUs5TCxzQkFBTDtBQUNEOzs7d0NBRW1CO0FBQ2xCLFVBQU10TSxPQUFPLElBQWI7O0FBRUF6RSxRQUFLeUUsS0FBSzJILGVBQVYsU0FBNkIzSCxLQUFLNEgsZUFBbEMsRUFBcURyTCxFQUFyRCxDQUF3RCxPQUF4RCxFQUFpRSxTQUFTZ2MsT0FBVCxHQUFtQjtBQUNsRnZZLGFBQUs0RyxzQkFBTCxDQUE0QnJMLEVBQUUsSUFBRixFQUFRbUYsSUFBUixDQUFhLFVBQWIsQ0FBNUIsSUFBd0QsSUFBeEQ7QUFDQW5GLFVBQUUsSUFBRixFQUFRcUssUUFBUixDQUFpQixRQUFqQjtBQUNBckssVUFBRSxJQUFGLEVBQ0c2SSxPQURILENBQ1dwRSxLQUFLMkgsZUFEaEIsRUFFRzFGLElBRkgsQ0FFUWpDLEtBQUs2SCxlQUZiLEVBR0doQyxXQUhILENBR2UsUUFIZjtBQUlBN0YsYUFBS3NNLHNCQUFMO0FBQ0QsT0FSRDs7QUFVQS9RLFFBQUt5RSxLQUFLMkgsZUFBVixTQUE2QjNILEtBQUs2SCxlQUFsQyxFQUFxRHRMLEVBQXJELENBQXdELE9BQXhELEVBQWlFLFNBQVNnYyxPQUFULEdBQW1CO0FBQ2xGdlksYUFBSzRHLHNCQUFMLENBQTRCckwsRUFBRSxJQUFGLEVBQVFtRixJQUFSLENBQWEsVUFBYixDQUE1QixJQUF3RCxLQUF4RDtBQUNBbkYsVUFBRSxJQUFGLEVBQVFxSyxRQUFSLENBQWlCLFFBQWpCO0FBQ0FySyxVQUFFLElBQUYsRUFDRzZJLE9BREgsQ0FDV3BFLEtBQUsySCxlQURoQixFQUVHMUYsSUFGSCxDQUVRakMsS0FBSzRILGVBRmIsRUFHRy9CLFdBSEgsQ0FHZSxRQUhmO0FBSUE3RixhQUFLc00sc0JBQUw7QUFDRCxPQVJEO0FBU0Q7Ozt5Q0FFb0I7QUFDbkIsVUFBTXRNLE9BQU8sSUFBYjtBQUNBLFVBQU13WSxxQkFBcUIsU0FBckJBLGtCQUFxQixDQUFDNVcsT0FBRCxFQUFVYyxLQUFWLEVBQW9CO0FBQzdDLFlBQU0rVixlQUFlN1csUUFBUTZCLElBQVIsR0FBZXFNLEtBQWYsQ0FBcUIsR0FBckIsQ0FBckI7QUFDQTJJLHFCQUFhLENBQWIsSUFBa0IvVixLQUFsQjtBQUNBZCxnQkFBUTZCLElBQVIsQ0FBYWdWLGFBQWFyRCxJQUFiLENBQWtCLEdBQWxCLENBQWI7QUFDRCxPQUpEOztBQU1BO0FBQ0EsVUFBTXNELGNBQWNuZCxFQUFFLG9CQUFGLENBQXBCOztBQUVBLFVBQUltZCxZQUFZclksTUFBWixHQUFxQixDQUF6QixFQUE0QjtBQUMxQnFZLG9CQUFZcEwsSUFBWixDQUFpQixTQUFTcUwsVUFBVCxHQUFzQjtBQUNyQyxjQUFNbEssUUFBUWxULEVBQUUsSUFBRixDQUFkO0FBQ0FpZCw2QkFDRS9KLE1BQU14TSxJQUFOLENBQVcsK0JBQVgsQ0FERixFQUVFd00sTUFBTTBILElBQU4sQ0FBVyxlQUFYLEVBQTRCbFUsSUFBNUIsQ0FBaUMsY0FBakMsRUFBaUQ1QixNQUZuRDtBQUlELFNBTkQ7O0FBUUE7QUFDRCxPQVZELE1BVU87QUFDTCxZQUFNdVksZUFBZXJkLEVBQUUsZUFBRixFQUFtQjBHLElBQW5CLENBQXdCLGNBQXhCLEVBQXdDNUIsTUFBN0Q7QUFDQW1ZLDJCQUFtQmpkLEVBQUUsK0JBQUYsQ0FBbkIsRUFBdURxZCxZQUF2RDs7QUFFQTtBQUNBLFlBQU1DLG1CQUNKN1ksS0FBSzZHLGNBQUwsS0FBd0I3RyxLQUFLMEcsWUFBN0IsR0FBNEMsS0FBSzhCLHFCQUFqRCxHQUF5RSxLQUFLRCxxQkFEaEY7QUFFQWhOLFVBQUVzZCxnQkFBRixFQUFvQnhWLE1BQXBCLENBQTJCdVYsaUJBQWlCLEtBQUtwUixXQUFMLENBQWlCbkgsTUFBakIsR0FBMEIsQ0FBdEU7O0FBRUEsWUFBSXVZLGlCQUFpQixDQUFyQixFQUF3QjtBQUN0QnJkLFlBQUUsNEJBQUYsRUFBZ0MyRSxJQUFoQyxDQUNFLE1BREYsRUFFSyxLQUFLaUgsYUFGVixnQ0FFa0RnTyxtQkFBbUIsS0FBS3BPLGVBQUwsQ0FBcUJxTyxJQUFyQixDQUEwQixHQUExQixDQUFuQixDQUZsRDtBQUlEO0FBQ0Y7QUFDRjs7O29DQUVlO0FBQ2QsYUFBTzdaLEVBQUUsS0FBS21OLGdCQUFQLEVBQXlCckksTUFBekIsS0FBb0MsQ0FBcEMsSUFBeUM5RSxFQUFFLEtBQUtxTixxQkFBUCxFQUE4QnZJLE1BQTlCLEtBQXlDLENBQXpGO0FBQ0Q7Ozs7O2tCQUdZZ0cscUI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ2h4Q2Y7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Y0F5QlkvSyxNO0lBQUxDLEMsV0FBQUEsQzs7QUFFUDs7Ozs7SUFJTXVkLFk7QUFDSiwwQkFBYztBQUFBOztBQUNaQSxpQkFBYUMsWUFBYjtBQUNBRCxpQkFBYUUsWUFBYjtBQUNEOzs7O21DQUVxQjtBQUNwQixVQUFNdkcsZUFBZWxYLEVBQUUsZ0JBQUYsQ0FBckI7QUFDQWtYLG1CQUFhN1AsS0FBYixDQUFtQixZQUFNO0FBQ3ZCNlAscUJBQWE3TSxRQUFiLENBQXNCLFNBQXRCLEVBQWlDLEdBQWpDLEVBQXNDcVQsUUFBdEM7QUFDRCxPQUZEOztBQUlBLGVBQVNBLFFBQVQsR0FBb0I7QUFDbEI3RyxtQkFDRSxZQUFNO0FBQ0pLLHVCQUFhNU0sV0FBYixDQUF5QixTQUF6QjtBQUNBNE0sdUJBQWE3TSxRQUFiLENBQXNCLFVBQXRCLEVBQWtDLEdBQWxDLEVBQXVDdkgsUUFBdkM7QUFDRCxTQUpILEVBS0UsSUFMRjtBQU9EO0FBQ0QsZUFBU0EsUUFBVCxHQUFvQjtBQUNsQitULG1CQUNFLFlBQU07QUFDSkssdUJBQWE1TSxXQUFiLENBQXlCLFVBQXpCO0FBQ0QsU0FISCxFQUlFLElBSkY7QUFNRDtBQUNGOzs7bUNBRXFCO0FBQ3BCdEssUUFBRSxNQUFGLEVBQVVnQixFQUFWLENBQ0UsT0FERixFQUVFLDBEQUZGLEVBR0UsVUFBQ2dDLEtBQUQsRUFBVztBQUNUQSxjQUFNa0MsY0FBTjtBQUNBLFlBQU15WSxlQUFlM2QsRUFBRWdELE1BQU1rRCxNQUFSLEVBQWdCZixJQUFoQixDQUFxQixRQUFyQixDQUFyQjs7QUFFQW5GLFVBQUU0ZCxHQUFGLENBQU01YSxNQUFNa0QsTUFBTixDQUFhMlgsSUFBbkIsRUFBeUIsVUFBQzFZLElBQUQsRUFBVTtBQUNqQ25GLFlBQUUyZCxZQUFGLEVBQWdCak0sSUFBaEIsQ0FBcUJ2TSxJQUFyQjtBQUNBbkYsWUFBRTJkLFlBQUYsRUFBZ0JyZCxLQUFoQjtBQUNELFNBSEQ7QUFJRCxPQVhIO0FBYUQ7Ozs7O2tCQUdZaWQsWTs7Ozs7Ozs7OztBQy9FZixrQkFBa0IsWUFBWSxtQkFBTyxDQUFDLHNGQUErQixzQjs7Ozs7Ozs7OztBQ0FyRSxrQkFBa0IsWUFBWSxtQkFBTyxDQUFDLDRGQUFrQyxzQjs7Ozs7Ozs7OztBQ0F4RSxrQkFBa0IsWUFBWSxtQkFBTyxDQUFDLDhHQUEyQyxzQjs7Ozs7Ozs7OztBQ0FqRixrQkFBa0IsWUFBWSxtQkFBTyxDQUFDLHdGQUFnQyxzQjs7Ozs7Ozs7Ozs7QUNBekQ7O0FBRWIsa0JBQWtCOztBQUVsQixlQUFlO0FBQ2Y7QUFDQTtBQUNBO0FBQ0EsRTs7Ozs7Ozs7Ozs7QUNSYTs7QUFFYixrQkFBa0I7O0FBRWxCLHNCQUFzQixtQkFBTyxDQUFDLHlHQUFtQzs7QUFFakU7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGLGVBQWU7QUFDZjtBQUNBLG1CQUFtQixrQkFBa0I7QUFDckM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUMsRzs7Ozs7Ozs7Ozs7QUMxQlk7O0FBRWIsa0JBQWtCOztBQUVsQixZQUFZLG1CQUFPLENBQUMsaUZBQXVCOztBQUUzQzs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0YsZUFBZTtBQUNmO0FBQ0EsNkNBQTZDLGdCQUFnQjtBQUM3RDtBQUNBOztBQUVBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQSxFOzs7Ozs7Ozs7O0FDcEJBLG1CQUFPLENBQUMsd0dBQW1DO0FBQzNDLG1CQUFPLENBQUMsOEZBQThCO0FBQ3RDLDZIQUEwRDs7Ozs7Ozs7Ozs7QUNGMUQsbUJBQU8sQ0FBQyxvR0FBaUM7QUFDekMsK0hBQTREOzs7Ozs7Ozs7OztBQ0Q1RCxtQkFBTyxDQUFDLHNIQUEwQztBQUNsRCxjQUFjLHdHQUFxQztBQUNuRDtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7O0FDSkEsbUJBQU8sQ0FBQyxnR0FBK0I7QUFDdkMsOEhBQTJEOzs7Ozs7Ozs7OztBQ0QzRDtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNIQSxlQUFlLG1CQUFPLENBQUMsMEVBQWM7QUFDckM7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7O0FDSkE7QUFDQTtBQUNBLGdCQUFnQixtQkFBTyxDQUFDLDRFQUFlO0FBQ3ZDLGVBQWUsbUJBQU8sQ0FBQywwRUFBYztBQUNyQyxzQkFBc0IsbUJBQU8sQ0FBQywwRkFBc0I7QUFDcEQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLLFlBQVksZUFBZTtBQUNoQztBQUNBLEtBQUs7QUFDTDtBQUNBOzs7Ozs7Ozs7OztBQ3RCQTtBQUNBLFVBQVUsbUJBQU8sQ0FBQyw4REFBUTtBQUMxQixVQUFVLG1CQUFPLENBQUMsOERBQVE7QUFDMUI7QUFDQSwyQkFBMkIsa0JBQWtCLEVBQUU7O0FBRS9DO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRyxZQUFZO0FBQ2Y7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7O0FDdEJBLGlCQUFpQjs7QUFFakI7QUFDQTtBQUNBOzs7Ozs7Ozs7OztBQ0pBLDZCQUE2QjtBQUM3Qix1Q0FBdUM7Ozs7Ozs7Ozs7OztBQ0QxQjtBQUNiLHNCQUFzQixtQkFBTyxDQUFDLDBFQUFjO0FBQzVDLGlCQUFpQixtQkFBTyxDQUFDLGtGQUFrQjs7QUFFM0M7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7O0FDUEE7QUFDQSxnQkFBZ0IsbUJBQU8sQ0FBQyw0RUFBZTtBQUN2QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7O0FDbkJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7O0FDSkE7QUFDQSxrQkFBa0IsbUJBQU8sQ0FBQyxrRUFBVTtBQUNwQyxpQ0FBaUMsUUFBUSxtQkFBbUIsVUFBVSxFQUFFLEVBQUU7QUFDMUUsQ0FBQzs7Ozs7Ozs7Ozs7QUNIRCxlQUFlLG1CQUFPLENBQUMsMEVBQWM7QUFDckMsZUFBZSxrR0FBNkI7QUFDNUM7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNOQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNIQSxhQUFhLG1CQUFPLENBQUMsb0VBQVc7QUFDaEMsV0FBVyxtQkFBTyxDQUFDLGdFQUFTO0FBQzVCLFVBQVUsbUJBQU8sQ0FBQyw4REFBUTtBQUMxQixXQUFXLG1CQUFPLENBQUMsZ0VBQVM7QUFDNUIsVUFBVSxtQkFBTyxDQUFDLDhEQUFRO0FBQzFCOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUVBQWlFO0FBQ2pFO0FBQ0Esa0ZBQWtGO0FBQ2xGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXO0FBQ1gsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSwrQ0FBK0M7QUFDL0M7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsY0FBYztBQUNkLGNBQWM7QUFDZCxjQUFjO0FBQ2QsY0FBYztBQUNkLGVBQWU7QUFDZixlQUFlO0FBQ2YsZUFBZTtBQUNmLGdCQUFnQjtBQUNoQjs7Ozs7Ozs7Ozs7QUM3REE7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNOQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EseUNBQXlDOzs7Ozs7Ozs7OztBQ0x6Qyx1QkFBdUI7QUFDdkI7QUFDQTtBQUNBOzs7Ozs7Ozs7OztBQ0hBLFNBQVMsbUJBQU8sQ0FBQywwRUFBYztBQUMvQixpQkFBaUIsbUJBQU8sQ0FBQyxrRkFBa0I7QUFDM0MsaUJBQWlCLG1CQUFPLENBQUMsOEVBQWdCO0FBQ3pDO0FBQ0EsQ0FBQztBQUNEO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNQQSxlQUFlLGtHQUE2QjtBQUM1Qzs7Ozs7Ozs7Ozs7QUNEQSxrQkFBa0IsbUJBQU8sQ0FBQyw4RUFBZ0IsTUFBTSxtQkFBTyxDQUFDLGtFQUFVO0FBQ2xFLCtCQUErQixtQkFBTyxDQUFDLDRFQUFlLGdCQUFnQixtQkFBbUIsVUFBVSxFQUFFLEVBQUU7QUFDdkcsQ0FBQzs7Ozs7Ozs7Ozs7QUNGRDtBQUNBLFVBQVUsbUJBQU8sQ0FBQyw4REFBUTtBQUMxQjtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNMQTtBQUNBLGdCQUFnQixtQkFBTyxDQUFDLDBFQUFjO0FBQ3RDLGVBQWUsbUJBQU8sQ0FBQyw4REFBUTtBQUMvQjs7QUFFQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7O0FDUEE7QUFDQTtBQUNBOzs7Ozs7Ozs7OztBQ0ZBO0FBQ0EsZUFBZSxtQkFBTyxDQUFDLDBFQUFjO0FBQ3JDO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7OztBQ1hhO0FBQ2IsYUFBYSxtQkFBTyxDQUFDLGtGQUFrQjtBQUN2QyxpQkFBaUIsbUJBQU8sQ0FBQyxrRkFBa0I7QUFDM0MscUJBQXFCLG1CQUFPLENBQUMsMEZBQXNCO0FBQ25EOztBQUVBO0FBQ0EsbUJBQU8sQ0FBQyxnRUFBUyxxQkFBcUIsbUJBQU8sQ0FBQyw4REFBUSw0QkFBNEIsYUFBYSxFQUFFOztBQUVqRztBQUNBLHFEQUFxRCw0QkFBNEI7QUFDakY7QUFDQTs7Ozs7Ozs7Ozs7O0FDWmE7QUFDYixjQUFjLG1CQUFPLENBQUMsc0VBQVk7QUFDbEMsY0FBYyxtQkFBTyxDQUFDLG9FQUFXO0FBQ2pDLGVBQWUsbUJBQU8sQ0FBQyx3RUFBYTtBQUNwQyxXQUFXLG1CQUFPLENBQUMsZ0VBQVM7QUFDNUIsZ0JBQWdCLG1CQUFPLENBQUMsMEVBQWM7QUFDdEMsa0JBQWtCLG1CQUFPLENBQUMsOEVBQWdCO0FBQzFDLHFCQUFxQixtQkFBTyxDQUFDLDBGQUFzQjtBQUNuRCxxQkFBcUIsbUJBQU8sQ0FBQyw0RUFBZTtBQUM1QyxlQUFlLG1CQUFPLENBQUMsOERBQVE7QUFDL0IsOENBQThDO0FBQzlDO0FBQ0E7QUFDQTs7QUFFQSw4QkFBOEIsYUFBYTs7QUFFM0M7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHlDQUF5QyxvQ0FBb0M7QUFDN0UsNkNBQTZDLG9DQUFvQztBQUNqRixLQUFLLDRCQUE0QixvQ0FBb0M7QUFDckU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGdCQUFnQixtQkFBbUI7QUFDbkM7QUFDQTtBQUNBLGtDQUFrQywyQkFBMkI7QUFDN0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBOzs7Ozs7Ozs7OztBQ3BFQSxlQUFlLG1CQUFPLENBQUMsOERBQVE7QUFDL0I7O0FBRUE7QUFDQTtBQUNBLGlDQUFpQyxxQkFBcUI7QUFDdEQ7QUFDQSxpQ0FBaUMsU0FBUyxFQUFFO0FBQzVDLENBQUMsWUFBWTs7QUFFYjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSw2QkFBNkIsU0FBUyxxQkFBcUI7QUFDM0QsaUNBQWlDLGFBQWE7QUFDOUM7QUFDQSxHQUFHLFlBQVk7QUFDZjtBQUNBOzs7Ozs7Ozs7OztBQ3JCQTs7Ozs7Ozs7Ozs7QUNBQTs7Ozs7Ozs7Ozs7QUNBQTtBQUNBLGVBQWUsbUJBQU8sQ0FBQywwRUFBYztBQUNyQyxVQUFVLG1CQUFPLENBQUMsNEVBQWU7QUFDakMsa0JBQWtCLG1CQUFPLENBQUMsa0ZBQWtCO0FBQzVDLGVBQWUsbUJBQU8sQ0FBQyw0RUFBZTtBQUN0Qyx5QkFBeUI7QUFDekI7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsZUFBZSxtQkFBTyxDQUFDLDRFQUFlO0FBQ3RDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxFQUFFLGlHQUE4QjtBQUNoQyw2QkFBNkI7QUFDN0I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBOzs7Ozs7Ozs7OztBQ3hDQSxlQUFlLG1CQUFPLENBQUMsMEVBQWM7QUFDckMscUJBQXFCLG1CQUFPLENBQUMsb0ZBQW1CO0FBQ2hELGtCQUFrQixtQkFBTyxDQUFDLGdGQUFpQjtBQUMzQzs7QUFFQSxTQUFTLEdBQUcsbUJBQU8sQ0FBQyw4RUFBZ0I7QUFDcEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUcsWUFBWTtBQUNmO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7OztBQ2ZBLFNBQVMsbUJBQU8sQ0FBQywwRUFBYztBQUMvQixlQUFlLG1CQUFPLENBQUMsMEVBQWM7QUFDckMsY0FBYyxtQkFBTyxDQUFDLDhFQUFnQjs7QUFFdEMsaUJBQWlCLG1CQUFPLENBQUMsOEVBQWdCO0FBQ3pDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7O0FDWkE7QUFDQSxVQUFVLG1CQUFPLENBQUMsOERBQVE7QUFDMUIsZUFBZSxtQkFBTyxDQUFDLDBFQUFjO0FBQ3JDLGVBQWUsbUJBQU8sQ0FBQyw0RUFBZTtBQUN0Qzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIOzs7Ozs7Ozs7OztBQ1pBLFVBQVUsbUJBQU8sQ0FBQyw4REFBUTtBQUMxQixnQkFBZ0IsbUJBQU8sQ0FBQyw0RUFBZTtBQUN2QyxtQkFBbUIsbUJBQU8sQ0FBQyxvRkFBbUI7QUFDOUMsZUFBZSxtQkFBTyxDQUFDLDRFQUFlOztBQUV0QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7O0FDaEJBO0FBQ0EsWUFBWSxtQkFBTyxDQUFDLGdHQUF5QjtBQUM3QyxrQkFBa0IsbUJBQU8sQ0FBQyxrRkFBa0I7O0FBRTVDO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNOQTtBQUNBLGNBQWMsbUJBQU8sQ0FBQyxvRUFBVztBQUNqQyxXQUFXLG1CQUFPLENBQUMsZ0VBQVM7QUFDNUIsWUFBWSxtQkFBTyxDQUFDLGtFQUFVO0FBQzlCO0FBQ0EsNkJBQTZCO0FBQzdCO0FBQ0E7QUFDQSxxREFBcUQsT0FBTyxFQUFFO0FBQzlEOzs7Ozs7Ozs7OztBQ1RBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7O0FDUEEsc0dBQW1DOzs7Ozs7Ozs7OztBQ0FuQyxVQUFVLGlHQUF5QjtBQUNuQyxVQUFVLG1CQUFPLENBQUMsOERBQVE7QUFDMUIsVUFBVSxtQkFBTyxDQUFDLDhEQUFROztBQUUxQjtBQUNBLG9FQUFvRSxpQ0FBaUM7QUFDckc7Ozs7Ozs7Ozs7O0FDTkEsYUFBYSxtQkFBTyxDQUFDLG9FQUFXO0FBQ2hDLFVBQVUsbUJBQU8sQ0FBQyw4REFBUTtBQUMxQjtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7O0FDSkEsV0FBVyxtQkFBTyxDQUFDLGdFQUFTO0FBQzVCLGFBQWEsbUJBQU8sQ0FBQyxvRUFBVztBQUNoQztBQUNBLGtEQUFrRDs7QUFFbEQ7QUFDQSxxRUFBcUU7QUFDckUsQ0FBQztBQUNEO0FBQ0EsUUFBUSxtQkFBTyxDQUFDLHNFQUFZO0FBQzVCO0FBQ0EsQ0FBQzs7Ozs7Ozs7Ozs7QUNYRCxnQkFBZ0IsbUJBQU8sQ0FBQyw0RUFBZTtBQUN2QyxjQUFjLG1CQUFPLENBQUMsc0VBQVk7QUFDbEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7OztBQ2hCQSxnQkFBZ0IsbUJBQU8sQ0FBQyw0RUFBZTtBQUN2QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7O0FDTkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7OztBQ0xBO0FBQ0EsY0FBYyxtQkFBTyxDQUFDLHNFQUFZO0FBQ2xDLGNBQWMsbUJBQU8sQ0FBQyxzRUFBWTtBQUNsQztBQUNBO0FBQ0E7Ozs7Ozs7Ozs7O0FDTEE7QUFDQSxnQkFBZ0IsbUJBQU8sQ0FBQyw0RUFBZTtBQUN2QztBQUNBO0FBQ0EsMkRBQTJEO0FBQzNEOzs7Ozs7Ozs7OztBQ0xBO0FBQ0EsY0FBYyxtQkFBTyxDQUFDLHNFQUFZO0FBQ2xDO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNKQTtBQUNBLGVBQWUsbUJBQU8sQ0FBQywwRUFBYztBQUNyQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNYQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7OztBQ0pBLFlBQVksbUJBQU8sQ0FBQyxvRUFBVztBQUMvQixVQUFVLG1CQUFPLENBQUMsOERBQVE7QUFDMUIsYUFBYSxnR0FBMkI7QUFDeEM7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7Ozs7Ozs7O0FDVkEsY0FBYyxtQkFBTyxDQUFDLHNFQUFZO0FBQ2xDLGVBQWUsbUJBQU8sQ0FBQyw4REFBUTtBQUMvQixnQkFBZ0IsbUJBQU8sQ0FBQywwRUFBYztBQUN0QyxpQkFBaUIsdUdBQW9DO0FBQ3JEO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7QUNQYTtBQUNiLFVBQVUsbUJBQU8sQ0FBQyw4REFBUTtBQUMxQixjQUFjLG1CQUFPLENBQUMsb0VBQVc7QUFDakMsZUFBZSxtQkFBTyxDQUFDLDBFQUFjO0FBQ3JDLFdBQVcsbUJBQU8sQ0FBQywwRUFBYztBQUNqQyxrQkFBa0IsbUJBQU8sQ0FBQyxrRkFBa0I7QUFDNUMsZUFBZSxtQkFBTyxDQUFDLDBFQUFjO0FBQ3JDLHFCQUFxQixtQkFBTyxDQUFDLHNGQUFvQjtBQUNqRCxnQkFBZ0IsbUJBQU8sQ0FBQyxzR0FBNEI7O0FBRXBELGlDQUFpQyxtQkFBTyxDQUFDLDhFQUFnQixtQkFBbUIsa0JBQWtCLEVBQUU7QUFDaEc7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx1REFBdUQsZ0NBQWdDO0FBQ3ZGO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQSxrQ0FBa0MsZ0JBQWdCO0FBQ2xEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUM7Ozs7Ozs7Ozs7O0FDcENEO0FBQ0EsY0FBYyxtQkFBTyxDQUFDLG9FQUFXOztBQUVqQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7Ozs7Ozs7Ozs7QUNSRCxjQUFjLG1CQUFPLENBQUMsb0VBQVc7QUFDakM7QUFDQSxpQ0FBaUMsbUJBQU8sQ0FBQyw4RUFBZ0IsY0FBYyxpQkFBaUIsaUdBQXlCLEVBQUU7Ozs7Ozs7Ozs7O0FDRm5IO0FBQ0EsZUFBZSxtQkFBTyxDQUFDLDBFQUFjO0FBQ3JDLFlBQVksbUJBQU8sQ0FBQyw4RUFBZ0I7O0FBRXBDLG1CQUFPLENBQUMsNEVBQWU7QUFDdkI7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7Ozs7Ozs7Ozs7O0FDUlk7QUFDYixVQUFVLG1CQUFPLENBQUMsMEVBQWM7O0FBRWhDO0FBQ0EsbUJBQU8sQ0FBQyw4RUFBZ0I7QUFDeEIsNkJBQTZCO0FBQzdCLGNBQWM7QUFDZDtBQUNBLENBQUM7QUFDRDtBQUNBO0FBQ0E7QUFDQSxpQ0FBaUM7QUFDakM7QUFDQTtBQUNBLFVBQVU7QUFDVixDQUFDOzs7Ozs7Ozs7Ozs7QUNoQkQsa0M7Ozs7OztVQ0FBO1VBQ0E7O1VBRUE7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7O1VBRUE7VUFDQTs7VUFFQTtVQUNBO1VBQ0E7Ozs7Ozs7Ozs7OztBQ0dBOzs7O0FBQ0E7Ozs7QUFDQTs7Ozs7O2NBRVl4ZCxNO0lBQUxDLEMsV0FBQUEsQyxFQTdCUDs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQStCQUEsRUFBRSxZQUFNO0FBQ04sTUFBTStLLHVCQUF1QixJQUFJeEgsb0JBQUosRUFBN0I7QUFDQSxNQUFJZ2EsZ0JBQUo7QUFDQSxNQUFJelMsb0JBQUosQ0FBMEJDLG9CQUExQjtBQUNELENBSkQsRSIsImZpbGUiOiJtb2R1bGUuYnVuZGxlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiBDb3B5cmlnaHQgc2luY2UgMjAwNyBQcmVzdGFTaG9wIFNBIGFuZCBDb250cmlidXRvcnNcbiAqIFByZXN0YVNob3AgaXMgYW4gSW50ZXJuYXRpb25hbCBSZWdpc3RlcmVkIFRyYWRlbWFyayAmIFByb3BlcnR5IG9mIFByZXN0YVNob3AgU0FcbiAqXG4gKiBOT1RJQ0UgT0YgTElDRU5TRVxuICpcbiAqIFRoaXMgc291cmNlIGZpbGUgaXMgc3ViamVjdCB0byB0aGUgT3BlbiBTb2Z0d2FyZSBMaWNlbnNlIChPU0wgMy4wKVxuICogdGhhdCBpcyBidW5kbGVkIHdpdGggdGhpcyBwYWNrYWdlIGluIHRoZSBmaWxlIExJQ0VOU0UubWQuXG4gKiBJdCBpcyBhbHNvIGF2YWlsYWJsZSB0aHJvdWdoIHRoZSB3b3JsZC13aWRlLXdlYiBhdCB0aGlzIFVSTDpcbiAqIGh0dHBzOi8vb3BlbnNvdXJjZS5vcmcvbGljZW5zZXMvT1NMLTMuMFxuICogSWYgeW91IGRpZCBub3QgcmVjZWl2ZSBhIGNvcHkgb2YgdGhlIGxpY2Vuc2UgYW5kIGFyZSB1bmFibGUgdG9cbiAqIG9idGFpbiBpdCB0aHJvdWdoIHRoZSB3b3JsZC13aWRlLXdlYiwgcGxlYXNlIHNlbmQgYW4gZW1haWxcbiAqIHRvIGxpY2Vuc2VAcHJlc3Rhc2hvcC5jb20gc28gd2UgY2FuIHNlbmQgeW91IGEgY29weSBpbW1lZGlhdGVseS5cbiAqXG4gKiBESVNDTEFJTUVSXG4gKlxuICogRG8gbm90IGVkaXQgb3IgYWRkIHRvIHRoaXMgZmlsZSBpZiB5b3Ugd2lzaCB0byB1cGdyYWRlIFByZXN0YVNob3AgdG8gbmV3ZXJcbiAqIHZlcnNpb25zIGluIHRoZSBmdXR1cmUuIElmIHlvdSB3aXNoIHRvIGN1c3RvbWl6ZSBQcmVzdGFTaG9wIGZvciB5b3VyXG4gKiBuZWVkcyBwbGVhc2UgcmVmZXIgdG8gaHR0cHM6Ly9kZXZkb2NzLnByZXN0YXNob3AuY29tLyBmb3IgbW9yZSBpbmZvcm1hdGlvbi5cbiAqXG4gKiBAYXV0aG9yICAgIFByZXN0YVNob3AgU0EgYW5kIENvbnRyaWJ1dG9ycyA8Y29udGFjdEBwcmVzdGFzaG9wLmNvbT5cbiAqIEBjb3B5cmlnaHQgU2luY2UgMjAwNyBQcmVzdGFTaG9wIFNBIGFuZCBDb250cmlidXRvcnNcbiAqIEBsaWNlbnNlICAgaHR0cHM6Ly9vcGVuc291cmNlLm9yZy9saWNlbnNlcy9PU0wtMy4wIE9wZW4gU29mdHdhcmUgTGljZW5zZSAoT1NMIDMuMClcbiAqL1xuXG5jb25zdCB7JH0gPSB3aW5kb3c7XG5cbi8qKlxuICogQ29uZmlybU1vZGFsIGNvbXBvbmVudFxuICpcbiAqIEBwYXJhbSB7U3RyaW5nfSBpZFxuICogQHBhcmFtIHtTdHJpbmd9IGNvbmZpcm1UaXRsZVxuICogQHBhcmFtIHtTdHJpbmd9IGNvbmZpcm1NZXNzYWdlXG4gKiBAcGFyYW0ge1N0cmluZ30gY2xvc2VCdXR0b25MYWJlbFxuICogQHBhcmFtIHtTdHJpbmd9IGNvbmZpcm1CdXR0b25MYWJlbFxuICogQHBhcmFtIHtTdHJpbmd9IGNvbmZpcm1CdXR0b25DbGFzc1xuICogQHBhcmFtIHtBcnJheX0gY3VzdG9tQnV0dG9uc1xuICogQHBhcmFtIHtCb29sZWFufSBjbG9zYWJsZVxuICogQHBhcmFtIHtGdW5jdGlvbn0gY29uZmlybUNhbGxiYWNrXG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSBjYW5jZWxDYWxsYmFja1xuICpcbiAqL1xuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gQ29uZmlybU1vZGFsKHBhcmFtcywgY29uZmlybUNhbGxiYWNrLCBjYW5jZWxDYWxsYmFjaykge1xuICAvLyBDb25zdHJ1Y3QgdGhlIG1vZGFsXG4gIGNvbnN0IHtpZCwgY2xvc2FibGV9ID0gcGFyYW1zO1xuICB0aGlzLm1vZGFsID0gTW9kYWwocGFyYW1zKTtcblxuICAvLyBqUXVlcnkgbW9kYWwgb2JqZWN0XG4gIHRoaXMuJG1vZGFsID0gJCh0aGlzLm1vZGFsLmNvbnRhaW5lcik7XG5cbiAgdGhpcy5zaG93ID0gKCkgPT4ge1xuICAgIHRoaXMuJG1vZGFsLm1vZGFsKCk7XG4gIH07XG5cbiAgdGhpcy5tb2RhbC5jb25maXJtQnV0dG9uLmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgY29uZmlybUNhbGxiYWNrKTtcblxuICB0aGlzLiRtb2RhbC5tb2RhbCh7XG4gICAgYmFja2Ryb3A6IGNsb3NhYmxlID8gdHJ1ZSA6ICdzdGF0aWMnLFxuICAgIGtleWJvYXJkOiBjbG9zYWJsZSAhPT0gdW5kZWZpbmVkID8gY2xvc2FibGUgOiB0cnVlLFxuICAgIGNsb3NhYmxlOiBjbG9zYWJsZSAhPT0gdW5kZWZpbmVkID8gY2xvc2FibGUgOiB0cnVlLFxuICAgIHNob3c6IGZhbHNlLFxuICB9KTtcblxuICB0aGlzLiRtb2RhbC5vbignaGlkZGVuLmJzLm1vZGFsJywgKCkgPT4ge1xuICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoYCMke2lkfWApLnJlbW92ZSgpO1xuICAgIGlmIChjYW5jZWxDYWxsYmFjaykge1xuICAgICAgY2FuY2VsQ2FsbGJhY2soKTtcbiAgICB9XG4gIH0pO1xuXG4gIGRvY3VtZW50LmJvZHkuYXBwZW5kQ2hpbGQodGhpcy5tb2RhbC5jb250YWluZXIpO1xufVxuXG4vKipcbiAqIE1vZGFsIGNvbXBvbmVudCB0byBpbXByb3ZlIGxpc2liaWxpdHkgYnkgY29uc3RydWN0aW5nIHRoZSBtb2RhbCBvdXRzaWRlIHRoZSBtYWluIGZ1bmN0aW9uXG4gKlxuICogQHBhcmFtIHtPYmplY3R9IHBhcmFtc1xuICpcbiAqL1xuZnVuY3Rpb24gTW9kYWwoe1xuICBpZCA9ICdjb25maXJtLW1vZGFsJyxcbiAgY29uZmlybVRpdGxlLFxuICBjb25maXJtTWVzc2FnZSA9ICcnLFxuICBjbG9zZUJ1dHRvbkxhYmVsID0gJ0Nsb3NlJyxcbiAgY29uZmlybUJ1dHRvbkxhYmVsID0gJ0FjY2VwdCcsXG4gIGNvbmZpcm1CdXR0b25DbGFzcyA9ICdidG4tcHJpbWFyeScsXG4gIGN1c3RvbUJ1dHRvbnMgPSBbXSxcbn0pIHtcbiAgY29uc3QgbW9kYWwgPSB7fTtcblxuICAvLyBNYWluIG1vZGFsIGVsZW1lbnRcbiAgbW9kYWwuY29udGFpbmVyID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnZGl2Jyk7XG4gIG1vZGFsLmNvbnRhaW5lci5jbGFzc0xpc3QuYWRkKCdtb2RhbCcsICdmYWRlJyk7XG4gIG1vZGFsLmNvbnRhaW5lci5pZCA9IGlkO1xuXG4gIC8vIE1vZGFsIGRpYWxvZyBlbGVtZW50XG4gIG1vZGFsLmRpYWxvZyA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2RpdicpO1xuICBtb2RhbC5kaWFsb2cuY2xhc3NMaXN0LmFkZCgnbW9kYWwtZGlhbG9nJyk7XG5cbiAgLy8gTW9kYWwgY29udGVudCBlbGVtZW50XG4gIG1vZGFsLmNvbnRlbnQgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdkaXYnKTtcbiAgbW9kYWwuY29udGVudC5jbGFzc0xpc3QuYWRkKCdtb2RhbC1jb250ZW50Jyk7XG5cbiAgLy8gTW9kYWwgaGVhZGVyIGVsZW1lbnRcbiAgbW9kYWwuaGVhZGVyID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnZGl2Jyk7XG4gIG1vZGFsLmhlYWRlci5jbGFzc0xpc3QuYWRkKCdtb2RhbC1oZWFkZXInKTtcblxuICAvLyBNb2RhbCB0aXRsZSBlbGVtZW50XG4gIGlmIChjb25maXJtVGl0bGUpIHtcbiAgICBtb2RhbC50aXRsZSA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2g0Jyk7XG4gICAgbW9kYWwudGl0bGUuY2xhc3NMaXN0LmFkZCgnbW9kYWwtdGl0bGUnKTtcbiAgICBtb2RhbC50aXRsZS5pbm5lckhUTUwgPSBjb25maXJtVGl0bGU7XG4gIH1cblxuICAvLyBNb2RhbCBjbG9zZSBidXR0b24gaWNvblxuICBtb2RhbC5jbG9zZUljb24gPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdidXR0b24nKTtcbiAgbW9kYWwuY2xvc2VJY29uLmNsYXNzTGlzdC5hZGQoJ2Nsb3NlJyk7XG4gIG1vZGFsLmNsb3NlSWNvbi5zZXRBdHRyaWJ1dGUoJ3R5cGUnLCAnYnV0dG9uJyk7XG4gIG1vZGFsLmNsb3NlSWNvbi5kYXRhc2V0LmRpc21pc3MgPSAnbW9kYWwnO1xuICBtb2RhbC5jbG9zZUljb24uaW5uZXJIVE1MID0gJ8OXJztcblxuICAvLyBNb2RhbCBib2R5IGVsZW1lbnRcbiAgbW9kYWwuYm9keSA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2RpdicpO1xuICBtb2RhbC5ib2R5LmNsYXNzTGlzdC5hZGQoJ21vZGFsLWJvZHknLCAndGV4dC1sZWZ0JywgJ2ZvbnQtd2VpZ2h0LW5vcm1hbCcpO1xuXG4gIC8vIE1vZGFsIG1lc3NhZ2UgZWxlbWVudFxuICBtb2RhbC5tZXNzYWdlID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgncCcpO1xuICBtb2RhbC5tZXNzYWdlLmNsYXNzTGlzdC5hZGQoJ2NvbmZpcm0tbWVzc2FnZScpO1xuICBtb2RhbC5tZXNzYWdlLmlubmVySFRNTCA9IGNvbmZpcm1NZXNzYWdlO1xuXG4gIC8vIE1vZGFsIGZvb3RlciBlbGVtZW50XG4gIG1vZGFsLmZvb3RlciA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2RpdicpO1xuICBtb2RhbC5mb290ZXIuY2xhc3NMaXN0LmFkZCgnbW9kYWwtZm9vdGVyJyk7XG5cbiAgLy8gTW9kYWwgY2xvc2UgYnV0dG9uIGVsZW1lbnRcbiAgbW9kYWwuY2xvc2VCdXR0b24gPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdidXR0b24nKTtcbiAgbW9kYWwuY2xvc2VCdXR0b24uc2V0QXR0cmlidXRlKCd0eXBlJywgJ2J1dHRvbicpO1xuICBtb2RhbC5jbG9zZUJ1dHRvbi5jbGFzc0xpc3QuYWRkKCdidG4nLCAnYnRuLW91dGxpbmUtc2Vjb25kYXJ5JywgJ2J0bi1sZycpO1xuICBtb2RhbC5jbG9zZUJ1dHRvbi5kYXRhc2V0LmRpc21pc3MgPSAnbW9kYWwnO1xuICBtb2RhbC5jbG9zZUJ1dHRvbi5pbm5lckhUTUwgPSBjbG9zZUJ1dHRvbkxhYmVsO1xuXG4gIC8vIE1vZGFsIGNvbmZpcm0gYnV0dG9uIGVsZW1lbnRcbiAgbW9kYWwuY29uZmlybUJ1dHRvbiA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2J1dHRvbicpO1xuICBtb2RhbC5jb25maXJtQnV0dG9uLnNldEF0dHJpYnV0ZSgndHlwZScsICdidXR0b24nKTtcbiAgbW9kYWwuY29uZmlybUJ1dHRvbi5jbGFzc0xpc3QuYWRkKCdidG4nLCBjb25maXJtQnV0dG9uQ2xhc3MsICdidG4tbGcnLCAnYnRuLWNvbmZpcm0tc3VibWl0Jyk7XG4gIG1vZGFsLmNvbmZpcm1CdXR0b24uZGF0YXNldC5kaXNtaXNzID0gJ21vZGFsJztcbiAgbW9kYWwuY29uZmlybUJ1dHRvbi5pbm5lckhUTUwgPSBjb25maXJtQnV0dG9uTGFiZWw7XG5cbiAgLy8gQ29uc3RydWN0aW5nIHRoZSBtb2RhbFxuICBpZiAoY29uZmlybVRpdGxlKSB7XG4gICAgbW9kYWwuaGVhZGVyLmFwcGVuZChtb2RhbC50aXRsZSwgbW9kYWwuY2xvc2VJY29uKTtcbiAgfSBlbHNlIHtcbiAgICBtb2RhbC5oZWFkZXIuYXBwZW5kQ2hpbGQobW9kYWwuY2xvc2VJY29uKTtcbiAgfVxuXG4gIG1vZGFsLmJvZHkuYXBwZW5kQ2hpbGQobW9kYWwubWVzc2FnZSk7XG4gIG1vZGFsLmZvb3Rlci5hcHBlbmQobW9kYWwuY2xvc2VCdXR0b24sIC4uLmN1c3RvbUJ1dHRvbnMsIG1vZGFsLmNvbmZpcm1CdXR0b24pO1xuICBtb2RhbC5jb250ZW50LmFwcGVuZChtb2RhbC5oZWFkZXIsIG1vZGFsLmJvZHksIG1vZGFsLmZvb3Rlcik7XG4gIG1vZGFsLmRpYWxvZy5hcHBlbmRDaGlsZChtb2RhbC5jb250ZW50KTtcbiAgbW9kYWwuY29udGFpbmVyLmFwcGVuZENoaWxkKG1vZGFsLmRpYWxvZyk7XG5cbiAgcmV0dXJuIG1vZGFsO1xufVxuIiwiLyoqXG4gKiBDb3B5cmlnaHQgc2luY2UgMjAwNyBQcmVzdGFTaG9wIFNBIGFuZCBDb250cmlidXRvcnNcbiAqIFByZXN0YVNob3AgaXMgYW4gSW50ZXJuYXRpb25hbCBSZWdpc3RlcmVkIFRyYWRlbWFyayAmIFByb3BlcnR5IG9mIFByZXN0YVNob3AgU0FcbiAqXG4gKiBOT1RJQ0UgT0YgTElDRU5TRVxuICpcbiAqIFRoaXMgc291cmNlIGZpbGUgaXMgc3ViamVjdCB0byB0aGUgT3BlbiBTb2Z0d2FyZSBMaWNlbnNlIChPU0wgMy4wKVxuICogdGhhdCBpcyBidW5kbGVkIHdpdGggdGhpcyBwYWNrYWdlIGluIHRoZSBmaWxlIExJQ0VOU0UubWQuXG4gKiBJdCBpcyBhbHNvIGF2YWlsYWJsZSB0aHJvdWdoIHRoZSB3b3JsZC13aWRlLXdlYiBhdCB0aGlzIFVSTDpcbiAqIGh0dHBzOi8vb3BlbnNvdXJjZS5vcmcvbGljZW5zZXMvT1NMLTMuMFxuICogSWYgeW91IGRpZCBub3QgcmVjZWl2ZSBhIGNvcHkgb2YgdGhlIGxpY2Vuc2UgYW5kIGFyZSB1bmFibGUgdG9cbiAqIG9idGFpbiBpdCB0aHJvdWdoIHRoZSB3b3JsZC13aWRlLXdlYiwgcGxlYXNlIHNlbmQgYW4gZW1haWxcbiAqIHRvIGxpY2Vuc2VAcHJlc3Rhc2hvcC5jb20gc28gd2UgY2FuIHNlbmQgeW91IGEgY29weSBpbW1lZGlhdGVseS5cbiAqXG4gKiBESVNDTEFJTUVSXG4gKlxuICogRG8gbm90IGVkaXQgb3IgYWRkIHRvIHRoaXMgZmlsZSBpZiB5b3Ugd2lzaCB0byB1cGdyYWRlIFByZXN0YVNob3AgdG8gbmV3ZXJcbiAqIHZlcnNpb25zIGluIHRoZSBmdXR1cmUuIElmIHlvdSB3aXNoIHRvIGN1c3RvbWl6ZSBQcmVzdGFTaG9wIGZvciB5b3VyXG4gKiBuZWVkcyBwbGVhc2UgcmVmZXIgdG8gaHR0cHM6Ly9kZXZkb2NzLnByZXN0YXNob3AuY29tLyBmb3IgbW9yZSBpbmZvcm1hdGlvbi5cbiAqXG4gKiBAYXV0aG9yICAgIFByZXN0YVNob3AgU0EgYW5kIENvbnRyaWJ1dG9ycyA8Y29udGFjdEBwcmVzdGFzaG9wLmNvbT5cbiAqIEBjb3B5cmlnaHQgU2luY2UgMjAwNyBQcmVzdGFTaG9wIFNBIGFuZCBDb250cmlidXRvcnNcbiAqIEBsaWNlbnNlICAgaHR0cHM6Ly9vcGVuc291cmNlLm9yZy9saWNlbnNlcy9PU0wtMy4wIE9wZW4gU29mdHdhcmUgTGljZW5zZSAoT1NMIDMuMClcbiAqL1xuaW1wb3J0IENvbmZpcm1Nb2RhbCBmcm9tICdAY29tcG9uZW50cy9tb2RhbCc7XG5cbmNvbnN0IHskfSA9IHdpbmRvdztcblxuY29uc3QgQk9FdmVudCA9IHtcbiAgb24oZXZlbnROYW1lLCBjYWxsYmFjaywgY29udGV4dCkge1xuICAgIGRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoZXZlbnROYW1lLCAoZXZlbnQpID0+IHtcbiAgICAgIGlmICh0eXBlb2YgY29udGV4dCAhPT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgICAgY2FsbGJhY2suY2FsbChjb250ZXh0LCBldmVudCk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBjYWxsYmFjayhldmVudCk7XG4gICAgICB9XG4gICAgfSk7XG4gIH0sXG5cbiAgZW1pdEV2ZW50KGV2ZW50TmFtZSwgZXZlbnRUeXBlKSB7XG4gICAgY29uc3QgZXZlbnQgPSBkb2N1bWVudC5jcmVhdGVFdmVudChldmVudFR5cGUpO1xuICAgIC8vIHRydWUgdmFsdWVzIHN0YW5kIGZvcjogY2FuIGJ1YmJsZSwgYW5kIGlzIGNhbmNlbGxhYmxlXG4gICAgZXZlbnQuaW5pdEV2ZW50KGV2ZW50TmFtZSwgdHJ1ZSwgdHJ1ZSk7XG4gICAgZG9jdW1lbnQuZGlzcGF0Y2hFdmVudChldmVudCk7XG4gIH0sXG59O1xuXG4vKipcbiAqIENsYXNzIGlzIHJlc3BvbnNpYmxlIGZvciBoYW5kbGluZyBNb2R1bGUgQ2FyZCBiZWhhdmlvclxuICpcbiAqIFRoaXMgaXMgYSBwb3J0IG9mIGFkbWluLWRldi90aGVtZXMvZGVmYXVsdC9qcy9idW5kbGUvbW9kdWxlL21vZHVsZV9jYXJkLmpzXG4gKi9cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIE1vZHVsZUNhcmQge1xuICBjb25zdHJ1Y3RvcigpIHtcbiAgICAvKiBTZWxlY3RvcnMgZm9yIG1vZHVsZSBhY3Rpb24gbGlua3MgKHVuaW5zdGFsbCwgcmVzZXQsIGV0Yy4uLikgdG8gYWRkIGEgY29uZmlybSBwb3BpbiAqL1xuICAgIHRoaXMubW9kdWxlQWN0aW9uTWVudUxpbmtTZWxlY3RvciA9ICdidXR0b24ubW9kdWxlX2FjdGlvbl9tZW51Xyc7XG4gICAgdGhpcy5tb2R1bGVBY3Rpb25NZW51SW5zdGFsbExpbmtTZWxlY3RvciA9ICdidXR0b24ubW9kdWxlX2FjdGlvbl9tZW51X2luc3RhbGwnO1xuICAgIHRoaXMubW9kdWxlQWN0aW9uTWVudUVuYWJsZUxpbmtTZWxlY3RvciA9ICdidXR0b24ubW9kdWxlX2FjdGlvbl9tZW51X2VuYWJsZSc7XG4gICAgdGhpcy5tb2R1bGVBY3Rpb25NZW51VW5pbnN0YWxsTGlua1NlbGVjdG9yID0gJ2J1dHRvbi5tb2R1bGVfYWN0aW9uX21lbnVfdW5pbnN0YWxsJztcbiAgICB0aGlzLm1vZHVsZUFjdGlvbk1lbnVEaXNhYmxlTGlua1NlbGVjdG9yID0gJ2J1dHRvbi5tb2R1bGVfYWN0aW9uX21lbnVfZGlzYWJsZSc7XG4gICAgdGhpcy5tb2R1bGVBY3Rpb25NZW51RW5hYmxlTW9iaWxlTGlua1NlbGVjdG9yID0gJ2J1dHRvbi5tb2R1bGVfYWN0aW9uX21lbnVfZW5hYmxlX21vYmlsZSc7XG4gICAgdGhpcy5tb2R1bGVBY3Rpb25NZW51RGlzYWJsZU1vYmlsZUxpbmtTZWxlY3RvciA9ICdidXR0b24ubW9kdWxlX2FjdGlvbl9tZW51X2Rpc2FibGVfbW9iaWxlJztcbiAgICB0aGlzLm1vZHVsZUFjdGlvbk1lbnVSZXNldExpbmtTZWxlY3RvciA9ICdidXR0b24ubW9kdWxlX2FjdGlvbl9tZW51X3Jlc2V0JztcbiAgICB0aGlzLm1vZHVsZUFjdGlvbk1lbnVVcGRhdGVMaW5rU2VsZWN0b3IgPSAnYnV0dG9uLm1vZHVsZV9hY3Rpb25fbWVudV91cGdyYWRlJztcbiAgICB0aGlzLm1vZHVsZUl0ZW1MaXN0U2VsZWN0b3IgPSAnLm1vZHVsZS1pdGVtLWxpc3QnO1xuICAgIHRoaXMubW9kdWxlSXRlbUdyaWRTZWxlY3RvciA9ICcubW9kdWxlLWl0ZW0tZ3JpZCc7XG4gICAgdGhpcy5tb2R1bGVJdGVtQWN0aW9uc1NlbGVjdG9yID0gJy5tb2R1bGUtYWN0aW9ucyc7XG5cbiAgICAvKiBTZWxlY3RvcnMgb25seSBmb3IgbW9kYWwgYnV0dG9ucyAqL1xuICAgIHRoaXMubW9kdWxlQWN0aW9uTW9kYWxEaXNhYmxlTGlua1NlbGVjdG9yID0gJ2EubW9kdWxlX2FjdGlvbl9tb2RhbF9kaXNhYmxlJztcbiAgICB0aGlzLm1vZHVsZUFjdGlvbk1vZGFsUmVzZXRMaW5rU2VsZWN0b3IgPSAnYS5tb2R1bGVfYWN0aW9uX21vZGFsX3Jlc2V0JztcbiAgICB0aGlzLm1vZHVsZUFjdGlvbk1vZGFsVW5pbnN0YWxsTGlua1NlbGVjdG9yID0gJ2EubW9kdWxlX2FjdGlvbl9tb2RhbF91bmluc3RhbGwnO1xuICAgIHRoaXMuZm9yY2VEZWxldGlvbk9wdGlvbiA9ICcjZm9yY2VfZGVsZXRpb24nO1xuXG4gICAgdGhpcy5pbml0QWN0aW9uQnV0dG9ucygpO1xuICB9XG5cbiAgaW5pdEFjdGlvbkJ1dHRvbnMoKSB7XG4gICAgY29uc3Qgc2VsZiA9IHRoaXM7XG5cbiAgICAkKGRvY3VtZW50KS5vbignY2xpY2snLCB0aGlzLmZvcmNlRGVsZXRpb25PcHRpb24sIGZ1bmN0aW9uICgpIHtcbiAgICAgIGNvbnN0IGJ0biA9ICQoXG4gICAgICAgIHNlbGYubW9kdWxlQWN0aW9uTW9kYWxVbmluc3RhbGxMaW5rU2VsZWN0b3IsXG4gICAgICAgICQoYGRpdi5tb2R1bGUtaXRlbS1saXN0W2RhdGEtdGVjaC1uYW1lPSckeyQodGhpcykuYXR0cignZGF0YS10ZWNoLW5hbWUnKX0nXWApLFxuICAgICAgKTtcblxuICAgICAgaWYgKCQodGhpcykucHJvcCgnY2hlY2tlZCcpID09PSB0cnVlKSB7XG4gICAgICAgIGJ0bi5hdHRyKCdkYXRhLWRlbGV0aW9uJywgJ3RydWUnKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGJ0bi5yZW1vdmVBdHRyKCdkYXRhLWRlbGV0aW9uJyk7XG4gICAgICB9XG4gICAgfSk7XG5cbiAgICAkKGRvY3VtZW50KS5vbignY2xpY2snLCB0aGlzLm1vZHVsZUFjdGlvbk1lbnVJbnN0YWxsTGlua1NlbGVjdG9yLCBmdW5jdGlvbiAoKSB7XG4gICAgICBpZiAoJCgnI21vZGFsLXByZXN0YXRydXN0JykubGVuZ3RoKSB7XG4gICAgICAgICQoJyNtb2RhbC1wcmVzdGF0cnVzdCcpLm1vZGFsKCdoaWRlJyk7XG4gICAgICB9XG5cbiAgICAgIHJldHVybiAoXG4gICAgICAgIHNlbGYuZGlzcGF0Y2hQcmVFdmVudCgnaW5zdGFsbCcsIHRoaXMpXG4gICAgICAgICYmIHNlbGYuY29uZmlybUFjdGlvbignaW5zdGFsbCcsIHRoaXMpXG4gICAgICAgICYmIHNlbGYucmVxdWVzdFRvQ29udHJvbGxlcignaW5zdGFsbCcsICQodGhpcykpXG4gICAgICApO1xuICAgIH0pO1xuXG4gICAgJChkb2N1bWVudCkub24oJ2NsaWNrJywgdGhpcy5tb2R1bGVBY3Rpb25NZW51RW5hYmxlTGlua1NlbGVjdG9yLCBmdW5jdGlvbiAoKSB7XG4gICAgICByZXR1cm4gKFxuICAgICAgICBzZWxmLmRpc3BhdGNoUHJlRXZlbnQoJ2VuYWJsZScsIHRoaXMpXG4gICAgICAgICYmIHNlbGYuY29uZmlybUFjdGlvbignZW5hYmxlJywgdGhpcylcbiAgICAgICAgJiYgc2VsZi5yZXF1ZXN0VG9Db250cm9sbGVyKCdlbmFibGUnLCAkKHRoaXMpKVxuICAgICAgKTtcbiAgICB9KTtcblxuICAgICQoZG9jdW1lbnQpLm9uKCdjbGljaycsIHRoaXMubW9kdWxlQWN0aW9uTWVudVVuaW5zdGFsbExpbmtTZWxlY3RvciwgZnVuY3Rpb24gKCkge1xuICAgICAgcmV0dXJuIChcbiAgICAgICAgc2VsZi5kaXNwYXRjaFByZUV2ZW50KCd1bmluc3RhbGwnLCB0aGlzKVxuICAgICAgICAmJiBzZWxmLmNvbmZpcm1BY3Rpb24oJ3VuaW5zdGFsbCcsIHRoaXMpXG4gICAgICAgICYmIHNlbGYucmVxdWVzdFRvQ29udHJvbGxlcigndW5pbnN0YWxsJywgJCh0aGlzKSlcbiAgICAgICk7XG4gICAgfSk7XG5cbiAgICAkKGRvY3VtZW50KS5vbignY2xpY2snLCB0aGlzLm1vZHVsZUFjdGlvbk1lbnVEaXNhYmxlTGlua1NlbGVjdG9yLCBmdW5jdGlvbiAoKSB7XG4gICAgICByZXR1cm4gKFxuICAgICAgICBzZWxmLmRpc3BhdGNoUHJlRXZlbnQoJ2Rpc2FibGUnLCB0aGlzKVxuICAgICAgICAmJiBzZWxmLmNvbmZpcm1BY3Rpb24oJ2Rpc2FibGUnLCB0aGlzKVxuICAgICAgICAmJiBzZWxmLnJlcXVlc3RUb0NvbnRyb2xsZXIoJ2Rpc2FibGUnLCAkKHRoaXMpKVxuICAgICAgKTtcbiAgICB9KTtcblxuICAgICQoZG9jdW1lbnQpLm9uKCdjbGljaycsIHRoaXMubW9kdWxlQWN0aW9uTWVudUVuYWJsZU1vYmlsZUxpbmtTZWxlY3RvciwgZnVuY3Rpb24gKCkge1xuICAgICAgcmV0dXJuIChcbiAgICAgICAgc2VsZi5kaXNwYXRjaFByZUV2ZW50KCdlbmFibGVfbW9iaWxlJywgdGhpcylcbiAgICAgICAgJiYgc2VsZi5jb25maXJtQWN0aW9uKCdlbmFibGVfbW9iaWxlJywgdGhpcylcbiAgICAgICAgJiYgc2VsZi5yZXF1ZXN0VG9Db250cm9sbGVyKCdlbmFibGVfbW9iaWxlJywgJCh0aGlzKSlcbiAgICAgICk7XG4gICAgfSk7XG5cbiAgICAkKGRvY3VtZW50KS5vbignY2xpY2snLCB0aGlzLm1vZHVsZUFjdGlvbk1lbnVEaXNhYmxlTW9iaWxlTGlua1NlbGVjdG9yLCBmdW5jdGlvbiAoKSB7XG4gICAgICByZXR1cm4gKFxuICAgICAgICBzZWxmLmRpc3BhdGNoUHJlRXZlbnQoJ2Rpc2FibGVfbW9iaWxlJywgdGhpcylcbiAgICAgICAgJiYgc2VsZi5jb25maXJtQWN0aW9uKCdkaXNhYmxlX21vYmlsZScsIHRoaXMpXG4gICAgICAgICYmIHNlbGYucmVxdWVzdFRvQ29udHJvbGxlcignZGlzYWJsZV9tb2JpbGUnLCAkKHRoaXMpKVxuICAgICAgKTtcbiAgICB9KTtcblxuICAgICQoZG9jdW1lbnQpLm9uKCdjbGljaycsIHRoaXMubW9kdWxlQWN0aW9uTWVudVJlc2V0TGlua1NlbGVjdG9yLCBmdW5jdGlvbiAoKSB7XG4gICAgICByZXR1cm4gKFxuICAgICAgICBzZWxmLmRpc3BhdGNoUHJlRXZlbnQoJ3Jlc2V0JywgdGhpcylcbiAgICAgICAgJiYgc2VsZi5jb25maXJtQWN0aW9uKCdyZXNldCcsIHRoaXMpXG4gICAgICAgICYmIHNlbGYucmVxdWVzdFRvQ29udHJvbGxlcigncmVzZXQnLCAkKHRoaXMpKVxuICAgICAgKTtcbiAgICB9KTtcblxuICAgICQoZG9jdW1lbnQpLm9uKCdjbGljaycsIHRoaXMubW9kdWxlQWN0aW9uTWVudVVwZGF0ZUxpbmtTZWxlY3RvciwgZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgY29uc3QgbW9kYWwgPSAkKGAjJHskKHRoaXMpLmRhdGEoJ2NvbmZpcm1fbW9kYWwnKX1gKTtcbiAgICAgIGNvbnN0IGlzTWFpbnRlbmFuY2VNb2RlID0gd2luZG93LmlzU2hvcE1haW50ZW5hbmNlO1xuXG4gICAgICBpZiAobW9kYWwubGVuZ3RoICE9PSAxKSB7XG4gICAgICAgIC8vIE1vZGFsIGJvZHkgZWxlbWVudFxuICAgICAgICBjb25zdCBtYWludGVuYW5jZUxpbmsgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdhJyk7XG4gICAgICAgIG1haW50ZW5hbmNlTGluay5jbGFzc0xpc3QuYWRkKCdidG4nLCAnYnRuLXByaW1hcnknLCAnYnRuLWxnJyk7XG4gICAgICAgIG1haW50ZW5hbmNlTGluay5zZXRBdHRyaWJ1dGUoJ2hyZWYnLCB3aW5kb3cubW9kdWxlVVJMcy5tYWludGVuYW5jZVBhZ2UpO1xuICAgICAgICBtYWludGVuYW5jZUxpbmsuaW5uZXJIVE1MID0gd2luZG93Lm1vZHVsZVRyYW5zbGF0aW9ucy5tb2R1bGVNb2RhbFVwZGF0ZU1haW50ZW5hbmNlO1xuXG4gICAgICAgIGNvbnN0IHVwZGF0ZUNvbmZpcm1Nb2RhbCA9IG5ldyBDb25maXJtTW9kYWwoXG4gICAgICAgICAge1xuICAgICAgICAgICAgaWQ6ICdjb25maXJtLW1vZHVsZS11cGRhdGUtbW9kYWwnLFxuICAgICAgICAgICAgY29uZmlybVRpdGxlOiB3aW5kb3cubW9kdWxlVHJhbnNsYXRpb25zLnNpbmdsZU1vZHVsZU1vZGFsVXBkYXRlVGl0bGUsXG4gICAgICAgICAgICBjbG9zZUJ1dHRvbkxhYmVsOiB3aW5kb3cubW9kdWxlVHJhbnNsYXRpb25zLm1vZHVsZU1vZGFsVXBkYXRlQ2FuY2VsLFxuICAgICAgICAgICAgY29uZmlybUJ1dHRvbkxhYmVsOiBpc01haW50ZW5hbmNlTW9kZVxuICAgICAgICAgICAgICA/IHdpbmRvdy5tb2R1bGVUcmFuc2xhdGlvbnMubW9kdWxlTW9kYWxVcGRhdGVVcGdyYWRlXG4gICAgICAgICAgICAgIDogd2luZG93Lm1vZHVsZVRyYW5zbGF0aW9ucy51cGdyYWRlQW55d2F5QnV0dG9uVGV4dCxcbiAgICAgICAgICAgIGNvbmZpcm1CdXR0b25DbGFzczogaXNNYWludGVuYW5jZU1vZGUgPyAnYnRuLXByaW1hcnknIDogJ2J0bi1zZWNvbmRhcnknLFxuICAgICAgICAgICAgY29uZmlybU1lc3NhZ2U6IGlzTWFpbnRlbmFuY2VNb2RlID8gJycgOiB3aW5kb3cubW9kdWxlVHJhbnNsYXRpb25zLm1vZHVsZU1vZGFsVXBkYXRlQ29uZmlybU1lc3NhZ2UsXG4gICAgICAgICAgICBjbG9zYWJsZTogdHJ1ZSxcbiAgICAgICAgICAgIGN1c3RvbUJ1dHRvbnM6IGlzTWFpbnRlbmFuY2VNb2RlID8gW10gOiBbbWFpbnRlbmFuY2VMaW5rXSxcbiAgICAgICAgICB9LFxuXG4gICAgICAgICAgKCkgPT4gc2VsZi5kaXNwYXRjaFByZUV2ZW50KCd1cGRhdGUnLCB0aGlzKVxuICAgICAgICAgICAgJiYgc2VsZi5jb25maXJtQWN0aW9uKCd1cGRhdGUnLCB0aGlzKVxuICAgICAgICAgICAgJiYgc2VsZi5yZXF1ZXN0VG9Db250cm9sbGVyKCd1cGRhdGUnLCAkKHRoaXMpKSxcbiAgICAgICAgKTtcblxuICAgICAgICB1cGRhdGVDb25maXJtTW9kYWwuc2hvdygpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgcmV0dXJuIChcbiAgICAgICAgICBzZWxmLmRpc3BhdGNoUHJlRXZlbnQoJ3VwZGF0ZScsIHRoaXMpXG4gICAgICAgICAgJiYgc2VsZi5jb25maXJtQWN0aW9uKCd1cGRhdGUnLCB0aGlzKVxuICAgICAgICAgICYmIHNlbGYucmVxdWVzdFRvQ29udHJvbGxlcigndXBkYXRlJywgJCh0aGlzKSlcbiAgICAgICAgKTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH0pO1xuXG4gICAgJChkb2N1bWVudCkub24oJ2NsaWNrJywgdGhpcy5tb2R1bGVBY3Rpb25Nb2RhbERpc2FibGVMaW5rU2VsZWN0b3IsIGZ1bmN0aW9uICgpIHtcbiAgICAgIHJldHVybiBzZWxmLnJlcXVlc3RUb0NvbnRyb2xsZXIoXG4gICAgICAgICdkaXNhYmxlJyxcbiAgICAgICAgJChcbiAgICAgICAgICBzZWxmLm1vZHVsZUFjdGlvbk1lbnVEaXNhYmxlTGlua1NlbGVjdG9yLFxuICAgICAgICAgICQoYGRpdi5tb2R1bGUtaXRlbS1saXN0W2RhdGEtdGVjaC1uYW1lPSckeyQodGhpcykuYXR0cignZGF0YS10ZWNoLW5hbWUnKX0nXWApLFxuICAgICAgICApLFxuICAgICAgKTtcbiAgICB9KTtcblxuICAgICQoZG9jdW1lbnQpLm9uKCdjbGljaycsIHRoaXMubW9kdWxlQWN0aW9uTW9kYWxSZXNldExpbmtTZWxlY3RvciwgZnVuY3Rpb24gKCkge1xuICAgICAgcmV0dXJuIHNlbGYucmVxdWVzdFRvQ29udHJvbGxlcihcbiAgICAgICAgJ3Jlc2V0JyxcbiAgICAgICAgJChcbiAgICAgICAgICBzZWxmLm1vZHVsZUFjdGlvbk1lbnVSZXNldExpbmtTZWxlY3RvcixcbiAgICAgICAgICAkKGBkaXYubW9kdWxlLWl0ZW0tbGlzdFtkYXRhLXRlY2gtbmFtZT0nJHskKHRoaXMpLmF0dHIoJ2RhdGEtdGVjaC1uYW1lJyl9J11gKSxcbiAgICAgICAgKSxcbiAgICAgICk7XG4gICAgfSk7XG5cbiAgICAkKGRvY3VtZW50KS5vbignY2xpY2snLCB0aGlzLm1vZHVsZUFjdGlvbk1vZGFsVW5pbnN0YWxsTGlua1NlbGVjdG9yLCAoZSkgPT4ge1xuICAgICAgJChlLnRhcmdldClcbiAgICAgICAgLnBhcmVudHMoJy5tb2RhbCcpXG4gICAgICAgIC5vbignaGlkZGVuLmJzLm1vZGFsJywgKCkgPT4gc2VsZi5yZXF1ZXN0VG9Db250cm9sbGVyKFxuICAgICAgICAgICd1bmluc3RhbGwnLFxuICAgICAgICAgICQoXG4gICAgICAgICAgICBzZWxmLm1vZHVsZUFjdGlvbk1lbnVVbmluc3RhbGxMaW5rU2VsZWN0b3IsXG4gICAgICAgICAgICAkKGBkaXYubW9kdWxlLWl0ZW0tbGlzdFtkYXRhLXRlY2gtbmFtZT0nJHskKGUudGFyZ2V0KS5hdHRyKCdkYXRhLXRlY2gtbmFtZScpfSddYCksXG4gICAgICAgICAgKSxcbiAgICAgICAgICAkKGUudGFyZ2V0KS5hdHRyKCdkYXRhLWRlbGV0aW9uJyksXG4gICAgICAgICksXG4gICAgICAgICk7XG4gICAgfSk7XG4gIH1cblxuICBnZXRNb2R1bGVJdGVtU2VsZWN0b3IoKSB7XG4gICAgaWYgKCQodGhpcy5tb2R1bGVJdGVtTGlzdFNlbGVjdG9yKS5sZW5ndGgpIHtcbiAgICAgIHJldHVybiB0aGlzLm1vZHVsZUl0ZW1MaXN0U2VsZWN0b3I7XG4gICAgfVxuXG4gICAgcmV0dXJuIHRoaXMubW9kdWxlSXRlbUdyaWRTZWxlY3RvcjtcbiAgfVxuXG4gIGNvbmZpcm1BY3Rpb24oYWN0aW9uLCBlbGVtZW50KSB7XG4gICAgY29uc3QgbW9kYWwgPSAkKGAjJHskKGVsZW1lbnQpLmRhdGEoJ2NvbmZpcm1fbW9kYWwnKX1gKTtcblxuICAgIGlmIChtb2RhbC5sZW5ndGggIT09IDEpIHtcbiAgICAgIHJldHVybiB0cnVlO1xuICAgIH1cblxuICAgIG1vZGFsLmZpcnN0KCkubW9kYWwoJ3Nob3cnKTtcblxuICAgIHJldHVybiBmYWxzZTsgLy8gZG8gbm90IGFsbG93IGEuaHJlZiB0byByZWxvYWQgdGhlIHBhZ2UuIFRoZSBjb25maXJtIG1vZGFsIGRpYWxvZyB3aWxsIGRvIGl0IGFzeW5jIGlmIG5lZWRlZC5cbiAgfVxuXG4gIC8qKlxuICAgKiBVcGRhdGUgdGhlIGNvbnRlbnQgb2YgYSBtb2RhbCBhc2tpbmcgYSBjb25maXJtYXRpb24gZm9yIFByZXN0YVRydXN0IGFuZCBvcGVuIGl0XG4gICAqXG4gICAqIEBwYXJhbSB7YXJyYXl9IHJlc3VsdCBjb250YWluaW5nIG1vZHVsZSBkYXRhXG4gICAqIEByZXR1cm4ge3ZvaWR9XG4gICAqL1xuICBjb25maXJtUHJlc3RhVHJ1c3QocmVzdWx0KSB7XG4gICAgY29uc3QgdGhhdCA9IHRoaXM7XG4gICAgY29uc3QgbW9kYWwgPSB0aGlzLnJlcGxhY2VQcmVzdGFUcnVzdFBsYWNlaG9sZGVycyhyZXN1bHQpO1xuXG4gICAgbW9kYWxcbiAgICAgIC5maW5kKCcucHN0cnVzdC1pbnN0YWxsJylcbiAgICAgIC5vZmYoJ2NsaWNrJylcbiAgICAgIC5vbignY2xpY2snLCAoKSA9PiB7XG4gICAgICAgIC8vIEZpbmQgcmVsYXRlZCBmb3JtLCB1cGRhdGUgaXQgYW5kIHN1Ym1pdCBpdFxuICAgICAgICBjb25zdCBpbnN0YWxsQnV0dG9uID0gJChcbiAgICAgICAgICB0aGF0Lm1vZHVsZUFjdGlvbk1lbnVJbnN0YWxsTGlua1NlbGVjdG9yLFxuICAgICAgICAgIGAubW9kdWxlLWl0ZW1bZGF0YS10ZWNoLW5hbWU9XCIke3Jlc3VsdC5tb2R1bGUuYXR0cmlidXRlcy5uYW1lfVwiXWAsXG4gICAgICAgICk7XG5cbiAgICAgICAgY29uc3QgZm9ybSA9IGluc3RhbGxCdXR0b24ucGFyZW50KCdmb3JtJyk7XG4gICAgICAgICQoJzxpbnB1dD4nKVxuICAgICAgICAgIC5hdHRyKHtcbiAgICAgICAgICAgIHR5cGU6ICdoaWRkZW4nLFxuICAgICAgICAgICAgdmFsdWU6ICcxJyxcbiAgICAgICAgICAgIG5hbWU6ICdhY3Rpb25QYXJhbXNbY29uZmlybVByZXN0YVRydXN0XScsXG4gICAgICAgICAgfSlcbiAgICAgICAgICAuYXBwZW5kVG8oZm9ybSk7XG5cbiAgICAgICAgaW5zdGFsbEJ1dHRvbi5jbGljaygpO1xuICAgICAgICBtb2RhbC5tb2RhbCgnaGlkZScpO1xuICAgICAgfSk7XG5cbiAgICBtb2RhbC5tb2RhbCgpO1xuICB9XG5cbiAgcmVwbGFjZVByZXN0YVRydXN0UGxhY2Vob2xkZXJzKHJlc3VsdCkge1xuICAgIGNvbnN0IG1vZGFsID0gJCgnI21vZGFsLXByZXN0YXRydXN0Jyk7XG4gICAgY29uc3QgbW9kdWxlID0gcmVzdWx0Lm1vZHVsZS5hdHRyaWJ1dGVzO1xuXG4gICAgaWYgKHJlc3VsdC5jb25maXJtYXRpb25fc3ViamVjdCAhPT0gJ1ByZXN0YVRydXN0JyB8fCAhbW9kYWwubGVuZ3RoKSB7XG4gICAgICByZXR1cm4gZmFsc2U7XG4gICAgfVxuXG4gICAgY29uc3QgYWxlcnRDbGFzcyA9IG1vZHVsZS5wcmVzdGF0cnVzdC5zdGF0dXMgPyAnc3VjY2VzcycgOiAnd2FybmluZyc7XG5cbiAgICBpZiAobW9kdWxlLnByZXN0YXRydXN0LmNoZWNrX2xpc3QucHJvcGVydHkpIHtcbiAgICAgIG1vZGFsLmZpbmQoJyNwc3RydXN0LWJ0bi1wcm9wZXJ0eS1vaycpLnNob3coKTtcbiAgICAgIG1vZGFsLmZpbmQoJyNwc3RydXN0LWJ0bi1wcm9wZXJ0eS1ub2snKS5oaWRlKCk7XG4gICAgfSBlbHNlIHtcbiAgICAgIG1vZGFsLmZpbmQoJyNwc3RydXN0LWJ0bi1wcm9wZXJ0eS1vaycpLmhpZGUoKTtcbiAgICAgIG1vZGFsLmZpbmQoJyNwc3RydXN0LWJ0bi1wcm9wZXJ0eS1ub2snKS5zaG93KCk7XG4gICAgICBtb2RhbFxuICAgICAgICAuZmluZCgnI3BzdHJ1c3QtYnV5JylcbiAgICAgICAgLmF0dHIoJ2hyZWYnLCBtb2R1bGUudXJsKVxuICAgICAgICAudG9nZ2xlKG1vZHVsZS51cmwgIT09IG51bGwpO1xuICAgIH1cblxuICAgIG1vZGFsLmZpbmQoJyNwc3RydXN0LWltZycpLmF0dHIoe3NyYzogbW9kdWxlLmltZywgYWx0OiBtb2R1bGUubmFtZX0pO1xuICAgIG1vZGFsLmZpbmQoJyNwc3RydXN0LW5hbWUnKS50ZXh0KG1vZHVsZS5kaXNwbGF5TmFtZSk7XG4gICAgbW9kYWwuZmluZCgnI3BzdHJ1c3QtYXV0aG9yJykudGV4dChtb2R1bGUuYXV0aG9yKTtcbiAgICBtb2RhbFxuICAgICAgLmZpbmQoJyNwc3RydXN0LWxhYmVsJylcbiAgICAgIC5hdHRyKCdjbGFzcycsIGB0ZXh0LSR7YWxlcnRDbGFzc31gKVxuICAgICAgLnRleHQobW9kdWxlLnByZXN0YXRydXN0LnN0YXR1cyA/ICdPSycgOiAnS08nKTtcbiAgICBtb2RhbC5maW5kKCcjcHN0cnVzdC1tZXNzYWdlJykuYXR0cignY2xhc3MnLCBgYWxlcnQgYWxlcnQtJHthbGVydENsYXNzfWApO1xuICAgIG1vZGFsLmZpbmQoJyNwc3RydXN0LW1lc3NhZ2UgPiBwJykudGV4dChtb2R1bGUucHJlc3RhdHJ1c3QubWVzc2FnZSk7XG5cbiAgICByZXR1cm4gbW9kYWw7XG4gIH1cblxuICBkaXNwYXRjaFByZUV2ZW50KGFjdGlvbiwgZWxlbWVudCkge1xuICAgIGNvbnN0IGV2ZW50ID0galF1ZXJ5LkV2ZW50KCdtb2R1bGVfY2FyZF9hY3Rpb25fZXZlbnQnKTtcblxuICAgICQoZWxlbWVudCkudHJpZ2dlcihldmVudCwgW2FjdGlvbl0pO1xuICAgIGlmIChldmVudC5pc1Byb3BhZ2F0aW9uU3RvcHBlZCgpICE9PSBmYWxzZSB8fCBldmVudC5pc0ltbWVkaWF0ZVByb3BhZ2F0aW9uU3RvcHBlZCgpICE9PSBmYWxzZSkge1xuICAgICAgcmV0dXJuIGZhbHNlOyAvLyBpZiBhbGwgaGFuZGxlcnMgaGF2ZSBub3QgYmVlbiBjYWxsZWQsIHRoZW4gc3RvcCBwcm9wYWdhdGlvbiBvZiB0aGUgY2xpY2sgZXZlbnQuXG4gICAgfVxuXG4gICAgcmV0dXJuIGV2ZW50LnJlc3VsdCAhPT0gZmFsc2U7IC8vIGV4cGxpY2l0IGZhbHNlIG11c3QgYmUgc2V0IGZyb20gaGFuZGxlcnMgdG8gc3RvcCBwcm9wYWdhdGlvbiBvZiB0aGUgY2xpY2sgZXZlbnQuXG4gIH1cblxuICByZXF1ZXN0VG9Db250cm9sbGVyKGFjdGlvbiwgZWxlbWVudCwgZm9yY2VEZWxldGlvbiwgZGlzYWJsZUNhY2hlQ2xlYXIsIGNhbGxiYWNrKSB7XG4gICAgY29uc3Qgc2VsZiA9IHRoaXM7XG4gICAgY29uc3QganFFbGVtZW50T2JqID0gZWxlbWVudC5jbG9zZXN0KHRoaXMubW9kdWxlSXRlbUFjdGlvbnNTZWxlY3Rvcik7XG4gICAgY29uc3QgZm9ybSA9IGVsZW1lbnQuY2xvc2VzdCgnZm9ybScpO1xuICAgIGNvbnN0IHNwaW5uZXJPYmogPSAkKCc8YnV0dG9uIGNsYXNzPVwiYnRuLXByaW1hcnktcmV2ZXJzZSBvbmNsaWNrIHVuYmluZCBzcGlubmVyIFwiPjwvYnV0dG9uPicpO1xuICAgIGNvbnN0IHVybCA9IGAvLyR7d2luZG93LmxvY2F0aW9uLmhvc3R9JHtmb3JtLmF0dHIoJ2FjdGlvbicpfWA7XG4gICAgY29uc3QgYWN0aW9uUGFyYW1zID0gZm9ybS5zZXJpYWxpemVBcnJheSgpO1xuXG4gICAgaWYgKGZvcmNlRGVsZXRpb24gPT09ICd0cnVlJyB8fCBmb3JjZURlbGV0aW9uID09PSB0cnVlKSB7XG4gICAgICBhY3Rpb25QYXJhbXMucHVzaCh7bmFtZTogJ2FjdGlvblBhcmFtc1tkZWxldGlvbl0nLCB2YWx1ZTogdHJ1ZX0pO1xuICAgIH1cbiAgICBpZiAoZGlzYWJsZUNhY2hlQ2xlYXIgPT09ICd0cnVlJyB8fCBkaXNhYmxlQ2FjaGVDbGVhciA9PT0gdHJ1ZSkge1xuICAgICAgYWN0aW9uUGFyYW1zLnB1c2goe25hbWU6ICdhY3Rpb25QYXJhbXNbY2FjaGVDbGVhckVuYWJsZWRdJywgdmFsdWU6IDB9KTtcbiAgICB9XG5cbiAgICAkLmFqYXgoe1xuICAgICAgdXJsLFxuICAgICAgZGF0YVR5cGU6ICdqc29uJyxcbiAgICAgIG1ldGhvZDogJ1BPU1QnLFxuICAgICAgZGF0YTogYWN0aW9uUGFyYW1zLFxuICAgICAgYmVmb3JlU2VuZCgpIHtcbiAgICAgICAganFFbGVtZW50T2JqLmhpZGUoKTtcbiAgICAgICAganFFbGVtZW50T2JqLmFmdGVyKHNwaW5uZXJPYmopO1xuICAgICAgfSxcbiAgICB9KVxuICAgICAgLmRvbmUoKHJlc3VsdCkgPT4ge1xuICAgICAgICBpZiAocmVzdWx0ID09PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAkLmdyb3dsLmVycm9yKHtcbiAgICAgICAgICAgIG1lc3NhZ2U6ICdObyBhbnN3ZXIgcmVjZWl2ZWQgZnJvbSBzZXJ2ZXInLFxuICAgICAgICAgICAgZml4ZWQ6IHRydWUsXG4gICAgICAgICAgfSk7XG4gICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKHR5cGVvZiByZXN1bHQuc3RhdHVzICE9PSAndW5kZWZpbmVkJyAmJiByZXN1bHQuc3RhdHVzID09PSBmYWxzZSkge1xuICAgICAgICAgICQuZ3Jvd2wuZXJyb3Ioe21lc3NhZ2U6IHJlc3VsdC5tc2csIGZpeGVkOiB0cnVlfSk7XG4gICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG5cbiAgICAgICAgY29uc3QgbW9kdWxlVGVjaE5hbWUgPSBPYmplY3Qua2V5cyhyZXN1bHQpWzBdO1xuXG4gICAgICAgIGlmIChyZXN1bHRbbW9kdWxlVGVjaE5hbWVdLnN0YXR1cyA9PT0gZmFsc2UpIHtcbiAgICAgICAgICBpZiAodHlwZW9mIHJlc3VsdFttb2R1bGVUZWNoTmFtZV0uY29uZmlybWF0aW9uX3N1YmplY3QgIT09ICd1bmRlZmluZWQnKSB7XG4gICAgICAgICAgICBzZWxmLmNvbmZpcm1QcmVzdGFUcnVzdChyZXN1bHRbbW9kdWxlVGVjaE5hbWVdKTtcbiAgICAgICAgICB9XG5cbiAgICAgICAgICAkLmdyb3dsLmVycm9yKHttZXNzYWdlOiByZXN1bHRbbW9kdWxlVGVjaE5hbWVdLm1zZywgZml4ZWQ6IHRydWV9KTtcbiAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cblxuICAgICAgICAkLmdyb3dsKHtcbiAgICAgICAgICBtZXNzYWdlOiByZXN1bHRbbW9kdWxlVGVjaE5hbWVdLm1zZyxcbiAgICAgICAgICBkdXJhdGlvbjogNjAwMCxcbiAgICAgICAgfSk7XG5cbiAgICAgICAgY29uc3QgYWx0ZXJlZFNlbGVjdG9yID0gc2VsZi5nZXRNb2R1bGVJdGVtU2VsZWN0b3IoKS5yZXBsYWNlKCcuJywgJycpO1xuICAgICAgICBsZXQgbWFpbkVsZW1lbnQgPSBudWxsO1xuXG4gICAgICAgIGlmIChhY3Rpb24gPT09ICd1bmluc3RhbGwnKSB7XG4gICAgICAgICAgbWFpbkVsZW1lbnQgPSBqcUVsZW1lbnRPYmouY2xvc2VzdChgLiR7YWx0ZXJlZFNlbGVjdG9yfWApO1xuICAgICAgICAgIG1haW5FbGVtZW50LnJlbW92ZSgpO1xuXG4gICAgICAgICAgQk9FdmVudC5lbWl0RXZlbnQoJ01vZHVsZSBVbmluc3RhbGxlZCcsICdDdXN0b21FdmVudCcpO1xuICAgICAgICB9IGVsc2UgaWYgKGFjdGlvbiA9PT0gJ2Rpc2FibGUnKSB7XG4gICAgICAgICAgbWFpbkVsZW1lbnQgPSBqcUVsZW1lbnRPYmouY2xvc2VzdChgLiR7YWx0ZXJlZFNlbGVjdG9yfWApO1xuICAgICAgICAgIG1haW5FbGVtZW50LmFkZENsYXNzKGAke2FsdGVyZWRTZWxlY3Rvcn0taXNOb3RBY3RpdmVgKTtcbiAgICAgICAgICBtYWluRWxlbWVudC5hdHRyKCdkYXRhLWFjdGl2ZScsICcwJyk7XG5cbiAgICAgICAgICBCT0V2ZW50LmVtaXRFdmVudCgnTW9kdWxlIERpc2FibGVkJywgJ0N1c3RvbUV2ZW50Jyk7XG4gICAgICAgIH0gZWxzZSBpZiAoYWN0aW9uID09PSAnZW5hYmxlJykge1xuICAgICAgICAgIG1haW5FbGVtZW50ID0ganFFbGVtZW50T2JqLmNsb3Nlc3QoYC4ke2FsdGVyZWRTZWxlY3Rvcn1gKTtcbiAgICAgICAgICBtYWluRWxlbWVudC5yZW1vdmVDbGFzcyhgJHthbHRlcmVkU2VsZWN0b3J9LWlzTm90QWN0aXZlYCk7XG4gICAgICAgICAgbWFpbkVsZW1lbnQuYXR0cignZGF0YS1hY3RpdmUnLCAnMScpO1xuXG4gICAgICAgICAgQk9FdmVudC5lbWl0RXZlbnQoJ01vZHVsZSBFbmFibGVkJywgJ0N1c3RvbUV2ZW50Jyk7XG4gICAgICAgIH1cblxuICAgICAgICBqcUVsZW1lbnRPYmoucmVwbGFjZVdpdGgocmVzdWx0W21vZHVsZVRlY2hOYW1lXS5hY3Rpb25fbWVudV9odG1sKTtcbiAgICAgIH0pXG4gICAgICAuZmFpbCgoKSA9PiB7XG4gICAgICAgIGNvbnN0IG1vZHVsZUl0ZW0gPSBqcUVsZW1lbnRPYmouY2xvc2VzdCgnbW9kdWxlLWl0ZW0tbGlzdCcpO1xuICAgICAgICBjb25zdCB0ZWNoTmFtZSA9IG1vZHVsZUl0ZW0uZGF0YSgndGVjaE5hbWUnKTtcbiAgICAgICAgJC5ncm93bC5lcnJvcih7XG4gICAgICAgICAgbWVzc2FnZTogYENvdWxkIG5vdCBwZXJmb3JtIGFjdGlvbiAke2FjdGlvbn0gZm9yIG1vZHVsZSAke3RlY2hOYW1lfWAsXG4gICAgICAgICAgZml4ZWQ6IHRydWUsXG4gICAgICAgIH0pO1xuICAgICAgfSlcbiAgICAgIC5hbHdheXMoKCkgPT4ge1xuICAgICAgICBqcUVsZW1lbnRPYmouZmFkZUluKCk7XG4gICAgICAgIHNwaW5uZXJPYmoucmVtb3ZlKCk7XG4gICAgICAgIGlmIChjYWxsYmFjaykge1xuICAgICAgICAgIGNhbGxiYWNrKCk7XG4gICAgICAgIH1cbiAgICAgIH0pO1xuXG4gICAgcmV0dXJuIGZhbHNlO1xuICB9XG59XG4iLCIvKipcbiAqIENvcHlyaWdodCBzaW5jZSAyMDA3IFByZXN0YVNob3AgU0EgYW5kIENvbnRyaWJ1dG9yc1xuICogUHJlc3RhU2hvcCBpcyBhbiBJbnRlcm5hdGlvbmFsIFJlZ2lzdGVyZWQgVHJhZGVtYXJrICYgUHJvcGVydHkgb2YgUHJlc3RhU2hvcCBTQVxuICpcbiAqIE5PVElDRSBPRiBMSUNFTlNFXG4gKlxuICogVGhpcyBzb3VyY2UgZmlsZSBpcyBzdWJqZWN0IHRvIHRoZSBPcGVuIFNvZnR3YXJlIExpY2Vuc2UgKE9TTCAzLjApXG4gKiB0aGF0IGlzIGJ1bmRsZWQgd2l0aCB0aGlzIHBhY2thZ2UgaW4gdGhlIGZpbGUgTElDRU5TRS5tZC5cbiAqIEl0IGlzIGFsc28gYXZhaWxhYmxlIHRocm91Z2ggdGhlIHdvcmxkLXdpZGUtd2ViIGF0IHRoaXMgVVJMOlxuICogaHR0cHM6Ly9vcGVuc291cmNlLm9yZy9saWNlbnNlcy9PU0wtMy4wXG4gKiBJZiB5b3UgZGlkIG5vdCByZWNlaXZlIGEgY29weSBvZiB0aGUgbGljZW5zZSBhbmQgYXJlIHVuYWJsZSB0b1xuICogb2J0YWluIGl0IHRocm91Z2ggdGhlIHdvcmxkLXdpZGUtd2ViLCBwbGVhc2Ugc2VuZCBhbiBlbWFpbFxuICogdG8gbGljZW5zZUBwcmVzdGFzaG9wLmNvbSBzbyB3ZSBjYW4gc2VuZCB5b3UgYSBjb3B5IGltbWVkaWF0ZWx5LlxuICpcbiAqIERJU0NMQUlNRVJcbiAqXG4gKiBEbyBub3QgZWRpdCBvciBhZGQgdG8gdGhpcyBmaWxlIGlmIHlvdSB3aXNoIHRvIHVwZ3JhZGUgUHJlc3RhU2hvcCB0byBuZXdlclxuICogdmVyc2lvbnMgaW4gdGhlIGZ1dHVyZS4gSWYgeW91IHdpc2ggdG8gY3VzdG9taXplIFByZXN0YVNob3AgZm9yIHlvdXJcbiAqIG5lZWRzIHBsZWFzZSByZWZlciB0byBodHRwczovL2RldmRvY3MucHJlc3Rhc2hvcC5jb20vIGZvciBtb3JlIGluZm9ybWF0aW9uLlxuICpcbiAqIEBhdXRob3IgICAgUHJlc3RhU2hvcCBTQSBhbmQgQ29udHJpYnV0b3JzIDxjb250YWN0QHByZXN0YXNob3AuY29tPlxuICogQGNvcHlyaWdodCBTaW5jZSAyMDA3IFByZXN0YVNob3AgU0EgYW5kIENvbnRyaWJ1dG9yc1xuICogQGxpY2Vuc2UgICBodHRwczovL29wZW5zb3VyY2Uub3JnL2xpY2Vuc2VzL09TTC0zLjAgT3BlbiBTb2Z0d2FyZSBMaWNlbnNlIChPU0wgMy4wKVxuICovXG5cbmltcG9ydCBDb25maXJtTW9kYWwgZnJvbSAnQGNvbXBvbmVudHMvbW9kYWwnO1xuXG5jb25zdCB7JH0gPSB3aW5kb3c7XG5cbi8qKlxuICogTW9kdWxlIEFkbWluIFBhZ2UgQ29udHJvbGxlci5cbiAqIEBjb25zdHJ1Y3RvclxuICovXG5jbGFzcyBBZG1pbk1vZHVsZUNvbnRyb2xsZXIge1xuICAvKipcbiAgICogSW5pdGlhbGl6ZSBhbGwgbGlzdGVuZXJzIGFuZCBiaW5kIGV2ZXJ5dGhpbmdcbiAgICogQG1ldGhvZCBpbml0XG4gICAqIEBtZW1iZXJvZiBBZG1pbk1vZHVsZVxuICAgKi9cbiAgY29uc3RydWN0b3IobW9kdWxlQ2FyZENvbnRyb2xsZXIpIHtcbiAgICB0aGlzLm1vZHVsZUNhcmRDb250cm9sbGVyID0gbW9kdWxlQ2FyZENvbnRyb2xsZXI7XG5cbiAgICB0aGlzLkRFRkFVTFRfTUFYX1JFQ0VOVExZX1VTRUQgPSAxMDtcbiAgICB0aGlzLkRFRkFVTFRfTUFYX1BFUl9DQVRFR09SSUVTID0gNjtcbiAgICB0aGlzLkRJU1BMQVlfR1JJRCA9ICdncmlkJztcbiAgICB0aGlzLkRJU1BMQVlfTElTVCA9ICdsaXN0JztcbiAgICB0aGlzLkNBVEVHT1JZX1JFQ0VOVExZX1VTRUQgPSAncmVjZW50bHktdXNlZCc7XG5cbiAgICB0aGlzLmN1cnJlbnRDYXRlZ29yeURpc3BsYXkgPSB7fTtcbiAgICB0aGlzLmN1cnJlbnREaXNwbGF5ID0gJyc7XG4gICAgdGhpcy5pc0NhdGVnb3J5R3JpZERpc3BsYXllZCA9IGZhbHNlO1xuICAgIHRoaXMuY3VycmVudFRhZ3NMaXN0ID0gW107XG4gICAgdGhpcy5jdXJyZW50UmVmQ2F0ZWdvcnkgPSBudWxsO1xuICAgIHRoaXMuY3VycmVudFJlZlN0YXR1cyA9IG51bGw7XG4gICAgdGhpcy5jdXJyZW50U29ydGluZyA9IG51bGw7XG4gICAgdGhpcy5iYXNlQWRkb25zVXJsID0gJ2h0dHBzOi8vYWRkb25zLnByZXN0YXNob3AuY29tLyc7XG4gICAgdGhpcy5wc3RhZ2dlcklucHV0ID0gbnVsbDtcbiAgICB0aGlzLmxhc3RCdWxrQWN0aW9uID0gbnVsbDtcbiAgICB0aGlzLmlzVXBsb2FkU3RhcnRlZCA9IGZhbHNlO1xuXG4gICAgdGhpcy5yZWNlbnRseVVzZWRTZWxlY3RvciA9ICcjbW9kdWxlLXJlY2VudGx5LXVzZWQtbGlzdCAubW9kdWxlcy1saXN0JztcblxuICAgIC8qKlxuICAgICAqIExvYWRlZCBtb2R1bGVzIGxpc3QuXG4gICAgICogQ29udGFpbmluZyB0aGUgY2FyZCBhbmQgbGlzdCBkaXNwbGF5LlxuICAgICAqIEB0eXBlIHtBcnJheX1cbiAgICAgKi9cbiAgICB0aGlzLm1vZHVsZXNMaXN0ID0gW107XG4gICAgdGhpcy5hZGRvbnNDYXJkR3JpZCA9IG51bGw7XG4gICAgdGhpcy5hZGRvbnNDYXJkTGlzdCA9IG51bGw7XG5cbiAgICB0aGlzLm1vZHVsZVNob3J0TGlzdCA9ICcubW9kdWxlLXNob3J0LWxpc3QnO1xuICAgIC8vIFNlZSBtb3JlICYgU2VlIGxlc3Mgc2VsZWN0b3JcbiAgICB0aGlzLnNlZU1vcmVTZWxlY3RvciA9ICcuc2VlLW1vcmUnO1xuICAgIHRoaXMuc2VlTGVzc1NlbGVjdG9yID0gJy5zZWUtbGVzcyc7XG5cbiAgICAvLyBTZWxlY3RvcnMgaW50byB2YXJzIHRvIG1ha2UgaXQgZWFzaWVyIHRvIGNoYW5nZSB0aGVtIHdoaWxlIGtlZXBpbmcgc2FtZSBjb2RlIGxvZ2ljXG4gICAgdGhpcy5tb2R1bGVJdGVtR3JpZFNlbGVjdG9yID0gJy5tb2R1bGUtaXRlbS1ncmlkJztcbiAgICB0aGlzLm1vZHVsZUl0ZW1MaXN0U2VsZWN0b3IgPSAnLm1vZHVsZS1pdGVtLWxpc3QnO1xuICAgIHRoaXMuY2F0ZWdvcnlTZWxlY3RvckxhYmVsU2VsZWN0b3IgPSAnLm1vZHVsZS1jYXRlZ29yeS1zZWxlY3Rvci1sYWJlbCc7XG4gICAgdGhpcy5jYXRlZ29yeVNlbGVjdG9yID0gJy5tb2R1bGUtY2F0ZWdvcnktc2VsZWN0b3InO1xuICAgIHRoaXMuY2F0ZWdvcnlJdGVtU2VsZWN0b3IgPSAnLm1vZHVsZS1jYXRlZ29yeS1tZW51JztcbiAgICB0aGlzLmFkZG9uc0xvZ2luQnV0dG9uU2VsZWN0b3IgPSAnI2FkZG9uc19sb2dpbl9idG4nO1xuICAgIHRoaXMuY2F0ZWdvcnlSZXNldEJ0blNlbGVjdG9yID0gJy5tb2R1bGUtY2F0ZWdvcnktcmVzZXQnO1xuICAgIHRoaXMubW9kdWxlSW5zdGFsbEJ0blNlbGVjdG9yID0gJ2lucHV0Lm1vZHVsZS1pbnN0YWxsLWJ0bic7XG4gICAgdGhpcy5tb2R1bGVTb3J0aW5nRHJvcGRvd25TZWxlY3RvciA9ICcubW9kdWxlLXNvcnRpbmctYXV0aG9yIHNlbGVjdCc7XG4gICAgdGhpcy5jYXRlZ29yeUdyaWRTZWxlY3RvciA9ICcjbW9kdWxlcy1jYXRlZ29yaWVzLWdyaWQnO1xuICAgIHRoaXMuY2F0ZWdvcnlHcmlkSXRlbVNlbGVjdG9yID0gJy5tb2R1bGUtY2F0ZWdvcnktaXRlbSc7XG4gICAgdGhpcy5hZGRvbkl0ZW1HcmlkU2VsZWN0b3IgPSAnLm1vZHVsZS1hZGRvbnMtaXRlbS1ncmlkJztcbiAgICB0aGlzLmFkZG9uSXRlbUxpc3RTZWxlY3RvciA9ICcubW9kdWxlLWFkZG9ucy1pdGVtLWxpc3QnO1xuXG4gICAgLy8gVXBncmFkZSBBbGwgc2VsZWN0b3JzXG4gICAgdGhpcy51cGdyYWRlQWxsU291cmNlID0gJy5tb2R1bGVfYWN0aW9uX21lbnVfdXBncmFkZV9hbGwnO1xuICAgIHRoaXMudXBncmFkZUNvbnRhaW5lciA9ICcjbW9kdWxlcy1saXN0LWNvbnRhaW5lci11cGRhdGUnO1xuICAgIHRoaXMudXBncmFkZUFsbFRhcmdldHMgPSBgJHt0aGlzLnVwZ3JhZGVDb250YWluZXJ9IC5tb2R1bGVfYWN0aW9uX21lbnVfdXBncmFkZTp2aXNpYmxlYDtcblxuICAgIC8vIE5vdGlmaWNhdGlvbiBzZWxlY3RvcnNcbiAgICB0aGlzLm5vdGlmaWNhdGlvbkNvbnRhaW5lciA9ICcjbW9kdWxlcy1saXN0LWNvbnRhaW5lci1ub3RpZmljYXRpb24nO1xuXG4gICAgLy8gQnVsayBhY3Rpb24gc2VsZWN0b3JzXG4gICAgdGhpcy5idWxrQWN0aW9uRHJvcERvd25TZWxlY3RvciA9ICcubW9kdWxlLWJ1bGstYWN0aW9ucyc7XG4gICAgdGhpcy5idWxrSXRlbVNlbGVjdG9yID0gJy5tb2R1bGUtYnVsay1tZW51JztcbiAgICB0aGlzLmJ1bGtBY3Rpb25DaGVja2JveExpc3RTZWxlY3RvciA9ICcubW9kdWxlLWNoZWNrYm94LWJ1bGstbGlzdCBpbnB1dCc7XG4gICAgdGhpcy5idWxrQWN0aW9uQ2hlY2tib3hHcmlkU2VsZWN0b3IgPSAnLm1vZHVsZS1jaGVja2JveC1idWxrLWdyaWQgaW5wdXQnO1xuICAgIHRoaXMuY2hlY2tlZEJ1bGtBY3Rpb25MaXN0U2VsZWN0b3IgPSBgJHt0aGlzLmJ1bGtBY3Rpb25DaGVja2JveExpc3RTZWxlY3Rvcn06Y2hlY2tlZGA7XG4gICAgdGhpcy5jaGVja2VkQnVsa0FjdGlvbkdyaWRTZWxlY3RvciA9IGAke3RoaXMuYnVsa0FjdGlvbkNoZWNrYm94R3JpZFNlbGVjdG9yfTpjaGVja2VkYDtcbiAgICB0aGlzLmJ1bGtBY3Rpb25DaGVja2JveFNlbGVjdG9yID0gJyNtb2R1bGUtbW9kYWwtYnVsay1jaGVja2JveCc7XG4gICAgdGhpcy5idWxrQ29uZmlybU1vZGFsU2VsZWN0b3IgPSAnI21vZHVsZS1tb2RhbC1idWxrLWNvbmZpcm0nO1xuICAgIHRoaXMuYnVsa0NvbmZpcm1Nb2RhbEFjdGlvbk5hbWVTZWxlY3RvciA9ICcjbW9kdWxlLW1vZGFsLWJ1bGstY29uZmlybS1hY3Rpb24tbmFtZSc7XG4gICAgdGhpcy5idWxrQ29uZmlybU1vZGFsTGlzdFNlbGVjdG9yID0gJyNtb2R1bGUtbW9kYWwtYnVsay1jb25maXJtLWxpc3QnO1xuICAgIHRoaXMuYnVsa0NvbmZpcm1Nb2RhbEFja0J0blNlbGVjdG9yID0gJyNtb2R1bGUtbW9kYWwtY29uZmlybS1idWxrLWFjayc7XG5cbiAgICAvLyBQbGFjZWhvbGRlcnNcbiAgICB0aGlzLnBsYWNlaG9sZGVyR2xvYmFsU2VsZWN0b3IgPSAnLm1vZHVsZS1wbGFjZWhvbGRlcnMtd3JhcHBlcic7XG4gICAgdGhpcy5wbGFjZWhvbGRlckZhaWx1cmVHbG9iYWxTZWxlY3RvciA9ICcubW9kdWxlLXBsYWNlaG9sZGVycy1mYWlsdXJlJztcbiAgICB0aGlzLnBsYWNlaG9sZGVyRmFpbHVyZU1zZ1NlbGVjdG9yID0gJy5tb2R1bGUtcGxhY2Vob2xkZXJzLWZhaWx1cmUtbXNnJztcbiAgICB0aGlzLnBsYWNlaG9sZGVyRmFpbHVyZVJldHJ5QnRuU2VsZWN0b3IgPSAnI21vZHVsZS1wbGFjZWhvbGRlcnMtZmFpbHVyZS1yZXRyeSc7XG5cbiAgICAvLyBNb2R1bGUncyBzdGF0dXNlcyBzZWxlY3RvcnNcbiAgICB0aGlzLnN0YXR1c1NlbGVjdG9yTGFiZWxTZWxlY3RvciA9ICcubW9kdWxlLXN0YXR1cy1zZWxlY3Rvci1sYWJlbCc7XG4gICAgdGhpcy5zdGF0dXNJdGVtU2VsZWN0b3IgPSAnLm1vZHVsZS1zdGF0dXMtbWVudSc7XG4gICAgdGhpcy5zdGF0dXNSZXNldEJ0blNlbGVjdG9yID0gJy5tb2R1bGUtc3RhdHVzLXJlc2V0JztcblxuICAgIC8vIFNlbGVjdG9ycyBmb3IgTW9kdWxlIEltcG9ydCBhbmQgQWRkb25zIGNvbm5lY3RcbiAgICB0aGlzLmFkZG9uc0Nvbm5lY3RNb2RhbEJ0blNlbGVjdG9yID0gJyNwYWdlLWhlYWRlci1kZXNjLWNvbmZpZ3VyYXRpb24tYWRkb25zX2Nvbm5lY3QnO1xuICAgIHRoaXMuYWRkb25zTG9nb3V0TW9kYWxCdG5TZWxlY3RvciA9ICcjcGFnZS1oZWFkZXItZGVzYy1jb25maWd1cmF0aW9uLWFkZG9uc19sb2dvdXQnO1xuICAgIHRoaXMuYWRkb25zSW1wb3J0TW9kYWxCdG5TZWxlY3RvciA9ICcjcGFnZS1oZWFkZXItZGVzYy1jb25maWd1cmF0aW9uLWFkZF9tb2R1bGUnO1xuICAgIHRoaXMuZHJvcFpvbmVNb2RhbFNlbGVjdG9yID0gJyNtb2R1bGUtbW9kYWwtaW1wb3J0JztcbiAgICB0aGlzLmRyb3Bab25lTW9kYWxGb290ZXJTZWxlY3RvciA9ICcjbW9kdWxlLW1vZGFsLWltcG9ydCAubW9kYWwtZm9vdGVyJztcbiAgICB0aGlzLmRyb3Bab25lSW1wb3J0Wm9uZVNlbGVjdG9yID0gJyNpbXBvcnREcm9wem9uZSc7XG4gICAgdGhpcy5hZGRvbnNDb25uZWN0TW9kYWxTZWxlY3RvciA9ICcjbW9kdWxlLW1vZGFsLWFkZG9ucy1jb25uZWN0JztcbiAgICB0aGlzLmFkZG9uc0xvZ291dE1vZGFsU2VsZWN0b3IgPSAnI21vZHVsZS1tb2RhbC1hZGRvbnMtbG9nb3V0JztcbiAgICB0aGlzLmFkZG9uc0Nvbm5lY3RGb3JtID0gJyNhZGRvbnMtY29ubmVjdC1mb3JtJztcbiAgICB0aGlzLm1vZHVsZUltcG9ydE1vZGFsQ2xvc2VCdG4gPSAnI21vZHVsZS1tb2RhbC1pbXBvcnQtY2xvc2luZy1jcm9zcyc7XG4gICAgdGhpcy5tb2R1bGVJbXBvcnRTdGFydFNlbGVjdG9yID0gJy5tb2R1bGUtaW1wb3J0LXN0YXJ0JztcbiAgICB0aGlzLm1vZHVsZUltcG9ydFByb2Nlc3NpbmdTZWxlY3RvciA9ICcubW9kdWxlLWltcG9ydC1wcm9jZXNzaW5nJztcbiAgICB0aGlzLm1vZHVsZUltcG9ydFN1Y2Nlc3NTZWxlY3RvciA9ICcubW9kdWxlLWltcG9ydC1zdWNjZXNzJztcbiAgICB0aGlzLm1vZHVsZUltcG9ydFN1Y2Nlc3NDb25maWd1cmVCdG5TZWxlY3RvciA9ICcubW9kdWxlLWltcG9ydC1zdWNjZXNzLWNvbmZpZ3VyZSc7XG4gICAgdGhpcy5tb2R1bGVJbXBvcnRGYWlsdXJlU2VsZWN0b3IgPSAnLm1vZHVsZS1pbXBvcnQtZmFpbHVyZSc7XG4gICAgdGhpcy5tb2R1bGVJbXBvcnRGYWlsdXJlUmV0cnlTZWxlY3RvciA9ICcubW9kdWxlLWltcG9ydC1mYWlsdXJlLXJldHJ5JztcbiAgICB0aGlzLm1vZHVsZUltcG9ydEZhaWx1cmVEZXRhaWxzQnRuU2VsZWN0b3IgPSAnLm1vZHVsZS1pbXBvcnQtZmFpbHVyZS1kZXRhaWxzLWFjdGlvbic7XG4gICAgdGhpcy5tb2R1bGVJbXBvcnRTZWxlY3RGaWxlTWFudWFsU2VsZWN0b3IgPSAnLm1vZHVsZS1pbXBvcnQtc3RhcnQtc2VsZWN0LW1hbnVhbCc7XG4gICAgdGhpcy5tb2R1bGVJbXBvcnRGYWlsdXJlTXNnRGV0YWlsc1NlbGVjdG9yID0gJy5tb2R1bGUtaW1wb3J0LWZhaWx1cmUtZGV0YWlscyc7XG4gICAgdGhpcy5tb2R1bGVJbXBvcnRDb25maXJtU2VsZWN0b3IgPSAnLm1vZHVsZS1pbXBvcnQtY29uZmlybSc7XG5cbiAgICB0aGlzLmluaXRTb3J0aW5nRHJvcGRvd24oKTtcbiAgICB0aGlzLmluaXRCT0V2ZW50UmVnaXN0ZXJpbmcoKTtcbiAgICB0aGlzLmluaXRDdXJyZW50RGlzcGxheSgpO1xuICAgIHRoaXMuaW5pdFNvcnRpbmdEaXNwbGF5U3dpdGNoKCk7XG4gICAgdGhpcy5pbml0QnVsa0Ryb3Bkb3duKCk7XG4gICAgdGhpcy5pbml0U2VhcmNoQmxvY2soKTtcbiAgICB0aGlzLmluaXRDYXRlZ29yeVNlbGVjdCgpO1xuICAgIHRoaXMuaW5pdENhdGVnb3JpZXNHcmlkKCk7XG4gICAgdGhpcy5pbml0QWN0aW9uQnV0dG9ucygpO1xuICAgIHRoaXMuaW5pdEFkZG9uc1NlYXJjaCgpO1xuICAgIHRoaXMuaW5pdEFkZG9uc0Nvbm5lY3QoKTtcbiAgICB0aGlzLmluaXRBZGRNb2R1bGVBY3Rpb24oKTtcbiAgICB0aGlzLmluaXREcm9wem9uZSgpO1xuICAgIHRoaXMuaW5pdFBhZ2VDaGFuZ2VQcm90ZWN0aW9uKCk7XG4gICAgdGhpcy5pbml0UGxhY2Vob2xkZXJNZWNoYW5pc20oKTtcbiAgICB0aGlzLmluaXRGaWx0ZXJTdGF0dXNEcm9wZG93bigpO1xuICAgIHRoaXMuZmV0Y2hNb2R1bGVzTGlzdCgpO1xuICAgIHRoaXMuZ2V0Tm90aWZpY2F0aW9uc0NvdW50KCk7XG4gICAgdGhpcy5pbml0aWFsaXplU2VlTW9yZSgpO1xuICB9XG5cbiAgaW5pdEZpbHRlclN0YXR1c0Ryb3Bkb3duKCkge1xuICAgIGNvbnN0IHNlbGYgPSB0aGlzO1xuICAgIGNvbnN0IGJvZHkgPSAkKCdib2R5Jyk7XG4gICAgYm9keS5vbignY2xpY2snLCBzZWxmLnN0YXR1c0l0ZW1TZWxlY3RvciwgZnVuY3Rpb24gKCkge1xuICAgICAgLy8gR2V0IGRhdGEgZnJvbSBsaSBET00gaW5wdXRcbiAgICAgIHNlbGYuY3VycmVudFJlZlN0YXR1cyA9IHBhcnNlSW50KCQodGhpcykuZGF0YSgnc3RhdHVzLXJlZicpLCAxMCk7XG4gICAgICAvLyBDaGFuZ2UgZHJvcGRvd24gbGFiZWwgdG8gc2V0IGl0IHRvIHRoZSBjdXJyZW50IHN0YXR1cycgZGlzcGxheW5hbWVcbiAgICAgICQoc2VsZi5zdGF0dXNTZWxlY3RvckxhYmVsU2VsZWN0b3IpLnRleHQoJCh0aGlzKS50ZXh0KCkpO1xuICAgICAgJChzZWxmLnN0YXR1c1Jlc2V0QnRuU2VsZWN0b3IpLnNob3coKTtcbiAgICAgIHNlbGYudXBkYXRlTW9kdWxlVmlzaWJpbGl0eSgpO1xuICAgIH0pO1xuXG4gICAgYm9keS5vbignY2xpY2snLCBzZWxmLnN0YXR1c1Jlc2V0QnRuU2VsZWN0b3IsIGZ1bmN0aW9uICgpIHtcbiAgICAgICQoc2VsZi5zdGF0dXNTZWxlY3RvckxhYmVsU2VsZWN0b3IpLnRleHQoJCh0aGlzKS50ZXh0KCkpO1xuICAgICAgJCh0aGlzKS5oaWRlKCk7XG4gICAgICBzZWxmLmN1cnJlbnRSZWZTdGF0dXMgPSBudWxsO1xuICAgICAgc2VsZi51cGRhdGVNb2R1bGVWaXNpYmlsaXR5KCk7XG4gICAgfSk7XG4gIH1cblxuICBpbml0QnVsa0Ryb3Bkb3duKCkge1xuICAgIGNvbnN0IHNlbGYgPSB0aGlzO1xuICAgIGNvbnN0IGJvZHkgPSAkKCdib2R5Jyk7XG5cbiAgICBib2R5Lm9uKCdjbGljaycsIHNlbGYuZ2V0QnVsa0NoZWNrYm94ZXNTZWxlY3RvcigpLCAoKSA9PiB7XG4gICAgICBjb25zdCBzZWxlY3RvciA9ICQoc2VsZi5idWxrQWN0aW9uRHJvcERvd25TZWxlY3Rvcik7XG5cbiAgICAgIGlmICgkKHNlbGYuZ2V0QnVsa0NoZWNrYm94ZXNDaGVja2VkU2VsZWN0b3IoKSkubGVuZ3RoID4gMCkge1xuICAgICAgICBzZWxlY3Rvci5jbG9zZXN0KCcubW9kdWxlLXRvcC1tZW51LWl0ZW0nKS5yZW1vdmVDbGFzcygnZGlzYWJsZWQnKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHNlbGVjdG9yLmNsb3Nlc3QoJy5tb2R1bGUtdG9wLW1lbnUtaXRlbScpLmFkZENsYXNzKCdkaXNhYmxlZCcpO1xuICAgICAgfVxuICAgIH0pO1xuXG4gICAgYm9keS5vbignY2xpY2snLCBzZWxmLmJ1bGtJdGVtU2VsZWN0b3IsIGZ1bmN0aW9uIGluaXRpYWxpemVCb2R5Q2hhbmdlKCkge1xuICAgICAgaWYgKCQoc2VsZi5nZXRCdWxrQ2hlY2tib3hlc0NoZWNrZWRTZWxlY3RvcigpKS5sZW5ndGggPT09IDApIHtcbiAgICAgICAgJC5ncm93bC53YXJuaW5nKHtcbiAgICAgICAgICBtZXNzYWdlOiB3aW5kb3cudHJhbnNsYXRlX2phdmFzY3JpcHRzWydCdWxrIEFjdGlvbiAtIE9uZSBtb2R1bGUgbWluaW11bSddLFxuICAgICAgICB9KTtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICBzZWxmLmxhc3RCdWxrQWN0aW9uID0gJCh0aGlzKS5kYXRhKCdyZWYnKTtcbiAgICAgIGNvbnN0IG1vZHVsZXNMaXN0U3RyaW5nID0gc2VsZi5idWlsZEJ1bGtBY3Rpb25Nb2R1bGVMaXN0KCk7XG4gICAgICBjb25zdCBhY3Rpb25TdHJpbmcgPSAkKHRoaXMpXG4gICAgICAgIC5maW5kKCc6Y2hlY2tlZCcpXG4gICAgICAgIC50ZXh0KClcbiAgICAgICAgLnRvTG93ZXJDYXNlKCk7XG4gICAgICAkKHNlbGYuYnVsa0NvbmZpcm1Nb2RhbExpc3RTZWxlY3RvcikuaHRtbChtb2R1bGVzTGlzdFN0cmluZyk7XG4gICAgICAkKHNlbGYuYnVsa0NvbmZpcm1Nb2RhbEFjdGlvbk5hbWVTZWxlY3RvcikudGV4dChhY3Rpb25TdHJpbmcpO1xuXG4gICAgICBpZiAoc2VsZi5sYXN0QnVsa0FjdGlvbiA9PT0gJ2J1bGstdW5pbnN0YWxsJykge1xuICAgICAgICAkKHNlbGYuYnVsa0FjdGlvbkNoZWNrYm94U2VsZWN0b3IpLnNob3coKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgICQoc2VsZi5idWxrQWN0aW9uQ2hlY2tib3hTZWxlY3RvcikuaGlkZSgpO1xuICAgICAgfVxuXG4gICAgICAkKHNlbGYuYnVsa0NvbmZpcm1Nb2RhbFNlbGVjdG9yKS5tb2RhbCgnc2hvdycpO1xuICAgIH0pO1xuXG4gICAgYm9keS5vbignY2xpY2snLCB0aGlzLmJ1bGtDb25maXJtTW9kYWxBY2tCdG5TZWxlY3RvciwgKGV2ZW50KSA9PiB7XG4gICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgZXZlbnQuc3RvcFByb3BhZ2F0aW9uKCk7XG4gICAgICAkKHNlbGYuYnVsa0NvbmZpcm1Nb2RhbFNlbGVjdG9yKS5tb2RhbCgnaGlkZScpO1xuICAgICAgc2VsZi5kb0J1bGtBY3Rpb24oc2VsZi5sYXN0QnVsa0FjdGlvbik7XG4gICAgfSk7XG4gIH1cblxuICBpbml0Qk9FdmVudFJlZ2lzdGVyaW5nKCkge1xuICAgIHdpbmRvdy5CT0V2ZW50Lm9uKCdNb2R1bGUgRGlzYWJsZWQnLCB0aGlzLm9uTW9kdWxlRGlzYWJsZWQsIHRoaXMpO1xuICAgIHdpbmRvdy5CT0V2ZW50Lm9uKCdNb2R1bGUgVW5pbnN0YWxsZWQnLCB0aGlzLnVwZGF0ZVRvdGFsUmVzdWx0cywgdGhpcyk7XG4gIH1cblxuICBvbk1vZHVsZURpc2FibGVkKCkge1xuICAgIGNvbnN0IHNlbGYgPSB0aGlzO1xuICAgIHNlbGYuZ2V0TW9kdWxlSXRlbVNlbGVjdG9yKCk7XG5cbiAgICAkKCcubW9kdWxlcy1saXN0JykuZWFjaCgoKSA9PiB7XG4gICAgICBzZWxmLnVwZGF0ZVRvdGFsUmVzdWx0cygpO1xuICAgIH0pO1xuICB9XG5cbiAgaW5pdFBsYWNlaG9sZGVyTWVjaGFuaXNtKCkge1xuICAgIGNvbnN0IHNlbGYgPSB0aGlzO1xuXG4gICAgaWYgKCQoc2VsZi5wbGFjZWhvbGRlckdsb2JhbFNlbGVjdG9yKS5sZW5ndGgpIHtcbiAgICAgIHNlbGYuYWpheExvYWRQYWdlKCk7XG4gICAgfVxuXG4gICAgLy8gUmV0cnkgbG9hZGluZyBtZWNoYW5pc21cbiAgICAkKCdib2R5Jykub24oJ2NsaWNrJywgc2VsZi5wbGFjZWhvbGRlckZhaWx1cmVSZXRyeUJ0blNlbGVjdG9yLCAoKSA9PiB7XG4gICAgICAkKHNlbGYucGxhY2Vob2xkZXJGYWlsdXJlR2xvYmFsU2VsZWN0b3IpLmZhZGVPdXQoKTtcbiAgICAgICQoc2VsZi5wbGFjZWhvbGRlckdsb2JhbFNlbGVjdG9yKS5mYWRlSW4oKTtcbiAgICAgIHNlbGYuYWpheExvYWRQYWdlKCk7XG4gICAgfSk7XG4gIH1cblxuICBhamF4TG9hZFBhZ2UoKSB7XG4gICAgY29uc3Qgc2VsZiA9IHRoaXM7XG5cbiAgICAkLmFqYXgoe1xuICAgICAgbWV0aG9kOiAnR0VUJyxcbiAgICAgIHVybDogd2luZG93Lm1vZHVsZVVSTHMuY2F0YWxvZ1JlZnJlc2gsXG4gICAgfSlcbiAgICAgIC5kb25lKChyZXNwb25zZSkgPT4ge1xuICAgICAgICBpZiAocmVzcG9uc2Uuc3RhdHVzID09PSB0cnVlKSB7XG4gICAgICAgICAgaWYgKHR5cGVvZiByZXNwb25zZS5kb21FbGVtZW50cyA9PT0gJ3VuZGVmaW5lZCcpIHJlc3BvbnNlLmRvbUVsZW1lbnRzID0gbnVsbDtcbiAgICAgICAgICBpZiAodHlwZW9mIHJlc3BvbnNlLm1zZyA9PT0gJ3VuZGVmaW5lZCcpIHJlc3BvbnNlLm1zZyA9IG51bGw7XG5cbiAgICAgICAgICBjb25zdCBzdHlsZXNoZWV0ID0gZG9jdW1lbnQuc3R5bGVTaGVldHNbMF07XG4gICAgICAgICAgY29uc3Qgc3R5bGVzaGVldFJ1bGUgPSAne2Rpc3BsYXk6IG5vbmV9JztcbiAgICAgICAgICBjb25zdCBtb2R1bGVHbG9iYWxTZWxlY3RvciA9ICcubW9kdWxlcy1saXN0JztcbiAgICAgICAgICBjb25zdCBtb2R1bGVTb3J0aW5nU2VsZWN0b3IgPSAnLm1vZHVsZS1zb3J0aW5nLW1lbnUnO1xuICAgICAgICAgIGNvbnN0IHJlcXVpcmVkU2VsZWN0b3JDb21iaW5hdGlvbiA9IGAke21vZHVsZUdsb2JhbFNlbGVjdG9yfSwke21vZHVsZVNvcnRpbmdTZWxlY3Rvcn1gO1xuXG4gICAgICAgICAgaWYgKHN0eWxlc2hlZXQuaW5zZXJ0UnVsZSkge1xuICAgICAgICAgICAgc3R5bGVzaGVldC5pbnNlcnRSdWxlKHJlcXVpcmVkU2VsZWN0b3JDb21iaW5hdGlvbiArIHN0eWxlc2hlZXRSdWxlLCBzdHlsZXNoZWV0LmNzc1J1bGVzLmxlbmd0aCk7XG4gICAgICAgICAgfSBlbHNlIGlmIChzdHlsZXNoZWV0LmFkZFJ1bGUpIHtcbiAgICAgICAgICAgIHN0eWxlc2hlZXQuYWRkUnVsZShyZXF1aXJlZFNlbGVjdG9yQ29tYmluYXRpb24sIHN0eWxlc2hlZXRSdWxlLCAtMSk7XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgJChzZWxmLnBsYWNlaG9sZGVyR2xvYmFsU2VsZWN0b3IpLmZhZGVPdXQoODAwLCAoKSA9PiB7XG4gICAgICAgICAgICAkLmVhY2gocmVzcG9uc2UuZG9tRWxlbWVudHMsIChpbmRleCwgZWxlbWVudCkgPT4ge1xuICAgICAgICAgICAgICAkKGVsZW1lbnQuc2VsZWN0b3IpLmFwcGVuZChlbGVtZW50LmNvbnRlbnQpO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAkKG1vZHVsZUdsb2JhbFNlbGVjdG9yKVxuICAgICAgICAgICAgICAuZmFkZUluKDgwMClcbiAgICAgICAgICAgICAgLmNzcygnZGlzcGxheScsICdmbGV4Jyk7XG4gICAgICAgICAgICAkKG1vZHVsZVNvcnRpbmdTZWxlY3RvcikuZmFkZUluKDgwMCk7XG4gICAgICAgICAgICAkKCdbZGF0YS10b2dnbGU9XCJwb3BvdmVyXCJdJykucG9wb3ZlcigpO1xuICAgICAgICAgICAgc2VsZi5pbml0Q3VycmVudERpc3BsYXkoKTtcbiAgICAgICAgICAgIHNlbGYuZmV0Y2hNb2R1bGVzTGlzdCgpO1xuICAgICAgICAgIH0pO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICQoc2VsZi5wbGFjZWhvbGRlckdsb2JhbFNlbGVjdG9yKS5mYWRlT3V0KDgwMCwgKCkgPT4ge1xuICAgICAgICAgICAgJChzZWxmLnBsYWNlaG9sZGVyRmFpbHVyZU1zZ1NlbGVjdG9yKS50ZXh0KHJlc3BvbnNlLm1zZyk7XG4gICAgICAgICAgICAkKHNlbGYucGxhY2Vob2xkZXJGYWlsdXJlR2xvYmFsU2VsZWN0b3IpLmZhZGVJbig4MDApO1xuICAgICAgICAgIH0pO1xuICAgICAgICB9XG4gICAgICB9KVxuICAgICAgLmZhaWwoKHJlc3BvbnNlKSA9PiB7XG4gICAgICAgICQoc2VsZi5wbGFjZWhvbGRlckdsb2JhbFNlbGVjdG9yKS5mYWRlT3V0KDgwMCwgKCkgPT4ge1xuICAgICAgICAgICQoc2VsZi5wbGFjZWhvbGRlckZhaWx1cmVNc2dTZWxlY3RvcikudGV4dChyZXNwb25zZS5zdGF0dXNUZXh0KTtcbiAgICAgICAgICAkKHNlbGYucGxhY2Vob2xkZXJGYWlsdXJlR2xvYmFsU2VsZWN0b3IpLmZhZGVJbig4MDApO1xuICAgICAgICB9KTtcbiAgICAgIH0pO1xuICB9XG5cbiAgZmV0Y2hNb2R1bGVzTGlzdCgpIHtcbiAgICBjb25zdCBzZWxmID0gdGhpcztcbiAgICBsZXQgY29udGFpbmVyO1xuICAgIGxldCAkdGhpcztcblxuICAgIHNlbGYubW9kdWxlc0xpc3QgPSBbXTtcbiAgICAkKCcubW9kdWxlcy1saXN0JykuZWFjaChmdW5jdGlvbiBwcmVwYXJlQ29udGFpbmVyKCkge1xuICAgICAgY29udGFpbmVyID0gJCh0aGlzKTtcbiAgICAgIGNvbnRhaW5lci5maW5kKCcubW9kdWxlLWl0ZW0nKS5lYWNoKGZ1bmN0aW9uIHByZXBhcmVNb2R1bGVzKCkge1xuICAgICAgICAkdGhpcyA9ICQodGhpcyk7XG4gICAgICAgIHNlbGYubW9kdWxlc0xpc3QucHVzaCh7XG4gICAgICAgICAgZG9tT2JqZWN0OiAkdGhpcyxcbiAgICAgICAgICBpZDogJHRoaXMuZGF0YSgnaWQnKSxcbiAgICAgICAgICBuYW1lOiAkdGhpcy5kYXRhKCduYW1lJykudG9Mb3dlckNhc2UoKSxcbiAgICAgICAgICBzY29yaW5nOiBwYXJzZUZsb2F0KCR0aGlzLmRhdGEoJ3Njb3JpbmcnKSksXG4gICAgICAgICAgbG9nbzogJHRoaXMuZGF0YSgnbG9nbycpLFxuICAgICAgICAgIGF1dGhvcjogJHRoaXMuZGF0YSgnYXV0aG9yJykudG9Mb3dlckNhc2UoKSxcbiAgICAgICAgICB2ZXJzaW9uOiAkdGhpcy5kYXRhKCd2ZXJzaW9uJyksXG4gICAgICAgICAgZGVzY3JpcHRpb246ICR0aGlzLmRhdGEoJ2Rlc2NyaXB0aW9uJykudG9Mb3dlckNhc2UoKSxcbiAgICAgICAgICB0ZWNoTmFtZTogJHRoaXMuZGF0YSgndGVjaC1uYW1lJykudG9Mb3dlckNhc2UoKSxcbiAgICAgICAgICBjaGlsZENhdGVnb3JpZXM6ICR0aGlzLmRhdGEoJ2NoaWxkLWNhdGVnb3JpZXMnKSxcbiAgICAgICAgICBjYXRlZ29yaWVzOiBTdHJpbmcoJHRoaXMuZGF0YSgnY2F0ZWdvcmllcycpKS50b0xvd2VyQ2FzZSgpLFxuICAgICAgICAgIHR5cGU6ICR0aGlzLmRhdGEoJ3R5cGUnKSxcbiAgICAgICAgICBwcmljZTogcGFyc2VGbG9hdCgkdGhpcy5kYXRhKCdwcmljZScpKSxcbiAgICAgICAgICBhY3RpdmU6IHBhcnNlSW50KCR0aGlzLmRhdGEoJ2FjdGl2ZScpLCAxMCksXG4gICAgICAgICAgYWNjZXNzOiAkdGhpcy5kYXRhKCdsYXN0LWFjY2VzcycpLFxuICAgICAgICAgIGRpc3BsYXk6ICR0aGlzLmhhc0NsYXNzKCdtb2R1bGUtaXRlbS1saXN0JykgPyBzZWxmLkRJU1BMQVlfTElTVCA6IHNlbGYuRElTUExBWV9HUklELFxuICAgICAgICAgIGNvbnRhaW5lcixcbiAgICAgICAgfSk7XG5cbiAgICAgICAgaWYgKHNlbGYuaXNNb2R1bGVzUGFnZSgpKSB7XG4gICAgICAgICAgJHRoaXMucmVtb3ZlKCk7XG4gICAgICAgIH1cbiAgICAgIH0pO1xuICAgIH0pO1xuXG4gICAgc2VsZi5hZGRvbnNDYXJkR3JpZCA9ICQodGhpcy5hZGRvbkl0ZW1HcmlkU2VsZWN0b3IpO1xuICAgIHNlbGYuYWRkb25zQ2FyZExpc3QgPSAkKHRoaXMuYWRkb25JdGVtTGlzdFNlbGVjdG9yKTtcbiAgICBzZWxmLnVwZGF0ZU1vZHVsZVZpc2liaWxpdHkoKTtcbiAgICAkKCdib2R5JykudHJpZ2dlcignbW9kdWxlQ2F0YWxvZ0xvYWRlZCcpO1xuICB9XG5cbiAgLyoqXG4gICAqIFByZXBhcmUgc29ydGluZ1xuICAgKlxuICAgKi9cbiAgdXBkYXRlTW9kdWxlU29ydGluZygpIHtcbiAgICBjb25zdCBzZWxmID0gdGhpcztcblxuICAgIGlmICghc2VsZi5jdXJyZW50U29ydGluZykge1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIC8vIE1vZHVsZXMgc29ydGluZ1xuICAgIGxldCBvcmRlciA9ICdhc2MnO1xuICAgIGxldCBrZXkgPSBzZWxmLmN1cnJlbnRTb3J0aW5nO1xuICAgIGNvbnN0IHNwbGl0dGVkS2V5ID0ga2V5LnNwbGl0KCctJyk7XG5cbiAgICBpZiAoc3BsaXR0ZWRLZXkubGVuZ3RoID4gMSkge1xuICAgICAga2V5ID0gc3BsaXR0ZWRLZXlbMF07XG4gICAgICBpZiAoc3BsaXR0ZWRLZXlbMV0gPT09ICdkZXNjJykge1xuICAgICAgICBvcmRlciA9ICdkZXNjJztcbiAgICAgIH1cbiAgICB9XG5cbiAgICBjb25zdCBjdXJyZW50Q29tcGFyZSA9IChhLCBiKSA9PiB7XG4gICAgICBsZXQgYURhdGEgPSBhW2tleV07XG4gICAgICBsZXQgYkRhdGEgPSBiW2tleV07XG5cbiAgICAgIGlmIChrZXkgPT09ICdhY2Nlc3MnKSB7XG4gICAgICAgIGFEYXRhID0gbmV3IERhdGUoYURhdGEpLmdldFRpbWUoKTtcbiAgICAgICAgYkRhdGEgPSBuZXcgRGF0ZShiRGF0YSkuZ2V0VGltZSgpO1xuICAgICAgICBhRGF0YSA9IE51bWJlci5pc05hTihhRGF0YSkgPyAwIDogYURhdGE7XG4gICAgICAgIGJEYXRhID0gTnVtYmVyLmlzTmFOKGJEYXRhKSA/IDAgOiBiRGF0YTtcbiAgICAgICAgaWYgKGFEYXRhID09PSBiRGF0YSkge1xuICAgICAgICAgIHJldHVybiBiLm5hbWUubG9jYWxlQ29tcGFyZShhLm5hbWUpO1xuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIGlmIChhRGF0YSA8IGJEYXRhKSByZXR1cm4gLTE7XG4gICAgICBpZiAoYURhdGEgPiBiRGF0YSkgcmV0dXJuIDE7XG5cbiAgICAgIHJldHVybiAwO1xuICAgIH07XG5cbiAgICBzZWxmLm1vZHVsZXNMaXN0LnNvcnQoY3VycmVudENvbXBhcmUpO1xuICAgIGlmIChvcmRlciA9PT0gJ2Rlc2MnKSB7XG4gICAgICBzZWxmLm1vZHVsZXNMaXN0LnJldmVyc2UoKTtcbiAgICB9XG4gIH1cblxuICB1cGRhdGVNb2R1bGVDb250YWluZXJEaXNwbGF5KCkge1xuICAgIGNvbnN0IHNlbGYgPSB0aGlzO1xuXG4gICAgJCgnLm1vZHVsZS1zaG9ydC1saXN0JykuZWFjaChmdW5jdGlvbiBzZXRTaG9ydExpc3RWaXNpYmlsaXR5KCkge1xuICAgICAgY29uc3QgY29udGFpbmVyID0gJCh0aGlzKTtcbiAgICAgIGNvbnN0IG5iTW9kdWxlc0luQ29udGFpbmVyID0gY29udGFpbmVyLmZpbmQoJy5tb2R1bGUtaXRlbScpLmxlbmd0aDtcblxuICAgICAgaWYgKFxuICAgICAgICAoc2VsZi5jdXJyZW50UmVmQ2F0ZWdvcnkgJiYgc2VsZi5jdXJyZW50UmVmQ2F0ZWdvcnkgIT09IFN0cmluZyhjb250YWluZXIuZmluZCgnLm1vZHVsZXMtbGlzdCcpLmRhdGEoJ25hbWUnKSkpXG4gICAgICAgIHx8IChzZWxmLmN1cnJlbnRSZWZTdGF0dXMgIT09IG51bGwgJiYgbmJNb2R1bGVzSW5Db250YWluZXIgPT09IDApXG4gICAgICAgIHx8IChuYk1vZHVsZXNJbkNvbnRhaW5lciA9PT0gMFxuICAgICAgICAgICYmIFN0cmluZyhjb250YWluZXIuZmluZCgnLm1vZHVsZXMtbGlzdCcpLmRhdGEoJ25hbWUnKSkgPT09IHNlbGYuQ0FURUdPUllfUkVDRU5UTFlfVVNFRClcbiAgICAgICAgfHwgKHNlbGYuY3VycmVudFRhZ3NMaXN0Lmxlbmd0aCA+IDAgJiYgbmJNb2R1bGVzSW5Db250YWluZXIgPT09IDApXG4gICAgICApIHtcbiAgICAgICAgY29udGFpbmVyLmhpZGUoKTtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICBjb250YWluZXIuc2hvdygpO1xuICAgICAgY29udGFpbmVyXG4gICAgICAgIC5maW5kKGAke3NlbGYuc2VlTW9yZVNlbGVjdG9yfSwgJHtzZWxmLnNlZUxlc3NTZWxlY3Rvcn1gKVxuICAgICAgICAudG9nZ2xlKG5iTW9kdWxlc0luQ29udGFpbmVyID49IHNlbGYuREVGQVVMVF9NQVhfUEVSX0NBVEVHT1JJRVMpO1xuICAgIH0pO1xuICB9XG5cbiAgdXBkYXRlTW9kdWxlVmlzaWJpbGl0eSgpIHtcbiAgICBjb25zdCBzZWxmID0gdGhpcztcblxuICAgIHNlbGYudXBkYXRlTW9kdWxlU29ydGluZygpO1xuXG4gICAgaWYgKHNlbGYuaXNNb2R1bGVzUGFnZSgpKSB7XG4gICAgICAkKHNlbGYucmVjZW50bHlVc2VkU2VsZWN0b3IpXG4gICAgICAgIC5maW5kKCcubW9kdWxlLWl0ZW0nKVxuICAgICAgICAucmVtb3ZlKCk7XG4gICAgICAkKCcubW9kdWxlcy1saXN0JylcbiAgICAgICAgLmZpbmQoJy5tb2R1bGUtaXRlbScpXG4gICAgICAgIC5yZW1vdmUoKTtcbiAgICB9XG5cbiAgICAvLyBNb2R1bGVzIHZpc2liaWxpdHkgbWFuYWdlbWVudFxuICAgIGxldCBpc1Zpc2libGU7XG4gICAgbGV0IGN1cnJlbnRNb2R1bGU7XG4gICAgbGV0IG1vZHVsZUNhdGVnb3J5O1xuICAgIGxldCB0YWdFeGlzdHM7XG4gICAgbGV0IG5ld1ZhbHVlO1xuICAgIGxldCBkZWZhdWx0TWF4O1xuXG4gICAgY29uc3QgbW9kdWxlc0xpc3RMZW5ndGggPSBzZWxmLm1vZHVsZXNMaXN0Lmxlbmd0aDtcbiAgICBjb25zdCBjb3VudGVyID0ge307XG4gICAgY29uc3QgY2hlY2tUYWcgPSAoaW5kZXgsIHZhbHVlKSA9PiB7XG4gICAgICBuZXdWYWx1ZSA9IHZhbHVlLnRvTG93ZXJDYXNlKCk7XG4gICAgICB0YWdFeGlzdHNcbiAgICAgICAgfD0gY3VycmVudE1vZHVsZS5uYW1lLmluZGV4T2YobmV3VmFsdWUpICE9PSAtMVxuICAgICAgICB8fCBjdXJyZW50TW9kdWxlLmRlc2NyaXB0aW9uLmluZGV4T2YobmV3VmFsdWUpICE9PSAtMVxuICAgICAgICB8fCBjdXJyZW50TW9kdWxlLmF1dGhvci5pbmRleE9mKG5ld1ZhbHVlKSAhPT0gLTFcbiAgICAgICAgfHwgY3VycmVudE1vZHVsZS50ZWNoTmFtZS5pbmRleE9mKG5ld1ZhbHVlKSAhPT0gLTE7XG4gICAgfTtcblxuICAgIGZvciAobGV0IGkgPSAwOyBpIDwgbW9kdWxlc0xpc3RMZW5ndGg7IGkgKz0gMSkge1xuICAgICAgY3VycmVudE1vZHVsZSA9IHNlbGYubW9kdWxlc0xpc3RbaV07XG4gICAgICBpZiAoY3VycmVudE1vZHVsZS5kaXNwbGF5ID09PSBzZWxmLmN1cnJlbnREaXNwbGF5KSB7XG4gICAgICAgIGlzVmlzaWJsZSA9IHRydWU7XG5cbiAgICAgICAgbW9kdWxlQ2F0ZWdvcnkgPSBzZWxmLmN1cnJlbnRSZWZDYXRlZ29yeSA9PT0gc2VsZi5DQVRFR09SWV9SRUNFTlRMWV9VU0VEXG4gICAgICAgICAgPyBzZWxmLkNBVEVHT1JZX1JFQ0VOVExZX1VTRURcbiAgICAgICAgICA6IGN1cnJlbnRNb2R1bGUuY2F0ZWdvcmllcztcblxuICAgICAgICAvLyBDaGVjayBmb3Igc2FtZSBjYXRlZ29yeVxuICAgICAgICBpZiAoc2VsZi5jdXJyZW50UmVmQ2F0ZWdvcnkgIT09IG51bGwpIHtcbiAgICAgICAgICBpc1Zpc2libGUgJj0gbW9kdWxlQ2F0ZWdvcnkgPT09IHNlbGYuY3VycmVudFJlZkNhdGVnb3J5O1xuICAgICAgICB9XG5cbiAgICAgICAgLy8gQ2hlY2sgZm9yIHNhbWUgc3RhdHVzXG4gICAgICAgIGlmIChzZWxmLmN1cnJlbnRSZWZTdGF0dXMgIT09IG51bGwpIHtcbiAgICAgICAgICBpc1Zpc2libGUgJj0gY3VycmVudE1vZHVsZS5hY3RpdmUgPT09IHNlbGYuY3VycmVudFJlZlN0YXR1cztcbiAgICAgICAgfVxuXG4gICAgICAgIC8vIENoZWNrIGZvciB0YWcgbGlzdFxuICAgICAgICBpZiAoc2VsZi5jdXJyZW50VGFnc0xpc3QubGVuZ3RoKSB7XG4gICAgICAgICAgdGFnRXhpc3RzID0gZmFsc2U7XG4gICAgICAgICAgJC5lYWNoKHNlbGYuY3VycmVudFRhZ3NMaXN0LCBjaGVja1RhZyk7XG4gICAgICAgICAgaXNWaXNpYmxlICY9IHRhZ0V4aXN0cztcbiAgICAgICAgfVxuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiBJZiBsaXN0IGRpc3BsYXkgd2l0aG91dCBzZWFyY2ggd2UgbXVzdCBkaXNwbGF5IG9ubHkgdGhlIGZpcnN0IDUgbW9kdWxlc1xuICAgICAgICAgKi9cbiAgICAgICAgaWYgKHNlbGYuY3VycmVudERpc3BsYXkgPT09IHNlbGYuRElTUExBWV9MSVNUICYmICFzZWxmLmN1cnJlbnRUYWdzTGlzdC5sZW5ndGgpIHtcbiAgICAgICAgICBpZiAoc2VsZi5jdXJyZW50Q2F0ZWdvcnlEaXNwbGF5W21vZHVsZUNhdGVnb3J5XSA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICBzZWxmLmN1cnJlbnRDYXRlZ29yeURpc3BsYXlbbW9kdWxlQ2F0ZWdvcnldID0gZmFsc2U7XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgaWYgKCFjb3VudGVyW21vZHVsZUNhdGVnb3J5XSkge1xuICAgICAgICAgICAgY291bnRlclttb2R1bGVDYXRlZ29yeV0gPSAwO1xuICAgICAgICAgIH1cblxuICAgICAgICAgIGRlZmF1bHRNYXggPSBtb2R1bGVDYXRlZ29yeSA9PT0gc2VsZi5DQVRFR09SWV9SRUNFTlRMWV9VU0VEXG4gICAgICAgICAgICA/IHNlbGYuREVGQVVMVF9NQVhfUkVDRU5UTFlfVVNFRFxuICAgICAgICAgICAgOiBzZWxmLkRFRkFVTFRfTUFYX1BFUl9DQVRFR09SSUVTO1xuICAgICAgICAgIGlmIChjb3VudGVyW21vZHVsZUNhdGVnb3J5XSA+PSBkZWZhdWx0TWF4KSB7XG4gICAgICAgICAgICBpc1Zpc2libGUgJj0gc2VsZi5jdXJyZW50Q2F0ZWdvcnlEaXNwbGF5W21vZHVsZUNhdGVnb3J5XTtcbiAgICAgICAgICB9XG5cbiAgICAgICAgICBjb3VudGVyW21vZHVsZUNhdGVnb3J5XSArPSAxO1xuICAgICAgICB9XG5cbiAgICAgICAgLy8gSWYgdmlzaWJsZSwgZGlzcGxheSAoVGh4IGNhcHRhaW4gb2J2aW91cylcbiAgICAgICAgaWYgKGlzVmlzaWJsZSkge1xuICAgICAgICAgIGlmIChzZWxmLmN1cnJlbnRSZWZDYXRlZ29yeSA9PT0gc2VsZi5DQVRFR09SWV9SRUNFTlRMWV9VU0VEKSB7XG4gICAgICAgICAgICAkKHNlbGYucmVjZW50bHlVc2VkU2VsZWN0b3IpLmFwcGVuZChjdXJyZW50TW9kdWxlLmRvbU9iamVjdCk7XG4gICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIGN1cnJlbnRNb2R1bGUuY29udGFpbmVyLmFwcGVuZChjdXJyZW50TW9kdWxlLmRvbU9iamVjdCk7XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuXG4gICAgc2VsZi51cGRhdGVNb2R1bGVDb250YWluZXJEaXNwbGF5KCk7XG5cbiAgICBpZiAoc2VsZi5jdXJyZW50VGFnc0xpc3QubGVuZ3RoKSB7XG4gICAgICAkKCcubW9kdWxlcy1saXN0JykuYXBwZW5kKHRoaXMuY3VycmVudERpc3BsYXkgPT09IHNlbGYuRElTUExBWV9HUklEID8gdGhpcy5hZGRvbnNDYXJkR3JpZCA6IHRoaXMuYWRkb25zQ2FyZExpc3QpO1xuICAgIH1cblxuICAgIHNlbGYudXBkYXRlVG90YWxSZXN1bHRzKCk7XG4gIH1cblxuICBpbml0UGFnZUNoYW5nZVByb3RlY3Rpb24oKSB7XG4gICAgY29uc3Qgc2VsZiA9IHRoaXM7XG5cbiAgICAkKHdpbmRvdykub24oJ2JlZm9yZXVubG9hZCcsICgpID0+IHtcbiAgICAgIGlmIChzZWxmLmlzVXBsb2FkU3RhcnRlZCA9PT0gdHJ1ZSkge1xuICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICdJdCBzZWVtcyBzb21lIGNyaXRpY2FsIG9wZXJhdGlvbiBhcmUgcnVubmluZywgYXJlIHlvdSBzdXJlIHlvdSB3YW50IHRvIGNoYW5nZSBwYWdlPyAnXG4gICAgICAgICAgKyAnSXQgbWlnaHQgY2F1c2Ugc29tZSB1bmV4ZXBjdGVkIGJlaGF2aW9ycy4nXG4gICAgICAgICk7XG4gICAgICB9XG5cbiAgICAgIHJldHVybiB1bmRlZmluZWQ7XG4gICAgfSk7XG4gIH1cblxuICBidWlsZEJ1bGtBY3Rpb25Nb2R1bGVMaXN0KCkge1xuICAgIGNvbnN0IGNoZWNrQm94ZXNTZWxlY3RvciA9IHRoaXMuZ2V0QnVsa0NoZWNrYm94ZXNDaGVja2VkU2VsZWN0b3IoKTtcbiAgICBjb25zdCBtb2R1bGVJdGVtU2VsZWN0b3IgPSB0aGlzLmdldE1vZHVsZUl0ZW1TZWxlY3RvcigpO1xuICAgIGxldCBhbHJlYWR5RG9uZUZsYWcgPSAwO1xuICAgIGxldCBodG1sR2VuZXJhdGVkID0gJyc7XG4gICAgbGV0IGN1cnJlbnRFbGVtZW50O1xuXG4gICAgJChjaGVja0JveGVzU2VsZWN0b3IpLmVhY2goZnVuY3Rpb24gcHJlcGFyZUNoZWNrYm94ZXMoKSB7XG4gICAgICBpZiAoYWxyZWFkeURvbmVGbGFnID09PSAxMCkge1xuICAgICAgICAvLyBCcmVhayBlYWNoXG4gICAgICAgIGh0bWxHZW5lcmF0ZWQgKz0gJy0gLi4uJztcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgfVxuXG4gICAgICBjdXJyZW50RWxlbWVudCA9ICQodGhpcykuY2xvc2VzdChtb2R1bGVJdGVtU2VsZWN0b3IpO1xuICAgICAgaHRtbEdlbmVyYXRlZCArPSBgLSAke2N1cnJlbnRFbGVtZW50LmRhdGEoJ25hbWUnKX08YnIvPmA7XG4gICAgICBhbHJlYWR5RG9uZUZsYWcgKz0gMTtcblxuICAgICAgcmV0dXJuIHRydWU7XG4gICAgfSk7XG5cbiAgICByZXR1cm4gaHRtbEdlbmVyYXRlZDtcbiAgfVxuXG4gIGluaXRBZGRvbnNDb25uZWN0KCkge1xuICAgIGNvbnN0IHNlbGYgPSB0aGlzO1xuXG4gICAgLy8gTWFrZSBhZGRvbnMgY29ubmVjdCBtb2RhbCByZWFkeSB0byBiZSBjbGlja2VkXG4gICAgaWYgKCQoc2VsZi5hZGRvbnNDb25uZWN0TW9kYWxCdG5TZWxlY3RvcikuYXR0cignaHJlZicpID09PSAnIycpIHtcbiAgICAgICQoc2VsZi5hZGRvbnNDb25uZWN0TW9kYWxCdG5TZWxlY3RvcikuYXR0cignZGF0YS10b2dnbGUnLCAnbW9kYWwnKTtcbiAgICAgICQoc2VsZi5hZGRvbnNDb25uZWN0TW9kYWxCdG5TZWxlY3RvcikuYXR0cignZGF0YS10YXJnZXQnLCBzZWxmLmFkZG9uc0Nvbm5lY3RNb2RhbFNlbGVjdG9yKTtcbiAgICB9XG5cbiAgICBpZiAoJChzZWxmLmFkZG9uc0xvZ291dE1vZGFsQnRuU2VsZWN0b3IpLmF0dHIoJ2hyZWYnKSA9PT0gJyMnKSB7XG4gICAgICAkKHNlbGYuYWRkb25zTG9nb3V0TW9kYWxCdG5TZWxlY3RvcikuYXR0cignZGF0YS10b2dnbGUnLCAnbW9kYWwnKTtcbiAgICAgICQoc2VsZi5hZGRvbnNMb2dvdXRNb2RhbEJ0blNlbGVjdG9yKS5hdHRyKCdkYXRhLXRhcmdldCcsIHNlbGYuYWRkb25zTG9nb3V0TW9kYWxTZWxlY3Rvcik7XG4gICAgfVxuXG4gICAgJCgnYm9keScpLm9uKCdzdWJtaXQnLCBzZWxmLmFkZG9uc0Nvbm5lY3RGb3JtLCBmdW5jdGlvbiBpbml0aWFsaXplQm9keVN1Ym1pdChldmVudCkge1xuICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgIGV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO1xuXG4gICAgICAkLmFqYXgoe1xuICAgICAgICBtZXRob2Q6ICdQT1NUJyxcbiAgICAgICAgdXJsOiAkKHRoaXMpLmF0dHIoJ2FjdGlvbicpLFxuICAgICAgICBkYXRhVHlwZTogJ2pzb24nLFxuICAgICAgICBkYXRhOiAkKHRoaXMpLnNlcmlhbGl6ZSgpLFxuICAgICAgICBiZWZvcmVTZW5kOiAoKSA9PiB7XG4gICAgICAgICAgJChzZWxmLmFkZG9uc0xvZ2luQnV0dG9uU2VsZWN0b3IpLnNob3coKTtcbiAgICAgICAgICAkKCdidXR0b24uYnRuW3R5cGU9XCJzdWJtaXRcIl0nLCBzZWxmLmFkZG9uc0Nvbm5lY3RGb3JtKS5oaWRlKCk7XG4gICAgICAgIH0sXG4gICAgICB9KS5kb25lKChyZXNwb25zZSkgPT4ge1xuICAgICAgICBpZiAocmVzcG9uc2Uuc3VjY2VzcyA9PT0gMSkge1xuICAgICAgICAgIHdpbmRvdy5sb2NhdGlvbi5yZWxvYWQoKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAkLmdyb3dsLmVycm9yKHttZXNzYWdlOiByZXNwb25zZS5tZXNzYWdlfSk7XG4gICAgICAgICAgJChzZWxmLmFkZG9uc0xvZ2luQnV0dG9uU2VsZWN0b3IpLmhpZGUoKTtcbiAgICAgICAgICAkKCdidXR0b24uYnRuW3R5cGU9XCJzdWJtaXRcIl0nLCBzZWxmLmFkZG9uc0Nvbm5lY3RGb3JtKS5mYWRlSW4oKTtcbiAgICAgICAgfVxuICAgICAgfSk7XG4gICAgfSk7XG4gIH1cblxuICBpbml0QWRkTW9kdWxlQWN0aW9uKCkge1xuICAgIGNvbnN0IHNlbGYgPSB0aGlzO1xuICAgIGNvbnN0IGFkZE1vZHVsZUJ1dHRvbiA9ICQoc2VsZi5hZGRvbnNJbXBvcnRNb2RhbEJ0blNlbGVjdG9yKTtcbiAgICBhZGRNb2R1bGVCdXR0b24uYXR0cignZGF0YS10b2dnbGUnLCAnbW9kYWwnKTtcbiAgICBhZGRNb2R1bGVCdXR0b24uYXR0cignZGF0YS10YXJnZXQnLCBzZWxmLmRyb3Bab25lTW9kYWxTZWxlY3Rvcik7XG4gIH1cblxuICBpbml0RHJvcHpvbmUoKSB7XG4gICAgY29uc3Qgc2VsZiA9IHRoaXM7XG4gICAgY29uc3QgYm9keSA9ICQoJ2JvZHknKTtcbiAgICBjb25zdCBkcm9wem9uZSA9ICQoJy5kcm9wem9uZScpO1xuXG4gICAgLy8gUmVzZXQgbW9kYWwgd2hlbiBjbGljayBvbiBSZXRyeSBpbiBjYXNlIG9mIGZhaWx1cmVcbiAgICBib2R5Lm9uKCdjbGljaycsIHRoaXMubW9kdWxlSW1wb3J0RmFpbHVyZVJldHJ5U2VsZWN0b3IsICgpID0+IHtcbiAgICAgIC8qIGVzbGludC1kaXNhYmxlIG1heC1sZW4gKi9cbiAgICAgICQoXG4gICAgICAgIGAke3NlbGYubW9kdWxlSW1wb3J0U3VjY2Vzc1NlbGVjdG9yfSwke3NlbGYubW9kdWxlSW1wb3J0RmFpbHVyZVNlbGVjdG9yfSwke3NlbGYubW9kdWxlSW1wb3J0UHJvY2Vzc2luZ1NlbGVjdG9yfWAsXG4gICAgICApLmZhZGVPdXQoKCkgPT4ge1xuICAgICAgICAvKipcbiAgICAgICAgICogQWRkZWQgdGltZW91dCBmb3IgYSBiZXR0ZXIgcmVuZGVyIG9mIGFuaW1hdGlvblxuICAgICAgICAgKiBhbmQgYXZvaWQgdG8gaGF2ZSBkaXNwbGF5ZWQgYXQgdGhlIHNhbWUgdGltZVxuICAgICAgICAgKi9cbiAgICAgICAgc2V0VGltZW91dCgoKSA9PiB7XG4gICAgICAgICAgJChzZWxmLm1vZHVsZUltcG9ydFN0YXJ0U2VsZWN0b3IpLmZhZGVJbigoKSA9PiB7XG4gICAgICAgICAgICAkKHNlbGYubW9kdWxlSW1wb3J0RmFpbHVyZU1zZ0RldGFpbHNTZWxlY3RvcikuaGlkZSgpO1xuICAgICAgICAgICAgJChzZWxmLm1vZHVsZUltcG9ydFN1Y2Nlc3NDb25maWd1cmVCdG5TZWxlY3RvcikuaGlkZSgpO1xuICAgICAgICAgICAgZHJvcHpvbmUucmVtb3ZlQXR0cignc3R5bGUnKTtcbiAgICAgICAgICB9KTtcbiAgICAgICAgfSwgNTUwKTtcbiAgICAgIH0pO1xuICAgICAgLyogZXNsaW50LWVuYWJsZSBtYXgtbGVuICovXG4gICAgfSk7XG5cbiAgICAvLyBSZWluaXQgbW9kYWwgb24gZXhpdCwgYnV0IGNoZWNrIGlmIG5vdCBhbHJlYWR5IHByb2Nlc3Npbmcgc29tZXRoaW5nXG4gICAgYm9keS5vbignaGlkZGVuLmJzLm1vZGFsJywgdGhpcy5kcm9wWm9uZU1vZGFsU2VsZWN0b3IsICgpID0+IHtcbiAgICAgICQoYCR7c2VsZi5tb2R1bGVJbXBvcnRTdWNjZXNzU2VsZWN0b3J9LCAke3NlbGYubW9kdWxlSW1wb3J0RmFpbHVyZVNlbGVjdG9yfWApLmhpZGUoKTtcbiAgICAgICQoc2VsZi5tb2R1bGVJbXBvcnRTdGFydFNlbGVjdG9yKS5zaG93KCk7XG5cbiAgICAgIGRyb3B6b25lLnJlbW92ZUF0dHIoJ3N0eWxlJyk7XG4gICAgICAkKHNlbGYubW9kdWxlSW1wb3J0RmFpbHVyZU1zZ0RldGFpbHNTZWxlY3RvcikuaGlkZSgpO1xuICAgICAgJChzZWxmLm1vZHVsZUltcG9ydFN1Y2Nlc3NDb25maWd1cmVCdG5TZWxlY3RvcikuaGlkZSgpO1xuICAgICAgJChzZWxmLmRyb3Bab25lTW9kYWxGb290ZXJTZWxlY3RvcikuaHRtbCgnJyk7XG4gICAgICAkKHNlbGYubW9kdWxlSW1wb3J0Q29uZmlybVNlbGVjdG9yKS5oaWRlKCk7XG4gICAgfSk7XG5cbiAgICAvLyBDaGFuZ2UgdGhlIHdheSBEcm9wem9uZS5qcyBsaWIgaGFuZGxlIGZpbGUgaW5wdXQgdHJpZ2dlclxuICAgIGJvZHkub24oXG4gICAgICAnY2xpY2snLFxuICAgICAgYC5kcm9wem9uZTpub3QoJHt0aGlzLm1vZHVsZUltcG9ydFNlbGVjdEZpbGVNYW51YWxTZWxlY3Rvcn0sICR7dGhpcy5tb2R1bGVJbXBvcnRTdWNjZXNzQ29uZmlndXJlQnRuU2VsZWN0b3J9KWAsXG4gICAgICAoZXZlbnQsIG1hbnVhbFNlbGVjdCkgPT4ge1xuICAgICAgICAvLyBpZiBjbGljayBjb21lcyBmcm9tIC5tb2R1bGUtaW1wb3J0LXN0YXJ0LXNlbGVjdC1tYW51YWwsIHN0b3AgZXZlcnl0aGluZ1xuICAgICAgICBpZiAodHlwZW9mIG1hbnVhbFNlbGVjdCA9PT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgICAgICBldmVudC5zdG9wUHJvcGFnYXRpb24oKTtcbiAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICB9XG4gICAgICB9LFxuICAgICk7XG5cbiAgICBib2R5Lm9uKCdjbGljaycsIHRoaXMubW9kdWxlSW1wb3J0U2VsZWN0RmlsZU1hbnVhbFNlbGVjdG9yLCAoZXZlbnQpID0+IHtcbiAgICAgIGV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO1xuICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgIC8qKlxuICAgICAgICogVHJpZ2dlciBjbGljayBvbiBoaWRkZW4gZmlsZSBpbnB1dCwgYW5kIHBhc3MgZXh0cmEgZGF0YVxuICAgICAgICogdG8gLmRyb3B6b25lIGNsaWNrIGhhbmRsZXIgZnJvIGl0IHRvIG5vdGljZSBpdCBjb21lcyBmcm9tIGhlcmVcbiAgICAgICAqL1xuICAgICAgJCgnLmR6LWhpZGRlbi1pbnB1dCcpLnRyaWdnZXIoJ2NsaWNrJywgWydtYW51YWxfc2VsZWN0J10pO1xuICAgIH0pO1xuXG4gICAgLy8gSGFuZGxlIG1vZGFsIGNsb3N1cmVcbiAgICBib2R5Lm9uKCdjbGljaycsIHRoaXMubW9kdWxlSW1wb3J0TW9kYWxDbG9zZUJ0biwgKCkgPT4ge1xuICAgICAgaWYgKHNlbGYuaXNVcGxvYWRTdGFydGVkICE9PSB0cnVlKSB7XG4gICAgICAgICQoc2VsZi5kcm9wWm9uZU1vZGFsU2VsZWN0b3IpLm1vZGFsKCdoaWRlJyk7XG4gICAgICB9XG4gICAgfSk7XG5cbiAgICAvLyBGaXggaXNzdWUgb24gY2xpY2sgY29uZmlndXJlIGJ1dHRvblxuICAgIGJvZHkub24oJ2NsaWNrJywgdGhpcy5tb2R1bGVJbXBvcnRTdWNjZXNzQ29uZmlndXJlQnRuU2VsZWN0b3IsIGZ1bmN0aW9uIGluaXRpYWxpemVCb2R5Q2xpY2tPbk1vZHVsZUltcG9ydChldmVudCkge1xuICAgICAgZXZlbnQuc3RvcFByb3BhZ2F0aW9uKCk7XG4gICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgd2luZG93LmxvY2F0aW9uID0gJCh0aGlzKS5hdHRyKCdocmVmJyk7XG4gICAgfSk7XG5cbiAgICAvLyBPcGVuIGZhaWx1cmUgbWVzc2FnZSBkZXRhaWxzIGJveFxuICAgIGJvZHkub24oJ2NsaWNrJywgdGhpcy5tb2R1bGVJbXBvcnRGYWlsdXJlRGV0YWlsc0J0blNlbGVjdG9yLCAoKSA9PiB7XG4gICAgICAkKHNlbGYubW9kdWxlSW1wb3J0RmFpbHVyZU1zZ0RldGFpbHNTZWxlY3Rvcikuc2xpZGVEb3duKCk7XG4gICAgfSk7XG5cbiAgICAvLyBAc2VlOiBkcm9wem9uZS5qc1xuICAgIGNvbnN0IGRyb3B6b25lT3B0aW9ucyA9IHtcbiAgICAgIHVybDogd2luZG93Lm1vZHVsZVVSTHMubW9kdWxlSW1wb3J0LFxuICAgICAgYWNjZXB0ZWRGaWxlczogJy56aXAsIC50YXInLFxuICAgICAgLy8gVGhlIG5hbWUgdGhhdCB3aWxsIGJlIHVzZWQgdG8gdHJhbnNmZXIgdGhlIGZpbGVcbiAgICAgIHBhcmFtTmFtZTogJ2ZpbGVfdXBsb2FkZWQnLFxuICAgICAgbWF4RmlsZXNpemU6IDUwLCAvLyBjYW4ndCBiZSBncmVhdGVyIHRoYW4gNTBNYiBiZWNhdXNlIGl0J3MgYW4gYWRkb25zIGxpbWl0YXRpb25cbiAgICAgIHVwbG9hZE11bHRpcGxlOiBmYWxzZSxcbiAgICAgIGFkZFJlbW92ZUxpbmtzOiB0cnVlLFxuICAgICAgZGljdERlZmF1bHRNZXNzYWdlOiAnJyxcbiAgICAgIGhpZGRlbklucHV0Q29udGFpbmVyOiBzZWxmLmRyb3Bab25lSW1wb3J0Wm9uZVNlbGVjdG9yLFxuICAgICAgLyoqXG4gICAgICAgKiBBZGQgdW5saW1pdGVkIHRpbWVvdXQuIE90aGVyd2lzZSBkcm9wem9uZSB0aW1lb3V0IGlzIDMwIHNlY29uZHNcbiAgICAgICAqICBhbmQgaWYgYSBtb2R1bGUgaXMgbG9uZyB0byBpbnN0YWxsLCBpdCBpcyBub3QgcG9zc2libGUgdG8gaW5zdGFsbCB0aGUgbW9kdWxlLlxuICAgICAgICovXG4gICAgICB0aW1lb3V0OiAwLFxuICAgICAgYWRkZWRmaWxlOiAoKSA9PiB7XG4gICAgICAgIHNlbGYuYW5pbWF0ZVN0YXJ0VXBsb2FkKCk7XG4gICAgICB9LFxuICAgICAgcHJvY2Vzc2luZzogKCkgPT4ge1xuICAgICAgICAvLyBMZWF2ZSBpdCBlbXB0eSBzaW5jZSB3ZSBkb24ndCByZXF1aXJlIGFueXRoaW5nIHdoaWxlIHByb2Nlc3NpbmcgdXBsb2FkXG4gICAgICB9LFxuICAgICAgZXJyb3I6IChmaWxlLCBtZXNzYWdlKSA9PiB7XG4gICAgICAgIHNlbGYuZGlzcGxheU9uVXBsb2FkRXJyb3IobWVzc2FnZSk7XG4gICAgICB9LFxuICAgICAgY29tcGxldGU6IChmaWxlKSA9PiB7XG4gICAgICAgIGlmIChmaWxlLnN0YXR1cyAhPT0gJ2Vycm9yJykge1xuICAgICAgICAgIGNvbnN0IHJlc3BvbnNlT2JqZWN0ID0gJC5wYXJzZUpTT04oZmlsZS54aHIucmVzcG9uc2UpO1xuXG4gICAgICAgICAgaWYgKHR5cGVvZiByZXNwb25zZU9iamVjdC5pc19jb25maWd1cmFibGUgPT09ICd1bmRlZmluZWQnKSByZXNwb25zZU9iamVjdC5pc19jb25maWd1cmFibGUgPSBudWxsO1xuICAgICAgICAgIGlmICh0eXBlb2YgcmVzcG9uc2VPYmplY3QubW9kdWxlX25hbWUgPT09ICd1bmRlZmluZWQnKSByZXNwb25zZU9iamVjdC5tb2R1bGVfbmFtZSA9IG51bGw7XG5cbiAgICAgICAgICBzZWxmLmRpc3BsYXlPblVwbG9hZERvbmUocmVzcG9uc2VPYmplY3QpO1xuICAgICAgICB9XG4gICAgICAgIC8vIFN0YXRlIHRoYXQgd2UgaGF2ZSBmaW5pc2ggdGhlIHByb2Nlc3MgdG8gdW5sb2NrIHNvbWUgYWN0aW9uc1xuICAgICAgICBzZWxmLmlzVXBsb2FkU3RhcnRlZCA9IGZhbHNlO1xuICAgICAgfSxcbiAgICB9O1xuXG4gICAgZHJvcHpvbmUuZHJvcHpvbmUoJC5leHRlbmQoZHJvcHpvbmVPcHRpb25zKSk7XG4gIH1cblxuICBhbmltYXRlU3RhcnRVcGxvYWQoKSB7XG4gICAgY29uc3Qgc2VsZiA9IHRoaXM7XG4gICAgY29uc3QgZHJvcHpvbmUgPSAkKCcuZHJvcHpvbmUnKTtcbiAgICAvLyBTdGF0ZSB0aGF0IHdlIHN0YXJ0IG1vZHVsZSB1cGxvYWRcbiAgICBzZWxmLmlzVXBsb2FkU3RhcnRlZCA9IHRydWU7XG4gICAgJChzZWxmLm1vZHVsZUltcG9ydFN0YXJ0U2VsZWN0b3IpLmhpZGUoMCk7XG4gICAgZHJvcHpvbmUuY3NzKCdib3JkZXInLCAnbm9uZScpO1xuICAgICQoc2VsZi5tb2R1bGVJbXBvcnRQcm9jZXNzaW5nU2VsZWN0b3IpLmZhZGVJbigpO1xuICB9XG5cbiAgYW5pbWF0ZUVuZFVwbG9hZChjYWxsYmFjaykge1xuICAgIGNvbnN0IHNlbGYgPSB0aGlzO1xuICAgICQoc2VsZi5tb2R1bGVJbXBvcnRQcm9jZXNzaW5nU2VsZWN0b3IpXG4gICAgICAuZmluaXNoKClcbiAgICAgIC5mYWRlT3V0KGNhbGxiYWNrKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBNZXRob2QgdG8gY2FsbCBmb3IgdXBsb2FkIG1vZGFsLCB3aGVuIHRoZSBhamF4IGNhbGwgd2VudCB3ZWxsLlxuICAgKlxuICAgKiBAcGFyYW0gb2JqZWN0IHJlc3VsdCBjb250YWluaW5nIHRoZSBzZXJ2ZXIgcmVzcG9uc2VcbiAgICovXG4gIGRpc3BsYXlPblVwbG9hZERvbmUocmVzdWx0KSB7XG4gICAgY29uc3Qgc2VsZiA9IHRoaXM7XG4gICAgc2VsZi5hbmltYXRlRW5kVXBsb2FkKCgpID0+IHtcbiAgICAgIGlmIChyZXN1bHQuc3RhdHVzID09PSB0cnVlKSB7XG4gICAgICAgIGlmIChyZXN1bHQuaXNfY29uZmlndXJhYmxlID09PSB0cnVlKSB7XG4gICAgICAgICAgY29uc3QgY29uZmlndXJlTGluayA9IHdpbmRvdy5tb2R1bGVVUkxzLmNvbmZpZ3VyYXRpb25QYWdlLnJlcGxhY2UoLzpudW1iZXI6LywgcmVzdWx0Lm1vZHVsZV9uYW1lKTtcbiAgICAgICAgICAkKHNlbGYubW9kdWxlSW1wb3J0U3VjY2Vzc0NvbmZpZ3VyZUJ0blNlbGVjdG9yKS5hdHRyKCdocmVmJywgY29uZmlndXJlTGluayk7XG4gICAgICAgICAgJChzZWxmLm1vZHVsZUltcG9ydFN1Y2Nlc3NDb25maWd1cmVCdG5TZWxlY3Rvcikuc2hvdygpO1xuICAgICAgICB9XG4gICAgICAgICQoc2VsZi5tb2R1bGVJbXBvcnRTdWNjZXNzU2VsZWN0b3IpLmZhZGVJbigpO1xuICAgICAgfSBlbHNlIGlmICh0eXBlb2YgcmVzdWx0LmNvbmZpcm1hdGlvbl9zdWJqZWN0ICE9PSAndW5kZWZpbmVkJykge1xuICAgICAgICBzZWxmLmRpc3BsYXlQcmVzdGFUcnVzdFN0ZXAocmVzdWx0KTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgICQoc2VsZi5tb2R1bGVJbXBvcnRGYWlsdXJlTXNnRGV0YWlsc1NlbGVjdG9yKS5odG1sKHJlc3VsdC5tc2cpO1xuICAgICAgICAkKHNlbGYubW9kdWxlSW1wb3J0RmFpbHVyZVNlbGVjdG9yKS5mYWRlSW4oKTtcbiAgICAgIH1cbiAgICB9KTtcbiAgfVxuXG4gIC8qKlxuICAgKiBNZXRob2QgdG8gY2FsbCBmb3IgdXBsb2FkIG1vZGFsLCB3aGVuIHRoZSBhamF4IGNhbGwgd2VudCB3cm9uZyBvciB3aGVuIHRoZSBhY3Rpb24gcmVxdWVzdGVkIGNvdWxkIG5vdFxuICAgKiBzdWNjZWVkIGZvciBzb21lIHJlYXNvbi5cbiAgICpcbiAgICogQHBhcmFtIHN0cmluZyBtZXNzYWdlIGV4cGxhaW5pbmcgdGhlIGVycm9yLlxuICAgKi9cbiAgZGlzcGxheU9uVXBsb2FkRXJyb3IobWVzc2FnZSkge1xuICAgIGNvbnN0IHNlbGYgPSB0aGlzO1xuICAgIHNlbGYuYW5pbWF0ZUVuZFVwbG9hZCgoKSA9PiB7XG4gICAgICAkKHNlbGYubW9kdWxlSW1wb3J0RmFpbHVyZU1zZ0RldGFpbHNTZWxlY3RvcikuaHRtbChtZXNzYWdlKTtcbiAgICAgICQoc2VsZi5tb2R1bGVJbXBvcnRGYWlsdXJlU2VsZWN0b3IpLmZhZGVJbigpO1xuICAgIH0pO1xuICB9XG5cbiAgLyoqXG4gICAqIElmIFByZXN0YVRydXN0IG5lZWRzIHRvIGJlIGNvbmZpcm1lZCwgd2UgYXNrIGZvciB0aGUgY29uZmlybWF0aW9uXG4gICAqIG1vZGFsIGNvbnRlbnQgYW5kIHdlIGRpc3BsYXkgaXQgaW4gdGhlIGN1cnJlbnRseSBkaXNwbGF5ZWQgb25lLlxuICAgKiBXZSBhbHNvIGdlbmVyYXRlIHRoZSBhamF4IGNhbGwgdG8gdHJpZ2dlciBvbmNlIHdlIGNvbmZpcm0gd2Ugd2FudCB0byBpbnN0YWxsXG4gICAqIHRoZSBtb2R1bGUuXG4gICAqXG4gICAqIEBwYXJhbSBQcmV2aW91cyBzZXJ2ZXIgcmVzcG9uc2UgcmVzdWx0XG4gICAqL1xuICBkaXNwbGF5UHJlc3RhVHJ1c3RTdGVwKHJlc3VsdCkge1xuICAgIGNvbnN0IHNlbGYgPSB0aGlzO1xuICAgIGNvbnN0IG1vZGFsID0gc2VsZi5tb2R1bGVDYXJkQ29udHJvbGxlci5yZXBsYWNlUHJlc3RhVHJ1c3RQbGFjZWhvbGRlcnMocmVzdWx0KTtcbiAgICBjb25zdCBtb2R1bGVOYW1lID0gcmVzdWx0Lm1vZHVsZS5hdHRyaWJ1dGVzLm5hbWU7XG5cbiAgICAkKHRoaXMubW9kdWxlSW1wb3J0Q29uZmlybVNlbGVjdG9yKVxuICAgICAgLmh0bWwobW9kYWwuZmluZCgnLm1vZGFsLWJvZHknKS5odG1sKCkpXG4gICAgICAuZmFkZUluKCk7XG4gICAgJCh0aGlzLmRyb3Bab25lTW9kYWxGb290ZXJTZWxlY3RvcilcbiAgICAgIC5odG1sKG1vZGFsLmZpbmQoJy5tb2RhbC1mb290ZXInKS5odG1sKCkpXG4gICAgICAuZmFkZUluKCk7XG5cbiAgICAkKHRoaXMuZHJvcFpvbmVNb2RhbEZvb3RlclNlbGVjdG9yKVxuICAgICAgLmZpbmQoJy5wc3RydXN0LWluc3RhbGwnKVxuICAgICAgLm9mZignY2xpY2snKVxuICAgICAgLm9uKCdjbGljaycsICgpID0+IHtcbiAgICAgICAgJChzZWxmLm1vZHVsZUltcG9ydENvbmZpcm1TZWxlY3RvcikuaGlkZSgpO1xuICAgICAgICAkKHNlbGYuZHJvcFpvbmVNb2RhbEZvb3RlclNlbGVjdG9yKS5odG1sKCcnKTtcbiAgICAgICAgc2VsZi5hbmltYXRlU3RhcnRVcGxvYWQoKTtcblxuICAgICAgICAvLyBJbnN0YWxsIGFqYXggY2FsbFxuICAgICAgICAkLnBvc3QocmVzdWx0Lm1vZHVsZS5hdHRyaWJ1dGVzLnVybHMuaW5zdGFsbCwge1xuICAgICAgICAgICdhY3Rpb25QYXJhbXNbY29uZmlybVByZXN0YVRydXN0XSc6ICcxJyxcbiAgICAgICAgfSlcbiAgICAgICAgICAuZG9uZSgoZGF0YSkgPT4ge1xuICAgICAgICAgICAgc2VsZi5kaXNwbGF5T25VcGxvYWREb25lKGRhdGFbbW9kdWxlTmFtZV0pO1xuICAgICAgICAgIH0pXG4gICAgICAgICAgLmZhaWwoKGRhdGEpID0+IHtcbiAgICAgICAgICAgIHNlbGYuZGlzcGxheU9uVXBsb2FkRXJyb3IoZGF0YVttb2R1bGVOYW1lXSk7XG4gICAgICAgICAgfSlcbiAgICAgICAgICAuYWx3YXlzKCgpID0+IHtcbiAgICAgICAgICAgIHNlbGYuaXNVcGxvYWRTdGFydGVkID0gZmFsc2U7XG4gICAgICAgICAgfSk7XG4gICAgICB9KTtcbiAgfVxuXG4gIGdldEJ1bGtDaGVja2JveGVzU2VsZWN0b3IoKSB7XG4gICAgcmV0dXJuIHRoaXMuY3VycmVudERpc3BsYXkgPT09IHRoaXMuRElTUExBWV9HUklEXG4gICAgICA/IHRoaXMuYnVsa0FjdGlvbkNoZWNrYm94R3JpZFNlbGVjdG9yXG4gICAgICA6IHRoaXMuYnVsa0FjdGlvbkNoZWNrYm94TGlzdFNlbGVjdG9yO1xuICB9XG5cbiAgZ2V0QnVsa0NoZWNrYm94ZXNDaGVja2VkU2VsZWN0b3IoKSB7XG4gICAgcmV0dXJuIHRoaXMuY3VycmVudERpc3BsYXkgPT09IHRoaXMuRElTUExBWV9HUklEXG4gICAgICA/IHRoaXMuY2hlY2tlZEJ1bGtBY3Rpb25HcmlkU2VsZWN0b3JcbiAgICAgIDogdGhpcy5jaGVja2VkQnVsa0FjdGlvbkxpc3RTZWxlY3RvcjtcbiAgfVxuXG4gIGdldE1vZHVsZUl0ZW1TZWxlY3RvcigpIHtcbiAgICByZXR1cm4gdGhpcy5jdXJyZW50RGlzcGxheSA9PT0gdGhpcy5ESVNQTEFZX0dSSUQgPyB0aGlzLm1vZHVsZUl0ZW1HcmlkU2VsZWN0b3IgOiB0aGlzLm1vZHVsZUl0ZW1MaXN0U2VsZWN0b3I7XG4gIH1cblxuICAvKipcbiAgICogR2V0IHRoZSBtb2R1bGUgbm90aWZpY2F0aW9ucyBjb3VudCBhbmQgZGlzcGxheXMgaXQgYXMgYSBiYWRnZSBvbiB0aGUgbm90aWZpY2F0aW9uIHRhYlxuICAgKiBAcmV0dXJuIHZvaWRcbiAgICovXG4gIGdldE5vdGlmaWNhdGlvbnNDb3VudCgpIHtcbiAgICBjb25zdCBzZWxmID0gdGhpcztcbiAgICAkLmdldEpTT04od2luZG93Lm1vZHVsZVVSTHMubm90aWZpY2F0aW9uc0NvdW50LCBzZWxmLnVwZGF0ZU5vdGlmaWNhdGlvbnNDb3VudCkuZmFpbCgoKSA9PiB7XG4gICAgICBjb25zb2xlLmVycm9yKCdDb3VsZCBub3QgcmV0cmlldmUgbW9kdWxlIG5vdGlmaWNhdGlvbnMgY291bnQuJyk7XG4gICAgfSk7XG4gIH1cblxuICB1cGRhdGVOb3RpZmljYXRpb25zQ291bnQoYmFkZ2UpIHtcbiAgICBjb25zdCBkZXN0aW5hdGlvblRhYnMgPSB7XG4gICAgICB0b19jb25maWd1cmU6ICQoJyNzdWJ0YWItQWRtaW5Nb2R1bGVzTm90aWZpY2F0aW9ucycpLFxuICAgICAgdG9fdXBkYXRlOiAkKCcjc3VidGFiLUFkbWluTW9kdWxlc1VwZGF0ZXMnKSxcbiAgICB9O1xuXG4gICAgT2JqZWN0LmtleXMoZGVzdGluYXRpb25UYWJzKS5mb3JFYWNoKChkZXN0aW5hdGlvbktleSkgPT4ge1xuICAgICAgaWYgKGRlc3RpbmF0aW9uVGFic1tkZXN0aW5hdGlvbktleV0ubGVuZ3RoICE9PSAwKSB7XG4gICAgICAgIGRlc3RpbmF0aW9uVGFic1tkZXN0aW5hdGlvbktleV0uZmluZCgnLm5vdGlmaWNhdGlvbi1jb3VudGVyJykudGV4dChiYWRnZVtkZXN0aW5hdGlvbktleV0pO1xuICAgICAgfVxuICAgIH0pO1xuICB9XG5cbiAgaW5pdEFkZG9uc1NlYXJjaCgpIHtcbiAgICBjb25zdCBzZWxmID0gdGhpcztcbiAgICAkKCdib2R5Jykub24oJ2NsaWNrJywgYCR7c2VsZi5hZGRvbkl0ZW1HcmlkU2VsZWN0b3J9LCAke3NlbGYuYWRkb25JdGVtTGlzdFNlbGVjdG9yfWAsICgpID0+IHtcbiAgICAgIGxldCBzZWFyY2hRdWVyeSA9ICcnO1xuXG4gICAgICBpZiAoc2VsZi5jdXJyZW50VGFnc0xpc3QubGVuZ3RoKSB7XG4gICAgICAgIHNlYXJjaFF1ZXJ5ID0gZW5jb2RlVVJJQ29tcG9uZW50KHNlbGYuY3VycmVudFRhZ3NMaXN0LmpvaW4oJyAnKSk7XG4gICAgICB9XG5cbiAgICAgIHdpbmRvdy5vcGVuKGAke3NlbGYuYmFzZUFkZG9uc1VybH1zZWFyY2gucGhwP3NlYXJjaF9xdWVyeT0ke3NlYXJjaFF1ZXJ5fWAsICdfYmxhbmsnKTtcbiAgICB9KTtcbiAgfVxuXG4gIGluaXRDYXRlZ29yaWVzR3JpZCgpIHtcbiAgICBjb25zdCBzZWxmID0gdGhpcztcblxuICAgICQoJ2JvZHknKS5vbignY2xpY2snLCB0aGlzLmNhdGVnb3J5R3JpZEl0ZW1TZWxlY3RvciwgZnVuY3Rpb24gaW5pdGlsYWl6ZUdyaWRCb2R5Q2xpY2soZXZlbnQpIHtcbiAgICAgIGV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO1xuICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgIGNvbnN0IHJlZkNhdGVnb3J5ID0gJCh0aGlzKS5kYXRhKCdjYXRlZ29yeS1yZWYnKTtcblxuICAgICAgLy8gSW4gY2FzZSB3ZSBoYXZlIHNvbWUgdGFncyB3ZSBuZWVkIHRvIHJlc2V0IGl0ICFcbiAgICAgIGlmIChzZWxmLmN1cnJlbnRUYWdzTGlzdC5sZW5ndGgpIHtcbiAgICAgICAgc2VsZi5wc3RhZ2dlcklucHV0LnJlc2V0VGFncyhmYWxzZSk7XG4gICAgICAgIHNlbGYuY3VycmVudFRhZ3NMaXN0ID0gW107XG4gICAgICB9XG4gICAgICBjb25zdCBtZW51Q2F0ZWdvcnlUb1RyaWdnZXIgPSAkKGAke3NlbGYuY2F0ZWdvcnlJdGVtU2VsZWN0b3J9W2RhdGEtY2F0ZWdvcnktcmVmPVwiJHtyZWZDYXRlZ29yeX1cIl1gKTtcblxuICAgICAgaWYgKCFtZW51Q2F0ZWdvcnlUb1RyaWdnZXIubGVuZ3RoKSB7XG4gICAgICAgIGNvbnNvbGUud2FybihgTm8gY2F0ZWdvcnkgd2l0aCByZWYgKCR7cmVmQ2F0ZWdvcnl9KSBzZWVtcyB0byBleGlzdCFgKTtcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgfVxuXG4gICAgICAvLyBIaWRlIGN1cnJlbnQgY2F0ZWdvcnkgZ3JpZFxuICAgICAgaWYgKHNlbGYuaXNDYXRlZ29yeUdyaWREaXNwbGF5ZWQgPT09IHRydWUpIHtcbiAgICAgICAgJChzZWxmLmNhdGVnb3J5R3JpZFNlbGVjdG9yKS5mYWRlT3V0KCk7XG4gICAgICAgIHNlbGYuaXNDYXRlZ29yeUdyaWREaXNwbGF5ZWQgPSBmYWxzZTtcbiAgICAgIH1cblxuICAgICAgLy8gVHJpZ2dlciBjbGljayBvbiByaWdodCBjYXRlZ29yeVxuICAgICAgJChgJHtzZWxmLmNhdGVnb3J5SXRlbVNlbGVjdG9yfVtkYXRhLWNhdGVnb3J5LXJlZj1cIiR7cmVmQ2F0ZWdvcnl9XCJdYCkuY2xpY2soKTtcbiAgICAgIHJldHVybiB0cnVlO1xuICAgIH0pO1xuICB9XG5cbiAgaW5pdEN1cnJlbnREaXNwbGF5KCkge1xuICAgIHRoaXMuY3VycmVudERpc3BsYXkgPSB0aGlzLmN1cnJlbnREaXNwbGF5ID09PSAnJyA/IHRoaXMuRElTUExBWV9MSVNUIDogdGhpcy5ESVNQTEFZX0dSSUQ7XG4gIH1cblxuICBpbml0U29ydGluZ0Ryb3Bkb3duKCkge1xuICAgIGNvbnN0IHNlbGYgPSB0aGlzO1xuXG4gICAgc2VsZi5jdXJyZW50U29ydGluZyA9ICQodGhpcy5tb2R1bGVTb3J0aW5nRHJvcGRvd25TZWxlY3RvcilcbiAgICAgIC5maW5kKCc6Y2hlY2tlZCcpXG4gICAgICAuYXR0cigndmFsdWUnKTtcbiAgICBpZiAoIXNlbGYuY3VycmVudFNvcnRpbmcpIHtcbiAgICAgIHNlbGYuY3VycmVudFNvcnRpbmcgPSAnYWNjZXNzLWRlc2MnO1xuICAgIH1cblxuICAgICQoJ2JvZHknKS5vbignY2hhbmdlJywgc2VsZi5tb2R1bGVTb3J0aW5nRHJvcGRvd25TZWxlY3RvciwgZnVuY3Rpb24gaW5pdGlhbGl6ZUJvZHlTb3J0aW5nQ2hhbmdlKCkge1xuICAgICAgc2VsZi5jdXJyZW50U29ydGluZyA9ICQodGhpcylcbiAgICAgICAgLmZpbmQoJzpjaGVja2VkJylcbiAgICAgICAgLmF0dHIoJ3ZhbHVlJyk7XG4gICAgICBzZWxmLnVwZGF0ZU1vZHVsZVZpc2liaWxpdHkoKTtcbiAgICB9KTtcbiAgfVxuXG4gIGRvQnVsa0FjdGlvbihyZXF1ZXN0ZWRCdWxrQWN0aW9uKSB7XG4gICAgLy8gVGhpcyBvYmplY3QgaXMgdXNlZCB0byBjaGVjayBpZiByZXF1ZXN0ZWQgYnVsa0FjdGlvbiBpcyBhdmFpbGFibGUgYW5kIGdpdmUgcHJvcGVyXG4gICAgLy8gdXJsIHNlZ21lbnQgdG8gYmUgY2FsbGVkIGZvciBpdFxuICAgIGNvbnN0IGZvcmNlRGVsZXRpb24gPSAkKCcjZm9yY2VfYnVsa19kZWxldGlvbicpLnByb3AoJ2NoZWNrZWQnKTtcblxuICAgIGNvbnN0IGJ1bGtBY3Rpb25Ub1VybCA9IHtcbiAgICAgICdidWxrLXVuaW5zdGFsbCc6ICd1bmluc3RhbGwnLFxuICAgICAgJ2J1bGstZGlzYWJsZSc6ICdkaXNhYmxlJyxcbiAgICAgICdidWxrLWVuYWJsZSc6ICdlbmFibGUnLFxuICAgICAgJ2J1bGstZGlzYWJsZS1tb2JpbGUnOiAnZGlzYWJsZV9tb2JpbGUnLFxuICAgICAgJ2J1bGstZW5hYmxlLW1vYmlsZSc6ICdlbmFibGVfbW9iaWxlJyxcbiAgICAgICdidWxrLXJlc2V0JzogJ3Jlc2V0JyxcbiAgICB9O1xuXG4gICAgLy8gTm90ZSBubyBncmlkIHNlbGVjdG9yIHVzZWQgeWV0IHNpbmNlIHdlIGRvIG5vdCBuZWVkZWQgaXQgYXQgZGV2IHRpbWVcbiAgICAvLyBNYXliZSB1c2VmdWwgdG8gaW1wbGVtZW50IHRoaXMga2luZCBvZiB0aGluZ3MgbGF0ZXIgaWYgaW50ZW5kZWQgdG9cbiAgICAvLyB1c2UgdGhpcyBmdW5jdGlvbmFsaXR5IGVsc2V3aGVyZSBidXQgXCJtYW5hZ2UgbXkgbW9kdWxlXCIgc2VjdGlvblxuICAgIGlmICh0eXBlb2YgYnVsa0FjdGlvblRvVXJsW3JlcXVlc3RlZEJ1bGtBY3Rpb25dID09PSAndW5kZWZpbmVkJykge1xuICAgICAgJC5ncm93bC5lcnJvcih7XG4gICAgICAgIG1lc3NhZ2U6IHdpbmRvdy50cmFuc2xhdGVfamF2YXNjcmlwdHNbJ0J1bGsgQWN0aW9uIC0gUmVxdWVzdCBub3QgZm91bmQnXS5yZXBsYWNlKCdbMV0nLCByZXF1ZXN0ZWRCdWxrQWN0aW9uKSxcbiAgICAgIH0pO1xuICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH1cblxuICAgIC8vIExvb3Agb3ZlciBhbGwgY2hlY2tlZCBidWxrIGNoZWNrYm94ZXNcbiAgICBjb25zdCBidWxrQWN0aW9uU2VsZWN0ZWRTZWxlY3RvciA9IHRoaXMuZ2V0QnVsa0NoZWNrYm94ZXNDaGVja2VkU2VsZWN0b3IoKTtcbiAgICBjb25zdCBidWxrTW9kdWxlQWN0aW9uID0gYnVsa0FjdGlvblRvVXJsW3JlcXVlc3RlZEJ1bGtBY3Rpb25dO1xuXG4gICAgaWYgKCQoYnVsa0FjdGlvblNlbGVjdGVkU2VsZWN0b3IpLmxlbmd0aCA8PSAwKSB7XG4gICAgICBjb25zb2xlLndhcm4od2luZG93LnRyYW5zbGF0ZV9qYXZhc2NyaXB0c1snQnVsayBBY3Rpb24gLSBPbmUgbW9kdWxlIG1pbmltdW0nXSk7XG4gICAgICByZXR1cm4gZmFsc2U7XG4gICAgfVxuXG4gICAgY29uc3QgbW9kdWxlc0FjdGlvbnMgPSBbXTtcbiAgICBsZXQgbW9kdWxlVGVjaE5hbWU7XG4gICAgJChidWxrQWN0aW9uU2VsZWN0ZWRTZWxlY3RvcikuZWFjaChmdW5jdGlvbiBidWxrQWN0aW9uU2VsZWN0b3IoKSB7XG4gICAgICBtb2R1bGVUZWNoTmFtZSA9ICQodGhpcykuZGF0YSgndGVjaC1uYW1lJyk7XG4gICAgICBtb2R1bGVzQWN0aW9ucy5wdXNoKHtcbiAgICAgICAgdGVjaE5hbWU6IG1vZHVsZVRlY2hOYW1lLFxuICAgICAgICBhY3Rpb25NZW51T2JqOiAkKHRoaXMpXG4gICAgICAgICAgLmNsb3Nlc3QoJy5tb2R1bGUtY2hlY2tib3gtYnVsay1saXN0JylcbiAgICAgICAgICAubmV4dCgpLFxuICAgICAgfSk7XG4gICAgfSk7XG5cbiAgICB0aGlzLnBlcmZvcm1Nb2R1bGVzQWN0aW9uKG1vZHVsZXNBY3Rpb25zLCBidWxrTW9kdWxlQWN0aW9uLCBmb3JjZURlbGV0aW9uKTtcblxuICAgIHJldHVybiB0cnVlO1xuICB9XG5cbiAgcGVyZm9ybU1vZHVsZXNBY3Rpb24obW9kdWxlc0FjdGlvbnMsIGJ1bGtNb2R1bGVBY3Rpb24sIGZvcmNlRGVsZXRpb24pIHtcbiAgICBjb25zdCBzZWxmID0gdGhpcztcblxuICAgIGlmICh0eXBlb2Ygc2VsZi5tb2R1bGVDYXJkQ29udHJvbGxlciA9PT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICAvLyBGaXJzdCBsZXQncyBmaWx0ZXIgbW9kdWxlcyB0aGF0IGNhbid0IHBlcmZvcm0gdGhpcyBhY3Rpb25cbiAgICBjb25zdCBhY3Rpb25NZW51TGlua3MgPSBmaWx0ZXJBbGxvd2VkQWN0aW9ucyhtb2R1bGVzQWN0aW9ucyk7XG5cbiAgICBpZiAoIWFjdGlvbk1lbnVMaW5rcy5sZW5ndGgpIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICBsZXQgbW9kdWxlc1JlcXVlc3RlZENvdW50ZG93biA9IGFjdGlvbk1lbnVMaW5rcy5sZW5ndGggLSAxO1xuICAgIGxldCBzcGlubmVyT2JqID0gJCgnPGJ1dHRvbiBjbGFzcz1cImJ0bi1wcmltYXJ5LXJldmVyc2Ugb25jbGljayB1bmJpbmQgc3Bpbm5lciBcIj48L2J1dHRvbj4nKTtcblxuICAgIGlmIChhY3Rpb25NZW51TGlua3MubGVuZ3RoID4gMSkge1xuICAgICAgLy8gTG9vcCB0aHJvdWdoIGFsbCB0aGUgbW9kdWxlcyBleGNlcHQgdGhlIGxhc3Qgb25lIHdoaWNoIHdhaXRzIGZvciBvdGhlclxuICAgICAgLy8gcmVxdWVzdHMgYW5kIHRoZW4gY2FsbCBpdHMgcmVxdWVzdCB3aXRoIGNhY2hlIGNsZWFyIGVuYWJsZWRcbiAgICAgICQuZWFjaChhY3Rpb25NZW51TGlua3MsIChpbmRleCwgYWN0aW9uTWVudUxpbmspID0+IHtcbiAgICAgICAgaWYgKGluZGV4ID49IGFjdGlvbk1lbnVMaW5rcy5sZW5ndGggLSAxKSB7XG4gICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG4gICAgICAgIHJlcXVlc3RNb2R1bGVBY3Rpb24oYWN0aW9uTWVudUxpbmssIHRydWUsIGNvdW50ZG93bk1vZHVsZXNSZXF1ZXN0KTtcbiAgICAgIH0pO1xuICAgICAgLy8gRGlzcGxheSBhIHNwaW5uZXIgZm9yIHRoZSBsYXN0IG1vZHVsZVxuICAgICAgY29uc3QgbGFzdE1lbnVMaW5rID0gYWN0aW9uTWVudUxpbmtzW2FjdGlvbk1lbnVMaW5rcy5sZW5ndGggLSAxXTtcbiAgICAgIGNvbnN0IGFjdGlvbk1lbnVPYmogPSBsYXN0TWVudUxpbmsuY2xvc2VzdChzZWxmLm1vZHVsZUNhcmRDb250cm9sbGVyLm1vZHVsZUl0ZW1BY3Rpb25zU2VsZWN0b3IpO1xuICAgICAgYWN0aW9uTWVudU9iai5oaWRlKCk7XG4gICAgICBhY3Rpb25NZW51T2JqLmFmdGVyKHNwaW5uZXJPYmopO1xuICAgIH0gZWxzZSB7XG4gICAgICByZXF1ZXN0TW9kdWxlQWN0aW9uKGFjdGlvbk1lbnVMaW5rc1swXSk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gcmVxdWVzdE1vZHVsZUFjdGlvbihhY3Rpb25NZW51TGluaywgZGlzYWJsZUNhY2hlQ2xlYXIsIHJlcXVlc3RFbmRDYWxsYmFjaykge1xuICAgICAgc2VsZi5tb2R1bGVDYXJkQ29udHJvbGxlci5yZXF1ZXN0VG9Db250cm9sbGVyKFxuICAgICAgICBidWxrTW9kdWxlQWN0aW9uLFxuICAgICAgICBhY3Rpb25NZW51TGluayxcbiAgICAgICAgZm9yY2VEZWxldGlvbixcbiAgICAgICAgZGlzYWJsZUNhY2hlQ2xlYXIsXG4gICAgICAgIHJlcXVlc3RFbmRDYWxsYmFjayxcbiAgICAgICk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gY291bnRkb3duTW9kdWxlc1JlcXVlc3QoKSB7XG4gICAgICBtb2R1bGVzUmVxdWVzdGVkQ291bnRkb3duIC09IDE7XG4gICAgICAvLyBOb3cgdGhhdCBhbGwgb3RoZXIgbW9kdWxlcyBoYXZlIHBlcmZvcm1lZCB0aGVpciBhY3Rpb24gV0lUSE9VVCBjYWNoZSBjbGVhciwgd2VcbiAgICAgIC8vIGNhbiByZXF1ZXN0IHRoZSBsYXN0IG1vZHVsZSByZXF1ZXN0IFdJVEggY2FjaGUgY2xlYXJcbiAgICAgIGlmIChtb2R1bGVzUmVxdWVzdGVkQ291bnRkb3duIDw9IDApIHtcbiAgICAgICAgaWYgKHNwaW5uZXJPYmopIHtcbiAgICAgICAgICBzcGlubmVyT2JqLnJlbW92ZSgpO1xuICAgICAgICAgIHNwaW5uZXJPYmogPSBudWxsO1xuICAgICAgICB9XG5cbiAgICAgICAgY29uc3QgbGFzdE1lbnVMaW5rID0gYWN0aW9uTWVudUxpbmtzW2FjdGlvbk1lbnVMaW5rcy5sZW5ndGggLSAxXTtcbiAgICAgICAgY29uc3QgYWN0aW9uTWVudU9iaiA9IGxhc3RNZW51TGluay5jbG9zZXN0KHNlbGYubW9kdWxlQ2FyZENvbnRyb2xsZXIubW9kdWxlSXRlbUFjdGlvbnNTZWxlY3Rvcik7XG4gICAgICAgIGFjdGlvbk1lbnVPYmouZmFkZUluKCk7XG4gICAgICAgIHJlcXVlc3RNb2R1bGVBY3Rpb24obGFzdE1lbnVMaW5rKTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICBmdW5jdGlvbiBmaWx0ZXJBbGxvd2VkQWN0aW9ucyhhY3Rpb25zKSB7XG4gICAgICBjb25zdCBtZW51TGlua3MgPSBbXTtcbiAgICAgIGxldCBhY3Rpb25NZW51TGluaztcbiAgICAgICQuZWFjaChhY3Rpb25zLCAoaW5kZXgsIG1vZHVsZURhdGEpID0+IHtcbiAgICAgICAgYWN0aW9uTWVudUxpbmsgPSAkKFxuICAgICAgICAgIHNlbGYubW9kdWxlQ2FyZENvbnRyb2xsZXIubW9kdWxlQWN0aW9uTWVudUxpbmtTZWxlY3RvciArIGJ1bGtNb2R1bGVBY3Rpb24sXG4gICAgICAgICAgbW9kdWxlRGF0YS5hY3Rpb25NZW51T2JqLFxuICAgICAgICApO1xuICAgICAgICBpZiAoYWN0aW9uTWVudUxpbmsubGVuZ3RoID4gMCkge1xuICAgICAgICAgIG1lbnVMaW5rcy5wdXNoKGFjdGlvbk1lbnVMaW5rKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAkLmdyb3dsLmVycm9yKHtcbiAgICAgICAgICAgIG1lc3NhZ2U6IHdpbmRvdy50cmFuc2xhdGVfamF2YXNjcmlwdHNbJ0J1bGsgQWN0aW9uIC0gUmVxdWVzdCBub3QgYXZhaWxhYmxlIGZvciBtb2R1bGUnXVxuICAgICAgICAgICAgICAucmVwbGFjZSgnWzFdJywgYnVsa01vZHVsZUFjdGlvbilcbiAgICAgICAgICAgICAgLnJlcGxhY2UoJ1syXScsIG1vZHVsZURhdGEudGVjaE5hbWUpLFxuICAgICAgICAgIH0pO1xuICAgICAgICB9XG4gICAgICB9KTtcblxuICAgICAgcmV0dXJuIG1lbnVMaW5rcztcbiAgICB9XG4gIH1cblxuICBpbml0QWN0aW9uQnV0dG9ucygpIHtcbiAgICBjb25zdCBzZWxmID0gdGhpcztcbiAgICAkKCdib2R5Jykub24oJ2NsaWNrJywgc2VsZi5tb2R1bGVJbnN0YWxsQnRuU2VsZWN0b3IsIGZ1bmN0aW9uIGluaXRpYWxpemVBY3Rpb25CdXR0b25zQ2xpY2soZXZlbnQpIHtcbiAgICAgIGNvbnN0ICR0aGlzID0gJCh0aGlzKTtcbiAgICAgIGNvbnN0ICRuZXh0ID0gJCgkdGhpcy5uZXh0KCkpO1xuICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcblxuICAgICAgJHRoaXMuaGlkZSgpO1xuICAgICAgJG5leHQuc2hvdygpO1xuXG4gICAgICAkLmFqYXgoe1xuICAgICAgICB1cmw6ICR0aGlzLmRhdGEoJ3VybCcpLFxuICAgICAgICBkYXRhVHlwZTogJ2pzb24nLFxuICAgICAgfSkuZG9uZSgoKSA9PiB7XG4gICAgICAgICRuZXh0LmZhZGVPdXQoKTtcbiAgICAgIH0pO1xuICAgIH0pO1xuXG4gICAgLy8gXCJVcGdyYWRlIEFsbFwiIGJ1dHRvbiBoYW5kbGVyXG4gICAgJCgnYm9keScpLm9uKCdjbGljaycsIHNlbGYudXBncmFkZUFsbFNvdXJjZSwgKGV2ZW50KSA9PiB7XG4gICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgY29uc3QgaXNNYWludGVuYW5jZU1vZGUgPSB3aW5kb3cuaXNTaG9wTWFpbnRlbmFuY2U7XG5cbiAgICAgIC8vIE1vZGFsIGJvZHkgZWxlbWVudFxuICAgICAgY29uc3QgbWFpbnRlbmFuY2VMaW5rID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnYScpO1xuICAgICAgbWFpbnRlbmFuY2VMaW5rLmNsYXNzTGlzdC5hZGQoJ2J0bicsICdidG4tcHJpbWFyeScsICdidG4tbGcnKTtcbiAgICAgIG1haW50ZW5hbmNlTGluay5zZXRBdHRyaWJ1dGUoJ2hyZWYnLCB3aW5kb3cubW9kdWxlVVJMcy5tYWludGVuYW5jZVBhZ2UpO1xuICAgICAgbWFpbnRlbmFuY2VMaW5rLmlubmVySFRNTCA9IHdpbmRvdy5tb2R1bGVUcmFuc2xhdGlvbnMubW9kdWxlTW9kYWxVcGRhdGVNYWludGVuYW5jZTtcblxuICAgICAgY29uc3QgdXBkYXRlQWxsQ29uZmlybU1vZGFsID0gbmV3IENvbmZpcm1Nb2RhbChcbiAgICAgICAge1xuICAgICAgICAgIGlkOiAnY29uZmlybS1tb2R1bGUtdXBkYXRlLW1vZGFsJyxcbiAgICAgICAgICBjb25maXJtVGl0bGU6IHdpbmRvdy5tb2R1bGVUcmFuc2xhdGlvbnMuc2luZ2xlTW9kdWxlTW9kYWxVcGRhdGVUaXRsZSxcbiAgICAgICAgICBjbG9zZUJ1dHRvbkxhYmVsOiB3aW5kb3cubW9kdWxlVHJhbnNsYXRpb25zLm1vZHVsZU1vZGFsVXBkYXRlQ2FuY2VsLFxuICAgICAgICAgIGNvbmZpcm1CdXR0b25MYWJlbDogaXNNYWludGVuYW5jZU1vZGVcbiAgICAgICAgICAgID8gd2luZG93Lm1vZHVsZVRyYW5zbGF0aW9ucy5tb2R1bGVNb2RhbFVwZGF0ZVVwZ3JhZGVcbiAgICAgICAgICAgIDogd2luZG93Lm1vZHVsZVRyYW5zbGF0aW9ucy51cGdyYWRlQW55d2F5QnV0dG9uVGV4dCxcbiAgICAgICAgICBjb25maXJtQnV0dG9uQ2xhc3M6IGlzTWFpbnRlbmFuY2VNb2RlID8gJ2J0bi1wcmltYXJ5JyA6ICdidG4tc2Vjb25kYXJ5JyxcbiAgICAgICAgICBjb25maXJtTWVzc2FnZTogaXNNYWludGVuYW5jZU1vZGUgPyAnJyA6IHdpbmRvdy5tb2R1bGVUcmFuc2xhdGlvbnMubW9kdWxlTW9kYWxVcGRhdGVDb25maXJtTWVzc2FnZSxcbiAgICAgICAgICBjbG9zYWJsZTogdHJ1ZSxcbiAgICAgICAgICBjdXN0b21CdXR0b25zOiBpc01haW50ZW5hbmNlTW9kZSA/IFtdIDogW21haW50ZW5hbmNlTGlua10sXG4gICAgICAgIH0sXG4gICAgICAgICgpID0+IHtcbiAgICAgICAgICBpZiAoJChzZWxmLnVwZ3JhZGVBbGxUYXJnZXRzKS5sZW5ndGggPD0gMCkge1xuICAgICAgICAgICAgY29uc29sZS53YXJuKHdpbmRvdy50cmFuc2xhdGVfamF2YXNjcmlwdHNbJ1VwZ3JhZGUgQWxsIEFjdGlvbiAtIE9uZSBtb2R1bGUgbWluaW11bSddKTtcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICB9XG5cbiAgICAgICAgICBjb25zdCBtb2R1bGVzQWN0aW9ucyA9IFtdO1xuICAgICAgICAgIGxldCBtb2R1bGVUZWNoTmFtZTtcbiAgICAgICAgICAkKHNlbGYudXBncmFkZUFsbFRhcmdldHMpLmVhY2goZnVuY3Rpb24gYnVsa0FjdGlvblNlbGVjdG9yKCkge1xuICAgICAgICAgICAgY29uc3QgbW9kdWxlSXRlbUxpc3QgPSAkKHRoaXMpLmNsb3Nlc3QoJy5tb2R1bGUtaXRlbS1saXN0Jyk7XG4gICAgICAgICAgICBtb2R1bGVUZWNoTmFtZSA9IG1vZHVsZUl0ZW1MaXN0LmRhdGEoJ3RlY2gtbmFtZScpO1xuICAgICAgICAgICAgbW9kdWxlc0FjdGlvbnMucHVzaCh7XG4gICAgICAgICAgICAgIHRlY2hOYW1lOiBtb2R1bGVUZWNoTmFtZSxcbiAgICAgICAgICAgICAgYWN0aW9uTWVudU9iajogJCgnLm1vZHVsZS1hY3Rpb25zJywgbW9kdWxlSXRlbUxpc3QpLFxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgfSk7XG5cbiAgICAgICAgICB0aGlzLnBlcmZvcm1Nb2R1bGVzQWN0aW9uKG1vZHVsZXNBY3Rpb25zLCAndXBncmFkZScpO1xuXG4gICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgIH0sXG4gICAgICApO1xuXG4gICAgICB1cGRhdGVBbGxDb25maXJtTW9kYWwuc2hvdygpO1xuXG4gICAgICByZXR1cm4gdHJ1ZTtcbiAgICB9KTtcbiAgfVxuXG4gIGluaXRDYXRlZ29yeVNlbGVjdCgpIHtcbiAgICBjb25zdCBzZWxmID0gdGhpcztcbiAgICBjb25zdCBib2R5ID0gJCgnYm9keScpO1xuICAgIGJvZHkub24oJ2NsaWNrJywgc2VsZi5jYXRlZ29yeUl0ZW1TZWxlY3RvciwgZnVuY3Rpb24gaW5pdGlhbGl6ZUNhdGVnb3J5U2VsZWN0Q2xpY2soKSB7XG4gICAgICAvLyBHZXQgZGF0YSBmcm9tIGxpIERPTSBpbnB1dFxuICAgICAgc2VsZi5jdXJyZW50UmVmQ2F0ZWdvcnkgPSAkKHRoaXMpLmRhdGEoJ2NhdGVnb3J5LXJlZicpO1xuICAgICAgc2VsZi5jdXJyZW50UmVmQ2F0ZWdvcnkgPSBzZWxmLmN1cnJlbnRSZWZDYXRlZ29yeSA/IFN0cmluZyhzZWxmLmN1cnJlbnRSZWZDYXRlZ29yeSkudG9Mb3dlckNhc2UoKSA6IG51bGw7XG4gICAgICAvLyBDaGFuZ2UgZHJvcGRvd24gbGFiZWwgdG8gc2V0IGl0IHRvIHRoZSBjdXJyZW50IGNhdGVnb3J5J3MgZGlzcGxheW5hbWVcbiAgICAgICQoc2VsZi5jYXRlZ29yeVNlbGVjdG9yTGFiZWxTZWxlY3RvcikudGV4dCgkKHRoaXMpLmRhdGEoJ2NhdGVnb3J5LWRpc3BsYXktbmFtZScpKTtcbiAgICAgICQoc2VsZi5jYXRlZ29yeVJlc2V0QnRuU2VsZWN0b3IpLnNob3coKTtcbiAgICAgIHNlbGYudXBkYXRlTW9kdWxlVmlzaWJpbGl0eSgpO1xuICAgIH0pO1xuXG4gICAgYm9keS5vbignY2xpY2snLCBzZWxmLmNhdGVnb3J5UmVzZXRCdG5TZWxlY3RvciwgZnVuY3Rpb24gaW5pdGlhbGl6ZUNhdGVnb3J5UmVzZXRCdXR0b25DbGljaygpIHtcbiAgICAgIGNvbnN0IHJhd1RleHQgPSAkKHNlbGYuY2F0ZWdvcnlTZWxlY3RvcikuYXR0cignYXJpYS1sYWJlbGxlZGJ5Jyk7XG4gICAgICBjb25zdCB1cHBlckZpcnN0TGV0dGVyID0gcmF3VGV4dC5jaGFyQXQoMCkudG9VcHBlckNhc2UoKTtcbiAgICAgIGNvbnN0IHJlbW92ZWRGaXJzdExldHRlciA9IHJhd1RleHQuc2xpY2UoMSk7XG4gICAgICBjb25zdCBvcmlnaW5hbFRleHQgPSB1cHBlckZpcnN0TGV0dGVyICsgcmVtb3ZlZEZpcnN0TGV0dGVyO1xuXG4gICAgICAkKHNlbGYuY2F0ZWdvcnlTZWxlY3RvckxhYmVsU2VsZWN0b3IpLnRleHQob3JpZ2luYWxUZXh0KTtcbiAgICAgICQodGhpcykuaGlkZSgpO1xuICAgICAgc2VsZi5jdXJyZW50UmVmQ2F0ZWdvcnkgPSBudWxsO1xuICAgICAgc2VsZi51cGRhdGVNb2R1bGVWaXNpYmlsaXR5KCk7XG4gICAgfSk7XG4gIH1cblxuICBpbml0U2VhcmNoQmxvY2soKSB7XG4gICAgY29uc3Qgc2VsZiA9IHRoaXM7XG4gICAgc2VsZi5wc3RhZ2dlcklucHV0ID0gJCgnI21vZHVsZS1zZWFyY2gtYmFyJykucHN0YWdnZXIoe1xuICAgICAgb25UYWdzQ2hhbmdlZDogKHRhZ0xpc3QpID0+IHtcbiAgICAgICAgc2VsZi5jdXJyZW50VGFnc0xpc3QgPSB0YWdMaXN0O1xuICAgICAgICBzZWxmLnVwZGF0ZU1vZHVsZVZpc2liaWxpdHkoKTtcbiAgICAgIH0sXG4gICAgICBvblJlc2V0VGFnczogKCkgPT4ge1xuICAgICAgICBzZWxmLmN1cnJlbnRUYWdzTGlzdCA9IFtdO1xuICAgICAgICBzZWxmLnVwZGF0ZU1vZHVsZVZpc2liaWxpdHkoKTtcbiAgICAgIH0sXG4gICAgICBpbnB1dFBsYWNlaG9sZGVyOiB3aW5kb3cudHJhbnNsYXRlX2phdmFzY3JpcHRzWydTZWFyY2ggLSBwbGFjZWhvbGRlciddLFxuICAgICAgY2xvc2luZ0Nyb3NzOiB0cnVlLFxuICAgICAgY29udGV4dDogc2VsZixcbiAgICB9KTtcblxuICAgICQoJ2JvZHknKS5vbignY2xpY2snLCAnLm1vZHVsZS1hZGRvbnMtc2VhcmNoLWxpbmsnLCAoZXZlbnQpID0+IHtcbiAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICBldmVudC5zdG9wUHJvcGFnYXRpb24oKTtcbiAgICAgIHdpbmRvdy5vcGVuKCQodGhpcykuYXR0cignaHJlZicpLCAnX2JsYW5rJyk7XG4gICAgfSk7XG4gIH1cblxuICAvKipcbiAgICogSW5pdGlhbGl6ZSBkaXNwbGF5IHN3aXRjaGluZyBiZXR3ZWVuIExpc3Qgb3IgR3JpZFxuICAgKi9cbiAgaW5pdFNvcnRpbmdEaXNwbGF5U3dpdGNoKCkge1xuICAgIGNvbnN0IHNlbGYgPSB0aGlzO1xuXG4gICAgJCgnYm9keScpLm9uKCdjbGljaycsICcubW9kdWxlLXNvcnQtc3dpdGNoJywgZnVuY3Rpb24gc3dpdGNoU29ydCgpIHtcbiAgICAgIGNvbnN0IHN3aXRjaFRvID0gJCh0aGlzKS5kYXRhKCdzd2l0Y2gnKTtcbiAgICAgIGNvbnN0IGlzQWxyZWFkeURpc3BsYXllZCA9ICQodGhpcykuaGFzQ2xhc3MoJ2FjdGl2ZS1kaXNwbGF5Jyk7XG5cbiAgICAgIGlmICh0eXBlb2Ygc3dpdGNoVG8gIT09ICd1bmRlZmluZWQnICYmIGlzQWxyZWFkeURpc3BsYXllZCA9PT0gZmFsc2UpIHtcbiAgICAgICAgc2VsZi5zd2l0Y2hTb3J0aW5nRGlzcGxheVRvKHN3aXRjaFRvKTtcbiAgICAgICAgc2VsZi5jdXJyZW50RGlzcGxheSA9IHN3aXRjaFRvO1xuICAgICAgfVxuICAgIH0pO1xuICB9XG5cbiAgc3dpdGNoU29ydGluZ0Rpc3BsYXlUbyhzd2l0Y2hUbykge1xuICAgIGlmIChzd2l0Y2hUbyAhPT0gdGhpcy5ESVNQTEFZX0dSSUQgJiYgc3dpdGNoVG8gIT09IHRoaXMuRElTUExBWV9MSVNUKSB7XG4gICAgICBjb25zb2xlLmVycm9yKGBDYW4ndCBzd2l0Y2ggdG8gdW5kZWZpbmVkIGRpc3BsYXkgcHJvcGVydHkgXCIke3N3aXRjaFRvfVwiYCk7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgJCgnLm1vZHVsZS1zb3J0LXN3aXRjaCcpLnJlbW92ZUNsYXNzKCdtb2R1bGUtc29ydC1hY3RpdmUnKTtcbiAgICAkKGAjbW9kdWxlLXNvcnQtJHtzd2l0Y2hUb31gKS5hZGRDbGFzcygnbW9kdWxlLXNvcnQtYWN0aXZlJyk7XG4gICAgdGhpcy5jdXJyZW50RGlzcGxheSA9IHN3aXRjaFRvO1xuICAgIHRoaXMudXBkYXRlTW9kdWxlVmlzaWJpbGl0eSgpO1xuICB9XG5cbiAgaW5pdGlhbGl6ZVNlZU1vcmUoKSB7XG4gICAgY29uc3Qgc2VsZiA9IHRoaXM7XG5cbiAgICAkKGAke3NlbGYubW9kdWxlU2hvcnRMaXN0fSAke3NlbGYuc2VlTW9yZVNlbGVjdG9yfWApLm9uKCdjbGljaycsIGZ1bmN0aW9uIHNlZU1vcmUoKSB7XG4gICAgICBzZWxmLmN1cnJlbnRDYXRlZ29yeURpc3BsYXlbJCh0aGlzKS5kYXRhKCdjYXRlZ29yeScpXSA9IHRydWU7XG4gICAgICAkKHRoaXMpLmFkZENsYXNzKCdkLW5vbmUnKTtcbiAgICAgICQodGhpcylcbiAgICAgICAgLmNsb3Nlc3Qoc2VsZi5tb2R1bGVTaG9ydExpc3QpXG4gICAgICAgIC5maW5kKHNlbGYuc2VlTGVzc1NlbGVjdG9yKVxuICAgICAgICAucmVtb3ZlQ2xhc3MoJ2Qtbm9uZScpO1xuICAgICAgc2VsZi51cGRhdGVNb2R1bGVWaXNpYmlsaXR5KCk7XG4gICAgfSk7XG5cbiAgICAkKGAke3NlbGYubW9kdWxlU2hvcnRMaXN0fSAke3NlbGYuc2VlTGVzc1NlbGVjdG9yfWApLm9uKCdjbGljaycsIGZ1bmN0aW9uIHNlZU1vcmUoKSB7XG4gICAgICBzZWxmLmN1cnJlbnRDYXRlZ29yeURpc3BsYXlbJCh0aGlzKS5kYXRhKCdjYXRlZ29yeScpXSA9IGZhbHNlO1xuICAgICAgJCh0aGlzKS5hZGRDbGFzcygnZC1ub25lJyk7XG4gICAgICAkKHRoaXMpXG4gICAgICAgIC5jbG9zZXN0KHNlbGYubW9kdWxlU2hvcnRMaXN0KVxuICAgICAgICAuZmluZChzZWxmLnNlZU1vcmVTZWxlY3RvcilcbiAgICAgICAgLnJlbW92ZUNsYXNzKCdkLW5vbmUnKTtcbiAgICAgIHNlbGYudXBkYXRlTW9kdWxlVmlzaWJpbGl0eSgpO1xuICAgIH0pO1xuICB9XG5cbiAgdXBkYXRlVG90YWxSZXN1bHRzKCkge1xuICAgIGNvbnN0IHNlbGYgPSB0aGlzO1xuICAgIGNvbnN0IHJlcGxhY2VGaXJzdFdvcmRCeSA9IChlbGVtZW50LCB2YWx1ZSkgPT4ge1xuICAgICAgY29uc3QgZXhwbG9kZWRUZXh0ID0gZWxlbWVudC50ZXh0KCkuc3BsaXQoJyAnKTtcbiAgICAgIGV4cGxvZGVkVGV4dFswXSA9IHZhbHVlO1xuICAgICAgZWxlbWVudC50ZXh0KGV4cGxvZGVkVGV4dC5qb2luKCcgJykpO1xuICAgIH07XG5cbiAgICAvLyBJZiB0aGVyZSBhcmUgc29tZSBzaG9ydGxpc3Q6IGVhY2ggc2hvcnRsaXN0IGNvdW50IHRoZSBtb2R1bGVzIG9uIHRoZSBuZXh0IGNvbnRhaW5lci5cbiAgICBjb25zdCAkc2hvcnRMaXN0cyA9ICQoJy5tb2R1bGUtc2hvcnQtbGlzdCcpO1xuXG4gICAgaWYgKCRzaG9ydExpc3RzLmxlbmd0aCA+IDApIHtcbiAgICAgICRzaG9ydExpc3RzLmVhY2goZnVuY3Rpb24gc2hvcnRMaXN0cygpIHtcbiAgICAgICAgY29uc3QgJHRoaXMgPSAkKHRoaXMpO1xuICAgICAgICByZXBsYWNlRmlyc3RXb3JkQnkoXG4gICAgICAgICAgJHRoaXMuZmluZCgnLm1vZHVsZS1zZWFyY2gtcmVzdWx0LXdvcmRpbmcnKSxcbiAgICAgICAgICAkdGhpcy5uZXh0KCcubW9kdWxlcy1saXN0JykuZmluZCgnLm1vZHVsZS1pdGVtJykubGVuZ3RoLFxuICAgICAgICApO1xuICAgICAgfSk7XG5cbiAgICAgIC8vIElmIHRoZXJlIGlzIG5vIHNob3J0bGlzdDogdGhlIHdvcmRpbmcgZGlyZWN0bHkgdXBkYXRlIGZyb20gdGhlIG9ubHkgbW9kdWxlIGNvbnRhaW5lci5cbiAgICB9IGVsc2Uge1xuICAgICAgY29uc3QgbW9kdWxlc0NvdW50ID0gJCgnLm1vZHVsZXMtbGlzdCcpLmZpbmQoJy5tb2R1bGUtaXRlbScpLmxlbmd0aDtcbiAgICAgIHJlcGxhY2VGaXJzdFdvcmRCeSgkKCcubW9kdWxlLXNlYXJjaC1yZXN1bHQtd29yZGluZycpLCBtb2R1bGVzQ291bnQpO1xuXG4gICAgICAvLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmVcbiAgICAgIGNvbnN0IHNlbGVjdG9yVG9Ub2dnbGUgPVxuICAgICAgICBzZWxmLmN1cnJlbnREaXNwbGF5ID09PSBzZWxmLkRJU1BMQVlfTElTVCA/IHRoaXMuYWRkb25JdGVtTGlzdFNlbGVjdG9yIDogdGhpcy5hZGRvbkl0ZW1HcmlkU2VsZWN0b3I7XG4gICAgICAkKHNlbGVjdG9yVG9Ub2dnbGUpLnRvZ2dsZShtb2R1bGVzQ291bnQgIT09IHRoaXMubW9kdWxlc0xpc3QubGVuZ3RoIC8gMik7XG5cbiAgICAgIGlmIChtb2R1bGVzQ291bnQgPT09IDApIHtcbiAgICAgICAgJCgnLm1vZHVsZS1hZGRvbnMtc2VhcmNoLWxpbmsnKS5hdHRyKFxuICAgICAgICAgICdocmVmJyxcbiAgICAgICAgICBgJHt0aGlzLmJhc2VBZGRvbnNVcmx9c2VhcmNoLnBocD9zZWFyY2hfcXVlcnk9JHtlbmNvZGVVUklDb21wb25lbnQodGhpcy5jdXJyZW50VGFnc0xpc3Quam9pbignICcpKX1gLFxuICAgICAgICApO1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIGlzTW9kdWxlc1BhZ2UoKSB7XG4gICAgcmV0dXJuICQodGhpcy51cGdyYWRlQ29udGFpbmVyKS5sZW5ndGggPT09IDAgJiYgJCh0aGlzLm5vdGlmaWNhdGlvbkNvbnRhaW5lcikubGVuZ3RoID09PSAwO1xuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IEFkbWluTW9kdWxlQ29udHJvbGxlcjtcbiIsIi8qKlxuICogQ29weXJpZ2h0IHNpbmNlIDIwMDcgUHJlc3RhU2hvcCBTQSBhbmQgQ29udHJpYnV0b3JzXG4gKiBQcmVzdGFTaG9wIGlzIGFuIEludGVybmF0aW9uYWwgUmVnaXN0ZXJlZCBUcmFkZW1hcmsgJiBQcm9wZXJ0eSBvZiBQcmVzdGFTaG9wIFNBXG4gKlxuICogTk9USUNFIE9GIExJQ0VOU0VcbiAqXG4gKiBUaGlzIHNvdXJjZSBmaWxlIGlzIHN1YmplY3QgdG8gdGhlIE9wZW4gU29mdHdhcmUgTGljZW5zZSAoT1NMIDMuMClcbiAqIHRoYXQgaXMgYnVuZGxlZCB3aXRoIHRoaXMgcGFja2FnZSBpbiB0aGUgZmlsZSBMSUNFTlNFLm1kLlxuICogSXQgaXMgYWxzbyBhdmFpbGFibGUgdGhyb3VnaCB0aGUgd29ybGQtd2lkZS13ZWIgYXQgdGhpcyBVUkw6XG4gKiBodHRwczovL29wZW5zb3VyY2Uub3JnL2xpY2Vuc2VzL09TTC0zLjBcbiAqIElmIHlvdSBkaWQgbm90IHJlY2VpdmUgYSBjb3B5IG9mIHRoZSBsaWNlbnNlIGFuZCBhcmUgdW5hYmxlIHRvXG4gKiBvYnRhaW4gaXQgdGhyb3VnaCB0aGUgd29ybGQtd2lkZS13ZWIsIHBsZWFzZSBzZW5kIGFuIGVtYWlsXG4gKiB0byBsaWNlbnNlQHByZXN0YXNob3AuY29tIHNvIHdlIGNhbiBzZW5kIHlvdSBhIGNvcHkgaW1tZWRpYXRlbHkuXG4gKlxuICogRElTQ0xBSU1FUlxuICpcbiAqIERvIG5vdCBlZGl0IG9yIGFkZCB0byB0aGlzIGZpbGUgaWYgeW91IHdpc2ggdG8gdXBncmFkZSBQcmVzdGFTaG9wIHRvIG5ld2VyXG4gKiB2ZXJzaW9ucyBpbiB0aGUgZnV0dXJlLiBJZiB5b3Ugd2lzaCB0byBjdXN0b21pemUgUHJlc3RhU2hvcCBmb3IgeW91clxuICogbmVlZHMgcGxlYXNlIHJlZmVyIHRvIGh0dHBzOi8vZGV2ZG9jcy5wcmVzdGFzaG9wLmNvbS8gZm9yIG1vcmUgaW5mb3JtYXRpb24uXG4gKlxuICogQGF1dGhvciAgICBQcmVzdGFTaG9wIFNBIGFuZCBDb250cmlidXRvcnMgPGNvbnRhY3RAcHJlc3Rhc2hvcC5jb20+XG4gKiBAY29weXJpZ2h0IFNpbmNlIDIwMDcgUHJlc3RhU2hvcCBTQSBhbmQgQ29udHJpYnV0b3JzXG4gKiBAbGljZW5zZSAgIGh0dHBzOi8vb3BlbnNvdXJjZS5vcmcvbGljZW5zZXMvT1NMLTMuMCBPcGVuIFNvZnR3YXJlIExpY2Vuc2UgKE9TTCAzLjApXG4gKi9cblxuY29uc3QgeyR9ID0gd2luZG93O1xuXG4vKipcbiAqIE1vZHVsZSBBZG1pbiBQYWdlIExvYWRlci5cbiAqIEBjb25zdHJ1Y3RvclxuICovXG5jbGFzcyBNb2R1bGVMb2FkZXIge1xuICBjb25zdHJ1Y3RvcigpIHtcbiAgICBNb2R1bGVMb2FkZXIuaGFuZGxlSW1wb3J0KCk7XG4gICAgTW9kdWxlTG9hZGVyLmhhbmRsZUV2ZW50cygpO1xuICB9XG5cbiAgc3RhdGljIGhhbmRsZUltcG9ydCgpIHtcbiAgICBjb25zdCBtb2R1bGVJbXBvcnQgPSAkKCcjbW9kdWxlLWltcG9ydCcpO1xuICAgIG1vZHVsZUltcG9ydC5jbGljaygoKSA9PiB7XG4gICAgICBtb2R1bGVJbXBvcnQuYWRkQ2xhc3MoJ29uY2xpY2snLCAyNTAsIHZhbGlkYXRlKTtcbiAgICB9KTtcblxuICAgIGZ1bmN0aW9uIHZhbGlkYXRlKCkge1xuICAgICAgc2V0VGltZW91dChcbiAgICAgICAgKCkgPT4ge1xuICAgICAgICAgIG1vZHVsZUltcG9ydC5yZW1vdmVDbGFzcygnb25jbGljaycpO1xuICAgICAgICAgIG1vZHVsZUltcG9ydC5hZGRDbGFzcygndmFsaWRhdGUnLCA0NTAsIGNhbGxiYWNrKTtcbiAgICAgICAgfSxcbiAgICAgICAgMjI1MCxcbiAgICAgICk7XG4gICAgfVxuICAgIGZ1bmN0aW9uIGNhbGxiYWNrKCkge1xuICAgICAgc2V0VGltZW91dChcbiAgICAgICAgKCkgPT4ge1xuICAgICAgICAgIG1vZHVsZUltcG9ydC5yZW1vdmVDbGFzcygndmFsaWRhdGUnKTtcbiAgICAgICAgfSxcbiAgICAgICAgMTI1MCxcbiAgICAgICk7XG4gICAgfVxuICB9XG5cbiAgc3RhdGljIGhhbmRsZUV2ZW50cygpIHtcbiAgICAkKCdib2R5Jykub24oXG4gICAgICAnY2xpY2snLFxuICAgICAgJ2EubW9kdWxlLXJlYWQtbW9yZS1ncmlkLWJ0biwgYS5tb2R1bGUtcmVhZC1tb3JlLWxpc3QtYnRuJyxcbiAgICAgIChldmVudCkgPT4ge1xuICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICBjb25zdCBtb2R1bGVQb3BwaW4gPSAkKGV2ZW50LnRhcmdldCkuZGF0YSgndGFyZ2V0Jyk7XG5cbiAgICAgICAgJC5nZXQoZXZlbnQudGFyZ2V0LmhyZWYsIChkYXRhKSA9PiB7XG4gICAgICAgICAgJChtb2R1bGVQb3BwaW4pLmh0bWwoZGF0YSk7XG4gICAgICAgICAgJChtb2R1bGVQb3BwaW4pLm1vZGFsKCk7XG4gICAgICAgIH0pO1xuICAgICAgfSxcbiAgICApO1xuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IE1vZHVsZUxvYWRlcjtcbiIsIm1vZHVsZS5leHBvcnRzID0geyBcImRlZmF1bHRcIjogcmVxdWlyZShcImNvcmUtanMvbGlicmFyeS9mbi9hcnJheS9mcm9tXCIpLCBfX2VzTW9kdWxlOiB0cnVlIH07IiwibW9kdWxlLmV4cG9ydHMgPSB7IFwiZGVmYXVsdFwiOiByZXF1aXJlKFwiY29yZS1qcy9saWJyYXJ5L2ZuL251bWJlci9pcy1uYW5cIiksIF9fZXNNb2R1bGU6IHRydWUgfTsiLCJtb2R1bGUuZXhwb3J0cyA9IHsgXCJkZWZhdWx0XCI6IHJlcXVpcmUoXCJjb3JlLWpzL2xpYnJhcnkvZm4vb2JqZWN0L2RlZmluZS1wcm9wZXJ0eVwiKSwgX19lc01vZHVsZTogdHJ1ZSB9OyIsIm1vZHVsZS5leHBvcnRzID0geyBcImRlZmF1bHRcIjogcmVxdWlyZShcImNvcmUtanMvbGlicmFyeS9mbi9vYmplY3Qva2V5c1wiKSwgX19lc01vZHVsZTogdHJ1ZSB9OyIsIlwidXNlIHN0cmljdFwiO1xuXG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xuXG5leHBvcnRzLmRlZmF1bHQgPSBmdW5jdGlvbiAoaW5zdGFuY2UsIENvbnN0cnVjdG9yKSB7XG4gIGlmICghKGluc3RhbmNlIGluc3RhbmNlb2YgQ29uc3RydWN0b3IpKSB7XG4gICAgdGhyb3cgbmV3IFR5cGVFcnJvcihcIkNhbm5vdCBjYWxsIGEgY2xhc3MgYXMgYSBmdW5jdGlvblwiKTtcbiAgfVxufTsiLCJcInVzZSBzdHJpY3RcIjtcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcblxudmFyIF9kZWZpbmVQcm9wZXJ0eSA9IHJlcXVpcmUoXCIuLi9jb3JlLWpzL29iamVjdC9kZWZpbmUtcHJvcGVydHlcIik7XG5cbnZhciBfZGVmaW5lUHJvcGVydHkyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZGVmaW5lUHJvcGVydHkpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG5leHBvcnRzLmRlZmF1bHQgPSBmdW5jdGlvbiAoKSB7XG4gIGZ1bmN0aW9uIGRlZmluZVByb3BlcnRpZXModGFyZ2V0LCBwcm9wcykge1xuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgcHJvcHMubGVuZ3RoOyBpKyspIHtcbiAgICAgIHZhciBkZXNjcmlwdG9yID0gcHJvcHNbaV07XG4gICAgICBkZXNjcmlwdG9yLmVudW1lcmFibGUgPSBkZXNjcmlwdG9yLmVudW1lcmFibGUgfHwgZmFsc2U7XG4gICAgICBkZXNjcmlwdG9yLmNvbmZpZ3VyYWJsZSA9IHRydWU7XG4gICAgICBpZiAoXCJ2YWx1ZVwiIGluIGRlc2NyaXB0b3IpIGRlc2NyaXB0b3Iud3JpdGFibGUgPSB0cnVlO1xuICAgICAgKDAsIF9kZWZpbmVQcm9wZXJ0eTIuZGVmYXVsdCkodGFyZ2V0LCBkZXNjcmlwdG9yLmtleSwgZGVzY3JpcHRvcik7XG4gICAgfVxuICB9XG5cbiAgcmV0dXJuIGZ1bmN0aW9uIChDb25zdHJ1Y3RvciwgcHJvdG9Qcm9wcywgc3RhdGljUHJvcHMpIHtcbiAgICBpZiAocHJvdG9Qcm9wcykgZGVmaW5lUHJvcGVydGllcyhDb25zdHJ1Y3Rvci5wcm90b3R5cGUsIHByb3RvUHJvcHMpO1xuICAgIGlmIChzdGF0aWNQcm9wcykgZGVmaW5lUHJvcGVydGllcyhDb25zdHJ1Y3Rvciwgc3RhdGljUHJvcHMpO1xuICAgIHJldHVybiBDb25zdHJ1Y3RvcjtcbiAgfTtcbn0oKTsiLCJcInVzZSBzdHJpY3RcIjtcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcblxudmFyIF9mcm9tID0gcmVxdWlyZShcIi4uL2NvcmUtanMvYXJyYXkvZnJvbVwiKTtcblxudmFyIF9mcm9tMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2Zyb20pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG5leHBvcnRzLmRlZmF1bHQgPSBmdW5jdGlvbiAoYXJyKSB7XG4gIGlmIChBcnJheS5pc0FycmF5KGFycikpIHtcbiAgICBmb3IgKHZhciBpID0gMCwgYXJyMiA9IEFycmF5KGFyci5sZW5ndGgpOyBpIDwgYXJyLmxlbmd0aDsgaSsrKSB7XG4gICAgICBhcnIyW2ldID0gYXJyW2ldO1xuICAgIH1cblxuICAgIHJldHVybiBhcnIyO1xuICB9IGVsc2Uge1xuICAgIHJldHVybiAoMCwgX2Zyb20yLmRlZmF1bHQpKGFycik7XG4gIH1cbn07IiwicmVxdWlyZSgnLi4vLi4vbW9kdWxlcy9lczYuc3RyaW5nLml0ZXJhdG9yJyk7XG5yZXF1aXJlKCcuLi8uLi9tb2R1bGVzL2VzNi5hcnJheS5mcm9tJyk7XG5tb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoJy4uLy4uL21vZHVsZXMvX2NvcmUnKS5BcnJheS5mcm9tO1xuIiwicmVxdWlyZSgnLi4vLi4vbW9kdWxlcy9lczYubnVtYmVyLmlzLW5hbicpO1xubW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKCcuLi8uLi9tb2R1bGVzL19jb3JlJykuTnVtYmVyLmlzTmFOO1xuIiwicmVxdWlyZSgnLi4vLi4vbW9kdWxlcy9lczYub2JqZWN0LmRlZmluZS1wcm9wZXJ0eScpO1xudmFyICRPYmplY3QgPSByZXF1aXJlKCcuLi8uLi9tb2R1bGVzL19jb3JlJykuT2JqZWN0O1xubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBkZWZpbmVQcm9wZXJ0eShpdCwga2V5LCBkZXNjKSB7XG4gIHJldHVybiAkT2JqZWN0LmRlZmluZVByb3BlcnR5KGl0LCBrZXksIGRlc2MpO1xufTtcbiIsInJlcXVpcmUoJy4uLy4uL21vZHVsZXMvZXM2Lm9iamVjdC5rZXlzJyk7XG5tb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoJy4uLy4uL21vZHVsZXMvX2NvcmUnKS5PYmplY3Qua2V5cztcbiIsIm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKGl0KSB7XG4gIGlmICh0eXBlb2YgaXQgIT0gJ2Z1bmN0aW9uJykgdGhyb3cgVHlwZUVycm9yKGl0ICsgJyBpcyBub3QgYSBmdW5jdGlvbiEnKTtcbiAgcmV0dXJuIGl0O1xufTtcbiIsInZhciBpc09iamVjdCA9IHJlcXVpcmUoJy4vX2lzLW9iamVjdCcpO1xubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAoaXQpIHtcbiAgaWYgKCFpc09iamVjdChpdCkpIHRocm93IFR5cGVFcnJvcihpdCArICcgaXMgbm90IGFuIG9iamVjdCEnKTtcbiAgcmV0dXJuIGl0O1xufTtcbiIsIi8vIGZhbHNlIC0+IEFycmF5I2luZGV4T2Zcbi8vIHRydWUgIC0+IEFycmF5I2luY2x1ZGVzXG52YXIgdG9JT2JqZWN0ID0gcmVxdWlyZSgnLi9fdG8taW9iamVjdCcpO1xudmFyIHRvTGVuZ3RoID0gcmVxdWlyZSgnLi9fdG8tbGVuZ3RoJyk7XG52YXIgdG9BYnNvbHV0ZUluZGV4ID0gcmVxdWlyZSgnLi9fdG8tYWJzb2x1dGUtaW5kZXgnKTtcbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKElTX0lOQ0xVREVTKSB7XG4gIHJldHVybiBmdW5jdGlvbiAoJHRoaXMsIGVsLCBmcm9tSW5kZXgpIHtcbiAgICB2YXIgTyA9IHRvSU9iamVjdCgkdGhpcyk7XG4gICAgdmFyIGxlbmd0aCA9IHRvTGVuZ3RoKE8ubGVuZ3RoKTtcbiAgICB2YXIgaW5kZXggPSB0b0Fic29sdXRlSW5kZXgoZnJvbUluZGV4LCBsZW5ndGgpO1xuICAgIHZhciB2YWx1ZTtcbiAgICAvLyBBcnJheSNpbmNsdWRlcyB1c2VzIFNhbWVWYWx1ZVplcm8gZXF1YWxpdHkgYWxnb3JpdGhtXG4gICAgLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lIG5vLXNlbGYtY29tcGFyZVxuICAgIGlmIChJU19JTkNMVURFUyAmJiBlbCAhPSBlbCkgd2hpbGUgKGxlbmd0aCA+IGluZGV4KSB7XG4gICAgICB2YWx1ZSA9IE9baW5kZXgrK107XG4gICAgICAvLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmUgbm8tc2VsZi1jb21wYXJlXG4gICAgICBpZiAodmFsdWUgIT0gdmFsdWUpIHJldHVybiB0cnVlO1xuICAgIC8vIEFycmF5I2luZGV4T2YgaWdub3JlcyBob2xlcywgQXJyYXkjaW5jbHVkZXMgLSBub3RcbiAgICB9IGVsc2UgZm9yICg7bGVuZ3RoID4gaW5kZXg7IGluZGV4KyspIGlmIChJU19JTkNMVURFUyB8fCBpbmRleCBpbiBPKSB7XG4gICAgICBpZiAoT1tpbmRleF0gPT09IGVsKSByZXR1cm4gSVNfSU5DTFVERVMgfHwgaW5kZXggfHwgMDtcbiAgICB9IHJldHVybiAhSVNfSU5DTFVERVMgJiYgLTE7XG4gIH07XG59O1xuIiwiLy8gZ2V0dGluZyB0YWcgZnJvbSAxOS4xLjMuNiBPYmplY3QucHJvdG90eXBlLnRvU3RyaW5nKClcbnZhciBjb2YgPSByZXF1aXJlKCcuL19jb2YnKTtcbnZhciBUQUcgPSByZXF1aXJlKCcuL193a3MnKSgndG9TdHJpbmdUYWcnKTtcbi8vIEVTMyB3cm9uZyBoZXJlXG52YXIgQVJHID0gY29mKGZ1bmN0aW9uICgpIHsgcmV0dXJuIGFyZ3VtZW50czsgfSgpKSA9PSAnQXJndW1lbnRzJztcblxuLy8gZmFsbGJhY2sgZm9yIElFMTEgU2NyaXB0IEFjY2VzcyBEZW5pZWQgZXJyb3JcbnZhciB0cnlHZXQgPSBmdW5jdGlvbiAoaXQsIGtleSkge1xuICB0cnkge1xuICAgIHJldHVybiBpdFtrZXldO1xuICB9IGNhdGNoIChlKSB7IC8qIGVtcHR5ICovIH1cbn07XG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKGl0KSB7XG4gIHZhciBPLCBULCBCO1xuICByZXR1cm4gaXQgPT09IHVuZGVmaW5lZCA/ICdVbmRlZmluZWQnIDogaXQgPT09IG51bGwgPyAnTnVsbCdcbiAgICAvLyBAQHRvU3RyaW5nVGFnIGNhc2VcbiAgICA6IHR5cGVvZiAoVCA9IHRyeUdldChPID0gT2JqZWN0KGl0KSwgVEFHKSkgPT0gJ3N0cmluZycgPyBUXG4gICAgLy8gYnVpbHRpblRhZyBjYXNlXG4gICAgOiBBUkcgPyBjb2YoTylcbiAgICAvLyBFUzMgYXJndW1lbnRzIGZhbGxiYWNrXG4gICAgOiAoQiA9IGNvZihPKSkgPT0gJ09iamVjdCcgJiYgdHlwZW9mIE8uY2FsbGVlID09ICdmdW5jdGlvbicgPyAnQXJndW1lbnRzJyA6IEI7XG59O1xuIiwidmFyIHRvU3RyaW5nID0ge30udG9TdHJpbmc7XG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKGl0KSB7XG4gIHJldHVybiB0b1N0cmluZy5jYWxsKGl0KS5zbGljZSg4LCAtMSk7XG59O1xuIiwidmFyIGNvcmUgPSBtb2R1bGUuZXhwb3J0cyA9IHsgdmVyc2lvbjogJzIuNi4xMScgfTtcbmlmICh0eXBlb2YgX19lID09ICdudW1iZXInKSBfX2UgPSBjb3JlOyAvLyBlc2xpbnQtZGlzYWJsZS1saW5lIG5vLXVuZGVmXG4iLCIndXNlIHN0cmljdCc7XG52YXIgJGRlZmluZVByb3BlcnR5ID0gcmVxdWlyZSgnLi9fb2JqZWN0LWRwJyk7XG52YXIgY3JlYXRlRGVzYyA9IHJlcXVpcmUoJy4vX3Byb3BlcnR5LWRlc2MnKTtcblxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAob2JqZWN0LCBpbmRleCwgdmFsdWUpIHtcbiAgaWYgKGluZGV4IGluIG9iamVjdCkgJGRlZmluZVByb3BlcnR5LmYob2JqZWN0LCBpbmRleCwgY3JlYXRlRGVzYygwLCB2YWx1ZSkpO1xuICBlbHNlIG9iamVjdFtpbmRleF0gPSB2YWx1ZTtcbn07XG4iLCIvLyBvcHRpb25hbCAvIHNpbXBsZSBjb250ZXh0IGJpbmRpbmdcbnZhciBhRnVuY3Rpb24gPSByZXF1aXJlKCcuL19hLWZ1bmN0aW9uJyk7XG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIChmbiwgdGhhdCwgbGVuZ3RoKSB7XG4gIGFGdW5jdGlvbihmbik7XG4gIGlmICh0aGF0ID09PSB1bmRlZmluZWQpIHJldHVybiBmbjtcbiAgc3dpdGNoIChsZW5ndGgpIHtcbiAgICBjYXNlIDE6IHJldHVybiBmdW5jdGlvbiAoYSkge1xuICAgICAgcmV0dXJuIGZuLmNhbGwodGhhdCwgYSk7XG4gICAgfTtcbiAgICBjYXNlIDI6IHJldHVybiBmdW5jdGlvbiAoYSwgYikge1xuICAgICAgcmV0dXJuIGZuLmNhbGwodGhhdCwgYSwgYik7XG4gICAgfTtcbiAgICBjYXNlIDM6IHJldHVybiBmdW5jdGlvbiAoYSwgYiwgYykge1xuICAgICAgcmV0dXJuIGZuLmNhbGwodGhhdCwgYSwgYiwgYyk7XG4gICAgfTtcbiAgfVxuICByZXR1cm4gZnVuY3Rpb24gKC8qIC4uLmFyZ3MgKi8pIHtcbiAgICByZXR1cm4gZm4uYXBwbHkodGhhdCwgYXJndW1lbnRzKTtcbiAgfTtcbn07XG4iLCIvLyA3LjIuMSBSZXF1aXJlT2JqZWN0Q29lcmNpYmxlKGFyZ3VtZW50KVxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAoaXQpIHtcbiAgaWYgKGl0ID09IHVuZGVmaW5lZCkgdGhyb3cgVHlwZUVycm9yKFwiQ2FuJ3QgY2FsbCBtZXRob2Qgb24gIFwiICsgaXQpO1xuICByZXR1cm4gaXQ7XG59O1xuIiwiLy8gVGhhbmsncyBJRTggZm9yIGhpcyBmdW5ueSBkZWZpbmVQcm9wZXJ0eVxubW9kdWxlLmV4cG9ydHMgPSAhcmVxdWlyZSgnLi9fZmFpbHMnKShmdW5jdGlvbiAoKSB7XG4gIHJldHVybiBPYmplY3QuZGVmaW5lUHJvcGVydHkoe30sICdhJywgeyBnZXQ6IGZ1bmN0aW9uICgpIHsgcmV0dXJuIDc7IH0gfSkuYSAhPSA3O1xufSk7XG4iLCJ2YXIgaXNPYmplY3QgPSByZXF1aXJlKCcuL19pcy1vYmplY3QnKTtcbnZhciBkb2N1bWVudCA9IHJlcXVpcmUoJy4vX2dsb2JhbCcpLmRvY3VtZW50O1xuLy8gdHlwZW9mIGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQgaXMgJ29iamVjdCcgaW4gb2xkIElFXG52YXIgaXMgPSBpc09iamVjdChkb2N1bWVudCkgJiYgaXNPYmplY3QoZG9jdW1lbnQuY3JlYXRlRWxlbWVudCk7XG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIChpdCkge1xuICByZXR1cm4gaXMgPyBkb2N1bWVudC5jcmVhdGVFbGVtZW50KGl0KSA6IHt9O1xufTtcbiIsIi8vIElFIDgtIGRvbid0IGVudW0gYnVnIGtleXNcbm1vZHVsZS5leHBvcnRzID0gKFxuICAnY29uc3RydWN0b3IsaGFzT3duUHJvcGVydHksaXNQcm90b3R5cGVPZixwcm9wZXJ0eUlzRW51bWVyYWJsZSx0b0xvY2FsZVN0cmluZyx0b1N0cmluZyx2YWx1ZU9mJ1xuKS5zcGxpdCgnLCcpO1xuIiwidmFyIGdsb2JhbCA9IHJlcXVpcmUoJy4vX2dsb2JhbCcpO1xudmFyIGNvcmUgPSByZXF1aXJlKCcuL19jb3JlJyk7XG52YXIgY3R4ID0gcmVxdWlyZSgnLi9fY3R4Jyk7XG52YXIgaGlkZSA9IHJlcXVpcmUoJy4vX2hpZGUnKTtcbnZhciBoYXMgPSByZXF1aXJlKCcuL19oYXMnKTtcbnZhciBQUk9UT1RZUEUgPSAncHJvdG90eXBlJztcblxudmFyICRleHBvcnQgPSBmdW5jdGlvbiAodHlwZSwgbmFtZSwgc291cmNlKSB7XG4gIHZhciBJU19GT1JDRUQgPSB0eXBlICYgJGV4cG9ydC5GO1xuICB2YXIgSVNfR0xPQkFMID0gdHlwZSAmICRleHBvcnQuRztcbiAgdmFyIElTX1NUQVRJQyA9IHR5cGUgJiAkZXhwb3J0LlM7XG4gIHZhciBJU19QUk9UTyA9IHR5cGUgJiAkZXhwb3J0LlA7XG4gIHZhciBJU19CSU5EID0gdHlwZSAmICRleHBvcnQuQjtcbiAgdmFyIElTX1dSQVAgPSB0eXBlICYgJGV4cG9ydC5XO1xuICB2YXIgZXhwb3J0cyA9IElTX0dMT0JBTCA/IGNvcmUgOiBjb3JlW25hbWVdIHx8IChjb3JlW25hbWVdID0ge30pO1xuICB2YXIgZXhwUHJvdG8gPSBleHBvcnRzW1BST1RPVFlQRV07XG4gIHZhciB0YXJnZXQgPSBJU19HTE9CQUwgPyBnbG9iYWwgOiBJU19TVEFUSUMgPyBnbG9iYWxbbmFtZV0gOiAoZ2xvYmFsW25hbWVdIHx8IHt9KVtQUk9UT1RZUEVdO1xuICB2YXIga2V5LCBvd24sIG91dDtcbiAgaWYgKElTX0dMT0JBTCkgc291cmNlID0gbmFtZTtcbiAgZm9yIChrZXkgaW4gc291cmNlKSB7XG4gICAgLy8gY29udGFpbnMgaW4gbmF0aXZlXG4gICAgb3duID0gIUlTX0ZPUkNFRCAmJiB0YXJnZXQgJiYgdGFyZ2V0W2tleV0gIT09IHVuZGVmaW5lZDtcbiAgICBpZiAob3duICYmIGhhcyhleHBvcnRzLCBrZXkpKSBjb250aW51ZTtcbiAgICAvLyBleHBvcnQgbmF0aXZlIG9yIHBhc3NlZFxuICAgIG91dCA9IG93biA/IHRhcmdldFtrZXldIDogc291cmNlW2tleV07XG4gICAgLy8gcHJldmVudCBnbG9iYWwgcG9sbHV0aW9uIGZvciBuYW1lc3BhY2VzXG4gICAgZXhwb3J0c1trZXldID0gSVNfR0xPQkFMICYmIHR5cGVvZiB0YXJnZXRba2V5XSAhPSAnZnVuY3Rpb24nID8gc291cmNlW2tleV1cbiAgICAvLyBiaW5kIHRpbWVycyB0byBnbG9iYWwgZm9yIGNhbGwgZnJvbSBleHBvcnQgY29udGV4dFxuICAgIDogSVNfQklORCAmJiBvd24gPyBjdHgob3V0LCBnbG9iYWwpXG4gICAgLy8gd3JhcCBnbG9iYWwgY29uc3RydWN0b3JzIGZvciBwcmV2ZW50IGNoYW5nZSB0aGVtIGluIGxpYnJhcnlcbiAgICA6IElTX1dSQVAgJiYgdGFyZ2V0W2tleV0gPT0gb3V0ID8gKGZ1bmN0aW9uIChDKSB7XG4gICAgICB2YXIgRiA9IGZ1bmN0aW9uIChhLCBiLCBjKSB7XG4gICAgICAgIGlmICh0aGlzIGluc3RhbmNlb2YgQykge1xuICAgICAgICAgIHN3aXRjaCAoYXJndW1lbnRzLmxlbmd0aCkge1xuICAgICAgICAgICAgY2FzZSAwOiByZXR1cm4gbmV3IEMoKTtcbiAgICAgICAgICAgIGNhc2UgMTogcmV0dXJuIG5ldyBDKGEpO1xuICAgICAgICAgICAgY2FzZSAyOiByZXR1cm4gbmV3IEMoYSwgYik7XG4gICAgICAgICAgfSByZXR1cm4gbmV3IEMoYSwgYiwgYyk7XG4gICAgICAgIH0gcmV0dXJuIEMuYXBwbHkodGhpcywgYXJndW1lbnRzKTtcbiAgICAgIH07XG4gICAgICBGW1BST1RPVFlQRV0gPSBDW1BST1RPVFlQRV07XG4gICAgICByZXR1cm4gRjtcbiAgICAvLyBtYWtlIHN0YXRpYyB2ZXJzaW9ucyBmb3IgcHJvdG90eXBlIG1ldGhvZHNcbiAgICB9KShvdXQpIDogSVNfUFJPVE8gJiYgdHlwZW9mIG91dCA9PSAnZnVuY3Rpb24nID8gY3R4KEZ1bmN0aW9uLmNhbGwsIG91dCkgOiBvdXQ7XG4gICAgLy8gZXhwb3J0IHByb3RvIG1ldGhvZHMgdG8gY29yZS4lQ09OU1RSVUNUT1IlLm1ldGhvZHMuJU5BTUUlXG4gICAgaWYgKElTX1BST1RPKSB7XG4gICAgICAoZXhwb3J0cy52aXJ0dWFsIHx8IChleHBvcnRzLnZpcnR1YWwgPSB7fSkpW2tleV0gPSBvdXQ7XG4gICAgICAvLyBleHBvcnQgcHJvdG8gbWV0aG9kcyB0byBjb3JlLiVDT05TVFJVQ1RPUiUucHJvdG90eXBlLiVOQU1FJVxuICAgICAgaWYgKHR5cGUgJiAkZXhwb3J0LlIgJiYgZXhwUHJvdG8gJiYgIWV4cFByb3RvW2tleV0pIGhpZGUoZXhwUHJvdG8sIGtleSwgb3V0KTtcbiAgICB9XG4gIH1cbn07XG4vLyB0eXBlIGJpdG1hcFxuJGV4cG9ydC5GID0gMTsgICAvLyBmb3JjZWRcbiRleHBvcnQuRyA9IDI7ICAgLy8gZ2xvYmFsXG4kZXhwb3J0LlMgPSA0OyAgIC8vIHN0YXRpY1xuJGV4cG9ydC5QID0gODsgICAvLyBwcm90b1xuJGV4cG9ydC5CID0gMTY7ICAvLyBiaW5kXG4kZXhwb3J0LlcgPSAzMjsgIC8vIHdyYXBcbiRleHBvcnQuVSA9IDY0OyAgLy8gc2FmZVxuJGV4cG9ydC5SID0gMTI4OyAvLyByZWFsIHByb3RvIG1ldGhvZCBmb3IgYGxpYnJhcnlgXG5tb2R1bGUuZXhwb3J0cyA9ICRleHBvcnQ7XG4iLCJtb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIChleGVjKSB7XG4gIHRyeSB7XG4gICAgcmV0dXJuICEhZXhlYygpO1xuICB9IGNhdGNoIChlKSB7XG4gICAgcmV0dXJuIHRydWU7XG4gIH1cbn07XG4iLCIvLyBodHRwczovL2dpdGh1Yi5jb20vemxvaXJvY2svY29yZS1qcy9pc3N1ZXMvODYjaXNzdWVjb21tZW50LTExNTc1OTAyOFxudmFyIGdsb2JhbCA9IG1vZHVsZS5leHBvcnRzID0gdHlwZW9mIHdpbmRvdyAhPSAndW5kZWZpbmVkJyAmJiB3aW5kb3cuTWF0aCA9PSBNYXRoXG4gID8gd2luZG93IDogdHlwZW9mIHNlbGYgIT0gJ3VuZGVmaW5lZCcgJiYgc2VsZi5NYXRoID09IE1hdGggPyBzZWxmXG4gIC8vIGVzbGludC1kaXNhYmxlLW5leHQtbGluZSBuby1uZXctZnVuY1xuICA6IEZ1bmN0aW9uKCdyZXR1cm4gdGhpcycpKCk7XG5pZiAodHlwZW9mIF9fZyA9PSAnbnVtYmVyJykgX19nID0gZ2xvYmFsOyAvLyBlc2xpbnQtZGlzYWJsZS1saW5lIG5vLXVuZGVmXG4iLCJ2YXIgaGFzT3duUHJvcGVydHkgPSB7fS5oYXNPd25Qcm9wZXJ0eTtcbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKGl0LCBrZXkpIHtcbiAgcmV0dXJuIGhhc093blByb3BlcnR5LmNhbGwoaXQsIGtleSk7XG59O1xuIiwidmFyIGRQID0gcmVxdWlyZSgnLi9fb2JqZWN0LWRwJyk7XG52YXIgY3JlYXRlRGVzYyA9IHJlcXVpcmUoJy4vX3Byb3BlcnR5LWRlc2MnKTtcbm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZSgnLi9fZGVzY3JpcHRvcnMnKSA/IGZ1bmN0aW9uIChvYmplY3QsIGtleSwgdmFsdWUpIHtcbiAgcmV0dXJuIGRQLmYob2JqZWN0LCBrZXksIGNyZWF0ZURlc2MoMSwgdmFsdWUpKTtcbn0gOiBmdW5jdGlvbiAob2JqZWN0LCBrZXksIHZhbHVlKSB7XG4gIG9iamVjdFtrZXldID0gdmFsdWU7XG4gIHJldHVybiBvYmplY3Q7XG59O1xuIiwidmFyIGRvY3VtZW50ID0gcmVxdWlyZSgnLi9fZ2xvYmFsJykuZG9jdW1lbnQ7XG5tb2R1bGUuZXhwb3J0cyA9IGRvY3VtZW50ICYmIGRvY3VtZW50LmRvY3VtZW50RWxlbWVudDtcbiIsIm1vZHVsZS5leHBvcnRzID0gIXJlcXVpcmUoJy4vX2Rlc2NyaXB0b3JzJykgJiYgIXJlcXVpcmUoJy4vX2ZhaWxzJykoZnVuY3Rpb24gKCkge1xuICByZXR1cm4gT2JqZWN0LmRlZmluZVByb3BlcnR5KHJlcXVpcmUoJy4vX2RvbS1jcmVhdGUnKSgnZGl2JyksICdhJywgeyBnZXQ6IGZ1bmN0aW9uICgpIHsgcmV0dXJuIDc7IH0gfSkuYSAhPSA3O1xufSk7XG4iLCIvLyBmYWxsYmFjayBmb3Igbm9uLWFycmF5LWxpa2UgRVMzIGFuZCBub24tZW51bWVyYWJsZSBvbGQgVjggc3RyaW5nc1xudmFyIGNvZiA9IHJlcXVpcmUoJy4vX2NvZicpO1xuLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lIG5vLXByb3RvdHlwZS1idWlsdGluc1xubW9kdWxlLmV4cG9ydHMgPSBPYmplY3QoJ3onKS5wcm9wZXJ0eUlzRW51bWVyYWJsZSgwKSA/IE9iamVjdCA6IGZ1bmN0aW9uIChpdCkge1xuICByZXR1cm4gY29mKGl0KSA9PSAnU3RyaW5nJyA/IGl0LnNwbGl0KCcnKSA6IE9iamVjdChpdCk7XG59O1xuIiwiLy8gY2hlY2sgb24gZGVmYXVsdCBBcnJheSBpdGVyYXRvclxudmFyIEl0ZXJhdG9ycyA9IHJlcXVpcmUoJy4vX2l0ZXJhdG9ycycpO1xudmFyIElURVJBVE9SID0gcmVxdWlyZSgnLi9fd2tzJykoJ2l0ZXJhdG9yJyk7XG52YXIgQXJyYXlQcm90byA9IEFycmF5LnByb3RvdHlwZTtcblxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAoaXQpIHtcbiAgcmV0dXJuIGl0ICE9PSB1bmRlZmluZWQgJiYgKEl0ZXJhdG9ycy5BcnJheSA9PT0gaXQgfHwgQXJyYXlQcm90b1tJVEVSQVRPUl0gPT09IGl0KTtcbn07XG4iLCJtb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIChpdCkge1xuICByZXR1cm4gdHlwZW9mIGl0ID09PSAnb2JqZWN0JyA/IGl0ICE9PSBudWxsIDogdHlwZW9mIGl0ID09PSAnZnVuY3Rpb24nO1xufTtcbiIsIi8vIGNhbGwgc29tZXRoaW5nIG9uIGl0ZXJhdG9yIHN0ZXAgd2l0aCBzYWZlIGNsb3Npbmcgb24gZXJyb3JcbnZhciBhbk9iamVjdCA9IHJlcXVpcmUoJy4vX2FuLW9iamVjdCcpO1xubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAoaXRlcmF0b3IsIGZuLCB2YWx1ZSwgZW50cmllcykge1xuICB0cnkge1xuICAgIHJldHVybiBlbnRyaWVzID8gZm4oYW5PYmplY3QodmFsdWUpWzBdLCB2YWx1ZVsxXSkgOiBmbih2YWx1ZSk7XG4gIC8vIDcuNC42IEl0ZXJhdG9yQ2xvc2UoaXRlcmF0b3IsIGNvbXBsZXRpb24pXG4gIH0gY2F0Y2ggKGUpIHtcbiAgICB2YXIgcmV0ID0gaXRlcmF0b3JbJ3JldHVybiddO1xuICAgIGlmIChyZXQgIT09IHVuZGVmaW5lZCkgYW5PYmplY3QocmV0LmNhbGwoaXRlcmF0b3IpKTtcbiAgICB0aHJvdyBlO1xuICB9XG59O1xuIiwiJ3VzZSBzdHJpY3QnO1xudmFyIGNyZWF0ZSA9IHJlcXVpcmUoJy4vX29iamVjdC1jcmVhdGUnKTtcbnZhciBkZXNjcmlwdG9yID0gcmVxdWlyZSgnLi9fcHJvcGVydHktZGVzYycpO1xudmFyIHNldFRvU3RyaW5nVGFnID0gcmVxdWlyZSgnLi9fc2V0LXRvLXN0cmluZy10YWcnKTtcbnZhciBJdGVyYXRvclByb3RvdHlwZSA9IHt9O1xuXG4vLyAyNS4xLjIuMS4xICVJdGVyYXRvclByb3RvdHlwZSVbQEBpdGVyYXRvcl0oKVxucmVxdWlyZSgnLi9faGlkZScpKEl0ZXJhdG9yUHJvdG90eXBlLCByZXF1aXJlKCcuL193a3MnKSgnaXRlcmF0b3InKSwgZnVuY3Rpb24gKCkgeyByZXR1cm4gdGhpczsgfSk7XG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKENvbnN0cnVjdG9yLCBOQU1FLCBuZXh0KSB7XG4gIENvbnN0cnVjdG9yLnByb3RvdHlwZSA9IGNyZWF0ZShJdGVyYXRvclByb3RvdHlwZSwgeyBuZXh0OiBkZXNjcmlwdG9yKDEsIG5leHQpIH0pO1xuICBzZXRUb1N0cmluZ1RhZyhDb25zdHJ1Y3RvciwgTkFNRSArICcgSXRlcmF0b3InKTtcbn07XG4iLCIndXNlIHN0cmljdCc7XG52YXIgTElCUkFSWSA9IHJlcXVpcmUoJy4vX2xpYnJhcnknKTtcbnZhciAkZXhwb3J0ID0gcmVxdWlyZSgnLi9fZXhwb3J0Jyk7XG52YXIgcmVkZWZpbmUgPSByZXF1aXJlKCcuL19yZWRlZmluZScpO1xudmFyIGhpZGUgPSByZXF1aXJlKCcuL19oaWRlJyk7XG52YXIgSXRlcmF0b3JzID0gcmVxdWlyZSgnLi9faXRlcmF0b3JzJyk7XG52YXIgJGl0ZXJDcmVhdGUgPSByZXF1aXJlKCcuL19pdGVyLWNyZWF0ZScpO1xudmFyIHNldFRvU3RyaW5nVGFnID0gcmVxdWlyZSgnLi9fc2V0LXRvLXN0cmluZy10YWcnKTtcbnZhciBnZXRQcm90b3R5cGVPZiA9IHJlcXVpcmUoJy4vX29iamVjdC1ncG8nKTtcbnZhciBJVEVSQVRPUiA9IHJlcXVpcmUoJy4vX3drcycpKCdpdGVyYXRvcicpO1xudmFyIEJVR0dZID0gIShbXS5rZXlzICYmICduZXh0JyBpbiBbXS5rZXlzKCkpOyAvLyBTYWZhcmkgaGFzIGJ1Z2d5IGl0ZXJhdG9ycyB3L28gYG5leHRgXG52YXIgRkZfSVRFUkFUT1IgPSAnQEBpdGVyYXRvcic7XG52YXIgS0VZUyA9ICdrZXlzJztcbnZhciBWQUxVRVMgPSAndmFsdWVzJztcblxudmFyIHJldHVyblRoaXMgPSBmdW5jdGlvbiAoKSB7IHJldHVybiB0aGlzOyB9O1xuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIChCYXNlLCBOQU1FLCBDb25zdHJ1Y3RvciwgbmV4dCwgREVGQVVMVCwgSVNfU0VULCBGT1JDRUQpIHtcbiAgJGl0ZXJDcmVhdGUoQ29uc3RydWN0b3IsIE5BTUUsIG5leHQpO1xuICB2YXIgZ2V0TWV0aG9kID0gZnVuY3Rpb24gKGtpbmQpIHtcbiAgICBpZiAoIUJVR0dZICYmIGtpbmQgaW4gcHJvdG8pIHJldHVybiBwcm90b1traW5kXTtcbiAgICBzd2l0Y2ggKGtpbmQpIHtcbiAgICAgIGNhc2UgS0VZUzogcmV0dXJuIGZ1bmN0aW9uIGtleXMoKSB7IHJldHVybiBuZXcgQ29uc3RydWN0b3IodGhpcywga2luZCk7IH07XG4gICAgICBjYXNlIFZBTFVFUzogcmV0dXJuIGZ1bmN0aW9uIHZhbHVlcygpIHsgcmV0dXJuIG5ldyBDb25zdHJ1Y3Rvcih0aGlzLCBraW5kKTsgfTtcbiAgICB9IHJldHVybiBmdW5jdGlvbiBlbnRyaWVzKCkgeyByZXR1cm4gbmV3IENvbnN0cnVjdG9yKHRoaXMsIGtpbmQpOyB9O1xuICB9O1xuICB2YXIgVEFHID0gTkFNRSArICcgSXRlcmF0b3InO1xuICB2YXIgREVGX1ZBTFVFUyA9IERFRkFVTFQgPT0gVkFMVUVTO1xuICB2YXIgVkFMVUVTX0JVRyA9IGZhbHNlO1xuICB2YXIgcHJvdG8gPSBCYXNlLnByb3RvdHlwZTtcbiAgdmFyICRuYXRpdmUgPSBwcm90b1tJVEVSQVRPUl0gfHwgcHJvdG9bRkZfSVRFUkFUT1JdIHx8IERFRkFVTFQgJiYgcHJvdG9bREVGQVVMVF07XG4gIHZhciAkZGVmYXVsdCA9ICRuYXRpdmUgfHwgZ2V0TWV0aG9kKERFRkFVTFQpO1xuICB2YXIgJGVudHJpZXMgPSBERUZBVUxUID8gIURFRl9WQUxVRVMgPyAkZGVmYXVsdCA6IGdldE1ldGhvZCgnZW50cmllcycpIDogdW5kZWZpbmVkO1xuICB2YXIgJGFueU5hdGl2ZSA9IE5BTUUgPT0gJ0FycmF5JyA/IHByb3RvLmVudHJpZXMgfHwgJG5hdGl2ZSA6ICRuYXRpdmU7XG4gIHZhciBtZXRob2RzLCBrZXksIEl0ZXJhdG9yUHJvdG90eXBlO1xuICAvLyBGaXggbmF0aXZlXG4gIGlmICgkYW55TmF0aXZlKSB7XG4gICAgSXRlcmF0b3JQcm90b3R5cGUgPSBnZXRQcm90b3R5cGVPZigkYW55TmF0aXZlLmNhbGwobmV3IEJhc2UoKSkpO1xuICAgIGlmIChJdGVyYXRvclByb3RvdHlwZSAhPT0gT2JqZWN0LnByb3RvdHlwZSAmJiBJdGVyYXRvclByb3RvdHlwZS5uZXh0KSB7XG4gICAgICAvLyBTZXQgQEB0b1N0cmluZ1RhZyB0byBuYXRpdmUgaXRlcmF0b3JzXG4gICAgICBzZXRUb1N0cmluZ1RhZyhJdGVyYXRvclByb3RvdHlwZSwgVEFHLCB0cnVlKTtcbiAgICAgIC8vIGZpeCBmb3Igc29tZSBvbGQgZW5naW5lc1xuICAgICAgaWYgKCFMSUJSQVJZICYmIHR5cGVvZiBJdGVyYXRvclByb3RvdHlwZVtJVEVSQVRPUl0gIT0gJ2Z1bmN0aW9uJykgaGlkZShJdGVyYXRvclByb3RvdHlwZSwgSVRFUkFUT1IsIHJldHVyblRoaXMpO1xuICAgIH1cbiAgfVxuICAvLyBmaXggQXJyYXkje3ZhbHVlcywgQEBpdGVyYXRvcn0ubmFtZSBpbiBWOCAvIEZGXG4gIGlmIChERUZfVkFMVUVTICYmICRuYXRpdmUgJiYgJG5hdGl2ZS5uYW1lICE9PSBWQUxVRVMpIHtcbiAgICBWQUxVRVNfQlVHID0gdHJ1ZTtcbiAgICAkZGVmYXVsdCA9IGZ1bmN0aW9uIHZhbHVlcygpIHsgcmV0dXJuICRuYXRpdmUuY2FsbCh0aGlzKTsgfTtcbiAgfVxuICAvLyBEZWZpbmUgaXRlcmF0b3JcbiAgaWYgKCghTElCUkFSWSB8fCBGT1JDRUQpICYmIChCVUdHWSB8fCBWQUxVRVNfQlVHIHx8ICFwcm90b1tJVEVSQVRPUl0pKSB7XG4gICAgaGlkZShwcm90bywgSVRFUkFUT1IsICRkZWZhdWx0KTtcbiAgfVxuICAvLyBQbHVnIGZvciBsaWJyYXJ5XG4gIEl0ZXJhdG9yc1tOQU1FXSA9ICRkZWZhdWx0O1xuICBJdGVyYXRvcnNbVEFHXSA9IHJldHVyblRoaXM7XG4gIGlmIChERUZBVUxUKSB7XG4gICAgbWV0aG9kcyA9IHtcbiAgICAgIHZhbHVlczogREVGX1ZBTFVFUyA/ICRkZWZhdWx0IDogZ2V0TWV0aG9kKFZBTFVFUyksXG4gICAgICBrZXlzOiBJU19TRVQgPyAkZGVmYXVsdCA6IGdldE1ldGhvZChLRVlTKSxcbiAgICAgIGVudHJpZXM6ICRlbnRyaWVzXG4gICAgfTtcbiAgICBpZiAoRk9SQ0VEKSBmb3IgKGtleSBpbiBtZXRob2RzKSB7XG4gICAgICBpZiAoIShrZXkgaW4gcHJvdG8pKSByZWRlZmluZShwcm90bywga2V5LCBtZXRob2RzW2tleV0pO1xuICAgIH0gZWxzZSAkZXhwb3J0KCRleHBvcnQuUCArICRleHBvcnQuRiAqIChCVUdHWSB8fCBWQUxVRVNfQlVHKSwgTkFNRSwgbWV0aG9kcyk7XG4gIH1cbiAgcmV0dXJuIG1ldGhvZHM7XG59O1xuIiwidmFyIElURVJBVE9SID0gcmVxdWlyZSgnLi9fd2tzJykoJ2l0ZXJhdG9yJyk7XG52YXIgU0FGRV9DTE9TSU5HID0gZmFsc2U7XG5cbnRyeSB7XG4gIHZhciByaXRlciA9IFs3XVtJVEVSQVRPUl0oKTtcbiAgcml0ZXJbJ3JldHVybiddID0gZnVuY3Rpb24gKCkgeyBTQUZFX0NMT1NJTkcgPSB0cnVlOyB9O1xuICAvLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmUgbm8tdGhyb3ctbGl0ZXJhbFxuICBBcnJheS5mcm9tKHJpdGVyLCBmdW5jdGlvbiAoKSB7IHRocm93IDI7IH0pO1xufSBjYXRjaCAoZSkgeyAvKiBlbXB0eSAqLyB9XG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKGV4ZWMsIHNraXBDbG9zaW5nKSB7XG4gIGlmICghc2tpcENsb3NpbmcgJiYgIVNBRkVfQ0xPU0lORykgcmV0dXJuIGZhbHNlO1xuICB2YXIgc2FmZSA9IGZhbHNlO1xuICB0cnkge1xuICAgIHZhciBhcnIgPSBbN107XG4gICAgdmFyIGl0ZXIgPSBhcnJbSVRFUkFUT1JdKCk7XG4gICAgaXRlci5uZXh0ID0gZnVuY3Rpb24gKCkgeyByZXR1cm4geyBkb25lOiBzYWZlID0gdHJ1ZSB9OyB9O1xuICAgIGFycltJVEVSQVRPUl0gPSBmdW5jdGlvbiAoKSB7IHJldHVybiBpdGVyOyB9O1xuICAgIGV4ZWMoYXJyKTtcbiAgfSBjYXRjaCAoZSkgeyAvKiBlbXB0eSAqLyB9XG4gIHJldHVybiBzYWZlO1xufTtcbiIsIm1vZHVsZS5leHBvcnRzID0ge307XG4iLCJtb2R1bGUuZXhwb3J0cyA9IHRydWU7XG4iLCIvLyAxOS4xLjIuMiAvIDE1LjIuMy41IE9iamVjdC5jcmVhdGUoTyBbLCBQcm9wZXJ0aWVzXSlcbnZhciBhbk9iamVjdCA9IHJlcXVpcmUoJy4vX2FuLW9iamVjdCcpO1xudmFyIGRQcyA9IHJlcXVpcmUoJy4vX29iamVjdC1kcHMnKTtcbnZhciBlbnVtQnVnS2V5cyA9IHJlcXVpcmUoJy4vX2VudW0tYnVnLWtleXMnKTtcbnZhciBJRV9QUk9UTyA9IHJlcXVpcmUoJy4vX3NoYXJlZC1rZXknKSgnSUVfUFJPVE8nKTtcbnZhciBFbXB0eSA9IGZ1bmN0aW9uICgpIHsgLyogZW1wdHkgKi8gfTtcbnZhciBQUk9UT1RZUEUgPSAncHJvdG90eXBlJztcblxuLy8gQ3JlYXRlIG9iamVjdCB3aXRoIGZha2UgYG51bGxgIHByb3RvdHlwZTogdXNlIGlmcmFtZSBPYmplY3Qgd2l0aCBjbGVhcmVkIHByb3RvdHlwZVxudmFyIGNyZWF0ZURpY3QgPSBmdW5jdGlvbiAoKSB7XG4gIC8vIFRocmFzaCwgd2FzdGUgYW5kIHNvZG9teTogSUUgR0MgYnVnXG4gIHZhciBpZnJhbWUgPSByZXF1aXJlKCcuL19kb20tY3JlYXRlJykoJ2lmcmFtZScpO1xuICB2YXIgaSA9IGVudW1CdWdLZXlzLmxlbmd0aDtcbiAgdmFyIGx0ID0gJzwnO1xuICB2YXIgZ3QgPSAnPic7XG4gIHZhciBpZnJhbWVEb2N1bWVudDtcbiAgaWZyYW1lLnN0eWxlLmRpc3BsYXkgPSAnbm9uZSc7XG4gIHJlcXVpcmUoJy4vX2h0bWwnKS5hcHBlbmRDaGlsZChpZnJhbWUpO1xuICBpZnJhbWUuc3JjID0gJ2phdmFzY3JpcHQ6JzsgLy8gZXNsaW50LWRpc2FibGUtbGluZSBuby1zY3JpcHQtdXJsXG4gIC8vIGNyZWF0ZURpY3QgPSBpZnJhbWUuY29udGVudFdpbmRvdy5PYmplY3Q7XG4gIC8vIGh0bWwucmVtb3ZlQ2hpbGQoaWZyYW1lKTtcbiAgaWZyYW1lRG9jdW1lbnQgPSBpZnJhbWUuY29udGVudFdpbmRvdy5kb2N1bWVudDtcbiAgaWZyYW1lRG9jdW1lbnQub3BlbigpO1xuICBpZnJhbWVEb2N1bWVudC53cml0ZShsdCArICdzY3JpcHQnICsgZ3QgKyAnZG9jdW1lbnQuRj1PYmplY3QnICsgbHQgKyAnL3NjcmlwdCcgKyBndCk7XG4gIGlmcmFtZURvY3VtZW50LmNsb3NlKCk7XG4gIGNyZWF0ZURpY3QgPSBpZnJhbWVEb2N1bWVudC5GO1xuICB3aGlsZSAoaS0tKSBkZWxldGUgY3JlYXRlRGljdFtQUk9UT1RZUEVdW2VudW1CdWdLZXlzW2ldXTtcbiAgcmV0dXJuIGNyZWF0ZURpY3QoKTtcbn07XG5cbm1vZHVsZS5leHBvcnRzID0gT2JqZWN0LmNyZWF0ZSB8fCBmdW5jdGlvbiBjcmVhdGUoTywgUHJvcGVydGllcykge1xuICB2YXIgcmVzdWx0O1xuICBpZiAoTyAhPT0gbnVsbCkge1xuICAgIEVtcHR5W1BST1RPVFlQRV0gPSBhbk9iamVjdChPKTtcbiAgICByZXN1bHQgPSBuZXcgRW1wdHkoKTtcbiAgICBFbXB0eVtQUk9UT1RZUEVdID0gbnVsbDtcbiAgICAvLyBhZGQgXCJfX3Byb3RvX19cIiBmb3IgT2JqZWN0LmdldFByb3RvdHlwZU9mIHBvbHlmaWxsXG4gICAgcmVzdWx0W0lFX1BST1RPXSA9IE87XG4gIH0gZWxzZSByZXN1bHQgPSBjcmVhdGVEaWN0KCk7XG4gIHJldHVybiBQcm9wZXJ0aWVzID09PSB1bmRlZmluZWQgPyByZXN1bHQgOiBkUHMocmVzdWx0LCBQcm9wZXJ0aWVzKTtcbn07XG4iLCJ2YXIgYW5PYmplY3QgPSByZXF1aXJlKCcuL19hbi1vYmplY3QnKTtcbnZhciBJRThfRE9NX0RFRklORSA9IHJlcXVpcmUoJy4vX2llOC1kb20tZGVmaW5lJyk7XG52YXIgdG9QcmltaXRpdmUgPSByZXF1aXJlKCcuL190by1wcmltaXRpdmUnKTtcbnZhciBkUCA9IE9iamVjdC5kZWZpbmVQcm9wZXJ0eTtcblxuZXhwb3J0cy5mID0gcmVxdWlyZSgnLi9fZGVzY3JpcHRvcnMnKSA/IE9iamVjdC5kZWZpbmVQcm9wZXJ0eSA6IGZ1bmN0aW9uIGRlZmluZVByb3BlcnR5KE8sIFAsIEF0dHJpYnV0ZXMpIHtcbiAgYW5PYmplY3QoTyk7XG4gIFAgPSB0b1ByaW1pdGl2ZShQLCB0cnVlKTtcbiAgYW5PYmplY3QoQXR0cmlidXRlcyk7XG4gIGlmIChJRThfRE9NX0RFRklORSkgdHJ5IHtcbiAgICByZXR1cm4gZFAoTywgUCwgQXR0cmlidXRlcyk7XG4gIH0gY2F0Y2ggKGUpIHsgLyogZW1wdHkgKi8gfVxuICBpZiAoJ2dldCcgaW4gQXR0cmlidXRlcyB8fCAnc2V0JyBpbiBBdHRyaWJ1dGVzKSB0aHJvdyBUeXBlRXJyb3IoJ0FjY2Vzc29ycyBub3Qgc3VwcG9ydGVkIScpO1xuICBpZiAoJ3ZhbHVlJyBpbiBBdHRyaWJ1dGVzKSBPW1BdID0gQXR0cmlidXRlcy52YWx1ZTtcbiAgcmV0dXJuIE87XG59O1xuIiwidmFyIGRQID0gcmVxdWlyZSgnLi9fb2JqZWN0LWRwJyk7XG52YXIgYW5PYmplY3QgPSByZXF1aXJlKCcuL19hbi1vYmplY3QnKTtcbnZhciBnZXRLZXlzID0gcmVxdWlyZSgnLi9fb2JqZWN0LWtleXMnKTtcblxubW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKCcuL19kZXNjcmlwdG9ycycpID8gT2JqZWN0LmRlZmluZVByb3BlcnRpZXMgOiBmdW5jdGlvbiBkZWZpbmVQcm9wZXJ0aWVzKE8sIFByb3BlcnRpZXMpIHtcbiAgYW5PYmplY3QoTyk7XG4gIHZhciBrZXlzID0gZ2V0S2V5cyhQcm9wZXJ0aWVzKTtcbiAgdmFyIGxlbmd0aCA9IGtleXMubGVuZ3RoO1xuICB2YXIgaSA9IDA7XG4gIHZhciBQO1xuICB3aGlsZSAobGVuZ3RoID4gaSkgZFAuZihPLCBQID0ga2V5c1tpKytdLCBQcm9wZXJ0aWVzW1BdKTtcbiAgcmV0dXJuIE87XG59O1xuIiwiLy8gMTkuMS4yLjkgLyAxNS4yLjMuMiBPYmplY3QuZ2V0UHJvdG90eXBlT2YoTylcbnZhciBoYXMgPSByZXF1aXJlKCcuL19oYXMnKTtcbnZhciB0b09iamVjdCA9IHJlcXVpcmUoJy4vX3RvLW9iamVjdCcpO1xudmFyIElFX1BST1RPID0gcmVxdWlyZSgnLi9fc2hhcmVkLWtleScpKCdJRV9QUk9UTycpO1xudmFyIE9iamVjdFByb3RvID0gT2JqZWN0LnByb3RvdHlwZTtcblxubW9kdWxlLmV4cG9ydHMgPSBPYmplY3QuZ2V0UHJvdG90eXBlT2YgfHwgZnVuY3Rpb24gKE8pIHtcbiAgTyA9IHRvT2JqZWN0KE8pO1xuICBpZiAoaGFzKE8sIElFX1BST1RPKSkgcmV0dXJuIE9bSUVfUFJPVE9dO1xuICBpZiAodHlwZW9mIE8uY29uc3RydWN0b3IgPT0gJ2Z1bmN0aW9uJyAmJiBPIGluc3RhbmNlb2YgTy5jb25zdHJ1Y3Rvcikge1xuICAgIHJldHVybiBPLmNvbnN0cnVjdG9yLnByb3RvdHlwZTtcbiAgfSByZXR1cm4gTyBpbnN0YW5jZW9mIE9iamVjdCA/IE9iamVjdFByb3RvIDogbnVsbDtcbn07XG4iLCJ2YXIgaGFzID0gcmVxdWlyZSgnLi9faGFzJyk7XG52YXIgdG9JT2JqZWN0ID0gcmVxdWlyZSgnLi9fdG8taW9iamVjdCcpO1xudmFyIGFycmF5SW5kZXhPZiA9IHJlcXVpcmUoJy4vX2FycmF5LWluY2x1ZGVzJykoZmFsc2UpO1xudmFyIElFX1BST1RPID0gcmVxdWlyZSgnLi9fc2hhcmVkLWtleScpKCdJRV9QUk9UTycpO1xuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIChvYmplY3QsIG5hbWVzKSB7XG4gIHZhciBPID0gdG9JT2JqZWN0KG9iamVjdCk7XG4gIHZhciBpID0gMDtcbiAgdmFyIHJlc3VsdCA9IFtdO1xuICB2YXIga2V5O1xuICBmb3IgKGtleSBpbiBPKSBpZiAoa2V5ICE9IElFX1BST1RPKSBoYXMoTywga2V5KSAmJiByZXN1bHQucHVzaChrZXkpO1xuICAvLyBEb24ndCBlbnVtIGJ1ZyAmIGhpZGRlbiBrZXlzXG4gIHdoaWxlIChuYW1lcy5sZW5ndGggPiBpKSBpZiAoaGFzKE8sIGtleSA9IG5hbWVzW2krK10pKSB7XG4gICAgfmFycmF5SW5kZXhPZihyZXN1bHQsIGtleSkgfHwgcmVzdWx0LnB1c2goa2V5KTtcbiAgfVxuICByZXR1cm4gcmVzdWx0O1xufTtcbiIsIi8vIDE5LjEuMi4xNCAvIDE1LjIuMy4xNCBPYmplY3Qua2V5cyhPKVxudmFyICRrZXlzID0gcmVxdWlyZSgnLi9fb2JqZWN0LWtleXMtaW50ZXJuYWwnKTtcbnZhciBlbnVtQnVnS2V5cyA9IHJlcXVpcmUoJy4vX2VudW0tYnVnLWtleXMnKTtcblxubW9kdWxlLmV4cG9ydHMgPSBPYmplY3Qua2V5cyB8fCBmdW5jdGlvbiBrZXlzKE8pIHtcbiAgcmV0dXJuICRrZXlzKE8sIGVudW1CdWdLZXlzKTtcbn07XG4iLCIvLyBtb3N0IE9iamVjdCBtZXRob2RzIGJ5IEVTNiBzaG91bGQgYWNjZXB0IHByaW1pdGl2ZXNcbnZhciAkZXhwb3J0ID0gcmVxdWlyZSgnLi9fZXhwb3J0Jyk7XG52YXIgY29yZSA9IHJlcXVpcmUoJy4vX2NvcmUnKTtcbnZhciBmYWlscyA9IHJlcXVpcmUoJy4vX2ZhaWxzJyk7XG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIChLRVksIGV4ZWMpIHtcbiAgdmFyIGZuID0gKGNvcmUuT2JqZWN0IHx8IHt9KVtLRVldIHx8IE9iamVjdFtLRVldO1xuICB2YXIgZXhwID0ge307XG4gIGV4cFtLRVldID0gZXhlYyhmbik7XG4gICRleHBvcnQoJGV4cG9ydC5TICsgJGV4cG9ydC5GICogZmFpbHMoZnVuY3Rpb24gKCkgeyBmbigxKTsgfSksICdPYmplY3QnLCBleHApO1xufTtcbiIsIm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKGJpdG1hcCwgdmFsdWUpIHtcbiAgcmV0dXJuIHtcbiAgICBlbnVtZXJhYmxlOiAhKGJpdG1hcCAmIDEpLFxuICAgIGNvbmZpZ3VyYWJsZTogIShiaXRtYXAgJiAyKSxcbiAgICB3cml0YWJsZTogIShiaXRtYXAgJiA0KSxcbiAgICB2YWx1ZTogdmFsdWVcbiAgfTtcbn07XG4iLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoJy4vX2hpZGUnKTtcbiIsInZhciBkZWYgPSByZXF1aXJlKCcuL19vYmplY3QtZHAnKS5mO1xudmFyIGhhcyA9IHJlcXVpcmUoJy4vX2hhcycpO1xudmFyIFRBRyA9IHJlcXVpcmUoJy4vX3drcycpKCd0b1N0cmluZ1RhZycpO1xuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIChpdCwgdGFnLCBzdGF0KSB7XG4gIGlmIChpdCAmJiAhaGFzKGl0ID0gc3RhdCA/IGl0IDogaXQucHJvdG90eXBlLCBUQUcpKSBkZWYoaXQsIFRBRywgeyBjb25maWd1cmFibGU6IHRydWUsIHZhbHVlOiB0YWcgfSk7XG59O1xuIiwidmFyIHNoYXJlZCA9IHJlcXVpcmUoJy4vX3NoYXJlZCcpKCdrZXlzJyk7XG52YXIgdWlkID0gcmVxdWlyZSgnLi9fdWlkJyk7XG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIChrZXkpIHtcbiAgcmV0dXJuIHNoYXJlZFtrZXldIHx8IChzaGFyZWRba2V5XSA9IHVpZChrZXkpKTtcbn07XG4iLCJ2YXIgY29yZSA9IHJlcXVpcmUoJy4vX2NvcmUnKTtcbnZhciBnbG9iYWwgPSByZXF1aXJlKCcuL19nbG9iYWwnKTtcbnZhciBTSEFSRUQgPSAnX19jb3JlLWpzX3NoYXJlZF9fJztcbnZhciBzdG9yZSA9IGdsb2JhbFtTSEFSRURdIHx8IChnbG9iYWxbU0hBUkVEXSA9IHt9KTtcblxuKG1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKGtleSwgdmFsdWUpIHtcbiAgcmV0dXJuIHN0b3JlW2tleV0gfHwgKHN0b3JlW2tleV0gPSB2YWx1ZSAhPT0gdW5kZWZpbmVkID8gdmFsdWUgOiB7fSk7XG59KSgndmVyc2lvbnMnLCBbXSkucHVzaCh7XG4gIHZlcnNpb246IGNvcmUudmVyc2lvbixcbiAgbW9kZTogcmVxdWlyZSgnLi9fbGlicmFyeScpID8gJ3B1cmUnIDogJ2dsb2JhbCcsXG4gIGNvcHlyaWdodDogJ8KpIDIwMTkgRGVuaXMgUHVzaGthcmV2ICh6bG9pcm9jay5ydSknXG59KTtcbiIsInZhciB0b0ludGVnZXIgPSByZXF1aXJlKCcuL190by1pbnRlZ2VyJyk7XG52YXIgZGVmaW5lZCA9IHJlcXVpcmUoJy4vX2RlZmluZWQnKTtcbi8vIHRydWUgIC0+IFN0cmluZyNhdFxuLy8gZmFsc2UgLT4gU3RyaW5nI2NvZGVQb2ludEF0XG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIChUT19TVFJJTkcpIHtcbiAgcmV0dXJuIGZ1bmN0aW9uICh0aGF0LCBwb3MpIHtcbiAgICB2YXIgcyA9IFN0cmluZyhkZWZpbmVkKHRoYXQpKTtcbiAgICB2YXIgaSA9IHRvSW50ZWdlcihwb3MpO1xuICAgIHZhciBsID0gcy5sZW5ndGg7XG4gICAgdmFyIGEsIGI7XG4gICAgaWYgKGkgPCAwIHx8IGkgPj0gbCkgcmV0dXJuIFRPX1NUUklORyA/ICcnIDogdW5kZWZpbmVkO1xuICAgIGEgPSBzLmNoYXJDb2RlQXQoaSk7XG4gICAgcmV0dXJuIGEgPCAweGQ4MDAgfHwgYSA+IDB4ZGJmZiB8fCBpICsgMSA9PT0gbCB8fCAoYiA9IHMuY2hhckNvZGVBdChpICsgMSkpIDwgMHhkYzAwIHx8IGIgPiAweGRmZmZcbiAgICAgID8gVE9fU1RSSU5HID8gcy5jaGFyQXQoaSkgOiBhXG4gICAgICA6IFRPX1NUUklORyA/IHMuc2xpY2UoaSwgaSArIDIpIDogKGEgLSAweGQ4MDAgPDwgMTApICsgKGIgLSAweGRjMDApICsgMHgxMDAwMDtcbiAgfTtcbn07XG4iLCJ2YXIgdG9JbnRlZ2VyID0gcmVxdWlyZSgnLi9fdG8taW50ZWdlcicpO1xudmFyIG1heCA9IE1hdGgubWF4O1xudmFyIG1pbiA9IE1hdGgubWluO1xubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAoaW5kZXgsIGxlbmd0aCkge1xuICBpbmRleCA9IHRvSW50ZWdlcihpbmRleCk7XG4gIHJldHVybiBpbmRleCA8IDAgPyBtYXgoaW5kZXggKyBsZW5ndGgsIDApIDogbWluKGluZGV4LCBsZW5ndGgpO1xufTtcbiIsIi8vIDcuMS40IFRvSW50ZWdlclxudmFyIGNlaWwgPSBNYXRoLmNlaWw7XG52YXIgZmxvb3IgPSBNYXRoLmZsb29yO1xubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAoaXQpIHtcbiAgcmV0dXJuIGlzTmFOKGl0ID0gK2l0KSA/IDAgOiAoaXQgPiAwID8gZmxvb3IgOiBjZWlsKShpdCk7XG59O1xuIiwiLy8gdG8gaW5kZXhlZCBvYmplY3QsIHRvT2JqZWN0IHdpdGggZmFsbGJhY2sgZm9yIG5vbi1hcnJheS1saWtlIEVTMyBzdHJpbmdzXG52YXIgSU9iamVjdCA9IHJlcXVpcmUoJy4vX2lvYmplY3QnKTtcbnZhciBkZWZpbmVkID0gcmVxdWlyZSgnLi9fZGVmaW5lZCcpO1xubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAoaXQpIHtcbiAgcmV0dXJuIElPYmplY3QoZGVmaW5lZChpdCkpO1xufTtcbiIsIi8vIDcuMS4xNSBUb0xlbmd0aFxudmFyIHRvSW50ZWdlciA9IHJlcXVpcmUoJy4vX3RvLWludGVnZXInKTtcbnZhciBtaW4gPSBNYXRoLm1pbjtcbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKGl0KSB7XG4gIHJldHVybiBpdCA+IDAgPyBtaW4odG9JbnRlZ2VyKGl0KSwgMHgxZmZmZmZmZmZmZmZmZikgOiAwOyAvLyBwb3coMiwgNTMpIC0gMSA9PSA5MDA3MTk5MjU0NzQwOTkxXG59O1xuIiwiLy8gNy4xLjEzIFRvT2JqZWN0KGFyZ3VtZW50KVxudmFyIGRlZmluZWQgPSByZXF1aXJlKCcuL19kZWZpbmVkJyk7XG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIChpdCkge1xuICByZXR1cm4gT2JqZWN0KGRlZmluZWQoaXQpKTtcbn07XG4iLCIvLyA3LjEuMSBUb1ByaW1pdGl2ZShpbnB1dCBbLCBQcmVmZXJyZWRUeXBlXSlcbnZhciBpc09iamVjdCA9IHJlcXVpcmUoJy4vX2lzLW9iamVjdCcpO1xuLy8gaW5zdGVhZCBvZiB0aGUgRVM2IHNwZWMgdmVyc2lvbiwgd2UgZGlkbid0IGltcGxlbWVudCBAQHRvUHJpbWl0aXZlIGNhc2Vcbi8vIGFuZCB0aGUgc2Vjb25kIGFyZ3VtZW50IC0gZmxhZyAtIHByZWZlcnJlZCB0eXBlIGlzIGEgc3RyaW5nXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIChpdCwgUykge1xuICBpZiAoIWlzT2JqZWN0KGl0KSkgcmV0dXJuIGl0O1xuICB2YXIgZm4sIHZhbDtcbiAgaWYgKFMgJiYgdHlwZW9mIChmbiA9IGl0LnRvU3RyaW5nKSA9PSAnZnVuY3Rpb24nICYmICFpc09iamVjdCh2YWwgPSBmbi5jYWxsKGl0KSkpIHJldHVybiB2YWw7XG4gIGlmICh0eXBlb2YgKGZuID0gaXQudmFsdWVPZikgPT0gJ2Z1bmN0aW9uJyAmJiAhaXNPYmplY3QodmFsID0gZm4uY2FsbChpdCkpKSByZXR1cm4gdmFsO1xuICBpZiAoIVMgJiYgdHlwZW9mIChmbiA9IGl0LnRvU3RyaW5nKSA9PSAnZnVuY3Rpb24nICYmICFpc09iamVjdCh2YWwgPSBmbi5jYWxsKGl0KSkpIHJldHVybiB2YWw7XG4gIHRocm93IFR5cGVFcnJvcihcIkNhbid0IGNvbnZlcnQgb2JqZWN0IHRvIHByaW1pdGl2ZSB2YWx1ZVwiKTtcbn07XG4iLCJ2YXIgaWQgPSAwO1xudmFyIHB4ID0gTWF0aC5yYW5kb20oKTtcbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKGtleSkge1xuICByZXR1cm4gJ1N5bWJvbCgnLmNvbmNhdChrZXkgPT09IHVuZGVmaW5lZCA/ICcnIDoga2V5LCAnKV8nLCAoKytpZCArIHB4KS50b1N0cmluZygzNikpO1xufTtcbiIsInZhciBzdG9yZSA9IHJlcXVpcmUoJy4vX3NoYXJlZCcpKCd3a3MnKTtcbnZhciB1aWQgPSByZXF1aXJlKCcuL191aWQnKTtcbnZhciBTeW1ib2wgPSByZXF1aXJlKCcuL19nbG9iYWwnKS5TeW1ib2w7XG52YXIgVVNFX1NZTUJPTCA9IHR5cGVvZiBTeW1ib2wgPT0gJ2Z1bmN0aW9uJztcblxudmFyICRleHBvcnRzID0gbW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAobmFtZSkge1xuICByZXR1cm4gc3RvcmVbbmFtZV0gfHwgKHN0b3JlW25hbWVdID1cbiAgICBVU0VfU1lNQk9MICYmIFN5bWJvbFtuYW1lXSB8fCAoVVNFX1NZTUJPTCA/IFN5bWJvbCA6IHVpZCkoJ1N5bWJvbC4nICsgbmFtZSkpO1xufTtcblxuJGV4cG9ydHMuc3RvcmUgPSBzdG9yZTtcbiIsInZhciBjbGFzc29mID0gcmVxdWlyZSgnLi9fY2xhc3NvZicpO1xudmFyIElURVJBVE9SID0gcmVxdWlyZSgnLi9fd2tzJykoJ2l0ZXJhdG9yJyk7XG52YXIgSXRlcmF0b3JzID0gcmVxdWlyZSgnLi9faXRlcmF0b3JzJyk7XG5tb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoJy4vX2NvcmUnKS5nZXRJdGVyYXRvck1ldGhvZCA9IGZ1bmN0aW9uIChpdCkge1xuICBpZiAoaXQgIT0gdW5kZWZpbmVkKSByZXR1cm4gaXRbSVRFUkFUT1JdXG4gICAgfHwgaXRbJ0BAaXRlcmF0b3InXVxuICAgIHx8IEl0ZXJhdG9yc1tjbGFzc29mKGl0KV07XG59O1xuIiwiJ3VzZSBzdHJpY3QnO1xudmFyIGN0eCA9IHJlcXVpcmUoJy4vX2N0eCcpO1xudmFyICRleHBvcnQgPSByZXF1aXJlKCcuL19leHBvcnQnKTtcbnZhciB0b09iamVjdCA9IHJlcXVpcmUoJy4vX3RvLW9iamVjdCcpO1xudmFyIGNhbGwgPSByZXF1aXJlKCcuL19pdGVyLWNhbGwnKTtcbnZhciBpc0FycmF5SXRlciA9IHJlcXVpcmUoJy4vX2lzLWFycmF5LWl0ZXInKTtcbnZhciB0b0xlbmd0aCA9IHJlcXVpcmUoJy4vX3RvLWxlbmd0aCcpO1xudmFyIGNyZWF0ZVByb3BlcnR5ID0gcmVxdWlyZSgnLi9fY3JlYXRlLXByb3BlcnR5Jyk7XG52YXIgZ2V0SXRlckZuID0gcmVxdWlyZSgnLi9jb3JlLmdldC1pdGVyYXRvci1tZXRob2QnKTtcblxuJGV4cG9ydCgkZXhwb3J0LlMgKyAkZXhwb3J0LkYgKiAhcmVxdWlyZSgnLi9faXRlci1kZXRlY3QnKShmdW5jdGlvbiAoaXRlcikgeyBBcnJheS5mcm9tKGl0ZXIpOyB9KSwgJ0FycmF5Jywge1xuICAvLyAyMi4xLjIuMSBBcnJheS5mcm9tKGFycmF5TGlrZSwgbWFwZm4gPSB1bmRlZmluZWQsIHRoaXNBcmcgPSB1bmRlZmluZWQpXG4gIGZyb206IGZ1bmN0aW9uIGZyb20oYXJyYXlMaWtlIC8qICwgbWFwZm4gPSB1bmRlZmluZWQsIHRoaXNBcmcgPSB1bmRlZmluZWQgKi8pIHtcbiAgICB2YXIgTyA9IHRvT2JqZWN0KGFycmF5TGlrZSk7XG4gICAgdmFyIEMgPSB0eXBlb2YgdGhpcyA9PSAnZnVuY3Rpb24nID8gdGhpcyA6IEFycmF5O1xuICAgIHZhciBhTGVuID0gYXJndW1lbnRzLmxlbmd0aDtcbiAgICB2YXIgbWFwZm4gPSBhTGVuID4gMSA/IGFyZ3VtZW50c1sxXSA6IHVuZGVmaW5lZDtcbiAgICB2YXIgbWFwcGluZyA9IG1hcGZuICE9PSB1bmRlZmluZWQ7XG4gICAgdmFyIGluZGV4ID0gMDtcbiAgICB2YXIgaXRlckZuID0gZ2V0SXRlckZuKE8pO1xuICAgIHZhciBsZW5ndGgsIHJlc3VsdCwgc3RlcCwgaXRlcmF0b3I7XG4gICAgaWYgKG1hcHBpbmcpIG1hcGZuID0gY3R4KG1hcGZuLCBhTGVuID4gMiA/IGFyZ3VtZW50c1syXSA6IHVuZGVmaW5lZCwgMik7XG4gICAgLy8gaWYgb2JqZWN0IGlzbid0IGl0ZXJhYmxlIG9yIGl0J3MgYXJyYXkgd2l0aCBkZWZhdWx0IGl0ZXJhdG9yIC0gdXNlIHNpbXBsZSBjYXNlXG4gICAgaWYgKGl0ZXJGbiAhPSB1bmRlZmluZWQgJiYgIShDID09IEFycmF5ICYmIGlzQXJyYXlJdGVyKGl0ZXJGbikpKSB7XG4gICAgICBmb3IgKGl0ZXJhdG9yID0gaXRlckZuLmNhbGwoTyksIHJlc3VsdCA9IG5ldyBDKCk7ICEoc3RlcCA9IGl0ZXJhdG9yLm5leHQoKSkuZG9uZTsgaW5kZXgrKykge1xuICAgICAgICBjcmVhdGVQcm9wZXJ0eShyZXN1bHQsIGluZGV4LCBtYXBwaW5nID8gY2FsbChpdGVyYXRvciwgbWFwZm4sIFtzdGVwLnZhbHVlLCBpbmRleF0sIHRydWUpIDogc3RlcC52YWx1ZSk7XG4gICAgICB9XG4gICAgfSBlbHNlIHtcbiAgICAgIGxlbmd0aCA9IHRvTGVuZ3RoKE8ubGVuZ3RoKTtcbiAgICAgIGZvciAocmVzdWx0ID0gbmV3IEMobGVuZ3RoKTsgbGVuZ3RoID4gaW5kZXg7IGluZGV4KyspIHtcbiAgICAgICAgY3JlYXRlUHJvcGVydHkocmVzdWx0LCBpbmRleCwgbWFwcGluZyA/IG1hcGZuKE9baW5kZXhdLCBpbmRleCkgOiBPW2luZGV4XSk7XG4gICAgICB9XG4gICAgfVxuICAgIHJlc3VsdC5sZW5ndGggPSBpbmRleDtcbiAgICByZXR1cm4gcmVzdWx0O1xuICB9XG59KTtcbiIsIi8vIDIwLjEuMi40IE51bWJlci5pc05hTihudW1iZXIpXG52YXIgJGV4cG9ydCA9IHJlcXVpcmUoJy4vX2V4cG9ydCcpO1xuXG4kZXhwb3J0KCRleHBvcnQuUywgJ051bWJlcicsIHtcbiAgaXNOYU46IGZ1bmN0aW9uIGlzTmFOKG51bWJlcikge1xuICAgIC8vIGVzbGludC1kaXNhYmxlLW5leHQtbGluZSBuby1zZWxmLWNvbXBhcmVcbiAgICByZXR1cm4gbnVtYmVyICE9IG51bWJlcjtcbiAgfVxufSk7XG4iLCJ2YXIgJGV4cG9ydCA9IHJlcXVpcmUoJy4vX2V4cG9ydCcpO1xuLy8gMTkuMS4yLjQgLyAxNS4yLjMuNiBPYmplY3QuZGVmaW5lUHJvcGVydHkoTywgUCwgQXR0cmlidXRlcylcbiRleHBvcnQoJGV4cG9ydC5TICsgJGV4cG9ydC5GICogIXJlcXVpcmUoJy4vX2Rlc2NyaXB0b3JzJyksICdPYmplY3QnLCB7IGRlZmluZVByb3BlcnR5OiByZXF1aXJlKCcuL19vYmplY3QtZHAnKS5mIH0pO1xuIiwiLy8gMTkuMS4yLjE0IE9iamVjdC5rZXlzKE8pXG52YXIgdG9PYmplY3QgPSByZXF1aXJlKCcuL190by1vYmplY3QnKTtcbnZhciAka2V5cyA9IHJlcXVpcmUoJy4vX29iamVjdC1rZXlzJyk7XG5cbnJlcXVpcmUoJy4vX29iamVjdC1zYXAnKSgna2V5cycsIGZ1bmN0aW9uICgpIHtcbiAgcmV0dXJuIGZ1bmN0aW9uIGtleXMoaXQpIHtcbiAgICByZXR1cm4gJGtleXModG9PYmplY3QoaXQpKTtcbiAgfTtcbn0pO1xuIiwiJ3VzZSBzdHJpY3QnO1xudmFyICRhdCA9IHJlcXVpcmUoJy4vX3N0cmluZy1hdCcpKHRydWUpO1xuXG4vLyAyMS4xLjMuMjcgU3RyaW5nLnByb3RvdHlwZVtAQGl0ZXJhdG9yXSgpXG5yZXF1aXJlKCcuL19pdGVyLWRlZmluZScpKFN0cmluZywgJ1N0cmluZycsIGZ1bmN0aW9uIChpdGVyYXRlZCkge1xuICB0aGlzLl90ID0gU3RyaW5nKGl0ZXJhdGVkKTsgLy8gdGFyZ2V0XG4gIHRoaXMuX2kgPSAwOyAgICAgICAgICAgICAgICAvLyBuZXh0IGluZGV4XG4vLyAyMS4xLjUuMi4xICVTdHJpbmdJdGVyYXRvclByb3RvdHlwZSUubmV4dCgpXG59LCBmdW5jdGlvbiAoKSB7XG4gIHZhciBPID0gdGhpcy5fdDtcbiAgdmFyIGluZGV4ID0gdGhpcy5faTtcbiAgdmFyIHBvaW50O1xuICBpZiAoaW5kZXggPj0gTy5sZW5ndGgpIHJldHVybiB7IHZhbHVlOiB1bmRlZmluZWQsIGRvbmU6IHRydWUgfTtcbiAgcG9pbnQgPSAkYXQoTywgaW5kZXgpO1xuICB0aGlzLl9pICs9IHBvaW50Lmxlbmd0aDtcbiAgcmV0dXJuIHsgdmFsdWU6IHBvaW50LCBkb25lOiBmYWxzZSB9O1xufSk7XG4iLCJtb2R1bGUuZXhwb3J0cyA9IHdpbmRvd1tcImpRdWVyeVwiXTsiLCIvLyBUaGUgbW9kdWxlIGNhY2hlXG52YXIgX193ZWJwYWNrX21vZHVsZV9jYWNoZV9fID0ge307XG5cbi8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG5mdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuXHR2YXIgY2FjaGVkTW9kdWxlID0gX193ZWJwYWNrX21vZHVsZV9jYWNoZV9fW21vZHVsZUlkXTtcblx0aWYgKGNhY2hlZE1vZHVsZSAhPT0gdW5kZWZpbmVkKSB7XG5cdFx0cmV0dXJuIGNhY2hlZE1vZHVsZS5leHBvcnRzO1xuXHR9XG5cdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG5cdHZhciBtb2R1bGUgPSBfX3dlYnBhY2tfbW9kdWxlX2NhY2hlX19bbW9kdWxlSWRdID0ge1xuXHRcdC8vIG5vIG1vZHVsZS5pZCBuZWVkZWRcblx0XHQvLyBubyBtb2R1bGUubG9hZGVkIG5lZWRlZFxuXHRcdGV4cG9ydHM6IHt9XG5cdH07XG5cblx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG5cdF9fd2VicGFja19tb2R1bGVzX19bbW9kdWxlSWRdKG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG5cdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG5cdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbn1cblxuIiwiLyoqXG4gKiBDb3B5cmlnaHQgc2luY2UgMjAwNyBQcmVzdGFTaG9wIFNBIGFuZCBDb250cmlidXRvcnNcbiAqIFByZXN0YVNob3AgaXMgYW4gSW50ZXJuYXRpb25hbCBSZWdpc3RlcmVkIFRyYWRlbWFyayAmIFByb3BlcnR5IG9mIFByZXN0YVNob3AgU0FcbiAqXG4gKiBOT1RJQ0UgT0YgTElDRU5TRVxuICpcbiAqIFRoaXMgc291cmNlIGZpbGUgaXMgc3ViamVjdCB0byB0aGUgT3BlbiBTb2Z0d2FyZSBMaWNlbnNlIChPU0wgMy4wKVxuICogdGhhdCBpcyBidW5kbGVkIHdpdGggdGhpcyBwYWNrYWdlIGluIHRoZSBmaWxlIExJQ0VOU0UubWQuXG4gKiBJdCBpcyBhbHNvIGF2YWlsYWJsZSB0aHJvdWdoIHRoZSB3b3JsZC13aWRlLXdlYiBhdCB0aGlzIFVSTDpcbiAqIGh0dHBzOi8vb3BlbnNvdXJjZS5vcmcvbGljZW5zZXMvT1NMLTMuMFxuICogSWYgeW91IGRpZCBub3QgcmVjZWl2ZSBhIGNvcHkgb2YgdGhlIGxpY2Vuc2UgYW5kIGFyZSB1bmFibGUgdG9cbiAqIG9idGFpbiBpdCB0aHJvdWdoIHRoZSB3b3JsZC13aWRlLXdlYiwgcGxlYXNlIHNlbmQgYW4gZW1haWxcbiAqIHRvIGxpY2Vuc2VAcHJlc3Rhc2hvcC5jb20gc28gd2UgY2FuIHNlbmQgeW91IGEgY29weSBpbW1lZGlhdGVseS5cbiAqXG4gKiBESVNDTEFJTUVSXG4gKlxuICogRG8gbm90IGVkaXQgb3IgYWRkIHRvIHRoaXMgZmlsZSBpZiB5b3Ugd2lzaCB0byB1cGdyYWRlIFByZXN0YVNob3AgdG8gbmV3ZXJcbiAqIHZlcnNpb25zIGluIHRoZSBmdXR1cmUuIElmIHlvdSB3aXNoIHRvIGN1c3RvbWl6ZSBQcmVzdGFTaG9wIGZvciB5b3VyXG4gKiBuZWVkcyBwbGVhc2UgcmVmZXIgdG8gaHR0cHM6Ly9kZXZkb2NzLnByZXN0YXNob3AuY29tLyBmb3IgbW9yZSBpbmZvcm1hdGlvbi5cbiAqXG4gKiBAYXV0aG9yICAgIFByZXN0YVNob3AgU0EgYW5kIENvbnRyaWJ1dG9ycyA8Y29udGFjdEBwcmVzdGFzaG9wLmNvbT5cbiAqIEBjb3B5cmlnaHQgU2luY2UgMjAwNyBQcmVzdGFTaG9wIFNBIGFuZCBDb250cmlidXRvcnNcbiAqIEBsaWNlbnNlICAgaHR0cHM6Ly9vcGVuc291cmNlLm9yZy9saWNlbnNlcy9PU0wtMy4wIE9wZW4gU29mdHdhcmUgTGljZW5zZSAoT1NMIDMuMClcbiAqL1xuXG5pbXBvcnQgTW9kdWxlQ2FyZCBmcm9tICdAY29tcG9uZW50cy9tb2R1bGUtY2FyZCc7XG5pbXBvcnQgQWRtaW5Nb2R1bGVDb250cm9sbGVyIGZyb20gJ0BwYWdlcy9tb2R1bGUvY29udHJvbGxlcic7XG5pbXBvcnQgTW9kdWxlTG9hZGVyIGZyb20gJ0BwYWdlcy9tb2R1bGUvbG9hZGVyJztcblxuY29uc3QgeyR9ID0gd2luZG93O1xuXG4kKCgpID0+IHtcbiAgY29uc3QgbW9kdWxlQ2FyZENvbnRyb2xsZXIgPSBuZXcgTW9kdWxlQ2FyZCgpO1xuICBuZXcgTW9kdWxlTG9hZGVyKCk7XG4gIG5ldyBBZG1pbk1vZHVsZUNvbnRyb2xsZXIobW9kdWxlQ2FyZENvbnRyb2xsZXIpO1xufSk7XG4iXSwic291cmNlUm9vdCI6IiJ9