/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./js/pages/catalog/product/specific-price-form-handler.js":
/*!*****************************************************************!*\
  !*** ./js/pages/catalog/product/specific-price-form-handler.js ***!
  \*****************************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

"use strict";


Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 */

var _window = window,
    $ = _window.$;

var SpecificPriceFormHandler = function () {
  function SpecificPriceFormHandler() {
    (0, _classCallCheck3.default)(this, SpecificPriceFormHandler);

    this.prefixCreateForm = 'form_step2_specific_price_';
    this.prefixEditForm = 'form_modal_';
    this.editModalIsOpen = false;

    this.$createPriceFormDefaultValues = {};
    this.storePriceFormDefaultValues();

    this.loadAndDisplayExistingSpecificPricesList();

    this.configureAddPriceFormBehavior();

    this.configureEditPriceModalBehavior();

    this.configureDeletePriceButtonsBehavior();

    this.configureMultipleModalsBehavior();
  }

  /**
   * @private
   */


  (0, _createClass3.default)(SpecificPriceFormHandler, [{
    key: 'loadAndDisplayExistingSpecificPricesList',
    value: function loadAndDisplayExistingSpecificPricesList() {
      var _this = this;

      var listContainer = $('#js-specific-price-list');
      var url = listContainer.data('listingUrl').replace(/list\/\d+/, 'list/' + this.getProductId());

      $.ajax({
        type: 'GET',
        url: url
      }).done(function (specificPrices) {
        var tbody = listContainer.find('tbody');
        tbody.find('tr').remove();

        if (specificPrices.length > 0) {
          listContainer.removeClass('hide');
        } else {
          listContainer.addClass('hide');
        }

        var specificPricesList = _this.renderSpecificPricesListingAsHtml(specificPrices);

        tbody.append(specificPricesList);
      });
    }

    /**
     * @param array specificPrices
     *
     * @returns string
     *
     * @private
     */

  }, {
    key: 'renderSpecificPricesListingAsHtml',
    value: function renderSpecificPricesListingAsHtml(specificPrices) {
      var specificPricesList = '';

      var self = this;

      $.each(specificPrices, function (index, specificPrice) {
        var deleteUrl = $('#js-specific-price-list').attr('data-action-delete').replace(/delete\/\d+/, 'delete/' + specificPrice.id_specific_price);
        var row = self.renderSpecificPriceRow(specificPrice, deleteUrl);

        specificPricesList += row;
      });

      return specificPricesList;
    }

    /**
     * @param Object specificPrice
     * @param string deleteUrl
     *
     * @returns string
     *
     * @private
     */

  }, {
    key: 'renderSpecificPriceRow',
    value: function renderSpecificPriceRow(specificPrice, deleteUrl) {
      var specificPriceId = specificPrice.id_specific_price;

      /* eslint-disable max-len */
      var canDelete = specificPrice.can_delete ? '<a href="' + deleteUrl + '" class="js-delete delete btn tooltip-link delete pl-0 pr-0"><i class="material-icons">delete</i></a>' : '';
      var canEdit = specificPrice.can_edit ? '<a href="#" data-specific-price-id="' + specificPriceId + '" class="js-edit edit btn tooltip-link delete pl-0 pr-0"><i class="material-icons">edit</i></a>' : '';
      var row = '<tr>     <td>' + specificPrice.id_specific_price + '</td>     <td>' + specificPrice.rule_name + '</td>     <td>' + specificPrice.attributes_name + '</td>     <td>' + specificPrice.currency + '</td>     <td>' + specificPrice.country + '</td>     <td>' + specificPrice.group + '</td>     <td>' + specificPrice.customer + '</td>     <td>' + specificPrice.fixed_price + '</td>     <td>' + specificPrice.impact + '</td>     <td>' + specificPrice.period + '</td>     <td>' + specificPrice.from_quantity + '</td>     <td>' + canDelete + '</td>     <td>' + canEdit + '</td></tr>';
      /* eslint-enable max-len */

      return row;
    }

    /**
     * @private
     */

  }, {
    key: 'configureAddPriceFormBehavior',
    value: function configureAddPriceFormBehavior() {
      var _this2 = this;

      var usePrefixForCreate = true;
      var selectorPrefix = this.getPrefixSelector(usePrefixForCreate);

      $('#specific_price_form .js-cancel').click(function () {
        _this2.resetCreatePriceFormDefaultValues();
        $('#specific_price_form').collapse('hide');
      });

      $('#specific_price_form .js-save').on('click', function () {
        return _this2.submitCreatePriceForm();
      });

      $('#js-open-create-specific-price-form').on('click', function () {
        return _this2.loadAndFillOptionsForSelectCombinationInput(usePrefixForCreate);
      });

      $(selectorPrefix + 'leave_bprice').on('click', function () {
        return _this2.enableSpecificPriceFieldIfEligible(usePrefixForCreate);
      });

      $(selectorPrefix + 'sp_reduction_type').on('change', function () {
        return _this2.enableSpecificPriceTaxFieldIfEligible(usePrefixForCreate);
      });
    }

    /**
     * @private
     */

  }, {
    key: 'configureEditPriceFormInsideModalBehavior',
    value: function configureEditPriceFormInsideModalBehavior() {
      var _this3 = this;

      var usePrefixForCreate = false;
      var selectorPrefix = this.getPrefixSelector(usePrefixForCreate);

      $('#form_modal_cancel').click(function () {
        return _this3.closeEditPriceModalAndRemoveForm();
      });
      $('#form_modal_close').click(function () {
        return _this3.closeEditPriceModalAndRemoveForm();
      });

      $('#form_modal_save').click(function () {
        return _this3.submitEditPriceForm();
      });

      this.loadAndFillOptionsForSelectCombinationInput(usePrefixForCreate);

      $(selectorPrefix + 'leave_bprice').on('click', function () {
        return _this3.enableSpecificPriceFieldIfEligible(usePrefixForCreate);
      });

      $(selectorPrefix + 'sp_reduction_type').on('change', function () {
        return _this3.enableSpecificPriceTaxFieldIfEligible(usePrefixForCreate);
      });

      this.reinitializeDatePickers();

      this.initializeLeaveBPriceField(usePrefixForCreate);
      this.enableSpecificPriceTaxFieldIfEligible(usePrefixForCreate);
    }

    /**
     * @private
     */

  }, {
    key: 'reinitializeDatePickers',
    value: function reinitializeDatePickers() {
      $('.datepicker input').datetimepicker({ format: 'YYYY-MM-DD' });
    }

    /**
     * @param boolean usePrefixForCreate
     *
     * @private
     */

  }, {
    key: 'initializeLeaveBPriceField',
    value: function initializeLeaveBPriceField(usePrefixForCreate) {
      var selectorPrefix = this.getPrefixSelector(usePrefixForCreate);

      if ($(selectorPrefix + 'sp_price').val() !== '') {
        $(selectorPrefix + 'sp_price').prop('disabled', false);
        $(selectorPrefix + 'leave_bprice').prop('checked', false);
      }
    }

    /**
     * @private
     */

  }, {
    key: 'configureEditPriceModalBehavior',
    value: function configureEditPriceModalBehavior() {
      var _this4 = this;

      $(document).on('click', '#js-specific-price-list .js-edit', function (event) {
        event.preventDefault();

        var specificPriceId = $(event.currentTarget).data('specificPriceId');

        _this4.openEditPriceModalAndLoadForm(specificPriceId);
      });
    }

    /**
     * @private
     */

  }, {
    key: 'configureDeletePriceButtonsBehavior',
    value: function configureDeletePriceButtonsBehavior() {
      var _this5 = this;

      $(document).on('click', '#js-specific-price-list .js-delete', function (event) {
        event.preventDefault();
        _this5.deleteSpecificPrice(event.currentTarget);
      });
    }
  }, {
    key: 'configureMultipleModalsBehavior',
    value: function configureMultipleModalsBehavior() {
      var _this6 = this;

      $('.modal').on('hidden.bs.modal', function () {
        if (_this6.editModalIsOpen) {
          $('body').addClass('modal-open');
        }
      });
    }

    /**
     * @private
     */

  }, {
    key: 'submitCreatePriceForm',
    value: function submitCreatePriceForm() {
      var _this7 = this;

      var url = $('#specific_price_form').attr('data-action');
      var data = $('#specific_price_form input, #specific_price_form select, #form_id_product').serialize();

      $('#specific_price_form .js-save').attr('disabled', 'disabled');

      $.ajax({
        type: 'POST',
        url: url,
        data: data
      }).done(function () {
        window.showSuccessMessage(window.translate_javascripts['Form update success']);
        _this7.resetCreatePriceFormDefaultValues();
        $('#specific_price_form').collapse('hide');
        _this7.loadAndDisplayExistingSpecificPricesList();

        $('#specific_price_form .js-save').removeAttr('disabled');
      }).fail(function (errors) {
        window.showErrorMessage(errors.responseJSON);

        $('#specific_price_form .js-save').removeAttr('disabled');
      });
    }

    /**
     * @private
     */

  }, {
    key: 'submitEditPriceForm',
    value: function submitEditPriceForm() {
      var _this8 = this;

      var baseUrl = $('#edit-specific-price-modal-form').attr('data-action');
      var specificPriceId = $('#edit-specific-price-modal-form').data('specificPriceId');
      var url = baseUrl.replace(/update\/\d+/, 'update/' + specificPriceId);

      /* eslint-disable-next-line max-len */
      var data = $('#edit-specific-price-modal-form input, #edit-specific-price-modal-form select, #form_id_product').serialize();

      $('#edit-specific-price-modal-form .js-save').attr('disabled', 'disabled');

      $.ajax({
        type: 'POST',
        url: url,
        data: data
      }).done(function () {
        window.showSuccessMessage(window.translate_javascripts['Form update success']);
        _this8.closeEditPriceModalAndRemoveForm();
        _this8.loadAndDisplayExistingSpecificPricesList();
        $('#edit-specific-price-modal-form .js-save').removeAttr('disabled');
      }).fail(function (errors) {
        window.showErrorMessage(errors.responseJSON);

        $('#edit-specific-price-modal-form .js-save').removeAttr('disabled');
      });
    }

    /**
     * @param string clickedLink selector
     *
     * @private
     */

  }, {
    key: 'deleteSpecificPrice',
    value: function deleteSpecificPrice(clickedLink) {
      var _this9 = this;

      window.modalConfirmation.create(window.translate_javascripts['Are you sure you want to delete this item?'], null, {
        onContinue: function onContinue() {
          var url = $(clickedLink).attr('href');
          $(clickedLink).attr('disabled', 'disabled');

          $.ajax({
            type: 'GET',
            url: url
          }).done(function (response) {
            _this9.loadAndDisplayExistingSpecificPricesList();
            window.showSuccessMessage(response);
            $(clickedLink).removeAttr('disabled');
          }).fail(function (errors) {
            window.showErrorMessage(errors.responseJSON);
            $(clickedLink).removeAttr('disabled');
          });
        }
      }).show();
    }

    /**
     * Store 'add specific price' form values
     * for future usage
     *
     * @private
     */

  }, {
    key: 'storePriceFormDefaultValues',
    value: function storePriceFormDefaultValues() {
      var storage = this.$createPriceFormDefaultValues;

      $('#specific_price_form').find('select,input').each(function (index, value) {
        storage[$(value).attr('id')] = $(value).val();
      });

      $('#specific_price_form').find('input:checkbox').each(function (index, value) {
        storage[$(value).attr('id')] = $(value).prop('checked');
      });

      this.$createPriceFormDefaultValues = storage;
    }

    /**
     * @param boolean usePrefixForCreate
     *
     * @private
     */

  }, {
    key: 'loadAndFillOptionsForSelectCombinationInput',
    value: function loadAndFillOptionsForSelectCombinationInput(usePrefixForCreate) {
      var selectorPrefix = this.getPrefixSelector(usePrefixForCreate);

      var inputField = $(selectorPrefix + 'sp_id_product_attribute');
      var url = inputField.attr('data-action').replace(/product-combinations\/\d+/, 'product-combinations/' + this.getProductId());

      $.ajax({
        type: 'GET',
        url: url
      }).done(function (combinations) {
        /** remove all options except first one */
        inputField.find('option:gt(0)').remove();

        $.each(combinations, function (index, combination) {
          inputField.append('<option value="' + combination.id + '">' + combination.name + '</option>');
        });

        if (inputField.data('selectedAttribute') !== '0') {
          inputField.val(inputField.data('selectedAttribute')).trigger('change');
        }
      });
    }

    /**
     * @param boolean usePrefixForCreate
     *
     * @private
     */

  }, {
    key: 'enableSpecificPriceTaxFieldIfEligible',
    value: function enableSpecificPriceTaxFieldIfEligible(usePrefixForCreate) {
      var selectorPrefix = this.getPrefixSelector(usePrefixForCreate);

      if ($(selectorPrefix + 'sp_reduction_type').val() === 'percentage') {
        $(selectorPrefix + 'sp_reduction_tax').hide();
      } else {
        $(selectorPrefix + 'sp_reduction_tax').show();
      }
    }

    /**
     * Reset 'add specific price' form values
     * using previously stored default values
     *
     * @private
     */

  }, {
    key: 'resetCreatePriceFormDefaultValues',
    value: function resetCreatePriceFormDefaultValues() {
      var previouslyStoredValues = this.$createPriceFormDefaultValues;

      $('#specific_price_form').find('input').each(function (index, value) {
        $(value).val(previouslyStoredValues[$(value).attr('id')]);
      });

      $('#specific_price_form').find('select').each(function (index, value) {
        $(value).val(previouslyStoredValues[$(value).attr('id')]).change();
      });

      $('#specific_price_form').find('input:checkbox').each(function (index, value) {
        $(value).prop('checked', true);
      });
    }

    /**
     * @param boolean usePrefixForCreate
     *
     * @private
     */

  }, {
    key: 'enableSpecificPriceFieldIfEligible',
    value: function enableSpecificPriceFieldIfEligible(usePrefixForCreate) {
      var selectorPrefix = this.getPrefixSelector(usePrefixForCreate);

      $(selectorPrefix + 'sp_price').prop('disabled', $(selectorPrefix + 'leave_bprice').is(':checked')).val('');
    }

    /**
     * Open 'edit specific price' form into a modal
     *
     * @param integer specificPriceId
     *
     * @private
     */

  }, {
    key: 'openEditPriceModalAndLoadForm',
    value: function openEditPriceModalAndLoadForm(specificPriceId) {
      var _this10 = this;

      var url = $('#js-specific-price-list').data('actionEdit').replace(/form\/\d+/, 'form/' + specificPriceId);

      $('#edit-specific-price-modal').modal('show');
      this.editModalIsOpen = true;

      $.ajax({
        type: 'GET',
        url: url
      }).done(function (response) {
        _this10.insertEditSpecificPriceFormIntoModal(response);
        $('#edit-specific-price-modal-form').data('specificPriceId', specificPriceId);
        _this10.configureEditPriceFormInsideModalBehavior();
      }).fail(function (errors) {
        window.showErrorMessage(errors.responseJSON);
      });
    }

    /**
     * @private
     */

  }, {
    key: 'closeEditPriceModalAndRemoveForm',
    value: function closeEditPriceModalAndRemoveForm() {
      $('#edit-specific-price-modal').modal('hide');
      this.editModalIsOpen = false;

      var formLocationHolder = $('#edit-specific-price-modal-form');

      formLocationHolder.empty();
    }

    /**
     * @param string form: HTML 'edit specific price' form
     *
     * @private
     */

  }, {
    key: 'insertEditSpecificPriceFormIntoModal',
    value: function insertEditSpecificPriceFormIntoModal(form) {
      var formLocationHolder = $('#edit-specific-price-modal-form');

      formLocationHolder.empty();
      formLocationHolder.append(form);
    }

    /**
     * Get product ID for current Catalog Product page
     *
     * @returns integer
     *
     * @private
     */

  }, {
    key: 'getProductId',
    value: function getProductId() {
      return $('#form_id_product').val();
    }

    /**
     * @param boolean usePrefixForCreate
     *
     * @returns string
     *
     * @private
     */

  }, {
    key: 'getPrefixSelector',
    value: function getPrefixSelector(usePrefixForCreate) {
      if (usePrefixForCreate) {
        return '#' + this.prefixCreateForm;
      }

      return '#' + this.prefixEditForm;
    }
  }]);
  return SpecificPriceFormHandler;
}();

exports.default = SpecificPriceFormHandler;

/***/ }),

/***/ "./node_modules/babel-runtime/core-js/object/define-property.js":
/*!**********************************************************************!*\
  !*** ./node_modules/babel-runtime/core-js/object/define-property.js ***!
  \**********************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

module.exports = { "default": __webpack_require__(/*! core-js/library/fn/object/define-property */ "./node_modules/core-js/library/fn/object/define-property.js"), __esModule: true };

/***/ }),

/***/ "./node_modules/babel-runtime/helpers/classCallCheck.js":
/*!**************************************************************!*\
  !*** ./node_modules/babel-runtime/helpers/classCallCheck.js ***!
  \**************************************************************/
/***/ ((__unused_webpack_module, exports) => {

"use strict";


exports.__esModule = true;

exports.default = function (instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
};

/***/ }),

/***/ "./node_modules/babel-runtime/helpers/createClass.js":
/*!***********************************************************!*\
  !*** ./node_modules/babel-runtime/helpers/createClass.js ***!
  \***********************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

"use strict";


exports.__esModule = true;

var _defineProperty = __webpack_require__(/*! ../core-js/object/define-property */ "./node_modules/babel-runtime/core-js/object/define-property.js");

var _defineProperty2 = _interopRequireDefault(_defineProperty);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      (0, _defineProperty2.default)(target, descriptor.key, descriptor);
    }
  }

  return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);
    if (staticProps) defineProperties(Constructor, staticProps);
    return Constructor;
  };
}();

/***/ }),

/***/ "./node_modules/core-js/library/fn/object/define-property.js":
/*!*******************************************************************!*\
  !*** ./node_modules/core-js/library/fn/object/define-property.js ***!
  \*******************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

__webpack_require__(/*! ../../modules/es6.object.define-property */ "./node_modules/core-js/library/modules/es6.object.define-property.js");
var $Object = __webpack_require__(/*! ../../modules/_core */ "./node_modules/core-js/library/modules/_core.js").Object;
module.exports = function defineProperty(it, key, desc) {
  return $Object.defineProperty(it, key, desc);
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_a-function.js":
/*!*************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_a-function.js ***!
  \*************************************************************/
/***/ ((module) => {

module.exports = function (it) {
  if (typeof it != 'function') throw TypeError(it + ' is not a function!');
  return it;
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_an-object.js":
/*!************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_an-object.js ***!
  \************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var isObject = __webpack_require__(/*! ./_is-object */ "./node_modules/core-js/library/modules/_is-object.js");
module.exports = function (it) {
  if (!isObject(it)) throw TypeError(it + ' is not an object!');
  return it;
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_core.js":
/*!*******************************************************!*\
  !*** ./node_modules/core-js/library/modules/_core.js ***!
  \*******************************************************/
/***/ ((module) => {

var core = module.exports = { version: '2.6.11' };
if (typeof __e == 'number') __e = core; // eslint-disable-line no-undef


/***/ }),

/***/ "./node_modules/core-js/library/modules/_ctx.js":
/*!******************************************************!*\
  !*** ./node_modules/core-js/library/modules/_ctx.js ***!
  \******************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// optional / simple context binding
var aFunction = __webpack_require__(/*! ./_a-function */ "./node_modules/core-js/library/modules/_a-function.js");
module.exports = function (fn, that, length) {
  aFunction(fn);
  if (that === undefined) return fn;
  switch (length) {
    case 1: return function (a) {
      return fn.call(that, a);
    };
    case 2: return function (a, b) {
      return fn.call(that, a, b);
    };
    case 3: return function (a, b, c) {
      return fn.call(that, a, b, c);
    };
  }
  return function (/* ...args */) {
    return fn.apply(that, arguments);
  };
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_descriptors.js":
/*!**************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_descriptors.js ***!
  \**************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// Thank's IE8 for his funny defineProperty
module.exports = !__webpack_require__(/*! ./_fails */ "./node_modules/core-js/library/modules/_fails.js")(function () {
  return Object.defineProperty({}, 'a', { get: function () { return 7; } }).a != 7;
});


/***/ }),

/***/ "./node_modules/core-js/library/modules/_dom-create.js":
/*!*************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_dom-create.js ***!
  \*************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var isObject = __webpack_require__(/*! ./_is-object */ "./node_modules/core-js/library/modules/_is-object.js");
var document = __webpack_require__(/*! ./_global */ "./node_modules/core-js/library/modules/_global.js").document;
// typeof document.createElement is 'object' in old IE
var is = isObject(document) && isObject(document.createElement);
module.exports = function (it) {
  return is ? document.createElement(it) : {};
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_export.js":
/*!*********************************************************!*\
  !*** ./node_modules/core-js/library/modules/_export.js ***!
  \*********************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var global = __webpack_require__(/*! ./_global */ "./node_modules/core-js/library/modules/_global.js");
var core = __webpack_require__(/*! ./_core */ "./node_modules/core-js/library/modules/_core.js");
var ctx = __webpack_require__(/*! ./_ctx */ "./node_modules/core-js/library/modules/_ctx.js");
var hide = __webpack_require__(/*! ./_hide */ "./node_modules/core-js/library/modules/_hide.js");
var has = __webpack_require__(/*! ./_has */ "./node_modules/core-js/library/modules/_has.js");
var PROTOTYPE = 'prototype';

var $export = function (type, name, source) {
  var IS_FORCED = type & $export.F;
  var IS_GLOBAL = type & $export.G;
  var IS_STATIC = type & $export.S;
  var IS_PROTO = type & $export.P;
  var IS_BIND = type & $export.B;
  var IS_WRAP = type & $export.W;
  var exports = IS_GLOBAL ? core : core[name] || (core[name] = {});
  var expProto = exports[PROTOTYPE];
  var target = IS_GLOBAL ? global : IS_STATIC ? global[name] : (global[name] || {})[PROTOTYPE];
  var key, own, out;
  if (IS_GLOBAL) source = name;
  for (key in source) {
    // contains in native
    own = !IS_FORCED && target && target[key] !== undefined;
    if (own && has(exports, key)) continue;
    // export native or passed
    out = own ? target[key] : source[key];
    // prevent global pollution for namespaces
    exports[key] = IS_GLOBAL && typeof target[key] != 'function' ? source[key]
    // bind timers to global for call from export context
    : IS_BIND && own ? ctx(out, global)
    // wrap global constructors for prevent change them in library
    : IS_WRAP && target[key] == out ? (function (C) {
      var F = function (a, b, c) {
        if (this instanceof C) {
          switch (arguments.length) {
            case 0: return new C();
            case 1: return new C(a);
            case 2: return new C(a, b);
          } return new C(a, b, c);
        } return C.apply(this, arguments);
      };
      F[PROTOTYPE] = C[PROTOTYPE];
      return F;
    // make static versions for prototype methods
    })(out) : IS_PROTO && typeof out == 'function' ? ctx(Function.call, out) : out;
    // export proto methods to core.%CONSTRUCTOR%.methods.%NAME%
    if (IS_PROTO) {
      (exports.virtual || (exports.virtual = {}))[key] = out;
      // export proto methods to core.%CONSTRUCTOR%.prototype.%NAME%
      if (type & $export.R && expProto && !expProto[key]) hide(expProto, key, out);
    }
  }
};
// type bitmap
$export.F = 1;   // forced
$export.G = 2;   // global
$export.S = 4;   // static
$export.P = 8;   // proto
$export.B = 16;  // bind
$export.W = 32;  // wrap
$export.U = 64;  // safe
$export.R = 128; // real proto method for `library`
module.exports = $export;


/***/ }),

/***/ "./node_modules/core-js/library/modules/_fails.js":
/*!********************************************************!*\
  !*** ./node_modules/core-js/library/modules/_fails.js ***!
  \********************************************************/
/***/ ((module) => {

module.exports = function (exec) {
  try {
    return !!exec();
  } catch (e) {
    return true;
  }
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_global.js":
/*!*********************************************************!*\
  !*** ./node_modules/core-js/library/modules/_global.js ***!
  \*********************************************************/
/***/ ((module) => {

// https://github.com/zloirock/core-js/issues/86#issuecomment-115759028
var global = module.exports = typeof window != 'undefined' && window.Math == Math
  ? window : typeof self != 'undefined' && self.Math == Math ? self
  // eslint-disable-next-line no-new-func
  : Function('return this')();
if (typeof __g == 'number') __g = global; // eslint-disable-line no-undef


/***/ }),

/***/ "./node_modules/core-js/library/modules/_has.js":
/*!******************************************************!*\
  !*** ./node_modules/core-js/library/modules/_has.js ***!
  \******************************************************/
/***/ ((module) => {

var hasOwnProperty = {}.hasOwnProperty;
module.exports = function (it, key) {
  return hasOwnProperty.call(it, key);
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_hide.js":
/*!*******************************************************!*\
  !*** ./node_modules/core-js/library/modules/_hide.js ***!
  \*******************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var dP = __webpack_require__(/*! ./_object-dp */ "./node_modules/core-js/library/modules/_object-dp.js");
var createDesc = __webpack_require__(/*! ./_property-desc */ "./node_modules/core-js/library/modules/_property-desc.js");
module.exports = __webpack_require__(/*! ./_descriptors */ "./node_modules/core-js/library/modules/_descriptors.js") ? function (object, key, value) {
  return dP.f(object, key, createDesc(1, value));
} : function (object, key, value) {
  object[key] = value;
  return object;
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_ie8-dom-define.js":
/*!*****************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_ie8-dom-define.js ***!
  \*****************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

module.exports = !__webpack_require__(/*! ./_descriptors */ "./node_modules/core-js/library/modules/_descriptors.js") && !__webpack_require__(/*! ./_fails */ "./node_modules/core-js/library/modules/_fails.js")(function () {
  return Object.defineProperty(__webpack_require__(/*! ./_dom-create */ "./node_modules/core-js/library/modules/_dom-create.js")('div'), 'a', { get: function () { return 7; } }).a != 7;
});


/***/ }),

/***/ "./node_modules/core-js/library/modules/_is-object.js":
/*!************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_is-object.js ***!
  \************************************************************/
/***/ ((module) => {

module.exports = function (it) {
  return typeof it === 'object' ? it !== null : typeof it === 'function';
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_object-dp.js":
/*!************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_object-dp.js ***!
  \************************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

var anObject = __webpack_require__(/*! ./_an-object */ "./node_modules/core-js/library/modules/_an-object.js");
var IE8_DOM_DEFINE = __webpack_require__(/*! ./_ie8-dom-define */ "./node_modules/core-js/library/modules/_ie8-dom-define.js");
var toPrimitive = __webpack_require__(/*! ./_to-primitive */ "./node_modules/core-js/library/modules/_to-primitive.js");
var dP = Object.defineProperty;

exports.f = __webpack_require__(/*! ./_descriptors */ "./node_modules/core-js/library/modules/_descriptors.js") ? Object.defineProperty : function defineProperty(O, P, Attributes) {
  anObject(O);
  P = toPrimitive(P, true);
  anObject(Attributes);
  if (IE8_DOM_DEFINE) try {
    return dP(O, P, Attributes);
  } catch (e) { /* empty */ }
  if ('get' in Attributes || 'set' in Attributes) throw TypeError('Accessors not supported!');
  if ('value' in Attributes) O[P] = Attributes.value;
  return O;
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_property-desc.js":
/*!****************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_property-desc.js ***!
  \****************************************************************/
/***/ ((module) => {

module.exports = function (bitmap, value) {
  return {
    enumerable: !(bitmap & 1),
    configurable: !(bitmap & 2),
    writable: !(bitmap & 4),
    value: value
  };
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_to-primitive.js":
/*!***************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_to-primitive.js ***!
  \***************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// 7.1.1 ToPrimitive(input [, PreferredType])
var isObject = __webpack_require__(/*! ./_is-object */ "./node_modules/core-js/library/modules/_is-object.js");
// instead of the ES6 spec version, we didn't implement @@toPrimitive case
// and the second argument - flag - preferred type is a string
module.exports = function (it, S) {
  if (!isObject(it)) return it;
  var fn, val;
  if (S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;
  if (typeof (fn = it.valueOf) == 'function' && !isObject(val = fn.call(it))) return val;
  if (!S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;
  throw TypeError("Can't convert object to primitive value");
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/es6.object.define-property.js":
/*!****************************************************************************!*\
  !*** ./node_modules/core-js/library/modules/es6.object.define-property.js ***!
  \****************************************************************************/
/***/ ((__unused_webpack_module, __unused_webpack_exports, __webpack_require__) => {

var $export = __webpack_require__(/*! ./_export */ "./node_modules/core-js/library/modules/_export.js");
// 19.1.2.4 / 15.2.3.6 Object.defineProperty(O, P, Attributes)
$export($export.S + $export.F * !__webpack_require__(/*! ./_descriptors */ "./node_modules/core-js/library/modules/_descriptors.js"), 'Object', { defineProperty: __webpack_require__(/*! ./_object-dp */ "./node_modules/core-js/library/modules/_object-dp.js").f });


/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be in strict mode.
(() => {
"use strict";
/*!*******************************************!*\
  !*** ./js/pages/catalog/product/index.js ***!
  \*******************************************/


var _specificPriceFormHandler = __webpack_require__(/*! ./specific-price-form-handler */ "./js/pages/catalog/product/specific-price-form-handler.js");

var _specificPriceFormHandler2 = _interopRequireDefault(_specificPriceFormHandler);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _window = window,
    $ = _window.$; /**
                    * Copyright since 2007 PrestaShop SA and Contributors
                    * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
                    *
                    * NOTICE OF LICENSE
                    *
                    * This source file is subject to the Open Software License (OSL 3.0)
                    * that is bundled with this package in the file LICENSE.md.
                    * It is also available through the world-wide-web at this URL:
                    * https://opensource.org/licenses/OSL-3.0
                    * If you did not receive a copy of the license and are unable to
                    * obtain it through the world-wide-web, please send an email
                    * to license@prestashop.com so we can send you a copy immediately.
                    *
                    * DISCLAIMER
                    *
                    * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
                    * versions in the future. If you wish to customize PrestaShop for your
                    * needs please refer to https://devdocs.prestashop.com/ for more information.
                    *
                    * @author    PrestaShop SA and Contributors <contact@prestashop.com>
                    * @copyright Since 2007 PrestaShop SA and Contributors
                    * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
                    */

$(function () {
  new _specificPriceFormHandler2.default();
});
})();

window.catalog_product = __webpack_exports__;
/******/ })()
;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9bbmFtZV0vLi9qcy9wYWdlcy9jYXRhbG9nL3Byb2R1Y3Qvc3BlY2lmaWMtcHJpY2UtZm9ybS1oYW5kbGVyLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9iYWJlbC1ydW50aW1lL2NvcmUtanMvb2JqZWN0L2RlZmluZS1wcm9wZXJ0eS5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvYmFiZWwtcnVudGltZS9oZWxwZXJzL2NsYXNzQ2FsbENoZWNrLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9iYWJlbC1ydW50aW1lL2hlbHBlcnMvY3JlYXRlQ2xhc3MuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9mbi9vYmplY3QvZGVmaW5lLXByb3BlcnR5LmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fYS1mdW5jdGlvbi5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX2FuLW9iamVjdC5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX2NvcmUuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19jdHguanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19kZXNjcmlwdG9ycy5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX2RvbS1jcmVhdGUuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19leHBvcnQuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19mYWlscy5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX2dsb2JhbC5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX2hhcy5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX2hpZGUuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19pZTgtZG9tLWRlZmluZS5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX2lzLW9iamVjdC5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX29iamVjdC1kcC5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX3Byb3BlcnR5LWRlc2MuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL190by1wcmltaXRpdmUuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL2VzNi5vYmplY3QuZGVmaW5lLXByb3BlcnR5LmpzIiwid2VicGFjazovL1tuYW1lXS93ZWJwYWNrL2Jvb3RzdHJhcCIsIndlYnBhY2s6Ly9bbmFtZV0vLi9qcy9wYWdlcy9jYXRhbG9nL3Byb2R1Y3QvaW5kZXguanMiXSwibmFtZXMiOlsid2luZG93IiwiJCIsIlNwZWNpZmljUHJpY2VGb3JtSGFuZGxlciIsInByZWZpeENyZWF0ZUZvcm0iLCJwcmVmaXhFZGl0Rm9ybSIsImVkaXRNb2RhbElzT3BlbiIsIiRjcmVhdGVQcmljZUZvcm1EZWZhdWx0VmFsdWVzIiwic3RvcmVQcmljZUZvcm1EZWZhdWx0VmFsdWVzIiwibG9hZEFuZERpc3BsYXlFeGlzdGluZ1NwZWNpZmljUHJpY2VzTGlzdCIsImNvbmZpZ3VyZUFkZFByaWNlRm9ybUJlaGF2aW9yIiwiY29uZmlndXJlRWRpdFByaWNlTW9kYWxCZWhhdmlvciIsImNvbmZpZ3VyZURlbGV0ZVByaWNlQnV0dG9uc0JlaGF2aW9yIiwiY29uZmlndXJlTXVsdGlwbGVNb2RhbHNCZWhhdmlvciIsImxpc3RDb250YWluZXIiLCJ1cmwiLCJkYXRhIiwicmVwbGFjZSIsImdldFByb2R1Y3RJZCIsImFqYXgiLCJ0eXBlIiwiZG9uZSIsInNwZWNpZmljUHJpY2VzIiwidGJvZHkiLCJmaW5kIiwicmVtb3ZlIiwibGVuZ3RoIiwicmVtb3ZlQ2xhc3MiLCJhZGRDbGFzcyIsInNwZWNpZmljUHJpY2VzTGlzdCIsInJlbmRlclNwZWNpZmljUHJpY2VzTGlzdGluZ0FzSHRtbCIsImFwcGVuZCIsInNlbGYiLCJlYWNoIiwiaW5kZXgiLCJzcGVjaWZpY1ByaWNlIiwiZGVsZXRlVXJsIiwiYXR0ciIsImlkX3NwZWNpZmljX3ByaWNlIiwicm93IiwicmVuZGVyU3BlY2lmaWNQcmljZVJvdyIsInNwZWNpZmljUHJpY2VJZCIsImNhbkRlbGV0ZSIsImNhbl9kZWxldGUiLCJjYW5FZGl0IiwiY2FuX2VkaXQiLCJydWxlX25hbWUiLCJhdHRyaWJ1dGVzX25hbWUiLCJjdXJyZW5jeSIsImNvdW50cnkiLCJncm91cCIsImN1c3RvbWVyIiwiZml4ZWRfcHJpY2UiLCJpbXBhY3QiLCJwZXJpb2QiLCJmcm9tX3F1YW50aXR5IiwidXNlUHJlZml4Rm9yQ3JlYXRlIiwic2VsZWN0b3JQcmVmaXgiLCJnZXRQcmVmaXhTZWxlY3RvciIsImNsaWNrIiwicmVzZXRDcmVhdGVQcmljZUZvcm1EZWZhdWx0VmFsdWVzIiwiY29sbGFwc2UiLCJvbiIsInN1Ym1pdENyZWF0ZVByaWNlRm9ybSIsImxvYWRBbmRGaWxsT3B0aW9uc0ZvclNlbGVjdENvbWJpbmF0aW9uSW5wdXQiLCJlbmFibGVTcGVjaWZpY1ByaWNlRmllbGRJZkVsaWdpYmxlIiwiZW5hYmxlU3BlY2lmaWNQcmljZVRheEZpZWxkSWZFbGlnaWJsZSIsImNsb3NlRWRpdFByaWNlTW9kYWxBbmRSZW1vdmVGb3JtIiwic3VibWl0RWRpdFByaWNlRm9ybSIsInJlaW5pdGlhbGl6ZURhdGVQaWNrZXJzIiwiaW5pdGlhbGl6ZUxlYXZlQlByaWNlRmllbGQiLCJkYXRldGltZXBpY2tlciIsImZvcm1hdCIsInZhbCIsInByb3AiLCJkb2N1bWVudCIsImV2ZW50IiwicHJldmVudERlZmF1bHQiLCJjdXJyZW50VGFyZ2V0Iiwib3BlbkVkaXRQcmljZU1vZGFsQW5kTG9hZEZvcm0iLCJkZWxldGVTcGVjaWZpY1ByaWNlIiwic2VyaWFsaXplIiwic2hvd1N1Y2Nlc3NNZXNzYWdlIiwidHJhbnNsYXRlX2phdmFzY3JpcHRzIiwicmVtb3ZlQXR0ciIsImZhaWwiLCJlcnJvcnMiLCJzaG93RXJyb3JNZXNzYWdlIiwicmVzcG9uc2VKU09OIiwiYmFzZVVybCIsImNsaWNrZWRMaW5rIiwibW9kYWxDb25maXJtYXRpb24iLCJjcmVhdGUiLCJvbkNvbnRpbnVlIiwicmVzcG9uc2UiLCJzaG93Iiwic3RvcmFnZSIsInZhbHVlIiwiaW5wdXRGaWVsZCIsImNvbWJpbmF0aW9ucyIsImNvbWJpbmF0aW9uIiwiaWQiLCJuYW1lIiwidHJpZ2dlciIsImhpZGUiLCJwcmV2aW91c2x5U3RvcmVkVmFsdWVzIiwiY2hhbmdlIiwiaXMiLCJtb2RhbCIsImluc2VydEVkaXRTcGVjaWZpY1ByaWNlRm9ybUludG9Nb2RhbCIsImNvbmZpZ3VyZUVkaXRQcmljZUZvcm1JbnNpZGVNb2RhbEJlaGF2aW9yIiwiZm9ybUxvY2F0aW9uSG9sZGVyIiwiZW1wdHkiLCJmb3JtIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O2NBeUJZQSxNO0lBQUxDLEMsV0FBQUEsQzs7SUFFREMsd0I7QUFDSixzQ0FBYztBQUFBOztBQUNaLFNBQUtDLGdCQUFMLEdBQXdCLDRCQUF4QjtBQUNBLFNBQUtDLGNBQUwsR0FBc0IsYUFBdEI7QUFDQSxTQUFLQyxlQUFMLEdBQXVCLEtBQXZCOztBQUVBLFNBQUtDLDZCQUFMLEdBQXFDLEVBQXJDO0FBQ0EsU0FBS0MsMkJBQUw7O0FBRUEsU0FBS0Msd0NBQUw7O0FBRUEsU0FBS0MsNkJBQUw7O0FBRUEsU0FBS0MsK0JBQUw7O0FBRUEsU0FBS0MsbUNBQUw7O0FBRUEsU0FBS0MsK0JBQUw7QUFDRDs7QUFFRDs7Ozs7OzsrREFHMkM7QUFBQTs7QUFDekMsVUFBTUMsZ0JBQWdCWixFQUFFLHlCQUFGLENBQXRCO0FBQ0EsVUFBTWEsTUFBTUQsY0FBY0UsSUFBZCxDQUFtQixZQUFuQixFQUFpQ0MsT0FBakMsQ0FBeUMsV0FBekMsWUFBOEQsS0FBS0MsWUFBTCxFQUE5RCxDQUFaOztBQUVBaEIsUUFBRWlCLElBQUYsQ0FBTztBQUNMQyxjQUFNLEtBREQ7QUFFTEw7QUFGSyxPQUFQLEVBSUdNLElBSkgsQ0FJUSxVQUFDQyxjQUFELEVBQW9CO0FBQ3hCLFlBQU1DLFFBQVFULGNBQWNVLElBQWQsQ0FBbUIsT0FBbkIsQ0FBZDtBQUNBRCxjQUFNQyxJQUFOLENBQVcsSUFBWCxFQUFpQkMsTUFBakI7O0FBRUEsWUFBSUgsZUFBZUksTUFBZixHQUF3QixDQUE1QixFQUErQjtBQUM3Qlosd0JBQWNhLFdBQWQsQ0FBMEIsTUFBMUI7QUFDRCxTQUZELE1BRU87QUFDTGIsd0JBQWNjLFFBQWQsQ0FBdUIsTUFBdkI7QUFDRDs7QUFFRCxZQUFNQyxxQkFBcUIsTUFBS0MsaUNBQUwsQ0FBdUNSLGNBQXZDLENBQTNCOztBQUVBQyxjQUFNUSxNQUFOLENBQWFGLGtCQUFiO0FBQ0QsT0FqQkg7QUFrQkQ7O0FBRUQ7Ozs7Ozs7Ozs7c0RBT2tDUCxjLEVBQWdCO0FBQ2hELFVBQUlPLHFCQUFxQixFQUF6Qjs7QUFFQSxVQUFNRyxPQUFPLElBQWI7O0FBRUE5QixRQUFFK0IsSUFBRixDQUFPWCxjQUFQLEVBQXVCLFVBQUNZLEtBQUQsRUFBUUMsYUFBUixFQUEwQjtBQUMvQyxZQUFNQyxZQUFZbEMsRUFBRSx5QkFBRixFQUNmbUMsSUFEZSxDQUNWLG9CQURVLEVBRWZwQixPQUZlLENBRVAsYUFGTyxjQUVrQmtCLGNBQWNHLGlCQUZoQyxDQUFsQjtBQUdBLFlBQU1DLE1BQU1QLEtBQUtRLHNCQUFMLENBQTRCTCxhQUE1QixFQUEyQ0MsU0FBM0MsQ0FBWjs7QUFFQVAsOEJBQXNCVSxHQUF0QjtBQUNELE9BUEQ7O0FBU0EsYUFBT1Ysa0JBQVA7QUFDRDs7QUFFRDs7Ozs7Ozs7Ozs7MkNBUXVCTSxhLEVBQWVDLFMsRUFBVztBQUMvQyxVQUFNSyxrQkFBa0JOLGNBQWNHLGlCQUF0Qzs7QUFFQTtBQUNBLFVBQU1JLFlBQVlQLGNBQWNRLFVBQWQsaUJBQ0ZQLFNBREUsNkdBRWQsRUFGSjtBQUdBLFVBQU1RLFVBQVVULGNBQWNVLFFBQWQsNENBQzJCSixlQUQzQix1R0FFWixFQUZKO0FBR0EsVUFBTUYsd0JBQ0FKLGNBQWNHLGlCQURkLHNCQUVBSCxjQUFjVyxTQUZkLHNCQUdBWCxjQUFjWSxlQUhkLHNCQUlBWixjQUFjYSxRQUpkLHNCQUtBYixjQUFjYyxPQUxkLHNCQU1BZCxjQUFjZSxLQU5kLHNCQU9BZixjQUFjZ0IsUUFQZCxzQkFRQWhCLGNBQWNpQixXQVJkLHNCQVNBakIsY0FBY2tCLE1BVGQsc0JBVUFsQixjQUFjbUIsTUFWZCxzQkFXQW5CLGNBQWNvQixhQVhkLHNCQVlBYixTQVpBLHNCQWFBRSxPQWJBLGVBQU47QUFjQTs7QUFFQSxhQUFPTCxHQUFQO0FBQ0Q7O0FBRUQ7Ozs7OztvREFHZ0M7QUFBQTs7QUFDOUIsVUFBTWlCLHFCQUFxQixJQUEzQjtBQUNBLFVBQU1DLGlCQUFpQixLQUFLQyxpQkFBTCxDQUF1QkYsa0JBQXZCLENBQXZCOztBQUVBdEQsUUFBRSxpQ0FBRixFQUFxQ3lELEtBQXJDLENBQTJDLFlBQU07QUFDL0MsZUFBS0MsaUNBQUw7QUFDQTFELFVBQUUsc0JBQUYsRUFBMEIyRCxRQUExQixDQUFtQyxNQUFuQztBQUNELE9BSEQ7O0FBS0EzRCxRQUFFLCtCQUFGLEVBQW1DNEQsRUFBbkMsQ0FDRSxPQURGLEVBRUU7QUFBQSxlQUFNLE9BQUtDLHFCQUFMLEVBQU47QUFBQSxPQUZGOztBQUtBN0QsUUFBRSxxQ0FBRixFQUF5QzRELEVBQXpDLENBQ0UsT0FERixFQUVFO0FBQUEsZUFBTSxPQUFLRSwyQ0FBTCxDQUFpRFIsa0JBQWpELENBQU47QUFBQSxPQUZGOztBQUtBdEQsUUFBS3VELGNBQUwsbUJBQW1DSyxFQUFuQyxDQUNFLE9BREYsRUFFRTtBQUFBLGVBQU0sT0FBS0csa0NBQUwsQ0FBd0NULGtCQUF4QyxDQUFOO0FBQUEsT0FGRjs7QUFLQXRELFFBQUt1RCxjQUFMLHdCQUF3Q0ssRUFBeEMsQ0FDRSxRQURGLEVBRUU7QUFBQSxlQUFNLE9BQUtJLHFDQUFMLENBQTJDVixrQkFBM0MsQ0FBTjtBQUFBLE9BRkY7QUFJRDs7QUFFRDs7Ozs7O2dFQUc0QztBQUFBOztBQUMxQyxVQUFNQSxxQkFBcUIsS0FBM0I7QUFDQSxVQUFNQyxpQkFBaUIsS0FBS0MsaUJBQUwsQ0FBdUJGLGtCQUF2QixDQUF2Qjs7QUFFQXRELFFBQUUsb0JBQUYsRUFBd0J5RCxLQUF4QixDQUE4QjtBQUFBLGVBQU0sT0FBS1EsZ0NBQUwsRUFBTjtBQUFBLE9BQTlCO0FBQ0FqRSxRQUFFLG1CQUFGLEVBQXVCeUQsS0FBdkIsQ0FBNkI7QUFBQSxlQUFNLE9BQUtRLGdDQUFMLEVBQU47QUFBQSxPQUE3Qjs7QUFFQWpFLFFBQUUsa0JBQUYsRUFBc0J5RCxLQUF0QixDQUE0QjtBQUFBLGVBQU0sT0FBS1MsbUJBQUwsRUFBTjtBQUFBLE9BQTVCOztBQUVBLFdBQUtKLDJDQUFMLENBQWlEUixrQkFBakQ7O0FBRUF0RCxRQUFLdUQsY0FBTCxtQkFBbUNLLEVBQW5DLENBQXNDLE9BQXRDLEVBQStDO0FBQUEsZUFBTSxPQUFLRyxrQ0FBTCxDQUF3Q1Qsa0JBQXhDLENBQU47QUFBQSxPQUEvQzs7QUFFQXRELFFBQUt1RCxjQUFMLHdCQUF3Q0ssRUFBeEMsQ0FDRSxRQURGLEVBRUU7QUFBQSxlQUFNLE9BQUtJLHFDQUFMLENBQTJDVixrQkFBM0MsQ0FBTjtBQUFBLE9BRkY7O0FBS0EsV0FBS2EsdUJBQUw7O0FBRUEsV0FBS0MsMEJBQUwsQ0FBZ0NkLGtCQUFoQztBQUNBLFdBQUtVLHFDQUFMLENBQTJDVixrQkFBM0M7QUFDRDs7QUFFRDs7Ozs7OzhDQUcwQjtBQUN4QnRELFFBQUUsbUJBQUYsRUFBdUJxRSxjQUF2QixDQUFzQyxFQUFDQyxRQUFRLFlBQVQsRUFBdEM7QUFDRDs7QUFFRDs7Ozs7Ozs7K0NBSzJCaEIsa0IsRUFBb0I7QUFDN0MsVUFBTUMsaUJBQWlCLEtBQUtDLGlCQUFMLENBQXVCRixrQkFBdkIsQ0FBdkI7O0FBRUEsVUFBSXRELEVBQUt1RCxjQUFMLGVBQStCZ0IsR0FBL0IsT0FBeUMsRUFBN0MsRUFBaUQ7QUFDL0N2RSxVQUFLdUQsY0FBTCxlQUErQmlCLElBQS9CLENBQW9DLFVBQXBDLEVBQWdELEtBQWhEO0FBQ0F4RSxVQUFLdUQsY0FBTCxtQkFBbUNpQixJQUFuQyxDQUF3QyxTQUF4QyxFQUFtRCxLQUFuRDtBQUNEO0FBQ0Y7O0FBRUQ7Ozs7OztzREFHa0M7QUFBQTs7QUFDaEN4RSxRQUFFeUUsUUFBRixFQUFZYixFQUFaLENBQWUsT0FBZixFQUF3QixrQ0FBeEIsRUFBNEQsVUFBQ2MsS0FBRCxFQUFXO0FBQ3JFQSxjQUFNQyxjQUFOOztBQUVBLFlBQU1wQyxrQkFBa0J2QyxFQUFFMEUsTUFBTUUsYUFBUixFQUF1QjlELElBQXZCLENBQTRCLGlCQUE1QixDQUF4Qjs7QUFFQSxlQUFLK0QsNkJBQUwsQ0FBbUN0QyxlQUFuQztBQUNELE9BTkQ7QUFPRDs7QUFFRDs7Ozs7OzBEQUdzQztBQUFBOztBQUNwQ3ZDLFFBQUV5RSxRQUFGLEVBQVliLEVBQVosQ0FBZSxPQUFmLEVBQXdCLG9DQUF4QixFQUE4RCxVQUFDYyxLQUFELEVBQVc7QUFDdkVBLGNBQU1DLGNBQU47QUFDQSxlQUFLRyxtQkFBTCxDQUF5QkosTUFBTUUsYUFBL0I7QUFDRCxPQUhEO0FBSUQ7OztzREFFaUM7QUFBQTs7QUFDaEM1RSxRQUFFLFFBQUYsRUFBWTRELEVBQVosQ0FBZSxpQkFBZixFQUFrQyxZQUFNO0FBQ3RDLFlBQUksT0FBS3hELGVBQVQsRUFBMEI7QUFDeEJKLFlBQUUsTUFBRixFQUFVMEIsUUFBVixDQUFtQixZQUFuQjtBQUNEO0FBQ0YsT0FKRDtBQUtEOztBQUVEOzs7Ozs7NENBR3dCO0FBQUE7O0FBQ3RCLFVBQU1iLE1BQU1iLEVBQUUsc0JBQUYsRUFBMEJtQyxJQUExQixDQUErQixhQUEvQixDQUFaO0FBQ0EsVUFBTXJCLE9BQU9kLEVBQUUsMkVBQUYsRUFBK0UrRSxTQUEvRSxFQUFiOztBQUVBL0UsUUFBRSwrQkFBRixFQUFtQ21DLElBQW5DLENBQXdDLFVBQXhDLEVBQW9ELFVBQXBEOztBQUVBbkMsUUFBRWlCLElBQUYsQ0FBTztBQUNMQyxjQUFNLE1BREQ7QUFFTEwsZ0JBRks7QUFHTEM7QUFISyxPQUFQLEVBSUdLLElBSkgsQ0FJUSxZQUFNO0FBQ1pwQixlQUFPaUYsa0JBQVAsQ0FBMEJqRixPQUFPa0YscUJBQVAsQ0FBNkIscUJBQTdCLENBQTFCO0FBQ0EsZUFBS3ZCLGlDQUFMO0FBQ0ExRCxVQUFFLHNCQUFGLEVBQTBCMkQsUUFBMUIsQ0FBbUMsTUFBbkM7QUFDQSxlQUFLcEQsd0NBQUw7O0FBRUFQLFVBQUUsK0JBQUYsRUFBbUNrRixVQUFuQyxDQUE4QyxVQUE5QztBQUNELE9BWEQsRUFXR0MsSUFYSCxDQVdRLFVBQUNDLE1BQUQsRUFBWTtBQUNsQnJGLGVBQU9zRixnQkFBUCxDQUF3QkQsT0FBT0UsWUFBL0I7O0FBRUF0RixVQUFFLCtCQUFGLEVBQW1Da0YsVUFBbkMsQ0FBOEMsVUFBOUM7QUFDRCxPQWZEO0FBZ0JEOztBQUVEOzs7Ozs7MENBR3NCO0FBQUE7O0FBQ3BCLFVBQU1LLFVBQVV2RixFQUFFLGlDQUFGLEVBQXFDbUMsSUFBckMsQ0FBMEMsYUFBMUMsQ0FBaEI7QUFDQSxVQUFNSSxrQkFBa0J2QyxFQUFFLGlDQUFGLEVBQXFDYyxJQUFyQyxDQUEwQyxpQkFBMUMsQ0FBeEI7QUFDQSxVQUFNRCxNQUFNMEUsUUFBUXhFLE9BQVIsQ0FBZ0IsYUFBaEIsY0FBeUN3QixlQUF6QyxDQUFaOztBQUVBO0FBQ0EsVUFBTXpCLE9BQU9kLEVBQUUsaUdBQUYsRUFBcUcrRSxTQUFyRyxFQUFiOztBQUVBL0UsUUFBRSwwQ0FBRixFQUE4Q21DLElBQTlDLENBQW1ELFVBQW5ELEVBQStELFVBQS9EOztBQUVBbkMsUUFBRWlCLElBQUYsQ0FBTztBQUNMQyxjQUFNLE1BREQ7QUFFTEwsZ0JBRks7QUFHTEM7QUFISyxPQUFQLEVBS0dLLElBTEgsQ0FLUSxZQUFNO0FBQ1ZwQixlQUFPaUYsa0JBQVAsQ0FBMEJqRixPQUFPa0YscUJBQVAsQ0FBNkIscUJBQTdCLENBQTFCO0FBQ0EsZUFBS2hCLGdDQUFMO0FBQ0EsZUFBSzFELHdDQUFMO0FBQ0FQLFVBQUUsMENBQUYsRUFBOENrRixVQUE5QyxDQUF5RCxVQUF6RDtBQUNELE9BVkgsRUFXR0MsSUFYSCxDQVdRLFVBQUNDLE1BQUQsRUFBWTtBQUNoQnJGLGVBQU9zRixnQkFBUCxDQUF3QkQsT0FBT0UsWUFBL0I7O0FBRUF0RixVQUFFLDBDQUFGLEVBQThDa0YsVUFBOUMsQ0FBeUQsVUFBekQ7QUFDRCxPQWZIO0FBZ0JEOztBQUVEOzs7Ozs7Ozt3Q0FLb0JNLFcsRUFBYTtBQUFBOztBQUMvQnpGLGFBQU8wRixpQkFBUCxDQUF5QkMsTUFBekIsQ0FDRTNGLE9BQU9rRixxQkFBUCxDQUE2Qiw0Q0FBN0IsQ0FERixFQUVFLElBRkYsRUFHRTtBQUNFVSxvQkFBWSxzQkFBTTtBQUNoQixjQUFNOUUsTUFBTWIsRUFBRXdGLFdBQUYsRUFBZXJELElBQWYsQ0FBb0IsTUFBcEIsQ0FBWjtBQUNBbkMsWUFBRXdGLFdBQUYsRUFBZXJELElBQWYsQ0FBb0IsVUFBcEIsRUFBZ0MsVUFBaEM7O0FBRUFuQyxZQUFFaUIsSUFBRixDQUFPO0FBQ0xDLGtCQUFNLEtBREQ7QUFFTEw7QUFGSyxXQUFQLEVBR0dNLElBSEgsQ0FHUSxVQUFDeUUsUUFBRCxFQUFjO0FBQ3BCLG1CQUFLckYsd0NBQUw7QUFDQVIsbUJBQU9pRixrQkFBUCxDQUEwQlksUUFBMUI7QUFDQTVGLGNBQUV3RixXQUFGLEVBQWVOLFVBQWYsQ0FBMEIsVUFBMUI7QUFDRCxXQVBELEVBT0dDLElBUEgsQ0FPUSxVQUFDQyxNQUFELEVBQVk7QUFDbEJyRixtQkFBT3NGLGdCQUFQLENBQXdCRCxPQUFPRSxZQUEvQjtBQUNBdEYsY0FBRXdGLFdBQUYsRUFBZU4sVUFBZixDQUEwQixVQUExQjtBQUNELFdBVkQ7QUFXRDtBQWhCSCxPQUhGLEVBcUJFVyxJQXJCRjtBQXNCRDs7QUFFRDs7Ozs7Ozs7O2tEQU04QjtBQUM1QixVQUFNQyxVQUFVLEtBQUt6Riw2QkFBckI7O0FBRUFMLFFBQUUsc0JBQUYsRUFBMEJzQixJQUExQixDQUErQixjQUEvQixFQUErQ1MsSUFBL0MsQ0FBb0QsVUFBQ0MsS0FBRCxFQUFRK0QsS0FBUixFQUFrQjtBQUNwRUQsZ0JBQVE5RixFQUFFK0YsS0FBRixFQUFTNUQsSUFBVCxDQUFjLElBQWQsQ0FBUixJQUErQm5DLEVBQUUrRixLQUFGLEVBQVN4QixHQUFULEVBQS9CO0FBQ0QsT0FGRDs7QUFJQXZFLFFBQUUsc0JBQUYsRUFBMEJzQixJQUExQixDQUErQixnQkFBL0IsRUFBaURTLElBQWpELENBQXNELFVBQUNDLEtBQUQsRUFBUStELEtBQVIsRUFBa0I7QUFDdEVELGdCQUFROUYsRUFBRStGLEtBQUYsRUFBUzVELElBQVQsQ0FBYyxJQUFkLENBQVIsSUFBK0JuQyxFQUFFK0YsS0FBRixFQUFTdkIsSUFBVCxDQUFjLFNBQWQsQ0FBL0I7QUFDRCxPQUZEOztBQUlBLFdBQUtuRSw2QkFBTCxHQUFxQ3lGLE9BQXJDO0FBQ0Q7O0FBRUQ7Ozs7Ozs7O2dFQUs0Q3hDLGtCLEVBQW9CO0FBQzlELFVBQU1DLGlCQUFpQixLQUFLQyxpQkFBTCxDQUF1QkYsa0JBQXZCLENBQXZCOztBQUVBLFVBQU0wQyxhQUFhaEcsRUFBS3VELGNBQUwsNkJBQW5CO0FBQ0EsVUFBTTFDLE1BQU1tRixXQUNUN0QsSUFEUyxDQUNKLGFBREksRUFFVHBCLE9BRlMsQ0FFRCwyQkFGQyw0QkFFb0QsS0FBS0MsWUFBTCxFQUZwRCxDQUFaOztBQUlBaEIsUUFBRWlCLElBQUYsQ0FBTztBQUNMQyxjQUFNLEtBREQ7QUFFTEw7QUFGSyxPQUFQLEVBR0dNLElBSEgsQ0FHUSxVQUFDOEUsWUFBRCxFQUFrQjtBQUN4QjtBQUNBRCxtQkFBVzFFLElBQVgsQ0FBZ0IsY0FBaEIsRUFBZ0NDLE1BQWhDOztBQUVBdkIsVUFBRStCLElBQUYsQ0FBT2tFLFlBQVAsRUFBcUIsVUFBQ2pFLEtBQUQsRUFBUWtFLFdBQVIsRUFBd0I7QUFDM0NGLHFCQUFXbkUsTUFBWCxxQkFBb0NxRSxZQUFZQyxFQUFoRCxVQUF1REQsWUFBWUUsSUFBbkU7QUFDRCxTQUZEOztBQUlBLFlBQUlKLFdBQVdsRixJQUFYLENBQWdCLG1CQUFoQixNQUF5QyxHQUE3QyxFQUFrRDtBQUNoRGtGLHFCQUFXekIsR0FBWCxDQUFleUIsV0FBV2xGLElBQVgsQ0FBZ0IsbUJBQWhCLENBQWYsRUFBcUR1RixPQUFyRCxDQUE2RCxRQUE3RDtBQUNEO0FBQ0YsT0FkRDtBQWVEOztBQUVEOzs7Ozs7OzswREFLc0MvQyxrQixFQUFvQjtBQUN4RCxVQUFNQyxpQkFBaUIsS0FBS0MsaUJBQUwsQ0FBdUJGLGtCQUF2QixDQUF2Qjs7QUFFQSxVQUFJdEQsRUFBS3VELGNBQUwsd0JBQXdDZ0IsR0FBeEMsT0FBa0QsWUFBdEQsRUFBb0U7QUFDbEV2RSxVQUFLdUQsY0FBTCx1QkFBdUMrQyxJQUF2QztBQUNELE9BRkQsTUFFTztBQUNMdEcsVUFBS3VELGNBQUwsdUJBQXVDc0MsSUFBdkM7QUFDRDtBQUNGOztBQUVEOzs7Ozs7Ozs7d0RBTW9DO0FBQ2xDLFVBQU1VLHlCQUF5QixLQUFLbEcsNkJBQXBDOztBQUVBTCxRQUFFLHNCQUFGLEVBQTBCc0IsSUFBMUIsQ0FBK0IsT0FBL0IsRUFBd0NTLElBQXhDLENBQTZDLFVBQUNDLEtBQUQsRUFBUStELEtBQVIsRUFBa0I7QUFDN0QvRixVQUFFK0YsS0FBRixFQUFTeEIsR0FBVCxDQUFhZ0MsdUJBQXVCdkcsRUFBRStGLEtBQUYsRUFBUzVELElBQVQsQ0FBYyxJQUFkLENBQXZCLENBQWI7QUFDRCxPQUZEOztBQUlBbkMsUUFBRSxzQkFBRixFQUEwQnNCLElBQTFCLENBQStCLFFBQS9CLEVBQXlDUyxJQUF6QyxDQUE4QyxVQUFDQyxLQUFELEVBQVErRCxLQUFSLEVBQWtCO0FBQzlEL0YsVUFBRStGLEtBQUYsRUFBU3hCLEdBQVQsQ0FBYWdDLHVCQUF1QnZHLEVBQUUrRixLQUFGLEVBQVM1RCxJQUFULENBQWMsSUFBZCxDQUF2QixDQUFiLEVBQTBEcUUsTUFBMUQ7QUFDRCxPQUZEOztBQUlBeEcsUUFBRSxzQkFBRixFQUEwQnNCLElBQTFCLENBQStCLGdCQUEvQixFQUFpRFMsSUFBakQsQ0FBc0QsVUFBQ0MsS0FBRCxFQUFRK0QsS0FBUixFQUFrQjtBQUN0RS9GLFVBQUUrRixLQUFGLEVBQVN2QixJQUFULENBQWMsU0FBZCxFQUF5QixJQUF6QjtBQUNELE9BRkQ7QUFHRDs7QUFFRDs7Ozs7Ozs7dURBS21DbEIsa0IsRUFBb0I7QUFDckQsVUFBTUMsaUJBQWlCLEtBQUtDLGlCQUFMLENBQXVCRixrQkFBdkIsQ0FBdkI7O0FBRUF0RCxRQUFLdUQsY0FBTCxlQUErQmlCLElBQS9CLENBQW9DLFVBQXBDLEVBQWdEeEUsRUFBS3VELGNBQUwsbUJBQW1Da0QsRUFBbkMsQ0FBc0MsVUFBdEMsQ0FBaEQsRUFBbUdsQyxHQUFuRyxDQUF1RyxFQUF2RztBQUNEOztBQUVEOzs7Ozs7Ozs7O2tEQU84QmhDLGUsRUFBaUI7QUFBQTs7QUFDN0MsVUFBTTFCLE1BQU1iLEVBQUUseUJBQUYsRUFBNkJjLElBQTdCLENBQWtDLFlBQWxDLEVBQWdEQyxPQUFoRCxDQUF3RCxXQUF4RCxZQUE2RXdCLGVBQTdFLENBQVo7O0FBRUF2QyxRQUFFLDRCQUFGLEVBQWdDMEcsS0FBaEMsQ0FBc0MsTUFBdEM7QUFDQSxXQUFLdEcsZUFBTCxHQUF1QixJQUF2Qjs7QUFFQUosUUFBRWlCLElBQUYsQ0FBTztBQUNMQyxjQUFNLEtBREQ7QUFFTEw7QUFGSyxPQUFQLEVBSUdNLElBSkgsQ0FJUSxVQUFDeUUsUUFBRCxFQUFjO0FBQ2xCLGdCQUFLZSxvQ0FBTCxDQUEwQ2YsUUFBMUM7QUFDQTVGLFVBQUUsaUNBQUYsRUFBcUNjLElBQXJDLENBQTBDLGlCQUExQyxFQUE2RHlCLGVBQTdEO0FBQ0EsZ0JBQUtxRSx5Q0FBTDtBQUNELE9BUkgsRUFTR3pCLElBVEgsQ0FTUSxVQUFDQyxNQUFELEVBQVk7QUFDaEJyRixlQUFPc0YsZ0JBQVAsQ0FBd0JELE9BQU9FLFlBQS9CO0FBQ0QsT0FYSDtBQVlEOztBQUVEOzs7Ozs7dURBR21DO0FBQ2pDdEYsUUFBRSw0QkFBRixFQUFnQzBHLEtBQWhDLENBQXNDLE1BQXRDO0FBQ0EsV0FBS3RHLGVBQUwsR0FBdUIsS0FBdkI7O0FBRUEsVUFBTXlHLHFCQUFxQjdHLEVBQUUsaUNBQUYsQ0FBM0I7O0FBRUE2Ryx5QkFBbUJDLEtBQW5CO0FBQ0Q7O0FBRUQ7Ozs7Ozs7O3lEQUtxQ0MsSSxFQUFNO0FBQ3pDLFVBQU1GLHFCQUFxQjdHLEVBQUUsaUNBQUYsQ0FBM0I7O0FBRUE2Ryx5QkFBbUJDLEtBQW5CO0FBQ0FELHlCQUFtQmhGLE1BQW5CLENBQTBCa0YsSUFBMUI7QUFDRDs7QUFFRDs7Ozs7Ozs7OzttQ0FPZTtBQUNiLGFBQU8vRyxFQUFFLGtCQUFGLEVBQXNCdUUsR0FBdEIsRUFBUDtBQUNEOztBQUVEOzs7Ozs7Ozs7O3NDQU9rQmpCLGtCLEVBQW9CO0FBQ3BDLFVBQUlBLGtCQUFKLEVBQXdCO0FBQ3RCLHFCQUFXLEtBQUtwRCxnQkFBaEI7QUFDRDs7QUFFRCxtQkFBVyxLQUFLQyxjQUFoQjtBQUNEOzs7OztrQkFHWUYsd0I7Ozs7Ozs7Ozs7QUMvZmYsa0JBQWtCLFlBQVksbUJBQU8sQ0FBQyw4R0FBMkMsc0I7Ozs7Ozs7Ozs7O0FDQXBFOztBQUViLGtCQUFrQjs7QUFFbEIsZUFBZTtBQUNmO0FBQ0E7QUFDQTtBQUNBLEU7Ozs7Ozs7Ozs7O0FDUmE7O0FBRWIsa0JBQWtCOztBQUVsQixzQkFBc0IsbUJBQU8sQ0FBQyx5R0FBbUM7O0FBRWpFOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixlQUFlO0FBQ2Y7QUFDQSxtQkFBbUIsa0JBQWtCO0FBQ3JDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDLEc7Ozs7Ozs7Ozs7QUMxQkQsbUJBQU8sQ0FBQyxzSEFBMEM7QUFDbEQsY0FBYyx3R0FBcUM7QUFDbkQ7QUFDQTtBQUNBOzs7Ozs7Ozs7OztBQ0pBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7OztBQ0hBLGVBQWUsbUJBQU8sQ0FBQywwRUFBYztBQUNyQztBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNKQSw2QkFBNkI7QUFDN0IsdUNBQXVDOzs7Ozs7Ozs7OztBQ0R2QztBQUNBLGdCQUFnQixtQkFBTyxDQUFDLDRFQUFlO0FBQ3ZDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNuQkE7QUFDQSxrQkFBa0IsbUJBQU8sQ0FBQyxrRUFBVTtBQUNwQyxpQ0FBaUMsUUFBUSxtQkFBbUIsVUFBVSxFQUFFLEVBQUU7QUFDMUUsQ0FBQzs7Ozs7Ozs7Ozs7QUNIRCxlQUFlLG1CQUFPLENBQUMsMEVBQWM7QUFDckMsZUFBZSxrR0FBNkI7QUFDNUM7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNOQSxhQUFhLG1CQUFPLENBQUMsb0VBQVc7QUFDaEMsV0FBVyxtQkFBTyxDQUFDLGdFQUFTO0FBQzVCLFVBQVUsbUJBQU8sQ0FBQyw4REFBUTtBQUMxQixXQUFXLG1CQUFPLENBQUMsZ0VBQVM7QUFDNUIsVUFBVSxtQkFBTyxDQUFDLDhEQUFRO0FBQzFCOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUVBQWlFO0FBQ2pFO0FBQ0Esa0ZBQWtGO0FBQ2xGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXO0FBQ1gsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSwrQ0FBK0M7QUFDL0M7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsY0FBYztBQUNkLGNBQWM7QUFDZCxjQUFjO0FBQ2QsY0FBYztBQUNkLGVBQWU7QUFDZixlQUFlO0FBQ2YsZUFBZTtBQUNmLGdCQUFnQjtBQUNoQjs7Ozs7Ozs7Ozs7QUM3REE7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNOQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EseUNBQXlDOzs7Ozs7Ozs7OztBQ0x6Qyx1QkFBdUI7QUFDdkI7QUFDQTtBQUNBOzs7Ozs7Ozs7OztBQ0hBLFNBQVMsbUJBQU8sQ0FBQywwRUFBYztBQUMvQixpQkFBaUIsbUJBQU8sQ0FBQyxrRkFBa0I7QUFDM0MsaUJBQWlCLG1CQUFPLENBQUMsOEVBQWdCO0FBQ3pDO0FBQ0EsQ0FBQztBQUNEO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNQQSxrQkFBa0IsbUJBQU8sQ0FBQyw4RUFBZ0IsTUFBTSxtQkFBTyxDQUFDLGtFQUFVO0FBQ2xFLCtCQUErQixtQkFBTyxDQUFDLDRFQUFlLGdCQUFnQixtQkFBbUIsVUFBVSxFQUFFLEVBQUU7QUFDdkcsQ0FBQzs7Ozs7Ozs7Ozs7QUNGRDtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7O0FDRkEsZUFBZSxtQkFBTyxDQUFDLDBFQUFjO0FBQ3JDLHFCQUFxQixtQkFBTyxDQUFDLG9GQUFtQjtBQUNoRCxrQkFBa0IsbUJBQU8sQ0FBQyxnRkFBaUI7QUFDM0M7O0FBRUEsU0FBUyxHQUFHLG1CQUFPLENBQUMsOEVBQWdCO0FBQ3BDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHLFlBQVk7QUFDZjtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNmQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7OztBQ1BBO0FBQ0EsZUFBZSxtQkFBTyxDQUFDLDBFQUFjO0FBQ3JDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7OztBQ1hBLGNBQWMsbUJBQU8sQ0FBQyxvRUFBVztBQUNqQztBQUNBLGlDQUFpQyxtQkFBTyxDQUFDLDhFQUFnQixjQUFjLGlCQUFpQixpR0FBeUIsRUFBRTs7Ozs7OztVQ0ZuSDtVQUNBOztVQUVBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBOztVQUVBO1VBQ0E7O1VBRUE7VUFDQTtVQUNBOzs7Ozs7Ozs7Ozs7QUNHQTs7Ozs7O2NBRVlGLE07SUFBTEMsQyxXQUFBQSxDLEVBM0JQOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBNkJBQSxFQUFFLFlBQU07QUFDTixNQUFJQyxrQ0FBSjtBQUNELENBRkQsRSIsImZpbGUiOiJjYXRhbG9nX3Byb2R1Y3QuYnVuZGxlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiBDb3B5cmlnaHQgc2luY2UgMjAwNyBQcmVzdGFTaG9wIFNBIGFuZCBDb250cmlidXRvcnNcbiAqIFByZXN0YVNob3AgaXMgYW4gSW50ZXJuYXRpb25hbCBSZWdpc3RlcmVkIFRyYWRlbWFyayAmIFByb3BlcnR5IG9mIFByZXN0YVNob3AgU0FcbiAqXG4gKiBOT1RJQ0UgT0YgTElDRU5TRVxuICpcbiAqIFRoaXMgc291cmNlIGZpbGUgaXMgc3ViamVjdCB0byB0aGUgT3BlbiBTb2Z0d2FyZSBMaWNlbnNlIChPU0wgMy4wKVxuICogdGhhdCBpcyBidW5kbGVkIHdpdGggdGhpcyBwYWNrYWdlIGluIHRoZSBmaWxlIExJQ0VOU0UubWQuXG4gKiBJdCBpcyBhbHNvIGF2YWlsYWJsZSB0aHJvdWdoIHRoZSB3b3JsZC13aWRlLXdlYiBhdCB0aGlzIFVSTDpcbiAqIGh0dHBzOi8vb3BlbnNvdXJjZS5vcmcvbGljZW5zZXMvT1NMLTMuMFxuICogSWYgeW91IGRpZCBub3QgcmVjZWl2ZSBhIGNvcHkgb2YgdGhlIGxpY2Vuc2UgYW5kIGFyZSB1bmFibGUgdG9cbiAqIG9idGFpbiBpdCB0aHJvdWdoIHRoZSB3b3JsZC13aWRlLXdlYiwgcGxlYXNlIHNlbmQgYW4gZW1haWxcbiAqIHRvIGxpY2Vuc2VAcHJlc3Rhc2hvcC5jb20gc28gd2UgY2FuIHNlbmQgeW91IGEgY29weSBpbW1lZGlhdGVseS5cbiAqXG4gKiBESVNDTEFJTUVSXG4gKlxuICogRG8gbm90IGVkaXQgb3IgYWRkIHRvIHRoaXMgZmlsZSBpZiB5b3Ugd2lzaCB0byB1cGdyYWRlIFByZXN0YVNob3AgdG8gbmV3ZXJcbiAqIHZlcnNpb25zIGluIHRoZSBmdXR1cmUuIElmIHlvdSB3aXNoIHRvIGN1c3RvbWl6ZSBQcmVzdGFTaG9wIGZvciB5b3VyXG4gKiBuZWVkcyBwbGVhc2UgcmVmZXIgdG8gaHR0cHM6Ly9kZXZkb2NzLnByZXN0YXNob3AuY29tLyBmb3IgbW9yZSBpbmZvcm1hdGlvbi5cbiAqXG4gKiBAYXV0aG9yICAgIFByZXN0YVNob3AgU0EgYW5kIENvbnRyaWJ1dG9ycyA8Y29udGFjdEBwcmVzdGFzaG9wLmNvbT5cbiAqIEBjb3B5cmlnaHQgU2luY2UgMjAwNyBQcmVzdGFTaG9wIFNBIGFuZCBDb250cmlidXRvcnNcbiAqIEBsaWNlbnNlICAgaHR0cHM6Ly9vcGVuc291cmNlLm9yZy9saWNlbnNlcy9PU0wtMy4wIE9wZW4gU29mdHdhcmUgTGljZW5zZSAoT1NMIDMuMClcbiAqL1xuXG5jb25zdCB7JH0gPSB3aW5kb3c7XG5cbmNsYXNzIFNwZWNpZmljUHJpY2VGb3JtSGFuZGxlciB7XG4gIGNvbnN0cnVjdG9yKCkge1xuICAgIHRoaXMucHJlZml4Q3JlYXRlRm9ybSA9ICdmb3JtX3N0ZXAyX3NwZWNpZmljX3ByaWNlXyc7XG4gICAgdGhpcy5wcmVmaXhFZGl0Rm9ybSA9ICdmb3JtX21vZGFsXyc7XG4gICAgdGhpcy5lZGl0TW9kYWxJc09wZW4gPSBmYWxzZTtcblxuICAgIHRoaXMuJGNyZWF0ZVByaWNlRm9ybURlZmF1bHRWYWx1ZXMgPSB7fTtcbiAgICB0aGlzLnN0b3JlUHJpY2VGb3JtRGVmYXVsdFZhbHVlcygpO1xuXG4gICAgdGhpcy5sb2FkQW5kRGlzcGxheUV4aXN0aW5nU3BlY2lmaWNQcmljZXNMaXN0KCk7XG5cbiAgICB0aGlzLmNvbmZpZ3VyZUFkZFByaWNlRm9ybUJlaGF2aW9yKCk7XG5cbiAgICB0aGlzLmNvbmZpZ3VyZUVkaXRQcmljZU1vZGFsQmVoYXZpb3IoKTtcblxuICAgIHRoaXMuY29uZmlndXJlRGVsZXRlUHJpY2VCdXR0b25zQmVoYXZpb3IoKTtcblxuICAgIHRoaXMuY29uZmlndXJlTXVsdGlwbGVNb2RhbHNCZWhhdmlvcigpO1xuICB9XG5cbiAgLyoqXG4gICAqIEBwcml2YXRlXG4gICAqL1xuICBsb2FkQW5kRGlzcGxheUV4aXN0aW5nU3BlY2lmaWNQcmljZXNMaXN0KCkge1xuICAgIGNvbnN0IGxpc3RDb250YWluZXIgPSAkKCcjanMtc3BlY2lmaWMtcHJpY2UtbGlzdCcpO1xuICAgIGNvbnN0IHVybCA9IGxpc3RDb250YWluZXIuZGF0YSgnbGlzdGluZ1VybCcpLnJlcGxhY2UoL2xpc3RcXC9cXGQrLywgYGxpc3QvJHt0aGlzLmdldFByb2R1Y3RJZCgpfWApO1xuXG4gICAgJC5hamF4KHtcbiAgICAgIHR5cGU6ICdHRVQnLFxuICAgICAgdXJsLFxuICAgIH0pXG4gICAgICAuZG9uZSgoc3BlY2lmaWNQcmljZXMpID0+IHtcbiAgICAgICAgY29uc3QgdGJvZHkgPSBsaXN0Q29udGFpbmVyLmZpbmQoJ3Rib2R5Jyk7XG4gICAgICAgIHRib2R5LmZpbmQoJ3RyJykucmVtb3ZlKCk7XG5cbiAgICAgICAgaWYgKHNwZWNpZmljUHJpY2VzLmxlbmd0aCA+IDApIHtcbiAgICAgICAgICBsaXN0Q29udGFpbmVyLnJlbW92ZUNsYXNzKCdoaWRlJyk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgbGlzdENvbnRhaW5lci5hZGRDbGFzcygnaGlkZScpO1xuICAgICAgICB9XG5cbiAgICAgICAgY29uc3Qgc3BlY2lmaWNQcmljZXNMaXN0ID0gdGhpcy5yZW5kZXJTcGVjaWZpY1ByaWNlc0xpc3RpbmdBc0h0bWwoc3BlY2lmaWNQcmljZXMpO1xuXG4gICAgICAgIHRib2R5LmFwcGVuZChzcGVjaWZpY1ByaWNlc0xpc3QpO1xuICAgICAgfSk7XG4gIH1cblxuICAvKipcbiAgICogQHBhcmFtIGFycmF5IHNwZWNpZmljUHJpY2VzXG4gICAqXG4gICAqIEByZXR1cm5zIHN0cmluZ1xuICAgKlxuICAgKiBAcHJpdmF0ZVxuICAgKi9cbiAgcmVuZGVyU3BlY2lmaWNQcmljZXNMaXN0aW5nQXNIdG1sKHNwZWNpZmljUHJpY2VzKSB7XG4gICAgbGV0IHNwZWNpZmljUHJpY2VzTGlzdCA9ICcnO1xuXG4gICAgY29uc3Qgc2VsZiA9IHRoaXM7XG5cbiAgICAkLmVhY2goc3BlY2lmaWNQcmljZXMsIChpbmRleCwgc3BlY2lmaWNQcmljZSkgPT4ge1xuICAgICAgY29uc3QgZGVsZXRlVXJsID0gJCgnI2pzLXNwZWNpZmljLXByaWNlLWxpc3QnKVxuICAgICAgICAuYXR0cignZGF0YS1hY3Rpb24tZGVsZXRlJylcbiAgICAgICAgLnJlcGxhY2UoL2RlbGV0ZVxcL1xcZCsvLCBgZGVsZXRlLyR7c3BlY2lmaWNQcmljZS5pZF9zcGVjaWZpY19wcmljZX1gKTtcbiAgICAgIGNvbnN0IHJvdyA9IHNlbGYucmVuZGVyU3BlY2lmaWNQcmljZVJvdyhzcGVjaWZpY1ByaWNlLCBkZWxldGVVcmwpO1xuXG4gICAgICBzcGVjaWZpY1ByaWNlc0xpc3QgKz0gcm93O1xuICAgIH0pO1xuXG4gICAgcmV0dXJuIHNwZWNpZmljUHJpY2VzTGlzdDtcbiAgfVxuXG4gIC8qKlxuICAgKiBAcGFyYW0gT2JqZWN0IHNwZWNpZmljUHJpY2VcbiAgICogQHBhcmFtIHN0cmluZyBkZWxldGVVcmxcbiAgICpcbiAgICogQHJldHVybnMgc3RyaW5nXG4gICAqXG4gICAqIEBwcml2YXRlXG4gICAqL1xuICByZW5kZXJTcGVjaWZpY1ByaWNlUm93KHNwZWNpZmljUHJpY2UsIGRlbGV0ZVVybCkge1xuICAgIGNvbnN0IHNwZWNpZmljUHJpY2VJZCA9IHNwZWNpZmljUHJpY2UuaWRfc3BlY2lmaWNfcHJpY2U7XG5cbiAgICAvKiBlc2xpbnQtZGlzYWJsZSBtYXgtbGVuICovXG4gICAgY29uc3QgY2FuRGVsZXRlID0gc3BlY2lmaWNQcmljZS5jYW5fZGVsZXRlXG4gICAgICA/IGA8YSBocmVmPVwiJHtkZWxldGVVcmx9XCIgY2xhc3M9XCJqcy1kZWxldGUgZGVsZXRlIGJ0biB0b29sdGlwLWxpbmsgZGVsZXRlIHBsLTAgcHItMFwiPjxpIGNsYXNzPVwibWF0ZXJpYWwtaWNvbnNcIj5kZWxldGU8L2k+PC9hPmBcbiAgICAgIDogJyc7XG4gICAgY29uc3QgY2FuRWRpdCA9IHNwZWNpZmljUHJpY2UuY2FuX2VkaXRcbiAgICAgID8gYDxhIGhyZWY9XCIjXCIgZGF0YS1zcGVjaWZpYy1wcmljZS1pZD1cIiR7c3BlY2lmaWNQcmljZUlkfVwiIGNsYXNzPVwianMtZWRpdCBlZGl0IGJ0biB0b29sdGlwLWxpbmsgZGVsZXRlIHBsLTAgcHItMFwiPjxpIGNsYXNzPVwibWF0ZXJpYWwtaWNvbnNcIj5lZGl0PC9pPjwvYT5gXG4gICAgICA6ICcnO1xuICAgIGNvbnN0IHJvdyA9IGA8dHI+IFxcXG4gICAgPHRkPiR7c3BlY2lmaWNQcmljZS5pZF9zcGVjaWZpY19wcmljZX08L3RkPiBcXFxuICAgIDx0ZD4ke3NwZWNpZmljUHJpY2UucnVsZV9uYW1lfTwvdGQ+IFxcXG4gICAgPHRkPiR7c3BlY2lmaWNQcmljZS5hdHRyaWJ1dGVzX25hbWV9PC90ZD4gXFxcbiAgICA8dGQ+JHtzcGVjaWZpY1ByaWNlLmN1cnJlbmN5fTwvdGQ+IFxcXG4gICAgPHRkPiR7c3BlY2lmaWNQcmljZS5jb3VudHJ5fTwvdGQ+IFxcXG4gICAgPHRkPiR7c3BlY2lmaWNQcmljZS5ncm91cH08L3RkPiBcXFxuICAgIDx0ZD4ke3NwZWNpZmljUHJpY2UuY3VzdG9tZXJ9PC90ZD4gXFxcbiAgICA8dGQ+JHtzcGVjaWZpY1ByaWNlLmZpeGVkX3ByaWNlfTwvdGQ+IFxcXG4gICAgPHRkPiR7c3BlY2lmaWNQcmljZS5pbXBhY3R9PC90ZD4gXFxcbiAgICA8dGQ+JHtzcGVjaWZpY1ByaWNlLnBlcmlvZH08L3RkPiBcXFxuICAgIDx0ZD4ke3NwZWNpZmljUHJpY2UuZnJvbV9xdWFudGl0eX08L3RkPiBcXFxuICAgIDx0ZD4ke2NhbkRlbGV0ZX08L3RkPiBcXFxuICAgIDx0ZD4ke2NhbkVkaXR9PC90ZD48L3RyPmA7XG4gICAgLyogZXNsaW50LWVuYWJsZSBtYXgtbGVuICovXG5cbiAgICByZXR1cm4gcm93O1xuICB9XG5cbiAgLyoqXG4gICAqIEBwcml2YXRlXG4gICAqL1xuICBjb25maWd1cmVBZGRQcmljZUZvcm1CZWhhdmlvcigpIHtcbiAgICBjb25zdCB1c2VQcmVmaXhGb3JDcmVhdGUgPSB0cnVlO1xuICAgIGNvbnN0IHNlbGVjdG9yUHJlZml4ID0gdGhpcy5nZXRQcmVmaXhTZWxlY3Rvcih1c2VQcmVmaXhGb3JDcmVhdGUpO1xuXG4gICAgJCgnI3NwZWNpZmljX3ByaWNlX2Zvcm0gLmpzLWNhbmNlbCcpLmNsaWNrKCgpID0+IHtcbiAgICAgIHRoaXMucmVzZXRDcmVhdGVQcmljZUZvcm1EZWZhdWx0VmFsdWVzKCk7XG4gICAgICAkKCcjc3BlY2lmaWNfcHJpY2VfZm9ybScpLmNvbGxhcHNlKCdoaWRlJyk7XG4gICAgfSk7XG5cbiAgICAkKCcjc3BlY2lmaWNfcHJpY2VfZm9ybSAuanMtc2F2ZScpLm9uKFxuICAgICAgJ2NsaWNrJyxcbiAgICAgICgpID0+IHRoaXMuc3VibWl0Q3JlYXRlUHJpY2VGb3JtKCksXG4gICAgKTtcblxuICAgICQoJyNqcy1vcGVuLWNyZWF0ZS1zcGVjaWZpYy1wcmljZS1mb3JtJykub24oXG4gICAgICAnY2xpY2snLFxuICAgICAgKCkgPT4gdGhpcy5sb2FkQW5kRmlsbE9wdGlvbnNGb3JTZWxlY3RDb21iaW5hdGlvbklucHV0KHVzZVByZWZpeEZvckNyZWF0ZSksXG4gICAgKTtcblxuICAgICQoYCR7c2VsZWN0b3JQcmVmaXh9bGVhdmVfYnByaWNlYCkub24oXG4gICAgICAnY2xpY2snLFxuICAgICAgKCkgPT4gdGhpcy5lbmFibGVTcGVjaWZpY1ByaWNlRmllbGRJZkVsaWdpYmxlKHVzZVByZWZpeEZvckNyZWF0ZSksXG4gICAgKTtcblxuICAgICQoYCR7c2VsZWN0b3JQcmVmaXh9c3BfcmVkdWN0aW9uX3R5cGVgKS5vbihcbiAgICAgICdjaGFuZ2UnLFxuICAgICAgKCkgPT4gdGhpcy5lbmFibGVTcGVjaWZpY1ByaWNlVGF4RmllbGRJZkVsaWdpYmxlKHVzZVByZWZpeEZvckNyZWF0ZSksXG4gICAgKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBAcHJpdmF0ZVxuICAgKi9cbiAgY29uZmlndXJlRWRpdFByaWNlRm9ybUluc2lkZU1vZGFsQmVoYXZpb3IoKSB7XG4gICAgY29uc3QgdXNlUHJlZml4Rm9yQ3JlYXRlID0gZmFsc2U7XG4gICAgY29uc3Qgc2VsZWN0b3JQcmVmaXggPSB0aGlzLmdldFByZWZpeFNlbGVjdG9yKHVzZVByZWZpeEZvckNyZWF0ZSk7XG5cbiAgICAkKCcjZm9ybV9tb2RhbF9jYW5jZWwnKS5jbGljaygoKSA9PiB0aGlzLmNsb3NlRWRpdFByaWNlTW9kYWxBbmRSZW1vdmVGb3JtKCkpO1xuICAgICQoJyNmb3JtX21vZGFsX2Nsb3NlJykuY2xpY2soKCkgPT4gdGhpcy5jbG9zZUVkaXRQcmljZU1vZGFsQW5kUmVtb3ZlRm9ybSgpKTtcblxuICAgICQoJyNmb3JtX21vZGFsX3NhdmUnKS5jbGljaygoKSA9PiB0aGlzLnN1Ym1pdEVkaXRQcmljZUZvcm0oKSk7XG5cbiAgICB0aGlzLmxvYWRBbmRGaWxsT3B0aW9uc0ZvclNlbGVjdENvbWJpbmF0aW9uSW5wdXQodXNlUHJlZml4Rm9yQ3JlYXRlKTtcblxuICAgICQoYCR7c2VsZWN0b3JQcmVmaXh9bGVhdmVfYnByaWNlYCkub24oJ2NsaWNrJywgKCkgPT4gdGhpcy5lbmFibGVTcGVjaWZpY1ByaWNlRmllbGRJZkVsaWdpYmxlKHVzZVByZWZpeEZvckNyZWF0ZSkpO1xuXG4gICAgJChgJHtzZWxlY3RvclByZWZpeH1zcF9yZWR1Y3Rpb25fdHlwZWApLm9uKFxuICAgICAgJ2NoYW5nZScsXG4gICAgICAoKSA9PiB0aGlzLmVuYWJsZVNwZWNpZmljUHJpY2VUYXhGaWVsZElmRWxpZ2libGUodXNlUHJlZml4Rm9yQ3JlYXRlKSxcbiAgICApO1xuXG4gICAgdGhpcy5yZWluaXRpYWxpemVEYXRlUGlja2VycygpO1xuXG4gICAgdGhpcy5pbml0aWFsaXplTGVhdmVCUHJpY2VGaWVsZCh1c2VQcmVmaXhGb3JDcmVhdGUpO1xuICAgIHRoaXMuZW5hYmxlU3BlY2lmaWNQcmljZVRheEZpZWxkSWZFbGlnaWJsZSh1c2VQcmVmaXhGb3JDcmVhdGUpO1xuICB9XG5cbiAgLyoqXG4gICAqIEBwcml2YXRlXG4gICAqL1xuICByZWluaXRpYWxpemVEYXRlUGlja2VycygpIHtcbiAgICAkKCcuZGF0ZXBpY2tlciBpbnB1dCcpLmRhdGV0aW1lcGlja2VyKHtmb3JtYXQ6ICdZWVlZLU1NLUREJ30pO1xuICB9XG5cbiAgLyoqXG4gICAqIEBwYXJhbSBib29sZWFuIHVzZVByZWZpeEZvckNyZWF0ZVxuICAgKlxuICAgKiBAcHJpdmF0ZVxuICAgKi9cbiAgaW5pdGlhbGl6ZUxlYXZlQlByaWNlRmllbGQodXNlUHJlZml4Rm9yQ3JlYXRlKSB7XG4gICAgY29uc3Qgc2VsZWN0b3JQcmVmaXggPSB0aGlzLmdldFByZWZpeFNlbGVjdG9yKHVzZVByZWZpeEZvckNyZWF0ZSk7XG5cbiAgICBpZiAoJChgJHtzZWxlY3RvclByZWZpeH1zcF9wcmljZWApLnZhbCgpICE9PSAnJykge1xuICAgICAgJChgJHtzZWxlY3RvclByZWZpeH1zcF9wcmljZWApLnByb3AoJ2Rpc2FibGVkJywgZmFsc2UpO1xuICAgICAgJChgJHtzZWxlY3RvclByZWZpeH1sZWF2ZV9icHJpY2VgKS5wcm9wKCdjaGVja2VkJywgZmFsc2UpO1xuICAgIH1cbiAgfVxuXG4gIC8qKlxuICAgKiBAcHJpdmF0ZVxuICAgKi9cbiAgY29uZmlndXJlRWRpdFByaWNlTW9kYWxCZWhhdmlvcigpIHtcbiAgICAkKGRvY3VtZW50KS5vbignY2xpY2snLCAnI2pzLXNwZWNpZmljLXByaWNlLWxpc3QgLmpzLWVkaXQnLCAoZXZlbnQpID0+IHtcbiAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG5cbiAgICAgIGNvbnN0IHNwZWNpZmljUHJpY2VJZCA9ICQoZXZlbnQuY3VycmVudFRhcmdldCkuZGF0YSgnc3BlY2lmaWNQcmljZUlkJyk7XG5cbiAgICAgIHRoaXMub3BlbkVkaXRQcmljZU1vZGFsQW5kTG9hZEZvcm0oc3BlY2lmaWNQcmljZUlkKTtcbiAgICB9KTtcbiAgfVxuXG4gIC8qKlxuICAgKiBAcHJpdmF0ZVxuICAgKi9cbiAgY29uZmlndXJlRGVsZXRlUHJpY2VCdXR0b25zQmVoYXZpb3IoKSB7XG4gICAgJChkb2N1bWVudCkub24oJ2NsaWNrJywgJyNqcy1zcGVjaWZpYy1wcmljZS1saXN0IC5qcy1kZWxldGUnLCAoZXZlbnQpID0+IHtcbiAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICB0aGlzLmRlbGV0ZVNwZWNpZmljUHJpY2UoZXZlbnQuY3VycmVudFRhcmdldCk7XG4gICAgfSk7XG4gIH1cblxuICBjb25maWd1cmVNdWx0aXBsZU1vZGFsc0JlaGF2aW9yKCkge1xuICAgICQoJy5tb2RhbCcpLm9uKCdoaWRkZW4uYnMubW9kYWwnLCAoKSA9PiB7XG4gICAgICBpZiAodGhpcy5lZGl0TW9kYWxJc09wZW4pIHtcbiAgICAgICAgJCgnYm9keScpLmFkZENsYXNzKCdtb2RhbC1vcGVuJyk7XG4gICAgICB9XG4gICAgfSk7XG4gIH1cblxuICAvKipcbiAgICogQHByaXZhdGVcbiAgICovXG4gIHN1Ym1pdENyZWF0ZVByaWNlRm9ybSgpIHtcbiAgICBjb25zdCB1cmwgPSAkKCcjc3BlY2lmaWNfcHJpY2VfZm9ybScpLmF0dHIoJ2RhdGEtYWN0aW9uJyk7XG4gICAgY29uc3QgZGF0YSA9ICQoJyNzcGVjaWZpY19wcmljZV9mb3JtIGlucHV0LCAjc3BlY2lmaWNfcHJpY2VfZm9ybSBzZWxlY3QsICNmb3JtX2lkX3Byb2R1Y3QnKS5zZXJpYWxpemUoKTtcblxuICAgICQoJyNzcGVjaWZpY19wcmljZV9mb3JtIC5qcy1zYXZlJykuYXR0cignZGlzYWJsZWQnLCAnZGlzYWJsZWQnKTtcblxuICAgICQuYWpheCh7XG4gICAgICB0eXBlOiAnUE9TVCcsXG4gICAgICB1cmwsXG4gICAgICBkYXRhLFxuICAgIH0pLmRvbmUoKCkgPT4ge1xuICAgICAgd2luZG93LnNob3dTdWNjZXNzTWVzc2FnZSh3aW5kb3cudHJhbnNsYXRlX2phdmFzY3JpcHRzWydGb3JtIHVwZGF0ZSBzdWNjZXNzJ10pO1xuICAgICAgdGhpcy5yZXNldENyZWF0ZVByaWNlRm9ybURlZmF1bHRWYWx1ZXMoKTtcbiAgICAgICQoJyNzcGVjaWZpY19wcmljZV9mb3JtJykuY29sbGFwc2UoJ2hpZGUnKTtcbiAgICAgIHRoaXMubG9hZEFuZERpc3BsYXlFeGlzdGluZ1NwZWNpZmljUHJpY2VzTGlzdCgpO1xuXG4gICAgICAkKCcjc3BlY2lmaWNfcHJpY2VfZm9ybSAuanMtc2F2ZScpLnJlbW92ZUF0dHIoJ2Rpc2FibGVkJyk7XG4gICAgfSkuZmFpbCgoZXJyb3JzKSA9PiB7XG4gICAgICB3aW5kb3cuc2hvd0Vycm9yTWVzc2FnZShlcnJvcnMucmVzcG9uc2VKU09OKTtcblxuICAgICAgJCgnI3NwZWNpZmljX3ByaWNlX2Zvcm0gLmpzLXNhdmUnKS5yZW1vdmVBdHRyKCdkaXNhYmxlZCcpO1xuICAgIH0pO1xuICB9XG5cbiAgLyoqXG4gICAqIEBwcml2YXRlXG4gICAqL1xuICBzdWJtaXRFZGl0UHJpY2VGb3JtKCkge1xuICAgIGNvbnN0IGJhc2VVcmwgPSAkKCcjZWRpdC1zcGVjaWZpYy1wcmljZS1tb2RhbC1mb3JtJykuYXR0cignZGF0YS1hY3Rpb24nKTtcbiAgICBjb25zdCBzcGVjaWZpY1ByaWNlSWQgPSAkKCcjZWRpdC1zcGVjaWZpYy1wcmljZS1tb2RhbC1mb3JtJykuZGF0YSgnc3BlY2lmaWNQcmljZUlkJyk7XG4gICAgY29uc3QgdXJsID0gYmFzZVVybC5yZXBsYWNlKC91cGRhdGVcXC9cXGQrLywgYHVwZGF0ZS8ke3NwZWNpZmljUHJpY2VJZH1gKTtcblxuICAgIC8qIGVzbGludC1kaXNhYmxlLW5leHQtbGluZSBtYXgtbGVuICovXG4gICAgY29uc3QgZGF0YSA9ICQoJyNlZGl0LXNwZWNpZmljLXByaWNlLW1vZGFsLWZvcm0gaW5wdXQsICNlZGl0LXNwZWNpZmljLXByaWNlLW1vZGFsLWZvcm0gc2VsZWN0LCAjZm9ybV9pZF9wcm9kdWN0Jykuc2VyaWFsaXplKCk7XG5cbiAgICAkKCcjZWRpdC1zcGVjaWZpYy1wcmljZS1tb2RhbC1mb3JtIC5qcy1zYXZlJykuYXR0cignZGlzYWJsZWQnLCAnZGlzYWJsZWQnKTtcblxuICAgICQuYWpheCh7XG4gICAgICB0eXBlOiAnUE9TVCcsXG4gICAgICB1cmwsXG4gICAgICBkYXRhLFxuICAgIH0pXG4gICAgICAuZG9uZSgoKSA9PiB7XG4gICAgICAgIHdpbmRvdy5zaG93U3VjY2Vzc01lc3NhZ2Uod2luZG93LnRyYW5zbGF0ZV9qYXZhc2NyaXB0c1snRm9ybSB1cGRhdGUgc3VjY2VzcyddKTtcbiAgICAgICAgdGhpcy5jbG9zZUVkaXRQcmljZU1vZGFsQW5kUmVtb3ZlRm9ybSgpO1xuICAgICAgICB0aGlzLmxvYWRBbmREaXNwbGF5RXhpc3RpbmdTcGVjaWZpY1ByaWNlc0xpc3QoKTtcbiAgICAgICAgJCgnI2VkaXQtc3BlY2lmaWMtcHJpY2UtbW9kYWwtZm9ybSAuanMtc2F2ZScpLnJlbW92ZUF0dHIoJ2Rpc2FibGVkJyk7XG4gICAgICB9KVxuICAgICAgLmZhaWwoKGVycm9ycykgPT4ge1xuICAgICAgICB3aW5kb3cuc2hvd0Vycm9yTWVzc2FnZShlcnJvcnMucmVzcG9uc2VKU09OKTtcblxuICAgICAgICAkKCcjZWRpdC1zcGVjaWZpYy1wcmljZS1tb2RhbC1mb3JtIC5qcy1zYXZlJykucmVtb3ZlQXR0cignZGlzYWJsZWQnKTtcbiAgICAgIH0pO1xuICB9XG5cbiAgLyoqXG4gICAqIEBwYXJhbSBzdHJpbmcgY2xpY2tlZExpbmsgc2VsZWN0b3JcbiAgICpcbiAgICogQHByaXZhdGVcbiAgICovXG4gIGRlbGV0ZVNwZWNpZmljUHJpY2UoY2xpY2tlZExpbmspIHtcbiAgICB3aW5kb3cubW9kYWxDb25maXJtYXRpb24uY3JlYXRlKFxuICAgICAgd2luZG93LnRyYW5zbGF0ZV9qYXZhc2NyaXB0c1snQXJlIHlvdSBzdXJlIHlvdSB3YW50IHRvIGRlbGV0ZSB0aGlzIGl0ZW0/J10sXG4gICAgICBudWxsLFxuICAgICAge1xuICAgICAgICBvbkNvbnRpbnVlOiAoKSA9PiB7XG4gICAgICAgICAgY29uc3QgdXJsID0gJChjbGlja2VkTGluaykuYXR0cignaHJlZicpO1xuICAgICAgICAgICQoY2xpY2tlZExpbmspLmF0dHIoJ2Rpc2FibGVkJywgJ2Rpc2FibGVkJyk7XG5cbiAgICAgICAgICAkLmFqYXgoe1xuICAgICAgICAgICAgdHlwZTogJ0dFVCcsXG4gICAgICAgICAgICB1cmwsXG4gICAgICAgICAgfSkuZG9uZSgocmVzcG9uc2UpID0+IHtcbiAgICAgICAgICAgIHRoaXMubG9hZEFuZERpc3BsYXlFeGlzdGluZ1NwZWNpZmljUHJpY2VzTGlzdCgpO1xuICAgICAgICAgICAgd2luZG93LnNob3dTdWNjZXNzTWVzc2FnZShyZXNwb25zZSk7XG4gICAgICAgICAgICAkKGNsaWNrZWRMaW5rKS5yZW1vdmVBdHRyKCdkaXNhYmxlZCcpO1xuICAgICAgICAgIH0pLmZhaWwoKGVycm9ycykgPT4ge1xuICAgICAgICAgICAgd2luZG93LnNob3dFcnJvck1lc3NhZ2UoZXJyb3JzLnJlc3BvbnNlSlNPTik7XG4gICAgICAgICAgICAkKGNsaWNrZWRMaW5rKS5yZW1vdmVBdHRyKCdkaXNhYmxlZCcpO1xuICAgICAgICAgIH0pO1xuICAgICAgICB9LFxuICAgICAgfSxcbiAgICApLnNob3coKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBTdG9yZSAnYWRkIHNwZWNpZmljIHByaWNlJyBmb3JtIHZhbHVlc1xuICAgKiBmb3IgZnV0dXJlIHVzYWdlXG4gICAqXG4gICAqIEBwcml2YXRlXG4gICAqL1xuICBzdG9yZVByaWNlRm9ybURlZmF1bHRWYWx1ZXMoKSB7XG4gICAgY29uc3Qgc3RvcmFnZSA9IHRoaXMuJGNyZWF0ZVByaWNlRm9ybURlZmF1bHRWYWx1ZXM7XG5cbiAgICAkKCcjc3BlY2lmaWNfcHJpY2VfZm9ybScpLmZpbmQoJ3NlbGVjdCxpbnB1dCcpLmVhY2goKGluZGV4LCB2YWx1ZSkgPT4ge1xuICAgICAgc3RvcmFnZVskKHZhbHVlKS5hdHRyKCdpZCcpXSA9ICQodmFsdWUpLnZhbCgpO1xuICAgIH0pO1xuXG4gICAgJCgnI3NwZWNpZmljX3ByaWNlX2Zvcm0nKS5maW5kKCdpbnB1dDpjaGVja2JveCcpLmVhY2goKGluZGV4LCB2YWx1ZSkgPT4ge1xuICAgICAgc3RvcmFnZVskKHZhbHVlKS5hdHRyKCdpZCcpXSA9ICQodmFsdWUpLnByb3AoJ2NoZWNrZWQnKTtcbiAgICB9KTtcblxuICAgIHRoaXMuJGNyZWF0ZVByaWNlRm9ybURlZmF1bHRWYWx1ZXMgPSBzdG9yYWdlO1xuICB9XG5cbiAgLyoqXG4gICAqIEBwYXJhbSBib29sZWFuIHVzZVByZWZpeEZvckNyZWF0ZVxuICAgKlxuICAgKiBAcHJpdmF0ZVxuICAgKi9cbiAgbG9hZEFuZEZpbGxPcHRpb25zRm9yU2VsZWN0Q29tYmluYXRpb25JbnB1dCh1c2VQcmVmaXhGb3JDcmVhdGUpIHtcbiAgICBjb25zdCBzZWxlY3RvclByZWZpeCA9IHRoaXMuZ2V0UHJlZml4U2VsZWN0b3IodXNlUHJlZml4Rm9yQ3JlYXRlKTtcblxuICAgIGNvbnN0IGlucHV0RmllbGQgPSAkKGAke3NlbGVjdG9yUHJlZml4fXNwX2lkX3Byb2R1Y3RfYXR0cmlidXRlYCk7XG4gICAgY29uc3QgdXJsID0gaW5wdXRGaWVsZFxuICAgICAgLmF0dHIoJ2RhdGEtYWN0aW9uJylcbiAgICAgIC5yZXBsYWNlKC9wcm9kdWN0LWNvbWJpbmF0aW9uc1xcL1xcZCsvLCBgcHJvZHVjdC1jb21iaW5hdGlvbnMvJHt0aGlzLmdldFByb2R1Y3RJZCgpfWApO1xuXG4gICAgJC5hamF4KHtcbiAgICAgIHR5cGU6ICdHRVQnLFxuICAgICAgdXJsLFxuICAgIH0pLmRvbmUoKGNvbWJpbmF0aW9ucykgPT4ge1xuICAgICAgLyoqIHJlbW92ZSBhbGwgb3B0aW9ucyBleGNlcHQgZmlyc3Qgb25lICovXG4gICAgICBpbnB1dEZpZWxkLmZpbmQoJ29wdGlvbjpndCgwKScpLnJlbW92ZSgpO1xuXG4gICAgICAkLmVhY2goY29tYmluYXRpb25zLCAoaW5kZXgsIGNvbWJpbmF0aW9uKSA9PiB7XG4gICAgICAgIGlucHV0RmllbGQuYXBwZW5kKGA8b3B0aW9uIHZhbHVlPVwiJHtjb21iaW5hdGlvbi5pZH1cIj4ke2NvbWJpbmF0aW9uLm5hbWV9PC9vcHRpb24+YCk7XG4gICAgICB9KTtcblxuICAgICAgaWYgKGlucHV0RmllbGQuZGF0YSgnc2VsZWN0ZWRBdHRyaWJ1dGUnKSAhPT0gJzAnKSB7XG4gICAgICAgIGlucHV0RmllbGQudmFsKGlucHV0RmllbGQuZGF0YSgnc2VsZWN0ZWRBdHRyaWJ1dGUnKSkudHJpZ2dlcignY2hhbmdlJyk7XG4gICAgICB9XG4gICAgfSk7XG4gIH1cblxuICAvKipcbiAgICogQHBhcmFtIGJvb2xlYW4gdXNlUHJlZml4Rm9yQ3JlYXRlXG4gICAqXG4gICAqIEBwcml2YXRlXG4gICAqL1xuICBlbmFibGVTcGVjaWZpY1ByaWNlVGF4RmllbGRJZkVsaWdpYmxlKHVzZVByZWZpeEZvckNyZWF0ZSkge1xuICAgIGNvbnN0IHNlbGVjdG9yUHJlZml4ID0gdGhpcy5nZXRQcmVmaXhTZWxlY3Rvcih1c2VQcmVmaXhGb3JDcmVhdGUpO1xuXG4gICAgaWYgKCQoYCR7c2VsZWN0b3JQcmVmaXh9c3BfcmVkdWN0aW9uX3R5cGVgKS52YWwoKSA9PT0gJ3BlcmNlbnRhZ2UnKSB7XG4gICAgICAkKGAke3NlbGVjdG9yUHJlZml4fXNwX3JlZHVjdGlvbl90YXhgKS5oaWRlKCk7XG4gICAgfSBlbHNlIHtcbiAgICAgICQoYCR7c2VsZWN0b3JQcmVmaXh9c3BfcmVkdWN0aW9uX3RheGApLnNob3coKTtcbiAgICB9XG4gIH1cblxuICAvKipcbiAgICogUmVzZXQgJ2FkZCBzcGVjaWZpYyBwcmljZScgZm9ybSB2YWx1ZXNcbiAgICogdXNpbmcgcHJldmlvdXNseSBzdG9yZWQgZGVmYXVsdCB2YWx1ZXNcbiAgICpcbiAgICogQHByaXZhdGVcbiAgICovXG4gIHJlc2V0Q3JlYXRlUHJpY2VGb3JtRGVmYXVsdFZhbHVlcygpIHtcbiAgICBjb25zdCBwcmV2aW91c2x5U3RvcmVkVmFsdWVzID0gdGhpcy4kY3JlYXRlUHJpY2VGb3JtRGVmYXVsdFZhbHVlcztcblxuICAgICQoJyNzcGVjaWZpY19wcmljZV9mb3JtJykuZmluZCgnaW5wdXQnKS5lYWNoKChpbmRleCwgdmFsdWUpID0+IHtcbiAgICAgICQodmFsdWUpLnZhbChwcmV2aW91c2x5U3RvcmVkVmFsdWVzWyQodmFsdWUpLmF0dHIoJ2lkJyldKTtcbiAgICB9KTtcblxuICAgICQoJyNzcGVjaWZpY19wcmljZV9mb3JtJykuZmluZCgnc2VsZWN0JykuZWFjaCgoaW5kZXgsIHZhbHVlKSA9PiB7XG4gICAgICAkKHZhbHVlKS52YWwocHJldmlvdXNseVN0b3JlZFZhbHVlc1skKHZhbHVlKS5hdHRyKCdpZCcpXSkuY2hhbmdlKCk7XG4gICAgfSk7XG5cbiAgICAkKCcjc3BlY2lmaWNfcHJpY2VfZm9ybScpLmZpbmQoJ2lucHV0OmNoZWNrYm94JykuZWFjaCgoaW5kZXgsIHZhbHVlKSA9PiB7XG4gICAgICAkKHZhbHVlKS5wcm9wKCdjaGVja2VkJywgdHJ1ZSk7XG4gICAgfSk7XG4gIH1cblxuICAvKipcbiAgICogQHBhcmFtIGJvb2xlYW4gdXNlUHJlZml4Rm9yQ3JlYXRlXG4gICAqXG4gICAqIEBwcml2YXRlXG4gICAqL1xuICBlbmFibGVTcGVjaWZpY1ByaWNlRmllbGRJZkVsaWdpYmxlKHVzZVByZWZpeEZvckNyZWF0ZSkge1xuICAgIGNvbnN0IHNlbGVjdG9yUHJlZml4ID0gdGhpcy5nZXRQcmVmaXhTZWxlY3Rvcih1c2VQcmVmaXhGb3JDcmVhdGUpO1xuXG4gICAgJChgJHtzZWxlY3RvclByZWZpeH1zcF9wcmljZWApLnByb3AoJ2Rpc2FibGVkJywgJChgJHtzZWxlY3RvclByZWZpeH1sZWF2ZV9icHJpY2VgKS5pcygnOmNoZWNrZWQnKSkudmFsKCcnKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBPcGVuICdlZGl0IHNwZWNpZmljIHByaWNlJyBmb3JtIGludG8gYSBtb2RhbFxuICAgKlxuICAgKiBAcGFyYW0gaW50ZWdlciBzcGVjaWZpY1ByaWNlSWRcbiAgICpcbiAgICogQHByaXZhdGVcbiAgICovXG4gIG9wZW5FZGl0UHJpY2VNb2RhbEFuZExvYWRGb3JtKHNwZWNpZmljUHJpY2VJZCkge1xuICAgIGNvbnN0IHVybCA9ICQoJyNqcy1zcGVjaWZpYy1wcmljZS1saXN0JykuZGF0YSgnYWN0aW9uRWRpdCcpLnJlcGxhY2UoL2Zvcm1cXC9cXGQrLywgYGZvcm0vJHtzcGVjaWZpY1ByaWNlSWR9YCk7XG5cbiAgICAkKCcjZWRpdC1zcGVjaWZpYy1wcmljZS1tb2RhbCcpLm1vZGFsKCdzaG93Jyk7XG4gICAgdGhpcy5lZGl0TW9kYWxJc09wZW4gPSB0cnVlO1xuXG4gICAgJC5hamF4KHtcbiAgICAgIHR5cGU6ICdHRVQnLFxuICAgICAgdXJsLFxuICAgIH0pXG4gICAgICAuZG9uZSgocmVzcG9uc2UpID0+IHtcbiAgICAgICAgdGhpcy5pbnNlcnRFZGl0U3BlY2lmaWNQcmljZUZvcm1JbnRvTW9kYWwocmVzcG9uc2UpO1xuICAgICAgICAkKCcjZWRpdC1zcGVjaWZpYy1wcmljZS1tb2RhbC1mb3JtJykuZGF0YSgnc3BlY2lmaWNQcmljZUlkJywgc3BlY2lmaWNQcmljZUlkKTtcbiAgICAgICAgdGhpcy5jb25maWd1cmVFZGl0UHJpY2VGb3JtSW5zaWRlTW9kYWxCZWhhdmlvcigpO1xuICAgICAgfSlcbiAgICAgIC5mYWlsKChlcnJvcnMpID0+IHtcbiAgICAgICAgd2luZG93LnNob3dFcnJvck1lc3NhZ2UoZXJyb3JzLnJlc3BvbnNlSlNPTik7XG4gICAgICB9KTtcbiAgfVxuXG4gIC8qKlxuICAgKiBAcHJpdmF0ZVxuICAgKi9cbiAgY2xvc2VFZGl0UHJpY2VNb2RhbEFuZFJlbW92ZUZvcm0oKSB7XG4gICAgJCgnI2VkaXQtc3BlY2lmaWMtcHJpY2UtbW9kYWwnKS5tb2RhbCgnaGlkZScpO1xuICAgIHRoaXMuZWRpdE1vZGFsSXNPcGVuID0gZmFsc2U7XG5cbiAgICBjb25zdCBmb3JtTG9jYXRpb25Ib2xkZXIgPSAkKCcjZWRpdC1zcGVjaWZpYy1wcmljZS1tb2RhbC1mb3JtJyk7XG5cbiAgICBmb3JtTG9jYXRpb25Ib2xkZXIuZW1wdHkoKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBAcGFyYW0gc3RyaW5nIGZvcm06IEhUTUwgJ2VkaXQgc3BlY2lmaWMgcHJpY2UnIGZvcm1cbiAgICpcbiAgICogQHByaXZhdGVcbiAgICovXG4gIGluc2VydEVkaXRTcGVjaWZpY1ByaWNlRm9ybUludG9Nb2RhbChmb3JtKSB7XG4gICAgY29uc3QgZm9ybUxvY2F0aW9uSG9sZGVyID0gJCgnI2VkaXQtc3BlY2lmaWMtcHJpY2UtbW9kYWwtZm9ybScpO1xuXG4gICAgZm9ybUxvY2F0aW9uSG9sZGVyLmVtcHR5KCk7XG4gICAgZm9ybUxvY2F0aW9uSG9sZGVyLmFwcGVuZChmb3JtKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBHZXQgcHJvZHVjdCBJRCBmb3IgY3VycmVudCBDYXRhbG9nIFByb2R1Y3QgcGFnZVxuICAgKlxuICAgKiBAcmV0dXJucyBpbnRlZ2VyXG4gICAqXG4gICAqIEBwcml2YXRlXG4gICAqL1xuICBnZXRQcm9kdWN0SWQoKSB7XG4gICAgcmV0dXJuICQoJyNmb3JtX2lkX3Byb2R1Y3QnKS52YWwoKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBAcGFyYW0gYm9vbGVhbiB1c2VQcmVmaXhGb3JDcmVhdGVcbiAgICpcbiAgICogQHJldHVybnMgc3RyaW5nXG4gICAqXG4gICAqIEBwcml2YXRlXG4gICAqL1xuICBnZXRQcmVmaXhTZWxlY3Rvcih1c2VQcmVmaXhGb3JDcmVhdGUpIHtcbiAgICBpZiAodXNlUHJlZml4Rm9yQ3JlYXRlKSB7XG4gICAgICByZXR1cm4gYCMke3RoaXMucHJlZml4Q3JlYXRlRm9ybX1gO1xuICAgIH1cblxuICAgIHJldHVybiBgIyR7dGhpcy5wcmVmaXhFZGl0Rm9ybX1gO1xuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IFNwZWNpZmljUHJpY2VGb3JtSGFuZGxlcjtcbiIsIm1vZHVsZS5leHBvcnRzID0geyBcImRlZmF1bHRcIjogcmVxdWlyZShcImNvcmUtanMvbGlicmFyeS9mbi9vYmplY3QvZGVmaW5lLXByb3BlcnR5XCIpLCBfX2VzTW9kdWxlOiB0cnVlIH07IiwiXCJ1c2Ugc3RyaWN0XCI7XG5cbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IGZ1bmN0aW9uIChpbnN0YW5jZSwgQ29uc3RydWN0b3IpIHtcbiAgaWYgKCEoaW5zdGFuY2UgaW5zdGFuY2VvZiBDb25zdHJ1Y3RvcikpIHtcbiAgICB0aHJvdyBuZXcgVHlwZUVycm9yKFwiQ2Fubm90IGNhbGwgYSBjbGFzcyBhcyBhIGZ1bmN0aW9uXCIpO1xuICB9XG59OyIsIlwidXNlIHN0cmljdFwiO1xuXG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xuXG52YXIgX2RlZmluZVByb3BlcnR5ID0gcmVxdWlyZShcIi4uL2NvcmUtanMvb2JqZWN0L2RlZmluZS1wcm9wZXJ0eVwiKTtcblxudmFyIF9kZWZpbmVQcm9wZXJ0eTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9kZWZpbmVQcm9wZXJ0eSk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbmV4cG9ydHMuZGVmYXVsdCA9IGZ1bmN0aW9uICgpIHtcbiAgZnVuY3Rpb24gZGVmaW5lUHJvcGVydGllcyh0YXJnZXQsIHByb3BzKSB7XG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBwcm9wcy5sZW5ndGg7IGkrKykge1xuICAgICAgdmFyIGRlc2NyaXB0b3IgPSBwcm9wc1tpXTtcbiAgICAgIGRlc2NyaXB0b3IuZW51bWVyYWJsZSA9IGRlc2NyaXB0b3IuZW51bWVyYWJsZSB8fCBmYWxzZTtcbiAgICAgIGRlc2NyaXB0b3IuY29uZmlndXJhYmxlID0gdHJ1ZTtcbiAgICAgIGlmIChcInZhbHVlXCIgaW4gZGVzY3JpcHRvcikgZGVzY3JpcHRvci53cml0YWJsZSA9IHRydWU7XG4gICAgICAoMCwgX2RlZmluZVByb3BlcnR5Mi5kZWZhdWx0KSh0YXJnZXQsIGRlc2NyaXB0b3Iua2V5LCBkZXNjcmlwdG9yKTtcbiAgICB9XG4gIH1cblxuICByZXR1cm4gZnVuY3Rpb24gKENvbnN0cnVjdG9yLCBwcm90b1Byb3BzLCBzdGF0aWNQcm9wcykge1xuICAgIGlmIChwcm90b1Byb3BzKSBkZWZpbmVQcm9wZXJ0aWVzKENvbnN0cnVjdG9yLnByb3RvdHlwZSwgcHJvdG9Qcm9wcyk7XG4gICAgaWYgKHN0YXRpY1Byb3BzKSBkZWZpbmVQcm9wZXJ0aWVzKENvbnN0cnVjdG9yLCBzdGF0aWNQcm9wcyk7XG4gICAgcmV0dXJuIENvbnN0cnVjdG9yO1xuICB9O1xufSgpOyIsInJlcXVpcmUoJy4uLy4uL21vZHVsZXMvZXM2Lm9iamVjdC5kZWZpbmUtcHJvcGVydHknKTtcbnZhciAkT2JqZWN0ID0gcmVxdWlyZSgnLi4vLi4vbW9kdWxlcy9fY29yZScpLk9iamVjdDtcbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gZGVmaW5lUHJvcGVydHkoaXQsIGtleSwgZGVzYykge1xuICByZXR1cm4gJE9iamVjdC5kZWZpbmVQcm9wZXJ0eShpdCwga2V5LCBkZXNjKTtcbn07XG4iLCJtb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIChpdCkge1xuICBpZiAodHlwZW9mIGl0ICE9ICdmdW5jdGlvbicpIHRocm93IFR5cGVFcnJvcihpdCArICcgaXMgbm90IGEgZnVuY3Rpb24hJyk7XG4gIHJldHVybiBpdDtcbn07XG4iLCJ2YXIgaXNPYmplY3QgPSByZXF1aXJlKCcuL19pcy1vYmplY3QnKTtcbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKGl0KSB7XG4gIGlmICghaXNPYmplY3QoaXQpKSB0aHJvdyBUeXBlRXJyb3IoaXQgKyAnIGlzIG5vdCBhbiBvYmplY3QhJyk7XG4gIHJldHVybiBpdDtcbn07XG4iLCJ2YXIgY29yZSA9IG1vZHVsZS5leHBvcnRzID0geyB2ZXJzaW9uOiAnMi42LjExJyB9O1xuaWYgKHR5cGVvZiBfX2UgPT0gJ251bWJlcicpIF9fZSA9IGNvcmU7IC8vIGVzbGludC1kaXNhYmxlLWxpbmUgbm8tdW5kZWZcbiIsIi8vIG9wdGlvbmFsIC8gc2ltcGxlIGNvbnRleHQgYmluZGluZ1xudmFyIGFGdW5jdGlvbiA9IHJlcXVpcmUoJy4vX2EtZnVuY3Rpb24nKTtcbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKGZuLCB0aGF0LCBsZW5ndGgpIHtcbiAgYUZ1bmN0aW9uKGZuKTtcbiAgaWYgKHRoYXQgPT09IHVuZGVmaW5lZCkgcmV0dXJuIGZuO1xuICBzd2l0Y2ggKGxlbmd0aCkge1xuICAgIGNhc2UgMTogcmV0dXJuIGZ1bmN0aW9uIChhKSB7XG4gICAgICByZXR1cm4gZm4uY2FsbCh0aGF0LCBhKTtcbiAgICB9O1xuICAgIGNhc2UgMjogcmV0dXJuIGZ1bmN0aW9uIChhLCBiKSB7XG4gICAgICByZXR1cm4gZm4uY2FsbCh0aGF0LCBhLCBiKTtcbiAgICB9O1xuICAgIGNhc2UgMzogcmV0dXJuIGZ1bmN0aW9uIChhLCBiLCBjKSB7XG4gICAgICByZXR1cm4gZm4uY2FsbCh0aGF0LCBhLCBiLCBjKTtcbiAgICB9O1xuICB9XG4gIHJldHVybiBmdW5jdGlvbiAoLyogLi4uYXJncyAqLykge1xuICAgIHJldHVybiBmbi5hcHBseSh0aGF0LCBhcmd1bWVudHMpO1xuICB9O1xufTtcbiIsIi8vIFRoYW5rJ3MgSUU4IGZvciBoaXMgZnVubnkgZGVmaW5lUHJvcGVydHlcbm1vZHVsZS5leHBvcnRzID0gIXJlcXVpcmUoJy4vX2ZhaWxzJykoZnVuY3Rpb24gKCkge1xuICByZXR1cm4gT2JqZWN0LmRlZmluZVByb3BlcnR5KHt9LCAnYScsIHsgZ2V0OiBmdW5jdGlvbiAoKSB7IHJldHVybiA3OyB9IH0pLmEgIT0gNztcbn0pO1xuIiwidmFyIGlzT2JqZWN0ID0gcmVxdWlyZSgnLi9faXMtb2JqZWN0Jyk7XG52YXIgZG9jdW1lbnQgPSByZXF1aXJlKCcuL19nbG9iYWwnKS5kb2N1bWVudDtcbi8vIHR5cGVvZiBkb2N1bWVudC5jcmVhdGVFbGVtZW50IGlzICdvYmplY3QnIGluIG9sZCBJRVxudmFyIGlzID0gaXNPYmplY3QoZG9jdW1lbnQpICYmIGlzT2JqZWN0KGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQpO1xubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAoaXQpIHtcbiAgcmV0dXJuIGlzID8gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChpdCkgOiB7fTtcbn07XG4iLCJ2YXIgZ2xvYmFsID0gcmVxdWlyZSgnLi9fZ2xvYmFsJyk7XG52YXIgY29yZSA9IHJlcXVpcmUoJy4vX2NvcmUnKTtcbnZhciBjdHggPSByZXF1aXJlKCcuL19jdHgnKTtcbnZhciBoaWRlID0gcmVxdWlyZSgnLi9faGlkZScpO1xudmFyIGhhcyA9IHJlcXVpcmUoJy4vX2hhcycpO1xudmFyIFBST1RPVFlQRSA9ICdwcm90b3R5cGUnO1xuXG52YXIgJGV4cG9ydCA9IGZ1bmN0aW9uICh0eXBlLCBuYW1lLCBzb3VyY2UpIHtcbiAgdmFyIElTX0ZPUkNFRCA9IHR5cGUgJiAkZXhwb3J0LkY7XG4gIHZhciBJU19HTE9CQUwgPSB0eXBlICYgJGV4cG9ydC5HO1xuICB2YXIgSVNfU1RBVElDID0gdHlwZSAmICRleHBvcnQuUztcbiAgdmFyIElTX1BST1RPID0gdHlwZSAmICRleHBvcnQuUDtcbiAgdmFyIElTX0JJTkQgPSB0eXBlICYgJGV4cG9ydC5CO1xuICB2YXIgSVNfV1JBUCA9IHR5cGUgJiAkZXhwb3J0Llc7XG4gIHZhciBleHBvcnRzID0gSVNfR0xPQkFMID8gY29yZSA6IGNvcmVbbmFtZV0gfHwgKGNvcmVbbmFtZV0gPSB7fSk7XG4gIHZhciBleHBQcm90byA9IGV4cG9ydHNbUFJPVE9UWVBFXTtcbiAgdmFyIHRhcmdldCA9IElTX0dMT0JBTCA/IGdsb2JhbCA6IElTX1NUQVRJQyA/IGdsb2JhbFtuYW1lXSA6IChnbG9iYWxbbmFtZV0gfHwge30pW1BST1RPVFlQRV07XG4gIHZhciBrZXksIG93biwgb3V0O1xuICBpZiAoSVNfR0xPQkFMKSBzb3VyY2UgPSBuYW1lO1xuICBmb3IgKGtleSBpbiBzb3VyY2UpIHtcbiAgICAvLyBjb250YWlucyBpbiBuYXRpdmVcbiAgICBvd24gPSAhSVNfRk9SQ0VEICYmIHRhcmdldCAmJiB0YXJnZXRba2V5XSAhPT0gdW5kZWZpbmVkO1xuICAgIGlmIChvd24gJiYgaGFzKGV4cG9ydHMsIGtleSkpIGNvbnRpbnVlO1xuICAgIC8vIGV4cG9ydCBuYXRpdmUgb3IgcGFzc2VkXG4gICAgb3V0ID0gb3duID8gdGFyZ2V0W2tleV0gOiBzb3VyY2Vba2V5XTtcbiAgICAvLyBwcmV2ZW50IGdsb2JhbCBwb2xsdXRpb24gZm9yIG5hbWVzcGFjZXNcbiAgICBleHBvcnRzW2tleV0gPSBJU19HTE9CQUwgJiYgdHlwZW9mIHRhcmdldFtrZXldICE9ICdmdW5jdGlvbicgPyBzb3VyY2Vba2V5XVxuICAgIC8vIGJpbmQgdGltZXJzIHRvIGdsb2JhbCBmb3IgY2FsbCBmcm9tIGV4cG9ydCBjb250ZXh0XG4gICAgOiBJU19CSU5EICYmIG93biA/IGN0eChvdXQsIGdsb2JhbClcbiAgICAvLyB3cmFwIGdsb2JhbCBjb25zdHJ1Y3RvcnMgZm9yIHByZXZlbnQgY2hhbmdlIHRoZW0gaW4gbGlicmFyeVxuICAgIDogSVNfV1JBUCAmJiB0YXJnZXRba2V5XSA9PSBvdXQgPyAoZnVuY3Rpb24gKEMpIHtcbiAgICAgIHZhciBGID0gZnVuY3Rpb24gKGEsIGIsIGMpIHtcbiAgICAgICAgaWYgKHRoaXMgaW5zdGFuY2VvZiBDKSB7XG4gICAgICAgICAgc3dpdGNoIChhcmd1bWVudHMubGVuZ3RoKSB7XG4gICAgICAgICAgICBjYXNlIDA6IHJldHVybiBuZXcgQygpO1xuICAgICAgICAgICAgY2FzZSAxOiByZXR1cm4gbmV3IEMoYSk7XG4gICAgICAgICAgICBjYXNlIDI6IHJldHVybiBuZXcgQyhhLCBiKTtcbiAgICAgICAgICB9IHJldHVybiBuZXcgQyhhLCBiLCBjKTtcbiAgICAgICAgfSByZXR1cm4gQy5hcHBseSh0aGlzLCBhcmd1bWVudHMpO1xuICAgICAgfTtcbiAgICAgIEZbUFJPVE9UWVBFXSA9IENbUFJPVE9UWVBFXTtcbiAgICAgIHJldHVybiBGO1xuICAgIC8vIG1ha2Ugc3RhdGljIHZlcnNpb25zIGZvciBwcm90b3R5cGUgbWV0aG9kc1xuICAgIH0pKG91dCkgOiBJU19QUk9UTyAmJiB0eXBlb2Ygb3V0ID09ICdmdW5jdGlvbicgPyBjdHgoRnVuY3Rpb24uY2FsbCwgb3V0KSA6IG91dDtcbiAgICAvLyBleHBvcnQgcHJvdG8gbWV0aG9kcyB0byBjb3JlLiVDT05TVFJVQ1RPUiUubWV0aG9kcy4lTkFNRSVcbiAgICBpZiAoSVNfUFJPVE8pIHtcbiAgICAgIChleHBvcnRzLnZpcnR1YWwgfHwgKGV4cG9ydHMudmlydHVhbCA9IHt9KSlba2V5XSA9IG91dDtcbiAgICAgIC8vIGV4cG9ydCBwcm90byBtZXRob2RzIHRvIGNvcmUuJUNPTlNUUlVDVE9SJS5wcm90b3R5cGUuJU5BTUUlXG4gICAgICBpZiAodHlwZSAmICRleHBvcnQuUiAmJiBleHBQcm90byAmJiAhZXhwUHJvdG9ba2V5XSkgaGlkZShleHBQcm90bywga2V5LCBvdXQpO1xuICAgIH1cbiAgfVxufTtcbi8vIHR5cGUgYml0bWFwXG4kZXhwb3J0LkYgPSAxOyAgIC8vIGZvcmNlZFxuJGV4cG9ydC5HID0gMjsgICAvLyBnbG9iYWxcbiRleHBvcnQuUyA9IDQ7ICAgLy8gc3RhdGljXG4kZXhwb3J0LlAgPSA4OyAgIC8vIHByb3RvXG4kZXhwb3J0LkIgPSAxNjsgIC8vIGJpbmRcbiRleHBvcnQuVyA9IDMyOyAgLy8gd3JhcFxuJGV4cG9ydC5VID0gNjQ7ICAvLyBzYWZlXG4kZXhwb3J0LlIgPSAxMjg7IC8vIHJlYWwgcHJvdG8gbWV0aG9kIGZvciBgbGlicmFyeWBcbm1vZHVsZS5leHBvcnRzID0gJGV4cG9ydDtcbiIsIm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKGV4ZWMpIHtcbiAgdHJ5IHtcbiAgICByZXR1cm4gISFleGVjKCk7XG4gIH0gY2F0Y2ggKGUpIHtcbiAgICByZXR1cm4gdHJ1ZTtcbiAgfVxufTtcbiIsIi8vIGh0dHBzOi8vZ2l0aHViLmNvbS96bG9pcm9jay9jb3JlLWpzL2lzc3Vlcy84NiNpc3N1ZWNvbW1lbnQtMTE1NzU5MDI4XG52YXIgZ2xvYmFsID0gbW9kdWxlLmV4cG9ydHMgPSB0eXBlb2Ygd2luZG93ICE9ICd1bmRlZmluZWQnICYmIHdpbmRvdy5NYXRoID09IE1hdGhcbiAgPyB3aW5kb3cgOiB0eXBlb2Ygc2VsZiAhPSAndW5kZWZpbmVkJyAmJiBzZWxmLk1hdGggPT0gTWF0aCA/IHNlbGZcbiAgLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lIG5vLW5ldy1mdW5jXG4gIDogRnVuY3Rpb24oJ3JldHVybiB0aGlzJykoKTtcbmlmICh0eXBlb2YgX19nID09ICdudW1iZXInKSBfX2cgPSBnbG9iYWw7IC8vIGVzbGludC1kaXNhYmxlLWxpbmUgbm8tdW5kZWZcbiIsInZhciBoYXNPd25Qcm9wZXJ0eSA9IHt9Lmhhc093blByb3BlcnR5O1xubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAoaXQsIGtleSkge1xuICByZXR1cm4gaGFzT3duUHJvcGVydHkuY2FsbChpdCwga2V5KTtcbn07XG4iLCJ2YXIgZFAgPSByZXF1aXJlKCcuL19vYmplY3QtZHAnKTtcbnZhciBjcmVhdGVEZXNjID0gcmVxdWlyZSgnLi9fcHJvcGVydHktZGVzYycpO1xubW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKCcuL19kZXNjcmlwdG9ycycpID8gZnVuY3Rpb24gKG9iamVjdCwga2V5LCB2YWx1ZSkge1xuICByZXR1cm4gZFAuZihvYmplY3QsIGtleSwgY3JlYXRlRGVzYygxLCB2YWx1ZSkpO1xufSA6IGZ1bmN0aW9uIChvYmplY3QsIGtleSwgdmFsdWUpIHtcbiAgb2JqZWN0W2tleV0gPSB2YWx1ZTtcbiAgcmV0dXJuIG9iamVjdDtcbn07XG4iLCJtb2R1bGUuZXhwb3J0cyA9ICFyZXF1aXJlKCcuL19kZXNjcmlwdG9ycycpICYmICFyZXF1aXJlKCcuL19mYWlscycpKGZ1bmN0aW9uICgpIHtcbiAgcmV0dXJuIE9iamVjdC5kZWZpbmVQcm9wZXJ0eShyZXF1aXJlKCcuL19kb20tY3JlYXRlJykoJ2RpdicpLCAnYScsIHsgZ2V0OiBmdW5jdGlvbiAoKSB7IHJldHVybiA3OyB9IH0pLmEgIT0gNztcbn0pO1xuIiwibW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAoaXQpIHtcbiAgcmV0dXJuIHR5cGVvZiBpdCA9PT0gJ29iamVjdCcgPyBpdCAhPT0gbnVsbCA6IHR5cGVvZiBpdCA9PT0gJ2Z1bmN0aW9uJztcbn07XG4iLCJ2YXIgYW5PYmplY3QgPSByZXF1aXJlKCcuL19hbi1vYmplY3QnKTtcbnZhciBJRThfRE9NX0RFRklORSA9IHJlcXVpcmUoJy4vX2llOC1kb20tZGVmaW5lJyk7XG52YXIgdG9QcmltaXRpdmUgPSByZXF1aXJlKCcuL190by1wcmltaXRpdmUnKTtcbnZhciBkUCA9IE9iamVjdC5kZWZpbmVQcm9wZXJ0eTtcblxuZXhwb3J0cy5mID0gcmVxdWlyZSgnLi9fZGVzY3JpcHRvcnMnKSA/IE9iamVjdC5kZWZpbmVQcm9wZXJ0eSA6IGZ1bmN0aW9uIGRlZmluZVByb3BlcnR5KE8sIFAsIEF0dHJpYnV0ZXMpIHtcbiAgYW5PYmplY3QoTyk7XG4gIFAgPSB0b1ByaW1pdGl2ZShQLCB0cnVlKTtcbiAgYW5PYmplY3QoQXR0cmlidXRlcyk7XG4gIGlmIChJRThfRE9NX0RFRklORSkgdHJ5IHtcbiAgICByZXR1cm4gZFAoTywgUCwgQXR0cmlidXRlcyk7XG4gIH0gY2F0Y2ggKGUpIHsgLyogZW1wdHkgKi8gfVxuICBpZiAoJ2dldCcgaW4gQXR0cmlidXRlcyB8fCAnc2V0JyBpbiBBdHRyaWJ1dGVzKSB0aHJvdyBUeXBlRXJyb3IoJ0FjY2Vzc29ycyBub3Qgc3VwcG9ydGVkIScpO1xuICBpZiAoJ3ZhbHVlJyBpbiBBdHRyaWJ1dGVzKSBPW1BdID0gQXR0cmlidXRlcy52YWx1ZTtcbiAgcmV0dXJuIE87XG59O1xuIiwibW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAoYml0bWFwLCB2YWx1ZSkge1xuICByZXR1cm4ge1xuICAgIGVudW1lcmFibGU6ICEoYml0bWFwICYgMSksXG4gICAgY29uZmlndXJhYmxlOiAhKGJpdG1hcCAmIDIpLFxuICAgIHdyaXRhYmxlOiAhKGJpdG1hcCAmIDQpLFxuICAgIHZhbHVlOiB2YWx1ZVxuICB9O1xufTtcbiIsIi8vIDcuMS4xIFRvUHJpbWl0aXZlKGlucHV0IFssIFByZWZlcnJlZFR5cGVdKVxudmFyIGlzT2JqZWN0ID0gcmVxdWlyZSgnLi9faXMtb2JqZWN0Jyk7XG4vLyBpbnN0ZWFkIG9mIHRoZSBFUzYgc3BlYyB2ZXJzaW9uLCB3ZSBkaWRuJ3QgaW1wbGVtZW50IEBAdG9QcmltaXRpdmUgY2FzZVxuLy8gYW5kIHRoZSBzZWNvbmQgYXJndW1lbnQgLSBmbGFnIC0gcHJlZmVycmVkIHR5cGUgaXMgYSBzdHJpbmdcbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKGl0LCBTKSB7XG4gIGlmICghaXNPYmplY3QoaXQpKSByZXR1cm4gaXQ7XG4gIHZhciBmbiwgdmFsO1xuICBpZiAoUyAmJiB0eXBlb2YgKGZuID0gaXQudG9TdHJpbmcpID09ICdmdW5jdGlvbicgJiYgIWlzT2JqZWN0KHZhbCA9IGZuLmNhbGwoaXQpKSkgcmV0dXJuIHZhbDtcbiAgaWYgKHR5cGVvZiAoZm4gPSBpdC52YWx1ZU9mKSA9PSAnZnVuY3Rpb24nICYmICFpc09iamVjdCh2YWwgPSBmbi5jYWxsKGl0KSkpIHJldHVybiB2YWw7XG4gIGlmICghUyAmJiB0eXBlb2YgKGZuID0gaXQudG9TdHJpbmcpID09ICdmdW5jdGlvbicgJiYgIWlzT2JqZWN0KHZhbCA9IGZuLmNhbGwoaXQpKSkgcmV0dXJuIHZhbDtcbiAgdGhyb3cgVHlwZUVycm9yKFwiQ2FuJ3QgY29udmVydCBvYmplY3QgdG8gcHJpbWl0aXZlIHZhbHVlXCIpO1xufTtcbiIsInZhciAkZXhwb3J0ID0gcmVxdWlyZSgnLi9fZXhwb3J0Jyk7XG4vLyAxOS4xLjIuNCAvIDE1LjIuMy42IE9iamVjdC5kZWZpbmVQcm9wZXJ0eShPLCBQLCBBdHRyaWJ1dGVzKVxuJGV4cG9ydCgkZXhwb3J0LlMgKyAkZXhwb3J0LkYgKiAhcmVxdWlyZSgnLi9fZGVzY3JpcHRvcnMnKSwgJ09iamVjdCcsIHsgZGVmaW5lUHJvcGVydHk6IHJlcXVpcmUoJy4vX29iamVjdC1kcCcpLmYgfSk7XG4iLCIvLyBUaGUgbW9kdWxlIGNhY2hlXG52YXIgX193ZWJwYWNrX21vZHVsZV9jYWNoZV9fID0ge307XG5cbi8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG5mdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuXHR2YXIgY2FjaGVkTW9kdWxlID0gX193ZWJwYWNrX21vZHVsZV9jYWNoZV9fW21vZHVsZUlkXTtcblx0aWYgKGNhY2hlZE1vZHVsZSAhPT0gdW5kZWZpbmVkKSB7XG5cdFx0cmV0dXJuIGNhY2hlZE1vZHVsZS5leHBvcnRzO1xuXHR9XG5cdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG5cdHZhciBtb2R1bGUgPSBfX3dlYnBhY2tfbW9kdWxlX2NhY2hlX19bbW9kdWxlSWRdID0ge1xuXHRcdC8vIG5vIG1vZHVsZS5pZCBuZWVkZWRcblx0XHQvLyBubyBtb2R1bGUubG9hZGVkIG5lZWRlZFxuXHRcdGV4cG9ydHM6IHt9XG5cdH07XG5cblx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG5cdF9fd2VicGFja19tb2R1bGVzX19bbW9kdWxlSWRdKG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG5cdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG5cdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbn1cblxuIiwiLyoqXG4gKiBDb3B5cmlnaHQgc2luY2UgMjAwNyBQcmVzdGFTaG9wIFNBIGFuZCBDb250cmlidXRvcnNcbiAqIFByZXN0YVNob3AgaXMgYW4gSW50ZXJuYXRpb25hbCBSZWdpc3RlcmVkIFRyYWRlbWFyayAmIFByb3BlcnR5IG9mIFByZXN0YVNob3AgU0FcbiAqXG4gKiBOT1RJQ0UgT0YgTElDRU5TRVxuICpcbiAqIFRoaXMgc291cmNlIGZpbGUgaXMgc3ViamVjdCB0byB0aGUgT3BlbiBTb2Z0d2FyZSBMaWNlbnNlIChPU0wgMy4wKVxuICogdGhhdCBpcyBidW5kbGVkIHdpdGggdGhpcyBwYWNrYWdlIGluIHRoZSBmaWxlIExJQ0VOU0UubWQuXG4gKiBJdCBpcyBhbHNvIGF2YWlsYWJsZSB0aHJvdWdoIHRoZSB3b3JsZC13aWRlLXdlYiBhdCB0aGlzIFVSTDpcbiAqIGh0dHBzOi8vb3BlbnNvdXJjZS5vcmcvbGljZW5zZXMvT1NMLTMuMFxuICogSWYgeW91IGRpZCBub3QgcmVjZWl2ZSBhIGNvcHkgb2YgdGhlIGxpY2Vuc2UgYW5kIGFyZSB1bmFibGUgdG9cbiAqIG9idGFpbiBpdCB0aHJvdWdoIHRoZSB3b3JsZC13aWRlLXdlYiwgcGxlYXNlIHNlbmQgYW4gZW1haWxcbiAqIHRvIGxpY2Vuc2VAcHJlc3Rhc2hvcC5jb20gc28gd2UgY2FuIHNlbmQgeW91IGEgY29weSBpbW1lZGlhdGVseS5cbiAqXG4gKiBESVNDTEFJTUVSXG4gKlxuICogRG8gbm90IGVkaXQgb3IgYWRkIHRvIHRoaXMgZmlsZSBpZiB5b3Ugd2lzaCB0byB1cGdyYWRlIFByZXN0YVNob3AgdG8gbmV3ZXJcbiAqIHZlcnNpb25zIGluIHRoZSBmdXR1cmUuIElmIHlvdSB3aXNoIHRvIGN1c3RvbWl6ZSBQcmVzdGFTaG9wIGZvciB5b3VyXG4gKiBuZWVkcyBwbGVhc2UgcmVmZXIgdG8gaHR0cHM6Ly9kZXZkb2NzLnByZXN0YXNob3AuY29tLyBmb3IgbW9yZSBpbmZvcm1hdGlvbi5cbiAqXG4gKiBAYXV0aG9yICAgIFByZXN0YVNob3AgU0EgYW5kIENvbnRyaWJ1dG9ycyA8Y29udGFjdEBwcmVzdGFzaG9wLmNvbT5cbiAqIEBjb3B5cmlnaHQgU2luY2UgMjAwNyBQcmVzdGFTaG9wIFNBIGFuZCBDb250cmlidXRvcnNcbiAqIEBsaWNlbnNlICAgaHR0cHM6Ly9vcGVuc291cmNlLm9yZy9saWNlbnNlcy9PU0wtMy4wIE9wZW4gU29mdHdhcmUgTGljZW5zZSAoT1NMIDMuMClcbiAqL1xuXG5pbXBvcnQgU3BlY2lmaWNQcmljZUZvcm1IYW5kbGVyIGZyb20gJy4vc3BlY2lmaWMtcHJpY2UtZm9ybS1oYW5kbGVyJztcblxuY29uc3QgeyR9ID0gd2luZG93O1xuXG4kKCgpID0+IHtcbiAgbmV3IFNwZWNpZmljUHJpY2VGb3JtSGFuZGxlcigpO1xufSk7XG4iXSwic291cmNlUm9vdCI6IiJ9