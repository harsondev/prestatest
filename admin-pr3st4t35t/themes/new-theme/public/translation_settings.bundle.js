/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./js/pages/translation-settings/ExportFormFieldToggle.js":
/*!****************************************************************!*\
  !*** ./js/pages/translation-settings/ExportFormFieldToggle.js ***!
  \****************************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

"use strict";


Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var _values = __webpack_require__(/*! babel-runtime/core-js/object/values */ "./node_modules/babel-runtime/core-js/object/values.js");

var _values2 = _interopRequireDefault(_values);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _TranslationSettingsMap = __webpack_require__(/*! ./TranslationSettingsMap */ "./js/pages/translation-settings/TranslationSettingsMap.js");

var _TranslationSettingsMap2 = _interopRequireDefault(_TranslationSettingsMap);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _window = window,
    $ = _window.$; /**
                    * Copyright since 2007 PrestaShop SA and Contributors
                    * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
                    *
                    * NOTICE OF LICENSE
                    *
                    * This source file is subject to the Open Software License (OSL 3.0)
                    * that is bundled with this package in the file LICENSE.md.
                    * It is also available through the world-wide-web at this URL:
                    * https://opensource.org/licenses/OSL-3.0
                    * If you did not receive a copy of the license and are unable to
                    * obtain it through the world-wide-web, please send an email
                    * to license@prestashop.com so we can send you a copy immediately.
                    *
                    * DISCLAIMER
                    *
                    * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
                    * versions in the future. If you wish to customize PrestaShop for your
                    * needs please refer to https://devdocs.prestashop.com/ for more information.
                    *
                    * @author    PrestaShop SA and Contributors <contact@prestashop.com>
                    * @copyright Since 2007 PrestaShop SA and Contributors
                    * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
                    */

var $coreType = $(_TranslationSettingsMap2.default.exportCoreType);
var $themesType = $(_TranslationSettingsMap2.default.exportThemesType);
var $modulesType = $(_TranslationSettingsMap2.default.exportModulesType);

var $coreValues = $(_TranslationSettingsMap2.default.exportCoreValues).closest('.form-group');
var $themesValues = $(_TranslationSettingsMap2.default.exportThemesValues).closest('.form-group');
var $modulesValues = $(_TranslationSettingsMap2.default.exportModulesValues).closest('.form-group');

var $coreCheckboxes = $(_TranslationSettingsMap2.default.exportCoreValues);
var $themesSelect = $(_TranslationSettingsMap2.default.exportThemesValues);
var $modulesSelect = $(_TranslationSettingsMap2.default.exportModulesValues);

var $exportButton = $(_TranslationSettingsMap2.default.exportLanguageButton);

/**
 * Toggles show/hide for the selectors of subtypes (in case of Core type), theme or module when a Type is selected
 *
 * Example : If Core type is selected, the subtypes checkboxes are shown,
 * Theme and Module types are unselected and their value selector are hidden
 */

var ExportFormFieldToggle = function () {
  function ExportFormFieldToggle() {
    (0, _classCallCheck3.default)(this, ExportFormFieldToggle);

    $coreType.on('change', this.coreTypeChanged.bind(this));
    $themesType.on('change', this.themesTypeChanged.bind(this));
    $modulesType.on('change', this.modulesTypeChanged.bind(this));

    $coreCheckboxes.on('change', this.subChoicesChanged.bind(this));
    $themesSelect.on('change', this.subChoicesChanged.bind(this));
    $modulesSelect.on('change', this.subChoicesChanged.bind(this));

    this.check($coreType);
  }

  (0, _createClass3.default)(ExportFormFieldToggle, [{
    key: 'coreTypeChanged',
    value: function coreTypeChanged() {
      if (!$coreType.is(':checked')) {
        return;
      }

      $coreType.prop('disabled', false);
      this.uncheck($themesType, $modulesType);
      this.show($coreValues);
      this.hide($themesValues, $modulesValues);
      this.subChoicesChanged();
    }
  }, {
    key: 'themesTypeChanged',
    value: function themesTypeChanged() {
      if (!$themesType.is(':checked')) {
        return;
      }

      $themesType.prop('disabled', false);
      this.uncheck($coreType, $modulesType);
      this.show($themesValues);
      this.hide($coreValues, $modulesValues);
      this.subChoicesChanged();
    }
  }, {
    key: 'modulesTypeChanged',
    value: function modulesTypeChanged() {
      if (!$modulesType.is(':checked')) {
        return;
      }

      $modulesValues.prop('disabled', false);
      this.uncheck($themesType, $coreType);
      this.show($modulesValues);
      this.hide($themesValues, $coreValues);
      this.subChoicesChanged();
    }
  }, {
    key: 'subChoicesChanged',
    value: function subChoicesChanged() {
      if ($coreType.prop('checked') && $coreCheckboxes.find(':checked').size() > 0 || $themesType.prop('checked') && $themesSelect.val() !== null || $modulesType.prop('checked') && $modulesSelect.val() !== null) {
        $exportButton.prop('disabled', false);

        return;
      }

      $exportButton.prop('disabled', true);
    }

    /**
     * Make all given selectors hidden
     *
     * @param $selectors
     * @private
     */

  }, {
    key: 'hide',
    value: function hide() {
      for (var _len = arguments.length, $selectors = Array(_len), _key = 0; _key < _len; _key++) {
        $selectors[_key] = arguments[_key];
      }

      (0, _values2.default)($selectors).forEach(function (el) {
        el.addClass('d-none');
        el.find('select, input').prop('disabled', 'disabled');
      });
    }

    /**
     * Make all given selectors visible
     *
     * @param $selectors
     * @private
     */

  }, {
    key: 'show',
    value: function show() {
      for (var _len2 = arguments.length, $selectors = Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
        $selectors[_key2] = arguments[_key2];
      }

      (0, _values2.default)($selectors).forEach(function (el) {
        el.removeClass('d-none');
        el.find('select, input').prop('disabled', false);
      });
    }

    /**
     * Make all given selectors unchecked
     *
     * @param $selectors
     * @private
     */

  }, {
    key: 'uncheck',
    value: function uncheck() {
      for (var _len3 = arguments.length, $selectors = Array(_len3), _key3 = 0; _key3 < _len3; _key3++) {
        $selectors[_key3] = arguments[_key3];
      }

      (0, _values2.default)($selectors).forEach(function (el) {
        el.prop('checked', false);
      });
    }

    /**
     * Make all given selectors checked
     *
     * @param $selectors
     * @private
     */

  }, {
    key: 'check',
    value: function check() {
      for (var _len4 = arguments.length, $selectors = Array(_len4), _key4 = 0; _key4 < _len4; _key4++) {
        $selectors[_key4] = arguments[_key4];
      }

      (0, _values2.default)($selectors).forEach(function (el) {
        el.prop('checked', true);
      });
    }
  }]);
  return ExportFormFieldToggle;
}();

exports.default = ExportFormFieldToggle;

/***/ }),

/***/ "./js/pages/translation-settings/FormFieldToggle.js":
/*!**********************************************************!*\
  !*** ./js/pages/translation-settings/FormFieldToggle.js ***!
  \**********************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

"use strict";


Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var _values = __webpack_require__(/*! babel-runtime/core-js/object/values */ "./node_modules/babel-runtime/core-js/object/values.js");

var _values2 = _interopRequireDefault(_values);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _TranslationSettingsMap = __webpack_require__(/*! ./TranslationSettingsMap */ "./js/pages/translation-settings/TranslationSettingsMap.js");

var _TranslationSettingsMap2 = _interopRequireDefault(_TranslationSettingsMap);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _window = window,
    $ = _window.$;

/**
 * Back office translations type
 *
 * @type {string}
 */
/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 */

var back = 'back';

/**
 * Modules translations type
 * @type {string}
 */
var themes = 'themes';

/**
 * Modules translations type
 * @type {string}
 */
var modules = 'modules';

/**
 * Mails translations type
 * @type {string}
 */
var mails = 'mails';

/**
 * Other translations type
 * @type {string}
 */
var others = 'others';

/**
 * Email body translations type
 * @type {string}
 */
var emailContentBody = 'body';

var FormFieldToggle = function () {
  function FormFieldToggle() {
    (0, _classCallCheck3.default)(this, FormFieldToggle);

    $(_TranslationSettingsMap2.default.translationType).on('change', this.toggleFields.bind(this));
    $(_TranslationSettingsMap2.default.emailContentType).on('change', this.toggleEmailFields.bind(this));

    this.toggleFields();
  }

  /**
   * Toggle dependant translations fields, based on selected translation type
   */


  (0, _createClass3.default)(FormFieldToggle, [{
    key: 'toggleFields',
    value: function toggleFields() {
      var selectedOption = $(_TranslationSettingsMap2.default.translationType).val();
      var $modulesFormGroup = $(_TranslationSettingsMap2.default.modulesFormGroup);
      var $emailFormGroup = $(_TranslationSettingsMap2.default.emailFormGroup);
      var $themesFormGroup = $(_TranslationSettingsMap2.default.themesFormGroup);
      var $defaultThemeOption = $themesFormGroup.find(_TranslationSettingsMap2.default.defaultThemeOption);

      switch (selectedOption) {
        case back:
        case others:
          this.hide($modulesFormGroup, $emailFormGroup, $themesFormGroup);
          break;

        case themes:
          this.show($themesFormGroup);
          this.hide($modulesFormGroup, $emailFormGroup, $defaultThemeOption);
          break;

        case modules:
          this.hide($emailFormGroup, $themesFormGroup);
          this.show($modulesFormGroup);
          break;

        case mails:
          this.hide($modulesFormGroup, $themesFormGroup);
          this.show($emailFormGroup);
          break;

        default:
          break;
      }

      this.toggleEmailFields();
    }

    /**
     * Toggles fields, which are related to email translations
     */

  }, {
    key: 'toggleEmailFields',
    value: function toggleEmailFields() {
      if ($(_TranslationSettingsMap2.default.translationType).val() !== mails) {
        return;
      }

      var selectedEmailContentType = $(_TranslationSettingsMap2.default.emailFormGroup).find('select').val();
      var $themesFormGroup = $(_TranslationSettingsMap2.default.themesFormGroup);
      var $noThemeOption = $themesFormGroup.find(_TranslationSettingsMap2.default.noThemeOption);
      var $defaultThemeOption = $themesFormGroup.find(_TranslationSettingsMap2.default.defaultThemeOption);

      if (selectedEmailContentType === emailContentBody) {
        $noThemeOption.prop('selected', true);
        this.show($noThemeOption, $themesFormGroup, $defaultThemeOption);
      } else {
        this.hide($noThemeOption, $themesFormGroup, $defaultThemeOption);
      }
    }

    /**
     * Make all given selectors hidden
     *
     * @param $selectors
     * @private
     */

  }, {
    key: 'hide',
    value: function hide() {
      for (var _len = arguments.length, $selectors = Array(_len), _key = 0; _key < _len; _key++) {
        $selectors[_key] = arguments[_key];
      }

      (0, _values2.default)($selectors).forEach(function (el) {
        el.addClass('d-none');
        el.find('select').prop('disabled', 'disabled');
      });
    }

    /**
     * Make all given selectors visible
     *
     * @param $selectors
     * @private
     */

  }, {
    key: 'show',
    value: function show() {
      for (var _len2 = arguments.length, $selectors = Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
        $selectors[_key2] = arguments[_key2];
      }

      (0, _values2.default)($selectors).forEach(function (el) {
        el.removeClass('d-none');
        el.find('select').prop('disabled', false);
      });
    }
  }]);
  return FormFieldToggle;
}();

exports.default = FormFieldToggle;

/***/ }),

/***/ "./js/pages/translation-settings/TranslationSettingsMap.js":
/*!*****************************************************************!*\
  !*** ./js/pages/translation-settings/TranslationSettingsMap.js ***!
  \*****************************************************************/
/***/ ((__unused_webpack_module, exports) => {

"use strict";


Object.defineProperty(exports, "__esModule", ({
  value: true
}));
/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 */

exports.default = {
  translationType: '.js-translation-type',
  emailContentType: '.js-email-content-type',
  emailFormGroup: '.js-email-form-group',
  modulesFormGroup: '.js-module-form-group',
  themesFormGroup: '.js-theme-form-group',
  defaultThemeOption: '.js-default-theme',
  noThemeOption: '.js-no-theme',
  exportCoreType: '#form_core_selectors_core_type',
  exportCoreValues: '#form_core_selectors_selected_value',
  exportThemesType: '#form_themes_selectors_themes_type',
  exportThemesValues: '#form_themes_selectors_selected_value',
  exportModulesType: '#form_modules_selectors_modules_type',
  exportModulesValues: '#form_modules_selectors_selected_value',
  exportLanguageButton: '#form-export-language-button'
};

/***/ }),

/***/ "./js/pages/translation-settings/TranslationSettingsPage.js":
/*!******************************************************************!*\
  !*** ./js/pages/translation-settings/TranslationSettingsPage.js ***!
  \******************************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

"use strict";


Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _FormFieldToggle = __webpack_require__(/*! ./FormFieldToggle */ "./js/pages/translation-settings/FormFieldToggle.js");

var _FormFieldToggle2 = _interopRequireDefault(_FormFieldToggle);

var _ExportFormFieldToggle = __webpack_require__(/*! ./ExportFormFieldToggle */ "./js/pages/translation-settings/ExportFormFieldToggle.js");

var _ExportFormFieldToggle2 = _interopRequireDefault(_ExportFormFieldToggle);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 */

var TranslationSettingsPage = function TranslationSettingsPage() {
  (0, _classCallCheck3.default)(this, TranslationSettingsPage);

  new _FormFieldToggle2.default();
  new _ExportFormFieldToggle2.default();
};

exports.default = TranslationSettingsPage;

/***/ }),

/***/ "./node_modules/babel-runtime/core-js/object/define-property.js":
/*!**********************************************************************!*\
  !*** ./node_modules/babel-runtime/core-js/object/define-property.js ***!
  \**********************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

module.exports = { "default": __webpack_require__(/*! core-js/library/fn/object/define-property */ "./node_modules/core-js/library/fn/object/define-property.js"), __esModule: true };

/***/ }),

/***/ "./node_modules/babel-runtime/core-js/object/values.js":
/*!*************************************************************!*\
  !*** ./node_modules/babel-runtime/core-js/object/values.js ***!
  \*************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

module.exports = { "default": __webpack_require__(/*! core-js/library/fn/object/values */ "./node_modules/core-js/library/fn/object/values.js"), __esModule: true };

/***/ }),

/***/ "./node_modules/babel-runtime/helpers/classCallCheck.js":
/*!**************************************************************!*\
  !*** ./node_modules/babel-runtime/helpers/classCallCheck.js ***!
  \**************************************************************/
/***/ ((__unused_webpack_module, exports) => {

"use strict";


exports.__esModule = true;

exports.default = function (instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
};

/***/ }),

/***/ "./node_modules/babel-runtime/helpers/createClass.js":
/*!***********************************************************!*\
  !*** ./node_modules/babel-runtime/helpers/createClass.js ***!
  \***********************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

"use strict";


exports.__esModule = true;

var _defineProperty = __webpack_require__(/*! ../core-js/object/define-property */ "./node_modules/babel-runtime/core-js/object/define-property.js");

var _defineProperty2 = _interopRequireDefault(_defineProperty);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      (0, _defineProperty2.default)(target, descriptor.key, descriptor);
    }
  }

  return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);
    if (staticProps) defineProperties(Constructor, staticProps);
    return Constructor;
  };
}();

/***/ }),

/***/ "./node_modules/core-js/library/fn/object/define-property.js":
/*!*******************************************************************!*\
  !*** ./node_modules/core-js/library/fn/object/define-property.js ***!
  \*******************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

__webpack_require__(/*! ../../modules/es6.object.define-property */ "./node_modules/core-js/library/modules/es6.object.define-property.js");
var $Object = __webpack_require__(/*! ../../modules/_core */ "./node_modules/core-js/library/modules/_core.js").Object;
module.exports = function defineProperty(it, key, desc) {
  return $Object.defineProperty(it, key, desc);
};


/***/ }),

/***/ "./node_modules/core-js/library/fn/object/values.js":
/*!**********************************************************!*\
  !*** ./node_modules/core-js/library/fn/object/values.js ***!
  \**********************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

__webpack_require__(/*! ../../modules/es7.object.values */ "./node_modules/core-js/library/modules/es7.object.values.js");
module.exports = __webpack_require__(/*! ../../modules/_core */ "./node_modules/core-js/library/modules/_core.js").Object.values;


/***/ }),

/***/ "./node_modules/core-js/library/modules/_a-function.js":
/*!*************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_a-function.js ***!
  \*************************************************************/
/***/ ((module) => {

module.exports = function (it) {
  if (typeof it != 'function') throw TypeError(it + ' is not a function!');
  return it;
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_an-object.js":
/*!************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_an-object.js ***!
  \************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var isObject = __webpack_require__(/*! ./_is-object */ "./node_modules/core-js/library/modules/_is-object.js");
module.exports = function (it) {
  if (!isObject(it)) throw TypeError(it + ' is not an object!');
  return it;
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_array-includes.js":
/*!*****************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_array-includes.js ***!
  \*****************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// false -> Array#indexOf
// true  -> Array#includes
var toIObject = __webpack_require__(/*! ./_to-iobject */ "./node_modules/core-js/library/modules/_to-iobject.js");
var toLength = __webpack_require__(/*! ./_to-length */ "./node_modules/core-js/library/modules/_to-length.js");
var toAbsoluteIndex = __webpack_require__(/*! ./_to-absolute-index */ "./node_modules/core-js/library/modules/_to-absolute-index.js");
module.exports = function (IS_INCLUDES) {
  return function ($this, el, fromIndex) {
    var O = toIObject($this);
    var length = toLength(O.length);
    var index = toAbsoluteIndex(fromIndex, length);
    var value;
    // Array#includes uses SameValueZero equality algorithm
    // eslint-disable-next-line no-self-compare
    if (IS_INCLUDES && el != el) while (length > index) {
      value = O[index++];
      // eslint-disable-next-line no-self-compare
      if (value != value) return true;
    // Array#indexOf ignores holes, Array#includes - not
    } else for (;length > index; index++) if (IS_INCLUDES || index in O) {
      if (O[index] === el) return IS_INCLUDES || index || 0;
    } return !IS_INCLUDES && -1;
  };
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_cof.js":
/*!******************************************************!*\
  !*** ./node_modules/core-js/library/modules/_cof.js ***!
  \******************************************************/
/***/ ((module) => {

var toString = {}.toString;

module.exports = function (it) {
  return toString.call(it).slice(8, -1);
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_core.js":
/*!*******************************************************!*\
  !*** ./node_modules/core-js/library/modules/_core.js ***!
  \*******************************************************/
/***/ ((module) => {

var core = module.exports = { version: '2.6.11' };
if (typeof __e == 'number') __e = core; // eslint-disable-line no-undef


/***/ }),

/***/ "./node_modules/core-js/library/modules/_ctx.js":
/*!******************************************************!*\
  !*** ./node_modules/core-js/library/modules/_ctx.js ***!
  \******************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// optional / simple context binding
var aFunction = __webpack_require__(/*! ./_a-function */ "./node_modules/core-js/library/modules/_a-function.js");
module.exports = function (fn, that, length) {
  aFunction(fn);
  if (that === undefined) return fn;
  switch (length) {
    case 1: return function (a) {
      return fn.call(that, a);
    };
    case 2: return function (a, b) {
      return fn.call(that, a, b);
    };
    case 3: return function (a, b, c) {
      return fn.call(that, a, b, c);
    };
  }
  return function (/* ...args */) {
    return fn.apply(that, arguments);
  };
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_defined.js":
/*!**********************************************************!*\
  !*** ./node_modules/core-js/library/modules/_defined.js ***!
  \**********************************************************/
/***/ ((module) => {

// 7.2.1 RequireObjectCoercible(argument)
module.exports = function (it) {
  if (it == undefined) throw TypeError("Can't call method on  " + it);
  return it;
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_descriptors.js":
/*!**************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_descriptors.js ***!
  \**************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// Thank's IE8 for his funny defineProperty
module.exports = !__webpack_require__(/*! ./_fails */ "./node_modules/core-js/library/modules/_fails.js")(function () {
  return Object.defineProperty({}, 'a', { get: function () { return 7; } }).a != 7;
});


/***/ }),

/***/ "./node_modules/core-js/library/modules/_dom-create.js":
/*!*************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_dom-create.js ***!
  \*************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var isObject = __webpack_require__(/*! ./_is-object */ "./node_modules/core-js/library/modules/_is-object.js");
var document = __webpack_require__(/*! ./_global */ "./node_modules/core-js/library/modules/_global.js").document;
// typeof document.createElement is 'object' in old IE
var is = isObject(document) && isObject(document.createElement);
module.exports = function (it) {
  return is ? document.createElement(it) : {};
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_enum-bug-keys.js":
/*!****************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_enum-bug-keys.js ***!
  \****************************************************************/
/***/ ((module) => {

// IE 8- don't enum bug keys
module.exports = (
  'constructor,hasOwnProperty,isPrototypeOf,propertyIsEnumerable,toLocaleString,toString,valueOf'
).split(',');


/***/ }),

/***/ "./node_modules/core-js/library/modules/_export.js":
/*!*********************************************************!*\
  !*** ./node_modules/core-js/library/modules/_export.js ***!
  \*********************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var global = __webpack_require__(/*! ./_global */ "./node_modules/core-js/library/modules/_global.js");
var core = __webpack_require__(/*! ./_core */ "./node_modules/core-js/library/modules/_core.js");
var ctx = __webpack_require__(/*! ./_ctx */ "./node_modules/core-js/library/modules/_ctx.js");
var hide = __webpack_require__(/*! ./_hide */ "./node_modules/core-js/library/modules/_hide.js");
var has = __webpack_require__(/*! ./_has */ "./node_modules/core-js/library/modules/_has.js");
var PROTOTYPE = 'prototype';

var $export = function (type, name, source) {
  var IS_FORCED = type & $export.F;
  var IS_GLOBAL = type & $export.G;
  var IS_STATIC = type & $export.S;
  var IS_PROTO = type & $export.P;
  var IS_BIND = type & $export.B;
  var IS_WRAP = type & $export.W;
  var exports = IS_GLOBAL ? core : core[name] || (core[name] = {});
  var expProto = exports[PROTOTYPE];
  var target = IS_GLOBAL ? global : IS_STATIC ? global[name] : (global[name] || {})[PROTOTYPE];
  var key, own, out;
  if (IS_GLOBAL) source = name;
  for (key in source) {
    // contains in native
    own = !IS_FORCED && target && target[key] !== undefined;
    if (own && has(exports, key)) continue;
    // export native or passed
    out = own ? target[key] : source[key];
    // prevent global pollution for namespaces
    exports[key] = IS_GLOBAL && typeof target[key] != 'function' ? source[key]
    // bind timers to global for call from export context
    : IS_BIND && own ? ctx(out, global)
    // wrap global constructors for prevent change them in library
    : IS_WRAP && target[key] == out ? (function (C) {
      var F = function (a, b, c) {
        if (this instanceof C) {
          switch (arguments.length) {
            case 0: return new C();
            case 1: return new C(a);
            case 2: return new C(a, b);
          } return new C(a, b, c);
        } return C.apply(this, arguments);
      };
      F[PROTOTYPE] = C[PROTOTYPE];
      return F;
    // make static versions for prototype methods
    })(out) : IS_PROTO && typeof out == 'function' ? ctx(Function.call, out) : out;
    // export proto methods to core.%CONSTRUCTOR%.methods.%NAME%
    if (IS_PROTO) {
      (exports.virtual || (exports.virtual = {}))[key] = out;
      // export proto methods to core.%CONSTRUCTOR%.prototype.%NAME%
      if (type & $export.R && expProto && !expProto[key]) hide(expProto, key, out);
    }
  }
};
// type bitmap
$export.F = 1;   // forced
$export.G = 2;   // global
$export.S = 4;   // static
$export.P = 8;   // proto
$export.B = 16;  // bind
$export.W = 32;  // wrap
$export.U = 64;  // safe
$export.R = 128; // real proto method for `library`
module.exports = $export;


/***/ }),

/***/ "./node_modules/core-js/library/modules/_fails.js":
/*!********************************************************!*\
  !*** ./node_modules/core-js/library/modules/_fails.js ***!
  \********************************************************/
/***/ ((module) => {

module.exports = function (exec) {
  try {
    return !!exec();
  } catch (e) {
    return true;
  }
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_global.js":
/*!*********************************************************!*\
  !*** ./node_modules/core-js/library/modules/_global.js ***!
  \*********************************************************/
/***/ ((module) => {

// https://github.com/zloirock/core-js/issues/86#issuecomment-115759028
var global = module.exports = typeof window != 'undefined' && window.Math == Math
  ? window : typeof self != 'undefined' && self.Math == Math ? self
  // eslint-disable-next-line no-new-func
  : Function('return this')();
if (typeof __g == 'number') __g = global; // eslint-disable-line no-undef


/***/ }),

/***/ "./node_modules/core-js/library/modules/_has.js":
/*!******************************************************!*\
  !*** ./node_modules/core-js/library/modules/_has.js ***!
  \******************************************************/
/***/ ((module) => {

var hasOwnProperty = {}.hasOwnProperty;
module.exports = function (it, key) {
  return hasOwnProperty.call(it, key);
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_hide.js":
/*!*******************************************************!*\
  !*** ./node_modules/core-js/library/modules/_hide.js ***!
  \*******************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var dP = __webpack_require__(/*! ./_object-dp */ "./node_modules/core-js/library/modules/_object-dp.js");
var createDesc = __webpack_require__(/*! ./_property-desc */ "./node_modules/core-js/library/modules/_property-desc.js");
module.exports = __webpack_require__(/*! ./_descriptors */ "./node_modules/core-js/library/modules/_descriptors.js") ? function (object, key, value) {
  return dP.f(object, key, createDesc(1, value));
} : function (object, key, value) {
  object[key] = value;
  return object;
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_ie8-dom-define.js":
/*!*****************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_ie8-dom-define.js ***!
  \*****************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

module.exports = !__webpack_require__(/*! ./_descriptors */ "./node_modules/core-js/library/modules/_descriptors.js") && !__webpack_require__(/*! ./_fails */ "./node_modules/core-js/library/modules/_fails.js")(function () {
  return Object.defineProperty(__webpack_require__(/*! ./_dom-create */ "./node_modules/core-js/library/modules/_dom-create.js")('div'), 'a', { get: function () { return 7; } }).a != 7;
});


/***/ }),

/***/ "./node_modules/core-js/library/modules/_iobject.js":
/*!**********************************************************!*\
  !*** ./node_modules/core-js/library/modules/_iobject.js ***!
  \**********************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// fallback for non-array-like ES3 and non-enumerable old V8 strings
var cof = __webpack_require__(/*! ./_cof */ "./node_modules/core-js/library/modules/_cof.js");
// eslint-disable-next-line no-prototype-builtins
module.exports = Object('z').propertyIsEnumerable(0) ? Object : function (it) {
  return cof(it) == 'String' ? it.split('') : Object(it);
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_is-object.js":
/*!************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_is-object.js ***!
  \************************************************************/
/***/ ((module) => {

module.exports = function (it) {
  return typeof it === 'object' ? it !== null : typeof it === 'function';
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_library.js":
/*!**********************************************************!*\
  !*** ./node_modules/core-js/library/modules/_library.js ***!
  \**********************************************************/
/***/ ((module) => {

module.exports = true;


/***/ }),

/***/ "./node_modules/core-js/library/modules/_object-dp.js":
/*!************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_object-dp.js ***!
  \************************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

var anObject = __webpack_require__(/*! ./_an-object */ "./node_modules/core-js/library/modules/_an-object.js");
var IE8_DOM_DEFINE = __webpack_require__(/*! ./_ie8-dom-define */ "./node_modules/core-js/library/modules/_ie8-dom-define.js");
var toPrimitive = __webpack_require__(/*! ./_to-primitive */ "./node_modules/core-js/library/modules/_to-primitive.js");
var dP = Object.defineProperty;

exports.f = __webpack_require__(/*! ./_descriptors */ "./node_modules/core-js/library/modules/_descriptors.js") ? Object.defineProperty : function defineProperty(O, P, Attributes) {
  anObject(O);
  P = toPrimitive(P, true);
  anObject(Attributes);
  if (IE8_DOM_DEFINE) try {
    return dP(O, P, Attributes);
  } catch (e) { /* empty */ }
  if ('get' in Attributes || 'set' in Attributes) throw TypeError('Accessors not supported!');
  if ('value' in Attributes) O[P] = Attributes.value;
  return O;
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_object-keys-internal.js":
/*!***********************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_object-keys-internal.js ***!
  \***********************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var has = __webpack_require__(/*! ./_has */ "./node_modules/core-js/library/modules/_has.js");
var toIObject = __webpack_require__(/*! ./_to-iobject */ "./node_modules/core-js/library/modules/_to-iobject.js");
var arrayIndexOf = __webpack_require__(/*! ./_array-includes */ "./node_modules/core-js/library/modules/_array-includes.js")(false);
var IE_PROTO = __webpack_require__(/*! ./_shared-key */ "./node_modules/core-js/library/modules/_shared-key.js")('IE_PROTO');

module.exports = function (object, names) {
  var O = toIObject(object);
  var i = 0;
  var result = [];
  var key;
  for (key in O) if (key != IE_PROTO) has(O, key) && result.push(key);
  // Don't enum bug & hidden keys
  while (names.length > i) if (has(O, key = names[i++])) {
    ~arrayIndexOf(result, key) || result.push(key);
  }
  return result;
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_object-keys.js":
/*!**************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_object-keys.js ***!
  \**************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// 19.1.2.14 / 15.2.3.14 Object.keys(O)
var $keys = __webpack_require__(/*! ./_object-keys-internal */ "./node_modules/core-js/library/modules/_object-keys-internal.js");
var enumBugKeys = __webpack_require__(/*! ./_enum-bug-keys */ "./node_modules/core-js/library/modules/_enum-bug-keys.js");

module.exports = Object.keys || function keys(O) {
  return $keys(O, enumBugKeys);
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_object-pie.js":
/*!*************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_object-pie.js ***!
  \*************************************************************/
/***/ ((__unused_webpack_module, exports) => {

exports.f = {}.propertyIsEnumerable;


/***/ }),

/***/ "./node_modules/core-js/library/modules/_object-to-array.js":
/*!******************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_object-to-array.js ***!
  \******************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var DESCRIPTORS = __webpack_require__(/*! ./_descriptors */ "./node_modules/core-js/library/modules/_descriptors.js");
var getKeys = __webpack_require__(/*! ./_object-keys */ "./node_modules/core-js/library/modules/_object-keys.js");
var toIObject = __webpack_require__(/*! ./_to-iobject */ "./node_modules/core-js/library/modules/_to-iobject.js");
var isEnum = __webpack_require__(/*! ./_object-pie */ "./node_modules/core-js/library/modules/_object-pie.js").f;
module.exports = function (isEntries) {
  return function (it) {
    var O = toIObject(it);
    var keys = getKeys(O);
    var length = keys.length;
    var i = 0;
    var result = [];
    var key;
    while (length > i) {
      key = keys[i++];
      if (!DESCRIPTORS || isEnum.call(O, key)) {
        result.push(isEntries ? [key, O[key]] : O[key]);
      }
    }
    return result;
  };
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_property-desc.js":
/*!****************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_property-desc.js ***!
  \****************************************************************/
/***/ ((module) => {

module.exports = function (bitmap, value) {
  return {
    enumerable: !(bitmap & 1),
    configurable: !(bitmap & 2),
    writable: !(bitmap & 4),
    value: value
  };
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_shared-key.js":
/*!*************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_shared-key.js ***!
  \*************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var shared = __webpack_require__(/*! ./_shared */ "./node_modules/core-js/library/modules/_shared.js")('keys');
var uid = __webpack_require__(/*! ./_uid */ "./node_modules/core-js/library/modules/_uid.js");
module.exports = function (key) {
  return shared[key] || (shared[key] = uid(key));
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_shared.js":
/*!*********************************************************!*\
  !*** ./node_modules/core-js/library/modules/_shared.js ***!
  \*********************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var core = __webpack_require__(/*! ./_core */ "./node_modules/core-js/library/modules/_core.js");
var global = __webpack_require__(/*! ./_global */ "./node_modules/core-js/library/modules/_global.js");
var SHARED = '__core-js_shared__';
var store = global[SHARED] || (global[SHARED] = {});

(module.exports = function (key, value) {
  return store[key] || (store[key] = value !== undefined ? value : {});
})('versions', []).push({
  version: core.version,
  mode: __webpack_require__(/*! ./_library */ "./node_modules/core-js/library/modules/_library.js") ? 'pure' : 'global',
  copyright: '© 2019 Denis Pushkarev (zloirock.ru)'
});


/***/ }),

/***/ "./node_modules/core-js/library/modules/_to-absolute-index.js":
/*!********************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_to-absolute-index.js ***!
  \********************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var toInteger = __webpack_require__(/*! ./_to-integer */ "./node_modules/core-js/library/modules/_to-integer.js");
var max = Math.max;
var min = Math.min;
module.exports = function (index, length) {
  index = toInteger(index);
  return index < 0 ? max(index + length, 0) : min(index, length);
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_to-integer.js":
/*!*************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_to-integer.js ***!
  \*************************************************************/
/***/ ((module) => {

// 7.1.4 ToInteger
var ceil = Math.ceil;
var floor = Math.floor;
module.exports = function (it) {
  return isNaN(it = +it) ? 0 : (it > 0 ? floor : ceil)(it);
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_to-iobject.js":
/*!*************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_to-iobject.js ***!
  \*************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// to indexed object, toObject with fallback for non-array-like ES3 strings
var IObject = __webpack_require__(/*! ./_iobject */ "./node_modules/core-js/library/modules/_iobject.js");
var defined = __webpack_require__(/*! ./_defined */ "./node_modules/core-js/library/modules/_defined.js");
module.exports = function (it) {
  return IObject(defined(it));
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_to-length.js":
/*!************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_to-length.js ***!
  \************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// 7.1.15 ToLength
var toInteger = __webpack_require__(/*! ./_to-integer */ "./node_modules/core-js/library/modules/_to-integer.js");
var min = Math.min;
module.exports = function (it) {
  return it > 0 ? min(toInteger(it), 0x1fffffffffffff) : 0; // pow(2, 53) - 1 == 9007199254740991
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_to-primitive.js":
/*!***************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_to-primitive.js ***!
  \***************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// 7.1.1 ToPrimitive(input [, PreferredType])
var isObject = __webpack_require__(/*! ./_is-object */ "./node_modules/core-js/library/modules/_is-object.js");
// instead of the ES6 spec version, we didn't implement @@toPrimitive case
// and the second argument - flag - preferred type is a string
module.exports = function (it, S) {
  if (!isObject(it)) return it;
  var fn, val;
  if (S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;
  if (typeof (fn = it.valueOf) == 'function' && !isObject(val = fn.call(it))) return val;
  if (!S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;
  throw TypeError("Can't convert object to primitive value");
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_uid.js":
/*!******************************************************!*\
  !*** ./node_modules/core-js/library/modules/_uid.js ***!
  \******************************************************/
/***/ ((module) => {

var id = 0;
var px = Math.random();
module.exports = function (key) {
  return 'Symbol('.concat(key === undefined ? '' : key, ')_', (++id + px).toString(36));
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/es6.object.define-property.js":
/*!****************************************************************************!*\
  !*** ./node_modules/core-js/library/modules/es6.object.define-property.js ***!
  \****************************************************************************/
/***/ ((__unused_webpack_module, __unused_webpack_exports, __webpack_require__) => {

var $export = __webpack_require__(/*! ./_export */ "./node_modules/core-js/library/modules/_export.js");
// 19.1.2.4 / 15.2.3.6 Object.defineProperty(O, P, Attributes)
$export($export.S + $export.F * !__webpack_require__(/*! ./_descriptors */ "./node_modules/core-js/library/modules/_descriptors.js"), 'Object', { defineProperty: __webpack_require__(/*! ./_object-dp */ "./node_modules/core-js/library/modules/_object-dp.js").f });


/***/ }),

/***/ "./node_modules/core-js/library/modules/es7.object.values.js":
/*!*******************************************************************!*\
  !*** ./node_modules/core-js/library/modules/es7.object.values.js ***!
  \*******************************************************************/
/***/ ((__unused_webpack_module, __unused_webpack_exports, __webpack_require__) => {

// https://github.com/tc39/proposal-object-values-entries
var $export = __webpack_require__(/*! ./_export */ "./node_modules/core-js/library/modules/_export.js");
var $values = __webpack_require__(/*! ./_object-to-array */ "./node_modules/core-js/library/modules/_object-to-array.js")(false);

$export($export.S, 'Object', {
  values: function values(it) {
    return $values(it);
  }
});


/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be in strict mode.
(() => {
"use strict";
/*!************************************************!*\
  !*** ./js/pages/translation-settings/index.js ***!
  \************************************************/


var _TranslationSettingsPage = __webpack_require__(/*! ./TranslationSettingsPage */ "./js/pages/translation-settings/TranslationSettingsPage.js");

var _TranslationSettingsPage2 = _interopRequireDefault(_TranslationSettingsPage);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _window = window,
    $ = _window.$; /**
                    * Copyright since 2007 PrestaShop SA and Contributors
                    * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
                    *
                    * NOTICE OF LICENSE
                    *
                    * This source file is subject to the Open Software License (OSL 3.0)
                    * that is bundled with this package in the file LICENSE.md.
                    * It is also available through the world-wide-web at this URL:
                    * https://opensource.org/licenses/OSL-3.0
                    * If you did not receive a copy of the license and are unable to
                    * obtain it through the world-wide-web, please send an email
                    * to license@prestashop.com so we can send you a copy immediately.
                    *
                    * DISCLAIMER
                    *
                    * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
                    * versions in the future. If you wish to customize PrestaShop for your
                    * needs please refer to https://devdocs.prestashop.com/ for more information.
                    *
                    * @author    PrestaShop SA and Contributors <contact@prestashop.com>
                    * @copyright Since 2007 PrestaShop SA and Contributors
                    * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
                    */

$(function () {
  new _TranslationSettingsPage2.default();
});
})();

window.translation_settings = __webpack_exports__;
/******/ })()
;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9bbmFtZV0vLi9qcy9wYWdlcy90cmFuc2xhdGlvbi1zZXR0aW5ncy9FeHBvcnRGb3JtRmllbGRUb2dnbGUuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vanMvcGFnZXMvdHJhbnNsYXRpb24tc2V0dGluZ3MvRm9ybUZpZWxkVG9nZ2xlLmpzIiwid2VicGFjazovL1tuYW1lXS8uL2pzL3BhZ2VzL3RyYW5zbGF0aW9uLXNldHRpbmdzL1RyYW5zbGF0aW9uU2V0dGluZ3NNYXAuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vanMvcGFnZXMvdHJhbnNsYXRpb24tc2V0dGluZ3MvVHJhbnNsYXRpb25TZXR0aW5nc1BhZ2UuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2JhYmVsLXJ1bnRpbWUvY29yZS1qcy9vYmplY3QvZGVmaW5lLXByb3BlcnR5LmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9iYWJlbC1ydW50aW1lL2NvcmUtanMvb2JqZWN0L3ZhbHVlcy5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvYmFiZWwtcnVudGltZS9oZWxwZXJzL2NsYXNzQ2FsbENoZWNrLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9iYWJlbC1ydW50aW1lL2hlbHBlcnMvY3JlYXRlQ2xhc3MuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9mbi9vYmplY3QvZGVmaW5lLXByb3BlcnR5LmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvZm4vb2JqZWN0L3ZhbHVlcy5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX2EtZnVuY3Rpb24uanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19hbi1vYmplY3QuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19hcnJheS1pbmNsdWRlcy5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX2NvZi5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX2NvcmUuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19jdHguanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19kZWZpbmVkLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fZGVzY3JpcHRvcnMuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19kb20tY3JlYXRlLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fZW51bS1idWcta2V5cy5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX2V4cG9ydC5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX2ZhaWxzLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fZ2xvYmFsLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9faGFzLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9faGlkZS5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX2llOC1kb20tZGVmaW5lLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9faW9iamVjdC5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX2lzLW9iamVjdC5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX2xpYnJhcnkuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19vYmplY3QtZHAuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19vYmplY3Qta2V5cy1pbnRlcm5hbC5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX29iamVjdC1rZXlzLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fb2JqZWN0LXBpZS5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX29iamVjdC10by1hcnJheS5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX3Byb3BlcnR5LWRlc2MuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19zaGFyZWQta2V5LmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fc2hhcmVkLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fdG8tYWJzb2x1dGUtaW5kZXguanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL190by1pbnRlZ2VyLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fdG8taW9iamVjdC5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX3RvLWxlbmd0aC5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX3RvLXByaW1pdGl2ZS5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX3VpZC5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvZXM2Lm9iamVjdC5kZWZpbmUtcHJvcGVydHkuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL2VzNy5vYmplY3QudmFsdWVzLmpzIiwid2VicGFjazovL1tuYW1lXS93ZWJwYWNrL2Jvb3RzdHJhcCIsIndlYnBhY2s6Ly9bbmFtZV0vLi9qcy9wYWdlcy90cmFuc2xhdGlvbi1zZXR0aW5ncy9pbmRleC5qcyJdLCJuYW1lcyI6WyJ3aW5kb3ciLCIkIiwiJGNvcmVUeXBlIiwiVHJhbnNsYXRpb25TZXR0aW5nc01hcCIsImV4cG9ydENvcmVUeXBlIiwiJHRoZW1lc1R5cGUiLCJleHBvcnRUaGVtZXNUeXBlIiwiJG1vZHVsZXNUeXBlIiwiZXhwb3J0TW9kdWxlc1R5cGUiLCIkY29yZVZhbHVlcyIsImV4cG9ydENvcmVWYWx1ZXMiLCJjbG9zZXN0IiwiJHRoZW1lc1ZhbHVlcyIsImV4cG9ydFRoZW1lc1ZhbHVlcyIsIiRtb2R1bGVzVmFsdWVzIiwiZXhwb3J0TW9kdWxlc1ZhbHVlcyIsIiRjb3JlQ2hlY2tib3hlcyIsIiR0aGVtZXNTZWxlY3QiLCIkbW9kdWxlc1NlbGVjdCIsIiRleHBvcnRCdXR0b24iLCJleHBvcnRMYW5ndWFnZUJ1dHRvbiIsIkV4cG9ydEZvcm1GaWVsZFRvZ2dsZSIsIm9uIiwiY29yZVR5cGVDaGFuZ2VkIiwiYmluZCIsInRoZW1lc1R5cGVDaGFuZ2VkIiwibW9kdWxlc1R5cGVDaGFuZ2VkIiwic3ViQ2hvaWNlc0NoYW5nZWQiLCJjaGVjayIsImlzIiwicHJvcCIsInVuY2hlY2siLCJzaG93IiwiaGlkZSIsImZpbmQiLCJzaXplIiwidmFsIiwiJHNlbGVjdG9ycyIsImZvckVhY2giLCJlbCIsImFkZENsYXNzIiwicmVtb3ZlQ2xhc3MiLCJiYWNrIiwidGhlbWVzIiwibW9kdWxlcyIsIm1haWxzIiwib3RoZXJzIiwiZW1haWxDb250ZW50Qm9keSIsIkZvcm1GaWVsZFRvZ2dsZSIsInRyYW5zbGF0aW9uVHlwZSIsInRvZ2dsZUZpZWxkcyIsImVtYWlsQ29udGVudFR5cGUiLCJ0b2dnbGVFbWFpbEZpZWxkcyIsInNlbGVjdGVkT3B0aW9uIiwiJG1vZHVsZXNGb3JtR3JvdXAiLCJtb2R1bGVzRm9ybUdyb3VwIiwiJGVtYWlsRm9ybUdyb3VwIiwiZW1haWxGb3JtR3JvdXAiLCIkdGhlbWVzRm9ybUdyb3VwIiwidGhlbWVzRm9ybUdyb3VwIiwiJGRlZmF1bHRUaGVtZU9wdGlvbiIsImRlZmF1bHRUaGVtZU9wdGlvbiIsInNlbGVjdGVkRW1haWxDb250ZW50VHlwZSIsIiRub1RoZW1lT3B0aW9uIiwibm9UaGVtZU9wdGlvbiIsIlRyYW5zbGF0aW9uU2V0dGluZ3NQYWdlIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBeUJBOzs7Ozs7Y0FFWUEsTTtJQUFMQyxDLFdBQUFBLEMsRUEzQlA7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUE2QkEsSUFBTUMsWUFBWUQsRUFBRUUsaUNBQXVCQyxjQUF6QixDQUFsQjtBQUNBLElBQU1DLGNBQWNKLEVBQUVFLGlDQUF1QkcsZ0JBQXpCLENBQXBCO0FBQ0EsSUFBTUMsZUFBZU4sRUFBRUUsaUNBQXVCSyxpQkFBekIsQ0FBckI7O0FBRUEsSUFBTUMsY0FBY1IsRUFBRUUsaUNBQXVCTyxnQkFBekIsRUFBMkNDLE9BQTNDLENBQW1ELGFBQW5ELENBQXBCO0FBQ0EsSUFBTUMsZ0JBQWdCWCxFQUFFRSxpQ0FBdUJVLGtCQUF6QixFQUE2Q0YsT0FBN0MsQ0FBcUQsYUFBckQsQ0FBdEI7QUFDQSxJQUFNRyxpQkFBaUJiLEVBQUVFLGlDQUF1QlksbUJBQXpCLEVBQThDSixPQUE5QyxDQUFzRCxhQUF0RCxDQUF2Qjs7QUFFQSxJQUFNSyxrQkFBa0JmLEVBQUVFLGlDQUF1Qk8sZ0JBQXpCLENBQXhCO0FBQ0EsSUFBTU8sZ0JBQWdCaEIsRUFBRUUsaUNBQXVCVSxrQkFBekIsQ0FBdEI7QUFDQSxJQUFNSyxpQkFBaUJqQixFQUFFRSxpQ0FBdUJZLG1CQUF6QixDQUF2Qjs7QUFFQSxJQUFNSSxnQkFBZ0JsQixFQUFFRSxpQ0FBdUJpQixvQkFBekIsQ0FBdEI7O0FBRUE7Ozs7Ozs7SUFNcUJDLHFCO0FBQ25CLG1DQUFjO0FBQUE7O0FBQ1puQixjQUFVb0IsRUFBVixDQUFhLFFBQWIsRUFBdUIsS0FBS0MsZUFBTCxDQUFxQkMsSUFBckIsQ0FBMEIsSUFBMUIsQ0FBdkI7QUFDQW5CLGdCQUFZaUIsRUFBWixDQUFlLFFBQWYsRUFBeUIsS0FBS0csaUJBQUwsQ0FBdUJELElBQXZCLENBQTRCLElBQTVCLENBQXpCO0FBQ0FqQixpQkFBYWUsRUFBYixDQUFnQixRQUFoQixFQUEwQixLQUFLSSxrQkFBTCxDQUF3QkYsSUFBeEIsQ0FBNkIsSUFBN0IsQ0FBMUI7O0FBRUFSLG9CQUFnQk0sRUFBaEIsQ0FBbUIsUUFBbkIsRUFBNkIsS0FBS0ssaUJBQUwsQ0FBdUJILElBQXZCLENBQTRCLElBQTVCLENBQTdCO0FBQ0FQLGtCQUFjSyxFQUFkLENBQWlCLFFBQWpCLEVBQTJCLEtBQUtLLGlCQUFMLENBQXVCSCxJQUF2QixDQUE0QixJQUE1QixDQUEzQjtBQUNBTixtQkFBZUksRUFBZixDQUFrQixRQUFsQixFQUE0QixLQUFLSyxpQkFBTCxDQUF1QkgsSUFBdkIsQ0FBNEIsSUFBNUIsQ0FBNUI7O0FBRUEsU0FBS0ksS0FBTCxDQUFXMUIsU0FBWDtBQUNEOzs7O3NDQUVpQjtBQUNoQixVQUFJLENBQUNBLFVBQVUyQixFQUFWLENBQWEsVUFBYixDQUFMLEVBQStCO0FBQzdCO0FBQ0Q7O0FBRUQzQixnQkFBVTRCLElBQVYsQ0FBZSxVQUFmLEVBQTJCLEtBQTNCO0FBQ0EsV0FBS0MsT0FBTCxDQUFhMUIsV0FBYixFQUEwQkUsWUFBMUI7QUFDQSxXQUFLeUIsSUFBTCxDQUFVdkIsV0FBVjtBQUNBLFdBQUt3QixJQUFMLENBQVVyQixhQUFWLEVBQXlCRSxjQUF6QjtBQUNBLFdBQUthLGlCQUFMO0FBQ0Q7Ozt3Q0FFbUI7QUFDbEIsVUFBSSxDQUFDdEIsWUFBWXdCLEVBQVosQ0FBZSxVQUFmLENBQUwsRUFBaUM7QUFDL0I7QUFDRDs7QUFFRHhCLGtCQUFZeUIsSUFBWixDQUFpQixVQUFqQixFQUE2QixLQUE3QjtBQUNBLFdBQUtDLE9BQUwsQ0FBYTdCLFNBQWIsRUFBd0JLLFlBQXhCO0FBQ0EsV0FBS3lCLElBQUwsQ0FBVXBCLGFBQVY7QUFDQSxXQUFLcUIsSUFBTCxDQUFVeEIsV0FBVixFQUF1QkssY0FBdkI7QUFDQSxXQUFLYSxpQkFBTDtBQUNEOzs7eUNBRW9CO0FBQ25CLFVBQUksQ0FBQ3BCLGFBQWFzQixFQUFiLENBQWdCLFVBQWhCLENBQUwsRUFBa0M7QUFDaEM7QUFDRDs7QUFFRGYscUJBQWVnQixJQUFmLENBQW9CLFVBQXBCLEVBQWdDLEtBQWhDO0FBQ0EsV0FBS0MsT0FBTCxDQUFhMUIsV0FBYixFQUEwQkgsU0FBMUI7QUFDQSxXQUFLOEIsSUFBTCxDQUFVbEIsY0FBVjtBQUNBLFdBQUttQixJQUFMLENBQVVyQixhQUFWLEVBQXlCSCxXQUF6QjtBQUNBLFdBQUtrQixpQkFBTDtBQUNEOzs7d0NBRW1CO0FBQ2xCLFVBQ0d6QixVQUFVNEIsSUFBVixDQUFlLFNBQWYsS0FBNkJkLGdCQUFnQmtCLElBQWhCLENBQXFCLFVBQXJCLEVBQWlDQyxJQUFqQyxLQUEwQyxDQUF4RSxJQUNJOUIsWUFBWXlCLElBQVosQ0FBaUIsU0FBakIsS0FBK0JiLGNBQWNtQixHQUFkLE9BQXdCLElBRDNELElBRUk3QixhQUFhdUIsSUFBYixDQUFrQixTQUFsQixLQUFnQ1osZUFBZWtCLEdBQWYsT0FBeUIsSUFIL0QsRUFJRTtBQUNBakIsc0JBQWNXLElBQWQsQ0FBbUIsVUFBbkIsRUFBK0IsS0FBL0I7O0FBRUE7QUFDRDs7QUFFRFgsb0JBQWNXLElBQWQsQ0FBbUIsVUFBbkIsRUFBK0IsSUFBL0I7QUFDRDs7QUFFRDs7Ozs7Ozs7OzJCQU1vQjtBQUFBLHdDQUFaTyxVQUFZO0FBQVpBLGtCQUFZO0FBQUE7O0FBQ2xCLDRCQUFjQSxVQUFkLEVBQTBCQyxPQUExQixDQUFrQyxVQUFDQyxFQUFELEVBQVE7QUFDeENBLFdBQUdDLFFBQUgsQ0FBWSxRQUFaO0FBQ0FELFdBQUdMLElBQUgsQ0FBUSxlQUFSLEVBQXlCSixJQUF6QixDQUE4QixVQUE5QixFQUEwQyxVQUExQztBQUNELE9BSEQ7QUFJRDs7QUFFRDs7Ozs7Ozs7OzJCQU1vQjtBQUFBLHlDQUFaTyxVQUFZO0FBQVpBLGtCQUFZO0FBQUE7O0FBQ2xCLDRCQUFjQSxVQUFkLEVBQTBCQyxPQUExQixDQUFrQyxVQUFDQyxFQUFELEVBQVE7QUFDeENBLFdBQUdFLFdBQUgsQ0FBZSxRQUFmO0FBQ0FGLFdBQUdMLElBQUgsQ0FBUSxlQUFSLEVBQXlCSixJQUF6QixDQUE4QixVQUE5QixFQUEwQyxLQUExQztBQUNELE9BSEQ7QUFJRDs7QUFFRDs7Ozs7Ozs7OzhCQU11QjtBQUFBLHlDQUFaTyxVQUFZO0FBQVpBLGtCQUFZO0FBQUE7O0FBQ3JCLDRCQUFjQSxVQUFkLEVBQTBCQyxPQUExQixDQUFrQyxVQUFDQyxFQUFELEVBQVE7QUFDeENBLFdBQUdULElBQUgsQ0FBUSxTQUFSLEVBQW1CLEtBQW5CO0FBQ0QsT0FGRDtBQUdEOztBQUVEOzs7Ozs7Ozs7NEJBTXFCO0FBQUEseUNBQVpPLFVBQVk7QUFBWkEsa0JBQVk7QUFBQTs7QUFDbkIsNEJBQWNBLFVBQWQsRUFBMEJDLE9BQTFCLENBQWtDLFVBQUNDLEVBQUQsRUFBUTtBQUN4Q0EsV0FBR1QsSUFBSCxDQUFRLFNBQVIsRUFBbUIsSUFBbkI7QUFDRCxPQUZEO0FBR0Q7Ozs7O2tCQS9Ha0JULHFCOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3ZCckI7Ozs7OztjQUVZckIsTTtJQUFMQyxDLFdBQUFBLEM7O0FBRVA7Ozs7O0FBOUJBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUNBLElBQU15QyxPQUFPLE1BQWI7O0FBRUE7Ozs7QUFJQSxJQUFNQyxTQUFTLFFBQWY7O0FBRUE7Ozs7QUFJQSxJQUFNQyxVQUFVLFNBQWhCOztBQUVBOzs7O0FBSUEsSUFBTUMsUUFBUSxPQUFkOztBQUVBOzs7O0FBSUEsSUFBTUMsU0FBUyxRQUFmOztBQUVBOzs7O0FBSUEsSUFBTUMsbUJBQW1CLE1BQXpCOztJQUVxQkMsZTtBQUNuQiw2QkFBYztBQUFBOztBQUNaL0MsTUFBRUUsaUNBQXVCOEMsZUFBekIsRUFBMEMzQixFQUExQyxDQUE2QyxRQUE3QyxFQUF1RCxLQUFLNEIsWUFBTCxDQUFrQjFCLElBQWxCLENBQXVCLElBQXZCLENBQXZEO0FBQ0F2QixNQUFFRSxpQ0FBdUJnRCxnQkFBekIsRUFBMkM3QixFQUEzQyxDQUE4QyxRQUE5QyxFQUF3RCxLQUFLOEIsaUJBQUwsQ0FBdUI1QixJQUF2QixDQUE0QixJQUE1QixDQUF4RDs7QUFFQSxTQUFLMEIsWUFBTDtBQUNEOztBQUVEOzs7Ozs7O21DQUdlO0FBQ2IsVUFBTUcsaUJBQWlCcEQsRUFBRUUsaUNBQXVCOEMsZUFBekIsRUFBMENiLEdBQTFDLEVBQXZCO0FBQ0EsVUFBTWtCLG9CQUFvQnJELEVBQUVFLGlDQUF1Qm9ELGdCQUF6QixDQUExQjtBQUNBLFVBQU1DLGtCQUFrQnZELEVBQUVFLGlDQUF1QnNELGNBQXpCLENBQXhCO0FBQ0EsVUFBTUMsbUJBQW1CekQsRUFBRUUsaUNBQXVCd0QsZUFBekIsQ0FBekI7QUFDQSxVQUFNQyxzQkFBc0JGLGlCQUFpQnhCLElBQWpCLENBQXNCL0IsaUNBQXVCMEQsa0JBQTdDLENBQTVCOztBQUVBLGNBQVFSLGNBQVI7QUFDRSxhQUFLWCxJQUFMO0FBQ0EsYUFBS0ksTUFBTDtBQUNFLGVBQUtiLElBQUwsQ0FBVXFCLGlCQUFWLEVBQTZCRSxlQUE3QixFQUE4Q0UsZ0JBQTlDO0FBQ0E7O0FBRUYsYUFBS2YsTUFBTDtBQUNFLGVBQUtYLElBQUwsQ0FBVTBCLGdCQUFWO0FBQ0EsZUFBS3pCLElBQUwsQ0FBVXFCLGlCQUFWLEVBQTZCRSxlQUE3QixFQUE4Q0ksbUJBQTlDO0FBQ0E7O0FBRUYsYUFBS2hCLE9BQUw7QUFDRSxlQUFLWCxJQUFMLENBQVV1QixlQUFWLEVBQTJCRSxnQkFBM0I7QUFDQSxlQUFLMUIsSUFBTCxDQUFVc0IsaUJBQVY7QUFDQTs7QUFFRixhQUFLVCxLQUFMO0FBQ0UsZUFBS1osSUFBTCxDQUFVcUIsaUJBQVYsRUFBNkJJLGdCQUE3QjtBQUNBLGVBQUsxQixJQUFMLENBQVV3QixlQUFWO0FBQ0E7O0FBRUY7QUFDRTtBQXRCSjs7QUF5QkEsV0FBS0osaUJBQUw7QUFDRDs7QUFFRDs7Ozs7O3dDQUdvQjtBQUNsQixVQUFJbkQsRUFBRUUsaUNBQXVCOEMsZUFBekIsRUFBMENiLEdBQTFDLE9BQW9EUyxLQUF4RCxFQUErRDtBQUM3RDtBQUNEOztBQUVELFVBQU1pQiwyQkFBMkI3RCxFQUFFRSxpQ0FBdUJzRCxjQUF6QixFQUF5Q3ZCLElBQXpDLENBQThDLFFBQTlDLEVBQXdERSxHQUF4RCxFQUFqQztBQUNBLFVBQU1zQixtQkFBbUJ6RCxFQUFFRSxpQ0FBdUJ3RCxlQUF6QixDQUF6QjtBQUNBLFVBQU1JLGlCQUFpQkwsaUJBQWlCeEIsSUFBakIsQ0FBc0IvQixpQ0FBdUI2RCxhQUE3QyxDQUF2QjtBQUNBLFVBQU1KLHNCQUFzQkYsaUJBQWlCeEIsSUFBakIsQ0FBc0IvQixpQ0FBdUIwRCxrQkFBN0MsQ0FBNUI7O0FBRUEsVUFBSUMsNkJBQTZCZixnQkFBakMsRUFBbUQ7QUFDakRnQix1QkFBZWpDLElBQWYsQ0FBb0IsVUFBcEIsRUFBZ0MsSUFBaEM7QUFDQSxhQUFLRSxJQUFMLENBQVUrQixjQUFWLEVBQTBCTCxnQkFBMUIsRUFBNENFLG1CQUE1QztBQUNELE9BSEQsTUFHTztBQUNMLGFBQUszQixJQUFMLENBQVU4QixjQUFWLEVBQTBCTCxnQkFBMUIsRUFBNENFLG1CQUE1QztBQUNEO0FBQ0Y7O0FBRUQ7Ozs7Ozs7OzsyQkFNb0I7QUFBQSx3Q0FBWnZCLFVBQVk7QUFBWkEsa0JBQVk7QUFBQTs7QUFDbEIsNEJBQWNBLFVBQWQsRUFBMEJDLE9BQTFCLENBQWtDLFVBQUNDLEVBQUQsRUFBUTtBQUN4Q0EsV0FBR0MsUUFBSCxDQUFZLFFBQVo7QUFDQUQsV0FBR0wsSUFBSCxDQUFRLFFBQVIsRUFBa0JKLElBQWxCLENBQXVCLFVBQXZCLEVBQW1DLFVBQW5DO0FBQ0QsT0FIRDtBQUlEOztBQUVEOzs7Ozs7Ozs7MkJBTW9CO0FBQUEseUNBQVpPLFVBQVk7QUFBWkEsa0JBQVk7QUFBQTs7QUFDbEIsNEJBQWNBLFVBQWQsRUFBMEJDLE9BQTFCLENBQWtDLFVBQUNDLEVBQUQsRUFBUTtBQUN4Q0EsV0FBR0UsV0FBSCxDQUFlLFFBQWY7QUFDQUYsV0FBR0wsSUFBSCxDQUFRLFFBQVIsRUFBa0JKLElBQWxCLENBQXVCLFVBQXZCLEVBQW1DLEtBQW5DO0FBQ0QsT0FIRDtBQUlEOzs7OztrQkEzRmtCa0IsZTs7Ozs7Ozs7Ozs7Ozs7OztBQ25FckI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7a0JBeUJlO0FBQ2JDLG1CQUFpQixzQkFESjtBQUViRSxvQkFBa0Isd0JBRkw7QUFHYk0sa0JBQWdCLHNCQUhIO0FBSWJGLG9CQUFrQix1QkFKTDtBQUtiSSxtQkFBaUIsc0JBTEo7QUFNYkUsc0JBQW9CLG1CQU5QO0FBT2JHLGlCQUFlLGNBUEY7QUFRYjVELGtCQUFnQixnQ0FSSDtBQVNiTSxvQkFBa0IscUNBVEw7QUFVYkosb0JBQWtCLG9DQVZMO0FBV2JPLHNCQUFvQix1Q0FYUDtBQVliTCxxQkFBbUIsc0NBWk47QUFhYk8sdUJBQXFCLHdDQWJSO0FBY2JLLHdCQUFzQjtBQWRULEM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ0FmOzs7O0FBQ0E7Ozs7OztBQTFCQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztJQTRCcUI2Qyx1QixHQUNuQixtQ0FBYztBQUFBOztBQUNaLE1BQUlqQix5QkFBSjtBQUNBLE1BQUkzQiwrQkFBSjtBQUNELEM7O2tCQUprQjRDLHVCOzs7Ozs7Ozs7O0FDNUJyQixrQkFBa0IsWUFBWSxtQkFBTyxDQUFDLDhHQUEyQyxzQjs7Ozs7Ozs7OztBQ0FqRixrQkFBa0IsWUFBWSxtQkFBTyxDQUFDLDRGQUFrQyxzQjs7Ozs7Ozs7Ozs7QUNBM0Q7O0FBRWIsa0JBQWtCOztBQUVsQixlQUFlO0FBQ2Y7QUFDQTtBQUNBO0FBQ0EsRTs7Ozs7Ozs7Ozs7QUNSYTs7QUFFYixrQkFBa0I7O0FBRWxCLHNCQUFzQixtQkFBTyxDQUFDLHlHQUFtQzs7QUFFakU7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGLGVBQWU7QUFDZjtBQUNBLG1CQUFtQixrQkFBa0I7QUFDckM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUMsRzs7Ozs7Ozs7OztBQzFCRCxtQkFBTyxDQUFDLHNIQUEwQztBQUNsRCxjQUFjLHdHQUFxQztBQUNuRDtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7O0FDSkEsbUJBQU8sQ0FBQyxvR0FBaUM7QUFDekMsZ0lBQTZEOzs7Ozs7Ozs7OztBQ0Q3RDtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNIQSxlQUFlLG1CQUFPLENBQUMsMEVBQWM7QUFDckM7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7O0FDSkE7QUFDQTtBQUNBLGdCQUFnQixtQkFBTyxDQUFDLDRFQUFlO0FBQ3ZDLGVBQWUsbUJBQU8sQ0FBQywwRUFBYztBQUNyQyxzQkFBc0IsbUJBQU8sQ0FBQywwRkFBc0I7QUFDcEQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLLFlBQVksZUFBZTtBQUNoQztBQUNBLEtBQUs7QUFDTDtBQUNBOzs7Ozs7Ozs7OztBQ3RCQSxpQkFBaUI7O0FBRWpCO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNKQSw2QkFBNkI7QUFDN0IsdUNBQXVDOzs7Ozs7Ozs7OztBQ0R2QztBQUNBLGdCQUFnQixtQkFBTyxDQUFDLDRFQUFlO0FBQ3ZDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNuQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNKQTtBQUNBLGtCQUFrQixtQkFBTyxDQUFDLGtFQUFVO0FBQ3BDLGlDQUFpQyxRQUFRLG1CQUFtQixVQUFVLEVBQUUsRUFBRTtBQUMxRSxDQUFDOzs7Ozs7Ozs7OztBQ0hELGVBQWUsbUJBQU8sQ0FBQywwRUFBYztBQUNyQyxlQUFlLGtHQUE2QjtBQUM1QztBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7OztBQ05BO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7OztBQ0hBLGFBQWEsbUJBQU8sQ0FBQyxvRUFBVztBQUNoQyxXQUFXLG1CQUFPLENBQUMsZ0VBQVM7QUFDNUIsVUFBVSxtQkFBTyxDQUFDLDhEQUFRO0FBQzFCLFdBQVcsbUJBQU8sQ0FBQyxnRUFBUztBQUM1QixVQUFVLG1CQUFPLENBQUMsOERBQVE7QUFDMUI7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpRUFBaUU7QUFDakU7QUFDQSxrRkFBa0Y7QUFDbEY7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVc7QUFDWCxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLCtDQUErQztBQUMvQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxjQUFjO0FBQ2QsY0FBYztBQUNkLGNBQWM7QUFDZCxjQUFjO0FBQ2QsZUFBZTtBQUNmLGVBQWU7QUFDZixlQUFlO0FBQ2YsZ0JBQWdCO0FBQ2hCOzs7Ozs7Ozs7OztBQzdEQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBOzs7Ozs7Ozs7OztBQ05BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx5Q0FBeUM7Ozs7Ozs7Ozs7O0FDTHpDLHVCQUF1QjtBQUN2QjtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7O0FDSEEsU0FBUyxtQkFBTyxDQUFDLDBFQUFjO0FBQy9CLGlCQUFpQixtQkFBTyxDQUFDLGtGQUFrQjtBQUMzQyxpQkFBaUIsbUJBQU8sQ0FBQyw4RUFBZ0I7QUFDekM7QUFDQSxDQUFDO0FBQ0Q7QUFDQTtBQUNBOzs7Ozs7Ozs7OztBQ1BBLGtCQUFrQixtQkFBTyxDQUFDLDhFQUFnQixNQUFNLG1CQUFPLENBQUMsa0VBQVU7QUFDbEUsK0JBQStCLG1CQUFPLENBQUMsNEVBQWUsZ0JBQWdCLG1CQUFtQixVQUFVLEVBQUUsRUFBRTtBQUN2RyxDQUFDOzs7Ozs7Ozs7OztBQ0ZEO0FBQ0EsVUFBVSxtQkFBTyxDQUFDLDhEQUFRO0FBQzFCO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7OztBQ0xBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNGQTs7Ozs7Ozs7Ozs7QUNBQSxlQUFlLG1CQUFPLENBQUMsMEVBQWM7QUFDckMscUJBQXFCLG1CQUFPLENBQUMsb0ZBQW1CO0FBQ2hELGtCQUFrQixtQkFBTyxDQUFDLGdGQUFpQjtBQUMzQzs7QUFFQSxTQUFTLEdBQUcsbUJBQU8sQ0FBQyw4RUFBZ0I7QUFDcEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUcsWUFBWTtBQUNmO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7OztBQ2ZBLFVBQVUsbUJBQU8sQ0FBQyw4REFBUTtBQUMxQixnQkFBZ0IsbUJBQU8sQ0FBQyw0RUFBZTtBQUN2QyxtQkFBbUIsbUJBQU8sQ0FBQyxvRkFBbUI7QUFDOUMsZUFBZSxtQkFBTyxDQUFDLDRFQUFlOztBQUV0QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7O0FDaEJBO0FBQ0EsWUFBWSxtQkFBTyxDQUFDLGdHQUF5QjtBQUM3QyxrQkFBa0IsbUJBQU8sQ0FBQyxrRkFBa0I7O0FBRTVDO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNOQSxTQUFTLEtBQUs7Ozs7Ozs7Ozs7O0FDQWQsa0JBQWtCLG1CQUFPLENBQUMsOEVBQWdCO0FBQzFDLGNBQWMsbUJBQU8sQ0FBQyw4RUFBZ0I7QUFDdEMsZ0JBQWdCLG1CQUFPLENBQUMsNEVBQWU7QUFDdkMsYUFBYSxtR0FBMEI7QUFDdkM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNwQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNQQSxhQUFhLG1CQUFPLENBQUMsb0VBQVc7QUFDaEMsVUFBVSxtQkFBTyxDQUFDLDhEQUFRO0FBQzFCO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNKQSxXQUFXLG1CQUFPLENBQUMsZ0VBQVM7QUFDNUIsYUFBYSxtQkFBTyxDQUFDLG9FQUFXO0FBQ2hDO0FBQ0Esa0RBQWtEOztBQUVsRDtBQUNBLHFFQUFxRTtBQUNyRSxDQUFDO0FBQ0Q7QUFDQSxRQUFRLG1CQUFPLENBQUMsc0VBQVk7QUFDNUI7QUFDQSxDQUFDOzs7Ozs7Ozs7OztBQ1hELGdCQUFnQixtQkFBTyxDQUFDLDRFQUFlO0FBQ3ZDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNOQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7O0FDTEE7QUFDQSxjQUFjLG1CQUFPLENBQUMsc0VBQVk7QUFDbEMsY0FBYyxtQkFBTyxDQUFDLHNFQUFZO0FBQ2xDO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNMQTtBQUNBLGdCQUFnQixtQkFBTyxDQUFDLDRFQUFlO0FBQ3ZDO0FBQ0E7QUFDQSwyREFBMkQ7QUFDM0Q7Ozs7Ozs7Ozs7O0FDTEE7QUFDQSxlQUFlLG1CQUFPLENBQUMsMEVBQWM7QUFDckM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7O0FDWEE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNKQSxjQUFjLG1CQUFPLENBQUMsb0VBQVc7QUFDakM7QUFDQSxpQ0FBaUMsbUJBQU8sQ0FBQyw4RUFBZ0IsY0FBYyxpQkFBaUIsaUdBQXlCLEVBQUU7Ozs7Ozs7Ozs7O0FDRm5IO0FBQ0EsY0FBYyxtQkFBTyxDQUFDLG9FQUFXO0FBQ2pDLGNBQWMsbUJBQU8sQ0FBQyxzRkFBb0I7O0FBRTFDO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7Ozs7OztVQ1JEO1VBQ0E7O1VBRUE7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7O1VBRUE7VUFDQTs7VUFFQTtVQUNBO1VBQ0E7Ozs7Ozs7Ozs7OztBQ0dBOzs7Ozs7Y0FFWWpFLE07SUFBTEMsQyxXQUFBQSxDLEVBM0JQOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBNkJBQSxFQUFFLFlBQU07QUFDTixNQUFJZ0UsaUNBQUo7QUFDRCxDQUZELEUiLCJmaWxlIjoidHJhbnNsYXRpb25fc2V0dGluZ3MuYnVuZGxlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiBDb3B5cmlnaHQgc2luY2UgMjAwNyBQcmVzdGFTaG9wIFNBIGFuZCBDb250cmlidXRvcnNcbiAqIFByZXN0YVNob3AgaXMgYW4gSW50ZXJuYXRpb25hbCBSZWdpc3RlcmVkIFRyYWRlbWFyayAmIFByb3BlcnR5IG9mIFByZXN0YVNob3AgU0FcbiAqXG4gKiBOT1RJQ0UgT0YgTElDRU5TRVxuICpcbiAqIFRoaXMgc291cmNlIGZpbGUgaXMgc3ViamVjdCB0byB0aGUgT3BlbiBTb2Z0d2FyZSBMaWNlbnNlIChPU0wgMy4wKVxuICogdGhhdCBpcyBidW5kbGVkIHdpdGggdGhpcyBwYWNrYWdlIGluIHRoZSBmaWxlIExJQ0VOU0UubWQuXG4gKiBJdCBpcyBhbHNvIGF2YWlsYWJsZSB0aHJvdWdoIHRoZSB3b3JsZC13aWRlLXdlYiBhdCB0aGlzIFVSTDpcbiAqIGh0dHBzOi8vb3BlbnNvdXJjZS5vcmcvbGljZW5zZXMvT1NMLTMuMFxuICogSWYgeW91IGRpZCBub3QgcmVjZWl2ZSBhIGNvcHkgb2YgdGhlIGxpY2Vuc2UgYW5kIGFyZSB1bmFibGUgdG9cbiAqIG9idGFpbiBpdCB0aHJvdWdoIHRoZSB3b3JsZC13aWRlLXdlYiwgcGxlYXNlIHNlbmQgYW4gZW1haWxcbiAqIHRvIGxpY2Vuc2VAcHJlc3Rhc2hvcC5jb20gc28gd2UgY2FuIHNlbmQgeW91IGEgY29weSBpbW1lZGlhdGVseS5cbiAqXG4gKiBESVNDTEFJTUVSXG4gKlxuICogRG8gbm90IGVkaXQgb3IgYWRkIHRvIHRoaXMgZmlsZSBpZiB5b3Ugd2lzaCB0byB1cGdyYWRlIFByZXN0YVNob3AgdG8gbmV3ZXJcbiAqIHZlcnNpb25zIGluIHRoZSBmdXR1cmUuIElmIHlvdSB3aXNoIHRvIGN1c3RvbWl6ZSBQcmVzdGFTaG9wIGZvciB5b3VyXG4gKiBuZWVkcyBwbGVhc2UgcmVmZXIgdG8gaHR0cHM6Ly9kZXZkb2NzLnByZXN0YXNob3AuY29tLyBmb3IgbW9yZSBpbmZvcm1hdGlvbi5cbiAqXG4gKiBAYXV0aG9yICAgIFByZXN0YVNob3AgU0EgYW5kIENvbnRyaWJ1dG9ycyA8Y29udGFjdEBwcmVzdGFzaG9wLmNvbT5cbiAqIEBjb3B5cmlnaHQgU2luY2UgMjAwNyBQcmVzdGFTaG9wIFNBIGFuZCBDb250cmlidXRvcnNcbiAqIEBsaWNlbnNlICAgaHR0cHM6Ly9vcGVuc291cmNlLm9yZy9saWNlbnNlcy9PU0wtMy4wIE9wZW4gU29mdHdhcmUgTGljZW5zZSAoT1NMIDMuMClcbiAqL1xuXG5pbXBvcnQgVHJhbnNsYXRpb25TZXR0aW5nc01hcCBmcm9tICcuL1RyYW5zbGF0aW9uU2V0dGluZ3NNYXAnO1xuXG5jb25zdCB7JH0gPSB3aW5kb3c7XG5cbmNvbnN0ICRjb3JlVHlwZSA9ICQoVHJhbnNsYXRpb25TZXR0aW5nc01hcC5leHBvcnRDb3JlVHlwZSk7XG5jb25zdCAkdGhlbWVzVHlwZSA9ICQoVHJhbnNsYXRpb25TZXR0aW5nc01hcC5leHBvcnRUaGVtZXNUeXBlKTtcbmNvbnN0ICRtb2R1bGVzVHlwZSA9ICQoVHJhbnNsYXRpb25TZXR0aW5nc01hcC5leHBvcnRNb2R1bGVzVHlwZSk7XG5cbmNvbnN0ICRjb3JlVmFsdWVzID0gJChUcmFuc2xhdGlvblNldHRpbmdzTWFwLmV4cG9ydENvcmVWYWx1ZXMpLmNsb3Nlc3QoJy5mb3JtLWdyb3VwJyk7XG5jb25zdCAkdGhlbWVzVmFsdWVzID0gJChUcmFuc2xhdGlvblNldHRpbmdzTWFwLmV4cG9ydFRoZW1lc1ZhbHVlcykuY2xvc2VzdCgnLmZvcm0tZ3JvdXAnKTtcbmNvbnN0ICRtb2R1bGVzVmFsdWVzID0gJChUcmFuc2xhdGlvblNldHRpbmdzTWFwLmV4cG9ydE1vZHVsZXNWYWx1ZXMpLmNsb3Nlc3QoJy5mb3JtLWdyb3VwJyk7XG5cbmNvbnN0ICRjb3JlQ2hlY2tib3hlcyA9ICQoVHJhbnNsYXRpb25TZXR0aW5nc01hcC5leHBvcnRDb3JlVmFsdWVzKTtcbmNvbnN0ICR0aGVtZXNTZWxlY3QgPSAkKFRyYW5zbGF0aW9uU2V0dGluZ3NNYXAuZXhwb3J0VGhlbWVzVmFsdWVzKTtcbmNvbnN0ICRtb2R1bGVzU2VsZWN0ID0gJChUcmFuc2xhdGlvblNldHRpbmdzTWFwLmV4cG9ydE1vZHVsZXNWYWx1ZXMpO1xuXG5jb25zdCAkZXhwb3J0QnV0dG9uID0gJChUcmFuc2xhdGlvblNldHRpbmdzTWFwLmV4cG9ydExhbmd1YWdlQnV0dG9uKTtcblxuLyoqXG4gKiBUb2dnbGVzIHNob3cvaGlkZSBmb3IgdGhlIHNlbGVjdG9ycyBvZiBzdWJ0eXBlcyAoaW4gY2FzZSBvZiBDb3JlIHR5cGUpLCB0aGVtZSBvciBtb2R1bGUgd2hlbiBhIFR5cGUgaXMgc2VsZWN0ZWRcbiAqXG4gKiBFeGFtcGxlIDogSWYgQ29yZSB0eXBlIGlzIHNlbGVjdGVkLCB0aGUgc3VidHlwZXMgY2hlY2tib3hlcyBhcmUgc2hvd24sXG4gKiBUaGVtZSBhbmQgTW9kdWxlIHR5cGVzIGFyZSB1bnNlbGVjdGVkIGFuZCB0aGVpciB2YWx1ZSBzZWxlY3RvciBhcmUgaGlkZGVuXG4gKi9cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIEV4cG9ydEZvcm1GaWVsZFRvZ2dsZSB7XG4gIGNvbnN0cnVjdG9yKCkge1xuICAgICRjb3JlVHlwZS5vbignY2hhbmdlJywgdGhpcy5jb3JlVHlwZUNoYW5nZWQuYmluZCh0aGlzKSk7XG4gICAgJHRoZW1lc1R5cGUub24oJ2NoYW5nZScsIHRoaXMudGhlbWVzVHlwZUNoYW5nZWQuYmluZCh0aGlzKSk7XG4gICAgJG1vZHVsZXNUeXBlLm9uKCdjaGFuZ2UnLCB0aGlzLm1vZHVsZXNUeXBlQ2hhbmdlZC5iaW5kKHRoaXMpKTtcblxuICAgICRjb3JlQ2hlY2tib3hlcy5vbignY2hhbmdlJywgdGhpcy5zdWJDaG9pY2VzQ2hhbmdlZC5iaW5kKHRoaXMpKTtcbiAgICAkdGhlbWVzU2VsZWN0Lm9uKCdjaGFuZ2UnLCB0aGlzLnN1YkNob2ljZXNDaGFuZ2VkLmJpbmQodGhpcykpO1xuICAgICRtb2R1bGVzU2VsZWN0Lm9uKCdjaGFuZ2UnLCB0aGlzLnN1YkNob2ljZXNDaGFuZ2VkLmJpbmQodGhpcykpO1xuXG4gICAgdGhpcy5jaGVjaygkY29yZVR5cGUpO1xuICB9XG5cbiAgY29yZVR5cGVDaGFuZ2VkKCkge1xuICAgIGlmICghJGNvcmVUeXBlLmlzKCc6Y2hlY2tlZCcpKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgJGNvcmVUeXBlLnByb3AoJ2Rpc2FibGVkJywgZmFsc2UpO1xuICAgIHRoaXMudW5jaGVjaygkdGhlbWVzVHlwZSwgJG1vZHVsZXNUeXBlKTtcbiAgICB0aGlzLnNob3coJGNvcmVWYWx1ZXMpO1xuICAgIHRoaXMuaGlkZSgkdGhlbWVzVmFsdWVzLCAkbW9kdWxlc1ZhbHVlcyk7XG4gICAgdGhpcy5zdWJDaG9pY2VzQ2hhbmdlZCgpO1xuICB9XG5cbiAgdGhlbWVzVHlwZUNoYW5nZWQoKSB7XG4gICAgaWYgKCEkdGhlbWVzVHlwZS5pcygnOmNoZWNrZWQnKSkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgICR0aGVtZXNUeXBlLnByb3AoJ2Rpc2FibGVkJywgZmFsc2UpO1xuICAgIHRoaXMudW5jaGVjaygkY29yZVR5cGUsICRtb2R1bGVzVHlwZSk7XG4gICAgdGhpcy5zaG93KCR0aGVtZXNWYWx1ZXMpO1xuICAgIHRoaXMuaGlkZSgkY29yZVZhbHVlcywgJG1vZHVsZXNWYWx1ZXMpO1xuICAgIHRoaXMuc3ViQ2hvaWNlc0NoYW5nZWQoKTtcbiAgfVxuXG4gIG1vZHVsZXNUeXBlQ2hhbmdlZCgpIHtcbiAgICBpZiAoISRtb2R1bGVzVHlwZS5pcygnOmNoZWNrZWQnKSkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgICRtb2R1bGVzVmFsdWVzLnByb3AoJ2Rpc2FibGVkJywgZmFsc2UpO1xuICAgIHRoaXMudW5jaGVjaygkdGhlbWVzVHlwZSwgJGNvcmVUeXBlKTtcbiAgICB0aGlzLnNob3coJG1vZHVsZXNWYWx1ZXMpO1xuICAgIHRoaXMuaGlkZSgkdGhlbWVzVmFsdWVzLCAkY29yZVZhbHVlcyk7XG4gICAgdGhpcy5zdWJDaG9pY2VzQ2hhbmdlZCgpO1xuICB9XG5cbiAgc3ViQ2hvaWNlc0NoYW5nZWQoKSB7XG4gICAgaWYgKFxuICAgICAgKCRjb3JlVHlwZS5wcm9wKCdjaGVja2VkJykgJiYgJGNvcmVDaGVja2JveGVzLmZpbmQoJzpjaGVja2VkJykuc2l6ZSgpID4gMClcbiAgICAgIHx8ICgkdGhlbWVzVHlwZS5wcm9wKCdjaGVja2VkJykgJiYgJHRoZW1lc1NlbGVjdC52YWwoKSAhPT0gbnVsbClcbiAgICAgIHx8ICgkbW9kdWxlc1R5cGUucHJvcCgnY2hlY2tlZCcpICYmICRtb2R1bGVzU2VsZWN0LnZhbCgpICE9PSBudWxsKVxuICAgICkge1xuICAgICAgJGV4cG9ydEJ1dHRvbi5wcm9wKCdkaXNhYmxlZCcsIGZhbHNlKTtcblxuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgICRleHBvcnRCdXR0b24ucHJvcCgnZGlzYWJsZWQnLCB0cnVlKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBNYWtlIGFsbCBnaXZlbiBzZWxlY3RvcnMgaGlkZGVuXG4gICAqXG4gICAqIEBwYXJhbSAkc2VsZWN0b3JzXG4gICAqIEBwcml2YXRlXG4gICAqL1xuICBoaWRlKC4uLiRzZWxlY3RvcnMpIHtcbiAgICBPYmplY3QudmFsdWVzKCRzZWxlY3RvcnMpLmZvckVhY2goKGVsKSA9PiB7XG4gICAgICBlbC5hZGRDbGFzcygnZC1ub25lJyk7XG4gICAgICBlbC5maW5kKCdzZWxlY3QsIGlucHV0JykucHJvcCgnZGlzYWJsZWQnLCAnZGlzYWJsZWQnKTtcbiAgICB9KTtcbiAgfVxuXG4gIC8qKlxuICAgKiBNYWtlIGFsbCBnaXZlbiBzZWxlY3RvcnMgdmlzaWJsZVxuICAgKlxuICAgKiBAcGFyYW0gJHNlbGVjdG9yc1xuICAgKiBAcHJpdmF0ZVxuICAgKi9cbiAgc2hvdyguLi4kc2VsZWN0b3JzKSB7XG4gICAgT2JqZWN0LnZhbHVlcygkc2VsZWN0b3JzKS5mb3JFYWNoKChlbCkgPT4ge1xuICAgICAgZWwucmVtb3ZlQ2xhc3MoJ2Qtbm9uZScpO1xuICAgICAgZWwuZmluZCgnc2VsZWN0LCBpbnB1dCcpLnByb3AoJ2Rpc2FibGVkJywgZmFsc2UpO1xuICAgIH0pO1xuICB9XG5cbiAgLyoqXG4gICAqIE1ha2UgYWxsIGdpdmVuIHNlbGVjdG9ycyB1bmNoZWNrZWRcbiAgICpcbiAgICogQHBhcmFtICRzZWxlY3RvcnNcbiAgICogQHByaXZhdGVcbiAgICovXG4gIHVuY2hlY2soLi4uJHNlbGVjdG9ycykge1xuICAgIE9iamVjdC52YWx1ZXMoJHNlbGVjdG9ycykuZm9yRWFjaCgoZWwpID0+IHtcbiAgICAgIGVsLnByb3AoJ2NoZWNrZWQnLCBmYWxzZSk7XG4gICAgfSk7XG4gIH1cblxuICAvKipcbiAgICogTWFrZSBhbGwgZ2l2ZW4gc2VsZWN0b3JzIGNoZWNrZWRcbiAgICpcbiAgICogQHBhcmFtICRzZWxlY3RvcnNcbiAgICogQHByaXZhdGVcbiAgICovXG4gIGNoZWNrKC4uLiRzZWxlY3RvcnMpIHtcbiAgICBPYmplY3QudmFsdWVzKCRzZWxlY3RvcnMpLmZvckVhY2goKGVsKSA9PiB7XG4gICAgICBlbC5wcm9wKCdjaGVja2VkJywgdHJ1ZSk7XG4gICAgfSk7XG4gIH1cbn1cbiIsIi8qKlxuICogQ29weXJpZ2h0IHNpbmNlIDIwMDcgUHJlc3RhU2hvcCBTQSBhbmQgQ29udHJpYnV0b3JzXG4gKiBQcmVzdGFTaG9wIGlzIGFuIEludGVybmF0aW9uYWwgUmVnaXN0ZXJlZCBUcmFkZW1hcmsgJiBQcm9wZXJ0eSBvZiBQcmVzdGFTaG9wIFNBXG4gKlxuICogTk9USUNFIE9GIExJQ0VOU0VcbiAqXG4gKiBUaGlzIHNvdXJjZSBmaWxlIGlzIHN1YmplY3QgdG8gdGhlIE9wZW4gU29mdHdhcmUgTGljZW5zZSAoT1NMIDMuMClcbiAqIHRoYXQgaXMgYnVuZGxlZCB3aXRoIHRoaXMgcGFja2FnZSBpbiB0aGUgZmlsZSBMSUNFTlNFLm1kLlxuICogSXQgaXMgYWxzbyBhdmFpbGFibGUgdGhyb3VnaCB0aGUgd29ybGQtd2lkZS13ZWIgYXQgdGhpcyBVUkw6XG4gKiBodHRwczovL29wZW5zb3VyY2Uub3JnL2xpY2Vuc2VzL09TTC0zLjBcbiAqIElmIHlvdSBkaWQgbm90IHJlY2VpdmUgYSBjb3B5IG9mIHRoZSBsaWNlbnNlIGFuZCBhcmUgdW5hYmxlIHRvXG4gKiBvYnRhaW4gaXQgdGhyb3VnaCB0aGUgd29ybGQtd2lkZS13ZWIsIHBsZWFzZSBzZW5kIGFuIGVtYWlsXG4gKiB0byBsaWNlbnNlQHByZXN0YXNob3AuY29tIHNvIHdlIGNhbiBzZW5kIHlvdSBhIGNvcHkgaW1tZWRpYXRlbHkuXG4gKlxuICogRElTQ0xBSU1FUlxuICpcbiAqIERvIG5vdCBlZGl0IG9yIGFkZCB0byB0aGlzIGZpbGUgaWYgeW91IHdpc2ggdG8gdXBncmFkZSBQcmVzdGFTaG9wIHRvIG5ld2VyXG4gKiB2ZXJzaW9ucyBpbiB0aGUgZnV0dXJlLiBJZiB5b3Ugd2lzaCB0byBjdXN0b21pemUgUHJlc3RhU2hvcCBmb3IgeW91clxuICogbmVlZHMgcGxlYXNlIHJlZmVyIHRvIGh0dHBzOi8vZGV2ZG9jcy5wcmVzdGFzaG9wLmNvbS8gZm9yIG1vcmUgaW5mb3JtYXRpb24uXG4gKlxuICogQGF1dGhvciAgICBQcmVzdGFTaG9wIFNBIGFuZCBDb250cmlidXRvcnMgPGNvbnRhY3RAcHJlc3Rhc2hvcC5jb20+XG4gKiBAY29weXJpZ2h0IFNpbmNlIDIwMDcgUHJlc3RhU2hvcCBTQSBhbmQgQ29udHJpYnV0b3JzXG4gKiBAbGljZW5zZSAgIGh0dHBzOi8vb3BlbnNvdXJjZS5vcmcvbGljZW5zZXMvT1NMLTMuMCBPcGVuIFNvZnR3YXJlIExpY2Vuc2UgKE9TTCAzLjApXG4gKi9cblxuXG5pbXBvcnQgVHJhbnNsYXRpb25TZXR0aW5nc01hcCBmcm9tICcuL1RyYW5zbGF0aW9uU2V0dGluZ3NNYXAnO1xuXG5jb25zdCB7JH0gPSB3aW5kb3c7XG5cbi8qKlxuICogQmFjayBvZmZpY2UgdHJhbnNsYXRpb25zIHR5cGVcbiAqXG4gKiBAdHlwZSB7c3RyaW5nfVxuICovXG5jb25zdCBiYWNrID0gJ2JhY2snO1xuXG4vKipcbiAqIE1vZHVsZXMgdHJhbnNsYXRpb25zIHR5cGVcbiAqIEB0eXBlIHtzdHJpbmd9XG4gKi9cbmNvbnN0IHRoZW1lcyA9ICd0aGVtZXMnO1xuXG4vKipcbiAqIE1vZHVsZXMgdHJhbnNsYXRpb25zIHR5cGVcbiAqIEB0eXBlIHtzdHJpbmd9XG4gKi9cbmNvbnN0IG1vZHVsZXMgPSAnbW9kdWxlcyc7XG5cbi8qKlxuICogTWFpbHMgdHJhbnNsYXRpb25zIHR5cGVcbiAqIEB0eXBlIHtzdHJpbmd9XG4gKi9cbmNvbnN0IG1haWxzID0gJ21haWxzJztcblxuLyoqXG4gKiBPdGhlciB0cmFuc2xhdGlvbnMgdHlwZVxuICogQHR5cGUge3N0cmluZ31cbiAqL1xuY29uc3Qgb3RoZXJzID0gJ290aGVycyc7XG5cbi8qKlxuICogRW1haWwgYm9keSB0cmFuc2xhdGlvbnMgdHlwZVxuICogQHR5cGUge3N0cmluZ31cbiAqL1xuY29uc3QgZW1haWxDb250ZW50Qm9keSA9ICdib2R5JztcblxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgRm9ybUZpZWxkVG9nZ2xlIHtcbiAgY29uc3RydWN0b3IoKSB7XG4gICAgJChUcmFuc2xhdGlvblNldHRpbmdzTWFwLnRyYW5zbGF0aW9uVHlwZSkub24oJ2NoYW5nZScsIHRoaXMudG9nZ2xlRmllbGRzLmJpbmQodGhpcykpO1xuICAgICQoVHJhbnNsYXRpb25TZXR0aW5nc01hcC5lbWFpbENvbnRlbnRUeXBlKS5vbignY2hhbmdlJywgdGhpcy50b2dnbGVFbWFpbEZpZWxkcy5iaW5kKHRoaXMpKTtcblxuICAgIHRoaXMudG9nZ2xlRmllbGRzKCk7XG4gIH1cblxuICAvKipcbiAgICogVG9nZ2xlIGRlcGVuZGFudCB0cmFuc2xhdGlvbnMgZmllbGRzLCBiYXNlZCBvbiBzZWxlY3RlZCB0cmFuc2xhdGlvbiB0eXBlXG4gICAqL1xuICB0b2dnbGVGaWVsZHMoKSB7XG4gICAgY29uc3Qgc2VsZWN0ZWRPcHRpb24gPSAkKFRyYW5zbGF0aW9uU2V0dGluZ3NNYXAudHJhbnNsYXRpb25UeXBlKS52YWwoKTtcbiAgICBjb25zdCAkbW9kdWxlc0Zvcm1Hcm91cCA9ICQoVHJhbnNsYXRpb25TZXR0aW5nc01hcC5tb2R1bGVzRm9ybUdyb3VwKTtcbiAgICBjb25zdCAkZW1haWxGb3JtR3JvdXAgPSAkKFRyYW5zbGF0aW9uU2V0dGluZ3NNYXAuZW1haWxGb3JtR3JvdXApO1xuICAgIGNvbnN0ICR0aGVtZXNGb3JtR3JvdXAgPSAkKFRyYW5zbGF0aW9uU2V0dGluZ3NNYXAudGhlbWVzRm9ybUdyb3VwKTtcbiAgICBjb25zdCAkZGVmYXVsdFRoZW1lT3B0aW9uID0gJHRoZW1lc0Zvcm1Hcm91cC5maW5kKFRyYW5zbGF0aW9uU2V0dGluZ3NNYXAuZGVmYXVsdFRoZW1lT3B0aW9uKTtcblxuICAgIHN3aXRjaCAoc2VsZWN0ZWRPcHRpb24pIHtcbiAgICAgIGNhc2UgYmFjazpcbiAgICAgIGNhc2Ugb3RoZXJzOlxuICAgICAgICB0aGlzLmhpZGUoJG1vZHVsZXNGb3JtR3JvdXAsICRlbWFpbEZvcm1Hcm91cCwgJHRoZW1lc0Zvcm1Hcm91cCk7XG4gICAgICAgIGJyZWFrO1xuXG4gICAgICBjYXNlIHRoZW1lczpcbiAgICAgICAgdGhpcy5zaG93KCR0aGVtZXNGb3JtR3JvdXApO1xuICAgICAgICB0aGlzLmhpZGUoJG1vZHVsZXNGb3JtR3JvdXAsICRlbWFpbEZvcm1Hcm91cCwgJGRlZmF1bHRUaGVtZU9wdGlvbik7XG4gICAgICAgIGJyZWFrO1xuXG4gICAgICBjYXNlIG1vZHVsZXM6XG4gICAgICAgIHRoaXMuaGlkZSgkZW1haWxGb3JtR3JvdXAsICR0aGVtZXNGb3JtR3JvdXApO1xuICAgICAgICB0aGlzLnNob3coJG1vZHVsZXNGb3JtR3JvdXApO1xuICAgICAgICBicmVhaztcblxuICAgICAgY2FzZSBtYWlsczpcbiAgICAgICAgdGhpcy5oaWRlKCRtb2R1bGVzRm9ybUdyb3VwLCAkdGhlbWVzRm9ybUdyb3VwKTtcbiAgICAgICAgdGhpcy5zaG93KCRlbWFpbEZvcm1Hcm91cCk7XG4gICAgICAgIGJyZWFrO1xuXG4gICAgICBkZWZhdWx0OlxuICAgICAgICBicmVhaztcbiAgICB9XG5cbiAgICB0aGlzLnRvZ2dsZUVtYWlsRmllbGRzKCk7XG4gIH1cblxuICAvKipcbiAgICogVG9nZ2xlcyBmaWVsZHMsIHdoaWNoIGFyZSByZWxhdGVkIHRvIGVtYWlsIHRyYW5zbGF0aW9uc1xuICAgKi9cbiAgdG9nZ2xlRW1haWxGaWVsZHMoKSB7XG4gICAgaWYgKCQoVHJhbnNsYXRpb25TZXR0aW5nc01hcC50cmFuc2xhdGlvblR5cGUpLnZhbCgpICE9PSBtYWlscykge1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIGNvbnN0IHNlbGVjdGVkRW1haWxDb250ZW50VHlwZSA9ICQoVHJhbnNsYXRpb25TZXR0aW5nc01hcC5lbWFpbEZvcm1Hcm91cCkuZmluZCgnc2VsZWN0JykudmFsKCk7XG4gICAgY29uc3QgJHRoZW1lc0Zvcm1Hcm91cCA9ICQoVHJhbnNsYXRpb25TZXR0aW5nc01hcC50aGVtZXNGb3JtR3JvdXApO1xuICAgIGNvbnN0ICRub1RoZW1lT3B0aW9uID0gJHRoZW1lc0Zvcm1Hcm91cC5maW5kKFRyYW5zbGF0aW9uU2V0dGluZ3NNYXAubm9UaGVtZU9wdGlvbik7XG4gICAgY29uc3QgJGRlZmF1bHRUaGVtZU9wdGlvbiA9ICR0aGVtZXNGb3JtR3JvdXAuZmluZChUcmFuc2xhdGlvblNldHRpbmdzTWFwLmRlZmF1bHRUaGVtZU9wdGlvbik7XG5cbiAgICBpZiAoc2VsZWN0ZWRFbWFpbENvbnRlbnRUeXBlID09PSBlbWFpbENvbnRlbnRCb2R5KSB7XG4gICAgICAkbm9UaGVtZU9wdGlvbi5wcm9wKCdzZWxlY3RlZCcsIHRydWUpO1xuICAgICAgdGhpcy5zaG93KCRub1RoZW1lT3B0aW9uLCAkdGhlbWVzRm9ybUdyb3VwLCAkZGVmYXVsdFRoZW1lT3B0aW9uKTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5oaWRlKCRub1RoZW1lT3B0aW9uLCAkdGhlbWVzRm9ybUdyb3VwLCAkZGVmYXVsdFRoZW1lT3B0aW9uKTtcbiAgICB9XG4gIH1cblxuICAvKipcbiAgICogTWFrZSBhbGwgZ2l2ZW4gc2VsZWN0b3JzIGhpZGRlblxuICAgKlxuICAgKiBAcGFyYW0gJHNlbGVjdG9yc1xuICAgKiBAcHJpdmF0ZVxuICAgKi9cbiAgaGlkZSguLi4kc2VsZWN0b3JzKSB7XG4gICAgT2JqZWN0LnZhbHVlcygkc2VsZWN0b3JzKS5mb3JFYWNoKChlbCkgPT4ge1xuICAgICAgZWwuYWRkQ2xhc3MoJ2Qtbm9uZScpO1xuICAgICAgZWwuZmluZCgnc2VsZWN0JykucHJvcCgnZGlzYWJsZWQnLCAnZGlzYWJsZWQnKTtcbiAgICB9KTtcbiAgfVxuXG4gIC8qKlxuICAgKiBNYWtlIGFsbCBnaXZlbiBzZWxlY3RvcnMgdmlzaWJsZVxuICAgKlxuICAgKiBAcGFyYW0gJHNlbGVjdG9yc1xuICAgKiBAcHJpdmF0ZVxuICAgKi9cbiAgc2hvdyguLi4kc2VsZWN0b3JzKSB7XG4gICAgT2JqZWN0LnZhbHVlcygkc2VsZWN0b3JzKS5mb3JFYWNoKChlbCkgPT4ge1xuICAgICAgZWwucmVtb3ZlQ2xhc3MoJ2Qtbm9uZScpO1xuICAgICAgZWwuZmluZCgnc2VsZWN0JykucHJvcCgnZGlzYWJsZWQnLCBmYWxzZSk7XG4gICAgfSk7XG4gIH1cbn1cbiIsIi8qKlxuICogQ29weXJpZ2h0IHNpbmNlIDIwMDcgUHJlc3RhU2hvcCBTQSBhbmQgQ29udHJpYnV0b3JzXG4gKiBQcmVzdGFTaG9wIGlzIGFuIEludGVybmF0aW9uYWwgUmVnaXN0ZXJlZCBUcmFkZW1hcmsgJiBQcm9wZXJ0eSBvZiBQcmVzdGFTaG9wIFNBXG4gKlxuICogTk9USUNFIE9GIExJQ0VOU0VcbiAqXG4gKiBUaGlzIHNvdXJjZSBmaWxlIGlzIHN1YmplY3QgdG8gdGhlIE9wZW4gU29mdHdhcmUgTGljZW5zZSAoT1NMIDMuMClcbiAqIHRoYXQgaXMgYnVuZGxlZCB3aXRoIHRoaXMgcGFja2FnZSBpbiB0aGUgZmlsZSBMSUNFTlNFLm1kLlxuICogSXQgaXMgYWxzbyBhdmFpbGFibGUgdGhyb3VnaCB0aGUgd29ybGQtd2lkZS13ZWIgYXQgdGhpcyBVUkw6XG4gKiBodHRwczovL29wZW5zb3VyY2Uub3JnL2xpY2Vuc2VzL09TTC0zLjBcbiAqIElmIHlvdSBkaWQgbm90IHJlY2VpdmUgYSBjb3B5IG9mIHRoZSBsaWNlbnNlIGFuZCBhcmUgdW5hYmxlIHRvXG4gKiBvYnRhaW4gaXQgdGhyb3VnaCB0aGUgd29ybGQtd2lkZS13ZWIsIHBsZWFzZSBzZW5kIGFuIGVtYWlsXG4gKiB0byBsaWNlbnNlQHByZXN0YXNob3AuY29tIHNvIHdlIGNhbiBzZW5kIHlvdSBhIGNvcHkgaW1tZWRpYXRlbHkuXG4gKlxuICogRElTQ0xBSU1FUlxuICpcbiAqIERvIG5vdCBlZGl0IG9yIGFkZCB0byB0aGlzIGZpbGUgaWYgeW91IHdpc2ggdG8gdXBncmFkZSBQcmVzdGFTaG9wIHRvIG5ld2VyXG4gKiB2ZXJzaW9ucyBpbiB0aGUgZnV0dXJlLiBJZiB5b3Ugd2lzaCB0byBjdXN0b21pemUgUHJlc3RhU2hvcCBmb3IgeW91clxuICogbmVlZHMgcGxlYXNlIHJlZmVyIHRvIGh0dHBzOi8vZGV2ZG9jcy5wcmVzdGFzaG9wLmNvbS8gZm9yIG1vcmUgaW5mb3JtYXRpb24uXG4gKlxuICogQGF1dGhvciAgICBQcmVzdGFTaG9wIFNBIGFuZCBDb250cmlidXRvcnMgPGNvbnRhY3RAcHJlc3Rhc2hvcC5jb20+XG4gKiBAY29weXJpZ2h0IFNpbmNlIDIwMDcgUHJlc3RhU2hvcCBTQSBhbmQgQ29udHJpYnV0b3JzXG4gKiBAbGljZW5zZSAgIGh0dHBzOi8vb3BlbnNvdXJjZS5vcmcvbGljZW5zZXMvT1NMLTMuMCBPcGVuIFNvZnR3YXJlIExpY2Vuc2UgKE9TTCAzLjApXG4gKi9cblxuZXhwb3J0IGRlZmF1bHQge1xuICB0cmFuc2xhdGlvblR5cGU6ICcuanMtdHJhbnNsYXRpb24tdHlwZScsXG4gIGVtYWlsQ29udGVudFR5cGU6ICcuanMtZW1haWwtY29udGVudC10eXBlJyxcbiAgZW1haWxGb3JtR3JvdXA6ICcuanMtZW1haWwtZm9ybS1ncm91cCcsXG4gIG1vZHVsZXNGb3JtR3JvdXA6ICcuanMtbW9kdWxlLWZvcm0tZ3JvdXAnLFxuICB0aGVtZXNGb3JtR3JvdXA6ICcuanMtdGhlbWUtZm9ybS1ncm91cCcsXG4gIGRlZmF1bHRUaGVtZU9wdGlvbjogJy5qcy1kZWZhdWx0LXRoZW1lJyxcbiAgbm9UaGVtZU9wdGlvbjogJy5qcy1uby10aGVtZScsXG4gIGV4cG9ydENvcmVUeXBlOiAnI2Zvcm1fY29yZV9zZWxlY3RvcnNfY29yZV90eXBlJyxcbiAgZXhwb3J0Q29yZVZhbHVlczogJyNmb3JtX2NvcmVfc2VsZWN0b3JzX3NlbGVjdGVkX3ZhbHVlJyxcbiAgZXhwb3J0VGhlbWVzVHlwZTogJyNmb3JtX3RoZW1lc19zZWxlY3RvcnNfdGhlbWVzX3R5cGUnLFxuICBleHBvcnRUaGVtZXNWYWx1ZXM6ICcjZm9ybV90aGVtZXNfc2VsZWN0b3JzX3NlbGVjdGVkX3ZhbHVlJyxcbiAgZXhwb3J0TW9kdWxlc1R5cGU6ICcjZm9ybV9tb2R1bGVzX3NlbGVjdG9yc19tb2R1bGVzX3R5cGUnLFxuICBleHBvcnRNb2R1bGVzVmFsdWVzOiAnI2Zvcm1fbW9kdWxlc19zZWxlY3RvcnNfc2VsZWN0ZWRfdmFsdWUnLFxuICBleHBvcnRMYW5ndWFnZUJ1dHRvbjogJyNmb3JtLWV4cG9ydC1sYW5ndWFnZS1idXR0b24nLFxufTtcbiIsIi8qKlxuICogQ29weXJpZ2h0IHNpbmNlIDIwMDcgUHJlc3RhU2hvcCBTQSBhbmQgQ29udHJpYnV0b3JzXG4gKiBQcmVzdGFTaG9wIGlzIGFuIEludGVybmF0aW9uYWwgUmVnaXN0ZXJlZCBUcmFkZW1hcmsgJiBQcm9wZXJ0eSBvZiBQcmVzdGFTaG9wIFNBXG4gKlxuICogTk9USUNFIE9GIExJQ0VOU0VcbiAqXG4gKiBUaGlzIHNvdXJjZSBmaWxlIGlzIHN1YmplY3QgdG8gdGhlIE9wZW4gU29mdHdhcmUgTGljZW5zZSAoT1NMIDMuMClcbiAqIHRoYXQgaXMgYnVuZGxlZCB3aXRoIHRoaXMgcGFja2FnZSBpbiB0aGUgZmlsZSBMSUNFTlNFLm1kLlxuICogSXQgaXMgYWxzbyBhdmFpbGFibGUgdGhyb3VnaCB0aGUgd29ybGQtd2lkZS13ZWIgYXQgdGhpcyBVUkw6XG4gKiBodHRwczovL29wZW5zb3VyY2Uub3JnL2xpY2Vuc2VzL09TTC0zLjBcbiAqIElmIHlvdSBkaWQgbm90IHJlY2VpdmUgYSBjb3B5IG9mIHRoZSBsaWNlbnNlIGFuZCBhcmUgdW5hYmxlIHRvXG4gKiBvYnRhaW4gaXQgdGhyb3VnaCB0aGUgd29ybGQtd2lkZS13ZWIsIHBsZWFzZSBzZW5kIGFuIGVtYWlsXG4gKiB0byBsaWNlbnNlQHByZXN0YXNob3AuY29tIHNvIHdlIGNhbiBzZW5kIHlvdSBhIGNvcHkgaW1tZWRpYXRlbHkuXG4gKlxuICogRElTQ0xBSU1FUlxuICpcbiAqIERvIG5vdCBlZGl0IG9yIGFkZCB0byB0aGlzIGZpbGUgaWYgeW91IHdpc2ggdG8gdXBncmFkZSBQcmVzdGFTaG9wIHRvIG5ld2VyXG4gKiB2ZXJzaW9ucyBpbiB0aGUgZnV0dXJlLiBJZiB5b3Ugd2lzaCB0byBjdXN0b21pemUgUHJlc3RhU2hvcCBmb3IgeW91clxuICogbmVlZHMgcGxlYXNlIHJlZmVyIHRvIGh0dHBzOi8vZGV2ZG9jcy5wcmVzdGFzaG9wLmNvbS8gZm9yIG1vcmUgaW5mb3JtYXRpb24uXG4gKlxuICogQGF1dGhvciAgICBQcmVzdGFTaG9wIFNBIGFuZCBDb250cmlidXRvcnMgPGNvbnRhY3RAcHJlc3Rhc2hvcC5jb20+XG4gKiBAY29weXJpZ2h0IFNpbmNlIDIwMDcgUHJlc3RhU2hvcCBTQSBhbmQgQ29udHJpYnV0b3JzXG4gKiBAbGljZW5zZSAgIGh0dHBzOi8vb3BlbnNvdXJjZS5vcmcvbGljZW5zZXMvT1NMLTMuMCBPcGVuIFNvZnR3YXJlIExpY2Vuc2UgKE9TTCAzLjApXG4gKi9cblxuaW1wb3J0IEZvcm1GaWVsZFRvZ2dsZSBmcm9tICcuL0Zvcm1GaWVsZFRvZ2dsZSc7XG5pbXBvcnQgRXhwb3J0Rm9ybUZpZWxkVG9nZ2xlIGZyb20gJy4vRXhwb3J0Rm9ybUZpZWxkVG9nZ2xlJztcblxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgVHJhbnNsYXRpb25TZXR0aW5nc1BhZ2Uge1xuICBjb25zdHJ1Y3RvcigpIHtcbiAgICBuZXcgRm9ybUZpZWxkVG9nZ2xlKCk7XG4gICAgbmV3IEV4cG9ydEZvcm1GaWVsZFRvZ2dsZSgpO1xuICB9XG59XG4iLCJtb2R1bGUuZXhwb3J0cyA9IHsgXCJkZWZhdWx0XCI6IHJlcXVpcmUoXCJjb3JlLWpzL2xpYnJhcnkvZm4vb2JqZWN0L2RlZmluZS1wcm9wZXJ0eVwiKSwgX19lc01vZHVsZTogdHJ1ZSB9OyIsIm1vZHVsZS5leHBvcnRzID0geyBcImRlZmF1bHRcIjogcmVxdWlyZShcImNvcmUtanMvbGlicmFyeS9mbi9vYmplY3QvdmFsdWVzXCIpLCBfX2VzTW9kdWxlOiB0cnVlIH07IiwiXCJ1c2Ugc3RyaWN0XCI7XG5cbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IGZ1bmN0aW9uIChpbnN0YW5jZSwgQ29uc3RydWN0b3IpIHtcbiAgaWYgKCEoaW5zdGFuY2UgaW5zdGFuY2VvZiBDb25zdHJ1Y3RvcikpIHtcbiAgICB0aHJvdyBuZXcgVHlwZUVycm9yKFwiQ2Fubm90IGNhbGwgYSBjbGFzcyBhcyBhIGZ1bmN0aW9uXCIpO1xuICB9XG59OyIsIlwidXNlIHN0cmljdFwiO1xuXG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xuXG52YXIgX2RlZmluZVByb3BlcnR5ID0gcmVxdWlyZShcIi4uL2NvcmUtanMvb2JqZWN0L2RlZmluZS1wcm9wZXJ0eVwiKTtcblxudmFyIF9kZWZpbmVQcm9wZXJ0eTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9kZWZpbmVQcm9wZXJ0eSk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbmV4cG9ydHMuZGVmYXVsdCA9IGZ1bmN0aW9uICgpIHtcbiAgZnVuY3Rpb24gZGVmaW5lUHJvcGVydGllcyh0YXJnZXQsIHByb3BzKSB7XG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBwcm9wcy5sZW5ndGg7IGkrKykge1xuICAgICAgdmFyIGRlc2NyaXB0b3IgPSBwcm9wc1tpXTtcbiAgICAgIGRlc2NyaXB0b3IuZW51bWVyYWJsZSA9IGRlc2NyaXB0b3IuZW51bWVyYWJsZSB8fCBmYWxzZTtcbiAgICAgIGRlc2NyaXB0b3IuY29uZmlndXJhYmxlID0gdHJ1ZTtcbiAgICAgIGlmIChcInZhbHVlXCIgaW4gZGVzY3JpcHRvcikgZGVzY3JpcHRvci53cml0YWJsZSA9IHRydWU7XG4gICAgICAoMCwgX2RlZmluZVByb3BlcnR5Mi5kZWZhdWx0KSh0YXJnZXQsIGRlc2NyaXB0b3Iua2V5LCBkZXNjcmlwdG9yKTtcbiAgICB9XG4gIH1cblxuICByZXR1cm4gZnVuY3Rpb24gKENvbnN0cnVjdG9yLCBwcm90b1Byb3BzLCBzdGF0aWNQcm9wcykge1xuICAgIGlmIChwcm90b1Byb3BzKSBkZWZpbmVQcm9wZXJ0aWVzKENvbnN0cnVjdG9yLnByb3RvdHlwZSwgcHJvdG9Qcm9wcyk7XG4gICAgaWYgKHN0YXRpY1Byb3BzKSBkZWZpbmVQcm9wZXJ0aWVzKENvbnN0cnVjdG9yLCBzdGF0aWNQcm9wcyk7XG4gICAgcmV0dXJuIENvbnN0cnVjdG9yO1xuICB9O1xufSgpOyIsInJlcXVpcmUoJy4uLy4uL21vZHVsZXMvZXM2Lm9iamVjdC5kZWZpbmUtcHJvcGVydHknKTtcbnZhciAkT2JqZWN0ID0gcmVxdWlyZSgnLi4vLi4vbW9kdWxlcy9fY29yZScpLk9iamVjdDtcbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gZGVmaW5lUHJvcGVydHkoaXQsIGtleSwgZGVzYykge1xuICByZXR1cm4gJE9iamVjdC5kZWZpbmVQcm9wZXJ0eShpdCwga2V5LCBkZXNjKTtcbn07XG4iLCJyZXF1aXJlKCcuLi8uLi9tb2R1bGVzL2VzNy5vYmplY3QudmFsdWVzJyk7XG5tb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoJy4uLy4uL21vZHVsZXMvX2NvcmUnKS5PYmplY3QudmFsdWVzO1xuIiwibW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAoaXQpIHtcbiAgaWYgKHR5cGVvZiBpdCAhPSAnZnVuY3Rpb24nKSB0aHJvdyBUeXBlRXJyb3IoaXQgKyAnIGlzIG5vdCBhIGZ1bmN0aW9uIScpO1xuICByZXR1cm4gaXQ7XG59O1xuIiwidmFyIGlzT2JqZWN0ID0gcmVxdWlyZSgnLi9faXMtb2JqZWN0Jyk7XG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIChpdCkge1xuICBpZiAoIWlzT2JqZWN0KGl0KSkgdGhyb3cgVHlwZUVycm9yKGl0ICsgJyBpcyBub3QgYW4gb2JqZWN0IScpO1xuICByZXR1cm4gaXQ7XG59O1xuIiwiLy8gZmFsc2UgLT4gQXJyYXkjaW5kZXhPZlxuLy8gdHJ1ZSAgLT4gQXJyYXkjaW5jbHVkZXNcbnZhciB0b0lPYmplY3QgPSByZXF1aXJlKCcuL190by1pb2JqZWN0Jyk7XG52YXIgdG9MZW5ndGggPSByZXF1aXJlKCcuL190by1sZW5ndGgnKTtcbnZhciB0b0Fic29sdXRlSW5kZXggPSByZXF1aXJlKCcuL190by1hYnNvbHV0ZS1pbmRleCcpO1xubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAoSVNfSU5DTFVERVMpIHtcbiAgcmV0dXJuIGZ1bmN0aW9uICgkdGhpcywgZWwsIGZyb21JbmRleCkge1xuICAgIHZhciBPID0gdG9JT2JqZWN0KCR0aGlzKTtcbiAgICB2YXIgbGVuZ3RoID0gdG9MZW5ndGgoTy5sZW5ndGgpO1xuICAgIHZhciBpbmRleCA9IHRvQWJzb2x1dGVJbmRleChmcm9tSW5kZXgsIGxlbmd0aCk7XG4gICAgdmFyIHZhbHVlO1xuICAgIC8vIEFycmF5I2luY2x1ZGVzIHVzZXMgU2FtZVZhbHVlWmVybyBlcXVhbGl0eSBhbGdvcml0aG1cbiAgICAvLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmUgbm8tc2VsZi1jb21wYXJlXG4gICAgaWYgKElTX0lOQ0xVREVTICYmIGVsICE9IGVsKSB3aGlsZSAobGVuZ3RoID4gaW5kZXgpIHtcbiAgICAgIHZhbHVlID0gT1tpbmRleCsrXTtcbiAgICAgIC8vIGVzbGludC1kaXNhYmxlLW5leHQtbGluZSBuby1zZWxmLWNvbXBhcmVcbiAgICAgIGlmICh2YWx1ZSAhPSB2YWx1ZSkgcmV0dXJuIHRydWU7XG4gICAgLy8gQXJyYXkjaW5kZXhPZiBpZ25vcmVzIGhvbGVzLCBBcnJheSNpbmNsdWRlcyAtIG5vdFxuICAgIH0gZWxzZSBmb3IgKDtsZW5ndGggPiBpbmRleDsgaW5kZXgrKykgaWYgKElTX0lOQ0xVREVTIHx8IGluZGV4IGluIE8pIHtcbiAgICAgIGlmIChPW2luZGV4XSA9PT0gZWwpIHJldHVybiBJU19JTkNMVURFUyB8fCBpbmRleCB8fCAwO1xuICAgIH0gcmV0dXJuICFJU19JTkNMVURFUyAmJiAtMTtcbiAgfTtcbn07XG4iLCJ2YXIgdG9TdHJpbmcgPSB7fS50b1N0cmluZztcblxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAoaXQpIHtcbiAgcmV0dXJuIHRvU3RyaW5nLmNhbGwoaXQpLnNsaWNlKDgsIC0xKTtcbn07XG4iLCJ2YXIgY29yZSA9IG1vZHVsZS5leHBvcnRzID0geyB2ZXJzaW9uOiAnMi42LjExJyB9O1xuaWYgKHR5cGVvZiBfX2UgPT0gJ251bWJlcicpIF9fZSA9IGNvcmU7IC8vIGVzbGludC1kaXNhYmxlLWxpbmUgbm8tdW5kZWZcbiIsIi8vIG9wdGlvbmFsIC8gc2ltcGxlIGNvbnRleHQgYmluZGluZ1xudmFyIGFGdW5jdGlvbiA9IHJlcXVpcmUoJy4vX2EtZnVuY3Rpb24nKTtcbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKGZuLCB0aGF0LCBsZW5ndGgpIHtcbiAgYUZ1bmN0aW9uKGZuKTtcbiAgaWYgKHRoYXQgPT09IHVuZGVmaW5lZCkgcmV0dXJuIGZuO1xuICBzd2l0Y2ggKGxlbmd0aCkge1xuICAgIGNhc2UgMTogcmV0dXJuIGZ1bmN0aW9uIChhKSB7XG4gICAgICByZXR1cm4gZm4uY2FsbCh0aGF0LCBhKTtcbiAgICB9O1xuICAgIGNhc2UgMjogcmV0dXJuIGZ1bmN0aW9uIChhLCBiKSB7XG4gICAgICByZXR1cm4gZm4uY2FsbCh0aGF0LCBhLCBiKTtcbiAgICB9O1xuICAgIGNhc2UgMzogcmV0dXJuIGZ1bmN0aW9uIChhLCBiLCBjKSB7XG4gICAgICByZXR1cm4gZm4uY2FsbCh0aGF0LCBhLCBiLCBjKTtcbiAgICB9O1xuICB9XG4gIHJldHVybiBmdW5jdGlvbiAoLyogLi4uYXJncyAqLykge1xuICAgIHJldHVybiBmbi5hcHBseSh0aGF0LCBhcmd1bWVudHMpO1xuICB9O1xufTtcbiIsIi8vIDcuMi4xIFJlcXVpcmVPYmplY3RDb2VyY2libGUoYXJndW1lbnQpXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIChpdCkge1xuICBpZiAoaXQgPT0gdW5kZWZpbmVkKSB0aHJvdyBUeXBlRXJyb3IoXCJDYW4ndCBjYWxsIG1ldGhvZCBvbiAgXCIgKyBpdCk7XG4gIHJldHVybiBpdDtcbn07XG4iLCIvLyBUaGFuaydzIElFOCBmb3IgaGlzIGZ1bm55IGRlZmluZVByb3BlcnR5XG5tb2R1bGUuZXhwb3J0cyA9ICFyZXF1aXJlKCcuL19mYWlscycpKGZ1bmN0aW9uICgpIHtcbiAgcmV0dXJuIE9iamVjdC5kZWZpbmVQcm9wZXJ0eSh7fSwgJ2EnLCB7IGdldDogZnVuY3Rpb24gKCkgeyByZXR1cm4gNzsgfSB9KS5hICE9IDc7XG59KTtcbiIsInZhciBpc09iamVjdCA9IHJlcXVpcmUoJy4vX2lzLW9iamVjdCcpO1xudmFyIGRvY3VtZW50ID0gcmVxdWlyZSgnLi9fZ2xvYmFsJykuZG9jdW1lbnQ7XG4vLyB0eXBlb2YgZG9jdW1lbnQuY3JlYXRlRWxlbWVudCBpcyAnb2JqZWN0JyBpbiBvbGQgSUVcbnZhciBpcyA9IGlzT2JqZWN0KGRvY3VtZW50KSAmJiBpc09iamVjdChkb2N1bWVudC5jcmVhdGVFbGVtZW50KTtcbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKGl0KSB7XG4gIHJldHVybiBpcyA/IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoaXQpIDoge307XG59O1xuIiwiLy8gSUUgOC0gZG9uJ3QgZW51bSBidWcga2V5c1xubW9kdWxlLmV4cG9ydHMgPSAoXG4gICdjb25zdHJ1Y3RvcixoYXNPd25Qcm9wZXJ0eSxpc1Byb3RvdHlwZU9mLHByb3BlcnR5SXNFbnVtZXJhYmxlLHRvTG9jYWxlU3RyaW5nLHRvU3RyaW5nLHZhbHVlT2YnXG4pLnNwbGl0KCcsJyk7XG4iLCJ2YXIgZ2xvYmFsID0gcmVxdWlyZSgnLi9fZ2xvYmFsJyk7XG52YXIgY29yZSA9IHJlcXVpcmUoJy4vX2NvcmUnKTtcbnZhciBjdHggPSByZXF1aXJlKCcuL19jdHgnKTtcbnZhciBoaWRlID0gcmVxdWlyZSgnLi9faGlkZScpO1xudmFyIGhhcyA9IHJlcXVpcmUoJy4vX2hhcycpO1xudmFyIFBST1RPVFlQRSA9ICdwcm90b3R5cGUnO1xuXG52YXIgJGV4cG9ydCA9IGZ1bmN0aW9uICh0eXBlLCBuYW1lLCBzb3VyY2UpIHtcbiAgdmFyIElTX0ZPUkNFRCA9IHR5cGUgJiAkZXhwb3J0LkY7XG4gIHZhciBJU19HTE9CQUwgPSB0eXBlICYgJGV4cG9ydC5HO1xuICB2YXIgSVNfU1RBVElDID0gdHlwZSAmICRleHBvcnQuUztcbiAgdmFyIElTX1BST1RPID0gdHlwZSAmICRleHBvcnQuUDtcbiAgdmFyIElTX0JJTkQgPSB0eXBlICYgJGV4cG9ydC5CO1xuICB2YXIgSVNfV1JBUCA9IHR5cGUgJiAkZXhwb3J0Llc7XG4gIHZhciBleHBvcnRzID0gSVNfR0xPQkFMID8gY29yZSA6IGNvcmVbbmFtZV0gfHwgKGNvcmVbbmFtZV0gPSB7fSk7XG4gIHZhciBleHBQcm90byA9IGV4cG9ydHNbUFJPVE9UWVBFXTtcbiAgdmFyIHRhcmdldCA9IElTX0dMT0JBTCA/IGdsb2JhbCA6IElTX1NUQVRJQyA/IGdsb2JhbFtuYW1lXSA6IChnbG9iYWxbbmFtZV0gfHwge30pW1BST1RPVFlQRV07XG4gIHZhciBrZXksIG93biwgb3V0O1xuICBpZiAoSVNfR0xPQkFMKSBzb3VyY2UgPSBuYW1lO1xuICBmb3IgKGtleSBpbiBzb3VyY2UpIHtcbiAgICAvLyBjb250YWlucyBpbiBuYXRpdmVcbiAgICBvd24gPSAhSVNfRk9SQ0VEICYmIHRhcmdldCAmJiB0YXJnZXRba2V5XSAhPT0gdW5kZWZpbmVkO1xuICAgIGlmIChvd24gJiYgaGFzKGV4cG9ydHMsIGtleSkpIGNvbnRpbnVlO1xuICAgIC8vIGV4cG9ydCBuYXRpdmUgb3IgcGFzc2VkXG4gICAgb3V0ID0gb3duID8gdGFyZ2V0W2tleV0gOiBzb3VyY2Vba2V5XTtcbiAgICAvLyBwcmV2ZW50IGdsb2JhbCBwb2xsdXRpb24gZm9yIG5hbWVzcGFjZXNcbiAgICBleHBvcnRzW2tleV0gPSBJU19HTE9CQUwgJiYgdHlwZW9mIHRhcmdldFtrZXldICE9ICdmdW5jdGlvbicgPyBzb3VyY2Vba2V5XVxuICAgIC8vIGJpbmQgdGltZXJzIHRvIGdsb2JhbCBmb3IgY2FsbCBmcm9tIGV4cG9ydCBjb250ZXh0XG4gICAgOiBJU19CSU5EICYmIG93biA/IGN0eChvdXQsIGdsb2JhbClcbiAgICAvLyB3cmFwIGdsb2JhbCBjb25zdHJ1Y3RvcnMgZm9yIHByZXZlbnQgY2hhbmdlIHRoZW0gaW4gbGlicmFyeVxuICAgIDogSVNfV1JBUCAmJiB0YXJnZXRba2V5XSA9PSBvdXQgPyAoZnVuY3Rpb24gKEMpIHtcbiAgICAgIHZhciBGID0gZnVuY3Rpb24gKGEsIGIsIGMpIHtcbiAgICAgICAgaWYgKHRoaXMgaW5zdGFuY2VvZiBDKSB7XG4gICAgICAgICAgc3dpdGNoIChhcmd1bWVudHMubGVuZ3RoKSB7XG4gICAgICAgICAgICBjYXNlIDA6IHJldHVybiBuZXcgQygpO1xuICAgICAgICAgICAgY2FzZSAxOiByZXR1cm4gbmV3IEMoYSk7XG4gICAgICAgICAgICBjYXNlIDI6IHJldHVybiBuZXcgQyhhLCBiKTtcbiAgICAgICAgICB9IHJldHVybiBuZXcgQyhhLCBiLCBjKTtcbiAgICAgICAgfSByZXR1cm4gQy5hcHBseSh0aGlzLCBhcmd1bWVudHMpO1xuICAgICAgfTtcbiAgICAgIEZbUFJPVE9UWVBFXSA9IENbUFJPVE9UWVBFXTtcbiAgICAgIHJldHVybiBGO1xuICAgIC8vIG1ha2Ugc3RhdGljIHZlcnNpb25zIGZvciBwcm90b3R5cGUgbWV0aG9kc1xuICAgIH0pKG91dCkgOiBJU19QUk9UTyAmJiB0eXBlb2Ygb3V0ID09ICdmdW5jdGlvbicgPyBjdHgoRnVuY3Rpb24uY2FsbCwgb3V0KSA6IG91dDtcbiAgICAvLyBleHBvcnQgcHJvdG8gbWV0aG9kcyB0byBjb3JlLiVDT05TVFJVQ1RPUiUubWV0aG9kcy4lTkFNRSVcbiAgICBpZiAoSVNfUFJPVE8pIHtcbiAgICAgIChleHBvcnRzLnZpcnR1YWwgfHwgKGV4cG9ydHMudmlydHVhbCA9IHt9KSlba2V5XSA9IG91dDtcbiAgICAgIC8vIGV4cG9ydCBwcm90byBtZXRob2RzIHRvIGNvcmUuJUNPTlNUUlVDVE9SJS5wcm90b3R5cGUuJU5BTUUlXG4gICAgICBpZiAodHlwZSAmICRleHBvcnQuUiAmJiBleHBQcm90byAmJiAhZXhwUHJvdG9ba2V5XSkgaGlkZShleHBQcm90bywga2V5LCBvdXQpO1xuICAgIH1cbiAgfVxufTtcbi8vIHR5cGUgYml0bWFwXG4kZXhwb3J0LkYgPSAxOyAgIC8vIGZvcmNlZFxuJGV4cG9ydC5HID0gMjsgICAvLyBnbG9iYWxcbiRleHBvcnQuUyA9IDQ7ICAgLy8gc3RhdGljXG4kZXhwb3J0LlAgPSA4OyAgIC8vIHByb3RvXG4kZXhwb3J0LkIgPSAxNjsgIC8vIGJpbmRcbiRleHBvcnQuVyA9IDMyOyAgLy8gd3JhcFxuJGV4cG9ydC5VID0gNjQ7ICAvLyBzYWZlXG4kZXhwb3J0LlIgPSAxMjg7IC8vIHJlYWwgcHJvdG8gbWV0aG9kIGZvciBgbGlicmFyeWBcbm1vZHVsZS5leHBvcnRzID0gJGV4cG9ydDtcbiIsIm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKGV4ZWMpIHtcbiAgdHJ5IHtcbiAgICByZXR1cm4gISFleGVjKCk7XG4gIH0gY2F0Y2ggKGUpIHtcbiAgICByZXR1cm4gdHJ1ZTtcbiAgfVxufTtcbiIsIi8vIGh0dHBzOi8vZ2l0aHViLmNvbS96bG9pcm9jay9jb3JlLWpzL2lzc3Vlcy84NiNpc3N1ZWNvbW1lbnQtMTE1NzU5MDI4XG52YXIgZ2xvYmFsID0gbW9kdWxlLmV4cG9ydHMgPSB0eXBlb2Ygd2luZG93ICE9ICd1bmRlZmluZWQnICYmIHdpbmRvdy5NYXRoID09IE1hdGhcbiAgPyB3aW5kb3cgOiB0eXBlb2Ygc2VsZiAhPSAndW5kZWZpbmVkJyAmJiBzZWxmLk1hdGggPT0gTWF0aCA/IHNlbGZcbiAgLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lIG5vLW5ldy1mdW5jXG4gIDogRnVuY3Rpb24oJ3JldHVybiB0aGlzJykoKTtcbmlmICh0eXBlb2YgX19nID09ICdudW1iZXInKSBfX2cgPSBnbG9iYWw7IC8vIGVzbGludC1kaXNhYmxlLWxpbmUgbm8tdW5kZWZcbiIsInZhciBoYXNPd25Qcm9wZXJ0eSA9IHt9Lmhhc093blByb3BlcnR5O1xubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAoaXQsIGtleSkge1xuICByZXR1cm4gaGFzT3duUHJvcGVydHkuY2FsbChpdCwga2V5KTtcbn07XG4iLCJ2YXIgZFAgPSByZXF1aXJlKCcuL19vYmplY3QtZHAnKTtcbnZhciBjcmVhdGVEZXNjID0gcmVxdWlyZSgnLi9fcHJvcGVydHktZGVzYycpO1xubW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKCcuL19kZXNjcmlwdG9ycycpID8gZnVuY3Rpb24gKG9iamVjdCwga2V5LCB2YWx1ZSkge1xuICByZXR1cm4gZFAuZihvYmplY3QsIGtleSwgY3JlYXRlRGVzYygxLCB2YWx1ZSkpO1xufSA6IGZ1bmN0aW9uIChvYmplY3QsIGtleSwgdmFsdWUpIHtcbiAgb2JqZWN0W2tleV0gPSB2YWx1ZTtcbiAgcmV0dXJuIG9iamVjdDtcbn07XG4iLCJtb2R1bGUuZXhwb3J0cyA9ICFyZXF1aXJlKCcuL19kZXNjcmlwdG9ycycpICYmICFyZXF1aXJlKCcuL19mYWlscycpKGZ1bmN0aW9uICgpIHtcbiAgcmV0dXJuIE9iamVjdC5kZWZpbmVQcm9wZXJ0eShyZXF1aXJlKCcuL19kb20tY3JlYXRlJykoJ2RpdicpLCAnYScsIHsgZ2V0OiBmdW5jdGlvbiAoKSB7IHJldHVybiA3OyB9IH0pLmEgIT0gNztcbn0pO1xuIiwiLy8gZmFsbGJhY2sgZm9yIG5vbi1hcnJheS1saWtlIEVTMyBhbmQgbm9uLWVudW1lcmFibGUgb2xkIFY4IHN0cmluZ3NcbnZhciBjb2YgPSByZXF1aXJlKCcuL19jb2YnKTtcbi8vIGVzbGludC1kaXNhYmxlLW5leHQtbGluZSBuby1wcm90b3R5cGUtYnVpbHRpbnNcbm1vZHVsZS5leHBvcnRzID0gT2JqZWN0KCd6JykucHJvcGVydHlJc0VudW1lcmFibGUoMCkgPyBPYmplY3QgOiBmdW5jdGlvbiAoaXQpIHtcbiAgcmV0dXJuIGNvZihpdCkgPT0gJ1N0cmluZycgPyBpdC5zcGxpdCgnJykgOiBPYmplY3QoaXQpO1xufTtcbiIsIm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKGl0KSB7XG4gIHJldHVybiB0eXBlb2YgaXQgPT09ICdvYmplY3QnID8gaXQgIT09IG51bGwgOiB0eXBlb2YgaXQgPT09ICdmdW5jdGlvbic7XG59O1xuIiwibW9kdWxlLmV4cG9ydHMgPSB0cnVlO1xuIiwidmFyIGFuT2JqZWN0ID0gcmVxdWlyZSgnLi9fYW4tb2JqZWN0Jyk7XG52YXIgSUU4X0RPTV9ERUZJTkUgPSByZXF1aXJlKCcuL19pZTgtZG9tLWRlZmluZScpO1xudmFyIHRvUHJpbWl0aXZlID0gcmVxdWlyZSgnLi9fdG8tcHJpbWl0aXZlJyk7XG52YXIgZFAgPSBPYmplY3QuZGVmaW5lUHJvcGVydHk7XG5cbmV4cG9ydHMuZiA9IHJlcXVpcmUoJy4vX2Rlc2NyaXB0b3JzJykgPyBPYmplY3QuZGVmaW5lUHJvcGVydHkgOiBmdW5jdGlvbiBkZWZpbmVQcm9wZXJ0eShPLCBQLCBBdHRyaWJ1dGVzKSB7XG4gIGFuT2JqZWN0KE8pO1xuICBQID0gdG9QcmltaXRpdmUoUCwgdHJ1ZSk7XG4gIGFuT2JqZWN0KEF0dHJpYnV0ZXMpO1xuICBpZiAoSUU4X0RPTV9ERUZJTkUpIHRyeSB7XG4gICAgcmV0dXJuIGRQKE8sIFAsIEF0dHJpYnV0ZXMpO1xuICB9IGNhdGNoIChlKSB7IC8qIGVtcHR5ICovIH1cbiAgaWYgKCdnZXQnIGluIEF0dHJpYnV0ZXMgfHwgJ3NldCcgaW4gQXR0cmlidXRlcykgdGhyb3cgVHlwZUVycm9yKCdBY2Nlc3NvcnMgbm90IHN1cHBvcnRlZCEnKTtcbiAgaWYgKCd2YWx1ZScgaW4gQXR0cmlidXRlcykgT1tQXSA9IEF0dHJpYnV0ZXMudmFsdWU7XG4gIHJldHVybiBPO1xufTtcbiIsInZhciBoYXMgPSByZXF1aXJlKCcuL19oYXMnKTtcbnZhciB0b0lPYmplY3QgPSByZXF1aXJlKCcuL190by1pb2JqZWN0Jyk7XG52YXIgYXJyYXlJbmRleE9mID0gcmVxdWlyZSgnLi9fYXJyYXktaW5jbHVkZXMnKShmYWxzZSk7XG52YXIgSUVfUFJPVE8gPSByZXF1aXJlKCcuL19zaGFyZWQta2V5JykoJ0lFX1BST1RPJyk7XG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKG9iamVjdCwgbmFtZXMpIHtcbiAgdmFyIE8gPSB0b0lPYmplY3Qob2JqZWN0KTtcbiAgdmFyIGkgPSAwO1xuICB2YXIgcmVzdWx0ID0gW107XG4gIHZhciBrZXk7XG4gIGZvciAoa2V5IGluIE8pIGlmIChrZXkgIT0gSUVfUFJPVE8pIGhhcyhPLCBrZXkpICYmIHJlc3VsdC5wdXNoKGtleSk7XG4gIC8vIERvbid0IGVudW0gYnVnICYgaGlkZGVuIGtleXNcbiAgd2hpbGUgKG5hbWVzLmxlbmd0aCA+IGkpIGlmIChoYXMoTywga2V5ID0gbmFtZXNbaSsrXSkpIHtcbiAgICB+YXJyYXlJbmRleE9mKHJlc3VsdCwga2V5KSB8fCByZXN1bHQucHVzaChrZXkpO1xuICB9XG4gIHJldHVybiByZXN1bHQ7XG59O1xuIiwiLy8gMTkuMS4yLjE0IC8gMTUuMi4zLjE0IE9iamVjdC5rZXlzKE8pXG52YXIgJGtleXMgPSByZXF1aXJlKCcuL19vYmplY3Qta2V5cy1pbnRlcm5hbCcpO1xudmFyIGVudW1CdWdLZXlzID0gcmVxdWlyZSgnLi9fZW51bS1idWcta2V5cycpO1xuXG5tb2R1bGUuZXhwb3J0cyA9IE9iamVjdC5rZXlzIHx8IGZ1bmN0aW9uIGtleXMoTykge1xuICByZXR1cm4gJGtleXMoTywgZW51bUJ1Z0tleXMpO1xufTtcbiIsImV4cG9ydHMuZiA9IHt9LnByb3BlcnR5SXNFbnVtZXJhYmxlO1xuIiwidmFyIERFU0NSSVBUT1JTID0gcmVxdWlyZSgnLi9fZGVzY3JpcHRvcnMnKTtcbnZhciBnZXRLZXlzID0gcmVxdWlyZSgnLi9fb2JqZWN0LWtleXMnKTtcbnZhciB0b0lPYmplY3QgPSByZXF1aXJlKCcuL190by1pb2JqZWN0Jyk7XG52YXIgaXNFbnVtID0gcmVxdWlyZSgnLi9fb2JqZWN0LXBpZScpLmY7XG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIChpc0VudHJpZXMpIHtcbiAgcmV0dXJuIGZ1bmN0aW9uIChpdCkge1xuICAgIHZhciBPID0gdG9JT2JqZWN0KGl0KTtcbiAgICB2YXIga2V5cyA9IGdldEtleXMoTyk7XG4gICAgdmFyIGxlbmd0aCA9IGtleXMubGVuZ3RoO1xuICAgIHZhciBpID0gMDtcbiAgICB2YXIgcmVzdWx0ID0gW107XG4gICAgdmFyIGtleTtcbiAgICB3aGlsZSAobGVuZ3RoID4gaSkge1xuICAgICAga2V5ID0ga2V5c1tpKytdO1xuICAgICAgaWYgKCFERVNDUklQVE9SUyB8fCBpc0VudW0uY2FsbChPLCBrZXkpKSB7XG4gICAgICAgIHJlc3VsdC5wdXNoKGlzRW50cmllcyA/IFtrZXksIE9ba2V5XV0gOiBPW2tleV0pO1xuICAgICAgfVxuICAgIH1cbiAgICByZXR1cm4gcmVzdWx0O1xuICB9O1xufTtcbiIsIm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKGJpdG1hcCwgdmFsdWUpIHtcbiAgcmV0dXJuIHtcbiAgICBlbnVtZXJhYmxlOiAhKGJpdG1hcCAmIDEpLFxuICAgIGNvbmZpZ3VyYWJsZTogIShiaXRtYXAgJiAyKSxcbiAgICB3cml0YWJsZTogIShiaXRtYXAgJiA0KSxcbiAgICB2YWx1ZTogdmFsdWVcbiAgfTtcbn07XG4iLCJ2YXIgc2hhcmVkID0gcmVxdWlyZSgnLi9fc2hhcmVkJykoJ2tleXMnKTtcbnZhciB1aWQgPSByZXF1aXJlKCcuL191aWQnKTtcbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKGtleSkge1xuICByZXR1cm4gc2hhcmVkW2tleV0gfHwgKHNoYXJlZFtrZXldID0gdWlkKGtleSkpO1xufTtcbiIsInZhciBjb3JlID0gcmVxdWlyZSgnLi9fY29yZScpO1xudmFyIGdsb2JhbCA9IHJlcXVpcmUoJy4vX2dsb2JhbCcpO1xudmFyIFNIQVJFRCA9ICdfX2NvcmUtanNfc2hhcmVkX18nO1xudmFyIHN0b3JlID0gZ2xvYmFsW1NIQVJFRF0gfHwgKGdsb2JhbFtTSEFSRURdID0ge30pO1xuXG4obW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAoa2V5LCB2YWx1ZSkge1xuICByZXR1cm4gc3RvcmVba2V5XSB8fCAoc3RvcmVba2V5XSA9IHZhbHVlICE9PSB1bmRlZmluZWQgPyB2YWx1ZSA6IHt9KTtcbn0pKCd2ZXJzaW9ucycsIFtdKS5wdXNoKHtcbiAgdmVyc2lvbjogY29yZS52ZXJzaW9uLFxuICBtb2RlOiByZXF1aXJlKCcuL19saWJyYXJ5JykgPyAncHVyZScgOiAnZ2xvYmFsJyxcbiAgY29weXJpZ2h0OiAnwqkgMjAxOSBEZW5pcyBQdXNoa2FyZXYgKHpsb2lyb2NrLnJ1KSdcbn0pO1xuIiwidmFyIHRvSW50ZWdlciA9IHJlcXVpcmUoJy4vX3RvLWludGVnZXInKTtcbnZhciBtYXggPSBNYXRoLm1heDtcbnZhciBtaW4gPSBNYXRoLm1pbjtcbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKGluZGV4LCBsZW5ndGgpIHtcbiAgaW5kZXggPSB0b0ludGVnZXIoaW5kZXgpO1xuICByZXR1cm4gaW5kZXggPCAwID8gbWF4KGluZGV4ICsgbGVuZ3RoLCAwKSA6IG1pbihpbmRleCwgbGVuZ3RoKTtcbn07XG4iLCIvLyA3LjEuNCBUb0ludGVnZXJcbnZhciBjZWlsID0gTWF0aC5jZWlsO1xudmFyIGZsb29yID0gTWF0aC5mbG9vcjtcbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKGl0KSB7XG4gIHJldHVybiBpc05hTihpdCA9ICtpdCkgPyAwIDogKGl0ID4gMCA/IGZsb29yIDogY2VpbCkoaXQpO1xufTtcbiIsIi8vIHRvIGluZGV4ZWQgb2JqZWN0LCB0b09iamVjdCB3aXRoIGZhbGxiYWNrIGZvciBub24tYXJyYXktbGlrZSBFUzMgc3RyaW5nc1xudmFyIElPYmplY3QgPSByZXF1aXJlKCcuL19pb2JqZWN0Jyk7XG52YXIgZGVmaW5lZCA9IHJlcXVpcmUoJy4vX2RlZmluZWQnKTtcbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKGl0KSB7XG4gIHJldHVybiBJT2JqZWN0KGRlZmluZWQoaXQpKTtcbn07XG4iLCIvLyA3LjEuMTUgVG9MZW5ndGhcbnZhciB0b0ludGVnZXIgPSByZXF1aXJlKCcuL190by1pbnRlZ2VyJyk7XG52YXIgbWluID0gTWF0aC5taW47XG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIChpdCkge1xuICByZXR1cm4gaXQgPiAwID8gbWluKHRvSW50ZWdlcihpdCksIDB4MWZmZmZmZmZmZmZmZmYpIDogMDsgLy8gcG93KDIsIDUzKSAtIDEgPT0gOTAwNzE5OTI1NDc0MDk5MVxufTtcbiIsIi8vIDcuMS4xIFRvUHJpbWl0aXZlKGlucHV0IFssIFByZWZlcnJlZFR5cGVdKVxudmFyIGlzT2JqZWN0ID0gcmVxdWlyZSgnLi9faXMtb2JqZWN0Jyk7XG4vLyBpbnN0ZWFkIG9mIHRoZSBFUzYgc3BlYyB2ZXJzaW9uLCB3ZSBkaWRuJ3QgaW1wbGVtZW50IEBAdG9QcmltaXRpdmUgY2FzZVxuLy8gYW5kIHRoZSBzZWNvbmQgYXJndW1lbnQgLSBmbGFnIC0gcHJlZmVycmVkIHR5cGUgaXMgYSBzdHJpbmdcbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKGl0LCBTKSB7XG4gIGlmICghaXNPYmplY3QoaXQpKSByZXR1cm4gaXQ7XG4gIHZhciBmbiwgdmFsO1xuICBpZiAoUyAmJiB0eXBlb2YgKGZuID0gaXQudG9TdHJpbmcpID09ICdmdW5jdGlvbicgJiYgIWlzT2JqZWN0KHZhbCA9IGZuLmNhbGwoaXQpKSkgcmV0dXJuIHZhbDtcbiAgaWYgKHR5cGVvZiAoZm4gPSBpdC52YWx1ZU9mKSA9PSAnZnVuY3Rpb24nICYmICFpc09iamVjdCh2YWwgPSBmbi5jYWxsKGl0KSkpIHJldHVybiB2YWw7XG4gIGlmICghUyAmJiB0eXBlb2YgKGZuID0gaXQudG9TdHJpbmcpID09ICdmdW5jdGlvbicgJiYgIWlzT2JqZWN0KHZhbCA9IGZuLmNhbGwoaXQpKSkgcmV0dXJuIHZhbDtcbiAgdGhyb3cgVHlwZUVycm9yKFwiQ2FuJ3QgY29udmVydCBvYmplY3QgdG8gcHJpbWl0aXZlIHZhbHVlXCIpO1xufTtcbiIsInZhciBpZCA9IDA7XG52YXIgcHggPSBNYXRoLnJhbmRvbSgpO1xubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAoa2V5KSB7XG4gIHJldHVybiAnU3ltYm9sKCcuY29uY2F0KGtleSA9PT0gdW5kZWZpbmVkID8gJycgOiBrZXksICcpXycsICgrK2lkICsgcHgpLnRvU3RyaW5nKDM2KSk7XG59O1xuIiwidmFyICRleHBvcnQgPSByZXF1aXJlKCcuL19leHBvcnQnKTtcbi8vIDE5LjEuMi40IC8gMTUuMi4zLjYgT2JqZWN0LmRlZmluZVByb3BlcnR5KE8sIFAsIEF0dHJpYnV0ZXMpXG4kZXhwb3J0KCRleHBvcnQuUyArICRleHBvcnQuRiAqICFyZXF1aXJlKCcuL19kZXNjcmlwdG9ycycpLCAnT2JqZWN0JywgeyBkZWZpbmVQcm9wZXJ0eTogcmVxdWlyZSgnLi9fb2JqZWN0LWRwJykuZiB9KTtcbiIsIi8vIGh0dHBzOi8vZ2l0aHViLmNvbS90YzM5L3Byb3Bvc2FsLW9iamVjdC12YWx1ZXMtZW50cmllc1xudmFyICRleHBvcnQgPSByZXF1aXJlKCcuL19leHBvcnQnKTtcbnZhciAkdmFsdWVzID0gcmVxdWlyZSgnLi9fb2JqZWN0LXRvLWFycmF5JykoZmFsc2UpO1xuXG4kZXhwb3J0KCRleHBvcnQuUywgJ09iamVjdCcsIHtcbiAgdmFsdWVzOiBmdW5jdGlvbiB2YWx1ZXMoaXQpIHtcbiAgICByZXR1cm4gJHZhbHVlcyhpdCk7XG4gIH1cbn0pO1xuIiwiLy8gVGhlIG1vZHVsZSBjYWNoZVxudmFyIF9fd2VicGFja19tb2R1bGVfY2FjaGVfXyA9IHt9O1xuXG4vLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcblx0dmFyIGNhY2hlZE1vZHVsZSA9IF9fd2VicGFja19tb2R1bGVfY2FjaGVfX1ttb2R1bGVJZF07XG5cdGlmIChjYWNoZWRNb2R1bGUgIT09IHVuZGVmaW5lZCkge1xuXHRcdHJldHVybiBjYWNoZWRNb2R1bGUuZXhwb3J0cztcblx0fVxuXHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuXHR2YXIgbW9kdWxlID0gX193ZWJwYWNrX21vZHVsZV9jYWNoZV9fW21vZHVsZUlkXSA9IHtcblx0XHQvLyBubyBtb2R1bGUuaWQgbmVlZGVkXG5cdFx0Ly8gbm8gbW9kdWxlLmxvYWRlZCBuZWVkZWRcblx0XHRleHBvcnRzOiB7fVxuXHR9O1xuXG5cdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuXHRfX3dlYnBhY2tfbW9kdWxlc19fW21vZHVsZUlkXShtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuXHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuXHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG59XG5cbiIsIi8qKlxuICogQ29weXJpZ2h0IHNpbmNlIDIwMDcgUHJlc3RhU2hvcCBTQSBhbmQgQ29udHJpYnV0b3JzXG4gKiBQcmVzdGFTaG9wIGlzIGFuIEludGVybmF0aW9uYWwgUmVnaXN0ZXJlZCBUcmFkZW1hcmsgJiBQcm9wZXJ0eSBvZiBQcmVzdGFTaG9wIFNBXG4gKlxuICogTk9USUNFIE9GIExJQ0VOU0VcbiAqXG4gKiBUaGlzIHNvdXJjZSBmaWxlIGlzIHN1YmplY3QgdG8gdGhlIE9wZW4gU29mdHdhcmUgTGljZW5zZSAoT1NMIDMuMClcbiAqIHRoYXQgaXMgYnVuZGxlZCB3aXRoIHRoaXMgcGFja2FnZSBpbiB0aGUgZmlsZSBMSUNFTlNFLm1kLlxuICogSXQgaXMgYWxzbyBhdmFpbGFibGUgdGhyb3VnaCB0aGUgd29ybGQtd2lkZS13ZWIgYXQgdGhpcyBVUkw6XG4gKiBodHRwczovL29wZW5zb3VyY2Uub3JnL2xpY2Vuc2VzL09TTC0zLjBcbiAqIElmIHlvdSBkaWQgbm90IHJlY2VpdmUgYSBjb3B5IG9mIHRoZSBsaWNlbnNlIGFuZCBhcmUgdW5hYmxlIHRvXG4gKiBvYnRhaW4gaXQgdGhyb3VnaCB0aGUgd29ybGQtd2lkZS13ZWIsIHBsZWFzZSBzZW5kIGFuIGVtYWlsXG4gKiB0byBsaWNlbnNlQHByZXN0YXNob3AuY29tIHNvIHdlIGNhbiBzZW5kIHlvdSBhIGNvcHkgaW1tZWRpYXRlbHkuXG4gKlxuICogRElTQ0xBSU1FUlxuICpcbiAqIERvIG5vdCBlZGl0IG9yIGFkZCB0byB0aGlzIGZpbGUgaWYgeW91IHdpc2ggdG8gdXBncmFkZSBQcmVzdGFTaG9wIHRvIG5ld2VyXG4gKiB2ZXJzaW9ucyBpbiB0aGUgZnV0dXJlLiBJZiB5b3Ugd2lzaCB0byBjdXN0b21pemUgUHJlc3RhU2hvcCBmb3IgeW91clxuICogbmVlZHMgcGxlYXNlIHJlZmVyIHRvIGh0dHBzOi8vZGV2ZG9jcy5wcmVzdGFzaG9wLmNvbS8gZm9yIG1vcmUgaW5mb3JtYXRpb24uXG4gKlxuICogQGF1dGhvciAgICBQcmVzdGFTaG9wIFNBIGFuZCBDb250cmlidXRvcnMgPGNvbnRhY3RAcHJlc3Rhc2hvcC5jb20+XG4gKiBAY29weXJpZ2h0IFNpbmNlIDIwMDcgUHJlc3RhU2hvcCBTQSBhbmQgQ29udHJpYnV0b3JzXG4gKiBAbGljZW5zZSAgIGh0dHBzOi8vb3BlbnNvdXJjZS5vcmcvbGljZW5zZXMvT1NMLTMuMCBPcGVuIFNvZnR3YXJlIExpY2Vuc2UgKE9TTCAzLjApXG4gKi9cblxuaW1wb3J0IFRyYW5zbGF0aW9uU2V0dGluZ3NQYWdlIGZyb20gJy4vVHJhbnNsYXRpb25TZXR0aW5nc1BhZ2UnO1xuXG5jb25zdCB7JH0gPSB3aW5kb3c7XG5cbiQoKCkgPT4ge1xuICBuZXcgVHJhbnNsYXRpb25TZXR0aW5nc1BhZ2UoKTtcbn0pO1xuIl0sInNvdXJjZVJvb3QiOiIifQ==