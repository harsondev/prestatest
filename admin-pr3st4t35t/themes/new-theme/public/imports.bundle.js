/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./js/pages/import/FormFieldToggle.js":
/*!********************************************!*\
  !*** ./js/pages/import/FormFieldToggle.js ***!
  \********************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

"use strict";


Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 */

var _window = window,
    $ = _window.$;


var entityCategories = 0;
var entityProducts = 1;
var entityCombinations = 2;
var entityCustomers = 3;
var entityAddresses = 4;
var entityBrands = 5;
var entitySuppliers = 6;
var entityAlias = 7;
var entityStoreContacts = 8;

var FormFieldToggle = function () {
  function FormFieldToggle() {
    var _this = this;

    (0, _classCallCheck3.default)(this, FormFieldToggle);

    $('.js-entity-select').on('change', function () {
      return _this.toggleForm();
    });

    this.toggleForm();
  }

  (0, _createClass3.default)(FormFieldToggle, [{
    key: 'toggleForm',
    value: function toggleForm() {
      var selectedOption = $('#entity').find('option:selected');
      var selectedEntity = parseInt(selectedOption.val(), 10);
      var entityName = selectedOption.text().toLowerCase();

      this.toggleEntityAlert(selectedEntity);
      this.toggleFields(selectedEntity, entityName);
      this.loadAvailableFields(selectedEntity);
    }

    /**
     * Toggle alert warning for selected import entity
     *
     * @param {int} selectedEntity
     */

  }, {
    key: 'toggleEntityAlert',
    value: function toggleEntityAlert(selectedEntity) {
      var $alert = $('.js-entity-alert');

      if ([entityCategories, entityProducts].includes(selectedEntity)) {
        $alert.show();
      } else {
        $alert.hide();
      }
    }

    /**
     * Toggle available options for selected entity
     *
     * @param {int} selectedEntity
     * @param {string} entityName
     */

  }, {
    key: 'toggleFields',
    value: function toggleFields(selectedEntity, entityName) {
      var $truncateFormGroup = $('.js-truncate-form-group');
      var $matchRefFormGroup = $('.js-match-ref-form-group');
      var $regenerateFormGroup = $('.js-regenerate-form-group');
      var $forceIdsFormGroup = $('.js-force-ids-form-group');
      var $entityNamePlaceholder = $('.js-entity-name');

      if (entityStoreContacts === selectedEntity) {
        $truncateFormGroup.hide();
      } else {
        $truncateFormGroup.show();
      }

      if ([entityProducts, entityCombinations].includes(selectedEntity)) {
        $matchRefFormGroup.show();
      } else {
        $matchRefFormGroup.hide();
      }

      if ([entityCategories, entityProducts, entityBrands, entitySuppliers, entityStoreContacts].includes(selectedEntity)) {
        $regenerateFormGroup.show();
      } else {
        $regenerateFormGroup.hide();
      }

      if ([entityCategories, entityProducts, entityCustomers, entityAddresses, entityBrands, entitySuppliers, entityStoreContacts, entityAlias].includes(selectedEntity)) {
        $forceIdsFormGroup.show();
      } else {
        $forceIdsFormGroup.hide();
      }

      $entityNamePlaceholder.html(entityName);
    }

    /**
     * Load available fields for given entity
     *
     * @param {int} entity
     */

  }, {
    key: 'loadAvailableFields',
    value: function loadAvailableFields(entity) {
      var _this2 = this;

      var $availableFields = $('.js-available-fields');

      $.ajax({
        url: $availableFields.data('url'),
        data: {
          entity: entity
        },
        dataType: 'json'
      }).then(function (response) {
        _this2.removeAvailableFields($availableFields);

        for (var i = 0; i < response.length; i += 1) {
          _this2.appendAvailableField($availableFields, response[i].label + (response[i].required ? '*' : ''), response[i].description);
        }

        $availableFields.find('[data-toggle="popover"]').popover();
      });
    }

    /**
     * Remove available fields content from given container.
     *
     * @param {jQuery} $container
     * @private
     */

  }, {
    key: 'removeAvailableFields',
    value: function removeAvailableFields($container) {
      $container.find('[data-toggle="popover"]').popover('hide');
      $container.empty();
    }

    /**
     * Append a help box to given field.
     *
     * @param {jQuery} $field
     * @param {String} helpBoxContent
     * @private
     */

  }, {
    key: 'appendHelpBox',
    value: function appendHelpBox($field, helpBoxContent) {
      var $helpBox = $('.js-available-field-popover-template').clone();

      $helpBox.attr('data-content', helpBoxContent);
      $helpBox.removeClass('js-available-field-popover-template d-none');
      $field.append($helpBox);
    }

    /**
     * Append available field to given container.
     *
     * @param {jQuery} $appendTo field will be appended to this container.
     * @param {String} fieldText
     * @param {String} helpBoxContent
     * @private
     */

  }, {
    key: 'appendAvailableField',
    value: function appendAvailableField($appendTo, fieldText, helpBoxContent) {
      var $field = $('.js-available-field-template').clone();

      $field.text(fieldText);

      if (helpBoxContent) {
        // Append help box next to the field
        this.appendHelpBox($field, helpBoxContent);
      }

      $field.removeClass('js-available-field-template d-none');
      $field.appendTo($appendTo);
    }
  }]);
  return FormFieldToggle;
}();

exports.default = FormFieldToggle;

/***/ }),

/***/ "./js/pages/import/ImportPage.js":
/*!***************************************!*\
  !*** ./js/pages/import/ImportPage.js ***!
  \***************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

"use strict";


Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _FormFieldToggle = __webpack_require__(/*! ./FormFieldToggle */ "./js/pages/import/FormFieldToggle.js");

var _FormFieldToggle2 = _interopRequireDefault(_FormFieldToggle);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _window = window,
    $ = _window.$; /**
                    * Copyright since 2007 PrestaShop SA and Contributors
                    * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
                    *
                    * NOTICE OF LICENSE
                    *
                    * This source file is subject to the Open Software License (OSL 3.0)
                    * that is bundled with this package in the file LICENSE.md.
                    * It is also available through the world-wide-web at this URL:
                    * https://opensource.org/licenses/OSL-3.0
                    * If you did not receive a copy of the license and are unable to
                    * obtain it through the world-wide-web, please send an email
                    * to license@prestashop.com so we can send you a copy immediately.
                    *
                    * DISCLAIMER
                    *
                    * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
                    * versions in the future. If you wish to customize PrestaShop for your
                    * needs please refer to https://devdocs.prestashop.com/ for more information.
                    *
                    * @author    PrestaShop SA and Contributors <contact@prestashop.com>
                    * @copyright Since 2007 PrestaShop SA and Contributors
                    * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
                    */

var ImportPage = function () {
  function ImportPage() {
    var _this = this;

    (0, _classCallCheck3.default)(this, ImportPage);

    new _FormFieldToggle2.default();

    $('.js-from-files-history-btn').on('click', function () {
      return _this.showFilesHistoryHandler();
    });
    $('.js-close-files-history-block-btn').on('click', function () {
      return _this.closeFilesHistoryHandler();
    });
    $('#fileHistoryTable').on('click', '.js-use-file-btn', function (event) {
      return _this.useFileFromFilesHistory(event);
    });
    $('.js-change-import-file-btn').on('click', function () {
      return _this.changeImportFileHandler();
    });
    $('.js-import-file').on('change', function () {
      return _this.uploadFile();
    });

    this.toggleSelectedFile();
    this.handleSubmit();
  }

  /**
   * Handle submit and add confirm box in case the toggle button about
   * deleting all entities before import is checked
   */


  (0, _createClass3.default)(ImportPage, [{
    key: 'handleSubmit',
    value: function handleSubmit() {
      $('.js-import-form').on('submit', function () {
        var $this = $(this);

        if ($this.find('input[name="truncate"]:checked').val() === '1') {
          /* eslint-disable-next-line max-len */
          return window.confirm($this.data('delete-confirm-message') + ' ' + $.trim($('#entity > option:selected').text().toLowerCase()) + '?');
        }

        return true;
      });
    }

    /**
     * Check if selected file names exists and if so, then display it
     */

  }, {
    key: 'toggleSelectedFile',
    value: function toggleSelectedFile() {
      var selectFilename = $('#csv').val();

      if (selectFilename.length > 0) {
        this.showImportFileAlert(selectFilename);
        this.hideFileUploadBlock();
      }
    }
  }, {
    key: 'changeImportFileHandler',
    value: function changeImportFileHandler() {
      this.hideImportFileAlert();
      this.showFileUploadBlock();
    }

    /**
     * Show files history event handler
     */

  }, {
    key: 'showFilesHistoryHandler',
    value: function showFilesHistoryHandler() {
      this.showFilesHistory();
      this.hideFileUploadBlock();
    }

    /**
     * Close files history event handler
     */

  }, {
    key: 'closeFilesHistoryHandler',
    value: function closeFilesHistoryHandler() {
      this.closeFilesHistory();
      this.showFileUploadBlock();
    }

    /**
     * Show files history block
     */

  }, {
    key: 'showFilesHistory',
    value: function showFilesHistory() {
      $('.js-files-history-block').removeClass('d-none');
    }

    /**
     * Hide files history block
     */

  }, {
    key: 'closeFilesHistory',
    value: function closeFilesHistory() {
      $('.js-files-history-block').addClass('d-none');
    }

    /**
     *  Prefill hidden file input with selected file name from history
     */

  }, {
    key: 'useFileFromFilesHistory',
    value: function useFileFromFilesHistory(event) {
      var filename = $(event.target).closest('.btn-group').data('file');

      $('.js-import-file-input').val(filename);

      this.showImportFileAlert(filename);
      this.closeFilesHistory();
    }

    /**
     * Show alert with imported file name
     */

  }, {
    key: 'showImportFileAlert',
    value: function showImportFileAlert(filename) {
      $('.js-import-file-alert').removeClass('d-none');
      $('.js-import-file').text(filename);
    }

    /**
     * Hides selected import file alert
     */

  }, {
    key: 'hideImportFileAlert',
    value: function hideImportFileAlert() {
      $('.js-import-file-alert').addClass('d-none');
    }

    /**
     * Hides import file upload block
     */

  }, {
    key: 'hideFileUploadBlock',
    value: function hideFileUploadBlock() {
      $('.js-file-upload-form-group').addClass('d-none');
    }

    /**
     * Hides import file upload block
     */

  }, {
    key: 'showFileUploadBlock',
    value: function showFileUploadBlock() {
      $('.js-file-upload-form-group').removeClass('d-none');
    }

    /**
     * Make file history button clickable
     */

  }, {
    key: 'enableFilesHistoryBtn',
    value: function enableFilesHistoryBtn() {
      $('.js-from-files-history-btn').removeAttr('disabled');
    }

    /**
     * Show error message if file uploading failed
     *
     * @param {string} fileName
     * @param {integer} fileSize
     * @param {string} message
     */

  }, {
    key: 'showImportFileError',
    value: function showImportFileError(fileName, fileSize, message) {
      var $alert = $('.js-import-file-error');

      var fileData = fileName + ' (' + this.humanizeSize(fileSize) + ')';

      $alert.find('.js-file-data').text(fileData);
      $alert.find('.js-error-message').text(message);
      $alert.removeClass('d-none');
    }

    /**
     * Hide file uploading error
     */

  }, {
    key: 'hideImportFileError',
    value: function hideImportFileError() {
      var $alert = $('.js-import-file-error');
      $alert.addClass('d-none');
    }

    /**
     * Show file size in human readable format
     *
     * @param {int} bytes
     *
     * @returns {string}
     */

  }, {
    key: 'humanizeSize',
    value: function humanizeSize(bytes) {
      if (typeof bytes !== 'number') {
        return '';
      }

      if (bytes >= 1000000000) {
        return (bytes / 1000000000).toFixed(2) + ' GB';
      }

      if (bytes >= 1000000) {
        return (bytes / 1000000).toFixed(2) + ' MB';
      }

      return (bytes / 1000).toFixed(2) + ' KB';
    }

    /**
     * Upload selected import file
     */

  }, {
    key: 'uploadFile',
    value: function uploadFile() {
      var _this2 = this;

      this.hideImportFileError();

      var $input = $('#file');
      var uploadedFile = $input.prop('files')[0];

      var maxUploadSize = $input.data('max-file-upload-size');

      if (maxUploadSize < uploadedFile.size) {
        this.showImportFileError(uploadedFile.name, uploadedFile.size, 'File is too large');
        return;
      }

      var data = new FormData();
      data.append('file', uploadedFile);

      $.ajax({
        type: 'POST',
        url: $('.js-import-form').data('file-upload-url'),
        data: data,
        cache: false,
        contentType: false,
        processData: false
      }).then(function (response) {
        if (response.error) {
          _this2.showImportFileError(uploadedFile.name, uploadedFile.size, response.error);
          return;
        }

        var filename = response.file.name;

        $('.js-import-file-input').val(filename);

        _this2.showImportFileAlert(filename);
        _this2.hideFileUploadBlock();
        _this2.addFileToHistoryTable(filename);
        _this2.enableFilesHistoryBtn();
      });
    }

    /**
     * Renders new row in files history table
     *
     * @param {string} filename
     */

  }, {
    key: 'addFileToHistoryTable',
    value: function addFileToHistoryTable(filename) {
      var $table = $('#fileHistoryTable');

      var baseDeleteUrl = $table.data('delete-file-url');
      var deleteUrl = baseDeleteUrl + '&filename=' + encodeURIComponent(filename);

      var baseDownloadUrl = $table.data('download-file-url');
      var downloadUrl = baseDownloadUrl + '&filename=' + encodeURIComponent(filename);

      var $template = $table.find('tr:first').clone();

      $template.removeClass('d-none');
      $template.find('td:first').text(filename);
      $template.find('.btn-group').attr('data-file', filename);
      $template.find('.js-delete-file-btn').attr('href', deleteUrl);
      $template.find('.js-download-file-btn').attr('href', downloadUrl);

      $table.find('tbody').append($template);

      var filesNumber = $table.find('tr').length - 1;
      $('.js-files-history-number').text(filesNumber);
    }
  }]);
  return ImportPage;
}();

exports.default = ImportPage;

/***/ }),

/***/ "./node_modules/babel-runtime/core-js/object/define-property.js":
/*!**********************************************************************!*\
  !*** ./node_modules/babel-runtime/core-js/object/define-property.js ***!
  \**********************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

module.exports = { "default": __webpack_require__(/*! core-js/library/fn/object/define-property */ "./node_modules/core-js/library/fn/object/define-property.js"), __esModule: true };

/***/ }),

/***/ "./node_modules/babel-runtime/helpers/classCallCheck.js":
/*!**************************************************************!*\
  !*** ./node_modules/babel-runtime/helpers/classCallCheck.js ***!
  \**************************************************************/
/***/ ((__unused_webpack_module, exports) => {

"use strict";


exports.__esModule = true;

exports.default = function (instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
};

/***/ }),

/***/ "./node_modules/babel-runtime/helpers/createClass.js":
/*!***********************************************************!*\
  !*** ./node_modules/babel-runtime/helpers/createClass.js ***!
  \***********************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

"use strict";


exports.__esModule = true;

var _defineProperty = __webpack_require__(/*! ../core-js/object/define-property */ "./node_modules/babel-runtime/core-js/object/define-property.js");

var _defineProperty2 = _interopRequireDefault(_defineProperty);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      (0, _defineProperty2.default)(target, descriptor.key, descriptor);
    }
  }

  return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);
    if (staticProps) defineProperties(Constructor, staticProps);
    return Constructor;
  };
}();

/***/ }),

/***/ "./node_modules/core-js/library/fn/object/define-property.js":
/*!*******************************************************************!*\
  !*** ./node_modules/core-js/library/fn/object/define-property.js ***!
  \*******************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

__webpack_require__(/*! ../../modules/es6.object.define-property */ "./node_modules/core-js/library/modules/es6.object.define-property.js");
var $Object = __webpack_require__(/*! ../../modules/_core */ "./node_modules/core-js/library/modules/_core.js").Object;
module.exports = function defineProperty(it, key, desc) {
  return $Object.defineProperty(it, key, desc);
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_a-function.js":
/*!*************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_a-function.js ***!
  \*************************************************************/
/***/ ((module) => {

module.exports = function (it) {
  if (typeof it != 'function') throw TypeError(it + ' is not a function!');
  return it;
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_an-object.js":
/*!************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_an-object.js ***!
  \************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var isObject = __webpack_require__(/*! ./_is-object */ "./node_modules/core-js/library/modules/_is-object.js");
module.exports = function (it) {
  if (!isObject(it)) throw TypeError(it + ' is not an object!');
  return it;
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_core.js":
/*!*******************************************************!*\
  !*** ./node_modules/core-js/library/modules/_core.js ***!
  \*******************************************************/
/***/ ((module) => {

var core = module.exports = { version: '2.6.11' };
if (typeof __e == 'number') __e = core; // eslint-disable-line no-undef


/***/ }),

/***/ "./node_modules/core-js/library/modules/_ctx.js":
/*!******************************************************!*\
  !*** ./node_modules/core-js/library/modules/_ctx.js ***!
  \******************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// optional / simple context binding
var aFunction = __webpack_require__(/*! ./_a-function */ "./node_modules/core-js/library/modules/_a-function.js");
module.exports = function (fn, that, length) {
  aFunction(fn);
  if (that === undefined) return fn;
  switch (length) {
    case 1: return function (a) {
      return fn.call(that, a);
    };
    case 2: return function (a, b) {
      return fn.call(that, a, b);
    };
    case 3: return function (a, b, c) {
      return fn.call(that, a, b, c);
    };
  }
  return function (/* ...args */) {
    return fn.apply(that, arguments);
  };
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_descriptors.js":
/*!**************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_descriptors.js ***!
  \**************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// Thank's IE8 for his funny defineProperty
module.exports = !__webpack_require__(/*! ./_fails */ "./node_modules/core-js/library/modules/_fails.js")(function () {
  return Object.defineProperty({}, 'a', { get: function () { return 7; } }).a != 7;
});


/***/ }),

/***/ "./node_modules/core-js/library/modules/_dom-create.js":
/*!*************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_dom-create.js ***!
  \*************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var isObject = __webpack_require__(/*! ./_is-object */ "./node_modules/core-js/library/modules/_is-object.js");
var document = __webpack_require__(/*! ./_global */ "./node_modules/core-js/library/modules/_global.js").document;
// typeof document.createElement is 'object' in old IE
var is = isObject(document) && isObject(document.createElement);
module.exports = function (it) {
  return is ? document.createElement(it) : {};
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_export.js":
/*!*********************************************************!*\
  !*** ./node_modules/core-js/library/modules/_export.js ***!
  \*********************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var global = __webpack_require__(/*! ./_global */ "./node_modules/core-js/library/modules/_global.js");
var core = __webpack_require__(/*! ./_core */ "./node_modules/core-js/library/modules/_core.js");
var ctx = __webpack_require__(/*! ./_ctx */ "./node_modules/core-js/library/modules/_ctx.js");
var hide = __webpack_require__(/*! ./_hide */ "./node_modules/core-js/library/modules/_hide.js");
var has = __webpack_require__(/*! ./_has */ "./node_modules/core-js/library/modules/_has.js");
var PROTOTYPE = 'prototype';

var $export = function (type, name, source) {
  var IS_FORCED = type & $export.F;
  var IS_GLOBAL = type & $export.G;
  var IS_STATIC = type & $export.S;
  var IS_PROTO = type & $export.P;
  var IS_BIND = type & $export.B;
  var IS_WRAP = type & $export.W;
  var exports = IS_GLOBAL ? core : core[name] || (core[name] = {});
  var expProto = exports[PROTOTYPE];
  var target = IS_GLOBAL ? global : IS_STATIC ? global[name] : (global[name] || {})[PROTOTYPE];
  var key, own, out;
  if (IS_GLOBAL) source = name;
  for (key in source) {
    // contains in native
    own = !IS_FORCED && target && target[key] !== undefined;
    if (own && has(exports, key)) continue;
    // export native or passed
    out = own ? target[key] : source[key];
    // prevent global pollution for namespaces
    exports[key] = IS_GLOBAL && typeof target[key] != 'function' ? source[key]
    // bind timers to global for call from export context
    : IS_BIND && own ? ctx(out, global)
    // wrap global constructors for prevent change them in library
    : IS_WRAP && target[key] == out ? (function (C) {
      var F = function (a, b, c) {
        if (this instanceof C) {
          switch (arguments.length) {
            case 0: return new C();
            case 1: return new C(a);
            case 2: return new C(a, b);
          } return new C(a, b, c);
        } return C.apply(this, arguments);
      };
      F[PROTOTYPE] = C[PROTOTYPE];
      return F;
    // make static versions for prototype methods
    })(out) : IS_PROTO && typeof out == 'function' ? ctx(Function.call, out) : out;
    // export proto methods to core.%CONSTRUCTOR%.methods.%NAME%
    if (IS_PROTO) {
      (exports.virtual || (exports.virtual = {}))[key] = out;
      // export proto methods to core.%CONSTRUCTOR%.prototype.%NAME%
      if (type & $export.R && expProto && !expProto[key]) hide(expProto, key, out);
    }
  }
};
// type bitmap
$export.F = 1;   // forced
$export.G = 2;   // global
$export.S = 4;   // static
$export.P = 8;   // proto
$export.B = 16;  // bind
$export.W = 32;  // wrap
$export.U = 64;  // safe
$export.R = 128; // real proto method for `library`
module.exports = $export;


/***/ }),

/***/ "./node_modules/core-js/library/modules/_fails.js":
/*!********************************************************!*\
  !*** ./node_modules/core-js/library/modules/_fails.js ***!
  \********************************************************/
/***/ ((module) => {

module.exports = function (exec) {
  try {
    return !!exec();
  } catch (e) {
    return true;
  }
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_global.js":
/*!*********************************************************!*\
  !*** ./node_modules/core-js/library/modules/_global.js ***!
  \*********************************************************/
/***/ ((module) => {

// https://github.com/zloirock/core-js/issues/86#issuecomment-115759028
var global = module.exports = typeof window != 'undefined' && window.Math == Math
  ? window : typeof self != 'undefined' && self.Math == Math ? self
  // eslint-disable-next-line no-new-func
  : Function('return this')();
if (typeof __g == 'number') __g = global; // eslint-disable-line no-undef


/***/ }),

/***/ "./node_modules/core-js/library/modules/_has.js":
/*!******************************************************!*\
  !*** ./node_modules/core-js/library/modules/_has.js ***!
  \******************************************************/
/***/ ((module) => {

var hasOwnProperty = {}.hasOwnProperty;
module.exports = function (it, key) {
  return hasOwnProperty.call(it, key);
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_hide.js":
/*!*******************************************************!*\
  !*** ./node_modules/core-js/library/modules/_hide.js ***!
  \*******************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var dP = __webpack_require__(/*! ./_object-dp */ "./node_modules/core-js/library/modules/_object-dp.js");
var createDesc = __webpack_require__(/*! ./_property-desc */ "./node_modules/core-js/library/modules/_property-desc.js");
module.exports = __webpack_require__(/*! ./_descriptors */ "./node_modules/core-js/library/modules/_descriptors.js") ? function (object, key, value) {
  return dP.f(object, key, createDesc(1, value));
} : function (object, key, value) {
  object[key] = value;
  return object;
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_ie8-dom-define.js":
/*!*****************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_ie8-dom-define.js ***!
  \*****************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

module.exports = !__webpack_require__(/*! ./_descriptors */ "./node_modules/core-js/library/modules/_descriptors.js") && !__webpack_require__(/*! ./_fails */ "./node_modules/core-js/library/modules/_fails.js")(function () {
  return Object.defineProperty(__webpack_require__(/*! ./_dom-create */ "./node_modules/core-js/library/modules/_dom-create.js")('div'), 'a', { get: function () { return 7; } }).a != 7;
});


/***/ }),

/***/ "./node_modules/core-js/library/modules/_is-object.js":
/*!************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_is-object.js ***!
  \************************************************************/
/***/ ((module) => {

module.exports = function (it) {
  return typeof it === 'object' ? it !== null : typeof it === 'function';
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_object-dp.js":
/*!************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_object-dp.js ***!
  \************************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

var anObject = __webpack_require__(/*! ./_an-object */ "./node_modules/core-js/library/modules/_an-object.js");
var IE8_DOM_DEFINE = __webpack_require__(/*! ./_ie8-dom-define */ "./node_modules/core-js/library/modules/_ie8-dom-define.js");
var toPrimitive = __webpack_require__(/*! ./_to-primitive */ "./node_modules/core-js/library/modules/_to-primitive.js");
var dP = Object.defineProperty;

exports.f = __webpack_require__(/*! ./_descriptors */ "./node_modules/core-js/library/modules/_descriptors.js") ? Object.defineProperty : function defineProperty(O, P, Attributes) {
  anObject(O);
  P = toPrimitive(P, true);
  anObject(Attributes);
  if (IE8_DOM_DEFINE) try {
    return dP(O, P, Attributes);
  } catch (e) { /* empty */ }
  if ('get' in Attributes || 'set' in Attributes) throw TypeError('Accessors not supported!');
  if ('value' in Attributes) O[P] = Attributes.value;
  return O;
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_property-desc.js":
/*!****************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_property-desc.js ***!
  \****************************************************************/
/***/ ((module) => {

module.exports = function (bitmap, value) {
  return {
    enumerable: !(bitmap & 1),
    configurable: !(bitmap & 2),
    writable: !(bitmap & 4),
    value: value
  };
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_to-primitive.js":
/*!***************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_to-primitive.js ***!
  \***************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// 7.1.1 ToPrimitive(input [, PreferredType])
var isObject = __webpack_require__(/*! ./_is-object */ "./node_modules/core-js/library/modules/_is-object.js");
// instead of the ES6 spec version, we didn't implement @@toPrimitive case
// and the second argument - flag - preferred type is a string
module.exports = function (it, S) {
  if (!isObject(it)) return it;
  var fn, val;
  if (S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;
  if (typeof (fn = it.valueOf) == 'function' && !isObject(val = fn.call(it))) return val;
  if (!S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;
  throw TypeError("Can't convert object to primitive value");
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/es6.object.define-property.js":
/*!****************************************************************************!*\
  !*** ./node_modules/core-js/library/modules/es6.object.define-property.js ***!
  \****************************************************************************/
/***/ ((__unused_webpack_module, __unused_webpack_exports, __webpack_require__) => {

var $export = __webpack_require__(/*! ./_export */ "./node_modules/core-js/library/modules/_export.js");
// 19.1.2.4 / 15.2.3.6 Object.defineProperty(O, P, Attributes)
$export($export.S + $export.F * !__webpack_require__(/*! ./_descriptors */ "./node_modules/core-js/library/modules/_descriptors.js"), 'Object', { defineProperty: __webpack_require__(/*! ./_object-dp */ "./node_modules/core-js/library/modules/_object-dp.js").f });


/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be in strict mode.
(() => {
"use strict";
/*!**********************************!*\
  !*** ./js/pages/import/index.js ***!
  \**********************************/


var _ImportPage = __webpack_require__(/*! ./ImportPage */ "./js/pages/import/ImportPage.js");

var _ImportPage2 = _interopRequireDefault(_ImportPage);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _window = window,
    $ = _window.$; /**
                    * Copyright since 2007 PrestaShop SA and Contributors
                    * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
                    *
                    * NOTICE OF LICENSE
                    *
                    * This source file is subject to the Open Software License (OSL 3.0)
                    * that is bundled with this package in the file LICENSE.md.
                    * It is also available through the world-wide-web at this URL:
                    * https://opensource.org/licenses/OSL-3.0
                    * If you did not receive a copy of the license and are unable to
                    * obtain it through the world-wide-web, please send an email
                    * to license@prestashop.com so we can send you a copy immediately.
                    *
                    * DISCLAIMER
                    *
                    * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
                    * versions in the future. If you wish to customize PrestaShop for your
                    * needs please refer to https://devdocs.prestashop.com/ for more information.
                    *
                    * @author    PrestaShop SA and Contributors <contact@prestashop.com>
                    * @copyright Since 2007 PrestaShop SA and Contributors
                    * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
                    */

$(function () {
  new _ImportPage2.default();
});
})();

window.imports = __webpack_exports__;
/******/ })()
;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9bbmFtZV0vLi9qcy9wYWdlcy9pbXBvcnQvRm9ybUZpZWxkVG9nZ2xlLmpzIiwid2VicGFjazovL1tuYW1lXS8uL2pzL3BhZ2VzL2ltcG9ydC9JbXBvcnRQYWdlLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9iYWJlbC1ydW50aW1lL2NvcmUtanMvb2JqZWN0L2RlZmluZS1wcm9wZXJ0eS5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvYmFiZWwtcnVudGltZS9oZWxwZXJzL2NsYXNzQ2FsbENoZWNrLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9iYWJlbC1ydW50aW1lL2hlbHBlcnMvY3JlYXRlQ2xhc3MuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9mbi9vYmplY3QvZGVmaW5lLXByb3BlcnR5LmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fYS1mdW5jdGlvbi5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX2FuLW9iamVjdC5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX2NvcmUuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19jdHguanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19kZXNjcmlwdG9ycy5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX2RvbS1jcmVhdGUuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19leHBvcnQuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19mYWlscy5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX2dsb2JhbC5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX2hhcy5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX2hpZGUuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19pZTgtZG9tLWRlZmluZS5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX2lzLW9iamVjdC5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX29iamVjdC1kcC5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX3Byb3BlcnR5LWRlc2MuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL190by1wcmltaXRpdmUuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL2VzNi5vYmplY3QuZGVmaW5lLXByb3BlcnR5LmpzIiwid2VicGFjazovL1tuYW1lXS93ZWJwYWNrL2Jvb3RzdHJhcCIsIndlYnBhY2s6Ly9bbmFtZV0vLi9qcy9wYWdlcy9pbXBvcnQvaW5kZXguanMiXSwibmFtZXMiOlsid2luZG93IiwiJCIsImVudGl0eUNhdGVnb3JpZXMiLCJlbnRpdHlQcm9kdWN0cyIsImVudGl0eUNvbWJpbmF0aW9ucyIsImVudGl0eUN1c3RvbWVycyIsImVudGl0eUFkZHJlc3NlcyIsImVudGl0eUJyYW5kcyIsImVudGl0eVN1cHBsaWVycyIsImVudGl0eUFsaWFzIiwiZW50aXR5U3RvcmVDb250YWN0cyIsIkZvcm1GaWVsZFRvZ2dsZSIsIm9uIiwidG9nZ2xlRm9ybSIsInNlbGVjdGVkT3B0aW9uIiwiZmluZCIsInNlbGVjdGVkRW50aXR5IiwicGFyc2VJbnQiLCJ2YWwiLCJlbnRpdHlOYW1lIiwidGV4dCIsInRvTG93ZXJDYXNlIiwidG9nZ2xlRW50aXR5QWxlcnQiLCJ0b2dnbGVGaWVsZHMiLCJsb2FkQXZhaWxhYmxlRmllbGRzIiwiJGFsZXJ0IiwiaW5jbHVkZXMiLCJzaG93IiwiaGlkZSIsIiR0cnVuY2F0ZUZvcm1Hcm91cCIsIiRtYXRjaFJlZkZvcm1Hcm91cCIsIiRyZWdlbmVyYXRlRm9ybUdyb3VwIiwiJGZvcmNlSWRzRm9ybUdyb3VwIiwiJGVudGl0eU5hbWVQbGFjZWhvbGRlciIsImh0bWwiLCJlbnRpdHkiLCIkYXZhaWxhYmxlRmllbGRzIiwiYWpheCIsInVybCIsImRhdGEiLCJkYXRhVHlwZSIsInRoZW4iLCJyZXNwb25zZSIsInJlbW92ZUF2YWlsYWJsZUZpZWxkcyIsImkiLCJsZW5ndGgiLCJhcHBlbmRBdmFpbGFibGVGaWVsZCIsImxhYmVsIiwicmVxdWlyZWQiLCJkZXNjcmlwdGlvbiIsInBvcG92ZXIiLCIkY29udGFpbmVyIiwiZW1wdHkiLCIkZmllbGQiLCJoZWxwQm94Q29udGVudCIsIiRoZWxwQm94IiwiY2xvbmUiLCJhdHRyIiwicmVtb3ZlQ2xhc3MiLCJhcHBlbmQiLCIkYXBwZW5kVG8iLCJmaWVsZFRleHQiLCJhcHBlbmRIZWxwQm94IiwiYXBwZW5kVG8iLCJJbXBvcnRQYWdlIiwic2hvd0ZpbGVzSGlzdG9yeUhhbmRsZXIiLCJjbG9zZUZpbGVzSGlzdG9yeUhhbmRsZXIiLCJldmVudCIsInVzZUZpbGVGcm9tRmlsZXNIaXN0b3J5IiwiY2hhbmdlSW1wb3J0RmlsZUhhbmRsZXIiLCJ1cGxvYWRGaWxlIiwidG9nZ2xlU2VsZWN0ZWRGaWxlIiwiaGFuZGxlU3VibWl0IiwiJHRoaXMiLCJjb25maXJtIiwidHJpbSIsInNlbGVjdEZpbGVuYW1lIiwic2hvd0ltcG9ydEZpbGVBbGVydCIsImhpZGVGaWxlVXBsb2FkQmxvY2siLCJoaWRlSW1wb3J0RmlsZUFsZXJ0Iiwic2hvd0ZpbGVVcGxvYWRCbG9jayIsInNob3dGaWxlc0hpc3RvcnkiLCJjbG9zZUZpbGVzSGlzdG9yeSIsImFkZENsYXNzIiwiZmlsZW5hbWUiLCJ0YXJnZXQiLCJjbG9zZXN0IiwicmVtb3ZlQXR0ciIsImZpbGVOYW1lIiwiZmlsZVNpemUiLCJtZXNzYWdlIiwiZmlsZURhdGEiLCJodW1hbml6ZVNpemUiLCJieXRlcyIsInRvRml4ZWQiLCJoaWRlSW1wb3J0RmlsZUVycm9yIiwiJGlucHV0IiwidXBsb2FkZWRGaWxlIiwicHJvcCIsIm1heFVwbG9hZFNpemUiLCJzaXplIiwic2hvd0ltcG9ydEZpbGVFcnJvciIsIm5hbWUiLCJGb3JtRGF0YSIsInR5cGUiLCJjYWNoZSIsImNvbnRlbnRUeXBlIiwicHJvY2Vzc0RhdGEiLCJlcnJvciIsImZpbGUiLCJhZGRGaWxlVG9IaXN0b3J5VGFibGUiLCJlbmFibGVGaWxlc0hpc3RvcnlCdG4iLCIkdGFibGUiLCJiYXNlRGVsZXRlVXJsIiwiZGVsZXRlVXJsIiwiZW5jb2RlVVJJQ29tcG9uZW50IiwiYmFzZURvd25sb2FkVXJsIiwiZG93bmxvYWRVcmwiLCIkdGVtcGxhdGUiLCJmaWxlc051bWJlciJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztjQXlCWUEsTTtJQUFMQyxDLFdBQUFBLEM7OztBQUVQLElBQU1DLG1CQUFtQixDQUF6QjtBQUNBLElBQU1DLGlCQUFpQixDQUF2QjtBQUNBLElBQU1DLHFCQUFxQixDQUEzQjtBQUNBLElBQU1DLGtCQUFrQixDQUF4QjtBQUNBLElBQU1DLGtCQUFrQixDQUF4QjtBQUNBLElBQU1DLGVBQWUsQ0FBckI7QUFDQSxJQUFNQyxrQkFBa0IsQ0FBeEI7QUFDQSxJQUFNQyxjQUFjLENBQXBCO0FBQ0EsSUFBTUMsc0JBQXNCLENBQTVCOztJQUVxQkMsZTtBQUNuQiw2QkFBYztBQUFBOztBQUFBOztBQUNaVixNQUFFLG1CQUFGLEVBQXVCVyxFQUF2QixDQUEwQixRQUExQixFQUFvQztBQUFBLGFBQU0sTUFBS0MsVUFBTCxFQUFOO0FBQUEsS0FBcEM7O0FBRUEsU0FBS0EsVUFBTDtBQUNEOzs7O2lDQUVZO0FBQ1gsVUFBTUMsaUJBQWlCYixFQUFFLFNBQUYsRUFBYWMsSUFBYixDQUFrQixpQkFBbEIsQ0FBdkI7QUFDQSxVQUFNQyxpQkFBaUJDLFNBQVNILGVBQWVJLEdBQWYsRUFBVCxFQUErQixFQUEvQixDQUF2QjtBQUNBLFVBQU1DLGFBQWFMLGVBQWVNLElBQWYsR0FBc0JDLFdBQXRCLEVBQW5COztBQUVBLFdBQUtDLGlCQUFMLENBQXVCTixjQUF2QjtBQUNBLFdBQUtPLFlBQUwsQ0FBa0JQLGNBQWxCLEVBQWtDRyxVQUFsQztBQUNBLFdBQUtLLG1CQUFMLENBQXlCUixjQUF6QjtBQUNEOztBQUVEOzs7Ozs7OztzQ0FLa0JBLGMsRUFBZ0I7QUFDaEMsVUFBTVMsU0FBU3hCLEVBQUUsa0JBQUYsQ0FBZjs7QUFFQSxVQUFJLENBQUNDLGdCQUFELEVBQW1CQyxjQUFuQixFQUFtQ3VCLFFBQW5DLENBQTRDVixjQUE1QyxDQUFKLEVBQWlFO0FBQy9EUyxlQUFPRSxJQUFQO0FBQ0QsT0FGRCxNQUVPO0FBQ0xGLGVBQU9HLElBQVA7QUFDRDtBQUNGOztBQUVEOzs7Ozs7Ozs7aUNBTWFaLGMsRUFBZ0JHLFUsRUFBWTtBQUN2QyxVQUFNVSxxQkFBcUI1QixFQUFFLHlCQUFGLENBQTNCO0FBQ0EsVUFBTTZCLHFCQUFxQjdCLEVBQUUsMEJBQUYsQ0FBM0I7QUFDQSxVQUFNOEIsdUJBQXVCOUIsRUFBRSwyQkFBRixDQUE3QjtBQUNBLFVBQU0rQixxQkFBcUIvQixFQUFFLDBCQUFGLENBQTNCO0FBQ0EsVUFBTWdDLHlCQUF5QmhDLEVBQUUsaUJBQUYsQ0FBL0I7O0FBRUEsVUFBSVMsd0JBQXdCTSxjQUE1QixFQUE0QztBQUMxQ2EsMkJBQW1CRCxJQUFuQjtBQUNELE9BRkQsTUFFTztBQUNMQywyQkFBbUJGLElBQW5CO0FBQ0Q7O0FBRUQsVUFBSSxDQUFDeEIsY0FBRCxFQUFpQkMsa0JBQWpCLEVBQXFDc0IsUUFBckMsQ0FBOENWLGNBQTlDLENBQUosRUFBbUU7QUFDakVjLDJCQUFtQkgsSUFBbkI7QUFDRCxPQUZELE1BRU87QUFDTEcsMkJBQW1CRixJQUFuQjtBQUNEOztBQUVELFVBQUksQ0FDRjFCLGdCQURFLEVBRUZDLGNBRkUsRUFHRkksWUFIRSxFQUlGQyxlQUpFLEVBS0ZFLG1CQUxFLEVBTUZnQixRQU5FLENBTU9WLGNBTlAsQ0FBSixFQU9FO0FBQ0FlLDZCQUFxQkosSUFBckI7QUFDRCxPQVRELE1BU087QUFDTEksNkJBQXFCSCxJQUFyQjtBQUNEOztBQUVELFVBQUksQ0FDRjFCLGdCQURFLEVBRUZDLGNBRkUsRUFHRkUsZUFIRSxFQUlGQyxlQUpFLEVBS0ZDLFlBTEUsRUFNRkMsZUFORSxFQU9GRSxtQkFQRSxFQVFGRCxXQVJFLEVBU0ZpQixRQVRFLENBU09WLGNBVFAsQ0FBSixFQVVFO0FBQ0FnQiwyQkFBbUJMLElBQW5CO0FBQ0QsT0FaRCxNQVlPO0FBQ0xLLDJCQUFtQkosSUFBbkI7QUFDRDs7QUFFREssNkJBQXVCQyxJQUF2QixDQUE0QmYsVUFBNUI7QUFDRDs7QUFFRDs7Ozs7Ozs7d0NBS29CZ0IsTSxFQUFRO0FBQUE7O0FBQzFCLFVBQU1DLG1CQUFtQm5DLEVBQUUsc0JBQUYsQ0FBekI7O0FBRUFBLFFBQUVvQyxJQUFGLENBQU87QUFDTEMsYUFBS0YsaUJBQWlCRyxJQUFqQixDQUFzQixLQUF0QixDQURBO0FBRUxBLGNBQU07QUFDSko7QUFESSxTQUZEO0FBS0xLLGtCQUFVO0FBTEwsT0FBUCxFQU1HQyxJQU5ILENBTVEsVUFBQ0MsUUFBRCxFQUFjO0FBQ3BCLGVBQUtDLHFCQUFMLENBQTJCUCxnQkFBM0I7O0FBRUEsYUFBSyxJQUFJUSxJQUFJLENBQWIsRUFBZ0JBLElBQUlGLFNBQVNHLE1BQTdCLEVBQXFDRCxLQUFLLENBQTFDLEVBQTZDO0FBQzNDLGlCQUFLRSxvQkFBTCxDQUNFVixnQkFERixFQUVFTSxTQUFTRSxDQUFULEVBQVlHLEtBQVosSUFBcUJMLFNBQVNFLENBQVQsRUFBWUksUUFBWixHQUF1QixHQUF2QixHQUE2QixFQUFsRCxDQUZGLEVBR0VOLFNBQVNFLENBQVQsRUFBWUssV0FIZDtBQUtEOztBQUVEYix5QkFBaUJyQixJQUFqQixDQUFzQix5QkFBdEIsRUFBaURtQyxPQUFqRDtBQUNELE9BbEJEO0FBbUJEOztBQUVEOzs7Ozs7Ozs7MENBTXNCQyxVLEVBQVk7QUFDaENBLGlCQUFXcEMsSUFBWCxDQUFnQix5QkFBaEIsRUFBMkNtQyxPQUEzQyxDQUFtRCxNQUFuRDtBQUNBQyxpQkFBV0MsS0FBWDtBQUNEOztBQUVEOzs7Ozs7Ozs7O2tDQU9jQyxNLEVBQVFDLGMsRUFBZ0I7QUFDcEMsVUFBTUMsV0FBV3RELEVBQUUsc0NBQUYsRUFBMEN1RCxLQUExQyxFQUFqQjs7QUFFQUQsZUFBU0UsSUFBVCxDQUFjLGNBQWQsRUFBOEJILGNBQTlCO0FBQ0FDLGVBQVNHLFdBQVQsQ0FBcUIsNENBQXJCO0FBQ0FMLGFBQU9NLE1BQVAsQ0FBY0osUUFBZDtBQUNEOztBQUVEOzs7Ozs7Ozs7Ozt5Q0FRcUJLLFMsRUFBV0MsUyxFQUFXUCxjLEVBQWdCO0FBQ3pELFVBQU1ELFNBQVNwRCxFQUFFLDhCQUFGLEVBQWtDdUQsS0FBbEMsRUFBZjs7QUFFQUgsYUFBT2pDLElBQVAsQ0FBWXlDLFNBQVo7O0FBRUEsVUFBSVAsY0FBSixFQUFvQjtBQUNsQjtBQUNBLGFBQUtRLGFBQUwsQ0FBbUJULE1BQW5CLEVBQTJCQyxjQUEzQjtBQUNEOztBQUVERCxhQUFPSyxXQUFQLENBQW1CLG9DQUFuQjtBQUNBTCxhQUFPVSxRQUFQLENBQWdCSCxTQUFoQjtBQUNEOzs7OztrQkFwS2tCakQsZTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ1pyQjs7Ozs7O2NBRVlYLE07SUFBTEMsQyxXQUFBQSxDLEVBM0JQOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0lBNkJxQitELFU7QUFDbkIsd0JBQWM7QUFBQTs7QUFBQTs7QUFDWixRQUFJckQseUJBQUo7O0FBRUFWLE1BQUUsNEJBQUYsRUFBZ0NXLEVBQWhDLENBQW1DLE9BQW5DLEVBQTRDO0FBQUEsYUFBTSxNQUFLcUQsdUJBQUwsRUFBTjtBQUFBLEtBQTVDO0FBQ0FoRSxNQUFFLG1DQUFGLEVBQXVDVyxFQUF2QyxDQUEwQyxPQUExQyxFQUFtRDtBQUFBLGFBQU0sTUFBS3NELHdCQUFMLEVBQU47QUFBQSxLQUFuRDtBQUNBakUsTUFBRSxtQkFBRixFQUF1QlcsRUFBdkIsQ0FBMEIsT0FBMUIsRUFBbUMsa0JBQW5DLEVBQXVELFVBQUN1RCxLQUFEO0FBQUEsYUFBVyxNQUFLQyx1QkFBTCxDQUE2QkQsS0FBN0IsQ0FBWDtBQUFBLEtBQXZEO0FBQ0FsRSxNQUFFLDRCQUFGLEVBQWdDVyxFQUFoQyxDQUFtQyxPQUFuQyxFQUE0QztBQUFBLGFBQU0sTUFBS3lELHVCQUFMLEVBQU47QUFBQSxLQUE1QztBQUNBcEUsTUFBRSxpQkFBRixFQUFxQlcsRUFBckIsQ0FBd0IsUUFBeEIsRUFBa0M7QUFBQSxhQUFNLE1BQUswRCxVQUFMLEVBQU47QUFBQSxLQUFsQzs7QUFFQSxTQUFLQyxrQkFBTDtBQUNBLFNBQUtDLFlBQUw7QUFDRDs7QUFFRDs7Ozs7Ozs7bUNBSWU7QUFDYnZFLFFBQUUsaUJBQUYsRUFBcUJXLEVBQXJCLENBQXdCLFFBQXhCLEVBQWtDLFlBQVk7QUFDNUMsWUFBTTZELFFBQVF4RSxFQUFFLElBQUYsQ0FBZDs7QUFFQSxZQUFJd0UsTUFBTTFELElBQU4sQ0FBVyxnQ0FBWCxFQUE2Q0csR0FBN0MsT0FBdUQsR0FBM0QsRUFBZ0U7QUFDOUQ7QUFDQSxpQkFBT2xCLE9BQU8wRSxPQUFQLENBQWtCRCxNQUFNbEMsSUFBTixDQUFXLHdCQUFYLENBQWxCLFNBQTBEdEMsRUFBRTBFLElBQUYsQ0FBTzFFLEVBQUUsMkJBQUYsRUFBK0JtQixJQUEvQixHQUFzQ0MsV0FBdEMsRUFBUCxDQUExRCxPQUFQO0FBQ0Q7O0FBRUQsZUFBTyxJQUFQO0FBQ0QsT0FURDtBQVVEOztBQUVEOzs7Ozs7eUNBR3FCO0FBQ25CLFVBQU11RCxpQkFBaUIzRSxFQUFFLE1BQUYsRUFBVWlCLEdBQVYsRUFBdkI7O0FBRUEsVUFBSTBELGVBQWUvQixNQUFmLEdBQXdCLENBQTVCLEVBQStCO0FBQzdCLGFBQUtnQyxtQkFBTCxDQUF5QkQsY0FBekI7QUFDQSxhQUFLRSxtQkFBTDtBQUNEO0FBQ0Y7Ozs4Q0FFeUI7QUFDeEIsV0FBS0MsbUJBQUw7QUFDQSxXQUFLQyxtQkFBTDtBQUNEOztBQUVEOzs7Ozs7OENBRzBCO0FBQ3hCLFdBQUtDLGdCQUFMO0FBQ0EsV0FBS0gsbUJBQUw7QUFDRDs7QUFFRDs7Ozs7OytDQUcyQjtBQUN6QixXQUFLSSxpQkFBTDtBQUNBLFdBQUtGLG1CQUFMO0FBQ0Q7O0FBRUQ7Ozs7Ozt1Q0FHbUI7QUFDakIvRSxRQUFFLHlCQUFGLEVBQTZCeUQsV0FBN0IsQ0FBeUMsUUFBekM7QUFDRDs7QUFFRDs7Ozs7O3dDQUdvQjtBQUNsQnpELFFBQUUseUJBQUYsRUFBNkJrRixRQUE3QixDQUFzQyxRQUF0QztBQUNEOztBQUVEOzs7Ozs7NENBR3dCaEIsSyxFQUFPO0FBQzdCLFVBQU1pQixXQUFXbkYsRUFBRWtFLE1BQU1rQixNQUFSLEVBQWdCQyxPQUFoQixDQUF3QixZQUF4QixFQUFzQy9DLElBQXRDLENBQTJDLE1BQTNDLENBQWpCOztBQUVBdEMsUUFBRSx1QkFBRixFQUEyQmlCLEdBQTNCLENBQStCa0UsUUFBL0I7O0FBRUEsV0FBS1AsbUJBQUwsQ0FBeUJPLFFBQXpCO0FBQ0EsV0FBS0YsaUJBQUw7QUFDRDs7QUFFRDs7Ozs7O3dDQUdvQkUsUSxFQUFVO0FBQzVCbkYsUUFBRSx1QkFBRixFQUEyQnlELFdBQTNCLENBQXVDLFFBQXZDO0FBQ0F6RCxRQUFFLGlCQUFGLEVBQXFCbUIsSUFBckIsQ0FBMEJnRSxRQUExQjtBQUNEOztBQUVEOzs7Ozs7MENBR3NCO0FBQ3BCbkYsUUFBRSx1QkFBRixFQUEyQmtGLFFBQTNCLENBQW9DLFFBQXBDO0FBQ0Q7O0FBRUQ7Ozs7OzswQ0FHc0I7QUFDcEJsRixRQUFFLDRCQUFGLEVBQWdDa0YsUUFBaEMsQ0FBeUMsUUFBekM7QUFDRDs7QUFFRDs7Ozs7OzBDQUdzQjtBQUNwQmxGLFFBQUUsNEJBQUYsRUFBZ0N5RCxXQUFoQyxDQUE0QyxRQUE1QztBQUNEOztBQUVEOzs7Ozs7NENBR3dCO0FBQ3RCekQsUUFBRSw0QkFBRixFQUFnQ3NGLFVBQWhDLENBQTJDLFVBQTNDO0FBQ0Q7O0FBRUQ7Ozs7Ozs7Ozs7d0NBT29CQyxRLEVBQVVDLFEsRUFBVUMsTyxFQUFTO0FBQy9DLFVBQU1qRSxTQUFTeEIsRUFBRSx1QkFBRixDQUFmOztBQUVBLFVBQU0wRixXQUFjSCxRQUFkLFVBQTJCLEtBQUtJLFlBQUwsQ0FBa0JILFFBQWxCLENBQTNCLE1BQU47O0FBRUFoRSxhQUFPVixJQUFQLENBQVksZUFBWixFQUE2QkssSUFBN0IsQ0FBa0N1RSxRQUFsQztBQUNBbEUsYUFBT1YsSUFBUCxDQUFZLG1CQUFaLEVBQWlDSyxJQUFqQyxDQUFzQ3NFLE9BQXRDO0FBQ0FqRSxhQUFPaUMsV0FBUCxDQUFtQixRQUFuQjtBQUNEOztBQUVEOzs7Ozs7MENBR3NCO0FBQ3BCLFVBQU1qQyxTQUFTeEIsRUFBRSx1QkFBRixDQUFmO0FBQ0F3QixhQUFPMEQsUUFBUCxDQUFnQixRQUFoQjtBQUNEOztBQUVEOzs7Ozs7Ozs7O2lDQU9hVSxLLEVBQU87QUFDbEIsVUFBSSxPQUFPQSxLQUFQLEtBQWlCLFFBQXJCLEVBQStCO0FBQzdCLGVBQU8sRUFBUDtBQUNEOztBQUVELFVBQUlBLFNBQVMsVUFBYixFQUF5QjtBQUN2QixlQUFVLENBQUNBLFFBQVEsVUFBVCxFQUFxQkMsT0FBckIsQ0FBNkIsQ0FBN0IsQ0FBVjtBQUNEOztBQUVELFVBQUlELFNBQVMsT0FBYixFQUFzQjtBQUNwQixlQUFVLENBQUNBLFFBQVEsT0FBVCxFQUFrQkMsT0FBbEIsQ0FBMEIsQ0FBMUIsQ0FBVjtBQUNEOztBQUVELGFBQVUsQ0FBQ0QsUUFBUSxJQUFULEVBQWVDLE9BQWYsQ0FBdUIsQ0FBdkIsQ0FBVjtBQUNEOztBQUVEOzs7Ozs7aUNBR2E7QUFBQTs7QUFDWCxXQUFLQyxtQkFBTDs7QUFFQSxVQUFNQyxTQUFTL0YsRUFBRSxPQUFGLENBQWY7QUFDQSxVQUFNZ0csZUFBZUQsT0FBT0UsSUFBUCxDQUFZLE9BQVosRUFBcUIsQ0FBckIsQ0FBckI7O0FBRUEsVUFBTUMsZ0JBQWdCSCxPQUFPekQsSUFBUCxDQUFZLHNCQUFaLENBQXRCOztBQUVBLFVBQUk0RCxnQkFBZ0JGLGFBQWFHLElBQWpDLEVBQXVDO0FBQ3JDLGFBQUtDLG1CQUFMLENBQXlCSixhQUFhSyxJQUF0QyxFQUE0Q0wsYUFBYUcsSUFBekQsRUFBK0QsbUJBQS9EO0FBQ0E7QUFDRDs7QUFFRCxVQUFNN0QsT0FBTyxJQUFJZ0UsUUFBSixFQUFiO0FBQ0FoRSxXQUFLb0IsTUFBTCxDQUFZLE1BQVosRUFBb0JzQyxZQUFwQjs7QUFFQWhHLFFBQUVvQyxJQUFGLENBQU87QUFDTG1FLGNBQU0sTUFERDtBQUVMbEUsYUFBS3JDLEVBQUUsaUJBQUYsRUFBcUJzQyxJQUFyQixDQUEwQixpQkFBMUIsQ0FGQTtBQUdMQSxrQkFISztBQUlMa0UsZUFBTyxLQUpGO0FBS0xDLHFCQUFhLEtBTFI7QUFNTEMscUJBQWE7QUFOUixPQUFQLEVBT0dsRSxJQVBILENBT1EsVUFBQ0MsUUFBRCxFQUFjO0FBQ3BCLFlBQUlBLFNBQVNrRSxLQUFiLEVBQW9CO0FBQ2xCLGlCQUFLUCxtQkFBTCxDQUF5QkosYUFBYUssSUFBdEMsRUFBNENMLGFBQWFHLElBQXpELEVBQStEMUQsU0FBU2tFLEtBQXhFO0FBQ0E7QUFDRDs7QUFFRCxZQUFNeEIsV0FBVzFDLFNBQVNtRSxJQUFULENBQWNQLElBQS9COztBQUVBckcsVUFBRSx1QkFBRixFQUEyQmlCLEdBQTNCLENBQStCa0UsUUFBL0I7O0FBRUEsZUFBS1AsbUJBQUwsQ0FBeUJPLFFBQXpCO0FBQ0EsZUFBS04sbUJBQUw7QUFDQSxlQUFLZ0MscUJBQUwsQ0FBMkIxQixRQUEzQjtBQUNBLGVBQUsyQixxQkFBTDtBQUNELE9BckJEO0FBc0JEOztBQUVEOzs7Ozs7OzswQ0FLc0IzQixRLEVBQVU7QUFDOUIsVUFBTTRCLFNBQVMvRyxFQUFFLG1CQUFGLENBQWY7O0FBRUEsVUFBTWdILGdCQUFnQkQsT0FBT3pFLElBQVAsQ0FBWSxpQkFBWixDQUF0QjtBQUNBLFVBQU0yRSxZQUFlRCxhQUFmLGtCQUF5Q0UsbUJBQW1CL0IsUUFBbkIsQ0FBL0M7O0FBRUEsVUFBTWdDLGtCQUFrQkosT0FBT3pFLElBQVAsQ0FBWSxtQkFBWixDQUF4QjtBQUNBLFVBQU04RSxjQUFpQkQsZUFBakIsa0JBQTZDRCxtQkFBbUIvQixRQUFuQixDQUFuRDs7QUFFQSxVQUFNa0MsWUFBWU4sT0FBT2pHLElBQVAsQ0FBWSxVQUFaLEVBQXdCeUMsS0FBeEIsRUFBbEI7O0FBRUE4RCxnQkFBVTVELFdBQVYsQ0FBc0IsUUFBdEI7QUFDQTRELGdCQUFVdkcsSUFBVixDQUFlLFVBQWYsRUFBMkJLLElBQTNCLENBQWdDZ0UsUUFBaEM7QUFDQWtDLGdCQUFVdkcsSUFBVixDQUFlLFlBQWYsRUFBNkIwQyxJQUE3QixDQUFrQyxXQUFsQyxFQUErQzJCLFFBQS9DO0FBQ0FrQyxnQkFBVXZHLElBQVYsQ0FBZSxxQkFBZixFQUFzQzBDLElBQXRDLENBQTJDLE1BQTNDLEVBQW1EeUQsU0FBbkQ7QUFDQUksZ0JBQVV2RyxJQUFWLENBQWUsdUJBQWYsRUFBd0MwQyxJQUF4QyxDQUE2QyxNQUE3QyxFQUFxRDRELFdBQXJEOztBQUVBTCxhQUFPakcsSUFBUCxDQUFZLE9BQVosRUFBcUI0QyxNQUFyQixDQUE0QjJELFNBQTVCOztBQUVBLFVBQU1DLGNBQWNQLE9BQU9qRyxJQUFQLENBQVksSUFBWixFQUFrQjhCLE1BQWxCLEdBQTJCLENBQS9DO0FBQ0E1QyxRQUFFLDBCQUFGLEVBQThCbUIsSUFBOUIsQ0FBbUNtRyxXQUFuQztBQUNEOzs7OztrQkFuUGtCdkQsVTs7Ozs7Ozs7OztBQzdCckIsa0JBQWtCLFlBQVksbUJBQU8sQ0FBQyw4R0FBMkMsc0I7Ozs7Ozs7Ozs7O0FDQXBFOztBQUViLGtCQUFrQjs7QUFFbEIsZUFBZTtBQUNmO0FBQ0E7QUFDQTtBQUNBLEU7Ozs7Ozs7Ozs7O0FDUmE7O0FBRWIsa0JBQWtCOztBQUVsQixzQkFBc0IsbUJBQU8sQ0FBQyx5R0FBbUM7O0FBRWpFOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixlQUFlO0FBQ2Y7QUFDQSxtQkFBbUIsa0JBQWtCO0FBQ3JDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDLEc7Ozs7Ozs7Ozs7QUMxQkQsbUJBQU8sQ0FBQyxzSEFBMEM7QUFDbEQsY0FBYyx3R0FBcUM7QUFDbkQ7QUFDQTtBQUNBOzs7Ozs7Ozs7OztBQ0pBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7OztBQ0hBLGVBQWUsbUJBQU8sQ0FBQywwRUFBYztBQUNyQztBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNKQSw2QkFBNkI7QUFDN0IsdUNBQXVDOzs7Ozs7Ozs7OztBQ0R2QztBQUNBLGdCQUFnQixtQkFBTyxDQUFDLDRFQUFlO0FBQ3ZDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNuQkE7QUFDQSxrQkFBa0IsbUJBQU8sQ0FBQyxrRUFBVTtBQUNwQyxpQ0FBaUMsUUFBUSxtQkFBbUIsVUFBVSxFQUFFLEVBQUU7QUFDMUUsQ0FBQzs7Ozs7Ozs7Ozs7QUNIRCxlQUFlLG1CQUFPLENBQUMsMEVBQWM7QUFDckMsZUFBZSxrR0FBNkI7QUFDNUM7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNOQSxhQUFhLG1CQUFPLENBQUMsb0VBQVc7QUFDaEMsV0FBVyxtQkFBTyxDQUFDLGdFQUFTO0FBQzVCLFVBQVUsbUJBQU8sQ0FBQyw4REFBUTtBQUMxQixXQUFXLG1CQUFPLENBQUMsZ0VBQVM7QUFDNUIsVUFBVSxtQkFBTyxDQUFDLDhEQUFRO0FBQzFCOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUVBQWlFO0FBQ2pFO0FBQ0Esa0ZBQWtGO0FBQ2xGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXO0FBQ1gsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSwrQ0FBK0M7QUFDL0M7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsY0FBYztBQUNkLGNBQWM7QUFDZCxjQUFjO0FBQ2QsY0FBYztBQUNkLGVBQWU7QUFDZixlQUFlO0FBQ2YsZUFBZTtBQUNmLGdCQUFnQjtBQUNoQjs7Ozs7Ozs7Ozs7QUM3REE7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNOQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EseUNBQXlDOzs7Ozs7Ozs7OztBQ0x6Qyx1QkFBdUI7QUFDdkI7QUFDQTtBQUNBOzs7Ozs7Ozs7OztBQ0hBLFNBQVMsbUJBQU8sQ0FBQywwRUFBYztBQUMvQixpQkFBaUIsbUJBQU8sQ0FBQyxrRkFBa0I7QUFDM0MsaUJBQWlCLG1CQUFPLENBQUMsOEVBQWdCO0FBQ3pDO0FBQ0EsQ0FBQztBQUNEO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNQQSxrQkFBa0IsbUJBQU8sQ0FBQyw4RUFBZ0IsTUFBTSxtQkFBTyxDQUFDLGtFQUFVO0FBQ2xFLCtCQUErQixtQkFBTyxDQUFDLDRFQUFlLGdCQUFnQixtQkFBbUIsVUFBVSxFQUFFLEVBQUU7QUFDdkcsQ0FBQzs7Ozs7Ozs7Ozs7QUNGRDtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7O0FDRkEsZUFBZSxtQkFBTyxDQUFDLDBFQUFjO0FBQ3JDLHFCQUFxQixtQkFBTyxDQUFDLG9GQUFtQjtBQUNoRCxrQkFBa0IsbUJBQU8sQ0FBQyxnRkFBaUI7QUFDM0M7O0FBRUEsU0FBUyxHQUFHLG1CQUFPLENBQUMsOEVBQWdCO0FBQ3BDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHLFlBQVk7QUFDZjtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNmQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7OztBQ1BBO0FBQ0EsZUFBZSxtQkFBTyxDQUFDLDBFQUFjO0FBQ3JDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7OztBQ1hBLGNBQWMsbUJBQU8sQ0FBQyxvRUFBVztBQUNqQztBQUNBLGlDQUFpQyxtQkFBTyxDQUFDLDhFQUFnQixjQUFjLGlCQUFpQixpR0FBeUIsRUFBRTs7Ozs7OztVQ0ZuSDtVQUNBOztVQUVBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBOztVQUVBO1VBQ0E7O1VBRUE7VUFDQTtVQUNBOzs7Ozs7Ozs7Ozs7QUNHQTs7Ozs7O2NBRVloRSxNO0lBQUxDLEMsV0FBQUEsQyxFQTNCUDs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQTZCQUEsRUFBRSxZQUFNO0FBQ04sTUFBSStELG9CQUFKO0FBQ0QsQ0FGRCxFIiwiZmlsZSI6ImltcG9ydHMuYnVuZGxlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiBDb3B5cmlnaHQgc2luY2UgMjAwNyBQcmVzdGFTaG9wIFNBIGFuZCBDb250cmlidXRvcnNcbiAqIFByZXN0YVNob3AgaXMgYW4gSW50ZXJuYXRpb25hbCBSZWdpc3RlcmVkIFRyYWRlbWFyayAmIFByb3BlcnR5IG9mIFByZXN0YVNob3AgU0FcbiAqXG4gKiBOT1RJQ0UgT0YgTElDRU5TRVxuICpcbiAqIFRoaXMgc291cmNlIGZpbGUgaXMgc3ViamVjdCB0byB0aGUgT3BlbiBTb2Z0d2FyZSBMaWNlbnNlIChPU0wgMy4wKVxuICogdGhhdCBpcyBidW5kbGVkIHdpdGggdGhpcyBwYWNrYWdlIGluIHRoZSBmaWxlIExJQ0VOU0UubWQuXG4gKiBJdCBpcyBhbHNvIGF2YWlsYWJsZSB0aHJvdWdoIHRoZSB3b3JsZC13aWRlLXdlYiBhdCB0aGlzIFVSTDpcbiAqIGh0dHBzOi8vb3BlbnNvdXJjZS5vcmcvbGljZW5zZXMvT1NMLTMuMFxuICogSWYgeW91IGRpZCBub3QgcmVjZWl2ZSBhIGNvcHkgb2YgdGhlIGxpY2Vuc2UgYW5kIGFyZSB1bmFibGUgdG9cbiAqIG9idGFpbiBpdCB0aHJvdWdoIHRoZSB3b3JsZC13aWRlLXdlYiwgcGxlYXNlIHNlbmQgYW4gZW1haWxcbiAqIHRvIGxpY2Vuc2VAcHJlc3Rhc2hvcC5jb20gc28gd2UgY2FuIHNlbmQgeW91IGEgY29weSBpbW1lZGlhdGVseS5cbiAqXG4gKiBESVNDTEFJTUVSXG4gKlxuICogRG8gbm90IGVkaXQgb3IgYWRkIHRvIHRoaXMgZmlsZSBpZiB5b3Ugd2lzaCB0byB1cGdyYWRlIFByZXN0YVNob3AgdG8gbmV3ZXJcbiAqIHZlcnNpb25zIGluIHRoZSBmdXR1cmUuIElmIHlvdSB3aXNoIHRvIGN1c3RvbWl6ZSBQcmVzdGFTaG9wIGZvciB5b3VyXG4gKiBuZWVkcyBwbGVhc2UgcmVmZXIgdG8gaHR0cHM6Ly9kZXZkb2NzLnByZXN0YXNob3AuY29tLyBmb3IgbW9yZSBpbmZvcm1hdGlvbi5cbiAqXG4gKiBAYXV0aG9yICAgIFByZXN0YVNob3AgU0EgYW5kIENvbnRyaWJ1dG9ycyA8Y29udGFjdEBwcmVzdGFzaG9wLmNvbT5cbiAqIEBjb3B5cmlnaHQgU2luY2UgMjAwNyBQcmVzdGFTaG9wIFNBIGFuZCBDb250cmlidXRvcnNcbiAqIEBsaWNlbnNlICAgaHR0cHM6Ly9vcGVuc291cmNlLm9yZy9saWNlbnNlcy9PU0wtMy4wIE9wZW4gU29mdHdhcmUgTGljZW5zZSAoT1NMIDMuMClcbiAqL1xuXG5jb25zdCB7JH0gPSB3aW5kb3c7XG5cbmNvbnN0IGVudGl0eUNhdGVnb3JpZXMgPSAwO1xuY29uc3QgZW50aXR5UHJvZHVjdHMgPSAxO1xuY29uc3QgZW50aXR5Q29tYmluYXRpb25zID0gMjtcbmNvbnN0IGVudGl0eUN1c3RvbWVycyA9IDM7XG5jb25zdCBlbnRpdHlBZGRyZXNzZXMgPSA0O1xuY29uc3QgZW50aXR5QnJhbmRzID0gNTtcbmNvbnN0IGVudGl0eVN1cHBsaWVycyA9IDY7XG5jb25zdCBlbnRpdHlBbGlhcyA9IDc7XG5jb25zdCBlbnRpdHlTdG9yZUNvbnRhY3RzID0gODtcblxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgRm9ybUZpZWxkVG9nZ2xlIHtcbiAgY29uc3RydWN0b3IoKSB7XG4gICAgJCgnLmpzLWVudGl0eS1zZWxlY3QnKS5vbignY2hhbmdlJywgKCkgPT4gdGhpcy50b2dnbGVGb3JtKCkpO1xuXG4gICAgdGhpcy50b2dnbGVGb3JtKCk7XG4gIH1cblxuICB0b2dnbGVGb3JtKCkge1xuICAgIGNvbnN0IHNlbGVjdGVkT3B0aW9uID0gJCgnI2VudGl0eScpLmZpbmQoJ29wdGlvbjpzZWxlY3RlZCcpO1xuICAgIGNvbnN0IHNlbGVjdGVkRW50aXR5ID0gcGFyc2VJbnQoc2VsZWN0ZWRPcHRpb24udmFsKCksIDEwKTtcbiAgICBjb25zdCBlbnRpdHlOYW1lID0gc2VsZWN0ZWRPcHRpb24udGV4dCgpLnRvTG93ZXJDYXNlKCk7XG5cbiAgICB0aGlzLnRvZ2dsZUVudGl0eUFsZXJ0KHNlbGVjdGVkRW50aXR5KTtcbiAgICB0aGlzLnRvZ2dsZUZpZWxkcyhzZWxlY3RlZEVudGl0eSwgZW50aXR5TmFtZSk7XG4gICAgdGhpcy5sb2FkQXZhaWxhYmxlRmllbGRzKHNlbGVjdGVkRW50aXR5KTtcbiAgfVxuXG4gIC8qKlxuICAgKiBUb2dnbGUgYWxlcnQgd2FybmluZyBmb3Igc2VsZWN0ZWQgaW1wb3J0IGVudGl0eVxuICAgKlxuICAgKiBAcGFyYW0ge2ludH0gc2VsZWN0ZWRFbnRpdHlcbiAgICovXG4gIHRvZ2dsZUVudGl0eUFsZXJ0KHNlbGVjdGVkRW50aXR5KSB7XG4gICAgY29uc3QgJGFsZXJ0ID0gJCgnLmpzLWVudGl0eS1hbGVydCcpO1xuXG4gICAgaWYgKFtlbnRpdHlDYXRlZ29yaWVzLCBlbnRpdHlQcm9kdWN0c10uaW5jbHVkZXMoc2VsZWN0ZWRFbnRpdHkpKSB7XG4gICAgICAkYWxlcnQuc2hvdygpO1xuICAgIH0gZWxzZSB7XG4gICAgICAkYWxlcnQuaGlkZSgpO1xuICAgIH1cbiAgfVxuXG4gIC8qKlxuICAgKiBUb2dnbGUgYXZhaWxhYmxlIG9wdGlvbnMgZm9yIHNlbGVjdGVkIGVudGl0eVxuICAgKlxuICAgKiBAcGFyYW0ge2ludH0gc2VsZWN0ZWRFbnRpdHlcbiAgICogQHBhcmFtIHtzdHJpbmd9IGVudGl0eU5hbWVcbiAgICovXG4gIHRvZ2dsZUZpZWxkcyhzZWxlY3RlZEVudGl0eSwgZW50aXR5TmFtZSkge1xuICAgIGNvbnN0ICR0cnVuY2F0ZUZvcm1Hcm91cCA9ICQoJy5qcy10cnVuY2F0ZS1mb3JtLWdyb3VwJyk7XG4gICAgY29uc3QgJG1hdGNoUmVmRm9ybUdyb3VwID0gJCgnLmpzLW1hdGNoLXJlZi1mb3JtLWdyb3VwJyk7XG4gICAgY29uc3QgJHJlZ2VuZXJhdGVGb3JtR3JvdXAgPSAkKCcuanMtcmVnZW5lcmF0ZS1mb3JtLWdyb3VwJyk7XG4gICAgY29uc3QgJGZvcmNlSWRzRm9ybUdyb3VwID0gJCgnLmpzLWZvcmNlLWlkcy1mb3JtLWdyb3VwJyk7XG4gICAgY29uc3QgJGVudGl0eU5hbWVQbGFjZWhvbGRlciA9ICQoJy5qcy1lbnRpdHktbmFtZScpO1xuXG4gICAgaWYgKGVudGl0eVN0b3JlQ29udGFjdHMgPT09IHNlbGVjdGVkRW50aXR5KSB7XG4gICAgICAkdHJ1bmNhdGVGb3JtR3JvdXAuaGlkZSgpO1xuICAgIH0gZWxzZSB7XG4gICAgICAkdHJ1bmNhdGVGb3JtR3JvdXAuc2hvdygpO1xuICAgIH1cblxuICAgIGlmIChbZW50aXR5UHJvZHVjdHMsIGVudGl0eUNvbWJpbmF0aW9uc10uaW5jbHVkZXMoc2VsZWN0ZWRFbnRpdHkpKSB7XG4gICAgICAkbWF0Y2hSZWZGb3JtR3JvdXAuc2hvdygpO1xuICAgIH0gZWxzZSB7XG4gICAgICAkbWF0Y2hSZWZGb3JtR3JvdXAuaGlkZSgpO1xuICAgIH1cblxuICAgIGlmIChbXG4gICAgICBlbnRpdHlDYXRlZ29yaWVzLFxuICAgICAgZW50aXR5UHJvZHVjdHMsXG4gICAgICBlbnRpdHlCcmFuZHMsXG4gICAgICBlbnRpdHlTdXBwbGllcnMsXG4gICAgICBlbnRpdHlTdG9yZUNvbnRhY3RzLFxuICAgIF0uaW5jbHVkZXMoc2VsZWN0ZWRFbnRpdHkpXG4gICAgKSB7XG4gICAgICAkcmVnZW5lcmF0ZUZvcm1Hcm91cC5zaG93KCk7XG4gICAgfSBlbHNlIHtcbiAgICAgICRyZWdlbmVyYXRlRm9ybUdyb3VwLmhpZGUoKTtcbiAgICB9XG5cbiAgICBpZiAoW1xuICAgICAgZW50aXR5Q2F0ZWdvcmllcyxcbiAgICAgIGVudGl0eVByb2R1Y3RzLFxuICAgICAgZW50aXR5Q3VzdG9tZXJzLFxuICAgICAgZW50aXR5QWRkcmVzc2VzLFxuICAgICAgZW50aXR5QnJhbmRzLFxuICAgICAgZW50aXR5U3VwcGxpZXJzLFxuICAgICAgZW50aXR5U3RvcmVDb250YWN0cyxcbiAgICAgIGVudGl0eUFsaWFzLFxuICAgIF0uaW5jbHVkZXMoc2VsZWN0ZWRFbnRpdHkpXG4gICAgKSB7XG4gICAgICAkZm9yY2VJZHNGb3JtR3JvdXAuc2hvdygpO1xuICAgIH0gZWxzZSB7XG4gICAgICAkZm9yY2VJZHNGb3JtR3JvdXAuaGlkZSgpO1xuICAgIH1cblxuICAgICRlbnRpdHlOYW1lUGxhY2Vob2xkZXIuaHRtbChlbnRpdHlOYW1lKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBMb2FkIGF2YWlsYWJsZSBmaWVsZHMgZm9yIGdpdmVuIGVudGl0eVxuICAgKlxuICAgKiBAcGFyYW0ge2ludH0gZW50aXR5XG4gICAqL1xuICBsb2FkQXZhaWxhYmxlRmllbGRzKGVudGl0eSkge1xuICAgIGNvbnN0ICRhdmFpbGFibGVGaWVsZHMgPSAkKCcuanMtYXZhaWxhYmxlLWZpZWxkcycpO1xuXG4gICAgJC5hamF4KHtcbiAgICAgIHVybDogJGF2YWlsYWJsZUZpZWxkcy5kYXRhKCd1cmwnKSxcbiAgICAgIGRhdGE6IHtcbiAgICAgICAgZW50aXR5LFxuICAgICAgfSxcbiAgICAgIGRhdGFUeXBlOiAnanNvbicsXG4gICAgfSkudGhlbigocmVzcG9uc2UpID0+IHtcbiAgICAgIHRoaXMucmVtb3ZlQXZhaWxhYmxlRmllbGRzKCRhdmFpbGFibGVGaWVsZHMpO1xuXG4gICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHJlc3BvbnNlLmxlbmd0aDsgaSArPSAxKSB7XG4gICAgICAgIHRoaXMuYXBwZW5kQXZhaWxhYmxlRmllbGQoXG4gICAgICAgICAgJGF2YWlsYWJsZUZpZWxkcyxcbiAgICAgICAgICByZXNwb25zZVtpXS5sYWJlbCArIChyZXNwb25zZVtpXS5yZXF1aXJlZCA/ICcqJyA6ICcnKSxcbiAgICAgICAgICByZXNwb25zZVtpXS5kZXNjcmlwdGlvbixcbiAgICAgICAgKTtcbiAgICAgIH1cblxuICAgICAgJGF2YWlsYWJsZUZpZWxkcy5maW5kKCdbZGF0YS10b2dnbGU9XCJwb3BvdmVyXCJdJykucG9wb3ZlcigpO1xuICAgIH0pO1xuICB9XG5cbiAgLyoqXG4gICAqIFJlbW92ZSBhdmFpbGFibGUgZmllbGRzIGNvbnRlbnQgZnJvbSBnaXZlbiBjb250YWluZXIuXG4gICAqXG4gICAqIEBwYXJhbSB7alF1ZXJ5fSAkY29udGFpbmVyXG4gICAqIEBwcml2YXRlXG4gICAqL1xuICByZW1vdmVBdmFpbGFibGVGaWVsZHMoJGNvbnRhaW5lcikge1xuICAgICRjb250YWluZXIuZmluZCgnW2RhdGEtdG9nZ2xlPVwicG9wb3ZlclwiXScpLnBvcG92ZXIoJ2hpZGUnKTtcbiAgICAkY29udGFpbmVyLmVtcHR5KCk7XG4gIH1cblxuICAvKipcbiAgICogQXBwZW5kIGEgaGVscCBib3ggdG8gZ2l2ZW4gZmllbGQuXG4gICAqXG4gICAqIEBwYXJhbSB7alF1ZXJ5fSAkZmllbGRcbiAgICogQHBhcmFtIHtTdHJpbmd9IGhlbHBCb3hDb250ZW50XG4gICAqIEBwcml2YXRlXG4gICAqL1xuICBhcHBlbmRIZWxwQm94KCRmaWVsZCwgaGVscEJveENvbnRlbnQpIHtcbiAgICBjb25zdCAkaGVscEJveCA9ICQoJy5qcy1hdmFpbGFibGUtZmllbGQtcG9wb3Zlci10ZW1wbGF0ZScpLmNsb25lKCk7XG5cbiAgICAkaGVscEJveC5hdHRyKCdkYXRhLWNvbnRlbnQnLCBoZWxwQm94Q29udGVudCk7XG4gICAgJGhlbHBCb3gucmVtb3ZlQ2xhc3MoJ2pzLWF2YWlsYWJsZS1maWVsZC1wb3BvdmVyLXRlbXBsYXRlIGQtbm9uZScpO1xuICAgICRmaWVsZC5hcHBlbmQoJGhlbHBCb3gpO1xuICB9XG5cbiAgLyoqXG4gICAqIEFwcGVuZCBhdmFpbGFibGUgZmllbGQgdG8gZ2l2ZW4gY29udGFpbmVyLlxuICAgKlxuICAgKiBAcGFyYW0ge2pRdWVyeX0gJGFwcGVuZFRvIGZpZWxkIHdpbGwgYmUgYXBwZW5kZWQgdG8gdGhpcyBjb250YWluZXIuXG4gICAqIEBwYXJhbSB7U3RyaW5nfSBmaWVsZFRleHRcbiAgICogQHBhcmFtIHtTdHJpbmd9IGhlbHBCb3hDb250ZW50XG4gICAqIEBwcml2YXRlXG4gICAqL1xuICBhcHBlbmRBdmFpbGFibGVGaWVsZCgkYXBwZW5kVG8sIGZpZWxkVGV4dCwgaGVscEJveENvbnRlbnQpIHtcbiAgICBjb25zdCAkZmllbGQgPSAkKCcuanMtYXZhaWxhYmxlLWZpZWxkLXRlbXBsYXRlJykuY2xvbmUoKTtcblxuICAgICRmaWVsZC50ZXh0KGZpZWxkVGV4dCk7XG5cbiAgICBpZiAoaGVscEJveENvbnRlbnQpIHtcbiAgICAgIC8vIEFwcGVuZCBoZWxwIGJveCBuZXh0IHRvIHRoZSBmaWVsZFxuICAgICAgdGhpcy5hcHBlbmRIZWxwQm94KCRmaWVsZCwgaGVscEJveENvbnRlbnQpO1xuICAgIH1cblxuICAgICRmaWVsZC5yZW1vdmVDbGFzcygnanMtYXZhaWxhYmxlLWZpZWxkLXRlbXBsYXRlIGQtbm9uZScpO1xuICAgICRmaWVsZC5hcHBlbmRUbygkYXBwZW5kVG8pO1xuICB9XG59XG4iLCIvKipcbiAqIENvcHlyaWdodCBzaW5jZSAyMDA3IFByZXN0YVNob3AgU0EgYW5kIENvbnRyaWJ1dG9yc1xuICogUHJlc3RhU2hvcCBpcyBhbiBJbnRlcm5hdGlvbmFsIFJlZ2lzdGVyZWQgVHJhZGVtYXJrICYgUHJvcGVydHkgb2YgUHJlc3RhU2hvcCBTQVxuICpcbiAqIE5PVElDRSBPRiBMSUNFTlNFXG4gKlxuICogVGhpcyBzb3VyY2UgZmlsZSBpcyBzdWJqZWN0IHRvIHRoZSBPcGVuIFNvZnR3YXJlIExpY2Vuc2UgKE9TTCAzLjApXG4gKiB0aGF0IGlzIGJ1bmRsZWQgd2l0aCB0aGlzIHBhY2thZ2UgaW4gdGhlIGZpbGUgTElDRU5TRS5tZC5cbiAqIEl0IGlzIGFsc28gYXZhaWxhYmxlIHRocm91Z2ggdGhlIHdvcmxkLXdpZGUtd2ViIGF0IHRoaXMgVVJMOlxuICogaHR0cHM6Ly9vcGVuc291cmNlLm9yZy9saWNlbnNlcy9PU0wtMy4wXG4gKiBJZiB5b3UgZGlkIG5vdCByZWNlaXZlIGEgY29weSBvZiB0aGUgbGljZW5zZSBhbmQgYXJlIHVuYWJsZSB0b1xuICogb2J0YWluIGl0IHRocm91Z2ggdGhlIHdvcmxkLXdpZGUtd2ViLCBwbGVhc2Ugc2VuZCBhbiBlbWFpbFxuICogdG8gbGljZW5zZUBwcmVzdGFzaG9wLmNvbSBzbyB3ZSBjYW4gc2VuZCB5b3UgYSBjb3B5IGltbWVkaWF0ZWx5LlxuICpcbiAqIERJU0NMQUlNRVJcbiAqXG4gKiBEbyBub3QgZWRpdCBvciBhZGQgdG8gdGhpcyBmaWxlIGlmIHlvdSB3aXNoIHRvIHVwZ3JhZGUgUHJlc3RhU2hvcCB0byBuZXdlclxuICogdmVyc2lvbnMgaW4gdGhlIGZ1dHVyZS4gSWYgeW91IHdpc2ggdG8gY3VzdG9taXplIFByZXN0YVNob3AgZm9yIHlvdXJcbiAqIG5lZWRzIHBsZWFzZSByZWZlciB0byBodHRwczovL2RldmRvY3MucHJlc3Rhc2hvcC5jb20vIGZvciBtb3JlIGluZm9ybWF0aW9uLlxuICpcbiAqIEBhdXRob3IgICAgUHJlc3RhU2hvcCBTQSBhbmQgQ29udHJpYnV0b3JzIDxjb250YWN0QHByZXN0YXNob3AuY29tPlxuICogQGNvcHlyaWdodCBTaW5jZSAyMDA3IFByZXN0YVNob3AgU0EgYW5kIENvbnRyaWJ1dG9yc1xuICogQGxpY2Vuc2UgICBodHRwczovL29wZW5zb3VyY2Uub3JnL2xpY2Vuc2VzL09TTC0zLjAgT3BlbiBTb2Z0d2FyZSBMaWNlbnNlIChPU0wgMy4wKVxuICovXG5cbmltcG9ydCBGb3JtRmllbGRUb2dnbGUgZnJvbSAnLi9Gb3JtRmllbGRUb2dnbGUnO1xuXG5jb25zdCB7JH0gPSB3aW5kb3c7XG5cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIEltcG9ydFBhZ2Uge1xuICBjb25zdHJ1Y3RvcigpIHtcbiAgICBuZXcgRm9ybUZpZWxkVG9nZ2xlKCk7XG5cbiAgICAkKCcuanMtZnJvbS1maWxlcy1oaXN0b3J5LWJ0bicpLm9uKCdjbGljaycsICgpID0+IHRoaXMuc2hvd0ZpbGVzSGlzdG9yeUhhbmRsZXIoKSk7XG4gICAgJCgnLmpzLWNsb3NlLWZpbGVzLWhpc3RvcnktYmxvY2stYnRuJykub24oJ2NsaWNrJywgKCkgPT4gdGhpcy5jbG9zZUZpbGVzSGlzdG9yeUhhbmRsZXIoKSk7XG4gICAgJCgnI2ZpbGVIaXN0b3J5VGFibGUnKS5vbignY2xpY2snLCAnLmpzLXVzZS1maWxlLWJ0bicsIChldmVudCkgPT4gdGhpcy51c2VGaWxlRnJvbUZpbGVzSGlzdG9yeShldmVudCkpO1xuICAgICQoJy5qcy1jaGFuZ2UtaW1wb3J0LWZpbGUtYnRuJykub24oJ2NsaWNrJywgKCkgPT4gdGhpcy5jaGFuZ2VJbXBvcnRGaWxlSGFuZGxlcigpKTtcbiAgICAkKCcuanMtaW1wb3J0LWZpbGUnKS5vbignY2hhbmdlJywgKCkgPT4gdGhpcy51cGxvYWRGaWxlKCkpO1xuXG4gICAgdGhpcy50b2dnbGVTZWxlY3RlZEZpbGUoKTtcbiAgICB0aGlzLmhhbmRsZVN1Ym1pdCgpO1xuICB9XG5cbiAgLyoqXG4gICAqIEhhbmRsZSBzdWJtaXQgYW5kIGFkZCBjb25maXJtIGJveCBpbiBjYXNlIHRoZSB0b2dnbGUgYnV0dG9uIGFib3V0XG4gICAqIGRlbGV0aW5nIGFsbCBlbnRpdGllcyBiZWZvcmUgaW1wb3J0IGlzIGNoZWNrZWRcbiAgICovXG4gIGhhbmRsZVN1Ym1pdCgpIHtcbiAgICAkKCcuanMtaW1wb3J0LWZvcm0nKS5vbignc3VibWl0JywgZnVuY3Rpb24gKCkge1xuICAgICAgY29uc3QgJHRoaXMgPSAkKHRoaXMpO1xuXG4gICAgICBpZiAoJHRoaXMuZmluZCgnaW5wdXRbbmFtZT1cInRydW5jYXRlXCJdOmNoZWNrZWQnKS52YWwoKSA9PT0gJzEnKSB7XG4gICAgICAgIC8qIGVzbGludC1kaXNhYmxlLW5leHQtbGluZSBtYXgtbGVuICovXG4gICAgICAgIHJldHVybiB3aW5kb3cuY29uZmlybShgJHskdGhpcy5kYXRhKCdkZWxldGUtY29uZmlybS1tZXNzYWdlJyl9ICR7JC50cmltKCQoJyNlbnRpdHkgPiBvcHRpb246c2VsZWN0ZWQnKS50ZXh0KCkudG9Mb3dlckNhc2UoKSl9P2ApO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gdHJ1ZTtcbiAgICB9KTtcbiAgfVxuXG4gIC8qKlxuICAgKiBDaGVjayBpZiBzZWxlY3RlZCBmaWxlIG5hbWVzIGV4aXN0cyBhbmQgaWYgc28sIHRoZW4gZGlzcGxheSBpdFxuICAgKi9cbiAgdG9nZ2xlU2VsZWN0ZWRGaWxlKCkge1xuICAgIGNvbnN0IHNlbGVjdEZpbGVuYW1lID0gJCgnI2NzdicpLnZhbCgpO1xuXG4gICAgaWYgKHNlbGVjdEZpbGVuYW1lLmxlbmd0aCA+IDApIHtcbiAgICAgIHRoaXMuc2hvd0ltcG9ydEZpbGVBbGVydChzZWxlY3RGaWxlbmFtZSk7XG4gICAgICB0aGlzLmhpZGVGaWxlVXBsb2FkQmxvY2soKTtcbiAgICB9XG4gIH1cblxuICBjaGFuZ2VJbXBvcnRGaWxlSGFuZGxlcigpIHtcbiAgICB0aGlzLmhpZGVJbXBvcnRGaWxlQWxlcnQoKTtcbiAgICB0aGlzLnNob3dGaWxlVXBsb2FkQmxvY2soKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBTaG93IGZpbGVzIGhpc3RvcnkgZXZlbnQgaGFuZGxlclxuICAgKi9cbiAgc2hvd0ZpbGVzSGlzdG9yeUhhbmRsZXIoKSB7XG4gICAgdGhpcy5zaG93RmlsZXNIaXN0b3J5KCk7XG4gICAgdGhpcy5oaWRlRmlsZVVwbG9hZEJsb2NrKCk7XG4gIH1cblxuICAvKipcbiAgICogQ2xvc2UgZmlsZXMgaGlzdG9yeSBldmVudCBoYW5kbGVyXG4gICAqL1xuICBjbG9zZUZpbGVzSGlzdG9yeUhhbmRsZXIoKSB7XG4gICAgdGhpcy5jbG9zZUZpbGVzSGlzdG9yeSgpO1xuICAgIHRoaXMuc2hvd0ZpbGVVcGxvYWRCbG9jaygpO1xuICB9XG5cbiAgLyoqXG4gICAqIFNob3cgZmlsZXMgaGlzdG9yeSBibG9ja1xuICAgKi9cbiAgc2hvd0ZpbGVzSGlzdG9yeSgpIHtcbiAgICAkKCcuanMtZmlsZXMtaGlzdG9yeS1ibG9jaycpLnJlbW92ZUNsYXNzKCdkLW5vbmUnKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBIaWRlIGZpbGVzIGhpc3RvcnkgYmxvY2tcbiAgICovXG4gIGNsb3NlRmlsZXNIaXN0b3J5KCkge1xuICAgICQoJy5qcy1maWxlcy1oaXN0b3J5LWJsb2NrJykuYWRkQ2xhc3MoJ2Qtbm9uZScpO1xuICB9XG5cbiAgLyoqXG4gICAqICBQcmVmaWxsIGhpZGRlbiBmaWxlIGlucHV0IHdpdGggc2VsZWN0ZWQgZmlsZSBuYW1lIGZyb20gaGlzdG9yeVxuICAgKi9cbiAgdXNlRmlsZUZyb21GaWxlc0hpc3RvcnkoZXZlbnQpIHtcbiAgICBjb25zdCBmaWxlbmFtZSA9ICQoZXZlbnQudGFyZ2V0KS5jbG9zZXN0KCcuYnRuLWdyb3VwJykuZGF0YSgnZmlsZScpO1xuXG4gICAgJCgnLmpzLWltcG9ydC1maWxlLWlucHV0JykudmFsKGZpbGVuYW1lKTtcblxuICAgIHRoaXMuc2hvd0ltcG9ydEZpbGVBbGVydChmaWxlbmFtZSk7XG4gICAgdGhpcy5jbG9zZUZpbGVzSGlzdG9yeSgpO1xuICB9XG5cbiAgLyoqXG4gICAqIFNob3cgYWxlcnQgd2l0aCBpbXBvcnRlZCBmaWxlIG5hbWVcbiAgICovXG4gIHNob3dJbXBvcnRGaWxlQWxlcnQoZmlsZW5hbWUpIHtcbiAgICAkKCcuanMtaW1wb3J0LWZpbGUtYWxlcnQnKS5yZW1vdmVDbGFzcygnZC1ub25lJyk7XG4gICAgJCgnLmpzLWltcG9ydC1maWxlJykudGV4dChmaWxlbmFtZSk7XG4gIH1cblxuICAvKipcbiAgICogSGlkZXMgc2VsZWN0ZWQgaW1wb3J0IGZpbGUgYWxlcnRcbiAgICovXG4gIGhpZGVJbXBvcnRGaWxlQWxlcnQoKSB7XG4gICAgJCgnLmpzLWltcG9ydC1maWxlLWFsZXJ0JykuYWRkQ2xhc3MoJ2Qtbm9uZScpO1xuICB9XG5cbiAgLyoqXG4gICAqIEhpZGVzIGltcG9ydCBmaWxlIHVwbG9hZCBibG9ja1xuICAgKi9cbiAgaGlkZUZpbGVVcGxvYWRCbG9jaygpIHtcbiAgICAkKCcuanMtZmlsZS11cGxvYWQtZm9ybS1ncm91cCcpLmFkZENsYXNzKCdkLW5vbmUnKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBIaWRlcyBpbXBvcnQgZmlsZSB1cGxvYWQgYmxvY2tcbiAgICovXG4gIHNob3dGaWxlVXBsb2FkQmxvY2soKSB7XG4gICAgJCgnLmpzLWZpbGUtdXBsb2FkLWZvcm0tZ3JvdXAnKS5yZW1vdmVDbGFzcygnZC1ub25lJyk7XG4gIH1cblxuICAvKipcbiAgICogTWFrZSBmaWxlIGhpc3RvcnkgYnV0dG9uIGNsaWNrYWJsZVxuICAgKi9cbiAgZW5hYmxlRmlsZXNIaXN0b3J5QnRuKCkge1xuICAgICQoJy5qcy1mcm9tLWZpbGVzLWhpc3RvcnktYnRuJykucmVtb3ZlQXR0cignZGlzYWJsZWQnKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBTaG93IGVycm9yIG1lc3NhZ2UgaWYgZmlsZSB1cGxvYWRpbmcgZmFpbGVkXG4gICAqXG4gICAqIEBwYXJhbSB7c3RyaW5nfSBmaWxlTmFtZVxuICAgKiBAcGFyYW0ge2ludGVnZXJ9IGZpbGVTaXplXG4gICAqIEBwYXJhbSB7c3RyaW5nfSBtZXNzYWdlXG4gICAqL1xuICBzaG93SW1wb3J0RmlsZUVycm9yKGZpbGVOYW1lLCBmaWxlU2l6ZSwgbWVzc2FnZSkge1xuICAgIGNvbnN0ICRhbGVydCA9ICQoJy5qcy1pbXBvcnQtZmlsZS1lcnJvcicpO1xuXG4gICAgY29uc3QgZmlsZURhdGEgPSBgJHtmaWxlTmFtZX0gKCR7dGhpcy5odW1hbml6ZVNpemUoZmlsZVNpemUpfSlgO1xuXG4gICAgJGFsZXJ0LmZpbmQoJy5qcy1maWxlLWRhdGEnKS50ZXh0KGZpbGVEYXRhKTtcbiAgICAkYWxlcnQuZmluZCgnLmpzLWVycm9yLW1lc3NhZ2UnKS50ZXh0KG1lc3NhZ2UpO1xuICAgICRhbGVydC5yZW1vdmVDbGFzcygnZC1ub25lJyk7XG4gIH1cblxuICAvKipcbiAgICogSGlkZSBmaWxlIHVwbG9hZGluZyBlcnJvclxuICAgKi9cbiAgaGlkZUltcG9ydEZpbGVFcnJvcigpIHtcbiAgICBjb25zdCAkYWxlcnQgPSAkKCcuanMtaW1wb3J0LWZpbGUtZXJyb3InKTtcbiAgICAkYWxlcnQuYWRkQ2xhc3MoJ2Qtbm9uZScpO1xuICB9XG5cbiAgLyoqXG4gICAqIFNob3cgZmlsZSBzaXplIGluIGh1bWFuIHJlYWRhYmxlIGZvcm1hdFxuICAgKlxuICAgKiBAcGFyYW0ge2ludH0gYnl0ZXNcbiAgICpcbiAgICogQHJldHVybnMge3N0cmluZ31cbiAgICovXG4gIGh1bWFuaXplU2l6ZShieXRlcykge1xuICAgIGlmICh0eXBlb2YgYnl0ZXMgIT09ICdudW1iZXInKSB7XG4gICAgICByZXR1cm4gJyc7XG4gICAgfVxuXG4gICAgaWYgKGJ5dGVzID49IDEwMDAwMDAwMDApIHtcbiAgICAgIHJldHVybiBgJHsoYnl0ZXMgLyAxMDAwMDAwMDAwKS50b0ZpeGVkKDIpfSBHQmA7XG4gICAgfVxuXG4gICAgaWYgKGJ5dGVzID49IDEwMDAwMDApIHtcbiAgICAgIHJldHVybiBgJHsoYnl0ZXMgLyAxMDAwMDAwKS50b0ZpeGVkKDIpfSBNQmA7XG4gICAgfVxuXG4gICAgcmV0dXJuIGAkeyhieXRlcyAvIDEwMDApLnRvRml4ZWQoMil9IEtCYDtcbiAgfVxuXG4gIC8qKlxuICAgKiBVcGxvYWQgc2VsZWN0ZWQgaW1wb3J0IGZpbGVcbiAgICovXG4gIHVwbG9hZEZpbGUoKSB7XG4gICAgdGhpcy5oaWRlSW1wb3J0RmlsZUVycm9yKCk7XG5cbiAgICBjb25zdCAkaW5wdXQgPSAkKCcjZmlsZScpO1xuICAgIGNvbnN0IHVwbG9hZGVkRmlsZSA9ICRpbnB1dC5wcm9wKCdmaWxlcycpWzBdO1xuXG4gICAgY29uc3QgbWF4VXBsb2FkU2l6ZSA9ICRpbnB1dC5kYXRhKCdtYXgtZmlsZS11cGxvYWQtc2l6ZScpO1xuXG4gICAgaWYgKG1heFVwbG9hZFNpemUgPCB1cGxvYWRlZEZpbGUuc2l6ZSkge1xuICAgICAgdGhpcy5zaG93SW1wb3J0RmlsZUVycm9yKHVwbG9hZGVkRmlsZS5uYW1lLCB1cGxvYWRlZEZpbGUuc2l6ZSwgJ0ZpbGUgaXMgdG9vIGxhcmdlJyk7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgY29uc3QgZGF0YSA9IG5ldyBGb3JtRGF0YSgpO1xuICAgIGRhdGEuYXBwZW5kKCdmaWxlJywgdXBsb2FkZWRGaWxlKTtcblxuICAgICQuYWpheCh7XG4gICAgICB0eXBlOiAnUE9TVCcsXG4gICAgICB1cmw6ICQoJy5qcy1pbXBvcnQtZm9ybScpLmRhdGEoJ2ZpbGUtdXBsb2FkLXVybCcpLFxuICAgICAgZGF0YSxcbiAgICAgIGNhY2hlOiBmYWxzZSxcbiAgICAgIGNvbnRlbnRUeXBlOiBmYWxzZSxcbiAgICAgIHByb2Nlc3NEYXRhOiBmYWxzZSxcbiAgICB9KS50aGVuKChyZXNwb25zZSkgPT4ge1xuICAgICAgaWYgKHJlc3BvbnNlLmVycm9yKSB7XG4gICAgICAgIHRoaXMuc2hvd0ltcG9ydEZpbGVFcnJvcih1cGxvYWRlZEZpbGUubmFtZSwgdXBsb2FkZWRGaWxlLnNpemUsIHJlc3BvbnNlLmVycm9yKTtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICBjb25zdCBmaWxlbmFtZSA9IHJlc3BvbnNlLmZpbGUubmFtZTtcblxuICAgICAgJCgnLmpzLWltcG9ydC1maWxlLWlucHV0JykudmFsKGZpbGVuYW1lKTtcblxuICAgICAgdGhpcy5zaG93SW1wb3J0RmlsZUFsZXJ0KGZpbGVuYW1lKTtcbiAgICAgIHRoaXMuaGlkZUZpbGVVcGxvYWRCbG9jaygpO1xuICAgICAgdGhpcy5hZGRGaWxlVG9IaXN0b3J5VGFibGUoZmlsZW5hbWUpO1xuICAgICAgdGhpcy5lbmFibGVGaWxlc0hpc3RvcnlCdG4oKTtcbiAgICB9KTtcbiAgfVxuXG4gIC8qKlxuICAgKiBSZW5kZXJzIG5ldyByb3cgaW4gZmlsZXMgaGlzdG9yeSB0YWJsZVxuICAgKlxuICAgKiBAcGFyYW0ge3N0cmluZ30gZmlsZW5hbWVcbiAgICovXG4gIGFkZEZpbGVUb0hpc3RvcnlUYWJsZShmaWxlbmFtZSkge1xuICAgIGNvbnN0ICR0YWJsZSA9ICQoJyNmaWxlSGlzdG9yeVRhYmxlJyk7XG5cbiAgICBjb25zdCBiYXNlRGVsZXRlVXJsID0gJHRhYmxlLmRhdGEoJ2RlbGV0ZS1maWxlLXVybCcpO1xuICAgIGNvbnN0IGRlbGV0ZVVybCA9IGAke2Jhc2VEZWxldGVVcmx9JmZpbGVuYW1lPSR7ZW5jb2RlVVJJQ29tcG9uZW50KGZpbGVuYW1lKX1gO1xuXG4gICAgY29uc3QgYmFzZURvd25sb2FkVXJsID0gJHRhYmxlLmRhdGEoJ2Rvd25sb2FkLWZpbGUtdXJsJyk7XG4gICAgY29uc3QgZG93bmxvYWRVcmwgPSBgJHtiYXNlRG93bmxvYWRVcmx9JmZpbGVuYW1lPSR7ZW5jb2RlVVJJQ29tcG9uZW50KGZpbGVuYW1lKX1gO1xuXG4gICAgY29uc3QgJHRlbXBsYXRlID0gJHRhYmxlLmZpbmQoJ3RyOmZpcnN0JykuY2xvbmUoKTtcblxuICAgICR0ZW1wbGF0ZS5yZW1vdmVDbGFzcygnZC1ub25lJyk7XG4gICAgJHRlbXBsYXRlLmZpbmQoJ3RkOmZpcnN0JykudGV4dChmaWxlbmFtZSk7XG4gICAgJHRlbXBsYXRlLmZpbmQoJy5idG4tZ3JvdXAnKS5hdHRyKCdkYXRhLWZpbGUnLCBmaWxlbmFtZSk7XG4gICAgJHRlbXBsYXRlLmZpbmQoJy5qcy1kZWxldGUtZmlsZS1idG4nKS5hdHRyKCdocmVmJywgZGVsZXRlVXJsKTtcbiAgICAkdGVtcGxhdGUuZmluZCgnLmpzLWRvd25sb2FkLWZpbGUtYnRuJykuYXR0cignaHJlZicsIGRvd25sb2FkVXJsKTtcblxuICAgICR0YWJsZS5maW5kKCd0Ym9keScpLmFwcGVuZCgkdGVtcGxhdGUpO1xuXG4gICAgY29uc3QgZmlsZXNOdW1iZXIgPSAkdGFibGUuZmluZCgndHInKS5sZW5ndGggLSAxO1xuICAgICQoJy5qcy1maWxlcy1oaXN0b3J5LW51bWJlcicpLnRleHQoZmlsZXNOdW1iZXIpO1xuICB9XG59XG4iLCJtb2R1bGUuZXhwb3J0cyA9IHsgXCJkZWZhdWx0XCI6IHJlcXVpcmUoXCJjb3JlLWpzL2xpYnJhcnkvZm4vb2JqZWN0L2RlZmluZS1wcm9wZXJ0eVwiKSwgX19lc01vZHVsZTogdHJ1ZSB9OyIsIlwidXNlIHN0cmljdFwiO1xuXG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xuXG5leHBvcnRzLmRlZmF1bHQgPSBmdW5jdGlvbiAoaW5zdGFuY2UsIENvbnN0cnVjdG9yKSB7XG4gIGlmICghKGluc3RhbmNlIGluc3RhbmNlb2YgQ29uc3RydWN0b3IpKSB7XG4gICAgdGhyb3cgbmV3IFR5cGVFcnJvcihcIkNhbm5vdCBjYWxsIGEgY2xhc3MgYXMgYSBmdW5jdGlvblwiKTtcbiAgfVxufTsiLCJcInVzZSBzdHJpY3RcIjtcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcblxudmFyIF9kZWZpbmVQcm9wZXJ0eSA9IHJlcXVpcmUoXCIuLi9jb3JlLWpzL29iamVjdC9kZWZpbmUtcHJvcGVydHlcIik7XG5cbnZhciBfZGVmaW5lUHJvcGVydHkyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZGVmaW5lUHJvcGVydHkpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG5leHBvcnRzLmRlZmF1bHQgPSBmdW5jdGlvbiAoKSB7XG4gIGZ1bmN0aW9uIGRlZmluZVByb3BlcnRpZXModGFyZ2V0LCBwcm9wcykge1xuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgcHJvcHMubGVuZ3RoOyBpKyspIHtcbiAgICAgIHZhciBkZXNjcmlwdG9yID0gcHJvcHNbaV07XG4gICAgICBkZXNjcmlwdG9yLmVudW1lcmFibGUgPSBkZXNjcmlwdG9yLmVudW1lcmFibGUgfHwgZmFsc2U7XG4gICAgICBkZXNjcmlwdG9yLmNvbmZpZ3VyYWJsZSA9IHRydWU7XG4gICAgICBpZiAoXCJ2YWx1ZVwiIGluIGRlc2NyaXB0b3IpIGRlc2NyaXB0b3Iud3JpdGFibGUgPSB0cnVlO1xuICAgICAgKDAsIF9kZWZpbmVQcm9wZXJ0eTIuZGVmYXVsdCkodGFyZ2V0LCBkZXNjcmlwdG9yLmtleSwgZGVzY3JpcHRvcik7XG4gICAgfVxuICB9XG5cbiAgcmV0dXJuIGZ1bmN0aW9uIChDb25zdHJ1Y3RvciwgcHJvdG9Qcm9wcywgc3RhdGljUHJvcHMpIHtcbiAgICBpZiAocHJvdG9Qcm9wcykgZGVmaW5lUHJvcGVydGllcyhDb25zdHJ1Y3Rvci5wcm90b3R5cGUsIHByb3RvUHJvcHMpO1xuICAgIGlmIChzdGF0aWNQcm9wcykgZGVmaW5lUHJvcGVydGllcyhDb25zdHJ1Y3Rvciwgc3RhdGljUHJvcHMpO1xuICAgIHJldHVybiBDb25zdHJ1Y3RvcjtcbiAgfTtcbn0oKTsiLCJyZXF1aXJlKCcuLi8uLi9tb2R1bGVzL2VzNi5vYmplY3QuZGVmaW5lLXByb3BlcnR5Jyk7XG52YXIgJE9iamVjdCA9IHJlcXVpcmUoJy4uLy4uL21vZHVsZXMvX2NvcmUnKS5PYmplY3Q7XG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIGRlZmluZVByb3BlcnR5KGl0LCBrZXksIGRlc2MpIHtcbiAgcmV0dXJuICRPYmplY3QuZGVmaW5lUHJvcGVydHkoaXQsIGtleSwgZGVzYyk7XG59O1xuIiwibW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAoaXQpIHtcbiAgaWYgKHR5cGVvZiBpdCAhPSAnZnVuY3Rpb24nKSB0aHJvdyBUeXBlRXJyb3IoaXQgKyAnIGlzIG5vdCBhIGZ1bmN0aW9uIScpO1xuICByZXR1cm4gaXQ7XG59O1xuIiwidmFyIGlzT2JqZWN0ID0gcmVxdWlyZSgnLi9faXMtb2JqZWN0Jyk7XG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIChpdCkge1xuICBpZiAoIWlzT2JqZWN0KGl0KSkgdGhyb3cgVHlwZUVycm9yKGl0ICsgJyBpcyBub3QgYW4gb2JqZWN0IScpO1xuICByZXR1cm4gaXQ7XG59O1xuIiwidmFyIGNvcmUgPSBtb2R1bGUuZXhwb3J0cyA9IHsgdmVyc2lvbjogJzIuNi4xMScgfTtcbmlmICh0eXBlb2YgX19lID09ICdudW1iZXInKSBfX2UgPSBjb3JlOyAvLyBlc2xpbnQtZGlzYWJsZS1saW5lIG5vLXVuZGVmXG4iLCIvLyBvcHRpb25hbCAvIHNpbXBsZSBjb250ZXh0IGJpbmRpbmdcbnZhciBhRnVuY3Rpb24gPSByZXF1aXJlKCcuL19hLWZ1bmN0aW9uJyk7XG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIChmbiwgdGhhdCwgbGVuZ3RoKSB7XG4gIGFGdW5jdGlvbihmbik7XG4gIGlmICh0aGF0ID09PSB1bmRlZmluZWQpIHJldHVybiBmbjtcbiAgc3dpdGNoIChsZW5ndGgpIHtcbiAgICBjYXNlIDE6IHJldHVybiBmdW5jdGlvbiAoYSkge1xuICAgICAgcmV0dXJuIGZuLmNhbGwodGhhdCwgYSk7XG4gICAgfTtcbiAgICBjYXNlIDI6IHJldHVybiBmdW5jdGlvbiAoYSwgYikge1xuICAgICAgcmV0dXJuIGZuLmNhbGwodGhhdCwgYSwgYik7XG4gICAgfTtcbiAgICBjYXNlIDM6IHJldHVybiBmdW5jdGlvbiAoYSwgYiwgYykge1xuICAgICAgcmV0dXJuIGZuLmNhbGwodGhhdCwgYSwgYiwgYyk7XG4gICAgfTtcbiAgfVxuICByZXR1cm4gZnVuY3Rpb24gKC8qIC4uLmFyZ3MgKi8pIHtcbiAgICByZXR1cm4gZm4uYXBwbHkodGhhdCwgYXJndW1lbnRzKTtcbiAgfTtcbn07XG4iLCIvLyBUaGFuaydzIElFOCBmb3IgaGlzIGZ1bm55IGRlZmluZVByb3BlcnR5XG5tb2R1bGUuZXhwb3J0cyA9ICFyZXF1aXJlKCcuL19mYWlscycpKGZ1bmN0aW9uICgpIHtcbiAgcmV0dXJuIE9iamVjdC5kZWZpbmVQcm9wZXJ0eSh7fSwgJ2EnLCB7IGdldDogZnVuY3Rpb24gKCkgeyByZXR1cm4gNzsgfSB9KS5hICE9IDc7XG59KTtcbiIsInZhciBpc09iamVjdCA9IHJlcXVpcmUoJy4vX2lzLW9iamVjdCcpO1xudmFyIGRvY3VtZW50ID0gcmVxdWlyZSgnLi9fZ2xvYmFsJykuZG9jdW1lbnQ7XG4vLyB0eXBlb2YgZG9jdW1lbnQuY3JlYXRlRWxlbWVudCBpcyAnb2JqZWN0JyBpbiBvbGQgSUVcbnZhciBpcyA9IGlzT2JqZWN0KGRvY3VtZW50KSAmJiBpc09iamVjdChkb2N1bWVudC5jcmVhdGVFbGVtZW50KTtcbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKGl0KSB7XG4gIHJldHVybiBpcyA/IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoaXQpIDoge307XG59O1xuIiwidmFyIGdsb2JhbCA9IHJlcXVpcmUoJy4vX2dsb2JhbCcpO1xudmFyIGNvcmUgPSByZXF1aXJlKCcuL19jb3JlJyk7XG52YXIgY3R4ID0gcmVxdWlyZSgnLi9fY3R4Jyk7XG52YXIgaGlkZSA9IHJlcXVpcmUoJy4vX2hpZGUnKTtcbnZhciBoYXMgPSByZXF1aXJlKCcuL19oYXMnKTtcbnZhciBQUk9UT1RZUEUgPSAncHJvdG90eXBlJztcblxudmFyICRleHBvcnQgPSBmdW5jdGlvbiAodHlwZSwgbmFtZSwgc291cmNlKSB7XG4gIHZhciBJU19GT1JDRUQgPSB0eXBlICYgJGV4cG9ydC5GO1xuICB2YXIgSVNfR0xPQkFMID0gdHlwZSAmICRleHBvcnQuRztcbiAgdmFyIElTX1NUQVRJQyA9IHR5cGUgJiAkZXhwb3J0LlM7XG4gIHZhciBJU19QUk9UTyA9IHR5cGUgJiAkZXhwb3J0LlA7XG4gIHZhciBJU19CSU5EID0gdHlwZSAmICRleHBvcnQuQjtcbiAgdmFyIElTX1dSQVAgPSB0eXBlICYgJGV4cG9ydC5XO1xuICB2YXIgZXhwb3J0cyA9IElTX0dMT0JBTCA/IGNvcmUgOiBjb3JlW25hbWVdIHx8IChjb3JlW25hbWVdID0ge30pO1xuICB2YXIgZXhwUHJvdG8gPSBleHBvcnRzW1BST1RPVFlQRV07XG4gIHZhciB0YXJnZXQgPSBJU19HTE9CQUwgPyBnbG9iYWwgOiBJU19TVEFUSUMgPyBnbG9iYWxbbmFtZV0gOiAoZ2xvYmFsW25hbWVdIHx8IHt9KVtQUk9UT1RZUEVdO1xuICB2YXIga2V5LCBvd24sIG91dDtcbiAgaWYgKElTX0dMT0JBTCkgc291cmNlID0gbmFtZTtcbiAgZm9yIChrZXkgaW4gc291cmNlKSB7XG4gICAgLy8gY29udGFpbnMgaW4gbmF0aXZlXG4gICAgb3duID0gIUlTX0ZPUkNFRCAmJiB0YXJnZXQgJiYgdGFyZ2V0W2tleV0gIT09IHVuZGVmaW5lZDtcbiAgICBpZiAob3duICYmIGhhcyhleHBvcnRzLCBrZXkpKSBjb250aW51ZTtcbiAgICAvLyBleHBvcnQgbmF0aXZlIG9yIHBhc3NlZFxuICAgIG91dCA9IG93biA/IHRhcmdldFtrZXldIDogc291cmNlW2tleV07XG4gICAgLy8gcHJldmVudCBnbG9iYWwgcG9sbHV0aW9uIGZvciBuYW1lc3BhY2VzXG4gICAgZXhwb3J0c1trZXldID0gSVNfR0xPQkFMICYmIHR5cGVvZiB0YXJnZXRba2V5XSAhPSAnZnVuY3Rpb24nID8gc291cmNlW2tleV1cbiAgICAvLyBiaW5kIHRpbWVycyB0byBnbG9iYWwgZm9yIGNhbGwgZnJvbSBleHBvcnQgY29udGV4dFxuICAgIDogSVNfQklORCAmJiBvd24gPyBjdHgob3V0LCBnbG9iYWwpXG4gICAgLy8gd3JhcCBnbG9iYWwgY29uc3RydWN0b3JzIGZvciBwcmV2ZW50IGNoYW5nZSB0aGVtIGluIGxpYnJhcnlcbiAgICA6IElTX1dSQVAgJiYgdGFyZ2V0W2tleV0gPT0gb3V0ID8gKGZ1bmN0aW9uIChDKSB7XG4gICAgICB2YXIgRiA9IGZ1bmN0aW9uIChhLCBiLCBjKSB7XG4gICAgICAgIGlmICh0aGlzIGluc3RhbmNlb2YgQykge1xuICAgICAgICAgIHN3aXRjaCAoYXJndW1lbnRzLmxlbmd0aCkge1xuICAgICAgICAgICAgY2FzZSAwOiByZXR1cm4gbmV3IEMoKTtcbiAgICAgICAgICAgIGNhc2UgMTogcmV0dXJuIG5ldyBDKGEpO1xuICAgICAgICAgICAgY2FzZSAyOiByZXR1cm4gbmV3IEMoYSwgYik7XG4gICAgICAgICAgfSByZXR1cm4gbmV3IEMoYSwgYiwgYyk7XG4gICAgICAgIH0gcmV0dXJuIEMuYXBwbHkodGhpcywgYXJndW1lbnRzKTtcbiAgICAgIH07XG4gICAgICBGW1BST1RPVFlQRV0gPSBDW1BST1RPVFlQRV07XG4gICAgICByZXR1cm4gRjtcbiAgICAvLyBtYWtlIHN0YXRpYyB2ZXJzaW9ucyBmb3IgcHJvdG90eXBlIG1ldGhvZHNcbiAgICB9KShvdXQpIDogSVNfUFJPVE8gJiYgdHlwZW9mIG91dCA9PSAnZnVuY3Rpb24nID8gY3R4KEZ1bmN0aW9uLmNhbGwsIG91dCkgOiBvdXQ7XG4gICAgLy8gZXhwb3J0IHByb3RvIG1ldGhvZHMgdG8gY29yZS4lQ09OU1RSVUNUT1IlLm1ldGhvZHMuJU5BTUUlXG4gICAgaWYgKElTX1BST1RPKSB7XG4gICAgICAoZXhwb3J0cy52aXJ0dWFsIHx8IChleHBvcnRzLnZpcnR1YWwgPSB7fSkpW2tleV0gPSBvdXQ7XG4gICAgICAvLyBleHBvcnQgcHJvdG8gbWV0aG9kcyB0byBjb3JlLiVDT05TVFJVQ1RPUiUucHJvdG90eXBlLiVOQU1FJVxuICAgICAgaWYgKHR5cGUgJiAkZXhwb3J0LlIgJiYgZXhwUHJvdG8gJiYgIWV4cFByb3RvW2tleV0pIGhpZGUoZXhwUHJvdG8sIGtleSwgb3V0KTtcbiAgICB9XG4gIH1cbn07XG4vLyB0eXBlIGJpdG1hcFxuJGV4cG9ydC5GID0gMTsgICAvLyBmb3JjZWRcbiRleHBvcnQuRyA9IDI7ICAgLy8gZ2xvYmFsXG4kZXhwb3J0LlMgPSA0OyAgIC8vIHN0YXRpY1xuJGV4cG9ydC5QID0gODsgICAvLyBwcm90b1xuJGV4cG9ydC5CID0gMTY7ICAvLyBiaW5kXG4kZXhwb3J0LlcgPSAzMjsgIC8vIHdyYXBcbiRleHBvcnQuVSA9IDY0OyAgLy8gc2FmZVxuJGV4cG9ydC5SID0gMTI4OyAvLyByZWFsIHByb3RvIG1ldGhvZCBmb3IgYGxpYnJhcnlgXG5tb2R1bGUuZXhwb3J0cyA9ICRleHBvcnQ7XG4iLCJtb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIChleGVjKSB7XG4gIHRyeSB7XG4gICAgcmV0dXJuICEhZXhlYygpO1xuICB9IGNhdGNoIChlKSB7XG4gICAgcmV0dXJuIHRydWU7XG4gIH1cbn07XG4iLCIvLyBodHRwczovL2dpdGh1Yi5jb20vemxvaXJvY2svY29yZS1qcy9pc3N1ZXMvODYjaXNzdWVjb21tZW50LTExNTc1OTAyOFxudmFyIGdsb2JhbCA9IG1vZHVsZS5leHBvcnRzID0gdHlwZW9mIHdpbmRvdyAhPSAndW5kZWZpbmVkJyAmJiB3aW5kb3cuTWF0aCA9PSBNYXRoXG4gID8gd2luZG93IDogdHlwZW9mIHNlbGYgIT0gJ3VuZGVmaW5lZCcgJiYgc2VsZi5NYXRoID09IE1hdGggPyBzZWxmXG4gIC8vIGVzbGludC1kaXNhYmxlLW5leHQtbGluZSBuby1uZXctZnVuY1xuICA6IEZ1bmN0aW9uKCdyZXR1cm4gdGhpcycpKCk7XG5pZiAodHlwZW9mIF9fZyA9PSAnbnVtYmVyJykgX19nID0gZ2xvYmFsOyAvLyBlc2xpbnQtZGlzYWJsZS1saW5lIG5vLXVuZGVmXG4iLCJ2YXIgaGFzT3duUHJvcGVydHkgPSB7fS5oYXNPd25Qcm9wZXJ0eTtcbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKGl0LCBrZXkpIHtcbiAgcmV0dXJuIGhhc093blByb3BlcnR5LmNhbGwoaXQsIGtleSk7XG59O1xuIiwidmFyIGRQID0gcmVxdWlyZSgnLi9fb2JqZWN0LWRwJyk7XG52YXIgY3JlYXRlRGVzYyA9IHJlcXVpcmUoJy4vX3Byb3BlcnR5LWRlc2MnKTtcbm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZSgnLi9fZGVzY3JpcHRvcnMnKSA/IGZ1bmN0aW9uIChvYmplY3QsIGtleSwgdmFsdWUpIHtcbiAgcmV0dXJuIGRQLmYob2JqZWN0LCBrZXksIGNyZWF0ZURlc2MoMSwgdmFsdWUpKTtcbn0gOiBmdW5jdGlvbiAob2JqZWN0LCBrZXksIHZhbHVlKSB7XG4gIG9iamVjdFtrZXldID0gdmFsdWU7XG4gIHJldHVybiBvYmplY3Q7XG59O1xuIiwibW9kdWxlLmV4cG9ydHMgPSAhcmVxdWlyZSgnLi9fZGVzY3JpcHRvcnMnKSAmJiAhcmVxdWlyZSgnLi9fZmFpbHMnKShmdW5jdGlvbiAoKSB7XG4gIHJldHVybiBPYmplY3QuZGVmaW5lUHJvcGVydHkocmVxdWlyZSgnLi9fZG9tLWNyZWF0ZScpKCdkaXYnKSwgJ2EnLCB7IGdldDogZnVuY3Rpb24gKCkgeyByZXR1cm4gNzsgfSB9KS5hICE9IDc7XG59KTtcbiIsIm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKGl0KSB7XG4gIHJldHVybiB0eXBlb2YgaXQgPT09ICdvYmplY3QnID8gaXQgIT09IG51bGwgOiB0eXBlb2YgaXQgPT09ICdmdW5jdGlvbic7XG59O1xuIiwidmFyIGFuT2JqZWN0ID0gcmVxdWlyZSgnLi9fYW4tb2JqZWN0Jyk7XG52YXIgSUU4X0RPTV9ERUZJTkUgPSByZXF1aXJlKCcuL19pZTgtZG9tLWRlZmluZScpO1xudmFyIHRvUHJpbWl0aXZlID0gcmVxdWlyZSgnLi9fdG8tcHJpbWl0aXZlJyk7XG52YXIgZFAgPSBPYmplY3QuZGVmaW5lUHJvcGVydHk7XG5cbmV4cG9ydHMuZiA9IHJlcXVpcmUoJy4vX2Rlc2NyaXB0b3JzJykgPyBPYmplY3QuZGVmaW5lUHJvcGVydHkgOiBmdW5jdGlvbiBkZWZpbmVQcm9wZXJ0eShPLCBQLCBBdHRyaWJ1dGVzKSB7XG4gIGFuT2JqZWN0KE8pO1xuICBQID0gdG9QcmltaXRpdmUoUCwgdHJ1ZSk7XG4gIGFuT2JqZWN0KEF0dHJpYnV0ZXMpO1xuICBpZiAoSUU4X0RPTV9ERUZJTkUpIHRyeSB7XG4gICAgcmV0dXJuIGRQKE8sIFAsIEF0dHJpYnV0ZXMpO1xuICB9IGNhdGNoIChlKSB7IC8qIGVtcHR5ICovIH1cbiAgaWYgKCdnZXQnIGluIEF0dHJpYnV0ZXMgfHwgJ3NldCcgaW4gQXR0cmlidXRlcykgdGhyb3cgVHlwZUVycm9yKCdBY2Nlc3NvcnMgbm90IHN1cHBvcnRlZCEnKTtcbiAgaWYgKCd2YWx1ZScgaW4gQXR0cmlidXRlcykgT1tQXSA9IEF0dHJpYnV0ZXMudmFsdWU7XG4gIHJldHVybiBPO1xufTtcbiIsIm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKGJpdG1hcCwgdmFsdWUpIHtcbiAgcmV0dXJuIHtcbiAgICBlbnVtZXJhYmxlOiAhKGJpdG1hcCAmIDEpLFxuICAgIGNvbmZpZ3VyYWJsZTogIShiaXRtYXAgJiAyKSxcbiAgICB3cml0YWJsZTogIShiaXRtYXAgJiA0KSxcbiAgICB2YWx1ZTogdmFsdWVcbiAgfTtcbn07XG4iLCIvLyA3LjEuMSBUb1ByaW1pdGl2ZShpbnB1dCBbLCBQcmVmZXJyZWRUeXBlXSlcbnZhciBpc09iamVjdCA9IHJlcXVpcmUoJy4vX2lzLW9iamVjdCcpO1xuLy8gaW5zdGVhZCBvZiB0aGUgRVM2IHNwZWMgdmVyc2lvbiwgd2UgZGlkbid0IGltcGxlbWVudCBAQHRvUHJpbWl0aXZlIGNhc2Vcbi8vIGFuZCB0aGUgc2Vjb25kIGFyZ3VtZW50IC0gZmxhZyAtIHByZWZlcnJlZCB0eXBlIGlzIGEgc3RyaW5nXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIChpdCwgUykge1xuICBpZiAoIWlzT2JqZWN0KGl0KSkgcmV0dXJuIGl0O1xuICB2YXIgZm4sIHZhbDtcbiAgaWYgKFMgJiYgdHlwZW9mIChmbiA9IGl0LnRvU3RyaW5nKSA9PSAnZnVuY3Rpb24nICYmICFpc09iamVjdCh2YWwgPSBmbi5jYWxsKGl0KSkpIHJldHVybiB2YWw7XG4gIGlmICh0eXBlb2YgKGZuID0gaXQudmFsdWVPZikgPT0gJ2Z1bmN0aW9uJyAmJiAhaXNPYmplY3QodmFsID0gZm4uY2FsbChpdCkpKSByZXR1cm4gdmFsO1xuICBpZiAoIVMgJiYgdHlwZW9mIChmbiA9IGl0LnRvU3RyaW5nKSA9PSAnZnVuY3Rpb24nICYmICFpc09iamVjdCh2YWwgPSBmbi5jYWxsKGl0KSkpIHJldHVybiB2YWw7XG4gIHRocm93IFR5cGVFcnJvcihcIkNhbid0IGNvbnZlcnQgb2JqZWN0IHRvIHByaW1pdGl2ZSB2YWx1ZVwiKTtcbn07XG4iLCJ2YXIgJGV4cG9ydCA9IHJlcXVpcmUoJy4vX2V4cG9ydCcpO1xuLy8gMTkuMS4yLjQgLyAxNS4yLjMuNiBPYmplY3QuZGVmaW5lUHJvcGVydHkoTywgUCwgQXR0cmlidXRlcylcbiRleHBvcnQoJGV4cG9ydC5TICsgJGV4cG9ydC5GICogIXJlcXVpcmUoJy4vX2Rlc2NyaXB0b3JzJyksICdPYmplY3QnLCB7IGRlZmluZVByb3BlcnR5OiByZXF1aXJlKCcuL19vYmplY3QtZHAnKS5mIH0pO1xuIiwiLy8gVGhlIG1vZHVsZSBjYWNoZVxudmFyIF9fd2VicGFja19tb2R1bGVfY2FjaGVfXyA9IHt9O1xuXG4vLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcblx0dmFyIGNhY2hlZE1vZHVsZSA9IF9fd2VicGFja19tb2R1bGVfY2FjaGVfX1ttb2R1bGVJZF07XG5cdGlmIChjYWNoZWRNb2R1bGUgIT09IHVuZGVmaW5lZCkge1xuXHRcdHJldHVybiBjYWNoZWRNb2R1bGUuZXhwb3J0cztcblx0fVxuXHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuXHR2YXIgbW9kdWxlID0gX193ZWJwYWNrX21vZHVsZV9jYWNoZV9fW21vZHVsZUlkXSA9IHtcblx0XHQvLyBubyBtb2R1bGUuaWQgbmVlZGVkXG5cdFx0Ly8gbm8gbW9kdWxlLmxvYWRlZCBuZWVkZWRcblx0XHRleHBvcnRzOiB7fVxuXHR9O1xuXG5cdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuXHRfX3dlYnBhY2tfbW9kdWxlc19fW21vZHVsZUlkXShtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuXHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuXHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG59XG5cbiIsIi8qKlxuICogQ29weXJpZ2h0IHNpbmNlIDIwMDcgUHJlc3RhU2hvcCBTQSBhbmQgQ29udHJpYnV0b3JzXG4gKiBQcmVzdGFTaG9wIGlzIGFuIEludGVybmF0aW9uYWwgUmVnaXN0ZXJlZCBUcmFkZW1hcmsgJiBQcm9wZXJ0eSBvZiBQcmVzdGFTaG9wIFNBXG4gKlxuICogTk9USUNFIE9GIExJQ0VOU0VcbiAqXG4gKiBUaGlzIHNvdXJjZSBmaWxlIGlzIHN1YmplY3QgdG8gdGhlIE9wZW4gU29mdHdhcmUgTGljZW5zZSAoT1NMIDMuMClcbiAqIHRoYXQgaXMgYnVuZGxlZCB3aXRoIHRoaXMgcGFja2FnZSBpbiB0aGUgZmlsZSBMSUNFTlNFLm1kLlxuICogSXQgaXMgYWxzbyBhdmFpbGFibGUgdGhyb3VnaCB0aGUgd29ybGQtd2lkZS13ZWIgYXQgdGhpcyBVUkw6XG4gKiBodHRwczovL29wZW5zb3VyY2Uub3JnL2xpY2Vuc2VzL09TTC0zLjBcbiAqIElmIHlvdSBkaWQgbm90IHJlY2VpdmUgYSBjb3B5IG9mIHRoZSBsaWNlbnNlIGFuZCBhcmUgdW5hYmxlIHRvXG4gKiBvYnRhaW4gaXQgdGhyb3VnaCB0aGUgd29ybGQtd2lkZS13ZWIsIHBsZWFzZSBzZW5kIGFuIGVtYWlsXG4gKiB0byBsaWNlbnNlQHByZXN0YXNob3AuY29tIHNvIHdlIGNhbiBzZW5kIHlvdSBhIGNvcHkgaW1tZWRpYXRlbHkuXG4gKlxuICogRElTQ0xBSU1FUlxuICpcbiAqIERvIG5vdCBlZGl0IG9yIGFkZCB0byB0aGlzIGZpbGUgaWYgeW91IHdpc2ggdG8gdXBncmFkZSBQcmVzdGFTaG9wIHRvIG5ld2VyXG4gKiB2ZXJzaW9ucyBpbiB0aGUgZnV0dXJlLiBJZiB5b3Ugd2lzaCB0byBjdXN0b21pemUgUHJlc3RhU2hvcCBmb3IgeW91clxuICogbmVlZHMgcGxlYXNlIHJlZmVyIHRvIGh0dHBzOi8vZGV2ZG9jcy5wcmVzdGFzaG9wLmNvbS8gZm9yIG1vcmUgaW5mb3JtYXRpb24uXG4gKlxuICogQGF1dGhvciAgICBQcmVzdGFTaG9wIFNBIGFuZCBDb250cmlidXRvcnMgPGNvbnRhY3RAcHJlc3Rhc2hvcC5jb20+XG4gKiBAY29weXJpZ2h0IFNpbmNlIDIwMDcgUHJlc3RhU2hvcCBTQSBhbmQgQ29udHJpYnV0b3JzXG4gKiBAbGljZW5zZSAgIGh0dHBzOi8vb3BlbnNvdXJjZS5vcmcvbGljZW5zZXMvT1NMLTMuMCBPcGVuIFNvZnR3YXJlIExpY2Vuc2UgKE9TTCAzLjApXG4gKi9cblxuaW1wb3J0IEltcG9ydFBhZ2UgZnJvbSAnLi9JbXBvcnRQYWdlJztcblxuY29uc3QgeyR9ID0gd2luZG93O1xuXG4kKCgpID0+IHtcbiAgbmV3IEltcG9ydFBhZ2UoKTtcbn0pO1xuIl0sInNvdXJjZVJvb3QiOiIifQ==