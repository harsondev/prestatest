/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./js/pages/product/combination/image-selector.js":
/*!********************************************************!*\
  !*** ./js/pages/product/combination/image-selector.js ***!
  \********************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

"use strict";


Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _productMap = __webpack_require__(/*! @pages/product/product-map */ "./js/pages/product/product-map.js");

var _productMap2 = _interopRequireDefault(_productMap);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _window = window,
    $ = _window.$; /**
                    * Copyright since 2007 PrestaShop SA and Contributors
                    * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
                    *
                    * NOTICE OF LICENSE
                    *
                    * This source file is subject to the Open Software License (OSL 3.0)
                    * that is bundled with this package in the file LICENSE.md.
                    * It is also available through the world-wide-web at this URL:
                    * https://opensource.org/licenses/OSL-3.0
                    * If you did not receive a copy of the license and are unable to
                    * obtain it through the world-wide-web, please send an email
                    * to license@prestashop.com so we can send you a copy immediately.
                    *
                    * DISCLAIMER
                    *
                    * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
                    * versions in the future. If you wish to customize PrestaShop for your
                    * needs please refer to https://devdocs.prestashop.com/ for more information.
                    *
                    * @author    PrestaShop SA and Contributors <contact@prestashop.com>
                    * @copyright Since 2007 PrestaShop SA and Contributors
                    * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
                    */

var ImageSelector = function () {
  function ImageSelector() {
    (0, _classCallCheck3.default)(this, ImageSelector);

    this.$selectorContainer = $(_productMap2.default.combinations.images.selectorContainer);
    this.init();
  }

  (0, _createClass3.default)(ImageSelector, [{
    key: 'init',
    value: function init() {
      $(_productMap2.default.combinations.images.checkboxContainer, this.$selectorContainer).hide();
      this.$selectorContainer.on('click', _productMap2.default.combinations.images.imageChoice, function (event) {
        var $imageChoice = $(event.currentTarget);
        var $checkbox = $(_productMap2.default.combinations.images.checkbox, $imageChoice);

        var isChecked = $checkbox.prop('checked');
        $imageChoice.toggleClass('selected', !isChecked);
        $checkbox.prop('checked', !isChecked);
      });
    }
  }]);
  return ImageSelector;
}();

exports.default = ImageSelector;

/***/ }),

/***/ "./js/pages/product/edit/product-suppliers-manager.js":
/*!************************************************************!*\
  !*** ./js/pages/product/edit/product-suppliers-manager.js ***!
  \************************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

"use strict";


Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var _create = __webpack_require__(/*! babel-runtime/core-js/object/create */ "./node_modules/babel-runtime/core-js/object/create.js");

var _create2 = _interopRequireDefault(_create);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _suppliersMap = __webpack_require__(/*! @pages/product/suppliers-map */ "./js/pages/product/suppliers-map.js");

var _suppliersMap2 = _interopRequireDefault(_suppliersMap);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _window = window,
    $ = _window.$; /**
                    * Copyright since 2007 PrestaShop SA and Contributors
                    * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
                    *
                    * NOTICE OF LICENSE
                    *
                    * This source file is subject to the Open Software License (OSL 3.0)
                    * that is bundled with this package in the file LICENSE.md.
                    * It is also available through the world-wide-web at this URL:
                    * https://opensource.org/licenses/OSL-3.0
                    * If you did not receive a copy of the license and are unable to
                    * obtain it through the world-wide-web, please send an email
                    * to license@prestashop.com so we can send you a copy immediately.
                    *
                    * DISCLAIMER
                    *
                    * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
                    * versions in the future. If you wish to customize PrestaShop for your
                    * needs please refer to https://devdocs.prestashop.com/ for more information.
                    *
                    * @author    PrestaShop SA and Contributors <contact@prestashop.com>
                    * @copyright Since 2007 PrestaShop SA and Contributors
                    * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
                    */

var ProductSuppliersManager = function () {
  /**
   *
   * @param {string} suppliersFormId
   * @param {boolean} forceUpdateDefault
   *
   * @returns {{}}
   */
  function ProductSuppliersManager(suppliersFormId, forceUpdateDefault) {
    (0, _classCallCheck3.default)(this, ProductSuppliersManager);

    this.forceUpdateDefault = forceUpdateDefault;
    this.suppliersMap = (0, _suppliersMap2.default)(suppliersFormId);
    this.$productSuppliersCollection = $(this.suppliersMap.productSuppliersCollection);
    this.$supplierIdsGroup = $(this.suppliersMap.supplierIdsInput).closest('.form-group');
    this.$defaultSupplierGroup = $(this.suppliersMap.defaultSupplierInput).closest('.form-group');
    this.$productsTable = $(this.suppliersMap.productSuppliersTable);
    this.$productsTableBody = $(this.suppliersMap.productsSuppliersTableBody);

    this.suppliers = [];
    this.prototypeTemplate = this.$productSuppliersCollection.data('prototype');
    this.prototypeName = this.$productSuppliersCollection.data('prototypeName');
    this.defaultDataForSupplier = this.getDefaultDataForSupplier();

    this.init();

    return {};
  }

  (0, _createClass3.default)(ProductSuppliersManager, [{
    key: 'init',
    value: function init() {
      var _this = this;

      this.memorizeCurrentSuppliers();
      this.toggleTableVisibility();
      this.refreshDefaultSupplierBlock();

      this.$initialDefault = this.$defaultSupplierGroup.find('input:checked').first();
      if (this.$initialDefault.length) {
        this.$initialDefault.closest(this.suppliersMap.checkboxContainer).addClass(this.suppliersMap.defaultSupplierClass);
      }

      this.$productsTable.on('change', 'input', function () {
        _this.memorizeCurrentSuppliers();
      });

      this.$supplierIdsGroup.on('change', 'input', function (e) {
        var input = e.currentTarget;

        if (input.checked) {
          _this.addSupplier({
            supplierId: input.value,
            supplierName: input.dataset.label
          });
        } else {
          _this.removeSupplier(input.value);
        }

        _this.renderSuppliers();
        _this.toggleTableVisibility();
        _this.refreshDefaultSupplierBlock();
      });
    }
  }, {
    key: 'toggleTableVisibility',
    value: function toggleTableVisibility() {
      if (this.getSelectedSuppliers().length === 0) {
        this.hideTable();

        return;
      }

      this.showTable();
    }

    /**
     * @param {Object} supplier
     */

  }, {
    key: 'addSupplier',
    value: function addSupplier(supplier) {
      if (typeof this.suppliers[supplier.supplierId] === 'undefined') {
        var newSupplier = (0, _create2.default)(this.defaultDataForSupplier);
        newSupplier.supplierId = supplier.supplierId;
        newSupplier.supplierName = supplier.supplierName;

        this.suppliers[supplier.supplierId] = newSupplier;
      } else {
        this.suppliers[supplier.supplierId].removed = false;
      }
    }

    /**
     * @param {int} supplierId
     */

  }, {
    key: 'removeSupplier',
    value: function removeSupplier(supplierId) {
      this.suppliers[supplierId].removed = true;
    }
  }, {
    key: 'renderSuppliers',
    value: function renderSuppliers() {
      var _this2 = this;

      this.$productsTableBody.empty();

      // Loop through select suppliers so that we use the same order as in the select list
      this.getSelectedSuppliers().forEach(function (selectedSupplier) {
        var supplier = _this2.suppliers[selectedSupplier.supplierId];

        if (supplier.removed) {
          return;
        }

        var productSupplierRow = _this2.prototypeTemplate.replace(new RegExp(_this2.prototypeName, 'g'), supplier.supplierId);

        _this2.$productsTableBody.append(productSupplierRow);
        // Fill inputs
        var rowMap = _this2.suppliersMap.productSupplierRow;
        $(rowMap.supplierIdInput(supplier.supplierId)).val(supplier.supplierId);
        $(rowMap.supplierNamePreview(supplier.supplierId)).html(supplier.supplierName);
        $(rowMap.supplierNameInput(supplier.supplierId)).val(supplier.supplierName);
        $(rowMap.productSupplierIdInput(supplier.supplierId)).val(supplier.productSupplierId);
        $(rowMap.referenceInput(supplier.supplierId)).val(supplier.reference);
        $(rowMap.priceInput(supplier.supplierId)).val(supplier.price);
        $(rowMap.currencyIdInput(supplier.supplierId)).val(supplier.currencyId);
      });
    }
  }, {
    key: 'getSelectedSuppliers',
    value: function getSelectedSuppliers() {
      var selectedSuppliers = [];
      this.$supplierIdsGroup.find('input:checked').each(function (index, input) {
        selectedSuppliers.push({
          supplierName: input.dataset.label,
          supplierId: input.value
        });
      });

      return selectedSuppliers;
    }
  }, {
    key: 'refreshDefaultSupplierBlock',
    value: function refreshDefaultSupplierBlock() {
      var _this3 = this;

      var suppliers = this.getSelectedSuppliers();

      if (suppliers.length === 0) {
        if (this.forceUpdateDefault) {
          this.$defaultSupplierGroup.find('input').prop('checked', false);
        }
        this.hideDefaultSuppliers();

        return;
      }

      this.showDefaultSuppliers();
      var selectedSupplierIds = suppliers.map(function (supplier) {
        return supplier.supplierId;
      });

      this.$defaultSupplierGroup.find('input').each(function (key, input) {
        var isValid = selectedSupplierIds.includes(input.value);

        if (_this3.forceUpdateDefault && !isValid) {
          input.checked = false;
        }
        input.disabled = !isValid;
      });

      if (this.$defaultSupplierGroup.find('input:checked').length === 0 && this.forceUpdateDefault) {
        this.checkFirstAvailableDefaultSupplier(selectedSupplierIds);
      }
    }
  }, {
    key: 'hideDefaultSuppliers',
    value: function hideDefaultSuppliers() {
      this.$defaultSupplierGroup.addClass('d-none');
    }
  }, {
    key: 'showDefaultSuppliers',
    value: function showDefaultSuppliers() {
      this.$defaultSupplierGroup.removeClass('d-none');
    }

    /**
     * @param {int[]} selectedSupplierIds
     */

  }, {
    key: 'checkFirstAvailableDefaultSupplier',
    value: function checkFirstAvailableDefaultSupplier(selectedSupplierIds) {
      var firstSupplierId = selectedSupplierIds[0];
      this.$defaultSupplierGroup.find('input[value="' + firstSupplierId + '"]').prop('checked', true);
    }
  }, {
    key: 'showTable',
    value: function showTable() {
      this.$productsTable.removeClass('d-none');
    }
  }, {
    key: 'hideTable',
    value: function hideTable() {
      this.$productsTable.addClass('d-none');
    }

    /**
     * Memorize suppliers to be able to re-render them later.
     * Flag `removed` allows identifying whether supplier was removed from list or should be rendered
     */

  }, {
    key: 'memorizeCurrentSuppliers',
    value: function memorizeCurrentSuppliers() {
      var _this4 = this;

      this.getSelectedSuppliers().forEach(function (supplier) {
        _this4.suppliers[supplier.supplierId] = {
          supplierId: supplier.supplierId,
          productSupplierId: $(_this4.suppliersMap.productSupplierRow.productSupplierIdInput(supplier.supplierId)).val(),
          supplierName: $(_this4.suppliersMap.productSupplierRow.supplierNameInput(supplier.supplierId)).val(),
          reference: $(_this4.suppliersMap.productSupplierRow.referenceInput(supplier.supplierId)).val(),
          price: $(_this4.suppliersMap.productSupplierRow.priceInput(supplier.supplierId)).val(),
          currencyId: $(_this4.suppliersMap.productSupplierRow.currencyIdInput(supplier.supplierId)).val(),
          removed: false
        };
      });
    }

    /**
     * Create a "shadow" prototype just to parse default values set inside the input fields,
     * this allow to build an object with default values set in the FormType
     *
     * @returns {{reference, removed: boolean, price, currencyId, productSupplierId}}
     */

  }, {
    key: 'getDefaultDataForSupplier',
    value: function getDefaultDataForSupplier() {
      var rowPrototype = new DOMParser().parseFromString(this.prototypeTemplate, 'text/html');

      return {
        removed: false,
        productSupplierId: this.getDataFromRow(this.suppliersMap.productSupplierRow.productSupplierIdInput, rowPrototype),
        reference: this.getDataFromRow(this.suppliersMap.productSupplierRow.referenceInput, rowPrototype),
        price: this.getDataFromRow(this.suppliersMap.productSupplierRow.priceInput, rowPrototype),
        currencyId: this.getDataFromRow(this.suppliersMap.productSupplierRow.currencyIdInput, rowPrototype)
      };
    }

    /**
     * @param selectorGenerator {function}
     * @param rowPrototype {Document}
     *
     * @returns {*}
     */

  }, {
    key: 'getDataFromRow',
    value: function getDataFromRow(selectorGenerator, rowPrototype) {
      return rowPrototype.querySelector(selectorGenerator(this.prototypeName)).value;
    }
  }]);
  return ProductSuppliersManager;
}();

exports.default = ProductSuppliersManager;

/***/ }),

/***/ "./js/pages/product/product-map.js":
/*!*****************************************!*\
  !*** ./js/pages/product/product-map.js ***!
  \*****************************************/
/***/ ((__unused_webpack_module, exports) => {

"use strict";


Object.defineProperty(exports, "__esModule", ({
  value: true
}));
/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 */

var combinationListId = '#combination_list';

exports.default = {
  productForm: 'form[name=product]',
  productTypeSelector: '#product_header_type',
  productType: {
    STANDARD: 'standard',
    PACK: 'pack',
    VIRTUAL: 'virtual',
    COMBINATIONS: 'combinations'
  },
  invalidField: '.is-invalid',
  productFormSubmitButton: '.product-form-save-button',
  navigationBar: '#form-nav',
  dropzoneImagesContainer: '.product-image-dropzone',
  featureValues: {
    collectionContainer: '.feature-values-collection',
    collectionRowsContainer: '.feature-values-collection > .col-sm',
    collectionRow: 'div.row.product-feature',
    featureSelect: 'select.feature-selector',
    featureValueSelect: 'select.feature-value-selector',
    customValueInput: '.custom-values input',
    customFeatureIdInput: 'input.custom-value-id',
    deleteFeatureValue: 'button.delete-feature-value',
    addFeatureValue: '.feature-value-add-button'
  },
  customizations: {
    customizationsContainer: '.product-customizations-collection',
    customizationFieldsList: '.product-customizations-collection ul',
    addCustomizationBtn: '.add-customization-btn',
    removeCustomizationBtn: '.remove-customization-btn',
    customizationFieldRow: '.customization-field-row'
  },
  combinations: {
    navigationTab: '#combinations-tab-nav',
    externalCombinationTab: '#external-combination-tab',
    preloader: '#combinations-preloader',
    emptyState: '#combinations-empty-state',
    combinationsPaginatedList: '#combinations-paginated-list',
    combinationsContainer: '' + combinationListId,
    combinationsFiltersContainer: '#combinations_filters',
    combinationsGeneratorContainer: '#product_combinations_generator',
    combinationsTable: combinationListId + ' table',
    combinationsTableBody: combinationListId + ' table tbody',
    combinationIdInputsSelector: '.combination-id-input',
    isDefaultInputsSelector: '.combination-is-default-input',
    removeCombinationSelector: '.remove-combination-item',
    combinationName: 'form .card-header span',
    paginationContainer: '#combinations-pagination',
    loadingSpinner: '#productCombinationsLoading',
    quantityInputWrapper: '.combination-quantity',
    impactOnPriceInputWrapper: '.combination-impact-on-price',
    referenceInputWrapper: '.combination-reference',
    sortableColumns: '.ps-sortable-column',
    combinationItemForm: {
      quantityKey: 'combination_item[quantity][value]',
      impactOnPriceKey: 'combination_item[impact_on_price][value]',
      referenceKey: 'combination_item[reference][value]',
      tokenKey: 'combination_item[_token]'
    },
    editionForm: 'form[name="combination_form"]',
    editionFormInputs:
    // eslint-disable-next-line
    'form[name="combination_form"] input, form[name="combination_form"] textarea, form[name="combination_form"] select',
    editCombinationButtons: '.edit-combination-item',
    tableRow: {
      combinationImg: '.combination-image',
      combinationCheckbox: function combinationCheckbox(rowIndex) {
        return combinationListId + '_combinations_' + rowIndex + '_is_selected';
      },
      combinationIdInput: function combinationIdInput(rowIndex) {
        return combinationListId + '_combinations_' + rowIndex + '_combination_id';
      },
      combinationNameInput: function combinationNameInput(rowIndex) {
        return combinationListId + '_combinations_' + rowIndex + '_name';
      },
      referenceInput: function referenceInput(rowIndex) {
        return combinationListId + '_combinations_' + rowIndex + '_reference_value';
      },
      impactOnPriceInput: function impactOnPriceInput(rowIndex) {
        return combinationListId + '_combinations_' + rowIndex + '_impact_on_price_value';
      },
      finalPriceTeInput: function finalPriceTeInput(rowIndex) {
        return combinationListId + '_combinations_' + rowIndex + '_final_price_te';
      },
      quantityInput: function quantityInput(rowIndex) {
        return combinationListId + '_combinations_' + rowIndex + '_quantity_value';
      },
      isDefaultInput: function isDefaultInput(rowIndex) {
        return combinationListId + '_combinations_' + rowIndex + '_is_default';
      },
      editButton: function editButton(rowIndex) {
        return combinationListId + '_combinations_' + rowIndex + '_edit';
      },
      deleteButton: function deleteButton(rowIndex) {
        return combinationListId + '_combinations_' + rowIndex + '_delete';
      }
    },
    editModal: '#combination-edit-modal',
    images: {
      selectorContainer: '.combination-images-selector',
      imageChoice: '.combination-image-choice',
      checkboxContainer: '.form-check',
      checkbox: 'input[type=checkbox]'
    },
    scrollBar: '.attributes-list-overflow',
    searchInput: '#product-combinations-generate .attributes-search',
    generateCombinationsButton: '.generate-combinations-button'
  },
  virtualProduct: {
    container: '.virtual-product-file-container',
    fileContentContainer: '.virtual-product-file-content'
  },
  dropzone: {
    configuration: {
      fileManager: '.openfilemanager'
    },
    photoswipe: {
      element: '.pswp'
    },
    dzTemplate: '.dz-template',
    dzPreview: '.dz-preview',
    sortableContainer: '#product-images-dropzone',
    sortableItems: 'div.dz-preview:not(.disabled)',
    dropzoneContainer: '.dropzone-container',
    checkbox: '.md-checkbox input',
    shownTooltips: '.tooltip.show',
    savedImageContainer: function savedImageContainer(imageId) {
      return '.dz-preview[data-id="' + imageId + '"]';
    },
    savedImage: function savedImage(imageId) {
      return '.dz-preview[data-id="' + imageId + '"] img';
    },
    coveredPreview: '.dz-preview.is-cover',
    windowFileManager: '.dropzone-window-filemanager'
  },
  suppliers: {
    productSuppliers: '#product_options_suppliers',
    combinationSuppliers: '#combination_form_suppliers'
  },
  seo: {
    container: '#product_seo_serp',
    defaultTitle: '.serp-default-title:input',
    watchedTitle: '.serp-watched-title:input',
    defaultDescription: '.serp-default-description',
    watchedDescription: '.serp-watched-description',
    watchedMetaUrl: '.serp-watched-url:input',
    redirectOption: {
      typeInput: '#product_seo_redirect_option_type',
      targetInput: '#product_seo_redirect_option_target'
    }
  },
  jsTabs: '.js-tabs',
  jsArrow: '.js-arrow',
  jsNavTabs: '.js-nav-tabs',
  toggleTab: '[data-toggle="tab"]',
  formContentTab: '#form_content > .form-contenttab',
  leftArrow: '.left-arrow',
  rightArrow: '.right-arrow',
  footer: {
    previewUrlButton: '.preview-url-button',
    deleteProductButton: '.delete-product-button'
  },
  categories: {
    categoriesContainer: '.js-categories-container',
    categoryTree: '.js-categories-tree',
    treeElement: '.category-tree-element',
    treeElementInputs: '.category-tree-inputs',
    checkboxInput: '[type=checkbox]',
    checkedCheckboxInputs: '[type=checkbox]:checked',
    checkboxName: function checkboxName(categoryId) {
      return 'product[categories][product_categories][' + categoryId + '][is_associated]';
    },
    materialCheckbox: '.md-checkbox',
    radioInput: '[type=radio]',
    defaultRadioInput: '[type=radio]:checked',
    radioName: function radioName(categoryId) {
      return 'product[categories][product_categories][' + categoryId + '][is_default]';
    },
    tagsContainer: '#categories-tags-container',
    searchInput: '#ps-select-product-category',
    fieldset: '.tree-fieldset',
    loader: '.categories-tree-loader',
    childrenList: '.children-list',
    everyItems: '.less, .more',
    expandAllButton: '#categories-tree-expand',
    reduceAllButton: '#categories-tree-reduce'
  },
  modules: {
    previewContainer: '.module-render-container.all-modules',
    previewButton: '.modules-list-button',
    selectorContainer: '.module-selection',
    moduleSelector: '.modules-list-select',
    selectorPreviews: '.module-selection .module-render-container',
    selectorPreview: function selectorPreview(moduleId) {
      return '.module-selection .module-render-container.' + moduleId;
    },
    contentContainer: '.module-contents',
    moduleContents: '.module-contents .module-render-container',
    moduleContent: function moduleContent(moduleId) {
      return '.module-contents .module-render-container.' + moduleId;
    }
  }
};

/***/ }),

/***/ "./js/pages/product/suppliers-map.js":
/*!*******************************************!*\
  !*** ./js/pages/product/suppliers-map.js ***!
  \*******************************************/
/***/ ((__unused_webpack_module, exports) => {

"use strict";


Object.defineProperty(exports, "__esModule", ({
  value: true
}));

/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 */

exports.default = function (suppliersFormId) {
  var productSuppliersId = suppliersFormId + '_product_suppliers';
  var productSupplierInputId = function productSupplierInputId(supplierIndex, inputName) {
    return productSuppliersId + '_' + supplierIndex + '_' + inputName;
  };

  return {
    productSuppliersCollection: '' + productSuppliersId,
    supplierIdsInput: suppliersFormId + '_supplier_ids',
    defaultSupplierInput: suppliersFormId + '_default_supplier_id',
    productSuppliersTable: productSuppliersId + ' table',
    productsSuppliersTableBody: productSuppliersId + ' table tbody',
    defaultSupplierClass: 'default-supplier',
    productSupplierRow: {
      supplierIdInput: function supplierIdInput(supplierIndex) {
        return productSupplierInputId(supplierIndex, 'supplier_id');
      },
      supplierNameInput: function supplierNameInput(supplierIndex) {
        return productSupplierInputId(supplierIndex, 'supplier_name');
      },
      productSupplierIdInput: function productSupplierIdInput(supplierIndex) {
        return productSupplierInputId(supplierIndex, 'product_supplier_id');
      },
      referenceInput: function referenceInput(supplierIndex) {
        return productSupplierInputId(supplierIndex, 'reference');
      },
      priceInput: function priceInput(supplierIndex) {
        return productSupplierInputId(supplierIndex, 'price_tax_excluded');
      },
      currencyIdInput: function currencyIdInput(supplierIndex) {
        return productSupplierInputId(supplierIndex, 'currency_id');
      },
      supplierNamePreview: function supplierNamePreview(supplierIndex) {
        return '#product_supplier_row_' + supplierIndex + ' .supplier_name .preview';
      }
    },
    checkboxContainer: '.form-check'
  };
};

/***/ }),

/***/ "./node_modules/babel-runtime/core-js/object/create.js":
/*!*************************************************************!*\
  !*** ./node_modules/babel-runtime/core-js/object/create.js ***!
  \*************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

module.exports = { "default": __webpack_require__(/*! core-js/library/fn/object/create */ "./node_modules/core-js/library/fn/object/create.js"), __esModule: true };

/***/ }),

/***/ "./node_modules/babel-runtime/core-js/object/define-property.js":
/*!**********************************************************************!*\
  !*** ./node_modules/babel-runtime/core-js/object/define-property.js ***!
  \**********************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

module.exports = { "default": __webpack_require__(/*! core-js/library/fn/object/define-property */ "./node_modules/core-js/library/fn/object/define-property.js"), __esModule: true };

/***/ }),

/***/ "./node_modules/babel-runtime/helpers/classCallCheck.js":
/*!**************************************************************!*\
  !*** ./node_modules/babel-runtime/helpers/classCallCheck.js ***!
  \**************************************************************/
/***/ ((__unused_webpack_module, exports) => {

"use strict";


exports.__esModule = true;

exports.default = function (instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
};

/***/ }),

/***/ "./node_modules/babel-runtime/helpers/createClass.js":
/*!***********************************************************!*\
  !*** ./node_modules/babel-runtime/helpers/createClass.js ***!
  \***********************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

"use strict";


exports.__esModule = true;

var _defineProperty = __webpack_require__(/*! ../core-js/object/define-property */ "./node_modules/babel-runtime/core-js/object/define-property.js");

var _defineProperty2 = _interopRequireDefault(_defineProperty);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      (0, _defineProperty2.default)(target, descriptor.key, descriptor);
    }
  }

  return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);
    if (staticProps) defineProperties(Constructor, staticProps);
    return Constructor;
  };
}();

/***/ }),

/***/ "./node_modules/core-js/library/fn/object/create.js":
/*!**********************************************************!*\
  !*** ./node_modules/core-js/library/fn/object/create.js ***!
  \**********************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

__webpack_require__(/*! ../../modules/es6.object.create */ "./node_modules/core-js/library/modules/es6.object.create.js");
var $Object = __webpack_require__(/*! ../../modules/_core */ "./node_modules/core-js/library/modules/_core.js").Object;
module.exports = function create(P, D) {
  return $Object.create(P, D);
};


/***/ }),

/***/ "./node_modules/core-js/library/fn/object/define-property.js":
/*!*******************************************************************!*\
  !*** ./node_modules/core-js/library/fn/object/define-property.js ***!
  \*******************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

__webpack_require__(/*! ../../modules/es6.object.define-property */ "./node_modules/core-js/library/modules/es6.object.define-property.js");
var $Object = __webpack_require__(/*! ../../modules/_core */ "./node_modules/core-js/library/modules/_core.js").Object;
module.exports = function defineProperty(it, key, desc) {
  return $Object.defineProperty(it, key, desc);
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_a-function.js":
/*!*************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_a-function.js ***!
  \*************************************************************/
/***/ ((module) => {

module.exports = function (it) {
  if (typeof it != 'function') throw TypeError(it + ' is not a function!');
  return it;
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_an-object.js":
/*!************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_an-object.js ***!
  \************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var isObject = __webpack_require__(/*! ./_is-object */ "./node_modules/core-js/library/modules/_is-object.js");
module.exports = function (it) {
  if (!isObject(it)) throw TypeError(it + ' is not an object!');
  return it;
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_array-includes.js":
/*!*****************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_array-includes.js ***!
  \*****************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// false -> Array#indexOf
// true  -> Array#includes
var toIObject = __webpack_require__(/*! ./_to-iobject */ "./node_modules/core-js/library/modules/_to-iobject.js");
var toLength = __webpack_require__(/*! ./_to-length */ "./node_modules/core-js/library/modules/_to-length.js");
var toAbsoluteIndex = __webpack_require__(/*! ./_to-absolute-index */ "./node_modules/core-js/library/modules/_to-absolute-index.js");
module.exports = function (IS_INCLUDES) {
  return function ($this, el, fromIndex) {
    var O = toIObject($this);
    var length = toLength(O.length);
    var index = toAbsoluteIndex(fromIndex, length);
    var value;
    // Array#includes uses SameValueZero equality algorithm
    // eslint-disable-next-line no-self-compare
    if (IS_INCLUDES && el != el) while (length > index) {
      value = O[index++];
      // eslint-disable-next-line no-self-compare
      if (value != value) return true;
    // Array#indexOf ignores holes, Array#includes - not
    } else for (;length > index; index++) if (IS_INCLUDES || index in O) {
      if (O[index] === el) return IS_INCLUDES || index || 0;
    } return !IS_INCLUDES && -1;
  };
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_cof.js":
/*!******************************************************!*\
  !*** ./node_modules/core-js/library/modules/_cof.js ***!
  \******************************************************/
/***/ ((module) => {

var toString = {}.toString;

module.exports = function (it) {
  return toString.call(it).slice(8, -1);
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_core.js":
/*!*******************************************************!*\
  !*** ./node_modules/core-js/library/modules/_core.js ***!
  \*******************************************************/
/***/ ((module) => {

var core = module.exports = { version: '2.6.11' };
if (typeof __e == 'number') __e = core; // eslint-disable-line no-undef


/***/ }),

/***/ "./node_modules/core-js/library/modules/_ctx.js":
/*!******************************************************!*\
  !*** ./node_modules/core-js/library/modules/_ctx.js ***!
  \******************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// optional / simple context binding
var aFunction = __webpack_require__(/*! ./_a-function */ "./node_modules/core-js/library/modules/_a-function.js");
module.exports = function (fn, that, length) {
  aFunction(fn);
  if (that === undefined) return fn;
  switch (length) {
    case 1: return function (a) {
      return fn.call(that, a);
    };
    case 2: return function (a, b) {
      return fn.call(that, a, b);
    };
    case 3: return function (a, b, c) {
      return fn.call(that, a, b, c);
    };
  }
  return function (/* ...args */) {
    return fn.apply(that, arguments);
  };
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_defined.js":
/*!**********************************************************!*\
  !*** ./node_modules/core-js/library/modules/_defined.js ***!
  \**********************************************************/
/***/ ((module) => {

// 7.2.1 RequireObjectCoercible(argument)
module.exports = function (it) {
  if (it == undefined) throw TypeError("Can't call method on  " + it);
  return it;
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_descriptors.js":
/*!**************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_descriptors.js ***!
  \**************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// Thank's IE8 for his funny defineProperty
module.exports = !__webpack_require__(/*! ./_fails */ "./node_modules/core-js/library/modules/_fails.js")(function () {
  return Object.defineProperty({}, 'a', { get: function () { return 7; } }).a != 7;
});


/***/ }),

/***/ "./node_modules/core-js/library/modules/_dom-create.js":
/*!*************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_dom-create.js ***!
  \*************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var isObject = __webpack_require__(/*! ./_is-object */ "./node_modules/core-js/library/modules/_is-object.js");
var document = __webpack_require__(/*! ./_global */ "./node_modules/core-js/library/modules/_global.js").document;
// typeof document.createElement is 'object' in old IE
var is = isObject(document) && isObject(document.createElement);
module.exports = function (it) {
  return is ? document.createElement(it) : {};
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_enum-bug-keys.js":
/*!****************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_enum-bug-keys.js ***!
  \****************************************************************/
/***/ ((module) => {

// IE 8- don't enum bug keys
module.exports = (
  'constructor,hasOwnProperty,isPrototypeOf,propertyIsEnumerable,toLocaleString,toString,valueOf'
).split(',');


/***/ }),

/***/ "./node_modules/core-js/library/modules/_export.js":
/*!*********************************************************!*\
  !*** ./node_modules/core-js/library/modules/_export.js ***!
  \*********************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var global = __webpack_require__(/*! ./_global */ "./node_modules/core-js/library/modules/_global.js");
var core = __webpack_require__(/*! ./_core */ "./node_modules/core-js/library/modules/_core.js");
var ctx = __webpack_require__(/*! ./_ctx */ "./node_modules/core-js/library/modules/_ctx.js");
var hide = __webpack_require__(/*! ./_hide */ "./node_modules/core-js/library/modules/_hide.js");
var has = __webpack_require__(/*! ./_has */ "./node_modules/core-js/library/modules/_has.js");
var PROTOTYPE = 'prototype';

var $export = function (type, name, source) {
  var IS_FORCED = type & $export.F;
  var IS_GLOBAL = type & $export.G;
  var IS_STATIC = type & $export.S;
  var IS_PROTO = type & $export.P;
  var IS_BIND = type & $export.B;
  var IS_WRAP = type & $export.W;
  var exports = IS_GLOBAL ? core : core[name] || (core[name] = {});
  var expProto = exports[PROTOTYPE];
  var target = IS_GLOBAL ? global : IS_STATIC ? global[name] : (global[name] || {})[PROTOTYPE];
  var key, own, out;
  if (IS_GLOBAL) source = name;
  for (key in source) {
    // contains in native
    own = !IS_FORCED && target && target[key] !== undefined;
    if (own && has(exports, key)) continue;
    // export native or passed
    out = own ? target[key] : source[key];
    // prevent global pollution for namespaces
    exports[key] = IS_GLOBAL && typeof target[key] != 'function' ? source[key]
    // bind timers to global for call from export context
    : IS_BIND && own ? ctx(out, global)
    // wrap global constructors for prevent change them in library
    : IS_WRAP && target[key] == out ? (function (C) {
      var F = function (a, b, c) {
        if (this instanceof C) {
          switch (arguments.length) {
            case 0: return new C();
            case 1: return new C(a);
            case 2: return new C(a, b);
          } return new C(a, b, c);
        } return C.apply(this, arguments);
      };
      F[PROTOTYPE] = C[PROTOTYPE];
      return F;
    // make static versions for prototype methods
    })(out) : IS_PROTO && typeof out == 'function' ? ctx(Function.call, out) : out;
    // export proto methods to core.%CONSTRUCTOR%.methods.%NAME%
    if (IS_PROTO) {
      (exports.virtual || (exports.virtual = {}))[key] = out;
      // export proto methods to core.%CONSTRUCTOR%.prototype.%NAME%
      if (type & $export.R && expProto && !expProto[key]) hide(expProto, key, out);
    }
  }
};
// type bitmap
$export.F = 1;   // forced
$export.G = 2;   // global
$export.S = 4;   // static
$export.P = 8;   // proto
$export.B = 16;  // bind
$export.W = 32;  // wrap
$export.U = 64;  // safe
$export.R = 128; // real proto method for `library`
module.exports = $export;


/***/ }),

/***/ "./node_modules/core-js/library/modules/_fails.js":
/*!********************************************************!*\
  !*** ./node_modules/core-js/library/modules/_fails.js ***!
  \********************************************************/
/***/ ((module) => {

module.exports = function (exec) {
  try {
    return !!exec();
  } catch (e) {
    return true;
  }
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_global.js":
/*!*********************************************************!*\
  !*** ./node_modules/core-js/library/modules/_global.js ***!
  \*********************************************************/
/***/ ((module) => {

// https://github.com/zloirock/core-js/issues/86#issuecomment-115759028
var global = module.exports = typeof window != 'undefined' && window.Math == Math
  ? window : typeof self != 'undefined' && self.Math == Math ? self
  // eslint-disable-next-line no-new-func
  : Function('return this')();
if (typeof __g == 'number') __g = global; // eslint-disable-line no-undef


/***/ }),

/***/ "./node_modules/core-js/library/modules/_has.js":
/*!******************************************************!*\
  !*** ./node_modules/core-js/library/modules/_has.js ***!
  \******************************************************/
/***/ ((module) => {

var hasOwnProperty = {}.hasOwnProperty;
module.exports = function (it, key) {
  return hasOwnProperty.call(it, key);
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_hide.js":
/*!*******************************************************!*\
  !*** ./node_modules/core-js/library/modules/_hide.js ***!
  \*******************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var dP = __webpack_require__(/*! ./_object-dp */ "./node_modules/core-js/library/modules/_object-dp.js");
var createDesc = __webpack_require__(/*! ./_property-desc */ "./node_modules/core-js/library/modules/_property-desc.js");
module.exports = __webpack_require__(/*! ./_descriptors */ "./node_modules/core-js/library/modules/_descriptors.js") ? function (object, key, value) {
  return dP.f(object, key, createDesc(1, value));
} : function (object, key, value) {
  object[key] = value;
  return object;
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_html.js":
/*!*******************************************************!*\
  !*** ./node_modules/core-js/library/modules/_html.js ***!
  \*******************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var document = __webpack_require__(/*! ./_global */ "./node_modules/core-js/library/modules/_global.js").document;
module.exports = document && document.documentElement;


/***/ }),

/***/ "./node_modules/core-js/library/modules/_ie8-dom-define.js":
/*!*****************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_ie8-dom-define.js ***!
  \*****************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

module.exports = !__webpack_require__(/*! ./_descriptors */ "./node_modules/core-js/library/modules/_descriptors.js") && !__webpack_require__(/*! ./_fails */ "./node_modules/core-js/library/modules/_fails.js")(function () {
  return Object.defineProperty(__webpack_require__(/*! ./_dom-create */ "./node_modules/core-js/library/modules/_dom-create.js")('div'), 'a', { get: function () { return 7; } }).a != 7;
});


/***/ }),

/***/ "./node_modules/core-js/library/modules/_iobject.js":
/*!**********************************************************!*\
  !*** ./node_modules/core-js/library/modules/_iobject.js ***!
  \**********************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// fallback for non-array-like ES3 and non-enumerable old V8 strings
var cof = __webpack_require__(/*! ./_cof */ "./node_modules/core-js/library/modules/_cof.js");
// eslint-disable-next-line no-prototype-builtins
module.exports = Object('z').propertyIsEnumerable(0) ? Object : function (it) {
  return cof(it) == 'String' ? it.split('') : Object(it);
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_is-object.js":
/*!************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_is-object.js ***!
  \************************************************************/
/***/ ((module) => {

module.exports = function (it) {
  return typeof it === 'object' ? it !== null : typeof it === 'function';
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_library.js":
/*!**********************************************************!*\
  !*** ./node_modules/core-js/library/modules/_library.js ***!
  \**********************************************************/
/***/ ((module) => {

module.exports = true;


/***/ }),

/***/ "./node_modules/core-js/library/modules/_object-create.js":
/*!****************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_object-create.js ***!
  \****************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// 19.1.2.2 / 15.2.3.5 Object.create(O [, Properties])
var anObject = __webpack_require__(/*! ./_an-object */ "./node_modules/core-js/library/modules/_an-object.js");
var dPs = __webpack_require__(/*! ./_object-dps */ "./node_modules/core-js/library/modules/_object-dps.js");
var enumBugKeys = __webpack_require__(/*! ./_enum-bug-keys */ "./node_modules/core-js/library/modules/_enum-bug-keys.js");
var IE_PROTO = __webpack_require__(/*! ./_shared-key */ "./node_modules/core-js/library/modules/_shared-key.js")('IE_PROTO');
var Empty = function () { /* empty */ };
var PROTOTYPE = 'prototype';

// Create object with fake `null` prototype: use iframe Object with cleared prototype
var createDict = function () {
  // Thrash, waste and sodomy: IE GC bug
  var iframe = __webpack_require__(/*! ./_dom-create */ "./node_modules/core-js/library/modules/_dom-create.js")('iframe');
  var i = enumBugKeys.length;
  var lt = '<';
  var gt = '>';
  var iframeDocument;
  iframe.style.display = 'none';
  __webpack_require__(/*! ./_html */ "./node_modules/core-js/library/modules/_html.js").appendChild(iframe);
  iframe.src = 'javascript:'; // eslint-disable-line no-script-url
  // createDict = iframe.contentWindow.Object;
  // html.removeChild(iframe);
  iframeDocument = iframe.contentWindow.document;
  iframeDocument.open();
  iframeDocument.write(lt + 'script' + gt + 'document.F=Object' + lt + '/script' + gt);
  iframeDocument.close();
  createDict = iframeDocument.F;
  while (i--) delete createDict[PROTOTYPE][enumBugKeys[i]];
  return createDict();
};

module.exports = Object.create || function create(O, Properties) {
  var result;
  if (O !== null) {
    Empty[PROTOTYPE] = anObject(O);
    result = new Empty();
    Empty[PROTOTYPE] = null;
    // add "__proto__" for Object.getPrototypeOf polyfill
    result[IE_PROTO] = O;
  } else result = createDict();
  return Properties === undefined ? result : dPs(result, Properties);
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_object-dp.js":
/*!************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_object-dp.js ***!
  \************************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

var anObject = __webpack_require__(/*! ./_an-object */ "./node_modules/core-js/library/modules/_an-object.js");
var IE8_DOM_DEFINE = __webpack_require__(/*! ./_ie8-dom-define */ "./node_modules/core-js/library/modules/_ie8-dom-define.js");
var toPrimitive = __webpack_require__(/*! ./_to-primitive */ "./node_modules/core-js/library/modules/_to-primitive.js");
var dP = Object.defineProperty;

exports.f = __webpack_require__(/*! ./_descriptors */ "./node_modules/core-js/library/modules/_descriptors.js") ? Object.defineProperty : function defineProperty(O, P, Attributes) {
  anObject(O);
  P = toPrimitive(P, true);
  anObject(Attributes);
  if (IE8_DOM_DEFINE) try {
    return dP(O, P, Attributes);
  } catch (e) { /* empty */ }
  if ('get' in Attributes || 'set' in Attributes) throw TypeError('Accessors not supported!');
  if ('value' in Attributes) O[P] = Attributes.value;
  return O;
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_object-dps.js":
/*!*************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_object-dps.js ***!
  \*************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var dP = __webpack_require__(/*! ./_object-dp */ "./node_modules/core-js/library/modules/_object-dp.js");
var anObject = __webpack_require__(/*! ./_an-object */ "./node_modules/core-js/library/modules/_an-object.js");
var getKeys = __webpack_require__(/*! ./_object-keys */ "./node_modules/core-js/library/modules/_object-keys.js");

module.exports = __webpack_require__(/*! ./_descriptors */ "./node_modules/core-js/library/modules/_descriptors.js") ? Object.defineProperties : function defineProperties(O, Properties) {
  anObject(O);
  var keys = getKeys(Properties);
  var length = keys.length;
  var i = 0;
  var P;
  while (length > i) dP.f(O, P = keys[i++], Properties[P]);
  return O;
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_object-keys-internal.js":
/*!***********************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_object-keys-internal.js ***!
  \***********************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var has = __webpack_require__(/*! ./_has */ "./node_modules/core-js/library/modules/_has.js");
var toIObject = __webpack_require__(/*! ./_to-iobject */ "./node_modules/core-js/library/modules/_to-iobject.js");
var arrayIndexOf = __webpack_require__(/*! ./_array-includes */ "./node_modules/core-js/library/modules/_array-includes.js")(false);
var IE_PROTO = __webpack_require__(/*! ./_shared-key */ "./node_modules/core-js/library/modules/_shared-key.js")('IE_PROTO');

module.exports = function (object, names) {
  var O = toIObject(object);
  var i = 0;
  var result = [];
  var key;
  for (key in O) if (key != IE_PROTO) has(O, key) && result.push(key);
  // Don't enum bug & hidden keys
  while (names.length > i) if (has(O, key = names[i++])) {
    ~arrayIndexOf(result, key) || result.push(key);
  }
  return result;
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_object-keys.js":
/*!**************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_object-keys.js ***!
  \**************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// 19.1.2.14 / 15.2.3.14 Object.keys(O)
var $keys = __webpack_require__(/*! ./_object-keys-internal */ "./node_modules/core-js/library/modules/_object-keys-internal.js");
var enumBugKeys = __webpack_require__(/*! ./_enum-bug-keys */ "./node_modules/core-js/library/modules/_enum-bug-keys.js");

module.exports = Object.keys || function keys(O) {
  return $keys(O, enumBugKeys);
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_property-desc.js":
/*!****************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_property-desc.js ***!
  \****************************************************************/
/***/ ((module) => {

module.exports = function (bitmap, value) {
  return {
    enumerable: !(bitmap & 1),
    configurable: !(bitmap & 2),
    writable: !(bitmap & 4),
    value: value
  };
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_shared-key.js":
/*!*************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_shared-key.js ***!
  \*************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var shared = __webpack_require__(/*! ./_shared */ "./node_modules/core-js/library/modules/_shared.js")('keys');
var uid = __webpack_require__(/*! ./_uid */ "./node_modules/core-js/library/modules/_uid.js");
module.exports = function (key) {
  return shared[key] || (shared[key] = uid(key));
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_shared.js":
/*!*********************************************************!*\
  !*** ./node_modules/core-js/library/modules/_shared.js ***!
  \*********************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var core = __webpack_require__(/*! ./_core */ "./node_modules/core-js/library/modules/_core.js");
var global = __webpack_require__(/*! ./_global */ "./node_modules/core-js/library/modules/_global.js");
var SHARED = '__core-js_shared__';
var store = global[SHARED] || (global[SHARED] = {});

(module.exports = function (key, value) {
  return store[key] || (store[key] = value !== undefined ? value : {});
})('versions', []).push({
  version: core.version,
  mode: __webpack_require__(/*! ./_library */ "./node_modules/core-js/library/modules/_library.js") ? 'pure' : 'global',
  copyright: '© 2019 Denis Pushkarev (zloirock.ru)'
});


/***/ }),

/***/ "./node_modules/core-js/library/modules/_to-absolute-index.js":
/*!********************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_to-absolute-index.js ***!
  \********************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var toInteger = __webpack_require__(/*! ./_to-integer */ "./node_modules/core-js/library/modules/_to-integer.js");
var max = Math.max;
var min = Math.min;
module.exports = function (index, length) {
  index = toInteger(index);
  return index < 0 ? max(index + length, 0) : min(index, length);
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_to-integer.js":
/*!*************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_to-integer.js ***!
  \*************************************************************/
/***/ ((module) => {

// 7.1.4 ToInteger
var ceil = Math.ceil;
var floor = Math.floor;
module.exports = function (it) {
  return isNaN(it = +it) ? 0 : (it > 0 ? floor : ceil)(it);
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_to-iobject.js":
/*!*************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_to-iobject.js ***!
  \*************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// to indexed object, toObject with fallback for non-array-like ES3 strings
var IObject = __webpack_require__(/*! ./_iobject */ "./node_modules/core-js/library/modules/_iobject.js");
var defined = __webpack_require__(/*! ./_defined */ "./node_modules/core-js/library/modules/_defined.js");
module.exports = function (it) {
  return IObject(defined(it));
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_to-length.js":
/*!************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_to-length.js ***!
  \************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// 7.1.15 ToLength
var toInteger = __webpack_require__(/*! ./_to-integer */ "./node_modules/core-js/library/modules/_to-integer.js");
var min = Math.min;
module.exports = function (it) {
  return it > 0 ? min(toInteger(it), 0x1fffffffffffff) : 0; // pow(2, 53) - 1 == 9007199254740991
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_to-primitive.js":
/*!***************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_to-primitive.js ***!
  \***************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// 7.1.1 ToPrimitive(input [, PreferredType])
var isObject = __webpack_require__(/*! ./_is-object */ "./node_modules/core-js/library/modules/_is-object.js");
// instead of the ES6 spec version, we didn't implement @@toPrimitive case
// and the second argument - flag - preferred type is a string
module.exports = function (it, S) {
  if (!isObject(it)) return it;
  var fn, val;
  if (S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;
  if (typeof (fn = it.valueOf) == 'function' && !isObject(val = fn.call(it))) return val;
  if (!S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;
  throw TypeError("Can't convert object to primitive value");
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_uid.js":
/*!******************************************************!*\
  !*** ./node_modules/core-js/library/modules/_uid.js ***!
  \******************************************************/
/***/ ((module) => {

var id = 0;
var px = Math.random();
module.exports = function (key) {
  return 'Symbol('.concat(key === undefined ? '' : key, ')_', (++id + px).toString(36));
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/es6.object.create.js":
/*!*******************************************************************!*\
  !*** ./node_modules/core-js/library/modules/es6.object.create.js ***!
  \*******************************************************************/
/***/ ((__unused_webpack_module, __unused_webpack_exports, __webpack_require__) => {

var $export = __webpack_require__(/*! ./_export */ "./node_modules/core-js/library/modules/_export.js");
// 19.1.2.2 / 15.2.3.5 Object.create(O [, Properties])
$export($export.S, 'Object', { create: __webpack_require__(/*! ./_object-create */ "./node_modules/core-js/library/modules/_object-create.js") });


/***/ }),

/***/ "./node_modules/core-js/library/modules/es6.object.define-property.js":
/*!****************************************************************************!*\
  !*** ./node_modules/core-js/library/modules/es6.object.define-property.js ***!
  \****************************************************************************/
/***/ ((__unused_webpack_module, __unused_webpack_exports, __webpack_require__) => {

var $export = __webpack_require__(/*! ./_export */ "./node_modules/core-js/library/modules/_export.js");
// 19.1.2.4 / 15.2.3.6 Object.defineProperty(O, P, Attributes)
$export($export.S + $export.F * !__webpack_require__(/*! ./_descriptors */ "./node_modules/core-js/library/modules/_descriptors.js"), 'Object', { defineProperty: __webpack_require__(/*! ./_object-dp */ "./node_modules/core-js/library/modules/_object-dp.js").f });


/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be in strict mode.
(() => {
"use strict";
/*!**********************************************!*\
  !*** ./js/pages/product/combination/edit.js ***!
  \**********************************************/


var _productSuppliersManager = __webpack_require__(/*! @pages/product/edit/product-suppliers-manager */ "./js/pages/product/edit/product-suppliers-manager.js");

var _productSuppliersManager2 = _interopRequireDefault(_productSuppliersManager);

var _imageSelector = __webpack_require__(/*! @pages/product/combination/image-selector */ "./js/pages/product/combination/image-selector.js");

var _imageSelector2 = _interopRequireDefault(_imageSelector);

var _productMap = __webpack_require__(/*! @pages/product/product-map */ "./js/pages/product/product-map.js");

var _productMap2 = _interopRequireDefault(_productMap);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _window = window,
    $ = _window.$; /**
                    * Copyright since 2007 PrestaShop SA and Contributors
                    * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
                    *
                    * NOTICE OF LICENSE
                    *
                    * This source file is subject to the Open Software License (OSL 3.0)
                    * that is bundled with this package in the file LICENSE.md.
                    * It is also available through the world-wide-web at this URL:
                    * https://opensource.org/licenses/OSL-3.0
                    * If you did not receive a copy of the license and are unable to
                    * obtain it through the world-wide-web, please send an email
                    * to license@prestashop.com so we can send you a copy immediately.
                    *
                    * DISCLAIMER
                    *
                    * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
                    * versions in the future. If you wish to customize PrestaShop for your
                    * needs please refer to https://devdocs.prestashop.com/ for more information.
                    *
                    * @author    PrestaShop SA and Contributors <contact@prestashop.com>
                    * @copyright Since 2007 PrestaShop SA and Contributors
                    * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
                    */

$(function () {
  window.prestashop.component.initComponents(['TranslatableField', 'TinyMCEEditor', 'TranslatableInput', 'EventEmitter', 'TextWithLengthCounter']);

  new _productSuppliersManager2.default(_productMap2.default.suppliers.combinationSuppliers, false);
  new _imageSelector2.default();
});
})();

window.combination_edit = __webpack_exports__;
/******/ })()
;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9bbmFtZV0vLi9qcy9wYWdlcy9wcm9kdWN0L2NvbWJpbmF0aW9uL2ltYWdlLXNlbGVjdG9yLmpzIiwid2VicGFjazovL1tuYW1lXS8uL2pzL3BhZ2VzL3Byb2R1Y3QvZWRpdC9wcm9kdWN0LXN1cHBsaWVycy1tYW5hZ2VyLmpzIiwid2VicGFjazovL1tuYW1lXS8uL2pzL3BhZ2VzL3Byb2R1Y3QvcHJvZHVjdC1tYXAuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vanMvcGFnZXMvcHJvZHVjdC9zdXBwbGllcnMtbWFwLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9iYWJlbC1ydW50aW1lL2NvcmUtanMvb2JqZWN0L2NyZWF0ZS5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvYmFiZWwtcnVudGltZS9jb3JlLWpzL29iamVjdC9kZWZpbmUtcHJvcGVydHkuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jbGFzc0NhbGxDaGVjay5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvYmFiZWwtcnVudGltZS9oZWxwZXJzL2NyZWF0ZUNsYXNzLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvZm4vb2JqZWN0L2NyZWF0ZS5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L2ZuL29iamVjdC9kZWZpbmUtcHJvcGVydHkuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19hLWZ1bmN0aW9uLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fYW4tb2JqZWN0LmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fYXJyYXktaW5jbHVkZXMuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19jb2YuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19jb3JlLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fY3R4LmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fZGVmaW5lZC5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX2Rlc2NyaXB0b3JzLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fZG9tLWNyZWF0ZS5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX2VudW0tYnVnLWtleXMuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19leHBvcnQuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19mYWlscy5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX2dsb2JhbC5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX2hhcy5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX2hpZGUuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19odG1sLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9faWU4LWRvbS1kZWZpbmUuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19pb2JqZWN0LmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9faXMtb2JqZWN0LmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fbGlicmFyeS5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX29iamVjdC1jcmVhdGUuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19vYmplY3QtZHAuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19vYmplY3QtZHBzLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fb2JqZWN0LWtleXMtaW50ZXJuYWwuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19vYmplY3Qta2V5cy5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX3Byb3BlcnR5LWRlc2MuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19zaGFyZWQta2V5LmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fc2hhcmVkLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fdG8tYWJzb2x1dGUtaW5kZXguanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL190by1pbnRlZ2VyLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fdG8taW9iamVjdC5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX3RvLWxlbmd0aC5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX3RvLXByaW1pdGl2ZS5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX3VpZC5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvZXM2Lm9iamVjdC5jcmVhdGUuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL2VzNi5vYmplY3QuZGVmaW5lLXByb3BlcnR5LmpzIiwid2VicGFjazovL1tuYW1lXS93ZWJwYWNrL2Jvb3RzdHJhcCIsIndlYnBhY2s6Ly9bbmFtZV0vLi9qcy9wYWdlcy9wcm9kdWN0L2NvbWJpbmF0aW9uL2VkaXQuanMiXSwibmFtZXMiOlsid2luZG93IiwiJCIsIkltYWdlU2VsZWN0b3IiLCIkc2VsZWN0b3JDb250YWluZXIiLCJQcm9kdWN0TWFwIiwiY29tYmluYXRpb25zIiwiaW1hZ2VzIiwic2VsZWN0b3JDb250YWluZXIiLCJpbml0IiwiY2hlY2tib3hDb250YWluZXIiLCJoaWRlIiwib24iLCJpbWFnZUNob2ljZSIsImV2ZW50IiwiJGltYWdlQ2hvaWNlIiwiY3VycmVudFRhcmdldCIsIiRjaGVja2JveCIsImNoZWNrYm94IiwiaXNDaGVja2VkIiwicHJvcCIsInRvZ2dsZUNsYXNzIiwiUHJvZHVjdFN1cHBsaWVyc01hbmFnZXIiLCJzdXBwbGllcnNGb3JtSWQiLCJmb3JjZVVwZGF0ZURlZmF1bHQiLCJzdXBwbGllcnNNYXAiLCIkcHJvZHVjdFN1cHBsaWVyc0NvbGxlY3Rpb24iLCJwcm9kdWN0U3VwcGxpZXJzQ29sbGVjdGlvbiIsIiRzdXBwbGllcklkc0dyb3VwIiwic3VwcGxpZXJJZHNJbnB1dCIsImNsb3Nlc3QiLCIkZGVmYXVsdFN1cHBsaWVyR3JvdXAiLCJkZWZhdWx0U3VwcGxpZXJJbnB1dCIsIiRwcm9kdWN0c1RhYmxlIiwicHJvZHVjdFN1cHBsaWVyc1RhYmxlIiwiJHByb2R1Y3RzVGFibGVCb2R5IiwicHJvZHVjdHNTdXBwbGllcnNUYWJsZUJvZHkiLCJzdXBwbGllcnMiLCJwcm90b3R5cGVUZW1wbGF0ZSIsImRhdGEiLCJwcm90b3R5cGVOYW1lIiwiZGVmYXVsdERhdGFGb3JTdXBwbGllciIsImdldERlZmF1bHREYXRhRm9yU3VwcGxpZXIiLCJtZW1vcml6ZUN1cnJlbnRTdXBwbGllcnMiLCJ0b2dnbGVUYWJsZVZpc2liaWxpdHkiLCJyZWZyZXNoRGVmYXVsdFN1cHBsaWVyQmxvY2siLCIkaW5pdGlhbERlZmF1bHQiLCJmaW5kIiwiZmlyc3QiLCJsZW5ndGgiLCJhZGRDbGFzcyIsImRlZmF1bHRTdXBwbGllckNsYXNzIiwiZSIsImlucHV0IiwiY2hlY2tlZCIsImFkZFN1cHBsaWVyIiwic3VwcGxpZXJJZCIsInZhbHVlIiwic3VwcGxpZXJOYW1lIiwiZGF0YXNldCIsImxhYmVsIiwicmVtb3ZlU3VwcGxpZXIiLCJyZW5kZXJTdXBwbGllcnMiLCJnZXRTZWxlY3RlZFN1cHBsaWVycyIsImhpZGVUYWJsZSIsInNob3dUYWJsZSIsInN1cHBsaWVyIiwibmV3U3VwcGxpZXIiLCJyZW1vdmVkIiwiZW1wdHkiLCJmb3JFYWNoIiwic2VsZWN0ZWRTdXBwbGllciIsInByb2R1Y3RTdXBwbGllclJvdyIsInJlcGxhY2UiLCJSZWdFeHAiLCJhcHBlbmQiLCJyb3dNYXAiLCJzdXBwbGllcklkSW5wdXQiLCJ2YWwiLCJzdXBwbGllck5hbWVQcmV2aWV3IiwiaHRtbCIsInN1cHBsaWVyTmFtZUlucHV0IiwicHJvZHVjdFN1cHBsaWVySWRJbnB1dCIsInByb2R1Y3RTdXBwbGllcklkIiwicmVmZXJlbmNlSW5wdXQiLCJyZWZlcmVuY2UiLCJwcmljZUlucHV0IiwicHJpY2UiLCJjdXJyZW5jeUlkSW5wdXQiLCJjdXJyZW5jeUlkIiwic2VsZWN0ZWRTdXBwbGllcnMiLCJlYWNoIiwiaW5kZXgiLCJwdXNoIiwiaGlkZURlZmF1bHRTdXBwbGllcnMiLCJzaG93RGVmYXVsdFN1cHBsaWVycyIsInNlbGVjdGVkU3VwcGxpZXJJZHMiLCJtYXAiLCJrZXkiLCJpc1ZhbGlkIiwiaW5jbHVkZXMiLCJkaXNhYmxlZCIsImNoZWNrRmlyc3RBdmFpbGFibGVEZWZhdWx0U3VwcGxpZXIiLCJyZW1vdmVDbGFzcyIsImZpcnN0U3VwcGxpZXJJZCIsInJvd1Byb3RvdHlwZSIsIkRPTVBhcnNlciIsInBhcnNlRnJvbVN0cmluZyIsImdldERhdGFGcm9tUm93Iiwic2VsZWN0b3JHZW5lcmF0b3IiLCJxdWVyeVNlbGVjdG9yIiwiY29tYmluYXRpb25MaXN0SWQiLCJwcm9kdWN0Rm9ybSIsInByb2R1Y3RUeXBlU2VsZWN0b3IiLCJwcm9kdWN0VHlwZSIsIlNUQU5EQVJEIiwiUEFDSyIsIlZJUlRVQUwiLCJDT01CSU5BVElPTlMiLCJpbnZhbGlkRmllbGQiLCJwcm9kdWN0Rm9ybVN1Ym1pdEJ1dHRvbiIsIm5hdmlnYXRpb25CYXIiLCJkcm9wem9uZUltYWdlc0NvbnRhaW5lciIsImZlYXR1cmVWYWx1ZXMiLCJjb2xsZWN0aW9uQ29udGFpbmVyIiwiY29sbGVjdGlvblJvd3NDb250YWluZXIiLCJjb2xsZWN0aW9uUm93IiwiZmVhdHVyZVNlbGVjdCIsImZlYXR1cmVWYWx1ZVNlbGVjdCIsImN1c3RvbVZhbHVlSW5wdXQiLCJjdXN0b21GZWF0dXJlSWRJbnB1dCIsImRlbGV0ZUZlYXR1cmVWYWx1ZSIsImFkZEZlYXR1cmVWYWx1ZSIsImN1c3RvbWl6YXRpb25zIiwiY3VzdG9taXphdGlvbnNDb250YWluZXIiLCJjdXN0b21pemF0aW9uRmllbGRzTGlzdCIsImFkZEN1c3RvbWl6YXRpb25CdG4iLCJyZW1vdmVDdXN0b21pemF0aW9uQnRuIiwiY3VzdG9taXphdGlvbkZpZWxkUm93IiwibmF2aWdhdGlvblRhYiIsImV4dGVybmFsQ29tYmluYXRpb25UYWIiLCJwcmVsb2FkZXIiLCJlbXB0eVN0YXRlIiwiY29tYmluYXRpb25zUGFnaW5hdGVkTGlzdCIsImNvbWJpbmF0aW9uc0NvbnRhaW5lciIsImNvbWJpbmF0aW9uc0ZpbHRlcnNDb250YWluZXIiLCJjb21iaW5hdGlvbnNHZW5lcmF0b3JDb250YWluZXIiLCJjb21iaW5hdGlvbnNUYWJsZSIsImNvbWJpbmF0aW9uc1RhYmxlQm9keSIsImNvbWJpbmF0aW9uSWRJbnB1dHNTZWxlY3RvciIsImlzRGVmYXVsdElucHV0c1NlbGVjdG9yIiwicmVtb3ZlQ29tYmluYXRpb25TZWxlY3RvciIsImNvbWJpbmF0aW9uTmFtZSIsInBhZ2luYXRpb25Db250YWluZXIiLCJsb2FkaW5nU3Bpbm5lciIsInF1YW50aXR5SW5wdXRXcmFwcGVyIiwiaW1wYWN0T25QcmljZUlucHV0V3JhcHBlciIsInJlZmVyZW5jZUlucHV0V3JhcHBlciIsInNvcnRhYmxlQ29sdW1ucyIsImNvbWJpbmF0aW9uSXRlbUZvcm0iLCJxdWFudGl0eUtleSIsImltcGFjdE9uUHJpY2VLZXkiLCJyZWZlcmVuY2VLZXkiLCJ0b2tlbktleSIsImVkaXRpb25Gb3JtIiwiZWRpdGlvbkZvcm1JbnB1dHMiLCJlZGl0Q29tYmluYXRpb25CdXR0b25zIiwidGFibGVSb3ciLCJjb21iaW5hdGlvbkltZyIsImNvbWJpbmF0aW9uQ2hlY2tib3giLCJyb3dJbmRleCIsImNvbWJpbmF0aW9uSWRJbnB1dCIsImNvbWJpbmF0aW9uTmFtZUlucHV0IiwiaW1wYWN0T25QcmljZUlucHV0IiwiZmluYWxQcmljZVRlSW5wdXQiLCJxdWFudGl0eUlucHV0IiwiaXNEZWZhdWx0SW5wdXQiLCJlZGl0QnV0dG9uIiwiZGVsZXRlQnV0dG9uIiwiZWRpdE1vZGFsIiwic2Nyb2xsQmFyIiwic2VhcmNoSW5wdXQiLCJnZW5lcmF0ZUNvbWJpbmF0aW9uc0J1dHRvbiIsInZpcnR1YWxQcm9kdWN0IiwiY29udGFpbmVyIiwiZmlsZUNvbnRlbnRDb250YWluZXIiLCJkcm9wem9uZSIsImNvbmZpZ3VyYXRpb24iLCJmaWxlTWFuYWdlciIsInBob3Rvc3dpcGUiLCJlbGVtZW50IiwiZHpUZW1wbGF0ZSIsImR6UHJldmlldyIsInNvcnRhYmxlQ29udGFpbmVyIiwic29ydGFibGVJdGVtcyIsImRyb3B6b25lQ29udGFpbmVyIiwic2hvd25Ub29sdGlwcyIsInNhdmVkSW1hZ2VDb250YWluZXIiLCJpbWFnZUlkIiwic2F2ZWRJbWFnZSIsImNvdmVyZWRQcmV2aWV3Iiwid2luZG93RmlsZU1hbmFnZXIiLCJwcm9kdWN0U3VwcGxpZXJzIiwiY29tYmluYXRpb25TdXBwbGllcnMiLCJzZW8iLCJkZWZhdWx0VGl0bGUiLCJ3YXRjaGVkVGl0bGUiLCJkZWZhdWx0RGVzY3JpcHRpb24iLCJ3YXRjaGVkRGVzY3JpcHRpb24iLCJ3YXRjaGVkTWV0YVVybCIsInJlZGlyZWN0T3B0aW9uIiwidHlwZUlucHV0IiwidGFyZ2V0SW5wdXQiLCJqc1RhYnMiLCJqc0Fycm93IiwianNOYXZUYWJzIiwidG9nZ2xlVGFiIiwiZm9ybUNvbnRlbnRUYWIiLCJsZWZ0QXJyb3ciLCJyaWdodEFycm93IiwiZm9vdGVyIiwicHJldmlld1VybEJ1dHRvbiIsImRlbGV0ZVByb2R1Y3RCdXR0b24iLCJjYXRlZ29yaWVzIiwiY2F0ZWdvcmllc0NvbnRhaW5lciIsImNhdGVnb3J5VHJlZSIsInRyZWVFbGVtZW50IiwidHJlZUVsZW1lbnRJbnB1dHMiLCJjaGVja2JveElucHV0IiwiY2hlY2tlZENoZWNrYm94SW5wdXRzIiwiY2hlY2tib3hOYW1lIiwiY2F0ZWdvcnlJZCIsIm1hdGVyaWFsQ2hlY2tib3giLCJyYWRpb0lucHV0IiwiZGVmYXVsdFJhZGlvSW5wdXQiLCJyYWRpb05hbWUiLCJ0YWdzQ29udGFpbmVyIiwiZmllbGRzZXQiLCJsb2FkZXIiLCJjaGlsZHJlbkxpc3QiLCJldmVyeUl0ZW1zIiwiZXhwYW5kQWxsQnV0dG9uIiwicmVkdWNlQWxsQnV0dG9uIiwibW9kdWxlcyIsInByZXZpZXdDb250YWluZXIiLCJwcmV2aWV3QnV0dG9uIiwibW9kdWxlU2VsZWN0b3IiLCJzZWxlY3RvclByZXZpZXdzIiwic2VsZWN0b3JQcmV2aWV3IiwibW9kdWxlSWQiLCJjb250ZW50Q29udGFpbmVyIiwibW9kdWxlQ29udGVudHMiLCJtb2R1bGVDb250ZW50IiwicHJvZHVjdFN1cHBsaWVyc0lkIiwicHJvZHVjdFN1cHBsaWVySW5wdXRJZCIsInN1cHBsaWVySW5kZXgiLCJpbnB1dE5hbWUiLCJwcmVzdGFzaG9wIiwiY29tcG9uZW50IiwiaW5pdENvbXBvbmVudHMiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQXlCQTs7Ozs7O2NBRVlBLE07SUFBTEMsQyxXQUFBQSxDLEVBM0JQOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0lBNkJxQkMsYTtBQUNuQiwyQkFBYztBQUFBOztBQUNaLFNBQUtDLGtCQUFMLEdBQTBCRixFQUFFRyxxQkFBV0MsWUFBWCxDQUF3QkMsTUFBeEIsQ0FBK0JDLGlCQUFqQyxDQUExQjtBQUNBLFNBQUtDLElBQUw7QUFDRDs7OzsyQkFFTTtBQUNMUCxRQUFFRyxxQkFBV0MsWUFBWCxDQUF3QkMsTUFBeEIsQ0FBK0JHLGlCQUFqQyxFQUFvRCxLQUFLTixrQkFBekQsRUFBNkVPLElBQTdFO0FBQ0EsV0FBS1Asa0JBQUwsQ0FBd0JRLEVBQXhCLENBQTJCLE9BQTNCLEVBQW9DUCxxQkFBV0MsWUFBWCxDQUF3QkMsTUFBeEIsQ0FBK0JNLFdBQW5FLEVBQWdGLFVBQUNDLEtBQUQsRUFBVztBQUN6RixZQUFNQyxlQUFlYixFQUFFWSxNQUFNRSxhQUFSLENBQXJCO0FBQ0EsWUFBTUMsWUFBWWYsRUFBRUcscUJBQVdDLFlBQVgsQ0FBd0JDLE1BQXhCLENBQStCVyxRQUFqQyxFQUEyQ0gsWUFBM0MsQ0FBbEI7O0FBRUEsWUFBTUksWUFBWUYsVUFBVUcsSUFBVixDQUFlLFNBQWYsQ0FBbEI7QUFDQUwscUJBQWFNLFdBQWIsQ0FBeUIsVUFBekIsRUFBcUMsQ0FBQ0YsU0FBdEM7QUFDQUYsa0JBQVVHLElBQVYsQ0FBZSxTQUFmLEVBQTBCLENBQUNELFNBQTNCO0FBQ0QsT0FQRDtBQVFEOzs7OztrQkFoQmtCaEIsYTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNKckI7Ozs7OztjQUVZRixNO0lBQUxDLEMsV0FBQUEsQyxFQTNCUDs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztJQTZCcUJvQix1QjtBQUNuQjs7Ozs7OztBQU9BLG1DQUFZQyxlQUFaLEVBQTZCQyxrQkFBN0IsRUFBaUQ7QUFBQTs7QUFDL0MsU0FBS0Esa0JBQUwsR0FBMEJBLGtCQUExQjtBQUNBLFNBQUtDLFlBQUwsR0FBb0IsNEJBQWFGLGVBQWIsQ0FBcEI7QUFDQSxTQUFLRywyQkFBTCxHQUFtQ3hCLEVBQUUsS0FBS3VCLFlBQUwsQ0FBa0JFLDBCQUFwQixDQUFuQztBQUNBLFNBQUtDLGlCQUFMLEdBQXlCMUIsRUFBRSxLQUFLdUIsWUFBTCxDQUFrQkksZ0JBQXBCLEVBQXNDQyxPQUF0QyxDQUE4QyxhQUE5QyxDQUF6QjtBQUNBLFNBQUtDLHFCQUFMLEdBQTZCN0IsRUFBRSxLQUFLdUIsWUFBTCxDQUFrQk8sb0JBQXBCLEVBQTBDRixPQUExQyxDQUFrRCxhQUFsRCxDQUE3QjtBQUNBLFNBQUtHLGNBQUwsR0FBc0IvQixFQUFFLEtBQUt1QixZQUFMLENBQWtCUyxxQkFBcEIsQ0FBdEI7QUFDQSxTQUFLQyxrQkFBTCxHQUEwQmpDLEVBQUUsS0FBS3VCLFlBQUwsQ0FBa0JXLDBCQUFwQixDQUExQjs7QUFFQSxTQUFLQyxTQUFMLEdBQWlCLEVBQWpCO0FBQ0EsU0FBS0MsaUJBQUwsR0FBeUIsS0FBS1osMkJBQUwsQ0FBaUNhLElBQWpDLENBQXNDLFdBQXRDLENBQXpCO0FBQ0EsU0FBS0MsYUFBTCxHQUFxQixLQUFLZCwyQkFBTCxDQUFpQ2EsSUFBakMsQ0FBc0MsZUFBdEMsQ0FBckI7QUFDQSxTQUFLRSxzQkFBTCxHQUE4QixLQUFLQyx5QkFBTCxFQUE5Qjs7QUFFQSxTQUFLakMsSUFBTDs7QUFFQSxXQUFPLEVBQVA7QUFDRDs7OzsyQkFFTTtBQUFBOztBQUNMLFdBQUtrQyx3QkFBTDtBQUNBLFdBQUtDLHFCQUFMO0FBQ0EsV0FBS0MsMkJBQUw7O0FBRUEsV0FBS0MsZUFBTCxHQUF1QixLQUFLZixxQkFBTCxDQUEyQmdCLElBQTNCLENBQWdDLGVBQWhDLEVBQWlEQyxLQUFqRCxFQUF2QjtBQUNBLFVBQUksS0FBS0YsZUFBTCxDQUFxQkcsTUFBekIsRUFBaUM7QUFDL0IsYUFBS0gsZUFBTCxDQUNHaEIsT0FESCxDQUNXLEtBQUtMLFlBQUwsQ0FBa0JmLGlCQUQ3QixFQUVHd0MsUUFGSCxDQUVZLEtBQUt6QixZQUFMLENBQWtCMEIsb0JBRjlCO0FBR0Q7O0FBRUQsV0FBS2xCLGNBQUwsQ0FBb0JyQixFQUFwQixDQUF1QixRQUF2QixFQUFpQyxPQUFqQyxFQUEwQyxZQUFNO0FBQzlDLGNBQUsrQix3QkFBTDtBQUNELE9BRkQ7O0FBSUEsV0FBS2YsaUJBQUwsQ0FBdUJoQixFQUF2QixDQUEwQixRQUExQixFQUFvQyxPQUFwQyxFQUE2QyxVQUFDd0MsQ0FBRCxFQUFPO0FBQ2xELFlBQU1DLFFBQVFELEVBQUVwQyxhQUFoQjs7QUFFQSxZQUFJcUMsTUFBTUMsT0FBVixFQUFtQjtBQUNqQixnQkFBS0MsV0FBTCxDQUFpQjtBQUNmQyx3QkFBWUgsTUFBTUksS0FESDtBQUVmQywwQkFBY0wsTUFBTU0sT0FBTixDQUFjQztBQUZiLFdBQWpCO0FBSUQsU0FMRCxNQUtPO0FBQ0wsZ0JBQUtDLGNBQUwsQ0FBb0JSLE1BQU1JLEtBQTFCO0FBQ0Q7O0FBRUQsY0FBS0ssZUFBTDtBQUNBLGNBQUtsQixxQkFBTDtBQUNBLGNBQUtDLDJCQUFMO0FBQ0QsT0FmRDtBQWdCRDs7OzRDQUV1QjtBQUN0QixVQUFJLEtBQUtrQixvQkFBTCxHQUE0QmQsTUFBNUIsS0FBdUMsQ0FBM0MsRUFBOEM7QUFDNUMsYUFBS2UsU0FBTDs7QUFFQTtBQUNEOztBQUVELFdBQUtDLFNBQUw7QUFDRDs7QUFFRDs7Ozs7O2dDQUdZQyxRLEVBQVU7QUFDcEIsVUFBSSxPQUFPLEtBQUs3QixTQUFMLENBQWU2QixTQUFTVixVQUF4QixDQUFQLEtBQStDLFdBQW5ELEVBQWdFO0FBQzlELFlBQU1XLGNBQWMsc0JBQWMsS0FBSzFCLHNCQUFuQixDQUFwQjtBQUNBMEIsb0JBQVlYLFVBQVosR0FBeUJVLFNBQVNWLFVBQWxDO0FBQ0FXLG9CQUFZVCxZQUFaLEdBQTJCUSxTQUFTUixZQUFwQzs7QUFFQSxhQUFLckIsU0FBTCxDQUFlNkIsU0FBU1YsVUFBeEIsSUFBc0NXLFdBQXRDO0FBQ0QsT0FORCxNQU1PO0FBQ0wsYUFBSzlCLFNBQUwsQ0FBZTZCLFNBQVNWLFVBQXhCLEVBQW9DWSxPQUFwQyxHQUE4QyxLQUE5QztBQUNEO0FBQ0Y7O0FBRUQ7Ozs7OzttQ0FHZVosVSxFQUFZO0FBQ3pCLFdBQUtuQixTQUFMLENBQWVtQixVQUFmLEVBQTJCWSxPQUEzQixHQUFxQyxJQUFyQztBQUNEOzs7c0NBRWlCO0FBQUE7O0FBQ2hCLFdBQUtqQyxrQkFBTCxDQUF3QmtDLEtBQXhCOztBQUVBO0FBQ0EsV0FBS04sb0JBQUwsR0FBNEJPLE9BQTVCLENBQW9DLFVBQUNDLGdCQUFELEVBQXNCO0FBQ3hELFlBQU1MLFdBQVcsT0FBSzdCLFNBQUwsQ0FBZWtDLGlCQUFpQmYsVUFBaEMsQ0FBakI7O0FBRUEsWUFBSVUsU0FBU0UsT0FBYixFQUFzQjtBQUNwQjtBQUNEOztBQUVELFlBQU1JLHFCQUFxQixPQUFLbEMsaUJBQUwsQ0FBdUJtQyxPQUF2QixDQUN6QixJQUFJQyxNQUFKLENBQVcsT0FBS2xDLGFBQWhCLEVBQStCLEdBQS9CLENBRHlCLEVBRXpCMEIsU0FBU1YsVUFGZ0IsQ0FBM0I7O0FBS0EsZUFBS3JCLGtCQUFMLENBQXdCd0MsTUFBeEIsQ0FBK0JILGtCQUEvQjtBQUNBO0FBQ0EsWUFBTUksU0FBUyxPQUFLbkQsWUFBTCxDQUFrQitDLGtCQUFqQztBQUNBdEUsVUFBRTBFLE9BQU9DLGVBQVAsQ0FBdUJYLFNBQVNWLFVBQWhDLENBQUYsRUFBK0NzQixHQUEvQyxDQUFtRFosU0FBU1YsVUFBNUQ7QUFDQXRELFVBQUUwRSxPQUFPRyxtQkFBUCxDQUEyQmIsU0FBU1YsVUFBcEMsQ0FBRixFQUFtRHdCLElBQW5ELENBQXdEZCxTQUFTUixZQUFqRTtBQUNBeEQsVUFBRTBFLE9BQU9LLGlCQUFQLENBQXlCZixTQUFTVixVQUFsQyxDQUFGLEVBQWlEc0IsR0FBakQsQ0FBcURaLFNBQVNSLFlBQTlEO0FBQ0F4RCxVQUFFMEUsT0FBT00sc0JBQVAsQ0FBOEJoQixTQUFTVixVQUF2QyxDQUFGLEVBQXNEc0IsR0FBdEQsQ0FBMERaLFNBQVNpQixpQkFBbkU7QUFDQWpGLFVBQUUwRSxPQUFPUSxjQUFQLENBQXNCbEIsU0FBU1YsVUFBL0IsQ0FBRixFQUE4Q3NCLEdBQTlDLENBQWtEWixTQUFTbUIsU0FBM0Q7QUFDQW5GLFVBQUUwRSxPQUFPVSxVQUFQLENBQWtCcEIsU0FBU1YsVUFBM0IsQ0FBRixFQUEwQ3NCLEdBQTFDLENBQThDWixTQUFTcUIsS0FBdkQ7QUFDQXJGLFVBQUUwRSxPQUFPWSxlQUFQLENBQXVCdEIsU0FBU1YsVUFBaEMsQ0FBRixFQUErQ3NCLEdBQS9DLENBQW1EWixTQUFTdUIsVUFBNUQ7QUFDRCxPQXRCRDtBQXVCRDs7OzJDQUVzQjtBQUNyQixVQUFNQyxvQkFBb0IsRUFBMUI7QUFDQSxXQUFLOUQsaUJBQUwsQ0FBdUJtQixJQUF2QixDQUE0QixlQUE1QixFQUE2QzRDLElBQTdDLENBQWtELFVBQUNDLEtBQUQsRUFBUXZDLEtBQVIsRUFBa0I7QUFDbEVxQywwQkFBa0JHLElBQWxCLENBQXVCO0FBQ3JCbkMsd0JBQWNMLE1BQU1NLE9BQU4sQ0FBY0MsS0FEUDtBQUVyQkosc0JBQVlILE1BQU1JO0FBRkcsU0FBdkI7QUFJRCxPQUxEOztBQU9BLGFBQU9pQyxpQkFBUDtBQUNEOzs7a0RBRTZCO0FBQUE7O0FBQzVCLFVBQU1yRCxZQUFZLEtBQUswQixvQkFBTCxFQUFsQjs7QUFFQSxVQUFJMUIsVUFBVVksTUFBVixLQUFxQixDQUF6QixFQUE0QjtBQUMxQixZQUFJLEtBQUt6QixrQkFBVCxFQUE2QjtBQUMzQixlQUFLTyxxQkFBTCxDQUEyQmdCLElBQTNCLENBQWdDLE9BQWhDLEVBQXlDM0IsSUFBekMsQ0FBOEMsU0FBOUMsRUFBeUQsS0FBekQ7QUFDRDtBQUNELGFBQUswRSxvQkFBTDs7QUFFQTtBQUNEOztBQUVELFdBQUtDLG9CQUFMO0FBQ0EsVUFBTUMsc0JBQXNCM0QsVUFBVTRELEdBQVYsQ0FBYyxVQUFDL0IsUUFBRDtBQUFBLGVBQWNBLFNBQVNWLFVBQXZCO0FBQUEsT0FBZCxDQUE1Qjs7QUFFQSxXQUFLekIscUJBQUwsQ0FBMkJnQixJQUEzQixDQUFnQyxPQUFoQyxFQUF5QzRDLElBQXpDLENBQThDLFVBQUNPLEdBQUQsRUFBTTdDLEtBQU4sRUFBZ0I7QUFDNUQsWUFBTThDLFVBQVVILG9CQUFvQkksUUFBcEIsQ0FBNkIvQyxNQUFNSSxLQUFuQyxDQUFoQjs7QUFFQSxZQUFJLE9BQUtqQyxrQkFBTCxJQUEyQixDQUFDMkUsT0FBaEMsRUFBeUM7QUFDdkM5QyxnQkFBTUMsT0FBTixHQUFnQixLQUFoQjtBQUNEO0FBQ0RELGNBQU1nRCxRQUFOLEdBQWlCLENBQUNGLE9BQWxCO0FBQ0QsT0FQRDs7QUFTQSxVQUFJLEtBQUtwRSxxQkFBTCxDQUEyQmdCLElBQTNCLENBQWdDLGVBQWhDLEVBQWlERSxNQUFqRCxLQUE0RCxDQUE1RCxJQUFpRSxLQUFLekIsa0JBQTFFLEVBQThGO0FBQzVGLGFBQUs4RSxrQ0FBTCxDQUF3Q04sbUJBQXhDO0FBQ0Q7QUFDRjs7OzJDQUVzQjtBQUNyQixXQUFLakUscUJBQUwsQ0FBMkJtQixRQUEzQixDQUFvQyxRQUFwQztBQUNEOzs7MkNBRXNCO0FBQ3JCLFdBQUtuQixxQkFBTCxDQUEyQndFLFdBQTNCLENBQXVDLFFBQXZDO0FBQ0Q7O0FBRUQ7Ozs7Ozt1REFHbUNQLG1CLEVBQXFCO0FBQ3RELFVBQU1RLGtCQUFrQlIsb0JBQW9CLENBQXBCLENBQXhCO0FBQ0EsV0FBS2pFLHFCQUFMLENBQTJCZ0IsSUFBM0IsbUJBQWdEeUQsZUFBaEQsU0FBcUVwRixJQUFyRSxDQUEwRSxTQUExRSxFQUFxRixJQUFyRjtBQUNEOzs7Z0NBRVc7QUFDVixXQUFLYSxjQUFMLENBQW9Cc0UsV0FBcEIsQ0FBZ0MsUUFBaEM7QUFDRDs7O2dDQUVXO0FBQ1YsV0FBS3RFLGNBQUwsQ0FBb0JpQixRQUFwQixDQUE2QixRQUE3QjtBQUNEOztBQUVEOzs7Ozs7OytDQUkyQjtBQUFBOztBQUN6QixXQUFLYSxvQkFBTCxHQUE0Qk8sT0FBNUIsQ0FBb0MsVUFBQ0osUUFBRCxFQUFjO0FBQ2hELGVBQUs3QixTQUFMLENBQWU2QixTQUFTVixVQUF4QixJQUFzQztBQUNwQ0Esc0JBQVlVLFNBQVNWLFVBRGU7QUFFcEMyQiw2QkFBbUJqRixFQUFFLE9BQUt1QixZQUFMLENBQWtCK0Msa0JBQWxCLENBQXFDVSxzQkFBckMsQ0FBNERoQixTQUFTVixVQUFyRSxDQUFGLEVBQW9Gc0IsR0FBcEYsRUFGaUI7QUFHcENwQix3QkFBY3hELEVBQUUsT0FBS3VCLFlBQUwsQ0FBa0IrQyxrQkFBbEIsQ0FBcUNTLGlCQUFyQyxDQUF1RGYsU0FBU1YsVUFBaEUsQ0FBRixFQUErRXNCLEdBQS9FLEVBSHNCO0FBSXBDTyxxQkFBV25GLEVBQUUsT0FBS3VCLFlBQUwsQ0FBa0IrQyxrQkFBbEIsQ0FBcUNZLGNBQXJDLENBQW9EbEIsU0FBU1YsVUFBN0QsQ0FBRixFQUE0RXNCLEdBQTVFLEVBSnlCO0FBS3BDUyxpQkFBT3JGLEVBQUUsT0FBS3VCLFlBQUwsQ0FBa0IrQyxrQkFBbEIsQ0FBcUNjLFVBQXJDLENBQWdEcEIsU0FBU1YsVUFBekQsQ0FBRixFQUF3RXNCLEdBQXhFLEVBTDZCO0FBTXBDVyxzQkFBWXZGLEVBQUUsT0FBS3VCLFlBQUwsQ0FBa0IrQyxrQkFBbEIsQ0FBcUNnQixlQUFyQyxDQUFxRHRCLFNBQVNWLFVBQTlELENBQUYsRUFBNkVzQixHQUE3RSxFQU53QjtBQU9wQ1YsbUJBQVM7QUFQMkIsU0FBdEM7QUFTRCxPQVZEO0FBV0Q7O0FBRUQ7Ozs7Ozs7OztnREFNNEI7QUFDMUIsVUFBTXFDLGVBQWUsSUFBSUMsU0FBSixHQUFnQkMsZUFBaEIsQ0FDbkIsS0FBS3JFLGlCQURjLEVBRW5CLFdBRm1CLENBQXJCOztBQUtBLGFBQU87QUFDTDhCLGlCQUFTLEtBREo7QUFFTGUsMkJBQW1CLEtBQUt5QixjQUFMLENBQW9CLEtBQUtuRixZQUFMLENBQWtCK0Msa0JBQWxCLENBQXFDVSxzQkFBekQsRUFBaUZ1QixZQUFqRixDQUZkO0FBR0xwQixtQkFBVyxLQUFLdUIsY0FBTCxDQUFvQixLQUFLbkYsWUFBTCxDQUFrQitDLGtCQUFsQixDQUFxQ1ksY0FBekQsRUFBeUVxQixZQUF6RSxDQUhOO0FBSUxsQixlQUFPLEtBQUtxQixjQUFMLENBQW9CLEtBQUtuRixZQUFMLENBQWtCK0Msa0JBQWxCLENBQXFDYyxVQUF6RCxFQUFxRW1CLFlBQXJFLENBSkY7QUFLTGhCLG9CQUFZLEtBQUttQixjQUFMLENBQW9CLEtBQUtuRixZQUFMLENBQWtCK0Msa0JBQWxCLENBQXFDZ0IsZUFBekQsRUFBMEVpQixZQUExRTtBQUxQLE9BQVA7QUFPRDs7QUFFRDs7Ozs7Ozs7O21DQU1lSSxpQixFQUFtQkosWSxFQUFjO0FBQzlDLGFBQU9BLGFBQWFLLGFBQWIsQ0FBMkJELGtCQUFrQixLQUFLckUsYUFBdkIsQ0FBM0IsRUFBa0VpQixLQUF6RTtBQUNEOzs7OztrQkExT2tCbkMsdUI7Ozs7Ozs7Ozs7Ozs7Ozs7QUM3QnJCOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBeUJBLElBQU15RixvQkFBb0IsbUJBQTFCOztrQkFFZTtBQUNiQyxlQUFhLG9CQURBO0FBRWJDLHVCQUFxQixzQkFGUjtBQUdiQyxlQUFhO0FBQ1hDLGNBQVUsVUFEQztBQUVYQyxVQUFNLE1BRks7QUFHWEMsYUFBUyxTQUhFO0FBSVhDLGtCQUFjO0FBSkgsR0FIQTtBQVNiQyxnQkFBYyxhQVREO0FBVWJDLDJCQUF5QiwyQkFWWjtBQVdiQyxpQkFBZSxXQVhGO0FBWWJDLDJCQUF5Qix5QkFaWjtBQWFiQyxpQkFBZTtBQUNiQyx5QkFBcUIsNEJBRFI7QUFFYkMsNkJBQXlCLHNDQUZaO0FBR2JDLG1CQUFlLHlCQUhGO0FBSWJDLG1CQUFlLHlCQUpGO0FBS2JDLHdCQUFvQiwrQkFMUDtBQU1iQyxzQkFBa0Isc0JBTkw7QUFPYkMsMEJBQXNCLHVCQVBUO0FBUWJDLHdCQUFvQiw2QkFSUDtBQVNiQyxxQkFBaUI7QUFUSixHQWJGO0FBd0JiQyxrQkFBZ0I7QUFDZEMsNkJBQXlCLG9DQURYO0FBRWRDLDZCQUF5Qix1Q0FGWDtBQUdkQyx5QkFBcUIsd0JBSFA7QUFJZEMsNEJBQXdCLDJCQUpWO0FBS2RDLDJCQUF1QjtBQUxULEdBeEJIO0FBK0JicEksZ0JBQWM7QUFDWnFJLG1CQUFlLHVCQURIO0FBRVpDLDRCQUF3QiwyQkFGWjtBQUdaQyxlQUFXLHlCQUhDO0FBSVpDLGdCQUFZLDJCQUpBO0FBS1pDLCtCQUEyQiw4QkFMZjtBQU1aQyxnQ0FBMEJqQyxpQkFOZDtBQU9aa0Msa0NBQThCLHVCQVBsQjtBQVFaQyxvQ0FBZ0MsaUNBUnBCO0FBU1pDLHVCQUFzQnBDLGlCQUF0QixXQVRZO0FBVVpxQywyQkFBMEJyQyxpQkFBMUIsaUJBVlk7QUFXWnNDLGlDQUE2Qix1QkFYakI7QUFZWkMsNkJBQXlCLCtCQVpiO0FBYVpDLCtCQUEyQiwwQkFiZjtBQWNaQyxxQkFBaUIsd0JBZEw7QUFlWkMseUJBQXFCLDBCQWZUO0FBZ0JaQyxvQkFBZ0IsNkJBaEJKO0FBaUJaQywwQkFBc0IsdUJBakJWO0FBa0JaQywrQkFBMkIsOEJBbEJmO0FBbUJaQywyQkFBdUIsd0JBbkJYO0FBb0JaQyxxQkFBaUIscUJBcEJMO0FBcUJaQyx5QkFBcUI7QUFDbkJDLG1CQUFhLG1DQURNO0FBRW5CQyx3QkFBa0IsMENBRkM7QUFHbkJDLG9CQUFjLG9DQUhLO0FBSW5CQyxnQkFBVTtBQUpTLEtBckJUO0FBMkJaQyxpQkFBYSwrQkEzQkQ7QUE0QlpDO0FBQ0U7QUFDQSx1SEE5QlU7QUErQlpDLDRCQUF3Qix3QkEvQlo7QUFnQ1pDLGNBQVU7QUFDUkMsc0JBQWdCLG9CQURSO0FBRVJDLDJCQUFxQiw2QkFBQ0MsUUFBRDtBQUFBLGVBQWlCM0QsaUJBQWpCLHNCQUFtRDJELFFBQW5EO0FBQUEsT0FGYjtBQUdSQywwQkFBb0IsNEJBQUNELFFBQUQ7QUFBQSxlQUFpQjNELGlCQUFqQixzQkFBbUQyRCxRQUFuRDtBQUFBLE9BSFo7QUFJUkUsNEJBQXNCLDhCQUFDRixRQUFEO0FBQUEsZUFBaUIzRCxpQkFBakIsc0JBQW1EMkQsUUFBbkQ7QUFBQSxPQUpkO0FBS1J0RixzQkFBZ0Isd0JBQUNzRixRQUFEO0FBQUEsZUFBaUIzRCxpQkFBakIsc0JBQW1EMkQsUUFBbkQ7QUFBQSxPQUxSO0FBTVJHLDBCQUFvQiw0QkFBQ0gsUUFBRDtBQUFBLGVBQWlCM0QsaUJBQWpCLHNCQUFtRDJELFFBQW5EO0FBQUEsT0FOWjtBQU9SSSx5QkFBbUIsMkJBQUNKLFFBQUQ7QUFBQSxlQUFpQjNELGlCQUFqQixzQkFBbUQyRCxRQUFuRDtBQUFBLE9BUFg7QUFRUksscUJBQWUsdUJBQUNMLFFBQUQ7QUFBQSxlQUFpQjNELGlCQUFqQixzQkFBbUQyRCxRQUFuRDtBQUFBLE9BUlA7QUFTUk0sc0JBQWdCLHdCQUFDTixRQUFEO0FBQUEsZUFBaUIzRCxpQkFBakIsc0JBQW1EMkQsUUFBbkQ7QUFBQSxPQVRSO0FBVVJPLGtCQUFZLG9CQUFDUCxRQUFEO0FBQUEsZUFBaUIzRCxpQkFBakIsc0JBQW1EMkQsUUFBbkQ7QUFBQSxPQVZKO0FBV1JRLG9CQUFjLHNCQUFDUixRQUFEO0FBQUEsZUFBaUIzRCxpQkFBakIsc0JBQW1EMkQsUUFBbkQ7QUFBQTtBQVhOLEtBaENFO0FBNkNaUyxlQUFXLHlCQTdDQztBQThDWjVLLFlBQVE7QUFDTkMseUJBQW1CLDhCQURiO0FBRU5LLG1CQUFhLDJCQUZQO0FBR05ILHlCQUFtQixhQUhiO0FBSU5RLGdCQUFVO0FBSkosS0E5Q0k7QUFvRFprSyxlQUFXLDJCQXBEQztBQXFEWkMsaUJBQWEsbURBckREO0FBc0RaQyxnQ0FBNEI7QUF0RGhCLEdBL0JEO0FBdUZiQyxrQkFBZ0I7QUFDZEMsZUFBVyxpQ0FERztBQUVkQywwQkFBc0I7QUFGUixHQXZGSDtBQTJGYkMsWUFBVTtBQUNSQyxtQkFBZTtBQUNiQyxtQkFBYTtBQURBLEtBRFA7QUFJUkMsZ0JBQVk7QUFDVkMsZUFBUztBQURDLEtBSko7QUFPUkMsZ0JBQVksY0FQSjtBQVFSQyxlQUFXLGFBUkg7QUFTUkMsdUJBQW1CLDBCQVRYO0FBVVJDLG1CQUFlLCtCQVZQO0FBV1JDLHVCQUFtQixxQkFYWDtBQVlSakwsY0FBVSxvQkFaRjtBQWFSa0wsbUJBQWUsZUFiUDtBQWNSQyx5QkFBcUIsNkJBQUNDLE9BQUQ7QUFBQSx1Q0FBcUNBLE9BQXJDO0FBQUEsS0FkYjtBQWVSQyxnQkFBWSxvQkFBQ0QsT0FBRDtBQUFBLHVDQUFxQ0EsT0FBckM7QUFBQSxLQWZKO0FBZ0JSRSxvQkFBZ0Isc0JBaEJSO0FBaUJSQyx1QkFBbUI7QUFqQlgsR0EzRkc7QUE4R2JwSyxhQUFXO0FBQ1RxSyxzQkFBa0IsNEJBRFQ7QUFFVEMsMEJBQXNCO0FBRmIsR0E5R0U7QUFrSGJDLE9BQUs7QUFDSHBCLGVBQVcsbUJBRFI7QUFFSHFCLGtCQUFjLDJCQUZYO0FBR0hDLGtCQUFjLDJCQUhYO0FBSUhDLHdCQUFvQiwyQkFKakI7QUFLSEMsd0JBQW9CLDJCQUxqQjtBQU1IQyxvQkFBZ0IseUJBTmI7QUFPSEMsb0JBQWdCO0FBQ2RDLGlCQUFXLG1DQURHO0FBRWRDLG1CQUFhO0FBRkM7QUFQYixHQWxIUTtBQThIYkMsVUFBUSxVQTlISztBQStIYkMsV0FBUyxXQS9ISTtBQWdJYkMsYUFBVyxjQWhJRTtBQWlJYkMsYUFBVyxxQkFqSUU7QUFrSWJDLGtCQUFnQixrQ0FsSUg7QUFtSWJDLGFBQVcsYUFuSUU7QUFvSWJDLGNBQVksY0FwSUM7QUFxSWJDLFVBQVE7QUFDTkMsc0JBQWtCLHFCQURaO0FBRU5DLHlCQUFxQjtBQUZmLEdBcklLO0FBeUliQyxjQUFZO0FBQ1ZDLHlCQUFxQiwwQkFEWDtBQUVWQyxrQkFBYyxxQkFGSjtBQUdWQyxpQkFBYSx3QkFISDtBQUlWQyx1QkFBbUIsdUJBSlQ7QUFLVkMsbUJBQWUsaUJBTEw7QUFNVkMsMkJBQXVCLHlCQU5iO0FBT1ZDLGtCQUFjLHNCQUFDQyxVQUFEO0FBQUEsMERBQTJEQSxVQUEzRDtBQUFBLEtBUEo7QUFRVkMsc0JBQWtCLGNBUlI7QUFTVkMsZ0JBQVksY0FURjtBQVVWQyx1QkFBbUIsc0JBVlQ7QUFXVkMsZUFBVyxtQkFBQ0osVUFBRDtBQUFBLDBEQUEyREEsVUFBM0Q7QUFBQSxLQVhEO0FBWVZLLG1CQUFlLDRCQVpMO0FBYVZ2RCxpQkFBYSw2QkFiSDtBQWNWd0QsY0FBVSxnQkFkQTtBQWVWQyxZQUFRLHlCQWZFO0FBZ0JWQyxrQkFBYyxnQkFoQko7QUFpQlZDLGdCQUFZLGNBakJGO0FBa0JWQyxxQkFBaUIseUJBbEJQO0FBbUJWQyxxQkFBaUI7QUFuQlAsR0F6SUM7QUE4SmJDLFdBQVM7QUFDUEMsc0JBQWtCLHNDQURYO0FBRVBDLG1CQUFlLHNCQUZSO0FBR1A3Tyx1QkFBbUIsbUJBSFo7QUFJUDhPLG9CQUFnQixzQkFKVDtBQUtQQyxzQkFBa0IsNENBTFg7QUFNUEMscUJBQWlCLHlCQUFDQyxRQUFEO0FBQUEsNkRBQTREQSxRQUE1RDtBQUFBLEtBTlY7QUFPUEMsc0JBQWtCLGtCQVBYO0FBUVBDLG9CQUFnQiwyQ0FSVDtBQVNQQyxtQkFBZSx1QkFBQ0gsUUFBRDtBQUFBLDREQUEyREEsUUFBM0Q7QUFBQTtBQVRSO0FBOUpJLEM7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDM0JmOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O2tCQXlCZSxVQUFDbE8sZUFBRCxFQUFxQjtBQUNsQyxNQUFNc08scUJBQXdCdE8sZUFBeEIsdUJBQU47QUFDQSxNQUFNdU8seUJBQXlCLFNBQXpCQSxzQkFBeUIsQ0FBQ0MsYUFBRCxFQUFnQkMsU0FBaEI7QUFBQSxXQUFpQ0gsa0JBQWpDLFNBQXVERSxhQUF2RCxTQUF3RUMsU0FBeEU7QUFBQSxHQUEvQjs7QUFFQSxTQUFPO0FBQ0xyTyxxQ0FBK0JrTyxrQkFEMUI7QUFFTGhPLHNCQUFxQk4sZUFBckIsa0JBRks7QUFHTFMsMEJBQXlCVCxlQUF6Qix5QkFISztBQUlMVywyQkFBMEIyTixrQkFBMUIsV0FKSztBQUtMek4sZ0NBQStCeU4sa0JBQS9CLGlCQUxLO0FBTUwxTSwwQkFBc0Isa0JBTmpCO0FBT0xxQix3QkFBb0I7QUFDbEJLLHVCQUFpQix5QkFBQ2tMLGFBQUQ7QUFBQSxlQUFtQkQsdUJBQXVCQyxhQUF2QixFQUFzQyxhQUF0QyxDQUFuQjtBQUFBLE9BREM7QUFFbEI5Syx5QkFBbUIsMkJBQUM4SyxhQUFEO0FBQUEsZUFBbUJELHVCQUF1QkMsYUFBdkIsRUFBc0MsZUFBdEMsQ0FBbkI7QUFBQSxPQUZEO0FBR2xCN0ssOEJBQXdCLGdDQUFDNkssYUFBRDtBQUFBLGVBQW1CRCx1QkFBdUJDLGFBQXZCLEVBQXNDLHFCQUF0QyxDQUFuQjtBQUFBLE9BSE47QUFJbEIzSyxzQkFBZ0Isd0JBQUMySyxhQUFEO0FBQUEsZUFBbUJELHVCQUF1QkMsYUFBdkIsRUFBc0MsV0FBdEMsQ0FBbkI7QUFBQSxPQUpFO0FBS2xCekssa0JBQVksb0JBQUN5SyxhQUFEO0FBQUEsZUFBbUJELHVCQUF1QkMsYUFBdkIsRUFBc0Msb0JBQXRDLENBQW5CO0FBQUEsT0FMTTtBQU1sQnZLLHVCQUFpQix5QkFBQ3VLLGFBQUQ7QUFBQSxlQUFtQkQsdUJBQXVCQyxhQUF2QixFQUFzQyxhQUF0QyxDQUFuQjtBQUFBLE9BTkM7QUFPbEJoTCwyQkFBcUIsNkJBQUNnTCxhQUFEO0FBQUEsMENBQTRDQSxhQUE1QztBQUFBO0FBUEgsS0FQZjtBQWdCTHJQLHVCQUFtQjtBQWhCZCxHQUFQO0FBa0JELEM7Ozs7Ozs7Ozs7QUMvQ0Qsa0JBQWtCLFlBQVksbUJBQU8sQ0FBQyw0RkFBa0Msc0I7Ozs7Ozs7Ozs7QUNBeEUsa0JBQWtCLFlBQVksbUJBQU8sQ0FBQyw4R0FBMkMsc0I7Ozs7Ozs7Ozs7O0FDQXBFOztBQUViLGtCQUFrQjs7QUFFbEIsZUFBZTtBQUNmO0FBQ0E7QUFDQTtBQUNBLEU7Ozs7Ozs7Ozs7O0FDUmE7O0FBRWIsa0JBQWtCOztBQUVsQixzQkFBc0IsbUJBQU8sQ0FBQyx5R0FBbUM7O0FBRWpFOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixlQUFlO0FBQ2Y7QUFDQSxtQkFBbUIsa0JBQWtCO0FBQ3JDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDLEc7Ozs7Ozs7Ozs7QUMxQkQsbUJBQU8sQ0FBQyxvR0FBaUM7QUFDekMsY0FBYyx3R0FBcUM7QUFDbkQ7QUFDQTtBQUNBOzs7Ozs7Ozs7OztBQ0pBLG1CQUFPLENBQUMsc0hBQTBDO0FBQ2xELGNBQWMsd0dBQXFDO0FBQ25EO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNKQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNIQSxlQUFlLG1CQUFPLENBQUMsMEVBQWM7QUFDckM7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7O0FDSkE7QUFDQTtBQUNBLGdCQUFnQixtQkFBTyxDQUFDLDRFQUFlO0FBQ3ZDLGVBQWUsbUJBQU8sQ0FBQywwRUFBYztBQUNyQyxzQkFBc0IsbUJBQU8sQ0FBQywwRkFBc0I7QUFDcEQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLLFlBQVksZUFBZTtBQUNoQztBQUNBLEtBQUs7QUFDTDtBQUNBOzs7Ozs7Ozs7OztBQ3RCQSxpQkFBaUI7O0FBRWpCO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNKQSw2QkFBNkI7QUFDN0IsdUNBQXVDOzs7Ozs7Ozs7OztBQ0R2QztBQUNBLGdCQUFnQixtQkFBTyxDQUFDLDRFQUFlO0FBQ3ZDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNuQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNKQTtBQUNBLGtCQUFrQixtQkFBTyxDQUFDLGtFQUFVO0FBQ3BDLGlDQUFpQyxRQUFRLG1CQUFtQixVQUFVLEVBQUUsRUFBRTtBQUMxRSxDQUFDOzs7Ozs7Ozs7OztBQ0hELGVBQWUsbUJBQU8sQ0FBQywwRUFBYztBQUNyQyxlQUFlLGtHQUE2QjtBQUM1QztBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7OztBQ05BO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7OztBQ0hBLGFBQWEsbUJBQU8sQ0FBQyxvRUFBVztBQUNoQyxXQUFXLG1CQUFPLENBQUMsZ0VBQVM7QUFDNUIsVUFBVSxtQkFBTyxDQUFDLDhEQUFRO0FBQzFCLFdBQVcsbUJBQU8sQ0FBQyxnRUFBUztBQUM1QixVQUFVLG1CQUFPLENBQUMsOERBQVE7QUFDMUI7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpRUFBaUU7QUFDakU7QUFDQSxrRkFBa0Y7QUFDbEY7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVc7QUFDWCxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLCtDQUErQztBQUMvQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxjQUFjO0FBQ2QsY0FBYztBQUNkLGNBQWM7QUFDZCxjQUFjO0FBQ2QsZUFBZTtBQUNmLGVBQWU7QUFDZixlQUFlO0FBQ2YsZ0JBQWdCO0FBQ2hCOzs7Ozs7Ozs7OztBQzdEQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBOzs7Ozs7Ozs7OztBQ05BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx5Q0FBeUM7Ozs7Ozs7Ozs7O0FDTHpDLHVCQUF1QjtBQUN2QjtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7O0FDSEEsU0FBUyxtQkFBTyxDQUFDLDBFQUFjO0FBQy9CLGlCQUFpQixtQkFBTyxDQUFDLGtGQUFrQjtBQUMzQyxpQkFBaUIsbUJBQU8sQ0FBQyw4RUFBZ0I7QUFDekM7QUFDQSxDQUFDO0FBQ0Q7QUFDQTtBQUNBOzs7Ozs7Ozs7OztBQ1BBLGVBQWUsa0dBQTZCO0FBQzVDOzs7Ozs7Ozs7OztBQ0RBLGtCQUFrQixtQkFBTyxDQUFDLDhFQUFnQixNQUFNLG1CQUFPLENBQUMsa0VBQVU7QUFDbEUsK0JBQStCLG1CQUFPLENBQUMsNEVBQWUsZ0JBQWdCLG1CQUFtQixVQUFVLEVBQUUsRUFBRTtBQUN2RyxDQUFDOzs7Ozs7Ozs7OztBQ0ZEO0FBQ0EsVUFBVSxtQkFBTyxDQUFDLDhEQUFRO0FBQzFCO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7OztBQ0xBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNGQTs7Ozs7Ozs7Ozs7QUNBQTtBQUNBLGVBQWUsbUJBQU8sQ0FBQywwRUFBYztBQUNyQyxVQUFVLG1CQUFPLENBQUMsNEVBQWU7QUFDakMsa0JBQWtCLG1CQUFPLENBQUMsa0ZBQWtCO0FBQzVDLGVBQWUsbUJBQU8sQ0FBQyw0RUFBZTtBQUN0Qyx5QkFBeUI7QUFDekI7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsZUFBZSxtQkFBTyxDQUFDLDRFQUFlO0FBQ3RDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxFQUFFLGlHQUE4QjtBQUNoQyw2QkFBNkI7QUFDN0I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBOzs7Ozs7Ozs7OztBQ3hDQSxlQUFlLG1CQUFPLENBQUMsMEVBQWM7QUFDckMscUJBQXFCLG1CQUFPLENBQUMsb0ZBQW1CO0FBQ2hELGtCQUFrQixtQkFBTyxDQUFDLGdGQUFpQjtBQUMzQzs7QUFFQSxTQUFTLEdBQUcsbUJBQU8sQ0FBQyw4RUFBZ0I7QUFDcEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUcsWUFBWTtBQUNmO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7OztBQ2ZBLFNBQVMsbUJBQU8sQ0FBQywwRUFBYztBQUMvQixlQUFlLG1CQUFPLENBQUMsMEVBQWM7QUFDckMsY0FBYyxtQkFBTyxDQUFDLDhFQUFnQjs7QUFFdEMsaUJBQWlCLG1CQUFPLENBQUMsOEVBQWdCO0FBQ3pDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7O0FDWkEsVUFBVSxtQkFBTyxDQUFDLDhEQUFRO0FBQzFCLGdCQUFnQixtQkFBTyxDQUFDLDRFQUFlO0FBQ3ZDLG1CQUFtQixtQkFBTyxDQUFDLG9GQUFtQjtBQUM5QyxlQUFlLG1CQUFPLENBQUMsNEVBQWU7O0FBRXRDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNoQkE7QUFDQSxZQUFZLG1CQUFPLENBQUMsZ0dBQXlCO0FBQzdDLGtCQUFrQixtQkFBTyxDQUFDLGtGQUFrQjs7QUFFNUM7QUFDQTtBQUNBOzs7Ozs7Ozs7OztBQ05BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7O0FDUEEsYUFBYSxtQkFBTyxDQUFDLG9FQUFXO0FBQ2hDLFVBQVUsbUJBQU8sQ0FBQyw4REFBUTtBQUMxQjtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7O0FDSkEsV0FBVyxtQkFBTyxDQUFDLGdFQUFTO0FBQzVCLGFBQWEsbUJBQU8sQ0FBQyxvRUFBVztBQUNoQztBQUNBLGtEQUFrRDs7QUFFbEQ7QUFDQSxxRUFBcUU7QUFDckUsQ0FBQztBQUNEO0FBQ0EsUUFBUSxtQkFBTyxDQUFDLHNFQUFZO0FBQzVCO0FBQ0EsQ0FBQzs7Ozs7Ozs7Ozs7QUNYRCxnQkFBZ0IsbUJBQU8sQ0FBQyw0RUFBZTtBQUN2QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7O0FDTkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7OztBQ0xBO0FBQ0EsY0FBYyxtQkFBTyxDQUFDLHNFQUFZO0FBQ2xDLGNBQWMsbUJBQU8sQ0FBQyxzRUFBWTtBQUNsQztBQUNBO0FBQ0E7Ozs7Ozs7Ozs7O0FDTEE7QUFDQSxnQkFBZ0IsbUJBQU8sQ0FBQyw0RUFBZTtBQUN2QztBQUNBO0FBQ0EsMkRBQTJEO0FBQzNEOzs7Ozs7Ozs7OztBQ0xBO0FBQ0EsZUFBZSxtQkFBTyxDQUFDLDBFQUFjO0FBQ3JDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7OztBQ1hBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7O0FDSkEsY0FBYyxtQkFBTyxDQUFDLG9FQUFXO0FBQ2pDO0FBQ0EsOEJBQThCLFNBQVMsbUJBQU8sQ0FBQyxrRkFBa0IsR0FBRzs7Ozs7Ozs7Ozs7QUNGcEUsY0FBYyxtQkFBTyxDQUFDLG9FQUFXO0FBQ2pDO0FBQ0EsaUNBQWlDLG1CQUFPLENBQUMsOEVBQWdCLGNBQWMsaUJBQWlCLGlHQUF5QixFQUFFOzs7Ozs7O1VDRm5IO1VBQ0E7O1VBRUE7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7O1VBRUE7VUFDQTs7VUFFQTtVQUNBO1VBQ0E7Ozs7Ozs7Ozs7OztBQ0dBOzs7O0FBQ0E7Ozs7QUFDQTs7Ozs7O2NBRVlULE07SUFBTEMsQyxXQUFBQSxDLEVBN0JQOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBK0JBQSxFQUFFLFlBQU07QUFDTkQsU0FBT2dRLFVBQVAsQ0FBa0JDLFNBQWxCLENBQTRCQyxjQUE1QixDQUEyQyxDQUN6QyxtQkFEeUMsRUFFekMsZUFGeUMsRUFHekMsbUJBSHlDLEVBSXpDLGNBSnlDLEVBS3pDLHVCQUx5QyxDQUEzQzs7QUFRQSxNQUFJN08saUNBQUosQ0FBNEJqQixxQkFBV2dDLFNBQVgsQ0FBcUJzSyxvQkFBakQsRUFBdUUsS0FBdkU7QUFDQSxNQUFJeE0sdUJBQUo7QUFDRCxDQVhELEUiLCJmaWxlIjoiY29tYmluYXRpb25fZWRpdC5idW5kbGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIENvcHlyaWdodCBzaW5jZSAyMDA3IFByZXN0YVNob3AgU0EgYW5kIENvbnRyaWJ1dG9yc1xuICogUHJlc3RhU2hvcCBpcyBhbiBJbnRlcm5hdGlvbmFsIFJlZ2lzdGVyZWQgVHJhZGVtYXJrICYgUHJvcGVydHkgb2YgUHJlc3RhU2hvcCBTQVxuICpcbiAqIE5PVElDRSBPRiBMSUNFTlNFXG4gKlxuICogVGhpcyBzb3VyY2UgZmlsZSBpcyBzdWJqZWN0IHRvIHRoZSBPcGVuIFNvZnR3YXJlIExpY2Vuc2UgKE9TTCAzLjApXG4gKiB0aGF0IGlzIGJ1bmRsZWQgd2l0aCB0aGlzIHBhY2thZ2UgaW4gdGhlIGZpbGUgTElDRU5TRS5tZC5cbiAqIEl0IGlzIGFsc28gYXZhaWxhYmxlIHRocm91Z2ggdGhlIHdvcmxkLXdpZGUtd2ViIGF0IHRoaXMgVVJMOlxuICogaHR0cHM6Ly9vcGVuc291cmNlLm9yZy9saWNlbnNlcy9PU0wtMy4wXG4gKiBJZiB5b3UgZGlkIG5vdCByZWNlaXZlIGEgY29weSBvZiB0aGUgbGljZW5zZSBhbmQgYXJlIHVuYWJsZSB0b1xuICogb2J0YWluIGl0IHRocm91Z2ggdGhlIHdvcmxkLXdpZGUtd2ViLCBwbGVhc2Ugc2VuZCBhbiBlbWFpbFxuICogdG8gbGljZW5zZUBwcmVzdGFzaG9wLmNvbSBzbyB3ZSBjYW4gc2VuZCB5b3UgYSBjb3B5IGltbWVkaWF0ZWx5LlxuICpcbiAqIERJU0NMQUlNRVJcbiAqXG4gKiBEbyBub3QgZWRpdCBvciBhZGQgdG8gdGhpcyBmaWxlIGlmIHlvdSB3aXNoIHRvIHVwZ3JhZGUgUHJlc3RhU2hvcCB0byBuZXdlclxuICogdmVyc2lvbnMgaW4gdGhlIGZ1dHVyZS4gSWYgeW91IHdpc2ggdG8gY3VzdG9taXplIFByZXN0YVNob3AgZm9yIHlvdXJcbiAqIG5lZWRzIHBsZWFzZSByZWZlciB0byBodHRwczovL2RldmRvY3MucHJlc3Rhc2hvcC5jb20vIGZvciBtb3JlIGluZm9ybWF0aW9uLlxuICpcbiAqIEBhdXRob3IgICAgUHJlc3RhU2hvcCBTQSBhbmQgQ29udHJpYnV0b3JzIDxjb250YWN0QHByZXN0YXNob3AuY29tPlxuICogQGNvcHlyaWdodCBTaW5jZSAyMDA3IFByZXN0YVNob3AgU0EgYW5kIENvbnRyaWJ1dG9yc1xuICogQGxpY2Vuc2UgICBodHRwczovL29wZW5zb3VyY2Uub3JnL2xpY2Vuc2VzL09TTC0zLjAgT3BlbiBTb2Z0d2FyZSBMaWNlbnNlIChPU0wgMy4wKVxuICovXG5cbmltcG9ydCBQcm9kdWN0TWFwIGZyb20gJ0BwYWdlcy9wcm9kdWN0L3Byb2R1Y3QtbWFwJztcblxuY29uc3QgeyR9ID0gd2luZG93O1xuXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBJbWFnZVNlbGVjdG9yIHtcbiAgY29uc3RydWN0b3IoKSB7XG4gICAgdGhpcy4kc2VsZWN0b3JDb250YWluZXIgPSAkKFByb2R1Y3RNYXAuY29tYmluYXRpb25zLmltYWdlcy5zZWxlY3RvckNvbnRhaW5lcik7XG4gICAgdGhpcy5pbml0KCk7XG4gIH1cblxuICBpbml0KCkge1xuICAgICQoUHJvZHVjdE1hcC5jb21iaW5hdGlvbnMuaW1hZ2VzLmNoZWNrYm94Q29udGFpbmVyLCB0aGlzLiRzZWxlY3RvckNvbnRhaW5lcikuaGlkZSgpO1xuICAgIHRoaXMuJHNlbGVjdG9yQ29udGFpbmVyLm9uKCdjbGljaycsIFByb2R1Y3RNYXAuY29tYmluYXRpb25zLmltYWdlcy5pbWFnZUNob2ljZSwgKGV2ZW50KSA9PiB7XG4gICAgICBjb25zdCAkaW1hZ2VDaG9pY2UgPSAkKGV2ZW50LmN1cnJlbnRUYXJnZXQpO1xuICAgICAgY29uc3QgJGNoZWNrYm94ID0gJChQcm9kdWN0TWFwLmNvbWJpbmF0aW9ucy5pbWFnZXMuY2hlY2tib3gsICRpbWFnZUNob2ljZSk7XG5cbiAgICAgIGNvbnN0IGlzQ2hlY2tlZCA9ICRjaGVja2JveC5wcm9wKCdjaGVja2VkJyk7XG4gICAgICAkaW1hZ2VDaG9pY2UudG9nZ2xlQ2xhc3MoJ3NlbGVjdGVkJywgIWlzQ2hlY2tlZCk7XG4gICAgICAkY2hlY2tib3gucHJvcCgnY2hlY2tlZCcsICFpc0NoZWNrZWQpO1xuICAgIH0pO1xuICB9XG59XG4iLCIvKipcbiAqIENvcHlyaWdodCBzaW5jZSAyMDA3IFByZXN0YVNob3AgU0EgYW5kIENvbnRyaWJ1dG9yc1xuICogUHJlc3RhU2hvcCBpcyBhbiBJbnRlcm5hdGlvbmFsIFJlZ2lzdGVyZWQgVHJhZGVtYXJrICYgUHJvcGVydHkgb2YgUHJlc3RhU2hvcCBTQVxuICpcbiAqIE5PVElDRSBPRiBMSUNFTlNFXG4gKlxuICogVGhpcyBzb3VyY2UgZmlsZSBpcyBzdWJqZWN0IHRvIHRoZSBPcGVuIFNvZnR3YXJlIExpY2Vuc2UgKE9TTCAzLjApXG4gKiB0aGF0IGlzIGJ1bmRsZWQgd2l0aCB0aGlzIHBhY2thZ2UgaW4gdGhlIGZpbGUgTElDRU5TRS5tZC5cbiAqIEl0IGlzIGFsc28gYXZhaWxhYmxlIHRocm91Z2ggdGhlIHdvcmxkLXdpZGUtd2ViIGF0IHRoaXMgVVJMOlxuICogaHR0cHM6Ly9vcGVuc291cmNlLm9yZy9saWNlbnNlcy9PU0wtMy4wXG4gKiBJZiB5b3UgZGlkIG5vdCByZWNlaXZlIGEgY29weSBvZiB0aGUgbGljZW5zZSBhbmQgYXJlIHVuYWJsZSB0b1xuICogb2J0YWluIGl0IHRocm91Z2ggdGhlIHdvcmxkLXdpZGUtd2ViLCBwbGVhc2Ugc2VuZCBhbiBlbWFpbFxuICogdG8gbGljZW5zZUBwcmVzdGFzaG9wLmNvbSBzbyB3ZSBjYW4gc2VuZCB5b3UgYSBjb3B5IGltbWVkaWF0ZWx5LlxuICpcbiAqIERJU0NMQUlNRVJcbiAqXG4gKiBEbyBub3QgZWRpdCBvciBhZGQgdG8gdGhpcyBmaWxlIGlmIHlvdSB3aXNoIHRvIHVwZ3JhZGUgUHJlc3RhU2hvcCB0byBuZXdlclxuICogdmVyc2lvbnMgaW4gdGhlIGZ1dHVyZS4gSWYgeW91IHdpc2ggdG8gY3VzdG9taXplIFByZXN0YVNob3AgZm9yIHlvdXJcbiAqIG5lZWRzIHBsZWFzZSByZWZlciB0byBodHRwczovL2RldmRvY3MucHJlc3Rhc2hvcC5jb20vIGZvciBtb3JlIGluZm9ybWF0aW9uLlxuICpcbiAqIEBhdXRob3IgICAgUHJlc3RhU2hvcCBTQSBhbmQgQ29udHJpYnV0b3JzIDxjb250YWN0QHByZXN0YXNob3AuY29tPlxuICogQGNvcHlyaWdodCBTaW5jZSAyMDA3IFByZXN0YVNob3AgU0EgYW5kIENvbnRyaWJ1dG9yc1xuICogQGxpY2Vuc2UgICBodHRwczovL29wZW5zb3VyY2Uub3JnL2xpY2Vuc2VzL09TTC0zLjAgT3BlbiBTb2Z0d2FyZSBMaWNlbnNlIChPU0wgMy4wKVxuICovXG5cbmltcG9ydCBTdXBwbGllcnNNYXAgZnJvbSAnQHBhZ2VzL3Byb2R1Y3Qvc3VwcGxpZXJzLW1hcCc7XG5cbmNvbnN0IHskfSA9IHdpbmRvdztcblxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgUHJvZHVjdFN1cHBsaWVyc01hbmFnZXIge1xuICAvKipcbiAgICpcbiAgICogQHBhcmFtIHtzdHJpbmd9IHN1cHBsaWVyc0Zvcm1JZFxuICAgKiBAcGFyYW0ge2Jvb2xlYW59IGZvcmNlVXBkYXRlRGVmYXVsdFxuICAgKlxuICAgKiBAcmV0dXJucyB7e319XG4gICAqL1xuICBjb25zdHJ1Y3RvcihzdXBwbGllcnNGb3JtSWQsIGZvcmNlVXBkYXRlRGVmYXVsdCkge1xuICAgIHRoaXMuZm9yY2VVcGRhdGVEZWZhdWx0ID0gZm9yY2VVcGRhdGVEZWZhdWx0O1xuICAgIHRoaXMuc3VwcGxpZXJzTWFwID0gU3VwcGxpZXJzTWFwKHN1cHBsaWVyc0Zvcm1JZCk7XG4gICAgdGhpcy4kcHJvZHVjdFN1cHBsaWVyc0NvbGxlY3Rpb24gPSAkKHRoaXMuc3VwcGxpZXJzTWFwLnByb2R1Y3RTdXBwbGllcnNDb2xsZWN0aW9uKTtcbiAgICB0aGlzLiRzdXBwbGllcklkc0dyb3VwID0gJCh0aGlzLnN1cHBsaWVyc01hcC5zdXBwbGllcklkc0lucHV0KS5jbG9zZXN0KCcuZm9ybS1ncm91cCcpO1xuICAgIHRoaXMuJGRlZmF1bHRTdXBwbGllckdyb3VwID0gJCh0aGlzLnN1cHBsaWVyc01hcC5kZWZhdWx0U3VwcGxpZXJJbnB1dCkuY2xvc2VzdCgnLmZvcm0tZ3JvdXAnKTtcbiAgICB0aGlzLiRwcm9kdWN0c1RhYmxlID0gJCh0aGlzLnN1cHBsaWVyc01hcC5wcm9kdWN0U3VwcGxpZXJzVGFibGUpO1xuICAgIHRoaXMuJHByb2R1Y3RzVGFibGVCb2R5ID0gJCh0aGlzLnN1cHBsaWVyc01hcC5wcm9kdWN0c1N1cHBsaWVyc1RhYmxlQm9keSk7XG5cbiAgICB0aGlzLnN1cHBsaWVycyA9IFtdO1xuICAgIHRoaXMucHJvdG90eXBlVGVtcGxhdGUgPSB0aGlzLiRwcm9kdWN0U3VwcGxpZXJzQ29sbGVjdGlvbi5kYXRhKCdwcm90b3R5cGUnKTtcbiAgICB0aGlzLnByb3RvdHlwZU5hbWUgPSB0aGlzLiRwcm9kdWN0U3VwcGxpZXJzQ29sbGVjdGlvbi5kYXRhKCdwcm90b3R5cGVOYW1lJyk7XG4gICAgdGhpcy5kZWZhdWx0RGF0YUZvclN1cHBsaWVyID0gdGhpcy5nZXREZWZhdWx0RGF0YUZvclN1cHBsaWVyKCk7XG5cbiAgICB0aGlzLmluaXQoKTtcblxuICAgIHJldHVybiB7fTtcbiAgfVxuXG4gIGluaXQoKSB7XG4gICAgdGhpcy5tZW1vcml6ZUN1cnJlbnRTdXBwbGllcnMoKTtcbiAgICB0aGlzLnRvZ2dsZVRhYmxlVmlzaWJpbGl0eSgpO1xuICAgIHRoaXMucmVmcmVzaERlZmF1bHRTdXBwbGllckJsb2NrKCk7XG5cbiAgICB0aGlzLiRpbml0aWFsRGVmYXVsdCA9IHRoaXMuJGRlZmF1bHRTdXBwbGllckdyb3VwLmZpbmQoJ2lucHV0OmNoZWNrZWQnKS5maXJzdCgpO1xuICAgIGlmICh0aGlzLiRpbml0aWFsRGVmYXVsdC5sZW5ndGgpIHtcbiAgICAgIHRoaXMuJGluaXRpYWxEZWZhdWx0XG4gICAgICAgIC5jbG9zZXN0KHRoaXMuc3VwcGxpZXJzTWFwLmNoZWNrYm94Q29udGFpbmVyKVxuICAgICAgICAuYWRkQ2xhc3ModGhpcy5zdXBwbGllcnNNYXAuZGVmYXVsdFN1cHBsaWVyQ2xhc3MpO1xuICAgIH1cblxuICAgIHRoaXMuJHByb2R1Y3RzVGFibGUub24oJ2NoYW5nZScsICdpbnB1dCcsICgpID0+IHtcbiAgICAgIHRoaXMubWVtb3JpemVDdXJyZW50U3VwcGxpZXJzKCk7XG4gICAgfSk7XG5cbiAgICB0aGlzLiRzdXBwbGllcklkc0dyb3VwLm9uKCdjaGFuZ2UnLCAnaW5wdXQnLCAoZSkgPT4ge1xuICAgICAgY29uc3QgaW5wdXQgPSBlLmN1cnJlbnRUYXJnZXQ7XG5cbiAgICAgIGlmIChpbnB1dC5jaGVja2VkKSB7XG4gICAgICAgIHRoaXMuYWRkU3VwcGxpZXIoe1xuICAgICAgICAgIHN1cHBsaWVySWQ6IGlucHV0LnZhbHVlLFxuICAgICAgICAgIHN1cHBsaWVyTmFtZTogaW5wdXQuZGF0YXNldC5sYWJlbCxcbiAgICAgICAgfSk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICB0aGlzLnJlbW92ZVN1cHBsaWVyKGlucHV0LnZhbHVlKTtcbiAgICAgIH1cblxuICAgICAgdGhpcy5yZW5kZXJTdXBwbGllcnMoKTtcbiAgICAgIHRoaXMudG9nZ2xlVGFibGVWaXNpYmlsaXR5KCk7XG4gICAgICB0aGlzLnJlZnJlc2hEZWZhdWx0U3VwcGxpZXJCbG9jaygpO1xuICAgIH0pO1xuICB9XG5cbiAgdG9nZ2xlVGFibGVWaXNpYmlsaXR5KCkge1xuICAgIGlmICh0aGlzLmdldFNlbGVjdGVkU3VwcGxpZXJzKCkubGVuZ3RoID09PSAwKSB7XG4gICAgICB0aGlzLmhpZGVUYWJsZSgpO1xuXG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgdGhpcy5zaG93VGFibGUoKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBAcGFyYW0ge09iamVjdH0gc3VwcGxpZXJcbiAgICovXG4gIGFkZFN1cHBsaWVyKHN1cHBsaWVyKSB7XG4gICAgaWYgKHR5cGVvZiB0aGlzLnN1cHBsaWVyc1tzdXBwbGllci5zdXBwbGllcklkXSA9PT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgIGNvbnN0IG5ld1N1cHBsaWVyID0gT2JqZWN0LmNyZWF0ZSh0aGlzLmRlZmF1bHREYXRhRm9yU3VwcGxpZXIpO1xuICAgICAgbmV3U3VwcGxpZXIuc3VwcGxpZXJJZCA9IHN1cHBsaWVyLnN1cHBsaWVySWQ7XG4gICAgICBuZXdTdXBwbGllci5zdXBwbGllck5hbWUgPSBzdXBwbGllci5zdXBwbGllck5hbWU7XG5cbiAgICAgIHRoaXMuc3VwcGxpZXJzW3N1cHBsaWVyLnN1cHBsaWVySWRdID0gbmV3U3VwcGxpZXI7XG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMuc3VwcGxpZXJzW3N1cHBsaWVyLnN1cHBsaWVySWRdLnJlbW92ZWQgPSBmYWxzZTtcbiAgICB9XG4gIH1cblxuICAvKipcbiAgICogQHBhcmFtIHtpbnR9IHN1cHBsaWVySWRcbiAgICovXG4gIHJlbW92ZVN1cHBsaWVyKHN1cHBsaWVySWQpIHtcbiAgICB0aGlzLnN1cHBsaWVyc1tzdXBwbGllcklkXS5yZW1vdmVkID0gdHJ1ZTtcbiAgfVxuXG4gIHJlbmRlclN1cHBsaWVycygpIHtcbiAgICB0aGlzLiRwcm9kdWN0c1RhYmxlQm9keS5lbXB0eSgpO1xuXG4gICAgLy8gTG9vcCB0aHJvdWdoIHNlbGVjdCBzdXBwbGllcnMgc28gdGhhdCB3ZSB1c2UgdGhlIHNhbWUgb3JkZXIgYXMgaW4gdGhlIHNlbGVjdCBsaXN0XG4gICAgdGhpcy5nZXRTZWxlY3RlZFN1cHBsaWVycygpLmZvckVhY2goKHNlbGVjdGVkU3VwcGxpZXIpID0+IHtcbiAgICAgIGNvbnN0IHN1cHBsaWVyID0gdGhpcy5zdXBwbGllcnNbc2VsZWN0ZWRTdXBwbGllci5zdXBwbGllcklkXTtcblxuICAgICAgaWYgKHN1cHBsaWVyLnJlbW92ZWQpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICBjb25zdCBwcm9kdWN0U3VwcGxpZXJSb3cgPSB0aGlzLnByb3RvdHlwZVRlbXBsYXRlLnJlcGxhY2UoXG4gICAgICAgIG5ldyBSZWdFeHAodGhpcy5wcm90b3R5cGVOYW1lLCAnZycpLFxuICAgICAgICBzdXBwbGllci5zdXBwbGllcklkLFxuICAgICAgKTtcblxuICAgICAgdGhpcy4kcHJvZHVjdHNUYWJsZUJvZHkuYXBwZW5kKHByb2R1Y3RTdXBwbGllclJvdyk7XG4gICAgICAvLyBGaWxsIGlucHV0c1xuICAgICAgY29uc3Qgcm93TWFwID0gdGhpcy5zdXBwbGllcnNNYXAucHJvZHVjdFN1cHBsaWVyUm93O1xuICAgICAgJChyb3dNYXAuc3VwcGxpZXJJZElucHV0KHN1cHBsaWVyLnN1cHBsaWVySWQpKS52YWwoc3VwcGxpZXIuc3VwcGxpZXJJZCk7XG4gICAgICAkKHJvd01hcC5zdXBwbGllck5hbWVQcmV2aWV3KHN1cHBsaWVyLnN1cHBsaWVySWQpKS5odG1sKHN1cHBsaWVyLnN1cHBsaWVyTmFtZSk7XG4gICAgICAkKHJvd01hcC5zdXBwbGllck5hbWVJbnB1dChzdXBwbGllci5zdXBwbGllcklkKSkudmFsKHN1cHBsaWVyLnN1cHBsaWVyTmFtZSk7XG4gICAgICAkKHJvd01hcC5wcm9kdWN0U3VwcGxpZXJJZElucHV0KHN1cHBsaWVyLnN1cHBsaWVySWQpKS52YWwoc3VwcGxpZXIucHJvZHVjdFN1cHBsaWVySWQpO1xuICAgICAgJChyb3dNYXAucmVmZXJlbmNlSW5wdXQoc3VwcGxpZXIuc3VwcGxpZXJJZCkpLnZhbChzdXBwbGllci5yZWZlcmVuY2UpO1xuICAgICAgJChyb3dNYXAucHJpY2VJbnB1dChzdXBwbGllci5zdXBwbGllcklkKSkudmFsKHN1cHBsaWVyLnByaWNlKTtcbiAgICAgICQocm93TWFwLmN1cnJlbmN5SWRJbnB1dChzdXBwbGllci5zdXBwbGllcklkKSkudmFsKHN1cHBsaWVyLmN1cnJlbmN5SWQpO1xuICAgIH0pO1xuICB9XG5cbiAgZ2V0U2VsZWN0ZWRTdXBwbGllcnMoKSB7XG4gICAgY29uc3Qgc2VsZWN0ZWRTdXBwbGllcnMgPSBbXTtcbiAgICB0aGlzLiRzdXBwbGllcklkc0dyb3VwLmZpbmQoJ2lucHV0OmNoZWNrZWQnKS5lYWNoKChpbmRleCwgaW5wdXQpID0+IHtcbiAgICAgIHNlbGVjdGVkU3VwcGxpZXJzLnB1c2goe1xuICAgICAgICBzdXBwbGllck5hbWU6IGlucHV0LmRhdGFzZXQubGFiZWwsXG4gICAgICAgIHN1cHBsaWVySWQ6IGlucHV0LnZhbHVlLFxuICAgICAgfSk7XG4gICAgfSk7XG5cbiAgICByZXR1cm4gc2VsZWN0ZWRTdXBwbGllcnM7XG4gIH1cblxuICByZWZyZXNoRGVmYXVsdFN1cHBsaWVyQmxvY2soKSB7XG4gICAgY29uc3Qgc3VwcGxpZXJzID0gdGhpcy5nZXRTZWxlY3RlZFN1cHBsaWVycygpO1xuXG4gICAgaWYgKHN1cHBsaWVycy5sZW5ndGggPT09IDApIHtcbiAgICAgIGlmICh0aGlzLmZvcmNlVXBkYXRlRGVmYXVsdCkge1xuICAgICAgICB0aGlzLiRkZWZhdWx0U3VwcGxpZXJHcm91cC5maW5kKCdpbnB1dCcpLnByb3AoJ2NoZWNrZWQnLCBmYWxzZSk7XG4gICAgICB9XG4gICAgICB0aGlzLmhpZGVEZWZhdWx0U3VwcGxpZXJzKCk7XG5cbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICB0aGlzLnNob3dEZWZhdWx0U3VwcGxpZXJzKCk7XG4gICAgY29uc3Qgc2VsZWN0ZWRTdXBwbGllcklkcyA9IHN1cHBsaWVycy5tYXAoKHN1cHBsaWVyKSA9PiBzdXBwbGllci5zdXBwbGllcklkKTtcblxuICAgIHRoaXMuJGRlZmF1bHRTdXBwbGllckdyb3VwLmZpbmQoJ2lucHV0JykuZWFjaCgoa2V5LCBpbnB1dCkgPT4ge1xuICAgICAgY29uc3QgaXNWYWxpZCA9IHNlbGVjdGVkU3VwcGxpZXJJZHMuaW5jbHVkZXMoaW5wdXQudmFsdWUpO1xuXG4gICAgICBpZiAodGhpcy5mb3JjZVVwZGF0ZURlZmF1bHQgJiYgIWlzVmFsaWQpIHtcbiAgICAgICAgaW5wdXQuY2hlY2tlZCA9IGZhbHNlO1xuICAgICAgfVxuICAgICAgaW5wdXQuZGlzYWJsZWQgPSAhaXNWYWxpZDtcbiAgICB9KTtcblxuICAgIGlmICh0aGlzLiRkZWZhdWx0U3VwcGxpZXJHcm91cC5maW5kKCdpbnB1dDpjaGVja2VkJykubGVuZ3RoID09PSAwICYmIHRoaXMuZm9yY2VVcGRhdGVEZWZhdWx0KSB7XG4gICAgICB0aGlzLmNoZWNrRmlyc3RBdmFpbGFibGVEZWZhdWx0U3VwcGxpZXIoc2VsZWN0ZWRTdXBwbGllcklkcyk7XG4gICAgfVxuICB9XG5cbiAgaGlkZURlZmF1bHRTdXBwbGllcnMoKSB7XG4gICAgdGhpcy4kZGVmYXVsdFN1cHBsaWVyR3JvdXAuYWRkQ2xhc3MoJ2Qtbm9uZScpO1xuICB9XG5cbiAgc2hvd0RlZmF1bHRTdXBwbGllcnMoKSB7XG4gICAgdGhpcy4kZGVmYXVsdFN1cHBsaWVyR3JvdXAucmVtb3ZlQ2xhc3MoJ2Qtbm9uZScpO1xuICB9XG5cbiAgLyoqXG4gICAqIEBwYXJhbSB7aW50W119IHNlbGVjdGVkU3VwcGxpZXJJZHNcbiAgICovXG4gIGNoZWNrRmlyc3RBdmFpbGFibGVEZWZhdWx0U3VwcGxpZXIoc2VsZWN0ZWRTdXBwbGllcklkcykge1xuICAgIGNvbnN0IGZpcnN0U3VwcGxpZXJJZCA9IHNlbGVjdGVkU3VwcGxpZXJJZHNbMF07XG4gICAgdGhpcy4kZGVmYXVsdFN1cHBsaWVyR3JvdXAuZmluZChgaW5wdXRbdmFsdWU9XCIke2ZpcnN0U3VwcGxpZXJJZH1cIl1gKS5wcm9wKCdjaGVja2VkJywgdHJ1ZSk7XG4gIH1cblxuICBzaG93VGFibGUoKSB7XG4gICAgdGhpcy4kcHJvZHVjdHNUYWJsZS5yZW1vdmVDbGFzcygnZC1ub25lJyk7XG4gIH1cblxuICBoaWRlVGFibGUoKSB7XG4gICAgdGhpcy4kcHJvZHVjdHNUYWJsZS5hZGRDbGFzcygnZC1ub25lJyk7XG4gIH1cblxuICAvKipcbiAgICogTWVtb3JpemUgc3VwcGxpZXJzIHRvIGJlIGFibGUgdG8gcmUtcmVuZGVyIHRoZW0gbGF0ZXIuXG4gICAqIEZsYWcgYHJlbW92ZWRgIGFsbG93cyBpZGVudGlmeWluZyB3aGV0aGVyIHN1cHBsaWVyIHdhcyByZW1vdmVkIGZyb20gbGlzdCBvciBzaG91bGQgYmUgcmVuZGVyZWRcbiAgICovXG4gIG1lbW9yaXplQ3VycmVudFN1cHBsaWVycygpIHtcbiAgICB0aGlzLmdldFNlbGVjdGVkU3VwcGxpZXJzKCkuZm9yRWFjaCgoc3VwcGxpZXIpID0+IHtcbiAgICAgIHRoaXMuc3VwcGxpZXJzW3N1cHBsaWVyLnN1cHBsaWVySWRdID0ge1xuICAgICAgICBzdXBwbGllcklkOiBzdXBwbGllci5zdXBwbGllcklkLFxuICAgICAgICBwcm9kdWN0U3VwcGxpZXJJZDogJCh0aGlzLnN1cHBsaWVyc01hcC5wcm9kdWN0U3VwcGxpZXJSb3cucHJvZHVjdFN1cHBsaWVySWRJbnB1dChzdXBwbGllci5zdXBwbGllcklkKSkudmFsKCksXG4gICAgICAgIHN1cHBsaWVyTmFtZTogJCh0aGlzLnN1cHBsaWVyc01hcC5wcm9kdWN0U3VwcGxpZXJSb3cuc3VwcGxpZXJOYW1lSW5wdXQoc3VwcGxpZXIuc3VwcGxpZXJJZCkpLnZhbCgpLFxuICAgICAgICByZWZlcmVuY2U6ICQodGhpcy5zdXBwbGllcnNNYXAucHJvZHVjdFN1cHBsaWVyUm93LnJlZmVyZW5jZUlucHV0KHN1cHBsaWVyLnN1cHBsaWVySWQpKS52YWwoKSxcbiAgICAgICAgcHJpY2U6ICQodGhpcy5zdXBwbGllcnNNYXAucHJvZHVjdFN1cHBsaWVyUm93LnByaWNlSW5wdXQoc3VwcGxpZXIuc3VwcGxpZXJJZCkpLnZhbCgpLFxuICAgICAgICBjdXJyZW5jeUlkOiAkKHRoaXMuc3VwcGxpZXJzTWFwLnByb2R1Y3RTdXBwbGllclJvdy5jdXJyZW5jeUlkSW5wdXQoc3VwcGxpZXIuc3VwcGxpZXJJZCkpLnZhbCgpLFxuICAgICAgICByZW1vdmVkOiBmYWxzZSxcbiAgICAgIH07XG4gICAgfSk7XG4gIH1cblxuICAvKipcbiAgICogQ3JlYXRlIGEgXCJzaGFkb3dcIiBwcm90b3R5cGUganVzdCB0byBwYXJzZSBkZWZhdWx0IHZhbHVlcyBzZXQgaW5zaWRlIHRoZSBpbnB1dCBmaWVsZHMsXG4gICAqIHRoaXMgYWxsb3cgdG8gYnVpbGQgYW4gb2JqZWN0IHdpdGggZGVmYXVsdCB2YWx1ZXMgc2V0IGluIHRoZSBGb3JtVHlwZVxuICAgKlxuICAgKiBAcmV0dXJucyB7e3JlZmVyZW5jZSwgcmVtb3ZlZDogYm9vbGVhbiwgcHJpY2UsIGN1cnJlbmN5SWQsIHByb2R1Y3RTdXBwbGllcklkfX1cbiAgICovXG4gIGdldERlZmF1bHREYXRhRm9yU3VwcGxpZXIoKSB7XG4gICAgY29uc3Qgcm93UHJvdG90eXBlID0gbmV3IERPTVBhcnNlcigpLnBhcnNlRnJvbVN0cmluZyhcbiAgICAgIHRoaXMucHJvdG90eXBlVGVtcGxhdGUsXG4gICAgICAndGV4dC9odG1sJyxcbiAgICApO1xuXG4gICAgcmV0dXJuIHtcbiAgICAgIHJlbW92ZWQ6IGZhbHNlLFxuICAgICAgcHJvZHVjdFN1cHBsaWVySWQ6IHRoaXMuZ2V0RGF0YUZyb21Sb3codGhpcy5zdXBwbGllcnNNYXAucHJvZHVjdFN1cHBsaWVyUm93LnByb2R1Y3RTdXBwbGllcklkSW5wdXQsIHJvd1Byb3RvdHlwZSksXG4gICAgICByZWZlcmVuY2U6IHRoaXMuZ2V0RGF0YUZyb21Sb3codGhpcy5zdXBwbGllcnNNYXAucHJvZHVjdFN1cHBsaWVyUm93LnJlZmVyZW5jZUlucHV0LCByb3dQcm90b3R5cGUpLFxuICAgICAgcHJpY2U6IHRoaXMuZ2V0RGF0YUZyb21Sb3codGhpcy5zdXBwbGllcnNNYXAucHJvZHVjdFN1cHBsaWVyUm93LnByaWNlSW5wdXQsIHJvd1Byb3RvdHlwZSksXG4gICAgICBjdXJyZW5jeUlkOiB0aGlzLmdldERhdGFGcm9tUm93KHRoaXMuc3VwcGxpZXJzTWFwLnByb2R1Y3RTdXBwbGllclJvdy5jdXJyZW5jeUlkSW5wdXQsIHJvd1Byb3RvdHlwZSksXG4gICAgfTtcbiAgfVxuXG4gIC8qKlxuICAgKiBAcGFyYW0gc2VsZWN0b3JHZW5lcmF0b3Ige2Z1bmN0aW9ufVxuICAgKiBAcGFyYW0gcm93UHJvdG90eXBlIHtEb2N1bWVudH1cbiAgICpcbiAgICogQHJldHVybnMgeyp9XG4gICAqL1xuICBnZXREYXRhRnJvbVJvdyhzZWxlY3RvckdlbmVyYXRvciwgcm93UHJvdG90eXBlKSB7XG4gICAgcmV0dXJuIHJvd1Byb3RvdHlwZS5xdWVyeVNlbGVjdG9yKHNlbGVjdG9yR2VuZXJhdG9yKHRoaXMucHJvdG90eXBlTmFtZSkpLnZhbHVlO1xuICB9XG59XG4iLCIvKipcbiAqIENvcHlyaWdodCBzaW5jZSAyMDA3IFByZXN0YVNob3AgU0EgYW5kIENvbnRyaWJ1dG9yc1xuICogUHJlc3RhU2hvcCBpcyBhbiBJbnRlcm5hdGlvbmFsIFJlZ2lzdGVyZWQgVHJhZGVtYXJrICYgUHJvcGVydHkgb2YgUHJlc3RhU2hvcCBTQVxuICpcbiAqIE5PVElDRSBPRiBMSUNFTlNFXG4gKlxuICogVGhpcyBzb3VyY2UgZmlsZSBpcyBzdWJqZWN0IHRvIHRoZSBPcGVuIFNvZnR3YXJlIExpY2Vuc2UgKE9TTCAzLjApXG4gKiB0aGF0IGlzIGJ1bmRsZWQgd2l0aCB0aGlzIHBhY2thZ2UgaW4gdGhlIGZpbGUgTElDRU5TRS5tZC5cbiAqIEl0IGlzIGFsc28gYXZhaWxhYmxlIHRocm91Z2ggdGhlIHdvcmxkLXdpZGUtd2ViIGF0IHRoaXMgVVJMOlxuICogaHR0cHM6Ly9vcGVuc291cmNlLm9yZy9saWNlbnNlcy9PU0wtMy4wXG4gKiBJZiB5b3UgZGlkIG5vdCByZWNlaXZlIGEgY29weSBvZiB0aGUgbGljZW5zZSBhbmQgYXJlIHVuYWJsZSB0b1xuICogb2J0YWluIGl0IHRocm91Z2ggdGhlIHdvcmxkLXdpZGUtd2ViLCBwbGVhc2Ugc2VuZCBhbiBlbWFpbFxuICogdG8gbGljZW5zZUBwcmVzdGFzaG9wLmNvbSBzbyB3ZSBjYW4gc2VuZCB5b3UgYSBjb3B5IGltbWVkaWF0ZWx5LlxuICpcbiAqIERJU0NMQUlNRVJcbiAqXG4gKiBEbyBub3QgZWRpdCBvciBhZGQgdG8gdGhpcyBmaWxlIGlmIHlvdSB3aXNoIHRvIHVwZ3JhZGUgUHJlc3RhU2hvcCB0byBuZXdlclxuICogdmVyc2lvbnMgaW4gdGhlIGZ1dHVyZS4gSWYgeW91IHdpc2ggdG8gY3VzdG9taXplIFByZXN0YVNob3AgZm9yIHlvdXJcbiAqIG5lZWRzIHBsZWFzZSByZWZlciB0byBodHRwczovL2RldmRvY3MucHJlc3Rhc2hvcC5jb20vIGZvciBtb3JlIGluZm9ybWF0aW9uLlxuICpcbiAqIEBhdXRob3IgICAgUHJlc3RhU2hvcCBTQSBhbmQgQ29udHJpYnV0b3JzIDxjb250YWN0QHByZXN0YXNob3AuY29tPlxuICogQGNvcHlyaWdodCBTaW5jZSAyMDA3IFByZXN0YVNob3AgU0EgYW5kIENvbnRyaWJ1dG9yc1xuICogQGxpY2Vuc2UgICBodHRwczovL29wZW5zb3VyY2Uub3JnL2xpY2Vuc2VzL09TTC0zLjAgT3BlbiBTb2Z0d2FyZSBMaWNlbnNlIChPU0wgMy4wKVxuICovXG5cbmNvbnN0IGNvbWJpbmF0aW9uTGlzdElkID0gJyNjb21iaW5hdGlvbl9saXN0JztcblxuZXhwb3J0IGRlZmF1bHQge1xuICBwcm9kdWN0Rm9ybTogJ2Zvcm1bbmFtZT1wcm9kdWN0XScsXG4gIHByb2R1Y3RUeXBlU2VsZWN0b3I6ICcjcHJvZHVjdF9oZWFkZXJfdHlwZScsXG4gIHByb2R1Y3RUeXBlOiB7XG4gICAgU1RBTkRBUkQ6ICdzdGFuZGFyZCcsXG4gICAgUEFDSzogJ3BhY2snLFxuICAgIFZJUlRVQUw6ICd2aXJ0dWFsJyxcbiAgICBDT01CSU5BVElPTlM6ICdjb21iaW5hdGlvbnMnLFxuICB9LFxuICBpbnZhbGlkRmllbGQ6ICcuaXMtaW52YWxpZCcsXG4gIHByb2R1Y3RGb3JtU3VibWl0QnV0dG9uOiAnLnByb2R1Y3QtZm9ybS1zYXZlLWJ1dHRvbicsXG4gIG5hdmlnYXRpb25CYXI6ICcjZm9ybS1uYXYnLFxuICBkcm9wem9uZUltYWdlc0NvbnRhaW5lcjogJy5wcm9kdWN0LWltYWdlLWRyb3B6b25lJyxcbiAgZmVhdHVyZVZhbHVlczoge1xuICAgIGNvbGxlY3Rpb25Db250YWluZXI6ICcuZmVhdHVyZS12YWx1ZXMtY29sbGVjdGlvbicsXG4gICAgY29sbGVjdGlvblJvd3NDb250YWluZXI6ICcuZmVhdHVyZS12YWx1ZXMtY29sbGVjdGlvbiA+IC5jb2wtc20nLFxuICAgIGNvbGxlY3Rpb25Sb3c6ICdkaXYucm93LnByb2R1Y3QtZmVhdHVyZScsXG4gICAgZmVhdHVyZVNlbGVjdDogJ3NlbGVjdC5mZWF0dXJlLXNlbGVjdG9yJyxcbiAgICBmZWF0dXJlVmFsdWVTZWxlY3Q6ICdzZWxlY3QuZmVhdHVyZS12YWx1ZS1zZWxlY3RvcicsXG4gICAgY3VzdG9tVmFsdWVJbnB1dDogJy5jdXN0b20tdmFsdWVzIGlucHV0JyxcbiAgICBjdXN0b21GZWF0dXJlSWRJbnB1dDogJ2lucHV0LmN1c3RvbS12YWx1ZS1pZCcsXG4gICAgZGVsZXRlRmVhdHVyZVZhbHVlOiAnYnV0dG9uLmRlbGV0ZS1mZWF0dXJlLXZhbHVlJyxcbiAgICBhZGRGZWF0dXJlVmFsdWU6ICcuZmVhdHVyZS12YWx1ZS1hZGQtYnV0dG9uJyxcbiAgfSxcbiAgY3VzdG9taXphdGlvbnM6IHtcbiAgICBjdXN0b21pemF0aW9uc0NvbnRhaW5lcjogJy5wcm9kdWN0LWN1c3RvbWl6YXRpb25zLWNvbGxlY3Rpb24nLFxuICAgIGN1c3RvbWl6YXRpb25GaWVsZHNMaXN0OiAnLnByb2R1Y3QtY3VzdG9taXphdGlvbnMtY29sbGVjdGlvbiB1bCcsXG4gICAgYWRkQ3VzdG9taXphdGlvbkJ0bjogJy5hZGQtY3VzdG9taXphdGlvbi1idG4nLFxuICAgIHJlbW92ZUN1c3RvbWl6YXRpb25CdG46ICcucmVtb3ZlLWN1c3RvbWl6YXRpb24tYnRuJyxcbiAgICBjdXN0b21pemF0aW9uRmllbGRSb3c6ICcuY3VzdG9taXphdGlvbi1maWVsZC1yb3cnLFxuICB9LFxuICBjb21iaW5hdGlvbnM6IHtcbiAgICBuYXZpZ2F0aW9uVGFiOiAnI2NvbWJpbmF0aW9ucy10YWItbmF2JyxcbiAgICBleHRlcm5hbENvbWJpbmF0aW9uVGFiOiAnI2V4dGVybmFsLWNvbWJpbmF0aW9uLXRhYicsXG4gICAgcHJlbG9hZGVyOiAnI2NvbWJpbmF0aW9ucy1wcmVsb2FkZXInLFxuICAgIGVtcHR5U3RhdGU6ICcjY29tYmluYXRpb25zLWVtcHR5LXN0YXRlJyxcbiAgICBjb21iaW5hdGlvbnNQYWdpbmF0ZWRMaXN0OiAnI2NvbWJpbmF0aW9ucy1wYWdpbmF0ZWQtbGlzdCcsXG4gICAgY29tYmluYXRpb25zQ29udGFpbmVyOiBgJHtjb21iaW5hdGlvbkxpc3RJZH1gLFxuICAgIGNvbWJpbmF0aW9uc0ZpbHRlcnNDb250YWluZXI6ICcjY29tYmluYXRpb25zX2ZpbHRlcnMnLFxuICAgIGNvbWJpbmF0aW9uc0dlbmVyYXRvckNvbnRhaW5lcjogJyNwcm9kdWN0X2NvbWJpbmF0aW9uc19nZW5lcmF0b3InLFxuICAgIGNvbWJpbmF0aW9uc1RhYmxlOiBgJHtjb21iaW5hdGlvbkxpc3RJZH0gdGFibGVgLFxuICAgIGNvbWJpbmF0aW9uc1RhYmxlQm9keTogYCR7Y29tYmluYXRpb25MaXN0SWR9IHRhYmxlIHRib2R5YCxcbiAgICBjb21iaW5hdGlvbklkSW5wdXRzU2VsZWN0b3I6ICcuY29tYmluYXRpb24taWQtaW5wdXQnLFxuICAgIGlzRGVmYXVsdElucHV0c1NlbGVjdG9yOiAnLmNvbWJpbmF0aW9uLWlzLWRlZmF1bHQtaW5wdXQnLFxuICAgIHJlbW92ZUNvbWJpbmF0aW9uU2VsZWN0b3I6ICcucmVtb3ZlLWNvbWJpbmF0aW9uLWl0ZW0nLFxuICAgIGNvbWJpbmF0aW9uTmFtZTogJ2Zvcm0gLmNhcmQtaGVhZGVyIHNwYW4nLFxuICAgIHBhZ2luYXRpb25Db250YWluZXI6ICcjY29tYmluYXRpb25zLXBhZ2luYXRpb24nLFxuICAgIGxvYWRpbmdTcGlubmVyOiAnI3Byb2R1Y3RDb21iaW5hdGlvbnNMb2FkaW5nJyxcbiAgICBxdWFudGl0eUlucHV0V3JhcHBlcjogJy5jb21iaW5hdGlvbi1xdWFudGl0eScsXG4gICAgaW1wYWN0T25QcmljZUlucHV0V3JhcHBlcjogJy5jb21iaW5hdGlvbi1pbXBhY3Qtb24tcHJpY2UnLFxuICAgIHJlZmVyZW5jZUlucHV0V3JhcHBlcjogJy5jb21iaW5hdGlvbi1yZWZlcmVuY2UnLFxuICAgIHNvcnRhYmxlQ29sdW1uczogJy5wcy1zb3J0YWJsZS1jb2x1bW4nLFxuICAgIGNvbWJpbmF0aW9uSXRlbUZvcm06IHtcbiAgICAgIHF1YW50aXR5S2V5OiAnY29tYmluYXRpb25faXRlbVtxdWFudGl0eV1bdmFsdWVdJyxcbiAgICAgIGltcGFjdE9uUHJpY2VLZXk6ICdjb21iaW5hdGlvbl9pdGVtW2ltcGFjdF9vbl9wcmljZV1bdmFsdWVdJyxcbiAgICAgIHJlZmVyZW5jZUtleTogJ2NvbWJpbmF0aW9uX2l0ZW1bcmVmZXJlbmNlXVt2YWx1ZV0nLFxuICAgICAgdG9rZW5LZXk6ICdjb21iaW5hdGlvbl9pdGVtW190b2tlbl0nLFxuICAgIH0sXG4gICAgZWRpdGlvbkZvcm06ICdmb3JtW25hbWU9XCJjb21iaW5hdGlvbl9mb3JtXCJdJyxcbiAgICBlZGl0aW9uRm9ybUlucHV0czpcbiAgICAgIC8vIGVzbGludC1kaXNhYmxlLW5leHQtbGluZVxuICAgICAgJ2Zvcm1bbmFtZT1cImNvbWJpbmF0aW9uX2Zvcm1cIl0gaW5wdXQsIGZvcm1bbmFtZT1cImNvbWJpbmF0aW9uX2Zvcm1cIl0gdGV4dGFyZWEsIGZvcm1bbmFtZT1cImNvbWJpbmF0aW9uX2Zvcm1cIl0gc2VsZWN0JyxcbiAgICBlZGl0Q29tYmluYXRpb25CdXR0b25zOiAnLmVkaXQtY29tYmluYXRpb24taXRlbScsXG4gICAgdGFibGVSb3c6IHtcbiAgICAgIGNvbWJpbmF0aW9uSW1nOiAnLmNvbWJpbmF0aW9uLWltYWdlJyxcbiAgICAgIGNvbWJpbmF0aW9uQ2hlY2tib3g6IChyb3dJbmRleCkgPT4gYCR7Y29tYmluYXRpb25MaXN0SWR9X2NvbWJpbmF0aW9uc18ke3Jvd0luZGV4fV9pc19zZWxlY3RlZGAsXG4gICAgICBjb21iaW5hdGlvbklkSW5wdXQ6IChyb3dJbmRleCkgPT4gYCR7Y29tYmluYXRpb25MaXN0SWR9X2NvbWJpbmF0aW9uc18ke3Jvd0luZGV4fV9jb21iaW5hdGlvbl9pZGAsXG4gICAgICBjb21iaW5hdGlvbk5hbWVJbnB1dDogKHJvd0luZGV4KSA9PiBgJHtjb21iaW5hdGlvbkxpc3RJZH1fY29tYmluYXRpb25zXyR7cm93SW5kZXh9X25hbWVgLFxuICAgICAgcmVmZXJlbmNlSW5wdXQ6IChyb3dJbmRleCkgPT4gYCR7Y29tYmluYXRpb25MaXN0SWR9X2NvbWJpbmF0aW9uc18ke3Jvd0luZGV4fV9yZWZlcmVuY2VfdmFsdWVgLFxuICAgICAgaW1wYWN0T25QcmljZUlucHV0OiAocm93SW5kZXgpID0+IGAke2NvbWJpbmF0aW9uTGlzdElkfV9jb21iaW5hdGlvbnNfJHtyb3dJbmRleH1faW1wYWN0X29uX3ByaWNlX3ZhbHVlYCxcbiAgICAgIGZpbmFsUHJpY2VUZUlucHV0OiAocm93SW5kZXgpID0+IGAke2NvbWJpbmF0aW9uTGlzdElkfV9jb21iaW5hdGlvbnNfJHtyb3dJbmRleH1fZmluYWxfcHJpY2VfdGVgLFxuICAgICAgcXVhbnRpdHlJbnB1dDogKHJvd0luZGV4KSA9PiBgJHtjb21iaW5hdGlvbkxpc3RJZH1fY29tYmluYXRpb25zXyR7cm93SW5kZXh9X3F1YW50aXR5X3ZhbHVlYCxcbiAgICAgIGlzRGVmYXVsdElucHV0OiAocm93SW5kZXgpID0+IGAke2NvbWJpbmF0aW9uTGlzdElkfV9jb21iaW5hdGlvbnNfJHtyb3dJbmRleH1faXNfZGVmYXVsdGAsXG4gICAgICBlZGl0QnV0dG9uOiAocm93SW5kZXgpID0+IGAke2NvbWJpbmF0aW9uTGlzdElkfV9jb21iaW5hdGlvbnNfJHtyb3dJbmRleH1fZWRpdGAsXG4gICAgICBkZWxldGVCdXR0b246IChyb3dJbmRleCkgPT4gYCR7Y29tYmluYXRpb25MaXN0SWR9X2NvbWJpbmF0aW9uc18ke3Jvd0luZGV4fV9kZWxldGVgLFxuICAgIH0sXG4gICAgZWRpdE1vZGFsOiAnI2NvbWJpbmF0aW9uLWVkaXQtbW9kYWwnLFxuICAgIGltYWdlczoge1xuICAgICAgc2VsZWN0b3JDb250YWluZXI6ICcuY29tYmluYXRpb24taW1hZ2VzLXNlbGVjdG9yJyxcbiAgICAgIGltYWdlQ2hvaWNlOiAnLmNvbWJpbmF0aW9uLWltYWdlLWNob2ljZScsXG4gICAgICBjaGVja2JveENvbnRhaW5lcjogJy5mb3JtLWNoZWNrJyxcbiAgICAgIGNoZWNrYm94OiAnaW5wdXRbdHlwZT1jaGVja2JveF0nLFxuICAgIH0sXG4gICAgc2Nyb2xsQmFyOiAnLmF0dHJpYnV0ZXMtbGlzdC1vdmVyZmxvdycsXG4gICAgc2VhcmNoSW5wdXQ6ICcjcHJvZHVjdC1jb21iaW5hdGlvbnMtZ2VuZXJhdGUgLmF0dHJpYnV0ZXMtc2VhcmNoJyxcbiAgICBnZW5lcmF0ZUNvbWJpbmF0aW9uc0J1dHRvbjogJy5nZW5lcmF0ZS1jb21iaW5hdGlvbnMtYnV0dG9uJyxcbiAgfSxcbiAgdmlydHVhbFByb2R1Y3Q6IHtcbiAgICBjb250YWluZXI6ICcudmlydHVhbC1wcm9kdWN0LWZpbGUtY29udGFpbmVyJyxcbiAgICBmaWxlQ29udGVudENvbnRhaW5lcjogJy52aXJ0dWFsLXByb2R1Y3QtZmlsZS1jb250ZW50JyxcbiAgfSxcbiAgZHJvcHpvbmU6IHtcbiAgICBjb25maWd1cmF0aW9uOiB7XG4gICAgICBmaWxlTWFuYWdlcjogJy5vcGVuZmlsZW1hbmFnZXInLFxuICAgIH0sXG4gICAgcGhvdG9zd2lwZToge1xuICAgICAgZWxlbWVudDogJy5wc3dwJyxcbiAgICB9LFxuICAgIGR6VGVtcGxhdGU6ICcuZHotdGVtcGxhdGUnLFxuICAgIGR6UHJldmlldzogJy5kei1wcmV2aWV3JyxcbiAgICBzb3J0YWJsZUNvbnRhaW5lcjogJyNwcm9kdWN0LWltYWdlcy1kcm9wem9uZScsXG4gICAgc29ydGFibGVJdGVtczogJ2Rpdi5kei1wcmV2aWV3Om5vdCguZGlzYWJsZWQpJyxcbiAgICBkcm9wem9uZUNvbnRhaW5lcjogJy5kcm9wem9uZS1jb250YWluZXInLFxuICAgIGNoZWNrYm94OiAnLm1kLWNoZWNrYm94IGlucHV0JyxcbiAgICBzaG93blRvb2x0aXBzOiAnLnRvb2x0aXAuc2hvdycsXG4gICAgc2F2ZWRJbWFnZUNvbnRhaW5lcjogKGltYWdlSWQpID0+IGAuZHotcHJldmlld1tkYXRhLWlkPVwiJHtpbWFnZUlkfVwiXWAsXG4gICAgc2F2ZWRJbWFnZTogKGltYWdlSWQpID0+IGAuZHotcHJldmlld1tkYXRhLWlkPVwiJHtpbWFnZUlkfVwiXSBpbWdgLFxuICAgIGNvdmVyZWRQcmV2aWV3OiAnLmR6LXByZXZpZXcuaXMtY292ZXInLFxuICAgIHdpbmRvd0ZpbGVNYW5hZ2VyOiAnLmRyb3B6b25lLXdpbmRvdy1maWxlbWFuYWdlcicsXG4gIH0sXG4gIHN1cHBsaWVyczoge1xuICAgIHByb2R1Y3RTdXBwbGllcnM6ICcjcHJvZHVjdF9vcHRpb25zX3N1cHBsaWVycycsXG4gICAgY29tYmluYXRpb25TdXBwbGllcnM6ICcjY29tYmluYXRpb25fZm9ybV9zdXBwbGllcnMnLFxuICB9LFxuICBzZW86IHtcbiAgICBjb250YWluZXI6ICcjcHJvZHVjdF9zZW9fc2VycCcsXG4gICAgZGVmYXVsdFRpdGxlOiAnLnNlcnAtZGVmYXVsdC10aXRsZTppbnB1dCcsXG4gICAgd2F0Y2hlZFRpdGxlOiAnLnNlcnAtd2F0Y2hlZC10aXRsZTppbnB1dCcsXG4gICAgZGVmYXVsdERlc2NyaXB0aW9uOiAnLnNlcnAtZGVmYXVsdC1kZXNjcmlwdGlvbicsXG4gICAgd2F0Y2hlZERlc2NyaXB0aW9uOiAnLnNlcnAtd2F0Y2hlZC1kZXNjcmlwdGlvbicsXG4gICAgd2F0Y2hlZE1ldGFVcmw6ICcuc2VycC13YXRjaGVkLXVybDppbnB1dCcsXG4gICAgcmVkaXJlY3RPcHRpb246IHtcbiAgICAgIHR5cGVJbnB1dDogJyNwcm9kdWN0X3Nlb19yZWRpcmVjdF9vcHRpb25fdHlwZScsXG4gICAgICB0YXJnZXRJbnB1dDogJyNwcm9kdWN0X3Nlb19yZWRpcmVjdF9vcHRpb25fdGFyZ2V0JyxcbiAgICB9LFxuICB9LFxuICBqc1RhYnM6ICcuanMtdGFicycsXG4gIGpzQXJyb3c6ICcuanMtYXJyb3cnLFxuICBqc05hdlRhYnM6ICcuanMtbmF2LXRhYnMnLFxuICB0b2dnbGVUYWI6ICdbZGF0YS10b2dnbGU9XCJ0YWJcIl0nLFxuICBmb3JtQ29udGVudFRhYjogJyNmb3JtX2NvbnRlbnQgPiAuZm9ybS1jb250ZW50dGFiJyxcbiAgbGVmdEFycm93OiAnLmxlZnQtYXJyb3cnLFxuICByaWdodEFycm93OiAnLnJpZ2h0LWFycm93JyxcbiAgZm9vdGVyOiB7XG4gICAgcHJldmlld1VybEJ1dHRvbjogJy5wcmV2aWV3LXVybC1idXR0b24nLFxuICAgIGRlbGV0ZVByb2R1Y3RCdXR0b246ICcuZGVsZXRlLXByb2R1Y3QtYnV0dG9uJyxcbiAgfSxcbiAgY2F0ZWdvcmllczoge1xuICAgIGNhdGVnb3JpZXNDb250YWluZXI6ICcuanMtY2F0ZWdvcmllcy1jb250YWluZXInLFxuICAgIGNhdGVnb3J5VHJlZTogJy5qcy1jYXRlZ29yaWVzLXRyZWUnLFxuICAgIHRyZWVFbGVtZW50OiAnLmNhdGVnb3J5LXRyZWUtZWxlbWVudCcsXG4gICAgdHJlZUVsZW1lbnRJbnB1dHM6ICcuY2F0ZWdvcnktdHJlZS1pbnB1dHMnLFxuICAgIGNoZWNrYm94SW5wdXQ6ICdbdHlwZT1jaGVja2JveF0nLFxuICAgIGNoZWNrZWRDaGVja2JveElucHV0czogJ1t0eXBlPWNoZWNrYm94XTpjaGVja2VkJyxcbiAgICBjaGVja2JveE5hbWU6IChjYXRlZ29yeUlkKSA9PiBgcHJvZHVjdFtjYXRlZ29yaWVzXVtwcm9kdWN0X2NhdGVnb3JpZXNdWyR7Y2F0ZWdvcnlJZH1dW2lzX2Fzc29jaWF0ZWRdYCxcbiAgICBtYXRlcmlhbENoZWNrYm94OiAnLm1kLWNoZWNrYm94JyxcbiAgICByYWRpb0lucHV0OiAnW3R5cGU9cmFkaW9dJyxcbiAgICBkZWZhdWx0UmFkaW9JbnB1dDogJ1t0eXBlPXJhZGlvXTpjaGVja2VkJyxcbiAgICByYWRpb05hbWU6IChjYXRlZ29yeUlkKSA9PiBgcHJvZHVjdFtjYXRlZ29yaWVzXVtwcm9kdWN0X2NhdGVnb3JpZXNdWyR7Y2F0ZWdvcnlJZH1dW2lzX2RlZmF1bHRdYCxcbiAgICB0YWdzQ29udGFpbmVyOiAnI2NhdGVnb3JpZXMtdGFncy1jb250YWluZXInLFxuICAgIHNlYXJjaElucHV0OiAnI3BzLXNlbGVjdC1wcm9kdWN0LWNhdGVnb3J5JyxcbiAgICBmaWVsZHNldDogJy50cmVlLWZpZWxkc2V0JyxcbiAgICBsb2FkZXI6ICcuY2F0ZWdvcmllcy10cmVlLWxvYWRlcicsXG4gICAgY2hpbGRyZW5MaXN0OiAnLmNoaWxkcmVuLWxpc3QnLFxuICAgIGV2ZXJ5SXRlbXM6ICcubGVzcywgLm1vcmUnLFxuICAgIGV4cGFuZEFsbEJ1dHRvbjogJyNjYXRlZ29yaWVzLXRyZWUtZXhwYW5kJyxcbiAgICByZWR1Y2VBbGxCdXR0b246ICcjY2F0ZWdvcmllcy10cmVlLXJlZHVjZScsXG4gIH0sXG4gIG1vZHVsZXM6IHtcbiAgICBwcmV2aWV3Q29udGFpbmVyOiAnLm1vZHVsZS1yZW5kZXItY29udGFpbmVyLmFsbC1tb2R1bGVzJyxcbiAgICBwcmV2aWV3QnV0dG9uOiAnLm1vZHVsZXMtbGlzdC1idXR0b24nLFxuICAgIHNlbGVjdG9yQ29udGFpbmVyOiAnLm1vZHVsZS1zZWxlY3Rpb24nLFxuICAgIG1vZHVsZVNlbGVjdG9yOiAnLm1vZHVsZXMtbGlzdC1zZWxlY3QnLFxuICAgIHNlbGVjdG9yUHJldmlld3M6ICcubW9kdWxlLXNlbGVjdGlvbiAubW9kdWxlLXJlbmRlci1jb250YWluZXInLFxuICAgIHNlbGVjdG9yUHJldmlldzogKG1vZHVsZUlkKSA9PiBgLm1vZHVsZS1zZWxlY3Rpb24gLm1vZHVsZS1yZW5kZXItY29udGFpbmVyLiR7bW9kdWxlSWR9YCxcbiAgICBjb250ZW50Q29udGFpbmVyOiAnLm1vZHVsZS1jb250ZW50cycsXG4gICAgbW9kdWxlQ29udGVudHM6ICcubW9kdWxlLWNvbnRlbnRzIC5tb2R1bGUtcmVuZGVyLWNvbnRhaW5lcicsXG4gICAgbW9kdWxlQ29udGVudDogKG1vZHVsZUlkKSA9PiBgLm1vZHVsZS1jb250ZW50cyAubW9kdWxlLXJlbmRlci1jb250YWluZXIuJHttb2R1bGVJZH1gLFxuICB9LFxufTtcbiIsIi8qKlxuICogQ29weXJpZ2h0IHNpbmNlIDIwMDcgUHJlc3RhU2hvcCBTQSBhbmQgQ29udHJpYnV0b3JzXG4gKiBQcmVzdGFTaG9wIGlzIGFuIEludGVybmF0aW9uYWwgUmVnaXN0ZXJlZCBUcmFkZW1hcmsgJiBQcm9wZXJ0eSBvZiBQcmVzdGFTaG9wIFNBXG4gKlxuICogTk9USUNFIE9GIExJQ0VOU0VcbiAqXG4gKiBUaGlzIHNvdXJjZSBmaWxlIGlzIHN1YmplY3QgdG8gdGhlIE9wZW4gU29mdHdhcmUgTGljZW5zZSAoT1NMIDMuMClcbiAqIHRoYXQgaXMgYnVuZGxlZCB3aXRoIHRoaXMgcGFja2FnZSBpbiB0aGUgZmlsZSBMSUNFTlNFLm1kLlxuICogSXQgaXMgYWxzbyBhdmFpbGFibGUgdGhyb3VnaCB0aGUgd29ybGQtd2lkZS13ZWIgYXQgdGhpcyBVUkw6XG4gKiBodHRwczovL29wZW5zb3VyY2Uub3JnL2xpY2Vuc2VzL09TTC0zLjBcbiAqIElmIHlvdSBkaWQgbm90IHJlY2VpdmUgYSBjb3B5IG9mIHRoZSBsaWNlbnNlIGFuZCBhcmUgdW5hYmxlIHRvXG4gKiBvYnRhaW4gaXQgdGhyb3VnaCB0aGUgd29ybGQtd2lkZS13ZWIsIHBsZWFzZSBzZW5kIGFuIGVtYWlsXG4gKiB0byBsaWNlbnNlQHByZXN0YXNob3AuY29tIHNvIHdlIGNhbiBzZW5kIHlvdSBhIGNvcHkgaW1tZWRpYXRlbHkuXG4gKlxuICogRElTQ0xBSU1FUlxuICpcbiAqIERvIG5vdCBlZGl0IG9yIGFkZCB0byB0aGlzIGZpbGUgaWYgeW91IHdpc2ggdG8gdXBncmFkZSBQcmVzdGFTaG9wIHRvIG5ld2VyXG4gKiB2ZXJzaW9ucyBpbiB0aGUgZnV0dXJlLiBJZiB5b3Ugd2lzaCB0byBjdXN0b21pemUgUHJlc3RhU2hvcCBmb3IgeW91clxuICogbmVlZHMgcGxlYXNlIHJlZmVyIHRvIGh0dHBzOi8vZGV2ZG9jcy5wcmVzdGFzaG9wLmNvbS8gZm9yIG1vcmUgaW5mb3JtYXRpb24uXG4gKlxuICogQGF1dGhvciAgICBQcmVzdGFTaG9wIFNBIGFuZCBDb250cmlidXRvcnMgPGNvbnRhY3RAcHJlc3Rhc2hvcC5jb20+XG4gKiBAY29weXJpZ2h0IFNpbmNlIDIwMDcgUHJlc3RhU2hvcCBTQSBhbmQgQ29udHJpYnV0b3JzXG4gKiBAbGljZW5zZSAgIGh0dHBzOi8vb3BlbnNvdXJjZS5vcmcvbGljZW5zZXMvT1NMLTMuMCBPcGVuIFNvZnR3YXJlIExpY2Vuc2UgKE9TTCAzLjApXG4gKi9cblxuZXhwb3J0IGRlZmF1bHQgKHN1cHBsaWVyc0Zvcm1JZCkgPT4ge1xuICBjb25zdCBwcm9kdWN0U3VwcGxpZXJzSWQgPSBgJHtzdXBwbGllcnNGb3JtSWR9X3Byb2R1Y3Rfc3VwcGxpZXJzYDtcbiAgY29uc3QgcHJvZHVjdFN1cHBsaWVySW5wdXRJZCA9IChzdXBwbGllckluZGV4LCBpbnB1dE5hbWUpID0+IGAke3Byb2R1Y3RTdXBwbGllcnNJZH1fJHtzdXBwbGllckluZGV4fV8ke2lucHV0TmFtZX1gO1xuXG4gIHJldHVybiB7XG4gICAgcHJvZHVjdFN1cHBsaWVyc0NvbGxlY3Rpb246IGAke3Byb2R1Y3RTdXBwbGllcnNJZH1gLFxuICAgIHN1cHBsaWVySWRzSW5wdXQ6IGAke3N1cHBsaWVyc0Zvcm1JZH1fc3VwcGxpZXJfaWRzYCxcbiAgICBkZWZhdWx0U3VwcGxpZXJJbnB1dDogYCR7c3VwcGxpZXJzRm9ybUlkfV9kZWZhdWx0X3N1cHBsaWVyX2lkYCxcbiAgICBwcm9kdWN0U3VwcGxpZXJzVGFibGU6IGAke3Byb2R1Y3RTdXBwbGllcnNJZH0gdGFibGVgLFxuICAgIHByb2R1Y3RzU3VwcGxpZXJzVGFibGVCb2R5OiBgJHtwcm9kdWN0U3VwcGxpZXJzSWR9IHRhYmxlIHRib2R5YCxcbiAgICBkZWZhdWx0U3VwcGxpZXJDbGFzczogJ2RlZmF1bHQtc3VwcGxpZXInLFxuICAgIHByb2R1Y3RTdXBwbGllclJvdzoge1xuICAgICAgc3VwcGxpZXJJZElucHV0OiAoc3VwcGxpZXJJbmRleCkgPT4gcHJvZHVjdFN1cHBsaWVySW5wdXRJZChzdXBwbGllckluZGV4LCAnc3VwcGxpZXJfaWQnKSxcbiAgICAgIHN1cHBsaWVyTmFtZUlucHV0OiAoc3VwcGxpZXJJbmRleCkgPT4gcHJvZHVjdFN1cHBsaWVySW5wdXRJZChzdXBwbGllckluZGV4LCAnc3VwcGxpZXJfbmFtZScpLFxuICAgICAgcHJvZHVjdFN1cHBsaWVySWRJbnB1dDogKHN1cHBsaWVySW5kZXgpID0+IHByb2R1Y3RTdXBwbGllcklucHV0SWQoc3VwcGxpZXJJbmRleCwgJ3Byb2R1Y3Rfc3VwcGxpZXJfaWQnKSxcbiAgICAgIHJlZmVyZW5jZUlucHV0OiAoc3VwcGxpZXJJbmRleCkgPT4gcHJvZHVjdFN1cHBsaWVySW5wdXRJZChzdXBwbGllckluZGV4LCAncmVmZXJlbmNlJyksXG4gICAgICBwcmljZUlucHV0OiAoc3VwcGxpZXJJbmRleCkgPT4gcHJvZHVjdFN1cHBsaWVySW5wdXRJZChzdXBwbGllckluZGV4LCAncHJpY2VfdGF4X2V4Y2x1ZGVkJyksXG4gICAgICBjdXJyZW5jeUlkSW5wdXQ6IChzdXBwbGllckluZGV4KSA9PiBwcm9kdWN0U3VwcGxpZXJJbnB1dElkKHN1cHBsaWVySW5kZXgsICdjdXJyZW5jeV9pZCcpLFxuICAgICAgc3VwcGxpZXJOYW1lUHJldmlldzogKHN1cHBsaWVySW5kZXgpID0+IGAjcHJvZHVjdF9zdXBwbGllcl9yb3dfJHtzdXBwbGllckluZGV4fSAuc3VwcGxpZXJfbmFtZSAucHJldmlld2AsXG4gICAgfSxcbiAgICBjaGVja2JveENvbnRhaW5lcjogJy5mb3JtLWNoZWNrJyxcbiAgfTtcbn07XG4iLCJtb2R1bGUuZXhwb3J0cyA9IHsgXCJkZWZhdWx0XCI6IHJlcXVpcmUoXCJjb3JlLWpzL2xpYnJhcnkvZm4vb2JqZWN0L2NyZWF0ZVwiKSwgX19lc01vZHVsZTogdHJ1ZSB9OyIsIm1vZHVsZS5leHBvcnRzID0geyBcImRlZmF1bHRcIjogcmVxdWlyZShcImNvcmUtanMvbGlicmFyeS9mbi9vYmplY3QvZGVmaW5lLXByb3BlcnR5XCIpLCBfX2VzTW9kdWxlOiB0cnVlIH07IiwiXCJ1c2Ugc3RyaWN0XCI7XG5cbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IGZ1bmN0aW9uIChpbnN0YW5jZSwgQ29uc3RydWN0b3IpIHtcbiAgaWYgKCEoaW5zdGFuY2UgaW5zdGFuY2VvZiBDb25zdHJ1Y3RvcikpIHtcbiAgICB0aHJvdyBuZXcgVHlwZUVycm9yKFwiQ2Fubm90IGNhbGwgYSBjbGFzcyBhcyBhIGZ1bmN0aW9uXCIpO1xuICB9XG59OyIsIlwidXNlIHN0cmljdFwiO1xuXG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xuXG52YXIgX2RlZmluZVByb3BlcnR5ID0gcmVxdWlyZShcIi4uL2NvcmUtanMvb2JqZWN0L2RlZmluZS1wcm9wZXJ0eVwiKTtcblxudmFyIF9kZWZpbmVQcm9wZXJ0eTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9kZWZpbmVQcm9wZXJ0eSk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbmV4cG9ydHMuZGVmYXVsdCA9IGZ1bmN0aW9uICgpIHtcbiAgZnVuY3Rpb24gZGVmaW5lUHJvcGVydGllcyh0YXJnZXQsIHByb3BzKSB7XG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBwcm9wcy5sZW5ndGg7IGkrKykge1xuICAgICAgdmFyIGRlc2NyaXB0b3IgPSBwcm9wc1tpXTtcbiAgICAgIGRlc2NyaXB0b3IuZW51bWVyYWJsZSA9IGRlc2NyaXB0b3IuZW51bWVyYWJsZSB8fCBmYWxzZTtcbiAgICAgIGRlc2NyaXB0b3IuY29uZmlndXJhYmxlID0gdHJ1ZTtcbiAgICAgIGlmIChcInZhbHVlXCIgaW4gZGVzY3JpcHRvcikgZGVzY3JpcHRvci53cml0YWJsZSA9IHRydWU7XG4gICAgICAoMCwgX2RlZmluZVByb3BlcnR5Mi5kZWZhdWx0KSh0YXJnZXQsIGRlc2NyaXB0b3Iua2V5LCBkZXNjcmlwdG9yKTtcbiAgICB9XG4gIH1cblxuICByZXR1cm4gZnVuY3Rpb24gKENvbnN0cnVjdG9yLCBwcm90b1Byb3BzLCBzdGF0aWNQcm9wcykge1xuICAgIGlmIChwcm90b1Byb3BzKSBkZWZpbmVQcm9wZXJ0aWVzKENvbnN0cnVjdG9yLnByb3RvdHlwZSwgcHJvdG9Qcm9wcyk7XG4gICAgaWYgKHN0YXRpY1Byb3BzKSBkZWZpbmVQcm9wZXJ0aWVzKENvbnN0cnVjdG9yLCBzdGF0aWNQcm9wcyk7XG4gICAgcmV0dXJuIENvbnN0cnVjdG9yO1xuICB9O1xufSgpOyIsInJlcXVpcmUoJy4uLy4uL21vZHVsZXMvZXM2Lm9iamVjdC5jcmVhdGUnKTtcbnZhciAkT2JqZWN0ID0gcmVxdWlyZSgnLi4vLi4vbW9kdWxlcy9fY29yZScpLk9iamVjdDtcbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gY3JlYXRlKFAsIEQpIHtcbiAgcmV0dXJuICRPYmplY3QuY3JlYXRlKFAsIEQpO1xufTtcbiIsInJlcXVpcmUoJy4uLy4uL21vZHVsZXMvZXM2Lm9iamVjdC5kZWZpbmUtcHJvcGVydHknKTtcbnZhciAkT2JqZWN0ID0gcmVxdWlyZSgnLi4vLi4vbW9kdWxlcy9fY29yZScpLk9iamVjdDtcbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gZGVmaW5lUHJvcGVydHkoaXQsIGtleSwgZGVzYykge1xuICByZXR1cm4gJE9iamVjdC5kZWZpbmVQcm9wZXJ0eShpdCwga2V5LCBkZXNjKTtcbn07XG4iLCJtb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIChpdCkge1xuICBpZiAodHlwZW9mIGl0ICE9ICdmdW5jdGlvbicpIHRocm93IFR5cGVFcnJvcihpdCArICcgaXMgbm90IGEgZnVuY3Rpb24hJyk7XG4gIHJldHVybiBpdDtcbn07XG4iLCJ2YXIgaXNPYmplY3QgPSByZXF1aXJlKCcuL19pcy1vYmplY3QnKTtcbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKGl0KSB7XG4gIGlmICghaXNPYmplY3QoaXQpKSB0aHJvdyBUeXBlRXJyb3IoaXQgKyAnIGlzIG5vdCBhbiBvYmplY3QhJyk7XG4gIHJldHVybiBpdDtcbn07XG4iLCIvLyBmYWxzZSAtPiBBcnJheSNpbmRleE9mXG4vLyB0cnVlICAtPiBBcnJheSNpbmNsdWRlc1xudmFyIHRvSU9iamVjdCA9IHJlcXVpcmUoJy4vX3RvLWlvYmplY3QnKTtcbnZhciB0b0xlbmd0aCA9IHJlcXVpcmUoJy4vX3RvLWxlbmd0aCcpO1xudmFyIHRvQWJzb2x1dGVJbmRleCA9IHJlcXVpcmUoJy4vX3RvLWFic29sdXRlLWluZGV4Jyk7XG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIChJU19JTkNMVURFUykge1xuICByZXR1cm4gZnVuY3Rpb24gKCR0aGlzLCBlbCwgZnJvbUluZGV4KSB7XG4gICAgdmFyIE8gPSB0b0lPYmplY3QoJHRoaXMpO1xuICAgIHZhciBsZW5ndGggPSB0b0xlbmd0aChPLmxlbmd0aCk7XG4gICAgdmFyIGluZGV4ID0gdG9BYnNvbHV0ZUluZGV4KGZyb21JbmRleCwgbGVuZ3RoKTtcbiAgICB2YXIgdmFsdWU7XG4gICAgLy8gQXJyYXkjaW5jbHVkZXMgdXNlcyBTYW1lVmFsdWVaZXJvIGVxdWFsaXR5IGFsZ29yaXRobVxuICAgIC8vIGVzbGludC1kaXNhYmxlLW5leHQtbGluZSBuby1zZWxmLWNvbXBhcmVcbiAgICBpZiAoSVNfSU5DTFVERVMgJiYgZWwgIT0gZWwpIHdoaWxlIChsZW5ndGggPiBpbmRleCkge1xuICAgICAgdmFsdWUgPSBPW2luZGV4KytdO1xuICAgICAgLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lIG5vLXNlbGYtY29tcGFyZVxuICAgICAgaWYgKHZhbHVlICE9IHZhbHVlKSByZXR1cm4gdHJ1ZTtcbiAgICAvLyBBcnJheSNpbmRleE9mIGlnbm9yZXMgaG9sZXMsIEFycmF5I2luY2x1ZGVzIC0gbm90XG4gICAgfSBlbHNlIGZvciAoO2xlbmd0aCA+IGluZGV4OyBpbmRleCsrKSBpZiAoSVNfSU5DTFVERVMgfHwgaW5kZXggaW4gTykge1xuICAgICAgaWYgKE9baW5kZXhdID09PSBlbCkgcmV0dXJuIElTX0lOQ0xVREVTIHx8IGluZGV4IHx8IDA7XG4gICAgfSByZXR1cm4gIUlTX0lOQ0xVREVTICYmIC0xO1xuICB9O1xufTtcbiIsInZhciB0b1N0cmluZyA9IHt9LnRvU3RyaW5nO1xuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIChpdCkge1xuICByZXR1cm4gdG9TdHJpbmcuY2FsbChpdCkuc2xpY2UoOCwgLTEpO1xufTtcbiIsInZhciBjb3JlID0gbW9kdWxlLmV4cG9ydHMgPSB7IHZlcnNpb246ICcyLjYuMTEnIH07XG5pZiAodHlwZW9mIF9fZSA9PSAnbnVtYmVyJykgX19lID0gY29yZTsgLy8gZXNsaW50LWRpc2FibGUtbGluZSBuby11bmRlZlxuIiwiLy8gb3B0aW9uYWwgLyBzaW1wbGUgY29udGV4dCBiaW5kaW5nXG52YXIgYUZ1bmN0aW9uID0gcmVxdWlyZSgnLi9fYS1mdW5jdGlvbicpO1xubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAoZm4sIHRoYXQsIGxlbmd0aCkge1xuICBhRnVuY3Rpb24oZm4pO1xuICBpZiAodGhhdCA9PT0gdW5kZWZpbmVkKSByZXR1cm4gZm47XG4gIHN3aXRjaCAobGVuZ3RoKSB7XG4gICAgY2FzZSAxOiByZXR1cm4gZnVuY3Rpb24gKGEpIHtcbiAgICAgIHJldHVybiBmbi5jYWxsKHRoYXQsIGEpO1xuICAgIH07XG4gICAgY2FzZSAyOiByZXR1cm4gZnVuY3Rpb24gKGEsIGIpIHtcbiAgICAgIHJldHVybiBmbi5jYWxsKHRoYXQsIGEsIGIpO1xuICAgIH07XG4gICAgY2FzZSAzOiByZXR1cm4gZnVuY3Rpb24gKGEsIGIsIGMpIHtcbiAgICAgIHJldHVybiBmbi5jYWxsKHRoYXQsIGEsIGIsIGMpO1xuICAgIH07XG4gIH1cbiAgcmV0dXJuIGZ1bmN0aW9uICgvKiAuLi5hcmdzICovKSB7XG4gICAgcmV0dXJuIGZuLmFwcGx5KHRoYXQsIGFyZ3VtZW50cyk7XG4gIH07XG59O1xuIiwiLy8gNy4yLjEgUmVxdWlyZU9iamVjdENvZXJjaWJsZShhcmd1bWVudClcbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKGl0KSB7XG4gIGlmIChpdCA9PSB1bmRlZmluZWQpIHRocm93IFR5cGVFcnJvcihcIkNhbid0IGNhbGwgbWV0aG9kIG9uICBcIiArIGl0KTtcbiAgcmV0dXJuIGl0O1xufTtcbiIsIi8vIFRoYW5rJ3MgSUU4IGZvciBoaXMgZnVubnkgZGVmaW5lUHJvcGVydHlcbm1vZHVsZS5leHBvcnRzID0gIXJlcXVpcmUoJy4vX2ZhaWxzJykoZnVuY3Rpb24gKCkge1xuICByZXR1cm4gT2JqZWN0LmRlZmluZVByb3BlcnR5KHt9LCAnYScsIHsgZ2V0OiBmdW5jdGlvbiAoKSB7IHJldHVybiA3OyB9IH0pLmEgIT0gNztcbn0pO1xuIiwidmFyIGlzT2JqZWN0ID0gcmVxdWlyZSgnLi9faXMtb2JqZWN0Jyk7XG52YXIgZG9jdW1lbnQgPSByZXF1aXJlKCcuL19nbG9iYWwnKS5kb2N1bWVudDtcbi8vIHR5cGVvZiBkb2N1bWVudC5jcmVhdGVFbGVtZW50IGlzICdvYmplY3QnIGluIG9sZCBJRVxudmFyIGlzID0gaXNPYmplY3QoZG9jdW1lbnQpICYmIGlzT2JqZWN0KGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQpO1xubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAoaXQpIHtcbiAgcmV0dXJuIGlzID8gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChpdCkgOiB7fTtcbn07XG4iLCIvLyBJRSA4LSBkb24ndCBlbnVtIGJ1ZyBrZXlzXG5tb2R1bGUuZXhwb3J0cyA9IChcbiAgJ2NvbnN0cnVjdG9yLGhhc093blByb3BlcnR5LGlzUHJvdG90eXBlT2YscHJvcGVydHlJc0VudW1lcmFibGUsdG9Mb2NhbGVTdHJpbmcsdG9TdHJpbmcsdmFsdWVPZidcbikuc3BsaXQoJywnKTtcbiIsInZhciBnbG9iYWwgPSByZXF1aXJlKCcuL19nbG9iYWwnKTtcbnZhciBjb3JlID0gcmVxdWlyZSgnLi9fY29yZScpO1xudmFyIGN0eCA9IHJlcXVpcmUoJy4vX2N0eCcpO1xudmFyIGhpZGUgPSByZXF1aXJlKCcuL19oaWRlJyk7XG52YXIgaGFzID0gcmVxdWlyZSgnLi9faGFzJyk7XG52YXIgUFJPVE9UWVBFID0gJ3Byb3RvdHlwZSc7XG5cbnZhciAkZXhwb3J0ID0gZnVuY3Rpb24gKHR5cGUsIG5hbWUsIHNvdXJjZSkge1xuICB2YXIgSVNfRk9SQ0VEID0gdHlwZSAmICRleHBvcnQuRjtcbiAgdmFyIElTX0dMT0JBTCA9IHR5cGUgJiAkZXhwb3J0Lkc7XG4gIHZhciBJU19TVEFUSUMgPSB0eXBlICYgJGV4cG9ydC5TO1xuICB2YXIgSVNfUFJPVE8gPSB0eXBlICYgJGV4cG9ydC5QO1xuICB2YXIgSVNfQklORCA9IHR5cGUgJiAkZXhwb3J0LkI7XG4gIHZhciBJU19XUkFQID0gdHlwZSAmICRleHBvcnQuVztcbiAgdmFyIGV4cG9ydHMgPSBJU19HTE9CQUwgPyBjb3JlIDogY29yZVtuYW1lXSB8fCAoY29yZVtuYW1lXSA9IHt9KTtcbiAgdmFyIGV4cFByb3RvID0gZXhwb3J0c1tQUk9UT1RZUEVdO1xuICB2YXIgdGFyZ2V0ID0gSVNfR0xPQkFMID8gZ2xvYmFsIDogSVNfU1RBVElDID8gZ2xvYmFsW25hbWVdIDogKGdsb2JhbFtuYW1lXSB8fCB7fSlbUFJPVE9UWVBFXTtcbiAgdmFyIGtleSwgb3duLCBvdXQ7XG4gIGlmIChJU19HTE9CQUwpIHNvdXJjZSA9IG5hbWU7XG4gIGZvciAoa2V5IGluIHNvdXJjZSkge1xuICAgIC8vIGNvbnRhaW5zIGluIG5hdGl2ZVxuICAgIG93biA9ICFJU19GT1JDRUQgJiYgdGFyZ2V0ICYmIHRhcmdldFtrZXldICE9PSB1bmRlZmluZWQ7XG4gICAgaWYgKG93biAmJiBoYXMoZXhwb3J0cywga2V5KSkgY29udGludWU7XG4gICAgLy8gZXhwb3J0IG5hdGl2ZSBvciBwYXNzZWRcbiAgICBvdXQgPSBvd24gPyB0YXJnZXRba2V5XSA6IHNvdXJjZVtrZXldO1xuICAgIC8vIHByZXZlbnQgZ2xvYmFsIHBvbGx1dGlvbiBmb3IgbmFtZXNwYWNlc1xuICAgIGV4cG9ydHNba2V5XSA9IElTX0dMT0JBTCAmJiB0eXBlb2YgdGFyZ2V0W2tleV0gIT0gJ2Z1bmN0aW9uJyA/IHNvdXJjZVtrZXldXG4gICAgLy8gYmluZCB0aW1lcnMgdG8gZ2xvYmFsIGZvciBjYWxsIGZyb20gZXhwb3J0IGNvbnRleHRcbiAgICA6IElTX0JJTkQgJiYgb3duID8gY3R4KG91dCwgZ2xvYmFsKVxuICAgIC8vIHdyYXAgZ2xvYmFsIGNvbnN0cnVjdG9ycyBmb3IgcHJldmVudCBjaGFuZ2UgdGhlbSBpbiBsaWJyYXJ5XG4gICAgOiBJU19XUkFQICYmIHRhcmdldFtrZXldID09IG91dCA/IChmdW5jdGlvbiAoQykge1xuICAgICAgdmFyIEYgPSBmdW5jdGlvbiAoYSwgYiwgYykge1xuICAgICAgICBpZiAodGhpcyBpbnN0YW5jZW9mIEMpIHtcbiAgICAgICAgICBzd2l0Y2ggKGFyZ3VtZW50cy5sZW5ndGgpIHtcbiAgICAgICAgICAgIGNhc2UgMDogcmV0dXJuIG5ldyBDKCk7XG4gICAgICAgICAgICBjYXNlIDE6IHJldHVybiBuZXcgQyhhKTtcbiAgICAgICAgICAgIGNhc2UgMjogcmV0dXJuIG5ldyBDKGEsIGIpO1xuICAgICAgICAgIH0gcmV0dXJuIG5ldyBDKGEsIGIsIGMpO1xuICAgICAgICB9IHJldHVybiBDLmFwcGx5KHRoaXMsIGFyZ3VtZW50cyk7XG4gICAgICB9O1xuICAgICAgRltQUk9UT1RZUEVdID0gQ1tQUk9UT1RZUEVdO1xuICAgICAgcmV0dXJuIEY7XG4gICAgLy8gbWFrZSBzdGF0aWMgdmVyc2lvbnMgZm9yIHByb3RvdHlwZSBtZXRob2RzXG4gICAgfSkob3V0KSA6IElTX1BST1RPICYmIHR5cGVvZiBvdXQgPT0gJ2Z1bmN0aW9uJyA/IGN0eChGdW5jdGlvbi5jYWxsLCBvdXQpIDogb3V0O1xuICAgIC8vIGV4cG9ydCBwcm90byBtZXRob2RzIHRvIGNvcmUuJUNPTlNUUlVDVE9SJS5tZXRob2RzLiVOQU1FJVxuICAgIGlmIChJU19QUk9UTykge1xuICAgICAgKGV4cG9ydHMudmlydHVhbCB8fCAoZXhwb3J0cy52aXJ0dWFsID0ge30pKVtrZXldID0gb3V0O1xuICAgICAgLy8gZXhwb3J0IHByb3RvIG1ldGhvZHMgdG8gY29yZS4lQ09OU1RSVUNUT1IlLnByb3RvdHlwZS4lTkFNRSVcbiAgICAgIGlmICh0eXBlICYgJGV4cG9ydC5SICYmIGV4cFByb3RvICYmICFleHBQcm90b1trZXldKSBoaWRlKGV4cFByb3RvLCBrZXksIG91dCk7XG4gICAgfVxuICB9XG59O1xuLy8gdHlwZSBiaXRtYXBcbiRleHBvcnQuRiA9IDE7ICAgLy8gZm9yY2VkXG4kZXhwb3J0LkcgPSAyOyAgIC8vIGdsb2JhbFxuJGV4cG9ydC5TID0gNDsgICAvLyBzdGF0aWNcbiRleHBvcnQuUCA9IDg7ICAgLy8gcHJvdG9cbiRleHBvcnQuQiA9IDE2OyAgLy8gYmluZFxuJGV4cG9ydC5XID0gMzI7ICAvLyB3cmFwXG4kZXhwb3J0LlUgPSA2NDsgIC8vIHNhZmVcbiRleHBvcnQuUiA9IDEyODsgLy8gcmVhbCBwcm90byBtZXRob2QgZm9yIGBsaWJyYXJ5YFxubW9kdWxlLmV4cG9ydHMgPSAkZXhwb3J0O1xuIiwibW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAoZXhlYykge1xuICB0cnkge1xuICAgIHJldHVybiAhIWV4ZWMoKTtcbiAgfSBjYXRjaCAoZSkge1xuICAgIHJldHVybiB0cnVlO1xuICB9XG59O1xuIiwiLy8gaHR0cHM6Ly9naXRodWIuY29tL3psb2lyb2NrL2NvcmUtanMvaXNzdWVzLzg2I2lzc3VlY29tbWVudC0xMTU3NTkwMjhcbnZhciBnbG9iYWwgPSBtb2R1bGUuZXhwb3J0cyA9IHR5cGVvZiB3aW5kb3cgIT0gJ3VuZGVmaW5lZCcgJiYgd2luZG93Lk1hdGggPT0gTWF0aFxuICA/IHdpbmRvdyA6IHR5cGVvZiBzZWxmICE9ICd1bmRlZmluZWQnICYmIHNlbGYuTWF0aCA9PSBNYXRoID8gc2VsZlxuICAvLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmUgbm8tbmV3LWZ1bmNcbiAgOiBGdW5jdGlvbigncmV0dXJuIHRoaXMnKSgpO1xuaWYgKHR5cGVvZiBfX2cgPT0gJ251bWJlcicpIF9fZyA9IGdsb2JhbDsgLy8gZXNsaW50LWRpc2FibGUtbGluZSBuby11bmRlZlxuIiwidmFyIGhhc093blByb3BlcnR5ID0ge30uaGFzT3duUHJvcGVydHk7XG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIChpdCwga2V5KSB7XG4gIHJldHVybiBoYXNPd25Qcm9wZXJ0eS5jYWxsKGl0LCBrZXkpO1xufTtcbiIsInZhciBkUCA9IHJlcXVpcmUoJy4vX29iamVjdC1kcCcpO1xudmFyIGNyZWF0ZURlc2MgPSByZXF1aXJlKCcuL19wcm9wZXJ0eS1kZXNjJyk7XG5tb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoJy4vX2Rlc2NyaXB0b3JzJykgPyBmdW5jdGlvbiAob2JqZWN0LCBrZXksIHZhbHVlKSB7XG4gIHJldHVybiBkUC5mKG9iamVjdCwga2V5LCBjcmVhdGVEZXNjKDEsIHZhbHVlKSk7XG59IDogZnVuY3Rpb24gKG9iamVjdCwga2V5LCB2YWx1ZSkge1xuICBvYmplY3Rba2V5XSA9IHZhbHVlO1xuICByZXR1cm4gb2JqZWN0O1xufTtcbiIsInZhciBkb2N1bWVudCA9IHJlcXVpcmUoJy4vX2dsb2JhbCcpLmRvY3VtZW50O1xubW9kdWxlLmV4cG9ydHMgPSBkb2N1bWVudCAmJiBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQ7XG4iLCJtb2R1bGUuZXhwb3J0cyA9ICFyZXF1aXJlKCcuL19kZXNjcmlwdG9ycycpICYmICFyZXF1aXJlKCcuL19mYWlscycpKGZ1bmN0aW9uICgpIHtcbiAgcmV0dXJuIE9iamVjdC5kZWZpbmVQcm9wZXJ0eShyZXF1aXJlKCcuL19kb20tY3JlYXRlJykoJ2RpdicpLCAnYScsIHsgZ2V0OiBmdW5jdGlvbiAoKSB7IHJldHVybiA3OyB9IH0pLmEgIT0gNztcbn0pO1xuIiwiLy8gZmFsbGJhY2sgZm9yIG5vbi1hcnJheS1saWtlIEVTMyBhbmQgbm9uLWVudW1lcmFibGUgb2xkIFY4IHN0cmluZ3NcbnZhciBjb2YgPSByZXF1aXJlKCcuL19jb2YnKTtcbi8vIGVzbGludC1kaXNhYmxlLW5leHQtbGluZSBuby1wcm90b3R5cGUtYnVpbHRpbnNcbm1vZHVsZS5leHBvcnRzID0gT2JqZWN0KCd6JykucHJvcGVydHlJc0VudW1lcmFibGUoMCkgPyBPYmplY3QgOiBmdW5jdGlvbiAoaXQpIHtcbiAgcmV0dXJuIGNvZihpdCkgPT0gJ1N0cmluZycgPyBpdC5zcGxpdCgnJykgOiBPYmplY3QoaXQpO1xufTtcbiIsIm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKGl0KSB7XG4gIHJldHVybiB0eXBlb2YgaXQgPT09ICdvYmplY3QnID8gaXQgIT09IG51bGwgOiB0eXBlb2YgaXQgPT09ICdmdW5jdGlvbic7XG59O1xuIiwibW9kdWxlLmV4cG9ydHMgPSB0cnVlO1xuIiwiLy8gMTkuMS4yLjIgLyAxNS4yLjMuNSBPYmplY3QuY3JlYXRlKE8gWywgUHJvcGVydGllc10pXG52YXIgYW5PYmplY3QgPSByZXF1aXJlKCcuL19hbi1vYmplY3QnKTtcbnZhciBkUHMgPSByZXF1aXJlKCcuL19vYmplY3QtZHBzJyk7XG52YXIgZW51bUJ1Z0tleXMgPSByZXF1aXJlKCcuL19lbnVtLWJ1Zy1rZXlzJyk7XG52YXIgSUVfUFJPVE8gPSByZXF1aXJlKCcuL19zaGFyZWQta2V5JykoJ0lFX1BST1RPJyk7XG52YXIgRW1wdHkgPSBmdW5jdGlvbiAoKSB7IC8qIGVtcHR5ICovIH07XG52YXIgUFJPVE9UWVBFID0gJ3Byb3RvdHlwZSc7XG5cbi8vIENyZWF0ZSBvYmplY3Qgd2l0aCBmYWtlIGBudWxsYCBwcm90b3R5cGU6IHVzZSBpZnJhbWUgT2JqZWN0IHdpdGggY2xlYXJlZCBwcm90b3R5cGVcbnZhciBjcmVhdGVEaWN0ID0gZnVuY3Rpb24gKCkge1xuICAvLyBUaHJhc2gsIHdhc3RlIGFuZCBzb2RvbXk6IElFIEdDIGJ1Z1xuICB2YXIgaWZyYW1lID0gcmVxdWlyZSgnLi9fZG9tLWNyZWF0ZScpKCdpZnJhbWUnKTtcbiAgdmFyIGkgPSBlbnVtQnVnS2V5cy5sZW5ndGg7XG4gIHZhciBsdCA9ICc8JztcbiAgdmFyIGd0ID0gJz4nO1xuICB2YXIgaWZyYW1lRG9jdW1lbnQ7XG4gIGlmcmFtZS5zdHlsZS5kaXNwbGF5ID0gJ25vbmUnO1xuICByZXF1aXJlKCcuL19odG1sJykuYXBwZW5kQ2hpbGQoaWZyYW1lKTtcbiAgaWZyYW1lLnNyYyA9ICdqYXZhc2NyaXB0Oic7IC8vIGVzbGludC1kaXNhYmxlLWxpbmUgbm8tc2NyaXB0LXVybFxuICAvLyBjcmVhdGVEaWN0ID0gaWZyYW1lLmNvbnRlbnRXaW5kb3cuT2JqZWN0O1xuICAvLyBodG1sLnJlbW92ZUNoaWxkKGlmcmFtZSk7XG4gIGlmcmFtZURvY3VtZW50ID0gaWZyYW1lLmNvbnRlbnRXaW5kb3cuZG9jdW1lbnQ7XG4gIGlmcmFtZURvY3VtZW50Lm9wZW4oKTtcbiAgaWZyYW1lRG9jdW1lbnQud3JpdGUobHQgKyAnc2NyaXB0JyArIGd0ICsgJ2RvY3VtZW50LkY9T2JqZWN0JyArIGx0ICsgJy9zY3JpcHQnICsgZ3QpO1xuICBpZnJhbWVEb2N1bWVudC5jbG9zZSgpO1xuICBjcmVhdGVEaWN0ID0gaWZyYW1lRG9jdW1lbnQuRjtcbiAgd2hpbGUgKGktLSkgZGVsZXRlIGNyZWF0ZURpY3RbUFJPVE9UWVBFXVtlbnVtQnVnS2V5c1tpXV07XG4gIHJldHVybiBjcmVhdGVEaWN0KCk7XG59O1xuXG5tb2R1bGUuZXhwb3J0cyA9IE9iamVjdC5jcmVhdGUgfHwgZnVuY3Rpb24gY3JlYXRlKE8sIFByb3BlcnRpZXMpIHtcbiAgdmFyIHJlc3VsdDtcbiAgaWYgKE8gIT09IG51bGwpIHtcbiAgICBFbXB0eVtQUk9UT1RZUEVdID0gYW5PYmplY3QoTyk7XG4gICAgcmVzdWx0ID0gbmV3IEVtcHR5KCk7XG4gICAgRW1wdHlbUFJPVE9UWVBFXSA9IG51bGw7XG4gICAgLy8gYWRkIFwiX19wcm90b19fXCIgZm9yIE9iamVjdC5nZXRQcm90b3R5cGVPZiBwb2x5ZmlsbFxuICAgIHJlc3VsdFtJRV9QUk9UT10gPSBPO1xuICB9IGVsc2UgcmVzdWx0ID0gY3JlYXRlRGljdCgpO1xuICByZXR1cm4gUHJvcGVydGllcyA9PT0gdW5kZWZpbmVkID8gcmVzdWx0IDogZFBzKHJlc3VsdCwgUHJvcGVydGllcyk7XG59O1xuIiwidmFyIGFuT2JqZWN0ID0gcmVxdWlyZSgnLi9fYW4tb2JqZWN0Jyk7XG52YXIgSUU4X0RPTV9ERUZJTkUgPSByZXF1aXJlKCcuL19pZTgtZG9tLWRlZmluZScpO1xudmFyIHRvUHJpbWl0aXZlID0gcmVxdWlyZSgnLi9fdG8tcHJpbWl0aXZlJyk7XG52YXIgZFAgPSBPYmplY3QuZGVmaW5lUHJvcGVydHk7XG5cbmV4cG9ydHMuZiA9IHJlcXVpcmUoJy4vX2Rlc2NyaXB0b3JzJykgPyBPYmplY3QuZGVmaW5lUHJvcGVydHkgOiBmdW5jdGlvbiBkZWZpbmVQcm9wZXJ0eShPLCBQLCBBdHRyaWJ1dGVzKSB7XG4gIGFuT2JqZWN0KE8pO1xuICBQID0gdG9QcmltaXRpdmUoUCwgdHJ1ZSk7XG4gIGFuT2JqZWN0KEF0dHJpYnV0ZXMpO1xuICBpZiAoSUU4X0RPTV9ERUZJTkUpIHRyeSB7XG4gICAgcmV0dXJuIGRQKE8sIFAsIEF0dHJpYnV0ZXMpO1xuICB9IGNhdGNoIChlKSB7IC8qIGVtcHR5ICovIH1cbiAgaWYgKCdnZXQnIGluIEF0dHJpYnV0ZXMgfHwgJ3NldCcgaW4gQXR0cmlidXRlcykgdGhyb3cgVHlwZUVycm9yKCdBY2Nlc3NvcnMgbm90IHN1cHBvcnRlZCEnKTtcbiAgaWYgKCd2YWx1ZScgaW4gQXR0cmlidXRlcykgT1tQXSA9IEF0dHJpYnV0ZXMudmFsdWU7XG4gIHJldHVybiBPO1xufTtcbiIsInZhciBkUCA9IHJlcXVpcmUoJy4vX29iamVjdC1kcCcpO1xudmFyIGFuT2JqZWN0ID0gcmVxdWlyZSgnLi9fYW4tb2JqZWN0Jyk7XG52YXIgZ2V0S2V5cyA9IHJlcXVpcmUoJy4vX29iamVjdC1rZXlzJyk7XG5cbm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZSgnLi9fZGVzY3JpcHRvcnMnKSA/IE9iamVjdC5kZWZpbmVQcm9wZXJ0aWVzIDogZnVuY3Rpb24gZGVmaW5lUHJvcGVydGllcyhPLCBQcm9wZXJ0aWVzKSB7XG4gIGFuT2JqZWN0KE8pO1xuICB2YXIga2V5cyA9IGdldEtleXMoUHJvcGVydGllcyk7XG4gIHZhciBsZW5ndGggPSBrZXlzLmxlbmd0aDtcbiAgdmFyIGkgPSAwO1xuICB2YXIgUDtcbiAgd2hpbGUgKGxlbmd0aCA+IGkpIGRQLmYoTywgUCA9IGtleXNbaSsrXSwgUHJvcGVydGllc1tQXSk7XG4gIHJldHVybiBPO1xufTtcbiIsInZhciBoYXMgPSByZXF1aXJlKCcuL19oYXMnKTtcbnZhciB0b0lPYmplY3QgPSByZXF1aXJlKCcuL190by1pb2JqZWN0Jyk7XG52YXIgYXJyYXlJbmRleE9mID0gcmVxdWlyZSgnLi9fYXJyYXktaW5jbHVkZXMnKShmYWxzZSk7XG52YXIgSUVfUFJPVE8gPSByZXF1aXJlKCcuL19zaGFyZWQta2V5JykoJ0lFX1BST1RPJyk7XG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKG9iamVjdCwgbmFtZXMpIHtcbiAgdmFyIE8gPSB0b0lPYmplY3Qob2JqZWN0KTtcbiAgdmFyIGkgPSAwO1xuICB2YXIgcmVzdWx0ID0gW107XG4gIHZhciBrZXk7XG4gIGZvciAoa2V5IGluIE8pIGlmIChrZXkgIT0gSUVfUFJPVE8pIGhhcyhPLCBrZXkpICYmIHJlc3VsdC5wdXNoKGtleSk7XG4gIC8vIERvbid0IGVudW0gYnVnICYgaGlkZGVuIGtleXNcbiAgd2hpbGUgKG5hbWVzLmxlbmd0aCA+IGkpIGlmIChoYXMoTywga2V5ID0gbmFtZXNbaSsrXSkpIHtcbiAgICB+YXJyYXlJbmRleE9mKHJlc3VsdCwga2V5KSB8fCByZXN1bHQucHVzaChrZXkpO1xuICB9XG4gIHJldHVybiByZXN1bHQ7XG59O1xuIiwiLy8gMTkuMS4yLjE0IC8gMTUuMi4zLjE0IE9iamVjdC5rZXlzKE8pXG52YXIgJGtleXMgPSByZXF1aXJlKCcuL19vYmplY3Qta2V5cy1pbnRlcm5hbCcpO1xudmFyIGVudW1CdWdLZXlzID0gcmVxdWlyZSgnLi9fZW51bS1idWcta2V5cycpO1xuXG5tb2R1bGUuZXhwb3J0cyA9IE9iamVjdC5rZXlzIHx8IGZ1bmN0aW9uIGtleXMoTykge1xuICByZXR1cm4gJGtleXMoTywgZW51bUJ1Z0tleXMpO1xufTtcbiIsIm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKGJpdG1hcCwgdmFsdWUpIHtcbiAgcmV0dXJuIHtcbiAgICBlbnVtZXJhYmxlOiAhKGJpdG1hcCAmIDEpLFxuICAgIGNvbmZpZ3VyYWJsZTogIShiaXRtYXAgJiAyKSxcbiAgICB3cml0YWJsZTogIShiaXRtYXAgJiA0KSxcbiAgICB2YWx1ZTogdmFsdWVcbiAgfTtcbn07XG4iLCJ2YXIgc2hhcmVkID0gcmVxdWlyZSgnLi9fc2hhcmVkJykoJ2tleXMnKTtcbnZhciB1aWQgPSByZXF1aXJlKCcuL191aWQnKTtcbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKGtleSkge1xuICByZXR1cm4gc2hhcmVkW2tleV0gfHwgKHNoYXJlZFtrZXldID0gdWlkKGtleSkpO1xufTtcbiIsInZhciBjb3JlID0gcmVxdWlyZSgnLi9fY29yZScpO1xudmFyIGdsb2JhbCA9IHJlcXVpcmUoJy4vX2dsb2JhbCcpO1xudmFyIFNIQVJFRCA9ICdfX2NvcmUtanNfc2hhcmVkX18nO1xudmFyIHN0b3JlID0gZ2xvYmFsW1NIQVJFRF0gfHwgKGdsb2JhbFtTSEFSRURdID0ge30pO1xuXG4obW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAoa2V5LCB2YWx1ZSkge1xuICByZXR1cm4gc3RvcmVba2V5XSB8fCAoc3RvcmVba2V5XSA9IHZhbHVlICE9PSB1bmRlZmluZWQgPyB2YWx1ZSA6IHt9KTtcbn0pKCd2ZXJzaW9ucycsIFtdKS5wdXNoKHtcbiAgdmVyc2lvbjogY29yZS52ZXJzaW9uLFxuICBtb2RlOiByZXF1aXJlKCcuL19saWJyYXJ5JykgPyAncHVyZScgOiAnZ2xvYmFsJyxcbiAgY29weXJpZ2h0OiAnwqkgMjAxOSBEZW5pcyBQdXNoa2FyZXYgKHpsb2lyb2NrLnJ1KSdcbn0pO1xuIiwidmFyIHRvSW50ZWdlciA9IHJlcXVpcmUoJy4vX3RvLWludGVnZXInKTtcbnZhciBtYXggPSBNYXRoLm1heDtcbnZhciBtaW4gPSBNYXRoLm1pbjtcbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKGluZGV4LCBsZW5ndGgpIHtcbiAgaW5kZXggPSB0b0ludGVnZXIoaW5kZXgpO1xuICByZXR1cm4gaW5kZXggPCAwID8gbWF4KGluZGV4ICsgbGVuZ3RoLCAwKSA6IG1pbihpbmRleCwgbGVuZ3RoKTtcbn07XG4iLCIvLyA3LjEuNCBUb0ludGVnZXJcbnZhciBjZWlsID0gTWF0aC5jZWlsO1xudmFyIGZsb29yID0gTWF0aC5mbG9vcjtcbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKGl0KSB7XG4gIHJldHVybiBpc05hTihpdCA9ICtpdCkgPyAwIDogKGl0ID4gMCA/IGZsb29yIDogY2VpbCkoaXQpO1xufTtcbiIsIi8vIHRvIGluZGV4ZWQgb2JqZWN0LCB0b09iamVjdCB3aXRoIGZhbGxiYWNrIGZvciBub24tYXJyYXktbGlrZSBFUzMgc3RyaW5nc1xudmFyIElPYmplY3QgPSByZXF1aXJlKCcuL19pb2JqZWN0Jyk7XG52YXIgZGVmaW5lZCA9IHJlcXVpcmUoJy4vX2RlZmluZWQnKTtcbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKGl0KSB7XG4gIHJldHVybiBJT2JqZWN0KGRlZmluZWQoaXQpKTtcbn07XG4iLCIvLyA3LjEuMTUgVG9MZW5ndGhcbnZhciB0b0ludGVnZXIgPSByZXF1aXJlKCcuL190by1pbnRlZ2VyJyk7XG52YXIgbWluID0gTWF0aC5taW47XG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIChpdCkge1xuICByZXR1cm4gaXQgPiAwID8gbWluKHRvSW50ZWdlcihpdCksIDB4MWZmZmZmZmZmZmZmZmYpIDogMDsgLy8gcG93KDIsIDUzKSAtIDEgPT0gOTAwNzE5OTI1NDc0MDk5MVxufTtcbiIsIi8vIDcuMS4xIFRvUHJpbWl0aXZlKGlucHV0IFssIFByZWZlcnJlZFR5cGVdKVxudmFyIGlzT2JqZWN0ID0gcmVxdWlyZSgnLi9faXMtb2JqZWN0Jyk7XG4vLyBpbnN0ZWFkIG9mIHRoZSBFUzYgc3BlYyB2ZXJzaW9uLCB3ZSBkaWRuJ3QgaW1wbGVtZW50IEBAdG9QcmltaXRpdmUgY2FzZVxuLy8gYW5kIHRoZSBzZWNvbmQgYXJndW1lbnQgLSBmbGFnIC0gcHJlZmVycmVkIHR5cGUgaXMgYSBzdHJpbmdcbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKGl0LCBTKSB7XG4gIGlmICghaXNPYmplY3QoaXQpKSByZXR1cm4gaXQ7XG4gIHZhciBmbiwgdmFsO1xuICBpZiAoUyAmJiB0eXBlb2YgKGZuID0gaXQudG9TdHJpbmcpID09ICdmdW5jdGlvbicgJiYgIWlzT2JqZWN0KHZhbCA9IGZuLmNhbGwoaXQpKSkgcmV0dXJuIHZhbDtcbiAgaWYgKHR5cGVvZiAoZm4gPSBpdC52YWx1ZU9mKSA9PSAnZnVuY3Rpb24nICYmICFpc09iamVjdCh2YWwgPSBmbi5jYWxsKGl0KSkpIHJldHVybiB2YWw7XG4gIGlmICghUyAmJiB0eXBlb2YgKGZuID0gaXQudG9TdHJpbmcpID09ICdmdW5jdGlvbicgJiYgIWlzT2JqZWN0KHZhbCA9IGZuLmNhbGwoaXQpKSkgcmV0dXJuIHZhbDtcbiAgdGhyb3cgVHlwZUVycm9yKFwiQ2FuJ3QgY29udmVydCBvYmplY3QgdG8gcHJpbWl0aXZlIHZhbHVlXCIpO1xufTtcbiIsInZhciBpZCA9IDA7XG52YXIgcHggPSBNYXRoLnJhbmRvbSgpO1xubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAoa2V5KSB7XG4gIHJldHVybiAnU3ltYm9sKCcuY29uY2F0KGtleSA9PT0gdW5kZWZpbmVkID8gJycgOiBrZXksICcpXycsICgrK2lkICsgcHgpLnRvU3RyaW5nKDM2KSk7XG59O1xuIiwidmFyICRleHBvcnQgPSByZXF1aXJlKCcuL19leHBvcnQnKTtcbi8vIDE5LjEuMi4yIC8gMTUuMi4zLjUgT2JqZWN0LmNyZWF0ZShPIFssIFByb3BlcnRpZXNdKVxuJGV4cG9ydCgkZXhwb3J0LlMsICdPYmplY3QnLCB7IGNyZWF0ZTogcmVxdWlyZSgnLi9fb2JqZWN0LWNyZWF0ZScpIH0pO1xuIiwidmFyICRleHBvcnQgPSByZXF1aXJlKCcuL19leHBvcnQnKTtcbi8vIDE5LjEuMi40IC8gMTUuMi4zLjYgT2JqZWN0LmRlZmluZVByb3BlcnR5KE8sIFAsIEF0dHJpYnV0ZXMpXG4kZXhwb3J0KCRleHBvcnQuUyArICRleHBvcnQuRiAqICFyZXF1aXJlKCcuL19kZXNjcmlwdG9ycycpLCAnT2JqZWN0JywgeyBkZWZpbmVQcm9wZXJ0eTogcmVxdWlyZSgnLi9fb2JqZWN0LWRwJykuZiB9KTtcbiIsIi8vIFRoZSBtb2R1bGUgY2FjaGVcbnZhciBfX3dlYnBhY2tfbW9kdWxlX2NhY2hlX18gPSB7fTtcblxuLy8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbmZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG5cdHZhciBjYWNoZWRNb2R1bGUgPSBfX3dlYnBhY2tfbW9kdWxlX2NhY2hlX19bbW9kdWxlSWRdO1xuXHRpZiAoY2FjaGVkTW9kdWxlICE9PSB1bmRlZmluZWQpIHtcblx0XHRyZXR1cm4gY2FjaGVkTW9kdWxlLmV4cG9ydHM7XG5cdH1cblx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcblx0dmFyIG1vZHVsZSA9IF9fd2VicGFja19tb2R1bGVfY2FjaGVfX1ttb2R1bGVJZF0gPSB7XG5cdFx0Ly8gbm8gbW9kdWxlLmlkIG5lZWRlZFxuXHRcdC8vIG5vIG1vZHVsZS5sb2FkZWQgbmVlZGVkXG5cdFx0ZXhwb3J0czoge31cblx0fTtcblxuXHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cblx0X193ZWJwYWNrX21vZHVsZXNfX1ttb2R1bGVJZF0obW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cblx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcblx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xufVxuXG4iLCIvKipcbiAqIENvcHlyaWdodCBzaW5jZSAyMDA3IFByZXN0YVNob3AgU0EgYW5kIENvbnRyaWJ1dG9yc1xuICogUHJlc3RhU2hvcCBpcyBhbiBJbnRlcm5hdGlvbmFsIFJlZ2lzdGVyZWQgVHJhZGVtYXJrICYgUHJvcGVydHkgb2YgUHJlc3RhU2hvcCBTQVxuICpcbiAqIE5PVElDRSBPRiBMSUNFTlNFXG4gKlxuICogVGhpcyBzb3VyY2UgZmlsZSBpcyBzdWJqZWN0IHRvIHRoZSBPcGVuIFNvZnR3YXJlIExpY2Vuc2UgKE9TTCAzLjApXG4gKiB0aGF0IGlzIGJ1bmRsZWQgd2l0aCB0aGlzIHBhY2thZ2UgaW4gdGhlIGZpbGUgTElDRU5TRS5tZC5cbiAqIEl0IGlzIGFsc28gYXZhaWxhYmxlIHRocm91Z2ggdGhlIHdvcmxkLXdpZGUtd2ViIGF0IHRoaXMgVVJMOlxuICogaHR0cHM6Ly9vcGVuc291cmNlLm9yZy9saWNlbnNlcy9PU0wtMy4wXG4gKiBJZiB5b3UgZGlkIG5vdCByZWNlaXZlIGEgY29weSBvZiB0aGUgbGljZW5zZSBhbmQgYXJlIHVuYWJsZSB0b1xuICogb2J0YWluIGl0IHRocm91Z2ggdGhlIHdvcmxkLXdpZGUtd2ViLCBwbGVhc2Ugc2VuZCBhbiBlbWFpbFxuICogdG8gbGljZW5zZUBwcmVzdGFzaG9wLmNvbSBzbyB3ZSBjYW4gc2VuZCB5b3UgYSBjb3B5IGltbWVkaWF0ZWx5LlxuICpcbiAqIERJU0NMQUlNRVJcbiAqXG4gKiBEbyBub3QgZWRpdCBvciBhZGQgdG8gdGhpcyBmaWxlIGlmIHlvdSB3aXNoIHRvIHVwZ3JhZGUgUHJlc3RhU2hvcCB0byBuZXdlclxuICogdmVyc2lvbnMgaW4gdGhlIGZ1dHVyZS4gSWYgeW91IHdpc2ggdG8gY3VzdG9taXplIFByZXN0YVNob3AgZm9yIHlvdXJcbiAqIG5lZWRzIHBsZWFzZSByZWZlciB0byBodHRwczovL2RldmRvY3MucHJlc3Rhc2hvcC5jb20vIGZvciBtb3JlIGluZm9ybWF0aW9uLlxuICpcbiAqIEBhdXRob3IgICAgUHJlc3RhU2hvcCBTQSBhbmQgQ29udHJpYnV0b3JzIDxjb250YWN0QHByZXN0YXNob3AuY29tPlxuICogQGNvcHlyaWdodCBTaW5jZSAyMDA3IFByZXN0YVNob3AgU0EgYW5kIENvbnRyaWJ1dG9yc1xuICogQGxpY2Vuc2UgICBodHRwczovL29wZW5zb3VyY2Uub3JnL2xpY2Vuc2VzL09TTC0zLjAgT3BlbiBTb2Z0d2FyZSBMaWNlbnNlIChPU0wgMy4wKVxuICovXG5cbmltcG9ydCBQcm9kdWN0U3VwcGxpZXJzTWFuYWdlciBmcm9tICdAcGFnZXMvcHJvZHVjdC9lZGl0L3Byb2R1Y3Qtc3VwcGxpZXJzLW1hbmFnZXInO1xuaW1wb3J0IEltYWdlU2VsZWN0b3IgZnJvbSAnQHBhZ2VzL3Byb2R1Y3QvY29tYmluYXRpb24vaW1hZ2Utc2VsZWN0b3InO1xuaW1wb3J0IFByb2R1Y3RNYXAgZnJvbSAnQHBhZ2VzL3Byb2R1Y3QvcHJvZHVjdC1tYXAnO1xuXG5jb25zdCB7JH0gPSB3aW5kb3c7XG5cbiQoKCkgPT4ge1xuICB3aW5kb3cucHJlc3Rhc2hvcC5jb21wb25lbnQuaW5pdENvbXBvbmVudHMoW1xuICAgICdUcmFuc2xhdGFibGVGaWVsZCcsXG4gICAgJ1RpbnlNQ0VFZGl0b3InLFxuICAgICdUcmFuc2xhdGFibGVJbnB1dCcsXG4gICAgJ0V2ZW50RW1pdHRlcicsXG4gICAgJ1RleHRXaXRoTGVuZ3RoQ291bnRlcicsXG4gIF0pO1xuXG4gIG5ldyBQcm9kdWN0U3VwcGxpZXJzTWFuYWdlcihQcm9kdWN0TWFwLnN1cHBsaWVycy5jb21iaW5hdGlvblN1cHBsaWVycywgZmFsc2UpO1xuICBuZXcgSW1hZ2VTZWxlY3RvcigpO1xufSk7XG4iXSwic291cmNlUm9vdCI6IiJ9