/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./js/components/addons-connector.js":
/*!*******************************************!*\
  !*** ./js/components/addons-connector.js ***!
  \*******************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

"use strict";


Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 */

var _window = window,
    $ = _window.$;

/**
 * Responsible for connecting to addons marketplace.
 * Makes an addons connect request to the server, displays error messages if it fails.
 */

var AddonsConnector = function () {
  function AddonsConnector(addonsConnectFormSelector, loadingSpinnerSelector) {
    (0, _classCallCheck3.default)(this, AddonsConnector);

    this.addonsConnectFormSelector = addonsConnectFormSelector;
    this.$loadingSpinner = $(loadingSpinnerSelector);

    this.initEvents();

    return {};
  }

  /**
   * Initialize events related to connection to addons.
   *
   * @private
   */


  (0, _createClass3.default)(AddonsConnector, [{
    key: 'initEvents',
    value: function initEvents() {
      var _this = this;

      $('body').on('submit', this.addonsConnectFormSelector, function (event) {
        var $form = $(event.currentTarget);
        event.preventDefault();
        event.stopPropagation();

        _this.connect($form.attr('action'), $form.serialize());
      });
    }

    /**
     * Do a POST request to connect to addons.
     *
     * @param {String} addonsConnectUrl
     * @param {Object} formData
     *
     * @private
     */

  }, {
    key: 'connect',
    value: function connect(addonsConnectUrl, formData) {
      var _this2 = this;

      $.ajax({
        method: 'POST',
        url: addonsConnectUrl,
        dataType: 'json',
        data: formData,
        beforeSend: function beforeSend() {
          _this2.$loadingSpinner.show();
          $('button.btn[type="submit"]', _this2.addonsConnectFormSelector).hide();
        }
      }).then(function (response) {
        if (response.success === 1) {
          window.location.reload();
        } else {
          $.growl.error({
            message: response.message
          });

          _this2.$loadingSpinner.hide();
          $('button.btn[type="submit"]', _this2.addonsConnectFormSelector).fadeIn();
        }
      }, function () {
        $.growl.error({
          message: $(_this2.addonsConnectFormSelector).data('error-message')
        });

        _this2.$loadingSpinner.hide();
        $('button.btn[type="submit"]', _this2.addonsConnectFormSelector).show();
      });
    }
  }]);
  return AddonsConnector;
}();

exports.default = AddonsConnector;

/***/ }),

/***/ "./js/components/change-password-handler.js":
/*!**************************************************!*\
  !*** ./js/components/change-password-handler.js ***!
  \**************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

"use strict";


Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 */

var _window = window,
    $ = _window.$;

/**
 * Generates a password and informs about it's strength.
 * You can pass a password input to watch the password strength and display feedback messages.
 * You can also generate a random password into an input.
 */

var ChangePasswordHandler = function () {
  function ChangePasswordHandler(passwordStrengthFeedbackContainerSelector) {
    var _this = this;

    var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
    (0, _classCallCheck3.default)(this, ChangePasswordHandler);

    // Minimum length of the generated password.
    this.minLength = options.minLength || 8;

    // Feedback container holds messages representing password strength.
    this.$feedbackContainer = $(passwordStrengthFeedbackContainerSelector);

    return {
      watchPasswordStrength: function watchPasswordStrength($input) {
        return _this.watchPasswordStrength($input);
      },
      generatePassword: function generatePassword($input) {
        return _this.generatePassword($input);
      }
    };
  }

  /**
   * Watch password, which is entered in the input, strength and inform about it.
   *
   * @param {jQuery} $input the input to watch.
   */


  (0, _createClass3.default)(ChangePasswordHandler, [{
    key: 'watchPasswordStrength',
    value: function watchPasswordStrength($input) {
      var _this2 = this;

      $.passy.requirements.length.min = this.minLength;
      $.passy.requirements.characters = 'DIGIT';

      $input.each(function (index, element) {
        var $outputContainer = $('<span>');

        $outputContainer.insertAfter($(element));

        $(element).passy(function (strength, valid) {
          _this2.displayFeedback($outputContainer, strength, valid);
        });
      });
    }

    /**
     * Generates a password and fills it to given input.
     *
     * @param {jQuery} $input the input to fill the password into.
     */

  }, {
    key: 'generatePassword',
    value: function generatePassword($input) {
      $input.passy('generate', this.minLength);
    }

    /**
     * Display feedback about password's strength.
     *
     * @param {jQuery} $outputContainer a container to put feedback output into.
     * @param {number} passwordStrength
     * @param {boolean} isPasswordValid
     *
     * @private
     */

  }, {
    key: 'displayFeedback',
    value: function displayFeedback($outputContainer, passwordStrength, isPasswordValid) {
      var feedback = this.getPasswordStrengthFeedback(passwordStrength);
      $outputContainer.text(feedback.message);
      $outputContainer.removeClass('text-danger text-warning text-success');
      $outputContainer.addClass(feedback.elementClass);
      $outputContainer.toggleClass('d-none', !isPasswordValid);
    }

    /**
     * Get feedback that describes given password strength.
     * Response contains text message and element class.
     *
     * @param {number} strength
     *
     * @private
     */

  }, {
    key: 'getPasswordStrengthFeedback',
    value: function getPasswordStrengthFeedback(strength) {
      switch (strength) {
        case $.passy.strength.LOW:
          return {
            message: this.$feedbackContainer.find('.strength-low').text(),
            elementClass: 'text-danger'
          };

        case $.passy.strength.MEDIUM:
          return {
            message: this.$feedbackContainer.find('.strength-medium').text(),
            elementClass: 'text-warning'
          };

        case $.passy.strength.HIGH:
          return {
            message: this.$feedbackContainer.find('.strength-high').text(),
            elementClass: 'text-success'
          };

        case $.passy.strength.EXTREME:
          return {
            message: this.$feedbackContainer.find('.strength-extreme').text(),
            elementClass: 'text-success'
          };

        default:
          throw new Error('Invalid password strength indicator.');
      }
    }
  }]);
  return ChangePasswordHandler;
}();

exports.default = ChangePasswordHandler;

/***/ }),

/***/ "./js/components/form/change-password-control.js":
/*!*******************************************************!*\
  !*** ./js/components/form/change-password-control.js ***!
  \*******************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

"use strict";


Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _changePasswordHandler = __webpack_require__(/*! ../change-password-handler */ "./js/components/change-password-handler.js");

var _changePasswordHandler2 = _interopRequireDefault(_changePasswordHandler);

var _passwordValidator = __webpack_require__(/*! ../password-validator */ "./js/components/password-validator.js");

var _passwordValidator2 = _interopRequireDefault(_passwordValidator);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 */

var _window = window,
    $ = _window.$;

/**
 * Class responsible for actions related to "change password" form type.
 * Generates random passwords, validates new password and it's confirmation,
 * displays error messages related to validation.
 */

var ChangePasswordControl = function () {
  function ChangePasswordControl(inputsBlockSelector, showButtonSelector, hideButtonSelector, generatePasswordButtonSelector, oldPasswordInputSelector, newPasswordInputSelector, confirmNewPasswordInputSelector, generatedPasswordDisplaySelector, passwordStrengthFeedbackContainerSelector) {
    (0, _classCallCheck3.default)(this, ChangePasswordControl);

    // Block that contains password inputs
    this.$inputsBlock = $(inputsBlockSelector);

    // Button that shows the password inputs block
    this.showButtonSelector = showButtonSelector;

    // Button that hides the password inputs block
    this.hideButtonSelector = hideButtonSelector;

    // Button that generates a random password
    this.generatePasswordButtonSelector = generatePasswordButtonSelector;

    // Input to enter old password
    this.oldPasswordInputSelector = oldPasswordInputSelector;

    // Input to enter new password
    this.newPasswordInputSelector = newPasswordInputSelector;

    // Input to confirm the new password
    this.confirmNewPasswordInputSelector = confirmNewPasswordInputSelector;

    // Input that displays generated random password
    this.generatedPasswordDisplaySelector = generatedPasswordDisplaySelector;

    // Main input for password generation
    this.$newPasswordInputs = this.$inputsBlock.find(this.newPasswordInputSelector);

    // Generated password will be copied to these inputs
    this.$copyPasswordInputs = this.$inputsBlock.find(this.confirmNewPasswordInputSelector).add(this.generatedPasswordDisplaySelector);

    // All inputs in the change password block, that are submittable with the form.
    this.$submittableInputs = this.$inputsBlock.find(this.oldPasswordInputSelector).add(this.newPasswordInputSelector).add(this.confirmNewPasswordInputSelector);

    this.passwordHandler = new _changePasswordHandler2.default(passwordStrengthFeedbackContainerSelector);

    this.passwordValidator = new _passwordValidator2.default(this.newPasswordInputSelector, this.confirmNewPasswordInputSelector);

    this.hideInputsBlock();
    this.initEvents();

    return {};
  }

  /**
   * Initialize events.
   *
   * @private
   */


  (0, _createClass3.default)(ChangePasswordControl, [{
    key: 'initEvents',
    value: function initEvents() {
      var _this = this;

      // Show the inputs block when show button is clicked
      $(document).on('click', this.showButtonSelector, function (e) {
        _this.hide($(e.currentTarget));
        _this.showInputsBlock();
      });

      $(document).on('click', this.hideButtonSelector, function () {
        _this.hideInputsBlock();
        _this.show($(_this.showButtonSelector));
      });

      // Watch and display feedback about password's strength
      this.passwordHandler.watchPasswordStrength(this.$newPasswordInputs);

      $(document).on('click', this.generatePasswordButtonSelector, function () {
        // Generate the password into main input.
        _this.passwordHandler.generatePassword(_this.$newPasswordInputs);

        // Copy the generated password from main input to additional inputs
        _this.$copyPasswordInputs.val(_this.$newPasswordInputs.val());
        _this.checkPasswordValidity();
      });

      // Validate new password and it's confirmation when any of the inputs is changed
      $(document).on('keyup', this.newPasswordInputSelector + ',' + this.confirmNewPasswordInputSelector, function () {
        _this.checkPasswordValidity();
      });

      // Prevent submitting the form if new password is not valid
      $(document).on('submit', $(this.oldPasswordInputSelector).closest('form'), function (event) {
        // If password input is disabled - we don't need to validate it.
        if ($(_this.oldPasswordInputSelector).is(':disabled')) {
          return;
        }

        if (!_this.passwordValidator.isPasswordValid()) {
          event.preventDefault();
        }
      });
    }

    /**
     * Check if password is valid, show error messages if it's not.
     *
     * @private
     */

  }, {
    key: 'checkPasswordValidity',
    value: function checkPasswordValidity() {
      var $firstPasswordErrorContainer = $(this.newPasswordInputSelector).parent().find('.form-text');
      var $secondPasswordErrorContainer = $(this.confirmNewPasswordInputSelector).parent().find('.form-text');

      $firstPasswordErrorContainer.text(this.getPasswordLengthValidationMessage()).toggleClass('text-danger', !this.passwordValidator.isPasswordLengthValid());
      $secondPasswordErrorContainer.text(this.getPasswordConfirmationValidationMessage()).toggleClass('text-danger', !this.passwordValidator.isPasswordMatchingConfirmation());
    }

    /**
     * Get password confirmation validation message.
     *
     * @returns {String}
     *
     * @private
     */

  }, {
    key: 'getPasswordConfirmationValidationMessage',
    value: function getPasswordConfirmationValidationMessage() {
      if (!this.passwordValidator.isPasswordMatchingConfirmation()) {
        return $(this.confirmNewPasswordInputSelector).data('invalid-password');
      }

      return '';
    }

    /**
     * Get password length validation message.
     *
     * @returns {String}
     *
     * @private
     */

  }, {
    key: 'getPasswordLengthValidationMessage',
    value: function getPasswordLengthValidationMessage() {
      if (this.passwordValidator.isPasswordTooShort()) {
        return $(this.newPasswordInputSelector).data('password-too-short');
      }

      if (this.passwordValidator.isPasswordTooLong()) {
        return $(this.newPasswordInputSelector).data('password-too-long');
      }

      return '';
    }

    /**
     * Show the password inputs block.
     *
     * @private
     */

  }, {
    key: 'showInputsBlock',
    value: function showInputsBlock() {
      this.show(this.$inputsBlock);
      this.$submittableInputs.removeAttr('disabled');
      this.$submittableInputs.attr('required', 'required');
    }

    /**
     * Hide the password inputs block.
     *
     * @private
     */

  }, {
    key: 'hideInputsBlock',
    value: function hideInputsBlock() {
      this.hide(this.$inputsBlock);
      this.$submittableInputs.attr('disabled', 'disabled');
      this.$submittableInputs.removeAttr('required');
      this.$inputsBlock.find('input').val('');
      this.$inputsBlock.find('.form-text').text('');
    }

    /**
     * Hide an element.
     *
     * @param {jQuery} $el
     *
     * @private
     */

  }, {
    key: 'hide',
    value: function hide($el) {
      $el.addClass('d-none');
    }

    /**
     * Show hidden element.
     *
     * @param {jQuery} $el
     *
     * @private
     */

  }, {
    key: 'show',
    value: function show($el) {
      $el.removeClass('d-none');
    }
  }]);
  return ChangePasswordControl;
}();

exports.default = ChangePasswordControl;

/***/ }),

/***/ "./js/components/form/choice-tree.js":
/*!*******************************************!*\
  !*** ./js/components/form/choice-tree.js ***!
  \*******************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

"use strict";


Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 */

var _window = window,
    $ = _window.$;

/**
 * Handles UI interactions of choice tree
 */

var ChoiceTree = function () {
  /**
   * @param {String} treeSelector
   */
  function ChoiceTree(treeSelector) {
    var _this = this;

    (0, _classCallCheck3.default)(this, ChoiceTree);

    this.$container = $(treeSelector);

    this.$container.on('click', '.js-input-wrapper', function (event) {
      var $inputWrapper = $(event.currentTarget);

      _this.toggleChildTree($inputWrapper);
    });

    this.$container.on('click', '.js-toggle-choice-tree-action', function (event) {
      var $action = $(event.currentTarget);

      _this.toggleTree($action);
    });

    return {
      enableAutoCheckChildren: function enableAutoCheckChildren() {
        return _this.enableAutoCheckChildren();
      },
      enableAllInputs: function enableAllInputs() {
        return _this.enableAllInputs();
      },
      disableAllInputs: function disableAllInputs() {
        return _this.disableAllInputs();
      }
    };
  }

  /**
   * Enable automatic check/uncheck of clicked item's children.
   */


  (0, _createClass3.default)(ChoiceTree, [{
    key: 'enableAutoCheckChildren',
    value: function enableAutoCheckChildren() {
      this.$container.on('change', 'input[type="checkbox"]', function (event) {
        var $clickedCheckbox = $(event.currentTarget);
        var $itemWithChildren = $clickedCheckbox.closest('li');

        $itemWithChildren.find('ul input[type="checkbox"]').prop('checked', $clickedCheckbox.is(':checked'));
      });
    }

    /**
     * Enable all inputs in the choice tree.
     */

  }, {
    key: 'enableAllInputs',
    value: function enableAllInputs() {
      this.$container.find('input').removeAttr('disabled');
    }

    /**
     * Disable all inputs in the choice tree.
     */

  }, {
    key: 'disableAllInputs',
    value: function disableAllInputs() {
      this.$container.find('input').attr('disabled', 'disabled');
    }

    /**
     * Collapse or expand sub-tree for single parent
     *
     * @param {jQuery} $inputWrapper
     *
     * @private
     */

  }, {
    key: 'toggleChildTree',
    value: function toggleChildTree($inputWrapper) {
      var $parentWrapper = $inputWrapper.closest('li');

      if ($parentWrapper.hasClass('expanded')) {
        $parentWrapper.removeClass('expanded').addClass('collapsed');

        return;
      }

      if ($parentWrapper.hasClass('collapsed')) {
        $parentWrapper.removeClass('collapsed').addClass('expanded');
      }
    }

    /**
     * Collapse or expand whole tree
     *
     * @param {jQuery} $action
     *
     * @private
     */

  }, {
    key: 'toggleTree',
    value: function toggleTree($action) {
      var $parentContainer = $action.closest('.js-choice-tree-container');
      var action = $action.data('action');

      // toggle action configuration
      var config = {
        addClass: {
          expand: 'expanded',
          collapse: 'collapsed'
        },
        removeClass: {
          expand: 'collapsed',
          collapse: 'expanded'
        },
        nextAction: {
          expand: 'collapse',
          collapse: 'expand'
        },
        text: {
          expand: 'collapsed-text',
          collapse: 'expanded-text'
        },
        icon: {
          expand: 'collapsed-icon',
          collapse: 'expanded-icon'
        }
      };

      $parentContainer.find('li').each(function (index, item) {
        var $item = $(item);

        if ($item.hasClass(config.removeClass[action])) {
          $item.removeClass(config.removeClass[action]).addClass(config.addClass[action]);
        }
      });

      $action.data('action', config.nextAction[action]);
      $action.find('.material-icons').text($action.data(config.icon[action]));
      $action.find('.js-toggle-text').text($action.data(config.text[action]));
    }
  }]);
  return ChoiceTree;
}();

exports.default = ChoiceTree;

/***/ }),

/***/ "./js/components/password-validator.js":
/*!*********************************************!*\
  !*** ./js/components/password-validator.js ***!
  \*********************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

"use strict";


Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 */

/**
 * Class responsible for checking password's validity.
 * Can validate entered password's length against min/max values.
 * If password confirmation input is provided, can validate if entered password is matching confirmation.
 */
var PasswordValidator = function () {
  /**
   * @param {String} passwordInputSelector selector of the password input.
   * @param {String|null} confirmPasswordInputSelector (optional) selector for the password confirmation input.
   * @param {Object} options allows overriding default options.
   */
  function PasswordValidator(passwordInputSelector) {
    var confirmPasswordInputSelector = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
    var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
    (0, _classCallCheck3.default)(this, PasswordValidator);

    this.newPasswordInput = document.querySelector(passwordInputSelector);
    this.confirmPasswordInput = document.querySelector(confirmPasswordInputSelector);

    // Minimum allowed length for entered password
    this.minPasswordLength = options.minPasswordLength || 8;

    // Maximum allowed length for entered password
    this.maxPasswordLength = options.maxPasswordLength || 255;
  }

  /**
   * Check if the password is valid.
   *
   * @returns {boolean}
   */


  (0, _createClass3.default)(PasswordValidator, [{
    key: 'isPasswordValid',
    value: function isPasswordValid() {
      if (this.confirmPasswordInput && !this.isPasswordMatchingConfirmation()) {
        return false;
      }

      return this.isPasswordLengthValid();
    }

    /**
     * Check if password's length is valid.
     *
     * @returns {boolean}
     */

  }, {
    key: 'isPasswordLengthValid',
    value: function isPasswordLengthValid() {
      return !this.isPasswordTooShort() && !this.isPasswordTooLong();
    }

    /**
     * Check if password is matching it's confirmation.
     *
     * @returns {boolean}
     */

  }, {
    key: 'isPasswordMatchingConfirmation',
    value: function isPasswordMatchingConfirmation() {
      if (!this.confirmPasswordInput) {
        throw new Error('Confirm password input is not provided for the password validator.');
      }

      if (this.confirmPasswordInput.value === '') {
        return true;
      }

      return this.newPasswordInput.value === this.confirmPasswordInput.value;
    }

    /**
     * Check if password is too short.
     *
     * @returns {boolean}
     */

  }, {
    key: 'isPasswordTooShort',
    value: function isPasswordTooShort() {
      return this.newPasswordInput.value.length < this.minPasswordLength;
    }

    /**
     * Check if password is too long.
     *
     * @returns {boolean}
     */

  }, {
    key: 'isPasswordTooLong',
    value: function isPasswordTooLong() {
      return this.newPasswordInput.value.length > this.maxPasswordLength;
    }
  }]);
  return PasswordValidator;
}();

exports.default = PasswordValidator;

/***/ }),

/***/ "./js/pages/employee/EmployeeForm.js":
/*!*******************************************!*\
  !*** ./js/pages/employee/EmployeeForm.js ***!
  \*******************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

"use strict";
/* provided dependency */ var $ = __webpack_require__(/*! jquery */ "jquery");


Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var _keys = __webpack_require__(/*! babel-runtime/core-js/object/keys */ "./node_modules/babel-runtime/core-js/object/keys.js");

var _keys2 = _interopRequireDefault(_keys);

var _values = __webpack_require__(/*! babel-runtime/core-js/object/values */ "./node_modules/babel-runtime/core-js/object/values.js");

var _values2 = _interopRequireDefault(_values);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _choiceTree = __webpack_require__(/*! ../../components/form/choice-tree */ "./js/components/form/choice-tree.js");

var _choiceTree2 = _interopRequireDefault(_choiceTree);

var _addonsConnector = __webpack_require__(/*! ../../components/addons-connector */ "./js/components/addons-connector.js");

var _addonsConnector2 = _interopRequireDefault(_addonsConnector);

var _changePasswordControl = __webpack_require__(/*! ../../components/form/change-password-control */ "./js/components/form/change-password-control.js");

var _changePasswordControl2 = _interopRequireDefault(_changePasswordControl);

var _employeeFormMap = __webpack_require__(/*! ./employee-form-map */ "./js/pages/employee/employee-form-map.js");

var _employeeFormMap2 = _interopRequireDefault(_employeeFormMap);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Class responsible for javascript actions in employee add/edit page.
 */
/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 */

var EmployeeForm = function () {
  function EmployeeForm() {
    (0, _classCallCheck3.default)(this, EmployeeForm);

    this.shopChoiceTreeSelector = _employeeFormMap2.default.shopChoiceTree;
    this.shopChoiceTree = new _choiceTree2.default(this.shopChoiceTreeSelector);
    this.employeeProfileSelector = _employeeFormMap2.default.profileSelect;
    this.tabsDropdownSelector = _employeeFormMap2.default.defaultPageSelect;

    this.shopChoiceTree.enableAutoCheckChildren();

    new _addonsConnector2.default(_employeeFormMap2.default.addonsConnectForm, _employeeFormMap2.default.addonsLoginButton);

    new _changePasswordControl2.default(_employeeFormMap2.default.changePasswordInputsBlock, _employeeFormMap2.default.showChangePasswordBlockButton, _employeeFormMap2.default.hideChangePasswordBlockButton, _employeeFormMap2.default.generatePasswordButton, _employeeFormMap2.default.oldPasswordInput, _employeeFormMap2.default.newPasswordInput, _employeeFormMap2.default.confirmNewPasswordInput, _employeeFormMap2.default.generatedPasswordDisplayInput, _employeeFormMap2.default.passwordStrengthFeedbackContainer);

    this.initEvents();
    this.toggleShopTree();

    return {};
  }

  /**
   * Initialize page's events.
   *
   * @private
   */


  (0, _createClass3.default)(EmployeeForm, [{
    key: 'initEvents',
    value: function initEvents() {
      var _this = this;

      var $employeeProfilesDropdown = $(this.employeeProfileSelector);
      var getTabsUrl = $employeeProfilesDropdown.data('get-tabs-url');

      $(document).on('change', this.employeeProfileSelector, function () {
        return _this.toggleShopTree();
      });

      // Reload tabs dropdown when employee profile is changed.
      $(document).on('change', this.employeeProfileSelector, function (event) {
        $.get(getTabsUrl, {
          profileId: $(event.currentTarget).val()
        }, function (tabs) {
          _this.reloadTabsDropdown(tabs);
        }, 'json');
      });
    }

    /**
     * Reload tabs dropdown with new content.
     *
     * @param {Object} accessibleTabs
     *
     * @private
     */

  }, {
    key: 'reloadTabsDropdown',
    value: function reloadTabsDropdown(accessibleTabs) {
      var _this2 = this;

      var $tabsDropdown = $(this.tabsDropdownSelector);

      $tabsDropdown.empty();

      (0, _values2.default)(accessibleTabs).forEach(function (accessibleTab) {
        if (accessibleTab.children.length > 0 && accessibleTab.name) {
          // If tab has children - create an option group and put children inside.
          var $optgroup = _this2.createOptionGroup(accessibleTab.name);

          (0, _keys2.default)(accessibleTab.children).forEach(function (childKey) {
            if (accessibleTab.children[childKey].name) {
              $optgroup.append(_this2.createOption(accessibleTab.children[childKey].name, accessibleTab.children[childKey].id_tab));
            }
          });

          $tabsDropdown.append($optgroup);
        } else if (accessibleTab.name) {
          // If tab doesn't have children - create an option.
          $tabsDropdown.append(_this2.createOption(accessibleTab.name, accessibleTab.id_tab));
        }
      });
    }

    /**
     * Hide shop choice tree if superadmin profile is selected, show it otherwise.
     *
     * @private
     */

  }, {
    key: 'toggleShopTree',
    value: function toggleShopTree() {
      var $employeeProfileDropdown = $(this.employeeProfileSelector);
      var superAdminProfileId = $employeeProfileDropdown.data('admin-profile');
      $(this.shopChoiceTreeSelector).closest('.form-group').toggleClass('d-none', $employeeProfileDropdown.val() === superAdminProfileId);
    }

    /**
     * Creates an <optgroup> element
     *
     * @param {String} name
     *
     * @returns {jQuery}
     *
     * @private
     */

  }, {
    key: 'createOptionGroup',
    value: function createOptionGroup(name) {
      return $('<optgroup label="' + name + '">');
    }

    /**
     * Creates an <option> element.
     *
     * @param {String} name
     * @param {String} value
     *
     * @returns {jQuery}
     *
     * @private
     */

  }, {
    key: 'createOption',
    value: function createOption(name, value) {
      return $('<option value="' + value + '">' + name + '</option>');
    }
  }]);
  return EmployeeForm;
}();

exports.default = EmployeeForm;

/***/ }),

/***/ "./js/pages/employee/employee-form-map.js":
/*!************************************************!*\
  !*** ./js/pages/employee/employee-form-map.js ***!
  \************************************************/
/***/ ((__unused_webpack_module, exports) => {

"use strict";


Object.defineProperty(exports, "__esModule", ({
  value: true
}));
/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 */

/**
 * Defines all selectors that are used in employee add/edit form.
 */
exports.default = {
  shopChoiceTree: '#employee_shop_association',
  profileSelect: '#employee_profile',
  defaultPageSelect: '#employee_default_page',
  addonsConnectForm: '#addons-connect-form',
  addonsLoginButton: '#addons_login_btn',

  // selectors related to "change password" form control
  changePasswordInputsBlock: '.js-change-password-block',
  showChangePasswordBlockButton: '.js-change-password',
  hideChangePasswordBlockButton: '.js-change-password-cancel',
  generatePasswordButton: '#employee_change_password_generate_password_button',
  oldPasswordInput: '#employee_change_password_old_password',
  newPasswordInput: '#employee_change_password_new_password_first',
  confirmNewPasswordInput: '#employee_change_password_new_password_second',
  generatedPasswordDisplayInput: '#employee_change_password_generated_password',
  passwordStrengthFeedbackContainer: '.js-password-strength-feedback'
};

/***/ }),

/***/ "./node_modules/babel-runtime/core-js/object/define-property.js":
/*!**********************************************************************!*\
  !*** ./node_modules/babel-runtime/core-js/object/define-property.js ***!
  \**********************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

module.exports = { "default": __webpack_require__(/*! core-js/library/fn/object/define-property */ "./node_modules/core-js/library/fn/object/define-property.js"), __esModule: true };

/***/ }),

/***/ "./node_modules/babel-runtime/core-js/object/keys.js":
/*!***********************************************************!*\
  !*** ./node_modules/babel-runtime/core-js/object/keys.js ***!
  \***********************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

module.exports = { "default": __webpack_require__(/*! core-js/library/fn/object/keys */ "./node_modules/core-js/library/fn/object/keys.js"), __esModule: true };

/***/ }),

/***/ "./node_modules/babel-runtime/core-js/object/values.js":
/*!*************************************************************!*\
  !*** ./node_modules/babel-runtime/core-js/object/values.js ***!
  \*************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

module.exports = { "default": __webpack_require__(/*! core-js/library/fn/object/values */ "./node_modules/core-js/library/fn/object/values.js"), __esModule: true };

/***/ }),

/***/ "./node_modules/babel-runtime/helpers/classCallCheck.js":
/*!**************************************************************!*\
  !*** ./node_modules/babel-runtime/helpers/classCallCheck.js ***!
  \**************************************************************/
/***/ ((__unused_webpack_module, exports) => {

"use strict";


exports.__esModule = true;

exports.default = function (instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
};

/***/ }),

/***/ "./node_modules/babel-runtime/helpers/createClass.js":
/*!***********************************************************!*\
  !*** ./node_modules/babel-runtime/helpers/createClass.js ***!
  \***********************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

"use strict";


exports.__esModule = true;

var _defineProperty = __webpack_require__(/*! ../core-js/object/define-property */ "./node_modules/babel-runtime/core-js/object/define-property.js");

var _defineProperty2 = _interopRequireDefault(_defineProperty);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      (0, _defineProperty2.default)(target, descriptor.key, descriptor);
    }
  }

  return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);
    if (staticProps) defineProperties(Constructor, staticProps);
    return Constructor;
  };
}();

/***/ }),

/***/ "./node_modules/core-js/library/fn/object/define-property.js":
/*!*******************************************************************!*\
  !*** ./node_modules/core-js/library/fn/object/define-property.js ***!
  \*******************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

__webpack_require__(/*! ../../modules/es6.object.define-property */ "./node_modules/core-js/library/modules/es6.object.define-property.js");
var $Object = __webpack_require__(/*! ../../modules/_core */ "./node_modules/core-js/library/modules/_core.js").Object;
module.exports = function defineProperty(it, key, desc) {
  return $Object.defineProperty(it, key, desc);
};


/***/ }),

/***/ "./node_modules/core-js/library/fn/object/keys.js":
/*!********************************************************!*\
  !*** ./node_modules/core-js/library/fn/object/keys.js ***!
  \********************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

__webpack_require__(/*! ../../modules/es6.object.keys */ "./node_modules/core-js/library/modules/es6.object.keys.js");
module.exports = __webpack_require__(/*! ../../modules/_core */ "./node_modules/core-js/library/modules/_core.js").Object.keys;


/***/ }),

/***/ "./node_modules/core-js/library/fn/object/values.js":
/*!**********************************************************!*\
  !*** ./node_modules/core-js/library/fn/object/values.js ***!
  \**********************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

__webpack_require__(/*! ../../modules/es7.object.values */ "./node_modules/core-js/library/modules/es7.object.values.js");
module.exports = __webpack_require__(/*! ../../modules/_core */ "./node_modules/core-js/library/modules/_core.js").Object.values;


/***/ }),

/***/ "./node_modules/core-js/library/modules/_a-function.js":
/*!*************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_a-function.js ***!
  \*************************************************************/
/***/ ((module) => {

module.exports = function (it) {
  if (typeof it != 'function') throw TypeError(it + ' is not a function!');
  return it;
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_an-object.js":
/*!************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_an-object.js ***!
  \************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var isObject = __webpack_require__(/*! ./_is-object */ "./node_modules/core-js/library/modules/_is-object.js");
module.exports = function (it) {
  if (!isObject(it)) throw TypeError(it + ' is not an object!');
  return it;
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_array-includes.js":
/*!*****************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_array-includes.js ***!
  \*****************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// false -> Array#indexOf
// true  -> Array#includes
var toIObject = __webpack_require__(/*! ./_to-iobject */ "./node_modules/core-js/library/modules/_to-iobject.js");
var toLength = __webpack_require__(/*! ./_to-length */ "./node_modules/core-js/library/modules/_to-length.js");
var toAbsoluteIndex = __webpack_require__(/*! ./_to-absolute-index */ "./node_modules/core-js/library/modules/_to-absolute-index.js");
module.exports = function (IS_INCLUDES) {
  return function ($this, el, fromIndex) {
    var O = toIObject($this);
    var length = toLength(O.length);
    var index = toAbsoluteIndex(fromIndex, length);
    var value;
    // Array#includes uses SameValueZero equality algorithm
    // eslint-disable-next-line no-self-compare
    if (IS_INCLUDES && el != el) while (length > index) {
      value = O[index++];
      // eslint-disable-next-line no-self-compare
      if (value != value) return true;
    // Array#indexOf ignores holes, Array#includes - not
    } else for (;length > index; index++) if (IS_INCLUDES || index in O) {
      if (O[index] === el) return IS_INCLUDES || index || 0;
    } return !IS_INCLUDES && -1;
  };
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_cof.js":
/*!******************************************************!*\
  !*** ./node_modules/core-js/library/modules/_cof.js ***!
  \******************************************************/
/***/ ((module) => {

var toString = {}.toString;

module.exports = function (it) {
  return toString.call(it).slice(8, -1);
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_core.js":
/*!*******************************************************!*\
  !*** ./node_modules/core-js/library/modules/_core.js ***!
  \*******************************************************/
/***/ ((module) => {

var core = module.exports = { version: '2.6.11' };
if (typeof __e == 'number') __e = core; // eslint-disable-line no-undef


/***/ }),

/***/ "./node_modules/core-js/library/modules/_ctx.js":
/*!******************************************************!*\
  !*** ./node_modules/core-js/library/modules/_ctx.js ***!
  \******************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// optional / simple context binding
var aFunction = __webpack_require__(/*! ./_a-function */ "./node_modules/core-js/library/modules/_a-function.js");
module.exports = function (fn, that, length) {
  aFunction(fn);
  if (that === undefined) return fn;
  switch (length) {
    case 1: return function (a) {
      return fn.call(that, a);
    };
    case 2: return function (a, b) {
      return fn.call(that, a, b);
    };
    case 3: return function (a, b, c) {
      return fn.call(that, a, b, c);
    };
  }
  return function (/* ...args */) {
    return fn.apply(that, arguments);
  };
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_defined.js":
/*!**********************************************************!*\
  !*** ./node_modules/core-js/library/modules/_defined.js ***!
  \**********************************************************/
/***/ ((module) => {

// 7.2.1 RequireObjectCoercible(argument)
module.exports = function (it) {
  if (it == undefined) throw TypeError("Can't call method on  " + it);
  return it;
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_descriptors.js":
/*!**************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_descriptors.js ***!
  \**************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// Thank's IE8 for his funny defineProperty
module.exports = !__webpack_require__(/*! ./_fails */ "./node_modules/core-js/library/modules/_fails.js")(function () {
  return Object.defineProperty({}, 'a', { get: function () { return 7; } }).a != 7;
});


/***/ }),

/***/ "./node_modules/core-js/library/modules/_dom-create.js":
/*!*************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_dom-create.js ***!
  \*************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var isObject = __webpack_require__(/*! ./_is-object */ "./node_modules/core-js/library/modules/_is-object.js");
var document = __webpack_require__(/*! ./_global */ "./node_modules/core-js/library/modules/_global.js").document;
// typeof document.createElement is 'object' in old IE
var is = isObject(document) && isObject(document.createElement);
module.exports = function (it) {
  return is ? document.createElement(it) : {};
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_enum-bug-keys.js":
/*!****************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_enum-bug-keys.js ***!
  \****************************************************************/
/***/ ((module) => {

// IE 8- don't enum bug keys
module.exports = (
  'constructor,hasOwnProperty,isPrototypeOf,propertyIsEnumerable,toLocaleString,toString,valueOf'
).split(',');


/***/ }),

/***/ "./node_modules/core-js/library/modules/_export.js":
/*!*********************************************************!*\
  !*** ./node_modules/core-js/library/modules/_export.js ***!
  \*********************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var global = __webpack_require__(/*! ./_global */ "./node_modules/core-js/library/modules/_global.js");
var core = __webpack_require__(/*! ./_core */ "./node_modules/core-js/library/modules/_core.js");
var ctx = __webpack_require__(/*! ./_ctx */ "./node_modules/core-js/library/modules/_ctx.js");
var hide = __webpack_require__(/*! ./_hide */ "./node_modules/core-js/library/modules/_hide.js");
var has = __webpack_require__(/*! ./_has */ "./node_modules/core-js/library/modules/_has.js");
var PROTOTYPE = 'prototype';

var $export = function (type, name, source) {
  var IS_FORCED = type & $export.F;
  var IS_GLOBAL = type & $export.G;
  var IS_STATIC = type & $export.S;
  var IS_PROTO = type & $export.P;
  var IS_BIND = type & $export.B;
  var IS_WRAP = type & $export.W;
  var exports = IS_GLOBAL ? core : core[name] || (core[name] = {});
  var expProto = exports[PROTOTYPE];
  var target = IS_GLOBAL ? global : IS_STATIC ? global[name] : (global[name] || {})[PROTOTYPE];
  var key, own, out;
  if (IS_GLOBAL) source = name;
  for (key in source) {
    // contains in native
    own = !IS_FORCED && target && target[key] !== undefined;
    if (own && has(exports, key)) continue;
    // export native or passed
    out = own ? target[key] : source[key];
    // prevent global pollution for namespaces
    exports[key] = IS_GLOBAL && typeof target[key] != 'function' ? source[key]
    // bind timers to global for call from export context
    : IS_BIND && own ? ctx(out, global)
    // wrap global constructors for prevent change them in library
    : IS_WRAP && target[key] == out ? (function (C) {
      var F = function (a, b, c) {
        if (this instanceof C) {
          switch (arguments.length) {
            case 0: return new C();
            case 1: return new C(a);
            case 2: return new C(a, b);
          } return new C(a, b, c);
        } return C.apply(this, arguments);
      };
      F[PROTOTYPE] = C[PROTOTYPE];
      return F;
    // make static versions for prototype methods
    })(out) : IS_PROTO && typeof out == 'function' ? ctx(Function.call, out) : out;
    // export proto methods to core.%CONSTRUCTOR%.methods.%NAME%
    if (IS_PROTO) {
      (exports.virtual || (exports.virtual = {}))[key] = out;
      // export proto methods to core.%CONSTRUCTOR%.prototype.%NAME%
      if (type & $export.R && expProto && !expProto[key]) hide(expProto, key, out);
    }
  }
};
// type bitmap
$export.F = 1;   // forced
$export.G = 2;   // global
$export.S = 4;   // static
$export.P = 8;   // proto
$export.B = 16;  // bind
$export.W = 32;  // wrap
$export.U = 64;  // safe
$export.R = 128; // real proto method for `library`
module.exports = $export;


/***/ }),

/***/ "./node_modules/core-js/library/modules/_fails.js":
/*!********************************************************!*\
  !*** ./node_modules/core-js/library/modules/_fails.js ***!
  \********************************************************/
/***/ ((module) => {

module.exports = function (exec) {
  try {
    return !!exec();
  } catch (e) {
    return true;
  }
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_global.js":
/*!*********************************************************!*\
  !*** ./node_modules/core-js/library/modules/_global.js ***!
  \*********************************************************/
/***/ ((module) => {

// https://github.com/zloirock/core-js/issues/86#issuecomment-115759028
var global = module.exports = typeof window != 'undefined' && window.Math == Math
  ? window : typeof self != 'undefined' && self.Math == Math ? self
  // eslint-disable-next-line no-new-func
  : Function('return this')();
if (typeof __g == 'number') __g = global; // eslint-disable-line no-undef


/***/ }),

/***/ "./node_modules/core-js/library/modules/_has.js":
/*!******************************************************!*\
  !*** ./node_modules/core-js/library/modules/_has.js ***!
  \******************************************************/
/***/ ((module) => {

var hasOwnProperty = {}.hasOwnProperty;
module.exports = function (it, key) {
  return hasOwnProperty.call(it, key);
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_hide.js":
/*!*******************************************************!*\
  !*** ./node_modules/core-js/library/modules/_hide.js ***!
  \*******************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var dP = __webpack_require__(/*! ./_object-dp */ "./node_modules/core-js/library/modules/_object-dp.js");
var createDesc = __webpack_require__(/*! ./_property-desc */ "./node_modules/core-js/library/modules/_property-desc.js");
module.exports = __webpack_require__(/*! ./_descriptors */ "./node_modules/core-js/library/modules/_descriptors.js") ? function (object, key, value) {
  return dP.f(object, key, createDesc(1, value));
} : function (object, key, value) {
  object[key] = value;
  return object;
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_ie8-dom-define.js":
/*!*****************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_ie8-dom-define.js ***!
  \*****************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

module.exports = !__webpack_require__(/*! ./_descriptors */ "./node_modules/core-js/library/modules/_descriptors.js") && !__webpack_require__(/*! ./_fails */ "./node_modules/core-js/library/modules/_fails.js")(function () {
  return Object.defineProperty(__webpack_require__(/*! ./_dom-create */ "./node_modules/core-js/library/modules/_dom-create.js")('div'), 'a', { get: function () { return 7; } }).a != 7;
});


/***/ }),

/***/ "./node_modules/core-js/library/modules/_iobject.js":
/*!**********************************************************!*\
  !*** ./node_modules/core-js/library/modules/_iobject.js ***!
  \**********************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// fallback for non-array-like ES3 and non-enumerable old V8 strings
var cof = __webpack_require__(/*! ./_cof */ "./node_modules/core-js/library/modules/_cof.js");
// eslint-disable-next-line no-prototype-builtins
module.exports = Object('z').propertyIsEnumerable(0) ? Object : function (it) {
  return cof(it) == 'String' ? it.split('') : Object(it);
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_is-object.js":
/*!************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_is-object.js ***!
  \************************************************************/
/***/ ((module) => {

module.exports = function (it) {
  return typeof it === 'object' ? it !== null : typeof it === 'function';
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_library.js":
/*!**********************************************************!*\
  !*** ./node_modules/core-js/library/modules/_library.js ***!
  \**********************************************************/
/***/ ((module) => {

module.exports = true;


/***/ }),

/***/ "./node_modules/core-js/library/modules/_object-dp.js":
/*!************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_object-dp.js ***!
  \************************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

var anObject = __webpack_require__(/*! ./_an-object */ "./node_modules/core-js/library/modules/_an-object.js");
var IE8_DOM_DEFINE = __webpack_require__(/*! ./_ie8-dom-define */ "./node_modules/core-js/library/modules/_ie8-dom-define.js");
var toPrimitive = __webpack_require__(/*! ./_to-primitive */ "./node_modules/core-js/library/modules/_to-primitive.js");
var dP = Object.defineProperty;

exports.f = __webpack_require__(/*! ./_descriptors */ "./node_modules/core-js/library/modules/_descriptors.js") ? Object.defineProperty : function defineProperty(O, P, Attributes) {
  anObject(O);
  P = toPrimitive(P, true);
  anObject(Attributes);
  if (IE8_DOM_DEFINE) try {
    return dP(O, P, Attributes);
  } catch (e) { /* empty */ }
  if ('get' in Attributes || 'set' in Attributes) throw TypeError('Accessors not supported!');
  if ('value' in Attributes) O[P] = Attributes.value;
  return O;
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_object-keys-internal.js":
/*!***********************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_object-keys-internal.js ***!
  \***********************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var has = __webpack_require__(/*! ./_has */ "./node_modules/core-js/library/modules/_has.js");
var toIObject = __webpack_require__(/*! ./_to-iobject */ "./node_modules/core-js/library/modules/_to-iobject.js");
var arrayIndexOf = __webpack_require__(/*! ./_array-includes */ "./node_modules/core-js/library/modules/_array-includes.js")(false);
var IE_PROTO = __webpack_require__(/*! ./_shared-key */ "./node_modules/core-js/library/modules/_shared-key.js")('IE_PROTO');

module.exports = function (object, names) {
  var O = toIObject(object);
  var i = 0;
  var result = [];
  var key;
  for (key in O) if (key != IE_PROTO) has(O, key) && result.push(key);
  // Don't enum bug & hidden keys
  while (names.length > i) if (has(O, key = names[i++])) {
    ~arrayIndexOf(result, key) || result.push(key);
  }
  return result;
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_object-keys.js":
/*!**************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_object-keys.js ***!
  \**************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// 19.1.2.14 / 15.2.3.14 Object.keys(O)
var $keys = __webpack_require__(/*! ./_object-keys-internal */ "./node_modules/core-js/library/modules/_object-keys-internal.js");
var enumBugKeys = __webpack_require__(/*! ./_enum-bug-keys */ "./node_modules/core-js/library/modules/_enum-bug-keys.js");

module.exports = Object.keys || function keys(O) {
  return $keys(O, enumBugKeys);
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_object-pie.js":
/*!*************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_object-pie.js ***!
  \*************************************************************/
/***/ ((__unused_webpack_module, exports) => {

exports.f = {}.propertyIsEnumerable;


/***/ }),

/***/ "./node_modules/core-js/library/modules/_object-sap.js":
/*!*************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_object-sap.js ***!
  \*************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// most Object methods by ES6 should accept primitives
var $export = __webpack_require__(/*! ./_export */ "./node_modules/core-js/library/modules/_export.js");
var core = __webpack_require__(/*! ./_core */ "./node_modules/core-js/library/modules/_core.js");
var fails = __webpack_require__(/*! ./_fails */ "./node_modules/core-js/library/modules/_fails.js");
module.exports = function (KEY, exec) {
  var fn = (core.Object || {})[KEY] || Object[KEY];
  var exp = {};
  exp[KEY] = exec(fn);
  $export($export.S + $export.F * fails(function () { fn(1); }), 'Object', exp);
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_object-to-array.js":
/*!******************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_object-to-array.js ***!
  \******************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var DESCRIPTORS = __webpack_require__(/*! ./_descriptors */ "./node_modules/core-js/library/modules/_descriptors.js");
var getKeys = __webpack_require__(/*! ./_object-keys */ "./node_modules/core-js/library/modules/_object-keys.js");
var toIObject = __webpack_require__(/*! ./_to-iobject */ "./node_modules/core-js/library/modules/_to-iobject.js");
var isEnum = __webpack_require__(/*! ./_object-pie */ "./node_modules/core-js/library/modules/_object-pie.js").f;
module.exports = function (isEntries) {
  return function (it) {
    var O = toIObject(it);
    var keys = getKeys(O);
    var length = keys.length;
    var i = 0;
    var result = [];
    var key;
    while (length > i) {
      key = keys[i++];
      if (!DESCRIPTORS || isEnum.call(O, key)) {
        result.push(isEntries ? [key, O[key]] : O[key]);
      }
    }
    return result;
  };
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_property-desc.js":
/*!****************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_property-desc.js ***!
  \****************************************************************/
/***/ ((module) => {

module.exports = function (bitmap, value) {
  return {
    enumerable: !(bitmap & 1),
    configurable: !(bitmap & 2),
    writable: !(bitmap & 4),
    value: value
  };
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_shared-key.js":
/*!*************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_shared-key.js ***!
  \*************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var shared = __webpack_require__(/*! ./_shared */ "./node_modules/core-js/library/modules/_shared.js")('keys');
var uid = __webpack_require__(/*! ./_uid */ "./node_modules/core-js/library/modules/_uid.js");
module.exports = function (key) {
  return shared[key] || (shared[key] = uid(key));
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_shared.js":
/*!*********************************************************!*\
  !*** ./node_modules/core-js/library/modules/_shared.js ***!
  \*********************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var core = __webpack_require__(/*! ./_core */ "./node_modules/core-js/library/modules/_core.js");
var global = __webpack_require__(/*! ./_global */ "./node_modules/core-js/library/modules/_global.js");
var SHARED = '__core-js_shared__';
var store = global[SHARED] || (global[SHARED] = {});

(module.exports = function (key, value) {
  return store[key] || (store[key] = value !== undefined ? value : {});
})('versions', []).push({
  version: core.version,
  mode: __webpack_require__(/*! ./_library */ "./node_modules/core-js/library/modules/_library.js") ? 'pure' : 'global',
  copyright: '© 2019 Denis Pushkarev (zloirock.ru)'
});


/***/ }),

/***/ "./node_modules/core-js/library/modules/_to-absolute-index.js":
/*!********************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_to-absolute-index.js ***!
  \********************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var toInteger = __webpack_require__(/*! ./_to-integer */ "./node_modules/core-js/library/modules/_to-integer.js");
var max = Math.max;
var min = Math.min;
module.exports = function (index, length) {
  index = toInteger(index);
  return index < 0 ? max(index + length, 0) : min(index, length);
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_to-integer.js":
/*!*************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_to-integer.js ***!
  \*************************************************************/
/***/ ((module) => {

// 7.1.4 ToInteger
var ceil = Math.ceil;
var floor = Math.floor;
module.exports = function (it) {
  return isNaN(it = +it) ? 0 : (it > 0 ? floor : ceil)(it);
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_to-iobject.js":
/*!*************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_to-iobject.js ***!
  \*************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// to indexed object, toObject with fallback for non-array-like ES3 strings
var IObject = __webpack_require__(/*! ./_iobject */ "./node_modules/core-js/library/modules/_iobject.js");
var defined = __webpack_require__(/*! ./_defined */ "./node_modules/core-js/library/modules/_defined.js");
module.exports = function (it) {
  return IObject(defined(it));
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_to-length.js":
/*!************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_to-length.js ***!
  \************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// 7.1.15 ToLength
var toInteger = __webpack_require__(/*! ./_to-integer */ "./node_modules/core-js/library/modules/_to-integer.js");
var min = Math.min;
module.exports = function (it) {
  return it > 0 ? min(toInteger(it), 0x1fffffffffffff) : 0; // pow(2, 53) - 1 == 9007199254740991
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_to-object.js":
/*!************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_to-object.js ***!
  \************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// 7.1.13 ToObject(argument)
var defined = __webpack_require__(/*! ./_defined */ "./node_modules/core-js/library/modules/_defined.js");
module.exports = function (it) {
  return Object(defined(it));
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_to-primitive.js":
/*!***************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_to-primitive.js ***!
  \***************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// 7.1.1 ToPrimitive(input [, PreferredType])
var isObject = __webpack_require__(/*! ./_is-object */ "./node_modules/core-js/library/modules/_is-object.js");
// instead of the ES6 spec version, we didn't implement @@toPrimitive case
// and the second argument - flag - preferred type is a string
module.exports = function (it, S) {
  if (!isObject(it)) return it;
  var fn, val;
  if (S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;
  if (typeof (fn = it.valueOf) == 'function' && !isObject(val = fn.call(it))) return val;
  if (!S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;
  throw TypeError("Can't convert object to primitive value");
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/_uid.js":
/*!******************************************************!*\
  !*** ./node_modules/core-js/library/modules/_uid.js ***!
  \******************************************************/
/***/ ((module) => {

var id = 0;
var px = Math.random();
module.exports = function (key) {
  return 'Symbol('.concat(key === undefined ? '' : key, ')_', (++id + px).toString(36));
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/es6.object.define-property.js":
/*!****************************************************************************!*\
  !*** ./node_modules/core-js/library/modules/es6.object.define-property.js ***!
  \****************************************************************************/
/***/ ((__unused_webpack_module, __unused_webpack_exports, __webpack_require__) => {

var $export = __webpack_require__(/*! ./_export */ "./node_modules/core-js/library/modules/_export.js");
// 19.1.2.4 / 15.2.3.6 Object.defineProperty(O, P, Attributes)
$export($export.S + $export.F * !__webpack_require__(/*! ./_descriptors */ "./node_modules/core-js/library/modules/_descriptors.js"), 'Object', { defineProperty: __webpack_require__(/*! ./_object-dp */ "./node_modules/core-js/library/modules/_object-dp.js").f });


/***/ }),

/***/ "./node_modules/core-js/library/modules/es6.object.keys.js":
/*!*****************************************************************!*\
  !*** ./node_modules/core-js/library/modules/es6.object.keys.js ***!
  \*****************************************************************/
/***/ ((__unused_webpack_module, __unused_webpack_exports, __webpack_require__) => {

// 19.1.2.14 Object.keys(O)
var toObject = __webpack_require__(/*! ./_to-object */ "./node_modules/core-js/library/modules/_to-object.js");
var $keys = __webpack_require__(/*! ./_object-keys */ "./node_modules/core-js/library/modules/_object-keys.js");

__webpack_require__(/*! ./_object-sap */ "./node_modules/core-js/library/modules/_object-sap.js")('keys', function () {
  return function keys(it) {
    return $keys(toObject(it));
  };
});


/***/ }),

/***/ "./node_modules/core-js/library/modules/es7.object.values.js":
/*!*******************************************************************!*\
  !*** ./node_modules/core-js/library/modules/es7.object.values.js ***!
  \*******************************************************************/
/***/ ((__unused_webpack_module, __unused_webpack_exports, __webpack_require__) => {

// https://github.com/tc39/proposal-object-values-entries
var $export = __webpack_require__(/*! ./_export */ "./node_modules/core-js/library/modules/_export.js");
var $values = __webpack_require__(/*! ./_object-to-array */ "./node_modules/core-js/library/modules/_object-to-array.js")(false);

$export($export.S, 'Object', {
  values: function values(it) {
    return $values(it);
  }
});


/***/ }),

/***/ "jquery":
/*!*************************!*\
  !*** external "jQuery" ***!
  \*************************/
/***/ ((module) => {

"use strict";
module.exports = window["jQuery"];

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be in strict mode.
(() => {
"use strict";
/*!***********************************!*\
  !*** ./js/pages/employee/form.js ***!
  \***********************************/
/* provided dependency */ var $ = __webpack_require__(/*! jquery */ "jquery");


var _EmployeeForm = __webpack_require__(/*! ./EmployeeForm */ "./js/pages/employee/EmployeeForm.js");

var _EmployeeForm2 = _interopRequireDefault(_EmployeeForm);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

$(function () {
  new _EmployeeForm2.default();
}); /**
     * Copyright since 2007 PrestaShop SA and Contributors
     * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
     *
     * NOTICE OF LICENSE
     *
     * This source file is subject to the Open Software License (OSL 3.0)
     * that is bundled with this package in the file LICENSE.md.
     * It is also available through the world-wide-web at this URL:
     * https://opensource.org/licenses/OSL-3.0
     * If you did not receive a copy of the license and are unable to
     * obtain it through the world-wide-web, please send an email
     * to license@prestashop.com so we can send you a copy immediately.
     *
     * DISCLAIMER
     *
     * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
     * versions in the future. If you wish to customize PrestaShop for your
     * needs please refer to https://devdocs.prestashop.com/ for more information.
     *
     * @author    PrestaShop SA and Contributors <contact@prestashop.com>
     * @copyright Since 2007 PrestaShop SA and Contributors
     * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
     */
})();

window.employee_form = __webpack_exports__;
/******/ })()
;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9bbmFtZV0vLi9qcy9jb21wb25lbnRzL2FkZG9ucy1jb25uZWN0b3IuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vanMvY29tcG9uZW50cy9jaGFuZ2UtcGFzc3dvcmQtaGFuZGxlci5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9qcy9jb21wb25lbnRzL2Zvcm0vY2hhbmdlLXBhc3N3b3JkLWNvbnRyb2wuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vanMvY29tcG9uZW50cy9mb3JtL2Nob2ljZS10cmVlLmpzIiwid2VicGFjazovL1tuYW1lXS8uL2pzL2NvbXBvbmVudHMvcGFzc3dvcmQtdmFsaWRhdG9yLmpzIiwid2VicGFjazovL1tuYW1lXS8uL2pzL3BhZ2VzL2VtcGxveWVlL0VtcGxveWVlRm9ybS5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9qcy9wYWdlcy9lbXBsb3llZS9lbXBsb3llZS1mb3JtLW1hcC5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvYmFiZWwtcnVudGltZS9jb3JlLWpzL29iamVjdC9kZWZpbmUtcHJvcGVydHkuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2JhYmVsLXJ1bnRpbWUvY29yZS1qcy9vYmplY3Qva2V5cy5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvYmFiZWwtcnVudGltZS9jb3JlLWpzL29iamVjdC92YWx1ZXMuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jbGFzc0NhbGxDaGVjay5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvYmFiZWwtcnVudGltZS9oZWxwZXJzL2NyZWF0ZUNsYXNzLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvZm4vb2JqZWN0L2RlZmluZS1wcm9wZXJ0eS5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L2ZuL29iamVjdC9rZXlzLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvZm4vb2JqZWN0L3ZhbHVlcy5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX2EtZnVuY3Rpb24uanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19hbi1vYmplY3QuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19hcnJheS1pbmNsdWRlcy5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX2NvZi5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX2NvcmUuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19jdHguanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19kZWZpbmVkLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fZGVzY3JpcHRvcnMuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19kb20tY3JlYXRlLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fZW51bS1idWcta2V5cy5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX2V4cG9ydC5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX2ZhaWxzLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fZ2xvYmFsLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9faGFzLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9faGlkZS5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX2llOC1kb20tZGVmaW5lLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9faW9iamVjdC5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX2lzLW9iamVjdC5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX2xpYnJhcnkuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19vYmplY3QtZHAuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19vYmplY3Qta2V5cy1pbnRlcm5hbC5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX29iamVjdC1rZXlzLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fb2JqZWN0LXBpZS5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX29iamVjdC1zYXAuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19vYmplY3QtdG8tYXJyYXkuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19wcm9wZXJ0eS1kZXNjLmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fc2hhcmVkLWtleS5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX3NoYXJlZC5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX3RvLWFic29sdXRlLWluZGV4LmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fdG8taW50ZWdlci5qcyIsIndlYnBhY2s6Ly9bbmFtZV0vLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX3RvLWlvYmplY3QuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL190by1sZW5ndGguanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL190by1vYmplY3QuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL190by1wcmltaXRpdmUuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL191aWQuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL2VzNi5vYmplY3QuZGVmaW5lLXByb3BlcnR5LmpzIiwid2VicGFjazovL1tuYW1lXS8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9lczYub2JqZWN0LmtleXMuanMiLCJ3ZWJwYWNrOi8vW25hbWVdLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL2VzNy5vYmplY3QudmFsdWVzLmpzIiwid2VicGFjazovL1tuYW1lXS9leHRlcm5hbCBcImpRdWVyeVwiIiwid2VicGFjazovL1tuYW1lXS93ZWJwYWNrL2Jvb3RzdHJhcCIsIndlYnBhY2s6Ly9bbmFtZV0vLi9qcy9wYWdlcy9lbXBsb3llZS9mb3JtLmpzIl0sIm5hbWVzIjpbIndpbmRvdyIsIiQiLCJBZGRvbnNDb25uZWN0b3IiLCJhZGRvbnNDb25uZWN0Rm9ybVNlbGVjdG9yIiwibG9hZGluZ1NwaW5uZXJTZWxlY3RvciIsIiRsb2FkaW5nU3Bpbm5lciIsImluaXRFdmVudHMiLCJvbiIsImV2ZW50IiwiJGZvcm0iLCJjdXJyZW50VGFyZ2V0IiwicHJldmVudERlZmF1bHQiLCJzdG9wUHJvcGFnYXRpb24iLCJjb25uZWN0IiwiYXR0ciIsInNlcmlhbGl6ZSIsImFkZG9uc0Nvbm5lY3RVcmwiLCJmb3JtRGF0YSIsImFqYXgiLCJtZXRob2QiLCJ1cmwiLCJkYXRhVHlwZSIsImRhdGEiLCJiZWZvcmVTZW5kIiwic2hvdyIsImhpZGUiLCJ0aGVuIiwicmVzcG9uc2UiLCJzdWNjZXNzIiwibG9jYXRpb24iLCJyZWxvYWQiLCJncm93bCIsImVycm9yIiwibWVzc2FnZSIsImZhZGVJbiIsIkNoYW5nZVBhc3N3b3JkSGFuZGxlciIsInBhc3N3b3JkU3RyZW5ndGhGZWVkYmFja0NvbnRhaW5lclNlbGVjdG9yIiwib3B0aW9ucyIsIm1pbkxlbmd0aCIsIiRmZWVkYmFja0NvbnRhaW5lciIsIndhdGNoUGFzc3dvcmRTdHJlbmd0aCIsIiRpbnB1dCIsImdlbmVyYXRlUGFzc3dvcmQiLCJwYXNzeSIsInJlcXVpcmVtZW50cyIsImxlbmd0aCIsIm1pbiIsImNoYXJhY3RlcnMiLCJlYWNoIiwiaW5kZXgiLCJlbGVtZW50IiwiJG91dHB1dENvbnRhaW5lciIsImluc2VydEFmdGVyIiwic3RyZW5ndGgiLCJ2YWxpZCIsImRpc3BsYXlGZWVkYmFjayIsInBhc3N3b3JkU3RyZW5ndGgiLCJpc1Bhc3N3b3JkVmFsaWQiLCJmZWVkYmFjayIsImdldFBhc3N3b3JkU3RyZW5ndGhGZWVkYmFjayIsInRleHQiLCJyZW1vdmVDbGFzcyIsImFkZENsYXNzIiwiZWxlbWVudENsYXNzIiwidG9nZ2xlQ2xhc3MiLCJMT1ciLCJmaW5kIiwiTUVESVVNIiwiSElHSCIsIkVYVFJFTUUiLCJFcnJvciIsIkNoYW5nZVBhc3N3b3JkQ29udHJvbCIsImlucHV0c0Jsb2NrU2VsZWN0b3IiLCJzaG93QnV0dG9uU2VsZWN0b3IiLCJoaWRlQnV0dG9uU2VsZWN0b3IiLCJnZW5lcmF0ZVBhc3N3b3JkQnV0dG9uU2VsZWN0b3IiLCJvbGRQYXNzd29yZElucHV0U2VsZWN0b3IiLCJuZXdQYXNzd29yZElucHV0U2VsZWN0b3IiLCJjb25maXJtTmV3UGFzc3dvcmRJbnB1dFNlbGVjdG9yIiwiZ2VuZXJhdGVkUGFzc3dvcmREaXNwbGF5U2VsZWN0b3IiLCIkaW5wdXRzQmxvY2siLCIkbmV3UGFzc3dvcmRJbnB1dHMiLCIkY29weVBhc3N3b3JkSW5wdXRzIiwiYWRkIiwiJHN1Ym1pdHRhYmxlSW5wdXRzIiwicGFzc3dvcmRIYW5kbGVyIiwicGFzc3dvcmRWYWxpZGF0b3IiLCJQYXNzd29yZFZhbGlkYXRvciIsImhpZGVJbnB1dHNCbG9jayIsImRvY3VtZW50IiwiZSIsInNob3dJbnB1dHNCbG9jayIsInZhbCIsImNoZWNrUGFzc3dvcmRWYWxpZGl0eSIsImNsb3Nlc3QiLCJpcyIsIiRmaXJzdFBhc3N3b3JkRXJyb3JDb250YWluZXIiLCJwYXJlbnQiLCIkc2Vjb25kUGFzc3dvcmRFcnJvckNvbnRhaW5lciIsImdldFBhc3N3b3JkTGVuZ3RoVmFsaWRhdGlvbk1lc3NhZ2UiLCJpc1Bhc3N3b3JkTGVuZ3RoVmFsaWQiLCJnZXRQYXNzd29yZENvbmZpcm1hdGlvblZhbGlkYXRpb25NZXNzYWdlIiwiaXNQYXNzd29yZE1hdGNoaW5nQ29uZmlybWF0aW9uIiwiaXNQYXNzd29yZFRvb1Nob3J0IiwiaXNQYXNzd29yZFRvb0xvbmciLCJyZW1vdmVBdHRyIiwiJGVsIiwiQ2hvaWNlVHJlZSIsInRyZWVTZWxlY3RvciIsIiRjb250YWluZXIiLCIkaW5wdXRXcmFwcGVyIiwidG9nZ2xlQ2hpbGRUcmVlIiwiJGFjdGlvbiIsInRvZ2dsZVRyZWUiLCJlbmFibGVBdXRvQ2hlY2tDaGlsZHJlbiIsImVuYWJsZUFsbElucHV0cyIsImRpc2FibGVBbGxJbnB1dHMiLCIkY2xpY2tlZENoZWNrYm94IiwiJGl0ZW1XaXRoQ2hpbGRyZW4iLCJwcm9wIiwiJHBhcmVudFdyYXBwZXIiLCJoYXNDbGFzcyIsIiRwYXJlbnRDb250YWluZXIiLCJhY3Rpb24iLCJjb25maWciLCJleHBhbmQiLCJjb2xsYXBzZSIsIm5leHRBY3Rpb24iLCJpY29uIiwiaXRlbSIsIiRpdGVtIiwicGFzc3dvcmRJbnB1dFNlbGVjdG9yIiwiY29uZmlybVBhc3N3b3JkSW5wdXRTZWxlY3RvciIsIm5ld1Bhc3N3b3JkSW5wdXQiLCJxdWVyeVNlbGVjdG9yIiwiY29uZmlybVBhc3N3b3JkSW5wdXQiLCJtaW5QYXNzd29yZExlbmd0aCIsIm1heFBhc3N3b3JkTGVuZ3RoIiwidmFsdWUiLCJFbXBsb3llZUZvcm0iLCJzaG9wQ2hvaWNlVHJlZVNlbGVjdG9yIiwiZW1wbG95ZWVGb3JtTWFwIiwic2hvcENob2ljZVRyZWUiLCJlbXBsb3llZVByb2ZpbGVTZWxlY3RvciIsInByb2ZpbGVTZWxlY3QiLCJ0YWJzRHJvcGRvd25TZWxlY3RvciIsImRlZmF1bHRQYWdlU2VsZWN0IiwiYWRkb25zQ29ubmVjdEZvcm0iLCJhZGRvbnNMb2dpbkJ1dHRvbiIsImNoYW5nZVBhc3N3b3JkSW5wdXRzQmxvY2siLCJzaG93Q2hhbmdlUGFzc3dvcmRCbG9ja0J1dHRvbiIsImhpZGVDaGFuZ2VQYXNzd29yZEJsb2NrQnV0dG9uIiwiZ2VuZXJhdGVQYXNzd29yZEJ1dHRvbiIsIm9sZFBhc3N3b3JkSW5wdXQiLCJjb25maXJtTmV3UGFzc3dvcmRJbnB1dCIsImdlbmVyYXRlZFBhc3N3b3JkRGlzcGxheUlucHV0IiwicGFzc3dvcmRTdHJlbmd0aEZlZWRiYWNrQ29udGFpbmVyIiwidG9nZ2xlU2hvcFRyZWUiLCIkZW1wbG95ZWVQcm9maWxlc0Ryb3Bkb3duIiwiZ2V0VGFic1VybCIsImdldCIsInByb2ZpbGVJZCIsInRhYnMiLCJyZWxvYWRUYWJzRHJvcGRvd24iLCJhY2Nlc3NpYmxlVGFicyIsIiR0YWJzRHJvcGRvd24iLCJlbXB0eSIsImZvckVhY2giLCJhY2Nlc3NpYmxlVGFiIiwiY2hpbGRyZW4iLCJuYW1lIiwiJG9wdGdyb3VwIiwiY3JlYXRlT3B0aW9uR3JvdXAiLCJjaGlsZEtleSIsImFwcGVuZCIsImNyZWF0ZU9wdGlvbiIsImlkX3RhYiIsIiRlbXBsb3llZVByb2ZpbGVEcm9wZG93biIsInN1cGVyQWRtaW5Qcm9maWxlSWQiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Y0F5QllBLE07SUFBTEMsQyxXQUFBQSxDOztBQUVQOzs7OztJQUlxQkMsZTtBQUNuQiwyQkFDRUMseUJBREYsRUFFRUMsc0JBRkYsRUFHRTtBQUFBOztBQUNBLFNBQUtELHlCQUFMLEdBQWlDQSx5QkFBakM7QUFDQSxTQUFLRSxlQUFMLEdBQXVCSixFQUFFRyxzQkFBRixDQUF2Qjs7QUFFQSxTQUFLRSxVQUFMOztBQUVBLFdBQU8sRUFBUDtBQUNEOztBQUVEOzs7Ozs7Ozs7aUNBS2E7QUFBQTs7QUFDWEwsUUFBRSxNQUFGLEVBQVVNLEVBQVYsQ0FDRSxRQURGLEVBRUUsS0FBS0oseUJBRlAsRUFHRSxVQUFDSyxLQUFELEVBQVc7QUFDVCxZQUFNQyxRQUFRUixFQUFFTyxNQUFNRSxhQUFSLENBQWQ7QUFDQUYsY0FBTUcsY0FBTjtBQUNBSCxjQUFNSSxlQUFOOztBQUVBLGNBQUtDLE9BQUwsQ0FBYUosTUFBTUssSUFBTixDQUFXLFFBQVgsQ0FBYixFQUFtQ0wsTUFBTU0sU0FBTixFQUFuQztBQUNELE9BVEg7QUFXRDs7QUFFRDs7Ozs7Ozs7Ozs7NEJBUVFDLGdCLEVBQWtCQyxRLEVBQVU7QUFBQTs7QUFDbENoQixRQUFFaUIsSUFBRixDQUFPO0FBQ0xDLGdCQUFRLE1BREg7QUFFTEMsYUFBS0osZ0JBRkE7QUFHTEssa0JBQVUsTUFITDtBQUlMQyxjQUFNTCxRQUpEO0FBS0xNLG9CQUFZLHNCQUFNO0FBQ2hCLGlCQUFLbEIsZUFBTCxDQUFxQm1CLElBQXJCO0FBQ0F2QixZQUFFLDJCQUFGLEVBQStCLE9BQUtFLHlCQUFwQyxFQUErRHNCLElBQS9EO0FBQ0Q7QUFSSSxPQUFQLEVBU0dDLElBVEgsQ0FTUSxVQUFDQyxRQUFELEVBQWM7QUFDcEIsWUFBSUEsU0FBU0MsT0FBVCxLQUFxQixDQUF6QixFQUE0QjtBQUMxQjVCLGlCQUFPNkIsUUFBUCxDQUFnQkMsTUFBaEI7QUFDRCxTQUZELE1BRU87QUFDTDdCLFlBQUU4QixLQUFGLENBQVFDLEtBQVIsQ0FBYztBQUNaQyxxQkFBU04sU0FBU007QUFETixXQUFkOztBQUlBLGlCQUFLNUIsZUFBTCxDQUFxQm9CLElBQXJCO0FBQ0F4QixZQUFFLDJCQUFGLEVBQStCLE9BQUtFLHlCQUFwQyxFQUErRCtCLE1BQS9EO0FBQ0Q7QUFDRixPQXBCRCxFQW9CRyxZQUFNO0FBQ1BqQyxVQUFFOEIsS0FBRixDQUFRQyxLQUFSLENBQWM7QUFDWkMsbUJBQVNoQyxFQUFFLE9BQUtFLHlCQUFQLEVBQWtDbUIsSUFBbEMsQ0FBdUMsZUFBdkM7QUFERyxTQUFkOztBQUlBLGVBQUtqQixlQUFMLENBQXFCb0IsSUFBckI7QUFDQXhCLFVBQUUsMkJBQUYsRUFBK0IsT0FBS0UseUJBQXBDLEVBQStEcUIsSUFBL0Q7QUFDRCxPQTNCRDtBQTRCRDs7Ozs7a0JBckVrQnRCLGU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQy9CckI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Y0F5QllGLE07SUFBTEMsQyxXQUFBQSxDOztBQUVQOzs7Ozs7SUFLcUJrQyxxQjtBQUNuQixpQ0FBWUMseUNBQVosRUFBcUU7QUFBQTs7QUFBQSxRQUFkQyxPQUFjLHVFQUFKLEVBQUk7QUFBQTs7QUFDbkU7QUFDQSxTQUFLQyxTQUFMLEdBQWlCRCxRQUFRQyxTQUFSLElBQXFCLENBQXRDOztBQUVBO0FBQ0EsU0FBS0Msa0JBQUwsR0FBMEJ0QyxFQUFFbUMseUNBQUYsQ0FBMUI7O0FBRUEsV0FBTztBQUNMSSw2QkFBdUIsK0JBQUNDLE1BQUQ7QUFBQSxlQUFZLE1BQUtELHFCQUFMLENBQTJCQyxNQUEzQixDQUFaO0FBQUEsT0FEbEI7QUFFTEMsd0JBQWtCLDBCQUFDRCxNQUFEO0FBQUEsZUFBWSxNQUFLQyxnQkFBTCxDQUFzQkQsTUFBdEIsQ0FBWjtBQUFBO0FBRmIsS0FBUDtBQUlEOztBQUVEOzs7Ozs7Ozs7MENBS3NCQSxNLEVBQVE7QUFBQTs7QUFDNUJ4QyxRQUFFMEMsS0FBRixDQUFRQyxZQUFSLENBQXFCQyxNQUFyQixDQUE0QkMsR0FBNUIsR0FBa0MsS0FBS1IsU0FBdkM7QUFDQXJDLFFBQUUwQyxLQUFGLENBQVFDLFlBQVIsQ0FBcUJHLFVBQXJCLEdBQWtDLE9BQWxDOztBQUVBTixhQUFPTyxJQUFQLENBQVksVUFBQ0MsS0FBRCxFQUFRQyxPQUFSLEVBQW9CO0FBQzlCLFlBQU1DLG1CQUFtQmxELEVBQUUsUUFBRixDQUF6Qjs7QUFFQWtELHlCQUFpQkMsV0FBakIsQ0FBNkJuRCxFQUFFaUQsT0FBRixDQUE3Qjs7QUFFQWpELFVBQUVpRCxPQUFGLEVBQVdQLEtBQVgsQ0FBaUIsVUFBQ1UsUUFBRCxFQUFXQyxLQUFYLEVBQXFCO0FBQ3BDLGlCQUFLQyxlQUFMLENBQXFCSixnQkFBckIsRUFBdUNFLFFBQXZDLEVBQWlEQyxLQUFqRDtBQUNELFNBRkQ7QUFHRCxPQVJEO0FBU0Q7O0FBRUQ7Ozs7Ozs7O3FDQUtpQmIsTSxFQUFRO0FBQ3ZCQSxhQUFPRSxLQUFQLENBQWEsVUFBYixFQUF5QixLQUFLTCxTQUE5QjtBQUNEOztBQUVEOzs7Ozs7Ozs7Ozs7b0NBU2dCYSxnQixFQUFrQkssZ0IsRUFBa0JDLGUsRUFBaUI7QUFDbkUsVUFBTUMsV0FBVyxLQUFLQywyQkFBTCxDQUFpQ0gsZ0JBQWpDLENBQWpCO0FBQ0FMLHVCQUFpQlMsSUFBakIsQ0FBc0JGLFNBQVN6QixPQUEvQjtBQUNBa0IsdUJBQWlCVSxXQUFqQixDQUE2Qix1Q0FBN0I7QUFDQVYsdUJBQWlCVyxRQUFqQixDQUEwQkosU0FBU0ssWUFBbkM7QUFDQVosdUJBQWlCYSxXQUFqQixDQUE2QixRQUE3QixFQUF1QyxDQUFDUCxlQUF4QztBQUNEOztBQUVEOzs7Ozs7Ozs7OztnREFRNEJKLFEsRUFBVTtBQUNwQyxjQUFRQSxRQUFSO0FBQ0UsYUFBS3BELEVBQUUwQyxLQUFGLENBQVFVLFFBQVIsQ0FBaUJZLEdBQXRCO0FBQ0UsaUJBQU87QUFDTGhDLHFCQUFTLEtBQUtNLGtCQUFMLENBQXdCMkIsSUFBeEIsQ0FBNkIsZUFBN0IsRUFBOENOLElBQTlDLEVBREo7QUFFTEcsMEJBQWM7QUFGVCxXQUFQOztBQUtGLGFBQUs5RCxFQUFFMEMsS0FBRixDQUFRVSxRQUFSLENBQWlCYyxNQUF0QjtBQUNFLGlCQUFPO0FBQ0xsQyxxQkFBUyxLQUFLTSxrQkFBTCxDQUF3QjJCLElBQXhCLENBQTZCLGtCQUE3QixFQUFpRE4sSUFBakQsRUFESjtBQUVMRywwQkFBYztBQUZULFdBQVA7O0FBS0YsYUFBSzlELEVBQUUwQyxLQUFGLENBQVFVLFFBQVIsQ0FBaUJlLElBQXRCO0FBQ0UsaUJBQU87QUFDTG5DLHFCQUFTLEtBQUtNLGtCQUFMLENBQXdCMkIsSUFBeEIsQ0FBNkIsZ0JBQTdCLEVBQStDTixJQUEvQyxFQURKO0FBRUxHLDBCQUFjO0FBRlQsV0FBUDs7QUFLRixhQUFLOUQsRUFBRTBDLEtBQUYsQ0FBUVUsUUFBUixDQUFpQmdCLE9BQXRCO0FBQ0UsaUJBQU87QUFDTHBDLHFCQUFTLEtBQUtNLGtCQUFMLENBQXdCMkIsSUFBeEIsQ0FBNkIsbUJBQTdCLEVBQWtETixJQUFsRCxFQURKO0FBRUxHLDBCQUFjO0FBRlQsV0FBUDs7QUFLRjtBQUNFLGdCQUFNLElBQUlPLEtBQUosQ0FBVSxzQ0FBVixDQUFOO0FBMUJKO0FBNEJEOzs7OztrQkFqR2tCbkMscUI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNQckI7Ozs7QUFDQTs7Ozs7O0FBMUJBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O2NBNEJZbkMsTTtJQUFMQyxDLFdBQUFBLEM7O0FBRVA7Ozs7OztJQUtxQnNFLHFCO0FBQ25CLGlDQUNFQyxtQkFERixFQUVFQyxrQkFGRixFQUdFQyxrQkFIRixFQUlFQyw4QkFKRixFQUtFQyx3QkFMRixFQU1FQyx3QkFORixFQU9FQywrQkFQRixFQVFFQyxnQ0FSRixFQVNFM0MseUNBVEYsRUFVRTtBQUFBOztBQUNBO0FBQ0EsU0FBSzRDLFlBQUwsR0FBb0IvRSxFQUFFdUUsbUJBQUYsQ0FBcEI7O0FBRUE7QUFDQSxTQUFLQyxrQkFBTCxHQUEwQkEsa0JBQTFCOztBQUVBO0FBQ0EsU0FBS0Msa0JBQUwsR0FBMEJBLGtCQUExQjs7QUFFQTtBQUNBLFNBQUtDLDhCQUFMLEdBQXNDQSw4QkFBdEM7O0FBRUE7QUFDQSxTQUFLQyx3QkFBTCxHQUFnQ0Esd0JBQWhDOztBQUVBO0FBQ0EsU0FBS0Msd0JBQUwsR0FBZ0NBLHdCQUFoQzs7QUFFQTtBQUNBLFNBQUtDLCtCQUFMLEdBQXVDQSwrQkFBdkM7O0FBRUE7QUFDQSxTQUFLQyxnQ0FBTCxHQUF3Q0EsZ0NBQXhDOztBQUVBO0FBQ0EsU0FBS0Usa0JBQUwsR0FBMEIsS0FBS0QsWUFBTCxDQUN2QmQsSUFEdUIsQ0FDbEIsS0FBS1csd0JBRGEsQ0FBMUI7O0FBR0E7QUFDQSxTQUFLSyxtQkFBTCxHQUEyQixLQUFLRixZQUFMLENBQ3hCZCxJQUR3QixDQUNuQixLQUFLWSwrQkFEYyxFQUV4QkssR0FGd0IsQ0FFcEIsS0FBS0osZ0NBRmUsQ0FBM0I7O0FBSUE7QUFDQSxTQUFLSyxrQkFBTCxHQUEwQixLQUFLSixZQUFMLENBQ3ZCZCxJQUR1QixDQUNsQixLQUFLVSx3QkFEYSxFQUV2Qk8sR0FGdUIsQ0FFbkIsS0FBS04sd0JBRmMsRUFHdkJNLEdBSHVCLENBR25CLEtBQUtMLCtCQUhjLENBQTFCOztBQUtBLFNBQUtPLGVBQUwsR0FBdUIsSUFBSWxELCtCQUFKLENBQ3JCQyx5Q0FEcUIsQ0FBdkI7O0FBSUEsU0FBS2tELGlCQUFMLEdBQXlCLElBQUlDLDJCQUFKLENBQ3ZCLEtBQUtWLHdCQURrQixFQUV2QixLQUFLQywrQkFGa0IsQ0FBekI7O0FBS0EsU0FBS1UsZUFBTDtBQUNBLFNBQUtsRixVQUFMOztBQUVBLFdBQU8sRUFBUDtBQUNEOztBQUVEOzs7Ozs7Ozs7aUNBS2E7QUFBQTs7QUFDWDtBQUNBTCxRQUFFd0YsUUFBRixFQUFZbEYsRUFBWixDQUFlLE9BQWYsRUFBd0IsS0FBS2tFLGtCQUE3QixFQUFpRCxVQUFDaUIsQ0FBRCxFQUFPO0FBQ3RELGNBQUtqRSxJQUFMLENBQVV4QixFQUFFeUYsRUFBRWhGLGFBQUosQ0FBVjtBQUNBLGNBQUtpRixlQUFMO0FBQ0QsT0FIRDs7QUFLQTFGLFFBQUV3RixRQUFGLEVBQVlsRixFQUFaLENBQWUsT0FBZixFQUF3QixLQUFLbUUsa0JBQTdCLEVBQWlELFlBQU07QUFDckQsY0FBS2MsZUFBTDtBQUNBLGNBQUtoRSxJQUFMLENBQVV2QixFQUFFLE1BQUt3RSxrQkFBUCxDQUFWO0FBQ0QsT0FIRDs7QUFLQTtBQUNBLFdBQUtZLGVBQUwsQ0FBcUI3QyxxQkFBckIsQ0FBMkMsS0FBS3lDLGtCQUFoRDs7QUFFQWhGLFFBQUV3RixRQUFGLEVBQVlsRixFQUFaLENBQWUsT0FBZixFQUF3QixLQUFLb0UsOEJBQTdCLEVBQTZELFlBQU07QUFDakU7QUFDQSxjQUFLVSxlQUFMLENBQXFCM0MsZ0JBQXJCLENBQXNDLE1BQUt1QyxrQkFBM0M7O0FBRUE7QUFDQSxjQUFLQyxtQkFBTCxDQUF5QlUsR0FBekIsQ0FBNkIsTUFBS1gsa0JBQUwsQ0FBd0JXLEdBQXhCLEVBQTdCO0FBQ0EsY0FBS0MscUJBQUw7QUFDRCxPQVBEOztBQVNBO0FBQ0E1RixRQUFFd0YsUUFBRixFQUFZbEYsRUFBWixDQUNFLE9BREYsRUFFSyxLQUFLc0Usd0JBRlYsU0FFc0MsS0FBS0MsK0JBRjNDLEVBR0UsWUFBTTtBQUNKLGNBQUtlLHFCQUFMO0FBQ0QsT0FMSDs7QUFRQTtBQUNBNUYsUUFBRXdGLFFBQUYsRUFBWWxGLEVBQVosQ0FBZSxRQUFmLEVBQXlCTixFQUFFLEtBQUsyRSx3QkFBUCxFQUFpQ2tCLE9BQWpDLENBQXlDLE1BQXpDLENBQXpCLEVBQTJFLFVBQUN0RixLQUFELEVBQVc7QUFDcEY7QUFDQSxZQUFJUCxFQUFFLE1BQUsyRSx3QkFBUCxFQUFpQ21CLEVBQWpDLENBQW9DLFdBQXBDLENBQUosRUFBc0Q7QUFDcEQ7QUFDRDs7QUFFRCxZQUFJLENBQUMsTUFBS1QsaUJBQUwsQ0FBdUI3QixlQUF2QixFQUFMLEVBQStDO0FBQzdDakQsZ0JBQU1HLGNBQU47QUFDRDtBQUNGLE9BVEQ7QUFVRDs7QUFFRDs7Ozs7Ozs7NENBS3dCO0FBQ3RCLFVBQU1xRiwrQkFBK0IvRixFQUFFLEtBQUs0RSx3QkFBUCxFQUFpQ29CLE1BQWpDLEdBQTBDL0IsSUFBMUMsQ0FBK0MsWUFBL0MsQ0FBckM7QUFDQSxVQUFNZ0MsZ0NBQWdDakcsRUFBRSxLQUFLNkUsK0JBQVAsRUFBd0NtQixNQUF4QyxHQUFpRC9CLElBQWpELENBQXNELFlBQXRELENBQXRDOztBQUVBOEIsbUNBQ0dwQyxJQURILENBQ1EsS0FBS3VDLGtDQUFMLEVBRFIsRUFFR25DLFdBRkgsQ0FFZSxhQUZmLEVBRThCLENBQUMsS0FBS3NCLGlCQUFMLENBQXVCYyxxQkFBdkIsRUFGL0I7QUFHQUYsb0NBQ0d0QyxJQURILENBQ1EsS0FBS3lDLHdDQUFMLEVBRFIsRUFFR3JDLFdBRkgsQ0FFZSxhQUZmLEVBRThCLENBQUMsS0FBS3NCLGlCQUFMLENBQXVCZ0IsOEJBQXZCLEVBRi9CO0FBR0Q7O0FBRUQ7Ozs7Ozs7Ozs7K0RBTzJDO0FBQ3pDLFVBQUksQ0FBQyxLQUFLaEIsaUJBQUwsQ0FBdUJnQiw4QkFBdkIsRUFBTCxFQUE4RDtBQUM1RCxlQUFPckcsRUFBRSxLQUFLNkUsK0JBQVAsRUFBd0N4RCxJQUF4QyxDQUE2QyxrQkFBN0MsQ0FBUDtBQUNEOztBQUVELGFBQU8sRUFBUDtBQUNEOztBQUVEOzs7Ozs7Ozs7O3lEQU9xQztBQUNuQyxVQUFJLEtBQUtnRSxpQkFBTCxDQUF1QmlCLGtCQUF2QixFQUFKLEVBQWlEO0FBQy9DLGVBQU90RyxFQUFFLEtBQUs0RSx3QkFBUCxFQUFpQ3ZELElBQWpDLENBQXNDLG9CQUF0QyxDQUFQO0FBQ0Q7O0FBRUQsVUFBSSxLQUFLZ0UsaUJBQUwsQ0FBdUJrQixpQkFBdkIsRUFBSixFQUFnRDtBQUM5QyxlQUFPdkcsRUFBRSxLQUFLNEUsd0JBQVAsRUFBaUN2RCxJQUFqQyxDQUFzQyxtQkFBdEMsQ0FBUDtBQUNEOztBQUVELGFBQU8sRUFBUDtBQUNEOztBQUVEOzs7Ozs7OztzQ0FLa0I7QUFDaEIsV0FBS0UsSUFBTCxDQUFVLEtBQUt3RCxZQUFmO0FBQ0EsV0FBS0ksa0JBQUwsQ0FBd0JxQixVQUF4QixDQUFtQyxVQUFuQztBQUNBLFdBQUtyQixrQkFBTCxDQUF3QnRFLElBQXhCLENBQTZCLFVBQTdCLEVBQXlDLFVBQXpDO0FBQ0Q7O0FBRUQ7Ozs7Ozs7O3NDQUtrQjtBQUNoQixXQUFLVyxJQUFMLENBQVUsS0FBS3VELFlBQWY7QUFDQSxXQUFLSSxrQkFBTCxDQUF3QnRFLElBQXhCLENBQTZCLFVBQTdCLEVBQXlDLFVBQXpDO0FBQ0EsV0FBS3NFLGtCQUFMLENBQXdCcUIsVUFBeEIsQ0FBbUMsVUFBbkM7QUFDQSxXQUFLekIsWUFBTCxDQUFrQmQsSUFBbEIsQ0FBdUIsT0FBdkIsRUFBZ0MwQixHQUFoQyxDQUFvQyxFQUFwQztBQUNBLFdBQUtaLFlBQUwsQ0FBa0JkLElBQWxCLENBQXVCLFlBQXZCLEVBQXFDTixJQUFyQyxDQUEwQyxFQUExQztBQUNEOztBQUVEOzs7Ozs7Ozs7O3lCQU9LOEMsRyxFQUFLO0FBQ1JBLFVBQUk1QyxRQUFKLENBQWEsUUFBYjtBQUNEOztBQUVEOzs7Ozs7Ozs7O3lCQU9LNEMsRyxFQUFLO0FBQ1JBLFVBQUk3QyxXQUFKLENBQWdCLFFBQWhCO0FBQ0Q7Ozs7O2tCQXBOa0JVLHFCOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNuQ3JCOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O2NBeUJZdkUsTTtJQUFMQyxDLFdBQUFBLEM7O0FBRVA7Ozs7SUFHcUIwRyxVO0FBQ25COzs7QUFHQSxzQkFBWUMsWUFBWixFQUEwQjtBQUFBOztBQUFBOztBQUN4QixTQUFLQyxVQUFMLEdBQWtCNUcsRUFBRTJHLFlBQUYsQ0FBbEI7O0FBRUEsU0FBS0MsVUFBTCxDQUFnQnRHLEVBQWhCLENBQW1CLE9BQW5CLEVBQTRCLG1CQUE1QixFQUFpRCxVQUFDQyxLQUFELEVBQVc7QUFDMUQsVUFBTXNHLGdCQUFnQjdHLEVBQUVPLE1BQU1FLGFBQVIsQ0FBdEI7O0FBRUEsWUFBS3FHLGVBQUwsQ0FBcUJELGFBQXJCO0FBQ0QsS0FKRDs7QUFNQSxTQUFLRCxVQUFMLENBQWdCdEcsRUFBaEIsQ0FBbUIsT0FBbkIsRUFBNEIsK0JBQTVCLEVBQTZELFVBQUNDLEtBQUQsRUFBVztBQUN0RSxVQUFNd0csVUFBVS9HLEVBQUVPLE1BQU1FLGFBQVIsQ0FBaEI7O0FBRUEsWUFBS3VHLFVBQUwsQ0FBZ0JELE9BQWhCO0FBQ0QsS0FKRDs7QUFNQSxXQUFPO0FBQ0xFLCtCQUF5QjtBQUFBLGVBQU0sTUFBS0EsdUJBQUwsRUFBTjtBQUFBLE9BRHBCO0FBRUxDLHVCQUFpQjtBQUFBLGVBQU0sTUFBS0EsZUFBTCxFQUFOO0FBQUEsT0FGWjtBQUdMQyx3QkFBa0I7QUFBQSxlQUFNLE1BQUtBLGdCQUFMLEVBQU47QUFBQTtBQUhiLEtBQVA7QUFLRDs7QUFFRDs7Ozs7Ozs4Q0FHMEI7QUFDeEIsV0FBS1AsVUFBTCxDQUFnQnRHLEVBQWhCLENBQW1CLFFBQW5CLEVBQTZCLHdCQUE3QixFQUF1RCxVQUFDQyxLQUFELEVBQVc7QUFDaEUsWUFBTTZHLG1CQUFtQnBILEVBQUVPLE1BQU1FLGFBQVIsQ0FBekI7QUFDQSxZQUFNNEcsb0JBQW9CRCxpQkFBaUJ2QixPQUFqQixDQUF5QixJQUF6QixDQUExQjs7QUFFQXdCLDBCQUNHcEQsSUFESCxDQUNRLDJCQURSLEVBRUdxRCxJQUZILENBRVEsU0FGUixFQUVtQkYsaUJBQWlCdEIsRUFBakIsQ0FBb0IsVUFBcEIsQ0FGbkI7QUFHRCxPQVBEO0FBUUQ7O0FBRUQ7Ozs7OztzQ0FHa0I7QUFDaEIsV0FBS2MsVUFBTCxDQUFnQjNDLElBQWhCLENBQXFCLE9BQXJCLEVBQThCdUMsVUFBOUIsQ0FBeUMsVUFBekM7QUFDRDs7QUFFRDs7Ozs7O3VDQUdtQjtBQUNqQixXQUFLSSxVQUFMLENBQWdCM0MsSUFBaEIsQ0FBcUIsT0FBckIsRUFBOEJwRCxJQUE5QixDQUFtQyxVQUFuQyxFQUErQyxVQUEvQztBQUNEOztBQUVEOzs7Ozs7Ozs7O29DQU9nQmdHLGEsRUFBZTtBQUM3QixVQUFNVSxpQkFBaUJWLGNBQWNoQixPQUFkLENBQXNCLElBQXRCLENBQXZCOztBQUVBLFVBQUkwQixlQUFlQyxRQUFmLENBQXdCLFVBQXhCLENBQUosRUFBeUM7QUFDdkNELHVCQUNHM0QsV0FESCxDQUNlLFVBRGYsRUFFR0MsUUFGSCxDQUVZLFdBRlo7O0FBSUE7QUFDRDs7QUFFRCxVQUFJMEQsZUFBZUMsUUFBZixDQUF3QixXQUF4QixDQUFKLEVBQTBDO0FBQ3hDRCx1QkFDRzNELFdBREgsQ0FDZSxXQURmLEVBRUdDLFFBRkgsQ0FFWSxVQUZaO0FBR0Q7QUFDRjs7QUFFRDs7Ozs7Ozs7OzsrQkFPV2tELE8sRUFBUztBQUNsQixVQUFNVSxtQkFBbUJWLFFBQVFsQixPQUFSLENBQWdCLDJCQUFoQixDQUF6QjtBQUNBLFVBQU02QixTQUFTWCxRQUFRMUYsSUFBUixDQUFhLFFBQWIsQ0FBZjs7QUFFQTtBQUNBLFVBQU1zRyxTQUFTO0FBQ2I5RCxrQkFBVTtBQUNSK0Qsa0JBQVEsVUFEQTtBQUVSQyxvQkFBVTtBQUZGLFNBREc7QUFLYmpFLHFCQUFhO0FBQ1hnRSxrQkFBUSxXQURHO0FBRVhDLG9CQUFVO0FBRkMsU0FMQTtBQVNiQyxvQkFBWTtBQUNWRixrQkFBUSxVQURFO0FBRVZDLG9CQUFVO0FBRkEsU0FUQztBQWFibEUsY0FBTTtBQUNKaUUsa0JBQVEsZ0JBREo7QUFFSkMsb0JBQVU7QUFGTixTQWJPO0FBaUJiRSxjQUFNO0FBQ0pILGtCQUFRLGdCQURKO0FBRUpDLG9CQUFVO0FBRk47QUFqQk8sT0FBZjs7QUF1QkFKLHVCQUFpQnhELElBQWpCLENBQXNCLElBQXRCLEVBQTRCbEIsSUFBNUIsQ0FBaUMsVUFBQ0MsS0FBRCxFQUFRZ0YsSUFBUixFQUFpQjtBQUNoRCxZQUFNQyxRQUFRakksRUFBRWdJLElBQUYsQ0FBZDs7QUFFQSxZQUFJQyxNQUFNVCxRQUFOLENBQWVHLE9BQU8vRCxXQUFQLENBQW1COEQsTUFBbkIsQ0FBZixDQUFKLEVBQWdEO0FBQzlDTyxnQkFBTXJFLFdBQU4sQ0FBa0IrRCxPQUFPL0QsV0FBUCxDQUFtQjhELE1BQW5CLENBQWxCLEVBQ0c3RCxRQURILENBQ1k4RCxPQUFPOUQsUUFBUCxDQUFnQjZELE1BQWhCLENBRFo7QUFFRDtBQUNGLE9BUEQ7O0FBU0FYLGNBQVExRixJQUFSLENBQWEsUUFBYixFQUF1QnNHLE9BQU9HLFVBQVAsQ0FBa0JKLE1BQWxCLENBQXZCO0FBQ0FYLGNBQVE5QyxJQUFSLENBQWEsaUJBQWIsRUFBZ0NOLElBQWhDLENBQXFDb0QsUUFBUTFGLElBQVIsQ0FBYXNHLE9BQU9JLElBQVAsQ0FBWUwsTUFBWixDQUFiLENBQXJDO0FBQ0FYLGNBQVE5QyxJQUFSLENBQWEsaUJBQWIsRUFBZ0NOLElBQWhDLENBQXFDb0QsUUFBUTFGLElBQVIsQ0FBYXNHLE9BQU9oRSxJQUFQLENBQVkrRCxNQUFaLENBQWIsQ0FBckM7QUFDRDs7Ozs7a0JBOUhrQmhCLFU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzlCckI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUF5QkE7Ozs7O0lBS3FCcEIsaUI7QUFDbkI7Ozs7O0FBS0EsNkJBQVk0QyxxQkFBWixFQUFzRjtBQUFBLFFBQW5EQyw0QkFBbUQsdUVBQXBCLElBQW9CO0FBQUEsUUFBZC9GLE9BQWMsdUVBQUosRUFBSTtBQUFBOztBQUNwRixTQUFLZ0csZ0JBQUwsR0FBd0I1QyxTQUFTNkMsYUFBVCxDQUF1QkgscUJBQXZCLENBQXhCO0FBQ0EsU0FBS0ksb0JBQUwsR0FBNEI5QyxTQUFTNkMsYUFBVCxDQUF1QkYsNEJBQXZCLENBQTVCOztBQUVBO0FBQ0EsU0FBS0ksaUJBQUwsR0FBeUJuRyxRQUFRbUcsaUJBQVIsSUFBNkIsQ0FBdEQ7O0FBRUE7QUFDQSxTQUFLQyxpQkFBTCxHQUF5QnBHLFFBQVFvRyxpQkFBUixJQUE2QixHQUF0RDtBQUNEOztBQUVEOzs7Ozs7Ozs7c0NBS2tCO0FBQ2hCLFVBQUksS0FBS0Ysb0JBQUwsSUFBNkIsQ0FBQyxLQUFLakMsOEJBQUwsRUFBbEMsRUFBeUU7QUFDdkUsZUFBTyxLQUFQO0FBQ0Q7O0FBRUQsYUFBTyxLQUFLRixxQkFBTCxFQUFQO0FBQ0Q7O0FBRUQ7Ozs7Ozs7OzRDQUt3QjtBQUN0QixhQUFPLENBQUMsS0FBS0csa0JBQUwsRUFBRCxJQUE4QixDQUFDLEtBQUtDLGlCQUFMLEVBQXRDO0FBQ0Q7O0FBRUQ7Ozs7Ozs7O3FEQUtpQztBQUMvQixVQUFJLENBQUMsS0FBSytCLG9CQUFWLEVBQWdDO0FBQzlCLGNBQU0sSUFBSWpFLEtBQUosQ0FBVSxvRUFBVixDQUFOO0FBQ0Q7O0FBRUQsVUFBSSxLQUFLaUUsb0JBQUwsQ0FBMEJHLEtBQTFCLEtBQW9DLEVBQXhDLEVBQTRDO0FBQzFDLGVBQU8sSUFBUDtBQUNEOztBQUVELGFBQU8sS0FBS0wsZ0JBQUwsQ0FBc0JLLEtBQXRCLEtBQWdDLEtBQUtILG9CQUFMLENBQTBCRyxLQUFqRTtBQUNEOztBQUVEOzs7Ozs7Ozt5Q0FLcUI7QUFDbkIsYUFBTyxLQUFLTCxnQkFBTCxDQUFzQkssS0FBdEIsQ0FBNEI3RixNQUE1QixHQUFxQyxLQUFLMkYsaUJBQWpEO0FBQ0Q7O0FBRUQ7Ozs7Ozs7O3dDQUtvQjtBQUNsQixhQUFPLEtBQUtILGdCQUFMLENBQXNCSyxLQUF0QixDQUE0QjdGLE1BQTVCLEdBQXFDLEtBQUs0RixpQkFBakQ7QUFDRDs7Ozs7a0JBeEVrQmxELGlCOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDTHJCOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7Ozs7QUFFQTs7O0FBOUJBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0lBaUNxQm9ELFk7QUFDbkIsMEJBQWM7QUFBQTs7QUFDWixTQUFLQyxzQkFBTCxHQUE4QkMsMEJBQWdCQyxjQUE5QztBQUNBLFNBQUtBLGNBQUwsR0FBc0IsSUFBSW5DLG9CQUFKLENBQWUsS0FBS2lDLHNCQUFwQixDQUF0QjtBQUNBLFNBQUtHLHVCQUFMLEdBQStCRiwwQkFBZ0JHLGFBQS9DO0FBQ0EsU0FBS0Msb0JBQUwsR0FBNEJKLDBCQUFnQkssaUJBQTVDOztBQUVBLFNBQUtKLGNBQUwsQ0FBb0I1Qix1QkFBcEI7O0FBRUEsUUFBSWhILHlCQUFKLENBQ0UySSwwQkFBZ0JNLGlCQURsQixFQUVFTiwwQkFBZ0JPLGlCQUZsQjs7QUFLQSxRQUFJN0UsK0JBQUosQ0FDRXNFLDBCQUFnQlEseUJBRGxCLEVBRUVSLDBCQUFnQlMsNkJBRmxCLEVBR0VULDBCQUFnQlUsNkJBSGxCLEVBSUVWLDBCQUFnQlcsc0JBSmxCLEVBS0VYLDBCQUFnQlksZ0JBTGxCLEVBTUVaLDBCQUFnQlIsZ0JBTmxCLEVBT0VRLDBCQUFnQmEsdUJBUGxCLEVBUUViLDBCQUFnQmMsNkJBUmxCLEVBU0VkLDBCQUFnQmUsaUNBVGxCOztBQVlBLFNBQUt0SixVQUFMO0FBQ0EsU0FBS3VKLGNBQUw7O0FBRUEsV0FBTyxFQUFQO0FBQ0Q7O0FBRUQ7Ozs7Ozs7OztpQ0FLYTtBQUFBOztBQUNYLFVBQU1DLDRCQUE0QjdKLENBQUNBLENBQUMsS0FBSzhJLHVCQUFQLENBQWxDO0FBQ0EsVUFBTWdCLGFBQWFELDBCQUEwQnhJLElBQTFCLENBQStCLGNBQS9CLENBQW5COztBQUVBckIsT0FBQ0EsQ0FBQ3dGLFFBQUYsRUFBWWxGLEVBQVosQ0FBZSxRQUFmLEVBQXlCLEtBQUt3SSx1QkFBOUIsRUFBdUQ7QUFBQSxlQUFNLE1BQUtjLGNBQUwsRUFBTjtBQUFBLE9BQXZEOztBQUVBO0FBQ0E1SixPQUFDQSxDQUFDd0YsUUFBRixFQUFZbEYsRUFBWixDQUFlLFFBQWYsRUFBeUIsS0FBS3dJLHVCQUE5QixFQUF1RCxVQUFDdkksS0FBRCxFQUFXO0FBQ2hFUCxTQUFDQSxDQUFDK0osR0FBRixDQUNFRCxVQURGLEVBRUU7QUFDRUUscUJBQVdoSyxDQUFDQSxDQUFDTyxNQUFNRSxhQUFSLEVBQXVCa0YsR0FBdkI7QUFEYixTQUZGLEVBS0UsVUFBQ3NFLElBQUQsRUFBVTtBQUNSLGdCQUFLQyxrQkFBTCxDQUF3QkQsSUFBeEI7QUFDRCxTQVBILEVBUUUsTUFSRjtBQVVELE9BWEQ7QUFZRDs7QUFFRDs7Ozs7Ozs7Ozt1Q0FPbUJFLGMsRUFBZ0I7QUFBQTs7QUFDakMsVUFBTUMsZ0JBQWdCcEssQ0FBQ0EsQ0FBQyxLQUFLZ0osb0JBQVAsQ0FBdEI7O0FBRUFvQixvQkFBY0MsS0FBZDs7QUFFQSw0QkFBY0YsY0FBZCxFQUE4QkcsT0FBOUIsQ0FBc0MsVUFBQ0MsYUFBRCxFQUFtQjtBQUN2RCxZQUFJQSxjQUFjQyxRQUFkLENBQXVCNUgsTUFBdkIsR0FBZ0MsQ0FBaEMsSUFBcUMySCxjQUFjRSxJQUF2RCxFQUE2RDtBQUMzRDtBQUNBLGNBQU1DLFlBQVksT0FBS0MsaUJBQUwsQ0FBdUJKLGNBQWNFLElBQXJDLENBQWxCOztBQUVBLDhCQUFZRixjQUFjQyxRQUExQixFQUFvQ0YsT0FBcEMsQ0FBNEMsVUFBQ00sUUFBRCxFQUFjO0FBQ3hELGdCQUFJTCxjQUFjQyxRQUFkLENBQXVCSSxRQUF2QixFQUFpQ0gsSUFBckMsRUFBMkM7QUFDekNDLHdCQUFVRyxNQUFWLENBQ0UsT0FBS0MsWUFBTCxDQUNFUCxjQUFjQyxRQUFkLENBQXVCSSxRQUF2QixFQUFpQ0gsSUFEbkMsRUFFRUYsY0FBY0MsUUFBZCxDQUF1QkksUUFBdkIsRUFBaUNHLE1BRm5DLENBREY7QUFLRDtBQUNGLFdBUkQ7O0FBVUFYLHdCQUFjUyxNQUFkLENBQXFCSCxTQUFyQjtBQUNELFNBZkQsTUFlTyxJQUFJSCxjQUFjRSxJQUFsQixFQUF3QjtBQUM3QjtBQUNBTCx3QkFBY1MsTUFBZCxDQUNFLE9BQUtDLFlBQUwsQ0FDRVAsY0FBY0UsSUFEaEIsRUFFRUYsY0FBY1EsTUFGaEIsQ0FERjtBQU1EO0FBQ0YsT0F6QkQ7QUEwQkQ7O0FBRUQ7Ozs7Ozs7O3FDQUtpQjtBQUNmLFVBQU1DLDJCQUEyQmhMLENBQUNBLENBQUMsS0FBSzhJLHVCQUFQLENBQWpDO0FBQ0EsVUFBTW1DLHNCQUFzQkQseUJBQXlCM0osSUFBekIsQ0FBOEIsZUFBOUIsQ0FBNUI7QUFDQXJCLE9BQUNBLENBQUMsS0FBSzJJLHNCQUFQLEVBQ0c5QyxPQURILENBQ1csYUFEWCxFQUVHOUIsV0FGSCxDQUVlLFFBRmYsRUFFeUJpSCx5QkFBeUJyRixHQUF6QixPQUFtQ3NGLG1CQUY1RDtBQUdEOztBQUVEOzs7Ozs7Ozs7Ozs7c0NBU2tCUixJLEVBQU07QUFDdEIsYUFBT3pLLENBQUNBLHVCQUFxQnlLLElBQXRCLFFBQVA7QUFDRDs7QUFFRDs7Ozs7Ozs7Ozs7OztpQ0FVYUEsSSxFQUFNaEMsSyxFQUFPO0FBQ3hCLGFBQU96SSxDQUFDQSxxQkFBbUJ5SSxLQUFwQixVQUE4QmdDLElBQTlCLGVBQVA7QUFDRDs7Ozs7a0JBeElrQi9CLFk7Ozs7Ozs7Ozs7Ozs7Ozs7QUNqQ3JCOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBeUJBOzs7a0JBR2U7QUFDYkcsa0JBQWdCLDRCQURIO0FBRWJFLGlCQUFlLG1CQUZGO0FBR2JFLHFCQUFtQix3QkFITjtBQUliQyxxQkFBbUIsc0JBSk47QUFLYkMscUJBQW1CLG1CQUxOOztBQU9iO0FBQ0FDLDZCQUEyQiwyQkFSZDtBQVNiQyxpQ0FBK0IscUJBVGxCO0FBVWJDLGlDQUErQiw0QkFWbEI7QUFXYkMsMEJBQXdCLG9EQVhYO0FBWWJDLG9CQUFrQix3Q0FaTDtBQWFicEIsb0JBQWtCLDhDQWJMO0FBY2JxQiwyQkFBeUIsK0NBZFo7QUFlYkMsaUNBQStCLDhDQWZsQjtBQWdCYkMscUNBQW1DO0FBaEJ0QixDOzs7Ozs7Ozs7O0FDNUJmLGtCQUFrQixZQUFZLG1CQUFPLENBQUMsOEdBQTJDLHNCOzs7Ozs7Ozs7O0FDQWpGLGtCQUFrQixZQUFZLG1CQUFPLENBQUMsd0ZBQWdDLHNCOzs7Ozs7Ozs7O0FDQXRFLGtCQUFrQixZQUFZLG1CQUFPLENBQUMsNEZBQWtDLHNCOzs7Ozs7Ozs7OztBQ0EzRDs7QUFFYixrQkFBa0I7O0FBRWxCLGVBQWU7QUFDZjtBQUNBO0FBQ0E7QUFDQSxFOzs7Ozs7Ozs7OztBQ1JhOztBQUViLGtCQUFrQjs7QUFFbEIsc0JBQXNCLG1CQUFPLENBQUMseUdBQW1DOztBQUVqRTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0YsZUFBZTtBQUNmO0FBQ0EsbUJBQW1CLGtCQUFrQjtBQUNyQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQyxHOzs7Ozs7Ozs7O0FDMUJELG1CQUFPLENBQUMsc0hBQTBDO0FBQ2xELGNBQWMsd0dBQXFDO0FBQ25EO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNKQSxtQkFBTyxDQUFDLGdHQUErQjtBQUN2Qyw4SEFBMkQ7Ozs7Ozs7Ozs7O0FDRDNELG1CQUFPLENBQUMsb0dBQWlDO0FBQ3pDLGdJQUE2RDs7Ozs7Ozs7Ozs7QUNEN0Q7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7O0FDSEEsZUFBZSxtQkFBTyxDQUFDLDBFQUFjO0FBQ3JDO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7OztBQ0pBO0FBQ0E7QUFDQSxnQkFBZ0IsbUJBQU8sQ0FBQyw0RUFBZTtBQUN2QyxlQUFlLG1CQUFPLENBQUMsMEVBQWM7QUFDckMsc0JBQXNCLG1CQUFPLENBQUMsMEZBQXNCO0FBQ3BEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSyxZQUFZLGVBQWU7QUFDaEM7QUFDQSxLQUFLO0FBQ0w7QUFDQTs7Ozs7Ozs7Ozs7QUN0QkEsaUJBQWlCOztBQUVqQjtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7O0FDSkEsNkJBQTZCO0FBQzdCLHVDQUF1Qzs7Ozs7Ozs7Ozs7QUNEdkM7QUFDQSxnQkFBZ0IsbUJBQU8sQ0FBQyw0RUFBZTtBQUN2QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7O0FDbkJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7O0FDSkE7QUFDQSxrQkFBa0IsbUJBQU8sQ0FBQyxrRUFBVTtBQUNwQyxpQ0FBaUMsUUFBUSxtQkFBbUIsVUFBVSxFQUFFLEVBQUU7QUFDMUUsQ0FBQzs7Ozs7Ozs7Ozs7QUNIRCxlQUFlLG1CQUFPLENBQUMsMEVBQWM7QUFDckMsZUFBZSxrR0FBNkI7QUFDNUM7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNOQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNIQSxhQUFhLG1CQUFPLENBQUMsb0VBQVc7QUFDaEMsV0FBVyxtQkFBTyxDQUFDLGdFQUFTO0FBQzVCLFVBQVUsbUJBQU8sQ0FBQyw4REFBUTtBQUMxQixXQUFXLG1CQUFPLENBQUMsZ0VBQVM7QUFDNUIsVUFBVSxtQkFBTyxDQUFDLDhEQUFRO0FBQzFCOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUVBQWlFO0FBQ2pFO0FBQ0Esa0ZBQWtGO0FBQ2xGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXO0FBQ1gsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSwrQ0FBK0M7QUFDL0M7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsY0FBYztBQUNkLGNBQWM7QUFDZCxjQUFjO0FBQ2QsY0FBYztBQUNkLGVBQWU7QUFDZixlQUFlO0FBQ2YsZUFBZTtBQUNmLGdCQUFnQjtBQUNoQjs7Ozs7Ozs7Ozs7QUM3REE7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNOQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EseUNBQXlDOzs7Ozs7Ozs7OztBQ0x6Qyx1QkFBdUI7QUFDdkI7QUFDQTtBQUNBOzs7Ozs7Ozs7OztBQ0hBLFNBQVMsbUJBQU8sQ0FBQywwRUFBYztBQUMvQixpQkFBaUIsbUJBQU8sQ0FBQyxrRkFBa0I7QUFDM0MsaUJBQWlCLG1CQUFPLENBQUMsOEVBQWdCO0FBQ3pDO0FBQ0EsQ0FBQztBQUNEO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNQQSxrQkFBa0IsbUJBQU8sQ0FBQyw4RUFBZ0IsTUFBTSxtQkFBTyxDQUFDLGtFQUFVO0FBQ2xFLCtCQUErQixtQkFBTyxDQUFDLDRFQUFlLGdCQUFnQixtQkFBbUIsVUFBVSxFQUFFLEVBQUU7QUFDdkcsQ0FBQzs7Ozs7Ozs7Ozs7QUNGRDtBQUNBLFVBQVUsbUJBQU8sQ0FBQyw4REFBUTtBQUMxQjtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNMQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7O0FDRkE7Ozs7Ozs7Ozs7O0FDQUEsZUFBZSxtQkFBTyxDQUFDLDBFQUFjO0FBQ3JDLHFCQUFxQixtQkFBTyxDQUFDLG9GQUFtQjtBQUNoRCxrQkFBa0IsbUJBQU8sQ0FBQyxnRkFBaUI7QUFDM0M7O0FBRUEsU0FBUyxHQUFHLG1CQUFPLENBQUMsOEVBQWdCO0FBQ3BDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHLFlBQVk7QUFDZjtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNmQSxVQUFVLG1CQUFPLENBQUMsOERBQVE7QUFDMUIsZ0JBQWdCLG1CQUFPLENBQUMsNEVBQWU7QUFDdkMsbUJBQW1CLG1CQUFPLENBQUMsb0ZBQW1CO0FBQzlDLGVBQWUsbUJBQU8sQ0FBQyw0RUFBZTs7QUFFdEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7OztBQ2hCQTtBQUNBLFlBQVksbUJBQU8sQ0FBQyxnR0FBeUI7QUFDN0Msa0JBQWtCLG1CQUFPLENBQUMsa0ZBQWtCOztBQUU1QztBQUNBO0FBQ0E7Ozs7Ozs7Ozs7O0FDTkEsU0FBUyxLQUFLOzs7Ozs7Ozs7OztBQ0FkO0FBQ0EsY0FBYyxtQkFBTyxDQUFDLG9FQUFXO0FBQ2pDLFdBQVcsbUJBQU8sQ0FBQyxnRUFBUztBQUM1QixZQUFZLG1CQUFPLENBQUMsa0VBQVU7QUFDOUI7QUFDQSw2QkFBNkI7QUFDN0I7QUFDQTtBQUNBLHFEQUFxRCxPQUFPLEVBQUU7QUFDOUQ7Ozs7Ozs7Ozs7O0FDVEEsa0JBQWtCLG1CQUFPLENBQUMsOEVBQWdCO0FBQzFDLGNBQWMsbUJBQU8sQ0FBQyw4RUFBZ0I7QUFDdEMsZ0JBQWdCLG1CQUFPLENBQUMsNEVBQWU7QUFDdkMsYUFBYSxtR0FBMEI7QUFDdkM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNwQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNQQSxhQUFhLG1CQUFPLENBQUMsb0VBQVc7QUFDaEMsVUFBVSxtQkFBTyxDQUFDLDhEQUFRO0FBQzFCO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNKQSxXQUFXLG1CQUFPLENBQUMsZ0VBQVM7QUFDNUIsYUFBYSxtQkFBTyxDQUFDLG9FQUFXO0FBQ2hDO0FBQ0Esa0RBQWtEOztBQUVsRDtBQUNBLHFFQUFxRTtBQUNyRSxDQUFDO0FBQ0Q7QUFDQSxRQUFRLG1CQUFPLENBQUMsc0VBQVk7QUFDNUI7QUFDQSxDQUFDOzs7Ozs7Ozs7OztBQ1hELGdCQUFnQixtQkFBTyxDQUFDLDRFQUFlO0FBQ3ZDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNOQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7O0FDTEE7QUFDQSxjQUFjLG1CQUFPLENBQUMsc0VBQVk7QUFDbEMsY0FBYyxtQkFBTyxDQUFDLHNFQUFZO0FBQ2xDO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNMQTtBQUNBLGdCQUFnQixtQkFBTyxDQUFDLDRFQUFlO0FBQ3ZDO0FBQ0E7QUFDQSwyREFBMkQ7QUFDM0Q7Ozs7Ozs7Ozs7O0FDTEE7QUFDQSxjQUFjLG1CQUFPLENBQUMsc0VBQVk7QUFDbEM7QUFDQTtBQUNBOzs7Ozs7Ozs7OztBQ0pBO0FBQ0EsZUFBZSxtQkFBTyxDQUFDLDBFQUFjO0FBQ3JDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7OztBQ1hBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7O0FDSkEsY0FBYyxtQkFBTyxDQUFDLG9FQUFXO0FBQ2pDO0FBQ0EsaUNBQWlDLG1CQUFPLENBQUMsOEVBQWdCLGNBQWMsaUJBQWlCLGlHQUF5QixFQUFFOzs7Ozs7Ozs7OztBQ0ZuSDtBQUNBLGVBQWUsbUJBQU8sQ0FBQywwRUFBYztBQUNyQyxZQUFZLG1CQUFPLENBQUMsOEVBQWdCOztBQUVwQyxtQkFBTyxDQUFDLDRFQUFlO0FBQ3ZCO0FBQ0E7QUFDQTtBQUNBLENBQUM7Ozs7Ozs7Ozs7O0FDUkQ7QUFDQSxjQUFjLG1CQUFPLENBQUMsb0VBQVc7QUFDakMsY0FBYyxtQkFBTyxDQUFDLHNGQUFvQjs7QUFFMUM7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDOzs7Ozs7Ozs7Ozs7QUNSRCxrQzs7Ozs7O1VDQUE7VUFDQTs7VUFFQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTs7VUFFQTtVQUNBOztVQUVBO1VBQ0E7VUFDQTs7Ozs7Ozs7Ozs7OztBQ0dBOzs7Ozs7QUFFQTNKLENBQUNBLENBQUMsWUFBTTtBQUNOLE1BQUkwSSxzQkFBSjtBQUNELENBRkQsRSxDQTNCQSIsImZpbGUiOiJlbXBsb3llZV9mb3JtLmJ1bmRsZS5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQ29weXJpZ2h0IHNpbmNlIDIwMDcgUHJlc3RhU2hvcCBTQSBhbmQgQ29udHJpYnV0b3JzXG4gKiBQcmVzdGFTaG9wIGlzIGFuIEludGVybmF0aW9uYWwgUmVnaXN0ZXJlZCBUcmFkZW1hcmsgJiBQcm9wZXJ0eSBvZiBQcmVzdGFTaG9wIFNBXG4gKlxuICogTk9USUNFIE9GIExJQ0VOU0VcbiAqXG4gKiBUaGlzIHNvdXJjZSBmaWxlIGlzIHN1YmplY3QgdG8gdGhlIE9wZW4gU29mdHdhcmUgTGljZW5zZSAoT1NMIDMuMClcbiAqIHRoYXQgaXMgYnVuZGxlZCB3aXRoIHRoaXMgcGFja2FnZSBpbiB0aGUgZmlsZSBMSUNFTlNFLm1kLlxuICogSXQgaXMgYWxzbyBhdmFpbGFibGUgdGhyb3VnaCB0aGUgd29ybGQtd2lkZS13ZWIgYXQgdGhpcyBVUkw6XG4gKiBodHRwczovL29wZW5zb3VyY2Uub3JnL2xpY2Vuc2VzL09TTC0zLjBcbiAqIElmIHlvdSBkaWQgbm90IHJlY2VpdmUgYSBjb3B5IG9mIHRoZSBsaWNlbnNlIGFuZCBhcmUgdW5hYmxlIHRvXG4gKiBvYnRhaW4gaXQgdGhyb3VnaCB0aGUgd29ybGQtd2lkZS13ZWIsIHBsZWFzZSBzZW5kIGFuIGVtYWlsXG4gKiB0byBsaWNlbnNlQHByZXN0YXNob3AuY29tIHNvIHdlIGNhbiBzZW5kIHlvdSBhIGNvcHkgaW1tZWRpYXRlbHkuXG4gKlxuICogRElTQ0xBSU1FUlxuICpcbiAqIERvIG5vdCBlZGl0IG9yIGFkZCB0byB0aGlzIGZpbGUgaWYgeW91IHdpc2ggdG8gdXBncmFkZSBQcmVzdGFTaG9wIHRvIG5ld2VyXG4gKiB2ZXJzaW9ucyBpbiB0aGUgZnV0dXJlLiBJZiB5b3Ugd2lzaCB0byBjdXN0b21pemUgUHJlc3RhU2hvcCBmb3IgeW91clxuICogbmVlZHMgcGxlYXNlIHJlZmVyIHRvIGh0dHBzOi8vZGV2ZG9jcy5wcmVzdGFzaG9wLmNvbS8gZm9yIG1vcmUgaW5mb3JtYXRpb24uXG4gKlxuICogQGF1dGhvciAgICBQcmVzdGFTaG9wIFNBIGFuZCBDb250cmlidXRvcnMgPGNvbnRhY3RAcHJlc3Rhc2hvcC5jb20+XG4gKiBAY29weXJpZ2h0IFNpbmNlIDIwMDcgUHJlc3RhU2hvcCBTQSBhbmQgQ29udHJpYnV0b3JzXG4gKiBAbGljZW5zZSAgIGh0dHBzOi8vb3BlbnNvdXJjZS5vcmcvbGljZW5zZXMvT1NMLTMuMCBPcGVuIFNvZnR3YXJlIExpY2Vuc2UgKE9TTCAzLjApXG4gKi9cblxuY29uc3QgeyR9ID0gd2luZG93O1xuXG4vKipcbiAqIFJlc3BvbnNpYmxlIGZvciBjb25uZWN0aW5nIHRvIGFkZG9ucyBtYXJrZXRwbGFjZS5cbiAqIE1ha2VzIGFuIGFkZG9ucyBjb25uZWN0IHJlcXVlc3QgdG8gdGhlIHNlcnZlciwgZGlzcGxheXMgZXJyb3IgbWVzc2FnZXMgaWYgaXQgZmFpbHMuXG4gKi9cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIEFkZG9uc0Nvbm5lY3RvciB7XG4gIGNvbnN0cnVjdG9yKFxuICAgIGFkZG9uc0Nvbm5lY3RGb3JtU2VsZWN0b3IsXG4gICAgbG9hZGluZ1NwaW5uZXJTZWxlY3RvcixcbiAgKSB7XG4gICAgdGhpcy5hZGRvbnNDb25uZWN0Rm9ybVNlbGVjdG9yID0gYWRkb25zQ29ubmVjdEZvcm1TZWxlY3RvcjtcbiAgICB0aGlzLiRsb2FkaW5nU3Bpbm5lciA9ICQobG9hZGluZ1NwaW5uZXJTZWxlY3Rvcik7XG5cbiAgICB0aGlzLmluaXRFdmVudHMoKTtcblxuICAgIHJldHVybiB7fTtcbiAgfVxuXG4gIC8qKlxuICAgKiBJbml0aWFsaXplIGV2ZW50cyByZWxhdGVkIHRvIGNvbm5lY3Rpb24gdG8gYWRkb25zLlxuICAgKlxuICAgKiBAcHJpdmF0ZVxuICAgKi9cbiAgaW5pdEV2ZW50cygpIHtcbiAgICAkKCdib2R5Jykub24oXG4gICAgICAnc3VibWl0JyxcbiAgICAgIHRoaXMuYWRkb25zQ29ubmVjdEZvcm1TZWxlY3RvcixcbiAgICAgIChldmVudCkgPT4ge1xuICAgICAgICBjb25zdCAkZm9ybSA9ICQoZXZlbnQuY3VycmVudFRhcmdldCk7XG4gICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgIGV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO1xuXG4gICAgICAgIHRoaXMuY29ubmVjdCgkZm9ybS5hdHRyKCdhY3Rpb24nKSwgJGZvcm0uc2VyaWFsaXplKCkpO1xuICAgICAgfSxcbiAgICApO1xuICB9XG5cbiAgLyoqXG4gICAqIERvIGEgUE9TVCByZXF1ZXN0IHRvIGNvbm5lY3QgdG8gYWRkb25zLlxuICAgKlxuICAgKiBAcGFyYW0ge1N0cmluZ30gYWRkb25zQ29ubmVjdFVybFxuICAgKiBAcGFyYW0ge09iamVjdH0gZm9ybURhdGFcbiAgICpcbiAgICogQHByaXZhdGVcbiAgICovXG4gIGNvbm5lY3QoYWRkb25zQ29ubmVjdFVybCwgZm9ybURhdGEpIHtcbiAgICAkLmFqYXgoe1xuICAgICAgbWV0aG9kOiAnUE9TVCcsXG4gICAgICB1cmw6IGFkZG9uc0Nvbm5lY3RVcmwsXG4gICAgICBkYXRhVHlwZTogJ2pzb24nLFxuICAgICAgZGF0YTogZm9ybURhdGEsXG4gICAgICBiZWZvcmVTZW5kOiAoKSA9PiB7XG4gICAgICAgIHRoaXMuJGxvYWRpbmdTcGlubmVyLnNob3coKTtcbiAgICAgICAgJCgnYnV0dG9uLmJ0blt0eXBlPVwic3VibWl0XCJdJywgdGhpcy5hZGRvbnNDb25uZWN0Rm9ybVNlbGVjdG9yKS5oaWRlKCk7XG4gICAgICB9LFxuICAgIH0pLnRoZW4oKHJlc3BvbnNlKSA9PiB7XG4gICAgICBpZiAocmVzcG9uc2Uuc3VjY2VzcyA9PT0gMSkge1xuICAgICAgICB3aW5kb3cubG9jYXRpb24ucmVsb2FkKCk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICAkLmdyb3dsLmVycm9yKHtcbiAgICAgICAgICBtZXNzYWdlOiByZXNwb25zZS5tZXNzYWdlLFxuICAgICAgICB9KTtcblxuICAgICAgICB0aGlzLiRsb2FkaW5nU3Bpbm5lci5oaWRlKCk7XG4gICAgICAgICQoJ2J1dHRvbi5idG5bdHlwZT1cInN1Ym1pdFwiXScsIHRoaXMuYWRkb25zQ29ubmVjdEZvcm1TZWxlY3RvcikuZmFkZUluKCk7XG4gICAgICB9XG4gICAgfSwgKCkgPT4ge1xuICAgICAgJC5ncm93bC5lcnJvcih7XG4gICAgICAgIG1lc3NhZ2U6ICQodGhpcy5hZGRvbnNDb25uZWN0Rm9ybVNlbGVjdG9yKS5kYXRhKCdlcnJvci1tZXNzYWdlJyksXG4gICAgICB9KTtcblxuICAgICAgdGhpcy4kbG9hZGluZ1NwaW5uZXIuaGlkZSgpO1xuICAgICAgJCgnYnV0dG9uLmJ0blt0eXBlPVwic3VibWl0XCJdJywgdGhpcy5hZGRvbnNDb25uZWN0Rm9ybVNlbGVjdG9yKS5zaG93KCk7XG4gICAgfSk7XG4gIH1cbn1cbiIsIi8qKlxuICogQ29weXJpZ2h0IHNpbmNlIDIwMDcgUHJlc3RhU2hvcCBTQSBhbmQgQ29udHJpYnV0b3JzXG4gKiBQcmVzdGFTaG9wIGlzIGFuIEludGVybmF0aW9uYWwgUmVnaXN0ZXJlZCBUcmFkZW1hcmsgJiBQcm9wZXJ0eSBvZiBQcmVzdGFTaG9wIFNBXG4gKlxuICogTk9USUNFIE9GIExJQ0VOU0VcbiAqXG4gKiBUaGlzIHNvdXJjZSBmaWxlIGlzIHN1YmplY3QgdG8gdGhlIE9wZW4gU29mdHdhcmUgTGljZW5zZSAoT1NMIDMuMClcbiAqIHRoYXQgaXMgYnVuZGxlZCB3aXRoIHRoaXMgcGFja2FnZSBpbiB0aGUgZmlsZSBMSUNFTlNFLm1kLlxuICogSXQgaXMgYWxzbyBhdmFpbGFibGUgdGhyb3VnaCB0aGUgd29ybGQtd2lkZS13ZWIgYXQgdGhpcyBVUkw6XG4gKiBodHRwczovL29wZW5zb3VyY2Uub3JnL2xpY2Vuc2VzL09TTC0zLjBcbiAqIElmIHlvdSBkaWQgbm90IHJlY2VpdmUgYSBjb3B5IG9mIHRoZSBsaWNlbnNlIGFuZCBhcmUgdW5hYmxlIHRvXG4gKiBvYnRhaW4gaXQgdGhyb3VnaCB0aGUgd29ybGQtd2lkZS13ZWIsIHBsZWFzZSBzZW5kIGFuIGVtYWlsXG4gKiB0byBsaWNlbnNlQHByZXN0YXNob3AuY29tIHNvIHdlIGNhbiBzZW5kIHlvdSBhIGNvcHkgaW1tZWRpYXRlbHkuXG4gKlxuICogRElTQ0xBSU1FUlxuICpcbiAqIERvIG5vdCBlZGl0IG9yIGFkZCB0byB0aGlzIGZpbGUgaWYgeW91IHdpc2ggdG8gdXBncmFkZSBQcmVzdGFTaG9wIHRvIG5ld2VyXG4gKiB2ZXJzaW9ucyBpbiB0aGUgZnV0dXJlLiBJZiB5b3Ugd2lzaCB0byBjdXN0b21pemUgUHJlc3RhU2hvcCBmb3IgeW91clxuICogbmVlZHMgcGxlYXNlIHJlZmVyIHRvIGh0dHBzOi8vZGV2ZG9jcy5wcmVzdGFzaG9wLmNvbS8gZm9yIG1vcmUgaW5mb3JtYXRpb24uXG4gKlxuICogQGF1dGhvciAgICBQcmVzdGFTaG9wIFNBIGFuZCBDb250cmlidXRvcnMgPGNvbnRhY3RAcHJlc3Rhc2hvcC5jb20+XG4gKiBAY29weXJpZ2h0IFNpbmNlIDIwMDcgUHJlc3RhU2hvcCBTQSBhbmQgQ29udHJpYnV0b3JzXG4gKiBAbGljZW5zZSAgIGh0dHBzOi8vb3BlbnNvdXJjZS5vcmcvbGljZW5zZXMvT1NMLTMuMCBPcGVuIFNvZnR3YXJlIExpY2Vuc2UgKE9TTCAzLjApXG4gKi9cblxuY29uc3QgeyR9ID0gd2luZG93O1xuXG4vKipcbiAqIEdlbmVyYXRlcyBhIHBhc3N3b3JkIGFuZCBpbmZvcm1zIGFib3V0IGl0J3Mgc3RyZW5ndGguXG4gKiBZb3UgY2FuIHBhc3MgYSBwYXNzd29yZCBpbnB1dCB0byB3YXRjaCB0aGUgcGFzc3dvcmQgc3RyZW5ndGggYW5kIGRpc3BsYXkgZmVlZGJhY2sgbWVzc2FnZXMuXG4gKiBZb3UgY2FuIGFsc28gZ2VuZXJhdGUgYSByYW5kb20gcGFzc3dvcmQgaW50byBhbiBpbnB1dC5cbiAqL1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgQ2hhbmdlUGFzc3dvcmRIYW5kbGVyIHtcbiAgY29uc3RydWN0b3IocGFzc3dvcmRTdHJlbmd0aEZlZWRiYWNrQ29udGFpbmVyU2VsZWN0b3IsIG9wdGlvbnMgPSB7fSkge1xuICAgIC8vIE1pbmltdW0gbGVuZ3RoIG9mIHRoZSBnZW5lcmF0ZWQgcGFzc3dvcmQuXG4gICAgdGhpcy5taW5MZW5ndGggPSBvcHRpb25zLm1pbkxlbmd0aCB8fCA4O1xuXG4gICAgLy8gRmVlZGJhY2sgY29udGFpbmVyIGhvbGRzIG1lc3NhZ2VzIHJlcHJlc2VudGluZyBwYXNzd29yZCBzdHJlbmd0aC5cbiAgICB0aGlzLiRmZWVkYmFja0NvbnRhaW5lciA9ICQocGFzc3dvcmRTdHJlbmd0aEZlZWRiYWNrQ29udGFpbmVyU2VsZWN0b3IpO1xuXG4gICAgcmV0dXJuIHtcbiAgICAgIHdhdGNoUGFzc3dvcmRTdHJlbmd0aDogKCRpbnB1dCkgPT4gdGhpcy53YXRjaFBhc3N3b3JkU3RyZW5ndGgoJGlucHV0KSxcbiAgICAgIGdlbmVyYXRlUGFzc3dvcmQ6ICgkaW5wdXQpID0+IHRoaXMuZ2VuZXJhdGVQYXNzd29yZCgkaW5wdXQpLFxuICAgIH07XG4gIH1cblxuICAvKipcbiAgICogV2F0Y2ggcGFzc3dvcmQsIHdoaWNoIGlzIGVudGVyZWQgaW4gdGhlIGlucHV0LCBzdHJlbmd0aCBhbmQgaW5mb3JtIGFib3V0IGl0LlxuICAgKlxuICAgKiBAcGFyYW0ge2pRdWVyeX0gJGlucHV0IHRoZSBpbnB1dCB0byB3YXRjaC5cbiAgICovXG4gIHdhdGNoUGFzc3dvcmRTdHJlbmd0aCgkaW5wdXQpIHtcbiAgICAkLnBhc3N5LnJlcXVpcmVtZW50cy5sZW5ndGgubWluID0gdGhpcy5taW5MZW5ndGg7XG4gICAgJC5wYXNzeS5yZXF1aXJlbWVudHMuY2hhcmFjdGVycyA9ICdESUdJVCc7XG5cbiAgICAkaW5wdXQuZWFjaCgoaW5kZXgsIGVsZW1lbnQpID0+IHtcbiAgICAgIGNvbnN0ICRvdXRwdXRDb250YWluZXIgPSAkKCc8c3Bhbj4nKTtcblxuICAgICAgJG91dHB1dENvbnRhaW5lci5pbnNlcnRBZnRlcigkKGVsZW1lbnQpKTtcblxuICAgICAgJChlbGVtZW50KS5wYXNzeSgoc3RyZW5ndGgsIHZhbGlkKSA9PiB7XG4gICAgICAgIHRoaXMuZGlzcGxheUZlZWRiYWNrKCRvdXRwdXRDb250YWluZXIsIHN0cmVuZ3RoLCB2YWxpZCk7XG4gICAgICB9KTtcbiAgICB9KTtcbiAgfVxuXG4gIC8qKlxuICAgKiBHZW5lcmF0ZXMgYSBwYXNzd29yZCBhbmQgZmlsbHMgaXQgdG8gZ2l2ZW4gaW5wdXQuXG4gICAqXG4gICAqIEBwYXJhbSB7alF1ZXJ5fSAkaW5wdXQgdGhlIGlucHV0IHRvIGZpbGwgdGhlIHBhc3N3b3JkIGludG8uXG4gICAqL1xuICBnZW5lcmF0ZVBhc3N3b3JkKCRpbnB1dCkge1xuICAgICRpbnB1dC5wYXNzeSgnZ2VuZXJhdGUnLCB0aGlzLm1pbkxlbmd0aCk7XG4gIH1cblxuICAvKipcbiAgICogRGlzcGxheSBmZWVkYmFjayBhYm91dCBwYXNzd29yZCdzIHN0cmVuZ3RoLlxuICAgKlxuICAgKiBAcGFyYW0ge2pRdWVyeX0gJG91dHB1dENvbnRhaW5lciBhIGNvbnRhaW5lciB0byBwdXQgZmVlZGJhY2sgb3V0cHV0IGludG8uXG4gICAqIEBwYXJhbSB7bnVtYmVyfSBwYXNzd29yZFN0cmVuZ3RoXG4gICAqIEBwYXJhbSB7Ym9vbGVhbn0gaXNQYXNzd29yZFZhbGlkXG4gICAqXG4gICAqIEBwcml2YXRlXG4gICAqL1xuICBkaXNwbGF5RmVlZGJhY2soJG91dHB1dENvbnRhaW5lciwgcGFzc3dvcmRTdHJlbmd0aCwgaXNQYXNzd29yZFZhbGlkKSB7XG4gICAgY29uc3QgZmVlZGJhY2sgPSB0aGlzLmdldFBhc3N3b3JkU3RyZW5ndGhGZWVkYmFjayhwYXNzd29yZFN0cmVuZ3RoKTtcbiAgICAkb3V0cHV0Q29udGFpbmVyLnRleHQoZmVlZGJhY2subWVzc2FnZSk7XG4gICAgJG91dHB1dENvbnRhaW5lci5yZW1vdmVDbGFzcygndGV4dC1kYW5nZXIgdGV4dC13YXJuaW5nIHRleHQtc3VjY2VzcycpO1xuICAgICRvdXRwdXRDb250YWluZXIuYWRkQ2xhc3MoZmVlZGJhY2suZWxlbWVudENsYXNzKTtcbiAgICAkb3V0cHV0Q29udGFpbmVyLnRvZ2dsZUNsYXNzKCdkLW5vbmUnLCAhaXNQYXNzd29yZFZhbGlkKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBHZXQgZmVlZGJhY2sgdGhhdCBkZXNjcmliZXMgZ2l2ZW4gcGFzc3dvcmQgc3RyZW5ndGguXG4gICAqIFJlc3BvbnNlIGNvbnRhaW5zIHRleHQgbWVzc2FnZSBhbmQgZWxlbWVudCBjbGFzcy5cbiAgICpcbiAgICogQHBhcmFtIHtudW1iZXJ9IHN0cmVuZ3RoXG4gICAqXG4gICAqIEBwcml2YXRlXG4gICAqL1xuICBnZXRQYXNzd29yZFN0cmVuZ3RoRmVlZGJhY2soc3RyZW5ndGgpIHtcbiAgICBzd2l0Y2ggKHN0cmVuZ3RoKSB7XG4gICAgICBjYXNlICQucGFzc3kuc3RyZW5ndGguTE9XOlxuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgIG1lc3NhZ2U6IHRoaXMuJGZlZWRiYWNrQ29udGFpbmVyLmZpbmQoJy5zdHJlbmd0aC1sb3cnKS50ZXh0KCksXG4gICAgICAgICAgZWxlbWVudENsYXNzOiAndGV4dC1kYW5nZXInLFxuICAgICAgICB9O1xuXG4gICAgICBjYXNlICQucGFzc3kuc3RyZW5ndGguTUVESVVNOlxuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgIG1lc3NhZ2U6IHRoaXMuJGZlZWRiYWNrQ29udGFpbmVyLmZpbmQoJy5zdHJlbmd0aC1tZWRpdW0nKS50ZXh0KCksXG4gICAgICAgICAgZWxlbWVudENsYXNzOiAndGV4dC13YXJuaW5nJyxcbiAgICAgICAgfTtcblxuICAgICAgY2FzZSAkLnBhc3N5LnN0cmVuZ3RoLkhJR0g6XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgbWVzc2FnZTogdGhpcy4kZmVlZGJhY2tDb250YWluZXIuZmluZCgnLnN0cmVuZ3RoLWhpZ2gnKS50ZXh0KCksXG4gICAgICAgICAgZWxlbWVudENsYXNzOiAndGV4dC1zdWNjZXNzJyxcbiAgICAgICAgfTtcblxuICAgICAgY2FzZSAkLnBhc3N5LnN0cmVuZ3RoLkVYVFJFTUU6XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgbWVzc2FnZTogdGhpcy4kZmVlZGJhY2tDb250YWluZXIuZmluZCgnLnN0cmVuZ3RoLWV4dHJlbWUnKS50ZXh0KCksXG4gICAgICAgICAgZWxlbWVudENsYXNzOiAndGV4dC1zdWNjZXNzJyxcbiAgICAgICAgfTtcblxuICAgICAgZGVmYXVsdDpcbiAgICAgICAgdGhyb3cgbmV3IEVycm9yKCdJbnZhbGlkIHBhc3N3b3JkIHN0cmVuZ3RoIGluZGljYXRvci4nKTtcbiAgICB9XG4gIH1cbn1cbiIsIi8qKlxuICogQ29weXJpZ2h0IHNpbmNlIDIwMDcgUHJlc3RhU2hvcCBTQSBhbmQgQ29udHJpYnV0b3JzXG4gKiBQcmVzdGFTaG9wIGlzIGFuIEludGVybmF0aW9uYWwgUmVnaXN0ZXJlZCBUcmFkZW1hcmsgJiBQcm9wZXJ0eSBvZiBQcmVzdGFTaG9wIFNBXG4gKlxuICogTk9USUNFIE9GIExJQ0VOU0VcbiAqXG4gKiBUaGlzIHNvdXJjZSBmaWxlIGlzIHN1YmplY3QgdG8gdGhlIE9wZW4gU29mdHdhcmUgTGljZW5zZSAoT1NMIDMuMClcbiAqIHRoYXQgaXMgYnVuZGxlZCB3aXRoIHRoaXMgcGFja2FnZSBpbiB0aGUgZmlsZSBMSUNFTlNFLm1kLlxuICogSXQgaXMgYWxzbyBhdmFpbGFibGUgdGhyb3VnaCB0aGUgd29ybGQtd2lkZS13ZWIgYXQgdGhpcyBVUkw6XG4gKiBodHRwczovL29wZW5zb3VyY2Uub3JnL2xpY2Vuc2VzL09TTC0zLjBcbiAqIElmIHlvdSBkaWQgbm90IHJlY2VpdmUgYSBjb3B5IG9mIHRoZSBsaWNlbnNlIGFuZCBhcmUgdW5hYmxlIHRvXG4gKiBvYnRhaW4gaXQgdGhyb3VnaCB0aGUgd29ybGQtd2lkZS13ZWIsIHBsZWFzZSBzZW5kIGFuIGVtYWlsXG4gKiB0byBsaWNlbnNlQHByZXN0YXNob3AuY29tIHNvIHdlIGNhbiBzZW5kIHlvdSBhIGNvcHkgaW1tZWRpYXRlbHkuXG4gKlxuICogRElTQ0xBSU1FUlxuICpcbiAqIERvIG5vdCBlZGl0IG9yIGFkZCB0byB0aGlzIGZpbGUgaWYgeW91IHdpc2ggdG8gdXBncmFkZSBQcmVzdGFTaG9wIHRvIG5ld2VyXG4gKiB2ZXJzaW9ucyBpbiB0aGUgZnV0dXJlLiBJZiB5b3Ugd2lzaCB0byBjdXN0b21pemUgUHJlc3RhU2hvcCBmb3IgeW91clxuICogbmVlZHMgcGxlYXNlIHJlZmVyIHRvIGh0dHBzOi8vZGV2ZG9jcy5wcmVzdGFzaG9wLmNvbS8gZm9yIG1vcmUgaW5mb3JtYXRpb24uXG4gKlxuICogQGF1dGhvciAgICBQcmVzdGFTaG9wIFNBIGFuZCBDb250cmlidXRvcnMgPGNvbnRhY3RAcHJlc3Rhc2hvcC5jb20+XG4gKiBAY29weXJpZ2h0IFNpbmNlIDIwMDcgUHJlc3RhU2hvcCBTQSBhbmQgQ29udHJpYnV0b3JzXG4gKiBAbGljZW5zZSAgIGh0dHBzOi8vb3BlbnNvdXJjZS5vcmcvbGljZW5zZXMvT1NMLTMuMCBPcGVuIFNvZnR3YXJlIExpY2Vuc2UgKE9TTCAzLjApXG4gKi9cblxuaW1wb3J0IENoYW5nZVBhc3N3b3JkSGFuZGxlciBmcm9tICcuLi9jaGFuZ2UtcGFzc3dvcmQtaGFuZGxlcic7XG5pbXBvcnQgUGFzc3dvcmRWYWxpZGF0b3IgZnJvbSAnLi4vcGFzc3dvcmQtdmFsaWRhdG9yJztcblxuY29uc3QgeyR9ID0gd2luZG93O1xuXG4vKipcbiAqIENsYXNzIHJlc3BvbnNpYmxlIGZvciBhY3Rpb25zIHJlbGF0ZWQgdG8gXCJjaGFuZ2UgcGFzc3dvcmRcIiBmb3JtIHR5cGUuXG4gKiBHZW5lcmF0ZXMgcmFuZG9tIHBhc3N3b3JkcywgdmFsaWRhdGVzIG5ldyBwYXNzd29yZCBhbmQgaXQncyBjb25maXJtYXRpb24sXG4gKiBkaXNwbGF5cyBlcnJvciBtZXNzYWdlcyByZWxhdGVkIHRvIHZhbGlkYXRpb24uXG4gKi9cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIENoYW5nZVBhc3N3b3JkQ29udHJvbCB7XG4gIGNvbnN0cnVjdG9yKFxuICAgIGlucHV0c0Jsb2NrU2VsZWN0b3IsXG4gICAgc2hvd0J1dHRvblNlbGVjdG9yLFxuICAgIGhpZGVCdXR0b25TZWxlY3RvcixcbiAgICBnZW5lcmF0ZVBhc3N3b3JkQnV0dG9uU2VsZWN0b3IsXG4gICAgb2xkUGFzc3dvcmRJbnB1dFNlbGVjdG9yLFxuICAgIG5ld1Bhc3N3b3JkSW5wdXRTZWxlY3RvcixcbiAgICBjb25maXJtTmV3UGFzc3dvcmRJbnB1dFNlbGVjdG9yLFxuICAgIGdlbmVyYXRlZFBhc3N3b3JkRGlzcGxheVNlbGVjdG9yLFxuICAgIHBhc3N3b3JkU3RyZW5ndGhGZWVkYmFja0NvbnRhaW5lclNlbGVjdG9yLFxuICApIHtcbiAgICAvLyBCbG9jayB0aGF0IGNvbnRhaW5zIHBhc3N3b3JkIGlucHV0c1xuICAgIHRoaXMuJGlucHV0c0Jsb2NrID0gJChpbnB1dHNCbG9ja1NlbGVjdG9yKTtcblxuICAgIC8vIEJ1dHRvbiB0aGF0IHNob3dzIHRoZSBwYXNzd29yZCBpbnB1dHMgYmxvY2tcbiAgICB0aGlzLnNob3dCdXR0b25TZWxlY3RvciA9IHNob3dCdXR0b25TZWxlY3RvcjtcblxuICAgIC8vIEJ1dHRvbiB0aGF0IGhpZGVzIHRoZSBwYXNzd29yZCBpbnB1dHMgYmxvY2tcbiAgICB0aGlzLmhpZGVCdXR0b25TZWxlY3RvciA9IGhpZGVCdXR0b25TZWxlY3RvcjtcblxuICAgIC8vIEJ1dHRvbiB0aGF0IGdlbmVyYXRlcyBhIHJhbmRvbSBwYXNzd29yZFxuICAgIHRoaXMuZ2VuZXJhdGVQYXNzd29yZEJ1dHRvblNlbGVjdG9yID0gZ2VuZXJhdGVQYXNzd29yZEJ1dHRvblNlbGVjdG9yO1xuXG4gICAgLy8gSW5wdXQgdG8gZW50ZXIgb2xkIHBhc3N3b3JkXG4gICAgdGhpcy5vbGRQYXNzd29yZElucHV0U2VsZWN0b3IgPSBvbGRQYXNzd29yZElucHV0U2VsZWN0b3I7XG5cbiAgICAvLyBJbnB1dCB0byBlbnRlciBuZXcgcGFzc3dvcmRcbiAgICB0aGlzLm5ld1Bhc3N3b3JkSW5wdXRTZWxlY3RvciA9IG5ld1Bhc3N3b3JkSW5wdXRTZWxlY3RvcjtcblxuICAgIC8vIElucHV0IHRvIGNvbmZpcm0gdGhlIG5ldyBwYXNzd29yZFxuICAgIHRoaXMuY29uZmlybU5ld1Bhc3N3b3JkSW5wdXRTZWxlY3RvciA9IGNvbmZpcm1OZXdQYXNzd29yZElucHV0U2VsZWN0b3I7XG5cbiAgICAvLyBJbnB1dCB0aGF0IGRpc3BsYXlzIGdlbmVyYXRlZCByYW5kb20gcGFzc3dvcmRcbiAgICB0aGlzLmdlbmVyYXRlZFBhc3N3b3JkRGlzcGxheVNlbGVjdG9yID0gZ2VuZXJhdGVkUGFzc3dvcmREaXNwbGF5U2VsZWN0b3I7XG5cbiAgICAvLyBNYWluIGlucHV0IGZvciBwYXNzd29yZCBnZW5lcmF0aW9uXG4gICAgdGhpcy4kbmV3UGFzc3dvcmRJbnB1dHMgPSB0aGlzLiRpbnB1dHNCbG9ja1xuICAgICAgLmZpbmQodGhpcy5uZXdQYXNzd29yZElucHV0U2VsZWN0b3IpO1xuXG4gICAgLy8gR2VuZXJhdGVkIHBhc3N3b3JkIHdpbGwgYmUgY29waWVkIHRvIHRoZXNlIGlucHV0c1xuICAgIHRoaXMuJGNvcHlQYXNzd29yZElucHV0cyA9IHRoaXMuJGlucHV0c0Jsb2NrXG4gICAgICAuZmluZCh0aGlzLmNvbmZpcm1OZXdQYXNzd29yZElucHV0U2VsZWN0b3IpXG4gICAgICAuYWRkKHRoaXMuZ2VuZXJhdGVkUGFzc3dvcmREaXNwbGF5U2VsZWN0b3IpO1xuXG4gICAgLy8gQWxsIGlucHV0cyBpbiB0aGUgY2hhbmdlIHBhc3N3b3JkIGJsb2NrLCB0aGF0IGFyZSBzdWJtaXR0YWJsZSB3aXRoIHRoZSBmb3JtLlxuICAgIHRoaXMuJHN1Ym1pdHRhYmxlSW5wdXRzID0gdGhpcy4kaW5wdXRzQmxvY2tcbiAgICAgIC5maW5kKHRoaXMub2xkUGFzc3dvcmRJbnB1dFNlbGVjdG9yKVxuICAgICAgLmFkZCh0aGlzLm5ld1Bhc3N3b3JkSW5wdXRTZWxlY3RvcilcbiAgICAgIC5hZGQodGhpcy5jb25maXJtTmV3UGFzc3dvcmRJbnB1dFNlbGVjdG9yKTtcblxuICAgIHRoaXMucGFzc3dvcmRIYW5kbGVyID0gbmV3IENoYW5nZVBhc3N3b3JkSGFuZGxlcihcbiAgICAgIHBhc3N3b3JkU3RyZW5ndGhGZWVkYmFja0NvbnRhaW5lclNlbGVjdG9yLFxuICAgICk7XG5cbiAgICB0aGlzLnBhc3N3b3JkVmFsaWRhdG9yID0gbmV3IFBhc3N3b3JkVmFsaWRhdG9yKFxuICAgICAgdGhpcy5uZXdQYXNzd29yZElucHV0U2VsZWN0b3IsXG4gICAgICB0aGlzLmNvbmZpcm1OZXdQYXNzd29yZElucHV0U2VsZWN0b3IsXG4gICAgKTtcblxuICAgIHRoaXMuaGlkZUlucHV0c0Jsb2NrKCk7XG4gICAgdGhpcy5pbml0RXZlbnRzKCk7XG5cbiAgICByZXR1cm4ge307XG4gIH1cblxuICAvKipcbiAgICogSW5pdGlhbGl6ZSBldmVudHMuXG4gICAqXG4gICAqIEBwcml2YXRlXG4gICAqL1xuICBpbml0RXZlbnRzKCkge1xuICAgIC8vIFNob3cgdGhlIGlucHV0cyBibG9jayB3aGVuIHNob3cgYnV0dG9uIGlzIGNsaWNrZWRcbiAgICAkKGRvY3VtZW50KS5vbignY2xpY2snLCB0aGlzLnNob3dCdXR0b25TZWxlY3RvciwgKGUpID0+IHtcbiAgICAgIHRoaXMuaGlkZSgkKGUuY3VycmVudFRhcmdldCkpO1xuICAgICAgdGhpcy5zaG93SW5wdXRzQmxvY2soKTtcbiAgICB9KTtcblxuICAgICQoZG9jdW1lbnQpLm9uKCdjbGljaycsIHRoaXMuaGlkZUJ1dHRvblNlbGVjdG9yLCAoKSA9PiB7XG4gICAgICB0aGlzLmhpZGVJbnB1dHNCbG9jaygpO1xuICAgICAgdGhpcy5zaG93KCQodGhpcy5zaG93QnV0dG9uU2VsZWN0b3IpKTtcbiAgICB9KTtcblxuICAgIC8vIFdhdGNoIGFuZCBkaXNwbGF5IGZlZWRiYWNrIGFib3V0IHBhc3N3b3JkJ3Mgc3RyZW5ndGhcbiAgICB0aGlzLnBhc3N3b3JkSGFuZGxlci53YXRjaFBhc3N3b3JkU3RyZW5ndGgodGhpcy4kbmV3UGFzc3dvcmRJbnB1dHMpO1xuXG4gICAgJChkb2N1bWVudCkub24oJ2NsaWNrJywgdGhpcy5nZW5lcmF0ZVBhc3N3b3JkQnV0dG9uU2VsZWN0b3IsICgpID0+IHtcbiAgICAgIC8vIEdlbmVyYXRlIHRoZSBwYXNzd29yZCBpbnRvIG1haW4gaW5wdXQuXG4gICAgICB0aGlzLnBhc3N3b3JkSGFuZGxlci5nZW5lcmF0ZVBhc3N3b3JkKHRoaXMuJG5ld1Bhc3N3b3JkSW5wdXRzKTtcblxuICAgICAgLy8gQ29weSB0aGUgZ2VuZXJhdGVkIHBhc3N3b3JkIGZyb20gbWFpbiBpbnB1dCB0byBhZGRpdGlvbmFsIGlucHV0c1xuICAgICAgdGhpcy4kY29weVBhc3N3b3JkSW5wdXRzLnZhbCh0aGlzLiRuZXdQYXNzd29yZElucHV0cy52YWwoKSk7XG4gICAgICB0aGlzLmNoZWNrUGFzc3dvcmRWYWxpZGl0eSgpO1xuICAgIH0pO1xuXG4gICAgLy8gVmFsaWRhdGUgbmV3IHBhc3N3b3JkIGFuZCBpdCdzIGNvbmZpcm1hdGlvbiB3aGVuIGFueSBvZiB0aGUgaW5wdXRzIGlzIGNoYW5nZWRcbiAgICAkKGRvY3VtZW50KS5vbihcbiAgICAgICdrZXl1cCcsXG4gICAgICBgJHt0aGlzLm5ld1Bhc3N3b3JkSW5wdXRTZWxlY3Rvcn0sJHt0aGlzLmNvbmZpcm1OZXdQYXNzd29yZElucHV0U2VsZWN0b3J9YCxcbiAgICAgICgpID0+IHtcbiAgICAgICAgdGhpcy5jaGVja1Bhc3N3b3JkVmFsaWRpdHkoKTtcbiAgICAgIH0sXG4gICAgKTtcblxuICAgIC8vIFByZXZlbnQgc3VibWl0dGluZyB0aGUgZm9ybSBpZiBuZXcgcGFzc3dvcmQgaXMgbm90IHZhbGlkXG4gICAgJChkb2N1bWVudCkub24oJ3N1Ym1pdCcsICQodGhpcy5vbGRQYXNzd29yZElucHV0U2VsZWN0b3IpLmNsb3Nlc3QoJ2Zvcm0nKSwgKGV2ZW50KSA9PiB7XG4gICAgICAvLyBJZiBwYXNzd29yZCBpbnB1dCBpcyBkaXNhYmxlZCAtIHdlIGRvbid0IG5lZWQgdG8gdmFsaWRhdGUgaXQuXG4gICAgICBpZiAoJCh0aGlzLm9sZFBhc3N3b3JkSW5wdXRTZWxlY3RvcikuaXMoJzpkaXNhYmxlZCcpKSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cblxuICAgICAgaWYgKCF0aGlzLnBhc3N3b3JkVmFsaWRhdG9yLmlzUGFzc3dvcmRWYWxpZCgpKSB7XG4gICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICB9XG4gICAgfSk7XG4gIH1cblxuICAvKipcbiAgICogQ2hlY2sgaWYgcGFzc3dvcmQgaXMgdmFsaWQsIHNob3cgZXJyb3IgbWVzc2FnZXMgaWYgaXQncyBub3QuXG4gICAqXG4gICAqIEBwcml2YXRlXG4gICAqL1xuICBjaGVja1Bhc3N3b3JkVmFsaWRpdHkoKSB7XG4gICAgY29uc3QgJGZpcnN0UGFzc3dvcmRFcnJvckNvbnRhaW5lciA9ICQodGhpcy5uZXdQYXNzd29yZElucHV0U2VsZWN0b3IpLnBhcmVudCgpLmZpbmQoJy5mb3JtLXRleHQnKTtcbiAgICBjb25zdCAkc2Vjb25kUGFzc3dvcmRFcnJvckNvbnRhaW5lciA9ICQodGhpcy5jb25maXJtTmV3UGFzc3dvcmRJbnB1dFNlbGVjdG9yKS5wYXJlbnQoKS5maW5kKCcuZm9ybS10ZXh0Jyk7XG5cbiAgICAkZmlyc3RQYXNzd29yZEVycm9yQ29udGFpbmVyXG4gICAgICAudGV4dCh0aGlzLmdldFBhc3N3b3JkTGVuZ3RoVmFsaWRhdGlvbk1lc3NhZ2UoKSlcbiAgICAgIC50b2dnbGVDbGFzcygndGV4dC1kYW5nZXInLCAhdGhpcy5wYXNzd29yZFZhbGlkYXRvci5pc1Bhc3N3b3JkTGVuZ3RoVmFsaWQoKSk7XG4gICAgJHNlY29uZFBhc3N3b3JkRXJyb3JDb250YWluZXJcbiAgICAgIC50ZXh0KHRoaXMuZ2V0UGFzc3dvcmRDb25maXJtYXRpb25WYWxpZGF0aW9uTWVzc2FnZSgpKVxuICAgICAgLnRvZ2dsZUNsYXNzKCd0ZXh0LWRhbmdlcicsICF0aGlzLnBhc3N3b3JkVmFsaWRhdG9yLmlzUGFzc3dvcmRNYXRjaGluZ0NvbmZpcm1hdGlvbigpKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBHZXQgcGFzc3dvcmQgY29uZmlybWF0aW9uIHZhbGlkYXRpb24gbWVzc2FnZS5cbiAgICpcbiAgICogQHJldHVybnMge1N0cmluZ31cbiAgICpcbiAgICogQHByaXZhdGVcbiAgICovXG4gIGdldFBhc3N3b3JkQ29uZmlybWF0aW9uVmFsaWRhdGlvbk1lc3NhZ2UoKSB7XG4gICAgaWYgKCF0aGlzLnBhc3N3b3JkVmFsaWRhdG9yLmlzUGFzc3dvcmRNYXRjaGluZ0NvbmZpcm1hdGlvbigpKSB7XG4gICAgICByZXR1cm4gJCh0aGlzLmNvbmZpcm1OZXdQYXNzd29yZElucHV0U2VsZWN0b3IpLmRhdGEoJ2ludmFsaWQtcGFzc3dvcmQnKTtcbiAgICB9XG5cbiAgICByZXR1cm4gJyc7XG4gIH1cblxuICAvKipcbiAgICogR2V0IHBhc3N3b3JkIGxlbmd0aCB2YWxpZGF0aW9uIG1lc3NhZ2UuXG4gICAqXG4gICAqIEByZXR1cm5zIHtTdHJpbmd9XG4gICAqXG4gICAqIEBwcml2YXRlXG4gICAqL1xuICBnZXRQYXNzd29yZExlbmd0aFZhbGlkYXRpb25NZXNzYWdlKCkge1xuICAgIGlmICh0aGlzLnBhc3N3b3JkVmFsaWRhdG9yLmlzUGFzc3dvcmRUb29TaG9ydCgpKSB7XG4gICAgICByZXR1cm4gJCh0aGlzLm5ld1Bhc3N3b3JkSW5wdXRTZWxlY3RvcikuZGF0YSgncGFzc3dvcmQtdG9vLXNob3J0Jyk7XG4gICAgfVxuXG4gICAgaWYgKHRoaXMucGFzc3dvcmRWYWxpZGF0b3IuaXNQYXNzd29yZFRvb0xvbmcoKSkge1xuICAgICAgcmV0dXJuICQodGhpcy5uZXdQYXNzd29yZElucHV0U2VsZWN0b3IpLmRhdGEoJ3Bhc3N3b3JkLXRvby1sb25nJyk7XG4gICAgfVxuXG4gICAgcmV0dXJuICcnO1xuICB9XG5cbiAgLyoqXG4gICAqIFNob3cgdGhlIHBhc3N3b3JkIGlucHV0cyBibG9jay5cbiAgICpcbiAgICogQHByaXZhdGVcbiAgICovXG4gIHNob3dJbnB1dHNCbG9jaygpIHtcbiAgICB0aGlzLnNob3codGhpcy4kaW5wdXRzQmxvY2spO1xuICAgIHRoaXMuJHN1Ym1pdHRhYmxlSW5wdXRzLnJlbW92ZUF0dHIoJ2Rpc2FibGVkJyk7XG4gICAgdGhpcy4kc3VibWl0dGFibGVJbnB1dHMuYXR0cigncmVxdWlyZWQnLCAncmVxdWlyZWQnKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBIaWRlIHRoZSBwYXNzd29yZCBpbnB1dHMgYmxvY2suXG4gICAqXG4gICAqIEBwcml2YXRlXG4gICAqL1xuICBoaWRlSW5wdXRzQmxvY2soKSB7XG4gICAgdGhpcy5oaWRlKHRoaXMuJGlucHV0c0Jsb2NrKTtcbiAgICB0aGlzLiRzdWJtaXR0YWJsZUlucHV0cy5hdHRyKCdkaXNhYmxlZCcsICdkaXNhYmxlZCcpO1xuICAgIHRoaXMuJHN1Ym1pdHRhYmxlSW5wdXRzLnJlbW92ZUF0dHIoJ3JlcXVpcmVkJyk7XG4gICAgdGhpcy4kaW5wdXRzQmxvY2suZmluZCgnaW5wdXQnKS52YWwoJycpO1xuICAgIHRoaXMuJGlucHV0c0Jsb2NrLmZpbmQoJy5mb3JtLXRleHQnKS50ZXh0KCcnKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBIaWRlIGFuIGVsZW1lbnQuXG4gICAqXG4gICAqIEBwYXJhbSB7alF1ZXJ5fSAkZWxcbiAgICpcbiAgICogQHByaXZhdGVcbiAgICovXG4gIGhpZGUoJGVsKSB7XG4gICAgJGVsLmFkZENsYXNzKCdkLW5vbmUnKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBTaG93IGhpZGRlbiBlbGVtZW50LlxuICAgKlxuICAgKiBAcGFyYW0ge2pRdWVyeX0gJGVsXG4gICAqXG4gICAqIEBwcml2YXRlXG4gICAqL1xuICBzaG93KCRlbCkge1xuICAgICRlbC5yZW1vdmVDbGFzcygnZC1ub25lJyk7XG4gIH1cbn1cbiIsIi8qKlxuICogQ29weXJpZ2h0IHNpbmNlIDIwMDcgUHJlc3RhU2hvcCBTQSBhbmQgQ29udHJpYnV0b3JzXG4gKiBQcmVzdGFTaG9wIGlzIGFuIEludGVybmF0aW9uYWwgUmVnaXN0ZXJlZCBUcmFkZW1hcmsgJiBQcm9wZXJ0eSBvZiBQcmVzdGFTaG9wIFNBXG4gKlxuICogTk9USUNFIE9GIExJQ0VOU0VcbiAqXG4gKiBUaGlzIHNvdXJjZSBmaWxlIGlzIHN1YmplY3QgdG8gdGhlIE9wZW4gU29mdHdhcmUgTGljZW5zZSAoT1NMIDMuMClcbiAqIHRoYXQgaXMgYnVuZGxlZCB3aXRoIHRoaXMgcGFja2FnZSBpbiB0aGUgZmlsZSBMSUNFTlNFLm1kLlxuICogSXQgaXMgYWxzbyBhdmFpbGFibGUgdGhyb3VnaCB0aGUgd29ybGQtd2lkZS13ZWIgYXQgdGhpcyBVUkw6XG4gKiBodHRwczovL29wZW5zb3VyY2Uub3JnL2xpY2Vuc2VzL09TTC0zLjBcbiAqIElmIHlvdSBkaWQgbm90IHJlY2VpdmUgYSBjb3B5IG9mIHRoZSBsaWNlbnNlIGFuZCBhcmUgdW5hYmxlIHRvXG4gKiBvYnRhaW4gaXQgdGhyb3VnaCB0aGUgd29ybGQtd2lkZS13ZWIsIHBsZWFzZSBzZW5kIGFuIGVtYWlsXG4gKiB0byBsaWNlbnNlQHByZXN0YXNob3AuY29tIHNvIHdlIGNhbiBzZW5kIHlvdSBhIGNvcHkgaW1tZWRpYXRlbHkuXG4gKlxuICogRElTQ0xBSU1FUlxuICpcbiAqIERvIG5vdCBlZGl0IG9yIGFkZCB0byB0aGlzIGZpbGUgaWYgeW91IHdpc2ggdG8gdXBncmFkZSBQcmVzdGFTaG9wIHRvIG5ld2VyXG4gKiB2ZXJzaW9ucyBpbiB0aGUgZnV0dXJlLiBJZiB5b3Ugd2lzaCB0byBjdXN0b21pemUgUHJlc3RhU2hvcCBmb3IgeW91clxuICogbmVlZHMgcGxlYXNlIHJlZmVyIHRvIGh0dHBzOi8vZGV2ZG9jcy5wcmVzdGFzaG9wLmNvbS8gZm9yIG1vcmUgaW5mb3JtYXRpb24uXG4gKlxuICogQGF1dGhvciAgICBQcmVzdGFTaG9wIFNBIGFuZCBDb250cmlidXRvcnMgPGNvbnRhY3RAcHJlc3Rhc2hvcC5jb20+XG4gKiBAY29weXJpZ2h0IFNpbmNlIDIwMDcgUHJlc3RhU2hvcCBTQSBhbmQgQ29udHJpYnV0b3JzXG4gKiBAbGljZW5zZSAgIGh0dHBzOi8vb3BlbnNvdXJjZS5vcmcvbGljZW5zZXMvT1NMLTMuMCBPcGVuIFNvZnR3YXJlIExpY2Vuc2UgKE9TTCAzLjApXG4gKi9cblxuY29uc3QgeyR9ID0gd2luZG93O1xuXG4vKipcbiAqIEhhbmRsZXMgVUkgaW50ZXJhY3Rpb25zIG9mIGNob2ljZSB0cmVlXG4gKi9cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIENob2ljZVRyZWUge1xuICAvKipcbiAgICogQHBhcmFtIHtTdHJpbmd9IHRyZWVTZWxlY3RvclxuICAgKi9cbiAgY29uc3RydWN0b3IodHJlZVNlbGVjdG9yKSB7XG4gICAgdGhpcy4kY29udGFpbmVyID0gJCh0cmVlU2VsZWN0b3IpO1xuXG4gICAgdGhpcy4kY29udGFpbmVyLm9uKCdjbGljaycsICcuanMtaW5wdXQtd3JhcHBlcicsIChldmVudCkgPT4ge1xuICAgICAgY29uc3QgJGlucHV0V3JhcHBlciA9ICQoZXZlbnQuY3VycmVudFRhcmdldCk7XG5cbiAgICAgIHRoaXMudG9nZ2xlQ2hpbGRUcmVlKCRpbnB1dFdyYXBwZXIpO1xuICAgIH0pO1xuXG4gICAgdGhpcy4kY29udGFpbmVyLm9uKCdjbGljaycsICcuanMtdG9nZ2xlLWNob2ljZS10cmVlLWFjdGlvbicsIChldmVudCkgPT4ge1xuICAgICAgY29uc3QgJGFjdGlvbiA9ICQoZXZlbnQuY3VycmVudFRhcmdldCk7XG5cbiAgICAgIHRoaXMudG9nZ2xlVHJlZSgkYWN0aW9uKTtcbiAgICB9KTtcblxuICAgIHJldHVybiB7XG4gICAgICBlbmFibGVBdXRvQ2hlY2tDaGlsZHJlbjogKCkgPT4gdGhpcy5lbmFibGVBdXRvQ2hlY2tDaGlsZHJlbigpLFxuICAgICAgZW5hYmxlQWxsSW5wdXRzOiAoKSA9PiB0aGlzLmVuYWJsZUFsbElucHV0cygpLFxuICAgICAgZGlzYWJsZUFsbElucHV0czogKCkgPT4gdGhpcy5kaXNhYmxlQWxsSW5wdXRzKCksXG4gICAgfTtcbiAgfVxuXG4gIC8qKlxuICAgKiBFbmFibGUgYXV0b21hdGljIGNoZWNrL3VuY2hlY2sgb2YgY2xpY2tlZCBpdGVtJ3MgY2hpbGRyZW4uXG4gICAqL1xuICBlbmFibGVBdXRvQ2hlY2tDaGlsZHJlbigpIHtcbiAgICB0aGlzLiRjb250YWluZXIub24oJ2NoYW5nZScsICdpbnB1dFt0eXBlPVwiY2hlY2tib3hcIl0nLCAoZXZlbnQpID0+IHtcbiAgICAgIGNvbnN0ICRjbGlja2VkQ2hlY2tib3ggPSAkKGV2ZW50LmN1cnJlbnRUYXJnZXQpO1xuICAgICAgY29uc3QgJGl0ZW1XaXRoQ2hpbGRyZW4gPSAkY2xpY2tlZENoZWNrYm94LmNsb3Nlc3QoJ2xpJyk7XG5cbiAgICAgICRpdGVtV2l0aENoaWxkcmVuXG4gICAgICAgIC5maW5kKCd1bCBpbnB1dFt0eXBlPVwiY2hlY2tib3hcIl0nKVxuICAgICAgICAucHJvcCgnY2hlY2tlZCcsICRjbGlja2VkQ2hlY2tib3guaXMoJzpjaGVja2VkJykpO1xuICAgIH0pO1xuICB9XG5cbiAgLyoqXG4gICAqIEVuYWJsZSBhbGwgaW5wdXRzIGluIHRoZSBjaG9pY2UgdHJlZS5cbiAgICovXG4gIGVuYWJsZUFsbElucHV0cygpIHtcbiAgICB0aGlzLiRjb250YWluZXIuZmluZCgnaW5wdXQnKS5yZW1vdmVBdHRyKCdkaXNhYmxlZCcpO1xuICB9XG5cbiAgLyoqXG4gICAqIERpc2FibGUgYWxsIGlucHV0cyBpbiB0aGUgY2hvaWNlIHRyZWUuXG4gICAqL1xuICBkaXNhYmxlQWxsSW5wdXRzKCkge1xuICAgIHRoaXMuJGNvbnRhaW5lci5maW5kKCdpbnB1dCcpLmF0dHIoJ2Rpc2FibGVkJywgJ2Rpc2FibGVkJyk7XG4gIH1cblxuICAvKipcbiAgICogQ29sbGFwc2Ugb3IgZXhwYW5kIHN1Yi10cmVlIGZvciBzaW5nbGUgcGFyZW50XG4gICAqXG4gICAqIEBwYXJhbSB7alF1ZXJ5fSAkaW5wdXRXcmFwcGVyXG4gICAqXG4gICAqIEBwcml2YXRlXG4gICAqL1xuICB0b2dnbGVDaGlsZFRyZWUoJGlucHV0V3JhcHBlcikge1xuICAgIGNvbnN0ICRwYXJlbnRXcmFwcGVyID0gJGlucHV0V3JhcHBlci5jbG9zZXN0KCdsaScpO1xuXG4gICAgaWYgKCRwYXJlbnRXcmFwcGVyLmhhc0NsYXNzKCdleHBhbmRlZCcpKSB7XG4gICAgICAkcGFyZW50V3JhcHBlclxuICAgICAgICAucmVtb3ZlQ2xhc3MoJ2V4cGFuZGVkJylcbiAgICAgICAgLmFkZENsYXNzKCdjb2xsYXBzZWQnKTtcblxuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIGlmICgkcGFyZW50V3JhcHBlci5oYXNDbGFzcygnY29sbGFwc2VkJykpIHtcbiAgICAgICRwYXJlbnRXcmFwcGVyXG4gICAgICAgIC5yZW1vdmVDbGFzcygnY29sbGFwc2VkJylcbiAgICAgICAgLmFkZENsYXNzKCdleHBhbmRlZCcpO1xuICAgIH1cbiAgfVxuXG4gIC8qKlxuICAgKiBDb2xsYXBzZSBvciBleHBhbmQgd2hvbGUgdHJlZVxuICAgKlxuICAgKiBAcGFyYW0ge2pRdWVyeX0gJGFjdGlvblxuICAgKlxuICAgKiBAcHJpdmF0ZVxuICAgKi9cbiAgdG9nZ2xlVHJlZSgkYWN0aW9uKSB7XG4gICAgY29uc3QgJHBhcmVudENvbnRhaW5lciA9ICRhY3Rpb24uY2xvc2VzdCgnLmpzLWNob2ljZS10cmVlLWNvbnRhaW5lcicpO1xuICAgIGNvbnN0IGFjdGlvbiA9ICRhY3Rpb24uZGF0YSgnYWN0aW9uJyk7XG5cbiAgICAvLyB0b2dnbGUgYWN0aW9uIGNvbmZpZ3VyYXRpb25cbiAgICBjb25zdCBjb25maWcgPSB7XG4gICAgICBhZGRDbGFzczoge1xuICAgICAgICBleHBhbmQ6ICdleHBhbmRlZCcsXG4gICAgICAgIGNvbGxhcHNlOiAnY29sbGFwc2VkJyxcbiAgICAgIH0sXG4gICAgICByZW1vdmVDbGFzczoge1xuICAgICAgICBleHBhbmQ6ICdjb2xsYXBzZWQnLFxuICAgICAgICBjb2xsYXBzZTogJ2V4cGFuZGVkJyxcbiAgICAgIH0sXG4gICAgICBuZXh0QWN0aW9uOiB7XG4gICAgICAgIGV4cGFuZDogJ2NvbGxhcHNlJyxcbiAgICAgICAgY29sbGFwc2U6ICdleHBhbmQnLFxuICAgICAgfSxcbiAgICAgIHRleHQ6IHtcbiAgICAgICAgZXhwYW5kOiAnY29sbGFwc2VkLXRleHQnLFxuICAgICAgICBjb2xsYXBzZTogJ2V4cGFuZGVkLXRleHQnLFxuICAgICAgfSxcbiAgICAgIGljb246IHtcbiAgICAgICAgZXhwYW5kOiAnY29sbGFwc2VkLWljb24nLFxuICAgICAgICBjb2xsYXBzZTogJ2V4cGFuZGVkLWljb24nLFxuICAgICAgfSxcbiAgICB9O1xuXG4gICAgJHBhcmVudENvbnRhaW5lci5maW5kKCdsaScpLmVhY2goKGluZGV4LCBpdGVtKSA9PiB7XG4gICAgICBjb25zdCAkaXRlbSA9ICQoaXRlbSk7XG5cbiAgICAgIGlmICgkaXRlbS5oYXNDbGFzcyhjb25maWcucmVtb3ZlQ2xhc3NbYWN0aW9uXSkpIHtcbiAgICAgICAgJGl0ZW0ucmVtb3ZlQ2xhc3MoY29uZmlnLnJlbW92ZUNsYXNzW2FjdGlvbl0pXG4gICAgICAgICAgLmFkZENsYXNzKGNvbmZpZy5hZGRDbGFzc1thY3Rpb25dKTtcbiAgICAgIH1cbiAgICB9KTtcblxuICAgICRhY3Rpb24uZGF0YSgnYWN0aW9uJywgY29uZmlnLm5leHRBY3Rpb25bYWN0aW9uXSk7XG4gICAgJGFjdGlvbi5maW5kKCcubWF0ZXJpYWwtaWNvbnMnKS50ZXh0KCRhY3Rpb24uZGF0YShjb25maWcuaWNvblthY3Rpb25dKSk7XG4gICAgJGFjdGlvbi5maW5kKCcuanMtdG9nZ2xlLXRleHQnKS50ZXh0KCRhY3Rpb24uZGF0YShjb25maWcudGV4dFthY3Rpb25dKSk7XG4gIH1cbn1cbiIsIi8qKlxuICogQ29weXJpZ2h0IHNpbmNlIDIwMDcgUHJlc3RhU2hvcCBTQSBhbmQgQ29udHJpYnV0b3JzXG4gKiBQcmVzdGFTaG9wIGlzIGFuIEludGVybmF0aW9uYWwgUmVnaXN0ZXJlZCBUcmFkZW1hcmsgJiBQcm9wZXJ0eSBvZiBQcmVzdGFTaG9wIFNBXG4gKlxuICogTk9USUNFIE9GIExJQ0VOU0VcbiAqXG4gKiBUaGlzIHNvdXJjZSBmaWxlIGlzIHN1YmplY3QgdG8gdGhlIE9wZW4gU29mdHdhcmUgTGljZW5zZSAoT1NMIDMuMClcbiAqIHRoYXQgaXMgYnVuZGxlZCB3aXRoIHRoaXMgcGFja2FnZSBpbiB0aGUgZmlsZSBMSUNFTlNFLm1kLlxuICogSXQgaXMgYWxzbyBhdmFpbGFibGUgdGhyb3VnaCB0aGUgd29ybGQtd2lkZS13ZWIgYXQgdGhpcyBVUkw6XG4gKiBodHRwczovL29wZW5zb3VyY2Uub3JnL2xpY2Vuc2VzL09TTC0zLjBcbiAqIElmIHlvdSBkaWQgbm90IHJlY2VpdmUgYSBjb3B5IG9mIHRoZSBsaWNlbnNlIGFuZCBhcmUgdW5hYmxlIHRvXG4gKiBvYnRhaW4gaXQgdGhyb3VnaCB0aGUgd29ybGQtd2lkZS13ZWIsIHBsZWFzZSBzZW5kIGFuIGVtYWlsXG4gKiB0byBsaWNlbnNlQHByZXN0YXNob3AuY29tIHNvIHdlIGNhbiBzZW5kIHlvdSBhIGNvcHkgaW1tZWRpYXRlbHkuXG4gKlxuICogRElTQ0xBSU1FUlxuICpcbiAqIERvIG5vdCBlZGl0IG9yIGFkZCB0byB0aGlzIGZpbGUgaWYgeW91IHdpc2ggdG8gdXBncmFkZSBQcmVzdGFTaG9wIHRvIG5ld2VyXG4gKiB2ZXJzaW9ucyBpbiB0aGUgZnV0dXJlLiBJZiB5b3Ugd2lzaCB0byBjdXN0b21pemUgUHJlc3RhU2hvcCBmb3IgeW91clxuICogbmVlZHMgcGxlYXNlIHJlZmVyIHRvIGh0dHBzOi8vZGV2ZG9jcy5wcmVzdGFzaG9wLmNvbS8gZm9yIG1vcmUgaW5mb3JtYXRpb24uXG4gKlxuICogQGF1dGhvciAgICBQcmVzdGFTaG9wIFNBIGFuZCBDb250cmlidXRvcnMgPGNvbnRhY3RAcHJlc3Rhc2hvcC5jb20+XG4gKiBAY29weXJpZ2h0IFNpbmNlIDIwMDcgUHJlc3RhU2hvcCBTQSBhbmQgQ29udHJpYnV0b3JzXG4gKiBAbGljZW5zZSAgIGh0dHBzOi8vb3BlbnNvdXJjZS5vcmcvbGljZW5zZXMvT1NMLTMuMCBPcGVuIFNvZnR3YXJlIExpY2Vuc2UgKE9TTCAzLjApXG4gKi9cblxuLyoqXG4gKiBDbGFzcyByZXNwb25zaWJsZSBmb3IgY2hlY2tpbmcgcGFzc3dvcmQncyB2YWxpZGl0eS5cbiAqIENhbiB2YWxpZGF0ZSBlbnRlcmVkIHBhc3N3b3JkJ3MgbGVuZ3RoIGFnYWluc3QgbWluL21heCB2YWx1ZXMuXG4gKiBJZiBwYXNzd29yZCBjb25maXJtYXRpb24gaW5wdXQgaXMgcHJvdmlkZWQsIGNhbiB2YWxpZGF0ZSBpZiBlbnRlcmVkIHBhc3N3b3JkIGlzIG1hdGNoaW5nIGNvbmZpcm1hdGlvbi5cbiAqL1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgUGFzc3dvcmRWYWxpZGF0b3Ige1xuICAvKipcbiAgICogQHBhcmFtIHtTdHJpbmd9IHBhc3N3b3JkSW5wdXRTZWxlY3RvciBzZWxlY3RvciBvZiB0aGUgcGFzc3dvcmQgaW5wdXQuXG4gICAqIEBwYXJhbSB7U3RyaW5nfG51bGx9IGNvbmZpcm1QYXNzd29yZElucHV0U2VsZWN0b3IgKG9wdGlvbmFsKSBzZWxlY3RvciBmb3IgdGhlIHBhc3N3b3JkIGNvbmZpcm1hdGlvbiBpbnB1dC5cbiAgICogQHBhcmFtIHtPYmplY3R9IG9wdGlvbnMgYWxsb3dzIG92ZXJyaWRpbmcgZGVmYXVsdCBvcHRpb25zLlxuICAgKi9cbiAgY29uc3RydWN0b3IocGFzc3dvcmRJbnB1dFNlbGVjdG9yLCBjb25maXJtUGFzc3dvcmRJbnB1dFNlbGVjdG9yID0gbnVsbCwgb3B0aW9ucyA9IHt9KSB7XG4gICAgdGhpcy5uZXdQYXNzd29yZElucHV0ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihwYXNzd29yZElucHV0U2VsZWN0b3IpO1xuICAgIHRoaXMuY29uZmlybVBhc3N3b3JkSW5wdXQgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKGNvbmZpcm1QYXNzd29yZElucHV0U2VsZWN0b3IpO1xuXG4gICAgLy8gTWluaW11bSBhbGxvd2VkIGxlbmd0aCBmb3IgZW50ZXJlZCBwYXNzd29yZFxuICAgIHRoaXMubWluUGFzc3dvcmRMZW5ndGggPSBvcHRpb25zLm1pblBhc3N3b3JkTGVuZ3RoIHx8IDg7XG5cbiAgICAvLyBNYXhpbXVtIGFsbG93ZWQgbGVuZ3RoIGZvciBlbnRlcmVkIHBhc3N3b3JkXG4gICAgdGhpcy5tYXhQYXNzd29yZExlbmd0aCA9IG9wdGlvbnMubWF4UGFzc3dvcmRMZW5ndGggfHwgMjU1O1xuICB9XG5cbiAgLyoqXG4gICAqIENoZWNrIGlmIHRoZSBwYXNzd29yZCBpcyB2YWxpZC5cbiAgICpcbiAgICogQHJldHVybnMge2Jvb2xlYW59XG4gICAqL1xuICBpc1Bhc3N3b3JkVmFsaWQoKSB7XG4gICAgaWYgKHRoaXMuY29uZmlybVBhc3N3b3JkSW5wdXQgJiYgIXRoaXMuaXNQYXNzd29yZE1hdGNoaW5nQ29uZmlybWF0aW9uKCkpIHtcbiAgICAgIHJldHVybiBmYWxzZTtcbiAgICB9XG5cbiAgICByZXR1cm4gdGhpcy5pc1Bhc3N3b3JkTGVuZ3RoVmFsaWQoKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBDaGVjayBpZiBwYXNzd29yZCdzIGxlbmd0aCBpcyB2YWxpZC5cbiAgICpcbiAgICogQHJldHVybnMge2Jvb2xlYW59XG4gICAqL1xuICBpc1Bhc3N3b3JkTGVuZ3RoVmFsaWQoKSB7XG4gICAgcmV0dXJuICF0aGlzLmlzUGFzc3dvcmRUb29TaG9ydCgpICYmICF0aGlzLmlzUGFzc3dvcmRUb29Mb25nKCk7XG4gIH1cblxuICAvKipcbiAgICogQ2hlY2sgaWYgcGFzc3dvcmQgaXMgbWF0Y2hpbmcgaXQncyBjb25maXJtYXRpb24uXG4gICAqXG4gICAqIEByZXR1cm5zIHtib29sZWFufVxuICAgKi9cbiAgaXNQYXNzd29yZE1hdGNoaW5nQ29uZmlybWF0aW9uKCkge1xuICAgIGlmICghdGhpcy5jb25maXJtUGFzc3dvcmRJbnB1dCkge1xuICAgICAgdGhyb3cgbmV3IEVycm9yKCdDb25maXJtIHBhc3N3b3JkIGlucHV0IGlzIG5vdCBwcm92aWRlZCBmb3IgdGhlIHBhc3N3b3JkIHZhbGlkYXRvci4nKTtcbiAgICB9XG5cbiAgICBpZiAodGhpcy5jb25maXJtUGFzc3dvcmRJbnB1dC52YWx1ZSA9PT0gJycpIHtcbiAgICAgIHJldHVybiB0cnVlO1xuICAgIH1cblxuICAgIHJldHVybiB0aGlzLm5ld1Bhc3N3b3JkSW5wdXQudmFsdWUgPT09IHRoaXMuY29uZmlybVBhc3N3b3JkSW5wdXQudmFsdWU7XG4gIH1cblxuICAvKipcbiAgICogQ2hlY2sgaWYgcGFzc3dvcmQgaXMgdG9vIHNob3J0LlxuICAgKlxuICAgKiBAcmV0dXJucyB7Ym9vbGVhbn1cbiAgICovXG4gIGlzUGFzc3dvcmRUb29TaG9ydCgpIHtcbiAgICByZXR1cm4gdGhpcy5uZXdQYXNzd29yZElucHV0LnZhbHVlLmxlbmd0aCA8IHRoaXMubWluUGFzc3dvcmRMZW5ndGg7XG4gIH1cblxuICAvKipcbiAgICogQ2hlY2sgaWYgcGFzc3dvcmQgaXMgdG9vIGxvbmcuXG4gICAqXG4gICAqIEByZXR1cm5zIHtib29sZWFufVxuICAgKi9cbiAgaXNQYXNzd29yZFRvb0xvbmcoKSB7XG4gICAgcmV0dXJuIHRoaXMubmV3UGFzc3dvcmRJbnB1dC52YWx1ZS5sZW5ndGggPiB0aGlzLm1heFBhc3N3b3JkTGVuZ3RoO1xuICB9XG59XG4iLCIvKipcbiAqIENvcHlyaWdodCBzaW5jZSAyMDA3IFByZXN0YVNob3AgU0EgYW5kIENvbnRyaWJ1dG9yc1xuICogUHJlc3RhU2hvcCBpcyBhbiBJbnRlcm5hdGlvbmFsIFJlZ2lzdGVyZWQgVHJhZGVtYXJrICYgUHJvcGVydHkgb2YgUHJlc3RhU2hvcCBTQVxuICpcbiAqIE5PVElDRSBPRiBMSUNFTlNFXG4gKlxuICogVGhpcyBzb3VyY2UgZmlsZSBpcyBzdWJqZWN0IHRvIHRoZSBPcGVuIFNvZnR3YXJlIExpY2Vuc2UgKE9TTCAzLjApXG4gKiB0aGF0IGlzIGJ1bmRsZWQgd2l0aCB0aGlzIHBhY2thZ2UgaW4gdGhlIGZpbGUgTElDRU5TRS5tZC5cbiAqIEl0IGlzIGFsc28gYXZhaWxhYmxlIHRocm91Z2ggdGhlIHdvcmxkLXdpZGUtd2ViIGF0IHRoaXMgVVJMOlxuICogaHR0cHM6Ly9vcGVuc291cmNlLm9yZy9saWNlbnNlcy9PU0wtMy4wXG4gKiBJZiB5b3UgZGlkIG5vdCByZWNlaXZlIGEgY29weSBvZiB0aGUgbGljZW5zZSBhbmQgYXJlIHVuYWJsZSB0b1xuICogb2J0YWluIGl0IHRocm91Z2ggdGhlIHdvcmxkLXdpZGUtd2ViLCBwbGVhc2Ugc2VuZCBhbiBlbWFpbFxuICogdG8gbGljZW5zZUBwcmVzdGFzaG9wLmNvbSBzbyB3ZSBjYW4gc2VuZCB5b3UgYSBjb3B5IGltbWVkaWF0ZWx5LlxuICpcbiAqIERJU0NMQUlNRVJcbiAqXG4gKiBEbyBub3QgZWRpdCBvciBhZGQgdG8gdGhpcyBmaWxlIGlmIHlvdSB3aXNoIHRvIHVwZ3JhZGUgUHJlc3RhU2hvcCB0byBuZXdlclxuICogdmVyc2lvbnMgaW4gdGhlIGZ1dHVyZS4gSWYgeW91IHdpc2ggdG8gY3VzdG9taXplIFByZXN0YVNob3AgZm9yIHlvdXJcbiAqIG5lZWRzIHBsZWFzZSByZWZlciB0byBodHRwczovL2RldmRvY3MucHJlc3Rhc2hvcC5jb20vIGZvciBtb3JlIGluZm9ybWF0aW9uLlxuICpcbiAqIEBhdXRob3IgICAgUHJlc3RhU2hvcCBTQSBhbmQgQ29udHJpYnV0b3JzIDxjb250YWN0QHByZXN0YXNob3AuY29tPlxuICogQGNvcHlyaWdodCBTaW5jZSAyMDA3IFByZXN0YVNob3AgU0EgYW5kIENvbnRyaWJ1dG9yc1xuICogQGxpY2Vuc2UgICBodHRwczovL29wZW5zb3VyY2Uub3JnL2xpY2Vuc2VzL09TTC0zLjAgT3BlbiBTb2Z0d2FyZSBMaWNlbnNlIChPU0wgMy4wKVxuICovXG5cbmltcG9ydCBDaG9pY2VUcmVlIGZyb20gJy4uLy4uL2NvbXBvbmVudHMvZm9ybS9jaG9pY2UtdHJlZSc7XG5pbXBvcnQgQWRkb25zQ29ubmVjdG9yIGZyb20gJy4uLy4uL2NvbXBvbmVudHMvYWRkb25zLWNvbm5lY3Rvcic7XG5pbXBvcnQgQ2hhbmdlUGFzc3dvcmRDb250cm9sIGZyb20gJy4uLy4uL2NvbXBvbmVudHMvZm9ybS9jaGFuZ2UtcGFzc3dvcmQtY29udHJvbCc7XG5pbXBvcnQgZW1wbG95ZWVGb3JtTWFwIGZyb20gJy4vZW1wbG95ZWUtZm9ybS1tYXAnO1xuXG4vKipcbiAqIENsYXNzIHJlc3BvbnNpYmxlIGZvciBqYXZhc2NyaXB0IGFjdGlvbnMgaW4gZW1wbG95ZWUgYWRkL2VkaXQgcGFnZS5cbiAqL1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgRW1wbG95ZWVGb3JtIHtcbiAgY29uc3RydWN0b3IoKSB7XG4gICAgdGhpcy5zaG9wQ2hvaWNlVHJlZVNlbGVjdG9yID0gZW1wbG95ZWVGb3JtTWFwLnNob3BDaG9pY2VUcmVlO1xuICAgIHRoaXMuc2hvcENob2ljZVRyZWUgPSBuZXcgQ2hvaWNlVHJlZSh0aGlzLnNob3BDaG9pY2VUcmVlU2VsZWN0b3IpO1xuICAgIHRoaXMuZW1wbG95ZWVQcm9maWxlU2VsZWN0b3IgPSBlbXBsb3llZUZvcm1NYXAucHJvZmlsZVNlbGVjdDtcbiAgICB0aGlzLnRhYnNEcm9wZG93blNlbGVjdG9yID0gZW1wbG95ZWVGb3JtTWFwLmRlZmF1bHRQYWdlU2VsZWN0O1xuXG4gICAgdGhpcy5zaG9wQ2hvaWNlVHJlZS5lbmFibGVBdXRvQ2hlY2tDaGlsZHJlbigpO1xuXG4gICAgbmV3IEFkZG9uc0Nvbm5lY3RvcihcbiAgICAgIGVtcGxveWVlRm9ybU1hcC5hZGRvbnNDb25uZWN0Rm9ybSxcbiAgICAgIGVtcGxveWVlRm9ybU1hcC5hZGRvbnNMb2dpbkJ1dHRvbixcbiAgICApO1xuXG4gICAgbmV3IENoYW5nZVBhc3N3b3JkQ29udHJvbChcbiAgICAgIGVtcGxveWVlRm9ybU1hcC5jaGFuZ2VQYXNzd29yZElucHV0c0Jsb2NrLFxuICAgICAgZW1wbG95ZWVGb3JtTWFwLnNob3dDaGFuZ2VQYXNzd29yZEJsb2NrQnV0dG9uLFxuICAgICAgZW1wbG95ZWVGb3JtTWFwLmhpZGVDaGFuZ2VQYXNzd29yZEJsb2NrQnV0dG9uLFxuICAgICAgZW1wbG95ZWVGb3JtTWFwLmdlbmVyYXRlUGFzc3dvcmRCdXR0b24sXG4gICAgICBlbXBsb3llZUZvcm1NYXAub2xkUGFzc3dvcmRJbnB1dCxcbiAgICAgIGVtcGxveWVlRm9ybU1hcC5uZXdQYXNzd29yZElucHV0LFxuICAgICAgZW1wbG95ZWVGb3JtTWFwLmNvbmZpcm1OZXdQYXNzd29yZElucHV0LFxuICAgICAgZW1wbG95ZWVGb3JtTWFwLmdlbmVyYXRlZFBhc3N3b3JkRGlzcGxheUlucHV0LFxuICAgICAgZW1wbG95ZWVGb3JtTWFwLnBhc3N3b3JkU3RyZW5ndGhGZWVkYmFja0NvbnRhaW5lcixcbiAgICApO1xuXG4gICAgdGhpcy5pbml0RXZlbnRzKCk7XG4gICAgdGhpcy50b2dnbGVTaG9wVHJlZSgpO1xuXG4gICAgcmV0dXJuIHt9O1xuICB9XG5cbiAgLyoqXG4gICAqIEluaXRpYWxpemUgcGFnZSdzIGV2ZW50cy5cbiAgICpcbiAgICogQHByaXZhdGVcbiAgICovXG4gIGluaXRFdmVudHMoKSB7XG4gICAgY29uc3QgJGVtcGxveWVlUHJvZmlsZXNEcm9wZG93biA9ICQodGhpcy5lbXBsb3llZVByb2ZpbGVTZWxlY3Rvcik7XG4gICAgY29uc3QgZ2V0VGFic1VybCA9ICRlbXBsb3llZVByb2ZpbGVzRHJvcGRvd24uZGF0YSgnZ2V0LXRhYnMtdXJsJyk7XG5cbiAgICAkKGRvY3VtZW50KS5vbignY2hhbmdlJywgdGhpcy5lbXBsb3llZVByb2ZpbGVTZWxlY3RvciwgKCkgPT4gdGhpcy50b2dnbGVTaG9wVHJlZSgpKTtcblxuICAgIC8vIFJlbG9hZCB0YWJzIGRyb3Bkb3duIHdoZW4gZW1wbG95ZWUgcHJvZmlsZSBpcyBjaGFuZ2VkLlxuICAgICQoZG9jdW1lbnQpLm9uKCdjaGFuZ2UnLCB0aGlzLmVtcGxveWVlUHJvZmlsZVNlbGVjdG9yLCAoZXZlbnQpID0+IHtcbiAgICAgICQuZ2V0KFxuICAgICAgICBnZXRUYWJzVXJsLFxuICAgICAgICB7XG4gICAgICAgICAgcHJvZmlsZUlkOiAkKGV2ZW50LmN1cnJlbnRUYXJnZXQpLnZhbCgpLFxuICAgICAgICB9LFxuICAgICAgICAodGFicykgPT4ge1xuICAgICAgICAgIHRoaXMucmVsb2FkVGFic0Ryb3Bkb3duKHRhYnMpO1xuICAgICAgICB9LFxuICAgICAgICAnanNvbicsXG4gICAgICApO1xuICAgIH0pO1xuICB9XG5cbiAgLyoqXG4gICAqIFJlbG9hZCB0YWJzIGRyb3Bkb3duIHdpdGggbmV3IGNvbnRlbnQuXG4gICAqXG4gICAqIEBwYXJhbSB7T2JqZWN0fSBhY2Nlc3NpYmxlVGFic1xuICAgKlxuICAgKiBAcHJpdmF0ZVxuICAgKi9cbiAgcmVsb2FkVGFic0Ryb3Bkb3duKGFjY2Vzc2libGVUYWJzKSB7XG4gICAgY29uc3QgJHRhYnNEcm9wZG93biA9ICQodGhpcy50YWJzRHJvcGRvd25TZWxlY3Rvcik7XG5cbiAgICAkdGFic0Ryb3Bkb3duLmVtcHR5KCk7XG5cbiAgICBPYmplY3QudmFsdWVzKGFjY2Vzc2libGVUYWJzKS5mb3JFYWNoKChhY2Nlc3NpYmxlVGFiKSA9PiB7XG4gICAgICBpZiAoYWNjZXNzaWJsZVRhYi5jaGlsZHJlbi5sZW5ndGggPiAwICYmIGFjY2Vzc2libGVUYWIubmFtZSkge1xuICAgICAgICAvLyBJZiB0YWIgaGFzIGNoaWxkcmVuIC0gY3JlYXRlIGFuIG9wdGlvbiBncm91cCBhbmQgcHV0IGNoaWxkcmVuIGluc2lkZS5cbiAgICAgICAgY29uc3QgJG9wdGdyb3VwID0gdGhpcy5jcmVhdGVPcHRpb25Hcm91cChhY2Nlc3NpYmxlVGFiLm5hbWUpO1xuXG4gICAgICAgIE9iamVjdC5rZXlzKGFjY2Vzc2libGVUYWIuY2hpbGRyZW4pLmZvckVhY2goKGNoaWxkS2V5KSA9PiB7XG4gICAgICAgICAgaWYgKGFjY2Vzc2libGVUYWIuY2hpbGRyZW5bY2hpbGRLZXldLm5hbWUpIHtcbiAgICAgICAgICAgICRvcHRncm91cC5hcHBlbmQoXG4gICAgICAgICAgICAgIHRoaXMuY3JlYXRlT3B0aW9uKFxuICAgICAgICAgICAgICAgIGFjY2Vzc2libGVUYWIuY2hpbGRyZW5bY2hpbGRLZXldLm5hbWUsXG4gICAgICAgICAgICAgICAgYWNjZXNzaWJsZVRhYi5jaGlsZHJlbltjaGlsZEtleV0uaWRfdGFiKSxcbiAgICAgICAgICAgICk7XG4gICAgICAgICAgfVxuICAgICAgICB9KTtcblxuICAgICAgICAkdGFic0Ryb3Bkb3duLmFwcGVuZCgkb3B0Z3JvdXApO1xuICAgICAgfSBlbHNlIGlmIChhY2Nlc3NpYmxlVGFiLm5hbWUpIHtcbiAgICAgICAgLy8gSWYgdGFiIGRvZXNuJ3QgaGF2ZSBjaGlsZHJlbiAtIGNyZWF0ZSBhbiBvcHRpb24uXG4gICAgICAgICR0YWJzRHJvcGRvd24uYXBwZW5kKFxuICAgICAgICAgIHRoaXMuY3JlYXRlT3B0aW9uKFxuICAgICAgICAgICAgYWNjZXNzaWJsZVRhYi5uYW1lLFxuICAgICAgICAgICAgYWNjZXNzaWJsZVRhYi5pZF90YWIsXG4gICAgICAgICAgKSxcbiAgICAgICAgKTtcbiAgICAgIH1cbiAgICB9KTtcbiAgfVxuXG4gIC8qKlxuICAgKiBIaWRlIHNob3AgY2hvaWNlIHRyZWUgaWYgc3VwZXJhZG1pbiBwcm9maWxlIGlzIHNlbGVjdGVkLCBzaG93IGl0IG90aGVyd2lzZS5cbiAgICpcbiAgICogQHByaXZhdGVcbiAgICovXG4gIHRvZ2dsZVNob3BUcmVlKCkge1xuICAgIGNvbnN0ICRlbXBsb3llZVByb2ZpbGVEcm9wZG93biA9ICQodGhpcy5lbXBsb3llZVByb2ZpbGVTZWxlY3Rvcik7XG4gICAgY29uc3Qgc3VwZXJBZG1pblByb2ZpbGVJZCA9ICRlbXBsb3llZVByb2ZpbGVEcm9wZG93bi5kYXRhKCdhZG1pbi1wcm9maWxlJyk7XG4gICAgJCh0aGlzLnNob3BDaG9pY2VUcmVlU2VsZWN0b3IpXG4gICAgICAuY2xvc2VzdCgnLmZvcm0tZ3JvdXAnKVxuICAgICAgLnRvZ2dsZUNsYXNzKCdkLW5vbmUnLCAkZW1wbG95ZWVQcm9maWxlRHJvcGRvd24udmFsKCkgPT09IHN1cGVyQWRtaW5Qcm9maWxlSWQpO1xuICB9XG5cbiAgLyoqXG4gICAqIENyZWF0ZXMgYW4gPG9wdGdyb3VwPiBlbGVtZW50XG4gICAqXG4gICAqIEBwYXJhbSB7U3RyaW5nfSBuYW1lXG4gICAqXG4gICAqIEByZXR1cm5zIHtqUXVlcnl9XG4gICAqXG4gICAqIEBwcml2YXRlXG4gICAqL1xuICBjcmVhdGVPcHRpb25Hcm91cChuYW1lKSB7XG4gICAgcmV0dXJuICQoYDxvcHRncm91cCBsYWJlbD1cIiR7bmFtZX1cIj5gKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBDcmVhdGVzIGFuIDxvcHRpb24+IGVsZW1lbnQuXG4gICAqXG4gICAqIEBwYXJhbSB7U3RyaW5nfSBuYW1lXG4gICAqIEBwYXJhbSB7U3RyaW5nfSB2YWx1ZVxuICAgKlxuICAgKiBAcmV0dXJucyB7alF1ZXJ5fVxuICAgKlxuICAgKiBAcHJpdmF0ZVxuICAgKi9cbiAgY3JlYXRlT3B0aW9uKG5hbWUsIHZhbHVlKSB7XG4gICAgcmV0dXJuICQoYDxvcHRpb24gdmFsdWU9XCIke3ZhbHVlfVwiPiR7bmFtZX08L29wdGlvbj5gKTtcbiAgfVxufVxuIiwiLyoqXG4gKiBDb3B5cmlnaHQgc2luY2UgMjAwNyBQcmVzdGFTaG9wIFNBIGFuZCBDb250cmlidXRvcnNcbiAqIFByZXN0YVNob3AgaXMgYW4gSW50ZXJuYXRpb25hbCBSZWdpc3RlcmVkIFRyYWRlbWFyayAmIFByb3BlcnR5IG9mIFByZXN0YVNob3AgU0FcbiAqXG4gKiBOT1RJQ0UgT0YgTElDRU5TRVxuICpcbiAqIFRoaXMgc291cmNlIGZpbGUgaXMgc3ViamVjdCB0byB0aGUgT3BlbiBTb2Z0d2FyZSBMaWNlbnNlIChPU0wgMy4wKVxuICogdGhhdCBpcyBidW5kbGVkIHdpdGggdGhpcyBwYWNrYWdlIGluIHRoZSBmaWxlIExJQ0VOU0UubWQuXG4gKiBJdCBpcyBhbHNvIGF2YWlsYWJsZSB0aHJvdWdoIHRoZSB3b3JsZC13aWRlLXdlYiBhdCB0aGlzIFVSTDpcbiAqIGh0dHBzOi8vb3BlbnNvdXJjZS5vcmcvbGljZW5zZXMvT1NMLTMuMFxuICogSWYgeW91IGRpZCBub3QgcmVjZWl2ZSBhIGNvcHkgb2YgdGhlIGxpY2Vuc2UgYW5kIGFyZSB1bmFibGUgdG9cbiAqIG9idGFpbiBpdCB0aHJvdWdoIHRoZSB3b3JsZC13aWRlLXdlYiwgcGxlYXNlIHNlbmQgYW4gZW1haWxcbiAqIHRvIGxpY2Vuc2VAcHJlc3Rhc2hvcC5jb20gc28gd2UgY2FuIHNlbmQgeW91IGEgY29weSBpbW1lZGlhdGVseS5cbiAqXG4gKiBESVNDTEFJTUVSXG4gKlxuICogRG8gbm90IGVkaXQgb3IgYWRkIHRvIHRoaXMgZmlsZSBpZiB5b3Ugd2lzaCB0byB1cGdyYWRlIFByZXN0YVNob3AgdG8gbmV3ZXJcbiAqIHZlcnNpb25zIGluIHRoZSBmdXR1cmUuIElmIHlvdSB3aXNoIHRvIGN1c3RvbWl6ZSBQcmVzdGFTaG9wIGZvciB5b3VyXG4gKiBuZWVkcyBwbGVhc2UgcmVmZXIgdG8gaHR0cHM6Ly9kZXZkb2NzLnByZXN0YXNob3AuY29tLyBmb3IgbW9yZSBpbmZvcm1hdGlvbi5cbiAqXG4gKiBAYXV0aG9yICAgIFByZXN0YVNob3AgU0EgYW5kIENvbnRyaWJ1dG9ycyA8Y29udGFjdEBwcmVzdGFzaG9wLmNvbT5cbiAqIEBjb3B5cmlnaHQgU2luY2UgMjAwNyBQcmVzdGFTaG9wIFNBIGFuZCBDb250cmlidXRvcnNcbiAqIEBsaWNlbnNlICAgaHR0cHM6Ly9vcGVuc291cmNlLm9yZy9saWNlbnNlcy9PU0wtMy4wIE9wZW4gU29mdHdhcmUgTGljZW5zZSAoT1NMIDMuMClcbiAqL1xuXG4vKipcbiAqIERlZmluZXMgYWxsIHNlbGVjdG9ycyB0aGF0IGFyZSB1c2VkIGluIGVtcGxveWVlIGFkZC9lZGl0IGZvcm0uXG4gKi9cbmV4cG9ydCBkZWZhdWx0IHtcbiAgc2hvcENob2ljZVRyZWU6ICcjZW1wbG95ZWVfc2hvcF9hc3NvY2lhdGlvbicsXG4gIHByb2ZpbGVTZWxlY3Q6ICcjZW1wbG95ZWVfcHJvZmlsZScsXG4gIGRlZmF1bHRQYWdlU2VsZWN0OiAnI2VtcGxveWVlX2RlZmF1bHRfcGFnZScsXG4gIGFkZG9uc0Nvbm5lY3RGb3JtOiAnI2FkZG9ucy1jb25uZWN0LWZvcm0nLFxuICBhZGRvbnNMb2dpbkJ1dHRvbjogJyNhZGRvbnNfbG9naW5fYnRuJyxcblxuICAvLyBzZWxlY3RvcnMgcmVsYXRlZCB0byBcImNoYW5nZSBwYXNzd29yZFwiIGZvcm0gY29udHJvbFxuICBjaGFuZ2VQYXNzd29yZElucHV0c0Jsb2NrOiAnLmpzLWNoYW5nZS1wYXNzd29yZC1ibG9jaycsXG4gIHNob3dDaGFuZ2VQYXNzd29yZEJsb2NrQnV0dG9uOiAnLmpzLWNoYW5nZS1wYXNzd29yZCcsXG4gIGhpZGVDaGFuZ2VQYXNzd29yZEJsb2NrQnV0dG9uOiAnLmpzLWNoYW5nZS1wYXNzd29yZC1jYW5jZWwnLFxuICBnZW5lcmF0ZVBhc3N3b3JkQnV0dG9uOiAnI2VtcGxveWVlX2NoYW5nZV9wYXNzd29yZF9nZW5lcmF0ZV9wYXNzd29yZF9idXR0b24nLFxuICBvbGRQYXNzd29yZElucHV0OiAnI2VtcGxveWVlX2NoYW5nZV9wYXNzd29yZF9vbGRfcGFzc3dvcmQnLFxuICBuZXdQYXNzd29yZElucHV0OiAnI2VtcGxveWVlX2NoYW5nZV9wYXNzd29yZF9uZXdfcGFzc3dvcmRfZmlyc3QnLFxuICBjb25maXJtTmV3UGFzc3dvcmRJbnB1dDogJyNlbXBsb3llZV9jaGFuZ2VfcGFzc3dvcmRfbmV3X3Bhc3N3b3JkX3NlY29uZCcsXG4gIGdlbmVyYXRlZFBhc3N3b3JkRGlzcGxheUlucHV0OiAnI2VtcGxveWVlX2NoYW5nZV9wYXNzd29yZF9nZW5lcmF0ZWRfcGFzc3dvcmQnLFxuICBwYXNzd29yZFN0cmVuZ3RoRmVlZGJhY2tDb250YWluZXI6ICcuanMtcGFzc3dvcmQtc3RyZW5ndGgtZmVlZGJhY2snLFxufTtcbiIsIm1vZHVsZS5leHBvcnRzID0geyBcImRlZmF1bHRcIjogcmVxdWlyZShcImNvcmUtanMvbGlicmFyeS9mbi9vYmplY3QvZGVmaW5lLXByb3BlcnR5XCIpLCBfX2VzTW9kdWxlOiB0cnVlIH07IiwibW9kdWxlLmV4cG9ydHMgPSB7IFwiZGVmYXVsdFwiOiByZXF1aXJlKFwiY29yZS1qcy9saWJyYXJ5L2ZuL29iamVjdC9rZXlzXCIpLCBfX2VzTW9kdWxlOiB0cnVlIH07IiwibW9kdWxlLmV4cG9ydHMgPSB7IFwiZGVmYXVsdFwiOiByZXF1aXJlKFwiY29yZS1qcy9saWJyYXJ5L2ZuL29iamVjdC92YWx1ZXNcIiksIF9fZXNNb2R1bGU6IHRydWUgfTsiLCJcInVzZSBzdHJpY3RcIjtcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gZnVuY3Rpb24gKGluc3RhbmNlLCBDb25zdHJ1Y3Rvcikge1xuICBpZiAoIShpbnN0YW5jZSBpbnN0YW5jZW9mIENvbnN0cnVjdG9yKSkge1xuICAgIHRocm93IG5ldyBUeXBlRXJyb3IoXCJDYW5ub3QgY2FsbCBhIGNsYXNzIGFzIGEgZnVuY3Rpb25cIik7XG4gIH1cbn07IiwiXCJ1c2Ugc3RyaWN0XCI7XG5cbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG5cbnZhciBfZGVmaW5lUHJvcGVydHkgPSByZXF1aXJlKFwiLi4vY29yZS1qcy9vYmplY3QvZGVmaW5lLXByb3BlcnR5XCIpO1xuXG52YXIgX2RlZmluZVByb3BlcnR5MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2RlZmluZVByb3BlcnR5KTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxuZXhwb3J0cy5kZWZhdWx0ID0gZnVuY3Rpb24gKCkge1xuICBmdW5jdGlvbiBkZWZpbmVQcm9wZXJ0aWVzKHRhcmdldCwgcHJvcHMpIHtcbiAgICBmb3IgKHZhciBpID0gMDsgaSA8IHByb3BzLmxlbmd0aDsgaSsrKSB7XG4gICAgICB2YXIgZGVzY3JpcHRvciA9IHByb3BzW2ldO1xuICAgICAgZGVzY3JpcHRvci5lbnVtZXJhYmxlID0gZGVzY3JpcHRvci5lbnVtZXJhYmxlIHx8IGZhbHNlO1xuICAgICAgZGVzY3JpcHRvci5jb25maWd1cmFibGUgPSB0cnVlO1xuICAgICAgaWYgKFwidmFsdWVcIiBpbiBkZXNjcmlwdG9yKSBkZXNjcmlwdG9yLndyaXRhYmxlID0gdHJ1ZTtcbiAgICAgICgwLCBfZGVmaW5lUHJvcGVydHkyLmRlZmF1bHQpKHRhcmdldCwgZGVzY3JpcHRvci5rZXksIGRlc2NyaXB0b3IpO1xuICAgIH1cbiAgfVxuXG4gIHJldHVybiBmdW5jdGlvbiAoQ29uc3RydWN0b3IsIHByb3RvUHJvcHMsIHN0YXRpY1Byb3BzKSB7XG4gICAgaWYgKHByb3RvUHJvcHMpIGRlZmluZVByb3BlcnRpZXMoQ29uc3RydWN0b3IucHJvdG90eXBlLCBwcm90b1Byb3BzKTtcbiAgICBpZiAoc3RhdGljUHJvcHMpIGRlZmluZVByb3BlcnRpZXMoQ29uc3RydWN0b3IsIHN0YXRpY1Byb3BzKTtcbiAgICByZXR1cm4gQ29uc3RydWN0b3I7XG4gIH07XG59KCk7IiwicmVxdWlyZSgnLi4vLi4vbW9kdWxlcy9lczYub2JqZWN0LmRlZmluZS1wcm9wZXJ0eScpO1xudmFyICRPYmplY3QgPSByZXF1aXJlKCcuLi8uLi9tb2R1bGVzL19jb3JlJykuT2JqZWN0O1xubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBkZWZpbmVQcm9wZXJ0eShpdCwga2V5LCBkZXNjKSB7XG4gIHJldHVybiAkT2JqZWN0LmRlZmluZVByb3BlcnR5KGl0LCBrZXksIGRlc2MpO1xufTtcbiIsInJlcXVpcmUoJy4uLy4uL21vZHVsZXMvZXM2Lm9iamVjdC5rZXlzJyk7XG5tb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoJy4uLy4uL21vZHVsZXMvX2NvcmUnKS5PYmplY3Qua2V5cztcbiIsInJlcXVpcmUoJy4uLy4uL21vZHVsZXMvZXM3Lm9iamVjdC52YWx1ZXMnKTtcbm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZSgnLi4vLi4vbW9kdWxlcy9fY29yZScpLk9iamVjdC52YWx1ZXM7XG4iLCJtb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIChpdCkge1xuICBpZiAodHlwZW9mIGl0ICE9ICdmdW5jdGlvbicpIHRocm93IFR5cGVFcnJvcihpdCArICcgaXMgbm90IGEgZnVuY3Rpb24hJyk7XG4gIHJldHVybiBpdDtcbn07XG4iLCJ2YXIgaXNPYmplY3QgPSByZXF1aXJlKCcuL19pcy1vYmplY3QnKTtcbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKGl0KSB7XG4gIGlmICghaXNPYmplY3QoaXQpKSB0aHJvdyBUeXBlRXJyb3IoaXQgKyAnIGlzIG5vdCBhbiBvYmplY3QhJyk7XG4gIHJldHVybiBpdDtcbn07XG4iLCIvLyBmYWxzZSAtPiBBcnJheSNpbmRleE9mXG4vLyB0cnVlICAtPiBBcnJheSNpbmNsdWRlc1xudmFyIHRvSU9iamVjdCA9IHJlcXVpcmUoJy4vX3RvLWlvYmplY3QnKTtcbnZhciB0b0xlbmd0aCA9IHJlcXVpcmUoJy4vX3RvLWxlbmd0aCcpO1xudmFyIHRvQWJzb2x1dGVJbmRleCA9IHJlcXVpcmUoJy4vX3RvLWFic29sdXRlLWluZGV4Jyk7XG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIChJU19JTkNMVURFUykge1xuICByZXR1cm4gZnVuY3Rpb24gKCR0aGlzLCBlbCwgZnJvbUluZGV4KSB7XG4gICAgdmFyIE8gPSB0b0lPYmplY3QoJHRoaXMpO1xuICAgIHZhciBsZW5ndGggPSB0b0xlbmd0aChPLmxlbmd0aCk7XG4gICAgdmFyIGluZGV4ID0gdG9BYnNvbHV0ZUluZGV4KGZyb21JbmRleCwgbGVuZ3RoKTtcbiAgICB2YXIgdmFsdWU7XG4gICAgLy8gQXJyYXkjaW5jbHVkZXMgdXNlcyBTYW1lVmFsdWVaZXJvIGVxdWFsaXR5IGFsZ29yaXRobVxuICAgIC8vIGVzbGludC1kaXNhYmxlLW5leHQtbGluZSBuby1zZWxmLWNvbXBhcmVcbiAgICBpZiAoSVNfSU5DTFVERVMgJiYgZWwgIT0gZWwpIHdoaWxlIChsZW5ndGggPiBpbmRleCkge1xuICAgICAgdmFsdWUgPSBPW2luZGV4KytdO1xuICAgICAgLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lIG5vLXNlbGYtY29tcGFyZVxuICAgICAgaWYgKHZhbHVlICE9IHZhbHVlKSByZXR1cm4gdHJ1ZTtcbiAgICAvLyBBcnJheSNpbmRleE9mIGlnbm9yZXMgaG9sZXMsIEFycmF5I2luY2x1ZGVzIC0gbm90XG4gICAgfSBlbHNlIGZvciAoO2xlbmd0aCA+IGluZGV4OyBpbmRleCsrKSBpZiAoSVNfSU5DTFVERVMgfHwgaW5kZXggaW4gTykge1xuICAgICAgaWYgKE9baW5kZXhdID09PSBlbCkgcmV0dXJuIElTX0lOQ0xVREVTIHx8IGluZGV4IHx8IDA7XG4gICAgfSByZXR1cm4gIUlTX0lOQ0xVREVTICYmIC0xO1xuICB9O1xufTtcbiIsInZhciB0b1N0cmluZyA9IHt9LnRvU3RyaW5nO1xuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIChpdCkge1xuICByZXR1cm4gdG9TdHJpbmcuY2FsbChpdCkuc2xpY2UoOCwgLTEpO1xufTtcbiIsInZhciBjb3JlID0gbW9kdWxlLmV4cG9ydHMgPSB7IHZlcnNpb246ICcyLjYuMTEnIH07XG5pZiAodHlwZW9mIF9fZSA9PSAnbnVtYmVyJykgX19lID0gY29yZTsgLy8gZXNsaW50LWRpc2FibGUtbGluZSBuby11bmRlZlxuIiwiLy8gb3B0aW9uYWwgLyBzaW1wbGUgY29udGV4dCBiaW5kaW5nXG52YXIgYUZ1bmN0aW9uID0gcmVxdWlyZSgnLi9fYS1mdW5jdGlvbicpO1xubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAoZm4sIHRoYXQsIGxlbmd0aCkge1xuICBhRnVuY3Rpb24oZm4pO1xuICBpZiAodGhhdCA9PT0gdW5kZWZpbmVkKSByZXR1cm4gZm47XG4gIHN3aXRjaCAobGVuZ3RoKSB7XG4gICAgY2FzZSAxOiByZXR1cm4gZnVuY3Rpb24gKGEpIHtcbiAgICAgIHJldHVybiBmbi5jYWxsKHRoYXQsIGEpO1xuICAgIH07XG4gICAgY2FzZSAyOiByZXR1cm4gZnVuY3Rpb24gKGEsIGIpIHtcbiAgICAgIHJldHVybiBmbi5jYWxsKHRoYXQsIGEsIGIpO1xuICAgIH07XG4gICAgY2FzZSAzOiByZXR1cm4gZnVuY3Rpb24gKGEsIGIsIGMpIHtcbiAgICAgIHJldHVybiBmbi5jYWxsKHRoYXQsIGEsIGIsIGMpO1xuICAgIH07XG4gIH1cbiAgcmV0dXJuIGZ1bmN0aW9uICgvKiAuLi5hcmdzICovKSB7XG4gICAgcmV0dXJuIGZuLmFwcGx5KHRoYXQsIGFyZ3VtZW50cyk7XG4gIH07XG59O1xuIiwiLy8gNy4yLjEgUmVxdWlyZU9iamVjdENvZXJjaWJsZShhcmd1bWVudClcbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKGl0KSB7XG4gIGlmIChpdCA9PSB1bmRlZmluZWQpIHRocm93IFR5cGVFcnJvcihcIkNhbid0IGNhbGwgbWV0aG9kIG9uICBcIiArIGl0KTtcbiAgcmV0dXJuIGl0O1xufTtcbiIsIi8vIFRoYW5rJ3MgSUU4IGZvciBoaXMgZnVubnkgZGVmaW5lUHJvcGVydHlcbm1vZHVsZS5leHBvcnRzID0gIXJlcXVpcmUoJy4vX2ZhaWxzJykoZnVuY3Rpb24gKCkge1xuICByZXR1cm4gT2JqZWN0LmRlZmluZVByb3BlcnR5KHt9LCAnYScsIHsgZ2V0OiBmdW5jdGlvbiAoKSB7IHJldHVybiA3OyB9IH0pLmEgIT0gNztcbn0pO1xuIiwidmFyIGlzT2JqZWN0ID0gcmVxdWlyZSgnLi9faXMtb2JqZWN0Jyk7XG52YXIgZG9jdW1lbnQgPSByZXF1aXJlKCcuL19nbG9iYWwnKS5kb2N1bWVudDtcbi8vIHR5cGVvZiBkb2N1bWVudC5jcmVhdGVFbGVtZW50IGlzICdvYmplY3QnIGluIG9sZCBJRVxudmFyIGlzID0gaXNPYmplY3QoZG9jdW1lbnQpICYmIGlzT2JqZWN0KGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQpO1xubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAoaXQpIHtcbiAgcmV0dXJuIGlzID8gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChpdCkgOiB7fTtcbn07XG4iLCIvLyBJRSA4LSBkb24ndCBlbnVtIGJ1ZyBrZXlzXG5tb2R1bGUuZXhwb3J0cyA9IChcbiAgJ2NvbnN0cnVjdG9yLGhhc093blByb3BlcnR5LGlzUHJvdG90eXBlT2YscHJvcGVydHlJc0VudW1lcmFibGUsdG9Mb2NhbGVTdHJpbmcsdG9TdHJpbmcsdmFsdWVPZidcbikuc3BsaXQoJywnKTtcbiIsInZhciBnbG9iYWwgPSByZXF1aXJlKCcuL19nbG9iYWwnKTtcbnZhciBjb3JlID0gcmVxdWlyZSgnLi9fY29yZScpO1xudmFyIGN0eCA9IHJlcXVpcmUoJy4vX2N0eCcpO1xudmFyIGhpZGUgPSByZXF1aXJlKCcuL19oaWRlJyk7XG52YXIgaGFzID0gcmVxdWlyZSgnLi9faGFzJyk7XG52YXIgUFJPVE9UWVBFID0gJ3Byb3RvdHlwZSc7XG5cbnZhciAkZXhwb3J0ID0gZnVuY3Rpb24gKHR5cGUsIG5hbWUsIHNvdXJjZSkge1xuICB2YXIgSVNfRk9SQ0VEID0gdHlwZSAmICRleHBvcnQuRjtcbiAgdmFyIElTX0dMT0JBTCA9IHR5cGUgJiAkZXhwb3J0Lkc7XG4gIHZhciBJU19TVEFUSUMgPSB0eXBlICYgJGV4cG9ydC5TO1xuICB2YXIgSVNfUFJPVE8gPSB0eXBlICYgJGV4cG9ydC5QO1xuICB2YXIgSVNfQklORCA9IHR5cGUgJiAkZXhwb3J0LkI7XG4gIHZhciBJU19XUkFQID0gdHlwZSAmICRleHBvcnQuVztcbiAgdmFyIGV4cG9ydHMgPSBJU19HTE9CQUwgPyBjb3JlIDogY29yZVtuYW1lXSB8fCAoY29yZVtuYW1lXSA9IHt9KTtcbiAgdmFyIGV4cFByb3RvID0gZXhwb3J0c1tQUk9UT1RZUEVdO1xuICB2YXIgdGFyZ2V0ID0gSVNfR0xPQkFMID8gZ2xvYmFsIDogSVNfU1RBVElDID8gZ2xvYmFsW25hbWVdIDogKGdsb2JhbFtuYW1lXSB8fCB7fSlbUFJPVE9UWVBFXTtcbiAgdmFyIGtleSwgb3duLCBvdXQ7XG4gIGlmIChJU19HTE9CQUwpIHNvdXJjZSA9IG5hbWU7XG4gIGZvciAoa2V5IGluIHNvdXJjZSkge1xuICAgIC8vIGNvbnRhaW5zIGluIG5hdGl2ZVxuICAgIG93biA9ICFJU19GT1JDRUQgJiYgdGFyZ2V0ICYmIHRhcmdldFtrZXldICE9PSB1bmRlZmluZWQ7XG4gICAgaWYgKG93biAmJiBoYXMoZXhwb3J0cywga2V5KSkgY29udGludWU7XG4gICAgLy8gZXhwb3J0IG5hdGl2ZSBvciBwYXNzZWRcbiAgICBvdXQgPSBvd24gPyB0YXJnZXRba2V5XSA6IHNvdXJjZVtrZXldO1xuICAgIC8vIHByZXZlbnQgZ2xvYmFsIHBvbGx1dGlvbiBmb3IgbmFtZXNwYWNlc1xuICAgIGV4cG9ydHNba2V5XSA9IElTX0dMT0JBTCAmJiB0eXBlb2YgdGFyZ2V0W2tleV0gIT0gJ2Z1bmN0aW9uJyA/IHNvdXJjZVtrZXldXG4gICAgLy8gYmluZCB0aW1lcnMgdG8gZ2xvYmFsIGZvciBjYWxsIGZyb20gZXhwb3J0IGNvbnRleHRcbiAgICA6IElTX0JJTkQgJiYgb3duID8gY3R4KG91dCwgZ2xvYmFsKVxuICAgIC8vIHdyYXAgZ2xvYmFsIGNvbnN0cnVjdG9ycyBmb3IgcHJldmVudCBjaGFuZ2UgdGhlbSBpbiBsaWJyYXJ5XG4gICAgOiBJU19XUkFQICYmIHRhcmdldFtrZXldID09IG91dCA/IChmdW5jdGlvbiAoQykge1xuICAgICAgdmFyIEYgPSBmdW5jdGlvbiAoYSwgYiwgYykge1xuICAgICAgICBpZiAodGhpcyBpbnN0YW5jZW9mIEMpIHtcbiAgICAgICAgICBzd2l0Y2ggKGFyZ3VtZW50cy5sZW5ndGgpIHtcbiAgICAgICAgICAgIGNhc2UgMDogcmV0dXJuIG5ldyBDKCk7XG4gICAgICAgICAgICBjYXNlIDE6IHJldHVybiBuZXcgQyhhKTtcbiAgICAgICAgICAgIGNhc2UgMjogcmV0dXJuIG5ldyBDKGEsIGIpO1xuICAgICAgICAgIH0gcmV0dXJuIG5ldyBDKGEsIGIsIGMpO1xuICAgICAgICB9IHJldHVybiBDLmFwcGx5KHRoaXMsIGFyZ3VtZW50cyk7XG4gICAgICB9O1xuICAgICAgRltQUk9UT1RZUEVdID0gQ1tQUk9UT1RZUEVdO1xuICAgICAgcmV0dXJuIEY7XG4gICAgLy8gbWFrZSBzdGF0aWMgdmVyc2lvbnMgZm9yIHByb3RvdHlwZSBtZXRob2RzXG4gICAgfSkob3V0KSA6IElTX1BST1RPICYmIHR5cGVvZiBvdXQgPT0gJ2Z1bmN0aW9uJyA/IGN0eChGdW5jdGlvbi5jYWxsLCBvdXQpIDogb3V0O1xuICAgIC8vIGV4cG9ydCBwcm90byBtZXRob2RzIHRvIGNvcmUuJUNPTlNUUlVDVE9SJS5tZXRob2RzLiVOQU1FJVxuICAgIGlmIChJU19QUk9UTykge1xuICAgICAgKGV4cG9ydHMudmlydHVhbCB8fCAoZXhwb3J0cy52aXJ0dWFsID0ge30pKVtrZXldID0gb3V0O1xuICAgICAgLy8gZXhwb3J0IHByb3RvIG1ldGhvZHMgdG8gY29yZS4lQ09OU1RSVUNUT1IlLnByb3RvdHlwZS4lTkFNRSVcbiAgICAgIGlmICh0eXBlICYgJGV4cG9ydC5SICYmIGV4cFByb3RvICYmICFleHBQcm90b1trZXldKSBoaWRlKGV4cFByb3RvLCBrZXksIG91dCk7XG4gICAgfVxuICB9XG59O1xuLy8gdHlwZSBiaXRtYXBcbiRleHBvcnQuRiA9IDE7ICAgLy8gZm9yY2VkXG4kZXhwb3J0LkcgPSAyOyAgIC8vIGdsb2JhbFxuJGV4cG9ydC5TID0gNDsgICAvLyBzdGF0aWNcbiRleHBvcnQuUCA9IDg7ICAgLy8gcHJvdG9cbiRleHBvcnQuQiA9IDE2OyAgLy8gYmluZFxuJGV4cG9ydC5XID0gMzI7ICAvLyB3cmFwXG4kZXhwb3J0LlUgPSA2NDsgIC8vIHNhZmVcbiRleHBvcnQuUiA9IDEyODsgLy8gcmVhbCBwcm90byBtZXRob2QgZm9yIGBsaWJyYXJ5YFxubW9kdWxlLmV4cG9ydHMgPSAkZXhwb3J0O1xuIiwibW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAoZXhlYykge1xuICB0cnkge1xuICAgIHJldHVybiAhIWV4ZWMoKTtcbiAgfSBjYXRjaCAoZSkge1xuICAgIHJldHVybiB0cnVlO1xuICB9XG59O1xuIiwiLy8gaHR0cHM6Ly9naXRodWIuY29tL3psb2lyb2NrL2NvcmUtanMvaXNzdWVzLzg2I2lzc3VlY29tbWVudC0xMTU3NTkwMjhcbnZhciBnbG9iYWwgPSBtb2R1bGUuZXhwb3J0cyA9IHR5cGVvZiB3aW5kb3cgIT0gJ3VuZGVmaW5lZCcgJiYgd2luZG93Lk1hdGggPT0gTWF0aFxuICA/IHdpbmRvdyA6IHR5cGVvZiBzZWxmICE9ICd1bmRlZmluZWQnICYmIHNlbGYuTWF0aCA9PSBNYXRoID8gc2VsZlxuICAvLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmUgbm8tbmV3LWZ1bmNcbiAgOiBGdW5jdGlvbigncmV0dXJuIHRoaXMnKSgpO1xuaWYgKHR5cGVvZiBfX2cgPT0gJ251bWJlcicpIF9fZyA9IGdsb2JhbDsgLy8gZXNsaW50LWRpc2FibGUtbGluZSBuby11bmRlZlxuIiwidmFyIGhhc093blByb3BlcnR5ID0ge30uaGFzT3duUHJvcGVydHk7XG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIChpdCwga2V5KSB7XG4gIHJldHVybiBoYXNPd25Qcm9wZXJ0eS5jYWxsKGl0LCBrZXkpO1xufTtcbiIsInZhciBkUCA9IHJlcXVpcmUoJy4vX29iamVjdC1kcCcpO1xudmFyIGNyZWF0ZURlc2MgPSByZXF1aXJlKCcuL19wcm9wZXJ0eS1kZXNjJyk7XG5tb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoJy4vX2Rlc2NyaXB0b3JzJykgPyBmdW5jdGlvbiAob2JqZWN0LCBrZXksIHZhbHVlKSB7XG4gIHJldHVybiBkUC5mKG9iamVjdCwga2V5LCBjcmVhdGVEZXNjKDEsIHZhbHVlKSk7XG59IDogZnVuY3Rpb24gKG9iamVjdCwga2V5LCB2YWx1ZSkge1xuICBvYmplY3Rba2V5XSA9IHZhbHVlO1xuICByZXR1cm4gb2JqZWN0O1xufTtcbiIsIm1vZHVsZS5leHBvcnRzID0gIXJlcXVpcmUoJy4vX2Rlc2NyaXB0b3JzJykgJiYgIXJlcXVpcmUoJy4vX2ZhaWxzJykoZnVuY3Rpb24gKCkge1xuICByZXR1cm4gT2JqZWN0LmRlZmluZVByb3BlcnR5KHJlcXVpcmUoJy4vX2RvbS1jcmVhdGUnKSgnZGl2JyksICdhJywgeyBnZXQ6IGZ1bmN0aW9uICgpIHsgcmV0dXJuIDc7IH0gfSkuYSAhPSA3O1xufSk7XG4iLCIvLyBmYWxsYmFjayBmb3Igbm9uLWFycmF5LWxpa2UgRVMzIGFuZCBub24tZW51bWVyYWJsZSBvbGQgVjggc3RyaW5nc1xudmFyIGNvZiA9IHJlcXVpcmUoJy4vX2NvZicpO1xuLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lIG5vLXByb3RvdHlwZS1idWlsdGluc1xubW9kdWxlLmV4cG9ydHMgPSBPYmplY3QoJ3onKS5wcm9wZXJ0eUlzRW51bWVyYWJsZSgwKSA/IE9iamVjdCA6IGZ1bmN0aW9uIChpdCkge1xuICByZXR1cm4gY29mKGl0KSA9PSAnU3RyaW5nJyA/IGl0LnNwbGl0KCcnKSA6IE9iamVjdChpdCk7XG59O1xuIiwibW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAoaXQpIHtcbiAgcmV0dXJuIHR5cGVvZiBpdCA9PT0gJ29iamVjdCcgPyBpdCAhPT0gbnVsbCA6IHR5cGVvZiBpdCA9PT0gJ2Z1bmN0aW9uJztcbn07XG4iLCJtb2R1bGUuZXhwb3J0cyA9IHRydWU7XG4iLCJ2YXIgYW5PYmplY3QgPSByZXF1aXJlKCcuL19hbi1vYmplY3QnKTtcbnZhciBJRThfRE9NX0RFRklORSA9IHJlcXVpcmUoJy4vX2llOC1kb20tZGVmaW5lJyk7XG52YXIgdG9QcmltaXRpdmUgPSByZXF1aXJlKCcuL190by1wcmltaXRpdmUnKTtcbnZhciBkUCA9IE9iamVjdC5kZWZpbmVQcm9wZXJ0eTtcblxuZXhwb3J0cy5mID0gcmVxdWlyZSgnLi9fZGVzY3JpcHRvcnMnKSA/IE9iamVjdC5kZWZpbmVQcm9wZXJ0eSA6IGZ1bmN0aW9uIGRlZmluZVByb3BlcnR5KE8sIFAsIEF0dHJpYnV0ZXMpIHtcbiAgYW5PYmplY3QoTyk7XG4gIFAgPSB0b1ByaW1pdGl2ZShQLCB0cnVlKTtcbiAgYW5PYmplY3QoQXR0cmlidXRlcyk7XG4gIGlmIChJRThfRE9NX0RFRklORSkgdHJ5IHtcbiAgICByZXR1cm4gZFAoTywgUCwgQXR0cmlidXRlcyk7XG4gIH0gY2F0Y2ggKGUpIHsgLyogZW1wdHkgKi8gfVxuICBpZiAoJ2dldCcgaW4gQXR0cmlidXRlcyB8fCAnc2V0JyBpbiBBdHRyaWJ1dGVzKSB0aHJvdyBUeXBlRXJyb3IoJ0FjY2Vzc29ycyBub3Qgc3VwcG9ydGVkIScpO1xuICBpZiAoJ3ZhbHVlJyBpbiBBdHRyaWJ1dGVzKSBPW1BdID0gQXR0cmlidXRlcy52YWx1ZTtcbiAgcmV0dXJuIE87XG59O1xuIiwidmFyIGhhcyA9IHJlcXVpcmUoJy4vX2hhcycpO1xudmFyIHRvSU9iamVjdCA9IHJlcXVpcmUoJy4vX3RvLWlvYmplY3QnKTtcbnZhciBhcnJheUluZGV4T2YgPSByZXF1aXJlKCcuL19hcnJheS1pbmNsdWRlcycpKGZhbHNlKTtcbnZhciBJRV9QUk9UTyA9IHJlcXVpcmUoJy4vX3NoYXJlZC1rZXknKSgnSUVfUFJPVE8nKTtcblxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAob2JqZWN0LCBuYW1lcykge1xuICB2YXIgTyA9IHRvSU9iamVjdChvYmplY3QpO1xuICB2YXIgaSA9IDA7XG4gIHZhciByZXN1bHQgPSBbXTtcbiAgdmFyIGtleTtcbiAgZm9yIChrZXkgaW4gTykgaWYgKGtleSAhPSBJRV9QUk9UTykgaGFzKE8sIGtleSkgJiYgcmVzdWx0LnB1c2goa2V5KTtcbiAgLy8gRG9uJ3QgZW51bSBidWcgJiBoaWRkZW4ga2V5c1xuICB3aGlsZSAobmFtZXMubGVuZ3RoID4gaSkgaWYgKGhhcyhPLCBrZXkgPSBuYW1lc1tpKytdKSkge1xuICAgIH5hcnJheUluZGV4T2YocmVzdWx0LCBrZXkpIHx8IHJlc3VsdC5wdXNoKGtleSk7XG4gIH1cbiAgcmV0dXJuIHJlc3VsdDtcbn07XG4iLCIvLyAxOS4xLjIuMTQgLyAxNS4yLjMuMTQgT2JqZWN0LmtleXMoTylcbnZhciAka2V5cyA9IHJlcXVpcmUoJy4vX29iamVjdC1rZXlzLWludGVybmFsJyk7XG52YXIgZW51bUJ1Z0tleXMgPSByZXF1aXJlKCcuL19lbnVtLWJ1Zy1rZXlzJyk7XG5cbm1vZHVsZS5leHBvcnRzID0gT2JqZWN0LmtleXMgfHwgZnVuY3Rpb24ga2V5cyhPKSB7XG4gIHJldHVybiAka2V5cyhPLCBlbnVtQnVnS2V5cyk7XG59O1xuIiwiZXhwb3J0cy5mID0ge30ucHJvcGVydHlJc0VudW1lcmFibGU7XG4iLCIvLyBtb3N0IE9iamVjdCBtZXRob2RzIGJ5IEVTNiBzaG91bGQgYWNjZXB0IHByaW1pdGl2ZXNcbnZhciAkZXhwb3J0ID0gcmVxdWlyZSgnLi9fZXhwb3J0Jyk7XG52YXIgY29yZSA9IHJlcXVpcmUoJy4vX2NvcmUnKTtcbnZhciBmYWlscyA9IHJlcXVpcmUoJy4vX2ZhaWxzJyk7XG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIChLRVksIGV4ZWMpIHtcbiAgdmFyIGZuID0gKGNvcmUuT2JqZWN0IHx8IHt9KVtLRVldIHx8IE9iamVjdFtLRVldO1xuICB2YXIgZXhwID0ge307XG4gIGV4cFtLRVldID0gZXhlYyhmbik7XG4gICRleHBvcnQoJGV4cG9ydC5TICsgJGV4cG9ydC5GICogZmFpbHMoZnVuY3Rpb24gKCkgeyBmbigxKTsgfSksICdPYmplY3QnLCBleHApO1xufTtcbiIsInZhciBERVNDUklQVE9SUyA9IHJlcXVpcmUoJy4vX2Rlc2NyaXB0b3JzJyk7XG52YXIgZ2V0S2V5cyA9IHJlcXVpcmUoJy4vX29iamVjdC1rZXlzJyk7XG52YXIgdG9JT2JqZWN0ID0gcmVxdWlyZSgnLi9fdG8taW9iamVjdCcpO1xudmFyIGlzRW51bSA9IHJlcXVpcmUoJy4vX29iamVjdC1waWUnKS5mO1xubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAoaXNFbnRyaWVzKSB7XG4gIHJldHVybiBmdW5jdGlvbiAoaXQpIHtcbiAgICB2YXIgTyA9IHRvSU9iamVjdChpdCk7XG4gICAgdmFyIGtleXMgPSBnZXRLZXlzKE8pO1xuICAgIHZhciBsZW5ndGggPSBrZXlzLmxlbmd0aDtcbiAgICB2YXIgaSA9IDA7XG4gICAgdmFyIHJlc3VsdCA9IFtdO1xuICAgIHZhciBrZXk7XG4gICAgd2hpbGUgKGxlbmd0aCA+IGkpIHtcbiAgICAgIGtleSA9IGtleXNbaSsrXTtcbiAgICAgIGlmICghREVTQ1JJUFRPUlMgfHwgaXNFbnVtLmNhbGwoTywga2V5KSkge1xuICAgICAgICByZXN1bHQucHVzaChpc0VudHJpZXMgPyBba2V5LCBPW2tleV1dIDogT1trZXldKTtcbiAgICAgIH1cbiAgICB9XG4gICAgcmV0dXJuIHJlc3VsdDtcbiAgfTtcbn07XG4iLCJtb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIChiaXRtYXAsIHZhbHVlKSB7XG4gIHJldHVybiB7XG4gICAgZW51bWVyYWJsZTogIShiaXRtYXAgJiAxKSxcbiAgICBjb25maWd1cmFibGU6ICEoYml0bWFwICYgMiksXG4gICAgd3JpdGFibGU6ICEoYml0bWFwICYgNCksXG4gICAgdmFsdWU6IHZhbHVlXG4gIH07XG59O1xuIiwidmFyIHNoYXJlZCA9IHJlcXVpcmUoJy4vX3NoYXJlZCcpKCdrZXlzJyk7XG52YXIgdWlkID0gcmVxdWlyZSgnLi9fdWlkJyk7XG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIChrZXkpIHtcbiAgcmV0dXJuIHNoYXJlZFtrZXldIHx8IChzaGFyZWRba2V5XSA9IHVpZChrZXkpKTtcbn07XG4iLCJ2YXIgY29yZSA9IHJlcXVpcmUoJy4vX2NvcmUnKTtcbnZhciBnbG9iYWwgPSByZXF1aXJlKCcuL19nbG9iYWwnKTtcbnZhciBTSEFSRUQgPSAnX19jb3JlLWpzX3NoYXJlZF9fJztcbnZhciBzdG9yZSA9IGdsb2JhbFtTSEFSRURdIHx8IChnbG9iYWxbU0hBUkVEXSA9IHt9KTtcblxuKG1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKGtleSwgdmFsdWUpIHtcbiAgcmV0dXJuIHN0b3JlW2tleV0gfHwgKHN0b3JlW2tleV0gPSB2YWx1ZSAhPT0gdW5kZWZpbmVkID8gdmFsdWUgOiB7fSk7XG59KSgndmVyc2lvbnMnLCBbXSkucHVzaCh7XG4gIHZlcnNpb246IGNvcmUudmVyc2lvbixcbiAgbW9kZTogcmVxdWlyZSgnLi9fbGlicmFyeScpID8gJ3B1cmUnIDogJ2dsb2JhbCcsXG4gIGNvcHlyaWdodDogJ8KpIDIwMTkgRGVuaXMgUHVzaGthcmV2ICh6bG9pcm9jay5ydSknXG59KTtcbiIsInZhciB0b0ludGVnZXIgPSByZXF1aXJlKCcuL190by1pbnRlZ2VyJyk7XG52YXIgbWF4ID0gTWF0aC5tYXg7XG52YXIgbWluID0gTWF0aC5taW47XG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIChpbmRleCwgbGVuZ3RoKSB7XG4gIGluZGV4ID0gdG9JbnRlZ2VyKGluZGV4KTtcbiAgcmV0dXJuIGluZGV4IDwgMCA/IG1heChpbmRleCArIGxlbmd0aCwgMCkgOiBtaW4oaW5kZXgsIGxlbmd0aCk7XG59O1xuIiwiLy8gNy4xLjQgVG9JbnRlZ2VyXG52YXIgY2VpbCA9IE1hdGguY2VpbDtcbnZhciBmbG9vciA9IE1hdGguZmxvb3I7XG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIChpdCkge1xuICByZXR1cm4gaXNOYU4oaXQgPSAraXQpID8gMCA6IChpdCA+IDAgPyBmbG9vciA6IGNlaWwpKGl0KTtcbn07XG4iLCIvLyB0byBpbmRleGVkIG9iamVjdCwgdG9PYmplY3Qgd2l0aCBmYWxsYmFjayBmb3Igbm9uLWFycmF5LWxpa2UgRVMzIHN0cmluZ3NcbnZhciBJT2JqZWN0ID0gcmVxdWlyZSgnLi9faW9iamVjdCcpO1xudmFyIGRlZmluZWQgPSByZXF1aXJlKCcuL19kZWZpbmVkJyk7XG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIChpdCkge1xuICByZXR1cm4gSU9iamVjdChkZWZpbmVkKGl0KSk7XG59O1xuIiwiLy8gNy4xLjE1IFRvTGVuZ3RoXG52YXIgdG9JbnRlZ2VyID0gcmVxdWlyZSgnLi9fdG8taW50ZWdlcicpO1xudmFyIG1pbiA9IE1hdGgubWluO1xubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAoaXQpIHtcbiAgcmV0dXJuIGl0ID4gMCA/IG1pbih0b0ludGVnZXIoaXQpLCAweDFmZmZmZmZmZmZmZmZmKSA6IDA7IC8vIHBvdygyLCA1MykgLSAxID09IDkwMDcxOTkyNTQ3NDA5OTFcbn07XG4iLCIvLyA3LjEuMTMgVG9PYmplY3QoYXJndW1lbnQpXG52YXIgZGVmaW5lZCA9IHJlcXVpcmUoJy4vX2RlZmluZWQnKTtcbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKGl0KSB7XG4gIHJldHVybiBPYmplY3QoZGVmaW5lZChpdCkpO1xufTtcbiIsIi8vIDcuMS4xIFRvUHJpbWl0aXZlKGlucHV0IFssIFByZWZlcnJlZFR5cGVdKVxudmFyIGlzT2JqZWN0ID0gcmVxdWlyZSgnLi9faXMtb2JqZWN0Jyk7XG4vLyBpbnN0ZWFkIG9mIHRoZSBFUzYgc3BlYyB2ZXJzaW9uLCB3ZSBkaWRuJ3QgaW1wbGVtZW50IEBAdG9QcmltaXRpdmUgY2FzZVxuLy8gYW5kIHRoZSBzZWNvbmQgYXJndW1lbnQgLSBmbGFnIC0gcHJlZmVycmVkIHR5cGUgaXMgYSBzdHJpbmdcbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKGl0LCBTKSB7XG4gIGlmICghaXNPYmplY3QoaXQpKSByZXR1cm4gaXQ7XG4gIHZhciBmbiwgdmFsO1xuICBpZiAoUyAmJiB0eXBlb2YgKGZuID0gaXQudG9TdHJpbmcpID09ICdmdW5jdGlvbicgJiYgIWlzT2JqZWN0KHZhbCA9IGZuLmNhbGwoaXQpKSkgcmV0dXJuIHZhbDtcbiAgaWYgKHR5cGVvZiAoZm4gPSBpdC52YWx1ZU9mKSA9PSAnZnVuY3Rpb24nICYmICFpc09iamVjdCh2YWwgPSBmbi5jYWxsKGl0KSkpIHJldHVybiB2YWw7XG4gIGlmICghUyAmJiB0eXBlb2YgKGZuID0gaXQudG9TdHJpbmcpID09ICdmdW5jdGlvbicgJiYgIWlzT2JqZWN0KHZhbCA9IGZuLmNhbGwoaXQpKSkgcmV0dXJuIHZhbDtcbiAgdGhyb3cgVHlwZUVycm9yKFwiQ2FuJ3QgY29udmVydCBvYmplY3QgdG8gcHJpbWl0aXZlIHZhbHVlXCIpO1xufTtcbiIsInZhciBpZCA9IDA7XG52YXIgcHggPSBNYXRoLnJhbmRvbSgpO1xubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAoa2V5KSB7XG4gIHJldHVybiAnU3ltYm9sKCcuY29uY2F0KGtleSA9PT0gdW5kZWZpbmVkID8gJycgOiBrZXksICcpXycsICgrK2lkICsgcHgpLnRvU3RyaW5nKDM2KSk7XG59O1xuIiwidmFyICRleHBvcnQgPSByZXF1aXJlKCcuL19leHBvcnQnKTtcbi8vIDE5LjEuMi40IC8gMTUuMi4zLjYgT2JqZWN0LmRlZmluZVByb3BlcnR5KE8sIFAsIEF0dHJpYnV0ZXMpXG4kZXhwb3J0KCRleHBvcnQuUyArICRleHBvcnQuRiAqICFyZXF1aXJlKCcuL19kZXNjcmlwdG9ycycpLCAnT2JqZWN0JywgeyBkZWZpbmVQcm9wZXJ0eTogcmVxdWlyZSgnLi9fb2JqZWN0LWRwJykuZiB9KTtcbiIsIi8vIDE5LjEuMi4xNCBPYmplY3Qua2V5cyhPKVxudmFyIHRvT2JqZWN0ID0gcmVxdWlyZSgnLi9fdG8tb2JqZWN0Jyk7XG52YXIgJGtleXMgPSByZXF1aXJlKCcuL19vYmplY3Qta2V5cycpO1xuXG5yZXF1aXJlKCcuL19vYmplY3Qtc2FwJykoJ2tleXMnLCBmdW5jdGlvbiAoKSB7XG4gIHJldHVybiBmdW5jdGlvbiBrZXlzKGl0KSB7XG4gICAgcmV0dXJuICRrZXlzKHRvT2JqZWN0KGl0KSk7XG4gIH07XG59KTtcbiIsIi8vIGh0dHBzOi8vZ2l0aHViLmNvbS90YzM5L3Byb3Bvc2FsLW9iamVjdC12YWx1ZXMtZW50cmllc1xudmFyICRleHBvcnQgPSByZXF1aXJlKCcuL19leHBvcnQnKTtcbnZhciAkdmFsdWVzID0gcmVxdWlyZSgnLi9fb2JqZWN0LXRvLWFycmF5JykoZmFsc2UpO1xuXG4kZXhwb3J0KCRleHBvcnQuUywgJ09iamVjdCcsIHtcbiAgdmFsdWVzOiBmdW5jdGlvbiB2YWx1ZXMoaXQpIHtcbiAgICByZXR1cm4gJHZhbHVlcyhpdCk7XG4gIH1cbn0pO1xuIiwibW9kdWxlLmV4cG9ydHMgPSB3aW5kb3dbXCJqUXVlcnlcIl07IiwiLy8gVGhlIG1vZHVsZSBjYWNoZVxudmFyIF9fd2VicGFja19tb2R1bGVfY2FjaGVfXyA9IHt9O1xuXG4vLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcblx0dmFyIGNhY2hlZE1vZHVsZSA9IF9fd2VicGFja19tb2R1bGVfY2FjaGVfX1ttb2R1bGVJZF07XG5cdGlmIChjYWNoZWRNb2R1bGUgIT09IHVuZGVmaW5lZCkge1xuXHRcdHJldHVybiBjYWNoZWRNb2R1bGUuZXhwb3J0cztcblx0fVxuXHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuXHR2YXIgbW9kdWxlID0gX193ZWJwYWNrX21vZHVsZV9jYWNoZV9fW21vZHVsZUlkXSA9IHtcblx0XHQvLyBubyBtb2R1bGUuaWQgbmVlZGVkXG5cdFx0Ly8gbm8gbW9kdWxlLmxvYWRlZCBuZWVkZWRcblx0XHRleHBvcnRzOiB7fVxuXHR9O1xuXG5cdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuXHRfX3dlYnBhY2tfbW9kdWxlc19fW21vZHVsZUlkXShtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuXHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuXHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG59XG5cbiIsIi8qKlxuICogQ29weXJpZ2h0IHNpbmNlIDIwMDcgUHJlc3RhU2hvcCBTQSBhbmQgQ29udHJpYnV0b3JzXG4gKiBQcmVzdGFTaG9wIGlzIGFuIEludGVybmF0aW9uYWwgUmVnaXN0ZXJlZCBUcmFkZW1hcmsgJiBQcm9wZXJ0eSBvZiBQcmVzdGFTaG9wIFNBXG4gKlxuICogTk9USUNFIE9GIExJQ0VOU0VcbiAqXG4gKiBUaGlzIHNvdXJjZSBmaWxlIGlzIHN1YmplY3QgdG8gdGhlIE9wZW4gU29mdHdhcmUgTGljZW5zZSAoT1NMIDMuMClcbiAqIHRoYXQgaXMgYnVuZGxlZCB3aXRoIHRoaXMgcGFja2FnZSBpbiB0aGUgZmlsZSBMSUNFTlNFLm1kLlxuICogSXQgaXMgYWxzbyBhdmFpbGFibGUgdGhyb3VnaCB0aGUgd29ybGQtd2lkZS13ZWIgYXQgdGhpcyBVUkw6XG4gKiBodHRwczovL29wZW5zb3VyY2Uub3JnL2xpY2Vuc2VzL09TTC0zLjBcbiAqIElmIHlvdSBkaWQgbm90IHJlY2VpdmUgYSBjb3B5IG9mIHRoZSBsaWNlbnNlIGFuZCBhcmUgdW5hYmxlIHRvXG4gKiBvYnRhaW4gaXQgdGhyb3VnaCB0aGUgd29ybGQtd2lkZS13ZWIsIHBsZWFzZSBzZW5kIGFuIGVtYWlsXG4gKiB0byBsaWNlbnNlQHByZXN0YXNob3AuY29tIHNvIHdlIGNhbiBzZW5kIHlvdSBhIGNvcHkgaW1tZWRpYXRlbHkuXG4gKlxuICogRElTQ0xBSU1FUlxuICpcbiAqIERvIG5vdCBlZGl0IG9yIGFkZCB0byB0aGlzIGZpbGUgaWYgeW91IHdpc2ggdG8gdXBncmFkZSBQcmVzdGFTaG9wIHRvIG5ld2VyXG4gKiB2ZXJzaW9ucyBpbiB0aGUgZnV0dXJlLiBJZiB5b3Ugd2lzaCB0byBjdXN0b21pemUgUHJlc3RhU2hvcCBmb3IgeW91clxuICogbmVlZHMgcGxlYXNlIHJlZmVyIHRvIGh0dHBzOi8vZGV2ZG9jcy5wcmVzdGFzaG9wLmNvbS8gZm9yIG1vcmUgaW5mb3JtYXRpb24uXG4gKlxuICogQGF1dGhvciAgICBQcmVzdGFTaG9wIFNBIGFuZCBDb250cmlidXRvcnMgPGNvbnRhY3RAcHJlc3Rhc2hvcC5jb20+XG4gKiBAY29weXJpZ2h0IFNpbmNlIDIwMDcgUHJlc3RhU2hvcCBTQSBhbmQgQ29udHJpYnV0b3JzXG4gKiBAbGljZW5zZSAgIGh0dHBzOi8vb3BlbnNvdXJjZS5vcmcvbGljZW5zZXMvT1NMLTMuMCBPcGVuIFNvZnR3YXJlIExpY2Vuc2UgKE9TTCAzLjApXG4gKi9cblxuaW1wb3J0IEVtcGxveWVlRm9ybSBmcm9tICcuL0VtcGxveWVlRm9ybSc7XG5cbiQoKCkgPT4ge1xuICBuZXcgRW1wbG95ZWVGb3JtKCk7XG59KTtcbiJdLCJzb3VyY2VSb290IjoiIn0=